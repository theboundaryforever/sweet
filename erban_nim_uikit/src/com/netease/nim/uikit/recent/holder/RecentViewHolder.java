package com.netease.nim.uikit.recent.holder;

import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.R;
import com.netease.nim.uikit.cache.TeamDataCache;
import com.netease.nim.uikit.common.ui.drop.DropFake;
import com.netease.nim.uikit.common.ui.drop.DropManager;
import com.netease.nim.uikit.common.ui.imageview.CircleImageView;
import com.netease.nim.uikit.common.ui.recyclerview.adapter.BaseQuickAdapter;
import com.netease.nim.uikit.common.ui.recyclerview.holder.BaseViewHolder;
import com.netease.nim.uikit.common.ui.recyclerview.holder.RecyclerViewHolder;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.netease.nim.uikit.common.util.sys.TimeUtil;
import com.netease.nim.uikit.recent.RecentContactsCallback;
import com.netease.nim.uikit.recent.adapter.RecentContactAdapter;
import com.netease.nim.uikit.session.OfficSecrRedPointCountManager;
import com.netease.nim.uikit.session.emoji.MoonUtil;
import com.netease.nim.uikit.uinfo.UserInfoHelper;
import com.netease.nim.uikit.utils.ImageLoadUtils;
import com.netease.nimlib.sdk.msg.constant.MsgStatusEnum;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.RecentContact;
import com.netease.nimlib.sdk.team.model.Team;
import com.netease.nimlib.sdk.uinfo.model.UserInfo;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.LogUtils;

public abstract class RecentViewHolder extends RecyclerViewHolder<BaseQuickAdapter, BaseViewHolder, RecentContact> {

    private final String TAG = RecentViewHolder.class.getSimpleName();

    public RecentViewHolder(BaseQuickAdapter adapter) {
        super(adapter);
    }

    private int lastUnreadCount = 0;

    protected CircleImageView imgHead;

    protected TextView tvNickname;

    protected TextView tvMessage;

    protected TextView tvDatetime;

    // 消息发送错误状态标记，目前没有逻辑处理
    protected ImageView imgMsgStatus;

    // 未读红点（一个占坑，一个全屏动画）
    protected DropFake tvUnread;
    protected TextView tvMsgNumber;
    private ImageView imgUnreadExplosion;

    protected TextView tvOnlineState;

    private ImageView ivOffice;

    // 子类覆写
    protected abstract String getContent(RecentContact recent);

    @Override
    public void convert(BaseViewHolder holder, RecentContact data, int position, boolean isScrolling) {
        inflate(holder, data);
        refresh(holder, data, position);
    }

    public void inflate(BaseViewHolder holder, final RecentContact recent) {
        this.imgHead = holder.getView(R.id.img_head);
        this.tvNickname = holder.getView(R.id.tv_nickname);
        this.tvMessage = holder.getView(R.id.tv_message);
        this.tvUnread = holder.getView(R.id.unread_number_tip);
        this.tvMsgNumber = holder.getView(R.id.tvMsgNumber);
        this.imgUnreadExplosion = holder.getView(R.id.unread_number_explosion);
        this.tvDatetime = holder.getView(R.id.tv_date_time);
        this.imgMsgStatus = holder.getView(R.id.img_msg_status);
        this.tvOnlineState = holder.getView(R.id.tv_online_state);
        this.ivOffice = holder.getView(R.id.ivOffice);
        holder.addOnClickListener(R.id.unread_number_tip);

        this.tvUnread.setTouchListener(new DropFake.ITouchListener() {
            @Override
            public void onDown() {
                DropManager.getInstance().setCurrentId(recent);
                DropManager.getInstance().down(tvUnread, tvUnread.getText());
            }

            @Override
            public void onMove(float curX, float curY) {
                DropManager.getInstance().move(curX, curY);
            }

            @Override
            public void onUp() {
                DropManager.getInstance().up();
            }
        });
    }

    public void refresh(BaseViewHolder holder, RecentContact recent, final int position) {
        // unread count animation
        int showUnreadNum = recent.getUnreadCount();
        // 未读数从N->0执行爆裂动画;
        boolean shouldBoom = lastUnreadCount > 0 && showUnreadNum == 0;
        lastUnreadCount = showUnreadNum;

        lastUnreadCount = recent.getUnreadCount();

        updateBackground(holder, recent, position);

        loadPortrait(recent);

        updateNickLabel(UserInfoHelper.getUserTitleName(recent.getContactId(), recent.getSessionType()));

        updateOnlineState(recent);

        updateMsgLabel(holder, recent);

        updateNewIndicator(recent);

        if (shouldBoom) {
            Object o = DropManager.getInstance().getCurrentId();
            if (o instanceof String && o.equals("0")) {
                imgUnreadExplosion.setImageResource(R.drawable.explosion);
                imgUnreadExplosion.setVisibility(View.VISIBLE);
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        ((AnimationDrawable) imgUnreadExplosion.getDrawable()).start();
                        // 解决部分手机动画无法播放的问题（例如华为荣耀）
                        getAdapter().notifyItemChanged(getAdapter().getViewHolderPosition(position));
                    }
                });
            }
        } else {
            imgUnreadExplosion.setVisibility(View.GONE);
        }
    }

    private void updateBackground(BaseViewHolder holder, RecentContact recent, int position) {
//        if ((recent.getTag() & RecentContactsFragment.RECENT_TAG_STICKY) == 0) {
//            holder.getConvertView().setBackgroundResource(R.drawable.bg_common_touch_while);
//        } else {
//            holder.getConvertView().setBackgroundResource(R.drawable.nim_recent_contact_sticky_selecter);
//        }
    }

    protected void loadPortrait(RecentContact recent) {
        // 设置头像
        int imgWidthHeight = DisplayUtility.dp2px(imgHead.getContext(), 60);
        if (recent.getSessionType() == SessionTypeEnum.P2P) {
            boolean isOfficeAccount = OfficSecrRedPointCountManager.isOfficSecrAccount(recent.getContactId());
            final UserInfo userInfo = NimUIKit.getUserInfoProvider().getUserInfo(recent.getContactId());
            if (null != userInfo) {
                if (!isOfficeAccount) {
                    ImageLoadUtils.loadBannerRoundBg(imgHead.getContext(),
                            ImageLoadUtils.toThumbnailUrl(imgWidthHeight, imgWidthHeight, userInfo.getAvatar()),
                            imgHead,
                            imgWidthHeight,
                            R.drawable.bg_default_cover_round_placehold_size60);
                } else {
                    imgHead.setImageDrawable(imgHead.getContext().getResources().getDrawable(R.mipmap.ic_office_avatar));
                }
            }

            ivOffice.setVisibility(isOfficeAccount ? View.VISIBLE : View.GONE);
        } else if (recent.getSessionType() == SessionTypeEnum.Team) {
            Team team = TeamDataCache.getInstance().getTeamById(recent.getContactId());
            if (null != team) {
                ImageLoadUtils.loadBannerRoundBg(imgHead.getContext(),
                        ImageLoadUtils.toThumbnailUrl(imgWidthHeight, imgWidthHeight, team.getIcon()),
                        imgHead,
                        imgWidthHeight,
                        R.drawable.bg_default_cover_round_placehold_size60);
            }
        }
    }

    private void updateNewIndicator(RecentContact recent) {
        int unreadNum = recent.getUnreadCount();
        LogUtils.d(TAG, "updateNewIndicator-cid:" + recent.getContactId() + " unreadNum:" + unreadNum);
        if (OfficSecrRedPointCountManager.isOfficSecrAccount(recent.getContactId())) {
            unreadNum -= OfficSecrRedPointCountManager.getRedCountDelNum();
            LogUtils.d(TAG, "updateNewIndicator-cid:" + recent.getContactId() + " unreadNum2:" + unreadNum);
        }
        //替换为自实现红点未读数，动效暂时砍掉不要
        tvUnread.setVisibility(unreadNum > 0 ? View.GONE : View.GONE);
        tvUnread.setText(unreadCountShowRule(unreadNum));

        tvMsgNumber.setVisibility(unreadNum > 0 ? View.VISIBLE : View.GONE);
        tvMsgNumber.setText(unreadCountShowRule(unreadNum));
    }

    private void updateMsgLabel(BaseViewHolder holder, RecentContact recent) {
        // 显示消息具体内容
        MoonUtil.identifyRecentVHFaceExpressionAndTags(holder.getContext(), tvMessage, getContent(recent), -1, 0.45f);
        //tvMessage.setText(getContent());

        MsgStatusEnum status = recent.getMsgStatus();
        switch (status) {
            case fail:
                imgMsgStatus.setImageResource(R.drawable.nim_g_ic_failed_small);
                imgMsgStatus.setVisibility(View.VISIBLE);
                break;
            case sending:
                imgMsgStatus.setImageResource(R.drawable.nim_recent_contact_ic_sending);
                imgMsgStatus.setVisibility(View.VISIBLE);
                break;
            default:
                imgMsgStatus.setVisibility(View.GONE);
                break;
        }

        String timeString = TimeUtil.getTimeShowString(recent.getTime(), true);
        tvDatetime.setText(timeString);
    }

    protected String getOnlineStateContent(RecentContact recent) {
        return "";
    }

    protected void updateOnlineState(RecentContact recent) {
        if (recent.getSessionType() == SessionTypeEnum.Team) {
            tvOnlineState.setVisibility(View.GONE);
        } else {
            String onlineStateContent = getOnlineStateContent(recent);
            if (TextUtils.isEmpty(onlineStateContent)) {
                tvOnlineState.setVisibility(View.GONE);
            } else {
                //在线状态暂时隐藏掉
                tvOnlineState.setVisibility(View.GONE);
                tvOnlineState.setText(getOnlineStateContent(recent));
            }
        }
    }

    protected void updateNickLabel(String nick) {
        int labelWidth = ScreenUtil.screenWidth;
        // 减去固定的头像和时间宽度
        labelWidth -= ScreenUtil.dip2px(50 + 70);

        if (labelWidth > 0) {
            tvNickname.setMaxWidth(labelWidth);
        }

        tvNickname.setText(nick);
    }

    protected String unreadCountShowRule(int unread) {
        unread = Math.min(unread, 99);
        return String.valueOf(unread);
    }

    protected RecentContactsCallback getCallback() {
        return ((RecentContactAdapter) getAdapter()).getCallback();
    }
}
