package com.netease.nim.uikit.session;

import android.content.Context;
import android.text.TextUtils;

import com.netease.nim.uikit.NimUIKit;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.msg.MsgService;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.RecentContact;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;

import java.util.List;

/**
 * 专门用于记录甜甜小秘书帐号下发的，自定义私聊通知类消息，运行期间接收到的未读数
 * <p>
 * 1.启动后，接收到自定义私聊通知类消息，则自动计数+1
 * 2.消息列表，打开官方小秘书帐号，则清除计数
 * 3.如果当前私聊对象是官方小秘书，则不做计数处理 -- 云信SDK内部处理
 * 4.更新首页tab未读数红点，自动减去该计数
 * 5.最近联系人列表，官方小秘书帐号的未读数，减去该计数
 * <p>
 * 最后，理想状态下会剩下一种场景的bug，首次登录，拉取离线的
 */
public class OfficSecrRedPointCountManager {

    //线下官方小秘书帐号
    public static final String officSecrAccountDebug = "90000030";
    //线上官方小秘书帐号
    public static final String officSecrAccountRelease = "100001";
    private static final String TAG = OfficSecrRedPointCountManager.class.getSimpleName();
    private static int redCountDelNum = 0;

    public static boolean isOfficSecrAccount(String account) {
        boolean isOfficSecrAccount = !TextUtils.isEmpty(account) && account.equals(
                BasicConfig.isDebug ? officSecrAccountDebug : officSecrAccountRelease);
        LogUtils.d(TAG, "isOfficSecrAccount-account:" + account + " ");
        return isOfficSecrAccount;
    }

    public static void addRedCountDelNum() {
        OfficSecrRedPointCountManager.redCountDelNum += 1;
    }

    public static void delRedCountDelNum() {
        OfficSecrRedPointCountManager.redCountDelNum = 0;
    }

    public static int getRedCountDelNum() {
        return redCountDelNum;
    }

    /**
     * 忽略所有未读消息
     *
     * @param recentContacts
     */
    public static void changeUnreadMsgToReaded(List<RecentContact> recentContacts) {
        LogUtils.d(TAG, "changeUnreadMsgToReaded");
        for (RecentContact recentContact : recentContacts) {
            if (recentContact.getUnreadCount() > 0) {
                LogUtils.d(TAG, "changeUnreadMsgToReaded-cid:" + recentContact.getContactId()
                        + " unReadCount:" + recentContact.getUnreadCount());
                //直接设置会话状态，并清空未读数
                NIMSessionStatusManager.getInstance().updateChattingAccount(recentContact.getContactId(), SessionTypeEnum.None);
                NIMClient.getService(MsgService.class).clearUnreadCount(recentContact.getContactId(), SessionTypeEnum.P2P);
                //清空私聊会话自定义消息
                if (null != NimUIKit.onP2PSessionOpenListener) {
                    NimUIKit.onP2PSessionOpenListener.onP2PSessionOpened(recentContact.getContactId());
                }
            }
        }
        //注意：这里因为tmxq 1.0.0.1版本的忽略未读消息入口放在消息中心的右上角菜单界面中，因此还需要修改状态回去
        LogUtils.d(TAG, "changeUnreadMsgToReaded-修改状态为[进入最近联系人列表界面]");
        NIMSessionStatusManager.getInstance().updateChattingAccount(MsgService.MSG_CHATTING_ACCOUNT_ALL, SessionTypeEnum.None);
        //需要将私聊消息中过滤的自定义消息通知对应的未读数清零
        delRedCountDelNum();
    }

    public static int likeCount = 0;//点赞消息数量单独列出来

    public static void save(Context context, long uid) {
        try {
            SpUtils.put(context, "user_like_count" + uid, likeCount);
            SpUtils.put(context, "user_del_count" + uid, redCountDelNum);
            SpUtils.put(context, "user_sysmsg_count" + uid, sysMsgNum);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void restore(Context context, long uid) {
        try {
            likeCount = (int) SpUtils.get(context, "user_like_count" + uid, 0);
            redCountDelNum = (int) SpUtils.get(context, "user_del_count" + uid, 0);
            sysMsgNum = (int) SpUtils.get(context, "user_sysmsg_count" + uid, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //线下官方系统通知帐号
    public static final String officMsgAccountDebug = "90000031";
    //线上官方系统通知帐号
    public static final String officMsgAccountRelease = "832615";
    public static int sysMsgNum = 0;

    //是否是官方系统消息账号
    public static boolean isOfficMsgAccount(String account) {
        boolean isOfficSecrAccount = !TextUtils.isEmpty(account) && account.equals(
                BasicConfig.isDebug ? officMsgAccountDebug : officMsgAccountRelease);
        LogUtils.d(TAG, "isOfficSecrAccount-account:" + account + " ");
        return isOfficSecrAccount;
    }
}
