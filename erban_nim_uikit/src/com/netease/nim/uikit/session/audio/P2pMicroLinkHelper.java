package com.netease.nim.uikit.session.audio;

import com.netease.nim.uikit.NimUIKit;

public class P2pMicroLinkHelper {

    private static P2pMicroLinkObservable p2pMicroLinkObservable;

    public static void registerObserver(P2pMicroLinkObservable.P2pMicroLinkObserver observer) {
        if (p2pMicroLinkObservable == null) {
            p2pMicroLinkObservable = new P2pMicroLinkObservable(NimUIKit.getContext());
        }
        p2pMicroLinkObservable.registerObserver(observer);
    }

    public static void unregisterObserver(P2pMicroLinkObservable.P2pMicroLinkObserver observer) {
        if (p2pMicroLinkObservable != null) {
            p2pMicroLinkObservable.unregisterObserver(observer);
        }
    }

    public static void notifyChanged() {
        if (p2pMicroLinkObservable != null) {
            p2pMicroLinkObservable.notifyObservers();
        }
    }
}
