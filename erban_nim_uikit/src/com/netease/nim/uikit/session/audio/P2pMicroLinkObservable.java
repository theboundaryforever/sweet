package com.netease.nim.uikit.session.audio;

import android.content.Context;
import android.os.Handler;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户资料变动观察者。
 */
public class P2pMicroLinkObservable {

    private List<P2pMicroLinkObserver> observers = new ArrayList<>();
    private Handler uiHandler;

    public P2pMicroLinkObservable(Context context) {
        uiHandler = new Handler(context.getMainLooper());
    }

    synchronized public void registerObserver(P2pMicroLinkObserver observer) {
        if (observer != null) {
            observers.add(observer);
        }
    }

    synchronized public void unregisterObserver(P2pMicroLinkObserver observer) {
        if (observer != null) {
            observers.remove(observer);
        }
    }

    synchronized public void notifyObservers() {
        uiHandler.post(new Runnable() {
            @Override
            public void run() {
                for (P2pMicroLinkObserver observer : observers) {
                    observer.onDismissAfterEndLinkMicro();
                }
            }
        });
    }

    public interface P2pMicroLinkObserver {
        void onDismissAfterEndLinkMicro();
    }
}
