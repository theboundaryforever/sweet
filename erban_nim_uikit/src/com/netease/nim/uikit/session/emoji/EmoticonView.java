package com.netease.nim.uikit.session.emoji;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.juxiao.library_utils.DisplayUtils;
import com.netease.nim.uikit.R;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.tongdaxing.erban.libcommon.utils.LogUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 贴图显示viewpager
 */
public class EmoticonView {

    public static final int EMOJI_PER_LINE = 7;

    private ViewPager emotPager;
    private LinearLayout pageNumberLayout;
    /**
     * 总页数.
     */
    private int pageCount;

    /**
     * 每页显示的数量，Adapter保持一致.
     */
    public static final int EMOJI_PER_PAGE = 27; // 最后一个是删除键
    public static final int SERVER_EMOJI_PER_PAGE = 8;
    public static final int STICKER_PER_PAGE = 8;
    public static final int SERVER_EMOJI_PER_LINE = 4;
    private final String TAG = EmoticonView.class.getSimpleName();

    private Context context;
    private IEmoticonSelectedListener listener;
    private EmoticonViewPaperAdapter pagerAdapter = new EmoticonViewPaperAdapter();

    /**
     * 所有表情贴图支持横向滑动切换
     */
    private int categoryIndex;                           // 当套贴图的在picker中的索引
    private boolean isDataInitialized = false;             // 数据源只需要初始化一次,变更时再初始化
    private List<StickerCategory> categoryDataList;       // 表情贴图数据源
    private List<Integer> categoryPageNumberList;           // 每套表情贴图对应的页数
    private int[] pagerIndexInfo = new int[2];           // 0：category index；1：pager index in category
    private IEmoticonCategoryChanged categoryChangedCallback; // 横向滑动切换时回调picker
    public OnItemClickListener emojiListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
            int cPosition = emotPager.getCurrentItem();
            int pos = cPosition; // 如果只有表情，那么用默认方式计算
            if (categoryDataList != null && categoryPageNumberList != null) {
                // 包含贴图
                getPagerInfo(cPosition);
                pos = pagerIndexInfo[1];
            }

            if (listener != null) {
                if (pagerIndexInfo[0] == EmoticonPickerView.defaultEmojiTabIndex) {
                    //默认云信UI库自带表情
                    int index = position + pos * EMOJI_PER_PAGE;
                    int count = EmojiManager.getDisplayCount();
                    if (position == EMOJI_PER_PAGE || index >= count) {
                        listener.onEmojiSelected("/DEL", false);
                    } else {
                        String text = EmojiManager.getDisplayText((int) id);
                        if (!TextUtils.isEmpty(text)) {
                            listener.onEmojiSelected(text, false);
                        }
                    }
                } else if (pagerIndexInfo[0] == EmoticonPickerView.normalEmojiTabIndex) {
                    //普通表情
                    int index = position + pos * SERVER_EMOJI_PER_PAGE;
                    int count = 0;

                    if (null != SerEmojiTopicDataManager.getInstance().serverNormalEmojiList) {
                        count = SerEmojiTopicDataManager.getInstance().serverNormalEmojiList.size();
                    }

                    if (count > 0 && index < count) {
                        String text = SerEmojiTopicDataManager.getInstance().serverNormalEmojiList.get(index).getName();
                        if (!TextUtils.isEmpty(text)) {
                            listener.onEmojiSelected(text, true);
                        }
                    } else {
                        //删掉<DEL表情
                    }
                } else if (pagerIndexInfo[0] == EmoticonPickerView.hotEmojiTabIndex) {
                    //普通表情
                    int index = position + pos * SERVER_EMOJI_PER_PAGE;
                    int count = 0;

                    if (null != SerEmojiTopicDataManager.getInstance().serverHotEmojiList) {
                        count = SerEmojiTopicDataManager.getInstance().serverHotEmojiList.size();
                    }

                    if (count > 0 && index < count) {
                        String text = SerEmojiTopicDataManager.getInstance().serverHotEmojiList.get(index).getName();
                        if (!TextUtils.isEmpty(text)) {
                            listener.onEmojiSelected(text, true);
                        }
                    } else {
                        //删掉<DEL表情
                    }
                }
            }
        }
    };

    public void setCategoryDataReloadFlag() {
        isDataInitialized = false;
    }

    private OnItemClickListener stickerListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
            int position = emotPager.getCurrentItem();
            getPagerInfo(position);
            int cIndex = pagerIndexInfo[0];
            int pos = pagerIndexInfo[1];
            StickerCategory c = categoryDataList.get(cIndex);
            int index = arg2 + pos * STICKER_PER_PAGE; // 在category中贴图的index

            if (index >= c.getStickers().size()) {
                LogUtil.i("sticker", "index " + index + " larger than size " + c.getStickers().size());
                return;
            }

            if (listener != null) {
                StickerManager manager = StickerManager.getInstance();
                List<StickerItem> stickers = c.getStickers();
                StickerItem sticker = stickers.get(index);
                StickerCategory real = manager.getCategory(sticker.getCategory());

                if (real == null) {
                    return;
                }

                listener.onStickerSelected(sticker.getCategory(), sticker.getName());
            }
        }
    };

    public EmoticonView(Context context, IEmoticonSelectedListener mlistener,
                        ViewPager mCurPage, LinearLayout pageNumberLayout) {
        this.context = context.getApplicationContext();
        this.listener = mlistener;
        this.pageNumberLayout = pageNumberLayout;
        this.emotPager = mCurPage;

        emotPager.setOnPageChangeListener(new OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                LogUtils.d(TAG, "onPageSelected-position:" + position);
                if (categoryDataList != null) {
                    // 显示所有贴图表情
                    setCurStickerPage(position);
                    if (categoryChangedCallback != null) {
                        // 当前哪种类别被选中
                        int currentCategoryChecked = pagerIndexInfo[0];
                        LogUtils.d(TAG, "onPageSelected-currentCategoryChecked:" + currentCategoryChecked);
                        categoryChangedCallback.onCategoryChanged(currentCategoryChecked);
                    }
                } else {
                    // 只显示表情
                    setCurEmotionPage(position);
                }
            }

            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        emotPager.setAdapter(pagerAdapter);
        emotPager.setOffscreenPageLimit(5);
    }

    public void showStickers(int index) {
        LogUtils.d(TAG, "showStickers index:" + index);
        // 判断是否需要变化
        if (isDataInitialized && getPagerInfo(emotPager.getCurrentItem()) != null
                && pagerIndexInfo[0] == index && pagerIndexInfo[1] == 0) {
            LogUtils.d(TAG, "showStickers 条件判断,满足拦截,isDataInitialized:" + isDataInitialized
                    + " pagerIndexInfo[0]:" + pagerIndexInfo[0]
                    + " pagerIndexInfo[1]:" + pagerIndexInfo[1]
                    + " ,忽略本次拦截");
            //解决滑动pageview，以切换tab时，数据加载出错的bug
//            return;
        }

        this.categoryIndex = index;
        LogUtils.d(TAG, "showStickers-->showStickerGridView categoryIndex:" + categoryIndex);
        showStickerGridView();
    }

    public void showEmojis() {
        LogUtils.d(TAG, "showEmojis-->showEmojis");
        showEmojiGridView();
    }

    /**
     * @param category
     * @param type     0-自带表情，1-普通表情，2-热门表情，3-贴图（若有）
     * @return
     */
    private int getCategoryPageCount(StickerCategory category, int type) {
        LogUtils.d(TAG, "getCategoryPageCount-category:" + category + " type:" + type);
        if (category == null) {
            //不小于 xx 的最小整数
            //category == null目前始终为true，仅展示表情
            if (EmoticonPickerView.defaultEmojiTabIndex == type) {
                return (int) Math.ceil(EmojiManager.getDisplayCount() / (float) EMOJI_PER_PAGE);
            } else if (EmoticonPickerView.normalEmojiTabIndex == type) {
                return (int) Math.ceil(SerEmojiTopicDataManager.getInstance().serverNormalEmojiList.size() / (float) SERVER_EMOJI_PER_PAGE);
            } else if (EmoticonPickerView.hotEmojiTabIndex == type) {
                return (int) Math.ceil(SerEmojiTopicDataManager.getInstance().serverHotEmojiList.size() / (float) SERVER_EMOJI_PER_PAGE);
            } else {
                return 1;
            }

        } else {
            if (category.hasStickers() && EmoticonPickerView.chartletTabIndex <= type) {
                List<StickerItem> stickers = category.getStickers();
                return (int) Math.ceil(stickers.size() / (float) STICKER_PER_PAGE);
            } else {
                return 1;
            }
        }
    }

    /**
     * 更新页面指示器的选中状态
     *
     * @param page
     * @param pageCount
     */
    private void setCurPage(int page, int pageCount) {
        LogUtils.d(TAG, "setCurPage-page:" + page + " pageCount:" + pageCount);
        int hasCount = pageNumberLayout.getChildCount();
        int forMax = Math.max(hasCount, pageCount);
        LogUtils.d(TAG, "setCurPage-hasCount:" + hasCount + " forMax:" + forMax);
        ImageView imgCur = null;
        for (int i = 0; i < forMax; i++) {
            if (pageCount <= hasCount) {
                if (i >= pageCount) {
                    pageNumberLayout.getChildAt(i).setVisibility(View.GONE);
                    continue;
                } else {
                    imgCur = (ImageView) pageNumberLayout.getChildAt(i);
                }
            } else {
                if (i < hasCount) {
                    imgCur = (ImageView) pageNumberLayout.getChildAt(i);
                } else {
                    imgCur = new ImageView(context);
                    pageNumberLayout.addView(imgCur);
                }
            }

            imgCur.setId(i);
            // 判断当前页码来更新
            imgCur.setSelected(i == page);

            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) imgCur.getLayoutParams();
            params.leftMargin = DisplayUtils.dip2px(context, i == 0 ? 0 : 3);
            params.rightMargin = DisplayUtils.dip2px(context, i == hasCount - 1 ? 0 : 3);
            imgCur.setLayoutParams(params);
            imgCur.setImageResource(i == page ? R.drawable.bg_emoji_indicator_selected : R.drawable.bg_emoji_indicator_unselected);
            imgCur.setVisibility(View.VISIBLE);
        }
    }

    /**
     * ******************************** 表情  *******************************
     */
    private void showEmojiGridView() {
        LogUtils.d(TAG, "showEmojiGridView-->resetEmotionPager");
        pageCount = (int) Math.ceil(EmojiManager.getDisplayCount() / (float) EMOJI_PER_PAGE);
        pagerAdapter.notifyDataSetChanged();
        resetEmotionPager();
    }

    private void resetEmotionPager() {
        LogUtils.d(TAG, "resetEmotionPager-->setCurEmotionPage 0");
        setCurEmotionPage(0);
        emotPager.setCurrentItem(0, false);
    }

    private void setCurEmotionPage(int position) {
        LogUtils.d(TAG, "setCurEmotionPage-position:" + position);
        setCurPage(position, pageCount);
    }

    /**
     * ******************************** 贴图  *******************************
     */

    private void showStickerGridView() {
        LogUtils.d(TAG, "showStickerGridView-->initData");
        initData();
        pagerAdapter.setCurrEmojiType(categoryIndex);
        pagerAdapter.notifyDataSetChanged();

        // 计算起始的pager index
        int position = 0;
        for (int i = 0; i < categoryPageNumberList.size(); i++) {
            //categoryPageNumberList存储的是每个tab下的page数,size则等同于tab数
            if (i == categoryIndex) {
                break;
            }
            position += categoryPageNumberList.get(i);
        }
        LogUtils.d(TAG, "showStickerGridView-->setCurStickerPage position:" + position + " categoryIndex:" + categoryIndex);
        setCurStickerPage(position);
        emotPager.setCurrentItem(position, false);
        int tabPageCount = categoryPageNumberList.get(categoryIndex);
        LogUtils.d(TAG, "showStickerGridVie tabPageCount:" + tabPageCount);
    }

    private void initData() {
        LogUtils.d(TAG, "initData isDataInitialized:" + isDataInitialized + " categoryIndex:" + categoryIndex);
        if (isDataInitialized) {//数据已经初始化，未变动不重新加载数据
            return;
        }

        if (categoryDataList == null) {
            categoryDataList = new ArrayList<>();
        }

        if (categoryPageNumberList == null) {
            categoryPageNumberList = new ArrayList<>();
        }

        categoryDataList.clear();
        categoryPageNumberList.clear();

        final StickerManager manager = StickerManager.getInstance();

        categoryDataList.add(null);// 默认表情，null也可以使得categoryDataList.size>0
        categoryPageNumberList.add(getCategoryPageCount(null, EmoticonPickerView.defaultEmojiTabIndex));

        categoryDataList.add(null);// 普通表情，null也可以使得categoryDataList.size>0
        categoryPageNumberList.add(getCategoryPageCount(null, EmoticonPickerView.normalEmojiTabIndex));

        categoryDataList.add(null);// 火热表情，null也可以使得categoryDataList.size>0
        categoryPageNumberList.add(getCategoryPageCount(null, EmoticonPickerView.hotEmojiTabIndex));

        //贴图
        List<StickerCategory> categories = manager.getCategories();
        categoryDataList.addAll(categories);
        for (StickerCategory c : categories) {
            categoryPageNumberList.add(getCategoryPageCount(c, EmoticonPickerView.chartletTabIndex));
        }


        pageCount = 0;//总页数
        for (Integer count : categoryPageNumberList) {
            pageCount += count;
        }

        isDataInitialized = true;
        LogUtils.d(TAG, "initData isDataInitialized:" + isDataInitialized + " pageCount:" + pageCount);
    }

    // 给定pager中的索引，返回categoryIndex和positionInCategory
    private int[] getPagerInfo(int position) {
        //position是tab&page,总页数索引
        if (categoryDataList == null || categoryPageNumberList == null) {
            return pagerIndexInfo;
        }
        //categoryIndex 是tab索引
        int cIndex = categoryIndex;
        int startIndex = 0;
        int pageNumberPerCategory = 0;
        //categoryPageNumberList 是每个tab的page页数
        for (int i = 0; i < categoryPageNumberList.size(); i++) {
            pageNumberPerCategory = categoryPageNumberList.get(i);
            if (position < startIndex + pageNumberPerCategory) {
                //当i轮询到对应的页数大于position时，即说明当前的tab index为i
                cIndex = i;
                break;
            }
            startIndex += pageNumberPerCategory;
        }

        this.pagerIndexInfo[0] = cIndex;//tab index
        this.pagerIndexInfo[1] = position - startIndex;//tab下的page index
        LogUtils.d(TAG, "getPagerInfo pagerIndexInfo.0:" + pagerIndexInfo[0] + " pagerIndexInfo.1:" + pagerIndexInfo[1]);
        return pagerIndexInfo;
    }

    public void setCategoryChangCheckedCallback(IEmoticonCategoryChanged callback) {
        this.categoryChangedCallback = callback;
    }

    private void setCurStickerPage(int position) {
        LogUtils.d(TAG, "setCurStickerPage-->getPagerInfo position:" + position);
        getPagerInfo(position);
        int categoryIndex = pagerIndexInfo[0];
        int pageIndexInCategory = 0;
        if (pagerIndexInfo.length > 1) {
            pageIndexInCategory = pagerIndexInfo[1];
        }
        LogUtils.d(TAG, "setCurStickerPage categoryIndex:" + categoryIndex + " pageIndexInCategory:" + pageIndexInCategory);
        int categoryPageCount = 0;
        if (categoryPageNumberList.size() > categoryIndex) {
            categoryPageCount = categoryPageNumberList.get(categoryIndex);
        }
        LogUtils.d(TAG, "setCurStickerPage-->setCurPage pageIndexInCategory" + pageIndexInCategory
                + " categoryPageCount:" + categoryPageCount);
        setCurPage(pageIndexInCategory, categoryPageCount);
    }

    /**
     * ***************************** PagerAdapter ****************************
     */
    private class EmoticonViewPaperAdapter extends PagerAdapter {

        private final String TAG = EmoticonViewPaperAdapter.class.getSimpleName();
        private int currEmojiType = 0;

        public void setCurrEmojiType(int currEmojiType) {
            this.currEmojiType = currEmojiType;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public int getCount() {
            return pageCount == 0 ? 1 : pageCount;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            StickerCategory category;
            int pos;
            if (categoryDataList != null && categoryDataList.size() > 0 && categoryPageNumberList != null
                    && categoryPageNumberList.size() > 0) {
                // 显示所有贴图&表情
                LogUtils.d(TAG, "instantiateItem position:" + position + " 显示所有贴图&表情");
                //position 每个tab对应的page num，从0开始的索引
                getPagerInfo(position);
                int cIndex = pagerIndexInfo[0];
                category = categoryDataList.get(cIndex);
                LogUtils.d(TAG, "instantiateItem category is null :" + (null == category));
                pos = pagerIndexInfo[1];
            } else {
                // 只显示表情
                LogUtils.d(TAG, "instantiateItem position:" + position + " 只显示表情");
                category = null;
                pos = position;
            }

            if (category == null) {
                pageNumberLayout.setVisibility(View.VISIBLE);
                GridView gridView = new GridView(context);
                gridView.setOnItemClickListener(emojiListener);
                EmojiAdapter emojiAdapter = new EmojiAdapter(context,
                        pos * (pagerIndexInfo[0] == EmoticonPickerView.defaultEmojiTabIndex ? EMOJI_PER_PAGE : SERVER_EMOJI_PER_PAGE));
                emojiAdapter.setCurrEmojiType(currEmojiType);
                gridView.setAdapter(emojiAdapter);
                gridView.setNumColumns(pagerIndexInfo[0] == EmoticonPickerView.defaultEmojiTabIndex ? EMOJI_PER_LINE : SERVER_EMOJI_PER_LINE);
                if (pagerIndexInfo[0] == EmoticonPickerView.defaultEmojiTabIndex) {
                    gridView.setHorizontalSpacing(5);
                    gridView.setVerticalSpacing(5);
                }
                gridView.setGravity(Gravity.CENTER);
                gridView.setSelector(R.drawable.nim_emoji_item_selector);
                container.addView(gridView);
                return gridView;
            } else {
                pageNumberLayout.setVisibility(View.VISIBLE);
                GridView gridView = new GridView(context);
                gridView.setPadding(10, 0, 10, 0);
                gridView.setOnItemClickListener(stickerListener);
                gridView.setAdapter(new StickerAdapter(context, category, pos * STICKER_PER_PAGE));
                gridView.setNumColumns(4);
                gridView.setHorizontalSpacing(5);
                gridView.setGravity(Gravity.CENTER);
                gridView.setSelector(R.drawable.nim_emoji_item_selector);
                container.addView(gridView);
                return gridView;
            }
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View layout = (View) object;
            container.removeView(layout);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }
}
