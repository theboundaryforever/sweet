package com.netease.nim.uikit.session.emoji;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.netease.nim.uikit.R;
import com.netease.nim.uikit.common.ui.imageview.CheckedImageButton;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nim.uikit.common.util.media.BitmapDecoder;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.tongdaxing.erban.libcommon.utils.LogUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * 贴图表情选择控件
 */
public class EmoticonPickerView extends LinearLayout implements IEmoticonCategoryChanged {

    public static int defaultEmojiTabIndex = 0;//默认表情

    private Context context;

    private IEmoticonSelectedListener listener;

    private boolean loaded = false;

    private boolean withSticker;

    private EmoticonView gifView;

    private ViewPager currentEmojiPage;

    private LinearLayout pageNumberLayout;//页面布局

    private HorizontalScrollView scrollView;

    private LinearLayout tabView;

    private int categoryIndex;

    private Handler uiHandler;
    public static int normalEmojiTabIndex = 1;//普通表情
    public static int hotEmojiTabIndex = 2;//火热表情
    public static int chartletTabIndex = 3;//火热表情
    private final String TAG = EmoticonPickerView.class.getSimpleName();

    public EmoticonPickerView(Context context) {
        super(context);

        init(context);
    }

    public EmoticonPickerView(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(context);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public EmoticonPickerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        init(context);
    }

    private void init(Context context) {
        this.context = context;
        this.uiHandler = new Handler(context.getMainLooper());

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.nim_emoji_layout, this);
        setOrientation(LinearLayout.VERTICAL);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        setupEmojView();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    // 添加各个tab按钮
    OnClickListener tabCheckListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            //增加如下行代码，解决tab切换，无法正确加载表情列表的bug
            categoryIndex = v.getId();
            onEmoticonBtnChecked(v.getId());
        }
    };

    public void setListener(IEmoticonSelectedListener listener) {
        if (listener != null) {
            this.listener = listener;
        } else {
            LogUtil.i("sticker", "listener is null");
        }
    }

    public void show(IEmoticonSelectedListener listener) {
        setListener(listener);

        if (loaded) {
            return;
        }
        loadStickers();
        loaded = true;

        show();
    }

    protected void setupEmojView() {
        currentEmojiPage = (ViewPager) findViewById(R.id.scrPlugin);
        pageNumberLayout = (LinearLayout) findViewById(R.id.layout_scr_bottom);
        tabView = (LinearLayout) findViewById(R.id.emoj_tab_view);
        scrollView = (HorizontalScrollView) findViewById(R.id.emoj_tab_view_container);

        findViewById(R.id.top_divider_line).setVisibility(View.GONE);
    }

    private void loadStickers() {
        if (!withSticker) {
            scrollView.setVisibility(View.GONE);
            return;
        }

        final StickerManager manager = StickerManager.getInstance();

        tabView.removeAllViews();

        int index = defaultEmojiTabIndex;

        // emoji表情 0
        CheckedImageButton btn = addEmoticonTabBtn(index, tabCheckListener);
        btn.setNormalImageId(R.drawable.icon_emoji_default);
        btn.setCheckedImageId(R.drawable.icon_emoji_default);

        //com.netease.nim.uikit.session.changeEmojiBtnStatus.EmoticonView.EmoticonView-->onPageSelected限制了
        // 这里的tab不能依据list是否有数据做动态展示，只能写死在界面上

        //普通表情 1
//        if(!ListUtils.isListEmpty(SerEmojiTopicDataManager.getInstance().serverNormalEmojiList)){
        index = normalEmojiTabIndex;
        btn = addEmoticonTabBtn(index, tabCheckListener);
        btn.setNormalImageId(R.drawable.icon_emoji_normal);
        btn.setCheckedImageId(R.drawable.icon_emoji_normal);
//        }

        //火热表情 2
//        if(!ListUtils.isListEmpty(SerEmojiTopicDataManager.getInstance().serverHotEmojiList)){
        index = hotEmojiTabIndex;
        btn = addEmoticonTabBtn(index, tabCheckListener);
        btn.setNormalImageId(R.drawable.icon_emoji_hot);
        btn.setCheckedImageId(R.drawable.icon_emoji_hot);
//        }

        // 贴图 3
        List<StickerCategory> categories = manager.getCategories();
//        if(!ListUtils.isListEmpty(categories)){
        index = chartletTabIndex;
        for (StickerCategory category : categories) {
            btn = addEmoticonTabBtn(index++, tabCheckListener);
            setCheckedButtomImage(btn, category);
        }
//        }
    }


    private CheckedImageButton addEmoticonTabBtn(int index, OnClickListener listener) {
        CheckedImageButton emotBtn = new CheckedImageButton(context);
        emotBtn.setNormalBkResId(R.drawable.nim_sticker_button_background_normal_layer_list);
        emotBtn.setCheckedBkResId(R.drawable.nim_sticker_button_background_pressed_layer_list);
        emotBtn.setId(index);
        emotBtn.setOnClickListener(listener);
        emotBtn.setScaleType(ImageView.ScaleType.FIT_CENTER);
        emotBtn.setPaddingValue(ScreenUtil.dip2px(18), ScreenUtil.dip2px(12),
                ScreenUtil.dip2px(18), ScreenUtil.dip2px(12));

        final int emojiBtnWidth = ScreenUtil.dip2px(60);
        final int emojiBtnHeight = ScreenUtil.dip2px(48);

        tabView.addView(emotBtn);

        ViewGroup.LayoutParams emojBtnLayoutParams = emotBtn.getLayoutParams();
        emojBtnLayoutParams.width = emojiBtnWidth;
        emojBtnLayoutParams.height = emojiBtnHeight;
        emotBtn.setLayoutParams(emojBtnLayoutParams);

        return emotBtn;
    }

    private void setCheckedButtomImage(CheckedImageButton btn, StickerCategory category) {
        try {
            InputStream is = category.getCoverNormalInputStream(context);
            if (is != null) {
                Bitmap bmp = BitmapDecoder.decode(is);
                btn.setNormalImage(bmp);
                is.close();
            }
            is = category.getCoverPressedInputStream(context);
            if (is != null) {
                Bitmap bmp = BitmapDecoder.decode(is);
                btn.setCheckedImage(bmp);
                is.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void onEmoticonBtnChecked(int index) {
        LogUtils.d(TAG, "onEmoticonBtnChecked-index:" + index);
        updateTabButton(index);
        showEmotPager(index);
    }

    private void updateTabButton(int index) {
        LogUtils.d(TAG, "updateTabButton-index:" + index);
        for (int i = 0; i < tabView.getChildCount(); ++i) {
            View child = tabView.getChildAt(i);
            if (child instanceof FrameLayout) {
                child = ((FrameLayout) child).getChildAt(0);
            }

            if (child instanceof CheckedImageButton) {
                CheckedImageButton tabButton = (CheckedImageButton) child;
                if (tabButton.isChecked() && i != index) {
                    tabButton.setChecked(false);
                } else if (!tabButton.isChecked() && i == index) {
                    tabButton.setChecked(true);
                }
            }
        }
    }

    private void showEmotPager(int index) {
        LogUtils.d(TAG, "showEmotPager index:" + index);

        if (gifView == null) {
            gifView = new EmoticonView(context, listener, currentEmojiPage, pageNumberLayout);
            gifView.setCategoryChangCheckedCallback(this);
        }

        gifView.showStickers(index);
    }

    private void showEmojiView() {
        LogUtils.d(TAG, "showEmojiView");
        if (gifView == null) {
            gifView = new EmoticonView(context, listener, currentEmojiPage, pageNumberLayout);
        }
        gifView.showEmojis();
    }

    private void show() {
        if (listener == null) {
            LogUtil.i("sticker", "show picker view when listener is null");
        }
        LogUtils.d(TAG, "show withSticker:" + withSticker);
        if (!withSticker) {
            showEmojiView();
        } else {
            onEmoticonBtnChecked(EmoticonPickerView.defaultEmojiTabIndex);
            setSelectedVisible(EmoticonPickerView.defaultEmojiTabIndex);
        }
    }


    private void setSelectedVisible(final int index) {
        LogUtils.d(TAG, "setSelectedVisible-index:" + index);
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (scrollView.getChildAt(0).getWidth() == 0) {
                    uiHandler.postDelayed(this, 100);
                }
                int x = -1;
                View child = tabView.getChildAt(index);
                if (child != null) {
                    if (child.getRight() > scrollView.getWidth()) {
                        x = child.getRight() - scrollView.getWidth();
                    }
                }
                if (x != -1) {
                    scrollView.smoothScrollTo(x, 0);
                }
            }
        };
        uiHandler.postDelayed(runnable, 100);
    }


    @Override
    public void onCategoryChanged(int index) {
        LogUtils.d(TAG, "onCategoryChanged-index:" + index + " categoryIndex:" + categoryIndex);
        if (categoryIndex == index) {
            return;
        }

        categoryIndex = index;
//        updateTabButton(index);
        //由updateTabButton(index);改为如下代码，解决pageview滑动切换tab，列表加载数据失效的问题
        onEmoticonBtnChecked(index);

    }

    public void setWithSticker(boolean withSticker) {
        this.withSticker = withSticker;
    }
}
