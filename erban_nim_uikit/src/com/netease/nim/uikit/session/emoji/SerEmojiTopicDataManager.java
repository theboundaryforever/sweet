package com.netease.nim.uikit.session.emoji;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author weihaitao
 * @date 2019/8/20
 */
public class SerEmojiTopicDataManager {

    /*{
        "code": 200,
        "data": {
            "normal": [{
                "adminId": 1,
                "hasDynamic": 1,
                "id": 1,
                "name": "普通1",
                "picture": "https://img.pinjin88.com/FjmUw4vNW6KOqq8IOgxX62kV-PK4?imageslim",
                "seqNo": 1,
                "staticPicture": "https://img.pinjin88.com/FheEYabr4NqlMOYV8iEnj7RI68Ze?imageslim",
                "status": 1,
                "type": 1
            }, {
                "adminId": 1,
                "hasDynamic": 1,
                "id": 2,
                "name": "普通4",
                "picture": "https://img.pinjin88.com/FgKUuimdk8XJQ6x9cULMRfj6x56E?imageslim",
                "seqNo": 2,
                "staticPicture": "https://img.pinjin88.com/FkcGNMH5qPfclrlIwBpOGFojWmtN?imageslim",
                "status": 1,
                "type": 1
            }, {
                "adminId": 1,
                "hasDynamic": 1,
                "id": 3,
                "name": "普通5",
                "picture": "https://img.pinjin88.com/FnsoYTlGrVDuNwjKRKDU8pWOcS1Q?imageslim",
                "seqNo": 3,
                "staticPicture": "https://img.pinjin88.com/Fp5PJnsBUALa3OqGeTBZeyZl6-4E?imageslim",
                "status": 1,
                "type": 1
            }, {
                "adminId": 1,
                "hasDynamic": 1,
                "id": 4,
                "name": "普通2",
                "picture": "https://img.pinjin88.com/FlPVdM9TipkOCRsxpZRys-DeDJQv?imageslim",
                "seqNo": 4,
                "staticPicture": "https://img.pinjin88.com/FmxOSy__Zqm2dXmLXgHf-S0DDbgT?imageslim",
                "status": 1,
                "type": 1
            }, {
                "adminId": 1,
                "hasDynamic": 1,
                "id": 5,
                "name": "普通6",
                "picture": "https://img.pinjin88.com/FnlceYyzfwwLbacNih9gk-MQttGO?imageslim",
                "seqNo": 5,
                "staticPicture": "https://img.pinjin88.com/FkBiJKbhFjY0w5J-85jzObxUdAf3?imageslim",
                "status": 1,
                "type": 1
            }, {
                "adminId": 1,
                "hasDynamic": 1,
                "id": 6,
                "name": "普通3",
                "picture": "https://img.pinjin88.com/Fjao6QbuVWDxSvaJhuzVrOXPMRbN?imageslim",
                "seqNo": 6,
                "staticPicture": "https://img.pinjin88.com/Fsb44lephPIW-N-2PR7P89vlhjGJ?imageslim",
                "status": 1,
                "type": 1
            }, {
                "adminId": 1,
                "hasDynamic": 1,
                "id": 7,
                "name": "普通7",
                "picture": "https://img.pinjin88.com/Fpwxs3kJmJ8NbU50cfkWzUOjaEbo?imageslim",
                "seqNo": 7,
                "staticPicture": "https://img.pinjin88.com/Ftj_NVYkBRAHp1UTQ3nNBDwg5AZF?imageslim",
                "status": 1,
                "type": 1
            }],
            "slide": [{
                "adminId": 1,
                "hasDynamic": 0,
                "id": 15,
                "name": "滑动1",
                "picture": "https://img.pinjin88.com/Frhiwhjz50fYDupO_z80uFOPcdpF?imageslim",
                "seqNo": 1,
                "staticPicture": "https://img.pinjin88.com/FmrO7Hcjrebj8kpTD6UiVtjsH206?imageslim",
                "status": 1,
                "type": 3
            }, {
                "adminId": 1,
                "hasDynamic": 0,
                "id": 16,
                "name": "滑动2",
                "picture": "https://img.pinjin88.com/FiCcfS1oyU_CJH5NWC2Hmrq3NMN2?imageslim",
                "seqNo": 2,
                "staticPicture": "https://img.pinjin88.com/Firn8OqOouXUY63HNSS_-fHU000I?imageslim",
                "status": 1,
                "type": 3
            }],
            "hot": [{
                "adminId": 1,
                "hasDynamic": 1,
                "id": 8,
                "name": "火热1",
                "picture": "https://img.pinjin88.com/FgOYDuPIJr-s5qCIY1jsZXU6PRre?imageslim",
                "seqNo": 1,
                "staticPicture": "https://img.pinjin88.com/FoiaH0Q2jCxoYYp6zySAfB259nwQ?imageslim",
                "status": 1,
                "type": 2
            }, {
                "adminId": 1,
                "hasDynamic": 1,
                "id": 9,
                "name": "火热2",
                "picture": "https://img.pinjin88.com/FsTVJo5jqNcrzZ6ATQeK_HM7HwyH?imageslim",
                "seqNo": 2,
                "staticPicture": "https://img.pinjin88.com/FqwXQ3KPvQ42kJ_D8CuJV19h64PX?imageslim",
                "status": 1,
                "type": 2
            }, {
                "adminId": 1,
                "hasDynamic": 1,
                "id": 10,
                "name": "火热3",
                "picture": "https://img.pinjin88.com/Fky8hmeCuaQLq0KhPncMGc7xlzFD?imageslim",
                "seqNo": 3,
                "staticPicture": "https://img.pinjin88.com/FpJ5dzmvGNm8Z3Pw2kWfjScVhFip?imageslim",
                "status": 1,
                "type": 2
            }, {
                "adminId": 1,
                "hasDynamic": 1,
                "id": 11,
                "name": "火热4",
                "picture": "https://img.pinjin88.com/FmAthQlYhDgagVDrqPON_n64zlgN?imageslim",
                "seqNo": 4,
                "staticPicture": "https://img.pinjin88.com/Fsb44lephPIW-N-2PR7P89vlhjGJ?imageslim",
                "status": 1,
                "type": 2
            }, {
                "adminId": 1,
                "hasDynamic": 1,
                "id": 12,
                "name": "火热5",
                "picture": "https://img.pinjin88.com/FkMDH_knL9TyO32g3cQivoJ_iuYq?imageslim",
                "seqNo": 5,
                "staticPicture": "https://img.pinjin88.com/FnUXq-ahzwwZZCkHIYUShi-LVo-F?imageslim",
                "status": 1,
                "type": 2
            }]
        },
        "message": "success"
    }*/

    private static SerEmojiTopicDataManager instance = null;
    public List<Entry> serverNormalEmojiList = new ArrayList<Entry>();
    public List<Entry> serverHotEmojiList = new ArrayList<Entry>();
    public List<Entry> serverChildrenEmojiList = new ArrayList<Entry>();
    private Map<String, Entry> name2EmojiEntry = new HashMap<String, Entry>();
    private List<OnEmojiTopicDataChangeListener> listDataChangeListeners = new ArrayList<>();

    private SerEmojiTopicDataManager() {

    }

    public static SerEmojiTopicDataManager getInstance() {
        if (null == instance) {
            instance = new SerEmojiTopicDataManager();
        }
        return instance;
    }

    public Entry getDisplayEmojiEntry(String name) {
        if (null == name2EmojiEntry || TextUtils.isEmpty(name) || !name2EmojiEntry.containsKey(name)) {
            return null;
        }
        return name2EmojiEntry.get(name);
    }

    public void refreshNormalEmojiList(List<Entry> list) {
        serverNormalEmojiList = list;
        if (null != list && null != name2EmojiEntry) {
            for (Entry entry : list) {
                if (!name2EmojiEntry.containsKey(entry.getName())) {
                    name2EmojiEntry.put(entry.getName(), entry);
                }
            }
        }
    }

    public void refreshSlideEmojiList(List<Entry> list) {
        serverChildrenEmojiList = list;
        if (null != list && null != name2EmojiEntry) {
            for (Entry entry : list) {
                if (!name2EmojiEntry.containsKey(entry.getName())) {
                    name2EmojiEntry.put(entry.getName(), entry);
                }
            }
        }
    }

    public void refreshHotEmojiList(List<Entry> list) {
        serverHotEmojiList = list;
        if (null != list && null != name2EmojiEntry) {
            for (Entry entry : list) {
                if (!name2EmojiEntry.containsKey(entry.getName())) {
                    name2EmojiEntry.put(entry.getName(), entry);
                }
            }
        }
    }

    public final String getNormalEmojiUrl(int index) {
        String emojiUrl = (index >= 0 && index < serverNormalEmojiList.size() ?
                serverNormalEmojiList.get(index).getPicture() : null);
        return emojiUrl;
    }

    public final String getNormalStaticEmojiUrl(int index) {
        String emojiUrl = (index >= 0 && index < serverNormalEmojiList.size() ?
                serverNormalEmojiList.get(index).getStaticPicture() : null);
        return emojiUrl;
    }

    public final String getHotEmojiUrl(int index) {
        String emojiUrl = (index >= 0 && index < serverHotEmojiList.size() ?
                serverHotEmojiList.get(index).getPicture() : null);
        return emojiUrl;
    }

    public final String getHotStaticEmojiUrl(int index) {
        String emojiUrl = (index >= 0 && index < serverHotEmojiList.size() ?
                serverHotEmojiList.get(index).getStaticPicture() : null);
        return emojiUrl;
    }

    public final boolean isNormalEmojiDynamic(int index) {
        boolean dynamic = ((index >= 0 && index < serverNormalEmojiList.size()) && serverNormalEmojiList.get(index).getHasDynamic());
        return dynamic;
    }

    public final boolean isHotEmojiDynamic(int index) {
        boolean dynamic = ((index >= 0 && index < serverHotEmojiList.size()) && serverHotEmojiList.get(index).getHasDynamic());
        return dynamic;
    }

    public void registerEmojiListDataChangeListener(boolean isRegister, OnEmojiTopicDataChangeListener listDataChangeListener) {
        synchronized (listDataChangeListeners) {
            if (isRegister) {
                if (!listDataChangeListeners.contains(listDataChangeListener)) {
                    listDataChangeListeners.add(listDataChangeListener);
                }
            } else {
                listDataChangeListeners.remove(listDataChangeListener);
            }

        }
    }

    public void notifyEmojiListDataChanged(long targetUid, boolean showEmojiList) {
        synchronized (listDataChangeListeners) {
            if (null != listDataChangeListeners) {
                for (OnEmojiTopicDataChangeListener listener : listDataChangeListeners) {
                    listener.onEmojiListCHanged(targetUid, showEmojiList);
                }
            }
        }
    }

    public void notifyTopicDataUpdated(long targetUid, String topic) {
        synchronized (listDataChangeListeners) {
            if (null != listDataChangeListeners) {
                for (OnEmojiTopicDataChangeListener listener : listDataChangeListeners) {
                    listener.onTopicUpdated(targetUid, topic);
                }
            }
        }
    }

    public interface OnEmojiTopicDataChangeListener {
        void onEmojiListCHanged(long targetUid, boolean showEmojiList);

        void onTopicUpdated(long targetUid, String topic);
    }
}
