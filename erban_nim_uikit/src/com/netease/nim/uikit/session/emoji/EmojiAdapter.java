package com.netease.nim.uikit.session.emoji;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.juxiao.library_utils.DisplayUtils;
import com.netease.nim.uikit.R;
import com.netease.nim.uikit.utils.ImageLoadUtils;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.LogUtils;

public class EmojiAdapter extends BaseAdapter {

    private final String TAG = EmojiAdapter.class.getSimpleName();

	private Context context;
	
	private int startIndex;
    //0-nim uikit 自带，1-服务器普通表情，2-服务器热门表情
    private int currEmojiType = 0;
    private int emojiImgSize = 0;
    private int serEmojiSize = 0;

    //TODO 解决存在加载两遍的问题
    public EmojiAdapter(Context mContext, int startIndex) {
        this.context = mContext;
        this.startIndex = startIndex;
        emojiImgSize = DisplayUtility.dp2px(context, 28);
        serEmojiSize = DisplayUtils.dip2px(context, 70);
    }

    public void setCurrEmojiType(int currEmojiType) {
        this.currEmojiType = currEmojiType;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (EmoticonPickerView.normalEmojiTabIndex == currEmojiType) {
            count = SerEmojiTopicDataManager.getInstance().serverNormalEmojiList.size() - startIndex;
        } else if (EmoticonPickerView.hotEmojiTabIndex == currEmojiType) {
            count = SerEmojiTopicDataManager.getInstance().serverHotEmojiList.size() - startIndex;
        } else {
            count = EmojiManager.getDisplayCount() - startIndex + 1;
        }
        LogUtils.d(TAG, "getCount-count1:" + count + " currEmojiType:" + currEmojiType);
        if (count < 0) {
            count = 0;
        }
        count = Math.min(count, EmoticonPickerView.defaultEmojiTabIndex == currEmojiType ?
                (EmoticonView.EMOJI_PER_PAGE + 1) : EmoticonView.SERVER_EMOJI_PER_PAGE);
        LogUtils.d(TAG, "getCount-count2:" + count + " currEmojiType:" + currEmojiType);
        return count;
    }

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return startIndex + position;
	}

    @Override
    @SuppressLint({ "ViewHolder", "InflateParams" })
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.nim_emoji_item, null);
        ImageView emojiThumb = (ImageView) convertView.findViewById(R.id.imgEmoji);
        int count = 0;
        if (EmoticonPickerView.normalEmojiTabIndex == currEmojiType) {
            count = SerEmojiTopicDataManager.getInstance().serverNormalEmojiList.size();
        } else if (EmoticonPickerView.hotEmojiTabIndex == currEmojiType) {
            count = SerEmojiTopicDataManager.getInstance().serverHotEmojiList.size();
        } else {
            count = EmojiManager.getDisplayCount();
        }
        int index = startIndex + position;
        boolean isDefaultEmojiEndPosition = currEmojiType == EmoticonPickerView.defaultEmojiTabIndex
                && (position == EmoticonView.EMOJI_PER_PAGE || index == count);
        LogUtils.d(TAG, "getView-position:" + position + " index:" + index + " count:" + count + " isDefaultEmojiEndPosition:" + isDefaultEmojiEndPosition);
        ViewGroup.LayoutParams layoutParams = emojiThumb.getLayoutParams();
        layoutParams.width = isDefaultEmojiEndPosition
                || (EmoticonPickerView.hotEmojiTabIndex != currEmojiType
                && EmoticonPickerView.normalEmojiTabIndex != currEmojiType) ? emojiImgSize : serEmojiSize;
        layoutParams.height = isDefaultEmojiEndPosition
                || (EmoticonPickerView.hotEmojiTabIndex != currEmojiType
                && EmoticonPickerView.normalEmojiTabIndex != currEmojiType) ? emojiImgSize : serEmojiSize;
        emojiThumb.setLayoutParams(layoutParams);
        if (isDefaultEmojiEndPosition) {
            emojiThumb.setTag(null);
            emojiThumb.setImageResource(R.drawable.nim_emoji_del);
        } else if (index < count) {
            String emojiUrl = null;
            if (EmoticonPickerView.normalEmojiTabIndex == currEmojiType) {
                emojiUrl = SerEmojiTopicDataManager.getInstance().getNormalStaticEmojiUrl(index);
                loadEmojiFromUrl(emojiUrl, false, emojiThumb, serEmojiSize);
            } else if (EmoticonPickerView.hotEmojiTabIndex == currEmojiType) {
                emojiUrl = SerEmojiTopicDataManager.getInstance().getHotStaticEmojiUrl(index);
                loadEmojiFromUrl(emojiUrl, false, emojiThumb, serEmojiSize);
            } else {
                emojiThumb.setTag(null);
                emojiThumb.setImageDrawable(EmojiManager.getDisplayDrawable(context, index));
            }
        } else {
            emojiThumb.setTag(null);
            convertView.setVisibility(View.GONE);
        }

        return convertView;
    }

    private void loadEmojiFromUrl(String emojiUrl, boolean hasDynamic, ImageView emojiThumb, int serEmojiSize) {
        LogUtils.d(TAG, "loadEmojiFromUrl-emojiUrl:" + emojiUrl + " hasDynamic:" + hasDynamic);
        emojiThumb.setImageResource(R.mipmap.ic_default_server_emoji);
        String emojiTag = (String) emojiThumb.getTag();
        if (!TextUtils.isEmpty(emojiTag) && emojiUrl.equals(emojiTag)) {
            LogUtils.d(TAG, "loadEmojiFromUrl 阻止多次加载");
            return;
        }
        ImageLoadUtils.loadEmoji(context,
                ImageLoadUtils.toThumbnailUrl(serEmojiSize, serEmojiSize, emojiUrl),
                emojiThumb,
                R.mipmap.ic_default_server_emoji, hasDynamic, serEmojiSize);
    }
}