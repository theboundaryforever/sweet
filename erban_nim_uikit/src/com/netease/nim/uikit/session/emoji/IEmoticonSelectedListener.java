package com.netease.nim.uikit.session.emoji;

public interface IEmoticonSelectedListener {
    void onEmojiSelected(String key, boolean isCustomEmoji);

	void onStickerSelected(String categoryName, String stickerName);
}
