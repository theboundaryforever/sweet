package com.netease.nim.uikit.session.emoji;

import java.io.Serializable;

/**
 * 表情实体类
 *
 * @author weihaitao
 * @date 2019/8/20
 */
public class Entry implements Serializable {

    String text;
    String assetPath;
    /**
     * adminId : 0
     * createTime : 2019-08-20T09:39:26.058Z
     * hasDynamic : 0
     * id : 0
     * name : string
     * picture : string
     * seqNo : 0
     * status : 0
     * type : 0
     */

    private long adminId;
    private long createTime;
    /**
     * 是否为动态表情
     */
    private int hasDynamic;
    /**
     * 表情ID
     */
    private long id = -1L;
    /**
     * 表情名称
     */
    private String name;
    /**
     * 表情地址
     */
    private String picture;
    private int seqNo;
    private int status;
    private String staticPicture;
    /**
     * 表情类型: 1-普通，2-火热，3-滑动表情
     */
    private int type;

    /**
     * {
     * "adminId": 0,
     * "createTime": "2019-08-20T09:39:26.058Z",
     * "hasDynamic": 0,
     * "id": 0,
     * "name": "string",
     * "picture": "string",
     * "seqNo": 0,
     * "status": 0,
     * "type": 0
     * }
     */

    public Entry(String text, String assetPath) {
        this.text = text;
        this.assetPath = assetPath;
    }

    public String getStaticPicture() {
        return staticPicture;
    }

    public void setStaticPicture(String staticPicture) {
        this.staticPicture = staticPicture;
    }

    public long getAdminId() {
        return adminId;
    }

    public void setAdminId(long adminId) {
        this.adminId = adminId;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public boolean getHasDynamic() {
        return hasDynamic == 1;
    }

    public void setHasDynamic(int hasDynamic) {
        this.hasDynamic = hasDynamic;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
