package com.netease.nim.uikit.session.adapter;

import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.netease.nim.uikit.R;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.netease.nim.uikit.session.emoji.Entry;
import com.netease.nim.uikit.utils.ImageLoadUtils;

/**
 * Created by huangmeng1 on 2018/1/18.
 */

public class RecommEmojiListAdapter extends BaseQuickAdapter<Entry, BaseViewHolder> {

    private final String TAG = RecommEmojiListAdapter.class.getSimpleName();


    public RecommEmojiListAdapter() {
        super(R.layout.item_rv_p2p_message_emoji);
    }

    /**
     * Implement this method and use the helper to adapt the view to the given item.
     *
     * @param helper A fully initialized helper.
     * @param item   The item that needs to be displayed.
     */
    @Override
    protected void convert(BaseViewHolder helper, Entry item) {
        ImageView ivRecommEmoji = helper.getView(R.id.ivRecommEmoji);
        ImageLoadUtils.loadEmoji(ivRecommEmoji.getContext(),
                ImageLoadUtils.toThumbnailUrl(ScreenUtil.dip2px(48), ScreenUtil.dip2px(48), item.getStaticPicture()),
                ivRecommEmoji, R.mipmap.ic_default_server_emoji, false, ScreenUtil.dip2px(48));

    }
}
