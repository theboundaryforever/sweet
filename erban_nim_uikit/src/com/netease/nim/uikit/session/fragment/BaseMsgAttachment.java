package com.netease.nim.uikit.session.fragment;


import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;

/**
 * @author liaoxy
 * @Description:自定义消息附件基类，增加了消息是否在IM界面显示的功能
 * @date 2019/2/25 18:30
 */
public abstract class BaseMsgAttachment implements MsgAttachment {

    private boolean isVisible = true;

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

//    public abstract Json toJson();
}
