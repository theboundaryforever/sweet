package com.netease.nim.uikit.session.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.growingio.android.sdk.collection.GrowingIO;
import com.netease.nim.uikit.CustomPushContentProvider;
import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.R;
import com.netease.nim.uikit.ait.AitManager;
import com.netease.nim.uikit.cache.RobotInfoCache;
import com.netease.nim.uikit.common.fragment.TFragment;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.netease.nim.uikit.session.NIMSessionStatusManager;
import com.netease.nim.uikit.session.SessionCustomization;
import com.netease.nim.uikit.session.actions.BaseAction;
import com.netease.nim.uikit.session.activity.P2PMessageActivity;
import com.netease.nim.uikit.session.adapter.RecommEmojiListAdapter;
import com.netease.nim.uikit.session.audio.P2pMicroLinkHelper;
import com.netease.nim.uikit.session.constant.Extras;
import com.netease.nim.uikit.session.emoji.Entry;
import com.netease.nim.uikit.session.emoji.IEmoticonSelectedListener;
import com.netease.nim.uikit.session.emoji.SerEmojiTopicDataManager;
import com.netease.nim.uikit.session.module.Container;
import com.netease.nim.uikit.session.module.ModuleProxy;
import com.netease.nim.uikit.session.module.input.InputPanel;
import com.netease.nim.uikit.session.module.list.MessageListPanelEx;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.Observer;
import com.netease.nimlib.sdk.msg.MessageBuilder;
import com.netease.nimlib.sdk.msg.MsgService;
import com.netease.nimlib.sdk.msg.MsgServiceObserve;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.netease.nimlib.sdk.msg.model.MemberPushOption;
import com.netease.nimlib.sdk.msg.model.MessageReceipt;
import com.netease.nimlib.sdk.robot.model.NimRobotInfo;
import com.netease.nimlib.sdk.robot.model.RobotAttachment;
import com.netease.nimlib.sdk.robot.model.RobotMsgType;
import com.tongdaxing.erban.libcommon.utils.ButtonUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.ResolutionUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.json.Json;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.netease.nim.uikit.session.activity.P2PMessageActivity.BACK_ACTION;
import static com.netease.nim.uikit.session.activity.P2PMessageActivity.INVITATIOLN_LINK_MACRO;

/**
 * 聊天界面基类
 * <p/>
 * Created by huangjun on 2015/2/1.
 */
public class MessageFragment extends TFragment implements ModuleProxy, SerEmojiTopicDataManager.OnEmojiTopicDataChangeListener {

    private View rootView;

    private SessionCustomization customization;

    protected static final String TAG = "MessageActivity";

    // 聊天对象
    protected String sessionId; // p2p对方Account或者群id

    protected SessionTypeEnum sessionType;

    // modules
    protected InputPanel inputPanel;
    protected MessageListPanelEx messageListPanel;

    protected AitManager aitManager;

    private LinearLayout llMore;

    private LinearLayout llTopic;

    private static int defaultOuterRingMaxWidth = 0;
    private static int defaultAnimImgWidth = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.nim_message_fragment, container, false);
        return rootView;
    }

    /**
     * ***************************** life cycle *******************************
     */

    @Override
    public void onPause() {
        super.onPause();
        LogUtils.d(TAG, "onPause-退出聊天界面");
        NIMSessionStatusManager.getInstance().updateChattingAccount(MsgService.MSG_CHATTING_ACCOUNT_NONE, SessionTypeEnum.None);
        inputPanel.onPause();
        messageListPanel.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        messageListPanel.onResume();
        LogUtils.d(TAG, "onResume-进入聊天界面");
        NIMSessionStatusManager.getInstance().updateChattingAccount(sessionId, SessionTypeEnum.None);
        NIMClient.getService(MsgService.class).clearUnreadCount(sessionId, SessionTypeEnum.P2P);
        getActivity().setVolumeControlStream(AudioManager.STREAM_MUSIC); // 默认使用外放播放，原来是听筒播放
    }

    private View.OnClickListener p2pOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.ivCloseEmojiTips) {
                LogUtils.d(TAG, "p2pOnClickListener-onClick 关闭推荐表情列表");
                rlRecommEmoji.setVisibility(View.GONE);
            } else if (view.getId() == R.id.llLinkMacro) {
                LogUtils.d(TAG, "p2pOnClickListener-onClick 邀请连麦 isMyFriend:" + isMyFriend);

                if (isLinkMacroing()) {
                    SingleToastUtil.showToast("你正在连麦中...");
                    return;
                }
                if (isMyFriend) {
                    if (null != NimUIKit.onSendLinkMicroInviteMsgListener && NimUIKit.onSendLinkMicroInviteMsgListener.onSendLinkMicroInviteMsg()) {
                        SingleToastUtil.showToast("在房间内不可以连麦");
                        return;
                    }
                    //启动连麦
                    try {
                        Json json = new Json();
                        json.put("action", INVITATIOLN_LINK_MACRO);
                        json.put("linkUid", getOppoAccount());
                        dataAction.setActionCallback(actionCallback);
                        dataAction.setOutData(json);
                        dataAction.onClick();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    SingleToastUtil.showToast("相互关注才能打电话");
                }
            } else if (view.getId() == R.id.imvCloseMacro) {
                if (imvCloseMacro == null) {
                    return;
                }
                boolean hasMicroSelected = imvCloseMacro.isSelected();
                LogUtils.d(TAG, "p2pOnClickListener-onClick 是否静音 hasMicroSelected:" + hasMicroSelected);
                closeOrOpenMacro(!hasMicroSelected);
                return;
            } else if (view.getId() == R.id.imvFinishLinkMacro || view.getId() == R.id.tvCancelLinkMicro) {
                if (ButtonUtils.isFastDoubleClick(R.id.imvFinishLinkMacro)) {
                    LogUtils.d(TAG, "p2pOnClickListener-onClick 挂断连麦 阻止快速点击");
                    return;
                }
                if (imvFinishLinkMacro == null || tvCancelLinkMacro == null) {
                    return;
                }
                LogUtils.d(TAG, "p2pOnClickListener-onClick 挂断连麦");
                endLinkMicro(actionCallback);
                return;
            }
            llAudio.setSelected(false);
            llGame.setSelected(false);
            llEmoji.setSelected(false);
            if (view.getId() == R.id.llAudio) {
                if (llRecordAudio.getVisibility() == View.VISIBLE) {
                    llRecordAudio.setVisibility(View.GONE);
                    llAudio.setSelected(false);
                } else {
                    if (isLinkMacroing()) {
                        SingleToastUtil.showToast("连麦中不能发语音消息");
                        return;
                    }
                    llRecordAudio.setVisibility(View.VISIBLE);
                    llAudio.setSelected(true);
                    inputPanel.hideEmojiLayout();
                    inputPanel.hideActionPanelLayout();
                    llMoreItems.setVisibility(View.GONE);
                    llMore.setSelected(false);
                }
                inputPanel.hideInputMethod();
            } else if (view.getId() == R.id.llTopic) {
                if (null != NimUIKit.onServerEmojiOperaListener) {
                    NimUIKit.onServerEmojiOperaListener.getRandomTopic(Long.valueOf(sessionId));
                }
            } else if (view.getId() == R.id.llMore) {
                llAudio.setSelected(false);
                llGame.setSelected(false);
                llEmoji.setSelected(false);
                if (llMoreItems.getVisibility() == View.VISIBLE) {
                    llMoreItems.setVisibility(View.GONE);
                    llMore.setSelected(false);
                } else {
                    llMoreItems.setVisibility(View.VISIBLE);
                    llMore.setSelected(true);
                    llRecordAudio.setVisibility(View.GONE);
                    inputPanel.hideEmojiLayout();
                    inputPanel.hideActionPanelLayout();
                }
                inputPanel.hideInputMethod();
            } else if (view.getId() == R.id.llGifts) {
                if (getActionList().size() > 2) {
                    BaseAction baseAction = getActionList().get(1);
                    baseAction.onClick();
                    llRecordAudio.setVisibility(View.GONE);
                    inputPanel.hideEmojiLayout();
                    inputPanel.hideActionPanelLayout();

                    llMoreItems.setVisibility(View.GONE);
                    llMore.setSelected(false);
                }
            } else if (view.getId() == R.id.llGame) {
                boolean isShow = inputPanel.toggleActionPanelLayout(true);
                if (isShow) {
                    llGame.setSelected(true);
                    llMore.setSelected(false);
                    llRecordAudio.setVisibility(View.GONE);
                    llMoreItems.setVisibility(View.GONE);
                } else {
                    llGame.setSelected(false);
                }
                inputPanel.hideInputMethod();
            } else if (view.getId() == R.id.llPhotos) {
                if (!NimUIKit.checkInputMsgSendable(view.getContext())) {
                    return;
                }
                if (getActionList().size() > 1) {
                    BaseAction baseAction = getActionList().get(0);
                    baseAction.onClick();
                    llRecordAudio.setVisibility(View.GONE);
                    inputPanel.hideEmojiLayout();
                    inputPanel.hideActionPanelLayout();
                    llMoreItems.setVisibility(View.GONE);
                    llMore.setSelected(false);
                }
            } else if (view.getId() == R.id.llEmoji) {
                boolean isShow = inputPanel.toggleEmojiLayout(true);
                if (isShow) {
                    llEmoji.setSelected(true);
                    llMore.setSelected(false);
                    llGame.setSelected(false);
                    llRecordAudio.setVisibility(View.GONE);
                    llMoreItems.setVisibility(View.GONE);
                } else {
                    llEmoji.setSelected(false);
                }
                inputPanel.hideInputMethod();
            } else if (view.getId() == R.id.tvTextMessage) {
                showSoftInput(textMessageLayout);
                llRecordAudio.setVisibility(View.GONE);
                inputPanel.hideEmojiLayout();
                inputPanel.hideActionPanelLayout();
                llMoreItems.setVisibility(View.GONE);
                llMore.setSelected(false);
            }
        }
    };

    public void refreshMessageList() {
        messageListPanel.refreshMessageList();
    }

    private RelativeLayout textMessageLayout;

    /**
     * ************************* 消息收发 **********************************
     */
    // 是否允许发送消息
    protected boolean isAllowSendMessage(final IMMessage message) {
        return true;
    }

    private View rlRecommEmoji;

    /**
     * 消息接收观察者
     */
    Observer<List<IMMessage>> incomingMessageObserver = new Observer<List<IMMessage>>() {
        @Override
        public void onEvent(List<IMMessage> messages) {
            if (messages == null || messages.isEmpty()) {
                return;
            }
            messageListPanel.onIncomingMessage(messages);
            sendMsgReceipt(); // 发送已读回执
        }
    };

    private Observer<List<MessageReceipt>> messageReceiptObserver = new Observer<List<MessageReceipt>>() {
        @Override
        public void onEvent(List<MessageReceipt> messageReceipts) {
            receiveReceipt();
        }
    };

    /**
     * ********************** implements ModuleProxy *********************
     */
    @Override
    public boolean sendMessage(IMMessage message) {
        if (!isAllowSendMessage(message)) {
            return false;
        }

        appendTeamMemberPush(message);
        message = changeToRobotMsg(message);
        appendPushConfig(message);
        // send message to server and save to db
        NIMClient.getService(MsgService.class).sendMessage(message, false);

        messageListPanel.onMsgSend(message);

        aitManager.reset();
        return true;
    }

    private void appendTeamMemberPush(IMMessage message) {
        if (sessionType == SessionTypeEnum.Team) {
            List<String> pushList = aitManager.getAitTeamMember();
            if (pushList == null || pushList.isEmpty()) {
                return;
            }
            MemberPushOption memberPushOption = new MemberPushOption();
            memberPushOption.setForcePush(true);
            memberPushOption.setForcePushContent(message.getContent());
            memberPushOption.setForcePushList(pushList);
            message.setMemberPushOption(memberPushOption);
        }
    }

    private IMMessage changeToRobotMsg(IMMessage message) {
        if (isChatWithRobot()) {
            if (message.getMsgType() == MsgTypeEnum.text && message.getContent() != null) {
                String content = message.getContent().equals("") ? " " : message.getContent();
                message = MessageBuilder.createRobotMessage(message.getSessionId(), message.getSessionType(), message.getSessionId(), content, RobotMsgType.TEXT, content, null, null);
            }
        } else {
            String robotAccount = aitManager.getAitRobot();
            if (TextUtils.isEmpty(robotAccount)) {
                return message;
            }
            String text = message.getContent();
            String content = aitManager.removeRobotAitString(text, robotAccount);
            content = content.equals("") ? " " : content;
            message = MessageBuilder.createRobotMessage(message.getSessionId(), message.getSessionType(), robotAccount, text, RobotMsgType.TEXT, content, null, null);

        }
        return message;
    }

    private boolean isChatWithRobot() {
        return RobotInfoCache.getInstance().getRobotByAccount(sessionId) != null;
    }

    private void appendPushConfig(IMMessage message) {
        CustomPushContentProvider customConfig = NimUIKit.getCustomPushContentProvider();
        if (customConfig != null) {
            String content = customConfig.getPushContent(message);
            Map<String, Object> payload = customConfig.getPushPayload(message);
            message.setPushContent(content);
            message.setPushPayload(payload);
        }
    }

    @Override
    public void onInputPanelExpand() {
        messageListPanel.scrollToBottom();
    }

    @Override
    public void shouldCollapseInputPanel() {
        inputPanel.collapse(false);
    }

    @Override
    public boolean isLongClickEnabled() {
        return !inputPanel.isRecording();
    }

    @Override
    public void onItemFooterClick(IMMessage message) {
        if (messageListPanel.isSessionMode()) {
            RobotAttachment attachment = (RobotAttachment) message.getAttachment();
            NimRobotInfo robot = RobotInfoCache.getInstance().getRobotByAccount(attachment.getFromRobotAccount());
            aitManager.insertAitRobot(robot.getAccount(), robot.getName(), inputPanel.getEditSelectionStart());
        }
    }

    private View llPhotos;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        aitManager.onActivityResult(requestCode, resultCode, data);
        inputPanel.onActivityResult(requestCode, resultCode, data);
        messageListPanel.onActivityResult(requestCode, resultCode, data);
    }

    // 操作面板集合
    public List<BaseAction> getActionList() {
        List<BaseAction> actions = new ArrayList<>();
//        actions.add(new ImageAction());
//        actions.add(new VideoAction());

        if (customization != null && customization.actions != null) {
            actions.addAll(customization.actions);
        }
        return actions;
    }

    public String getAccount() {
        try {
            return inputPanel.getContainer().account;
        } catch (Exception e) {
            return null;
        }
    }

    public String getOppoAccount() {
        return sessionId;
    }

    /**
     * 发送已读回执
     */
    private void sendMsgReceipt() {
//        messageListPanel.sendReceipt();
    }

    /**
     * 收到已读回执
     */
    public void receiveReceipt() {
        messageListPanel.receiveReceipt();
    }


    /**
     * 私聊页修改主要逻辑
     */
    private int statusHeight = 0;
    private int screenHeight = 0;
    private int keyboardHeight = 583;
    private boolean isHasKey = false;
    private boolean lastStatus = false;
    private LinearLayout llInputAction;
    private LinearLayout llRecordAudio;
    private ImageView imvRecordAudio;
    private LinearLayout llAudio;
    private LinearLayout llGame;

    private LinearLayout llMoreItems;
    private View llGifts;
    private View llLinkMacro;

    @Override
    public void onMsgBlankRegionClicked() {
        llInputAction.setVisibility(View.VISIBLE);
        //键盘显示时所有键盘下面的View都隐藏
        llRecordAudio.setVisibility(View.GONE);
        if (null != inputPanel) {
            inputPanel.hideEmojiLayout();
            inputPanel.hideActionPanelLayout();
            inputPanel.hideInputMethod();
        }

        llAudio.setSelected(false);
        llGame.setSelected(false);
        llEmoji.setSelected(false);
        llMoreItems.setVisibility(View.GONE);
        llMore.setSelected(false);
    }

    private RecyclerView rvEmojiTips;
    private RecommEmojiListAdapter recommEmojiListAdapter;
    private LinearLayout llEmoji;
    private EditText editTextMessage;
    private TextView tvTextMessage;
    private View rootTopView;
    private View contentView;
    private boolean isMyFriend = false;
    private final int KEY_ORI_BOTTOM = R.id.emoticon_picker_view;
    private LinearLayout messageActivityBottomLayout;

    public static final String Extras_Show_In_Room = "showInRoom";
    private boolean showInRoom = false;

    /**
     * Show the soft input.
     *
     * @param view The view.
     */
    public static void showSoftInput(final View view) {
        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm == null) {
            return;
        }
        view.setFocusable(true);
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        imm.showSoftInput(view, InputMethodManager.SHOW_FORCED);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        LogUtils.d(TAG, "onActivityCreated-->parseIntent");
        parseIntent();
        try {
            NimUIKit.onServerEmojiOperaListener.onNeedInitEmojiFromServer(Long.valueOf(sessionId));
        } catch (NumberFormatException nef) {
            nef.printStackTrace();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LogUtils.d(TAG, "onDestroy");
        messageListPanel.onDestroy();
        registerObservers(false);
        if (null != aitManager) {
            aitManager.setTextChangeListener(null);
            aitManager.reset();
        }

        if (inputPanel != null) {
            inputPanel.onDestroy();
        }
        if (null != handler) {
            handler.removeCallbacksAndMessages(null);
        }
        if (null != imvRecordAudio) {
            imvRecordAudio.setOnTouchListener(null);
        }

        //清空Action对Activity的引用，避免内存泄露
        List<BaseAction> list = getActionList();
        if (list != null) {
            for (BaseAction action : list) {
                action.setActionCallback(null);
                action.setContainer(null);
            }
        }
        if (broadcastReceiver != null && getContext() != null) {
            LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(broadcastReceiver);
        }

        if (null != imvRecordAudio) {
            imvRecordAudio.clearAnimation();
        }
    }

    //连麦部分
    private LinearLayout llLinikMacro;
    private LinearLayout llLinikMacroStatus;
    private TextView tvLinkMacroStatus;
    private LinearLayout llLinikMacroClose;
    private ImageView imvCloseMacro;
    private ImageView imvFinishLinkMacro;
    private TextView tvCancelLinkMacro;
    private BaseAction dataAction;
    //数据Action结果监听
    private BaseAction.ActionCallback actionCallback = new BaseAction.ActionCallback() {
        @Override
        public void onActionSucceed(int code, Json json) {
            try {
                if (code == P2PMessageActivity.CANCEL_LINK_MACRO) {
                    finishLinkMacroSud();
                } else if (code == INVITATIOLN_LINK_MACRO) {
                    invitationLinkMacroSud();
                } else if (code == P2PMessageActivity.FINISH_LINK_MACRO) {
                    if (llLinikMacro != null) {
                        llLinikMacro.setVisibility(View.GONE);
                    }
                } else if (code == P2PMessageActivity.CLOSE_LINK_MACRO) {
                    closeOrOpenMacroSud();
                } else if (code == P2PMessageActivity.LINK_MICRO_TIME_OUT) {
                    SingleToastUtil.showToast("对方可能正忙，无法接听");
                } else if (code == BACK_ACTION) {
                    endLinkMicro(new BaseAction.ActionCallback() {
                        @Override
                        public void onActionSucceed(int code, Json json) {
                            exitLinkMacro();
                            if (showInRoom) {
                                P2pMicroLinkHelper.notifyChanged();
                            } else {
                                if (null != getActivity()) {
                                    getActivity().finish();
                                }
                            }
                        }

                        @Override
                        public void onActionFailed(int code, Json json) {
                            exitLinkMacro();
                            if (null != getActivity()) {
                                getActivity().finish();
                            }
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onActionFailed(int code, Json json) {

        }
    };

    public boolean onBackPressed() {
        if (inputPanel.collapse(true)) {
            if (llGame != null) {
                llGame.setSelected(false);
            }
            if (llEmoji != null) {
                llEmoji.setSelected(false);
            }
            return true;
        }
        if (messageListPanel.onBackPressed()) {
            return true;
        }
        if (llRecordAudio != null && llRecordAudio.getVisibility() == View.VISIBLE) {
            llRecordAudio.setVisibility(View.GONE);
            llAudio.setSelected(false);
            return true;
        }
        return false;
    }

    private int linkMacroDuring = 0;//连麦时长（秒）
    //连麦时长消息
    private static final int MSG_CODE_LINK_MACRO_TIMER = 1;
    //连麦对方未接受超时消息
    private static final int TIME_OUT = 60000;
    private static final int MSG_CODE_LINK_MACRO_TIME_OUT = 2;

    private Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == MSG_CODE_LINK_MACRO_TIMER) {
                String timeStr = String.format("%02d:%02d", linkMacroDuring / 60, linkMacroDuring % 60);
                tvLinkMacroStatus.setText(timeStr);
                linkMacroDuring++;
                removeMessages(MSG_CODE_LINK_MACRO_TIMER);
                sendEmptyMessageDelayed(MSG_CODE_LINK_MACRO_TIMER, 1000);
            } else if (msg.what == MSG_CODE_LINK_MACRO_TIME_OUT) {
                if (isLinkMacroing() && !isLinkMacroSuding()) {
                    //如果在连麦但是没有接通,则判断无应答
                    linkMacroTimeOut();
                }
            }
        }
    };

    public void setMyFriend(boolean myFriend) {
        isMyFriend = myFriend;
        LogUtils.d(TAG, "setMyFriend myFriend:" + myFriend);
    }

    private void registerLayoutListener() {
        messageActivityBottomLayout = rootView.findViewById(R.id.messageActivityBottomLayout);
        textMessageLayout = rootView.findViewById(R.id.textMessageLayout);
        initP2pModifyView();
        statusHeight = ResolutionUtils.getStatusBarHeight2(getContext());
        screenHeight = ResolutionUtils.getScreenHeight(getContext());
        rootTopView = (getActivity()).getWindow().getDecorView();
        contentView = ((ViewGroup) (rootView.getRootView().findViewById(android.R.id.content))).getChildAt(0);
        // (getActivity()).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        rootTopView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                rootTopView.getWindowVisibleDisplayFrame(r);
                int visibleHeight = r.bottom - r.top + statusHeight;

                keyboardHeight = screenHeight - (r.bottom - r.top);
                boolean visible = Math.abs(keyboardHeight + statusHeight) > screenHeight / 3;

                if (lastStatus != visible) {
                    lastStatus = visible;
                    if (!visible) {
                        isHasKey = false;
                        LogUtils.e("键盘隐藏");
                        closeInputLayout(messageActivityBottomLayout);
                    } else {
                        isHasKey = true;
                        openInputLayout(messageActivityBottomLayout, visibleHeight);
                        LogUtils.e("键盘显示");
                    }
                }
            }
        });
        if (getActivity() instanceof P2PMessageActivity) {
            ((P2PMessageActivity) getActivity()).initModifyData();
        }
    }

    private void openInputLayout(final View view, int visibleHeight) {
        int[] vPoint = new int[2];
        view.getLocationOnScreen(vPoint);
        int viewY = vPoint[1] + view.getMeasuredHeight();
        final int bottomMargin = viewY - visibleHeight;

        if (view.getParent() instanceof FrameLayout) {
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) view.getLayoutParams();
            view.setTag(KEY_ORI_BOTTOM, lp.bottomMargin);
            lp.bottomMargin += bottomMargin;
            view.setLayoutParams(lp);
        } else if (view.getParent() instanceof RelativeLayout) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) view.getLayoutParams();
            view.setTag(KEY_ORI_BOTTOM, lp.bottomMargin);
            lp.bottomMargin += bottomMargin;
            view.setLayoutParams(lp);
        } else if (view.getParent() instanceof LinearLayout) {
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) view.getLayoutParams();
            view.setTag(KEY_ORI_BOTTOM, lp.bottomMargin);
            lp.bottomMargin += bottomMargin;
            view.setLayoutParams(lp);
        }
        view.requestLayout();

        llInputAction.setVisibility(View.VISIBLE);
        //键盘显示时所有键盘下面的View都隐藏
        llRecordAudio.setVisibility(View.GONE);
        inputPanel.hideEmojiLayout();
        inputPanel.hideActionPanelLayout();
        llAudio.setSelected(false);
        llGame.setSelected(false);
        llEmoji.setSelected(false);
        llMoreItems.setVisibility(View.GONE);
        llMore.setSelected(false);

        //编辑框的处理
        //延时100毫秒获取焦点，如果直接弹出界面会抖动
        editTextMessage.postDelayed(new Runnable() {
            @Override
            public void run() {
                tvTextMessage.setVisibility(View.GONE);
                editTextMessage.setVisibility(View.VISIBLE);
                editTextMessage.setFocusable(true);
                editTextMessage.requestFocus();
            }
        }, 100);
    }

    private void closeInputLayout(final View view) {
        //显示Action列
        llInputAction.setVisibility(View.VISIBLE);

        if (view.getParent() instanceof FrameLayout) {
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) view.getLayoutParams();
            if (view.getTag(KEY_ORI_BOTTOM) != null) {
                lp.bottomMargin = (int) view.getTag(KEY_ORI_BOTTOM);
            } else {
                lp.bottomMargin = 0;
            }
        } else if (view.getParent() instanceof RelativeLayout) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) view.getLayoutParams();
            if (view.getTag(KEY_ORI_BOTTOM) != null) {
                lp.bottomMargin = (int) view.getTag(KEY_ORI_BOTTOM);
            } else {
                lp.bottomMargin = 0;
            }
        } else if (view.getParent() instanceof LinearLayout) {
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) view.getLayoutParams();
            if (view.getTag(KEY_ORI_BOTTOM) != null) {
                lp.bottomMargin = (int) view.getTag(KEY_ORI_BOTTOM);
            } else {
                lp.bottomMargin = 0;
            }
        }
        view.requestLayout();

        //编辑框的处理
        //延时80毫秒获取焦点，如果直接弹出界面会抖动
        editTextMessage.postDelayed(new Runnable() {
            @Override
            public void run() {
                editTextMessage.setVisibility(View.GONE);
                if (!TextUtils.isEmpty(editTextMessage.getText().toString())) {
                    tvTextMessage.setText(editTextMessage.getText().toString());
                } else {
                    tvTextMessage.setText(null);
                }
                tvTextMessage.setVisibility(View.VISIBLE);
                tvTextMessage.setFocusable(true);
                tvTextMessage.requestFocus();
            }
        }, 80);
    }

    public void showGames() {
        if (llGame != null) {
            llGame.callOnClick();
        }
    }

    private void parseIntent() {
        defaultOuterRingMaxWidth = ScreenUtil.dip2px(12);
        defaultAnimImgWidth = ScreenUtil.dip2px(92);
        sessionId = getArguments().getString(Extras.EXTRA_ACCOUNT);
        showInRoom = getArguments().getBoolean(Extras_Show_In_Room, false);
        LogUtils.d(TAG, "parseIntent sessionId:" + sessionId);
        sessionType = (SessionTypeEnum) getArguments().getSerializable(Extras.EXTRA_TYPE);
        IMMessage anchor = (IMMessage) getArguments().getSerializable(Extras.EXTRA_ANCHOR);
//        customization = (SessionCustomization) getArguments().getSerializable(Extras.EXTRA_CUSTOMIZATION);
        customization = NimUIKit.commonP2PSessionCustomization;
        Container container = new Container(getActivity(), sessionId, sessionType, this);
        if (messageListPanel == null) {
            messageListPanel = new MessageListPanelEx(container, rootView, anchor, false, false);
        } else {
            messageListPanel.reload(container, anchor);
        }

        if (inputPanel == null) {
            List<BaseAction> list = getActionList();
            inputPanel = new InputPanel(container, rootView, list);
            inputPanel.setCustomization(customization);
        } else {
            inputPanel.reload(container, customization);
        }
        inputPanel.setUiHandler(handler);
        aitManager = new AitManager(getContext(), sessionType == SessionTypeEnum.Team ? sessionId : null, true);

        inputPanel.addAitTextWatcher(aitManager);

        aitManager.setTextChangeListener(inputPanel);
        inputPanel.setIEmoticonSelectedListener(new IEmoticonSelectedListener() {
            @Override
            public void onEmojiSelected(String key, boolean isCustomEmoji) {
                if (null != inputPanel) {
                    if (inputPanel.onEmojiSelected(key, isCustomEmoji) && rlRecommEmoji.getVisibility() == View.VISIBLE) {
                        NimUIKit.onServerEmojiOperaListener.onReportEmojiSent(Long.valueOf(sessionId));
                        rlRecommEmoji.setVisibility(View.GONE);
                        if (null != messageListPanel) {
                            messageListPanel.scrollToBottom();
                        }
                    }
                }
            }

            @Override
            public void onStickerSelected(String categoryName, String stickerName) {
                if (null != inputPanel) {
                    inputPanel.onStickerSelected(categoryName, stickerName);
                }
            }
        });
        //  inputPanel.switchRobotMode(RobotInfoCache.getInstance().getRobotByAccount(sessionId) != null);

        registerObservers(true);

        if (customization != null) {
            messageListPanel.setChattingBackground(customization.backgroundUri, customization.backgroundColor);
        }

        registerLayoutListener();
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if ("getImageFile".equals(intent.getAction())) {
                int requestCode = intent.getIntExtra("requestCode", 0);
                int resultCode = intent.getIntExtra("resultCode", 0);
                onActivityResult(requestCode, resultCode, intent);
            } else if ("scrollToBottom".equals(intent.getAction())) {
                messageListPanel.scrollToBottom();
            } else if (P2PMessageActivity.ACTION_LINK_MICRO_FINISH.equals(intent.getAction())) {
                finishLinkMacroSud();
            } else if (P2PMessageActivity.ACTION_LINK_MICROING.equals(intent.getAction())) {
                linkMacroingSud(0, true);
            }
        }
    };

    /**
     * ****************** 观察者 **********************
     */
    private void registerObservers(boolean register) {
        MsgServiceObserve service = NIMClient.getService(MsgServiceObserve.class);
        service.observeReceiveMessage(incomingMessageObserver, register);
        service.observeMessageReceipt(messageReceiptObserver, register);
        SerEmojiTopicDataManager.getInstance().registerEmojiListDataChangeListener(register, this);
    }

    public void cancelAction() throws JSONException {
        Json json = new Json();
        json.put("action", BACK_ACTION);
        dataAction.setActionCallback(actionCallback);
        dataAction.setOutData(json);
        dataAction.onClick();
    }

    //是否已经发起了连麦流程
    public boolean isLinkMacroing() {
        return null != llLinikMacro && llLinikMacro.getVisibility() == View.VISIBLE;
    }

    //是否连麦成功，正在通话中
    public boolean isLinkMacroSuding() {
        return null != llLinikMacro && llLinikMacro.getVisibility() == View.VISIBLE &&
                null != imvCloseMacro && imvCloseMacro.getVisibility() == View.VISIBLE;
    }

    //结束通话，可能是取消或者挂断
    public void endLinkMicro(BaseAction.ActionCallback actionCallback) {
        boolean isLinkMacroing = isLinkMacroing();
        boolean isLinkMacroSuding = isLinkMacroSuding();
        LogUtils.d(TAG, "endLinkMicro isLinkMacroing:" + isLinkMacroing + " isLinkMacroSuding:" + isLinkMacroSuding);
        if (isLinkMacroing && !isLinkMacroSuding) {
            //如果是连麦邀请状态，则取消
            cancelLinkMacro(actionCallback);
        } else {
            finishLinkMacro(actionCallback);
        }
    }

    private void initP2pModifyView() {
        //获取从外部传入的逻辑Action
        if (getActionList() != null && getActionList().size() > 2) {
            dataAction = getActionList().get(2);
        }

        llLinikMacro = rootView.findViewById(R.id.llLinikMacro);
        llLinikMacroStatus = rootView.findViewById(R.id.llLinikMacroStatus);
        tvLinkMacroStatus = rootView.findViewById(R.id.tvLinkMacroStatus);
        llLinikMacroClose = rootView.findViewById(R.id.llLinikMacroClose);
        imvCloseMacro = rootView.findViewById(R.id.imvCloseMacro);
        imvFinishLinkMacro = rootView.findViewById(R.id.imvFinishLinkMacro);
        tvCancelLinkMacro = rootView.findViewById(R.id.tvCancelLinkMicro);

        imvCloseMacro.setSelected(true);
        imvCloseMacro.setImageResource(R.drawable.ic_p2p_linkmacro_kaimai);

        imvCloseMacro.setOnClickListener(p2pOnClickListener);
        imvFinishLinkMacro.setOnClickListener(p2pOnClickListener);
        tvCancelLinkMacro.setOnClickListener(p2pOnClickListener);

        llRecordAudio = rootView.findViewById(R.id.llRecordAudio);
        imvRecordAudio = rootView.findViewById(R.id.imvRecordAudio);

        llInputAction = rootView.findViewById(R.id.llInputAction);
        llAudio = rootView.findViewById(R.id.llAudio);
        llGame = rootView.findViewById(R.id.llGame);
        llEmoji = rootView.findViewById(R.id.llEmoji);
        llMore = rootView.findViewById(R.id.llMore);
        llTopic = rootView.findViewById(R.id.llTopic);

        editTextMessage = rootView.findViewById(R.id.editTextMessage);
        GrowingIO.getInstance().trackEditText(editTextMessage);
        tvTextMessage = rootView.findViewById(R.id.tvTextMessage);
        tvTextMessage.setOnClickListener(p2pOnClickListener);
        editTextMessage.setVisibility(View.VISIBLE);
        tvTextMessage.setVisibility(View.GONE);

        llAudio.setOnClickListener(p2pOnClickListener);
        llGame.setOnClickListener(p2pOnClickListener);
        llEmoji.setOnClickListener(p2pOnClickListener);
        llMore.setOnClickListener(p2pOnClickListener);
        llTopic.setOnClickListener(p2pOnClickListener);

        rlRecommEmoji = rootView.findViewById(R.id.rlRecommEmoji);
        rlRecommEmoji.setVisibility(View.GONE);
        rvEmojiTips = rootView.findViewById(R.id.rvEmojiTips);
        rootView.findViewById(R.id.ivCloseEmojiTips).setOnClickListener(p2pOnClickListener);
        recommEmojiListAdapter = new RecommEmojiListAdapter();
        rvEmojiTips.setAdapter(recommEmojiListAdapter);
        recommEmojiListAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (null != NimUIKit.onServerEmojiOperaListener && null != adapter.getItem(position) && null != inputPanel) {
                    if (inputPanel.sendTextMessage(((Entry) adapter.getItem(position)).getName())) {
                        NimUIKit.onServerEmojiOperaListener.onReportEmojiSent(Long.valueOf(sessionId));
                        rlRecommEmoji.setVisibility(View.GONE);
                        if (null != messageListPanel) {
                            messageListPanel.scrollToBottom();
                        }
                    }

                }
            }
        });

        llMoreItems = rootView.findViewById(R.id.llMoreItems);
        llPhotos = rootView.findViewById(R.id.llPhotos);
        llGifts = rootView.findViewById(R.id.llGifts);
        llLinkMacro = rootView.findViewById(R.id.llLinkMacro);
        llPhotos.setOnClickListener(p2pOnClickListener);
        llGifts.setOnClickListener(p2pOnClickListener);
        llLinkMacro.setOnClickListener(p2pOnClickListener);

        inputPanel.initAudioRecordButton(imvRecordAudio);
        inputPanel.setOnAudioRecordStatusChangeListener(new InputPanel.OnAudioRecordStatusChangeListener() {
            @Override
            public void onStartAudioRecord() {
                //默认全图
                //然后从内环开始，一点一点跑到外环
                //达到最大外环宽度，再循环往复，从内环开始
                //也即一开始 0 --> max   6 --> 250ms
                if (null != imvRecordAudio) {
                    imvRecordAudio.setImageResource(R.drawable.shape_dialog_btn_audio_record_press_anim);
                    Drawable loadingAnim = imvRecordAudio.getDrawable();
                    if (loadingAnim instanceof AnimationDrawable) {
                        ((AnimationDrawable) loadingAnim).start();
                    }
                }

            }

            @Override
            public void onEndAudioRecord(boolean cancel) {
                if (null != imvRecordAudio) {
                    imvRecordAudio.clearAnimation();
                    imvRecordAudio.setImageResource(R.drawable.shape_dialog_btn_audio_record_press_12);
                }
            }

            @Override
            public void onCancelAudioRecord(boolean cancel) {

            }
        });
        inputPanel.setActionStatusListener(new InputPanel.ActionStatusListener() {
            @Override
            public void changeEmojiBtnStatus(boolean isShow) {
                llEmoji.setSelected(isShow);
            }

            @Override
            public void changeGameBtnStatus(boolean isShow) {
                llGame.setSelected(isShow);
            }
        });

        //注册发送图片接受结果消息，可能引入MessageFragment的不是BaseMessageActivity，所以有可能收不到图片消息
        //这里从广播接收
        IntentFilter filter = new IntentFilter("getImageFile");
        filter.addAction("scrollToBottom");
        filter.addAction(P2PMessageActivity.ACTION_LINK_MICROING);
        filter.addAction(P2PMessageActivity.ACTION_LINK_MICRO_FINISH);
        LocalBroadcastManager.getInstance(getContext())
                .registerReceiver(broadcastReceiver, filter);
    }

    /**
     * 发送文本消息
     *
     * @param text
     * @return
     */
    public boolean sendTextMessage(String text) {
        boolean sendResult = false;
        if (null != inputPanel) {
            sendResult = inputPanel.sendTextMessage(text);
        }
        return sendResult;
    }

    //连麦超时
    private void linkMacroTimeOut() {
        try {
            Json json = new Json();
            json.put("action", P2PMessageActivity.LINK_MICRO_TIME_OUT);
            json.put("linkUid", getOppoAccount());
            json.put("type", 1);
            dataAction.setActionCallback(actionCallback);
            dataAction.setOutData(json);
            dataAction.onClick();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //结束连麦
    public void finishLinkMacro(BaseAction.ActionCallback actionCallback) {
        try {
            Json json = new Json();
            json.put("action", P2PMessageActivity.FINISH_LINK_MACRO);
            json.put("linkUid", getOppoAccount());
            json.put("talkTime", linkMacroDuring + "");
            dataAction.setActionCallback(actionCallback);
            dataAction.setOutData(json);
            dataAction.onClick();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //取消连麦
    public void cancelLinkMacro(BaseAction.ActionCallback actionCallback) {
        try {
            Json json = new Json();
            json.put("action", P2PMessageActivity.CANCEL_LINK_MACRO);
            json.put("linkUid", getOppoAccount());
            dataAction.setActionCallback(actionCallback);
            dataAction.setOutData(json);
            dataAction.onClick();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //显示语音通话通知栏
    public void showNotifyAudio() {
        try {
            Json json = new Json();
            json.put("action", P2PMessageActivity.SHOW_LINK_MICRO_NOFITY);
            dataAction.setActionCallback(actionCallback);
            dataAction.setOutData(json);
            dataAction.onClick();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void invitationLinkMacroSud() {
        if (llLinikMacro == null) {
            return;
        }
        llLinikMacro.setVisibility(View.VISIBLE);

        tvLinkMacroStatus.setText("等待接受");

        imvCloseMacro.setVisibility(View.GONE);
        tvCancelLinkMacro.setVisibility(View.VISIBLE);
        tvCancelLinkMacro.setText("取消");

        //一段时间后判断是否无应答
        handler.removeMessages(MSG_CODE_LINK_MACRO_TIME_OUT);
        handler.sendEmptyMessageDelayed(MSG_CODE_LINK_MACRO_TIME_OUT, TIME_OUT);
    }

    public void linkMacroingSud(long startLinkMicroTime, boolean isOpenMicro) {
        if (llLinikMacro == null) {
            return;
        }
        llLinikMacro.setVisibility(View.VISIBLE);
        //隐藏发语音按纽
        llRecordAudio.setVisibility(View.GONE);
        llAudio.setSelected(false);
        //如果是游戏连麦则连麦开始时间当外部传入进入计算
        if (startLinkMicroTime > 0) {
            long during = System.currentTimeMillis() - startLinkMicroTime;
            linkMacroDuring = (int) (during / 1000);
            String timeStr = String.format("聊天时长%02d:%02d", linkMacroDuring / 60, linkMacroDuring % 60);
            tvLinkMacroStatus.setText(timeStr);
        } else {
            tvLinkMacroStatus.setText("00:00");
            linkMacroDuring = 0;
        }
        if (isOpenMicro) {
            imvCloseMacro.setSelected(true);
            imvCloseMacro.setImageResource(R.drawable.ic_p2p_linkmacro_kaimai);
        } else {
            imvCloseMacro.setSelected(false);
            imvCloseMacro.setImageResource(R.drawable.ic_p2p_linkmacro_bimai);
        }

        imvCloseMacro.setVisibility(View.VISIBLE);
        tvCancelLinkMacro.setVisibility(View.GONE);

        //显示通话通知栏
        //showNotifyAudio();

        //开启连麦时间计时器
        handler.removeMessages(MSG_CODE_LINK_MACRO_TIMER);
        handler.sendEmptyMessage(MSG_CODE_LINK_MACRO_TIMER);
    }

    public void closeOrOpenMacroSud() {
        if (imvCloseMacro == null) {
            return;
        }
        imvCloseMacro.setVisibility(View.VISIBLE);
        imvCloseMacro.setSelected(!imvCloseMacro.isSelected());

        if (imvCloseMacro.isSelected()) {
            imvCloseMacro.setImageResource(R.drawable.ic_p2p_linkmacro_kaimai);
        } else {
            imvCloseMacro.setImageResource(R.drawable.ic_p2p_linkmacro_bimai);
        }
    }

    public void finishLinkMacroSud() {
        if (llLinikMacro == null || handler == null) {
            return;
        }
        handler.removeMessages(MSG_CODE_LINK_MACRO_TIMER);
        llLinikMacro.setVisibility(View.GONE);
    }

    public void scrollToBottom() {
        if (messageListPanel != null) {
            messageListPanel.scrollToBottom();
        }
    }

    //退出连麦
    public void exitLinkMacro() {
        try {
            Json json = new Json();
            json.put("action", P2PMessageActivity.LINK_MICRO_EXIT);
            dataAction.setOutData(json);
            dataAction.onClick();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Container getContainer() {
        if (inputPanel == null) {
            return null;
        }
        return inputPanel.getContainer();
    }

    public void closeOrOpenMacro(boolean isOpen) {
        try {
            Json json = new Json();
            json.put("action", P2PMessageActivity.CLOSE_LINK_MACRO);
            json.put("isOpen", isOpen);
            dataAction.setActionCallback(actionCallback);
            dataAction.setOutData(json);
            dataAction.onClick();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onEmojiListCHanged(long targetUid, boolean showEmojiList) {
        LogUtils.d(TAG, "onEmojiListCHanged targetUid:" + targetUid + " sessionId:" + sessionId + " showEmojiList:" + showEmojiList);
        if (!TextUtils.isEmpty(sessionId) && sessionId.equals(String.valueOf(targetUid))) {
            if (showEmojiList) {
                rlRecommEmoji.setVisibility(View.VISIBLE);
                recommEmojiListAdapter.setNewData(SerEmojiTopicDataManager.getInstance().serverChildrenEmojiList);
                messageListPanel.refreshMessageList();
            } else {
                rlRecommEmoji.setVisibility(View.GONE);
            }
        }
        messageListPanel.doScrollToBottom();
    }

    @Override
    public void onTopicUpdated(long targetUid, String topic) {
        LogUtils.d(TAG, "onEmojiListCHanged targetUid:" + targetUid + " sessionId:" + sessionId + " topic:" + topic);
        if (!TextUtils.isEmpty(sessionId) && sessionId.equals(String.valueOf(targetUid))
                && null != inputPanel && !TextUtils.isEmpty(topic)) {
            inputPanel.sendTextMessage(topic);
        }
    }
}
