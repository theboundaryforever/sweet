package com.netease.nim.uikit.session.actions;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.text.TextUtils;

import com.netease.nim.uikit.R;
import com.netease.nim.uikit.session.constant.RequestCode;
import com.netease.nim.uikit.session.helper.VideoMessageHelper;
import com.netease.nimlib.sdk.msg.MessageBuilder;
import com.netease.nimlib.sdk.msg.model.IMMessage;

import java.io.File;

/**
 * Created by hzxuwen on 2015/6/12.
 */
public class VideoAction extends BaseAction {
    // 视频
//    protected VideoMessageHelper videoMessageHelper;

    public VideoAction() {
        super(R.drawable.icon_video_action, R.string.input_panel_video);

    }

    @Override
    public void onClick() {
        VideoMessageHelper videoMessageHelper = initVideoMessageHelper();
        if (null != videoMessageHelper) {
            videoMessageHelper.showVideoSource(makeRequestCode(RequestCode.GET_LOCAL_VIDEO), makeRequestCode(RequestCode.CAPTURE_VIDEO));
        }
    }

    /**
     * ********************** 视频 *******************************
     */
    private VideoMessageHelper initVideoMessageHelper() {
        VideoMessageHelper videoMessageHelper = null;
        if (null != getActivity()) {
            videoMessageHelper = new VideoMessageHelper(getActivity(), new VideoMessageHelper.VideoMessageHelperListener() {

                @Override
                public void onVideoPicked(File file, String md5) {
                    MediaPlayer mediaPlayer = getVideoMediaPlayer(file);
                    long duration = mediaPlayer == null ? 0 : mediaPlayer.getDuration();
                    int height = mediaPlayer == null ? 0 : mediaPlayer.getVideoHeight();
                    int width = mediaPlayer == null ? 0 : mediaPlayer.getVideoWidth();
                    if (!TextUtils.isEmpty(getAccount())) {
                        IMMessage message = MessageBuilder.createVideoMessage(getAccount(), getSessionType(), file, duration, width, height, md5);
                        sendMessage(message);
                    }
                }
            });
        }
        return videoMessageHelper;
    }

    /**
     * 获取视频mediaPlayer
     * @param file 视频文件
     * @return mediaPlayer
     */
    private MediaPlayer getVideoMediaPlayer(File file) {
        try {
            return MediaPlayer.create(getActivity(), Uri.fromFile(file));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
        case RequestCode.GET_LOCAL_VIDEO:
//            videoHelper().onGetLocalVideoResult(data);
            break;
        case RequestCode.CAPTURE_VIDEO:
//            videoHelper().onCaptureVideoResult(data);
            break;
        }
    }

//    private VideoMessageHelper videoHelper() {
//        if (videoMessageHelper == null) {
//            initVideoMessageHelper();
//        }
//        return videoMessageHelper;
//    }
}
