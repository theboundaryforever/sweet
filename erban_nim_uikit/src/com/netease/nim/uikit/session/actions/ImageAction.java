package com.netease.nim.uikit.session.actions;

import android.text.TextUtils;

import com.netease.nim.uikit.R;
import com.netease.nimlib.sdk.msg.MessageBuilder;
import com.netease.nimlib.sdk.msg.model.IMMessage;

import java.io.File;

/**
 * Created by hzxuwen on 2015/6/12.
 */
public class ImageAction extends PickImageAction {

    public ImageAction() {
        super(R.drawable.icon_pic_action, R.string.input_panel_photo, true);
    }

    @Override
    protected void onPicked(File file) {
        try {
            if (!TextUtils.isEmpty(getAccount())) {
                IMMessage message = MessageBuilder.createImageMessage(getAccount(), getSessionType(), file, file.getName());
                sendMessage(message);
            }
        } catch (Exception ex) {
            //bugly上记录显示getAccount()有可能报空指针
            ex.printStackTrace();
        }
    }
}

