package com.netease.nim.uikit.session.viewholder;

import android.graphics.Color;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ImageSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.juxiao.library_utils.DisplayUtils;
import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.R;
import com.netease.nim.uikit.common.ui.recyclerview.adapter.BaseMultiItemFetchLoadAdapter;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.netease.nim.uikit.session.emoji.Entry;
import com.netease.nim.uikit.session.emoji.MoonUtil;
import com.netease.nim.uikit.session.emoji.SerEmojiTopicDataManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.StringUtils;

/**
 * Created by zhoujianghua on 2015/8/4.
 */
public class MsgViewHolderText extends MsgViewHolderBase {

    protected TextView bodyTextView;
    private ImageView ivEmoji;

    public MsgViewHolderText(BaseMultiItemFetchLoadAdapter adapter) {
        super(adapter);
    }

    @Override
    protected int getContentResId() {
        return R.layout.nim_message_item_text;
    }

    @Override
    protected void inflateContentView() {
        bodyTextView = findViewById(R.id.nim_message_item_text_body);
        ivEmoji = findViewById(R.id.ivEmoji);
    }

    @Override
    protected void bindContentView() {
        //设置控件背景，以区分消息接收方和发送方
        int serEmojiSize = layoutDirection();
        bodyTextView.setTextColor(isReceivedMessage() ? Color.BLACK : Color.WHITE);
        bodyTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick();
            }
        });
        MoonUtil.identifyFaceExpression(NimUIKit.getContext(), bodyTextView, ivEmoji,
                getDisplayText(), ImageSpan.ALIGN_BOTTOM, serEmojiSize);
        bodyTextView.setMovementMethod(LinkMovementMethod.getInstance());
        bodyTextView.setOnLongClickListener(longClickListener);

        bodyTextView.setMaxWidth(ScreenUtil.dip2px(222));
        int minHeight = ScreenUtil.dip2px(38);
        bodyTextView.setMinHeight(minHeight);
        String content = getDisplayText();
        int contentLength = 0;
        if (!TextUtils.isEmpty(content)) {
            contentLength = StringUtils.getStrLength(content);
        }
        if (isReceivedMessage()) {
            bodyTextView.setBackgroundResource(R.drawable.nim_message_content_left_bg);
            bodyTextView.setPadding(ScreenUtil.dip2px(10), ScreenUtil.dip2px(contentLength > 32 ? 8 : 10), ScreenUtil.dip2px(10), ScreenUtil.dip2px(contentLength > 32 ? 8 : 10));
        } else {
            bodyTextView.setBackgroundResource(R.drawable.nim_message_content_right_bg);
            bodyTextView.setPadding(ScreenUtil.dip2px(10), ScreenUtil.dip2px(contentLength > 32 ? 8 : 10), ScreenUtil.dip2px(10), ScreenUtil.dip2px(contentLength > 32 ? 8 : 10));
        }
    }

    private int layoutDirection() {
        int serEmojiSize = DisplayUtils.dip2px(context, 86);
        bodyTextView.setVisibility(View.VISIBLE);
        ivEmoji.setVisibility(View.GONE);
        bodyTextView.setTag(null);
        bodyTextView.setText(null);
        Entry entry = SerEmojiTopicDataManager.getInstance().getDisplayEmojiEntry(getDisplayText());
        if (null != entry) {
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) ivEmoji.getLayoutParams();
            layoutParams.weight = serEmojiSize;
            layoutParams.height = serEmojiSize;
            ivEmoji.setLayoutParams(layoutParams);
        }

        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) bodyTextView.getLayoutParams();
        layoutParams.weight = LinearLayout.LayoutParams.WRAP_CONTENT;
        layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        bodyTextView.setLayoutParams(layoutParams);

        return serEmojiSize;
    }

    @Override
    protected int leftBackground() {
        return 0;
    }

    @Override
    protected int rightBackground() {
        return 0;
    }

    protected String getDisplayText() {
        String displayText = message.getContent();
        if (!TextUtils.isEmpty(displayText)) {
            displayText = displayText.replace("\\n", "\n");
        }
        LogUtils.d(MsgViewHolderText.class.getSimpleName(), "getDisplayText-displayText:" + displayText);
        return displayText;
    }
}
