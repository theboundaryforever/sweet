package com.netease.nim.uikit.session;

import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.msg.MsgService;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;

public class NIMSessionStatusManager {

    private NIMSessionStatusManager() {

    }

    private static NIMSessionStatusManager instance = null;

    public static NIMSessionStatusManager getInstance() {
        if (null == instance) {
            instance = new NIMSessionStatusManager();
        }
        return instance;
    }

    public boolean isChatting() {
        return isChatting;
    }

    private boolean isChatting = false;

    public void updateChattingAccount(String account, SessionTypeEnum sessionType) {
        NIMClient.getService(MsgService.class).setChattingAccount(account, sessionType);
        isChatting = account != MsgService.MSG_CHATTING_ACCOUNT_NONE;
    }
}
