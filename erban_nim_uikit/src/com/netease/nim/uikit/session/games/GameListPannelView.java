package com.netease.nim.uikit.session.games;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.netease.nim.uikit.R;

/**
 * @author weihaitao
 * @date 2019/9/4
 */
public class GameListPannelView extends FrameLayout {

    private Context context;

    public GameListPannelView(Context context) {
        super(context);

        init(context);
    }

    public GameListPannelView(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(context);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public GameListPannelView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        init(context);
    }

    private void init(Context context) {
        this.context = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.nim_message_activity_actions_layout, this);
    }
}
