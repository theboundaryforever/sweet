package com.netease.nim.uikit.session.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.Glide;
import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.OnlineStateChangeListener;
import com.netease.nim.uikit.R;
import com.netease.nim.uikit.cache.FriendDataCache;
import com.netease.nim.uikit.session.SessionCustomization;
import com.netease.nim.uikit.session.actions.BaseAction;
import com.netease.nim.uikit.session.constant.Extras;
import com.netease.nim.uikit.session.fragment.MessageFragment;
import com.netease.nim.uikit.uinfo.UserInfoHelper;
import com.netease.nim.uikit.uinfo.UserInfoObservable;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.Observer;
import com.netease.nimlib.sdk.friend.FriendService;
import com.netease.nimlib.sdk.msg.MessageBuilder;
import com.netease.nimlib.sdk.msg.MsgServiceObserve;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.CustomNotification;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.tongdaxing.erban.libcommon.utils.LogUtil;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.json.Json;

import org.json.JSONException;

import java.util.List;
import java.util.Set;


/**
 * 点对点聊天界面
 * <p/>
 * Created by huangjun on 2015/2/1.
 */
public class P2PMessageActivity extends BaseMessageActivity {

    private final String TAG = P2PMessageActivity.class.getSimpleName();

    public final static int CUSTOM_ACTION_COUNT = 3;//自定义Action个数
    //以下是业务层操作的ACTION
    public final static int GET_RUNNING_INFO = 1;//获取对方进入的房间信息
    public final static int ENTER_RUNNING_ROOM = 2;//进入对方所在的房间
    public final static int INVITATIOLN_TIMER = 3;//游戏邀请倒计时的操作
    public final static int IS_ATTATION = 4;//是否关注了对方
    public final static int ATTATION = 5;//关注或取消关注对方
    //连麦相关
    public final static int INVITATIOLN_LINK_MACRO = 6;//邀请对方连麦
    public final static int CLOSE_LINK_MACRO = 7;//闭麦
    public final static int CANCEL_LINK_MACRO = 8;//发送方主动取消连麦
    public final static int FINISH_LINK_MACRO = 9;//结束连麦
    public final static int RUFRUSE_LINK_MACRO = 10;//拒绝连麦
    public final static int AGREE_LINK_MACRO = 11;//同意连麦
    public final static int BACK_ACTION = 12;//返回按纽动作
    public final static int LINK_MICRO_TIME_OUT = 13;//连麦超时
    public final static int LINK_MICRO_EXIT = 14;//退出连麦频道
    public final static int SHOW_LINK_MICRO_NOFITY = 15;//显示连麦通知栏

    //连麦广播动作
    public final static String ACTION_LINK_MICROING = "ACTION_LINK_MICROING";//正在连麦
    public final static String ACTION_LINK_MICRO_FINISH = "ACTION_LINK_MICRO_FINISH";//结束连麦

    public final static String ACTION_FINISH = "P2P_ACTION_FINISH";//结束页面

    FriendDataCache.FriendDataChangedObserver friendDataChangedObserver = new FriendDataCache.FriendDataChangedObserver() {
        @Override
        public void onAddedOrUpdatedFriends(List<String> accounts) {
            setTitle(UserInfoHelper.getUserTitleName(sessionId, SessionTypeEnum.P2P));
        }

        @Override
        public void onDeletedFriends(List<String> accounts) {
            setTitle(UserInfoHelper.getUserTitleName(sessionId, SessionTypeEnum.P2P));
        }

        @Override
        public void onAddUserToBlackList(List<String> account) {
            setTitle(UserInfoHelper.getUserTitleName(sessionId, SessionTypeEnum.P2P));
        }

        @Override
        public void onRemoveUserFromBlackList(List<String> account) {
            setTitle(UserInfoHelper.getUserTitleName(sessionId, SessionTypeEnum.P2P));
        }
    };
    OnlineStateChangeListener onlineStateChangeListener = new OnlineStateChangeListener() {
        @Override
        public void onlineStateChange(Set<String> accounts) {
            // 更新 toolbar
            if (accounts.contains(sessionId)) {
                // 按照交互来展示
                displayOnlineState();
            }
        }
    };

    private boolean isResume = false;
    /**
     * 命令消息接收观察者
     */
    Observer<CustomNotification> commandObserver = new Observer<CustomNotification>() {
        @Override
        public void onEvent(CustomNotification message) {
            if (!sessionId.equals(message.getSessionId()) || message.getSessionType() != SessionTypeEnum.P2P) {
                return;
            }
            showCommandMessage(message);
        }
    };

    private MessageFragment fragment;
    private UserInfoObservable.UserInfoObserver uinfoObserver;

    //以下是私聊页修改主逻辑
    private TextView bltvAttention;
    private LinearLayout llRoomRuning;
    private ImageView imvRuningGif;
    private TextView tvRuningRoomInfo;
    private ImageView imvMore;
    private TextView tvNickTitle;
    private ImageView ivBack;
    //是否已经关注了
    private boolean isAttentioned = false;
    private long oppRoomId;
    private int oppRoomType = 3;
    private BaseAction dataAction;
    private boolean isMyFriend = false;
    //数据Action结果监听
    private BaseAction.ActionCallback actionCallback = new BaseAction.ActionCallback() {
        @Override
        public void onActionSucceed(int code, Json json) {
            try {
                if (code == IS_ATTATION) {
                    boolean isLike = json.getBoolean("isLike");
                    LogUtils.d(TAG, "onActionSucceed-->IS_ATTATION isLike:" + isLike);
                    if (!isLike) {
                        showAttention();
                    }
                } else if (code == ATTATION) {
                    LogUtils.d(TAG, "onActionSucceed-->ATTATION isAttentioned:" + isAttentioned);
                    if (isAttentioned) {
                        bltvAttention.setVisibility(View.VISIBLE);
                        SingleToastUtil.showToast("取消关注成功");
                        isMyFriend = false;
                        fragment.setMyFriend(isMyFriend);
                    } else {
                        bltvAttention.setVisibility(View.GONE);
                        SingleToastUtil.showToast("关注成功");
                        //产品要求所有场景的关注对方，都需要发送[我关注了你]文本消息，因此改为后端发送
//                        fragment.sendTextMessage(getResources().getString(R.string.p2p_message_attention_msg));
                        isMyFriend = NIMClient.getService(FriendService.class).isMyFriend(fragment.getAccount());
                        fragment.setMyFriend(isMyFriend);
                    }
                    LogUtils.d(TAG, "onActionSucceed-->ATTATION isMyFriend:" + isMyFriend);
                    isAttentioned = !isAttentioned;
                } else if (code == GET_RUNNING_INFO) {
                    String runningInfo = json.getString("runningInfo");
                    LogUtils.d(TAG, "onActionSucceed-->GET_RUNNING_INFO runningInfo:" + runningInfo);
                    oppRoomId = json.getLong("roomId");
                    if (json.has("roomType")) {
                        oppRoomType = json.num("roomType");
                    }
                    if (!TextUtils.isEmpty(runningInfo) && oppRoomId != 0) {
                        llRoomRuning.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (fragment.isLinkMacroing()) {
                                    SingleToastUtil.showToast("连麦中不能进入房间");
                                    return;
                                }
                                enterRoom();
                            }
                        });
                        Glide.with(P2PMessageActivity.this).asGif().load(R.drawable.ic_p2p_modify_room_runing).into(imvRuningGif);
                        tvRuningRoomInfo.setText(runningInfo);
                        llRoomRuning.setVisibility(View.VISIBLE);
                        if (fragment != null) {
                            fragment.scrollToBottom();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onActionFailed(int code, Json json) {

        }
    };

    public static void start(Context context, String contactId, SessionCustomization customization, IMMessage anchor) {
        start(context, contactId, customization, anchor, null);
    }

    public static void start(Context context, String contactId, SessionCustomization customization, IMMessage anchor, Bundle arg) {
        Intent intent = new Intent();
        intent.putExtra(Extras.EXTRA_ACCOUNT, contactId);
//        intent.putExtra(Extras.EXTRA_CUSTOMIZATION, customization);
        intent.putExtra("out_arg", arg);
        if (anchor != null) {
            intent.putExtra(Extras.EXTRA_ANCHOR, anchor);
        }
        intent.setClass(context, P2PMessageActivity.class);
        //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //FLAG_ACTIVITY_SINGLE_TOP模式有问题,如果当前启动后私聊页,然后再启动不同账号的私聊页,则账号不会更新
        //改为FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP模式
        // intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        context.startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        // 单聊特例话数据，包括个人信息，
        setTitle(UserInfoHelper.getUserTitleName(sessionId, SessionTypeEnum.P2P));
        displayOnlineState();
        registerObservers(true);
        registerOnlineStateChangeListener(true);

        IntentFilter filter = new IntentFilter(ACTION_FINISH);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, filter);
    }

    private void initView() {
        tvNickTitle = (TextView) findViewById(R.id.tvNickTitle);
        ivBack = (ImageView) findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fragment != null && fragment.isLinkMacroing()) {
                    try {
                        fragment.cancelAction();
                    } catch (Exception e) {
                        e.printStackTrace();
                        onNavigateUpClicked();
                    }
                }
                finish();
            }
        });
        imvMore = (ImageView) findViewById(R.id.imvMore);
        bltvAttention = (TextView) findViewById(R.id.bltvAttention);
        bltvAttention.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String attType = "1";
                if (isAttentioned) {
                    attType = "2";
                }
                attention(attType);
            }
        });
        bltvAttention.setVisibility(View.GONE);
        llRoomRuning = (LinearLayout) findViewById(R.id.llRoomRuning);
        imvRuningGif = (ImageView) findViewById(R.id.imvRuningGif);
        tvRuningRoomInfo = (TextView) findViewById(R.id.tvRuningRoomInfo);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_FINISH.equals(intent.getAction())) {
                finish();
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        registerObservers(false);
        registerOnlineStateChangeListener(false);

        if (broadcastReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isResume = true;
        continueTimer();
        //避免在私聊界面，打开召集令消息进入房间后，仍旧收到同一用户的消息，首页底部未读红点提示逻辑未能清楚的问题
        if (null != NimUIKit.onP2PSessionOpenListener) {
            NimUIKit.onP2PSessionOpenListener.onP2PSessionOpened(sessionId);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        isResume = false;
        pauseTimer();
    }

    private void registerObservers(boolean register) {
        if (register) {
            registerUserInfoObserver();
        } else {
            unregisterUserInfoObserver();
        }
        NIMClient.getService(MsgServiceObserve.class).observeCustomNotification(commandObserver, register);
        FriendDataCache.getInstance().registerFriendDataChangedObserver(friendDataChangedObserver, register);
    }

    private void registerOnlineStateChangeListener(boolean register) {
        if (!NimUIKit.enableOnlineState()) {
            return;
        }
        if (register) {
            NimUIKit.addOnlineStateChangeListeners(onlineStateChangeListener);
        } else {
            NimUIKit.removeOnlineStateChangeListeners(onlineStateChangeListener);
        }
    }

    private void displayOnlineState() {
        if (!NimUIKit.enableOnlineState()) {
            return;
        }
        //TODO 如果开启了监听给对方在线功能，那么UI就需要修改了
    }

    private void registerUserInfoObserver() {
        if (uinfoObserver == null) {
            uinfoObserver = new UserInfoObservable.UserInfoObserver() {
                @Override
                public void onUserInfoChanged(List<String> accounts) {
                    if (accounts.contains(sessionId)) {
                        setTitle(UserInfoHelper.getUserTitleName(sessionId, SessionTypeEnum.P2P));
                    }
                }
            };
        }

        UserInfoHelper.registerObserver(uinfoObserver);
    }

    private void unregisterUserInfoObserver() {
        if (uinfoObserver != null) {
            UserInfoHelper.unregisterObserver(uinfoObserver);
        }
    }

    protected void showCommandMessage(CustomNotification message) {
        if (!isResume) {
            return;
        }
        String content = message.getContent();
        try {
            JSONObject json = JSON.parseObject(content);
            int id = json.getIntValue("id");
            if (isResume) {
                if (id == 1) {
                    // 正在输入
                    SingleToastUtil.showToast(P2PMessageActivity.this, "对方正在输入...");
                } else {
//                    ToastCompat.makeText(P2PMessageActivity.this, "command: " + content, ToastCompat.LENGTH_SHORT).show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected MessageFragment fragment() {
        fragment = new MessageFragment();
        Bundle arguments = getIntent().getExtras();
        if (null != arguments) {
            arguments.putSerializable(Extras.EXTRA_TYPE, SessionTypeEnum.P2P);
            fragment.setArguments(arguments);
        }
        fragment.setContainerId(R.id.message_fragment_container);
        return fragment;
    }

    @Override
    protected int getContentViewId() {
        return R.layout.nim_message_activity;
    }

    @Override
    protected void initToolBar() {
    }

    //拦截返回按纽
    @Override
    public void onNavigateUpClicked() {
        if (fragment != null && fragment.isLinkMacroing() && dataAction != null) {
            try {
                fragment.cancelAction();
            } catch (Exception e) {
                e.printStackTrace();
                super.onNavigateUpClicked();
            }
            return;
        }
        super.onNavigateUpClicked();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //连麦中禁掉返回按纽
            if (fragment != null && fragment.isLinkMacroing()) {
                try {
                    fragment.cancelAction();
                } catch (Exception e) {
                    e.printStackTrace();
                    super.onNavigateUpClicked();
                }

                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        try {
            String account = intent.getStringExtra(Extras.EXTRA_ACCOUNT);
            if (TextUtils.isEmpty(account)) {
                return;
            }
            if (!fragment.getOppoAccount().equals(account)) {
                finish();
                startActivity(intent);
            } else {
                dealInitBundle(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
            dealInitBundle(intent);
        }
    }

    //根据传入的参数处理初始状态
    private void dealInitBundle(Intent intent) {
        Bundle arg = intent.getBundleExtra("out_arg");
        if (arg != null) {
            String showAction = arg.getString("showAction");
            if (!TextUtils.isEmpty(showAction)) {
                if (showAction.equals("games")) {
                    fragment.showGames();
                } else if (showAction.equals("showGamesResult")) {
                    showGameResultPopView(arg);
                }
            }
            //连麦相关操作
            int linkMacroStatus = arg.getInt("linkMacro_status", -1);
            if (linkMacroStatus == AGREE_LINK_MACRO) {
                boolean isOpenMicro = arg.getBoolean("isOpenMicro", true);
                long linkStartTime = arg.getLong("linkStartTime", 0);
                fragment.linkMacroingSud(linkStartTime, isOpenMicro);
            } else if (linkMacroStatus == RUFRUSE_LINK_MACRO) {
                fragment.finishLinkMacroSud();
            } else if (linkMacroStatus == FINISH_LINK_MACRO) {
                fragment.finishLinkMacroSud();
            } else if (linkMacroStatus == CANCEL_LINK_MACRO) {
                fragment.finishLinkMacroSud();
            }
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        if (null != tvNickTitle) {
            tvNickTitle.setText(title);
        }
    }

    //这里的初始化必须等到fragment初始化完成才被fragment调用
    public void initModifyData() {
        //禁止掉左滑退出,因为假如是连麦中，这样退出体验不好
        setSwipeBackEnable(false);
        imvMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //跳转到个人资料页
                try {
                    String account = fragment.getOppoAccount();
                    SessionTypeEnum sessionType = SessionTypeEnum.P2P;
                    String text = "this is an example";
                    IMMessage textMessage = MessageBuilder.createTextMessage(account, sessionType, text);
                    textMessage.setFromAccount(account);
                    NimUIKit.getSessionListener().onAvatarClicked(P2PMessageActivity.this, textMessage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        //处理初始状态
        dealInitBundle(getIntent());

        if (fragment.getActionList().size() <= 2) {
            return;
        }
        dataAction = fragment.getActionList().get(2);
        //是否关注
        if (!TextUtils.isEmpty(fragment.getAccount())) {
            try {
                Json json = new Json();
                json.put("action", IS_ATTATION);
                json.put("likeUid", fragment.getAccount());
                dataAction.setActionCallback(actionCallback);
                dataAction.setOutData(json);
                dataAction.onClick();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //获取房间信息
        try {
            Json json = new Json();
            json.put("action", GET_RUNNING_INFO);
            json.put("uid", fragment.getOppoAccount());
            dataAction.setActionCallback(actionCallback);
            dataAction.setOutData(json);
            dataAction.onClick();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //是好友则显示连麦按纽
        isMyFriend = NIMClient.getService(FriendService.class).isMyFriend(fragment.getAccount());
        LogUtils.d(TAG, "initModifyData isMyFriend:" + isMyFriend);
        fragment.setMyFriend(isMyFriend);
    }

    //开启邀请消息的定时器
    private void continueTimer() {
        if (dataAction != null) {
            Json json = new Json();
            try {
                json.put("action", INVITATIOLN_TIMER);
                json.put("isContinue", true);
                dataAction.setOutData(json);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //暂停邀请消息的定时器
    private void pauseTimer() {
        if (dataAction != null) {
            Json json = new Json();
            try {
                json.put("action", INVITATIOLN_TIMER);
                json.put("isContinue", false);
                dataAction.setOutData(json);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void enterRoom() {
        if (oppRoomId == 0) {
            LogUtil.e("oppRoomId is 0");
            return;
        }

        if (dataAction != null) {
            Json json = new Json();
            try {
                json.put("action", ENTER_RUNNING_ROOM);
                json.put("roomId", oppRoomId);
                json.put("roomType", oppRoomType);
                dataAction.setActionCallback(actionCallback);
                dataAction.setOutData(json);
                dataAction.onClick();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void showAttention() {
        isAttentioned = false;
        bltvAttention.setVisibility(View.VISIBLE);
    }

    /**
     * 关注或取消关注
     *
     * @param attType //1:关注 2:取消关注
     */
    private void attention(final String attType) {
        //判断是否已经关注过了
        if (dataAction == null) {
            return;
        }
        try {
            Json json = new Json();
            json.put("action", ATTATION);
            json.put("attType", attType);
            json.put("likeUid", fragment.getAccount());
            dataAction.setActionCallback(actionCallback);
            dataAction.setOutData(json);
            dataAction.onClick();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //显示游戏结果弹窗
    private void showGameResultPopView(Bundle arg) {
        if (arg == null || !arg.containsKey("result") || !arg.containsKey("gameName")) {
            return;
        }
        String meAvatar = arg.getString("meAvatar");
        String meNick = arg.getString("meNick");
        String fromAvatar = arg.getString("fromAvatar");
        String fromNick = arg.getString("fromNick");
        int result = arg.getInt("result", 0);
        // long roomid = intent.getLongExtra("roomid", 0);
        // String gameImage = intent.getStringExtra("gameImage");
        String gameName = arg.getString("gameName");
        String gameType = arg.getString("gameType");
        int incScore = arg.getInt("incScore", 0);
        int oppWinCount = arg.getInt("oppWinCount", 0);
        int ownerWinCount = arg.getInt("ownerWinCount", 0);

        //在游戏列表中找出当前显示的游戏所在的位置
        //这是为了再次邀请时能得到游戏信息
        int position = -1;
        try {
            for (int i = 0; i < fragment.getActionList().size(); i++) {
                BaseAction action = fragment.getActionList().get(i);
                if (gameName.equals(action.getTitle())) {
                    position = i;
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        final View vTarget = LayoutInflater.from(getApplicationContext()).inflate(R.layout.view_minigame_win_pop, null);
        ImageView imvGameStatus = vTarget.findViewById(R.id.imvGameStatus);
        LinearLayout rlMain = vTarget.findViewById(R.id.rlMain);
        TextView tvAddScore = vTarget.findViewById(R.id.tvAddScore);
        ImageView imvWiner = vTarget.findViewById(R.id.imvWiner);
        TextView tvWinerName = vTarget.findViewById(R.id.tvWinerName);
        TextView tvScore = vTarget.findViewById(R.id.tvScore);
        ImageView imvLoser = vTarget.findViewById(R.id.imvLoser);
        TextView tvLoserName = vTarget.findViewById(R.id.tvLoserName);
        TextView tvAgain = vTarget.findViewById(R.id.tvAgain);
        TextView tvReturnHome = vTarget.findViewById(R.id.tvReturnHome);

        tvAddScore.setText("游戏积分+".concat(String.valueOf(incScore)));
        Glide.with(this).load(meAvatar).into(imvWiner);
        Glide.with(this).load(fromAvatar).into(imvLoser);
        tvWinerName.setText(meNick);
        tvLoserName.setText(fromNick);
        tvScore.setText(String.valueOf(ownerWinCount).concat(":").concat(String.valueOf(oppWinCount)));
        //由于result表示的输赢是对手的，所以这里显示要反过来
        if (result == 0) {//输
            imvGameStatus.setImageResource(R.drawable.ic_minigame_win_head);
            rlMain.setBackgroundResource(R.drawable.bg_minigame_win_pop);
        } else if (result == 1) {//赢
            imvGameStatus.setImageResource(R.drawable.ic_minigame_loser_head);
            rlMain.setBackgroundResource(R.drawable.bg_minigame_loser_pop);
        } else {//和
            imvGameStatus.setImageResource(R.drawable.ic_minigame_heju_head);
            rlMain.setBackgroundResource(R.drawable.bg_minigame_heju_pop);
        }
        rlMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        vTarget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removePopView(vTarget);
            }
        });

        final int finalPosition = position;
        tvAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removePopView(vTarget);
                if (finalPosition >= 0) {
                    fragment.getActionList().get(finalPosition).onClick();
                }
            }
        });
        tvReturnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removePopView(vTarget);
            }
        });
        showPopView(vTarget);

        fragment.showGames();
    }

    private void showPopView(final View view) {
        final ViewGroup decorView = (ViewGroup) getWindow().getDecorView();
        if (view.getParent() != null) {
            ((ViewGroup) view.getParent()).removeView(view);
        }
        if (getHandler() != null) {
            getHandler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    decorView.addView(view);
                }
            }, 300);
        }
    }

    private void removePopView(View view) {
        if (view.getParent() != null) {
            ((ViewGroup) view.getParent()).removeView(view);
        }
    }
}
