package com.netease.nim.uikit.session.module.input;


import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.netease.nim.uikit.R;
import com.netease.nim.uikit.session.actions.BaseAction;

import java.util.List;

public class ActionsGridviewAdapter extends BaseAdapter {

    private Context context;

    private List<BaseAction> baseActions;

    public ActionsGridviewAdapter(Context context, List<BaseAction> baseActions) {
        this.context = context;
        this.baseActions = baseActions;
    }

    @Override
    public int getCount() {
        return baseActions.size();
    }

    @Override
    public Object getItem(int position) {
        return baseActions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemlayout;
        if (convertView == null) {
            itemlayout = LayoutInflater.from(context).inflate(R.layout.nim_actions_item_layout, null);
        } else {
            itemlayout = convertView;
        }
        ImageView imvIcon = (ImageView) itemlayout.findViewById(R.id.imageView);
        TextView tvText = (TextView) itemlayout.findViewById(R.id.textView);

        TextView tvName = (TextView) itemlayout.findViewById(R.id.tvName);
//        TextView tvPlayers = (TextView) itemlayout.findViewById(R.id.tvPlayers);

        BaseAction viewHolder = baseActions.get(position);
        if (viewHolder.getIconResId() != 0) {
            imvIcon.setBackgroundResource(viewHolder.getIconResId());
            tvText.setText(context.getString(viewHolder.getTitleId()));
        } else if (!TextUtils.isEmpty(viewHolder.getIconResUrl())) {
            Glide.with(context).load(viewHolder.getIconResUrl()).into(imvIcon);
            tvText.setText(viewHolder.getTitle());
        }

        if (!TextUtils.isEmpty(viewHolder.getName())) {
            tvName.setText(viewHolder.getName());
        }
//        if (!TextUtils.isEmpty(viewHolder.getPlayers())) {
//            tvPlayers.setText(viewHolder.getPlayers() + "对在玩");
//        }

        return itemlayout;
    }
}

