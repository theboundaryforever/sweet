package com.netease.nim.uikit.session.module.input;

import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.juxiao.library_utils.DisplayUtils;
import com.netease.nim.uikit.R;
import com.netease.nim.uikit.session.actions.BaseAction;
import com.netease.nim.uikit.session.activity.P2PMessageActivity;
import com.tongdaxing.erban.libcommon.utils.LogUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 更多操作模块
 * Created by hzxuwen on 2015/6/17.
 */
public class ActionsPanel {

    private static final String TAG = ActionsPanel.class.getSimpleName();

    // 初始化更多布局adapter
    public static void init(View view, List<BaseAction> actions) {
        final ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        final ViewGroup indicator = (ViewGroup) view.findViewById(R.id.actions_page_indicator);

        //从第四个开始展示，前面的action不展示只用来从外部传递数据
        if (actions != null && actions.size() > P2PMessageActivity.CUSTOM_ACTION_COUNT) {
            actions = actions.subList(P2PMessageActivity.CUSTOM_ACTION_COUNT, actions.size());
        } else {
            actions = new ArrayList<>(0);
        }
        ActionsPagerAdapter adapter = new ActionsPagerAdapter(viewPager, actions);
        viewPager.setAdapter(adapter);
        initPageListener(indicator, adapter.getCount(), viewPager);
    }

    // 初始化更多布局PageListener
    private static void initPageListener(final ViewGroup indicator, final int count, final ViewPager viewPager) {
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                setIndicator(indicator, count, position);
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        setIndicator(indicator, count, 0);
    }

//    /**
//     * 设置页码
//     */
//    private static void setIndicator(ViewGroup indicator, int total, int current) {
//        if (total <= 0) {
//            indicator.removeAllViews();
//        } else {
//            indicator.removeAllViews();
//            for (int i = 0; i < total; i++) {
//                ImageView imgCur = new ImageView(indicator.getContext());
//                imgCur.setId(i);
//                // 判断当前页码来更新
//                if (i == current) {
//                    imgCur.setBackgroundResource(R.drawable.nim_moon_page_selected);
//                } else {
//                    imgCur.setBackgroundResource(R.drawable.nim_moon_page_unselected);
//                }
//
//                indicator.addView(imgCur);
//            }
//        }
//    }

    /**
     * 更新页面指示器的选中状态
     *
     * @param page
     * @param pageCount
     */
    private static void setIndicator(ViewGroup indicator, int pageCount, int page) {
        LogUtils.d(TAG, "setCurPage-page:" + page + " pageCount:" + pageCount);
        int hasCount = indicator.getChildCount();
        int forMax = Math.max(hasCount, pageCount);
        LogUtils.d(TAG, "setCurPage-hasCount:" + hasCount + " forMax:" + forMax);
        ImageView imgCur = null;
        for (int i = 0; i < forMax; i++) {
            if (pageCount <= hasCount) {
                if (i >= pageCount) {
                    indicator.getChildAt(i).setVisibility(View.GONE);
                    continue;
                } else {
                    imgCur = (ImageView) indicator.getChildAt(i);
                }
            } else {
                if (i < hasCount) {
                    imgCur = (ImageView) indicator.getChildAt(i);
                } else {
                    imgCur = new ImageView(indicator.getContext());
                    indicator.addView(imgCur);
                }
            }

            imgCur.setId(i);
            // 判断当前页码来更新
            imgCur.setSelected(i == page);

            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) imgCur.getLayoutParams();
            params.leftMargin = DisplayUtils.dip2px(indicator.getContext(), i == 0 ? 0 : 3);
            params.rightMargin = DisplayUtils.dip2px(indicator.getContext(), i == hasCount - 1 ? 0 : 3);
            imgCur.setLayoutParams(params);
            imgCur.setImageResource(i == page ? R.drawable.bg_emoji_indicator_selected : R.drawable.bg_emoji_indicator_unselected);
            imgCur.setVisibility(i == 0 && 1 == pageCount ? View.INVISIBLE : View.VISIBLE);
        }
    }
}
