package com.netease.nim.uikit.session.module;

import com.tongdaxing.erban.libcommon.coremanager.ICoreClient;

public interface IShareFansCoreClient extends ICoreClient {
    String onShareFansJoin = "onShareFansJoin";


    void onShareFansJoin(long roomId, long uid, int roomType);


}