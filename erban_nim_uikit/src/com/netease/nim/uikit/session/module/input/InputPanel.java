package com.netease.nim.uikit.session.module.input;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.SystemClock;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.growingio.android.sdk.collection.GrowingIO;
import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.R;
import com.netease.nim.uikit.ait.AitTextChangeListener;
import com.netease.nim.uikit.common.ui.dialog.EasyAlertDialogHelper;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.netease.nim.uikit.session.SessionCustomization;
import com.netease.nim.uikit.session.actions.BaseAction;
import com.netease.nim.uikit.session.emoji.EmoticonPickerView;
import com.netease.nim.uikit.session.emoji.IEmoticonSelectedListener;
import com.netease.nim.uikit.session.emoji.MoonUtil;
import com.netease.nim.uikit.session.games.GameListPannelView;
import com.netease.nim.uikit.session.module.Container;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.media.record.AudioRecorder;
import com.netease.nimlib.sdk.media.record.IAudioRecordCallback;
import com.netease.nimlib.sdk.media.record.RecordType;
import com.netease.nimlib.sdk.msg.MessageBuilder;
import com.netease.nimlib.sdk.msg.MsgService;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.CustomNotification;
import com.netease.nimlib.sdk.msg.model.CustomNotificationConfig;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.netease.nimlib.sdk.msg.model.NIMAntiSpamOption;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.coremanager.IUserInfoCore;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.utils.toast.ToastCompat;

import java.io.File;
import java.util.List;

/**
 * 底部文本编辑，语音等模块
 * Created by hzxuwen on 2015/6/16.
 */
public class InputPanel implements IAudioRecordCallback, AitTextChangeListener {

    private static final String TAG = "MsgSendLayout";

    public static final int SHOW_LAYOUT_DELAY = 200;

    protected Container container;
    protected View view;

    private OnAudioRecordStatusChangeListener onAudioRecordStatusChangeListener;

    protected Handler uiHandler;

    protected LinearLayout messageActivityBottomLayout;
    protected EditText messageEditText;// 文本消息编辑框
    protected Button audioRecordBtn; // 录音按钮
    protected View audioAnimLayout; // 录音动画布局
    protected FrameLayout textAudioSwitchLayout; // 切换文本，语音按钮布局
    protected View switchToTextButtonInInputBar;// 文本消息选择按钮
    protected View switchToAudioButtonInInputBar;// 语音消息选择按钮
    protected View moreFuntionButtonInInputBar;// 更多消息选择按钮
    protected View sendMessageButtonInInputBar;// 发送消息按钮
    protected View blflSendMessage;// 发送消息按钮
    protected View emojiButtonInInputBar;// 发送消息按钮
    protected View messageInputBar;

    private SessionCustomization customization;

    // 表情
    protected EmoticonPickerView emoticonPickerView;  // 贴图表情控件

    // 语音
    protected AudioRecorder audioMessageHelper;
    private Chronometer time;
    private TextView timerTip;
    private LinearLayout timerTipContainer;
    private boolean started = false;
    private boolean cancelled = false;
    private boolean touched = false; // 是否按着
    private boolean isKeyboardShowed = true; // 是否显示键盘

    // state
    private boolean actionPanelBottomLayoutHasSetup = false;
    protected GameListPannelView actionPanelBottomLayout; // 更多布局
    private boolean isTextAudioSwitchShow = true;

    // adapter
    private List<BaseAction> actions;

    // data
    private long typingTime = 0;

    private boolean isRobotSession;

    private TextWatcher aitTextWatcher;
    private TextWatcher editTextWatcher;

    //录音时显示的提示图片
    private ImageView imvAudioStatus;
    private IEmoticonSelectedListener iEmoticonSelectedListener;

    public void setIEmoticonSelectedListener(IEmoticonSelectedListener iEmoticonSelectedListener) {
        this.iEmoticonSelectedListener = iEmoticonSelectedListener;
    }

    public InputPanel(Container container, View view, List<BaseAction> actions, boolean isTextAudioSwitchShow) {
        this.container = container;
        this.view = view;
        this.actions = actions;
        this.isTextAudioSwitchShow = isTextAudioSwitchShow;
        init();
    }

    public InputPanel(Container container, View view, List<BaseAction> actions) {
        this(container, view, actions, true);
    }

    public void onPause() {
        // 停止录音
        if (audioMessageHelper != null) {
            onEndAudioRecord(true);
        }
    }

    public void setUiHandler(Handler uiHandler) {
        this.uiHandler = uiHandler;
    }

    /**
     * ************************* 键盘布局切换 *******************************
     */

    private View.OnClickListener clickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (v == switchToTextButtonInInputBar) {
                switchToTextLayout(true);// 显示文本发送的布局
            } else if (v.getId() == R.id.buttonSendMessage || v.getId() == R.id.blflSendMessage) {
                onTextMessageSendButtonPressed();
            } else if (v == switchToAudioButtonInInputBar) {
                if (NimUIKit.checkInputMsgSendable(view.getContext())) {
                    switchToAudioLayout();
                }
            } else if (v == moreFuntionButtonInInputBar) {
                toggleActionPanelLayout(true);
            } else if (v == emojiButtonInInputBar) {
                toggleEmojiLayout(true);
            }
        }
    };

    public void addAitTextWatcher(TextWatcher watcher) {
        aitTextWatcher = watcher;
    }

    private void init() {
        initViews();
        initInputBarListener();
        initTextEdit();
        initAudioRecordButton();
        restoreText(false);

        for (int i = 0; i < actions.size(); ++i) {
            actions.get(i).setIndex(i);
            actions.get(i).setContainer(container);
        }
        initActionPanelLayout();
    }

    public Container getContainer() {
        return container;
    }

    public void setCustomization(SessionCustomization customization) {
        this.customization = customization;
        if (customization != null) {
            emoticonPickerView.setWithSticker(customization.withSticker);
        }
    }

    public void reload(Container container, SessionCustomization customization) {
        this.container = container;
        if (actions != null) {
            for (int i = 0; i < actions.size(); ++i) {
                actions.get(i).setIndex(i);
                actions.get(i).setContainer(container);
            }
        }
        setCustomization(customization);
    }

    // 上滑取消录音判断
    private static boolean isCancelled(View view, MotionEvent event) {
        int[] location = new int[2];
        view.getLocationOnScreen(location);

        return event.getRawX() < location[0] || event.getRawX() > location[0] + view.getWidth()
                || event.getRawY() < location[1] - 40;

    }

    public boolean collapse(boolean immediately) {
        boolean respond = (emoticonPickerView != null && emoticonPickerView.getVisibility() == View.VISIBLE)
                || (actionPanelBottomLayout != null && actionPanelBottomLayout.getVisibility() == View.VISIBLE);

        hideAllInputLayout(immediately);

        return respond;
    }

    private void initInputBarListener() {
        switchToTextButtonInInputBar.setOnClickListener(clickListener);
        switchToAudioButtonInInputBar.setOnClickListener(clickListener);
        emojiButtonInInputBar.setOnClickListener(clickListener);
        sendMessageButtonInInputBar.setOnClickListener(clickListener);
        blflSendMessage.setOnClickListener(clickListener);
        moreFuntionButtonInInputBar.setOnClickListener(clickListener);
    }


    /**
     * 发送“正在输入”通知
     */
    private void sendTypingCommand() {
        if (container.account.equals(NimUIKit.getAccount())) {
            return;
        }

        if (container.sessionType == SessionTypeEnum.Team || container.sessionType == SessionTypeEnum.ChatRoom) {
            return;
        }

        if (System.currentTimeMillis() - typingTime > 5000L) {
            typingTime = System.currentTimeMillis();
            CustomNotification command = new CustomNotification();
            command.setSessionId(container.account);
            command.setSessionType(container.sessionType);
            CustomNotificationConfig config = new CustomNotificationConfig();
            config.enablePush = false;
            config.enableUnreadCount = false;
            command.setConfig(config);

            JSONObject json = new JSONObject();
            json.put("id", "1");
            command.setContent(json.toString());

            NIMClient.getService(MsgService.class).sendCustomNotification(command);
        }
    }

    private void initViews() {
        // input bar
        messageActivityBottomLayout = view.findViewById(R.id.messageActivityBottomLayout);
        messageInputBar = view.findViewById(R.id.textMessageLayout);
        switchToTextButtonInInputBar = view.findViewById(R.id.buttonTextMessage);
        switchToAudioButtonInInputBar = view.findViewById(R.id.buttonAudioMessage);
        moreFuntionButtonInInputBar = view.findViewById(R.id.buttonMoreFuntionInText);
        emojiButtonInInputBar = view.findViewById(R.id.emoji_button);
        sendMessageButtonInInputBar = view.findViewById(R.id.buttonSendMessage);
        blflSendMessage = view.findViewById(R.id.blflSendMessage);
        messageEditText = view.findViewById(R.id.editTextMessage);
        GrowingIO.getInstance().trackEditText(messageEditText);

        // 语音
        imvAudioStatus = view.findViewById(R.id.imvAudioStatus);
        audioRecordBtn = view.findViewById(R.id.audioRecord);
        audioAnimLayout = view.findViewById(R.id.layoutPlayAudio);
        time = view.findViewById(R.id.timer);
        timerTip = view.findViewById(R.id.timer_tip);
        timerTipContainer = view.findViewById(R.id.timer_tip_container);

        // 表情
        emoticonPickerView = view.findViewById(R.id.emoticon_picker_view);
        actionPanelBottomLayout = view.findViewById(R.id.actionsLayout);

        // 显示录音按钮
        //    switchToTextButtonInInputBar.setVisibility(View.GONE);
        //   switchToAudioButtonInInputBar.setVisibility(View.VISIBLE);

        // 文本录音按钮切换布局
        textAudioSwitchLayout = view.findViewById(R.id.switchLayout);
//        if (isTextAudioSwitchShow) {
//            textAudioSwitchLayout.setVisibility(View.VISIBLE);
//        } else {
//            textAudioSwitchLayout.setVisibility(View.GONE);
//        }
    }

    // 点击edittext，切换键盘和更多布局
    private void switchToTextLayout(boolean needShowInput) {
        hideEmojiLayout();
        hideActionPanelLayout();

        audioRecordBtn.setVisibility(View.GONE);
        messageEditText.setVisibility(View.VISIBLE);
        switchToTextButtonInInputBar.setVisibility(View.GONE);
        switchToAudioButtonInInputBar.setVisibility(View.VISIBLE);

        messageInputBar.setVisibility(View.VISIBLE);

        if (needShowInput) {
            uiHandler.postDelayed(showTextRunnable, SHOW_LAYOUT_DELAY);
        } else {
            hideInputMethod();
        }
    }

    // 发送按钮触发
    private void onTextMessageSendButtonPressed() {
        String text = messageEditText.getText().toString();
        if (!TextUtils.isEmpty(text) && sendTextMessage(text)) {
            restoreText(true);
        }
    }

    /**
     * 发送文本消息
     *
     * @param text
     * @return
     */
    public boolean sendTextMessage(String text) {
        if(!NimUIKit.checkInputMsgSendable(view.getContext())) {
            return false;
        }

        String sensitiveWordData = CoreManager.getCore(IUserInfoCore.class).getSensitiveWord();
        if (!TextUtils.isEmpty(sensitiveWordData) && !TextUtils.isEmpty(text)) {
            if (text.matches(sensitiveWordData)) {
                SingleToastUtil.showToast("发送失败 含有敏感词汇");
                return false;
            }
        }

        Json json = CoreManager.getCore(IUserInfoCore.class).getBannedMap();
        boolean all = json.boo(IUserInfoCore.BANNED_ALL + "");
        boolean room = json.boo(IUserInfoCore.BANNED_P2P + "");
        if (all || room) {
            SingleToastUtil.showToast("亲，由于您的发言违反了平台绿色公约，若有异议请联系客服");
            return false;
        }

        IMMessage textMessage = createTextMessage(text);

        // 构造反垃圾对象
        NIMAntiSpamOption antiSpamOption = textMessage.getNIMAntiSpamOption();
        if (antiSpamOption == null) {
            antiSpamOption = new NIMAntiSpamOption();
        }
        antiSpamOption.enable = NimUIKit.checkAntiOptionEnable();
        textMessage.setNIMAntiSpamOption(antiSpamOption);
        LogUtils.d(TAG, "onTextMessageSendButtonPressed text:" + text);
        return container.proxy.sendMessage(textMessage);
    }


    protected IMMessage createTextMessage(String text) {
        return MessageBuilder.createTextMessage(container.account, container.sessionType, text);
    }

    // 切换成音频，收起键盘，按钮切换成键盘
    private void switchToAudioLayout() {
        messageEditText.setVisibility(View.GONE);
        audioRecordBtn.setVisibility(View.VISIBLE);
        hideInputMethod();
        hideEmojiLayout();
        hideActionPanelLayout();

        switchToAudioButtonInInputBar.setVisibility(View.GONE);
        switchToTextButtonInInputBar.setVisibility(View.VISIBLE);
    }

    // 点击“+”号按钮，切换更多布局和键盘
    public boolean toggleActionPanelLayout(boolean isHideEmojiNow) {
        if (actionPanelBottomLayout == null || actionPanelBottomLayout.getVisibility() == View.GONE) {
            showActionPanelLayout(isHideEmojiNow);
            return true;
        } else {
            hideActionPanelLayout();
            return false;
        }
    }

    // 点击表情，切换到表情布局
    public boolean toggleEmojiLayout(boolean isHideActionNow) {
        if (emoticonPickerView == null || emoticonPickerView.getVisibility() == View.GONE) {
            showEmojiLayout(isHideActionNow);
            return true;
        } else {
            hideEmojiLayout();
            return false;
        }
    }

    // 隐藏表情布局
    public void hideEmojiLayout() {
        if (emoticonPickerView != null) {
            emoticonPickerView.setVisibility(View.GONE);
        }
        if(actionStatusListener!= null) {
            actionStatusListener.changeEmojiBtnStatus(false);
        }
    }

    // 隐藏更多布局
    public void hideActionPanelLayout() {
        if (actionPanelBottomLayout != null) {
            actionPanelBottomLayout.setVisibility(View.GONE);
        }
        if(actionStatusListener!= null) {
            actionStatusListener.changeGameBtnStatus(false);
        }
    }

    // 隐藏键盘布局
    public void hideInputMethod() {
        isKeyboardShowed = false;
        uiHandler.removeCallbacks(showTextRunnable);
        InputMethodManager imm = (InputMethodManager) container.activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(messageEditText.getWindowToken(), 0);
        messageEditText.clearFocus();
    }

    // 隐藏语音布局
    private void hideAudioLayout() {
        audioRecordBtn.setVisibility(View.GONE);
        messageEditText.setVisibility(View.VISIBLE);
        switchToTextButtonInInputBar.setVisibility(View.VISIBLE);
        switchToAudioButtonInInputBar.setVisibility(View.GONE);
    }

    // 显示表情布局
    private void showEmojiLayout(boolean isHideActionNow) {
        hideInputMethod();
        if (isHideActionNow) {
            hideActionPanelLayout();
        }
        hideAudioLayout();

        messageEditText.requestFocus();
        emoticonPickerView.setVisibility(View.VISIBLE);
        if (null != iEmoticonSelectedListener) {
            emoticonPickerView.show(iEmoticonSelectedListener);
        }

        container.proxy.onInputPanelExpand();

        if(actionStatusListener!= null) {
            actionStatusListener.changeEmojiBtnStatus(true);
        }
    }

    // 显示键盘布局
    private void showInputMethod(EditText editTextMessage) {
        editTextMessage.requestFocus();
        //如果已经显示,则继续操作时不需要把光标定位到最后
        if (!isKeyboardShowed) {
            editTextMessage.setSelection(editTextMessage.getText().length());
            isKeyboardShowed = true;
        }

        InputMethodManager imm = (InputMethodManager) container.activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editTextMessage, 0);

        container.proxy.onInputPanelExpand();
    }

    // 显示更多布局
    public void showActionPanelLayout(boolean isHideEmojiNow) {
        if (isHideEmojiNow) {
            hideEmojiLayout();
        }
        hideInputMethod();
        actionPanelBottomLayout.setVisibility(View.VISIBLE);
        container.proxy.onInputPanelExpand();
        if(actionStatusListener!= null) {
            actionStatusListener.changeGameBtnStatus(true);
        }
    }

    // 初始化具体more layout中的项目
    public void initActionPanelLayout() {
        if (actionPanelBottomLayoutHasSetup) {
            return;
        }

        ActionsPanel.init(actionPanelBottomLayout, actions);
        actionPanelBottomLayoutHasSetup = true;
    }

    private Runnable showTextRunnable = new Runnable() {
        @Override
        public void run() {
            showInputMethod(messageEditText);
        }
    };

    private void restoreText(boolean clearText) {
        if (clearText) {
            messageEditText.setText("");
        }

        checkSendButtonEnable(messageEditText);
    }

    /**
     * 显示发送或更多
     *
     * @param editText
     */
    private void checkSendButtonEnable(EditText editText) {
        if (isRobotSession) {
            return;
        }
        String textMessage = editText.getText().toString();
        if (!TextUtils.isEmpty(StringUtil.removeBlanks(textMessage)) && editText.hasFocus()) {
//            moreFuntionButtonInInputBar.setVisibility(View.GONE);
//            sendMessageButtonInInputBar.setVisibility(View.VISIBLE);
            sendMessageButtonInInputBar.setEnabled(true);
        } else {
//            sendMessageButtonInInputBar.setVisibility(View.GONE);
//            moreFuntionButtonInInputBar.setVisibility(View.VISIBLE);
            sendMessageButtonInInputBar.setEnabled(false);
        }
    }

    /**
     * *************** IEmojiSelectedListener ***************
     */
    public boolean onEmojiSelected(String key, boolean isCustomEmoji) {
        LogUtils.d(TAG, "onEmojiSelected key:" + key + " isCustomEmoji:" + isCustomEmoji);
        boolean result = false;
        Editable mEditable = messageEditText.getText();
        if (key.equals("/DEL")) {
            messageEditText.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_DEL));
        } else {
            if (isCustomEmoji) {
                //直接将表情文本发出去
                result = sendTextMessage(key);
            } else {
                int start = messageEditText.getSelectionStart();
                int end = messageEditText.getSelectionEnd();
                start = (start < 0 ? 0 : start);
                mEditable.replace(start, end, key);
            }
        }
        return result;
    }

    private Runnable hideAllInputLayoutRunnable;

    public boolean onStickerSelected(String category, String item) {
        LogUtils.d("InputPanel", "onStickerSelected, category =" + category + ", sticker =" + item);
        boolean result = false;
        if (customization != null) {
            MsgAttachment attachment = customization.createStickerAttachment(category, item);
            IMMessage stickerMessage = MessageBuilder.createCustomMessage(container.account, container.sessionType, "贴图消息", attachment);
            result = container.proxy.sendMessage(stickerMessage);
        }
        return result;
    }

    @Override
    public void onTextAdd(String content, int start, int length) {
        if (messageEditText.getVisibility() != View.VISIBLE) {
            switchToTextLayout(true);
        } else {
            uiHandler.postDelayed(showTextRunnable, SHOW_LAYOUT_DELAY);
        }
        messageEditText.getEditableText().insert(start, content);
    }

    @Override
    public void onTextDelete(int start, int length) {
        if (messageEditText.getVisibility() != View.VISIBLE) {
            switchToTextLayout(true);
        } else {
            uiHandler.postDelayed(showTextRunnable, SHOW_LAYOUT_DELAY);
        }
        int end = start + length - 1;
        messageEditText.getEditableText().replace(start, end, "");
    }

    public int getEditSelectionStart() {
        return messageEditText.getSelectionStart();
    }


    /**
     * 隐藏所有输入布局
     */
    private void hideAllInputLayout(boolean immediately) {
        if (hideAllInputLayoutRunnable == null) {
            hideAllInputLayoutRunnable = new Runnable() {

                @Override
                public void run() {
                    hideInputMethod();
                    hideActionPanelLayout();
                    hideEmojiLayout();
                }
            };
        }
        long delay = immediately ? 0 : ViewConfiguration.getDoubleTapTimeout();
        uiHandler.postDelayed(hideAllInputLayoutRunnable, delay);
    }

    /**
     * ****************************** 语音 ***********************************
     */
    private void initAudioRecordButton() {
        audioRecordBtn.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    touched = true;
                    initAudioRecord();
                    onStartAudioRecord();
                } else if (event.getAction() == MotionEvent.ACTION_CANCEL
                        || event.getAction() == MotionEvent.ACTION_UP) {
                    touched = false;
                    onEndAudioRecord(isCancelled(v, event));
                } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
                    touched = true;
                    cancelAudioRecord(isCancelled(v, event));
                }

                return false;
            }
        });
    }

    public void initAudioRecordButton(View view) {
        view.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    touched = true;
                    initAudioRecord();
                    onStartAudioRecord();
                } else if (event.getAction() == MotionEvent.ACTION_CANCEL
                        || event.getAction() == MotionEvent.ACTION_UP) {
                    touched = false;
                    onEndAudioRecord(isCancelled(v, event));
                } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
                    touched = true;
                    cancelAudioRecord(isCancelled(v, event));
                }

                return false;
            }
        });
    }

    private void initTextEdit() {
        messageEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        messageEditText.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    switchToTextLayout(true);
                }
                return false;
            }
        });

        messageEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                messageEditText.setHint("");
                //checkSendButtonEnable(messageEditText);
            }
        });

        if (null == editTextWatcher) {
            editTextWatcher = new TextWatcher() {
                private int start;
                private int count;

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    this.start = start;
                    this.count = count;
                    if (aitTextWatcher != null) {
                        aitTextWatcher.onTextChanged(s, start, before, count);
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    if (aitTextWatcher != null) {
                        aitTextWatcher.beforeTextChanged(s, start, count, after);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                    checkSendButtonEnable(messageEditText);
                    //转换表情字符
                    MoonUtil.replaceEmoticons(container.activity, s, start, count);

                    int editEnd = messageEditText.getSelectionEnd();
                    messageEditText.removeTextChangedListener(editTextWatcher);
                    while (StringUtil.counterChars(s.toString()) > 5000 && editEnd > 0) {
                        s.delete(editEnd - 1, editEnd);
                        editEnd--;
                    }
                    messageEditText.setSelection(editEnd);
                    messageEditText.addTextChangedListener(editTextWatcher);

                    if (aitTextWatcher != null) {
                        aitTextWatcher.afterTextChanged(s);
                    }

                    sendTypingCommand();
                }
            };
        }

        messageEditText.addTextChangedListener(editTextWatcher);
    }

    /**
     * 初始化AudioRecord
     */
    private void initAudioRecord() {
        if (audioMessageHelper == null) {
            audioMessageHelper = new AudioRecorder(container.activity, RecordType.AAC, AudioRecorder.DEFAULT_MAX_AUDIO_RECORD_TIME_SECOND, this);
        }
    }

    public void onDestroy() {
        // release
        if (audioMessageHelper != null) {
            audioMessageHelper.destroyAudioRecorder();
        }
        if (null != actionStatusListener) {
            actionStatusListener = null;
        }
        if (null != messageEditText) {
            if (null != editTextWatcher) {
                messageEditText.removeTextChangedListener(editTextWatcher);
            }
            messageEditText.setOnTouchListener(null);
            messageEditText.setOnFocusChangeListener(null);
        }
        if (null != audioRecordBtn) {
            audioRecordBtn.setOnTouchListener(null);
        }
        if (null != iEmoticonSelectedListener) {
            iEmoticonSelectedListener = null;
        }
        if (null != onAudioRecordStatusChangeListener) {
            onAudioRecordStatusChangeListener = null;
        }
        if (null != uiHandler) {
            if (null != showTextRunnable) {
                uiHandler.removeCallbacks(showTextRunnable);
            }
            uiHandler.removeCallbacksAndMessages(null);
            uiHandler = null;
        }
        if (null != time) {
            time.stop();
        }
    }

    /**
     * 开始语音录制
     */
    private void onStartAudioRecord() {
        container.activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        audioMessageHelper.startRecord();
        cancelled = false;
        if (null != onAudioRecordStatusChangeListener) {
            onAudioRecordStatusChangeListener.onStartAudioRecord();
        }
    }

    /**
     * 结束语音录制
     *
     * @param cancel
     */
    private void onEndAudioRecord(boolean cancel) {
        started = false;
        container.activity.getWindow().setFlags(0, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        audioMessageHelper.completeRecord(cancel);
        audioRecordBtn.setText(R.string.record_audio);
        audioRecordBtn.setBackgroundResource(R.drawable.nim_message_input_edittext_box);
        stopAudioRecordAnim();
        if (null != onAudioRecordStatusChangeListener) {
            onAudioRecordStatusChangeListener.onEndAudioRecord(cancel);
        }
    }

    /**
     * 正在进行语音录制和取消语音录制，界面展示
     *
     * @param cancel
     */
    private void updateTimerTip(boolean cancel) {
        if (cancel) {
            imvAudioStatus.setImageResource(R.drawable.ic_p2p_message_audio_recall);
            timerTip.setText(R.string.recording_cancel_tip);
            timerTip.setBackgroundResource(R.drawable.nim_cancel_record_red_bg);
        } else {
            imvAudioStatus.setImageResource(R.drawable.ic_p2p_message_audio_record);
            timerTip.setText(R.string.recording_cancel);
            timerTip.setBackgroundResource(0);
        }
    }

    /**
     * 开始语音录制动画
     */
    private void playAudioRecordAnim() {
        audioAnimLayout.setVisibility(View.VISIBLE);
        time.setBase(SystemClock.elapsedRealtime());
        time.start();
    }

    /**
     * 结束语音录制动画
     */
    private void stopAudioRecordAnim() {
        audioAnimLayout.setVisibility(View.GONE);
        time.stop();
        time.setBase(SystemClock.elapsedRealtime());
    }

    // 录音状态回调
    @Override
    public void onRecordReady() {

    }

    @Override
    public void onRecordStart(File audioFile, RecordType recordType) {
        started = true;
        if (!touched) {
            return;
        }

        audioRecordBtn.setText(R.string.record_audio_end);
        audioRecordBtn.setBackgroundResource(R.drawable.nim_message_input_edittext_box_pressed);

        updateTimerTip(false); // 初始化语音动画状态
        playAudioRecordAnim();
    }

    @Override
    public void onRecordSuccess(File audioFile, long audioLength, RecordType recordType) {
        IMMessage audioMessage = MessageBuilder.createAudioMessage(container.account, container.sessionType, audioFile, audioLength);
        container.proxy.sendMessage(audioMessage);
    }

    @Override
    public void onRecordFail() {
        if (started) {
            ToastCompat.makeText(container.activity, R.string.recording_error, ToastCompat.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRecordCancel() {

    }

    @Override
    public void onRecordReachedMaxTime(final int maxTime) {
        stopAudioRecordAnim();
        EasyAlertDialogHelper.createOkCancelDiolag(container.activity, "", container.activity.getString(R.string.recording_max_time), false, new EasyAlertDialogHelper.OnDialogActionListener() {
            @Override
            public void doCancelAction() {
            }

            @Override
            public void doOkAction() {
                audioMessageHelper.handleEndRecord(true, maxTime);
            }
        }).show();
    }

    public boolean isRecording() {
        return audioMessageHelper != null && audioMessageHelper.isRecording();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        int index = (requestCode << 16) >> 24;
        if (index != 0) {
            index--;
            if (index < 0 | index >= actions.size()) {
                LogUtil.d(TAG, "request code out of actions' range");
                return;
            }
            BaseAction action = actions.get(index);
            if (action != null) {
                action.onActivityResult(requestCode & 0xff, resultCode, data);
            }
        }
    }

    public void switchRobotMode(boolean isRobot) {
        isRobotSession = isRobot;
        if (isRobot) {
            textAudioSwitchLayout.setVisibility(View.GONE);
            emojiButtonInInputBar.setVisibility(View.GONE);
            sendMessageButtonInInputBar.setVisibility(View.VISIBLE);
            blflSendMessage.setVisibility(View.VISIBLE);
            moreFuntionButtonInInputBar.setVisibility(View.GONE);
        } else {
            textAudioSwitchLayout.setVisibility(View.VISIBLE);
            emojiButtonInInputBar.setVisibility(View.VISIBLE);
            sendMessageButtonInInputBar.setVisibility(View.GONE);
            blflSendMessage.setVisibility(View.GONE);
            moreFuntionButtonInInputBar.setVisibility(View.VISIBLE);
        }
    }

    /**
     * @author liaoxy
     * @Description:一些控件的状态显示监听，比如emoji隐藏或显示状态
     * @date  15:00
     */
    private ActionStatusListener actionStatusListener;

    public void setActionStatusListener(ActionStatusListener actionStatusListener) {
        this.actionStatusListener = actionStatusListener;
    }

    public interface ActionStatusListener {
        void changeEmojiBtnStatus(boolean isShow);

        void changeGameBtnStatus(boolean isShow);
    }

    //============================ 增加外部规则检测消息是否可以发送===================

    public interface OnInputMsgSendableCheckListener{
        boolean checkInputMsgSendable(Context context);
    }

    /**
     * 实时依据外部条件来决策是否走网易云盾反垃圾规则
     */
    public interface OnPrivateMsgAntiSpamOptionChangedListener{
        boolean checkAntiOptionEnable();
    }

    /**
     * 取消语音录制
     *
     * @param cancel
     */
    private void cancelAudioRecord(boolean cancel) {
        // reject
        if (!started) {
            return;
        }
        // no change
        if (cancelled == cancel) {
            return;
        }

        cancelled = cancel;
        updateTimerTip(cancel);
        if (null != onAudioRecordStatusChangeListener) {
            onAudioRecordStatusChangeListener.onCancelAudioRecord(cancel);
        }
    }

    public void setOnAudioRecordStatusChangeListener(OnAudioRecordStatusChangeListener onAudioRecordStatusChangeListener) {
        this.onAudioRecordStatusChangeListener = onAudioRecordStatusChangeListener;
    }

    public interface OnAudioRecordStatusChangeListener {
        void onStartAudioRecord();

        void onEndAudioRecord(boolean cancel);

        void onCancelAudioRecord(boolean cancel);
    }
}
