package com.netease.nim.uikit.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.libcommon.utils.LogUtils;

/**
 * 图片加载处理
 * Created by chenran on 2017/11/9.
 */
public class ImageLoadUtils {

    private static final String TAG = ImageLoadUtils.class.getSimpleName();

    /**
     * @param context
     * @param url
     * @param imageView
     */
    public static void loadEmoji(final Context context, final String url, final ImageView imageView,
                                 final int defaultResId, final boolean playGifAnim, final int serEmojiSize) {
        if (StringUtil.isEmpty(url)) {
            return;
        }
        imageView.setTag(url);
        GlideApp.with(context)
                .load(url)
                .skipMemoryCache(false)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transforms(new CenterCrop())
                .placeholder(defaultResId)
                .error(defaultResId)
                .into(new SimpleTarget<Drawable>() {

                    @Override
                    public void onLoadStarted(@Nullable Drawable placeholder) {
                        LogUtils.d(TAG, "loadEmoji-->onLoadStarted url:" + url + " placeholder is null : " + (null == placeholder));
                        super.onLoadStarted(placeholder);
                        if (imageView.getTag() instanceof String && url.equals(imageView.getTag())) {
//                            ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
//                            if (layoutParams instanceof LinearLayout.LayoutParams) {
//                                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) imageView.getLayoutParams();
//                                params.width = serEmojiSize;
//                                params.height = serEmojiSize;
//                                imageView.setLayoutParams(params);
//                                Rect rect = new Rect(0, 0, serEmojiSize, serEmojiSize);
//                                placeholder.setBounds(rect);
//                            }
                            imageView.setImageDrawable(placeholder);
                        }
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable placeholder) {
                        LogUtils.d(TAG, "loadEmoji-->onLoadFailed url:" + url + " placeholder is null : " + (null == placeholder));
                        super.onLoadFailed(placeholder);
                        if (imageView.getTag() instanceof String && url.equals(imageView.getTag())) {
//                            ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
//                            if (layoutParams instanceof LinearLayout.LayoutParams) {
//                                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) imageView.getLayoutParams();
//                                params.width = serEmojiSize;
//                                params.height = serEmojiSize;
//                                imageView.setLayoutParams(params);
//                                Rect rect = new Rect(0, 0, serEmojiSize, serEmojiSize);
//                                placeholder.setBounds(rect);
//                            }
                            imageView.setImageDrawable(placeholder);
                        }
                    }

                    @Override
                    public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                        LogUtils.d(TAG, "loadEmoji-->onResourceReady url:" + url);
                        if (imageView.getTag() instanceof String && url.equals(imageView.getTag())) {
//                            Rect rect = new Rect(0, 0, serEmojiSize, serEmojiSize);
//                            if (resource.getIntrinsicWidth() > serEmojiSize) {
//                                float downScale = (float) resource.getIntrinsicWidth() / serEmojiSize;
//                                float rectWidth = (float) resource.getIntrinsicWidth() / (float) downScale;
//                                float rectHeight = (float) resource.getIntrinsicHeight() / (float) downScale;
//                                rect = new Rect(0, 0, Math.round(rectWidth), Math.round(rectHeight));
//                            }
//                            resource.setBounds(rect);
//                            ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
//                            layoutParams.width = serEmojiSize;
//                            layoutParams.height = serEmojiSize;
//                            imageView.setLayoutParams(layoutParams);
                            Drawable drawable = imageView.getDrawable();
                            if (drawable instanceof GifDrawable) {
                                GifDrawable gifDrawable = (GifDrawable) drawable;
                                if (gifDrawable.isRunning()) {
                                    LogUtils.d(TAG, "loadEmoji-->onResourceReady 拦截多次加载播放gif");
                                    return;
                                }
                            }
                            imageView.setImageDrawable(resource);
                            if (resource instanceof GifDrawable && playGifAnim) {
                                GifDrawable gifDrawable = (GifDrawable) resource;
                                gifDrawable.setLoopCount(GifDrawable.LOOP_INTRINSIC);
                                if (!gifDrawable.isRunning()) {
                                    gifDrawable.start();
                                }
                            }
                        }
                    }
                });

    }

    /**
     * 首页banner图统一加载方式
     *
     * @param context
     * @param url
     * @param imageView
     * @param roundCorner in px
     */
    public static void loadBannerRoundBg(Context context, String url, ImageView imageView,
                                         int roundCorner, int defaultResId) {
        if (StringUtil.isEmpty(url)) {
            return;
        }

        GlideApp.with(context)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transforms(new CenterCrop(),
                        new RoundedCorners(roundCorner))
                .placeholder(defaultResId)
                .error(defaultResId)
                .into(imageView);
    }

    /**
     * ImageLoader异步加载
     */
    public static void doLoadCircleImage(Context context, final String avatar, final int defaultResId, final int thumbSize, ImageView imageView) {
        LogUtils.d(TAG, "doLoadCircleImage avatar:" + avatar + " thumbSize:" + thumbSize);
        if (TextUtils.isEmpty(avatar)) {
            return;
        }

        GlideApp.with(context.getApplicationContext())
                .load(ImageLoadUtils.toThumbnailUrl(thumbSize, thumbSize, avatar))
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transforms(new CircleCrop())
                .placeholder(defaultResId)
                .error(defaultResId)
                .into(imageView);
    }


    public static String toThumbnailUrl(int width, int height, String imgUrl) {
        if (!TextUtils.isEmpty(imgUrl) && imgUrl.endsWith("?imageslim")) {
            imgUrl = imgUrl.concat("|imageView2/1/w/").concat(String.valueOf(width)).concat("/h/").concat(String.valueOf(height));
        }
        return imgUrl;
    }
}
