package com.tongdaxing.xchat_core.im.custom.bean;

import java.io.Serializable;

public class QmLikeBean implements Serializable {

    private String nick;//": "刚哈😮😮😮😮😮",
    private long uid;//": 93000746,
    private int gender;//": 1,
    private long admireDate;//": 1562120527289,
    private String targetNick;//": "剑执",
    private int targetGender;//": 1,
    private String avatar;//": "https://img.pinjin88.com/FsD6pwIXCyoOXEw_gDhOKmEuO1On?imageslim",
    private long targetUid;//": 93000965,
    private String targetAvatar;//": "https://img.pinjin88.com/touxiangnan@3x.png",
    private String content;//": "通过速配发现你"

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public long getAdmireDate() {
        return admireDate;
    }

    public void setAdmireDate(long admireDate) {
        this.admireDate = admireDate;
    }

    public String getTargetNick() {
        return targetNick;
    }

    public void setTargetNick(String targetNick) {
        this.targetNick = targetNick;
    }

    public int getTargetGender() {
        return targetGender;
    }

    public void setTargetGender(int targetGender) {
        this.targetGender = targetGender;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public long getTargetUid() {
        return targetUid;
    }

    public void setTargetUid(long targetUid) {
        this.targetUid = targetUid;
    }

    public String getTargetAvatar() {
        return targetAvatar;
    }

    public void setTargetAvatar(String targetAvatar) {
        this.targetAvatar = targetAvatar;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
