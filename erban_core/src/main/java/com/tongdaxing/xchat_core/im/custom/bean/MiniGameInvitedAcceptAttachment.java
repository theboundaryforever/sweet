package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;
import com.tongdaxing.xchat_core.bean.MiniGameInvitedAcceptInfo;

/**
 * 小游戏邀请同意通知
 */

public class MiniGameInvitedAcceptAttachment extends CustomAttachment {
    private MiniGameInvitedAcceptInfo dataInfo;

    public MiniGameInvitedAcceptInfo getDataInfo() {
        return dataInfo;
    }

    public void setDataInfo(MiniGameInvitedAcceptInfo dataInfo) {
        this.dataInfo = dataInfo;
    }

    public MiniGameInvitedAcceptAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected void parseData(JSONObject data) {
        super.parseData(data);
        dataInfo = new MiniGameInvitedAcceptInfo();
        dataInfo.setFromAvatar(data.getString("fromAvatar"));
        dataInfo.setFromNick(data.getString("fromNick"));
        dataInfo.setFromUid(data.getLong("fromUid"));
        dataInfo.setRoomid(data.getLong("roomid"));
        dataInfo.setAcceptInvt(data.getInteger("acceptInvt"));
        dataInfo.setGameUrl(data.getString("gameUrl"));
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        object.put("fromAvatar", dataInfo.getFromAvatar());
        object.put("fromNick", dataInfo.getFromNick());
        object.put("fromUid", dataInfo.getFromUid());
        object.put("roomid", dataInfo.getRoomid());
        object.put("gameUrl", dataInfo.getGameUrl());
        return object;
    }
}
