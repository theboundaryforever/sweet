package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.JsonSyntaxException;
import com.tongdaxing.erban.libcommon.utils.json.JsonParser;
import com.tongdaxing.xchat_core.bean.RoomRedPacketFinish;

//红包领取完成附件
public class RoomRedPacketFinishAttachment extends CustomAttachment {
    private RoomRedPacketFinish dataInfo;

    public RoomRedPacketFinish getDataInfo() {
        return dataInfo;
    }

    public void setDataInfo(RoomRedPacketFinish dataInfo) {
        this.dataInfo = dataInfo;
    }

    public RoomRedPacketFinishAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected void parseData(JSONObject data) {
        super.parseData(data);
        dataInfo = new RoomRedPacketFinish();
        try {
            String json = data.getString("params");
            dataInfo = JsonParser.parseJsonToNormalObject(json, RoomRedPacketFinish.class);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        try {
            object = JSONObject.parseObject(JsonParser.toJson(dataInfo));
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return object;
    }
}
