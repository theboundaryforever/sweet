package com.tongdaxing.xchat_core.im.custom.bean;

import com.tongdaxing.xchat_core.room.bean.RoomInfo;

import java.io.Serializable;

public class CallUpBean implements Serializable {

    private String nick;//": "刚哈😮😮😮😮😮",
    private String avatar;//": "https:\/\/img.pinjin88.com\/FsD6pwIXCyoOXEw_gDhOKmEuO1On?imageslim",
    private String message;//": "鱼护",
    private long roomId;//": 104378924
    private long roomUid;//": 104378924


    /**
     * 房间类型
     */
    private int type = RoomInfo.ROOMTYPE_HOME_PARTY;

    private String title;
    //贵族勋章
    private String vipMedal;
    //性别
    private int gender;
    //年龄
    private int age;

    public String getVipMedal() {
        return vipMedal;
    }

    public void setVipMedal(String vipMedal) {
        this.vipMedal = vipMedal;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public long getRoomUid() {
        return roomUid;
    }

    public void setRoomUid(long roomUid) {
        this.roomUid = roomUid;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
