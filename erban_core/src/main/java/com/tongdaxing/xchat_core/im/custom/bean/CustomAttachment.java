package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.netease.nim.uikit.session.fragment.BaseMsgAttachment;
import com.tongdaxing.erban.libcommon.utils.json.Json;

/**
 * 先定义一个自定义消息附件的基类，负责解析你的自定义消息的公用字段，比如类型等等。
 *
 * @author zhouxiangfeng
 * @date 2017/6/8
 */
public class CustomAttachment extends BaseMsgAttachment {
    /**
     * 自定义消息附件的类型，根据该字段区分不同的自定义消息
     */
    protected int first;
    protected int second;
    protected JSONObject data;
    protected Json json;

    public Json getJson() {
        return json;
    }

    public void setJson(Json json) {
        this.json = json;
    }

    //竞拍房类型--竞拍相关，产品已明确废弃
    public static final int CUSTOM_MSG_HEADER_TYPE_AUCTION = 1;

    /**
     * 房间提示类消息：first  2
     * second
     * 21 房间分享成功提示消息
     * 22 房间关注房主提示消息
     * 23 房间的提醒关注（暂定房间内停留5分钟）
     */
    public static final int CUSTOM_MSG_HEADER_TYPE_ROOM_TIP = 2;
    public static final int CUSTOM_MSG_SUB_TYPE_ROOM_TIP_SHARE_ROOM = 21;
    public static final int CUSTOM_MSG_SUB_TYPE_ROOM_TIP_ATTENTION_ROOM_OWNER = 22;

    public static final int CUSTOM_MSG_HEADER_TYPE_GIFT = 3;
    public static final int CUSTOM_MSG_SUB_TYPE_SEND_GIFT = 31;
    public static final int CUSTOM_MSG_SUB_TYPE_SEND_GIFT_COMBO_SCREEN = 1;

    public static final int CUSTOM_MSG_HEADER_TYPE_ACCOUNT = 5;

    //通知关注的人中有人正在直播
    public static final int CUSTOM_MSG_HEADER_TYPE_OPEN_ROOM_NOTI = 6;

    public static final int CUSTOM_MSG_HEADER_TYPE_QUEUE = 8;
    public static final int CUSTOM_MSG_HEADER_TYPE_QUEUE_INVITE = 81;
    public static final int CUSTOM_MSG_HEADER_TYPE_QUEUE_KICK = 82;
    public static final int CUSTOM_MSG_HEADER_TYPE_QUEUE_CLOSE_MIC = 83;
    public static final int CUSTOM_MSG_HEADER_TYPE_QUEUE_OPEN_MIC = 84;
    public static final int CUSTOM_MSG_HEADER_TYPE_QUEUE_LOCK_MIC = 85;
    public static final int CUSTOM_MSG_HEADER_TYPE_QUEUE_UNLOCK_MIC = 86;
    public static final int CUSTOM_MSG_HEADER_TYPE_QUEUE_LOCK_MIC_ALL = 157;
    public static final int CUSTOM_MSG_HEADER_TYPE_QUEUE_UNLOCK_MIC_ALL = 158;
    public static final int CUSTOM_MSG_HEADER_TYPE_KICK_ROOM = 141;
    public static final int CUSTOM_MSG_HEADER_TYPE_ADD_BLACK_LIST = 151;
    public static final int CUSTOM_MSG_HEADER_TYPE_REMOVE_BLACK_LIST = 152;
    public static final int CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_OPEN = 153;
    public static final int CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_CLOSE = 154;
    public static final int CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_OPEN = 155;
    public static final int CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_CLOSE = 156;

    public static final int CUSTOM_MSG_HEADER_TYPE_FACE = 9;
    public static final int CUSTOM_MSG_SUB_TYPE_FACE_SEND = 91;

    public static final int CUSTOM_MSG_HEADER_TYPE_NOTICE = 10;

    public static final int CUSTOM_MSG_HEADER_TYPE_PACKET = 11;
    public static final int CUSTOM_MSG_SUB_TYPE_PACKET_FIRST = 111;

    /**
     * 全麦礼物礼物消息first
     */
    public static final int CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT = 12;
    public static final int CUSTOM_MSG_HEADER_TYPE_SUPER_GIFT = 14;
    public static final int CUSTOM_MSG_SUB_TYPE_SEND_MULTI_GIFT = 121;
    public static final int CUSTOM_MSG_SUB_TYPE_SEND_MULTI_GIFT_COMBO_SCREEN = 1;
    public static final int CUSTOM_MSG_SUB_PUBLIC_CHAT_ROOM = 15;
    public static final int CUSTOM_MSG_LOTTERY_BOX = 16;
    public static final int CUSTOM_MSG_MIC_IN_LIST = 17;
    public static final int CUSTOM_MSG_SHARE_FANS = 18;

    //转盘抽奖
    public static final int CUSTOM_MSG_HEADER_TYPE_LOTTERY = 13;
    public static final int CUSTOM_MSG_SUB_TYPE_NOTI_LOTTERY = 131;

    //PK 消息类型
    public static final int CUSTOM_MSG_HEADER_TYPE_PK = 19;//pk first
    public static final int CUSTOM_MSG_HEADER_TYPE_PK_START = 23;//pk开始
    public static final int CUSTOM_MSG_HEADER_TYPE_PK_VOTE = 26;//pk 投票
    public static final int CUSTOM_MSG_HEADER_TYPE_PK_END = 24;//pk结束 second
    public static final int CUSTOM_MSG_HEADER_TYPE_PK_CANCEL = 25;//pk取消 second
    public static final int CUSTOM_MSG_HEADER_TYPE_PK_START_NEW = 27;//新版多人pk开始
    public static final int CUSTOM_MSG_HEADER_TYPE_PK_END_NEW = 28;//新版pk结束 second
    public static final int CUSTOM_MSG_HEADER_TYPE_PK_CANCEL_NEW = 29;//新版pk取消 second
    public static final int CUSTOM_MSG_HEADER_TYPE_PK_VOTE_NEW = 30;//pk 投票

    public static final int CUSTOM_NOTI_HEADER_KTV = 20;//ktv

    public static final int CUSTOM_NOTI_HEADER_KTV_TIP_SELECT = 201;
    public static final int CUSTOM_NOTI_HEADER_KTV_TIP_TOP = 202;
    public static final int CUSTOM_NOTI_HEADER_KTV_TIP_DELE = 203;
    public static final int CUSTOM_NOTI_HEADER_KTV_TIP_QIE = 204;

    public static final int CUSTOM_MSG_ROOM_CHARM = 21;//魅力值
    public static final int CUSTOM_MSG_SECOND_ROOM_CHARM_UPDATE = 1;//魅力值清更新
    public static final int CUSTOM_MSG_SECOND_ROOM_CHARM_CLEAR = 211;//魅力值清空

    public static final int CUSTOM_MSG_ROOM_PAIR = 22;//相亲
    public static final int CUSTOM_MSG_ROOM_PAIR_START = 1;
    public static final int CUSTOM_MSG_ROOM_PAIR_END = 2;
    public static final int CUSTOM_MSG_ROOM_PAIR_CHANGE = 3;

    //分享房间
    public static final int CUSTOM_MSG_ROOM_SHARE = 27;
    //关注房主
    public static final int CUSTOM_MSG_ROOM_ATTENTION = 28;

    //房间富文本消息
    public static final int CUSTOM_MSG_ROOM_RICH_TXT = 29;
    public static final int CUSTOM_MSG_ROOM_RICH_TXT_LOVERS = 129;

    //房间内规则
    public static final int CUSTOM_MSG_TYPE_RULE_FIRST = 4;

    //情侣上麦
    public static final int CUSTOM_MSG_ROOM_LOVER_UP_MIC = 30;
    public static final int CUSTOM_MSG_ROOM_PAIR_SUC_ROOM_NOTICE = 31;

    //引爆礼物进度通知 first
    public static final int CUSTOM_MSG_ROOM_DETONATING_GIFT = 33;
    //引爆礼物进度通知 sencond
    public static final int CUSTOM_MSG_ROOM_DETONATING_GIFT_PROGRESS = 1;
    //引爆礼物全服通知 sencond
    public static final int CUSTOM_MSG_ROOM_DETONATING_GIFT_NOTIFY = 2;

    //房间年度排行榜
    public static final int CUSTOM_MSG_ANNUAL_ROOM_LIST = 34;

    //实名认证审核状态通知
    public static final int CUSTOM_MSG_AUTH_STATUS_NOTIFY = 35;

    //小游戏通知 first
    public static final int CUSTOM_MSG_MINI_GAME = 36;
    //邀请游戏通知 second
    public static final int CUSTOM_MSG_MINI_GAME_ON_INVITED_NOTIFY = 34;
    //邀请被同意通知 second
    public static final int CUSTOM_MSG_MINI_GAME_ON_INVITED_ACCEPT_NOTIFY = 35;
    //游戏结果通知 second
    public static final int CUSTOM_MSG_MINI_GAME_ON_RESULT_NOTIFY = 36;
    //游戏取消通知 second
    public static final int CUSTOM_MSG_MINI_GAME_ON_CANCEL_NOTIFY = 37;

    //游戏连麦相关
    public static final int CUSTOM_MSG_GAME_LINK_MACRO = 40;
    //游戏邀请连麦
    public static final int CUSTOM_MSG_GAME_LINK_MACRO_INVITATION = 1;
    //发起方取消连麦
    public static final int CUSTOM_MSG_GAME_LINK_MACRO_CANCEL = 2;
    //拒绝连麦
    public static final int CUSTOM_MSG_GAME_LINK_MACRO_REFRUSE = 3;
    //同意连麦
    public static final int CUSTOM_MSG_GAME_LINK_MACRO_AGREE = 4;
    //游戏连麦挂断
    public static final int CUSTOM_MSG_GAME_LINK_MACRO_FINISH = 5;
    //游戏同意连麦
    public static final int CUSTOM_MSG_GAME_LINK_MACRO_AGREE_GAME = 6;
    //连麦超时
    public static final int CUSTOM_MSG_GAME_LINK_MACRO_TIME_OUT = 7;
    //连麦占线
    public static final int CUSTOM_MSG_GAME_LINK_MACRO_BUSY = 8;

    //红包消息
    public static final int CUSTOM_MSG_ROOM_RED_PACKET = 38;
    //收到红包消息
    public static final int CUSTOM_MSG_ROOM_RED_PACKET_RECEIVE = 38;
    //红包领取完成消息
    public static final int CUSTOM_MSG_ROOM_RED_PACKET_FINISH = 39;
    public static final int CUSTOM_MSG_ROOM_RED_PACKET_RECEIVE_FINISH = 39;

    //偶遇私聊消息
    public static final int CUSTOM_MSG_CHANCE_MEETING = 41;

    //贵族消息
    public static final int CUSTOM_MSG_NOBLE = 42;
    //贵族发送广播消息
    public static final int CUSTOM_MSG_NOBLE_SEND_PUBLIC_MSG = 42;

    //公爵及国王开通/续费贵族后会在全站进行通知
    public static final int CUSTOM_MSG_OPEN_NOBLE_NOTIFY = 43;

    //官方赠送礼物，刷新礼物列表
    public static final int CUSTOM_MSG_OFFICIAL_PRESENTATION_GIFT = 44;

    //购买续费贵族，支付宝微信支付方式，由后台通知用户刷新
    public static final int CUSTOM_MSG_UPDATE_USER_NOBLE_INFO = 45;
    //更新禁言信息
    public static final int CUSTOM_MSG_UPDATE_MUTE_INFO = 46;
    //偶遇点赞
    public static final int CUSTOM_MSG_LIKE_FIRST = 47;
    public static final int CUSTOM_MSG_LIKE_SECOND = 47;
    //访客
    public static final int CUSTOM_MSG_VISITOR_FIRST = 48;
    public static final int CUSTOM_MSG_VISITOR_SCONED = 1;

    //挚友
    public static final int CUSTOM_MSG_BOSON_FIRST = 49;
    //挚友申请
    public static final int CUSTOM_MSG_BOSON_APPLY = 1;
    //挚友申请同意
    public static final int CUSTOM_MSG_BOSON_APPLY_AGREE = 2;
    //挚友上麦
    public static final int CUSTOM_MSG_BOSON_UP_MACRO = 3;

    //交友速配first
    public static final int CUSTOM_MSG_QUICK_MATING_FIRST = 60;
    public static final int CUSTOM_MSG_QUICK_MATING = 1;//交友速配成功
    public static final int CUSTOM_MSG_QUICK_MATING_ACTION = 2;//交友速配互动
    public static final int CUSTOM_MSG_QUICK_MATING_RENEW_TIME = 3;//交友速配续时
    public static final int CUSTOM_MSG_QUICK_MATING_FINISH = 4;//结束交友速配
    public static final int CUSTOM_MSG_QUICK_MATING_ATTENTION = 5;//交友速配关注


    //声音伴侣私聊消息
    public static final int CUSTOM_MSG_SOUND_MATING = 61;
    public static final int CUSTOM_MSG_SOUND_MATING_CARD = 1;

    /*--------------------------IM区别于云信的自定义消息通知------------------------------------>*/
    /**
     * 房间热度值更新通知
     */
    public static final int CUSTOM_MSG_ROOM_HOT_UPDATE = 50;

    public static final int CUSTOM_MSG_ROOM_OPERA_TIPS = 51;
    public static final int CUSTOM_MSG_ROOM_OPERA_TIPS_SHARE = 1;
    public static final int CUSTOM_MSG_ROOM_OPERA_TIPS_ATTENTION = 2;

    //粉丝 -- 后台暂时未有对应红点数通知逻辑
    public static final int CUSTOM_MSG_NEW_FANS = 52;

    //好友 -- 后台暂时未有对应红点数通知逻辑
    public static final int CUSTOM_MSG_NEW_FRIEND = 53;


    /**
     * 公聊的红包消息
     * first 15 公聊红包消息
     * second 1 发红包通知  2大厅抢红包通知
     * first   公屏的红包消息
     */
    public static final int CUSTOM_MSG_FIRST_PUBLIC_CHAT_ROOM = 15;

    public static final int CUSTOM_MSG_SECOND_PUBLIC_ROOM_SEND = 1;
    public static final int CUSTOM_MSG_SECOND_PUBLIC_ROOM_RECEIVE = 2;

    /**
     * （龙珠）速配 消息
     */
    public static final int CUSTOM_MSG_HEADER_TYPE_MATCH = 18;

    public static final int CUSTOM_MSG_HEADER_TYPE_MATCH_SPEED = 23;
    public static final int CUSTOM_MSG_HEADER_TYPE_MATCH_CHOICE = 24;

    /**
     * PK消息
     */
    public static final int CUSTOM_MSG_HEADER_TYPE_PK_FIRST = 19;

    public static final int CUSTOM_MSG_HEADER_TYPE_PK_SECOND_START = 27;//开始
    public static final int CUSTOM_MSG_HEADER_TYPE_PK_SECOND_END = 28;//结束
    public static final int CUSTOM_MSG_HEADER_TYPE_PK_SECOND_CANCEL = 25;//取消
    public static final int CUSTOM_MSG_HEADER_TYPE_PK_SECOND_ADD = 26;//投票

    /**
     * 爆出礼物的消息
     */
    public static final int CUSTOM_MSG_TYPE_BURST_GIFT = 31;//first和second一致

    /**
     * 房间的红包消息
     * first 32    房间的红包消息
     * second 1 房间发包通知  2房间抢红包通知
     */
    public static final int CUSTOM_MSG_FIRST_ROOM_RED_PACKAGE = 32;
    public static final int CUSTOM_MSG_SECOND_ROOM_SEND_RED_PACKAGE = 1;
    public static final int CUSTOM_MSG_SECOND_ROOM_RECEIVE_RED_PACKAGE = 2;
    public static final int CUSTOM_MSG_SECOND_ROOM_RED_PACKAGE_FULL_SERVICE = 3;

    //关注房间相关提示消息
    public static final int CUSTOM_MSG_FIRST_ROOM_ATTENTION_TIPS = 62;
    public static final int CUSTOM_MSG_SECOND_ROOM_ATTENTION_TIPS = 1;//已关注房间时公屏的提示消息
    public static final int CUSTOM_MSG_SECOND_ROOM_ATTENTION_GUIDE = 2;//未关注房间时公屏的引导消息

    //单人房--主播开/停播，相关通知
    public static final int CUSTOM_MSG_FIRST_ROOM_LIVE_NOTIFY = 63;
    public static final int CUSTOM_MSG_SECOND_ROOM_LIVE_START = 1;//主播已开播
    public static final int CUSTOM_MSG_SECOND_ROOM_LIVE_STOP = 2;//主播已停播

    //点赞给房主需要播放点赞动画
    public static final int CUSTOM_MSG_FIRST_ROOM_ADMIRE_NOTIFY = 65;
    public static final int CUSTOM_MSG_SECOND_ROOM_ADMIRE_ANIM = 1;//播放点赞动画
    public static final int CUSTOM_MSG_SECOND_ROOM_ADMIRE_MSG = 2;//公屏插入点赞消息


    //召集令
    public static final int CUSTOM_MSG_FIRST_CALL_UP = 64;
    public static final int CUSTOM_MSG_SECOND_CALL_UP = 1;//收到召集令,走的socket
    public static final int CUSTOM_MSG_SECOND_PUBLIC_CHAT_ROOM = 2;//召集令广播--走的云信，广播大厅
    public static final int CUSTOM_MSG_SECOND_PRIVATE_CHAT_MSG = 3;//召集令消息--走的云信，私聊消息
    public static final int CUSTOM_MSG_SECOND_CALL_UP_NUM_NOTIFY = 4;//房间通过召集令进房人数更新通知
    public static final int CUSTOM_MSG_SECOND_CALL_UP_CANCEL_NOTIFY = 5;//主播已经取消最近一次有效的召集令

    public static final int CUSTOM_MSG_QUICK_MATING_CHAT_CARD_FIRST = 66;//交友速配点赞私聊卡片
    public static final int CUSTOM_MSG_QUICK_MATING_CHAT_CARD_SECONED = 1;//交友速配点赞私聊卡片

    /**
     * 个人房连麦业务
     */
    public static final int CUSTOM_MSG_FIRST_AUDIO_MIC_CONN = 67;

    /**
     * 主播连线开关变更通知
     */
    public static final int CUSTOM_MSG_SECOND_AUDIO_MIC_SWITCH_CHANGE = 1;

    /**
     * 有新的用户申请语音连麦通知
     */
    public static final int CUSTOM_MSG_SECOND_AUDIO_CONN_REQ_NEW = 2;

    /**
     * 主播接收申请连接上麦通知
     */
    public static final int CUSTOM_MSG_SECOND_AUDIO_CONN_RECV_UP_MIC = 3;

    /**
     * 用户挂断语音连麦通知
     */
    public static final int CUSTOM_MSG_SECOND_AUDIO_CONN_USER_CALL_DOWN = 4;

    /**
     * 主播挂断语音连麦通知
     */
    public static final int CUSTOM_MSG_SECOND_AUDIO_CONN_ANCHOR_CALL_DOWN = 5;

    /**
     * 新的富文本通知，用于IM类房间，显示imroomMember信息
     */
    public static final int CUSTOM_MSG_ROOM_SYSTEM_RICH_TXT = 68;


    /**
     * 竞拍房消息
     */
    public static final int CUSTOM_MSG_FIRST_AUCTION = 69;
    //竞拍开始
    public static final int CUSTOM_MSG_SECOND_AUCTION_START = 1;
    //竞拍结束
    public static final int CUSTOM_MSG_SECOND_AUCTION_END = 2;
    //竞拍更新
    public static final int CUSTOM_MSG_SECOND_AUCTION_UPDATE = 3;
    //竞拍人数更新
    public static final int CUSTOM_MSG_SECOND_AUCTION_PARTICIPATE_COUNT = 4;
    /*<--------------------------IM区别于云信的自定义消息通知------------------------------------*/

    public CustomAttachment() {

    }

    //财富等级
    protected int experLevel;
    //魅力等级
    protected int charmLevel;

    public int getExperLevel() {
        return experLevel;
    }

    public void setExperLevel(int experLevel) {
        this.experLevel = experLevel;
    }

    public int getCharmLevel() {
        return charmLevel;
    }

    public void setCharmLevel(int charmLevel) {
        this.charmLevel = charmLevel;
    }


    public CustomAttachment(int first, int second) {
        this.first = first;
        this.second = second;
    }

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }

    // 解析附件内容。
    public void fromJson(JSONObject data) {
        if (data != null) {
            parseData(data);
        }
    }

    // 实现 MsgAttachment 的接口，封装公用字段，然后调用子类的封装函数。
    @Override
    public String toJson(boolean send) {

        return CustomAttachParser.packData(first, second, packData());
    }

//    @Override
//    public Json toJson() {
//        return CustomAttachParser.packData(first, second, packData2());
//    }

    // 子类的解析和封装接口。
    protected void parseData(JSONObject data) {

    }

    protected JSONObject packData() {
        return null;
    }


    protected JSONArray packArray() {
        return null;
    }

    //------------------im特殊字段----------------------------

    protected long time;

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
