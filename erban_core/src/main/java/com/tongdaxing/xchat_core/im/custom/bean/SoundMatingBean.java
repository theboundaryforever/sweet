package com.tongdaxing.xchat_core.im.custom.bean;

import java.io.Serializable;

public class SoundMatingBean implements Serializable {

    private String nick;//: 刚哈😮😮😮😮😮,
    private String targetNick;//: iPhone 6s,
    private int gender;//: 1,
    private int targetGender;//: 2,
    private String matchRatio;//: 91.17%,
    private long uid;//: 93000746,
    private long targetUid;//: 93000818,
    private String avatar;//: https://img.pinjin88.com/touxiangnan@3x.png,
    private String targetAvatar;//: https://img.pinjin88.com/Fjjkfxx8E3vOr6H4duxRGMKjXFd_?imageslim,
    private String question1;//: 你喜欢下雨天吗？
    private String introduce;//: 她是主音色为可爱少女音的巨蟹座女生,
    private String question2;//: 你做过最疯狂的事是什么？,

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getTargetNick() {
        return targetNick;
    }

    public void setTargetNick(String targetNick) {
        this.targetNick = targetNick;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getTargetGender() {
        return targetGender;
    }

    public void setTargetGender(int targetGender) {
        this.targetGender = targetGender;
    }

    public String getMatchRatio() {
        return matchRatio;
    }

    public void setMatchRatio(String matchRatio) {
        this.matchRatio = matchRatio;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public long getTargetUid() {
        return targetUid;
    }

    public void setTargetUid(long targetUid) {
        this.targetUid = targetUid;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getTargetAvatar() {
        return targetAvatar;
    }

    public void setTargetAvatar(String targetAvatar) {
        this.targetAvatar = targetAvatar;
    }

    public String getQuestion1() {
        return question1;
    }

    public void setQuestion1(String question1) {
        this.question1 = question1;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public String getQuestion2() {
        return question2;
    }

    public void setQuestion2(String question2) {
        this.question2 = question2;
    }
}
