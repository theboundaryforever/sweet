package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.tongdaxing.erban.libcommon.utils.json.JsonParser;

public class SoundMatingMsgAttachment extends CustomAttachment {

    private SoundMatingBean dataInfo;


    public SoundMatingMsgAttachment(int first, int second) {
        super(first, second);
    }

    public SoundMatingBean getDataInfo() {
        return dataInfo;
    }

    @Override
    protected void parseData(JSONObject data) {
        super.parseData(data);
        dataInfo = new SoundMatingBean();
        try {
            dataInfo = JsonParser.parseJsonToNormalObject(data.toJSONString(), SoundMatingBean.class);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        try {
            object = JSONObject.parseObject(new Gson().toJson(dataInfo));
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return object;
    }
}
