package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;

/**
 * Created by chenran on 2017/7/28.
 */

public class GiftAttachment extends CustomAttachment {

    private GiftReceiveInfo giftRecieveInfo;
    private String uid;


    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public GiftAttachment(int first, int second) {
        super(first, second);
    }


    public GiftReceiveInfo getGiftRecieveInfo() {
        return giftRecieveInfo;
    }

    public void setGiftRecieveInfo(GiftReceiveInfo giftRecieveInfo) {
        this.giftRecieveInfo = giftRecieveInfo;
    }

    //财富等级
    private int experLevel;
    //魅力等级
    private int charmLevel;

    @Override
    public int getExperLevel() {
        return experLevel;
    }

    @Override
    public void setExperLevel(int experLevel) {
        this.experLevel = experLevel;
    }

    @Override
    public int getCharmLevel() {
        return charmLevel;
    }

    @Override
    public void setCharmLevel(int charmLevel) {
        this.charmLevel = charmLevel;
    }

    @Override
    protected void parseData(JSONObject data) {
        super.parseData(data);
        giftRecieveInfo = new GiftReceiveInfo();
        giftRecieveInfo.setUid(data.getLong("uid"));
        giftRecieveInfo.setGiftId(data.getInteger("giftId"));
        if(data.containsKey("realGiftId")){
            giftRecieveInfo.setRealGiftId(data.getInteger("realGiftId"));
        }
        giftRecieveInfo.setAvatar(data.getString("avatar"));
        giftRecieveInfo.setNick(data.getString("nick"));
        giftRecieveInfo.setTargetUid(data.getLong("targetUid"));
        giftRecieveInfo.setGiftNum(data.getIntValue("giftNum"));
        giftRecieveInfo.setTargetNick(data.getString("targetNick"));
        giftRecieveInfo.setTargetAvatar(data.getString("targetAvatar"));
        giftRecieveInfo.setRoomId(data.getString("roomId"));
        giftRecieveInfo.setUserNo(data.getString("userNo"));

        giftRecieveInfo.setVipMedal(data.getString("vipMedal"));
        giftRecieveInfo.setVipIcon(data.getString("vipIcon"));
        giftRecieveInfo.setVipName(data.getString("vipName"));
        if (data.containsKey("roomType")) {
            giftRecieveInfo.setRoomType(data.getInteger("roomType"));
        }
        giftRecieveInfo.setVipId(data.getInteger("vipId"));
        giftRecieveInfo.setVipDate(data.getInteger("vipDate"));
        giftRecieveInfo.setInvisible(data.getBoolean("isInvisible"));
        giftRecieveInfo.setTargetVipId(data.getInteger("targetVipId"));
        giftRecieveInfo.setTargetVipDate(data.getInteger("targetVipDate"));
        giftRecieveInfo.setTargetIsInvisible(data.getBoolean("targetIsInvisible"));
        giftRecieveInfo.setTitle(data.getString("title"));

        giftRecieveInfo.setComboId(data.getLongValue("comboId"));
        giftRecieveInfo.setComboRangStart(data.getIntValue("comboRangStart"));
        giftRecieveInfo.setComboRangEnd(data.getIntValue("comboRangEnd"));
        giftRecieveInfo.setGiftType(data.getIntValue("giftType"));

        experLevel = data.getInteger("experLevel");
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        object.put("uid", giftRecieveInfo.getUid());
        object.put("giftId", giftRecieveInfo.getGiftId());
        object.put("realGiftId",giftRecieveInfo.getRealGiftId());
        object.put("avatar", giftRecieveInfo.getAvatar());
        object.put("nick", giftRecieveInfo.getNick());
        object.put("targetUid", giftRecieveInfo.getTargetUid());
        object.put("giftNum", giftRecieveInfo.getGiftNum());
        object.put("targetNick", giftRecieveInfo.getTargetNick());
        object.put("targetAvatar", giftRecieveInfo.getTargetAvatar());
        object.put("roomId", giftRecieveInfo.getRoomId());
        object.put("userNo", giftRecieveInfo.getUserNo());
        object.put("roomType", giftRecieveInfo.getRoomType());
        object.put("vipMedal", giftRecieveInfo.getVipMedal());
        object.put("vipIcon", giftRecieveInfo.getVipIcon());
        object.put("vipName", giftRecieveInfo.getVipName());

        object.put("vipId", giftRecieveInfo.getVipId());
        object.put("vipDate", giftRecieveInfo.getVipDate());
        object.put("isInvisible", giftRecieveInfo.isInvisible());

        object.put("targetVipId", giftRecieveInfo.getTargetVipId());
        object.put("targetVipDate", giftRecieveInfo.getTargetVipDate());
        object.put("targetIsInvisible", giftRecieveInfo.isTargetIsInvisible());

        object.put("experLevel", experLevel);

        object.put("comboId", giftRecieveInfo.getComboId());
        object.put("comboRangStart", giftRecieveInfo.getComboRangStart());
        object.put("comboRangEnd", giftRecieveInfo.getComboRangEnd());
        object.put("giftType", giftRecieveInfo.getGiftType());

        object.put("title", giftRecieveInfo.getTitle());
        return object;
    }

    @Override
    public String toString() {
        return "GiftAttachment{" +
                "giftRecieveInfo=" + giftRecieveInfo +
                ", uid='" + uid + '\'' +
                ", experLevel=" + experLevel +
                ", charmLevel=" + charmLevel +
                '}';
    }
}
