package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;

/**
 * @author liaoxy
 * @Description:交友速配匹配消息
 * @date 2019/5/29 17:03
 */
public class QuickMatingActionAttachment extends CustomAttachment {
    private String content;
    private int type;

    public String getContent() {
        return content;
    }

    public int getType() {
        return type;
    }

    public QuickMatingActionAttachment(int first, int second) {
        super(first, second);
        setVisible(false);
    }

    @Override
    protected void parseData(JSONObject data) {
        super.parseData(data);
        try {
            content = data.getString("content");
            type = data.getIntValue("type");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        object.put("type", type);
        object.put("content", content);
        return object;
    }
}
