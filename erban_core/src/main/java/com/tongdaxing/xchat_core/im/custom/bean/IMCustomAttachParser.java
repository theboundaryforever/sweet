package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachmentParser;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.attachmsg.RoomQueueMsgAttachment;

import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_FIRST_AUCTION;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_FIRST_AUDIO_MIC_CONN;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_FIRST_ROOM_ADMIRE_NOTIFY;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_FIRST_ROOM_ATTENTION_TIPS;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_FIRST_ROOM_LIVE_NOTIFY;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_VISITOR_FIRST;

/**
 * im自定义消息解析器
 * 避免和云信的混乱
 *
 * @author zwk
 * 自定义消息类型
 * 包装，解析
 */
public class IMCustomAttachParser implements MsgAttachmentParser {
    private static final String TAG = IMCustomAttachParser.class.getSimpleName();

    public static MsgAttachment parseCustomAttach(String json) {
        LogUtils.d(TAG, "parseCustomAttach-json:" + json);
        CustomAttachment attachment = null;
        try {
            JSONObject object = JSON.parseObject(json);
            int first = object.getInteger("first");
            int second = object.getInteger("second");
            JSONObject data = object.getJSONObject("data");
            switch (first) {
                case CustomAttachment.CUSTOM_MSG_ROOM_HOT_UPDATE:
                    attachment = new RoomHotRankUpdateAttachment(first, second);
                    attachment.setData(data);
                    break;
                case CustomAttachment.CUSTOM_MSG_ROOM_OPERA_TIPS:
                case CustomAttachment.CUSTOM_MSG_NEW_FRIEND:
                case CustomAttachment.CUSTOM_MSG_NEW_FANS:
                case CustomAttachment.CUSTOM_MSG_UPDATE_MUTE_INFO:
                case CustomAttachment.CUSTOM_MSG_OFFICIAL_PRESENTATION_GIFT:
                    attachment = new NotifyNoneAttachment(first, second);
                    attachment.setData(data);
                    break;
                case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_AUCTION:
                    //废弃的竞拍房逻辑
                    attachment = new CustomAttachment(first, second);
                    attachment.setData(data);
                    break;
                case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_ROOM_TIP:
                    attachment = new RoomTipAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT:
                    attachment = new GiftAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_OPEN_ROOM_NOTI:
                    attachment = new OpenRoomNotiAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE:
                    attachment = new RoomQueueMsgAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_FACE:
                    attachment = new FaceAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_NOTICE:
                    attachment = new NoticeAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PACKET:
                    attachment = new RedPacketAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT:
                    attachment = new MultiGiftAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_LOTTERY:
                    attachment = new LotteryAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUPER_GIFT:
                    attachment = new GiftAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_SUB_PUBLIC_CHAT_ROOM:
                    attachment = new PublicChatRoomAttachment(first, second);
                    break;

                case CustomAttachment.CUSTOM_MSG_LOTTERY_BOX:
                    attachment = new LotteryBoxAttachment(first, second);
                    break;

                case CustomAttachment.CUSTOM_MSG_MIC_IN_LIST:
                    attachment = new MicInListAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_SHARE_FANS:
                    attachment = new ShareFansAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK:
                    attachment = new PkCustomAttachment(first, second);
                    break;

                case CustomAttachment.CUSTOM_NOTI_HEADER_KTV:
                    break;
                case CustomAttachment.CUSTOM_MSG_ROOM_CHARM:
                    attachment = new RoomCharmAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_ROOM_PAIR:
                    attachment = new RoomPairAttachment(first, second);
                    break;
                case CustomAttachment.CUSTOM_MSG_UPDATE_USER_NOBLE_INFO:
                    attachment = new NobleNotifyAttachment(first, second);
                    attachment.setData(data);
                    break;
                case CustomAttachment.CUSTOM_MSG_ROOM_SYSTEM_RICH_TXT:
                case CustomAttachment.CUSTOM_MSG_ROOM_RICH_TXT:
                    attachment = new CustomAttachment(first, second);
                    attachment.setData(data);
                    break;
                case CustomAttachment.CUSTOM_MSG_ROOM_LOVER_UP_MIC:
                    attachment = new LoverUpMicAnimAttachment(first, second);
                    attachment.setData(data);
                    break;
                case CustomAttachment.CUSTOM_MSG_BOSON_FIRST:
                    if (second == CustomAttachment.CUSTOM_MSG_BOSON_UP_MACRO) {
                        attachment = new BosonFriendUpMicAnimAttachment(first, second);
                        attachment.setData(data);
                    }
                    break;
                case CustomAttachment.CUSTOM_MSG_ROOM_DETONATING_GIFT:
                    attachment = new DetonateGiftAttachment(first, second);
                    attachment.setData(data);
                    break;
                case CustomAttachment.CUSTOM_MSG_AUTH_STATUS_NOTIFY:
                    attachment = new CustomAttachment(first, second);
                    attachment.setData(data);
                    break;
                case CustomAttachment.CUSTOM_MSG_ANNUAL_ROOM_LIST:
                    attachment = new AnnualRoomRankAttachment(first, second);
                    attachment.setData(data);
                    break;
                case CustomAttachment.CUSTOM_MSG_MINI_GAME:
                    if (second == CustomAttachment.CUSTOM_MSG_MINI_GAME_ON_INVITED_NOTIFY) {
                        attachment = new MiniGameInvitedAttachment(first, second);
                        attachment.setData(data);
                    } else if (second == CustomAttachment.CUSTOM_MSG_MINI_GAME_ON_INVITED_ACCEPT_NOTIFY) {
                        attachment = new MiniGameInvitedAcceptAttachment(first, second);
                        attachment.setData(data);
                    } else if (second == CustomAttachment.CUSTOM_MSG_MINI_GAME_ON_RESULT_NOTIFY) {
                        long oppUid = data.getLongValue("oppUid");
                        long uid = CoreManager.getCore(IAuthCore.class).getCurrentAccount().getUid();
                        if (oppUid != uid) {
                            attachment = new MiniGameResultAttachmentGone(first, second);
                            attachment.setData(data);
                        } else {
                            attachment = new MiniGameResultAttachment(first, second);
                            attachment.setData(data);
                        }
                    } else if (second == CustomAttachment.CUSTOM_MSG_MINI_GAME_ON_CANCEL_NOTIFY) {
                        attachment = new MiniGameCancelAttachment(first, second);
                        attachment.setData(data);
                    }
                    break;
                case CustomAttachment.CUSTOM_MSG_ROOM_RED_PACKET:
                    attachment = new RoomRedPacketAttachment(first, second);
                    attachment.setData(data);
                    break;
                case CustomAttachment.CUSTOM_MSG_ROOM_RED_PACKET_FINISH:
                    attachment = new RoomRedPacketFinishAttachment(first, second);
                    attachment.setData(data);
                    break;
                case CustomAttachment.CUSTOM_MSG_GAME_LINK_MACRO:
                    if (second == CustomAttachment.CUSTOM_MSG_GAME_LINK_MACRO_INVITATION) {
                        attachment = new GameLinkMacroInvitationAttachment(first, second);
                        attachment.setData(data);
                    } else if (second == CustomAttachment.CUSTOM_MSG_GAME_LINK_MACRO_AGREE) {
                        attachment = new GameLinkMacroAgreeAttachment(first, second);
                        attachment.setData(data);
                    } else if (second == CustomAttachment.CUSTOM_MSG_GAME_LINK_MACRO_AGREE_GAME) {
                        attachment = new GameLinkMacroAgreeAttachment(first, second);
                        attachment.setData(data);
                    } else if (second == CustomAttachment.CUSTOM_MSG_GAME_LINK_MACRO_REFRUSE) {
                        attachment = new GameLinkMacroRefuseAttachment(first, second);
                        attachment.setData(data);
                    } else if (second == CustomAttachment.CUSTOM_MSG_GAME_LINK_MACRO_CANCEL) {
                        attachment = new GameLinkMacroCancelAttachment(first, second);
                        attachment.setData(data);
                    } else if (second == CustomAttachment.CUSTOM_MSG_GAME_LINK_MACRO_FINISH) {
                        attachment = new GameLinkMacroFinishAttachment(first, second);
                        attachment.setData(data);
                    } else if (second == CustomAttachment.CUSTOM_MSG_GAME_LINK_MACRO_BUSY) {
                        attachment = new GameLinkMacroBusyAttachment(first, second);
                        attachment.setData(data);
                    } else if (second == CustomAttachment.CUSTOM_MSG_GAME_LINK_MACRO_TIME_OUT) {
                        attachment = new GameLinkMacroTimeOutAttachment(first, second);
                        attachment.setData(data);
                    }
                    break;
                case CustomAttachment.CUSTOM_MSG_CHANCE_MEETING:
                    attachment = new ChanceMeetingMsgAttachment(first, second);
                    attachment.setData(data);
                    break;
                case CustomAttachment.CUSTOM_MSG_SOUND_MATING:
                    attachment = new SoundMatingMsgAttachment(first, second);
                    attachment.setData(data);
                    break;
                case CustomAttachment.CUSTOM_MSG_NOBLE:
                    if (second == CustomAttachment.CUSTOM_MSG_NOBLE_SEND_PUBLIC_MSG) {
                        attachment = new NoblePublicMsgAttachment(first, second);
                        attachment.setData(data);
                    }
                    break;
                case CustomAttachment.CUSTOM_MSG_OPEN_NOBLE_NOTIFY:
                    attachment = new OpenNobleNotifyAttachment(first, second);
                    attachment.setData(data);
                    break;
                case CustomAttachment.CUSTOM_MSG_LIKE_FIRST:
                    if (second == CustomAttachment.CUSTOM_MSG_LIKE_SECOND) {
                        attachment = new LikeAttachment(first, second);
                        attachment.setData(data);
                    }
                    break;
                case CUSTOM_MSG_VISITOR_FIRST:
                    //发送访客消息
                    if (second == CustomAttachment.CUSTOM_MSG_VISITOR_SCONED) {
                        attachment = new VisitorAttachment(first, second);
                        attachment.setData(data);
                    }
                    break;
                case CustomAttachment.CUSTOM_MSG_QUICK_MATING_FIRST:
                    if (second == CustomAttachment.CUSTOM_MSG_QUICK_MATING) {
                        attachment = new QuickMatingAttachment(first, second);
                        attachment.setData(data);
                    } else if (second == CustomAttachment.CUSTOM_MSG_QUICK_MATING_ACTION) {
                        attachment = new QuickMatingActionAttachment(first, second);
                        attachment.setData(data);
                    } else if (second == CustomAttachment.CUSTOM_MSG_QUICK_MATING_FINISH) {
                        attachment = new QuickMatingFinishAttachment(first, second);
                        attachment.setData(data);
                    } else if (second == CustomAttachment.CUSTOM_MSG_QUICK_MATING_RENEW_TIME) {
                        attachment = new QuickMatingRenewAttachment(first, second);
                        attachment.setData(data);
                    } else if (second == CustomAttachment.CUSTOM_MSG_QUICK_MATING_ATTENTION) {
                        attachment = new QuickMatingAttentionAttachment(first, second);
                        attachment.setData(data);
                    }
                    break;
                case CustomAttachment.CUSTOM_MSG_QUICK_MATING_CHAT_CARD_FIRST:
                    if (second == CustomAttachment.CUSTOM_MSG_QUICK_MATING_CHAT_CARD_SECONED) {
                        attachment = new QmLikeAttachment(first, second);
                        attachment.setData(data);
                    }
                    break;
                case CustomAttachment.CUSTOM_MSG_FIRST_CALL_UP:
                    if (second == CustomAttachment.CUSTOM_MSG_SECOND_PRIVATE_CHAT_MSG) {
                        attachment = new ShareFansAttachment(first, second);
                        attachment.setData(data);
                    } else if (second == CustomAttachment.CUSTOM_MSG_SECOND_CALL_UP_NUM_NOTIFY) {
                        attachment = new CallUpNumUpdateAttachment(first, second);
                        attachment.parseData(data);
                        attachment.setData(data);
                    } else {
                        attachment = new CallUpAttachment(first, second);
                        attachment.setData(data);
                    }
                    break;
                case CUSTOM_MSG_FIRST_AUCTION:
                    if (second == CustomAttachment.CUSTOM_MSG_SECOND_AUCTION_START
                            ||second == CustomAttachment.CUSTOM_MSG_SECOND_AUCTION_UPDATE
                            ||second == CustomAttachment.CUSTOM_MSG_SECOND_AUCTION_END){
                        attachment = new AuctionAttachment(first,second);
                    }else if (second == CustomAttachment.CUSTOM_MSG_SECOND_AUCTION_PARTICIPATE_COUNT){
                        attachment = new AuctionJoinCountAttachment(first,second);
                    }
                    break;
                case CUSTOM_MSG_FIRST_ROOM_LIVE_NOTIFY:
                case CUSTOM_MSG_FIRST_ROOM_ATTENTION_TIPS:
                case CUSTOM_MSG_FIRST_ROOM_ADMIRE_NOTIFY:
                case CUSTOM_MSG_FIRST_AUDIO_MIC_CONN:
                    attachment = new CustomAttachment(first, second);
                    attachment.setData(data);
                    break;
                default:
                    break;
            }

            if (attachment != null) {
                attachment.fromJson(data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return attachment;
    }

    // 根据解析到的消息类型，确定附件对象类型
    @Override
    public MsgAttachment parse(String json) {
        LogUtils.d(TAG, "parse-json:" + json);
        return parseCustomAttach(json);
    }

    public static String packData(int first, int second, JSONObject data) {
        JSONObject object = new JSONObject();
        object.put("first", first);
        object.put("second", second);
        if (data != null) {
            object.put("data", data);
        }
        return object.toJSONString();
    }


}