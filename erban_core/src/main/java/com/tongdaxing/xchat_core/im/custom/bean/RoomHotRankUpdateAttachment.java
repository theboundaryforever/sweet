package com.tongdaxing.xchat_core.im.custom.bean;


import com.alibaba.fastjson.JSONObject;
import com.tongdaxing.erban.libcommon.utils.json.Json;


/**
 * Created by Administrator on 2018/3/20.
 */

public class RoomHotRankUpdateAttachment extends CustomAttachment {

    private String params;

    private long hotRank;
    private long hotScore;

    public RoomHotRankUpdateAttachment(int first, int second) {
        super(first, second);
    }

    public long getHotRank() {
        return hotRank;
    }

    public long getHotScore() {
        return hotScore;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    @Override
    protected void parseData(JSONObject data) {
        params = data.getString("params");

        try {
            Json param = Json.parse(params);
            if (param.has("hotRank")) {
                hotRank = param.getLong("hotRank");
            }
            if (param.has("hotScore")) {
                hotScore = param.getLong("hotScore");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected JSONObject packData() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("params", params);
        return jsonObject;
    }
}
