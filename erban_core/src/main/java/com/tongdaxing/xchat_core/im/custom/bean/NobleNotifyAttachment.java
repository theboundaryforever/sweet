package com.tongdaxing.xchat_core.im.custom.bean;


import com.alibaba.fastjson.JSONObject;


/**
 * Created by weihiatao on 2018/3/20.
 * 开通贵族，到账通知
 */

public class NobleNotifyAttachment extends CustomAttachment {

    private String vipName;
    private int vipId;
    private String vipIcon;
    private String vipMedal;
    private int vipDate;
    private double money;
    private double renewMoney;
    private double giveGold;
    private double renewGiveGold;
    private int status;
    private String createTime;

    NobleNotifyAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    public String toString() {
        return "NobleNotifyAttachment{" +
                "vipName='" + vipName + '\'' +
                ", vipId=" + vipId +
                ", vipIcon='" + vipIcon + '\'' +
                ", vipMedal='" + vipMedal + '\'' +
                ", vipDate=" + vipDate +
                ", money=" + money +
                ", renewMoney=" + renewMoney +
                ", giveGold=" + giveGold +
                ", renewGiveGold=" + renewGiveGold +
                ", status=" + status +
                ", createTime=" + createTime +
                '}';
    }

    public String getVipName() {
        return vipName;
    }

    public void setVipName(String vipName) {
        this.vipName = vipName;
    }

    public int getVipId() {
        return vipId;
    }

    public void setVipId(int vipId) {
        this.vipId = vipId;
    }

    public String getVipIcon() {
        return vipIcon;
    }

    public void setVipIcon(String vipIcon) {
        this.vipIcon = vipIcon;
    }

    public int getVipDate() {
        return vipDate;
    }

    public void setVipDate(int vipDate) {
        this.vipDate = vipDate;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public double getRenewMoney() {
        return renewMoney;
    }

    public void setRenewMoney(double renewMoney) {
        this.renewMoney = renewMoney;
    }

    public double getGiveGold() {
        return giveGold;
    }

    public void setGiveGold(double giveGold) {
        this.giveGold = giveGold;
    }

    public double getRenewGiveGold() {
        return renewGiveGold;
    }

    public void setRenewGiveGold(double renewGiveGold) {
        this.renewGiveGold = renewGiveGold;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getVipMedal() {
        return vipMedal;
    }

    public void setVipMedal(String vipMedal) {
        this.vipMedal = vipMedal;
    }

    /**
     * {"first":45,"second":45,
     * "data":{
     * "vipId":3,
     * "vipName":"子爵",
     * "vipIcon":"https://img.pinjin88.com/zijue@3x.png",
     * "vipMedal":"https://img.pinjin88.com/3zijue@3x.png",
     * "vipDate":30,
     * "money":1500,
     * "renewMoney":1000,
     * "giveGold":9000,
     * "renewGiveGold":6000,
     * "status":1,
     * "createTime":"Mar 5, 2019 3:45:37 PM"}}
     */

    @Override
    protected void parseData(JSONObject data) {
        if (data.containsKey("vipId")) {
            vipId = data.getInteger("vipId");
        }
        if (data.containsKey("vipDate")) {
            vipDate = data.getInteger("vipDate");
        }
        if (data.containsKey("status")) {
            status = data.getInteger("status");
        }
        if (data.containsKey("vipName")) {
            vipName = data.getString("vipName");
        }
        if (data.containsKey("vipIcon")) {
            vipIcon = data.getString("vipIcon");
        }
        if (data.containsKey("vipMedal")) {
            vipMedal = data.getString("vipMedal");
        }
        if (data.containsKey("money")) {
            money = data.getDouble("money");
        }
        if (data.containsKey("renewMoney")) {
            renewMoney = data.getDouble("renewMoney");
        }
        if (data.containsKey("giveGold")) {
            giveGold = data.getDouble("giveGold");
        }
        if (data.containsKey("renewGiveGold")) {
            renewGiveGold = data.getDouble("renewGiveGold");
        }
        if (data.containsKey("createTime")) {
            createTime = data.getString("createTime");
        }

    }

    @Override
    protected JSONObject packData() {
        return data;
    }

}
