package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.JsonSyntaxException;

public class CallUpNumUpdateAttachment extends CustomAttachment {

    private long conveneUserCount;

    public CallUpNumUpdateAttachment(int first, int second) {
        super(first, second);
    }

    public long getConveneUserCount() {
        return conveneUserCount;
    }

    @Override
    protected void parseData(JSONObject data) {
        super.parseData(data);
        if (data != null && data.containsKey("conveneUserCount")) {
            conveneUserCount = data.getLongValue("conveneUserCount");
        }
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        try {
            object.put("conveneUserCount", conveneUserCount);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return object;
    }
}
