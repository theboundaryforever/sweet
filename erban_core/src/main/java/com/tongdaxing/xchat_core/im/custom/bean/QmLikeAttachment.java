package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.JsonSyntaxException;
import com.tongdaxing.erban.libcommon.utils.json.JsonParser;

public class QmLikeAttachment extends CustomAttachment {
    private QmLikeBean dataInfo;

    public QmLikeAttachment(int first, int second) {
        super(first, second);
        setVisible(true);
    }

    public QmLikeBean getDataInfo() {
        return dataInfo;
    }

    @Override
    protected void parseData(JSONObject data) {
        super.parseData(data);
        dataInfo = new QmLikeBean();
        try {
            dataInfo = JsonParser.parseJsonObject(data.toJSONString(), QmLikeBean.class);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        try {
            object = JSONObject.parseObject(JsonParser.toJson(dataInfo));
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return object;
    }
}
