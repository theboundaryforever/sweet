package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by chenran on 2017/7/28.
 */

public class BosonFriendUpMicAnimAttachment extends CustomAttachment {

    public String getFirstNick() {
        return firstNick;
    }

    public void setFirstNick(String firstNick) {
        this.firstNick = firstNick;
    }

    public String getFirstUid() {
        return firstUid;
    }

    public void setFirstUid(String firstUid) {
        this.firstUid = firstUid;
    }

    public String getSecondNick() {
        return secondNick;
    }

    public void setSecondNick(String secondNick) {
        this.secondNick = secondNick;
    }

    public String getSecondUid() {
        return secondUid;
    }

    public void setSecondUid(String secondUid) {
        this.secondUid = secondUid;
    }

    private String firstNick;
    private String firstUid;
    private String secondNick;
    private String secondUid;

    private int firstVipId;
    private int firstVipDate;
    private boolean firstIsInvisible;
    private int secondVipId;
    private int secondVipDate;
    private boolean secondIsInvisible;

    private String bosonFriendLevelName;
    private int bosonFriendLevel;

    public int getBosonFriendLevel() {
        return bosonFriendLevel;
    }

    public void setBosonFriendLevel(int bosonFriendLevel) {
        this.bosonFriendLevel = bosonFriendLevel;
    }

    public String getBosonFriendLevelName() {
        return bosonFriendLevelName;
    }

    public void setBosonFriendLevelName(String bosonFriendLevelName) {
        this.bosonFriendLevelName = bosonFriendLevelName;
    }

    public int getFirstVipId() {
        return firstVipId;
    }

    public void setFirstVipId(int firstVipId) {
        this.firstVipId = firstVipId;
    }

    public int getFirstVipDate() {
        return firstVipDate;
    }

    public void setFirstVipDate(int firstVipDate) {
        this.firstVipDate = firstVipDate;
    }

    public boolean isFirstIsInvisible() {
        return firstIsInvisible;
    }

    public void setFirstIsInvisible(boolean firstIsInvisible) {
        this.firstIsInvisible = firstIsInvisible;
    }

    public int getSecondVipId() {
        return secondVipId;
    }

    public void setSecondVipId(int secondVipId) {
        this.secondVipId = secondVipId;
    }

    public int getSecondVipDate() {
        return secondVipDate;
    }

    public void setSecondVipDate(int secondVipDate) {
        this.secondVipDate = secondVipDate;
    }

    public boolean isSecondIsInvisible() {
        return secondIsInvisible;
    }

    public void setSecondIsInvisible(boolean secondIsInvisible) {
        this.secondIsInvisible = secondIsInvisible;
    }

    public BosonFriendUpMicAnimAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected void parseData(JSONObject data) {
        super.parseData(data);
        firstNick = data.getString("firstNick");
        firstUid = data.getString("firstUid");
        secondNick = data.getString("secondNick");
        secondUid = data.getString("secondUid");

        firstVipId = data.getIntValue("firstVipId");
        firstVipDate = data.getIntValue("firstVipDate");
        firstIsInvisible = data.getBooleanValue("firstIsInvisible");
        secondVipId = data.getIntValue("secondVipId");
        secondVipDate = data.getIntValue("secondVipDate");
        secondIsInvisible = data.getBooleanValue("secondIsInvisible");
        bosonFriendLevel = data.getIntValue("bosonFriendLevel");
        bosonFriendLevelName = data.getString("bosonFriendLevelName");
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        object.put("firstNick", firstNick);
        object.put("firstUid", firstUid);
        object.put("secondNick", secondNick);
        object.put("secondUid", secondUid);
        object.put("firstVipId", firstVipId);
        object.put("firstVipDate", firstVipDate);
        object.put("firstIsInvisible", firstIsInvisible);
        object.put("secondVipId", secondVipId);
        object.put("secondVipDate", secondVipDate);
        object.put("secondIsInvisible", secondIsInvisible);
        object.put("bosonFriendLevel", bosonFriendLevel);
        object.put("bosonFriendLevelName", bosonFriendLevelName);
        return object;
    }

    @Override
    public String toString() {
        return "BosonFriendUpMicAnimAttachment{" +
                "firstNick='" + firstNick + '\'' +
                ", firstUid='" + firstUid + '\'' +
                ", secondNick='" + secondNick + '\'' +
                ", secondUid='" + secondUid + '\'' +
                ", firstVipId=" + firstVipId +
                ", firstVipDate=" + firstVipDate +
                ", firstIsInvisible=" + firstIsInvisible +
                ", secondVipId=" + secondVipId +
                ", secondVipDate=" + secondVipDate +
                ", secondIsInvisible=" + secondIsInvisible +
                '}';
    }
}
