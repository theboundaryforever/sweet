package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;

/**
 * 文件描述：竞拍房的参加竞拍人数更新消息
 *
 * @auther：zwk
 * @data：2019/8/21
 */
public class AuctionJoinCountAttachment extends CustomAttachment {

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        object.put("count", count);
        return object;
    }

    @Override
    protected void parseData(JSONObject data) {
        super.parseData(data);
        if (data.containsKey("count")) {
            count = data.getIntValue("count");
        }
    }

    public AuctionJoinCountAttachment(int first, int second) {
        super(first, second);
    }

    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

}
