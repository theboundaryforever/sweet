package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;

/**
 * @author liaoxy
 * @Description:交友速配续费消息
 * @date 2019/5/29 17:03
 */
public class QuickMatingRenewAttachment extends CustomAttachment {
    private String content;

    public String getContent() {
        return content;
    }

    public QuickMatingRenewAttachment(int first, int second) {
        super(first, second);
        setVisible(false);
    }

    @Override
    protected void parseData(JSONObject data) {
        super.parseData(data);
        try {
            content = data.getString("content");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        object.put("content", content);
        return object;
    }
}
