package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;
import com.tongdaxing.erban.libcommon.utils.json.JsonParser;
import com.tongdaxing.xchat_core.room.auction.RoomAuctionBean;

/**
 * 文件描述：竞拍房消息附件：竞拍开始、竞拍数据更新、竞拍结束
 *
 * @auther：zwk
 * @data：2019/8/20
 */
public class AuctionAttachment extends CustomAttachment {
    private RoomAuctionBean roomAuctionBean;

    public AuctionAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected void parseData(JSONObject data) {
        super.parseData(data);
        roomAuctionBean = JsonParser.parseJsonToNormalObject(data.toJSONString(), RoomAuctionBean.class);
    }

    @Override
    protected JSONObject packData() {
        if (roomAuctionBean != null) {
            return JSONObject.parseObject(JsonParser.toJson(roomAuctionBean));
        } else {
            return new JSONObject();
        }
    }

    public RoomAuctionBean getRoomAuctionBean() {
        return roomAuctionBean;
    }

    public void setRoomAuctionBean(RoomAuctionBean roomAuctionBean) {
        this.roomAuctionBean = roomAuctionBean;
    }
}
