package com.tongdaxing.xchat_core.im.avroom;

import com.tongdaxing.erban.libcommon.coremanager.IBaseCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

/**
 * Created by zhouxiangfeng on 2017/5/29.
 */

public interface IAVRoomCore extends IBaseCore {
    void joinChannel(String channelId, int uid);
    void joinHighQualityChannel(String channelId, int uid, boolean record);
    void leaveChannel();
    void setRole(int role);
    void setMute(boolean mute);
    void setRemoteMute(boolean mute);
    void setRecordMute(boolean mute);
    int startAudioMixing(String filePath, boolean loopback, int cycle);
    int resumeAudioMixing();
    int pauseAudioMixing();
    int stopAudioMixing();
    void adjustAudioMixingVolume(int volume);
    void adjustRecordingSignalVolume(int volume);
    boolean isAudienceRole();
    boolean isMute();
    boolean isRemoteMute();
    boolean isRecordMute();
    void requestRoomOwnerInfo(String uid);
    UserInfo getRoomOwner();
    void removeRoomOwnerInfo();

    void changeRoomMsgFilter(boolean roomOwner, int publicChatSwitch, String ticket, String uid,
                             int targetVipId, int targetVipDate, boolean targetIsInvisiable, String targetNick);

    void getRoomCharm(long myUid, long roomId);

    void clearCharm(String currentUid, String s);
}
