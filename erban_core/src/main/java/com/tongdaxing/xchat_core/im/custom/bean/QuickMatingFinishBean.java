package com.tongdaxing.xchat_core.im.custom.bean;

import java.io.Serializable;

public class QuickMatingFinishBean implements Serializable {

    private String content;
    private int oppPeaNum;
    private String talkTime;
    private int oppLastBuyCount;
    private int oppLastMatchCount;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getOppPeaNum() {
        return oppPeaNum;
    }

    public void setOppPeaNum(int oppPeaNum) {
        this.oppPeaNum = oppPeaNum;
    }

    public String getTalkTime() {
        return talkTime;
    }

    public void setTalkTime(String talkTime) {
        this.talkTime = talkTime;
    }

    public int getOppLastBuyCount() {
        return oppLastBuyCount;
    }

    public void setOppLastBuyCount(int oppLastBuyCount) {
        this.oppLastBuyCount = oppLastBuyCount;
    }

    public int getOppLastMatchCount() {
        return oppLastMatchCount;
    }

    public void setOppLastMatchCount(int oppLastMatchCount) {
        this.oppLastMatchCount = oppLastMatchCount;
    }
}
