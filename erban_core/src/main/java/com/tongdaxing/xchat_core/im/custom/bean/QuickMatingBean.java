package com.tongdaxing.xchat_core.im.custom.bean;

import java.io.Serializable;

/**
 * @author liaoxy
 * @Description:连麦IM消息
 * @date 2019/5/29 17:00
 */
public class QuickMatingBean implements Serializable {
    private int recordId;
    private String reason;//": "交友速配邀请",
    private int flowerCount;
    private int linkFlowerCount;
    private int gender;//": 1,
    private boolean isFans;//": true,
    private String avatar;//": "https://img.pinjin88.com/touxiangnan@3x.png",
    private int linkGender;//": 1,
    private String nick;//": "jndj要昵",
    private long uid;//": 93000811,
    private String linkAvatar;//": "https://img.pinjin88.com/touxiangnan@3x.png",
    private boolean linkIsFans;//": true,
    private String linkNick;//": "刚哈����������",
    private long linkUid;//": 93000746,
    private String channelId;//": "93000746_93000811"

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public int getLinkFlowerCount() {
        return linkFlowerCount;
    }

    public void setLinkFlowerCount(int linkFlowerCount) {
        this.linkFlowerCount = linkFlowerCount;
    }

    public int getFlowerCount() {
        return flowerCount;
    }

    public void setFlowerCount(int flowerCount) {
        this.flowerCount = flowerCount;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public boolean isFans() {
        return isFans;
    }

    public void setFans(boolean fans) {
        isFans = fans;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getLinkGender() {
        return linkGender;
    }

    public void setLinkGender(int linkGender) {
        this.linkGender = linkGender;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getLinkAvatar() {
        return linkAvatar;
    }

    public void setLinkAvatar(String linkAvatar) {
        this.linkAvatar = linkAvatar;
    }

    public boolean isLinkIsFans() {
        return linkIsFans;
    }

    public void setLinkIsFans(boolean linkIsFans) {
        this.linkIsFans = linkIsFans;
    }

    public String getLinkNick() {
        return linkNick;
    }

    public void setLinkNick(String linkNick) {
        this.linkNick = linkNick;
    }

    public long getLinkUid() {
        return linkUid;
    }

    public void setLinkUid(long linkUid) {
        this.linkUid = linkUid;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }
}