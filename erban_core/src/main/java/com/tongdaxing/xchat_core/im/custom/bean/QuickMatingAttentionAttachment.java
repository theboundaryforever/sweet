package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;

/**
 * @author liaoxy
 * @Description:交友速配中的关注IM消息
 * @date 2019/6/3 11:43
 */
public class QuickMatingAttentionAttachment extends CustomAttachment {
    private String content;

    public String getContent() {
        return content;
    }

    public QuickMatingAttentionAttachment(int first, int second) {
        super(first, second);
        setVisible(false);
    }

    @Override
    protected void parseData(JSONObject data) {
        super.parseData(data);
        try {
            content = data.getString("content");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        object.put("content", content);
        return object;
    }
}
