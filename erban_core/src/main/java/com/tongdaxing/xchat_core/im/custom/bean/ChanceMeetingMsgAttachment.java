package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by weihaitao on 2019/2/18.
 */

public class ChanceMeetingMsgAttachment extends CustomAttachment {

    /**
     * 接受方UID
     */
    private long targetUid = 0L;

    /**
     * 接收方头像url
     */
    private String targetAvatar = null;

    /**
     * 消息发送时间
     */
    private long sendTime = 0L;

    public ChanceMeetingMsgAttachment(int first, int second) {
        super(first, second);
    }

    public long getTargetUid() {
        return targetUid;
    }

    public void setTargetUid(long targetUid) {
        this.targetUid = targetUid;
    }

    public String getTargetAvatar() {
        return targetAvatar;
    }

    public void setTargetAvatar(String targetAvatar) {
        this.targetAvatar = targetAvatar;
    }


    public long getSendTime() {
        return sendTime;
    }

    public void setSendTime(long sendTime) {
        this.sendTime = sendTime;
    }

    @Override
    protected void parseData(JSONObject data) {
        super.parseData(data);

        if (null != data && data.containsKey("targetUid")) {
            targetUid = data.getLong("targetUid");
        }

        if (null != data && data.containsKey("targetAvatar")) {
            targetAvatar = data.getString("targetAvatar");
        }

        if (null != data && data.containsKey("sendTime")) {
            sendTime = data.getLong("sendTime");
        }
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        object.put("targetUid", targetUid);
        object.put("targetAvatar", targetAvatar);
        object.put("sendTime", sendTime);
        return object;
    }

    @Override
    public String toString() {
        return "ChanceMeetingMsgAttachment{" +
                "targetUid=" + targetUid +
                ", targetAvatar='" + targetAvatar + '\'' +
                ", sendTime=" + sendTime +
                '}';
    }

}
