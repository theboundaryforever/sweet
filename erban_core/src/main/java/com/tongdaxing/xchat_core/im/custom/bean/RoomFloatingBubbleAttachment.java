package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/7/12
 */
public class RoomFloatingBubbleAttachment extends CustomAttachment {
    private int msgType;
    private String avatar;
    private String msgLeftLogo;
    private String msgBgUrl;
    private String msgContent;

    @Override
    protected void parseData(JSONObject data) {
        if (data != null) {
            if (data.containsKey("msgType")) {
                msgType = data.getIntValue("msgType");
            }
            if (data.containsKey("avatar")) {
                avatar = data.getString("avatar");
            }
            if (data.containsKey("msgLeftLogo")) {
                msgLeftLogo = data.getString("msgLeftLogo");
            }
            if (data.containsKey("msgBgUrl")) {
                msgBgUrl = data.getString("msgBgUrl");
            }
            if (data.containsKey("msgContent")) {
                msgContent = data.getString("msgContent");
            }
        }
    }

    @Override
    protected JSONObject packData() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("msgType", msgType);
        jsonObject.put("avatar", avatar);
        jsonObject.put("msgLeftLogo", msgLeftLogo);
        jsonObject.put("msgBgUrl", msgBgUrl);
        jsonObject.put("msgContent", msgContent);
        return jsonObject;
    }


    public int getMsgType() {
        return msgType;
    }

    public void setMsgType(int msgType) {
        this.msgType = msgType;
    }

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getMsgLeftLogo() {
        return msgLeftLogo;
    }

    public void setMsgLeftLogo(String msgLefgLogo) {
        this.msgLeftLogo = msgLefgLogo;
    }

    public String getMsgBgUrl() {
        return msgBgUrl;
    }

    public void setMsgBgUrl(String msgBgUrl) {
        this.msgBgUrl = msgBgUrl;
    }
}
