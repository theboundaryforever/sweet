package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.JsonSyntaxException;
import com.tongdaxing.erban.libcommon.utils.json.JsonParser;
import com.tongdaxing.xchat_core.bean.RoomRedPacket;

public class RoomRedPacketAttachment extends CustomAttachment {
    private RoomRedPacket dataInfo;
    private long msgTime;

    public long getMsgTime() {
        return msgTime;
    }

    public void setMsgTime(long msgTime) {
        this.msgTime = msgTime;
    }

    public RoomRedPacket getDataInfo() {
        return dataInfo;
    }

    public void setDataInfo(RoomRedPacket dataInfo) {
        this.dataInfo = dataInfo;
    }

    public RoomRedPacketAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected void parseData(JSONObject data) {
        super.parseData(data);
        dataInfo = new RoomRedPacket();
        try {
            String json = data.getString("params");
            dataInfo = JsonParser.parseJsonToNormalObject(json, RoomRedPacket.class);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        try {
            object = JSONObject.parseObject(JsonParser.toJson(dataInfo));
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return object;
    }
}
