package com.tongdaxing.xchat_core.im.custom.bean;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;


/**
 * Created by weihiatao on 2018/3/20.
 */

public class OpenNobleNotifyAttachment extends CustomAttachment {

    private String nick;
    private long uid;
    private String vipName;
    private int vipId;
    private String avatar;
    private long roomId;
    private long roomUid;
    private long userNo;
    private String vipIcon;

    private String vipMedal;

    public String getVipMedal() {
        return vipMedal;
    }

    public void setVipMedal(String vipMedal) {
        this.vipMedal = vipMedal;
    }

    @Override
    public String toString() {
        return "OpenNobleNotifyAttachment{" +
                "nick='" + nick + '\'' +
                ", uid=" + uid +
                ", vipName='" + vipName + '\'' +
                ", vipId=" + vipId +
                ", avatar='" + avatar + '\'' +
                ", roomId=" + roomId +
                ", roomUid=" + roomUid +
                ", vipIcon='" + vipIcon + '\'' +
                ", vipMedal='" + vipMedal + '\'' +
                ", roomTitle='" + roomTitle + '\'' +
                ", userNo='" + userNo + '\'' +
                '}';
    }
    private String roomTitle;

    public OpenNobleNotifyAttachment(int first, int second) {
        super(first, second);
    }

    public long getRoomUid() {
        return roomUid;
    }

    public void setRoomUid(long roomUid) {
        this.roomUid = roomUid;
    }

    public String getRoomTitle() {
        return roomTitle;
    }

    public void setRoomTitle(String roomTitle) {
        this.roomTitle = roomTitle;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getVipName() {
        return vipName;
    }

    public void setVipName(String vipName) {
        this.vipName = vipName;
    }

    public int getVipId() {
        return vipId;
    }

    public void setVipId(int vipId) {
        this.vipId = vipId;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public String getVipIcon() {
        return vipIcon;
    }

    public void setVipIcon(String vipIcon) {
        this.vipIcon = vipIcon;
    }

    public long getUserNo() {
        return userNo;
    }

    public void setUserNo(long userNo) {
        this.userNo = userNo;
    }

    @Override
    protected void parseData(JSONObject data) {
        if (data.containsKey("params")) {
            String paramsStr = data.getString("params");
            JSONObject params = JSON.parseObject(paramsStr);
            if (params.containsKey("nick")) {
                nick = params.getString("nick");
            }
            if (params.containsKey("uid")) {
                uid = params.getLong("uid");
            }
            if (params.containsKey("vipName")) {
                vipName = params.getString("vipName");
            }
            if (params.containsKey("vipId")) {
                vipId = params.getInteger("vipId");
            }
            if (params.containsKey("avatar")) {
                avatar = params.getString("avatar");
            }
            if (params.containsKey("roomId")) {
                roomId = params.getLong("roomId");
            }
            if (params.containsKey("roomUid")) {
                roomUid = params.getLong("roomUid");
            }
            if (params.containsKey("userNo")) {
                userNo = params.getLong("userNo");
            }
            if (params.containsKey("vipIcon")) {
                vipIcon = params.getString("vipIcon");
            }
            if (params.containsKey("vipMedal")) {
                vipMedal = params.getString("vipMedal");
            }
            if (params.containsKey("roomTitle")) {
                roomTitle = params.getString("roomTitle");
            }
        }
    }

    @Override
    protected JSONObject packData() {
        return data;
    }

}
