package com.tongdaxing.xchat_core.im.custom.bean;


import com.alibaba.fastjson.JSONObject;


/**
 * Created by Administrator on 2018/3/20.
 */

public class PublicChatRoomAttachment extends CustomAttachment {


    private String msg;
    private String params;
    private String roomId;

    private long uid;
    private String avatar;
    private int charmLevel;
    private int experLevel;
    private String nick;
    private String txtColor;
    private long server_msg_id;
    private long redPackId;

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @Override
    public int getCharmLevel() {
        return charmLevel;
    }

    @Override
    public void setCharmLevel(int charmLevel) {
        this.charmLevel = charmLevel;
    }

    @Override
    public int getExperLevel() {
        return experLevel;
    }

    @Override
    public void setExperLevel(int experLevel) {
        this.experLevel = experLevel;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getTxtColor() {
        return txtColor;
    }

    public void setTxtColor(String txtColor) {
        this.txtColor = txtColor;
    }

    public long getServerMsgId() {
        return server_msg_id;
    }

    public void setServerMsgId(long server_msg_id) {
        this.server_msg_id = server_msg_id;
    }

    public long getRedPackId() {
        return redPackId;
    }

    public void setRedPackId(long redPackId) {
        this.redPackId = redPackId;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public PublicChatRoomAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected void parseData(JSONObject data) {
        roomId = data.getString("roomId");
        msg = data.getString("msg");
        if (data.containsKey("params")) {
            params = data.getString("params");
            try {
                JSONObject param = data.getJSONObject("params");
                if (null != param) {
                    if (param.containsKey("avatar")) {
                        avatar = param.getString("avatar");
                    }
                    if (param.containsKey("nick")) {
                        nick = param.getString("nick");
                    }

                    if (param.containsKey("uid")) {
                        uid = param.getLong("uid");
                    }

                    if (param.containsKey("charmLevel")) {
                        charmLevel = param.getIntValue("charmLevel");
                    }

                    if (param.containsKey("experLevel")) {
                        experLevel = param.getIntValue("experLevel");
                    }
                    if (param.containsKey("txtColor")) {
                        txtColor = param.getString("txtColor");
                    }
                    if (param.containsKey("redPackId")) {
                        redPackId = param.getLongValue("redPackId");
                    }
                    if (data.containsKey("server_msg_id")) {
                        server_msg_id = data.getLongValue("server_msg_id");
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    @Override
    protected JSONObject packData() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("msg", msg);
        jsonObject.put("params", params);
        if (uid > 0) {
            JSONObject params = new JSONObject();
            params.put("uid", uid);
            params.put("avatar", avatar);
            params.put("nick", nick);
            params.put("charmLevel", charmLevel);
            params.put("experLevel", experLevel);
            params.put("txtColor", txtColor);
            params.put("redPackId", redPackId);
            jsonObject.put("params", params);
        }

        jsonObject.put("roomId", roomId);
        return jsonObject;
    }
}
