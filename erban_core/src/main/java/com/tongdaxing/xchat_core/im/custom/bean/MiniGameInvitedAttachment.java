package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;
import com.tongdaxing.xchat_core.bean.MiniGameInvitedInfo;

/**
 * 小游戏邀请通知
 */

public class MiniGameInvitedAttachment extends CustomAttachment {
    private MiniGameInvitedInfo dataInfo;

    public MiniGameInvitedInfo getDataInfo() {
        return dataInfo;
    }

    public void setDataInfo(MiniGameInvitedInfo dataInfo) {
        this.dataInfo = dataInfo;
    }

    public MiniGameInvitedAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected void parseData(JSONObject data) {
        super.parseData(data);
        dataInfo = new MiniGameInvitedInfo();
        dataInfo.setFromAvatar(data.getString("fromAvatar"));
        dataInfo.setFromNick(data.getString("fromNick"));
        dataInfo.setFromUid(data.getLong("fromUid"));
        dataInfo.setRoomid(data.getLong("roomid"));
        dataInfo.setGameBgImage(data.getString("gameBgImage"));
        dataInfo.setGameName(data.getString("gameName"));
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        object.put("fromAvatar", dataInfo.getFromAvatar());
        object.put("fromNick", dataInfo.getFromNick());
        object.put("fromUid", dataInfo.getFromUid());
        object.put("roomid", dataInfo.getRoomid());
        object.put("gameBgImage", dataInfo.getGameBgImage());
        object.put("gameName", dataInfo.getGameName());
        return object;
    }
}
