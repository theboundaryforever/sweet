package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;
import com.tongdaxing.xchat_core.bean.MiniGameResult;

/**
 * 小游戏结束通知
 */

public class MiniGameResultAttachmentGone extends CustomAttachment {
    private MiniGameResult dataInfo;

    public MiniGameResult getDataInfo() {
        return dataInfo;
    }

    public void setDataInfo(MiniGameResult dataInfo) {
        this.dataInfo = dataInfo;
    }

    public MiniGameResultAttachmentGone(int first, int second) {
        super(first, second);
    }

    @Override
    protected void parseData(JSONObject data) {
        super.parseData(data);
        dataInfo = new MiniGameResult();
        dataInfo.setOppAvatar(data.getString("oppAvatar"));
        dataInfo.setOppNick(data.getString("oppNick"));
        dataInfo.setResult(data.getIntValue("result"));
        dataInfo.setRoomid(data.getLong("roomid"));
        dataInfo.setGameBgImage(data.getString("gameBgImage"));
        dataInfo.setGameName(data.getString("gameName"));
        dataInfo.setGameType(data.getString("gameType"));
        dataInfo.setIncScore(data.getIntValue("incScore"));
        dataInfo.setOppWinCount(data.getIntValue("oppWinCount"));
        dataInfo.setOwnerWinCount(data.getIntValue("ownerWinCount"));
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        object.put("oppAvatar", dataInfo.getOppAvatar());
        object.put("oppNick", dataInfo.getOppNick());
        object.put("result", dataInfo.getResult());
        object.put("roomid", dataInfo.getRoomid());
        object.put("gameBgImage", dataInfo.getGameBgImage());
        object.put("gameName", dataInfo.getGameName());
        object.put("gameType", dataInfo.getRoomid());
        object.put("incScore", dataInfo.getIncScore());
        object.put("oppWinCount", dataInfo.getOppWinCount());
        object.put("ownerWinCount", dataInfo.getOwnerWinCount());
        return object;
    }
}
