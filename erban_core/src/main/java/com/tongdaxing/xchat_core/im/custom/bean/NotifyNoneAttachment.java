package com.tongdaxing.xchat_core.im.custom.bean;


import com.alibaba.fastjson.JSONObject;
import com.tongdaxing.erban.libcommon.utils.LogUtils;

/**
 * Created by Administrator on 2018/3/13.
 */

public class NotifyNoneAttachment extends CustomAttachment {

    private final String TAG = NotifyNoneAttachment.class.getSimpleName();

    NotifyNoneAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected void parseData(JSONObject data) {
        LogUtils.d(TAG, "parseData-data:" + data);
    }

    @Override
    protected JSONObject packData() {
        return new JSONObject();
    }
}
