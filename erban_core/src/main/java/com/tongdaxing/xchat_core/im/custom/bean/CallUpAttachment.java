package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.JsonSyntaxException;
import com.tongdaxing.erban.libcommon.utils.json.JsonParser;

public class CallUpAttachment extends CustomAttachment {
    private CallUpBean dataInfo;

    public CallUpAttachment(int first, int second) {
        super(first, second);
        setVisible(true);
    }

    public CallUpBean getDataInfo() {
        return dataInfo;
    }

    @Override
    protected void parseData(JSONObject data) {
        super.parseData(data);
        dataInfo = new CallUpBean();
        try {
            dataInfo = JsonParser.parseJsonObject(data.toJSONString(), CallUpBean.class);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        try {
            object = JSONObject.parseObject(JsonParser.toJson(dataInfo));
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return object;
    }
}
