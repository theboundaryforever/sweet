package com.tongdaxing.xchat_core.im.custom.bean;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;


/**
 * Created by Administrator on 2018/3/20.
 */

public class AnnualRoomRankAttachment extends CustomAttachment {

    public String getAnnualCeremonyUrl() {
        return annualCeremonyUrl;
    }

    public void setAnnualCeremonyUrl(String annualCeremonyUrl) {
        this.annualCeremonyUrl = annualCeremonyUrl;
    }

    //年度活动房间排行
    private String annualCeremonyUrl;
    //当前房间排行
    private Long ranking;

    public Long getRanking() {
        return ranking;
    }

    public void setRanking(Long ranking) {
        this.ranking = ranking;
    }

    public Double getPreviousScore() {
        return previousScore;
    }

    public void setPreviousScore(Double previousScore) {
        this.previousScore = previousScore;
    }

    public Double getNextScore() {
        return nextScore;
    }

    public void setNextScore(Double nextScore) {
        this.nextScore = nextScore;
    }

    //同前一名相差的分数
    private Double previousScore;
    //同下一名相差的分数
    private Double nextScore;

    public AnnualRoomRankAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected void parseData(JSONObject data) {
        if (data.containsKey("content")) {
            String paramsStr = data.getString("content");
            JSONObject params = JSON.parseObject(paramsStr);
            if (params.containsKey("annualCeremonyUrl")) {
                annualCeremonyUrl = params.getString("annualCeremonyUrl");
            }
            if (params.containsKey("ranking")) {
                ranking = params.getLong("ranking");
            }
            if (params.containsKey("previousScore")) {
                previousScore = params.getDouble("previousScore");
            }
            if (params.containsKey("nextScore")) {
                nextScore = params.getDouble("nextScore");
            }
        }
    }

    @Override
    protected JSONObject packData() {
        return data;
    }
}
