package com.tongdaxing.xchat_core.im.avroom;

import com.tongdaxing.erban.libcommon.coremanager.ICoreClient;

import java.util.Map;

/**
 * Created by zhouxiangfeng on 2017/6/6.
 */

public interface IAVRoomCoreClient extends ICoreClient {

    String METHOD_ON_CREAT_AV_ROOM = "onCreatAVRoom";
    String METHOD_ON_CREAT_AV_ROOM_FAITH = "onCreatAVRoomFail";
    String METHOD_ON_JOIN_AV_ROOM = "onJoinAVRoom";
    String METHOD_ON_JOIN_AV_ROOM_FAITH = "onJoinAVRoomFail";
    String METHOD_ON_LEAVE_AV_ROOM = "onLeaveAVRoom";
    String METHOD_ON_SPEEK = "onSpeek";
    String METHOD_ON_AUDIO_MIXING_FINISHED = "onAudioMixingFinished";
    String METHOD_ON_AUDIO_MIXING_STARTED = "onAudioMixingStarted";
    String METHOD_ON_AUDIO_MIXING_ERROR = "onAudioMixingError";
    String METHOD_ON_USER_MUTE_AUDIO = "onUserMuteAudio";
    String METHOD_ON_MY_AUDIO_MUTE = "onMyAudioMute";
    String METHOD_ON_NETWORK_BAD = "onNetworkBad";
    String METHOD_ON_CONNECT_LOST = "onConnectionLost";
    String micInlistMoveToTop = "micInlistMoveToTop";
    String onMicInListChange = "onMicInListChange";
    String onUserCarIn = "onUserCarIn";
    String sendMsg = "sendMsg";
    String onMicInListToUpMic = "onMicInListToUpMic";
    String micInListDismiss = "micInListDismiss";


    void micInListDismiss();

    void onMicInListToUpMic(int key, String uid);

    void micInlistMoveToTop(int micPosition, String roomId, String value);

    void onMicInListChange();

    void sendMsg(String msg);


    void onAudioMixingStarted();


    void onAudioMixingFinished();

    void onAudioMixingError();

    void onNetworkBad();

    void onConnectionLost();

    void onCreatAVRoom();

    void onCreatAVRoomFail();

    void onJoinAVRoom();

    void onJoinAVRoomFail(int code);

    void onLeaveAVRoom();

    void onSpeek(Map<String, Integer> map);

    void onUserMuteAudio(int uid);

    void onMyAudioMute();

    void onUserCarIn(String carUrl);
}
