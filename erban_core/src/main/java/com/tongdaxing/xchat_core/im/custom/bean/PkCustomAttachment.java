package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.JsonSyntaxException;
import com.tongdaxing.erban.libcommon.utils.json.JsonParser;
import com.tongdaxing.xchat_core.pk.bean.PkVoteInfo;

public class PkCustomAttachment extends CustomAttachment {
    private PkVoteInfo pkVoteInfo;

    public PkCustomAttachment(int first, int second) {
        super(first, second);
    }

    public PkVoteInfo getPkVoteInfo() {
        return pkVoteInfo;
    }

    public void setPkVoteInfo(PkVoteInfo pkVoteInfo) {
        this.pkVoteInfo = pkVoteInfo;
    }

    @Override
    protected void parseData(JSONObject data) {
        super.parseData(data);
        pkVoteInfo = new PkVoteInfo();
        try {
            String createTime = data.getString("createTime");
            if (createTime == null || createTime.length() < 4) {
                data.put("createTime", 0);
            }
            pkVoteInfo = JsonParser.parseJsonObject(data.toJSONString(), PkVoteInfo.class);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        try {
            object = JSONObject.parseObject(JsonParser.toJson(pkVoteInfo));
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return object;
    }
}
