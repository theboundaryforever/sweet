package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.JsonSyntaxException;
import com.tongdaxing.erban.libcommon.utils.json.JsonParser;

/**
 * @author liaoxy
 * @Description:交友速配结束消息
 * @date 2019/5/29 17:03
 */
public class QuickMatingFinishAttachment extends CustomAttachment {
    private QuickMatingFinishBean dataInfo;

    public QuickMatingFinishBean getDataInfo() {
        return dataInfo;
    }

    public QuickMatingFinishAttachment(int first, int second) {
        super(first, second);
        setVisible(false);
    }

    @Override
    protected void parseData(JSONObject data) {
        super.parseData(data);
        dataInfo = new QuickMatingFinishBean();
        try {
            dataInfo = JsonParser.parseJsonObject(data.toJSONString(), QuickMatingFinishBean.class);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        try {
            object = JSONObject.parseObject(JsonParser.toJson(dataInfo));
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return object;
    }
}
