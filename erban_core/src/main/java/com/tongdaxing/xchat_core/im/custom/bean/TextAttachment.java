package com.tongdaxing.xchat_core.im.custom.bean;


import com.alibaba.fastjson.JSONObject;

/**
 * Created by Administrator on 2018/3/13.
 */

public class TextAttachment extends CustomAttachment {


    //财富等级
    private int experLevel;
    //魅力等级
    private int charmLevel;

    @Override
    public int getExperLevel() {
        return experLevel;
    }

    @Override
    public void setExperLevel(int experLevel) {
        this.experLevel = experLevel;
    }

    @Override
    public int getCharmLevel() {
        return charmLevel;
    }

    @Override
    public void setCharmLevel(int charmLevel) {
        this.charmLevel = charmLevel;
    }
    @Override
    protected void parseData(JSONObject data) {
        experLevel = data.getInteger("experLevel");

    }

    @Override
    protected JSONObject packData() {


        JSONObject jsonObject = new JSONObject();

        jsonObject.put("experLevel", experLevel);

        return jsonObject;
    }
}
