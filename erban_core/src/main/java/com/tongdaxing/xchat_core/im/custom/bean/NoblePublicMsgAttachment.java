package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.tongdaxing.erban.libcommon.utils.json.JsonParser;
import com.tongdaxing.xchat_core.bean.NoblePublicMsg;

/**
 * @author liaoxy
 * @Description:贵族全站广播消息附件
 * @date 2019/3/12 9:57
 */
public class NoblePublicMsgAttachment extends CustomAttachment {
    private NoblePublicMsg dataInfo;

    public NoblePublicMsg getDataInfo() {
        return dataInfo;
    }

    public void setDataInfo(NoblePublicMsg dataInfo) {
        this.dataInfo = dataInfo;
    }

    public NoblePublicMsgAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected void parseData(JSONObject data) {
        super.parseData(data);
        dataInfo = new NoblePublicMsg();
        try {
            String json = data.getString("params");
            dataInfo = JsonParser.parseJsonToNormalObject(json, NoblePublicMsg.class);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        try {
            object = JSONObject.parseObject(new Gson().toJson(dataInfo));
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return object;
    }
}
