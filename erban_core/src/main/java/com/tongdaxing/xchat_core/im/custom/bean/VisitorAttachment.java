package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.tongdaxing.erban.libcommon.utils.json.JsonParser;
import com.tongdaxing.xchat_core.bean.VisitorInfo;

public class VisitorAttachment extends CustomAttachment {
    private VisitorInfo dataInfo;

    public VisitorInfo getDataInfo() {
        return dataInfo;
    }

    public void setDataInfo(VisitorInfo dataInfo) {
        this.dataInfo = dataInfo;
    }

    public VisitorAttachment(int first, int second) {
        super(first, second);
        setVisible(false);
    }

    @Override
    protected void parseData(JSONObject data) {
        super.parseData(data);
        dataInfo = new VisitorInfo();
        try {
            //String json = data.getString("params");
            dataInfo = JsonParser.parseJsonToNormalObject(data.toJSONString(), VisitorInfo.class);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        try {
            object = JSONObject.parseObject(new Gson().toJson(dataInfo));
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return object;
    }
}
