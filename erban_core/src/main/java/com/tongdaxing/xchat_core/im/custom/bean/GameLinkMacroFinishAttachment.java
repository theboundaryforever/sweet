package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.tongdaxing.erban.libcommon.utils.json.JsonParser;
import com.tongdaxing.xchat_core.bean.GameLinkMacroInvitaion;

/**
 * @author liaoxy
 * @Description:游戏连麦邀请通话正常结束
 * @date 2019/2/15 11:13
 */
public class GameLinkMacroFinishAttachment extends CustomAttachment {
    private GameLinkMacroInvitaion dataInfo;

    public GameLinkMacroInvitaion getDataInfo() {
        return dataInfo;
    }

    public void setDataInfo(GameLinkMacroInvitaion dataInfo) {
        this.dataInfo = dataInfo;
    }

    public GameLinkMacroFinishAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected void parseData(JSONObject data) {
        super.parseData(data);
        dataInfo = new GameLinkMacroInvitaion();
        try {
            //String json = data.getString("params");
            dataInfo = JsonParser.parseJsonToNormalObject(data.toJSONString(), GameLinkMacroInvitaion.class);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        try {
            object = JSONObject.parseObject(new Gson().toJson(dataInfo));
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return object;
    }
}
