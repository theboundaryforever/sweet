package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;
import com.tongdaxing.xchat_core.bean.MiniGameCancel;

/**
 * 小游戏结束通知
 */

public class MiniGameCancelAttachment extends CustomAttachment {
    private MiniGameCancel dataInfo;

    public MiniGameCancel getDataInfo() {
        return dataInfo;
    }

    public void setDataInfo(MiniGameCancel dataInfo) {
        this.dataInfo = dataInfo;
    }

    public MiniGameCancelAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected void parseData(JSONObject data) {
        super.parseData(data);
        dataInfo = new MiniGameCancel();
        dataInfo.setFromAvatar(data.getString("fromAvatar"));
        dataInfo.setFromNick(data.getString("fromNick"));
        dataInfo.setFromUid(data.getIntValue("fromUid"));
        dataInfo.setGameType(data.getIntValue("gameType"));
        dataInfo.setRoomid(data.getLong("roomid"));
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        object.put("fromAvatar", dataInfo.getFromAvatar());
        object.put("fromNick", dataInfo.getFromNick());
        object.put("roomid", dataInfo.getRoomid());
        object.put("gameType", dataInfo.getRoomid());
        object.put("fromUid", dataInfo.getFromUid());
        return object;
    }
}
