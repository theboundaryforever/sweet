package com.tongdaxing.xchat_core.im.custom.bean;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;


/**
 * Created by Administrator on 2018/3/20.
 */

public class DetonateGiftAttachment extends CustomAttachment {

    public static final long detonateGiftAnimTime = 900000L;


    public int getDetonatingState() {
        return detonatingState;
    }

    public void setDetonatingState(int detonatingState) {
        this.detonatingState = detonatingState;
    }

    public int getDetonatingGifts() {
        return detonatingGifts;
    }

    public void setDetonatingGifts(int detonatingGifts) {
        this.detonatingGifts = detonatingGifts;
    }

    public long getDetonatingTime() {
        return detonatingTime;
    }

    public void setDetonatingTime(long detonatingTime) {
        this.detonatingTime = detonatingTime;
    }

    public long getDetonatingUid() {
        return detonatingUid;
    }

    public void setDetonatingUid(long detonatingUid) {
        this.detonatingUid = detonatingUid;
    }

    public long getRoomUid() {
        return roomUid;
    }

    public void setRoomUid(long roomUid) {
        this.roomUid = roomUid;
    }

    public String getDetonatingAvatar() {
        return detonatingAvatar;
    }

    public void setDetonatingAvatar(String detonatingAvatar) {
        this.detonatingAvatar = detonatingAvatar;
    }

    public String getDetonatingNick() {
        return detonatingNick;
    }

    public void setDetonatingNick(String detonatingNick) {
        this.detonatingNick = detonatingNick;
    }

    // 引爆进度,-1为隐藏
    private int detonatingState=-1;
    // 引爆礼物id
    private int detonatingGifts;
    // 引爆有效时间
    private long detonatingTime;
    //引爆人ID
    private long detonatingUid;
    private int roomType;

    //引爆礼物当前处于哪个房间
    private long roomUid;
    //引爆人头像
    private String detonatingAvatar;
    //引爆人昵称
    private String detonatingNick;
    private String detonatingGiftName;
    private String detonatingGiftUrl;

    public long getDetonatingDuration() {
        return detonatingDuration;
    }

    public void setDetonatingDuration(long detonatingDuration) {
        this.detonatingDuration = detonatingDuration;
    }

    //引爆协议有效期，以秒为单位
    private long detonatingDuration;

    public DetonateGiftAttachment(int first, int second) {
        super(first, second);
    }

    public static long getDetonateGiftAnimTime() {
        return detonateGiftAnimTime;
    }

    @Override
    protected JSONObject packData() {
        return data;
    }

    @Override
    protected void parseData(JSONObject data) {
        //{"params":"{\"detonating\":16,\"roomUid\":90000499,\"detonatingState\":16,\"detonatingUid\":0,\"detonatingGifts\":0,\"detonatingTime\":0}"}}

        if(data.containsKey("params")){
            String paramsStr = data.getString("params");
            JSONObject params = JSON.parseObject(paramsStr);
            if(params.containsKey("detonatingState")){
                detonatingState = params.getInteger("detonatingState");
            }
            if(params.containsKey("detonatingGifts")){
                detonatingGifts = params.getInteger("detonatingGifts");
            }
            if(params.containsKey("detonatingTime")){
                detonatingTime = params.getLong("detonatingTime");
            }
            if(params.containsKey("detonatingUid")){
                detonatingUid = params.getLong("detonatingUid");
            }
            if(params.containsKey("roomUid")){
                roomUid = params.getLong("roomUid");
            }
            if (params.containsKey("roomType")) {
                roomType = params.getIntValue("roomType");
            }
            if(params.containsKey("detonatingAvatar")){
                detonatingAvatar = params.getString("detonatingAvatar");
            }
            if(params.containsKey("detonatingNick")){
                detonatingNick = params.getString("detonatingNick");
            }
            if(params.containsKey("detonatingDuration")){
                detonatingDuration = params.getLong("detonatingDuration");
            }
            if (params.containsKey("detonatingGiftName")) {
                detonatingGiftName = params.getString("detonatingGiftName");
            }
            if (params.containsKey("detonatingGiftUrl")) {
                detonatingGiftUrl = params.getString("detonatingGiftUrl");
            }
        }
    }

    public String getDetonatingGiftName() {
        return detonatingGiftName;
    }

    public void setDetonatingGiftName(String detonatingGiftName) {
        this.detonatingGiftName = detonatingGiftName;
    }

    public String getDetonatingGiftUrl() {
        return detonatingGiftUrl;
    }

    public void setDetonatingGiftUrl(String detonatingGiftUrl) {
        this.detonatingGiftUrl = detonatingGiftUrl;
    }

    public int getRoomType() {
        return roomType;
    }

    public void setRoomType(int roomType) {
        this.roomType = roomType;
    }
}
