package com.tongdaxing.xchat_core.im.custom.bean;


import com.alibaba.fastjson.JSONObject;
import com.tongdaxing.erban.libcommon.utils.json.Json;


/**
 * Created by Administrator on 2018/3/20.
 */

public class LotteryBoxAttachment extends CustomAttachment {

    private String params;

    private String giftName;
    private int count;
    private int goldPrice;
    private String giftUrl;
    private String nick;

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getGoldPrice() {
        return goldPrice;
    }

    public void setGoldPrice(int goldPrice) {
        this.goldPrice = goldPrice;
    }

    public String getGiftUrl() {
        return giftUrl;
    }

    public void setGiftUrl(String giftUrl) {
        this.giftUrl = giftUrl;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public LotteryBoxAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected void parseData(JSONObject data) {
        params = data.getString("params");

        try {
            Json param = Json.parse(params);
            if (param.has("goldPrice")) {
                goldPrice = param.getInt("goldPrice");
            }
            if (param.has("count")) {
                count = param.getInt("count");
            }
            if (param.has("giftName")) {
                giftName = param.getString("giftName");
            }
            if (param.has("giftUrl")) {
                giftUrl = param.getString("giftUrl");
            }
            if (param.has("nick")) {
                nick = param.getString("nick");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected JSONObject packData() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("params", params);
        return jsonObject;
    }
}
