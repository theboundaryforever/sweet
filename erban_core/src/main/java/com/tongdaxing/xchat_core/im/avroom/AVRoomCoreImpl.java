package com.tongdaxing.xchat_core.im.avroom;

import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.netease.nim.uikit.common.util.log.LogUtil;
import com.tencent.bugly.crashreport.BuglyLog;
import com.tongdaxing.erban.libcommon.coremanager.AbstractBaseCore;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.result.UserResult;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import io.agora.rtc.Constants;
import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.RtcEngine;

import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_CLOSE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_OPEN;
import static io.agora.rtc.Constants.AUDIO_PROFILE_DEFAULT;
import static io.agora.rtc.Constants.AUDIO_PROFILE_MUSIC_HIGH_QUALITY;
import static io.agora.rtc.Constants.AUDIO_SCENARIO_CHATROOM_ENTERTAINMENT;
import static io.agora.rtc.Constants.AUDIO_SCENARIO_GAME_STREAMING;


/**
 * Created by zhouxiangfeng on 2017/5/29.
 */

public class AVRoomCoreImpl extends AbstractBaseCore implements IAVRoomCore {

    private final String TAG = AVRoomCoreImpl.class.getSimpleName();

    private static Map<String, Integer> speakers;
    private static String uid;
    private RtcEngine mRtcEngine;
    private boolean isAudienceRole;
    private boolean isMute;
    private boolean isRemoteMute;
    private boolean isRecordMute;


    private long time;
    private AVHandle handler = new AVHandle();
    private UserInfo roomOwnerInfo = null;

    @Override
    public void joinChannel(String channelId, int uid) {
        joinHighQualityChannel(channelId, uid, false);
//        AVRoomCoreImpl.uid = uid + "";
//        this.isMute = false;
//        this.isRemoteMute = false;
//        this.isRecordMute = false;
////        ensureRtcEngineReadyLock();
//        if (mRtcEngine == null) {
//            try {
//                BuglyLog.d(TAG, "joinChannel-channelId:" + channelId + " uid:" + uid + ", create RtcEngine!");
//                mRtcEngine = RtcEngine.create(BasicConfig.INSTANCE.getAppContext(), "314a1f874912455baeb1ad5ff838664a", new EngineEventHandler());
//            } catch (Exception e) {
//                throw new RuntimeException("NEED TO check rtc sdk init fatal error\n" + Log.getStackTraceString(e));
//            }
//            mRtcEngine.setChannelProfile(Constants.CHANNEL_PROFILE_LIVE_BROADCASTING);
//            mRtcEngine.setAudioProfile(AUDIO_PROFILE_MUSIC_HIGH_QUALITY, AUDIO_SCENARIO_CHATROOM_ENTERTAINMENT);
//            mRtcEngine.enableAudioVolumeIndication(600, 3); // 200 ms
//            mRtcEngine.setDefaultAudioRoutetoSpeakerphone(true);
//            mRtcEngine.setLogFile(Environment.getExternalStorageDirectory()
//                    + File.separator + BasicConfig.INSTANCE.getAppContext().getPackageName() + "/log/agora-rtc.log");
//        }
//        time = System.currentTimeMillis();
//        LogUtil.i("enterRoom", "av enterRoom--->");
//        RtcEngineManager.fixRtcSdkCompat(mRtcEngine);
//        mRtcEngine.joinChannel(null, channelId, null, uid);
    }

    @Override
    public void joinHighQualityChannel(String channelId, int uid, boolean record) {
        AVRoomCoreImpl.uid = uid + "";
        this.isMute = false;
        this.isRemoteMute = false;
        this.isRecordMute = false;

        if (mRtcEngine == null) {
            try {
                BuglyLog.d(TAG, "joinHighQualityChannel-channelId:" + channelId + " uid:" + uid + " record:" + record + ", create RtcEngine!");
                mRtcEngine = RtcEngine.create(BasicConfig.INSTANCE.getAppContext(), "314a1f874912455baeb1ad5ff838664a", new EngineEventHandler());
            } catch (Exception e) {
                throw new RuntimeException("NEED TO check rtc sdk init fatal error\n" + Log.getStackTraceString(e));
            }
            mRtcEngine.setChannelProfile(Constants.CHANNEL_PROFILE_LIVE_BROADCASTING);
            mRtcEngine.setAudioProfile(AUDIO_PROFILE_MUSIC_HIGH_QUALITY, AUDIO_SCENARIO_GAME_STREAMING);
            mRtcEngine.enableAudioVolumeIndication(600, 3); // 200 ms
            mRtcEngine.setDefaultAudioRoutetoSpeakerphone(true);
//            mRtcEngine.setParameters("{\"rtc.log_filter\":65535}");
            mRtcEngine.setLogFile(Environment.getExternalStorageDirectory()
                    + File.separator + BasicConfig.INSTANCE.getAppContext().getPackageName() + "/log/agora-rtc.log");
        }
        time = System.currentTimeMillis();
        LogUtil.i("enterRoom", "av enterRoom--->");
//        RtcEngineManager.fixRtcSdkCompat(mRtcEngine);
//        if (isSaveDumpFile) {
//            mRtcEngine.setParameters("{\"che.audio.start_debug_recording\":\"NoName\"}");
//        }
        mRtcEngine.joinChannel(null, channelId, null, uid);
    }

    @Override
    public void leaveChannel() {
        if (mRtcEngine != null) {
            stopAudioMixing();
            mRtcEngine.leaveChannel();
            mRtcEngine = null;
        }
        isAudienceRole = true;
        isMute = false;
        isRemoteMute = false;

    }

    private RtcEngine ensureRtcEngineReadyLock() {
        if (mRtcEngine == null) {
            try {
                mRtcEngine = RtcEngine.create(BasicConfig.INSTANCE.getAppContext(), "314a1f874912455baeb1ad5ff838664a", new EngineEventHandler());
            } catch (Exception e) {
                throw new RuntimeException("NEED TO check rtc sdk init fatal error\n" + Log.getStackTraceString(e));
            }
            mRtcEngine.setChannelProfile(Constants.CHANNEL_PROFILE_LIVE_BROADCASTING);
            mRtcEngine.setAudioProfile(AUDIO_PROFILE_DEFAULT, AUDIO_SCENARIO_CHATROOM_ENTERTAINMENT);
            mRtcEngine.enableAudioVolumeIndication(600, 3); // 200 ms
            mRtcEngine.setDefaultAudioRoutetoSpeakerphone(true);
            mRtcEngine.setLogFile(Environment.getExternalStorageDirectory()
                    + File.separator + BasicConfig.INSTANCE.getAppContext().getPackageName() + "/log/agora-rtc.log");
        }
        return mRtcEngine;
    }

    @Override
    public void setRole(int role) {
        if (mRtcEngine != null) {
            if (isMute) {
                mRtcEngine.setClientRole(Constants.CLIENT_ROLE_AUDIENCE);
            } else {
                mRtcEngine.setClientRole(role);
            }

            isAudienceRole = role != Constants.CLIENT_ROLE_BROADCASTER;
//                mRtcEngine.muteLocalAudioStream(isMute);
//                mRtcEngine.muteAllRemoteAudioStreams(isRemoteMute);
        }
    }

    @Override
    public int startAudioMixing(String filePath, boolean loopback, int cycle) {
        if (mRtcEngine != null) {
            mRtcEngine.stopAudioMixing();
            int result = 0;
            try {
                result = mRtcEngine.startAudioMixing(filePath, loopback, false, cycle);
            } catch (Exception e) {
                return -1;
            }
            return result;
        }
        return -1;
    }

    @Override
    public int resumeAudioMixing() {
        if (mRtcEngine != null) {
            int result = mRtcEngine.resumeAudioMixing();
            return result;
        }
        return -1;
    }

    @Override
    public int pauseAudioMixing() {
        if (mRtcEngine != null) {
            int result = mRtcEngine.pauseAudioMixing();
            return result;
        }
        return -1;
    }

    @Override
    public int stopAudioMixing() {
        if (mRtcEngine != null) {
            int result = mRtcEngine.stopAudioMixing();
            return result;
        }

        return -1;
    }

    @Override
    public void adjustAudioMixingVolume(int volume) {
        if (mRtcEngine != null) {
            mRtcEngine.adjustAudioMixingVolume(volume);
        }
    }

    @Override
    public void adjustRecordingSignalVolume(int volume) {
        if (mRtcEngine != null) {
            mRtcEngine.adjustRecordingSignalVolume(volume);
        }
    }

    @Override
    public boolean isAudienceRole() {
        return isAudienceRole;
    }

    @Override
    public boolean isMute() {
        return isMute;
    }

    @Override
    public void setMute(boolean mute) {
        if (mRtcEngine != null) {
            int result = 0;

            if (mute) {
                result = mRtcEngine.setClientRole(Constants.CLIENT_ROLE_AUDIENCE);
            } else {
                result = mRtcEngine.setClientRole(Constants.CLIENT_ROLE_BROADCASTER);
            }
            if (result == 0) {
                isMute = mute;
            }
        }
    }

    @Override
    public boolean isRemoteMute() {
        return isRemoteMute;
    }

    @Override
    public void setRemoteMute(boolean mute) {
        if (mRtcEngine != null) {
            int result = mRtcEngine.muteAllRemoteAudioStreams(mute);
            if (result == 0) {
                isRemoteMute = mute;
            }
        }
    }

    @Override
    public boolean isRecordMute() {
        return isRecordMute;
    }

    @Override
    public void setRecordMute(boolean recordMute) {
        if (mRtcEngine != null) {
            int result = -1;
            if (recordMute) {
                result = mRtcEngine.adjustRecordingSignalVolume(0);
            } else {
                result = mRtcEngine.adjustRecordingSignalVolume(100);
            }
            if (result == 0) {
                isRecordMute = recordMute;
            }
        }
    }

    @Override
    public void requestRoomOwnerInfo(String uid) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("queryUid", uid + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");

        OkHttpManager.getInstance().getRequest(UriProvider.getUserInfo(), params, new OkHttpManager.MyCallBack<UserResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(UserResult response) {
                if (response.isSuccess()) {
                    roomOwnerInfo = response.getData();
                }
            }
        });
    }

    @Override
    public UserInfo getRoomOwner() {
        return roomOwnerInfo;
    }

    @Override
    public void removeRoomOwnerInfo() {
        roomOwnerInfo = null;
    }

    @Override
    public void changeRoomMsgFilter(boolean roomOwner, final int publicChatSwitch, String ticket, String uid,
                                    int targetVipId, int targetVipDate, boolean targetIsInvisiable, String targetNick) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", uid + "");
        requestParam.put("ticket", ticket);
        requestParam.put("roomType", RoomInfo.ROOMTYPE_HOME_PARTY + "");
        requestParam.put("tagId", String.valueOf(AvRoomDataManager.get().mCurrentRoomInfo.tagId));
        requestParam.put("publicChatSwitch", publicChatSwitch + "");
        String shortUrl;
        if (roomOwner) {
            shortUrl = UriProvider.updateRoomInfo();
        } else {
            requestParam.put("roomUid", AvRoomDataManager.get().mCurrentRoomInfo.getUid() + "");
            shortUrl = UriProvider.updateRoomInfoByAdimin();
        }

        OkHttpManager.getInstance().postRequest(shortUrl, requestParam, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Json json) {
                if (json.num("code") == 200) {
                    if (publicChatSwitch == 0) {
                        IMNetEaseManager.get().systemNotificationBySdk(
                                -1, CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_OPEN, -1,
                                targetVipId, targetVipDate, targetIsInvisiable, targetNick);
                    } else {
                        IMNetEaseManager.get().systemNotificationBySdk(
                                -1, CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_CLOSE, -1,
                                targetVipId, targetVipDate, targetIsInvisiable, targetNick);
                    }
                }
            }
        });
    }

    @Override
    public void getRoomCharm(long uid, long roomId) {
        Map<String, String> param = OkHttpManager.getDefaultParam();
        param.put("uid", uid + "");
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        param.put("roomId", roomId + "");
        OkHttpManager.getInstance().postRequest(UriProvider.getRoomCharm(), param, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Json response) {
                if (response.num("code") == 200) {
                    Json json = response.json_ok("data");
                    IMNetEaseManager.get().noticeRoomCharm(json);
                }
            }
        });

    }

    @Override
    public void clearCharm(String currentUid, String roomId) {
        Map<String, String> param = OkHttpManager.getDefaultParam();
        param.put("uid", currentUid);
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        param.put("roomId", roomId);
        OkHttpManager.getInstance().postRequest(UriProvider.getResetRoomCharm(), param, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {

            }

            @Override
            public void onResponse(Json response) {

            }
        });
    }

    private static class AVHandle extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 0) {
                CoreManager.notifyClients(IAVRoomCoreClient.class, IAVRoomCoreClient.METHOD_ON_JOIN_AV_ROOM);
            } else if (msg.what == 1) {
                IRtcEngineEventHandler.AudioVolumeInfo[] audioVolumeInfos = (IRtcEngineEventHandler.AudioVolumeInfo[]) msg.obj;
                if (speakers == null) {
                    speakers = new HashMap<>();
                }
                speakers.clear();
                if (audioVolumeInfos != null) {
                    for (int i = 0; i < audioVolumeInfos.length; i++) {
                        String audioUid = audioVolumeInfos[i].uid + "";
                        if (audioUid.equals("0")) {
                            audioUid = uid;
                        }
                        speakers.put(audioUid, audioVolumeInfos[i].volume);
                    }
                }
                CoreManager.notifyClients(IAVRoomCoreClient.class, IAVRoomCoreClient.METHOD_ON_SPEEK, speakers);
            } else if (msg.what == 2) {
                Integer uid = (Integer) msg.obj;
                CoreManager.notifyClients(IAVRoomCoreClient.class, IAVRoomCoreClient.METHOD_ON_USER_MUTE_AUDIO, uid.intValue());
            }
        }

    }

    private class EngineEventHandler extends IRtcEngineEventHandler {
        @Override
        public void onJoinChannelSuccess(String channel, int uid, int elapsed) {
            super.onJoinChannelSuccess(channel, uid, elapsed);
            LogUtil.i("enterRoom", "av enterRoom Success--->" + (System.currentTimeMillis() - time));
            handler.sendEmptyMessage(0);
        }

        @Override
        public void onLeaveChannel(RtcStats stats) {
            super.onLeaveChannel(stats);
        }

        @Override
        public void onUserJoined(int uid, int elapsed) {
            super.onUserJoined(uid, elapsed);
        }

        @Override
        public void onActiveSpeaker(int uid) {
            super.onActiveSpeaker(uid);
        }

        @Override
        public void onLastmileQuality(int quality) {
            super.onLastmileQuality(quality);
            if (quality >= 3) {
                notifyClients(IAVRoomCoreClient.class, IAVRoomCoreClient.METHOD_ON_NETWORK_BAD);
            }
        }

        @Override
        public void onConnectionInterrupted() {
            super.onConnectionInterrupted();
            notifyClients(IAVRoomCoreClient.class, IAVRoomCoreClient.METHOD_ON_CONNECT_LOST);
        }

        @Override
        public void onConnectionLost() {
            super.onConnectionLost();
            notifyClients(IAVRoomCoreClient.class, IAVRoomCoreClient.METHOD_ON_CONNECT_LOST);
        }

        @Override
        public void onAudioVolumeIndication(AudioVolumeInfo[] speakers, int totalVolume) {
            super.onAudioVolumeIndication(speakers, totalVolume);
            Message message = handler.obtainMessage();
            message.what = 1;
            message.obj = speakers;
            handler.sendMessage(message);
        }

        @Override
        public void onUserMuteAudio(int uid, boolean muted) {
            super.onUserMuteAudio(uid, muted);
            if (muted) {
                Message message = handler.obtainMessage();
                message.what = 2;
                message.obj = uid;
                handler.sendMessage(message);
            }
        }

        @Override
        public void onAudioMixingFinished() {
            super.onAudioMixingFinished();
            notifyClients(IAVRoomCoreClient.class, IAVRoomCoreClient.METHOD_ON_AUDIO_MIXING_FINISHED);
        }
    }
}
