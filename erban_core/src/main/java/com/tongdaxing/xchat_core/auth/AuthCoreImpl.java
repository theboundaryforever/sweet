package com.tongdaxing.xchat_core.auth;

import android.text.TextUtils;

import com.growingio.android.sdk.collection.GrowingIO;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.tongdaxing.erban.libcommon.coremanager.AbstractBaseCore;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.VersionUtil;
import com.tongdaxing.erban.libcommon.utils.codec.DESUtils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.DemoCache;
import com.tongdaxing.xchat_core.R;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.linked.ILinkedCore;
import com.tongdaxing.xchat_core.linked.LinkedInfo;
import com.tongdaxing.xchat_core.liveroom.im.model.ReUsedSocketManager;
import com.tongdaxing.xchat_core.result.LoginResult;
import com.tongdaxing.xchat_core.result.RegisterResult;
import com.tongdaxing.xchat_core.result.TicketResult;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.HashMap;
import java.util.Map;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.wechat.friends.Wechat;

/**
 * Created by chenran on 2017/2/16.
 */

public class AuthCoreImpl extends AbstractBaseCore implements IAuthCore {
    private static final String TAG = "AuthCoreImpl";
    //    Timer timer = new Timer();
//    TimerTask task = new TimerTask() {
//        @Override
//        public void run() {
//            timer.schedule(task, 1000);
//        }
//    };
    private AccountInfo currentAccountInfo;
    private TicketInfo ticketInfo;
    private boolean isRequestTicket;
    private ThirdUserInfo thirdUserInfo;
    private Platform wechat;
    private Platform qq;

    public AuthCoreImpl() {
        currentAccountInfo = DemoCache.readCurrentAccountInfo();
        ticketInfo = DemoCache.readTicketInfo();
        if (currentAccountInfo == null) {
            currentAccountInfo = new AccountInfo();
        }
        if (ticketInfo == null) {
            ticketInfo = new TicketInfo();
        }

    }

    public boolean isRequestTicket() {
        return isRequestTicket;
    }

    public void setRequestTicket(boolean requestTicket) {
        isRequestTicket = requestTicket;
    }

    private String DESAndBase64(String psw) {
        String pwd = "";
        try {
            pwd = DESUtils.DESAndBase64Encrypt(psw);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pwd;
    }

    private void reset() {
        currentAccountInfo = new AccountInfo();
        ticketInfo = new TicketInfo();
        DemoCache.saveCurrentAccountInfo(new AccountInfo());
        DemoCache.saveTicketInfo(new TicketInfo());
    }

    @Override
    public boolean isLogin() {
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo == null || TextUtils.isEmpty(userInfo.getNick())) {
            return false;
        }
        if (currentAccountInfo == null || StringUtil.isEmpty(currentAccountInfo.getAccess_token()) || TextUtils.isEmpty(currentAccountInfo.getUid() + "")) {
            return false;
        }
        return currentAccountInfo.getAccess_token() != null && !isRequestTicket;
    }

    @Override
    public long getCurrentUid() {
        return currentAccountInfo == null ? 0 : currentAccountInfo.getUid();
    }

    @Override
    public String getTicket() {
        if (ticketInfo != null && ticketInfo.getTickets() != null && ticketInfo.getTickets().size() > 0) {
            return ticketInfo.getTickets().get(0).getTicket();
        }
        return "";
    }

    @Override
    public AccountInfo getCurrentAccount() {
        return currentAccountInfo;
    }

    @Override
    public ThirdUserInfo getThirdUserInfo() {
        return thirdUserInfo;
    }


    @Override
    public void setThirdUserInfo(ThirdUserInfo thirdUserInfo) {
        this.thirdUserInfo = thirdUserInfo;
    }

    @Override
    public void autoLogin() {
        if (!isLogin()) {
            notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_NEED_LOGIN);
            return;
        }

        isRequestTicket = true;
        LogUtil.d(TAG, "autoLogin-->getChannelConfigure isRequestTicket:" + isRequestTicket);
        CoreManager.getCore(VersionsCore.class).getChannelConfigure();
        requestTicket();
    }

    @Override
    public void login(final String account, String password) {
        //LogUtil.d(TAG, "login: account = " + account + " ---> password = " + password);
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("phone", account);
        params.put("version", VersionUtil.getLocalName(getContext()));
        params.put("client_id", "erban-client");
        params.put("username", account);
        params.put("password", DESAndBase64(password));
        params.put("grant_type", "password");
        params.put("client_secret", "uyzjdhds");

        OkHttpManager.getInstance().postRequest(UriProvider.getLoginResourceUrl(), params, new OkHttpManager.MyCallBack<LoginResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGIN_FAIL,
                        e.getMessage());
            }

            @Override
            public void onResponse(LoginResult response) {
                LogUtil.d(TAG, "login--->onResponse code = " + response.getCode());
                if (response.isSuccess()) {
                    currentAccountInfo = response.getData();
                    DemoCache.saveCurrentAccountInfo(currentAccountInfo);
                    CoreManager.getCore(VersionsCore.class).getChannelConfigure();
                    requestTicket();
                } else {
                    reset();
                    notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGIN_FAIL, response.getMessage());
                }
            }
        });
    }

    @Override
    public void requestTicket() {
        LogUtil.d(TAG, "requestTicket: access_token = " + currentAccountInfo.getAccess_token());
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("issue_type", "multi");
        params.put("access_token", currentAccountInfo.getAccess_token());

        OkHttpManager.getInstance().getRequest(UriProvider.getAuthTicket(), params, new OkHttpManager.MyCallBack<TicketResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                LogUtil.d(TAG, "requestTicket --->onErrorResponse e.Message:" + e.getMessage());
                isRequestTicket = false;
                reset();
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGOUT);
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_REQUEST_TICKET_FAIL,
                        e.getMessage());
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGIN_FAIL,
                        e.getMessage());
                GrowingIO.getInstance().clearUserId();
            }

            @Override
            public void onResponse(TicketResult response) {
                isRequestTicket = false;
                LogUtil.d(TAG, "requestTicket-->onResponse code = " + response.getCode());
                if (response.isSuccess()) {
                    ticketInfo = response.getData();
                    DemoCache.saveTicketInfo(ticketInfo);
                    ReUsedSocketManager.get().initIM();
                    notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGIN, currentAccountInfo);
                    GrowingIO.getInstance().setUserId(String.valueOf(currentAccountInfo.getUid()));
//                    CoreManager.getCore(IBlackListCore.class).requestBLackList(null);
                    //获取到用户信息之后加载用户抽奖礼物
                    LogUtil.d(TAG, "requestTicket-->requestGiftInfos");
                    CoreManager.getCore(IGiftCore.class).requestGiftInfos();
                } else {
                    reset();
                    notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGOUT);
                    notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_REQUEST_TICKET_FAIL, response.getMessage());
                    notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGIN_FAIL, response.getMessage());
                    GrowingIO.getInstance().clearUserId();
                }
            }
        });
    }

    @Override
    public void requestSMSCode(String phone, int type) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("phone", phone);
        params.put("type", String.valueOf(type));

        OkHttpManager.getInstance().getRequest(UriProvider.getSMSCode(), params, new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_SMS_FAIL, e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_SMS_SUCCESS);
                    } else {
                        notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_SMS_FAIL, response.getMessage());
                    }
                }
            }
        });
    }

    @Override
    public void requestResetPsw(String phone, String sms_code, String newPsw) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("phone", phone);
        params.put("smsCode", sms_code);
        params.put("newPwd", DESAndBase64(newPsw));

        OkHttpManager.getInstance().postRequest(UriProvider.modifyPsw(), params, new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_MODIFY_PSW_FAIL, e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_MODIFY_PSW);
                    } else {
                        notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_MODIFY_PSW_FAIL, response.getMessage());
                        System.out.println("e1" + response.getErrorMessage());
                    }
                }
            }
        });
    }


    @Override
    public void wxLogin() {
        LogUtil.d(TAG, "wxLogin--->");
        wechat = ShareSDK.getPlatform(Wechat.NAME);
        if (!wechat.isClientValid()) {
            LogUtil.d(TAG, "wxLogin: error 未安装微信");
            notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGIN_FAIL, "未安装微信");
            return;
        }
        if (wechat.isAuthValid()) {
            wechat.removeAccount(true);
        }

        wechat.setPlatformActionListener(new PlatformActionListener() {
            @Override
            public void onComplete(final Platform platform, int i, HashMap<String, Object> hashMap) {
                LogUtil.d(TAG, "wxLogin--->onComplete");
                if (i == Platform.ACTION_USER_INFOR) {
                    String openid = platform.getDb().getUserId();
                    String unionid = platform.getDb().get("unionid");
                    thirdUserInfo = new ThirdUserInfo();
                    thirdUserInfo.setUserName(platform.getDb().getUserName());
                    thirdUserInfo.setUserGender(platform.getDb().getUserGender());
                    thirdUserInfo.setUserIcon(platform.getDb().getUserIcon());
                    System.out.println("openid" + openid + "unionid" + unionid + platform.getDb().getUserIcon());
                    ThirdLogin(openid, unionid, 1, platform.getDb().getToken());
                }
            }

            @Override
            public void onError(Platform platform, int i, Throwable throwable) {
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGIN_FAIL, "登录失败: ".concat(String.valueOf(i)).concat("->>").concat(throwable != null ? throwable.getMessage() : ""));
                LogUtil.d(TAG, "wxLogin--->onError throwable = " + String.valueOf(i).concat("->>").concat(throwable != null ? throwable.getMessage() : ""));
            }

            @Override
            public void onCancel(Platform platform, int i) {
                LogUtil.d(TAG, "wxLogin--->onCancel 第三方登录取消");
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGIN_FAIL, "第三方登录取消");
            }
        });
        wechat.SSOSetting(false);
        wechat.showUser(null);
    }

    @Override
    public void ThirdLogin(String openid, String unionid, int type, String accessToken) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("openid", openid);
        params.put("unionid", unionid);
        params.put("type", String.valueOf(type));
        params.put("access_token", accessToken);
        LinkedInfo linkedInfo = CoreManager.getCore(ILinkedCore.class).getLinkedInfo();
        if (linkedInfo != null && !StringUtil.isEmpty(linkedInfo.getChannel())) {
            params.put("linkedmeChannel", linkedInfo.getChannel());
        }
        LogUtil.d(TAG, "ThirdLogin:  openid = " + openid + " ---> unionid = " + unionid + " ---> type = " + type + " ---> access_token = " + accessToken + " ---> linkedmeChannel = " + (linkedInfo != null && !StringUtil.isEmpty(linkedInfo.getChannel()) ? linkedInfo.getChannel() : ""));

        OkHttpManager.getInstance().postRequest(UriProvider.requestWXLogin(), params, new OkHttpManager.MyCallBack<LoginResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                LogUtil.d(TAG, "ThirdLogin: onErrorResponse e.message = " + e.getMessage());
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGIN_FAIL, e.getMessage());
            }

            @Override
            public void onResponse(LoginResult response) {
                LogUtil.d(TAG, "ThirdLogin: onResponse code = " + response.getCode());
                if (response.isSuccess()) {
                    currentAccountInfo = response.getData();
                    DemoCache.saveCurrentAccountInfo(currentAccountInfo);
                    CoreManager.getCore(VersionsCore.class).getChannelConfigure();
                    requestTicket();
                } else {
                    notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGIN_FAIL, response.getCode() + "错误," + response.getMessage());
                    logout();
                }
            }
        });
    }

    @Override
    public void qqLogin() {
        LogUtil.d(TAG, "qqLogin--->");
        qq = ShareSDK.getPlatform(QQ.NAME);
        if (qq.isAuthValid()) {
            qq.removeAccount(true);
        }
        qq.setPlatformActionListener(new PlatformActionListener() {
            @Override
            public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {
                LogUtil.d(TAG, "qqLogin--->onComplete i=" + i);
                if (i == Platform.ACTION_USER_INFOR) {
                    String openid = platform.getDb().getUserId();
                    String unionid = platform.getDb().get("unionid");
                    thirdUserInfo = new ThirdUserInfo();
                    thirdUserInfo.setUserName(platform.getDb().getUserName());
                    thirdUserInfo.setUserGender(platform.getDb().getUserGender());
                    thirdUserInfo.setUserIcon(platform.getDb().getUserIcon());
                    LogUtil.d(TAG, "qqLogin--->onComplete openid:" + openid
                            + " unionid:" + unionid + platform.getDb().getUserIcon());
                    ThirdLogin(openid, unionid, 2, platform.getDb().getToken());
                }
            }

            @Override
            public void onError(Platform platform, int i, Throwable throwable) {
                LogUtil.d(TAG, "qqLogin--->onError throwable = " +
                        String.valueOf(i).concat("->>")
                                .concat(throwable != null && null != throwable.getMessage() ? throwable.getMessage() : ""));
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGIN_FAIL, "第三方登录取消");
            }

            @Override
            public void onCancel(Platform platform, int i) {
                LogUtil.d(TAG, "qqLogin--->onCancel 第三方登录取消");
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGIN_FAIL, "第三方登录取消");
            }
        });
        qq.SSOSetting(false);
        qq.showUser(null);
    }

    @Override
    public void isPhone(long uid) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));

        OkHttpManager.getInstance().getRequest(UriProvider.isPhones(), params, new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_IS_PHONE_FAIL, e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_IS_PHONE);
                    } else {
                        notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_IS_PHONE_FAIL, response.getMessage());
                    }
                }
            }
        });
    }

    @Override
    public void ModifyBinderPhone(long uid, String phone, String code, String url) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("phone", phone);
        params.put("smsCode", code);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());

        OkHttpManager.getInstance().postRequest(url, params, new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IAuthClient.class, IAuthClient.METHOD_MOIDFY_ON_BINDER_FAIL, e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        notifyClients(IAuthClient.class, IAuthClient.METHOD_MOIDFY_ON_BINDER);
                        IUserCore core = CoreManager.getCore(IUserCore.class);
                        if (core != null && core.getCacheLoginUserInfo() != null) {
                            core.requestUserInfo(core.getCacheLoginUserInfo().getUid());
                        }
                    } else {
                        notifyClients(IAuthClient.class, IAuthClient.METHOD_MOIDFY_ON_BINDER_FAIL, response.getMessage());
                    }
                }
            }
        });
    }

    @Override
    public void BinderPhone(long uid, String phone, final String code) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("phone", phone);
        params.put("code", code);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());

        OkHttpManager.getInstance().postRequest(UriProvider.binderPhone(), params, new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_BINDER_FAIL, e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_BINDER);
                        IUserCore core = CoreManager.getCore(IUserCore.class);
                        if (core != null && core.getCacheLoginUserInfo() != null) {
                            core.requestUserInfo(core.getCacheLoginUserInfo().getUid());
                        }
                    } else {
                        notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_BINDER_FAIL, response.getMessage());
                    }
                }
            }
        });
    }


    @Override
    public void getSMSCode(String phone) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("phone", phone);

        OkHttpManager.getInstance().getRequest(UriProvider.getSmS(), params, new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_GET_SMS_CODE_FAIL,
                        BasicConfig.INSTANCE.getAppContext().getResources().getString(R.string.bind_phone_failed));
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                    } else {
                        notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_GET_SMS_CODE_FAIL, response.getMessage() + "");
                    }
                }
            }
        });
    }


    @Override
    public void getModifyPhoneSMSCode(String phone, String type) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("phone", phone);
        params.put("type", type);

        OkHttpManager.getInstance().getRequest(UriProvider.getModifyPhoneSMS(), params, new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_GET_SMS_CODE_FAIL,
                        BasicConfig.INSTANCE.getAppContext().getResources().getString(R.string.replace_phone_failed));
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                    } else {
                        notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_GET_SMS_CODE_FAIL, response.getMessage() + "");
                    }
                }
            }
        });
    }


    @Override
    public void logout() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("access_token", currentAccountInfo.getAccess_token());
        OkHttpManager.getInstance().postRequest(UriProvider.logout(), params, new OkHttpManager.MyCallBack<RegisterResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                //添加退出登录时的接口调用逻辑是为了确保能够正确统计用户在线状态，那么接口调用失败的情况下就不应该走后续退出流程
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGOUT_FAITH, e.getMessage());
            }

            @Override
            public void onResponse(RegisterResult response) {
                if (null != response && response.isSuccess()) {
                    reset();
                    notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGOUT);
                    GrowingIO.getInstance().clearUserId();
                } else {
                    //添加退出登录时的接口调用逻辑是为了确保能够正确统计用户在线状态，那么接口调用失败的情况下就不应该走后续退出流程
                    notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_LOGOUT_FAITH, null == response ? null : response.getMessage());
                }
            }
        });
    }

    @Override
    public void register(String phone, String sms_code, String password) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("phone", phone);
        params.put("smsCode", sms_code);
        params.put("password", DESAndBase64(password));
        params.put("os", "android");
        LinkedInfo linkedInfo = CoreManager.getCore(ILinkedCore.class).getLinkedInfo();
        if (linkedInfo != null && !StringUtil.isEmpty(linkedInfo.getChannel())) {
            params.put("linkedmeChannel", linkedInfo.getChannel());
        }

        OkHttpManager.getInstance().postRequest(UriProvider.getRegisterResourceUrl(), params, new OkHttpManager.MyCallBack<RegisterResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_REGISTER_FAIL, e.getMessage());
            }

            @Override
            public void onResponse(RegisterResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_REGISTER);
                    } else {
                        notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_REGISTER_FAIL, response.getMessage());
                    }
                }
            }
        });
    }

}
