package com.tongdaxing.xchat_core.auth;

import android.content.Context;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.coremanager.IUserInfoCore;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

public class RealNameAuthStatusChecker {

    private final String TAG = RealNameAuthStatusChecker.class.getSimpleName();

    private RealNameAuthStatusChecker(){

    }

    private static RealNameAuthStatusChecker instance = null;

    public static RealNameAuthStatusChecker getInstance() {
        if(null == instance){
            instance = new RealNameAuthStatusChecker();
        }
        return instance;
    }

    /**
     * 校验是否需要进行实名认证操作
     * @param context
     * @param msg
     * @return true 已实名认证， false 需要实名认证
     */
    public boolean authStatus(Context context, String msg){
        boolean result = false;
        //通过后台开关进行控制
//        if(!CoreManager.getCore(VersionsCore.class).needAuthBeforeUse()){
//            result = true;
//        }else{
            UserInfo cacheLoginUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            result = null != cacheLoginUserInfo && cacheLoginUserInfo.isRealNameAudit();
            LogUtils.d(TAG,"authStatus-result:"+result);
            if(!result){
                //MainActivity回调弹出弹框
                CoreManager.notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_NEED_REAL_NAME_AUTH, context,msg);
            }
//        }

        return result;
    }

    /**
     * 校验手机帐号是否需要绑定
     * @param context
     * @param msg
     * @return true，需要绑定，false不需要绑定
     */
    public boolean phoneStatus(Context context, String msg){
        boolean result = false;
        //通过后台开关进行控制
        if(!CoreManager.getCore(VersionsCore.class).needAuthBeforeUse()){
            result = false;
        }else{
            //由判断实名认证改为判断是否绑定了手机号码，否则提示需要绑定手机号
            IUserInfoCore core = CoreManager.getCore(IUserInfoCore.class);
            //如果没有绑定手机号，phone字段为erbanNo，checkHasBandPhone方法返回false
            result = null != core && !core.checkHasBandPhone();
            if(result){
                //MainActivity回调弹出弹框
                CoreManager.notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_NEED_BIND_PHONE,
                        context,msg);
            }
        }
        return result;
    }

    /**
     * 是否需要先绑定手机才能用
     * @param context
     * @param msg
     * @return false 未绑定手机且未实名认证，需要先绑定手机 ，true 已经绑定手机或者进行了实名认证，无需绑定手机
     */
    public boolean needBindPhoneFirst(Context context, String msg){
        boolean result = false;
        //通过后台开关进行控制
        if(!CoreManager.getCore(VersionsCore.class).needAuthBeforeUse()){
            result = true;
        }else{
            IUserInfoCore core = CoreManager.getCore(IUserInfoCore.class);
            //如果没有绑定手机号，phone字段为erbanNo，checkHasBandPhone方法返回false
            result = null != core && !core.checkHasBandPhone();//result=true代表未绑定手机,false代表绑定了手机
            UserInfo cacheLoginUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            result = !result || (null != cacheLoginUserInfo && cacheLoginUserInfo.isRealNameAudit());
            if(!result){
                //MainActivity回调弹出弹框
                CoreManager.notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_NEED_BIND_PHONE,
                        context,msg);
            }
        }
        return result;
    }

    //该功能需要进行实名认证
    public static final int RESULT_FAILED_NEED_REAL_NAME_AUTH = 2202;
}
