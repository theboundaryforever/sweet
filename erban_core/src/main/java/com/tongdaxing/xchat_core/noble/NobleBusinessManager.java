package com.tongdaxing.xchat_core.noble;

import android.text.TextUtils;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tongdaxing.erban.libcommon.utils.JavaUtil;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.R;
import com.tongdaxing.xchat_core.UriProvider;

import java.util.Map;

public class NobleBusinessManager {

    public static String getNobleRoomNick(int nobleLevel, boolean isInvisible, int nobleDate, String nick) {
        //1-7分别表示骑士，男爵，子爵，伯爵，侯爵，公爵，国王
        //0.进房，右上角入场动画，及公屏消息 --  右上角可以，但是公屏，用的是云信的，需要后端同步修改云信的信息
        boolean isMysteryMan = nobleLevel >= 6 && isInvisible && nobleDate > 0;
        return isMysteryMan ? BasicConfig.INSTANCE.getAppContext().getResources().getString(
                R.string.king_invisiable_nick) : nick;
    }

    public static String getNobleRoomAvatarUrl(int nobleLevel, boolean isInvisible, int nobleDate, String avatarUrl) {
        //1-7分别表示骑士，男爵，子爵，伯爵，侯爵，公爵，国王
        //0.进房，右上角入场动画，及公屏消息 --  右上角可以，但是公屏，用的是云信的，需要后端同步修改云信的信息
        boolean isMysteryMan = nobleLevel >= 6 && isInvisible && nobleDate > 0;
        return isMysteryMan ? UriProvider.getNobleMysteriousAvatarUrl() : avatarUrl;
    }

    public static long getNobleRoomUid(int nobleLevel, boolean isInvisible, int nobleDate, long uid, long invisiableUid) {
        boolean isMysteryMan = nobleLevel >= 6 && isInvisible && nobleDate > 0 && invisiableUid > 0L;
        return isMysteryMan ? invisiableUid : uid;
    }

    public static long getNobleRoomUID(ChatRoomMember chatRoomMember, String account) {
        if (null == chatRoomMember) {
            return TextUtils.isEmpty(account) ? 0L : JavaUtil.str2int(account);
        }
        Map<String, Object> extenMap = chatRoomMember.getExtension();
        boolean isInvisiable = false;
        int medalId = 0;
        int medalDate = 0;
        long inVisiableUid = 0L;
        if (null != extenMap) {
            if (extenMap.containsKey(Constants.USER_MEDAL_ID)) {
                medalId = (int) extenMap.get(Constants.USER_MEDAL_ID);
            }
            if (extenMap.containsKey(Constants.USER_MEDAL_DATE)) {
                medalDate = (int) extenMap.get(Constants.USER_MEDAL_DATE);
            }
            if (extenMap.containsKey(Constants.NOBLE_INVISIABLE_ENTER_ROOM)) {
                isInvisiable = (int) extenMap.get(Constants.NOBLE_INVISIABLE_ENTER_ROOM) == 1;
            }
            if (extenMap.containsKey(Constants.NOBLE_INVISIABLE_UID)) {
                inVisiableUid = (long) extenMap.get(Constants.NOBLE_INVISIABLE_UID);
            }
        }
        return NobleBusinessManager.getNobleRoomUid(medalId, isInvisiable, medalDate,
                TextUtils.isEmpty(chatRoomMember.getAccount()) ? 0L : Long.valueOf(chatRoomMember.getAccount()), inVisiableUid);
    }
}
