package com.tongdaxing.xchat_core.gift;

import com.tongdaxing.erban.libcommon.coremanager.ICoreClient;

/**
 * Created by chenran on 2017/7/27.
 */

public interface IGiftCoreClient extends ICoreClient {

    String METHOD_ON_RECIEVE_GIFT_MSG = "onRecieveGiftMsg";

    void onRecieveGiftMsg(GiftReceiveInfo giftRecieveInfo);

    String METHOD_ON_MULTI_GIFT_MSG = "onRecieveMultiGiftMsg";

    void onRecieveMultiGiftMsg(MultiGiftReceiveInfo multiGiftRecieveInfo);

    String METHOD_ON_RECEIVE_PERSONAL_GIFT = "onRecievePersonalGift";

    void onRecievePersonalGift(GiftReceiveInfo giftRecieveInfo);

    String METHOD_ON_SEND_PERSONAL_GIFT_FAIL = "onSendPersonalGiftFail";

    void onSendPersonalGiftFail(int code, String message);

    String METHOD_ON_GIFT_PAST_DUE = "onGiftPastDue";

    void onGiftPastDue();

    String METHOD_ON_SUPER_GIFT_MSG = "onSuperGiftMsg";

    void onSuperGiftMsg(GiftReceiveInfo giftRecieveInfo);

    String refreshFreeGift = "refreshFreeGift";

    void refreshFreeGift(int giftNum);

    String onGiftSendErrorMsg = "onGiftSendErrorMsg";
    void onGiftSendErrorMsg(String errorMsg);


    String METHOD_ON_SEND_GIFT_FAIL_AUTH = "onSendPersonalGiftFailAuth";
    void onSendPersonalGiftFailAuth(String msg);
}
