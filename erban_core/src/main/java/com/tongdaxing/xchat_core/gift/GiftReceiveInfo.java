package com.tongdaxing.xchat_core.gift;

import com.tongdaxing.xchat_core.room.bean.RoomInfo;

import java.io.Serializable;
import java.util.List;

/**
 * Created by chenran on 2017/7/28.
 */

public class GiftReceiveInfo implements Serializable {

    private long uid;

    private String nick;
    private String avatar;
    private String vipMedal = null;
    private String vipName = null;
    private String vipIcon = null;
    private int vipId;
    private int vipDate;
    private boolean isInvisible = false;

    private int giftId;
    private int realGiftId;
    private int giftNum;

    private long targetUid;
    private String targetNick;
    private String targetAvatar;
    private int targetVipId;
    private int targetVipDate;
    private boolean targetIsInvisible = false;

    private int isPea;//是否甜豆礼物 0：不是 1：是
    private int usePeaNum;

    private String title;//房间名称
    private String giftUrl;
    private boolean hasVggPic;
    private String vggUrl;
    private String giftName;
    private long goldPrice;

    //1-包裹，2-普通，3-宝箱，4-专属，
    // 但是这里的包裹类型是服务器数据层对应的包裹类型，客户端应当严格按照数量来算包裹礼物列表并展示给用户，
    // 因为有可能其他服务器类型的礼物也跑到客户端的包裹tab下去
    private int giftType = GiftType.Unknow.ordinal();
    private boolean isAllMicSend = false;
    /**
     * 礼物连击 为0 则非连击 不为0 则为每组连击的标记（时间戳）
     */
    private long comboId = 0L;
    /**
     * 连击范围，左界限
     */
    private int comboRangStart = 0;
    /**
     * 连击范围，右界限
     */
    private int comboRangEnd = 1;

    public int getGiftType() {
        return giftType;
    }

    public void setGiftType(int giftType) {
        this.giftType = giftType;
    }

    public boolean isAllMicSend() {
        return isAllMicSend;
    }

    public void setAllMicSend(boolean allMicSend) {
        isAllMicSend = allMicSend;
    }

    public long getComboId() {
        return comboId;
    }

    public void setComboId(long comboId) {
        this.comboId = comboId;
    }

    public int getComboRangStart() {
        return comboRangStart;
    }

    public void setComboRangStart(int comboRangStart) {
        this.comboRangStart = comboRangStart;
    }

    public int getComboRangEnd() {
        return comboRangEnd;
    }

    public void setComboRangEnd(int comboRangEnd) {
        this.comboRangEnd = comboRangEnd;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getUsePeaNum() {
        return usePeaNum;
    }

    public void setUsePeaNum(int usePeaNum) {
        this.usePeaNum = usePeaNum;
    }

    public int getIsPea() {
        return isPea;
    }

    public void setIsPea(int isPea) {
        this.isPea = isPea;
    }

    public int getTargetVipId() {
        return targetVipId;
    }

    public void setTargetVipId(int targetVipId) {
        this.targetVipId = targetVipId;
    }

    public int getTargetVipDate() {
        return targetVipDate;
    }

    public void setTargetVipDate(int targetVipDate) {
        this.targetVipDate = targetVipDate;
    }

    public boolean isTargetIsInvisible() {
        return targetIsInvisible;
    }

    public void setTargetIsInvisible(boolean targetIsInvisible) {
        this.targetIsInvisible = targetIsInvisible;
    }

    private int personCount;
    private List<Integer> roomIdList;
    private String roomId;

    private int roomType = RoomInfo.ROOMTYPE_HOME_PARTY;

    public int getRoomType() {
        return roomType;
    }

    public void setRoomType(int roomType) {
        this.roomType = roomType;
    }

    private String userNo;
    private int userGiftPurseNum;
    private int useGiftPurseGold;

    public int getRealGiftId() {
        return realGiftId;
    }

    public void setRealGiftId(int realGiftId) {
        this.realGiftId = realGiftId;
    }

    public int getUseGiftPurseGold() {
        return useGiftPurseGold;
    }

    public void setUseGiftPurseGold(int useGiftPurseGold) {
        this.useGiftPurseGold = useGiftPurseGold;
    }

    public int getUserGiftPurseNum() {
        return userGiftPurseNum;
    }

    public void setUserGiftPurseNum(int userGiftPurseNum) {
        this.userGiftPurseNum = userGiftPurseNum;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public List<Integer> getRoomIdList() {
        return roomIdList;
    }

    public void setRoomIdList(List<Integer> roomIdList) {
        this.roomIdList = roomIdList;
    }

    public int getPersonCount() {
        return personCount;
    }

    public void setPersonCount(int personCount) {
        this.personCount = personCount;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public long getTargetUid() {
        return targetUid;
    }

    public void setTargetUid(long targetUid) {
        this.targetUid = targetUid;
    }

    public int getGiftId() {
        return giftId;
    }

    public void setGiftId(int giftId) {
        this.giftId = giftId;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getGiftNum() {
        return giftNum;
    }

    public void setGiftNum(int giftNum) {
        this.giftNum = giftNum;
    }

    public String getTargetNick() {
        return targetNick;
    }

    public void setTargetNick(String targetNick) {
        this.targetNick = targetNick;
    }

    public String getTargetAvatar() {
        return targetAvatar;
    }

    public void setTargetAvatar(String targetAvatar) {
        this.targetAvatar = targetAvatar;
    }

    public int getVipId() {
        return vipId;
    }

    public void setVipId(int vipId) {
        this.vipId = vipId;
    }

    public int getVipDate() {
        return vipDate;
    }

    public void setVipDate(int vipDate) {
        this.vipDate = vipDate;
    }

    public boolean isInvisible() {
        return isInvisible;
    }

    public void setInvisible(boolean invisible) {
        isInvisible = invisible;
    }

    public String getVipName() {
        return vipName;
    }

    public void setVipName(String vipName) {
        this.vipName = vipName;
    }

    public String getVipIcon() {
        return vipIcon;
    }

    public void setVipIcon(String vipIcon) {
        this.vipIcon = vipIcon;
    }

    public String getVipMedal() {
        return vipMedal;
    }

    public void setVipMedal(String vipMedal) {
        this.vipMedal = vipMedal;
    }

    public String getGiftUrl() {
        return giftUrl;
    }

    public void setGiftUrl(String giftUrl) {
        this.giftUrl = giftUrl;
    }

    public boolean isHasVggPic() {
        return hasVggPic;
    }

    public void setHasVggPic(boolean hasVggPic) {
        this.hasVggPic = hasVggPic;
    }

    public String getVggUrl() {
        return vggUrl;
    }

    public void setVggUrl(String vggUrl) {
        this.vggUrl = vggUrl;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public long getGoldPrice() {
        return goldPrice;
    }

    public void setGoldPrice(long goldPrice) {
        this.goldPrice = goldPrice;
    }

    @Override
    public String toString() {
        return "GiftReceiveInfo{" +
                "uid=" + uid +
                ", nick='" + nick + '\'' +
                ", avatar='" + avatar + '\'' +
                ", vipMedal='" + vipMedal + '\'' +
                ", vipName='" + vipName + '\'' +
                ", vipIcon='" + vipIcon + '\'' +
                ", vipId=" + vipId +
                ", vipDate=" + vipDate +
                ", isInvisible=" + isInvisible +
                ", giftId=" + giftId +
                ", realGiftId=" + realGiftId +
                ", giftNum=" + giftNum +
                ", targetUid=" + targetUid +
                ", targetNick='" + targetNick + '\'' +
                ", targetAvatar='" + targetAvatar + '\'' +
                ", targetVipId=" + targetVipId +
                ", targetVipDate=" + targetVipDate +
                ", targetIsInvisible=" + targetIsInvisible +
                ", personCount=" + personCount +
                ", roomIdList=" + roomIdList +
                ", roomId='" + roomId + '\'' +
                ", roomType='" + roomType + '\'' +
                ", userNo='" + userNo + '\'' +
                ", giftType='" + giftType + '\'' +
                ", userGiftPurseNum=" + userGiftPurseNum +
                ", useGiftPurseGold=" + useGiftPurseGold +
                '}';
    }
}
