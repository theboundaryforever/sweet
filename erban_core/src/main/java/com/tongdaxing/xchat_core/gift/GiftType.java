package com.tongdaxing.xchat_core.gift;

/**
 * 礼物类型
 *
 * 礼物giftType: 1.包裹 2.普通 3.宝箱 4.专属
 */
public enum GiftType {
    /**
     * 未知
     */
    Unknow,
    /**
     * 包裹 客户端礼物列表 包裹tab展示的是所有类型&数量大于0的礼物数据 同服务器返回的礼物类型数据有所区别
     */
    Package,
    /**
     * 普通
     */
    Normal,
    /**
     * 宝箱
     */
    Box,
    /**
     * 专属
     */
    Exclusive,

    /**
     * 挚友礼物
     */
    BosomFriend
}
