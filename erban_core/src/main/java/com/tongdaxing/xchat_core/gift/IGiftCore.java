package com.tongdaxing.xchat_core.gift;

import com.netease.nim.uikit.session.module.Container;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.tongdaxing.erban.libcommon.coremanager.IBaseCore;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMessage;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by chenran on 2017/7/27.
 */

public interface IGiftCore extends IBaseCore {

    GiftInfo getGiftInfoByGiftId(int giftId);

    List<GiftInfo> getGiftInfosByType(GiftType type,boolean isRoomToShowLimitedExempteGift);

    List<GiftInfo> getGiftInfosByTypeIMRoom(GiftType type, boolean isRoomToShowLimitedExempteGift);

    void sendLotteryMeg(GiftInfo giftInfo, int count);

    void sendRoomGift(int giftId, long targetUid, long roomUid, int giftNum, int goldPrice);

    void sendRoomMultiGift(int giftId, List<Long> targetUids, long roomUid, int giftNum, int goldPrice);

    /**
     * 云信发P2P礼物专用
     */
    void sendPersonalGiftToNIM(int giftId, long targetUid, int giftNum, int goldPrice, WeakReference<Container> container, ChargeListener chargeListener);

    GiftInfo findGiftInfoById(int giftId);

    void requestGiftInfos();

    void onReceiveChatRoomMessages(List<ChatRoomMessage> chatRoomMessageList);

    List<GiftInfo> getLastRequestGiftInfos();

    //--------------IM服务器接收礼物消息-----------------------

    void onReceiveIMRoomMessages(List<IMRoomMessage> chatRoomMessageList);

}
