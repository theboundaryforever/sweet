package com.tongdaxing.xchat_core.gift;

import java.io.Serializable;

/**
 * Created by chenran on 2017/7/27.
 */

public class GiftInfo extends GridPageListItemBean implements Serializable {
    @Override
    public String toString() {
        return "GiftInfo{" +
                "giftId=" + giftId +
                ", giftType=" + giftType +
                ", giftName='" + giftName + '\'' +
                ", goldPrice=" + goldPrice +
                ", giftUrl='" + giftUrl + '\'' +
                ", seqNo=" + seqNo +
                ", hasGifPic=" + hasGifPic +
                ", gifUrl='" + gifUrl + '\'' +
                ", gifFile='" + gifFile + '\'' +
                ", hasVggPic=" + hasVggPic +
                ", vggUrl='" + vggUrl + '\'' +
                ", hasLatest=" + hasLatest +
                ", hasTimeLimit=" + hasTimeLimit +
                ", hasEffect=" + hasEffect +
                ", userGiftPurseNum=" + userGiftPurseNum +
                ", isNobleGift=" + isNobleGift +
                '}';
    }

    private int giftId;
    //1-包裹，2-普通，3-宝箱，4-专属，
    // 但是这里的包裹类型是服务器数据层对应的包裹类型，客户端应当严格按照数量来算包裹礼物列表并展示给用户，
    // 因为有可能其他服务器类型的礼物也跑到客户端的包裹tab下去
    private int giftType;
    private String giftName;
    private int goldPrice;
    private String giftUrl;
    private int seqNo;
    private boolean hasGifPic;
    private String gifUrl;
    private String gifFile;
    private boolean hasVggPic;
    private String vggUrl;
    private boolean hasLatest;//是否是最新礼物
    private boolean hasTimeLimit;//是否是限时礼物
    private boolean hasEffect;//是否有特效
    private int userGiftPurseNum;//抽奖礼物数量

    private int isPea;//是否甜豆礼物 0:不是，1：是

    private int isActivity;//是否是活动掉落
    private int isSale;//是否出售


    public int getIsActivity() {
        return isActivity;
    }

    public void setIsActivity(int isActivity) {
        this.isActivity = isActivity;
    }

    public int getIsSale() {
        return isSale;
    }

    public void setIsSale(int isSale) {
        this.isSale = isSale;
    }

    public int getIsPea() {
        return isPea;
    }

    public void setIsPea(int isPea) {
        this.isPea = isPea;
    }

    public int getUserGiftPurseNum() {
        return userGiftPurseNum;
    }

    public void setUserGiftPurseNum(int userGiftPurseNum) {
        this.userGiftPurseNum = userGiftPurseNum;
    }

    /**
     * 是否是贵族礼物
     */
    private boolean isNobleGift;
    /**
     * 是否是专属礼物
     */
    private boolean isExclusive;

    public boolean isExclusive() {
        return isExclusive;
    }

    public void setExclusive(boolean exclusive) {
        isExclusive = exclusive;
    }

    private String nobleIcon;//贵族礼物图标,
    private int nobleId;//贵族等级ID
    private String nobleName;////贵族名,

    public String getNobleIcon() {
        return nobleIcon;
    }

    public void setNobleIcon(String nobleIcon) {
        this.nobleIcon = nobleIcon;
    }

    public int getNobleId() {
        return nobleId;
    }

    public void setNobleId(int nobleId) {
        this.nobleId = nobleId;
    }

    public String getNobleName() {
        return nobleName;
    }

    public void setNobleName(String nobleName) {
        this.nobleName = nobleName;
    }

    public int getGiftId() {
        return giftId;
    }

    public void setGiftId(int giftId) {
        this.giftId = giftId;
    }

    public int getGiftType() {
        return giftType;
    }

    public void setGiftType(int giftType) {
        this.giftType = giftType;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public int getGoldPrice() {
        return goldPrice;
    }

    public void setGoldPrice(int goldPrice) {
        this.goldPrice = goldPrice;
    }

    public String getGiftUrl() {
        return giftUrl;
    }

    public void setGiftUrl(String giftUrl) {
        this.giftUrl = giftUrl;
    }

    public int getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    public boolean isHasGifPic() {
        return hasGifPic;
    }

    public void setHasGifPic(boolean hasGifPic) {
        this.hasGifPic = hasGifPic;
    }

    public String getGifUrl() {
        return gifUrl;
    }

    public void setGifUrl(String gifUrl) {
        this.gifUrl = gifUrl;
    }

    public String getGifFile() {
        return gifFile;
    }

    public void setGifFile(String gifFile) {
        this.gifFile = gifFile;
    }

    public boolean isHasVggPic() {
        return hasVggPic;
    }

    public void setHasVggPic(boolean hasVggPic) {
        this.hasVggPic = hasVggPic;
    }

    public String getVggUrl() {
        return vggUrl;
    }

    public void setVggUrl(String vggUrl) {
        this.vggUrl = vggUrl;
    }

    public boolean isHasLatest() {
        return hasLatest;
    }

    public void setHasLatest(boolean hasLatest) {
        this.hasLatest = hasLatest;
    }

    public boolean isHasTimeLimit() {
        return hasTimeLimit;
    }

    public void setHasTimeLimit(boolean hasTimeLimit) {
        this.hasTimeLimit = hasTimeLimit;
    }

    public boolean isHasEffect() {
        return hasEffect;
    }

    public void setHasEffect(boolean hasEffect) {
        this.hasEffect = hasEffect;
    }

    //isNobleGift的逻辑已经废弃 因为后台已经将该类型礼物更改为 gifttype=1包裹 了，也就是一律通过gifttype来过滤礼物类型了
    //isNobleGift是用于表示为特殊礼物，有单独逻辑来判断是否隐藏
    public boolean isNobleGift() {
        return isNobleGift;
    }

    public void setNobleGift(boolean nobleGift) {
        isNobleGift = nobleGift;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof GiftInfo)) {
            return false;
        }
        boolean isSameGift = false;
        GiftInfo other = (GiftInfo) obj;
        if (other.getGiftId() == this.getGiftId()) {
            isSameGift = true;
        }
        return isSameGift;
    }
}
