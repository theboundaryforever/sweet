package com.tongdaxing.xchat_core.gift;

import java.io.Serializable;

/**
 * @author weihaitao
 * @date 2019/8/27
 */
public class ComboRang implements Serializable {

    protected int comboRangStart;
    protected int comboRangEnd;

    public ComboRang(int start, int end) {
        comboRangStart = start;
        comboRangEnd = end;
    }

    public int getComboRangStart() {
        return comboRangStart;
    }

    public void setComboRangStart(int comboRangStart) {
        this.comboRangStart = comboRangStart;
    }

    public int getComboRangEnd() {
        return comboRangEnd;
    }

    public void setComboRangEnd(int comboRangEnd) {
        this.comboRangEnd = comboRangEnd;
    }
}
