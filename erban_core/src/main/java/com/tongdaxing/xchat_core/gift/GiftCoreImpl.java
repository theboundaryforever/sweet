package com.tongdaxing.xchat_core.gift;

import android.os.Handler;
import android.os.Message;

import com.netease.nim.uikit.session.module.Container;
import com.netease.nimlib.sdk.chatroom.ChatRoomMessageBuilder;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.msg.MessageBuilder;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.CustomMessageConfig;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.tongdaxing.erban.libcommon.coremanager.AbstractBaseCore;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.im.IMReportRoute;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.DemoCache;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.auth.RealNameAuthStatusChecker;
import com.tongdaxing.xchat_core.common.ICommonClient;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.DetonateGiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.GiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.LotteryBoxAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.MultiGiftAttachment;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomCoreClient;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMessage;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.noble.NobleBusinessManager;
import com.tongdaxing.xchat_core.pay.IPayCore;
import com.tongdaxing.xchat_core.pay.IPayCoreClient;
import com.tongdaxing.xchat_core.pk.IPKCoreClient;
import com.tongdaxing.xchat_core.result.GiftRecieveInfoResult;
import com.tongdaxing.xchat_core.result.GiftResult;
import com.tongdaxing.xchat_core.result.MultiGiftRecieveInfoResult;
import com.tongdaxing.xchat_core.room.IRoomCore;
import com.tongdaxing.xchat_core.room.bean.RoomAdditional;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.tongdaxing.xchat_core.Constants.NOBLE_INVISIABLE_ENTER_ROOM;
import static com.tongdaxing.xchat_core.Constants.USER_MEDAL_DATE;
import static com.tongdaxing.xchat_core.Constants.USER_MEDAL_ID;

/**
 * @author chenran
 * @date 2017/7/27
 */

public class GiftCoreImpl extends AbstractBaseCore implements IGiftCore {

    private static List<CustomAttachment> giftQueue;
    private final String TAG = GiftCoreImpl.class.getSimpleName();
    private GiftListInfo giftListInfo;
    private GIFTHandle handler = new GIFTHandle();

    //---------------------------IM服务器接收礼物消息------------------------------
    private static List<CustomAttachment> imGiftQueue;

    private static void parseChatRoomAttachMent(CustomAttachment attachMent) {
        if (attachMent.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT) {
            GiftAttachment giftAttachment = (GiftAttachment) attachMent;
            CoreManager.notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_RECIEVE_GIFT_MSG, giftAttachment.getGiftRecieveInfo());
        } else if (attachMent.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT) {
            MultiGiftAttachment multiGiftAttachment = (MultiGiftAttachment) attachMent;
            CoreManager.notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_MULTI_GIFT_MSG, multiGiftAttachment.getMultiGiftRecieveInfo());
        } else if (attachMent.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUPER_GIFT) {
            GiftAttachment giftAttachment = (GiftAttachment) attachMent;
            CoreManager.notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_SUPER_GIFT_MSG, giftAttachment.getGiftRecieveInfo());
        }
    }

    private void addGiftMessage(CustomAttachment attachment) {
        giftQueue.add(attachment);
        if (giftQueue.size() == 1) {
            handler.sendEmptyMessageDelayed(0, 150);
        }
    }

    @Override
    public void requestGiftInfos() {
        LogUtils.d(TAG, "requestGiftInfos");
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");

        OkHttpManager.getInstance().getRequest(UriProvider.getGiftList(), requestParam, new OkHttpManager.MyCallBack<GiftResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(GiftResult response) {
                if (response != null && response.isSuccess()) {
                    if (null != giftListInfo) {
                        synchronized (giftListInfo) {
                            giftListInfo = response.getData();
                        }
                    } else {
                        giftListInfo = response.getData();
                    }
                    DemoCache.saveGiftList(giftListInfo);
                }
            }
        });
    }

    @Override
    public GiftInfo getGiftInfoByGiftId(int giftId) {
        GiftInfo info = null;
        if (null != giftListInfo && null != giftListInfo.getGift()) {
            synchronized (giftListInfo) {
                GiftInfo tempInfo = new GiftInfo();
                tempInfo.setGiftId(giftId);
                int index = giftListInfo.getGift().indexOf(tempInfo);
                if (-1 != index) {
                    info = giftListInfo.getGift().get(index);
                }
            }
        }
        return info;
    }

    /**
     * 获取礼物列表数据
     *
     * @param type 礼物类型（礼物列表展示的tab）
     * @return
     */
    @Override
    public List<GiftInfo> getGiftInfosByType(GiftType type, boolean isRoomToShowLimitedExempteGift) {
        if (giftListInfo != null && giftListInfo.getGift() != null && giftListInfo.getGift().size() > 0) {
            synchronized (giftListInfo) {
                List<GiftInfo> giftInfos = new ArrayList<>();
                RoomInfo roomInfo = BaseRoomServiceScheduler.getServerRoomInfo();
                for (GiftInfo giftInfo : giftListInfo.getGift()) {
                    //客户端包裹tab同服务器的gifttype=1有所区别
                    if (type == GiftType.Package) {
                        if (giftInfo.getUserGiftPurseNum() > 0) {
                            giftInfos.add(giftInfo);
                        }
                    } else if (type == GiftType.Exclusive) {
                        //贵族礼物直接显示，非贵族的专属礼物要判断是否是人气礼物等等
                        if (giftInfo.getNobleId() > 0) {
                            giftInfos.add(giftInfo);
                        } else if (giftInfo.getGiftType() == type.ordinal()) {
                            if (giftInfo.isNobleGift()) {
                                if (null != roomInfo && isRoomToShowLimitedExempteGift) {
                                    long surplusDur = System.currentTimeMillis() - roomInfo.getDetonatingTime();
                                    if (null != roomInfo.getExclusiveGift() && roomInfo.getExclusiveGift().contains(giftInfo.getGiftId())) {
                                        giftInfos.add(giftInfo);
                                    } else if (roomInfo.getDetonatingGifts() == giftInfo.getGiftId() && surplusDur < DetonateGiftAttachment.detonateGiftAnimTime) {
                                        giftInfos.add(giftInfo);
                                    }
                                }
                            } else {
                                //对于专属礼物，新修改逻辑为默认隐藏所有的贵族礼物
                                giftInfos.add(giftInfo);
                            }
                        }
                    } else if (giftInfo.getGiftType() == type.ordinal()) {
                        giftInfos.add(giftInfo);
                    }
                }
                return giftInfos;
            }
        } else {
            requestGiftInfos();
            return null;
        }
    }

    /**
     * 获取礼物列表数据
     *
     * @param type 礼物类型（礼物列表展示的tab）
     * @return
     */
    @Override
    public List<GiftInfo> getGiftInfosByTypeIMRoom(GiftType type, boolean isRoomToShowLimitedExempteGift) {
        if (giftListInfo != null && giftListInfo.getGift() != null && giftListInfo.getGift().size() > 0) {
            synchronized (giftListInfo) {
                List<GiftInfo> giftInfos = new ArrayList<>();
                RoomAdditional roomAdditional = RoomDataManager.get().getAdditional();
                for (GiftInfo giftInfo : giftListInfo.getGift()) {
                    //客户端包裹tab同服务器的gifttype=1有所区别
                    if (type == GiftType.Package) {
                        if (giftInfo.getUserGiftPurseNum() > 0) {
                            giftInfos.add(giftInfo);
                        }
                    } else if (type == GiftType.Exclusive) {
                        //贵族礼物直接显示，非贵族的专属礼物要判断是否是人气礼物等等
                        if (giftInfo.getNobleId() > 0) {
                            giftInfos.add(giftInfo);
                        } else if (giftInfo.getGiftType() == type.ordinal()) {
                            if (giftInfo.isNobleGift()) {
                                if (null != roomAdditional && isRoomToShowLimitedExempteGift) {
                                    long surplusDur = System.currentTimeMillis() - roomAdditional.getDetonatingTime();
                                    if (null != roomAdditional.getExclusiveGift() && roomAdditional.getExclusiveGift().contains(giftInfo.getGiftId())) {
                                        giftInfos.add(giftInfo);
                                    } else if (roomAdditional.getDetonatingGifts() == giftInfo.getGiftId() && surplusDur < DetonateGiftAttachment.detonateGiftAnimTime) {
                                        giftInfos.add(giftInfo);
                                    }
                                }
                            } else {
                                //对于专属礼物，新修改逻辑为默认隐藏所有的贵族礼物
                                giftInfos.add(giftInfo);
                            }
                        }
                    } else if (giftInfo.getGiftType() == type.ordinal()) {
                        giftInfos.add(giftInfo);
                    }
                }
                return giftInfos;
            }
        } else {
            requestGiftInfos();
            return null;
        }
    }


    @Override
    public void onReceiveChatRoomMessages(List<ChatRoomMessage> chatRoomMessageList) {
        if (chatRoomMessageList != null && chatRoomMessageList.size() > 0) {
            for (ChatRoomMessage msg : chatRoomMessageList) {
                if (msg.getMsgType() == MsgTypeEnum.custom) {
                    CustomAttachment attachment = (CustomAttachment) msg.getAttachment();
                    if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT
                            || attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT
                            || attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUPER_GIFT) {
                        addGiftMessage(attachment);
                    }
                }
            }
        }
    }

    @Override
    public List<GiftInfo> getLastRequestGiftInfos() {
        return (null == giftListInfo || null == giftListInfo.getGift()) ? new ArrayList<GiftInfo>() : giftListInfo.getGift();
    }

    private GiftCoreHandler giftCoreHandler = new GiftCoreHandler();

    private void parsePersonalAttachMent(CustomAttachment attachMent) {
        if (attachMent.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT) {
            GiftAttachment giftAttachment = (GiftAttachment) attachMent;
            notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_RECEIVE_PERSONAL_GIFT, giftAttachment.getGiftRecieveInfo());
        }
    }

    public GiftCoreImpl() {
        CoreManager.addClient(this);
        giftListInfo = DemoCache.readGiftList();
        giftQueue = new ArrayList<>();
        imGiftQueue = new ArrayList<>();
    }

    @CoreEvent(coreClientClass = IMRoomCoreClient.class)
    public void onSendRoomMessageSuccess(ChatRoomMessage msg) {
        if (msg.getMsgType() == MsgTypeEnum.custom) {
            CustomAttachment attachment = (CustomAttachment) msg.getAttachment();
            if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT
                    || attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT) {
                addGiftMessage(attachment);
            }
        }
    }

    private void sendGiftMessage(GiftReceiveInfo giftReceiveInfo) {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null || giftReceiveInfo == null) {
            return;
        }
        long myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();

        GiftAttachment giftAttachment = new GiftAttachment(CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT, CustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_GIFT);
        giftAttachment.setUid(myUid + "");
        giftAttachment.setGiftRecieveInfo(giftReceiveInfo);
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        giftAttachment.setCharmLevel(userInfo.getCharmLevel());
        giftAttachment.setExperLevel(userInfo.getExperLevel());


        ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                // 聊天室id
                roomInfo.getRoomId() + "",
                giftAttachment
        );

        CoreManager.getCore(IRoomCore.class).sendMessage(message);
    }

    private void sendMultiGiftMessage(MultiGiftReceiveInfo giftReceiveInfo) {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo != null && giftReceiveInfo != null) {
            long myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
            MultiGiftAttachment giftAttachment = new MultiGiftAttachment(CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT, CustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_MULTI_GIFT);
            giftAttachment.setUid(myUid + "");
            giftAttachment.setMultiGiftAttachment(giftReceiveInfo);
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            giftAttachment.setCharmLevel(userInfo.getCharmLevel());
            giftAttachment.setExperLevel(userInfo.getExperLevel());
            ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                    // 聊天室id
                    roomInfo.getRoomId() + "",
                    giftAttachment
            );

            CoreManager.getCore(IRoomCore.class).sendMessage(message);


        }
    }

    @Override
    public void sendLotteryMeg(GiftInfo giftInfo, int count) {
        if (giftInfo == null) {
            return;
        }
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        LotteryBoxAttachment lotteryBoxAttachment = new LotteryBoxAttachment(CustomAttachment.CUSTOM_MSG_LOTTERY_BOX, CustomAttachment.CUSTOM_MSG_LOTTERY_BOX);
        Json json = new Json();
        json.set("giftName", giftInfo.getGiftName());
        json.set("count", count);
        json.set("goldPrice", giftInfo.getGoldPrice());
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (null != userInfo) {
            if (null != AvRoomDataManager.get().mCurrentRoomInfo && AvRoomDataManager.get().mCurrentRoomInfo.getUid() != userInfo.getUid()) {
                json.set(Constants.USER_NICK, NobleBusinessManager.getNobleRoomNick(userInfo.getVipId(), userInfo.getIsInvisible(), userInfo.getVipDate(), userInfo.getNick()));
                json.set(USER_MEDAL_ID, userInfo.getVipId());
                json.set(USER_MEDAL_DATE, userInfo.getVipDate());
                json.set(NOBLE_INVISIABLE_ENTER_ROOM, userInfo.getIsInvisible() ? 1 : 0);
            } else {
                json.set(Constants.USER_NICK, userInfo.getNick());
            }

            lotteryBoxAttachment.setParams(json + "");
            ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                    // 聊天室id
                    roomInfo.getRoomId() + "",
                    lotteryBoxAttachment
            );
            CoreManager.getCore(IRoomCore.class).sendMessage(message);
        }
    }

    @Override
    public void sendRoomGift(int giftId, final long targetUid, long roomUid, final int giftNum, final int goldPrice) {
        LogUtils.d("sendRoomGift", 1 + "");
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
        requestParam.put("giftId", giftId + "");
        requestParam.put("targetUid", targetUid + "");
        requestParam.put("uid", uid + "");
        requestParam.put("ticket", ticket);
        requestParam.put("roomUid", roomUid + "");
        requestParam.put("giftNum", giftNum + "");
        if (targetUid == roomUid) {
            requestParam.put("type", "1");
        } else {
            requestParam.put("type", "3");
        }

        RoomInfo cRoomInfo = BaseRoomServiceScheduler.getCurrentRoomInfo();
        if (null != cRoomInfo) {
            requestParam.put("roomType", cRoomInfo.getType() + "");
        }

        OkHttpManager.getInstance().postRequest(UriProvider.sendGiftV3(), requestParam, new OkHttpManager.MyCallBack<GiftRecieveInfoResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_WALLET_INFO_UPDATE, e.getMessage());
                notifyClients(IPKCoreClient.class, IPKCoreClient.METHOD_ON_PK_GIFT_FAIL, e.getMessage());
            }

            @Override
            public void onResponse(GiftRecieveInfoResult response) {
                if (response.isSuccess()) {
                    GiftReceiveInfo data = response.getData();
                    if (null == data) {
                        return;
                    }
                    if (data.getIsPea() == 1) {
                        //甜豆礼物
                        CoreManager.getCore(IPayCore.class).minusDouzi(data.getUsePeaNum());
                    } else {
                        //金币礼物
                        CoreManager.getCore(IPayCore.class).minusGold(data.getUseGiftPurseGold());
                    }
                    notifyClients(IPKCoreClient.class, IPKCoreClient.METHOD_ON_PK_GIFT, targetUid);
                    sendGiftMessage(data);
                    //刷新当前选中礼物剩余数量
                    GiftInfo giftInfo = CoreManager.getCore(IGiftCore.class).findGiftInfoById(data.getGiftId());
                    boolean needRefresh = false;
                    int giftNum = 0;
                    if (null != giftInfo) {
                        giftInfo.setUserGiftPurseNum(data.getUserGiftPurseNum());
                        needRefresh = true;
                        giftNum = data.getUserGiftPurseNum();
                    }
                    if (needRefresh) {
                        notifyClients(IGiftCoreClient.class, IGiftCoreClient.refreshFreeGift, giftNum);
                    }
                } else if (response.getCode() == 2103) {
                    notifyClients(ICommonClient.class, ICommonClient.METHOD_ON_RECIEVE_NEED_RECHARGE);
                } else if (response.getCode() == 8000) {
                    notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_GIFT_PAST_DUE);
                    requestGiftInfos();
                } else if (response.getCode() == RealNameAuthStatusChecker.RESULT_FAILED_NEED_REAL_NAME_AUTH) {
                    //送礼出错，提示实名认证
                    notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_SEND_GIFT_FAIL_AUTH, response.getMessage());
                } else {
                    notifyClients(IGiftCoreClient.class, IGiftCoreClient.onGiftSendErrorMsg, response.getMessage());
                }
            }
        });
    }

    @Override
    public GiftInfo findGiftInfoById(int giftId) {
        if (giftListInfo != null && null != giftListInfo.getGift()) {
            synchronized (giftListInfo) {
                GiftInfo tempGiftInfo = new GiftInfo();
                tempGiftInfo.setGiftId(giftId);
                GiftInfo giftInfo = null;
                int index = giftListInfo.getGift().indexOf(tempGiftInfo);
                if (-1 != index) {
                    giftInfo = giftListInfo.getGift().get(index);
                }
                return giftInfo;
            }
        }
        return null;
    }

    private static class GIFTHandle extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (giftQueue.size() > 0) {
                CustomAttachment attachment = giftQueue.remove(0);
                if (attachment != null) {
                    parseChatRoomAttachMent(attachment);
                }
            }

            if (giftQueue.size() > 0) {
                sendEmptyMessageDelayed(0, 150);
            }
        }
    }

    @Override
    public void sendPersonalGiftToNIM(final int giftId, final long targetUid, final int giftNum,
                                      final int goldPrice, final WeakReference<Container> reference,
                                      final ChargeListener chargeListener) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
        requestParam.put("giftId", giftId + "");
        requestParam.put("targetUid", targetUid + "");
        requestParam.put("uid", uid + "");
        requestParam.put("giftNum", giftNum + "");
        requestParam.put("ticket", ticket);
        requestParam.put("type", "2");

        OkHttpManager.getInstance().postRequest(UriProvider.sendGiftV3(), requestParam,
                new OkHttpManager.MyCallBack<GiftRecieveInfoResult>() {
                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                        notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_SEND_PERSONAL_GIFT_FAIL, 0);
                    }

                    @Override
                    public void onResponse(GiftRecieveInfoResult response) {
                        if (response.isSuccess()) {
                            GiftReceiveInfo data = response.getData();
                            Container container = reference.get();
                            if (container == null || null == data) {
                                return;
                            }
                            long myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
                            UserInfo myUserInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(myUid);
                            GiftAttachment giftAttachment = new GiftAttachment(CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT, CustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_GIFT);
                            giftAttachment.setUid(myUid + "");
                            GiftReceiveInfo giftRecieveInfo = new GiftReceiveInfo();
                            giftRecieveInfo.setNick(myUserInfo.getNick());
                            giftRecieveInfo.setTargetUid(targetUid);
                            giftRecieveInfo.setAvatar(myUserInfo.getAvatar());
                            giftRecieveInfo.setGiftId(giftId);
                            giftRecieveInfo.setUid(myUid);
                            giftRecieveInfo.setGiftNum(giftNum);
                            giftRecieveInfo.setVipName(myUserInfo.getVipName());
                            giftRecieveInfo.setVipIcon(myUserInfo.getVipIcon());
                            giftRecieveInfo.setVipMedal(myUserInfo.getVipMedal());
                            giftAttachment.setGiftRecieveInfo(giftRecieveInfo);
                            CustomMessageConfig customMessageConfig = new CustomMessageConfig();
                            customMessageConfig.enablePush = false;
                            IMMessage imMessage = MessageBuilder.createCustomMessage(targetUid + "", SessionTypeEnum.P2P, "", giftAttachment, customMessageConfig);
                            container.proxy.sendMessage(imMessage);

                            parsePersonalAttachMent((CustomAttachment) imMessage.getAttachment());
                            if (data.getIsPea() == 1) {
                                //甜豆礼物
                                CoreManager.getCore(IPayCore.class).minusDouzi(data.getUsePeaNum());
                            } else {
                                //金币礼物
                                CoreManager.getCore(IPayCore.class).minusGold(data.getUseGiftPurseGold());
                            }

                            GiftInfo giftInfo = CoreManager.getCore(IGiftCore.class).findGiftInfoById(data.getGiftId());
                            boolean needRefresh = false;
                            int giftNum = 0;
                            if (null != giftInfo) {
                                giftInfo.setUserGiftPurseNum(data.getUserGiftPurseNum());
                                needRefresh = true;
                                giftNum = data.getUserGiftPurseNum();
                            }
                            if (needRefresh) {
                                notifyClients(IGiftCoreClient.class, IGiftCoreClient.refreshFreeGift, giftNum);
                            }
                        } else {
                            if (response.getCode() == 2103) {
                                chargeListener.onNeedCharge();
                            } else if (response.getCode() == 8000) {
                                notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_GIFT_PAST_DUE);
                                requestGiftInfos();
                            } else if (response.getCode() == RealNameAuthStatusChecker.RESULT_FAILED_NEED_REAL_NAME_AUTH) {
                                //送礼出错，提示实名认证
                                notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_SEND_GIFT_FAIL_AUTH, response.getMessage());
                            }
                            notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_SEND_PERSONAL_GIFT_FAIL, response.getCode(), response.getMessage());
                        }
                    }
                });
    }

    @Override
    public void sendRoomMultiGift(int giftId, final List<Long> targetUids, long roomUid, final int giftNum, final int goldPrice) {
        LogUtils.d(TAG, "sendRoomMultiGift-giftId:" + giftId + " targetUids.size:" + targetUids.size()
                + " roomUid:" + roomUid + " giftNum:" + giftNum + " goldPrice:" + goldPrice);
        if (targetUids == null || targetUids.size() <= 0) {
            return;
        }

        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
        requestParam.put("giftId", giftId + "");
        requestParam.put("uid", uid + "");
        requestParam.put("ticket", ticket);
        requestParam.put("roomUid", roomUid + "");
        requestParam.put("giftNum", giftNum + "");

        RoomInfo cRoomInfo = BaseRoomServiceScheduler.getCurrentRoomInfo();
        if (null != cRoomInfo) {
            requestParam.put("roomType", cRoomInfo.getType() + "");
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < targetUids.size(); i++) {
            long targetUid = targetUids.get(i);
            sb.append(targetUid + "");
            sb.append(",");
        }
        sb.deleteCharAt(sb.length() - 1);
        requestParam.put("targetUids", sb.toString());

        OkHttpManager.getInstance().postRequest(UriProvider.sendWholeGiftV3(), requestParam, new OkHttpManager.MyCallBack<MultiGiftRecieveInfoResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();

            }

            @Override
            public void onResponse(MultiGiftRecieveInfoResult response) {
                if (response.isSuccess()) {
                    MultiGiftReceiveInfo data = response.getData();
                    if (null == data) {
                        return;
                    }
                    if (data.getIsPea() == 1) {
                        //甜豆礼物
                        CoreManager.getCore(IPayCore.class).minusDouzi(data.getUsePeaNum());
                    } else {
                        //金币礼物
                        CoreManager.getCore(IPayCore.class).minusGold(data.getUseGiftPurseGold());
                    }
                    sendMultiGiftMessage(data);
                    boolean needRefresh = false;
                    int giftNum = 0;
                    GiftInfo giftInfo = CoreManager.getCore(IGiftCore.class).findGiftInfoById(data.getGiftId());
                    if (null != giftInfo) {
                        giftInfo.setUserGiftPurseNum(data.getUserGiftPurseNum());
                        needRefresh = true;
                        giftNum = data.getUserGiftPurseNum();
                    }

                    notifyClients(IPKCoreClient.class, IPKCoreClient.METHOD_ON_PK_MULTI_GIFT, targetUids);
                    if (needRefresh) {
                        notifyClients(IGiftCoreClient.class, IGiftCoreClient.refreshFreeGift, giftNum);
                    }
                } else if (response.getCode() == 2103) {
                    notifyClients(ICommonClient.class, ICommonClient.METHOD_ON_RECIEVE_NEED_RECHARGE);
                } else if (response.getCode() == 8000) {
                    notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_GIFT_PAST_DUE);
                    requestGiftInfos();
                } else if (response.getCode() == RealNameAuthStatusChecker.RESULT_FAILED_NEED_REAL_NAME_AUTH) {
                    //送礼出错，提示实名认证
                    notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_SEND_GIFT_FAIL_AUTH, response.getMessage());
                } else {
                    notifyClients(IGiftCoreClient.class, IGiftCoreClient.onGiftSendErrorMsg, response.getMessage());
                }
            }
        });
    }

    private void addIMGiftMessage(CustomAttachment attachment) {
        imGiftQueue.add(attachment);
        if (imGiftQueue.size() == 1) {
            giftCoreHandler.sendEmptyMessageDelayed(0, 150);
        }
    }

    @Override
    public void onReceiveIMRoomMessages(List<IMRoomMessage> chatRoomMessageList) {
        if (chatRoomMessageList != null && chatRoomMessageList.size() > 0) {
            for (IMRoomMessage msg : chatRoomMessageList) {
                if (IMReportRoute.sendMessageReport.equalsIgnoreCase(msg.getRoute())) {
                    CustomAttachment attachment = msg.getAttachment();
                    if ((attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT
                            && attachment.getSecond() == CustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_GIFT)
                            || (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT
                            && attachment.getSecond() == CustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_MULTI_GIFT)
                            || attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUPER_GIFT) {
                        addIMGiftMessage(attachment);
                    }
                }
            }
        }
    }

    @CoreEvent(coreClientClass = IMRoomCoreClient.class)
    public void onSendIMRoomMessageSuccess(IMRoomMessage msg) {
        if (IMReportRoute.sendMessageReport.equalsIgnoreCase(msg.getRoute())) {
            CustomAttachment attachment = msg.getAttachment();
            if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT
                    || attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT) {
                addIMGiftMessage(attachment);
            }
        }
    }

    static class GiftCoreHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (imGiftQueue.size() > 0) {
                CustomAttachment attachment = imGiftQueue.remove(0);
                if (attachment != null) {
                    parseChatRoomAttachMent(attachment);
                }
            }

            if (imGiftQueue.size() > 0) {
                sendEmptyMessageDelayed(0, 150);
            }
        }
    }
}
