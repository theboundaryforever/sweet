package com.tongdaxing.xchat_core.gift;

import java.io.Serializable;

/**
 * @author weihaitao
 * @date 2019/8/27
 */
public class ComboInfo extends ComboRang implements Serializable {

    private long comboId = 0L;

    public ComboInfo(int start, int end, long comboId) {
        super(start, end);
        comboRangStart = start;
        comboRangEnd = end;
        this.comboId = comboId;
    }

    public long getComboId() {
        return comboId;
    }

    public void setComboId(long comboId) {
        this.comboId = comboId;
    }


}
