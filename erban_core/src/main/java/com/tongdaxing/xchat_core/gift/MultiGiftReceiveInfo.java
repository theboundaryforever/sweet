package com.tongdaxing.xchat_core.gift;

import java.io.Serializable;
import java.util.List;

/**
 * Created by chenran on 2017/10/25.
 */

public class MultiGiftReceiveInfo implements Serializable {

    private long uid;
    private List<Long> targetUids;
    private int giftId;
    private int realGiftId;
    private int giftNum;
    private String nick;
    private String avatar;
    private int userGiftPurseNum;
    private int useGiftPurseGold;
    private String vipMedal = null;
    private String vipName = null;
    private String vipIcon = null;
    private int vipId;
    private int vipDate;
    private boolean isInvisible = false;
    private List<Integer> roomIdList;

    private int isPea;//是否甜豆礼物 0：不是 1：是
    private int usePeaNum;

    private String giftUrl;
    private boolean hasVggPic;
    private String vggUrl;
    private String giftName;
    private long goldPrice;

    //1-包裹，2-普通，3-宝箱，4-专属，
    // 但是这里的包裹类型是服务器数据层对应的包裹类型，客户端应当严格按照数量来算包裹礼物列表并展示给用户，
    // 因为有可能其他服务器类型的礼物也跑到客户端的包裹tab下去
    private int giftType = GiftType.Unknow.ordinal();
    /**
     * 礼物连击 为0 则非连击 不为0 则为每组连击的标记（时间戳）
     */
    private long comboId = 0L;
    /**
     * 连击范围，左界限
     */
    private int comboRangStart = 0;
    /**
     * 连击范围，右界限
     */
    private int comboRangEnd = 1;

    public int getGiftType() {
        return giftType;
    }

    public void setGiftType(int giftType) {
        this.giftType = giftType;
    }

    public long getComboId() {
        return comboId;
    }

    public void setComboId(long comboId) {
        this.comboId = comboId;
    }

    public int getComboRangStart() {
        return comboRangStart;
    }

    public void setComboRangStart(int comboRangStart) {
        this.comboRangStart = comboRangStart;
    }

    public int getComboRangEnd() {
        return comboRangEnd;
    }

    public void setComboRangEnd(int comboRangEnd) {
        this.comboRangEnd = comboRangEnd;
    }

    public int getUsePeaNum() {
        return usePeaNum;
    }

    public void setUsePeaNum(int usePeaNum) {
        this.usePeaNum = usePeaNum;
    }

    public int getIsPea() {
        return isPea;
    }

    public void setIsPea(int isPea) {
        this.isPea = isPea;
    }


    public int getRealGiftId() {
        return realGiftId;
    }

    public void setRealGiftId(int realGiftId) {
        this.realGiftId = realGiftId;
    }

    public int getUseGiftPurseGold() {
        return useGiftPurseGold;
    }

    public void setUseGiftPurseGold(int useGiftPurseGold) {
        this.useGiftPurseGold = useGiftPurseGold;
    }

    public int getUserGiftPurseNum() {
        return userGiftPurseNum;
    }

    public void setUserGiftPurseNum(int userGiftPurseNum) {
        this.userGiftPurseNum = userGiftPurseNum;
    }

    public List<Integer> getRoomIdList() {
        return roomIdList;
    }

    public void setRoomIdList(List<Integer> roomIdList) {
        this.roomIdList = roomIdList;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public List<Long> getTargetUids() {
        return targetUids;
    }

    public void setTargetUids(List<Long> targetUids) {
        this.targetUids = targetUids;
    }

    public int getGiftId() {
        return giftId;
    }

    public void setGiftId(int giftId) {
        this.giftId = giftId;
    }

    public int getGiftNum() {
        return giftNum;
    }

    public void setGiftNum(int giftNum) {
        this.giftNum = giftNum;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getVipId() {
        return vipId;
    }

    public void setVipId(int vipId) {
        this.vipId = vipId;
    }

    public int getVipDate() {
        return vipDate;
    }

    public void setVipDate(int vipDate) {
        this.vipDate = vipDate;
    }

    public boolean isInvisible() {
        return isInvisible;
    }

    public void setInvisible(boolean invisible) {
        isInvisible = invisible;
    }

    public String getVipName() {
        return vipName;
    }

    public void setVipName(String vipName) {
        this.vipName = vipName;
    }

    public String getGiftUrl() {
        return giftUrl;
    }

    public void setGiftUrl(String giftUrl) {
        this.giftUrl = giftUrl;
    }

    public boolean isHasVggPic() {
        return hasVggPic;
    }

    public void setHasVggPic(boolean hasVggPic) {
        this.hasVggPic = hasVggPic;
    }

    public String getVggUrl() {
        return vggUrl;
    }

    public void setVggUrl(String vggUrl) {
        this.vggUrl = vggUrl;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public long getGoldPrice() {
        return goldPrice;
    }

    public void setGoldPrice(long goldPrice) {
        this.goldPrice = goldPrice;
    }

    @Override
    public String toString() {
        return "MultiGiftReceiveInfo{" +
                "uid=" + uid +
                ", targetUids=" + targetUids +
                ", giftId=" + giftId +
                ", realGiftId=" + realGiftId +
                ", giftNum=" + giftNum +
                ", nick='" + nick + '\'' +
                ", avatar='" + avatar + '\'' +
                ", userGiftPurseNum=" + userGiftPurseNum +
                ", useGiftPurseGold=" + useGiftPurseGold +
                ", roomIdList=" + roomIdList +
                ", vipMedal='" + vipMedal + '\'' +
                ", vipName='" + vipName + '\'' +
                ", vipIcon='" + vipIcon + '\'' +
                ", vipId=" + vipId +
                ", vipDate=" + vipDate +
                ", giftType=" + giftType +
                ", isInvisible=" + isInvisible +
                '}';
    }

    public String getVipIcon() {
        return vipIcon;
    }

    public void setVipIcon(String vipIcon) {
        this.vipIcon = vipIcon;
    }

    public String getVipMedal() {
        return vipMedal;
    }

    public void setVipMedal(String vipMedal) {
        this.vipMedal = vipMedal;
    }
}
