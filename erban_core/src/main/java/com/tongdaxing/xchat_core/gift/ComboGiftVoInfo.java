package com.tongdaxing.xchat_core.gift;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Administrator on 2019/4/7.
 * 礼物连击展示层bean
 */
public class ComboGiftVoInfo {
    private long uid;
    private long gid;
    private String nick;
    private String avatar;
    private String content;
    private String giftUrl;
    private String itemComboId;
    private long comboId = 0L;
    private ConcurrentLinkedQueue<ComboRang> comboRangs = new ConcurrentLinkedQueue<>();
    private boolean isAllMicCombo = false;
    private GiftAnimStatus giftAnimStatus;

    public ComboGiftVoInfo() {
    }

    public ComboGiftVoInfo(long uid, long gid, String userName) {
        this.uid = uid;
        this.gid = gid;
        this.nick = userName;
    }

    public long getComboId() {
        return comboId;
    }

    public void setComboId(long comboId) {
        this.comboId = comboId;
    }

    public void addComboRangToLast(ComboRang comboRang) {
        boolean isOffered = false;
        try {
            isOffered = comboRangs.offer(comboRang);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public ComboRang getFirstComboRang() {
        return comboRangs.peek();
    }

    public ComboRang removeFirstComboRang() {
        return comboRangs.poll();
    }

    public boolean isAllMicCombo() {
        return isAllMicCombo;
    }

    public void setAllMicCombo(boolean allMicCombo) {
        isAllMicCombo = allMicCombo;
    }

    public String getItemComboId() {
        return itemComboId;
    }

    public void setItemComboId(String itemComboId) {
        this.itemComboId = itemComboId;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public long getGid() {
        return gid;
    }

    public void setGid(long gid) {
        this.gid = gid;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getGiftUrl() {
        return giftUrl;
    }

    public void setGiftUrl(String giftUrl) {
        this.giftUrl = giftUrl;
    }

    public GiftAnimStatus getGiftAnimStatus() {
        return giftAnimStatus;
    }

    public void setGiftAnimStatus(GiftAnimStatus giftAnimStatus) {
        this.giftAnimStatus = giftAnimStatus;
    }

    /**
     * PrepareAndPlaying :  加载中
     * Vanishing :   消失中
     * Disappear  :   已消失
     */
    public enum GiftAnimStatus {
        /**
         * 加载、动画播放中
         */
        PrepareAndPlaying,
        /**
         * 5秒钟停留动画中
         */
        Holding,
        /**
         * 消失中
         */
        Vanishing,
        /**
         * 已消失
         */
        Disappear
    }
}
