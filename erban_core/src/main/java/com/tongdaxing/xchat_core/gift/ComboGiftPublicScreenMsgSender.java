package com.tongdaxing.xchat_core.gift;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.user.VersionsCore;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author weihaitao
 * @date 2019/8/28
 */
public class ComboGiftPublicScreenMsgSender {

    private static final String TAG = ComboGiftPublicScreenMsgSender.class.getSimpleName();
    private static ComboGiftPublicScreenMsgSender instance = null;
    private long currRoomId = 0L;
    /**
     * 因为短时间内同时操作的仅为update操作，因此暂时不用考虑同步的问题
     */
    private Map<String, GiftReceiveInfo> itemComboIdGiftReceiveInfos = new HashMap<>();
    private Map<String, MultiGiftReceiveInfo> itemComboIdMultiGiftReceiveInfos = new HashMap<>();
    private boolean isScreenGiftMsgSending = false;

    private ComboGiftPublicScreenMsgSender() {

    }

    //暂时不用考虑切换房间，短时间极速送礼，map的clear和add操作冲突的问题，视作手速没有那么快
    public static ComboGiftPublicScreenMsgSender getInstance() {

        if (null == instance) {
            instance = new ComboGiftPublicScreenMsgSender();
        }
        return instance;
    }

    public void addComboGiftRecvInfo(long roomId, GiftReceiveInfo giftReceiveInfo) {
        LogUtils.d(TAG, "addComboGiftRecvInfo roomId:" + roomId + " currRoomId:" + currRoomId);
        if (currRoomId == roomId) {
            if (null == itemComboIdGiftReceiveInfos) {
                itemComboIdGiftReceiveInfos = new HashMap<>();
            }
            String itemComboId = genComboGiftAnimId(giftReceiveInfo.getUid(), giftReceiveInfo.getGiftId(), giftReceiveInfo.getComboId());
            if (itemComboIdGiftReceiveInfos.containsKey(itemComboId)) {
                GiftReceiveInfo info = itemComboIdGiftReceiveInfos.get(itemComboId);
                if (info.getComboId() == giftReceiveInfo.getComboId()) {
                    //仅需更新结束时的界限值即可
                    info.setComboRangEnd(giftReceiveInfo.getComboRangEnd());
                    LogUtils.d(TAG, "addComboGiftRecvInfo 更新rangEnd：" + giftReceiveInfo.getComboRangEnd());
                }
            } else {
                itemComboIdGiftReceiveInfos.put(itemComboId, giftReceiveInfo);
                LogUtils.d(TAG, "addComboGiftRecvInfo 添加rangEnd：" + giftReceiveInfo.getComboRangEnd());
            }
        }
    }

    public void addComboGiftRecvInfo(long roomId, MultiGiftReceiveInfo giftReceiveInfo) {
        LogUtils.d(TAG, "addComboGiftRecvInfo roomId:" + roomId + " currRoomId:" + currRoomId);
        if (currRoomId == roomId) {
            if (null == itemComboIdMultiGiftReceiveInfos) {
                itemComboIdMultiGiftReceiveInfos = new HashMap<>();
            }
            String itemComboId = genComboGiftAnimId(giftReceiveInfo.getUid(), giftReceiveInfo.getGiftId(), giftReceiveInfo.getComboId());
            if (itemComboIdMultiGiftReceiveInfos.containsKey(itemComboId)) {
                MultiGiftReceiveInfo info = itemComboIdMultiGiftReceiveInfos.get(itemComboId);
                if (info.getComboId() == giftReceiveInfo.getComboId()) {
                    //仅需更新结束时的界限值即可
                    info.setComboRangEnd(giftReceiveInfo.getComboRangEnd());
                    LogUtils.d(TAG, "addComboGiftRecvInfo 更新rangEnd：" + giftReceiveInfo.getComboRangEnd());
                }
            } else {
                itemComboIdMultiGiftReceiveInfos.put(itemComboId, giftReceiveInfo);
                LogUtils.d(TAG, "addComboGiftRecvInfo 添加rangEnd：" + giftReceiveInfo.getComboRangEnd());
            }
        }
    }

    public String genComboGiftAnimId(long uid, long giftId, long comboId) {
        return String.valueOf(uid).concat("-").concat(String.valueOf(giftId)).concat("-").concat(String.valueOf(comboId));
    }

    public void setCurrRoomId(long roomId) {
        LogUtils.d(TAG, "setCurrRoomId roomId:" + roomId + " currRoomId:" + currRoomId + " isScreenGiftMsgSending:" + isScreenGiftMsgSending);
        if (currRoomId != roomId) {
            // 切换了房间
            //如果在连击过程中切换的房间，那么切换房间前，将公屏消息发出去
            if (!isScreenGiftMsgSending) {
                LogUtils.d(TAG, "setCurrRoomId-->sendComboGiftScreenMsg");
                sendComboGiftScreenMsg(currRoomId);
            }
            currRoomId = roomId;
        }
    }

    public void sendComboGiftScreenMsg() {
        if (!CoreManager.getCore(VersionsCore.class).isGiftComboSwitchOpen()) {
            return;
        }
        LogUtils.d(TAG, "sendComboGiftScreenMsg currRoomId:" + currRoomId + " isScreenGiftMsgSending:" + isScreenGiftMsgSending);
        if (0 != currRoomId && !isScreenGiftMsgSending) {
            LogUtils.d(TAG, "sendComboGiftScreenMsg-->sendComboGiftScreenMsg");
            sendComboGiftScreenMsg(currRoomId);
        }
    }

    /**
     * 发送暴击礼物公屏消息
     *
     * @param roomId
     */
    public void sendComboGiftScreenMsg(long roomId) {

        if (!CoreManager.getCore(VersionsCore.class).isGiftComboSwitchOpen()) {
            return;
        }

        if (isScreenGiftMsgSending) {
            return;
        }

        if (currRoomId != roomId) {
            LogUtils.d(TAG, "sendComboGiftScreenMsg roomId:" + roomId + " currRoomId:" + currRoomId + " roomId不同，拦截");
            return;
        }
        isScreenGiftMsgSending = true;
        LogUtils.d(TAG, "sendComboGiftScreenMsg roomId:" + roomId + " isScreenGiftMsgSending:" + isScreenGiftMsgSending);
        if (null != itemComboIdGiftReceiveInfos) {
            Iterator<String> iterator = itemComboIdGiftReceiveInfos.keySet().iterator();
            while (iterator.hasNext()) {
                String itemComboId = iterator.next();
                GiftReceiveInfo giftReceiveInfo = itemComboIdGiftReceiveInfos.get(itemComboId);
                LogUtils.d(TAG, "sendComboGiftScreenMsg-->sendSingleGiftComboScreenMessage");
                IMRoomMessageManager.get().sendSingleGiftComboScreenMessage(giftReceiveInfo, roomId);
            }
            itemComboIdGiftReceiveInfos.clear();
        }

        if (null != itemComboIdMultiGiftReceiveInfos) {
            Iterator<String> iterator = itemComboIdMultiGiftReceiveInfos.keySet().iterator();
            while (iterator.hasNext()) {
                String itemComboId = iterator.next();
                MultiGiftReceiveInfo giftReceiveInfo = itemComboIdMultiGiftReceiveInfos.get(itemComboId);
                LogUtils.d(TAG, "sendComboGiftScreenMsg-->sendMultiGiftComboScreenMessage");
                IMRoomMessageManager.get().sendMultiGiftComboScreenMessage(giftReceiveInfo, roomId);
            }
            itemComboIdMultiGiftReceiveInfos.clear();
        }

        isScreenGiftMsgSending = false;
        LogUtils.d(TAG, "sendComboGiftScreenMsg roomId:" + roomId + " isScreenGiftMsgSending:" + isScreenGiftMsgSending);
    }
}
