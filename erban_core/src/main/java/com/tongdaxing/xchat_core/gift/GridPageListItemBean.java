package com.tongdaxing.xchat_core.gift;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/5/10
 */
public abstract class GridPageListItemBean {
    private boolean isSelect;

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }
}
