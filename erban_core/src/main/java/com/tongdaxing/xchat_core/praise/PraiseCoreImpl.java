package com.tongdaxing.xchat_core.praise;

import com.netease.nimlib.sdk.chatroom.ChatRoomMessageBuilder;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.tongdaxing.erban.libcommon.coremanager.AbstractBaseCore;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomTipAttachment;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.result.BooleanResult;
import com.tongdaxing.xchat_core.room.IRoomCore;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.Map;

/**
 * Created by zhouxiangfeng on 2017/5/18.
 */

public class PraiseCoreImpl extends AbstractBaseCore implements IPraiseCore {
    public PraiseCoreImpl() {
        CoreManager.addClient(this);
    }

    @Override
    public void praise(final long likedUid) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("type", String.valueOf(1));
        params.put("likedUid", likedUid + "");

        OkHttpManager.getInstance().postRequest(UriProvider.praise(), params, new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_PRAISE_FAITH, e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (response.isSuccess()) {
                    RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                    if (roomInfo != null && roomInfo.getUid() == likedUid) {
                        sendAttentionRoomTipMsg(likedUid);
                    }
                    notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_PRAISE, likedUid);
                } else {
                    notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_PRAISE_FAITH, response.getMessage());
                }

            }
        });
    }

    @Override
    public void userInfoPraise(final long likedUid) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("type", String.valueOf(1));
        params.put("likedUid", likedUid + "");

        OkHttpManager.getInstance().postRequest(UriProvider.praise(), params, new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_PRAISE_FAITH, e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (response.isSuccess()) {
//                    RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
//                    if (roomInfo != null && roomInfo.getUid() == likedUid) {
//                        sendAttentionRoomTipMsg(likedUid);
//                    }
                    notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_PRAISE, likedUid);
                } else {
                    notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_PRAISE_FAITH, response.getMessage());
                }

            }
        });
    }


    @Override
    public void cancelPraise(final long canceledUid) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("type", String.valueOf(2));
        params.put("likedUid", canceledUid + "");

        OkHttpManager.getInstance().postRequest(UriProvider.praise(), params, new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_CANCELED_PRAISE_FAITH, e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (response.isSuccess()) {
                    notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_CANCELED_PRAISE, canceledUid);
                } else {
                    notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_CANCELED_PRAISE_FAITH, response.getMessage());
                }

            }
        });
    }

    @Override
    public void isPraised(long uid, final long isLikeUid) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));
        params.put("isLikeUid", String.valueOf(isLikeUid));
        OkHttpManager.getInstance().getRequest(UriProvider.isLike(), params, new OkHttpManager.MyCallBack<BooleanResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();

            }

            @Override
            public void onResponse(BooleanResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        if (null != response.getData()) {
                            notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_ISLIKED, response.getData(), isLikeUid);
                        } else {
                            notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_ISLIKED_FAITH, response.getMessage());
                        }
                    } else {
                        notifyClients(IPraiseClient.class, IPraiseClient.METHOD_ON_ISLIKED_FAITH, response.getMessage());
                    }
                }
            }
        });
    }

    private void sendAttentionRoomTipMsg(long targetUid) {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(targetUid);
        if (roomInfo != null && userInfo != null) {
            long myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
            UserInfo myUserInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(myUid);

            RoomTipAttachment roomTipAttachment = new RoomTipAttachment(CustomAttachment.CUSTOM_MSG_HEADER_TYPE_ROOM_TIP, CustomAttachment.CUSTOM_MSG_SUB_TYPE_ROOM_TIP_ATTENTION_ROOM_OWNER);
            roomTipAttachment.setUid(myUid);
            roomTipAttachment.setNick(myUserInfo.getNick());
            roomTipAttachment.setTargetUid(targetUid);
            roomTipAttachment.setTargetNick(userInfo.getNick());

            roomTipAttachment.setCharmLevel(userInfo.getCharmLevel());
            roomTipAttachment.setExperLevel(userInfo.getExperLevel());
            ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                    // 聊天室id
                    roomInfo.getRoomId() + "",
                    // 自定义消息
                    roomTipAttachment
            );

            CoreManager.getCore(IRoomCore.class).sendMessage(message);
        }
    }
}
