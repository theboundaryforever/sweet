package com.tongdaxing.xchat_core.utils;

/**
 * Function:
 * Author: Edward on 2019/3/19
 */
public class BaseConstant {
    /**
     * 礼物占位符
     */
    public static final String GIFT_PLACEHOLDER = "gift_placeholder";
    /**
     * 等级占位符
     */
    public static final String LEVEL_PLACEHOLDER = "level_placeholder";
    /**
     * 标签占位符
     */
    public static final String LABEL_PLACEHOLDER = "label_placeholder";

}
