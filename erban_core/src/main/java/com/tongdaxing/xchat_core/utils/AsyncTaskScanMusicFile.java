package com.tongdaxing.xchat_core.utils;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.BlankUtil;
import com.tongdaxing.erban.libcommon.utils.file.BasicFileUtils;
import com.tongdaxing.xchat_core.player.IPlayerDbCore;
import com.tongdaxing.xchat_core.player.bean.LocalMusicInfo;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

public class AsyncTaskScanMusicFile extends AsyncTask<Context, Integer, Set<String>> {

    private Context mContext;
    private int minAudioDuration;

    private List<LocalMusicInfo> localSongs = new ArrayList<>();
    private ScanMediaCallback mCallback;

    private Set<CountDownLatch> countDowns;//等待所有扫描到的文件都添加媒体库完成

    public AsyncTaskScanMusicFile(Context mContext) {
        this.mContext = mContext;
        this.countDowns = new HashSet<>();
    }

    public AsyncTaskScanMusicFile(Context context, int minAudioDuration) {
        this(context);
        this.minAudioDuration = minAudioDuration;
    }

    public AsyncTaskScanMusicFile(Context context, int minAudioDuration, ScanMediaCallback callback) {
        this(context, minAudioDuration);
        mCallback = callback;
    }

    @Override
    protected Set<String> doInBackground(Context... params) {
        String filePath = "";
        List<String> downloadPaths = MusicFileUtil.getDownloadPaths();
        for (int i = 0; i< downloadPaths.size(); i++) {
            filePath = downloadPaths.get(i);
            File file = new File(filePath);
            if (file.exists() && file.isDirectory()) {
                File[] files = file.listFiles();
                if (files != null && files.length > 0) {
                    for (int j = 0; j < files.length; j++) {
                        String path = files[j].getAbsolutePath();
                        if (MusicFileUtil.AUDIO_SUFFIX_MP3.equals(BasicFileUtils.getFileExt(path))) {
                            addToMediaDb(path);
                        }
                    }
                }
            }
        }

        try {
            for (CountDownLatch latch : countDowns) {
                latch.await();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

//        BasicFileUtils.scanFileSystem(new BasicFileUtils.ScannedFileCallback() {
//            @Override
//            public void onScanned(String filePath, float weightOfTotalFile) {
//                //1.加入到媒体库
//                if (MusicFileUtil.AUDIO_SUFFIX_MP3.equals(BasicFileUtils.getFileExt(filePath))
////                        || MusicFileUtil.AUDIO_SUFFIX_AAC.equals(BasicFileUtils.getFileExt(filePath))
////                        || MusicFileUtil.AUDIO_SUFFIX_3GP.equals(BasicFileUtils.getFileExt(filePath))
////                        || MusicFileUtil.AUDIO_SUFFIX_WAV.equals(BasicFileUtils.getFileExt(filePath))
////                        || MusicFileUtil.AUDIO_SUFFIX_M4A.equals(BasicFileUtils.getFileExt(filePath))
////                        || MusicFileUtil.AUDIO_SUFFIX_FLAC.equals(BasicFileUtils.getFileExt(filePath))
//                        ) {
//                    addToMediaDb(filePath);
//                }
//            }
//        });
//        try {
//            for (CountDownLatch latch : countDowns) {
//                latch.await();
//            }
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        return null;
    }

    @Override
    protected void onPostExecute(Set<String> dirs) {
        saveLocalSongsToDb();
        if (mCallback != null) {
            mCallback.onComplete(true,localSongs);
        }
    }

    private void addToMediaDb(final String filePath) {
        LocalMusicInfo song = findInLastSongs(filePath);
        if (song != null) {
            addLocalSongToSongsList(song);
        } else {
            final CountDownLatch latch = new CountDownLatch(1);
            countDowns.add(latch);
            MediaScannerConnection.scanFile(mContext, new String[]{filePath}, MusicFileUtil.mimeTypes,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        @Override
                        public void onScanCompleted(String path, Uri uri) {
                            if (uri != null) {
                                LocalMusicInfo song = MusicFileUtil.getLocalSongFromUri(mContext,uri);
                                addLocalSongToSongsList(song);
                                latch.countDown();
                            }
                        }
                    });
        }
    }

    private LocalMusicInfo findInLastSongs(String filePath) {
        if (BlankUtil.isBlank(localSongs)) {
            return null;
        }
        synchronized (localSongs){
            for (LocalMusicInfo song : localSongs) {
                if (song != null && song.getLocalUri() != null && song.getLocalUri().equals(filePath)){
                    return song;
                }
            }
            return null;
        }
    }

    /**
     * 增加到本地音乐
     */
    private synchronized boolean addLocalSongToSongsList(LocalMusicInfo songInfo) {
        synchronized (localSongs){
            if (songInfo != null && songInfo.getDuration() > minAudioDuration) {
                songInfo.setDeleted(false);
                    localSongs.add(songInfo);
                return true;
            }
            return false;
        }
    }

    private void saveLocalSongsToDb() {
        CoreManager.getCore(IPlayerDbCore.class).replaceAllLocalMusics(localSongs);
    }

    /**
     * 扫描音乐回调
     */
    public interface ScanMediaCallback {

        /**
         * 扫描完成通知
         *
         * @param result 扫描结果是否成功 true成功
         */
        public void onComplete(boolean result, List<LocalMusicInfo> musicInfos);
    }
}
