package com.tongdaxing.xchat_core.utils;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.coremanager.IUserInfoCore;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.json.Json;

public class ChatUtil {

    /**
     * 禁言
     *
     * @return
     */
    public static boolean checkBanned() {
        Json json = CoreManager.getCore(IUserInfoCore.class).getBannedMap();
        boolean all = json.boo(IUserInfoCore.BANNED_ALL + "");
        boolean room = json.boo(IUserInfoCore.BANNED_ROOM + "");
        if (all || room) {
            SingleToastUtil.showToast("亲，由于您的发言违反了平台绿色公约，若有异议请联系客服");
            return true;
        }
        return false;
    }
}
