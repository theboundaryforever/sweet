package com.tongdaxing.xchat_core.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import com.tongdaxing.xchat_core.player.bean.LocalMusicInfo;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MusicFileUtil {

    public static final String tiantianMusicDownloadUri = "/tiantian/music/";
    public static final String AUDIO_SUFFIX_MP3 = ".mp3";
//    public static final String AUDIO_SUFFIX_AAC = ".aac";
//    public static final String AUDIO_SUFFIX_3GP = ".3gp";
//    public static final String AUDIO_SUFFIX_WAV = ".wav";
//    public static final String AUDIO_SUFFIX_FLAC = ".flac";
//    public static final String AUDIO_SUFFIX_M4A = ".m4a";

    public static final String[] mimeTypes = new String[]{
            AUDIO_SUFFIX_MP3
    };

    public static final String[] projections = new String[]{
            MediaStore.Audio.Media.TITLE,
            MediaStore.Audio.Media.ARTIST,// 艺术家
            MediaStore.Audio.Media.ALBUM,// 专辑
            MediaStore.Audio.AudioColumns.ALBUM_ID,//专辑ID
            MediaStore.Audio.Media.DURATION,
            MediaStore.Audio.AudioColumns.DISPLAY_NAME,
            MediaStore.Audio.Media.YEAR,
            MediaStore.Audio.Media.DATA,
            MediaStore.Audio.Media._ID};

    private static List<String> downloadPaths;

    public static List<String> getDownloadPaths(){
        String rootPath = Environment.getExternalStorageDirectory() + "/";
        if(null == downloadPaths){
            downloadPaths = new ArrayList<>();
            downloadPaths.add(rootPath+"kgmusic/download/");//酷狗目录
            downloadPaths.add(rootPath+"qqmusic/song/");//qq音乐
            downloadPaths.add(rootPath+"netease/cloudmusic/Music/");//网易云音乐
            downloadPaths.add(rootPath+"KuwoMusic/music/");//酷我音乐
            downloadPaths.add(rootPath+"xiami/audios/");//虾米音乐
            downloadPaths.add(rootPath+"Baidu_music/download/");//百度音乐
            downloadPaths.add(rootPath+"Music/");//媒体库
            downloadPaths.add(rootPath+"MIUI/music/mp3/");//媒体库
            downloadPaths.add(rootPath+"Smartisan/music/cloud/");//媒体库
            downloadPaths.add(rootPath+"Music/Download/");//媒体库
            downloadPaths.add(rootPath+"Samsung/Music/Download/");//媒体库
            downloadPaths.add(rootPath+"i音乐/歌曲/");//媒体库
            downloadPaths.add(rootPath+"tiantian/music/");//甜甜语音新旧包共用
        }

        createMusicDownloadDir();

        return downloadPaths;
    }

    public static final Uri audioUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

    public static void createMusicDownloadDir(){
        File file = new File(getMusicDownloadDirByApp());
        if(null != file && !file.exists()){
            file.mkdirs();
        }
    }

    public static String getMusicDownloadDirByApp(){

        return Environment.getExternalStorageDirectory() + tiantianMusicDownloadUri;
    }

    public static String getMusicDownloadFilePath(String singName, long serverId){
        StringBuilder sb = new StringBuilder(Environment.getExternalStorageDirectory().getPath());
        sb.append(tiantianMusicDownloadUri);
        sb.append(singName);
//        sb.append(serverId);
        sb.append(AUDIO_SUFFIX_MP3);
        return sb.toString();
    }

    /**
     * 获取本地扫描音乐信息
     *
     * @param audioIdUri 音频ID URI
     *
     * @return
     */
    public static LocalMusicInfo getLocalSongFromUri(Context context, Uri audioIdUri) {
        LocalMusicInfo localSongInfo = null;
        if (isAudioUri(audioIdUri)) {
            ContentResolver contentResolver = context.getContentResolver();
            Cursor mCursor = contentResolver.query(audioIdUri, projections, null, null, null);
            if (mCursor != null && mCursor.moveToFirst()) {
                localSongInfo = getLocalSong(mCursor);
            }
            if (mCursor != null && !mCursor.isClosed()) {
                mCursor.close();
            }
        }
        return localSongInfo;
    }

    public static boolean isAudioUri(Uri audioIdUri) {
        if (audioIdUri == null) {
            return false;
        }
        return audioIdUri.getPath().contains(audioUri.getPath());
    }

    /**
     * 获取本地音乐
     */
    public static  LocalMusicInfo getLocalSong(Cursor cursor) {
        LocalMusicInfo song = new LocalMusicInfo();
        song.setSongName(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE)));
        song.setYear(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.YEAR)));
        song.setAlbumName(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM)));
        song.setDuration(cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION)));//与服务端统一精度 精确到毫秒
        List<String> artistNames = new ArrayList<>();
        artistNames.add(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST)));
        song.setArtistName(artistNames);
        song.setLocalUri(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA)));
        song.setLocalId(cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media._ID)));
        song.setSongId(SongUtils.generateThirdPartyId());
        return song;
    }
}
