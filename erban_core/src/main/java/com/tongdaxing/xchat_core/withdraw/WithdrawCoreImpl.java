package com.tongdaxing.xchat_core.withdraw;

import com.tongdaxing.erban.libcommon.coremanager.AbstractBaseCore;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.result.ExchangeInfoResult;
import com.tongdaxing.xchat_core.result.WithdrawListResult;
import com.tongdaxing.xchat_core.result.WithdrawUserInfoResult;

import java.util.Map;

/**
 * Created by Administrator on 2017/7/24.
 */

public class WithdrawCoreImpl extends AbstractBaseCore implements IWithdrawCore {
    //获取提现列表
    @Override
    public void getWithdrawList() {

        OkHttpManager.getInstance().getRequest(UriProvider.getWithdrawList(),
                CommonParamUtil.getDefaultParam(), new OkHttpManager.MyCallBack<WithdrawListResult>() {
                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                        notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_GET_WITHDRAW_LIST_FAIL, e.getMessage());
                    }

                    @Override
                    public void onResponse(WithdrawListResult response) {
                        if (response != null) {
                            if (response.isSuccess()) {
                                notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_GET_WITHDRAW_LIST, response.getData());
                            } else {
                                notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_GET_WITHDRAW_LIST_FAIL, response.getMessage());
                            }
                        } else {
                            notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_GET_WITHDRAW_LIST_FAIL);
                        }
                    }
                });
    }


    //获取提现页用户信息
    @Override
    public void getWithdrawUserInfo(long uid) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(uid));

        OkHttpManager.getInstance().getRequest(UriProvider.getWithdrawInfo(), param, new OkHttpManager.MyCallBack<WithdrawUserInfoResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_GET_WITHDRAW_USER_INFO_FAIL, e.getMessage());
            }

            @Override
            public void onResponse(WithdrawUserInfoResult response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_GET_WITHDRAW_USER_INFO, response.getData());
                    } else {
                        notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_GET_WITHDRAW_USER_INFO_FAIL, response.getMessage());
                    }
                } else {
                    notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_GET_WITHDRAW_USER_INFO_FAIL);
                }
            }
        });
    }


    //发起兑换
    @Override
    public void requestExchange(long uid, int pid, int type) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));
        params.put("pid", String.valueOf(pid));
        params.put("type", type + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());

        OkHttpManager.getInstance().postRequest(UriProvider.requestExchangeV2Url(), params, new OkHttpManager.MyCallBack<ExchangeInfoResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_REQUEST_EXCHANGE_FAIL, -1, e.getMessage());
            }

            @Override
            public void onResponse(ExchangeInfoResult response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_REQUEST_EXCHANGE, response.getData());
                    } else {
                        notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_REQUEST_EXCHANGE_FAIL, response.getCode(), response.getMessage());
                    }
                } else {
                    notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_REQUEST_EXCHANGE_FAIL, -1, null);
                }
            }
        });
    }

    //获取绑定支付宝验证码
    @Override
    public void getSmsCode(long uid) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));

        OkHttpManager.getInstance().getRequest(UriProvider.getSms(), params, new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_GET_SMS_CODE_FAIL, e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                    } else {
                        notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_GET_SMS_CODE_FAIL, response.getMessage());
                    }
                }
            }
        });
    }


    //绑定支付宝
    @Override
    public void binderAlipay(String aliPayAccount, String aliPayAccountName, String code) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("aliPayAccount", aliPayAccount);
        params.put("aliPayAccountName", aliPayAccountName);
        params.put("code", code);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());

        OkHttpManager.getInstance().postRequest(UriProvider.binder(), params, new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_BINDER_ALIPAY_FAIL, e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_BINDER_ALIPAY);
                    } else {
                        notifyClients(IWithdrawCoreClient.class, IWithdrawCoreClient.METHOD_ON_BINDER_ALIPAY_FAIL, response.getMessage());
                    }
                }
            }
        });
    }


}
