package com.tongdaxing.xchat_core.withdraw.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/7/25.
 */

public class WithdrawInfo implements Serializable{

    /*
        uid:90000
		alipayAccount:xxx //支付宝账号
		alipayAccountName:xxx //支付宝姓名
		diamondNum:钻石余额
		isNotBoundPhone:true//没有绑定手机，绑定了手机不返回该字段
     */
    public long uid;
    public String alipayAccount;
    public double diamondNum;
    public String alipayAccountName;
    public boolean isNotBoundPhone;

    public int withDrawType = 0;//1-支付宝，2-微信
    public boolean hasWx = false;//是否绑定了微信帐号,0-否，1-是
}
