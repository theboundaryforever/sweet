package com.tongdaxing.xchat_core;


import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.erban.libcommon.utils.pref.CommonPref;

/**
 * 环境配置类
 */
public class Env {

    public static final String PREF_SVC_SETTING = "PREF_SVC_SETTING";

    public static final String PREF_SVC_BROADCAST_SETTING = "PREF_SVC_BROADCAST_SETTING";

    public static final String PREF_URI_SETTING = "PREF_URI_SETTING";


    private static final Env mEnv = new Env();

    private Env() {

    }

    public static Env instance() {
        return mEnv;
    }

    public void init() {
        if (BasicConfig.INSTANCE.isDebuggable()) {
            UriProvider.init(UriSetting.Test);
        } else {
            UriProvider.init(UriSetting.Product);
        }
    }


    public enum UriSetting {
        Dev, Product, Test;
    }


    public boolean isUriDev() {
        return getUriSetting() == Env.UriSetting.Dev;
    }


    /**
     * 取uri环境
     *
     * @return
     */
    public UriSetting getUriSetting() {
        if (BasicConfig.INSTANCE.isDebuggable()) {
            int ordinal = CommonPref.instance(BasicConfig.INSTANCE.getAppContext()).getInt(PREF_URI_SETTING, -1);
            if (ordinal > -1 && ordinal < UriSetting.values().length) {
                return UriSetting.values()[ordinal];
            }
        }
        return UriSetting.Product;
    }
}
