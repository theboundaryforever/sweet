package com.tongdaxing.xchat_core.pair;

import com.tongdaxing.erban.libcommon.coremanager.IBaseCore;
import com.tongdaxing.erban.libcommon.utils.json.Json;

public interface IPairCore extends IBaseCore {

    void startPair(String roomId);

    void stopPair(String roomId);

    void choiceLover(String pickUid, String roomId);

    void getPairInfo(String roomId);


    Json checkPair(Json pairInfo);

    void parsePairSelectMicPosition(Json pairInfo);

    void updatePairSelectMicPosition();

    void clearPairInfo();
}
