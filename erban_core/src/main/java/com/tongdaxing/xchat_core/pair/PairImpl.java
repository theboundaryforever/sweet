package com.tongdaxing.xchat_core.pair;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.SparseArray;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tongdaxing.erban.libcommon.coremanager.AbstractBaseCore;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class PairImpl extends AbstractBaseCore implements IPairCore {
    @Override
    public void startPair(String roomId) {
        Map<String, String> param = OkHttpManager.getDefaultParam();
        param.put("ticket", getTicket());
        param.put("uid", getMyUid());
        param.put("roomId", roomId);
        OkHttpManager.getInstance().postRequest(UriProvider.getPairStart(), param, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                String s = "网络异常";
                makeToast(s);
            }

            @Override
            public void onResponse(Json response) {
                if (response.num("code") != 200) {
                    makeToast(response.str("message", "网络异常"));
                    return;
                }
                makeToast("开始相亲");
            }
        });
    }

    private void makeToast(String s) {
        CoreManager.notifyClients(IPairClient.class, IPairClient.onResponseToast, s);
    }

    @NonNull
    private String getMyUid() {
        return CoreManager.getCore(IAuthCore.class).getCurrentUid() + "";
    }


    private String getTicket() {
        return CoreManager.getCore(IAuthCore.class).getTicket();
    }

    @Override
    public void stopPair(String roomId) {
        Map<String, String> param = OkHttpManager.getDefaultParam();
        param.put("ticket", getTicket());
        param.put("uid", getMyUid());
        param.put("roomId", roomId);
        OkHttpManager.getInstance().postRequest(UriProvider.getPairStop(), param, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                String s = "网络异常";
                makeToast(s);
            }

            @Override
            public void onResponse(Json response) {
                if (response.num("code") != 200) {
                    makeToast(response.str("message", "网络异常"));
                    return;
                }
                makeToast("结束相亲");
            }
        });
    }

    /**
     * @param pickUid 选中对象的uid
     * @param roomId
     */
    @Override
    public void choiceLover(String pickUid, String roomId) {
        Map<String, String> param = OkHttpManager.getDefaultParam();
        param.put("ticket", getTicket());
        param.put("uid", getMyUid());
        param.put("roomId", roomId);
        param.put("pickUid", pickUid);
        OkHttpManager.getInstance().postRequest(UriProvider.getPairPick(), param, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                String s = "网络异常";
                makeToast(s);
            }

            @Override
            public void onResponse(Json response) {
                if (response.num("code") != 200) {
                    makeToast(response.str("message", "网络异常"));
                    return;
                }
                makeToast("选择成功");
            }
        });
    }

    @Override
    public void getPairInfo(String roomId) {
        Map<String, String> param = OkHttpManager.getDefaultParam();
        param.put("ticket", getTicket());
        param.put("uid", getMyUid());
        param.put("roomId", roomId);

        OkHttpManager.getInstance().getRequest(UriProvider.getPairInfo(), param, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {

            }

            @Override
            public void onResponse(Json response) {
                if (response.num("code") != 200) {
                    return;
                }
                CoreManager.notifyClients(IPairClient.class, IPairClient.onResponsePairInfo, response.json_ok("data"));
            }
        });
    }

    @Override
    public Json checkPair(Json pairJson) {
        Json markPairJson = new Json();
        if (pairJson == null || pairJson.key_names().length < 1){
            return markPairJson;
        }

        SparseArray<RoomQueueInfo> mMicQueueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;
        int size = mMicQueueMemberMap.size();

        Set<String> uidSet = new HashSet<>();
        Map<String,ChatRoomMember> micPosiMap = new HashMap<>();

        for (int i = 0; i < size; i++) {
            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(i);
            if (roomQueueInfo != null && roomQueueInfo.mChatRoomMember != null) {
                ChatRoomMember mChatRoomMember = roomQueueInfo.mChatRoomMember;
                String key = mChatRoomMember.getAccount();
                uidSet.add(key);
                micPosiMap.put(key,mChatRoomMember);
            }
        }

        //不算房主
        for (int i = 1; i < size; i++) {
            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(i);
            if (roomQueueInfo != null && roomQueueInfo.mChatRoomMember != null) {
                ChatRoomMember mChatRoomMember = roomQueueInfo.mChatRoomMember;
                String key = mChatRoomMember.getAccount();
                long tagUid = pairJson.num_l(key);
                //判断在麦上且是否互选
                if (tagUid > 0 && uidSet.contains(tagUid + "")) {
                    long tagMatchUid = pairJson.num_l(tagUid + "");
                    //对方选择的是自己---这里时自动计算配对成功的
                    if (("" + tagMatchUid).equals(key)) {
                        String markKey = getMarkKey(tagUid, tagMatchUid);
                        Json pairUser = new Json();
                        pairUser.set(Constants.USER_UID, mChatRoomMember.getAccount());
                        pairUser.set(Constants.USER_NICK, mChatRoomMember.getNick());
                        pairUser.set(Constants.USER_AVATAR, mChatRoomMember.getAvatar());
                        pairUser.set(Constants.USER_GENDER, roomQueueInfo.gender);

                        Map<String, Object> extenMap = mChatRoomMember.getExtension();
                        if(null != extenMap){
                            if(extenMap.containsKey(Constants.USER_MEDAL_ID)){
                                pairUser.set(Constants.USER_MEDAL_ID,(int)extenMap.get(Constants.USER_MEDAL_ID));
                            }
                            if(extenMap.containsKey(Constants.USER_MEDAL_DATE)){
                                pairUser.set(Constants.USER_MEDAL_DATE,(int)extenMap.get(Constants.USER_MEDAL_DATE));
                            }
                            if(extenMap.containsKey(Constants.NOBLE_INVISIABLE_ENTER_ROOM)){
                                pairUser.set(Constants.NOBLE_INVISIABLE_ENTER_ROOM, (int) extenMap.get(Constants.NOBLE_INVISIABLE_ENTER_ROOM));
                            }
                        }

                        //匹配成功的以markKey为key放在同一个json里
                        boolean has = markPairJson.has(markKey);
                        Json json;
                        if (has) {
                            json = markPairJson.json_ok(markKey);
                        } else {
                            json = new Json();
                        }
                        json.set(key, pairUser);
                        //json_ok这个方法获取到的不是json中的对象，是一个新的对象所以要把旧的覆盖
                        markPairJson.set(markKey, json);
                    }
                }
            }
        }
        return markPairJson;
    }

    private Json lastPairInfo = null;

    @Override
    public void clearPairInfo() {
        lastPairInfo = null;
    }

    @Override
    public synchronized void parsePairSelectMicPosition(Json pairInfo) {
        lastPairInfo = pairInfo;
        updatePairSelectMicPosition();
    }

    @Override
    public synchronized void updatePairSelectMicPosition() {
        if(null != lastPairInfo){
            //mic位更新后，直接重新检测数据，重新生成map，刷新adapter
            if(null != AvRoomDataManager.get().pairSelectMap){
                synchronized (AvRoomDataManager.get().pairSelectMap){
                    if (lastPairInfo != null && lastPairInfo.key_names().length >= 1){
                        AvRoomDataManager.get().pairSelectMap.clear();
                        SparseArray<RoomQueueInfo> mMicQueueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;
                        int size = mMicQueueMemberMap.size();
                        Map<String,RoomQueueInfo> micPosiMap = new HashMap<>();
                        for (int i = 1; i < size; i++) {
                            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(i);
                            if (roomQueueInfo != null && roomQueueInfo.mChatRoomMember != null && null != roomQueueInfo.mRoomMicInfo) {
                                ChatRoomMember mChatRoomMember = roomQueueInfo.mChatRoomMember;
                                String key = mChatRoomMember.getAccount();
                                micPosiMap.put(key,roomQueueInfo);
                            }
                        }

                        //不算房主
                        for (RoomQueueInfo roomQueueInfo : micPosiMap.values()) {
                            if (roomQueueInfo != null && roomQueueInfo.mChatRoomMember != null && null != roomQueueInfo.mRoomMicInfo) {
                                ChatRoomMember mChatRoomMember = roomQueueInfo.mChatRoomMember;
                                //循环麦上帐号
                                String key = mChatRoomMember.getAccount();
                                //麦上帐号有选择心动对象
                                String tagUid = lastPairInfo.str(key);
                                //判断心动帐号是否在麦上
                                if (!TextUtils.isEmpty(tagUid) && micPosiMap.containsKey(tagUid)) {
                                    AvRoomDataManager.get().pairSelectMap.put(key,micPosiMap.get(tagUid).mRoomMicInfo.getPosition());
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @NonNull
    private String getMarkKey(long tagUid, long tagMatchUid) {
        return (tagMatchUid + tagUid) + "" + Math.abs(tagMatchUid - tagUid);
    }


}
