package com.tongdaxing.xchat_core.pair;

import com.tongdaxing.erban.libcommon.coremanager.ICoreClient;
import com.tongdaxing.erban.libcommon.utils.json.Json;

public interface IPairClient extends ICoreClient {

    String onResponseToast = "onResponseToast";
    String onResponsePairInfo = "onResponsePairInfo";

    String onResponsePairInfo(Json json);

    String onResponseToast(String toastContent);
}
