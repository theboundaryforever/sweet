package com.tongdaxing.xchat_core.share;

/**
 * 应用于h5页面分享上报接口的shareType，拷贝自IOS 甜甜客户端
 */
public class SSDKPlatformType {
    /**
     * 未知
     */
    public static final int SSDKPlatformTypeUnknown = 0;
    /**
     * 新浪微博
     */
    public static final int SSDKPlatformTypeSinaWeibo = 23;
    /**
     * 腾讯微博
     */
    public static final int SSDKPlatformTypeTencentWeibo = 24;
    /**
     * 豆瓣
     */
    public static final int SSDKPlatformTypeDouBan = 5;

    /**
     * 人人网
     */
    public static final int SSDKPlatformTypeRenren = 7;
    /**
     * 开心网
     */
    public static final int SSDKPlatformTypeKaixin = 8;
    /**
     * Facebook
     */
    public static final int SSDKPlatformTypeFacebook = 10;
    /**
     * Twitter
     */
    public static final int SSDKPlatformTypeTwitter = 11;
    /**
     * 印象笔记
     */
    public static final int SSDKPlatformTypeYinXiang = 12;
    /**
     * Google+
     */
    public static final int SSDKPlatformTypeGooglePlus = 14;
    /**
     * Instagram
     */
    public static final int SSDKPlatformTypeInstagram = 15;
    /**
     * LinkedIn
     */
    public static final int SSDKPlatformTypeLinkedIn = 16;
    /**
     * Tumblr
     */
    public static final int SSDKPlatformTypeTumblr = 17;
    /**
     * 邮件
     */
    public static final int SSDKPlatformTypeMail = 18;
    /**
     * 短信
     */
    public static final int SSDKPlatformTypeSMS = 19;
    /**
     * 打印
     */
    public static final int SSDKPlatformTypePrint = 20;
    /**
     * 拷贝
     */
    public static final int SSDKPlatformTypeCopy = 21;
    /**
     * 微信好友
     */
    public static final int SSDKPlatformSubTypeWechatSession = 22;
    /**
     * 微信朋友圈
     */
    public static final int SSDKPlatformSubTypeWechatTimeline = 1;
    /**
     * QQ好友
     */
    public static final int SSDKPlatformSubTypeQQFriend = 6;

    /**
     * QQ空间
     */
    public static final int SSDKPlatformSubTypeQZone = 2;

    /**
     * Instapaper
     */
    public static final int SSDKPlatformTypeInstapaper = 25;
    /**
     * Pocket
     */
    public static final int SSDKPlatformTypePocket = 26;
    /**
     * 有道云笔记
     */
    public static final int SSDKPlatformTypeYouDaoNote = 27;
    /**
     * Pinterest
     */
    public static final int SSDKPlatformTypePinterest = 30;
    /**
     * Flickr
     */
    public static final int SSDKPlatformTypeFlickr = 34;
    /**
     * Dropbox
     */
    public static final int SSDKPlatformTypeDropbox = 35;
    /**
     * VKontakte
     */
    public static final int SSDKPlatformTypeVKontakte = 36;
    /**
     * 微信收藏
     */
    public static final int SSDKPlatformSubTypeWechatFav = 37;
    /**
     * 易信好友
     */
    public static final int SSDKPlatformSubTypeYiXinSession = 38;
    /**
     * 易信朋友圈
     */
    public static final int SSDKPlatformSubTypeYiXinTimeline = 39;
    /**
     * 易信收藏
     */
    public static final int SSDKPlatformSubTypeYiXinFav = 40;
    /**
     * 明道
     */
    public static final int SSDKPlatformTypeMingDao = 41;
    /**
     * Line
     */
    public static final int SSDKPlatformTypeLine = 42;
    /**
     * WhatsApp
     */
    public static final int SSDKPlatformTypeWhatsApp = 43;
    /**
     * KaKao Talk
     */
    public static final int SSDKPlatformSubTypeKakaoTalk = 44;
    /**
     * KaKao Story
     */
    public static final int SSDKPlatformSubTypeKakaoStory = 45;
    /**
     * Facebook Messenger
     */
    public static final int SSDKPlatformTypeFacebookMessenger = 46;
    /**
     * 支付宝好友
     */
    public static final int SSDKPlatformTypeAliSocial = 50;
    /**
     * 支付宝朋友圈
     */
    public static final int SSDKPlatformTypeAliSocialTimeline = 51;
    /**
     * 钉钉
     */
    public static final int SSDKPlatformTypeDingTalk = 52;
    /**
     * youtube
     */
    public static final int SSDKPlatformTypeYouTube = 53;
    /**
     * 美拍
     */
    public static final int SSDKPlatformTypeMeiPai = 54;
    /**
     * 易信
     */
    public static final int SSDKPlatformTypeYiXin = 994;
    /**
     * KaKao
     */
    public static final int SSDKPlatformTypeKakao = 995;
    /**
     * 印象笔记国际版
     */
    public static final int SSDKPlatformTypeEvernote = 996;
    /**
     * 微信平台,
     */
    public static final int SSDKPlatformTypeWechat = 3;
    /**
     * QQ平台
     */
    public static final int SSDKPlatformTypeQQ = 4;
    /**
     * 任意平台
     */
    public static final int SSDKPlatformTypeAny = 999;
}
