package com.tongdaxing.xchat_core.share;

import android.text.TextUtils;

import com.netease.nimlib.sdk.chatroom.ChatRoomMessageBuilder;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.tongdaxing.erban.libcommon.coremanager.AbstractBaseCore;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomTipAttachment;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.redpacket.bean.WebViewInfo;
import com.tongdaxing.xchat_core.result.ShareRedPacketResult;
import com.tongdaxing.xchat_core.room.IRoomCore;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.HashMap;
import java.util.Map;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;

/**
 * Created by chenran on 2017/8/14.
 */

public class ShareCoreImpl extends AbstractBaseCore implements IShareCore {

    private final String TAG = ShareCoreImpl.class.getSimpleName();

    public ShareCoreImpl() {
        CoreManager.addClient(this);
    }

    @Override
    public void shareH5(final WebViewInfo webViewInfo, Platform platform) {
        if (null != webViewInfo && platform != null) {
            Platform.ShareParams sp = new Platform.ShareParams();
            sp.setText(webViewInfo.getDesc());
            sp.setTitle(webViewInfo.getTitle());
            sp.setImageUrl(webViewInfo.getImgUrl());
            //QQ空间分享
            sp.setSite(webViewInfo.getDesc());
            String showUrl = webViewInfo.getShowUrl();
            LogUtils.d(TAG,"shareH5-showUrl0:"+showUrl);
            if (showUrl != null && showUrl.startsWith("/")) {
                showUrl = UriProvider.IM_SERVER_URL + showUrl;
                LogUtils.d(TAG,"shareH5-showUrl1:"+showUrl);
            }
            if(TextUtils.isEmpty(showUrl)){
               return;
            }
            final String finalUrl = showUrl.concat("?shareUid=").concat(String.valueOf(CoreManager
                    .getCore(IAuthCore.class).getCurrentUid()));
            sp.setSiteUrl(finalUrl);
            //QQ分享
            sp.setTitleUrl(finalUrl);
            //微信朋友圈分享
            sp.setUrl(finalUrl);
            sp.setShareType(Platform.SHARE_WEBPAGE);
            platform.setPlatformActionListener(new PlatformActionListener() {
                @Override
                public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {
                    String url = UriProvider.getLotteryActivityPage();
                    LogUtils.d(TAG,"shareH5-onComplete url:"+url);
                    LogUtils.d(TAG,"shareH5-onComplete finalUrl:"+finalUrl);
                    //Android端--房间外面的分享链接，sharePageId=888
                    if (finalUrl.contains("/ttyy/luckdraw/index.html")) {
                        //Android端同IOS端分享链接中，shareType值是统一的
                        reportShare(888, 0L, 0, platform);
                    } else {
                        //Android端--房间内的分享链接，sharePageId=1
                        reportShare(1, 0L, 0, platform);
                    }
                    notifyClients(IShareCoreClient.class, IShareCoreClient.METHOD_ON_SHARE_H5, url);
                }

                @Override
                public void onError(Platform platform, int i, Throwable throwable) {
                    if(null != throwable){
                        throwable.printStackTrace();
                    }
                    LogUtils.d(TAG,"shareH5-onError i:"+i);
                    notifyClients(IShareCoreClient.class, IShareCoreClient.METHOD_ON_HSARE_H5_FAIL);
                }

                @Override
                public void onCancel(Platform platform, int i) {
                    LogUtils.d(TAG,"shareH5-onCancel i:"+i);
                    notifyClients(IShareCoreClient.class, IShareCoreClient.METHOD_ON_HSARE_H5_CANCEL);
                }
            });
            platform.share(sp);
        }
    }

    @Override
    public void shareRoom(Platform platform, final long roomUid, String title, int roomType) {
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(roomUid);//房间主人信息
        //无UI API
        if (userInfo != null && platform != null) {
            Platform.ShareParams sp = new Platform.ShareParams();
            sp.setText("每一次心动都是因为你，喜欢与感动都在声音里。");
            UserInfo cacheLoginUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            String nick;
            if (cacheLoginUserInfo != null) {
                nick = cacheLoginUserInfo.getNick();
            } else {
                nick = userInfo.getNick();
            }

            if (nick != null && nick.length() > 7) {
                nick = nick.substring(0, 7) + "…";
            }
            sp.setTitle(nick + "正邀请你连麦互动");
            sp.setImageUrl(userInfo.getAvatar());
            //QQ空间分享
            sp.setSite("每一次心动都是因为你，喜欢与感动都在声音里。");
            String uid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            String roomuid = String.valueOf(roomUid);
            String shareUrl = UriProvider.JAVA_WEB_TMXQ_URL.concat("/ttyy/share_tmxq/share.html?shareUid=")
                    .concat(uid).concat("&uid=").concat(roomuid).concat("&roomType=").concat(String.valueOf(roomType));
            LogUtils.d(TAG,"shareRoom shareUrl:"+shareUrl);
            sp.setSiteUrl(shareUrl);
            //QQ分享
            sp.setTitleUrl(shareUrl);
            //微信朋友圈分享
            sp.setUrl(shareUrl);
            sp.setShareType(Platform.SHARE_WEBPAGE);


            platform.setPlatformActionListener(new PlatformActionListener() {
                @Override
                public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {
                    sendShareRoomTipMsg(roomUid);
                    reportShare(1, roomUid, roomType, platform);
                    notifyClients(IShareCoreClient.class, IShareCoreClient.METHOD_ON_SHARE_ROOM);
                    LogUtils.d(TAG,"shareRoom-onComplete i:"+i);
                }

                @Override
                public void onError(Platform platform, int i, Throwable throwable) {
                    notifyClients(IShareCoreClient.class, IShareCoreClient.METHOD_ON_SHARE_ROOM_FAIL);
                    if(null != throwable){
                        throwable.printStackTrace();
                    }
                    LogUtils.d(TAG,"shareRoom-onError i:"+i);
                }

                @Override
                public void onCancel(Platform platform, int i) {
                    notifyClients(IShareCoreClient.class, IShareCoreClient.METHOD_ON_SHARE_ROOM_CANCEL);
                    LogUtils.d(TAG,"shareRoom-onCancel i:"+i);
                }
            });
            platform.share(sp);
        }
    }

    @Override
    public void reportShare(int sharePageId, long roomUid, int roomType, Platform platform) {
        LogUtils.d(TAG, "reportShare sharePageId:" + sharePageId + " roomUid:" + roomUid + " roomType:" + roomType);
        int shareType = 0;
        //目前服务器最大支持的shareType值没有超过10
        if (platform.getName() == Wechat.NAME) {
            shareType = SSDKPlatformType.SSDKPlatformTypeWechat;
        } else if (platform.getName() == WechatMoments.NAME) {
            shareType = SSDKPlatformType.SSDKPlatformSubTypeWechatTimeline;
        } else if (platform.getName() == QQ.NAME) {
            shareType = SSDKPlatformType.SSDKPlatformTypeQQ;
        } else if (platform.getName() == QZone.NAME) {
            shareType = SSDKPlatformType.SSDKPlatformSubTypeQZone;
        }
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("shareType", String.valueOf(shareType));
        params.put("sharePageId", sharePageId + "");
        params.put("roomUid", roomUid + "");
        params.put("roomType", roomType + "");
        params.put("token", CoreManager.getCore(IAuthCore.class).getTicket());

        OkHttpManager.getInstance().postRequest(UriProvider.getShareRedPacket(), params, new OkHttpManager.MyCallBack<ShareRedPacketResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                LogUtils.d(TAG,"reportShare-onError sharePageId:"+sharePageId);
            }

            @Override
            public void onResponse(ShareRedPacketResult response) {
                LogUtils.d(TAG,"reportShare-onResponse sharePageId:"+sharePageId);
            }
        });
    }

    private void sendShareRoomTipMsg(long targetUid) {
        LogUtils.d(TAG,"sendShareRoomTipMsg targetUid:"+targetUid);
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(targetUid);
        if (roomInfo != null && userInfo != null) {
            long myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
            UserInfo myUserInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(myUid);

            RoomTipAttachment roomTipAttachment = new RoomTipAttachment(CustomAttachment.CUSTOM_MSG_HEADER_TYPE_ROOM_TIP, CustomAttachment.CUSTOM_MSG_SUB_TYPE_ROOM_TIP_SHARE_ROOM);
            roomTipAttachment.setUid(myUid);
            roomTipAttachment.setNick(myUserInfo.getNick());
            roomTipAttachment.setTargetUid(targetUid);
            roomTipAttachment.setTargetNick(userInfo.getNick());

            ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                    // 聊天室id
                    roomInfo.getRoomId() + "",
                    // 自定义消息
                    roomTipAttachment
            );

            CoreManager.getCore(IRoomCore.class).sendMessage(message);
        }
    }
}
