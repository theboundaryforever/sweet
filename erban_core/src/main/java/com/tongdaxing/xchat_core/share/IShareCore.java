package com.tongdaxing.xchat_core.share;

import com.tongdaxing.erban.libcommon.coremanager.IBaseCore;
import com.tongdaxing.xchat_core.redpacket.bean.WebViewInfo;

import cn.sharesdk.framework.Platform;

/**
 * Created by chenran on 2017/8/14.
 */

public interface IShareCore extends IBaseCore{
    void shareH5(WebViewInfo webViewInfo, Platform platform);

    void shareRoom(Platform platform, long roomUid, String title, int roomType);

    void reportShare(int sharePageId, long roomUid, int roomType, Platform platform);
}
