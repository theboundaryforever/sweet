package com.tongdaxing.xchat_core.home;

import java.util.List;

/**
 * <p> 首页数据 </p>
 *
 * @author Administrator
 * @date 2017/11/22
 */
public class HomeInfo {
    public List<BannerInfo> banners;
    /**
     * 排行数据
     */
    public RankingInfo rankHome;
    /**
     * 热门推荐
     */
    public List<HomeRoom> hotRooms;
    /**
     * 房间推荐
     */
    public List<HomeRoom> listRoom;

    public List<HomeIcon> homeIcons;

    public List<HomeRoom> listGreenRoom;

    @Override
    public String toString() {
        return "HomeInfo{" +
                "banners=" + banners +
                ", rankHome=" + rankHome +
                ", hotRooms=" + hotRooms +
                ", listRoom=" + listRoom +
                '}';
    }
}
