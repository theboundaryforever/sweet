package com.tongdaxing.xchat_core.home;

import android.os.Parcel;
import android.os.Parcelable;

import com.tongdaxing.xchat_core.room.bean.RoomInfo;

/**
 * @author zhouxiangfeng
 * @date 2017/5/17
 */

public class HomeRoom extends RoomInfo implements Parcelable {
    //性别 1:男 2：女 0 ：未知
    private int gender;
    //头像
    private String avatar;

    private String roomFrame;

    private String nick;

    private int seqNo;

    public int showLine = 0;

    private String roomLevelIcon = null;

    public String getRoomLevelIcon() {
        return roomLevelIcon;
    }

    public void setRoomLevelIcon(String roomLevelIcon) {
        this.roomLevelIcon = roomLevelIcon;
    }

    public int getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    @Override
    public String toString() {
        return "HomeRoom{" +
                "gender=" + gender +
                ", avatar='" + avatar + '\'' +
                ", nick='" + nick + '\'' +
                ", erbanNo=" + erbanNo +
                ", seqNo=" + seqNo +
                ", showLine=" + showLine +
                ", roomLevelIcon=" + roomLevelIcon +
                ", badge='" + badge + '\'' +
                '}';
    }

    //角标相关的

    public String badge;


    protected HomeRoom(Parcel in) {
        super(in);
        gender = in.readInt();
        avatar = in.readString();
        nick = in.readString();
        erbanNo = in.readLong();
        badge = in.readString();
        seqNo = in.readInt();

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(gender);
        dest.writeString(avatar);
        dest.writeString(nick);
        dest.writeLong(erbanNo);
        dest.writeString(badge);
        dest.writeInt(seqNo);

    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<HomeRoom> CREATOR = new Creator<HomeRoom>() {
        @Override
        public HomeRoom createFromParcel(Parcel in) {
            return new HomeRoom(in);
        }

        @Override
        public HomeRoom[] newArray(int size) {
            return new HomeRoom[size];
        }
    };

    @Override
    public int getOnlineNum() {
        return onlineNum;
    }

    @Override
    public void setOnlineNum(int onlineNum) {
        this.onlineNum = onlineNum;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getRoomFrame() {
        return roomFrame;
    }

    public void setRoomFrame(String roomFrame) {
        this.roomFrame = roomFrame;
    }
}
