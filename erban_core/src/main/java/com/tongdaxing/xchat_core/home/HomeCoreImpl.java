package com.tongdaxing.xchat_core.home;

import com.tongdaxing.erban.libcommon.coremanager.AbstractBaseCore;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.result.HomeTabResult;
import com.tongdaxing.xchat_core.result.NotHotResult;
import com.tongdaxing.xchat_core.result.TabResult;

import java.util.List;
import java.util.Map;

/**
 * @author zhouxiangfeng
 * @date 2017/5/17
 */

public class HomeCoreImpl extends AbstractBaseCore implements IHomeCore {
    private static final String TAG = "HomeCoreImpl";

    private List<TabInfo> mTabInfoList;

    @Override
    public List<TabInfo> getMainTabInfos() {
        return mTabInfoList;
    }

    @Override
    public void setMainTabInfos(List<TabInfo> tabInfoList) {
        this.mTabInfoList = tabInfoList;
    }

    @Override
    public void commitFeedback(long uid, String feedbackDesc, String contact,String img) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", String.valueOf(uid));
        requestParam.put("feedbackDesc", feedbackDesc);
        if (StringUtils.isNotEmpty(img)) {
            requestParam.put("img", img);
        }
        requestParam.put("contact", contact);
        requestParam.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());

        OkHttpManager.getInstance().postRequest(UriProvider.commitFeedback(), requestParam, new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_COMMIT_BACK_FAIL, e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_COMMIT_BACK);

                    } else {
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_COMMIT_BACK, response.getMessage());
                    }
                }
            }
        });
    }

    @Override
    public void getMainTabData() {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        if (CoreManager.getCore(IAuthCore.class).isLogin()) {
            requestParam.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        }
        //UriProvider.getMainDataByMenu()
        OkHttpManager.getInstance().getRequest(UriProvider.getMainTabList(), requestParam, new OkHttpManager.MyCallBack<TabResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_GET_HOME_TAB_LIST_ERROR, e.getMessage());
            }

            @Override
            public void onResponse(TabResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        List<TabInfo> data = response.getData();
                        setMainTabInfos(data);
                    } else {
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_GET_HOME_TAB_LIST_ERROR, response.getMessage());
                    }
                }
            }
        });
    }

    @Override
    public void getMainDataByTab(final int tagId, final int pageNum, int pageSize) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("tagId", String.valueOf(tagId));
        requestParam.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        requestParam.put("pageNum", String.valueOf(pageNum));
        requestParam.put("pageSize", String.valueOf(pageSize));

        OkHttpManager.getInstance().getRequest(UriProvider.getNotHotPageData(), requestParam, new OkHttpManager.MyCallBack<NotHotResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_GET_DATA_BY_TAB_ERROR, e.getMessage(), tagId, pageNum);
            }

            @Override
            public void onResponse(NotHotResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_GET_DATA_BY_TAB, response.getData(), tagId, pageNum);
                    } else {
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_GET_DATA_BY_TAB_ERROR, response.getMessage(), tagId, pageNum);
                    }
                }
            }
        });
    }

    @Override
    public void getSortDataByTab(final int tagId, final int pageNum, int pageSize) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("tagId", String.valueOf(tagId));
        requestParam.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        requestParam.put("pageNum", String.valueOf(pageNum));
        requestParam.put("pageSize", String.valueOf(pageSize));

        OkHttpManager.getInstance().getRequest(UriProvider.getMainDataByTab(), requestParam, new OkHttpManager.MyCallBack<HomeTabResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_SORT_GET_DATA_BY_TAB_ERROR, e.getMessage(), tagId, pageNum);
            }

            @Override
            public void onResponse(HomeTabResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_SORT_GET_DATA_BY_TAB, response.getData(), tagId, pageNum);
                    } else {
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.METHOD_ON_SORT_GET_DATA_BY_TAB_ERROR, response.getMessage(), tagId, pageNum);
                    }
                }
            }
        });
    }


    @Override
    public void getMainDataByMenu() {
        OkHttpManager.getInstance().getRequest(UriProvider.getMainDataByMenu(),
                CommonParamUtil.getDefaultParam(), new OkHttpManager.MyCallBack<Json>() {
                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                        notifyClients(IHomeCoreClient.class, IHomeCoreClient.onGetHomeDataByMenuFail);
                    }

                    @Override
                    public void onResponse(Json json) {
                        if (json.num("code") == 200) {
                            notifyClients(IHomeCoreClient.class, IHomeCoreClient.onGetHomeDataByMenuSuccess, json);
                        } else {
                            notifyClients(IHomeCoreClient.class, IHomeCoreClient.onGetHomeDataByMenuFail);
                        }
                    }
                });
    }
}




