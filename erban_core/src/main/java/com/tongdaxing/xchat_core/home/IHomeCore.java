package com.tongdaxing.xchat_core.home;

import com.tongdaxing.erban.libcommon.coremanager.IBaseCore;

import java.util.List;

/**
 * 主页接口逻辑处理
 * Created by zhouxiangfeng on 2017/5/17.
 */
public interface IHomeCore extends IBaseCore {
    //全局数据--------------------------

    List<TabInfo> getMainTabInfos();

    void setMainTabInfos(List<TabInfo> tabInfoList);

    //接口数据------------------------------

    void commitFeedback(long uid, String feedbackDesc, String contact,String img);

    /**
     * 获取首页tab数据
     */
    void getMainTabData();

    /**
     * 获取tab下的数据
     *
     * @param tagId    tab的id
     * @param pageNum  当前页数
     * @param pageSize 页的总数
     */
    void getMainDataByTab(int tagId, int pageNum, int pageSize);

    void getSortDataByTab(int tagId, int pageNum, int pageSize);

    void getMainDataByMenu();
}
