package com.tongdaxing.xchat_core.home;

import com.tongdaxing.xchat_core.match.MicroMatchPool;

import java.util.List;

public class NotHotData {
    public List<TabInfo> tagList;
    public List<BannerInfo> bannerList;
    public List<HomeRoom> recommendList;
    public List<HomeRoom> tagRoomList;
    public List<MicroMatchPool> showLink;
}
