package com.tongdaxing.xchat_core.home;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author zhouxiangfeng
 * @date 2017/5/17
 */

public class HomeIcon implements Parcelable {


    private String pic;

    private String activity;

    private String params;

    private String title;

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    protected HomeIcon(Parcel in) {

        params = in.readString();
        pic = in.readString();
        activity = in.readString();
        title = in.readString();
        url = in.readString();
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {


        dest.writeString(params);
        dest.writeString(pic);
        dest.writeString(activity);
        dest.writeString(title);
        dest.writeString(url);

    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<HomeIcon> CREATOR = new Creator<HomeIcon>() {
        @Override
        public HomeIcon createFromParcel(Parcel in) {
            return new HomeIcon(in);
        }

        @Override
        public HomeIcon[] newArray(int size) {
            return new HomeIcon[size];
        }
    };


    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

}
