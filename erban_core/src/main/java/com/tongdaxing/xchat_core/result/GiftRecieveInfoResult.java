package com.tongdaxing.xchat_core.result;

import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;

/**
 * Created by chenran on 2017/11/25.
 */

public class GiftRecieveInfoResult extends ServiceResult<GiftReceiveInfo> {
}
