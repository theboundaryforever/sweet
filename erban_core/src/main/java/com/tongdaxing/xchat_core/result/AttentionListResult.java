package com.tongdaxing.xchat_core.result;

import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.xchat_core.user.bean.AttentionInfo;

import java.util.List;

/**
 * Created by Administrator on 2017/7/5 0005.
 */

public class AttentionListResult extends ServiceResult<List<AttentionInfo>> {
}
