package com.tongdaxing.xchat_core.result;

import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;

/**
 * Created by zhouxiangfeng on 2017/5/4.
 */

public class WalletInfoResult extends ServiceResult<WalletInfo> {

}
