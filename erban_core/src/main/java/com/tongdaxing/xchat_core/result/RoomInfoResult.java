package com.tongdaxing.xchat_core.result;

import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;

/**
 * Created by chenran on 2017/10/18.
 */

public class RoomInfoResult extends ServiceResult<RoomInfo> {
}
