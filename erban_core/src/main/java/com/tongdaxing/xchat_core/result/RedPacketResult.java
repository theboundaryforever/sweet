package com.tongdaxing.xchat_core.result;


import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.xchat_core.redpacket.bean.RedPacketInfo;

/**
 * Created by zhouxiangfeng on 2017/5/28.
 */

public class RedPacketResult extends ServiceResult<RedPacketInfo> {
}
