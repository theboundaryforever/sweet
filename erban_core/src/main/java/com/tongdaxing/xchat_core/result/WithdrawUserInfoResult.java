package com.tongdaxing.xchat_core.result;

import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.xchat_core.withdraw.bean.WithdrawInfo;

/**
 * Created by zhouxiangfeng on 2017/5/4.
 */

public class WithdrawUserInfoResult extends ServiceResult<WithdrawInfo> {

}
