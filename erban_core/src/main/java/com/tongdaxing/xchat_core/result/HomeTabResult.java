package com.tongdaxing.xchat_core.result;

import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.xchat_core.home.HomeRoom;

import java.util.List;

/**
 * <p> 首页tab 非hot数据 + 搜索数据实体</p>
 *
 * @author Administrator
 * @date 2017/11/22
 */
public class HomeTabResult extends ServiceResult<List<HomeRoom>> {
}
