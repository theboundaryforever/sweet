package com.tongdaxing.xchat_core.result;

import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.xchat_core.room.queue.bean.RoomConsumeInfo;

import java.util.List;

/**
 * Created by chenran on 2017/10/2.
 */

public class RoomConsumeInfoListResult extends ServiceResult<List<RoomConsumeInfo>> {
}
