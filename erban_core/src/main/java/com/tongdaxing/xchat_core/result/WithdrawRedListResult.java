package com.tongdaxing.xchat_core.result;


import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.xchat_core.redpacket.bean.WithdrawRedListInfo;

import java.util.List;

/**
 * Created by zhouxiangfeng on 2017/5/28.
 */

public class WithdrawRedListResult extends ServiceResult<List<WithdrawRedListInfo>> {
}
