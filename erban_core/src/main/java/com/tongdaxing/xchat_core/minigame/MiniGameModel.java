package com.tongdaxing.xchat_core.minigame;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;

import java.util.Map;

/**
 * 小游戏model层
 */
public class MiniGameModel extends BaseMvpModel {
    /**
     * 邀请对方加入游戏
     */
    public void invitedUser(String gameType,long invitedUserId, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("game_type", gameType);
        params.put("invtdUid", String.valueOf(invitedUserId));
        OkHttpManager.getInstance().postRequest(UriProvider.invtdUser(), params, callBack);
    }

    /**
     * 同意游戏
     *
     * @param roomId  发起游戏的id
     * @param isAgree 是否同意 1同意 0不同意
     */
    public void acceptInvited(long roomId, int isAgree, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomId", String.valueOf(roomId));
        params.put("isAgree", String.valueOf(isAgree));
        OkHttpManager.getInstance().postRequest(UriProvider.acceptInvt(), params, callBack);
    }

    /**
     * 取消游戏邀请
     */
    public void cancelInvited(String roomId, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomId", roomId);
        OkHttpManager.getInstance().postRequest(UriProvider.cancelInvt(), params, callBack);
    }

    /**
     * 查询游戏结果
     *
     * @param roomId   发起游戏的id
     * @param callBack
     */
    public void queryResult(long roomId, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomid", String.valueOf(roomId));
        OkHttpManager.getInstance().postRequest(UriProvider.getGameReslt(), params, callBack);
    }

    /**
     * 查询游戏列表
     *
     * @param callBack
     */
    public void getGames(OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.getGames(), params, callBack);
    }
}
