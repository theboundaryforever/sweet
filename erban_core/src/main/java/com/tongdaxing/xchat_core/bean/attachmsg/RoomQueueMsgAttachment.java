package com.tongdaxing.xchat_core.bean.attachmsg;

import com.alibaba.fastjson.JSONObject;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;

/**
 * <p>  队列自定义消息</p>
 *
 * @author jiahui
 * @date 2017/12/18
 */
public class RoomQueueMsgAttachment extends CustomAttachment {
    public RoomQueueInfo roomQueueInfo;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String uid;
    public int micPosition;
    private boolean isAuctionList = false;

    public String targetNick;
    public String handleNick;

    public int handleVipId;
    public int handleVipDate;
    public boolean handleIsInvisible = false;
    public int targetVipId;
    public int targetVipDate;
    public boolean targetIsInvisiable = false;

    public int getHandleVipId() {
        return handleVipId;
    }

    public void setHandleVipId(int handleVipId) {
        this.handleVipId = handleVipId;
    }

    public int getHandleVipDate() {
        return handleVipDate;
    }

    public void setHandleVipDate(int handleVipDate) {
        this.handleVipDate = handleVipDate;
    }

    public boolean isHandleIsInvisible() {
        return handleIsInvisible;
    }

    public void setHandleIsInvisible(boolean handleIsInvisible) {
        this.handleIsInvisible = handleIsInvisible;
    }

    public int getTargetVipId() {
        return targetVipId;
    }

    public void setTargetVipId(int targetVipId) {
        this.targetVipId = targetVipId;
    }

    public int getTargetVipDate() {
        return targetVipDate;
    }

    public void setTargetVipDate(int targetVipDate) {
        this.targetVipDate = targetVipDate;
    }

    public boolean isTargetIsInvisiable() {
        return targetIsInvisiable;
    }

    public void setTargetIsInvisiable(boolean targetIsInvisiable) {
        this.targetIsInvisiable = targetIsInvisiable;
    }

    public String getTargetNick() {
        return targetNick;
    }

    public void setTargetNick(String targetNick) {
        this.targetNick = targetNick;
    }

    public String getHandleNick() {
        return handleNick;
    }

    public void setHandleNick(String handleNick) {
        this.handleNick = handleNick;
    }

    public boolean isAuctionList() {
        return isAuctionList;
    }

    public void setAuctionList(boolean auctionList) {
        isAuctionList = auctionList;
    }

    public RoomQueueMsgAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected JSONObject packData() {
        JSONObject jsonObject = new JSONObject();
        if (uid != null) {
            jsonObject.put("uid", uid);
        }
        if (targetNick != null) {
            jsonObject.put("targetNick", targetNick);
        }
        if (handleNick != null) {
            jsonObject.put("handleNick", handleNick);
        }
        jsonObject.put("handleVipId", handleVipId);
        jsonObject.put("handleVipDate", handleVipDate);
        jsonObject.put("handleIsInvisible", handleIsInvisible);
        jsonObject.put("targetVipId", targetVipId);
        jsonObject.put("targetVipDate", targetVipDate);
        jsonObject.put("targetIsInvisiable", targetIsInvisiable);

        jsonObject.put("micPosition", micPosition);
        jsonObject.put("isAuctionList",isAuctionList);
        return jsonObject;
    }

    @Override
    protected void parseData(JSONObject data) {
        super.parseData(data);
        if (data != null) {
            if (data.containsKey("uid")) {
                uid = data.getString("uid");
            }
            if (data.containsKey("micPosition")) {
                micPosition = data.getIntValue("micPosition");
            }
            if (data.containsKey("handleVipId")) {
                handleVipId = data.getIntValue("handleVipId");
            }
            if (data.containsKey("handleVipDate")) {
                handleVipDate = data.getIntValue("handleVipDate");
            }
            if (data.containsKey("handleIsInvisible")) {
                handleIsInvisible = data.getBoolean("handleIsInvisible");
            }
            if (data.containsKey("targetIsInvisiable")) {
                targetIsInvisiable = data.getBoolean("targetIsInvisiable");
            }
            if (data.containsKey("targetVipId")) {
                targetVipId = data.getInteger("targetVipId");
            }
            if (data.containsKey("targetVipDate")) {
                targetVipDate = data.getInteger("targetVipDate");
            }
            if (data.containsKey("handleNick")) {
                handleNick = data.getString("handleNick");
            }
            if (data.containsKey("targetNick")) {
                targetNick = data.getString("targetNick");
            }
        }
    }
}
