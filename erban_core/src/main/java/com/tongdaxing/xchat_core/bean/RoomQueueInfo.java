package com.tongdaxing.xchat_core.bean;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;

/**
 * <p>  房间麦序单个坑位信息实体，包含麦序状态，成员信息</p>
 *
 * @author jiahui
 * @date 2017/12/13
 */
public class RoomQueueInfo {
    /**
     * 坑位信息（是否所坑，开麦等）
     */
    public RoomMicInfo mRoomMicInfo;
    /**
     * 坑上人员信息
     */
    public ChatRoomMember mChatRoomMember;
    /**
     * 当前成员的性别，本属于ChatRoomMember的
     */
    public int gender;
    public long time;

    //pk用到
    public boolean isSelect = true;

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public RoomQueueInfo(RoomMicInfo roomMicInfo, ChatRoomMember chatRoomMember) {
        mRoomMicInfo = roomMicInfo;
        mChatRoomMember = chatRoomMember;
    }
}
