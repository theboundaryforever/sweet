package com.tongdaxing.xchat_core.bean;

import java.io.Serializable;

public enum WebViewStyle implements Serializable {
    NORMAL,
    NO_TITLE,
    NO_TITLE_BUT_STATUS_BAR,
    FULL_SCREEN,
    NO_TITLE_AND_FULL_SCREEN
}
