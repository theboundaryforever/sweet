package com.tongdaxing.xchat_core.bean;

import java.io.Serializable;

/**
 * @author liaoxy
 * @Description:房间红包领取结果实体
 * @date 2019/1/18 21:08
 */
public class RoomRedPacketReceiveResult implements Serializable {
    private String redPacketId;//": "b616dd2a3d7f4e99a0d49485656249e3",
    private long uid;//": 10251,
    private String nick;//": "湘",
    private String avatar;//": "https://img.pinjin88.com/default_head_nan.png",
    private int goldNum;//": 50,
    private long receiveDate;//": 1547816566848,
    private String redPacketGold;//": null,
    private String redPacketNum;//": null,
    private String redPacketReceiveNum;//": null

    public String getRedPacketId() {
        return redPacketId;
    }

    public void setRedPacketId(String redPacketId) {
        this.redPacketId = redPacketId;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getGoldNum() {
        return goldNum;
    }

    public void setGoldNum(int goldNum) {
        this.goldNum = goldNum;
    }

    public long getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(long receiveDate) {
        this.receiveDate = receiveDate;
    }

    public String getRedPacketGold() {
        return redPacketGold;
    }

    public void setRedPacketGold(String redPacketGold) {
        this.redPacketGold = redPacketGold;
    }

    public String getRedPacketNum() {
        return redPacketNum;
    }

    public void setRedPacketNum(String redPacketNum) {
        this.redPacketNum = redPacketNum;
    }

    public String getRedPacketReceiveNum() {
        return redPacketReceiveNum;
    }

    public void setRedPacketReceiveNum(String redPacketReceiveNum) {
        this.redPacketReceiveNum = redPacketReceiveNum;
    }
}
