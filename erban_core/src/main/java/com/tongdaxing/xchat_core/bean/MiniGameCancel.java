package com.tongdaxing.xchat_core.bean;

import java.io.Serializable;

public class MiniGameCancel implements Serializable {
    private long roomid;//：发起游戏的ID
    private int   fromUid;//：发起邀请的UID
    private String fromNick;//：发起邀请人的昵称
    private String  fromAvatar;//：发起邀请人头像
    private int  gameType;//:  1:五子棋 2：连连看 3：跳一跳

    public long getRoomid() {
        return roomid;
    }

    public void setRoomid(long roomid) {
        this.roomid = roomid;
    }

    public int getFromUid() {
        return fromUid;
    }

    public void setFromUid(int fromUid) {
        this.fromUid = fromUid;
    }

    public String getFromNick() {
        return fromNick;
    }

    public void setFromNick(String fromNick) {
        this.fromNick = fromNick;
    }

    public String getFromAvatar() {
        return fromAvatar;
    }

    public void setFromAvatar(String fromAvatar) {
        this.fromAvatar = fromAvatar;
    }

    public int getGameType() {
        return gameType;
    }

    public void setGameType(int gameType) {
        this.gameType = gameType;
    }
}
