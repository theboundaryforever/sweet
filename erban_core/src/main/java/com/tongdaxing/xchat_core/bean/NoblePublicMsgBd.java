package com.tongdaxing.xchat_core.bean;

import java.io.Serializable;

/**
 * @author liaoxy
 * @Description:贵族全站广播附件实体类
 * @date 2019/3/12 9:59
 */

public class NoblePublicMsgBd implements Serializable {
    private boolean canSend;
    private String broadcast;

    public boolean isCanSend() {
        return canSend;
    }

    public void setCanSend(boolean canSend) {
        this.canSend = canSend;
    }

    public String getBroadcast() {
        return broadcast;
    }

    public void setBroadcast(String broadcast) {
        this.broadcast = broadcast;
    }
}
