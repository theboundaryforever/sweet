package com.tongdaxing.xchat_core.bean;

import java.io.Serializable;

/**
 * @author liaoxy
 * @Description:游戏连麦邀请消息实体类
 * @date 2019/2/15 11:16
 */
public class GameLinkMacroInvitaion implements Serializable {
    private String  nick;//": "https://img.pinjin88.com/Fkaq2rC6RiABSK42JEH0PZs9pNtW?imageslim",
    private long  uid;//": 10534,
    private String  invitationNick;//": "小黎的守护神",
    private String  reason;//": "小明明腹股沟管哈哈哈方法出差邀请你进行语音通话",
    private long  invitationUid;//": 10406,
    private String  invitationAvatar;//": "https://img.pinjin88.com/Fl51AJoW45VOLX4A07yY4fbrnLxD?imageslim",
    private String  avatar;//": "小明明腹股沟管哈哈哈方法出差",
    private String  channelId;//": "10406_10534"
    private int talkTime;

    public int getTalkTime() {
        return talkTime;
    }

    public void setTalkTime(int talkTime) {
        this.talkTime = talkTime;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getInvitationNick() {
        return invitationNick;
    }

    public void setInvitationNick(String invitationNick) {
        this.invitationNick = invitationNick;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public long getInvitationUid() {
        return invitationUid;
    }

    public void setInvitationUid(long invitationUid) {
        this.invitationUid = invitationUid;
    }

    public String getInvitationAvatar() {
        return invitationAvatar;
    }

    public void setInvitationAvatar(String invitationAvatar) {
        this.invitationAvatar = invitationAvatar;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }
}
