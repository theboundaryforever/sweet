package com.tongdaxing.xchat_core.bean;

import java.io.Serializable;

public class VisitorInfo implements Serializable {

    private int visitorCount;//":0,"

    public int getVisitorCount() {
        return visitorCount;
    }

    public void setVisitorCount(int visitorCount) {
        this.visitorCount = visitorCount;
    }
}
