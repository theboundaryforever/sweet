package com.tongdaxing.xchat_core.bean;

import java.io.Serializable;

public class MiniGameInvitedInfo implements Serializable{
    private String fromNick;
    private long fromUid;
    private long roomid;
    private String fromAvatar;
    private String gameBgImage;
    private String gameName;

    public String getGameBgImage() {
        return gameBgImage;
    }

    public void setGameBgImage(String gameBgImage) {
        this.gameBgImage = gameBgImage;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getFromNick() {
        return fromNick;
    }

    public void setFromNick(String fromNick) {
        this.fromNick = fromNick;
    }

    public long getFromUid() {
        return fromUid;
    }

    public void setFromUid(long fromUid) {
        this.fromUid = fromUid;
    }

    public long getRoomid() {
        return roomid;
    }

    public void setRoomid(long roomid) {
        this.roomid = roomid;
    }

    public String getFromAvatar() {
        return fromAvatar;
    }

    public void setFromAvatar(String fromAvatar) {
        this.fromAvatar = fromAvatar;
    }
}
