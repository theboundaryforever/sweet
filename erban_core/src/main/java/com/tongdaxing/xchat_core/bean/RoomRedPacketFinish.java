package com.tongdaxing.xchat_core.bean;

import java.io.Serializable;

//红包领取完成消息
public class RoomRedPacketFinish implements Serializable {
    private String nick;//":"湘","
    private long uid;//":10251,"
    private int level;//":0,"
    private String alias;//":"https: //img.pinjin88.com/new_user_msg_icon.png","
    private int goldNum;//":5
    private String redPacketId;

    public String getRedPacketId() {
        return redPacketId;
    }

    public void setRedPacketId(String redPacketId) {
        this.redPacketId = redPacketId;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public int getGoldNum() {
        return goldNum;
    }

    public void setGoldNum(int goldNum) {
        this.goldNum = goldNum;
    }
}
