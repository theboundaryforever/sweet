package com.tongdaxing.xchat_core.bean;

import java.io.Serializable;

public class MiniGameInvitedAcceptInfo implements Serializable {
    private String fromNick;
    private int acceptInvt;// 0:拒绝邀请 1:接受邀请
    private long fromUid;
    private String gameUrl;
    private long roomid;
    private String fromAvatar;

    public String getFromNick() {
        return fromNick;
    }

    public void setFromNick(String fromNick) {
        this.fromNick = fromNick;
    }

    public int getAcceptInvt() {
        return acceptInvt;
    }

    public void setAcceptInvt(int acceptInvt) {
        this.acceptInvt = acceptInvt;
    }

    public long getFromUid() {
        return fromUid;
    }

    public void setFromUid(long fromUid) {
        this.fromUid = fromUid;
    }

    public String getGameUrl() {
        return gameUrl;
    }

    public void setGameUrl(String gameUrl) {
        this.gameUrl = gameUrl;
    }

    public long getRoomid() {
        return roomid;
    }

    public void setRoomid(long roomid) {
        this.roomid = roomid;
    }

    public String getFromAvatar() {
        return fromAvatar;
    }

    public void setFromAvatar(String fromAvatar) {
        this.fromAvatar = fromAvatar;
    }
}
