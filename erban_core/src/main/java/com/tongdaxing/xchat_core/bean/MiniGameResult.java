package com.tongdaxing.xchat_core.bean;

import java.io.Serializable;

public class MiniGameResult implements Serializable {
    private String oppNick;//：对方的昵称
    private String  oppAvatar;//：对方头像
    private int result;//0:输 1：赢 2:和
    private long roomid;
    private int incScore;
    private int oppWinCount;
    private int ownerWinCount;
    private String gameName;
    private String gameType;
    private String gameBgImage;

    public String getOppNick() {
        return oppNick;
    }

    public void setOppNick(String oppNick) {
        this.oppNick = oppNick;
    }

    public String getOppAvatar() {
        return oppAvatar;
    }

    public void setOppAvatar(String oppAvatar) {
        this.oppAvatar = oppAvatar;
    }

    public int getIncScore() {
        return incScore;
    }

    public void setIncScore(int incScore) {
        this.incScore = incScore;
    }

    public int getOppWinCount() {
        return oppWinCount;
    }

    public void setOppWinCount(int oppWinCount) {
        this.oppWinCount = oppWinCount;
    }

    public int getOwnerWinCount() {
        return ownerWinCount;
    }

    public void setOwnerWinCount(int ownerWinCount) {
        this.ownerWinCount = ownerWinCount;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public long getRoomid() {
        return roomid;
    }

    public void setRoomid(long roomid) {
        this.roomid = roomid;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getGameType() {
        return gameType;
    }

    public void setGameType(String gameType) {
        this.gameType = gameType;
    }

    public String getGameBgImage() {
        return gameBgImage;
    }

    public void setGameBgImage(String gameBgImage) {
        this.gameBgImage = gameBgImage;
    }
}
