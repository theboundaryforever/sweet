package com.tongdaxing.xchat_core.bean;

import java.io.Serializable;

/**
 * @author liaoxy
 * @Description:贵族全站广播附件实体类
 * @date 2019/3/12 9:59
 */

public class NoblePublicMsg implements Serializable {
    private long uid;//": 10408,
    private int charmLevel;//": 35,
    private int experLevel;//": 35,
    private String avatar;//": "https: //img.pinjin88.com/FjZ0Sb6Xoa99k1JGuCWCXMkv2HM5?imageslim",
    private String message;//": "GG呼救聚聚",
    private String vipIcon;//": "https: //img.pinjin88.com/guowang@3x.png"
    private String vipName;//": "国王",
    private int vipId;//": 7,
    private String vipMedal;//": "",
    private String nick;
    private String topUrl;
    private long current;//时间表缀

    public String getVipMedal() {
        return vipMedal;
    }

    public void setVipMedal(String vipMedal) {
        this.vipMedal = vipMedal;
    }

    public long getCurrent() {
        return current;
    }

    public void setCurrent(long current) {
        this.current = current;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getTopUrl() {
        return topUrl;
    }

    public void setTopUrl(String topUrl) {
        this.topUrl = topUrl;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getVipName() {
        return vipName;
    }

    public void setVipName(String vipName) {
        this.vipName = vipName;
    }

    public int getCharmLevel() {
        return charmLevel;
    }

    public void setCharmLevel(int charmLevel) {
        this.charmLevel = charmLevel;
    }

    public int getExperLevel() {
        return experLevel;
    }

    public void setExperLevel(int experLevel) {
        this.experLevel = experLevel;
    }

    public int getVipId() {
        return vipId;
    }

    public void setVipId(int vipId) {
        this.vipId = vipId;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getVipIcon() {
        return vipIcon;
    }

    public void setVipIcon(String vipIcon) {
        this.vipIcon = vipIcon;
    }
}
