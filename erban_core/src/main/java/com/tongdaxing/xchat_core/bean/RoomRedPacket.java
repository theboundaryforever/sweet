package com.tongdaxing.xchat_core.bean;

import java.io.Serializable;

public class RoomRedPacket implements Serializable {
    private String redPacketId;
    private long uid;
    private String nick;
    private String avatar;
    private long roomUid;
    private int roomType;
    private long roomNo;
    private long roomId;
    private String title;
    private int goldNum;
    private int redPacketNum;
    private long redPacketTime;
    private String redPacketDate;
    private int type;
    private int timing;
    private int showBroadcast;
    private int showAll;
    private String redEnvelopedRain;

    private long recieveTime;
    // 定时红包可用时间
    private long useabelTime;

    public long getUseabelTime() {
        return useabelTime;
    }

    public void setUseabelTime(long useabelTime) {
        this.useabelTime = useabelTime;
    }

    public long getRedPacketTime() {
        return redPacketTime;
    }

    public void setRedPacketTime(long redPacketTime) {
        this.redPacketTime = redPacketTime;
    }

    public String getRedPacketDate() {
        return redPacketDate;
    }

    public void setRedPacketDate(String redPacketDate) {
        this.redPacketDate = redPacketDate;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public long getRoomUid() {
        return roomUid;
    }

    public void setRoomUid(long roomUid) {
        this.roomUid = roomUid;
    }

    public long getRoomNo() {
        return roomNo;
    }

    public void setRoomNo(long roomNo) {
        this.roomNo = roomNo;
    }

    public String getRedPacketId() {
        return redPacketId;
    }

    public void setRedPacketId(String redPacketId) {
        this.redPacketId = redPacketId;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getGoldNum() {
        return goldNum;
    }

    public void setGoldNum(int goldNum) {
        this.goldNum = goldNum;
    }

    public int getRedPacketNum() {
        return redPacketNum;
    }

    public void setRedPacketNum(int redPacketNum) {
        this.redPacketNum = redPacketNum;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getTiming() {
        return timing;
    }

    public void setTiming(int timing) {
        this.timing = timing;
    }

    public int getShowBroadcast() {
        return showBroadcast;
    }

    public void setShowBroadcast(int showBroadcast) {
        this.showBroadcast = showBroadcast;
    }

    public int getShowAll() {
        return showAll;
    }

    public void setShowAll(int showAll) {
        this.showAll = showAll;
    }

    public String getRedEnvelopedRain() {
        return redEnvelopedRain;
    }

    public void setRedEnvelopedRain(String redEnvelopedRain) {
        this.redEnvelopedRain = redEnvelopedRain;
    }

    public long getRecieveTime() {
        return recieveTime;
    }

    public void setRecieveTime(long recieveTime) {
        this.recieveTime = recieveTime;
    }

    public int getRoomType() {
        return roomType;
    }

    public void setRoomType(int roomType) {
        this.roomType = roomType;
    }
}
