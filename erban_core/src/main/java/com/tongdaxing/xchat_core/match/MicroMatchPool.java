package com.tongdaxing.xchat_core.match;

import java.io.Serializable;

public class MicroMatchPool implements Serializable{


    /**
     * uid : 90000796
     * roomAvatar : https://img.pinjin88.com/FrGW0PFQNynFyE50WzAn6iEj9fku?imageslim
     * avatar : https://img.pinjin88.com/FrGW0PFQNynFyE50WzAn6iEj9fku?imageslim
     */

    private long uid;
    private String roomAvatar;
    private String avatar;

    private int linkNum;

    public int getLinkNum(){return linkNum;}

    public void setLinkNum(int linkNum){
        this.linkNum = linkNum;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getRoomAvatar() {
        return roomAvatar;
    }

    public void setRoomAvatar(String roomAvatar) {
        this.roomAvatar = roomAvatar;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @Override
    public String toString() {
        return "MicroMatchPool{" +
                "uid=" + uid +
                ", roomAvatar='" + roomAvatar + '\'' +
                ", avatar='" + avatar + '\'' +
                '}';
    }
}
