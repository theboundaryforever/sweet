package com.tongdaxing.xchat_core.player.bean;

import android.text.TextUtils;

import com.tongdaxing.erban.libcommon.utils.BlankUtil;
import com.tongdaxing.erban.libcommon.utils.json.JsonParser;

import java.io.Serializable;
import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by chenran on 2017/10/28.
 */

public class LocalMusicInfo extends RealmObject implements Serializable {
    @PrimaryKey
    private long localId;

    /**
     * 歌曲服务器id，对应于HotMusicInfo的id字段
     */
    private String songId;
    /**
     * 歌曲名
     */
    private String songName;

    /**
     * @deprecated 字段暂时没什么用处 为了保证新旧版本的数据库兼容性 保留废弃
     */
    private String albumId;

    /**
     * @deprecated 字段暂时没什么用处 为了保证新旧版本的数据库兼容性 保留废弃
     */
    private String albumIndex;

    /**
     * @deprecated 字段暂时没什么用处 为了保证新旧版本的数据库兼容性 保留废弃
     */
    private String albumName;

    /**
     * @deprecated 字段暂时没什么用处 为了保证新旧版本的数据库兼容性 保留废弃
     */
    private String artistIdsJson;

    /**
     * @deprecated 字段暂时没什么用处 为了保证新旧版本的数据库兼容性 保留废弃
     */
    private String artistIndex;

    /**
     * 艺术家、歌曲作者
     * 按说可以直接修改为非json格式的String 如"刘德华"
     * 为了保证新旧版本数据库兼容性 暂且保持旧版本的格式
     */
    private String artistNamesJson;

    /**
     * 服务器文件下载地址，对应HotMusicInfo中的singUrl
     */
    private String remoteUri;

    /**
     * 音乐文件本地路径
     */
    private String localUri;

    /**
     * @deprecated 字段暂时没什么用处 为了保证新旧版本的数据库兼容性 保留废弃
     */
    private String quality;

    /**
     * @deprecated 字段暂时没什么用处 为了保证新旧版本的数据库兼容性 保留废弃
     */
    private String year;

    /**
     * 歌曲时长
     */
    private long duration;

    /**
     * @deprecated 字段暂时没什么用处 为了保证新旧版本的数据库兼容性 保留废弃
     */
    private boolean deleted;

    /**
     * 是否处于播放列表中
     */
    private boolean isInPlayerList;

    /**
     * @deprecated 字段暂时没什么用处 为了保证新旧版本的数据库兼容性 保留废弃
     */
    private long fileSize;

    /**
     * @deprecated 字段暂时没什么用处 为了保证新旧版本的数据库兼容性 保留废弃
     */
    private String lyricUrl;

    /**
     * @deprecated 字段暂时没什么用处 为了保证新旧版本的数据库兼容性 保留废弃
     */
    private String songAlbumCover;

    public String getSongId() {
        return songId;
    }

    public void setSongId(String songId) {
        this.songId = songId;
    }

    public String getSongName() {

        if(!TextUtils.isEmpty(songName) && !songName.trim().isEmpty() && songName.startsWith(".")){
            return songName.replaceFirst(".","");
        }

        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
        if(!TextUtils.isEmpty(songName) && !songName.trim().isEmpty() && songName.startsWith(".")){
            this.songName = songName.replaceFirst(".","");
        }
    }

    /**
     * @deprecated
     */
    public String getAlbumId() {
        return albumId;
    }

    /**
     * @deprecated
     */
    public void setAlbumId(String albumId) {
        this.albumId = albumId;
    }

    /**
     * @deprecated
     */
    public String getAlbumIndex() {
        return albumIndex;
    }

    /**
     * @deprecated
     */
    public void setAlbumIndex(String albumIndex) {
        this.albumIndex = albumIndex;
    }

    /**
     * @deprecated
     */
    public String getAlbumName() {
        return albumName;
    }

    /**
     * @deprecated
     */
    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public String getArtistIdsJson() {
        return artistIdsJson;
    }

    public void setArtistIdsJson(String artistIdsJson) {
        this.artistIdsJson = artistIdsJson;
    }

    /**
     * @deprecated
     */
    public String getArtistIndex() {
        return artistIndex;
    }

    /**
     * @deprecated
     */
    public void setArtistIndex(String artistIndex) {
        this.artistIndex = artistIndex;
    }

    public String getArtistNamesJson() {
        return artistNamesJson;
    }

    public void setArtistNamesJson(String artistNamesJson) {
        this.artistNamesJson = artistNamesJson;
    }

    public String getRemoteUri() {
        return remoteUri;
    }

    public void setRemoteUri(String remoteUri) {
        this.remoteUri = remoteUri;
    }

    public String getLocalUri() {
        return localUri;
    }

    public void setLocalUri(String localUri) {
        this.localUri = localUri;
    }

    /**
     * @deprecated
     */
    public String getQuality() {
        return quality;
    }

    /**
     * @deprecated
     */
    public void setQuality(String quality) {
        this.quality = quality;
    }

    /**
     * @deprecated
     */
    public String getYear() {
        return year;
    }

    /**
     * @deprecated
     */
    public void setYear(String year) {
        this.year = year;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    /**
     * @deprecated
     */
    public boolean isDeleted() {
        return deleted;
    }

    /**
     * @deprecated
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isInPlayerList() {
        return isInPlayerList;
    }

    public void setInPlayerList(boolean inPlayerList) {
        isInPlayerList = inPlayerList;
    }

    /**
     * @deprecated
     */
    public long getFileSize() {
        return fileSize;
    }

    /**
     * @deprecated
     */
    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    /**
     * @deprecated
     */
    public String getLyricUrl() {
        return lyricUrl;
    }

    /**
     * @deprecated
     */
    public void setLyricUrl(String lyricUrl) {
        this.lyricUrl = lyricUrl;
    }

    /**
     * @deprecated
     */
    public String getSongAlbumCover() {
        return songAlbumCover;
    }

    /**
     * @deprecated
     */
    public void setSongAlbumCover(String songAlbumCover) {
        this.songAlbumCover = songAlbumCover;
    }

    public List<String> getArtistNames() {
        if (null != artistNamesJson) {
            return JsonParser.parseJsonList(artistNamesJson, String.class);
        }

        return null;
    }

    public void setArtistName(List<String> artistNames) {
        if (!BlankUtil.isBlank(artistNames)) {
            this.artistNamesJson = JsonParser.toJson(artistNames);
        }
    }

    public long getLocalId() {
        return localId;
    }

    public void setLocalId(long localId) {
        this.localId = localId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof LocalMusicInfo)) {
            return false;
        }
        boolean isSameMusic = false;
        LocalMusicInfo other = (LocalMusicInfo) obj;
        if (null != other.getSongId() && null != this.getSongId() && this.getSongId().equals(other.getSongId())) {
            isSameMusic = true;
        }else if (null != this.getLocalUri() && null != other.getLocalUri() && this.getLocalUri().equals(other.getLocalUri())) {
            isSameMusic = true;
        }/*else if (this.getLocalId() == other.getLocalId()) {
            isSameMusic = true;
        }*/
        return isSameMusic;
    }
}

