package com.tongdaxing.xchat_core.player;

import com.tongdaxing.erban.libcommon.coremanager.AbstractBaseCore;
import com.tongdaxing.xchat_core.player.bean.LocalMusicInfo;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.exceptions.RealmError;

/**
 * Created by chenran on 2017/10/31.
 */

public class PlayerDbCoreImpl extends AbstractBaseCore implements IPlayerDbCore {
    private Realm mRealm;

    public PlayerDbCoreImpl() {
        mRealm = Realm.getDefaultInstance();
    }

    @Override
    public void addToPlayerList(long localId) {
        try {
            mRealm.beginTransaction();
            LocalMusicInfo localMusicInfo = mRealm.where(LocalMusicInfo.class).equalTo("localId", localId).findFirst();
            localMusicInfo.setInPlayerList(true);
            mRealm.copyToRealmOrUpdate(localMusicInfo);
            mRealm.commitTransaction();
        } catch (RealmError | Exception ex){
            ex.printStackTrace();
        }
    }



    @Override
    public LocalMusicInfo requestLocalMusicInfoByLocalId(long localId) {
        LocalMusicInfo localMusicInfo = mRealm.where(LocalMusicInfo.class).equalTo("localId", localId).findFirst();
        return null == localMusicInfo ? new LocalMusicInfo() : Realm.getDefaultInstance().copyFromRealm(localMusicInfo);
    }

    @Override
    public void deleteFromPlayerList(long localId) {
        try {
            mRealm.beginTransaction();
            LocalMusicInfo localMusicInfo = mRealm.where(LocalMusicInfo.class).equalTo("localId", localId).findFirst();
            localMusicInfo.setInPlayerList(false);
            mRealm.copyToRealmOrUpdate(localMusicInfo);
            mRealm.commitTransaction();
        } catch (RealmError | Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public void replaceAllLocalMusics(List<LocalMusicInfo> localMusicInfoList) {
        try {
            if (localMusicInfoList != null) {
                for (int i = 0; i < localMusicInfoList.size(); i++) {
                    LocalMusicInfo localMusicInfo = localMusicInfoList.get(i);
                    LocalMusicInfo local = mRealm.where(LocalMusicInfo.class).equalTo("localId", localMusicInfo.getLocalId()).findFirst();
                    if (local != null) {
                        localMusicInfo.setInPlayerList(local.isInPlayerList());
                        localMusicInfo.setSongId(local.getSongId());
                        localMusicInfo.setRemoteUri(local.getRemoteUri());
                    }
                }
                mRealm.beginTransaction();
                mRealm.copyToRealmOrUpdate(localMusicInfoList);
                mRealm.commitTransaction();
            }
        } catch (RealmError | Exception ex){
            ex.printStackTrace();
        }
    }



    @Override
    public List<LocalMusicInfo> queryAllLocalMusicInfos() {
        RealmResults<LocalMusicInfo> localMusicInfos = mRealm.where(LocalMusicInfo.class).findAll();
        boolean isEmptyList = null == localMusicInfos || localMusicInfos.size() == 0;
        return isEmptyList ? new ArrayList<LocalMusicInfo>() : Realm.getDefaultInstance().copyFromRealm(localMusicInfos);
    }

    @Override
    public List<LocalMusicInfo> queryPlayerListLocalMusicInfos() {
        RealmResults<LocalMusicInfo> localMusicInfos = mRealm.where(LocalMusicInfo.class).equalTo("isInPlayerList", true).findAll();
        boolean isEmptyList = null == localMusicInfos || localMusicInfos.size() == 0;
        return isEmptyList ? new ArrayList<LocalMusicInfo>() : Realm.getDefaultInstance().copyFromRealm(localMusicInfos);
    }

    @Override
    public LocalMusicInfo gueryLocalMusicInfoByLocalUrl(String localUrl) {
        LocalMusicInfo localMusicInfo = mRealm.where(LocalMusicInfo.class).equalTo("localUri", localUrl).findFirst();
        return null == localMusicInfo ? null : Realm.getDefaultInstance().copyFromRealm(localMusicInfo);
    }

    /**
     * 插入下载后扫描得到的localMusicInfo到本地realm数据库
     *
     * @param localMusicInfo
     */
    @Override
    public void insertOrUpdateLocalMusicInfo(LocalMusicInfo localMusicInfo) {
        try {
            mRealm.beginTransaction();
            mRealm.copyToRealmOrUpdate(localMusicInfo);
            mRealm.commitTransaction();
        } catch (RealmError | Exception ex){
            ex.printStackTrace();
        }
    }
}
