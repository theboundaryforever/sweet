package com.tongdaxing.xchat_core.player;

import com.tongdaxing.erban.libcommon.coremanager.IBaseCore;
import com.tongdaxing.xchat_core.player.bean.LocalMusicInfo;

import java.util.List;

/**
 * Created by chenran on 2017/10/28.
 */

public interface IPlayerCore extends IBaseCore {
    public static final int STATE_STOP = 0;
    public static final int STATE_PLAY = 1;
    public static final int STATE_PAUSE = 2;

    public static final int PLAY_STATUS_SUCCESS = 0;//成功加载并播放
    public static final int PLAY_STATUS_MUSIC_INFO_INVALID = -2;//媒体资源信息为空
    public static final int PLAY_STATUS_FILE_NOT_FOUND = -1;//媒体资源文件未能找到/不存在
    public static final int PLAY_STATUS_MUSIC_LIST_EMPTY = -3;//播放列表为空

    boolean closeMicToStopMusic = false;

    /**
     * 刷新本地音乐库
     */
    void refreshLocalMusic();

    void addMusicToPlayerListAndUpdateDb(LocalMusicInfo localMusicInfo);
    void addMusicToPlayerList(LocalMusicInfo localMusicInfo);

    void deleteMusicFromPlayerListAndUpdateDb(LocalMusicInfo localMusicInfo);
    void deleteMusicFromPlayerList(LocalMusicInfo localMusicInfo);

    void replaceMusicInPlayerList(LocalMusicInfo oldMusicInfo,LocalMusicInfo newMusicInfo);

    /**
     * 查询本地音乐列表
     */
    List<LocalMusicInfo> requestLocalMusicInfos();

    /**
     * 查询本地播放器列表
     *
     * @return
     */
    List<LocalMusicInfo> requestPlayerListLocalMusicInfos();

    boolean isScaning();

    int getState();

    LocalMusicInfo getCurrLocalMusicInfo();

    void clearCurrLocalMusicInifo();

    void updateLocalMusicInfo(LocalMusicInfo localMusicInfo);

    long getCurrLocalMusicLocalId();

    List<LocalMusicInfo> getPlayerListMusicInfos();

    int play(LocalMusicInfo localMusicInfo);

    void pause();

    void stop();

    void seekPlaybackSignalVolume(int volume);

    void seekVolume(int volume);

    void seekRecordingVolume(int volume);

    int getCurrentVolume();

    int getCurrentRecordingVolume();

    int playNext();

    void updateMusicPlayProgress();

    void setAudioMixCurrPosition(int position);
}
