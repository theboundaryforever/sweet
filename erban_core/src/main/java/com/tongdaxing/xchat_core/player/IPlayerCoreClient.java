package com.tongdaxing.xchat_core.player;

import com.tongdaxing.erban.libcommon.coremanager.ICoreClient;
import com.tongdaxing.xchat_core.player.bean.LocalMusicInfo;

import java.util.List;

/**
 * Created by chenran on 2017/10/28.
 */

public interface IPlayerCoreClient extends ICoreClient{

    public static final String METHOD_ON_REFRESH_LOCAL_MUSIC = "onRefreshLocalMusic";
    void onRefreshLocalMusic(List<LocalMusicInfo> localMusicInfoList);

    public static final String METHOD_ON_PLAYER_INSERT = "onPlayerListInsert";
    void onPlayerListInsert(LocalMusicInfo musicInfo);

    public static final String METHOD_ON_MUSIC_PLAYING = "onMusicPlaying";
    void onMusicPlaying(LocalMusicInfo localMusicInfo);

    public static final String METHOD_ON_MUSIC_PAUSE = "onMusicPause";
    void onMusicPause(LocalMusicInfo localMusicInfo);

    public static final String METHOD_ON_MUSIC_STOP = "onMusicStop";
    void onMusicStop();

    public static final String METHOD_ON_PLAYER_MUSIC_UPDATE = "onPlayerListMusicUpdate";
    void onPlayerListMusicUpdate(LocalMusicInfo oldMusicInfo, LocalMusicInfo newMusicInfo);

    public static final String METHOD_ON_MUSIC_PROGRESS_UPDATE = "onMusicProgressUpdate";
    void onMusicProgressUpdate(long total, long current);
}
