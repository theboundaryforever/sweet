package com.tongdaxing.xchat_core.player;

import android.os.Handler;
import android.os.Message;

import com.tongdaxing.erban.libcommon.coremanager.AbstractBaseCore;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.erban.libcommon.utils.pref.CommonPref;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomEvent;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.manager.RtcEngineManager;
import com.tongdaxing.xchat_core.player.bean.LocalMusicInfo;
import com.tongdaxing.xchat_core.utils.AsyncTaskScanMusicFile;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by chenran on 2017/10/28.
 */

public class PlayerCoreImpl extends AbstractBaseCore implements IPlayerCore {
    private AsyncTaskScanMusicFile scanMediaTask;
    private boolean isScaning;
    private List<LocalMusicInfo> playerListMusicInfos;
    private List<LocalMusicInfo> lastScanedMusicInfos = new ArrayList<>();
    private final String AUDIO_SUFFIX_MP3 = ".mp3";
    private LocalMusicInfo currLocalMusicInfo;
    private long currentLocalId;
    private int state;
    private int volume;
    private int recordingVolume;
    private Disposable mDisposable;
    private Disposable mImDisposable;


    public PlayerCoreImpl() {
        CoreManager.addClient(this);
        playerListMusicInfos = requestPlayerListLocalMusicInfos();
        state = STATE_STOP;
        volume = CommonPref.instance(BasicConfig.INSTANCE.getAppContext()).getInt("volume", 50);
        recordingVolume = CommonPref.instance(BasicConfig.INSTANCE.getAppContext()).getInt("recordingVolume", 50);

        mDisposable = IMNetEaseManager.get().getChatRoomEventObservable()
                .subscribe(new Consumer<RoomEvent>() {
                    @Override
                    public void accept(RoomEvent roomEvent) throws Exception {
                        if (roomEvent == null) return;
                        int event = roomEvent.getEvent();
                        if (event == RoomEvent.ROOM_EXIT
                                || event == RoomEvent.KICK_OUT_ROOM
                                || event == RoomEvent.ADD_BLACK_LIST) {
                            state = STATE_STOP;
                            currLocalMusicInfo = null;
                            currentLocalId = 0;
                        } else if (event == RoomEvent.DOWN_MIC || event == RoomEvent.KICK_DOWN_MIC) {
                            if (AvRoomDataManager.get().isOwner(roomEvent.getAccount())){
                                stop();
                            }
                        } else if (event == RoomEvent.METHOD_ON_AUDIO_MIXING_FINISHED) {
                            handler.sendEmptyMessage(Play_Msg_What_Next);
                        }
                    }
                });

        mDisposable = IMRoomMessageManager.get().getIMRoomEventObservable()
                .subscribe(new Consumer<IMRoomEvent>() {
                    @Override
                    public void accept(IMRoomEvent roomEvent) throws Exception {
                        if (roomEvent == null) return;
                        int event = roomEvent.getEvent();
                        if (event == IMRoomEvent.ROOM_EXIT
                                || event == IMRoomEvent.KICK_OUT_ROOM
                                || event == IMRoomEvent.ADD_BLACK_LIST) {
                            state = STATE_STOP;
                            currLocalMusicInfo = null;
                            currentLocalId = 0;
                        } else if (event == IMRoomEvent.DOWN_MIC || event == IMRoomEvent.KICK_DOWN_MIC) {
                            if (RoomDataManager.get().isUserSelf(roomEvent.getAccount())) {
                                stop();
                            }
                        } else if (event == IMRoomEvent.METHOD_ON_AUDIO_MIXING_FINISHED) {
                            handler.sendEmptyMessage(Play_Msg_What_Next);
                        }
                    }
                });
    }

    @Override
    public boolean isScaning() {
        return isScaning;
    }

    @Override
    public int getState() {
        return state;
    }

    @Override
    public LocalMusicInfo getCurrLocalMusicInfo() {
        return currLocalMusicInfo;
    }

    @Override
    public void clearCurrLocalMusicInifo() {
        currLocalMusicInfo = null;
    }

    @Override
    public void updateLocalMusicInfo(LocalMusicInfo localMusicInfo) {
        //AsyncTaskScanMusicFile中的localSongs等同lastScanedMusicInfos，所以只需要更新lastScanedMusicInfos即可
        //可以避免本地列表重新扫描的时候，相同路径的mp3使用的还是localSongs中的数据，导致添加到播放列表，进行播放时，因为没有songid，热门曲库和搜索曲库无法同步播放状态
        int index = lastScanedMusicInfos.indexOf(localMusicInfo);
        if(-1 != index){
            lastScanedMusicInfos.set(index,localMusicInfo);
            //以为是在曲库或者热门曲库界面操作，走的流程，所以不用通知本地列表界面，
        }
        //并更新到数据库
        CoreManager.getCore(IPlayerDbCore.class).insertOrUpdateLocalMusicInfo(localMusicInfo);
        //同步更新到播放列表
        index = playerListMusicInfos.indexOf(localMusicInfo);
        if(-1 != index){
            playerListMusicInfos.set(index,localMusicInfo);
            //通知播放列表数据有更新,因为播放列表如果有该数据的话，那么一定是通过本地列表添加进来的，localId一定对应相等的,以同步信息，避免播放的时候，热门曲库列表播放不同步
            notifyClients(IPlayerCoreClient.class, IPlayerCoreClient.METHOD_ON_PLAYER_MUSIC_UPDATE,localMusicInfo,localMusicInfo);
        }
        if(null != currLocalMusicInfo && currentLocalId>0 && currLocalMusicInfo.getLocalId() == localMusicInfo.getLocalId()){
            currLocalMusicInfo = localMusicInfo;
        }
    }


    @Override
    public long getCurrLocalMusicLocalId() {
        return currentLocalId;
    }

    private PlayHandler handler = new PlayHandler(this);

    private static final int Play_Msg_What_Next = 0;//播放下一首
    private static final int Play_Msg_What_Progress = 1;//更新播放进度

    private static final long intervalOfMusicUpdateTimerExe = 1000L;

    static class PlayHandler extends Handler {
        private WeakReference<PlayerCoreImpl> mWeakReference;

        PlayHandler(PlayerCoreImpl playerCore) {
            mWeakReference = new WeakReference<>(playerCore);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            PlayerCoreImpl playerCore = mWeakReference.get();
            if (playerCore == null){
                return;
            }
            switch (msg.what) {
                case Play_Msg_What_Next:
                    playerCore.playNext();
                    break;
                case Play_Msg_What_Progress:
                    playerCore.updateMusicPlayProgress();
                    break;
                default:
            }
        }
    }

    @Override
    public List<LocalMusicInfo> getPlayerListMusicInfos() {
        return playerListMusicInfos;
    }

    @Override
    public void refreshLocalMusic() {
        if (isScaning) {
            return;
        }

        isScaning = true;
        scanMediaTask = new AsyncTaskScanMusicFile(BasicConfig.INSTANCE.getAppContext(), 0, new AsyncTaskScanMusicFile.ScanMediaCallback() {

            @Override
            public void onComplete(boolean result, List<LocalMusicInfo> musicInfos) {
                isScaning = false;
                lastScanedMusicInfos = musicInfos;
                notifyClients(IPlayerCoreClient.class, IPlayerCoreClient.METHOD_ON_REFRESH_LOCAL_MUSIC,lastScanedMusicInfos);
            }
        });
        scanMediaTask.execute(BasicConfig.INSTANCE.getAppContext());
    }

    @Override
    public void addMusicToPlayerListAndUpdateDb(LocalMusicInfo localMusicInfo) {
        CoreManager.getCore(IPlayerDbCore.class).addToPlayerList(localMusicInfo.getLocalId());
        localMusicInfo.setInPlayerList(true);
//        playerListMusicInfos.add(0,localMusicInfo);
        notifyClients(IPlayerCoreClient.class, IPlayerCoreClient.METHOD_ON_PLAYER_INSERT, localMusicInfo);
    }

    @Override
    public void addMusicToPlayerList(LocalMusicInfo localMusicInfo) {
//        playerListMusicInfos.add(0,localMusicInfo);
        notifyClients(IPlayerCoreClient.class, IPlayerCoreClient.METHOD_ON_PLAYER_INSERT, localMusicInfo);
    }

    @Override
    public void deleteMusicFromPlayerListAndUpdateDb(LocalMusicInfo localMusicInfo) {
        if (currLocalMusicInfo != null && localMusicInfo.getLocalId() == currLocalMusicInfo.getLocalId()) {
            stop();
        }
        CoreManager.getCore(IPlayerDbCore.class).deleteFromPlayerList(localMusicInfo.getLocalId());
        //基本上可以保证上层的playerListMusicInfos指向同一个内存地址，且与底层的realmresults独立
        playerListMusicInfos.remove(localMusicInfo);
    }

    @Override
    public void deleteMusicFromPlayerList(LocalMusicInfo localMusicInfo) {
        if (currLocalMusicInfo != null && localMusicInfo.getLocalId() == currLocalMusicInfo.getLocalId()) {
            stop();
        }
        //基本上可以保证上层的playerListMusicInfos指向同一个内存地址，且与底层的realmresults独立
        playerListMusicInfos.remove(localMusicInfo);
    }

    @Override
    public void replaceMusicInPlayerList(LocalMusicInfo oldMusicInfo, LocalMusicInfo newMusicInfo) {
        if(null != oldMusicInfo && playerListMusicInfos.contains(oldMusicInfo)){
//            playerListMusicInfos.set(playerListMusicInfos.indexOf(oldMusicInfo),newMusicInfo);
            notifyClients(IPlayerCoreClient.class, IPlayerCoreClient.METHOD_ON_PLAYER_MUSIC_UPDATE, oldMusicInfo,newMusicInfo);
        }
    }

    @Override
    public List<LocalMusicInfo> requestLocalMusicInfos() {
        if(lastScanedMusicInfos.size() == 0){
            lastScanedMusicInfos = CoreManager.getCore(IPlayerDbCore.class).queryAllLocalMusicInfos();
        }
        return lastScanedMusicInfos;
    }

    @Override
    public List<LocalMusicInfo> requestPlayerListLocalMusicInfos() {
        return CoreManager.getCore(IPlayerDbCore.class).queryPlayerListLocalMusicInfos();
    }

    @Override
    public int play(LocalMusicInfo localMusicInfo) {
        if(null != localMusicInfo){
            if (currLocalMusicInfo != null && state == STATE_PAUSE && currLocalMusicInfo.getLocalId() == localMusicInfo.getLocalId()) {
                state = STATE_PLAY;
                RtcEngineManager.get().resumeAudioMixing();
                notifyClients(IPlayerCoreClient.class, IPlayerCoreClient.METHOD_ON_MUSIC_PLAYING, currLocalMusicInfo);
                if(null != handler){
                    handler.sendEmptyMessageDelayed(Play_Msg_What_Progress,intervalOfMusicUpdateTimerExe);
                }
                return PLAY_STATUS_SUCCESS;
            }
        }else{
            if(null == playerListMusicInfos || playerListMusicInfos.size() == 0){
                return PLAY_STATUS_MUSIC_LIST_EMPTY;
            }
            localMusicInfo = playerListMusicInfos.get(0);
        }

        File file = new File(localMusicInfo.getLocalUri());
        if (!file.exists()) {
            return PLAY_STATUS_FILE_NOT_FOUND;
        }

        RtcEngineManager.get().adjustAudioMixingVolume(volume);
        RtcEngineManager.get().adjustRecordingSignalVolume(recordingVolume);
        int result = RtcEngineManager.get().startAudioMixing(localMusicInfo.getLocalUri(), false, 1);
        if (result == PLAY_STATUS_MUSIC_INFO_INVALID) {
            return PLAY_STATUS_MUSIC_INFO_INVALID;
        }
        currLocalMusicInfo = localMusicInfo;
        currentLocalId = currLocalMusicInfo.getLocalId();
        state = STATE_PLAY;
        notifyClients(IPlayerCoreClient.class, IPlayerCoreClient.METHOD_ON_MUSIC_PLAYING, currLocalMusicInfo);
        if(null != handler){
            handler.sendEmptyMessageDelayed(Play_Msg_What_Progress,intervalOfMusicUpdateTimerExe);
        }
        return PLAY_STATUS_SUCCESS;
    }

    @Override
    public void pause() {
        if (state == STATE_PLAY) {
            RtcEngineManager.get().pauseAudioMixing();
            state = STATE_PAUSE;
            notifyClients(IPlayerCoreClient.class, IPlayerCoreClient.METHOD_ON_MUSIC_PAUSE, currLocalMusicInfo);
            if(null != handler){
                handler.removeMessages(Play_Msg_What_Progress);
            }
        }
    }

    @Override
    public void stop() {
        if (state != STATE_STOP) {
            RtcEngineManager.get().stopAudioMixing();
            state = STATE_STOP;
            currentLocalId = 0;
            notifyClients(IPlayerCoreClient.class, IPlayerCoreClient.METHOD_ON_MUSIC_STOP);
            if(null != handler){
                handler.removeMessages(Play_Msg_What_Progress);
            }
        }
    }

    @Override
    public int playNext() {
        if (0 == currentLocalId) {
            if (playerListMusicInfos.size() > 0) {
                if(null != handler){
                    handler.removeMessages(Play_Msg_What_Progress);
                }
                return play(playerListMusicInfos.get(0));
            } else {
                return PLAY_STATUS_MUSIC_LIST_EMPTY;
            }
        } else {
            int index = 0;
            if(playerListMusicInfos.size() == 0){
                return PLAY_STATUS_MUSIC_LIST_EMPTY;
            }
            if(null != currLocalMusicInfo){
                index = playerListMusicInfos.indexOf(currLocalMusicInfo);
                if (index == playerListMusicInfos.size() - 1) {
                    index = 0;
                } else {
                    index += 1;
                }
            }
            if(null != handler){
                handler.removeMessages(Play_Msg_What_Progress);
            }
            return play(playerListMusicInfos.get(index));
        }
    }

    @Override
    public void setAudioMixCurrPosition(int position) {
        RtcEngineManager.get().setAudioMixingPosition(position);
    }

    @Override
    public void updateMusicPlayProgress(){
        long totalDur = RtcEngineManager.get().getAudioMixingDuration();
        long currPosition = RtcEngineManager.get().getAudioMixingCurrentPosition();
        if(totalDur > 0 && currPosition>0){
            notifyClients(IPlayerCoreClient.class, IPlayerCoreClient.METHOD_ON_MUSIC_PROGRESS_UPDATE, totalDur,currPosition);
            if(null != handler){
                handler.sendEmptyMessageDelayed(Play_Msg_What_Progress,intervalOfMusicUpdateTimerExe);
            }
        }
    }

    @Override
    public void seekPlaybackSignalVolume(int volume) {
        this.volume = volume;
        CommonPref.instance(BasicConfig.INSTANCE.getAppContext()).putInt("PlaybackSignalVolume", volume);
        RtcEngineManager.get().adjustPlaybackSignalVolume(volume);
    }

    @Override
    public void seekVolume(int volume) {
        this.volume = volume;
        CommonPref.instance(BasicConfig.INSTANCE.getAppContext()).putInt("volume", volume);
        LogUtils.d("changeVolume", volume);
        RtcEngineManager.get().adjustAudioMixingVolume(volume);
    }

    @Override
    public void seekRecordingVolume(int volume) {
        this.recordingVolume = volume;
        CommonPref.instance(BasicConfig.INSTANCE.getAppContext()).putInt("recordingVolume", volume);
        RtcEngineManager.get().adjustRecordingSignalVolume(volume);
    }

    @Override
    public int getCurrentVolume() {
        return volume;
    }

    @Override
    public int getCurrentRecordingVolume() {
        return recordingVolume;
    }
}
