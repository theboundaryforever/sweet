package com.tongdaxing.xchat_core.player;

import com.tongdaxing.erban.libcommon.coremanager.IBaseCore;
import com.tongdaxing.xchat_core.player.bean.LocalMusicInfo;

import java.util.List;

/**
 * Created by chenran on 2017/10/31.
 */

public interface IPlayerDbCore extends IBaseCore{

    /**
     * 存储本地音乐
     * @param localId
     */
    public void addToPlayerList(long localId);

    /**
     * 查询歌曲
     * @param localId
     * @return
     */
    public LocalMusicInfo requestLocalMusicInfoByLocalId(long localId);

    /**
     * 删除本地音乐
     * @param localId
     */
    public void deleteFromPlayerList(long localId);

    /**
     * 扫描本地音乐
     */
    public void replaceAllLocalMusics(List<LocalMusicInfo> localMusicInfoList);

    /**
     * 查询本地所有音乐
     */
    public List<LocalMusicInfo> queryAllLocalMusicInfos();

    /**
     * 查询本地音乐播放列表音乐
     */
    public List<LocalMusicInfo> queryPlayerListLocalMusicInfos();

    public LocalMusicInfo gueryLocalMusicInfoByLocalUrl(String localUrl);

    /**
     * 插入下载后扫描得到的localMusicInfo到本地realm数据库
     */
    public void insertOrUpdateLocalMusicInfo(LocalMusicInfo localMusicInfo);
}
