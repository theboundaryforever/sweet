package com.tongdaxing.xchat_core.legion;

import com.tongdaxing.erban.libcommon.coremanager.ICoreClient;

public interface ILegionClient extends ICoreClient {


    public static final String METHON_ON_BLOCK_USER = "onBlockUser";
    void onBlockUser(boolean isSuccess, String msg);

    public static final String METHON_ON_UNBLOCK_USER = "onUnBlockUser";
    void onUnBlockUser(boolean isSuccess, String msg);

    public static final String METHON_ON_FORBIDDEN_WORD_USER = "onForbiddenWordUser";
    void onForbiddenWordUser(boolean isSuccess, String msg);

    public static final String METHON_ON_CANCEL_FORBIDDEN_WORD_USER = "onCancelForbiddenWordUser";
    void onCancelForbiddenWordUser(boolean isSuccess, String msg);
}
