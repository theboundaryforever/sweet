package com.tongdaxing.xchat_core.legion;

import com.tongdaxing.erban.libcommon.coremanager.IBaseCore;

/**
 * 甜甜军团相关业务处理
 */
public interface ILegionCore extends IBaseCore {

    /**
     * 封禁用户
     * @param targetUserId
     * @param timeType 封禁时长类型
     */
    void blockUser(long targetUserId, int timeType);

    /**
     * 解除封禁用户
     * @param targetUserId
     */
    void unBlockUser(long targetUserId);

    /**
     * 禁言用户
     * @param targetUserId
     * @param timeType 封禁时长类型
     */
    void forbiddenWordUser(long targetUserId, int timeType);

    /**
     * 解除禁言用户
     * @param targetUserId
     */
    void cancelForbiddenWordUser(long targetUserId);

}



