package com.tongdaxing.xchat_core.legion;

import com.tongdaxing.erban.libcommon.coremanager.AbstractBaseCore;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;

import java.util.Map;

public class LegionCoreImpl extends AbstractBaseCore implements ILegionCore {
    /**
     * 封禁用户
     *
     * @param targetUserId
     * @param timeType     封禁时长类型
     */
    @Override
    public void blockUser(long targetUserId, int timeType) {
        Map<String,String> params = CommonParamUtil.getDefaultParam();
        params.put("targerUid", targetUserId+"");
        params.put("type", timeType+"");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid()+"");
        OkHttpManager.getInstance().postRequest(UriProvider.adminBlockUrl(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(ILegionClient.class, ILegionClient.METHON_ON_BLOCK_USER,false,e.getMessage());
            }

            @Override
            public void onResponse(Json json) {
                notifyClients(ILegionClient.class, ILegionClient.METHON_ON_BLOCK_USER,json.num("code")==200,json.str("message"));
            }
        });
    }

    /**
     * 解除封禁用户
     *
     * @param targetUserId
     */
    @Override
    public void unBlockUser(long targetUserId) {
        Map<String,String> params = CommonParamUtil.getDefaultParam();
        params.put("targerUid", targetUserId+"");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid()+"");
        OkHttpManager.getInstance().postRequest(UriProvider.adminUnBlockUrl(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(ILegionClient.class, ILegionClient.METHON_ON_UNBLOCK_USER,false,e.getMessage());
            }

            @Override
            public void onResponse(Json json) {
                notifyClients(ILegionClient.class, ILegionClient.METHON_ON_UNBLOCK_USER,json.num("code")==200,json.str("message"));
            }
        });
    }

    /**
     * 禁言用户
     *
     * @param targetUserId
     * @param timeType     封禁时长类型
     */
    @Override
    public void forbiddenWordUser(long targetUserId, int timeType) {
        Map<String,String> params = CommonParamUtil.getDefaultParam();
        params.put("targerUid", targetUserId+"");
        params.put("type", timeType+"");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid()+"");
        OkHttpManager.getInstance().postRequest(UriProvider.adminMuteUrl(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(ILegionClient.class, ILegionClient.METHON_ON_FORBIDDEN_WORD_USER,false,e.getMessage());
            }

            @Override
            public void onResponse(Json json) {
                notifyClients(ILegionClient.class, ILegionClient.METHON_ON_FORBIDDEN_WORD_USER,json.num("code")==200,json.str("message"));
            }
        });
    }

    /**
     * 解除禁言用户
     *
     * @param targetUserId
     */
    @Override
    public void cancelForbiddenWordUser(long targetUserId) {
        Map<String,String> params = CommonParamUtil.getDefaultParam();
        params.put("targerUid", targetUserId+"");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid()+"");
        OkHttpManager.getInstance().postRequest(UriProvider.adminUnMuteUrl(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(ILegionClient.class, ILegionClient.METHON_ON_CANCEL_FORBIDDEN_WORD_USER,false,e.getMessage());
            }

            @Override
            public void onResponse(Json json) {
                notifyClients(ILegionClient.class, ILegionClient.METHON_ON_CANCEL_FORBIDDEN_WORD_USER,json.num("code")==200,json.str("message"));
            }
        });
    }
}
