package com.tongdaxing.xchat_core.manager;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nimlib.sdk.NIMChatRoomSDK;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.Observer;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.ResponseCode;
import com.netease.nimlib.sdk.StatusCode;
import com.netease.nimlib.sdk.chatroom.ChatRoomMessageBuilder;
import com.netease.nimlib.sdk.chatroom.ChatRoomService;
import com.netease.nimlib.sdk.chatroom.constant.MemberQueryType;
import com.netease.nimlib.sdk.chatroom.constant.MemberType;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomKickOutEvent;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomNotificationAttachment;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomQueueChangeAttachment;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomStatusChangeData;
import com.netease.nimlib.sdk.chatroom.model.MemberOption;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.netease.nimlib.sdk.msg.attachment.NotificationAttachment;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.msg.constant.NotificationType;
import com.netease.nimlib.sdk.msg.model.NIMAntiSpamOption;
import com.netease.nimlib.sdk.util.Entry;
import com.netease.nimlib.sdk.util.api.RequestResult;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.exception.ErrorThrowable;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.TimeUtils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RoomMicInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.bean.attachmsg.RoomQueueMsgAttachment;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.gift.MultiGiftReceiveInfo;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCoreClient;
import com.tongdaxing.xchat_core.im.custom.bean.AnnualRoomRankAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.DetonateGiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.GiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.LotteryBoxAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.MicInListAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.MultiGiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.NoblePublicMsgAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.OpenNobleNotifyAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.PkCustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomCharmListAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomPairAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomRedPacketAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomRedPacketFinishAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomRuleAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomTipAttachment;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomCoreClient;
import com.tongdaxing.xchat_core.noble.NobleBusinessManager;
import com.tongdaxing.xchat_core.pk.bean.PkVoteInfo;
import com.tongdaxing.xchat_core.room.IRoomCore;
import com.tongdaxing.xchat_core.room.IRoomCoreClient;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.face.IFaceCore;
import com.tongdaxing.xchat_core.room.model.AvRoomModel;
import com.tongdaxing.xchat_core.room.publicchatroom.PublicChatRoomController;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_core.user.bean.MedalBean;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import io.agora.rtc.Constants;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.SingleSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiConsumer;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.processors.PublishProcessor;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

import static com.tongdaxing.xchat_core.Constants.NOBLE_INVISIABLE_ENTER_ROOM;
import static com.tongdaxing.xchat_core.Constants.NOBLE_INVISIABLE_UID;
import static com.tongdaxing.xchat_core.Constants.PUBLIC_CHAT_ROOM;
import static com.tongdaxing.xchat_core.Constants.USER_AVATAR;
import static com.tongdaxing.xchat_core.Constants.USER_GENDER;
import static com.tongdaxing.xchat_core.Constants.USER_MEDAL_DATE;
import static com.tongdaxing.xchat_core.Constants.USER_MEDAL_ID;
import static com.tongdaxing.xchat_core.Constants.USER_NICK;
import static com.tongdaxing.xchat_core.Constants.USER_UID;
import static com.tongdaxing.xchat_core.Constants.hasVggPic;
import static com.tongdaxing.xchat_core.Constants.headwearUrl;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_ANNUAL_ROOM_LIST;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_AUTH_STATUS_NOTIFY;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_AUCTION;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_FACE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_KICK_ROOM;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_INVITE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_KICK;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_OPEN_MIC;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUPER_GIFT;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_LOTTERY_BOX;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_MIC_IN_LIST;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_NOBLE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_NOBLE_SEND_PUBLIC_MSG;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_OPEN_NOBLE_NOTIFY;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_ROOM_CHARM;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_ROOM_DETONATING_GIFT;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_ROOM_PAIR;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_ROOM_RED_PACKET;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_ROOM_RED_PACKET_FINISH;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_SUB_PUBLIC_CHAT_ROOM;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_NOTI_HEADER_KTV;

/**
 * <p>云信聊天室管理，一个全局的Model </p>
 *
 * @author jiahui
 * @date 2017/12/12
 */
public final class IMNetEaseManager {
    private static final String TAG = "IMNetEaseManager";

    private static final Object SYNC_OBJECT = new Object();
    private static volatile IMNetEaseManager sManager;
    private final AvRoomModel model;
    public boolean inAvRoom = false;

    public List<ChatRoomMessage> messages;

    private RoomQueueInfo mCacheRoomQueueInfo;
    private String mMessId;
    private PublishProcessor<RoomEvent> roomProcessor;
    private PublishSubject<ChatRoomMessage> msgProcessor;

    /**
     * 是否已连接
     */
    private boolean isConnected;

    //进入房间坐驾动画计时器
    private Timer timer;

    private IMNetEaseManager() {
        registerInComingRoomMessage();
        registerKickoutEvent();
        registerOnlineStatusChange();
        messages = new ArrayList<>();
        model = new AvRoomModel();
    }

    public static IMNetEaseManager get() {
        if (sManager == null) {
            synchronized (SYNC_OBJECT) {
                if (sManager == null) {
                    sManager = new IMNetEaseManager();
                }
            }
        }
        return sManager;
    }

    private void registerOnlineStatusChange() {
        Observer<ChatRoomStatusChangeData> onlineStatus = new Observer<ChatRoomStatusChangeData>() {

            @Override
            public void onEvent(ChatRoomStatusChangeData chatRoomStatusChangeData) {
                //拦截其他房间的信息
                if (filterOtherRoomMsg(chatRoomStatusChangeData.roomId, false)) {
                    LogUtils.d(TAG, "onlineStatus - 其他房间信息_被拦截，roomId = " + chatRoomStatusChangeData.roomId);
                    return;
                }
                dealChatRoomOnlineStatus(chatRoomStatusChangeData);
            }

        };
        NIMChatRoomSDK.getChatRoomServiceObserve().observeOnlineStatus(onlineStatus, true);
    }

    private void registerKickoutEvent() {
        Observer<ChatRoomKickOutEvent> kickOutObserver = new Observer<ChatRoomKickOutEvent>() {
            @Override
            public void onEvent(ChatRoomKickOutEvent chatRoomKickOutEvent) {
                LogUtil.e(TAG, "收到踢人信息");
                //拦截其他房间的信息
                if (filterOtherRoomMsg(chatRoomKickOutEvent.getRoomId(), false)) {
                    LogUtils.d(TAG, "onlineStatus - 其他房间信息_被拦截，roomId = " + chatRoomKickOutEvent.getRoomId());
                    return;
                }

                // 提示被踢出的原因（聊天室已解散、被管理员踢出、被其他端踢出等
                Map<String, Object> extension = chatRoomKickOutEvent.getExtension();
                String account = null;
                if (extension != null) {
                    account = (String) extension.get("account");
                }
                noticeKickOutChatMember(chatRoomKickOutEvent, account);
                // 清空缓存数据
                AvRoomDataManager.get().release();
            }
        };
        NIMChatRoomSDK.getChatRoomServiceObserve().observeKickOutEvent(kickOutObserver, true);
    }

    private void registerInComingRoomMessage() {
        Observer<List<ChatRoomMessage>> incomingChatObserver = new Observer<List<ChatRoomMessage>>() {
            @Override
            public void onEvent(List<ChatRoomMessage> chatRoomMessages) {
                if (ListUtils.isListEmpty(chatRoomMessages)) {
                    return;
                }
                dealChatMessage(chatRoomMessages);
            }
        };
        NIMChatRoomSDK.getChatRoomServiceObserve().observeReceiveMessage(incomingChatObserver, true);
    }

    private void dealChatRoomOnlineStatus(ChatRoomStatusChangeData chatRoomStatusChangeData) {
        long currentUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if (chatRoomStatusChangeData.status == StatusCode.CONNECTING) {
//            MLog.info(TAG, "连接中...");
        } else if (chatRoomStatusChangeData.status == StatusCode.UNLOGIN) {

            setConnected(false);
            RtcEngineManager.get().setRole(Constants.CLIENT_ROLE_AUDIENCE);

            int errorCode = NIMChatRoomSDK.getChatRoomService().getEnterErrorCode(chatRoomStatusChangeData.roomId);
            LogUtil.d(TAG, "dealChatRoomOnlineStatus: error errorCode = " + errorCode);
            // 如果遇到错误码13001，13002，13003，403，404，414，表示无法进入聊天室，此时应该调用离开聊天室接口。
            if (errorCode == ResponseCode.RES_CHATROOM_STATUS_EXCEPTION) {
                // 聊天室连接状态异常
//                MLog.error(TAG, "聊天室状态异常！");
            }

            if (errorCode == ResponseCode.RES_CHATROOM_BLACKLIST) {
                RtcEngineManager.get().leaveChannel();
            }
//            MLog.error(TAG, "聊天室在线状态变为UNLOGIN！");
            if (AvRoomDataManager.get().isOnMic(currentUid)) {
                RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoByAccount(String.valueOf(currentUid));
                if (roomQueueInfo == null) {
                    return;
                }
                mCacheRoomQueueInfo = new RoomQueueInfo(roomQueueInfo.mRoomMicInfo, roomQueueInfo.mChatRoomMember);
            }
        } else if (chatRoomStatusChangeData.status == StatusCode.LOGINING) {
//            MLog.info(TAG, "登录中...");
        } else if (chatRoomStatusChangeData.status == StatusCode.LOGINED) {
            LogUtil.d(TAG, "dealChatRoomOnlineStatus: 云信聊天室已登录成功");
            String publicChatRoomId = PublicChatRoomController.getPublicChatRoomId() + "";
            String roomId = chatRoomStatusChangeData.roomId;
            LogUtils.d("chatRoomStatusChangeData", roomId + "");
            setConnected(true);
            //过滤公聊
            if (!publicChatRoomId.equals(roomId)) {
                noticeImNetReLogin(mCacheRoomQueueInfo);
            }
        } else if (chatRoomStatusChangeData.status.wontAutoLogin()) {
            LogUtil.d(TAG, "dealChatRoomOnlineStatus: 需要重新登录（被踢或验证信息错误）...");

            //  需要重新登录（被踢或验证信息错误）
            setConnected(false);
            RtcEngineManager.get().setRole(Constants.CLIENT_ROLE_AUDIENCE);

        } else if (chatRoomStatusChangeData.status == StatusCode.NET_BROKEN) {
            LogUtil.d(TAG, "dealChatRoomOnlineStatus: 网络断开...");
            setConnected(false);
            RtcEngineManager.get().setRole(Constants.CLIENT_ROLE_AUDIENCE);

            if (AvRoomDataManager.get().isOnMic(currentUid)) {
                RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoByAccount(String.valueOf(currentUid));
                if (roomQueueInfo == null) {
                    return;
                }
                mCacheRoomQueueInfo = new RoomQueueInfo(roomQueueInfo.mRoomMicInfo, roomQueueInfo.mChatRoomMember);

            }
        }
    }

    /**
     * 接受所有网易云服务器发来的（大厅广场）消息,但是私聊的消息通知封装在sdk内部，不在这里接受
     * <p>
     * 广场消息对应的房间ID写死
     *
     * @param chatRoomMessages
     */
    private void dealChatMessage(List<ChatRoomMessage> chatRoomMessages) {
        ArrayList<ChatRoomMessage> messages = new ArrayList<>();
        boolean face = false;
        boolean gift = false;


        for (ChatRoomMessage msg : chatRoomMessages) {
            LogUtils.d(TAG, "dealChatMessage-msg.getMsgType:" + msg.getMsgType());
            //拦截其他房间的信息
            if (filterOtherRoomMsg(msg.getSessionId(), true)) {
                LogUtils.d(TAG, "dealChatMessage-其他房间信息_被拦截 sessionId:" + msg.getSessionId());
                continue;
            }

            if (msg.getMsgType() == MsgTypeEnum.notification) {
                NotificationAttachment attachment = (NotificationAttachment) msg.getAttachment();
                if (attachment == null) {
                    continue;
                }
                //如果是公聊大厅的通知消息，修复公聊大厅可能下麦的问题
                LogUtils.d(TAG, "dealChatMessage-attachment.getType:" + attachment.getType() + " fromAccount:" + msg.getFromAccount());
                try {
                    String publicChatRoom = (String) msg.getChatRoomMessageExtension().getSenderExtension().get(PUBLIC_CHAT_ROOM);
                    if (PUBLIC_CHAT_ROOM.equals(publicChatRoom)) {
                        LogUtils.d(TAG, "dealChatMessage-attachment.getType:" + attachment.getType() + " 公聊大厅信息_被拦截");
                        continue;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                LogUtils.d(TAG, "dealChatMessage-attachment.getType:" + attachment.getType() + " 房间信息_未拦截");
                if (attachment.getType() == NotificationType.ChatRoomQueueChange) {
                    chatRoomQueueChangeNotice(msg);
                } else if (attachment.getType() == NotificationType.ChatRoomInfoUpdated) {
                    chatRoomInfoUpdate(msg);
                } else if (attachment.getType() == NotificationType.ChatRoomMemberIn) {
                    addMessagesImmediately(msg);
                    List<String> targets = ((ChatRoomNotificationAttachment) attachment).getTargets();
                    String account = targets.get(0);
                    //进房动画800毫秒
                    boolean isUserSelf = account.equals(String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
                    LogUtils.d(TAG, "dealChatMessage-ChatRoomMemberIn isUserSelf:" + isUserSelf);
                    try {
                        final String carName = (String) msg.getChatRoomMessageExtension().getSenderExtension().get("user_car");
                        if (!TextUtils.isEmpty(carName)) {
                            if (timer != null) {
                                timer.cancel();
                            }
                            timer = new Timer();
                            timer.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    CoreManager.notifyClients(IAVRoomCoreClient.class, IAVRoomCoreClient.onUserCarIn, carName);
                                }
                            }, isUserSelf ? 800 : 300);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    chatRoomMemberIn(account);
                } else if (attachment.getType() == NotificationType.ChatRoomMemberExit || attachment.getType() == NotificationType.ChatRoomMemberKicked) {
                    String fromAccount = msg.getFromAccount();
                    List<String> targets = ((ChatRoomNotificationAttachment) attachment).getTargets();
                    chatRoomMemberExit(targets.get(0), fromAccount);
                } else if (attachment.getType() == NotificationType.ChatRoomManagerAdd) {
                    //管理员id集合列表
                    List<String> targets = ((ChatRoomNotificationAttachment) attachment).getTargets();
                    addManagerMember(targets.get(0));
                } else if (attachment.getType() == NotificationType.ChatRoomManagerRemove) {
                    List<String> targets = ((ChatRoomNotificationAttachment) attachment).getTargets();
                    removeManagerMember(targets.get(0));
                } else if (attachment.getType() == NotificationType.ChatRoomMemberBlackAdd) {
                    List<String> targets = ((ChatRoomNotificationAttachment) attachment).getTargets();
                    noticeChatMemberBlackAdd(targets.get(0));
                } else if (attachment.getType() == NotificationType.ChatRoomQueueBatchChange) {
                    //-----排麦时杀掉进程，首个在麦上的人帮他移除------
                    String fromAccount = msg.getFromAccount();
                    SparseArray<Json> mMicInListMap = AvRoomDataManager.get().mMicInListMap;
                    if (fromAccount != null && mMicInListMap != null) {
                        final Json json = mMicInListMap.get(Integer.parseInt(fromAccount));
                        if (json != null) {
                            AvRoomDataManager.get().removeMicListInfo(fromAccount);
                            noticeMicInList();
                            LogUtils.d("ChatRoomQueueBatchChange", 2 + "");
                        } else {
                            LogUtils.d("ChatRoomQueueBatchChange", 1 + "");
                        }
                    }
                    //-----排麦时杀掉进程，首个在麦上的人帮他移除------
                }
            } else if (msg.getMsgType() == MsgTypeEnum.custom) {
                MsgAttachment attachment = msg.getAttachment();
                if (attachment == null) {
                    LogUtil.d("CustomAttachment", "attachment==null");
                    return;
                }

                //自定义消息
                CustomAttachment customAttachment = (CustomAttachment) msg.getAttachment();
                if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_AUCTION
                        || customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT
                        || customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_ROOM_TIP
                        || customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT
                        || customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_ROOM_RICH_TXT
                        || customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_ROOM_LOVER_UP_MIC
                        || (customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_BOSON_FIRST
                        && customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_BOSON_UP_MACRO)
                        || (customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_FIRST_CALL_UP
                        && customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_PUBLIC_CHAT_ROOM)
                ) {
                    addMessages(msg);
                }

                switch (customAttachment.getFirst()) {
                    case CUSTOM_MSG_HEADER_TYPE_AUCTION:
                        //已废弃的竞拍房逻辑
                        break;
                    case CUSTOM_MSG_HEADER_TYPE_QUEUE:
                        boolean messageViewShow = true;
                        RoomQueueMsgAttachment queueMsgAttachment = (RoomQueueMsgAttachment) attachment;
                        int position = queueMsgAttachment.micPosition;
                        String targetNick = queueMsgAttachment.targetNick;
//                        LogUtils.d("RoomQueueMsgAttachment", "getSecond：" + queueMsgAttachment.getSecond());
//                        LogUtils.d("RoomQueueMsgAttachment", targetNick + "  " + queueMsgAttachment.handleNick);
                        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
                        if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_INVITE) {
                            //邀請上麥
                            if (Objects.equals(queueMsgAttachment.uid, String.valueOf(uid))) {
                                noticeInviteUpMic(position, queueMsgAttachment.uid);
                            }
                            if (TextUtils.isEmpty(targetNick)) {
                                messageViewShow = false;
                            }
                        } else if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_KICK) {
                            //踢他下麥
                            if (Objects.equals(queueMsgAttachment.uid, String.valueOf(uid))) {
                                int micPosition = AvRoomDataManager.get().getMicPosition(uid);
                                noticeKickDownMic(micPosition);
                            }
                            if (TextUtils.isEmpty(targetNick)) {
                                messageViewShow = false;
                            }
                        } else if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_CLOSE_MIC) {
                            if (position < 0) {
                                messageViewShow = false;
                            }
                        } else if (customAttachment.getSecond() == CUSTOM_MSG_HEADER_TYPE_QUEUE_OPEN_MIC) {
                            if (position < 0) {
                                messageViewShow = false;
                            }
                        } else if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_LOCK_MIC) {
                            if (position < 0) {
                                messageViewShow = false;
                            }
                        } else if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_LOCK_MIC_ALL) {
                            if (position < 0) {
                                messageViewShow = false;
                            }
                        } else if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_UNLOCK_MIC) {
                            if (position < 0) {
                                messageViewShow = false;
                            }
                        } else if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_UNLOCK_MIC_ALL) {
                            if (position < 0) {
                                messageViewShow = false;
                            }
                        } else if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_KICK_ROOM) {
                            if (TextUtils.isEmpty(targetNick)) {
                                messageViewShow = false;
                            }
                        } else if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_ADD_BLACK_LIST) {
                            if (TextUtils.isEmpty(targetNick)) {
                                messageViewShow = false;
                            }
                        } else if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_BLACK_LIST) {
                            if (TextUtils.isEmpty(targetNick)) {
                                messageViewShow = false;
                            }
                        } else if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_CLOSE) {

                        } else if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_OPEN) {

                        } else if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_OPEN) {
                        } else if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_CLOSE) {
                        } else {
                            messageViewShow = false;
                        }
                        if (messageViewShow) {
                            IMNetEaseManager.get().addMessagesImmediately(msg);
                        }
                        break;
                    case CUSTOM_MSG_HEADER_TYPE_FACE:
                        messages.add(msg);
                        face = true;
                        break;
                    case CUSTOM_MSG_HEADER_TYPE_GIFT:
                    case CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT:
                    case CUSTOM_MSG_HEADER_TYPE_SUPER_GIFT:
                        gift = true;
                        messages.add(msg);
                        break;
                    case CUSTOM_MSG_SUB_PUBLIC_CHAT_ROOM:
                        addMessages(msg);
                        break;
                    case CUSTOM_MSG_LOTTERY_BOX:
                        //避免同一条中奖信息重复显示
                        LotteryBoxAttachment lotteryBoxAttachment = (LotteryBoxAttachment) attachment;
                        Json msgJson = Json.parse(lotteryBoxAttachment.getParams() + "");
                        String messId = msgJson.str("messId", "");
                        LogUtils.d("LotteryBoxAttachment", "messId:" + messId);
                        if (!TextUtils.isEmpty(messId) && messId.equals(mMessId)) {
                            break;
                        } else {
                            mMessId = messId;
                            addMessages(msg);
                        }
                        break;
                    case CUSTOM_MSG_MIC_IN_LIST:
                        MicInListAttachment micInListAttachment = (MicInListAttachment) msg.getAttachment();
                        String params = micInListAttachment.getParams();
                        String key = Json.parse(params).str("key");
                        SparseArray<RoomQueueInfo> mMicQueueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;
                        if (!TextUtils.isEmpty(key) && mMicQueueMemberMap != null) {
                            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.get(Integer.parseInt(key));
                            if (roomQueueInfo != null && !roomQueueInfo.mRoomMicInfo.isMicLock()) {
                                micInListToUpMic(key);
                            }
                        }
                        break;
                    case CUSTOM_MSG_HEADER_TYPE_PK:
                        addMessages(msg);
                        break;
                    case CUSTOM_NOTI_HEADER_KTV:
                        break;
                    case CUSTOM_MSG_ROOM_CHARM:
                        LogUtils.d(TAG, "parseRoomCharmDataFromJSON MicroViewAdapter CUSTOM_MSG_ROOM_CHARM");
                        RoomCharmListAttachment roomCharmListAttachment = (RoomCharmListAttachment) msg.getAttachment();
                        String attachmentParams = roomCharmListAttachment.getParams();
                        noticeRoomCharm(Json.parse(attachmentParams));
                        break;
                    case CUSTOM_MSG_ROOM_PAIR:
                        RoomPairAttachment roomPairAttachment = (RoomPairAttachment) msg.getAttachment();
                        int second = roomPairAttachment.getSecond();
                        String pairAttachmentParams = roomPairAttachment.getParams();
                        Json parse = Json.parse(pairAttachmentParams);
                        parse.set("second", second);
                        pairAttachmentParams = parse + "";
                        LogUtils.d(TAG, "CUSTOM_MSG_ROOM_PAIR-second:" + second + "  pairAttachmentParams:" + pairAttachmentParams);
                        noticeRoomPair(pairAttachmentParams);
                        break;
                    case CUSTOM_MSG_ROOM_DETONATING_GIFT:
                        DetonateGiftAttachment detonateGiftAttachment = (DetonateGiftAttachment) msg.getAttachment();
                        if (null != detonateGiftAttachment) {
                            int eventId = RoomEvent.NONE;
                            if (detonateGiftAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_ROOM_DETONATING_GIFT_NOTIFY) {
                                eventId = RoomEvent.ROOM_DETONATE_GIFT_NOTIFY;
                            } else if (detonateGiftAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_ROOM_DETONATING_GIFT_PROGRESS
                                    && null != AvRoomDataManager.get().mCurrentRoomInfo) {
                                eventId = RoomEvent.ROOM_DETONATE_GIFT_PROGRESS;
                            }
                            getChatRoomEventObservable().onNext(new RoomEvent()
                                    .setEvent(eventId)
                                    .setDetonateGiftAttachment(detonateGiftAttachment));
                        }
                        break;

                    case CUSTOM_MSG_OPEN_NOBLE_NOTIFY:
                        OpenNobleNotifyAttachment openNobleNotifyAttachment = (OpenNobleNotifyAttachment) msg.getAttachment();
                        if (null != openNobleNotifyAttachment) {
                            int eventId = RoomEvent.ROOM_RECEIVE_OPEN_NOBLE_NOTIFY;
                            getChatRoomEventObservable().onNext(new RoomEvent()
                                    .setEvent(eventId)
                                    .setOpenNobleNotifyAttachment(openNobleNotifyAttachment));
                        }
                        break;

                    case CUSTOM_MSG_ANNUAL_ROOM_LIST:
                        AnnualRoomRankAttachment annualRoomRankAttachment = (AnnualRoomRankAttachment) msg.getAttachment();
                        noticeAnnualRoomList(annualRoomRankAttachment);
                        break;
                    case CUSTOM_MSG_AUTH_STATUS_NOTIFY:
                        //更新个人信息
                        CoreManager.getCore(IUserCore.class).requestUserInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
                        break;
                    case CUSTOM_MSG_ROOM_RED_PACKET:
                        //发送红包消息
                        RoomRedPacketAttachment redPacketAttachment = (RoomRedPacketAttachment) msg.getAttachment();
                        if (null != redPacketAttachment) {
                            int eventId = RoomEvent.NONE;
                            if (redPacketAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_ROOM_RED_PACKET_RECEIVE) {
                                eventId = RoomEvent.ROOM_RECEIVE_RED_PACKET;
                            }
                            redPacketAttachment.setMsgTime(msg.getTime());
                            getChatRoomEventObservable().onNext(new RoomEvent()
                                    .setEvent(eventId)
                                    .setRoomRedPacketAttachment(redPacketAttachment));
                        }
                        break;
                    case CUSTOM_MSG_ROOM_RED_PACKET_FINISH:
                        //红包完成消息
                        RoomRedPacketFinishAttachment redPacketFinishAttachment = (RoomRedPacketFinishAttachment) msg.getAttachment();
                        if (null != redPacketFinishAttachment) {
                            int eventId = RoomEvent.NONE;
                            if (redPacketFinishAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_ROOM_RED_PACKET_RECEIVE_FINISH) {
                                eventId = RoomEvent.ROOM_RECEIVE_RED_PACKET_FINISH;
                            }
                            getChatRoomEventObservable().onNext(new RoomEvent()
                                    .setEvent(eventId)
                                    .setRoomRedPacketFinishAttachment(redPacketFinishAttachment));
                        }
                        addMessages(msg);
                        break;
                    case CUSTOM_MSG_NOBLE:
                        //发送贵族全站广播消息
                        NoblePublicMsgAttachment noblePublicMsgAttachment = (NoblePublicMsgAttachment) msg.getAttachment();
                        if (null != noblePublicMsgAttachment && noblePublicMsgAttachment.getSecond() == CUSTOM_MSG_NOBLE_SEND_PUBLIC_MSG) {
                            int eventId = RoomEvent.ROOM_RECEIVE_NOBLE_PUBLIC_MSG;
                            getChatRoomEventObservable().onNext(new RoomEvent()
                                    .setEvent(eventId)
                                    .setNoblePublicMsgAttachment(noblePublicMsgAttachment));
                        }
                        break;
                    default:
                }
            } else if (msg.getMsgType() == MsgTypeEnum.text) {
                addMessages(msg);
            }
        }

        if (face) {
            CoreManager.getCore(IFaceCore.class).onReceiveChatRoomMessages(messages);
        } else if (gift) {
            //分发礼物消息以播放礼物动画
            CoreManager.getCore(IGiftCore.class).onReceiveChatRoomMessages(messages);
        }
    }

    /**
     * 成员进入房间
     */
    private void chatRoomMemberIn(final String account) {
        List<String> list = new ArrayList<>(1);
        list.add(account);
        Disposable disposable = fetchRoomMembersByIds(list).subscribe(new Consumer<List<ChatRoomMember>>() {
            @Override
            public void accept(List<ChatRoomMember> chatRoomMemberList) throws Exception {
                if (ListUtils.isListEmpty(chatRoomMemberList)) {
                    return;
                }
                ChatRoomMember chatRoomMember = chatRoomMemberList.get(0);
                AvRoomDataManager.get().mRoomAllMemberList.add(chatRoomMember);
                if (chatRoomMember.getMemberType() == MemberType.ADMIN) {
                    addManagerMember(chatRoomMember);
                }
                noticeRoomMemberChange(true, account);
            }
        });
    }

    /**
     * 退出房间处理
     */
    private void chatRoomMemberExit(String account, final String fromAccount) {
        noticeRoomMemberChange(false, account);
        RoomInfo mCurrentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (mCurrentRoomInfo == null) {
            return;
        }
        if (AvRoomDataManager.get().isOnMic(Long.valueOf(account))) {
            //在麦上的要退出麦
            LogUtils.d("nim_sdk", "chatRoomMemberExit     " + account);
            IMNetEaseManager.get().downMicroPhoneBySdk(AvRoomDataManager.get().getMicPosition(Long.valueOf(account)), null);
            SparseArray<RoomQueueInfo> mMicQueueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;
            int size = mMicQueueMemberMap.size();
            for (int i = 0; i < size; i++) {
                RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(i);
                if (roomQueueInfo.mChatRoomMember != null
                        && Objects.equals(roomQueueInfo.mChatRoomMember.getAccount(), account)) {

                    roomQueueInfo.mChatRoomMember = null;

                    //增加防炸房校验逻辑
                    try {
                        //同时停止接收此用户的推流
                        RtcEngineManager.get().muteRemoteAudioStream(Integer.valueOf(account), true);
                        //记录一下下麦的时间
                        AvRoomDataManager.get().setTimeOnMicDown(Integer.valueOf(account), System.currentTimeMillis());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    LogUtils.d("remove  mChatRoomMember", 3 + "");
                    noticeDownMic(String.valueOf(mMicQueueMemberMap.keyAt(i)), account);
                    break;
                }
            }
        }

        ChatRoomMember removeChatMember = null;
        boolean isAdmin = false;
        ListIterator<ChatRoomMember> iterator = AvRoomDataManager.get().mRoomAllMemberList.listIterator();
        for (; iterator.hasNext(); ) {
            removeChatMember = iterator.next();
            if (Objects.equals(removeChatMember.getAccount(), account)) {
                if (removeChatMember.getMemberType() == MemberType.ADMIN) {
                    isAdmin = true;
                }
                iterator.remove();
            }
        }
        if (isAdmin) {
            removeManagerMember(removeChatMember);
        }
    }

    /**
     * 麦序更新
     *
     * @param extension -
     */
    private void roomQueueMicUpdate(Map<String, Object> extension) {
        String micInfo = (String) extension.get("micInfo");
        if (!TextUtils.isEmpty(micInfo)) {
            RoomMicInfo roomMicInfo = com.tongdaxing.erban.libcommon.utils.json.JsonParser.parseJsonToNormalObject(micInfo, RoomMicInfo.class);
            if (roomMicInfo != null) {
                int position = roomMicInfo.getPosition();
                RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().mMicQueueMemberMap.get(position);
                if (roomQueueInfo != null) {
                    //解锁麦位
                    if (roomQueueInfo.mRoomMicInfo.isMicLock() && !roomMicInfo.isMicLock()) {
                        LogUtils.d("roomQueueMicUpdate", "unLockMic");
                        micInListToUpMic(position + "");
                    }

                    roomQueueInfo.mRoomMicInfo = roomMicInfo;
                    //处理声网声音相关的
                    if (roomQueueInfo.mChatRoomMember != null) {
                        if (AvRoomDataManager.get().isOwner(roomQueueInfo.mChatRoomMember.getAccount())) {
                            RtcEngineManager.get().setRole(
                                    roomQueueInfo.mRoomMicInfo.isMicMute() ? Constants.CLIENT_ROLE_AUDIENCE : Constants.CLIENT_ROLE_BROADCASTER);
                        }
                    }
                    noticeMicPosStateChange(position + 1, roomQueueInfo);
                }
            }
        }
    }

    /**
     * 麦序状态，房间信息
     *
     * @param msg -
     */
    private void chatRoomInfoUpdate(ChatRoomMessage msg) {
        ChatRoomNotificationAttachment notificationAttachment = (ChatRoomNotificationAttachment) msg.getAttachment();
        Map<String, Object> extension = notificationAttachment.getExtension();
        if (extension != null) {
            if (extension.containsKey("type")) {
                // 1----房间信息更新   2-----麦序信息更新
                int type = (int) extension.get("type");
                if (type == 2) {
                    roomQueueMicUpdate(extension);
                } else if (type == 1) {
                    roomInfoUpdate(extension);
                }
            }
        }
    }

    /**
     * 房间信息更新
     *
     * @param extension
     */
    private void roomInfoUpdate(Map<String, Object> extension) {
        String roomInfoStr = (String) extension.get("roomInfo");
        if (!TextUtils.isEmpty(roomInfoStr)) {
            RoomInfo roomInfo = com.tongdaxing.erban.libcommon.utils.json.JsonParser.parseJsonToNormalObject(roomInfoStr, RoomInfo.class);
            if (roomInfo != null) {
                LogUtils.d("request_roomInfoUpdate1", roomInfo.toString());
                AvRoomDataManager.get().mCurrentRoomInfo = roomInfo;
                noticeRoomInfoUpdate(roomInfo);
            }
        }
    }

    private void chatRoomQueueChangeNotice(ChatRoomMessage msg) {
        ChatRoomQueueChangeAttachment roomQueueChangeAttachment = (ChatRoomQueueChangeAttachment) msg.getAttachment();
        //麦上成员信息（uid...）  key:坑位 ，content:{"nick":"行走的老者","uid":90972,"gender":1,"avatar":"https://img.erbanyy.com/Fmtbprx5cGc3KABKjDxs_udJZb3O?imageslim"}
        String content = roomQueueChangeAttachment.getContent();
        String key = roomQueueChangeAttachment.getKey();
        //排麦的key是用uid，所以length大于2
        boolean isMicInList = key != null && key.length() > 2;
        LogUtils.d(TAG, "chatRoomQueueChangeType:" + roomQueueChangeAttachment.getChatRoomQueueChangeType() + " fromAccount:" + msg.getFromAccount());
        LogUtils.d(TAG, "key:" + key + " content:" + content + " isMicInList:" + isMicInList);
        switch (roomQueueChangeAttachment.getChatRoomQueueChangeType()) {
            case DROP:
                break;
            case POLL:
                if (isMicInList) {
                    removeMicInList(key);
                } else {
                    downMicroQueue(key);
                }
                break;
            case OFFER:
                if (isMicInList) {
                    addMicInList(key, content);
                } else {
                    upMicroQueue(content, key);
                }
                break;
            case PARTCLEAR:
                break;
            case undefined:
                break;
            default:
        }
    }

    public void addMicInList(String key, String content) {
        if (TextUtils.isEmpty(key) || TextUtils.isEmpty(content)) {
            return;
        }
        int keyInt = Integer.parseInt(key);
        JsonParser jsonParser = new JsonParser();
        //获取云信下发的开发者扩展字段
        JsonObject valueJsonObj = jsonParser.parse(content).getAsJsonObject();
        if (valueJsonObj == null) {
            return;
        }

        //获取本地排麦序列中的开发者扩展字段
        SparseArray<Json> mMicInListMap = AvRoomDataManager.get().mMicInListMap;
        Json json = mMicInListMap.get(keyInt);
        if (json == null) {
            json = new Json();
        }

        //更新开发者扩展字段集
        Set<String> strings = valueJsonObj.keySet();
        for (String jsonKey : strings) {
            JsonElement value = valueJsonObj.get(jsonKey);
            //如果是数组，这里要用数据转换存放,兼容IOS传递的数据类型
            //测试场景:一个房间，8个麦位已满的情况下，一个ios用户排麦
            if (value instanceof JsonArray) {
                for (JsonElement arrayValue : (JsonArray) value) {
                    json.set(jsonKey, arrayValue.toString());
                }
            } else {
                json.set(jsonKey, value.toString());
            }
        }

        //添加进麦位列表
        AvRoomDataManager.get().addMicInListInfo(key, json);
        noticeMicInList();
    }

    private void removeMicInList(String key) {
        AvRoomDataManager.get().removeMicListInfo(key);
        noticeMicInList();

    }

    /**
     * 进入聊天室
     */
    public void joinAvRoom() {
        RoomInfo curRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        noticeEnterMessages();
        if (curRoomInfo != null) {
            long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();

            OkHttpManager.MyCallBack myCallBack = new OkHttpManager.MyCallBack<Json>() {
                @Override
                public void onError(Exception e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Json json) {
                }
            };

            //successListener设置为null 解决部分帐号解析数据异常，
            //com.google.gson.JsonSyntaxException: java.lang.IllegalStateException: Expected a string but was BEGI
            //导致退出自己的房间时，主界面没有右下角的操作面板
            model.userRoomIn(String.valueOf(uid), curRoomInfo.getUid(), myCallBack, curRoomInfo.getType());
        }
    }

    private ChatRoomMessage getFirstMessageContent() {
        String content = "官方:倡导绿色聊天，任何违法，低俗，暴力，敲诈等不良信息，官方将会采取封号处理。";
        ChatRoomMessage message = ChatRoomMessageBuilder.createTipMessage(content);
        message.setContent(content);
        return message;
    }

    public void addMessagesImmediately(ChatRoomMessage msg) {
        if (messages.size() == 0) {
            ChatRoomMessage firstMessageContent = getFirstMessageContent();
            messages.add(firstMessageContent);
            noticeReceiverMessageImmediately(firstMessageContent);
            sendRoomRulesMessage();
        }
        if (messages.size() > 1000) {
            messages.remove(0);
        }
        messages.add(msg);
        noticeReceiverMessageImmediately(msg);
    }

    /**
     * 插入房间消息到消息列表
     *
     * @param msg
     */
    private void addMessages(ChatRoomMessage msg) {
        if (messages.size() == 0) {
            ChatRoomMessage firstMessageContent = getFirstMessageContent();
            messages.add(firstMessageContent);
            noticeReceiverMessage(firstMessageContent);
            sendRoomRulesMessage();
        }
        if (messages.size() > 1000) {
            messages.remove(0);
        }
        messages.add(msg);
        noticeReceiverMessage(msg);
    }

    public Observable<List<ChatRoomMessage>> getChatRoomMsgFlowable() {
        return getChatRoomMsgPublisher().toFlowable(BackpressureStrategy.BUFFER)
                .toObservable().buffer(200, TimeUnit.MILLISECONDS, 20)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }


    private PublishSubject<ChatRoomMessage> getChatRoomMsgPublisher() {
        if (msgProcessor == null) {
            synchronized (IMNetEaseManager.class) {
                if (msgProcessor == null) {
                    msgProcessor = PublishSubject.create();
                }
            }
        }
        return msgProcessor;
    }

    public PublishProcessor<RoomEvent> getChatRoomEventObservable() {
        if (roomProcessor == null) {
            synchronized (IMNetEaseManager.class) {
                if (roomProcessor == null) {
                    roomProcessor = PublishProcessor.create();
                    roomProcessor.subscribeOn(AndroidSchedulers.mainThread())
                            .observeOn(AndroidSchedulers.mainThread());
                }
            }
        }
        return roomProcessor;
    }

    public void clear() {
        messages.clear();
        Log.e(IMNetEaseManager.class.getSimpleName(), "清除房间消息....");
    }

    /************************云信聊天室 普通操作(每个人都可以使用的) start******************************/

    /**
     * 发送公屏上的Tip信息
     * 子协议一: 关注房主提示- CustomAttachment.CUSTOM_MSG_SUB_TYPE_ROOM_TIP_SHARE_ROOM
     * 子协议二: 分享房间成功的提示- CustomAttachment.CUSTOM_MSG_SUB_TYPE_ROOM_TIP_ATTENTION_ROOM_OWNER
     *
     * @param targetUid -
     * @param subType   -发送公屏上Tip信息的子协议
     */
    public Single<ChatRoomMessage> sendTipMsg(long targetUid, int subType) {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(targetUid);
        long myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        UserInfo myUserInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(myUid);
        if (roomInfo != null && userInfo != null && myUserInfo != null) {
            RoomTipAttachment roomTipAttachment = new RoomTipAttachment(
                    CustomAttachment.CUSTOM_MSG_HEADER_TYPE_ROOM_TIP,
                    subType);
            roomTipAttachment.setUid(myUid);
            roomTipAttachment.setNick(myUserInfo.getNick());
            roomTipAttachment.setTargetUid(targetUid);
            roomTipAttachment.setTargetNick(userInfo.getNick());

            ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                    // 聊天室id
                    roomInfo.getRoomId() + "",
                    // 自定义消息
                    roomTipAttachment
            );

            return sendChatRoomMessage(message, false);
        }
        return Single.error(new Exception("roomInfo or userInfo or myUserInfo is null !"));
    }

    /**
     * 发送文本信息
     *
     * @param message -
     */
    public Single<ChatRoomMessage> sendTextMsg(long roomId, String message, String medalUrl) {
        if (TextUtils.isEmpty(message) || TextUtils.isEmpty(message.trim())) {
            return Single.error(new ErrorThrowable("message == null !!!"));
        }

        ChatRoomMessage chatRoomMessage = ChatRoomMessageBuilder.createChatRoomTextMessage(
                String.valueOf(roomId), message);
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        Map<String, Object> data = new HashMap<>();
        if (null != userInfo) {

            List<String> medalUrls = new ArrayList<>();
            if (userInfo.getWearList() != null) {
                for (MedalBean medalBean : userInfo.getWearList()) {
                    medalUrls.add(medalBean.getAlias());
                }
            }

            data.put(USER_NICK, NobleBusinessManager.getNobleRoomNick(userInfo.getVipId(),
                    userInfo.getIsInvisible(), userInfo.getVipDate(), userInfo.getNick()));
            data.put(com.tongdaxing.xchat_core.Constants.USER_EXPER_LEVEL, userInfo.getExperLevel());
            data.put(com.tongdaxing.xchat_core.Constants.ALIAS_ICON_URL, userInfo.getAlias());
            data.put(com.tongdaxing.xchat_core.Constants.MEDAL_URLS, medalUrls);
            data.put(com.tongdaxing.xchat_core.Constants.USER_NOBLE_MEDAL, userInfo.getVipMedal());
            data.put(USER_MEDAL_ID, userInfo.getVipId());
            data.put(com.tongdaxing.xchat_core.Constants.USER_NOBLE_BUBBLE, userInfo.getVipBubble());

            data.put(USER_MEDAL_DATE, userInfo.getVipDate());
            data.put(NOBLE_INVISIABLE_ENTER_ROOM, userInfo.getIsInvisible() ? 1 : 0);
        }
        chatRoomMessage.setRemoteExtension(data); // 设置服务器扩展字段
        chatRoomMessage.setLocalExtension(data); //设置本地扩展字段
        return sendChatRoomMessage(chatRoomMessage, false, false);
    }

    /**
     * 下麦
     *
     * @param key --
     */
    private void downMicroQueue(String key) {
        LogUtils.d("nim_sdk", "downMicroQueue_3");
        SparseArray<RoomQueueInfo> mMicQueueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;
        if (!TextUtils.isEmpty(key) && mMicQueueMemberMap != null) {
            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.get(Integer.parseInt(key));
            if (roomQueueInfo != null && roomQueueInfo.mChatRoomMember != null) {
                String account = roomQueueInfo.mChatRoomMember.getAccount();
                if (AvRoomDataManager.get().isOwner(account)) {
                    // 更新声网闭麦 ,发送状态信息
                    RtcEngineManager.get().setRole(Constants.CLIENT_ROLE_AUDIENCE);
                    AvRoomDataManager.get().mIsNeedOpenMic = true;
                }
                LogUtils.d("remove  mChatRoomMember", 2 + "");
                roomQueueInfo.mChatRoomMember = null;

                //增加防炸房校验逻辑
                try {
                    //同时停止接收此用户的推流
                    RtcEngineManager.get().muteRemoteAudioStream(Integer.valueOf(account), true);
                    //记录一下下麦的时间
                    AvRoomDataManager.get().setTimeOnMicDown(Integer.valueOf(account), System.currentTimeMillis());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // 通知界面更新麦序信息
                noticeDownMic(key, account);
                //排麦
                if (!roomQueueInfo.mRoomMicInfo.isMicLock()) {
                    micInListToUpMic(key);
                }
            }
        }
    }

    private void micInListToUpMic(final String key) {
        LogUtils.d("micInListToUpMic", "key:" + key);
        //房主不不能上||房间信息为空||之前更新过一次队列
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        final Json json = AvRoomDataManager.get().getMicInListTopInfo();
        if ("-1".equals(key) || roomInfo == null || json == null) {
            return;
        }

        final String micInListTopKey = json.str("uid");
        LogUtils.d("micInListToUpMic", micInListTopKey);
        checkMicInListUpMicSuccess(micInListTopKey, roomInfo.getRoomId(), key);
        if (!micInListTopKey.equals(CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getUid() + "")) {
            return;
        }
        remveMicInlistOrUpMic(key, roomInfo, micInListTopKey);
    }

    private void remveMicInlistOrUpMic(final String key, RoomInfo roomInfo, final String micInListTopKey) {
        removeMicInList(micInListTopKey, roomInfo.getRoomId() + "", new RequestCallback() {
            @Override
            public void onSuccess(Object param) {
                //移除成功报上麦,判断是否自己,如果是自动上麦
                LogUtils.d("micInListToUpMic", 1 + "");
                CoreManager.notifyClients(IAVRoomCoreClient.class, IAVRoomCoreClient.onMicInListToUpMic, Integer.parseInt(key), micInListTopKey);
                CoreManager.notifyClients(IAVRoomCoreClient.class, IAVRoomCoreClient.micInListDismiss, "");
            }

            @Override
            public void onFailed(int code) {
                LogUtils.d("micInListLogUpMic", 2 + "code:" + code);
            }

            @Override
            public void onException(Throwable exception) {
                LogUtils.d("micInListLogUpMic", 3 + "");
            }
        });
    }

    private void checkMicInListUpMicSuccess(final String micInListTopKey, final long roomId, final String key) {
        String account = getFirstMicUid();
        long currentUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        boolean isKicker = (currentUid + "").equals(account);
        //如果是首个在麦上的人
        if (isKicker) {
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    Json json = AvRoomDataManager.get().getMicInListTopInfo();
                    if (json == null) {
                        return;
                    }
                    String topKey = json.str("uid", "null");
                    //1.5秒后判断排麦，首位跟换没有
                    if (micInListTopKey.equals(topKey)) {
//                        remveMicInlistOrUpMic(key, roomInfo, micInListTopKey);
                        IMNetEaseManager.get().removeMicInList(micInListTopKey, roomId + "", new RequestCallback() {
                            @Override
                            public void onSuccess(Object param) {
                                //移除成功后通知别人上麦
                                sendMicInListNimMsg(key);
                                LogUtils.d("micInListToUpMicOnSuccess", key);
                            }

                            @Override
                            public void onFailed(int code) {
                                LogUtils.d("micInListToUpMiconFailed", key);
                            }

                            @Override
                            public void onException(Throwable exception) {
                                LogUtils.d("micInListToUpMiconException", key);
                            }
                        });
                        LogUtils.d("checkMicInListUpMicSuccess", "kick");
                    }
                }
            }, 1500);
        }
    }

    private String getFirstMicUid() {
        String account = "";
        SparseArray<RoomQueueInfo> mMicQueueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;

        for (int i = 0; i < mMicQueueMemberMap.size(); i++) {
            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(i);
            if (roomQueueInfo == null) {
                continue;
            }
            ChatRoomMember mChatRoomMember = roomQueueInfo.mChatRoomMember;
            if (mChatRoomMember == null) {
                continue;
            } else {
                account = mChatRoomMember.getAccount();
                break;
            }
        }
        return account;
    }

    private void sendMicInListNimMsg(final String key) {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        MicInListAttachment micInListAttachment = new MicInListAttachment(CUSTOM_MSG_MIC_IN_LIST, CUSTOM_MSG_MIC_IN_LIST);
        Json json = new Json();
        json.set("key", key);
        micInListAttachment.setParams(json + "");
        ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                // 聊天室id
                roomInfo.getRoomId() + "",
                micInListAttachment
        );
        IMNetEaseManager.get().sendChatRoomMessage(message, false)
                .subscribe(new BiConsumer<ChatRoomMessage, Throwable>() {
                    @Override
                    public void accept(ChatRoomMessage chatRoomMessage,
                                       Throwable throwable) throws Exception {
                        if (chatRoomMessage != null) {
                            new Timer().schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    micInListToUpMic(key);
                                }
                            }, 300);
                        }
                    }
                });
    }

    public void removeMicInList(String key, String roomId, RequestCallback requestCallback) {
        LogUtils.d("nim_sdk", "pollQueue_api_1");
        NIMClient.getService(ChatRoomService.class).pollQueue(roomId, key).setCallback(requestCallback);
    }

    /**
     * 上麦
     *
     * @param content --
     * @param key     --
     */
    private synchronized void upMicroQueue(String content, final String key) {
        final SparseArray<RoomQueueInfo> mMicQueueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;
        if (!TextUtils.isEmpty(content)) {
            JsonParser jsonParser = new JsonParser();
            JsonObject contentJsonObj = null;
            try {
                contentJsonObj = jsonParser.parse(content).getAsJsonObject();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (contentJsonObj != null) {
                int micPosition = Integer.parseInt(key);
                RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.get(micPosition);
                if (roomQueueInfo == null) {
                    return;
                }
                ChatRoomMember chatRoomMember = parseChatRoomMember(contentJsonObj, roomQueueInfo);
                int size = mMicQueueMemberMap.size();
                if (size > 0) {
                    for (int j = 0; j < size; j++) {
                        RoomQueueInfo temp = mMicQueueMemberMap.valueAt(j);
                        if (temp.mChatRoomMember != null && Objects.equals(temp.mChatRoomMember.getAccount(), chatRoomMember.getAccount())) {
                            //处理同一个人换坑问题
                            temp.mChatRoomMember = null;
                        }
                    }
                }
                RoomQueueInfo tempRoomQueueInfo = mMicQueueMemberMap.get(micPosition);
                if (tempRoomQueueInfo != null && tempRoomQueueInfo.mChatRoomMember != null
                        && !Objects.equals(tempRoomQueueInfo.mChatRoomMember.getAccount(), chatRoomMember.getAccount())) {
                    //被挤下麦的情况
                    noticeDownCrowdedMic(micPosition, tempRoomQueueInfo.mChatRoomMember.getAccount());
                }
                roomQueueInfo.mChatRoomMember = chatRoomMember;

                //增加防炸房校验逻辑--同时开始接收此用户的推流
                try {
                    RtcEngineManager.get().muteRemoteAudioStream(Integer.valueOf(chatRoomMember.getAccount()), false);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //重新更新队列，队列上是否还有自己
                if (!AvRoomDataManager.get().isOwnerOnMic()) {
                    //处理可能自己被挤下还能说话的情况
                    RtcEngineManager.get().setRole(Constants.CLIENT_ROLE_AUDIENCE);
                }

                if (AvRoomDataManager.get().isOwner(chatRoomMember.getAccount())) {
                    if (!roomQueueInfo.mRoomMicInfo.isMicMute()) {
                        //开麦
                        RtcEngineManager.get().setRole(Constants.CLIENT_ROLE_BROADCASTER);
                        //是否需要开麦
                        if (!AvRoomDataManager.get().mIsNeedOpenMic) {
                            //闭麦
                            RtcEngineManager.get().setMute(true);
                            AvRoomDataManager.get().mIsNeedOpenMic = true;
                        }
                    } else {
                        RtcEngineManager.get().setRole(Constants.CLIENT_ROLE_AUDIENCE);
                    }
                }
                micInListToDownMic(chatRoomMember.getAccount());
                noticeUpMic(Integer.parseInt(key), chatRoomMember.getAccount());
            }
        } else {
            RtcEngineManager.get().setRole(Constants.CLIENT_ROLE_AUDIENCE);
        }

    }

    private void micInListToDownMic(String key) {

        if (!AvRoomDataManager.get().checkInMicInlist()) {
            return;
        }
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        removeMicInList(key, roomInfo.getRoomId() + "", null);
    }

    @NonNull
    private ChatRoomMember parseChatRoomMember(JsonObject contentJsonObj, RoomQueueInfo roomQueueInfo) {
        ChatRoomMember chatRoomMember = new ChatRoomMember();
        if (contentJsonObj.has(USER_UID)) {
            int uid = contentJsonObj.get(USER_UID).getAsInt();
            chatRoomMember.setAccount(String.valueOf(uid));
        }
        if (contentJsonObj.has(USER_NICK)) {
            chatRoomMember.setNick(contentJsonObj.get(USER_NICK).getAsString());
        }
        if (contentJsonObj.has(USER_AVATAR)) {
            chatRoomMember.setAvatar(contentJsonObj.get(USER_AVATAR).getAsString());
        }
        if (contentJsonObj.has(USER_GENDER)) {
            roomQueueInfo.gender = contentJsonObj.get(USER_GENDER).getAsInt();
        }
        Map<String, Object> stringStringMap = new HashMap<>();
        if (contentJsonObj.has(headwearUrl)) {
            stringStringMap.put(headwearUrl, contentJsonObj.get(headwearUrl).getAsString());
        }
        if (contentJsonObj.has(hasVggPic)) {
            stringStringMap.put(hasVggPic, contentJsonObj.get(hasVggPic).getAsBoolean());
        }
        if (contentJsonObj.has(USER_MEDAL_DATE)) {
            stringStringMap.put(USER_MEDAL_DATE, contentJsonObj.get(USER_MEDAL_DATE).getAsInt());
        }
        if (contentJsonObj.has(USER_MEDAL_ID)) {
            stringStringMap.put(USER_MEDAL_ID, contentJsonObj.get(USER_MEDAL_ID).getAsInt());
        }
        if (contentJsonObj.has(NOBLE_INVISIABLE_ENTER_ROOM)) {
            boolean operaSuc = false;
            try {
                stringStringMap.put(NOBLE_INVISIABLE_ENTER_ROOM, contentJsonObj.get(NOBLE_INVISIABLE_ENTER_ROOM).getAsInt());
                operaSuc = true;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            if (!operaSuc) {
                try {
                    stringStringMap.put(NOBLE_INVISIABLE_ENTER_ROOM, contentJsonObj.get(NOBLE_INVISIABLE_ENTER_ROOM).getAsBoolean() ? 1 : 0);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        if (contentJsonObj.has(NOBLE_INVISIABLE_UID)) {
            stringStringMap.put(NOBLE_INVISIABLE_UID, contentJsonObj.get(NOBLE_INVISIABLE_UID).getAsLong());
        }
        chatRoomMember.setExtension(stringStringMap);
        return chatRoomMember;
    }


    //获取房间成员bug
    //海外出现房间成员超过200的情况，线上出现管理员失效的问题，需要移除后重新添加。
    //经过排查发现客户端获取的数据不准确
    //调用云信fetchRoomMembers这个api的时Limit这个值最大为200
    private int pageSize = 200;

    /**
     * 根据类型获取房间所有成员考虑外层改动小，进行如此封装
     *
     * @paramroomId
     * @parammemberQueryType
     * @paramcallback
     */
    public void getAllRoomMembers(final String roomId,
                                  final MemberQueryType memberQueryType,
                                  final RequestCallbackWrapper<List<ChatRoomMember>> callback) {
        //pageSize云信支持的最大值为200
        NIMClient.getService(ChatRoomService.class).fetchRoomMembers(roomId, memberQueryType, 0,
                pageSize).setCallback(new RequestCallbackWrapper<List<ChatRoomMember>>() {
            @Override
            public void onResult(int code, List<ChatRoomMember> result, Throwable
                    exception) {
                if (code == 200) {
                    //判断是否还有下一页数据
                    if (result.size() == pageSize) {
                        ChatRoomMember last = result.get(pageSize - 1);
                        long nextTime = MemberQueryType.NORMAL == memberQueryType ? last.getUpdateTime() : last.getEnterTime();
                        getRoomMembersNext(roomId, nextTime, memberQueryType, callback, result);
                    } else {
                        callback.onResult(code, result, exception);
                    }
                } else {
                    callback.onResult(code, result, exception);
                }
            }
        });
    }

    /**
     * 获取分页数据
     *
     * @paramroomId
     * @paramtime
     * @parammemberQueryType
     * @paramcallback
     * @paramdata
     */
    private void getRoomMembersNext(final String roomId, long time,
                                    final MemberQueryType memberQueryType,
                                    final RequestCallbackWrapper<List<ChatRoomMember>> callback,
                                    final List<ChatRoomMember> data) {
        NIMClient.getService(ChatRoomService.class)
                .fetchRoomMembers(roomId, memberQueryType, time,
                        pageSize).setCallback(new RequestCallbackWrapper<List<ChatRoomMember>>() {
            @Override
            public void onResult(int code, List<ChatRoomMember> result, Throwable
                    exception) {
                if (code == 200) {
                    data.addAll(result);
                    //判断是否还有下一页数据
                    if (result.size() == pageSize) {
                        ChatRoomMember last = result.get(pageSize - 1);
                        long nextTime =
                                MemberQueryType.NORMAL == memberQueryType ? last.getUpdateTime() : last.getEnterTime();
                        getRoomMembersNext(roomId, nextTime, memberQueryType, callback, data);
                    } else {
                        callback.onResult(code, data, exception);
                    }
                } else {
                    //因为之前数据获取正常，当前页数据异常也当正常返回
                    callback.onResult(200, data, exception);
                }
            }
        });
    }

    public Single<List<ChatRoomMember>> fetchRoomMembersByIds(final List<String> accounts) {
        return Single.create(new SingleOnSubscribe<List<ChatRoomMember>>() {
            @Override
            public void subscribe(SingleEmitter<List<ChatRoomMember>> e) throws Exception {

                final RoomInfo mCurrentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                if (mCurrentRoomInfo == null || ListUtils.isListEmpty(accounts)) {
                    e.onError(new IllegalArgumentException("RoomInfo is null or accounts is null"));
                    CoreManager.notifyClients(IMRoomCoreClient.class, IRoomCoreClient.enterError);
                    return;
                }
                RequestResult<List<ChatRoomMember>> result = NIMClient.syncRequest(NIMChatRoomSDK.getChatRoomService()
                        .fetchRoomMembersByIds(String.valueOf(mCurrentRoomInfo.getRoomId()), accounts));
                if (result.exception != null) {
                    e.onError(result.exception);
                } else if (result.code != BaseMvpModel.RESULT_OK) {
                    e.onError(new Exception("错误码: " + result.code));
                } else {
                    e.onSuccess(result.data);
                }
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /************************云信聊天室 普通操作 end******************************/

    /************************云信聊天室 房主/管理员操作 begin******************************/

    /**
     * 增加管理员
     *
     * @param chatRoomMember -
     */
    public void addManagerMember(final ChatRoomMember chatRoomMember) {
        AvRoomDataManager.get().addAdminMember(chatRoomMember);
    }

    /**
     * 增加管理员
     *
     * @param account -
     */
    private void addManagerMember(final String account) {
        List<String> accounts = new ArrayList<>(1);
        accounts.add(account);
        fetchRoomMembersByIds(accounts).subscribe(new BiConsumer<List<ChatRoomMember>, Throwable>() {
            @Override
            public void accept(List<ChatRoomMember> chatRoomMembers, Throwable throwable) throws Exception {
                if (!ListUtils.isListEmpty(chatRoomMembers)) {
                    ChatRoomMember chatRoomMember = chatRoomMembers.get(0);
                    addManagerMember(chatRoomMember);
                    if (AvRoomDataManager.get().isOwner(chatRoomMember.getAccount())) {
                        AvRoomDataManager.get().mOwnerMember = chatRoomMember;
                    }
                    // 放在这里的原因是,只有管理员身份改变了才能发送通知
                    noticeManagerChange(true, account);
                }
            }
        });
    }

    private void removeManagerMember(final String account) {
        if (TextUtils.isEmpty(account)) {
            return;
        }
        List<String> accounts = new ArrayList<>(1);
        accounts.add(account);
        Disposable disposable = fetchRoomMembersByIds(accounts).subscribe(new BiConsumer<List<ChatRoomMember>, Throwable>() {
            @Override
            public void accept(List<ChatRoomMember> chatRoomMemberList, Throwable throwable) throws Exception {
                if (!ListUtils.isListEmpty(chatRoomMemberList)) {
                    ChatRoomMember chatRoomMember = chatRoomMemberList.get(0);
                    if (AvRoomDataManager.get().isOwner(chatRoomMember.getAccount())) {
                        AvRoomDataManager.get().mOwnerMember = chatRoomMember;
                    }
                    removeManagerMember(chatRoomMember);
                    noticeManagerChange(false, account);
                }
            }
        });
    }

    public void removeManagerMember(ChatRoomMember chatRoomMember) {
        if (chatRoomMember == null) {
            return;
        }
        String account = chatRoomMember.getAccount();
        AvRoomDataManager.get().removeManagerMember(account);
    }

    /**
     * 加入黑名单
     *
     * @param roomId  -
     * @param account -
     * @param mark    true:设置，false：取消
     */
    public void markBlackListBySdk(String roomId, final String account, final boolean mark, final CallBack<ChatRoomMember> callBack) {
        NIMClient.getService(ChatRoomService.class)
                .markChatRoomBlackList(mark, new MemberOption(roomId, account))
                .setCallback(new RequestCallback<ChatRoomMember>() {
                    @Override
                    public void onSuccess(ChatRoomMember chatRoomMember) {
                        if (callBack != null) {
                            callBack.onSuccess(chatRoomMember);
                        }
                    }

                    @Override
                    public void onFailed(int i) {
                        SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), "操作失败，请重试");
                        if (callBack != null) {
                            callBack.onFail(i, "");
                        }
                    }

                    @Override
                    public void onException(Throwable throwable) {
                        SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), "操作失败，请重试");
                        if (callBack != null) {
                            callBack.onFail(-1, throwable.getMessage());
                        }
                    }
                });
    }

    /**
     * 设置管理员
     *
     * @param roomId  -
     * @param account 要设置的管理员id
     * @param mark    true：设置，false：取消
     */
    public void markManagerListBySdk(String roomId, final String account, final boolean mark, final CallBack<ChatRoomMember> callBack) {
        NIMClient.getService(ChatRoomService.class)
                .markChatRoomManager(mark, new MemberOption(roomId, account))
                .setCallback(new RequestCallback<ChatRoomMember>() {
                    @Override
                    public void onSuccess(ChatRoomMember chatRoomMember) {
                        if (callBack != null) {
                            callBack.onSuccess(chatRoomMember);
                        }
                        if (mark) {
                            addManagerMember(chatRoomMember);
                        } else {
                            removeManagerMember(chatRoomMember);
                        }
                    }

                    @Override
                    public void onFailed(int i) {
                        SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), "操作失败，请重试");
                        if (callBack != null) {
                            callBack.onFail(i, "");
                        }
                    }

                    @Override
                    public void onException(Throwable throwable) {
                        SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), "操作失败，请重试");
                        if (callBack != null) {
                            callBack.onFail(-1, throwable.getMessage());
                        }
                    }
                });
    }

    /**
     * 下麦
     *
     * @param micPosition -
     * @param callBack    -
     */
    public void downMicroPhoneBySdk(int micPosition, final CallBack<String> callBack) {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null) {
            if (null != callBack) {
                callBack.onSuccess("下麦成功");
            }
            return;
        }

        if (micPosition < -1) {
            if (null != callBack) {
                callBack.onSuccess("下麦成功");
            }
            return;
        }

        //防止房主掉麦
        if (micPosition == -1) {
            String currentUid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            if (!AvRoomDataManager.get().isRoomOwner(currentUid)) {
                if (null != callBack) {
                    callBack.onSuccess("下麦成功");
                }
                return;
            }
        }
        LogUtils.d("nim_sdk", "pollQueue_api_2");
        LogUtils.d("nim_sdk", "" + micPosition);
        NIMChatRoomSDK.getChatRoomService()
                .pollQueue(String.valueOf(roomInfo.getRoomId()), String.valueOf(micPosition))
                .setCallback(new RequestCallback<Entry<String, String>>() {

                    @Override
                    public void onSuccess(Entry<String, String> stringStringEntry) {
                        if (callBack != null) {
                            callBack.onSuccess("下麦成功");
                        }
                    }

                    @Override
                    public void onFailed(int i) {
                        if (callBack != null) {
                            callBack.onFail(-1, "下麦失败");
                        }
                    }

                    @Override
                    public void onException(Throwable throwable) {
                        if (callBack != null) {
                            callBack.onFail(-1, "下麦异常:" + throwable.getMessage());
                        }
                    }
                });
    }

    /**
     * <p>下麦</p>
     * 云信聊天室队列服务：取出队列头部或者指定元素
     * <p>roomId - 聊天室id</p>
     * <p>key - 需要取出的元素的唯一键。若为 null，则表示取出队头元素</p>
     *
     * @param micPosition -
     */
    public Single<String> downMicroPhoneBySdk(final int micPosition) {
        LogUtils.d("nim_sdk", "pollQueue_api_3");
        if (micPosition < -1) {
            return null;
        }

        //防止房主掉麦
        if (micPosition == -1) {
            String currentUid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            if (!AvRoomDataManager.get().isRoomOwner(currentUid)) {
                return null;
            }
        }
        final RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        LogUtil.d("incomingChatObserver", "downMicroPhoneBySdk" + "    do");

        if (roomInfo == null) {
            return null;
        }
        return Single.create(new SingleOnSubscribe<String>() {
            @Override
            public void subscribe(SingleEmitter<String> e) throws Exception {
                LogUtil.d("incomingChatObserver", "pollQueue" + "     midIndex" + micPosition);
                RequestResult<Entry<String, String>> result = NIMClient.syncRequest(NIMChatRoomSDK.getChatRoomService()
                        .pollQueue(String.valueOf(roomInfo.getRoomId()), String.valueOf(micPosition)));
                if (result.exception != null) {
                    e.onError(result.exception);
                } else if (result.code != BaseMvpModel.RESULT_OK) {
                    e.onError(new Exception("错误码: " + result.code));
                } else {
                    e.onSuccess("下麦回调成功");
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * 加入/移除黑名单
     * <p>添加/移出聊天室黑名单</p>
     * Parameters:
     * isAdd - true:添加, false:移出
     * memberOption - 请求参数，包含聊天室id，帐号id以及可选的扩展字段
     * Returns:
     * InvocationFuture 可以设置回调函数。回调中返回成员信息
     *
     * @param roomId  -
     * @param account -
     * @param mark    true:设置，false：取消
     */
    public Single<String> markBlackListBySdk(final String roomId, final String account, final boolean mark) {
        return Single.create(new SingleOnSubscribe<String>() {
            @Override
            public void subscribe(SingleEmitter<String> e) throws Exception {
                RequestResult<ChatRoomMember> result = NIMClient.syncRequest(NIMClient.getService(ChatRoomService.class)
                        .markChatRoomBlackList(mark, new MemberOption(roomId, account)));
                if (result.exception != null) {
                    e.onError(result.exception);
                } else if (result.code != BaseMvpModel.RESULT_OK) {
                    e.onError(new Exception("错误码: " + result.code));
                } else {
                    e.onSuccess("黑名单处理回调成功");
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * 踢出房间
     *
     * @param roomId          聊天室 id
     * @param account         踢出成员帐号。仅管理员可以踢；如目标是管理员仅创建者可以踢
     * @param notifyExtension 被踢通知扩展字段，这个字段会放到被踢通知的扩展字段中
     */
    public Single<String> kickMemberFromRoomBySdk(final long roomId, final long account,
                                                  final Map<String, Object> notifyExtension, int targetVipId,
                                                  int targetVipDate, boolean targetIsInvisiable, String targetNick) {
        statisticsReason(roomId, notifyExtension);
        return Single.create(new SingleOnSubscribe<String>() {
            @Override
            public void subscribe(SingleEmitter<String> e) throws Exception {
                RequestResult<Void> result = NIMClient.syncRequest(NIMChatRoomSDK.getChatRoomService()
                        .kickMember(String.valueOf(roomId), String.valueOf(account), notifyExtension));
                if (result.exception != null) {
                    e.onError(result.exception);
                } else if (result.code != BaseMvpModel.RESULT_OK) {
                    if (result.code == 404) {
                        e.onError(new Exception("找不到该用户"));
                    } else {
                        e.onError(new Exception("错误码: " + result.code));
                    }
                } else {
                    IMNetEaseManager.get().systemNotificationBySdk(account, CUSTOM_MSG_HEADER_TYPE_KICK_ROOM,
                            -1, targetVipId, targetVipDate, targetIsInvisiable, targetNick);
                    e.onSuccess("踢人出房间回调成功");
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    private void statisticsReason(long lroomId, Map<String, Object> extension) {
        if (extension == null) {
            return;
        }
        String roomId = lroomId + "";
        StringBuffer stringBuffer = new StringBuffer();
        Map<String, String> hashMap = new HashMap<>();
        System.currentTimeMillis();
        hashMap.put("uid", "b" + CoreManager.getCore(IAuthCore.class).getCurrentUid());
        hashMap.put("time", TimeUtils.stampToDate(System.currentTimeMillis()) + "  ");
        hashMap.put("roomId", roomId + "  ");
        for (Map.Entry<String, Object> entry : extension.entrySet()) {
            hashMap.put(entry.getKey() + "", entry.getValue() + "  ");
        }
        for (Map.Entry<String, String> entry : hashMap.entrySet()) {
            stringBuffer.append(entry.getValue() + ",");
        }
        LogUtils.d("statisticsReason", stringBuffer + "");
    }

    /**
     * <p>下麦</p>
     * 云信聊天室队列服务：加入或者更新队列元素，支持当用户掉线或退出聊天室后，是否删除这个元素
     * <p>roomId - 聊天室id</p>
     * <p>key -  新元素（或待更新元素）的唯一键</p>
     * <p>value -  新元素（待待更新元素）的内容</p>
     * <p>isTransient - (可选参数，不传默认false)。true表示当提交这个新元素的用户从聊天室掉线或退出的时候，需要删除这个元素；默认false表示不删除</p>
     *
     * @param micPosition -
     */
    public Single<Boolean> updateQueueEx(final long roomId, final int micPosition, final String value) {
        return Single.create(new SingleOnSubscribe<Boolean>() {
            @Override
            public void subscribe(SingleEmitter<Boolean> e) throws Exception {
                RequestResult<Void> result = NIMClient.syncRequest(NIMChatRoomSDK.getChatRoomService()
                        .updateQueueEx(String.valueOf(roomId), String.valueOf(micPosition), value, true));
                if (result.exception != null) {
                    e.onError(result.exception);
                } else if (result.code != BaseMvpModel.RESULT_OK) {
                    e.onError(new Exception("错误码: " + result.code));
                } else {
                    e.onSuccess(true);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * 邀请上麦的自定义消息
     *
     * @param micUid   上麦用户uid
     * @param position 要上麦的位置
     */
    public Single<ChatRoomMessage> inviteMicroPhoneBySdk(final long micUid, final int position) {
        final RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null) {
            return Single.error(new IllegalArgumentException("inviteMicroPhoneBySdk roomInfo 不能为null"));
        }
        return Single.create(
                new SingleOnSubscribe<ChatRoomMessage>() {
                    @Override
                    public void subscribe(SingleEmitter<ChatRoomMessage> e) throws Exception {
                        String uid = String.valueOf(micUid);
                        RoomQueueMsgAttachment queueMsgAttachment = new RoomQueueMsgAttachment(CUSTOM_MSG_HEADER_TYPE_QUEUE,
                                CUSTOM_MSG_HEADER_TYPE_QUEUE_INVITE);
                        setNickTOAction(uid, queueMsgAttachment);

                        queueMsgAttachment.uid = uid;
                        queueMsgAttachment.micPosition = position;

                        LogUtils.d("inviteMicroPhoneBySdk", micUid + "");
                        ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                                String.valueOf(roomInfo.getRoomId()), queueMsgAttachment);
                        e.onSuccess(message);
                    }
                }).subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .filter(new Predicate<ChatRoomMessage>() {
                    @Override
                    public boolean test(ChatRoomMessage chatRoomMessage) throws Exception {
                        //上麦条件:游客不在麦上 或者是管理员，房主
                        return (!AvRoomDataManager.get().isOnMic(micUid)
                                || AvRoomDataManager.get().isRoomOwner()
                                || AvRoomDataManager.get().isRoomAdmin())
                                && position != Integer.MIN_VALUE;
                    }
                })
                .toSingle()
                .flatMap(new Function<ChatRoomMessage, SingleSource<? extends ChatRoomMessage>>() {
                    @Override
                    public SingleSource<? extends ChatRoomMessage> apply(ChatRoomMessage chatRoomMessage) throws Exception {
                        return sendChatRoomMessage(chatRoomMessage, false);
                    }
                });
    }

    /**
     * @param uid        -1的时候不操作
     * @param attachment
     */
    private void setNickTOAction(String uid, RoomQueueMsgAttachment attachment) {
        if (!"-1".equals(uid)) {
            ChatRoomMember chatRoomMember = AvRoomDataManager.get().getChatRoomMember(uid);
            if (chatRoomMember != null) {
                attachment.targetNick = chatRoomMember.getNick();
            }
        }

        UserInfo cacheLoginUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (cacheLoginUserInfo != null) {
            attachment.handleNick = cacheLoginUserInfo.getNick();
        }
    }

    /**
     * 踢人下麦
     *
     * @param micUid 被踢用户uid
     * @param roomId 房间ID
     */
    public Single<ChatRoomMessage> kickMicroPhoneBySdk(long micUid, int position, long roomId,
                                                       int targetVipId, int targetVipDate,
                                                       boolean targetIsInvisiable, String targetNick) {

        RoomQueueMsgAttachment queueMsgAttachment = new RoomQueueMsgAttachment(CUSTOM_MSG_HEADER_TYPE_QUEUE,
                CUSTOM_MSG_HEADER_TYPE_QUEUE_KICK);
        setNickTOAction(micUid + "", queueMsgAttachment);
        queueMsgAttachment.uid = String.valueOf(micUid);
        queueMsgAttachment.micPosition = position;

        queueMsgAttachment.targetIsInvisiable = targetIsInvisiable;
        queueMsgAttachment.targetNick = NobleBusinessManager.getNobleRoomNick(targetVipId,
                targetIsInvisiable, targetVipDate, targetNick);
        queueMsgAttachment.targetVipDate = targetVipDate;
        queueMsgAttachment.targetVipId = targetVipId;

        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (null != userInfo) {
            queueMsgAttachment.handleVipId = userInfo.getVipId();
            queueMsgAttachment.handleVipDate = userInfo.getVipDate();
            queueMsgAttachment.handleIsInvisible = userInfo.getIsInvisible();
            if (null == AvRoomDataManager.get().mCurrentRoomInfo ||
                    AvRoomDataManager.get().mCurrentRoomInfo.getUid() == userInfo.getUid()) {
                queueMsgAttachment.handleNick = userInfo.getNick();
            } else {
                queueMsgAttachment.handleNick = NobleBusinessManager.getNobleRoomNick(userInfo.getVipId(),
                        userInfo.getIsInvisible(), userInfo.getVipDate(), userInfo.getNick());
            }

        }
        ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                String.valueOf(roomId), queueMsgAttachment);
        return sendChatRoomMessage(message, false);
    }


    /************************云信聊天室 房主/管理员操作 end******************************/

    /**
     * <p>发送 云信聊天室信息</p>
     * <p>返回的是一个观察者，需要subscribe才会开始发送信息</p>
     * <p>不关心信息发送是否成功的，可以直接调用无参subscribe()方法，
     * 相反关心的则需要在subscribe(new Consumer(..){...})方法中通过Consumer来消耗事件</p>
     * <p>ps: 无参subscribe()方法调用如果发生错误，会在rxjava的error handler中得到回调,默认的实现在application的onCreate中</p>
     *
     * @param chatRoomMessage 自定义的聊天室信息
     * @param resend          是否自动重发
     * @param enable          是否反垃圾
     * @return 返回一个可被observer观察订阅的observable
     */
    public Single<ChatRoomMessage> sendChatRoomMessage(final ChatRoomMessage chatRoomMessage, final boolean resend, boolean enable) {
        if (chatRoomMessage == null) {
            throw new IllegalArgumentException("ChatRoomMessage can't be null!");
        }

        // 构造反垃圾对象
        NIMAntiSpamOption antiSpamOption = chatRoomMessage.getNIMAntiSpamOption();
        if (antiSpamOption == null) {
            antiSpamOption = new NIMAntiSpamOption();
        }
        antiSpamOption.enable = enable;
        chatRoomMessage.setNIMAntiSpamOption(antiSpamOption);

        return Single.create(new SingleOnSubscribe<ChatRoomMessage>() {
            @Override
            public void subscribe(SingleEmitter<ChatRoomMessage> e) throws Exception {
                RequestResult<Void> result = NIMClient.syncRequest(NIMChatRoomSDK.getChatRoomService()
                        .sendMessage(chatRoomMessage, resend));
                if (result.exception != null) {
                    e.onError(result.exception);
                } else if (result.code != BaseMvpModel.RESULT_OK) {
                    e.onError(new Exception("错误码: " + result.code));

                } else {
                    e.onSuccess(chatRoomMessage);

                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * <p>发送 云信聊天室信息</p>
     * <p>返回的是一个观察者，需要subscribe才会开始发送信息</p>
     * <p>不关心信息发送是否成功的，可以直接调用无参subscribe()方法，
     * 相反关心的则需要在subscribe(new Consumer(..){...})方法中通过Consumer来消耗事件</p>
     * <p>ps: 无参subscribe()方法调用如果发生错误，会在rxjava的error handler中得到回调,默认的实现在application的onCreate中</p>
     *
     * @param chatRoomMessage 自定义的聊天室信息
     * @param resend          是否自动重发
     * @param enable          是否反垃圾
     * @param syncTimeOut     异步转同步最大等待时长
     * @return 返回一个可被observer观察订阅的observable
     */
    public Single<ChatRoomMessage> sendChatRoomMessage(final ChatRoomMessage chatRoomMessage,
                                                       final boolean resend, boolean enable, long syncTimeOut) {
        if (chatRoomMessage == null) {
            throw new IllegalArgumentException("ChatRoomMessage can't be null!");
        }

        // 构造反垃圾对象
        NIMAntiSpamOption antiSpamOption = chatRoomMessage.getNIMAntiSpamOption();
        if (antiSpamOption == null) {
            antiSpamOption = new NIMAntiSpamOption();
        }
        antiSpamOption.enable = enable;
        chatRoomMessage.setNIMAntiSpamOption(antiSpamOption);

        return Single.create(new SingleOnSubscribe<ChatRoomMessage>() {
            @Override
            public void subscribe(SingleEmitter<ChatRoomMessage> e) throws Exception {
                RequestResult<Void> result = NIMClient.syncRequest(NIMChatRoomSDK.getChatRoomService()
                        .sendMessage(chatRoomMessage, resend), syncTimeOut);
                if (result.exception != null) {
                    e.onError(result.exception);
                } else if (result.code != BaseMvpModel.RESULT_OK) {
                    e.onError(new Exception("错误码: " + result.code));
                } else {
                    e.onSuccess(chatRoomMessage);

                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Single<ChatRoomMessage> sendChatRoomMessage(final ChatRoomMessage chatRoomMessage, final boolean resend) {
        return sendChatRoomMessage(chatRoomMessage, resend, false);
    }


    public void systemNotificationBySdk(long uid, int second, int micIndex, int targetVipId, int targetVipDate,
                                        boolean targetIsInvisiable, String targetNick) {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null) {
            return;
        }
        long roomId = roomInfo.getRoomId();
        RoomQueueMsgAttachment queueMsgAttachment = new RoomQueueMsgAttachment(
                CUSTOM_MSG_HEADER_TYPE_QUEUE, second);
        setNickTOAction(uid + "", queueMsgAttachment);
        queueMsgAttachment.uid = String.valueOf(uid);
        if (micIndex != -1) {
            queueMsgAttachment.micPosition = micIndex;
        }

        queueMsgAttachment.targetIsInvisiable = targetIsInvisiable;
        if (!TextUtils.isEmpty(targetNick)) {
            queueMsgAttachment.targetNick = NobleBusinessManager.getNobleRoomNick(targetVipId,
                    targetIsInvisiable, targetVipDate, targetNick);
        }
        queueMsgAttachment.targetVipDate = targetVipDate;
        queueMsgAttachment.targetVipId = targetVipId;

        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (null != userInfo) {
            queueMsgAttachment.handleVipId = userInfo.getVipId();
            queueMsgAttachment.handleVipDate = userInfo.getVipDate();
            queueMsgAttachment.handleIsInvisible = userInfo.getIsInvisible();
            if (null == AvRoomDataManager.get().mCurrentRoomInfo ||
                    AvRoomDataManager.get().mCurrentRoomInfo.getUid() == userInfo.getUid()) {
                queueMsgAttachment.handleNick = userInfo.getNick();
            } else {
                queueMsgAttachment.handleNick = NobleBusinessManager.getNobleRoomNick(userInfo.getVipId(),
                        userInfo.getIsInvisible(), userInfo.getVipDate(), userInfo.getNick());
            }
        }

        final ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                String.valueOf(roomId), queueMsgAttachment);
        Disposable disposable = sendChatRoomMessage(message, false).subscribe(new BiConsumer<ChatRoomMessage, Throwable>() {
            @Override
            public void accept(ChatRoomMessage chatRoomMessage, Throwable throwable) throws Exception {
                if (throwable == null) {
                    addMessagesImmediately(message);
                }
            }
        });
    }

    /**
     * 发送Pk消息
     *
     * @param second
     * @param pkVoteInfo
     */
    public void sendPkNotificationBySdk(int first, int second, PkVoteInfo pkVoteInfo) {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null) {
            return;
        }
        long roomId = roomInfo.getRoomId();
        PkCustomAttachment pkCustomAttachment = new PkCustomAttachment(first, second);
        pkCustomAttachment.setPkVoteInfo(pkVoteInfo);
        ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                String.valueOf(roomId), pkCustomAttachment);
        sendMessage(message, false);
    }

    private void sendMessage(final ChatRoomMessage msg, boolean resend) {
        Disposable disposable = sendChatRoomMessage(msg, resend).subscribe(new BiConsumer<ChatRoomMessage, Throwable>() {
            @Override
            public void accept(ChatRoomMessage chatRoomMessage, Throwable throwable) throws Exception {
                if (throwable == null) {
                    addMessagesImmediately(msg);
                }
            }
        });
    }


    /**
     * -------------------------通知begin-------------------------------
     */
    private void noticeImNetReLogin(RoomQueueInfo roomQueueInfo) {
        LogUtil.d(TAG, "noticeImNetReLogin-ROOM_CHAT_RECONNECTION");
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.ROOM_CHAT_RECONNECTION)
                .setRoomQueueInfo(roomQueueInfo));
    }

    private void noticeRoomMemberChange(boolean isMemberIn, String account) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setAccount(account)
                .setEvent(isMemberIn ? RoomEvent.ROOM_MEMBER_IN : RoomEvent.ROOM_MEMBER_EXIT));
    }

    private void noticeManagerChange(boolean isAdd, String account) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(isAdd ? RoomEvent.ROOM_MANAGER_ADD : RoomEvent.ROOM_MANAGER_REMOVE)
                .setAccount(account));
    }


    private void noticeReceiverMessageImmediately(ChatRoomMessage chatRoomMessage) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.RECEIVE_MSG)
                .setChatRoomMessage(chatRoomMessage)
        );
    }

    private void noticeReceiverMessage(ChatRoomMessage chatRoomMessage) {
        getChatRoomMsgPublisher().onNext(chatRoomMessage);
    }

    private void noticeEnterMessages() {
        getChatRoomEventObservable().onNext(new RoomEvent().setEvent(RoomEvent.ENTER_ROOM));
    }

    public void noticeKickOutChatMember(ChatRoomKickOutEvent reason, String account) {
        LogUtils.d(TAG, account + ": noticeKickOutChatMember");
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.KICK_OUT_ROOM)
                .setReason(reason)
                .setAccount(account));
    }

    private void noticeKickDownMic(int position) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.KICK_DOWN_MIC)
                .setMicPosition(position));
    }

    private void noticeInviteUpMic(int position, String account) {
        getChatRoomEventObservable().onNext(new RoomEvent().setEvent(RoomEvent.INVITE_UP_MIC)
                .setAccount(account)
                .setMicPosition(position));
    }

    public void noticeDownMic(String position, String account) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.DOWN_MIC)
                .setAccount(account)
                .setMicPosition(Integer.valueOf(position)));
    }


    private void noticeMicPosStateChange(int position, RoomQueueInfo roomQueueInfo) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.MIC_QUEUE_STATE_CHANGE)
                .setMicPosition(position)
                .setRoomQueueInfo(roomQueueInfo));
    }


    private void noticeChatMemberBlackAdd(String account) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.ADD_BLACK_LIST)
                .setAccount(account));
    }


    private void noticeUpMic(int position, String account) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.UP_MIC)
                .setMicPosition(position)
                .setAccount(account)
        );
    }

    public void noticeRoomCharm(Json charmList) {
        LogUtils.d(TAG, "noticeRoomCharm MicroViewAdapter ROOM_CHARM");
        getChatRoomEventObservable().onNext(new RoomEvent().setRoomCharmList(charmList).setEvent(RoomEvent.ROOM_CHARM));
    }

    public void noticeAnnualRoomList(AnnualRoomRankAttachment annualRoomRankAttachment) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.ANNUAL_ROOM_RANK_NOTIFY)
                .setAnnualRoomRank(annualRoomRankAttachment)
        );
    }

    public void noticeRoomPair(String pairInfo) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.ROOM_PAIR)
                .setPairInfo(pairInfo)
        );
    }

    private void noticeMicInList() {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.MIC_IN_LIST_UPDATE)

        );
    }

    /**
     * 被挤下麦通知
     */
    private void noticeDownCrowdedMic(int position, String account) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.DOWN_CROWDED_MIC)
                .setMicPosition(position)
                .setAccount(account)
        );
    }

    private void noticeRoomInfoUpdate(RoomInfo roomInfo) {
        getChatRoomEventObservable().onNext(new RoomEvent().setEvent(RoomEvent.ROOM_INFO_UPDATE).setRoomInfo(roomInfo));
    }

    /**
     * 发送房间规则消息
     */
    public void sendRoomRulesMessage() {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        LogUtils.d(TAG, "sendRoomRulesMessage-roomInfo:" + roomInfo);
        if (null != roomInfo && !TextUtils.isEmpty(roomInfo.getPlayInfo())) {
            RoomRuleAttachment rules = new RoomRuleAttachment(CustomAttachment.CUSTOM_MSG_TYPE_RULE_FIRST,
                    CustomAttachment.CUSTOM_MSG_TYPE_RULE_FIRST);
            rules.setRule(roomInfo.getPlayInfo());
            ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                    String.valueOf(roomInfo.getRoomId()), rules);
            messages.add(message);
            noticeReceiverMessageImmediately(message);
        }
    }

    /**
     * 发送单个礼物的消息
     *
     * @param giftReceiveInfo
     */
    public void sendGiftMessage(GiftReceiveInfo giftReceiveInfo) {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null || giftReceiveInfo == null) {
            return;
        }
        long myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        GiftAttachment giftAttachment = new GiftAttachment(CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT, CustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_GIFT);
        giftAttachment.setUid(myUid + "");
        giftAttachment.setGiftRecieveInfo(giftReceiveInfo);
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        giftAttachment.setCharmLevel(userInfo.getCharmLevel());
        giftAttachment.setExperLevel(userInfo.getExperLevel());
        ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                // 聊天室id
                roomInfo.getRoomId() + "",
                giftAttachment
        );
        CoreManager.getCore(IRoomCore.class).sendMessage(message);
    }


    /**
     * 发送全麦礼物消息
     *
     * @param giftReceiveInfo
     */
    public void sendMultiGiftMessage(MultiGiftReceiveInfo giftReceiveInfo) {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo != null && giftReceiveInfo != null) {
            long myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
            MultiGiftAttachment giftAttachment = new MultiGiftAttachment(CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT, CustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_MULTI_GIFT);
            giftAttachment.setUid(myUid + "");
            giftAttachment.setMultiGiftAttachment(giftReceiveInfo);
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            giftAttachment.setCharmLevel(userInfo.getCharmLevel());
            giftAttachment.setExperLevel(userInfo.getExperLevel());
            ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                    // 聊天室id
                    roomInfo.getRoomId() + "",
                    giftAttachment
            );
            CoreManager.getCore(IRoomCore.class).sendMessage(message);
        }
    }

    //------------------声网云信麦序同步------------------------------
    public boolean isConnected() {
        return isConnected;
    }

    private void setConnected(boolean connected) {
        isConnected = connected;
//        isConnected = true;
    }

    /**
     * 消息是否被拦截，受roomId同sessionid是否相同的限制
     *
     * @param chatRoomMessage
     * @return
     */
    public boolean filterOtherRoomMsg(ChatRoomMessage chatRoomMessage) {
        return AvRoomDataManager.get().mCurrentRoomInfo == null ||
                !String.valueOf(AvRoomDataManager.get().mCurrentRoomInfo.getRoomId()).equals(chatRoomMessage.getSessionId());
    }

    /**
     * 消息是否被拦截，受roomId同targetRoomId是否相同的限制
     *
     * @param targetRoomId
     * @param incluedPublicChatRoomMsg
     * @return
     */
    public boolean filterOtherRoomMsg(String targetRoomId, boolean incluedPublicChatRoomMsg) {
        boolean isHomePartRoomMsg = BaseRoomServiceScheduler.getCurrentRoomInfo() == null ||
                !String.valueOf(BaseRoomServiceScheduler.getCurrentRoomInfo().getRoomId()).equals(targetRoomId);
        boolean isChatRoomMsg = TextUtils.isEmpty(targetRoomId) ||
                !targetRoomId.equals(String.valueOf(PublicChatRoomController.getPublicChatRoomId()));
        LogUtils.d(TAG, "filterOtherRoomMsg-isHomePartRoomMsg:" + isHomePartRoomMsg
                + " isChatRoomMsg:" + isChatRoomMsg
                + " incluedPublicChatRoomMsg:" + incluedPublicChatRoomMsg
                + " targetRoomId:" + targetRoomId);
        return incluedPublicChatRoomMsg ? isHomePartRoomMsg && isChatRoomMsg : isHomePartRoomMsg;
    }

    /**
     * 判断是否需要开启网易云盾反垃圾引擎
     *
     * @return
     */
    public boolean checkAntiOptionEnable() {
        //私聊消息|广播广场消息，默认开启网易云盾反垃圾引擎
        boolean enable = true;

        try {
            Json configData = CoreManager.getCore(VersionsCore.class).getConfigData();
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            int antiSpam = 0;
            if (null != configData && configData.has("antiSpam")) {
                antiSpam = configData.num("antiSpam");
            }

            LogUtils.d(TAG, "checkAntiOptionEnable-antiSpam:" + antiSpam);

            if (null != userInfo) {
                LogUtils.d(TAG, "checkAntiOptionEnable-experLevel:" + userInfo.getExperLevel());
                enable = userInfo.getExperLevel() <= antiSpam;
            }
            LogUtils.d(TAG, "checkAntiOptionEnable-enable:" + enable);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return enable;
    }
}
