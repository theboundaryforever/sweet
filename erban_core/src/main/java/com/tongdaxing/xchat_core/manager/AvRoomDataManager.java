package com.tongdaxing.xchat_core.manager;

import android.graphics.Point;
import android.text.TextUtils;
import android.util.SparseArray;

import com.netease.nimlib.sdk.chatroom.constant.MemberType;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomResultData;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCore;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCoreClient;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.TreeSet;

/**
 * <p> 全局变量，存储房间相关信息(周期与房间一样) </p>
 *
 * @author jiahui
 * @date 2017/12/11
 */
public final class AvRoomDataManager {
    public static final int ROOM_MAX_MIC_INDEX = 7;
    public static final int KTV_ROOM_MAX_MIC_INDEX = 3;
    public final static int MIC_FULL = -2;
    private static final Object SYNC_OBJECT = new Object();
    private static volatile AvRoomDataManager mInstance;
    public RoomInfo mServiceRoominfo;
    public RoomInfo mCurrentRoomInfo;
    public boolean isShowLikeTa = false;

    private boolean hasPkVoted = false;
    public EnterChatRoomResultData mEnterChatRoomResultData;
    /**
     * 房间创建者
     */
    public ChatRoomMember mRoomCreateMember;
    /**
     * 自己的实体
     */
    public ChatRoomMember mOwnerMember;
    /**
     * 房间管理员集合
     */
    public List<ChatRoomMember> mRoomManagerList;
    /**
     * 房间普通成员
     */
    public List<ChatRoomMember> mRoomNormalMemberList;
    /**
     * 房间受限成员：禁言用户和黑名单用户都属于受限用户
     */
    public List<ChatRoomMember> mRoomLimitMemberList;
    /**
     * 固定成员：包括创建者,管理员,普通用户,受限用户
     */
    public List<ChatRoomMember> mRoomFixedMemberList;
    /**
     * 房间所有人员
     */
    public List<ChatRoomMember> mRoomAllMemberList;
    /**
     * 麦序位置信息：对应的位置，坑位信息（用户成员，坑位状态）
     */
    public SparseArray<RoomQueueInfo>
            mMicQueueMemberMap;
    /**
     * 记录每一个麦位的中心点位置
     */
    public SparseArray<Point> mMicPointMap;
    public SparseArray<Json> mMicInListMap;
    public HashMap<String, Integer> pairSelectMap = new HashMap<>();
    /**
     * 是否需要打开麦克风,用户自己的行为，不受房主管理员的管理
     */
    public boolean mIsNeedOpenMic = true;
    TreeSet<Json> treeSet = new TreeSet<Json>(new Comparator<Json>() {
        @Override
        public int compare(Json o1, Json o2) {
            String time1 = o1.str("time");
            String time2 = o2.str("time");
            long l1 = Long.parseLong(time1);
            long l2 = Long.parseLong(time2);
            if (l1 > l2) {
                return 1;
            } else {
                return -1;
            }


        }
    });
    private SparseArray<Json> micInListTopInfo;
    private List<Integer> hideFaceList;
    /*魅力值*/
    private boolean showCharm = false;
    private int manMicTopCharmPosition = -1, ladyMicTopCharmPosition = -1;
    private String manCharmImgUrl = "", ladyCharmImgUrl = "";
    private Json roomCharmList = new Json();
    private Json lastRoomCharmList = new Json();
    private JSONArray hatList = new JSONArray();
    private JSONArray femaleHatList = new JSONArray();
    /*相亲*/
    private boolean showPairSelectStatus = false;
    /**
     * 每个人下麦的时间（记录，在做炸房检测判断的时候使用）
     */
    private SparseArray<Long> mTimeOnMicDown;

    //防止最小化时候这时候的进场消息是不能进入队列的
    private boolean isMinimize = false;//是否最小化 true 最小化

    public boolean isMinimize() {
        return isMinimize;
    }

    public void setMinimize(boolean minimize) {
        isMinimize = minimize;
    }

    private AvRoomDataManager() {
        mRoomManagerList = new ArrayList<>();
        mRoomFixedMemberList = new ArrayList<>();
        mRoomAllMemberList = new ArrayList<>();
        mMicQueueMemberMap = new SparseArray<>();
        mRoomNormalMemberList = new ArrayList<>();
        mRoomLimitMemberList = new ArrayList<>();
        mMicInListMap = new SparseArray<>();
        mTimeOnMicDown = new SparseArray<>();
    }

    public static AvRoomDataManager get() {
        if (mInstance == null) {
            synchronized (SYNC_OBJECT) {
                if (mInstance == null) {
                    mInstance = new AvRoomDataManager();
                }
            }
        }
        return mInstance;
    }

    /**
     * 设置当前投票状态
     *
     * @param hasPkVoted
     */
    public void setHasPkVoted(boolean hasPkVoted) {
        this.hasPkVoted = hasPkVoted;
    }


    public boolean hasPkVoted() {
        return hasPkVoted;
    }

    public void release() {
        LogUtils.d(RoomDataManager.TAG, "release");
        RtcEngineManager.get().leaveChannel();
        clear();
    }

    /**
     * 获取麦上队列信息
     *
     * @param micPosition 麦的位置
     * @return 对应队列信息
     */
    public RoomQueueInfo getRoomQueueMemberInfoByMicPosition(int micPosition) {
        if (micPosition >= mMicQueueMemberMap.size()) {
            return null;
        }
        return mMicQueueMemberMap.get(micPosition);
    }

    /**
     * 判断当前房间类型是否为相亲房
     *
     * @return
     */
    public boolean isPair() {
        return null != mCurrentRoomInfo && mCurrentRoomInfo.tagId == 6;
    }

    /**
     * 获取麦上队列信息
     *
     * @param account 用户的id
     * @return 对应队列信息
     */
    public RoomQueueInfo getRoomQueueMemberInfoByAccount(String account) {
        if (TextUtils.isEmpty(account)) {
            return null;
        }
        //同步解决随便传递进来一个account值，都有返回值的RoomQueueInfo的业务bug
        RoomQueueInfo roomQueueInfo = null;
        int size = mMicQueueMemberMap.size();
        for (int i = 0; i < size; i++) {
            RoomQueueInfo roomQueueInfo1 = mMicQueueMemberMap.valueAt(i);
            if (roomQueueInfo1.mChatRoomMember != null &&
                    Objects.equals(roomQueueInfo1.mChatRoomMember.getAccount(), account)) {
                roomQueueInfo = roomQueueInfo1;
                break;
            }
        }
        return roomQueueInfo;
    }

    /**
     * 清除房间成员信息
     */
    public void clearMembers() {
        if (mOwnerMember != null) {
            mOwnerMember = null;
        }
        if (mRoomCreateMember != null) {
            mRoomCreateMember = null;
        }
        mRoomAllMemberList.clear();
        mRoomFixedMemberList.clear();
        mRoomManagerList.clear();
        mRoomLimitMemberList.clear();
        mRoomNormalMemberList.clear();
        mTimeOnMicDown.clear();
    }

    public void clear() {
        isMinimize = false;
        clearMembers();
        if (mEnterChatRoomResultData != null) {
            mEnterChatRoomResultData = null;
        }
        if (mCurrentRoomInfo != null) {
            mCurrentRoomInfo = null;
        }
        if (mServiceRoominfo != null) {
            mServiceRoominfo = null;
        }

        /*魅力值*/
        showCharm = false;
        manMicTopCharmPosition = -1;
        ladyMicTopCharmPosition = -1;
        manCharmImgUrl = "";
        ladyCharmImgUrl = "";
        roomCharmList = new Json();
        lastRoomCharmList = new Json();
        hatList = new JSONArray();
        femaleHatList = new JSONArray();

        /*相亲*/
        showPairSelectStatus = false;

        isShowLikeTa = false;

        mMicInListMap.clear();
        mMicQueueMemberMap.clear();
        IMNetEaseManager.get().clear();

        hasPkVoted = false;
    }

    public ChatRoomMember getChatRoomMember(String uid) {
        if (!ListUtils.isListEmpty(mRoomAllMemberList)) {
            for (ChatRoomMember chatRoomMember : mRoomAllMemberList) {
                if (Objects.equals(chatRoomMember.getAccount(), uid)) {
                    return chatRoomMember;
                }
            }
        }
        return null;
    }

    public void addAdminMember(ChatRoomMember chatRoomMember) {
        if (chatRoomMember == null || containsAdminMember(chatRoomMember.getAccount())) {
            return;
        }
        mRoomManagerList.add(chatRoomMember);
    }

    public boolean containsAdminMember(String uid) {
        for (ChatRoomMember chatRoomMember : mRoomManagerList) {
            if (Objects.equals(chatRoomMember.getAccount(), String.valueOf(uid))) {
                return true;
            }
        }
        return false;
    }



    /***
     * 是否是房间创建者
     * @param currentUid
     * @return
     */
    public boolean isRoomOwner(String currentUid) {
        return mCurrentRoomInfo != null && Objects.equals(String.valueOf(mCurrentRoomInfo.getUid()), currentUid);
    }

    /***
     * 是否是房间创建者
     * @param currentUid
     * @return
     */
    public boolean isRoomOwner(long currentUid) {
        return mCurrentRoomInfo != null && mCurrentRoomInfo.getUid() == currentUid;
    }

    /**
     * 是否是自己
     */
    public boolean isOwner(String currentUid) {
        return Objects.equals(currentUid, String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
    }

    /**
     * 是否是自己
     */
    public boolean isOwner(long currentUid) {
        return currentUid == CoreManager.getCore(IAuthCore.class).getCurrentUid();
    }

    public boolean isRoomOwner() {
        return mCurrentRoomInfo != null && mCurrentRoomInfo.getUid() == CoreManager.getCore(IAuthCore.class).getCurrentUid();
    }

    void removeManagerMember(String account) {
        if (ListUtils.isListEmpty(mRoomManagerList) || TextUtils.isEmpty(account)) {
            return;
        }
        ListIterator<ChatRoomMember> iterator = mRoomManagerList.listIterator();
        for (; iterator.hasNext(); ) {
            ChatRoomMember chatRoomMember = iterator.next();
            if (Objects.equals(chatRoomMember.getAccount(), account)) {
                iterator.remove();
                break;
            }
        }
        if (AvRoomDataManager.get().isOwner(account) && mOwnerMember != null) {
            //自己是管理员被移除，恢复身份
            mOwnerMember.setMemberType(MemberType.NORMAL);
        }

    }

    public boolean isGuess() {
        return !isRoomAdmin() && !isRoomOwner();
    }

    public boolean isGuess(String account) {
        return !isRoomAdmin(account) && !isRoomOwner(account);
    }

    /**
     * 是否是房间管理员
     *
     * @return -
     */
    public boolean isRoomAdmin() {
        return isRoomAdmin(String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
    }

    public boolean isRoomAdmin(String account) {
        if (ListUtils.isListEmpty(mRoomManagerList)) {
            return false;
        }
        for (ChatRoomMember chatRoomMember : mRoomManagerList) {
            if (Objects.equals(chatRoomMember.getAccount(), account)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 房主是否在线
     */
    public boolean isRoomOwnnerOnline() {
        return mRoomCreateMember != null;
    }

    /**
     * 判断指定用户id是否在麦上
     *
     * @param myUid 用户id
     * @return -
     */
    public boolean isOnMic(long myUid) {
        return isOnMic(String.valueOf(myUid));
    }

    /**
     * 判断指定用户id是否在麦上
     *
     * @param myUid 用户id
     * @return -
     */
    public boolean isOnMic(String myUid) {
        int size = mMicQueueMemberMap.size();
        for (int i = 0; i < size; i++) {
            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(i);
            if (roomQueueInfo != null && roomQueueInfo.mChatRoomMember != null
                    && Objects.equals(roomQueueInfo.mChatRoomMember.getAccount(), myUid)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断自己是否在麦上
     *
     * @return -
     */
    public boolean isOwnerOnMic() {
        return isOnMic(CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }

    /**
     * 根据用户id去获取当前用户在麦上的位置
     *
     * @param currentUid -
     * @return -
     */
    public int getMicPosition(long currentUid) {
        int size = mMicQueueMemberMap.size();
        for (int i = 0; i < size; i++) {
            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(i);
            if (roomQueueInfo != null && roomQueueInfo.mChatRoomMember != null
                    && Objects.equals(roomQueueInfo.mChatRoomMember.getAccount(), String.valueOf(currentUid))) {
                return mMicQueueMemberMap.keyAt(i);
            }
        }
        //判断是否房主
        UserInfo roomOwner = CoreManager.getCore(IAVRoomCore.class).getRoomOwner();
        if (roomOwner != null) {
            if (roomOwner.getUid() == currentUid) {
                return -1;
            }
        }

        return Integer.MIN_VALUE;
    }

    /**
     * 根据麦位position获取microView的position
     */
    public static int getMicroViewPositionByMicPosition(int micPosition) {
        return micPosition + 1;//麦位从-1开始
    }


    /**
     * 根据用户id去获取当前用户在麦上的位置
     *
     * @param currentUid -
     * @return -
     */
    public int getMicPosition(String currentUid) {
        return getMicPosition(Long.valueOf(currentUid));
    }

    /**
     * 获取坑上没人的位置
     */
    public int findFreePosition() {
        int size;
        if (mMicQueueMemberMap != null && (size = mMicQueueMemberMap.size()) > 0) {
            for (int i = 0; i < size; i++) {
                int key = mMicQueueMemberMap.keyAt(i);
                RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(i);
                if (roomQueueInfo.mChatRoomMember == null) {
                    if (key == -1) {
                        continue;
                    }
                    return key;
                }
            }
        }
        return Integer.MIN_VALUE;
    }

    /**
     * 获取坑上没人的位置,排除房主的位置，也就是-1
     */
    public int findFreePositionNoOwner() {
        int size;
        if (mMicQueueMemberMap != null && (size = mMicQueueMemberMap.size()) > 0) {
            for (int i = 0; i < size; i++) {
                int key = mMicQueueMemberMap.keyAt(i);
                if (key == -1) {
                    continue;
                }
                RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(i);
                if (roomQueueInfo.mChatRoomMember == null && !roomQueueInfo.mRoomMicInfo.isMicLock()) {
                    return key;
                }
            }
        }
        return Integer.MIN_VALUE;
    }

    public void addRoomQueueInfo(String micPosition, RoomQueueInfo roomQueueInfo) {
        if (roomQueueInfo == null || roomQueueInfo.mChatRoomMember == null) {
            return;
        }
        Integer position = Integer.valueOf(micPosition);
        int size = AvRoomDataManager.get().mMicQueueMemberMap.size();
        //循环是移除重复的对象
        for (int i = 0; i < size; i++) {
            RoomQueueInfo temp = AvRoomDataManager.get().mMicQueueMemberMap.valueAt(i);
            if (temp.mChatRoomMember != null
                    && Objects.equals(temp.mChatRoomMember.getAccount(), roomQueueInfo.mChatRoomMember.getAccount())) {
                mMicQueueMemberMap.put(AvRoomDataManager.get().mMicQueueMemberMap.keyAt(i),
                        new RoomQueueInfo(temp.mRoomMicInfo, null));
            }
        }
        mMicQueueMemberMap.put(position, roomQueueInfo);
    }

    public void addMicInListInfo(String key, Json json) {
        if (json == null) {
            return;
        }
        Integer keyInt = Integer.valueOf(key);

        mMicInListMap.put(keyInt, json);
        CoreManager.notifyClients(IAVRoomCoreClient.class, IAVRoomCoreClient.onMicInListChange);
    }

    public void removeMicListInfo(String key) {
        if (TextUtils.isEmpty(key)) {
            return;
        }
        Integer keyInt = Integer.valueOf(key);

        removeMicListInfo(keyInt);
    }

    public void removeMicListInfo(int key) {

        mMicInListMap.remove(key);
        CoreManager.notifyClients(IAVRoomCoreClient.class, IAVRoomCoreClient.onMicInListChange);
    }

    public void resetMicMembers() {
        int size = mMicQueueMemberMap.size();
        for (int i = 0; i < size; i++) {
            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(i);
            if (roomQueueInfo.mChatRoomMember != null) {
                LogUtils.d("remove  mChatRoomMember", 4 + "");
                roomQueueInfo.mChatRoomMember = null;
            }
        }
    }

    public boolean checkInMicInlist() {
        UserInfo cacheLoginUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (cacheLoginUserInfo == null) {
            return false;
        }
        long uid = cacheLoginUserInfo.getUid();

        return mMicInListMap.get((int) uid) != null;

    }

    public Json getMicInListTopInfo() {
        if (mMicInListMap.size() < 1) {
            return null;
        }
        treeSet.clear();
        for (int i = 0; i < mMicInListMap.size(); i++) {
            treeSet.add(mMicInListMap.valueAt(i));
            LogUtils.d("micInListLog", "key:" + mMicInListMap.keyAt(i) + "   value:" + mMicInListMap.valueAt(i));
        }

        if (treeSet.size() > 0) {
            return treeSet.first();
        }
        return null;

    }

    /**
     * 返回-2的话代表满了
     *
     * @return
     */
    public int checkHasEmpteyMic() {
        if (mMicQueueMemberMap == null) {
            return MIC_FULL;
        }

        for (int i = 0; i < mMicQueueMemberMap.size(); i++) {
            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(i);
            int i1 = mMicQueueMemberMap.keyAt(i);
            if (roomQueueInfo.mChatRoomMember == null && i1 != -1 && !roomQueueInfo.mRoomMicInfo.isMicLock()) {
                return i1;
            }

        }
        return MIC_FULL;


    }

    public List<Integer> getHideFaceList() {
        return hideFaceList;
    }

    public void setHideFaceList(List<Integer> hideFaceList) {
        this.hideFaceList = hideFaceList;
    }

    public boolean isShowCharm() {
        return showCharm;
    }

    public void setShowCharm(boolean showCharm) {
        this.showCharm = showCharm;
    }

    public int getManMicTopCharmPosition() {
        return manMicTopCharmPosition;
    }

    public void setManMicTopCharmPosition(int manMicTopCharmPosition) {
        this.manMicTopCharmPosition = manMicTopCharmPosition;
    }

    public int getLadyMicTopCharmPosition() {
        return ladyMicTopCharmPosition;
    }

    public void setLadyMicTopCharmPosition(int ladyMicTopCharmPosition) {
        this.ladyMicTopCharmPosition = ladyMicTopCharmPosition;
    }

    public String getManCharmImgUrl() {
        return manCharmImgUrl;
    }

    public void setManCharmImgUrl(String manCharmImgUrl) {
        this.manCharmImgUrl = manCharmImgUrl;
    }

    public String getLadyCharmImgUrl() {
        return ladyCharmImgUrl;
    }

    public void setLadyCharmImgUrl(String ladyCharmImgUrl) {
        this.ladyCharmImgUrl = ladyCharmImgUrl;
    }

    public Json getRoomCharmList() {
        return roomCharmList;
    }

    public void setRoomCharmList(Json roomCharmList) {
        this.roomCharmList = roomCharmList;
    }

    public Json getLastRoomCharmList() {
        return lastRoomCharmList;
    }

    public void setLastRoomCharmList(Json lastRoomCharmList) {
        this.lastRoomCharmList = lastRoomCharmList;
    }

    public JSONArray getHatList() {
        return hatList;
    }

    public void setHatList(JSONArray hatList) {
        this.hatList = hatList;
    }

    public JSONArray getFemaleHatList() {
        return femaleHatList;
    }

    public void setFemaleHatList(JSONArray femaleHatList) {
        this.femaleHatList = femaleHatList;
    }

    public boolean isShowPairSelectStatus() {
        return showPairSelectStatus;
    }

//    /**
//     * 这个房间是否可以上报服务器检查声音异常（炸房嫌疑异常）
//     * (这里做频率限制，1个房间5秒只上报一次)
//     * return true 可以上报
//     */
//    public boolean isRoomCheckSpeakerExceptionEnable(long roomId) {
//        boolean enable = true;
//        if (lastCheckSpeakerExceptionTimeArray != null) {
//            long currTime = System.currentTimeMillis();
//            long lastTime = lastCheckSpeakerExceptionTimeArray.get(roomId) == null ? 0 : lastCheckSpeakerExceptionTimeArray.get(roomId);
//            enable = currTime - lastTime >= 5000;//1个房间5秒只上报一次
//        }
//        if (enable) {//刷新一下上次上报时间
//            if (lastCheckSpeakerExceptionTimeArray == null) {
//                lastCheckSpeakerExceptionTimeArray = new LongSparseArray<>();
//            }
//            lastCheckSpeakerExceptionTimeArray.put(roomId, System.currentTimeMillis());
//        }
//        return enable;
//    }

    public void setShowPairSelectStatus(boolean showPairSelectStatus) {
        this.showPairSelectStatus = showPairSelectStatus;
    }

    /**
     * 获取此人下麦的时间
     *
     * @param uid 此人的uid
     */
    public long getTimeOnMicDown(int uid) {
        return mTimeOnMicDown.get(uid, (long) 0);
    }

    /**
     * 设置此人下麦的时间
     *
     * @param uid  此人的uid
     * @param time 此人下麦的时间
     */
    public void setTimeOnMicDown(int uid, long time) {
        mTimeOnMicDown.put(uid, time);
    }


    //------------------对接IM后重新梳理的业务逻辑位置移动到下面，因为新旧房间--------------

    /**
     * 判断该用户是第一次进来还是切换房间
     *
     * @param roomUid  要进入的房间的房主UID
     * @param roomType 要进入房间的房间类型
     * @return
     */
    public boolean isFirstEnterRoomOrChangeOtherRoom(long roomUid, int roomType) {
        return mCurrentRoomInfo == null || mCurrentRoomInfo.getUid() != roomUid || mCurrentRoomInfo.getType() != roomType;
    }
}
