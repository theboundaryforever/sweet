package com.tongdaxing.xchat_core.manager.agora;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;

import com.netease.nim.uikit.common.util.log.LogUtil;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomEvent;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.OnLoginCompletionListener;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.manager.zego.BaseAudioEngine;
import com.tongdaxing.xchat_core.room.bean.RoomAdditional;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;

import java.io.File;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import io.agora.rtc.Constants;
import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.RtcEngine;
import io.agora.rtc.live.LiveTranscoding;

import static io.agora.rtc.Constants.AUDIO_PROFILE_MUSIC_HIGH_QUALITY;
import static io.agora.rtc.Constants.AUDIO_PROFILE_MUSIC_STANDARD;
import static io.agora.rtc.Constants.AUDIO_RECORDING_QUALITY_LOW;
import static io.agora.rtc.Constants.AUDIO_SCENARIO_GAME_STREAMING;

/**
 * <p> 声网管理类 </p>
 *
 * @author jiahui
 * @date 2017/12/12
 */
public class AgoraEngineManager extends BaseAudioEngine {

    public static final int audioSetting = 0;
    public static final int videoSetting = 1;
    public static final int videoAudienceSetting = 2;
    //    private static final String TAG1 = RtcEngineManager.class.getSimpleName();
    private static final String TAG = "room_log ---> Agora";
    private static final Object SYNC_OBJECT = new Object();
    private static volatile AgoraEngineManager sEngineManager;
    private static int lastRoomType = 0;

    public boolean isAudienceRole;

    /**
     * 是否保存用于定位声网杂音等问题的dump日志文件开关
     */
    public final static boolean isSaveDumpFile = false;

    @SuppressLint("HardwareIds,MissingPermission")
    public static void fixRtcSdkCompat(RtcEngine rtcEngine) {
        if (null != rtcEngine) {
            try {
                rtcEngine.setParameters(" {\"che.android_simulator\":false } ");
                String serial;
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
                    serial = android.os.Build.getSerial();
                } else {
                    serial = Build.SERIAL;
                }
                if ("unknown".equalsIgnoreCase(serial)) {
                    rtcEngine.setParameters(" {\"che.android_simulator\":true }");
                }
            } catch (Exception e) {
            }
        }
    }

    /**
     * 麦上是否闭麦，true：闭麦，false：开麦
     */
    public boolean isMute;

    private static void sendAppInnerRoomEvent(int roomEvent, int imRoomEvent) {
        if (lastRoomType == RoomInfo.ROOMTYPE_HOME_PARTY) {
            IMNetEaseManager.get().getChatRoomEventObservable().onNext(new RoomEvent().setEvent(roomEvent));
        }
        if (lastRoomType == RoomInfo.ROOMTYPE_SINGLE_AUDIO || lastRoomType == RoomInfo.ROOMTYPE_MULTI_AUDIO || lastRoomType == RoomInfo.ROOMTYPE_NEW_AUCTION) {
            IMRoomMessageManager.get().getIMRoomEventObservable().onNext(new IMRoomEvent().setEvent(imRoomEvent));
        }
    }

    public boolean isRemoteMute;
    private RtcEngine mRtcEngine;
    private Map<String, Integer> speakers;
    /**
     * 说话列表
     */
    private List<Integer> speakMembersPosition;
    /**
     * 队列说话列表
     */
    private List<Integer> speakQueueMembersPosition;
    private String uid;
    private boolean needRecord;
    private AgoraEngineManager.EngineEventHandler engineEventHandler = new AgoraEngineManager.EngineEventHandler(this);
    private Handler handler = new AgoraEngineManager.RtcEngineHandler(this);
    private ArrayList<LiveTranscoding.TranscodingUser> mUserList = new ArrayList<>(1);
    private LiveTranscoding mLiveTranscoding;
    private String pushUrl = null;
    //当前是否开麦
    private boolean isOpenMicro = true;

    private AgoraEngineManager() {
        speakMembersPosition = new ArrayList<>();
        speakQueueMembersPosition = new ArrayList<>();
    }

    public static AgoraEngineManager get() {
        if (sEngineManager == null) {
            synchronized (SYNC_OBJECT) {
                if (sEngineManager == null) {
                    sEngineManager = new AgoraEngineManager();
                }
            }
        }
        return sEngineManager;
    }

    public boolean isAudienceRole() {
        return isAudienceRole;
    }

    public boolean isMute() {
        return isMute;
    }

    @Override
    public void stopPlayingStream(String uid) {

    }

    /**
     * 设置是否能说话，静音,人自己的行为
     *
     * @param mute true：静音，false：不静音
     */
    public void setMute(boolean mute) {
        if (mRtcEngine != null) {
            int result;
            boolean isConnected = false;
            if (lastRoomType == RoomInfo.ROOMTYPE_HOME_PARTY) {
                isConnected = IMNetEaseManager.get().isConnected();
            } else if (lastRoomType == RoomInfo.ROOMTYPE_SINGLE_AUDIO || lastRoomType == RoomInfo.ROOMTYPE_MULTI_AUDIO || lastRoomType == RoomInfo.ROOMTYPE_NEW_AUCTION) {
                isConnected = IMRoomMessageManager.get().isImRoomConnection();
            }
            if (!mute && isConnected) {//已连接才能说话
                result = mRtcEngine.setClientRole(Constants.CLIENT_ROLE_BROADCASTER);
                if (result == 0) {
                    isMute = false;
                } else {
                    LogUtil.d(TAG, "setMute ----> mute = " + false + "  result = " + result);
                }
            } else {
                result = mRtcEngine.setClientRole(Constants.CLIENT_ROLE_AUDIENCE);
                if (result == 0) {
                    isMute = true;
                } else {
                    LogUtil.d(TAG, "setMute ----> mute = " + mute + "  result = " + result);
                }
            }

            if (result == 0) {
                boolean isMicOpen = !isAudienceRole && !isMute;
                LogUtils.d(TAG, "setRole-isMicOpen:" + isMicOpen);
//                if (isMicOpen) {
//                    cdnLayout(Integer.valueOf(uid));
//                } else {
//                    removeCdnLayout(Integer.valueOf(uid));
//                }
//                setLiveTranscoding();
//                addOrRemovePublishStreamUrl(isMicOpen);
            }
        }
    }

    public boolean isRemoteMute() {
        return isRemoteMute;
    }

    public void audioSetting(int quality, int audioShowRoom) {
        mRtcEngine.disableVideo();
        mRtcEngine.setAudioProfile(quality, audioShowRoom);
        mRtcEngine.enableAudioVolumeIndication(600, 3);
        mRtcEngine.setDefaultAudioRoutetoSpeakerphone(true);
    }

    public void videoAuStop() {
        if (mRtcEngine != null) {
            mRtcEngine.stopPreview();
        }
    }

    public void videoAudienceSetting() {
        mRtcEngine.enableAudio();
        mRtcEngine.enableVideo();
        mRtcEngine.setVideoProfile(Constants.VIDEO_PROFILE_360P, false);
    }

    public void videoSetting() {
        mRtcEngine.setExternalVideoSource(true, false, true);
        mRtcEngine.setRecordingAudioFrameParameters(16000, 2, 2, 640);
        mRtcEngine.enableVideo();
        mRtcEngine.enableAudio();
        mRtcEngine.setClientRole(Constants.CLIENT_ROLE_BROADCASTER);//CLIENT_ROLE_AUDIENCE
        mRtcEngine.setVideoProfile(Constants.VIDEO_PROFILE_360P, false);
    }

    public void stopAudioMixing() {
        if (mRtcEngine != null) {
            mRtcEngine.stopAudioMixing();
        }
    }

    public void setRemoteMute(boolean mute) {
        LogUtil.d(TAG, "setRemoteMute : mute = " + mute);
        if (mRtcEngine != null) {
            int result = mRtcEngine.muteAllRemoteAudioStreams(mute);
            if (result == 0) {
                isRemoteMute = mute;
            }
            LogUtil.d(TAG, "setRemoteMute : mute = " + mute + " ---> result = " + result);
        }
    }

    public boolean isSpeaking(int position) {
        if (speakMembersPosition != null) {
            return speakMembersPosition.contains(position);
        }
        return false;
    }

    public boolean isQueueSpeaking(int position) {
        if (speakQueueMembersPosition != null) {
            return speakQueueMembersPosition.contains(position);
        }
        return false;
    }

    /**
     * AUDIO_SCENARIO_CHATROOM_ENTERTAINMENT模式的特点：一进房间（initRtcEngine()），麦克风就会被全局占用，然后导致音量键调节不了媒体音量（切换到其他应用内也是这样），只能调节麦克风音量，除非退出房间或者彻底关闭应用；
     * AUDIO_SCENARIO_SHOWROOM模式的特点：可以在全局内调节媒体音量（麦克风依然是被应用占用的，上麦说话也不会有任何影响），调节不了麦克风音量。
     * AUDIO_SCENARIO_SHOWROOM模式频繁上下麦对app业务层没啥影响，对声网SDK内部，上下麦就需要对底层的音频进行一些操作和切换，外层不用关心
     *
     * @param uid
     * @param token
     */
    public boolean startRtcEngine(long uid, String token, int roomType) {
        RoomInfo curRoomInfo = null;
        LogUtil.d(TAG, "startRtcEngine uid:" + uid + " roomType:" + roomType + " token:" + token);
        if (roomType == RoomInfo.ROOMTYPE_HOME_PARTY) {
            curRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        } else if (roomType == RoomInfo.ROOMTYPE_SINGLE_AUDIO || roomType == RoomInfo.ROOMTYPE_MULTI_AUDIO || roomType == RoomInfo.ROOMTYPE_NEW_AUCTION) {
            curRoomInfo = RoomDataManager.get().getCurrentRoomInfo();
        } else {
            curRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        }

        if (null == curRoomInfo) {
            LogUtil.e(TAG, "startRtcEngine-房间信息为空，初始化声网失败!");
            return false;
        }
        lastRoomType = roomType;
        RoomAdditional roomAdditional = RoomDataManager.get().getAdditional();
        //ROOMTYPE_AUCTION、ROOMTYPE_LIGHT_CHAT、ROOMTYPE_KTV对应的声网初始化设置全部删除，后端已无对应的房间类型
        int result = initRtcEngine(curRoomInfo.getRoomId() + "", uid
                , roomAdditional == null ? AUDIO_PROFILE_MUSIC_STANDARD : roomAdditional.getAudioLevel()
                , roomAdditional == null ? AUDIO_SCENARIO_GAME_STREAMING : roomAdditional.getAudioScenario(), audioSetting, token);
        if (curRoomInfo.getUid() == uid
                && roomType != RoomInfo.ROOMTYPE_HOME_PARTY
                && roomType != RoomInfo.ROOMTYPE_SINGLE_AUDIO
                && roomType != RoomInfo.ROOMTYPE_MULTI_AUDIO
                && roomType != RoomInfo.ROOMTYPE_NEW_AUCTION) {
            //设置用户角色为主播,轰趴房房不能默认设置房主为主播
            setRole(Constants.CLIENT_ROLE_BROADCASTER);
        } else {
            setRole(Constants.CLIENT_ROLE_AUDIENCE);
        }
        return result == 0;
    }

    @Deprecated
    public void joinHighQualityChannel(long channelId, long uid, boolean record, String token) {
        this.needRecord = record;
        initRtcEngine(channelId + "", uid, AUDIO_PROFILE_MUSIC_HIGH_QUALITY, AUDIO_SCENARIO_GAME_STREAMING, audioSetting, token);
    }

    private int initRtcEngine(String channelId, long uid, int quality, int audioShowRoom, int type, String token) {

        this.uid = uid + "";
//        getTransCoding();
        this.isMute = false;
        this.isRemoteMute = false;
        if (mRtcEngine == null) {
            int joinResult = -1;
            try {
                if (engineEventHandler == null) {
                    engineEventHandler = new AgoraEngineManager.EngineEventHandler(this);
                }
                mRtcEngine = RtcEngine.create(BasicConfig.INSTANCE.getAppContext(), "314a1f874912455baeb1ad5ff838664a", engineEventHandler);
            } catch (Exception e) {
                e.printStackTrace();
                return joinResult;
            }
            //设置频道模式为直播
            mRtcEngine.setChannelProfile(Constants.CHANNEL_PROFILE_LIVE_BROADCASTING);
            mRtcEngine.setLogFile(Environment.getExternalStorageDirectory()
                    + File.separator
                    + BasicConfig.INSTANCE.getAppContext().getPackageName()
                    + "/log/agora-rtc.log");
            if (type == audioSetting) {
                audioSetting(quality, audioShowRoom);

            } else if (type == videoSetting) {
                videoSetting();
            } else if (type == videoAudienceSetting) {
                //主线程会造成卡顿
                videoAudienceSetting();
            }
            String channelStr = String.valueOf(channelId);
            //KTV房间
            if (type == videoSetting || type == videoAudienceSetting) {
                channelStr = "KTV" + channelStr;
            }
            LogUtil.d(TAG, "joinChannel: uid = " + uid + " ---> token = " + token + " ---> channel = " + channelStr);
            fixRtcSdkCompat(mRtcEngine);
            if (isSaveDumpFile) {
                mRtcEngine.setParameters("{\"che.audio.start_debug_recording\":\"NoName\"}");
            }
            joinResult = mRtcEngine.joinChannel(token, channelStr, null, (int) uid);
            return joinResult;
        } else {
            return -1;
        }
    }

    @Override
    public void setOnLoginCompletionListener(OnLoginCompletionListener listener) {

    }

    @Override
    public boolean startRtcEngine(long uid, String appId, RoomInfo curRoomInfo) {
        if (curRoomInfo == null) {
            return false;
        } else {
            return startRtcEngine(uid, appId, curRoomInfo.getType());
        }
    }

    public void leaveChannel() {
        LogUtil.d(TAG, "leaveChannel----->");
//        addOrRemovePublishStreamUrl(false);
        if (mRtcEngine != null) {
            stopAudioMixing();
            if (!isLinkMicroing) {
                mRtcEngine.leaveChannel();
            }
            mRtcEngine = null;
        }
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
        }
        isAudienceRole = true;
        isMute = false;
        isRemoteMute = false;
        needRecord = false;
    }

    /**
     * 设置是否能说话，静音,人自己的行为
     *
     * @param mute true：静音，false：不静音
     */
    public void setMuteAudio(boolean mute) {
        if (mRtcEngine != null) {
            int result = mRtcEngine.muteLocalAudioStream(mute);
            if (result == 0) {
                isMute = mute;
            }
        }
    }

    public RtcEngine getRtcEngine() {
        return mRtcEngine;
    }

    //音乐播放相关---------------begin--------------------------
    public void adjustAudioMixingVolume(int volume) {
        if (mRtcEngine != null) {
            mRtcEngine.adjustAudioMixingVolume(volume);
        }
    }

    public void adjustRecordingSignalVolume(int volume) {
        if (mRtcEngine != null) {
            mRtcEngine.adjustRecordingSignalVolume(volume);
        }
    }

    public void adjustPlaybackSignalVolume(int volume) {
        if (mRtcEngine != null) {
            mRtcEngine.adjustPlaybackSignalVolume(volume);
        }
    }

    public void resumeAudioMixing() {
        if (mRtcEngine != null) {
            mRtcEngine.resumeAudioMixing();
        }
    }

    public void pauseAudioMixing() {
        if (mRtcEngine != null) {
            mRtcEngine.pauseAudioMixing();
        }
    }

    public int startAudioMixing(String filePath, boolean loopback, int cycle) {
        if (mRtcEngine != null) {
            mRtcEngine.stopAudioMixing();
            int result = 0;
            try {
                result = mRtcEngine.startAudioMixing(filePath, loopback, false, cycle);
            } catch (Exception e) {
                return -1;
            }
            return result;
        }
        return -1;
    }

    /**
     * 获取当前房间伴奏播放位置
     *
     * @return
     */
    @Override
    public long getAudioMixingCurrentPosition() {
        long currPosition = -1;
        if (mRtcEngine != null) {
            currPosition = (int)mRtcEngine.getAudioMixingCurrentPosition();
        }
        return currPosition;
    }

    /**
     * 设置当前房间伴奏播放进度(百分比)
     *
     * @param position
     * @return
     */
    public void setAudioMixingPosition(int position) {
        if (mRtcEngine != null) {
            int totalDur = mRtcEngine.getAudioMixingDuration();
            if (totalDur > 0) {
                int currDur = position * totalDur / 100;
                mRtcEngine.setAudioMixingPosition(currDur);
            }
        }
    }

    public long getAudioMixingDuration() {
        long dur = -1;
        if (mRtcEngine != null) {
            dur = mRtcEngine.getAudioMixingDuration();
        }
        return dur;
    }

    //-----------------------声网推流到cdn---start-------------------------------------

    private LiveTranscoding getTransCoding() {
        if (mLiveTranscoding == null) {
            mLiveTranscoding = new LiveTranscoding();
            mLiveTranscoding.width = 16;
            mLiveTranscoding.height = 16;
            mLiveTranscoding.videoBitrate = 1;
            mLiveTranscoding.audioChannels = 1;
        }
        return mLiveTranscoding;
    }

    /**
     * 设置角色，上麦，下麦（调用）
     *
     * @param role CLIENT_ROLE_AUDIENCE: 听众 ，CLIENT_ROLE_BROADCASTER: 主播
     */
    public void setRole(int role) {
        if (mRtcEngine != null) {
            int result;
            boolean isConnected = false;
            if (lastRoomType == RoomInfo.ROOMTYPE_HOME_PARTY) {
                isConnected = IMNetEaseManager.get().isConnected();
            } else if (lastRoomType == RoomInfo.ROOMTYPE_SINGLE_AUDIO || lastRoomType == RoomInfo.ROOMTYPE_MULTI_AUDIO
                    || lastRoomType == RoomInfo.ROOMTYPE_NEW_AUCTION) {
                isConnected = IMRoomMessageManager.get().isImRoomConnection();
            }
            LogUtil.d(TAG, "setRole-isConnected:" + isConnected + " lastRoomType:" + lastRoomType);
            //已连接才能说话
            if (role == Constants.CLIENT_ROLE_BROADCASTER && isConnected) {
                result = mRtcEngine.setClientRole(isMute ? Constants.CLIENT_ROLE_AUDIENCE : Constants.CLIENT_ROLE_BROADCASTER);
                LogUtil.d(TAG, "setRole  ---> role = " + role + "  result = " + result);
                isAudienceRole = false;
            } else {
                result = mRtcEngine.setClientRole(Constants.CLIENT_ROLE_AUDIENCE);
                LogUtil.d(TAG, "setRole  ---> role = " + role + "  result = " + result);
                isAudienceRole = true;
            }

            //开麦add push url，闭麦remove
            if (result == 0) {
                boolean isMicOpen = role == Constants.CLIENT_ROLE_BROADCASTER && !isMute;
                LogUtil.d(TAG, "setRole-isMicOpen:" + isMicOpen);
//                if (isMicOpen) {
//                    cdnLayout(Integer.valueOf(uid));
//                } else {
//                    removeCdnLayout(Integer.valueOf(uid));
//                }
//                setLiveTranscoding();
//                addOrRemovePublishStreamUrl(isMicOpen);
            }
        }
    }

    /**
     * 设置直播转码
     * 该方法用于旁路推流的视图布局及音频设置等
     */
    private void setLiveTranscoding() {
        getTransCoding().setUsers(mUserList);
        mRtcEngine.setLiveTranscoding(getTransCoding());
    }

    public ArrayList<LiveTranscoding.TranscodingUser> cdnLayout(int uid) {
        LiveTranscoding.TranscodingUser user = new LiveTranscoding.TranscodingUser();
        user.uid = uid;
        user.alpha = 1;
        user.zOrder = 0;
        user.audioChannel = 0;

        user.x = 0;
        user.y = 0;
        user.width = 16;
        user.height = 16;
        mUserList.add(user);
        return mUserList;
    }

    @SuppressLint("SimpleDateFormat")
    private void addOrRemovePublishStreamUrl(boolean addOrRemovePublishUrl) {
        if (null != mRtcEngine && !TextUtils.isEmpty(uid)) {
            if (addOrRemovePublishUrl) {
                long roomid = 0L;
                if (lastRoomType == RoomInfo.ROOMTYPE_HOME_PARTY) {
                    if (null != AvRoomDataManager.get() && null != AvRoomDataManager.get().mCurrentRoomInfo) {
                        roomid = AvRoomDataManager.get().mCurrentRoomInfo.getRoomId();
                    }
                }
                if (lastRoomType == RoomInfo.ROOMTYPE_SINGLE_AUDIO || lastRoomType == RoomInfo.ROOMTYPE_MULTI_AUDIO || lastRoomType == RoomInfo.ROOMTYPE_NEW_AUCTION) {
                    if (null != RoomDataManager.get() && null != RoomDataManager.get().getCurrentRoomInfo()) {
                        roomid = RoomDataManager.get().getCurrentRoomInfo().getRoomId();
                    }
                }

                pushUrl = "rtmp://39.105.22.242:1936/tiantianlive/" + roomid + "_" + uid + "_" +
                        new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date());
                /**
                 * 增加旁路推流地址
                 * 该方法用于在旁路推流中，添加推流地址。会触发回调 onStreamPublished
                 * url 推流地址，格式为 RTMP
                 * transcodingEnabled 是否转码
                 */
                mRtcEngine.addPublishStreamUrl(pushUrl, true);
            } else {
                mRtcEngine.removePublishStreamUrl(pushUrl);
            }
        }
    }

    //-----------------------声网推流到cdn---end-------------------------------------

    /**
     * 接收/停止接收指定用户音频流
     *
     * @param uid   指定用户uid
     * @param muted true：停止接收指定用户的音频流 false：继续接收指定用户的音频流（默认）
     */
    public void muteRemoteAudioStream(int uid, boolean muted) {
        if (mRtcEngine != null) {
            if (uid != CoreManager.getCore(IAuthCore.class).getCurrentUid()) {
                mRtcEngine.muteRemoteAudioStream(uid, muted);
            }
        }
    }

    private void removeCdnLayout(int uid) {
        for (int i = 0; i < mUserList.size(); i++) {
            if (mUserList.get(i).uid == uid) {
                mUserList.remove(i);
            }
        }
    }

    //游戏连麦相关
    private RtcEngine linkMacroRtcEngine;
    private boolean isLinkMicroing = false;

    //加入连麦
    public void joinLinkMacro(String token, String channelId, long uid) {
        try {
            if (linkMacroRtcEngine == null) {
                linkMacroRtcEngine = RtcEngine.create(BasicConfig.INSTANCE.getAppContext(), "314a1f874912455baeb1ad5ff838664a", new IRtcEngineEventHandler() {
                    @Override
                    public void onJoinChannelSuccess(String channel, int uid, int elapsed) {
                        super.onJoinChannelSuccess(channel, uid, elapsed);
                    }
                });
            }
            linkMacroRtcEngine.leaveChannel();
            //设置为聊天模式
            linkMacroRtcEngine.setChannelProfile(Constants.CHANNEL_PROFILE_COMMUNICATION);
            //默认使用扬声器播放
            linkMacroRtcEngine.setDefaultAudioRoutetoSpeakerphone(true);
            //关闭耳返功能
            linkMacroRtcEngine.enableInEarMonitoring(false);
            //是否禁止本地音频发送
            linkMacroRtcEngine.muteLocalAudioStream(false);
            fixRtcSdkCompat(linkMacroRtcEngine);
            if (isSaveDumpFile) {
                mRtcEngine.setParameters("{\"che.audio.start_debug_recording\":\"NoName\"}");
            }
            int result = linkMacroRtcEngine.joinChannel(token, channelId, null, (int) uid);
            isLinkMicroing = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //切换扬声器或者听筒播放声音 true：切换到外放 false：切换到听筒
    public int changeSpeaker(boolean isLoudspeaker) {
        if (linkMacroRtcEngine != null) {
            int code = linkMacroRtcEngine.setEnableSpeakerphone(isLoudspeaker);
            return code;
        }
        return -1;
    }

    //静音或者开声音  true：静音 false：开启声音
    public int closeOrOpenSpeaker(boolean isClose) {
        if (linkMacroRtcEngine != null) {
            return linkMacroRtcEngine.muteAllRemoteAudioStreams(isClose);
        }
        return -1;
    }

    //退出连麦
    public void exitLinkMacro() {
        if (linkMacroRtcEngine != null) {
            int result = linkMacroRtcEngine.leaveChannel();
            isLinkMicroing = false;
        }
    }

    //闭麦或开麦
    public int closeOrOpenMacro(boolean isOpen) {
        int result = -1;
        if (linkMacroRtcEngine != null) {
            result = linkMacroRtcEngine.muteLocalAudioStream(!isOpen);//开关本地音频发送。
            if (result == 0) {
                isOpenMicro = isOpen;
            }
        }
        return result;
    }

    private static class EngineEventHandler extends IRtcEngineEventHandler {
        private WeakReference<AgoraEngineManager> mReference;

        EngineEventHandler(AgoraEngineManager manager) {
            mReference = new WeakReference<>(manager);
        }

        @Override
        public void onJoinChannelSuccess(String channel, int uid, int elapsed) {
            super.onJoinChannelSuccess(channel, uid, elapsed);
            LogUtil.d(TAG, "onJoinChannelSuccess: uid = " + uid + " ---> elapsed = " + elapsed + " ---> channel = " + channel);
            if (mReference.get() != null) {
                mReference.get().handler.sendEmptyMessage(0);
            }
        }

        @Override
        public void onUserJoined(int uid, int elapsed) {
            super.onUserJoined(uid, elapsed);
//            if (mReference.get() != null) {
//                mReference.get().cdnLayout(uid);
//                mReference.get().setLiveTranscoding();
//            }
        }

        @Override
        public void onUserOffline(int uid, int reason) {
            super.onUserOffline(uid, reason);
//            if (mReference.get() != null) {
//                mReference.get().removeCdnLayout(uid);
//                mReference.get().setLiveTranscoding();
//            }
        }

        @Override
        public void onLeaveChannel(RtcStats stats) {
            super.onLeaveChannel(stats);
            LogUtil.d(TAG, "onLeaveChannel: users = " + (stats != null ? stats.users : ""));
        }

        @Override
        public void onActiveSpeaker(int uid) {
            super.onActiveSpeaker(uid);
        }

        @Override
        public void onLastmileQuality(int quality) {
            super.onLastmileQuality(quality);
            if (quality >= 3) {
                sendAppInnerRoomEvent(RoomEvent.RTC_ENGINE_NETWORK_BAD, RoomEvent.RTC_ENGINE_NETWORK_BAD);
            }
        }

        @Override
        public void onConnectionInterrupted() {
            super.onConnectionInterrupted();
            sendAppInnerRoomEvent(RoomEvent.RTC_ENGINE_NETWORK_CLOSE, RoomEvent.RTC_ENGINE_NETWORK_CLOSE);
        }

        @Override
        public void onConnectionLost() {
            super.onConnectionLost();
            sendAppInnerRoomEvent(RoomEvent.RTC_ENGINE_NETWORK_CLOSE, RoomEvent.RTC_ENGINE_NETWORK_CLOSE);
        }

        @Override
        public void onAudioVolumeIndication(AudioVolumeInfo[] speakers, int totalVolume) {
            super.onAudioVolumeIndication(speakers, totalVolume);
            AgoraEngineManager manager = mReference.get();
            if (manager != null) {
                Message message = manager.handler.obtainMessage();
                message.what = 1;
                message.obj = speakers;
                manager.handler.sendMessage(message);
            }
        }

        @Override
        public void onUserMuteAudio(int uid, boolean muted) {
            super.onUserMuteAudio(uid, muted);
            AgoraEngineManager manager = mReference.get();
            if (manager != null) {
                if (muted) {
                    Message message = manager.handler.obtainMessage();
                    message.what = 2;
                    message.obj = uid;
                    manager.handler.sendMessage(message);
                }
            }
        }

        @Override
        public void onAudioMixingFinished() {
            super.onAudioMixingFinished();
            sendAppInnerRoomEvent(RoomEvent.METHOD_ON_AUDIO_MIXING_FINISHED, RoomEvent.METHOD_ON_AUDIO_MIXING_FINISHED);
        }
    }

    private static class RtcEngineHandler extends Handler {
        private WeakReference<AgoraEngineManager> mReference;

        RtcEngineHandler(AgoraEngineManager manager) {
            super(Looper.getMainLooper());
            mReference = new WeakReference<>(manager);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            AgoraEngineManager rtcEngineManager = mReference.get();
            if (rtcEngineManager == null) {
                return;
            }
            if (msg.what == 0) {
                if (lastRoomType == RoomInfo.ROOMTYPE_HOME_PARTY) {
                    IMNetEaseManager.get().joinAvRoom();
                } else if (lastRoomType == RoomInfo.ROOMTYPE_SINGLE_AUDIO || lastRoomType == RoomInfo.ROOMTYPE_MULTI_AUDIO || lastRoomType == RoomInfo.ROOMTYPE_NEW_AUCTION) {
                    IMRoomMessageManager.get().joinAvRoom();
                }

                if (rtcEngineManager.needRecord) {
                    rtcEngineManager.mRtcEngine.startAudioRecording(Environment.getExternalStorageDirectory()
                            + File.separator + BasicConfig.INSTANCE.getAppContext().getPackageName()
                            + "/audio/" + System.currentTimeMillis() + ".aac", AUDIO_RECORDING_QUALITY_LOW);
                }
            } else if (msg.what == 1) {
                IRtcEngineEventHandler.AudioVolumeInfo[] speakers = (IRtcEngineEventHandler.AudioVolumeInfo[]) msg.obj;
                RoomInfo roomInfo = null;
                if (lastRoomType == RoomInfo.ROOMTYPE_HOME_PARTY) {
                    roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                }
                if (lastRoomType == RoomInfo.ROOMTYPE_SINGLE_AUDIO || lastRoomType == RoomInfo.ROOMTYPE_MULTI_AUDIO || lastRoomType == RoomInfo.ROOMTYPE_NEW_AUCTION) {
                    roomInfo = RoomDataManager.get().getCurrentRoomInfo();
                }

                if (null == roomInfo) {
                    LogUtil.e(TAG, "startRtcEngine-房间信息为空，初始化声网失败!");
                    return;
                }

                if (speakers == null) {
                    return;
                }
                if (rtcEngineManager.speakQueueMembersPosition.size() > 0) {
                    rtcEngineManager.speakQueueMembersPosition.clear();
                }

//                String exceptionUserId = "";//多次异常的uid，多个逗号隔开（麦上并没有说话的这个人，疑似炸房）
                for (IRtcEngineEventHandler.AudioVolumeInfo speaker : speakers) {
                    // 0 代表的是房主,其他代表的是uid
                    int uid = speaker.uid == 0 ? Integer.valueOf(rtcEngineManager.uid) : speaker.uid;
                    int micPosition = -3;
                    if (lastRoomType == RoomInfo.ROOMTYPE_HOME_PARTY) {
                        micPosition = AvRoomDataManager.get().getMicPosition(uid);
                    }
                    if (lastRoomType == RoomInfo.ROOMTYPE_SINGLE_AUDIO || lastRoomType == RoomInfo.ROOMTYPE_MULTI_AUDIO || lastRoomType == RoomInfo.ROOMTYPE_NEW_AUCTION) {
                        micPosition = RoomDataManager.get().getMicPosition(uid);
                    }

                    if (micPosition == Integer.MIN_VALUE) {
                        LogUtil.e("speaker", "micPosition = " + micPosition);
                        if (roomInfo.isAlarmEnable() && speaker.volume > 0) {//开启了炸房检测 && 声音>0
                            long timeOnMicDown = 0L;
                            if (lastRoomType == RoomInfo.ROOMTYPE_HOME_PARTY) {
                                timeOnMicDown = AvRoomDataManager.get().getTimeOnMicDown(uid);
                            }
                            if (lastRoomType == RoomInfo.ROOMTYPE_SINGLE_AUDIO || lastRoomType == RoomInfo.ROOMTYPE_MULTI_AUDIO || lastRoomType == RoomInfo.ROOMTYPE_NEW_AUCTION) {
                                //单人音频房间暂时不考虑炸房问题 -- 新的多人房暂时不考虑
                                timeOnMicDown = 0;
                                return;
                            }
                            if (System.currentTimeMillis() - timeOnMicDown > roomInfo.getTimeInterval()) {
                                //这个人下麦时间已经超过设定的延时时间了（因为声音有延时，避免误杀）
                                //停止接收此用户的推流
                                AgoraEngineManager.get().muteRemoteAudioStream(uid, true);
//                                if (!TextUtils.isEmpty(exceptionUserId)) {
//                                    exceptionUserId = exceptionUserId.concat(",").concat(String.valueOf(uid));
//                                } else {//第1个前面不加逗号
//                                    exceptionUserId = exceptionUserId.concat(String.valueOf(uid));
//                                }
                            }
                        }
                    } else {
                        rtcEngineManager.speakQueueMembersPosition.add(micPosition);
                    }
                }

//                if (!TextUtils.isEmpty(exceptionUserId) && AvRoomDataManager.get().isOnMic(rtcEngineManager.uid)) {//自己在麦上才做异常上报
//                    LogUtil.e("speaker", "exceptionUserId = " + exceptionUserId);
//                    CoreManager.notifyClients(IAVRoomCoreClient.class, IAVRoomCoreClient.METHOD_ON_SPEAKER_EXCEPTION, exceptionUserId);
//                }

                if (lastRoomType == RoomInfo.ROOMTYPE_HOME_PARTY) {
                    IMNetEaseManager.get().getChatRoomEventObservable().onNext(
                            new RoomEvent().setEvent(RoomEvent.SPEAK_STATE_CHANGE)
                                    .setMicPositionList(rtcEngineManager.speakQueueMembersPosition));
                } else if (lastRoomType == RoomInfo.ROOMTYPE_SINGLE_AUDIO || lastRoomType == RoomInfo.ROOMTYPE_MULTI_AUDIO || lastRoomType == RoomInfo.ROOMTYPE_NEW_AUCTION) {
                    IMRoomMessageManager.get().getIMRoomEventObservable().onNext(
                            new IMRoomEvent().setEvent(RoomEvent.SPEAK_STATE_CHANGE)
                                    .setMicPositionList(rtcEngineManager.speakQueueMembersPosition));
                }
            } else if (msg.what == 2) {
                Integer uid = (Integer) msg.obj;
            }
        }
    }

    public boolean isOpenMicro() {
        return isOpenMicro;
    }
}
