package com.tongdaxing.xchat_core.manager;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomKickOutEvent;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.im.custom.bean.AnnualRoomRankAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.CallUpBean;
import com.tongdaxing.xchat_core.im.custom.bean.DetonateGiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.NoblePublicMsgAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.OpenNobleNotifyAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomRedPacketAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomRedPacketFinishAttachment;
import com.tongdaxing.xchat_core.room.auction.AuctionInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;

import java.util.List;

/**
 * 定义房间的操作
 *
 * @author xiaoyu
 * @date 2017/12/22
 */

public class RoomEvent {
    public static final int NONE = 0x00000000;
    public static final int ENTER_ROOM = 1;
    public static final int KICK_OUT_ROOM = 2;
    public static final int RECEIVE_MSG = 3;
    public static final int KICK_DOWN_MIC = 4;
    public static final int INVITE_UP_MIC = 5;
    public static final int DOWN_MIC = 6;
    /**
     * 坑的状态：1--锁，0--不锁
     */
    public static final int MIC_QUEUE_STATE_CHANGE = 7;
    public static final int ADD_BLACK_LIST = 8;
    public static final int UP_MIC = 9;
    public static final int MIC_IN_LIST_UPDATE = 32;
    public static final int ROOM_INFO_UPDATE = 10;
    public static final int ROOM_MANAGER_ADD = 11;
    public static final int ROOM_MANAGER_REMOVE = 12;
    public static final int SPEAK_STATE_CHANGE = 13;
    public static final int FOLLOW = 14;
    public static final int UNFOLLOW = 15;
    public static final int RECHARGE = 16;

    //竞拍房相关--产品已明确废弃
    public static final int AUCTION_START = 17;
    public static final int AUCTION_FINISH = 18;
    public static final int AUCTION_UPDATE = 19;
    public static final int AUCTION_UPDATE_FAIL = 21;

    public static final int ROOM_EXIT = 20;
    //房间成员进出
    public static final int ROOM_MEMBER_IN = 0x00000022;
    public static final int ROOM_MEMBER_EXIT = 0x00000023;
    /**
     * 房间断网重连
     */
    public static final int ROOM_CHAT_RECONNECTION = 0x00000024;
    /**
     * 用户被挤下麦
     */
    public static final int DOWN_CROWDED_MIC = 0x00000025;
    /**
     * 网络弱
     */
    public static final int RTC_ENGINE_NETWORK_BAD = 0x00000026;
    public static final int RTC_ENGINE_NETWORK_CLOSE = 0x00000027;
    public static final int METHOD_ON_AUDIO_MIXING_FINISHED = 0x00000028;
    public static final int ROOM_CHARM = 0x00000031;
    public static final int ROOM_PAIR = 0x00000032;
    public static final int ROOM_DETONATE_GIFT_PROGRESS = 0x00000033;
    public static final int ROOM_DETONATE_GIFT_NOTIFY = 0x00000034;
    public static final int ANNUAL_ROOM_RANK_NOTIFY = 0x00000035;

    //红包消息
    //收到自己房成员发出的红包
    public static final int ROOM_RECEIVE_RED_PACKET = 0x00000036;
    public static final int ROOM_RECEIVE_RED_PACKET_FINISH = 0x00000037;
    //贵族全站广播
    public static final int ROOM_RECEIVE_NOBLE_PUBLIC_MSG = 0x00000038;

    public static final int ROOM_RECEIVE_OPEN_NOBLE_NOTIFY = 0x00000039;

    public static final int ROOM_ENTER_OTHER_ROOM = 0x00000044;

    //socket进房成功
    public static final int SOCKET_ROOM_ENTER_SUC = 0x00000045;

    protected long roomUid = 0;
    protected int roomType = 0;

    /**
     * 召集令，首页展示，区别于房间roomEvent
     */
    public static final int CALL_UP_MSG = 0x00000048;

    public int getRoomType() {
        return roomType;
    }

    public RoomEvent setRoomType(int roomType) {
        this.roomType = roomType;
        return this;
    }

    public long getRoomUid() {
        return roomUid;
    }

    public RoomEvent setRoomUid(long roomUid) {
        this.roomUid = roomUid;
        return this;
    }


    protected int event = NONE;
    protected int micPosition = Integer.MIN_VALUE;
    protected int posState = -1;
    protected ChatRoomKickOutEvent reason;
    protected String account;
    protected RoomInfo roomInfo;
    protected boolean success;
    protected AuctionInfo auctionInfo;
    public RoomQueueInfo roomQueueInfo;
    protected int code;
    protected ChatRoomMessage mChatRoomMessage;
    protected List<Integer> micPositionList;
    protected String pairInfo;

    private Json roomCharmList;

    private CallUpBean callUpBean;

    public CallUpBean getCallUpBean() {
        return callUpBean;
    }

    public RoomEvent setCallUpBean(CallUpBean callUpBean) {
        this.callUpBean = callUpBean;
        return this;
    }

    public AnnualRoomRankAttachment getAnnualRoomRank() {
        return annualRoomRankAttachment;
    }

    public RoomEvent setAnnualRoomRank(AnnualRoomRankAttachment annualRoomRankAttachment) {
        this.annualRoomRankAttachment = annualRoomRankAttachment;
        return this;
    }

    private AnnualRoomRankAttachment annualRoomRankAttachment;

    public DetonateGiftAttachment getDetonateGiftAttachment() {
        return detonateGiftAttachment;
    }

    public RoomEvent setDetonateGiftAttachment(DetonateGiftAttachment detonateGiftAttachment) {
        this.detonateGiftAttachment = detonateGiftAttachment;
        return this;
    }

    private RoomRedPacketAttachment redPacketAttachment;

    public RoomRedPacketAttachment getRedPacketAttachment() {
        return redPacketAttachment;
    }

    public RoomEvent setRoomRedPacketAttachment(RoomRedPacketAttachment redPacketAttachment) {
        this.redPacketAttachment = redPacketAttachment;
        return this;
    }

    private RoomRedPacketFinishAttachment redPacketFinishAttachment;

    public RoomRedPacketFinishAttachment getRoomRedPacketFinishAttachment() {
        return redPacketFinishAttachment;
    }

    public RoomEvent setRoomRedPacketFinishAttachment(RoomRedPacketFinishAttachment redPacketAttachment) {
        this.redPacketFinishAttachment = redPacketAttachment;
        return this;
    }

    private NoblePublicMsgAttachment noblePublicMsgAttachment;

    public NoblePublicMsgAttachment getNoblePublicMsgAttachment() {
        return noblePublicMsgAttachment;
    }

    public RoomEvent setNoblePublicMsgAttachment(NoblePublicMsgAttachment noblePublicMsgAttachment) {
        this.noblePublicMsgAttachment = noblePublicMsgAttachment;
        return this;
    }

    private DetonateGiftAttachment detonateGiftAttachment;
    private OpenNobleNotifyAttachment openNobleNotifyAttachment;

    public OpenNobleNotifyAttachment getOpenNobleNotifyAttachment() {
        return openNobleNotifyAttachment;
    }

    public RoomEvent setOpenNobleNotifyAttachment(OpenNobleNotifyAttachment openNobleNotifyAttachment) {
        this.openNobleNotifyAttachment = openNobleNotifyAttachment;
        return this;
    }


    public String getPairInfo() {
        return pairInfo;
    }

    public RoomEvent setPairInfo(String pairInfo) {
        this.pairInfo = pairInfo;
        return this;
    }

    public Json getRoomCharmList() {
        return roomCharmList;
    }

    public RoomEvent setRoomCharmList(Json roomCharmList) {
        this.roomCharmList = roomCharmList;
        return this;
    }

    public int getEvent() {
        return event;
    }

    public RoomEvent setEvent(int event) {
        this.event = event;

        return this;
    }

    public ChatRoomKickOutEvent getReason() {
        return reason;
    }

    public RoomEvent setReason(ChatRoomKickOutEvent reason) {
        this.reason = reason;
        return this;
    }

    public RoomEvent setRoomQueueInfo(RoomQueueInfo roomQueueInfo) {
        this.roomQueueInfo = roomQueueInfo;
        return this;
    }

    public int getMicPosition() {
        return micPosition;
    }

    public RoomEvent setMicPosition(int micPosition) {
        this.micPosition = micPosition;
        return this;
    }

    public int getPosState() {
        return posState;
    }

    public RoomEvent setPosState(int posState) {
        this.posState = posState;
        return this;
    }

    public String getAccount() {
        return account;
    }

    public RoomEvent setAccount(String account) {
        this.account = account;
        return this;
    }

    public RoomInfo getRoomInfo() {
        return roomInfo;
    }

    public RoomEvent setRoomInfo(RoomInfo roomInfo) {
        this.roomInfo = roomInfo;
        return this;
    }

    public boolean isSuccess() {
        return success;
    }

    public RoomEvent setSuccess(boolean success) {
        this.success = success;
        return this;
    }

    public AuctionInfo getAuctionInfo() {
        return auctionInfo;
    }

    public RoomEvent setAuctionInfo(AuctionInfo auctionInfo) {
        this.auctionInfo = auctionInfo;
        return this;
    }

    public ChatRoomMessage getChatRoomMessage() {
        return mChatRoomMessage;
    }

    public RoomEvent setChatRoomMessage(ChatRoomMessage chatRoomMessage) {
        mChatRoomMessage = chatRoomMessage;
        return this;
    }

    public int getCode() {
        return code;
    }

    public RoomEvent setCode(int code) {
        this.code = code;
        return this;
    }

    public List<Integer> getMicPositionList() {
        return micPositionList;
    }

    public RoomEvent setMicPositionList(List<Integer> micPositionList) {
        this.micPositionList = micPositionList;
        return this;
    }
}
