package com.tongdaxing.xchat_core.pay;

import com.alibaba.fastjson.JSONObject;
import com.tongdaxing.erban.libcommon.coremanager.AbstractBaseCore;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.AccountInfo;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.im.notification.INotificationCoreClient;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_core.result.WalletInfoResult;

import java.util.Map;

import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_ACCOUNT;

/**
 * 实现网络的方法
 * Created by zhouxiangfeng on 2017/6/19.
 */

public class PayCoreImpl extends AbstractBaseCore implements IPayCore {
    public static final String TAG = "PayCoreImpl";
    private WalletInfo walletInfo;

    public PayCoreImpl() {
        CoreManager.addClient(this);
    }

    @Override
    public WalletInfo getCurrentWalletInfo() {
        return walletInfo;
    }

    @Override
    public void setCurrentWalletInfo(WalletInfo walletInfo) {
        this.walletInfo = walletInfo;
    }

    @Override
    public void minusGold(int price) {
        if (walletInfo != null) {
            double gold = walletInfo.getGoldNum();
            if (walletInfo.getGoldNum() > 0) {
                walletInfo.setGoldNum(gold - price);
                notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_WALLET_INFO_UPDATE, walletInfo);
            }
        }
    }

    @Override
    public void minusDouzi(int price) {
        if (walletInfo != null) {
            int gold = walletInfo.getPeaNum();
            if (walletInfo.getPeaNum() > 0) {
                walletInfo.setPeaNum(gold - price);
                notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_WALLET_INFO_UPDATE, walletInfo);
            }
        }
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogin(AccountInfo accountInfo) {
        if (accountInfo != null) {
            getWalletInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        }
    }

    @CoreEvent(coreClientClass = INotificationCoreClient.class)
    public void onReceivedCustomNotification(JSONObject attachment) {
        int headType = attachment.getIntValue("first");
        int subType = attachment.getIntValue("second");
        if (headType > 0 && subType > 0) {
            if (headType == CUSTOM_MSG_HEADER_TYPE_ACCOUNT) {
                JSONObject jsonObject = (JSONObject) attachment.get("data");
                if (jsonObject != null) {
                    WalletInfo walletInfo = new WalletInfo();
                    walletInfo.setUid(jsonObject.getLongValue("uid"));
                    walletInfo.setDepositNum(jsonObject.getIntValue("depositNum"));
                    walletInfo.setDiamondNum(jsonObject.getDouble("diamondNum"));
                    walletInfo.setPeaNum(jsonObject.getIntValue("peaNum"));
                    walletInfo.setGoldNum(jsonObject.getIntValue("goldNum"));
                    this.walletInfo = walletInfo;
                    notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_WALLET_INFO_UPDATE, this.walletInfo);
                }
            }
        }
    }

    //获取钱包信息
    @Override
    public void getWalletInfo(long uid) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(uid));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());

        OkHttpManager.getInstance().getRequest(UriProvider.getWalletInfos(), param, new OkHttpManager.MyCallBack<WalletInfoResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_GET_WALLENT_INOF_FAIL,
                        e.getMessage());
            }

            @Override
            public void onResponse(WalletInfoResult response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        walletInfo = response.getData();
                        notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_GET_WALLENT_INOF, response.getData());
                    } else {
                        notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_GET_WALLENT_INOF_FAIL, response.getMessage());
                    }
                }
            }
        });
    }

    @Override
    public void updateMyselfWalletInfo() {
        getWalletInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }
}
