package com.tongdaxing.xchat_core.pay.bean;

import java.io.Serializable;

/**
 * @author Administrator
 * @date 2017/7/20 0020
 */

public class WalletInfo implements Serializable {
    public long uid;
    /**
     * 金币总额
     */
    public double goldNum;
    /**
     * 钻石数量
     */
    public double diamondNum;
    /**
     * 预扣款（押金）
     */
    public int depositNum;
    /**
     * 账户
     */
    public int amount;
    /**
     * 普通金币
     */
    public double chargeGoldNum;
    /**
     * 贵族金币
     */
    public double nobleGoldNum;

    /**
     * 甜豆金币
     */
    public int peaNum;

    public int getPeaNum() {
        if (peaNum < 0) {
            peaNum = 0;
        }
        return peaNum;
    }

    public void setPeaNum(int peaNum) {
        this.peaNum = peaNum;
    }

    public double getDiamondNum() {
        return diamondNum;
    }

    public void setDiamondNum(double diamondNum) {
        this.diamondNum = diamondNum;
    }


    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public double getGoldNum() {
        return goldNum;
    }

    public void setGoldNum(double goldNum) {
        this.goldNum = goldNum;
    }


    public int getDepositNum() {
        return depositNum;
    }

    public void setDepositNum(int depositNum) {
        this.depositNum = depositNum;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "WalletInfo{" +
                "uid=" + uid +
                ", goldNum=" + goldNum +
                ", diamondNum=" + diamondNum +
                ", depositNum=" + depositNum +
                ", amount=" + amount +
                '}';
    }

    public double getChargeGoldNum() {
        return chargeGoldNum;
    }

    public void setChargeGoldNum(double chargeGoldNum) {
        this.chargeGoldNum = chargeGoldNum;
    }

    public double getNobleGoldNum() {
        return nobleGoldNum;
    }

    public void setNobleGoldNum(double nobleGoldNum) {
        this.nobleGoldNum = nobleGoldNum;
    }
}
