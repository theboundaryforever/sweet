package com.tongdaxing.xchat_core.pay;

import com.tongdaxing.erban.libcommon.coremanager.IBaseCore;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;

/**
 * 访问网络 的方法
 * Created by zhouxiangfeng on 2017/6/19.
 */

public interface IPayCore extends IBaseCore {
    WalletInfo getCurrentWalletInfo();

    void setCurrentWalletInfo(WalletInfo walletInfo);

    void minusGold(int price);

    void minusDouzi(int price);

    //    查询钱包的方法，需要加权限ticket
    void getWalletInfo(long uid);

    void updateMyselfWalletInfo();
}
