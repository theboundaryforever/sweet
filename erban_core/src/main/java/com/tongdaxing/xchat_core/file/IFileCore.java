package com.tongdaxing.xchat_core.file;

import com.tongdaxing.erban.libcommon.coremanager.IBaseCore;

import java.io.File;

/**
 * Created by zhouxiangfeng on 2017/5/16.
 */

public interface IFileCore extends IBaseCore{

    void upload(File file);

    void uploadPhoto(File file);

    void uploadLogFile(File file);

    void download();
}
