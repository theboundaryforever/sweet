package com.tongdaxing.xchat_core.audio;

import android.content.Context;
import android.media.AudioManager;

import com.netease.nimlib.sdk.media.record.AudioRecorder;
import com.netease.nimlib.sdk.media.record.IAudioRecordCallback;
import com.netease.nimlib.sdk.media.record.RecordType;
import com.tongdaxing.erban.libcommon.audio.AudioPlayer;
import com.tongdaxing.erban.libcommon.audio.OnPlayListener;

/**
 * Created by zhouxiangfeng on 2017/5/20.
 */

public class AudioPlayAndRecordManager {

    private static AudioPlayAndRecordManager audioPlayManager;
    private AudioPlayer player;
    private AudioRecorder recorder;

    private AudioPlayAndRecordManager() {

    }

    //        // 定义一个播放进程回调类
//        OnPlayListener listener = new OnPlayListener() {
//
//            // 音频转码解码完成，会马上开始播放了
//            public void onPrepared() {
//            }
//
//            // 播放结束
//            public void onCompletion() {
//            }
//
//            // 播放被中断了
//            public void onInterrupt() {
//            }
//
//            // 播放过程中出错。参数为出错原因描述
//            public void onError(String error) {
//            }
//
//            // 播放进度报告，每隔 500ms 会回调一次，告诉当前进度。 参数为当前进度，单位为毫秒，可用于更新 UI
//            public void onPlaying(long curPosition) {
//            }
//        };

    public static AudioPlayAndRecordManager getInstance() {
        if (audioPlayManager == null)
            audioPlayManager = new AudioPlayAndRecordManager();
        return audioPlayManager;
    }

    public AudioPlayer getAudioPlayer(Context context, String filePath, OnPlayListener listener) {
        // 构造播放器对象
        player = new AudioPlayer(context, filePath, listener);
        return player;
    }

    public void play() {
        // 开始播放。需要传入一个 Stream Type 参数，表示是用听筒播放还是扬声器。取值可参见
        // android.media.AudioManager#STREAM_***
        // AudioManager.STREAM_VOICE_CALL 表示使用听筒模式
        // AudioManager.STREAM_MUSIC 表示使用扬声器模式
        player.start(AudioManager.STREAM_MUSIC);
    }


    public void seekTo(int pausedPostion) {
        // 如果中途切换播放设备，重新调用 start，传入指定的 streamType 即可。player 会自动停止播放，然后再以新的 streamType 重新开始播放。
        // 如果需要从中断的地方继续播放，需要外面自己记住已经播放过的位置，然后在 onPrepared 回调中调用 seekTo
        player.seekTo(pausedPostion);
    }


    public void stopPlay() {
        // 主动停止播放
        if(null != player){
            player.stop();
        }
    }


    public AudioRecorder getAudioRecorder(Context context, IAudioRecordCallback callback) {
        // 初始化recorder
        recorder = new AudioRecorder(
                context,
                RecordType.AAC, // 录制音频类型（aac/amr)
                0, // 最长录音时长，到该长度后，会自动停止录音, 默认120s
                callback);
        return recorder;
    }

    public boolean isPlaying() {
        if (player != null) {
            return player.isPlaying();
        }
        return false;
    }

    public void startRecord() {

        if (null != recorder) {
            if (recorder.isRecording()) {
                recorder.completeRecord(true);
                try{
                    //云信内部的api，在执行 handlerThread.getLooper().quit();方法时，
                    // 可能会出现handlerThread.getLooper()为空导致的空指针的情况
                    recorder.destroyAudioRecorder();
                } catch (NullPointerException npex){
                    npex.printStackTrace();
                }

            }
            recorder.startRecord();
        }
    }

    public void stopRecord(boolean cancel) {
        if (null != recorder && recorder.isRecording()) {
            recorder.completeRecord(cancel);

        }
    }

    /**
     * 在录音过程中可以获取当前录音时最大振幅（40ms更新一次数据），接口为
     *
     * @return
     */
    public int getCurrentRecordMaxAmplitude(Context var1, RecordType var2, int var3, IAudioRecordCallback var4) {
        return recorder.getCurrentRecordMaxAmplitude();
    }

    /** 释放资源 */
    public void release() {
        if (isPlaying()){
            stopPlay();
        }
        if(null != player){
            player.setOnPlayListener(null);
        }
        if(null != recorder){
            recorder.destroyAudioRecorder();
            recorder = null;
        }
    }
}
