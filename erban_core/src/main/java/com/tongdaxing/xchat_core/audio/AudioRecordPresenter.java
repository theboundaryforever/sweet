package com.tongdaxing.xchat_core.audio;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;

/**
 * <p>  </p>
 *
 * @author jiahui
 * @date 2018/1/4
 */
public class AudioRecordPresenter extends AbstractMvpPresenter<IAudioRecordView> {

    public AudioRecordPresenter() {

    }

    public void exitRoom() {
        BaseRoomServiceScheduler.exitRoom(null);
    }
}
