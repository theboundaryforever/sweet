package com.tongdaxing.xchat_core.audio;

/**
 * 当前录音状态
 */
public enum  AudioRecordStatus {
    /**
     * 录音中
     */
    STATE_RECORD_RECORDING,
    /**
     * 录音成功
     */
    STATE_RECORD_SUCCESS,
    /**
     * 未录音或者录音失败
     */
    STATE_RECORD_NORMAL,
    /**
     * 录音已存在
     */
    STATE_RECORD_EXISTED
}
