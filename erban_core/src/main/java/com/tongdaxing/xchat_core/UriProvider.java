package com.tongdaxing.xchat_core;

import android.text.TextUtils;

import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.erban.libcommon.utils.pref.CommonPref;

/**
 * 请求接口接口地址
 */
public class UriProvider {

    /**
     * 默认以下生产环境地址
     */
    public static String JAVA_WEB_URL = "https://www.pinjin88.com";
    public static String JAVA_WEBVIEW_URL = "https://www.pinjin88.com";
    public static String IM_SERVER_URL = "https://www.pinjin88.com";

    public static String JAVA_WEB_TMXQ_URL = "https://www.yuhuankj.cn";
    public static String JAVA_IMG_URL = "https://img.pinjin88.com";
    public static String HOME_RANKING_URL = "/ttyy/rank/index.html";
    private static String DEBUG_URL;

    public static void init(Env.UriSetting uriSetting) {
        if (uriSetting == Env.UriSetting.Product) {
            //生产环境地址
            initProductUri();
        } else if (uriSetting == Env.UriSetting.Test) {
            //测试环境地址
            initTestUri();
        }
    }

    public static void initDevUri() {
        JAVA_WEB_URL = "https://beta.pinjin88.com";
        JAVA_WEBVIEW_URL = "http://beta.pinjin88.com";
        IM_SERVER_URL = "https://beta.pinjin88.com";
        JAVA_WEB_TMXQ_URL = "http://beta.yuhuankj.cn/";
    }

    public static String checkUpdate() {
        return IM_SERVER_URL.concat("/version/get");
    }

    public static void initDevUri(String url) {
        DEBUG_URL = url;
        JAVA_WEB_URL = url;
        IM_SERVER_URL = url;
    }

    public static void initProductUri() {
        JAVA_WEB_URL = "https://www.pinjin88.com";
        JAVA_WEBVIEW_URL = "https://www.pinjin88.com";
        IM_SERVER_URL = "https://www.pinjin88.com";
        JAVA_WEB_TMXQ_URL = "https://www.pinjin88.com";
    }

    public static void initTestUri() {
        int enviroment = CommonPref.instance(BasicConfig.INSTANCE.getAppContext()).getInt("enviroment");
        if (enviroment == 0) {
            initProductUri();//其他使用DEV的配置，就神曲这个跟新一下
        } else {
            if (!TextUtils.isEmpty(DEBUG_URL)) {
                initDevUri(DEBUG_URL);
            } else {
                initDevUri();
            }
        }
    }

    public static String getGroupResourceBaseUri() {
        return JAVA_WEB_URL.concat("/app/service/resource/list");
    }

    /**
     * 注册接口
     *
     * @return
     */
    public static String getRegisterResourceUrl() {
        return IM_SERVER_URL.concat("/acc/signup");
    }


    /**
     * 登录接口
     *
     * @return
     */
    public static String getLoginResourceUrl() {
        return IM_SERVER_URL.concat("/oauth/token");
    }

    /**
     * 获取ticket
     *
     * @return
     */
    public static String getAuthTicket() {
        return IM_SERVER_URL.concat("/oauth/ticket");
    }

    /**
     * 获取短信验证码
     *
     * @return
     */
    public static String getSMSCode() {
        return IM_SERVER_URL.concat("/acc/sms");
    }

    /**
     * 找回，修改密码
     *
     * @return
     */
    public static String modifyPsw() {
        return IM_SERVER_URL.concat("/acc/pwd/reset");
    }

    /**
     * 登出
     *
     * @return
     */
    public static String logout() {
        return IM_SERVER_URL.concat("/acc/logout");
    }

    /**
     * uid-操作人，ticket，queryUid-待查询UID
     *
     * @return
     */
    public static String getUserInfo() {
        return IM_SERVER_URL.concat("/user/v2/get");
    }

    public static String getUserInfoListUrl() {
        return IM_SERVER_URL.concat("/user/list");
    }


    public static String updateUserInfo() {
        return IM_SERVER_URL.concat("/user/update");
    }


    public static String updateTestUserInfo() {
        return IM_SERVER_URL.concat("/user/v2/updateTest");
    }

    public static String addPhoto() {
        return IM_SERVER_URL.concat("/photo/upload");
    }

    public static String deletePhoto() {
        return IM_SERVER_URL.concat("/photo/delPhoto");
    }

    /**
     * 获取礼物列表数据
     * userId
     * orderType = 1 按照数量排序
     * orderType = 2 按照价格排序
     *
     * @return
     */
    public static String giftWall() {
        return IM_SERVER_URL.concat("/giftwall/get");
    }

    public static String praise() {
        return IM_SERVER_URL.concat("/fans/like");
    }

    public static String deleteLike() {
        return IM_SERVER_URL.concat("/fans/fdelete");
    }

    public static String isLike() {
        return IM_SERVER_URL.concat("/fans/islike");
    }

    public static String searchUserInfo() {
        return IM_SERVER_URL.concat("/search/user");
    }

    public static String getAllFans() {
        return IM_SERVER_URL.concat("/fans/following");

    }

    public static String getFansList() {
        return IM_SERVER_URL.concat("/fans/fanslist");

    }

    public static String getKtvRoomList() {
        return IM_SERVER_URL.concat("/home/v2/ktv/index");

    }

    public static String getHomeFollow() {
        return IM_SERVER_URL.concat("/home/v2/getFollow");
    }

    public static String getUserRoom() {
        return IM_SERVER_URL.concat("/userroom/get");
    }

    public static String userRoomIn() {
        return IM_SERVER_URL.concat("/userroom/in");
    }

    public static String userRoomOut() {
        return IM_SERVER_URL.concat("/userroom/out");
    }

    /**
     * 房间标签列表
     */
    public static String getRoomTagList() {
        return IM_SERVER_URL.concat("/room/tag/all");
    }

    public static String openRoom() {
        return IM_SERVER_URL.concat("/room/open");
    }

    /**
     * uid-操作人，ticket，queryUid-待查询UID
     *
     * @return
     */
    public static String getRoomInfo() {
        return IM_SERVER_URL.concat("/room/v2/get");
    }

    public static String roomStatistics() {
        return IM_SERVER_URL.concat("/basicusers/record");
    }

    /**
     * 更新房间设置信息
     *
     * @return
     */
    public static String updateRoomInfo() {
        return IM_SERVER_URL.concat("/room/update");
    }

    public static String updateRoomInfoByAdimin() {
        return IM_SERVER_URL.concat("/room/updateByAdmin");
    }

    public static String closeRoom() {
        return IM_SERVER_URL.concat("/room/close");
    }

    /**
     * 获取轻聊房贡献榜人数信息列表
     *
     * @return
     */
    @Deprecated
    public static String getRoomConsumeList() {
        return IM_SERVER_URL.concat("/roomctrb/query");
    }

    public static String roomSearch() {
        return IM_SERVER_URL.concat("/search/room");
    }

    public static String searchByType() {
        return IM_SERVER_URL.concat("/search/v2/byType");
    }

    public static String recommRoom() {
        return IM_SERVER_URL.concat("/search/v2/recomm");
    }

    public static String getAuctionInfo() {
        return IM_SERVER_URL.concat("/auction/get");
    }

    public static String auctionStart() {
        return IM_SERVER_URL.concat("/auction/start");
    }

    /**
     * 用户参与竞拍报价
     *
     * @return
     */
    public static String auctionUp() {
        return IM_SERVER_URL.concat("/auctrival/up");
    }

    /**
     * 房主结束竞拍
     */
    public static String finishAuction() {
        return IM_SERVER_URL.concat("/auction/finish");
    }

    public static String weekAucionList() {
        return IM_SERVER_URL.concat("/weeklist/query");
    }

    public static String totalAuctionList() {
        return IM_SERVER_URL.concat("/sumlist/query");
    }

    /**
     * 锁坑，开坑操作
     *
     * @return
     */
    public static String getlockMicroPhone() {
        return IM_SERVER_URL.concat("/room/mic/lockpos");
    }

    /**
     * 锁麦，开麦操作
     *
     * @return
     */
    public static String operateMicroPhone() {
        return IM_SERVER_URL.concat("/room/mic/lockmic");
    }

    /**
     * 获取订单列表
     */
    public static String getOrderList() {
        return IM_SERVER_URL.concat("/order/list");
    }

    /**
     * 完成订单
     */
    public static String finishOrder() {
        return IM_SERVER_URL.concat("/order/finish");
    }


    /**
     * 获取指定订单
     */
    public static String getOrder() {
        return IM_SERVER_URL.concat("/order/get");
    }

    /**
     * 获取房间列表
     */
    public static String getRoomList() {
        return IM_SERVER_URL.concat("/home/get");

    }

    /**
     * 获取房间列表
     */
    public static String getRoomListV2() {
        return IM_SERVER_URL.concat("/home/getV2");

    }

    /**
     * 获取banner列表
     */
    public static String getBannerList() {
        return IM_SERVER_URL.concat("/banner/list");

    }


    /**
     * 获取礼物列表
     */
    public static String getGiftList() {
        return IM_SERVER_URL.concat("/gift/listV2");
    }

    /**
     * 送礼物新接口
     *
     * @return
     */
    public static String sendGiftV3() {

        return IM_SERVER_URL.concat("/gift/sendV3");
    }

    /**
     * 全麦送
     *
     * @return
     */
    public static String sendWholeGiftV3() {
        return IM_SERVER_URL.concat("/gift/sendWholeMicroV3");
    }

    /**
     * 获取表情列表
     */
    public static String getFaceList() {
        return IM_SERVER_URL.concat("/client/init");
    }

    /**
     * 客户端初始化
     *
     * @return --
     */
    public static String getInit() {
        return IM_SERVER_URL.concat("/client/init");
    }

    /**
     * 获取版本号
     */
    public static String getVersions() {
        return IM_SERVER_URL.concat("/appstore/check");
    }

    /**
     * 获取钱包信息
     */
    public static String getWalletInfos() {
        return IM_SERVER_URL.concat("/purse/query");
    }

    /**
     * 获取充值产品列表
     */
    public static String getChargeList() {
        return IM_SERVER_URL.concat("/chargeprod/list");
    }

    /**
     * 发起充值
     */
    public static String requestCharge() {
        return IM_SERVER_URL.concat("/charge/apply");
    }

    /**
     * 汇聚支付地址
     */
    public static String getJoinPay() {
        return IM_SERVER_URL.concat("/charge/joinpay/apply");
    }

    /**
     * 汇潮支付地址
     */
    public static String getHuiChaoPay() {
        return IM_SERVER_URL.concat("/charge/ecpss/alipay/apply");
    }

    public static String requestCDKeyCharge() {
        return IM_SERVER_URL.concat("/redeemcode/use");
    }

    /**
     * 钻石兑换
     */
    public static String changeGold() {
        return IM_SERVER_URL.concat("/change/gold");
    }

    /**
     * 获取提现列表
     */
    public static String getWithdrawList() {
        return IM_SERVER_URL.concat("/withDraw/findList");
    }

    /**
     * 获取提现页用户信息
     */
    public static String getWithdrawInfo() {
        return IM_SERVER_URL.concat("/withDraw/exchange");
    }

    /**
     * 发起兑换
     */
    public static String requestExchange() {
        return IM_SERVER_URL.concat("/withDraw/withDrawCash");
    }

    /**
     * 发起兑换
     */
    public static String requestExchangeV2Url() {
        return IM_SERVER_URL.concat("/withDraw/v2/withDrawCash");
    }

    /**
     * 获取绑定支付宝验证码
     */
    public static String getSms() {
        return IM_SERVER_URL.concat("/withDraw/getSms");
    }

    /**
     * 获取绑定手机验证码
     */
    public static String getSmS() {
        return IM_SERVER_URL.concat("/withDraw/phoneCode");
    }


    /**
     * 获取绑定手机验证码
     * 1注册短信；2更改手机短信；3找回密码短信（更改手机确认短信/钻石兑换金币确认）；4提现验证码；
     */
    public static String getModifyPhoneSMS() {
        return IM_SERVER_URL.concat("/acc/sms");
    }

    /**
     * 绑定支付宝
     */

    public static String binder() {
        return IM_SERVER_URL.concat("/withDraw/bound");
    }

    /**
     * 绑定手机
     */
    public static String binderPhone() {
        return IM_SERVER_URL.concat("/withDraw/phone");
    }

    public static String modifyBinderPhone() {
        return IM_SERVER_URL.concat("/user/confirm");
    }

    public static String modifyBinderNewPhone() {
        return IM_SERVER_URL.concat("/user/replace");
    }


    /**
     * 提交反馈
     */
    public static String commitFeedback() {
        return IM_SERVER_URL.concat("/feedback");
    }

    /**
     * 微信登陆接口
     */
    public static String requestWXLogin() {
        return IM_SERVER_URL.concat("/acc/third/login");
    }

    /**
     * 获取order平均时长
     */
    public static String getAvgChattime() {
        return IM_SERVER_URL.concat("/basicorder/avgchattime");
    }

    /**
     * 是否绑定手机
     */
    public static String isPhones() {
        return IM_SERVER_URL.concat("/user/isBindPhone");
    }

    /**
     * 获取个人支出账单，包含礼物 充值 订单支出
     */
    public static String getAllBills() {
        return IM_SERVER_URL.concat("/personbill/list");
    }

    /**
     * 礼物，密聊，充值，提现账单查询（不包括红包）
     */
    public static String getBillRecord() {
        return IM_SERVER_URL.concat("/billrecord/get");
    }

    /**
     * 红包账单查询
     */
    public static String getPacketRecord() {
        return IM_SERVER_URL.concat("/packetrecord/get");
    }

    /**
     * 账单提现查询(红包提现)
     */
    public static String getPacketRecordDeposit() {
        return IM_SERVER_URL.concat("/packetrecord/deposit");
    }

    public static String getRedPacket() {
        return IM_SERVER_URL.concat("/statpacket/get");
    }


    public static String getShareRedPacket() {
        return IM_SERVER_URL.concat("/usershare/save");
    }

    /**
     * 是否第一次进入，获取红包弹窗
     */
    public static String getRedBagDialog() {
        return IM_SERVER_URL.concat("/packet/first");
    }

    /**
     * 获取红包弹窗活动类型
     */
    public static String getRoomActivityList() {
        return IM_SERVER_URL.concat("/activity/query");
    }

    /**
     * 获取红包提现列表
     */
    public static String getRedBagList() {
        return IM_SERVER_URL.concat("/redpacket/list");
    }

    /**
     * 发起红包提现
     */
    public static String getRedWithdraw() {
        return IM_SERVER_URL.concat("/redpacket/withdraw");
    }

    /**
     * 红包提现列表
     */
    public static String getRedDrawList() {
        return IM_SERVER_URL.concat("/redpacket/drawlist");
        //邀请奖励      /ttyy/reward/reward.html                uid
        //邀请大作战     /ttyy/activity/activity.html           uid
        //奖励秘籍      /ttyy/method/method.html                uid
        //邀请人数      /ttyy/Invitation/Invitation.html        uid
        //邀请分成      /ttyy/percentage/percentage.html        uid
    }

    /**
     * 首页排行列表
     */
    public static String getHomeRanking() {
        return IM_SERVER_URL.concat("/allrank/homeV2");
    }

    /**
     * 获取首页tab数据
     */
    public static String getMainTabList() {
        return IM_SERVER_URL.concat("/room/tag/top");
    }

    public static String getMainDataByMenu() {
        return IM_SERVER_URL.concat("/room/tag/classification");
    }

    /**
     * 获取首页热门数据
     */
    public static String getMainHotData() {
        return IM_SERVER_URL.concat("/home/v2/hotindex");
    }

    /**
     * 获取首页遇见
     */
    public static String getHotMeetData() {
        return IM_SERVER_URL.concat("/home/v2/getOppositeSex");
    }


    /**
     * 通过tag获取首页数据
     */
    public static String getMainDataByTab() {

        return IM_SERVER_URL.concat("/home/v2/tagindex");
    }


    /**
     * 或者首页分类页面数据
     */
    public static String getNotHotPageData() {
        return IM_SERVER_URL.concat("/home/v2/tagTop");
    }

    public static String getLotteryActivityPage() {
        return IM_SERVER_URL.concat("/ttyy/luckdraw/index.html");
    }

    public static String getConfigUrl() {
        return IM_SERVER_URL.concat("/client/configure");
    }

    public static String getSensitiveWord() {
        return IM_SERVER_URL.concat("/sensitiveWord/regex");
    }

    public static String getBannedType() {
        return IM_SERVER_URL.concat("/banned/checkBanned");
    }

    //兑换金币接口
    public static String getExchangeGold() {
        return JAVA_WEB_URL.concat("/change/gold");
    }

    //获取钱包信息
    public static String getWalletInfo() {
        return JAVA_WEB_URL.concat("/purse/query");
    }

    //用户是否已经绑定手机
    public static String isBindPhone() {
        return JAVA_WEB_URL.concat("/user/isBindPhone");
    }

    //发现 -- 活动列表
    public static String getFindInfo() {
        return JAVA_WEB_URL.concat("/advertise/getDiscoverList");
    }


    /**
     * PK活动接口模块
     *
     * @return
     */
    //发起保存一个PK
    public static String savePk() {
        return JAVA_WEB_URL.concat("/room/pkvote/v2/save");
    }

    //取消一个PK
    public static String cancelPk() {
        return JAVA_WEB_URL.concat("/room/pkvote/v2/cancel");
    }

    //获取一个PK结果
    public static String getPkResult() {
        return JAVA_WEB_URL.concat("/room/pkvote/v2/get");
    }

    //获取PK历史
    public static String getPkHistoryList() {
        return JAVA_WEB_URL.concat("/room/pkvote/v2/list");
    }

    //取消一个PK
    public static String sendPkVote() {
        return JAVA_WEB_URL.concat("/room/pkvote/v2/vote");
    }

    /**
     * 修改用户列表中的额外字段
     *
     * @return
     */
    public static String markUserExt() {
        return JAVA_WEB_URL.concat("/netease/role/update");
    }

    //获取房间背景
    public static String getRoomBgList() {
        return JAVA_WEB_URL.concat("/room/bg/list");
    }

    //获取房间背景
    public static String getRoomBgV2List() {
        return JAVA_WEB_URL.concat("/room/bg/v2/list");
    }

    /**
     * 举报接口
     *
     * @return
     */
    public static String reportUserUrl() {
        return IM_SERVER_URL.concat("/user/report/save");
    }


    public static String getSearchKtvList() {
        return IM_SERVER_URL.concat("/ktv/search");
    }

    public static String getSingerSong() {
        return IM_SERVER_URL.concat("/ktv/sing/list");
    }

    public static String getKtvSelectSing() {
        return IM_SERVER_URL.concat("/ktv/sing/select");
    }

    public static String getSelectSongList() {
        return IM_SERVER_URL.concat("/ktv/sing/room/list");
    }

    public static String moveSongToSelectListTop() {

        return IM_SERVER_URL.concat("/ktv/sing/top");
    }

    public static String removeSelectSongList() {

        return IM_SERVER_URL.concat("/ktv/sing/remove");
    }

    public static String getOppositeRoom() {
        return IM_SERVER_URL.concat("/home/v2/getOppositeRoom");
    }

    public static String getRoomCharm() {
        return IM_SERVER_URL.concat("/userroom/getRoomCharm");
    }


    public static String getResetRoomCharm() {
        return IM_SERVER_URL.concat("/userroom/resetRoomCharm");
    }

    public static String getChannelConfigure() {
        return IM_SERVER_URL.concat("/client/channelConfigure");
    }

    public static String updateUserGuideStatus() {
        return IM_SERVER_URL.concat("/user/saveGuideState");
    }

    public static String getPairStart() {
        return IM_SERVER_URL.concat("/room/pair/start");
    }

    public static String getPairStop() {
        return IM_SERVER_URL.concat("/room/pair/end");
    }


    public static String getPairPick() {
        return IM_SERVER_URL.concat("/room/pair/pick");
    }


    public static String getPairInfo() {
        return IM_SERVER_URL.concat("/room/pair/get");
    }


    public static String getLinkPool() {
        return IM_SERVER_URL.concat("/room/linkPool");
    }

    public static String getLink() {
        return IM_SERVER_URL.concat("/room/link");
    }

    public static String getVoiceText() {
        return IM_SERVER_URL.concat("/user/getVoiceText");
    }

    public static String getMatchAnimUrl() {
        return "https://img.pinjin88.com/link_effect.svga";
    }

    public static String getCarListUrl() {
        return IM_SERVER_URL.concat("/giftCar/getList");
    }

    public static String getHeadWearListUrl() {
        return IM_SERVER_URL.concat("/headwear/getList");
    }

    public static String getHotMusicListUrl() {
        return IM_SERVER_URL.concat("/sings/hotList");
    }

    public static String getMyUploadMusicListUrl() {
        return IM_SERVER_URL.concat("/sings/myList");
    }

    public static String getMusicReportUrl() {
        return IM_SERVER_URL.concat("/sings/report");
    }

    public static String getQrCodeLoginPcUrl() {
        return IM_SERVER_URL.concat("/sings/login");
    }

    public static String reportMusicPlayUrl() {
        return IM_SERVER_URL.concat("/sings/play");
    }

    public static String adminBlockUrl() {
        return IM_SERVER_URL.concat("/external/admin/block");
    }

    public static String adminUnBlockUrl() {
        return IM_SERVER_URL.concat("/external/admin/unBlock");
    }

    public static String adminMuteUrl() {
        return IM_SERVER_URL.concat("/external/admin/mute");
    }

    public static String adminUnMuteUrl() {
        return IM_SERVER_URL.concat("/external/admin/unMute");
    }

    public static String checkMicroQueueUrl() {
        return IM_SERVER_URL.concat("/room/mic/check");
    }

    public static String handleMicroQueueUrl() {
        return IM_SERVER_URL.concat("/room/mic/handleCheck");
    }

    public static String detonateGiftAnimUrl() {
        return "https://img.pinjin88.com/detonating_gifts.svga";
    }

    //游戏大厅相关接口地址

    //游戏大厅banner
    public static String gameBanner() {
        return JAVA_WEB_URL.concat("/home/v2/gameBanner");
    }

    //游戏大厅icons
    public static String gameIcons() {
        return JAVA_WEB_URL.concat("/home/v2/gameIcon");
    }

    //游戏大厅房间列表
    public static String gameTops() {
        return JAVA_WEB_URL.concat("/home/v2/gameTop");
    }

    // 接受连麦
    public static String agreeLinkMacro() {
        return JAVA_WEB_URL.concat("/privateChat/link/agree");
    }

    // 断开连麦
    public static String finishLinkMacro() {
        return JAVA_WEB_URL.concat("/privateChat/link/break");
    }

    // 取消连麦
    public static String cancelLinkMacro() {
        return JAVA_WEB_URL.concat("/privateChat/link/cancel");
    }

    // 邀请连麦
    public static String invitationLinkMacro() {
        return JAVA_WEB_URL.concat("/privateChat/link/invitation");
    }

    // 拒绝连麦
    public static String refuseLinkMacro() {
        return JAVA_WEB_URL.concat("/privateChat/link/reject");
    }

    // 连麦异常
    public static String linkMacroException() {
        return JAVA_WEB_URL.concat("/privateChat/link/abnormal");
    }

    // 排行榜接口
    public static String getRankList() {
        return JAVA_WEB_URL.concat("/allrank/geth5");
    }

    // 实名认证结果上报接口
    public static String getRealNameResult() {
        return JAVA_WEB_URL.concat("/user/realname/v1/saveNoPic");
    }

    // 实名认证接口
    public static String getRealName() {
        return JAVA_WEB_URL.concat("/user/realname/v3/verifyData");
    }

    // 设置密码接口
    public static String setPwd() {
        return JAVA_WEB_URL.concat("/user/setPwd");
    }

    // 修改密码接口
    public static String modifyPwd() {
        return JAVA_WEB_URL.concat("/user/modifyPwd");
    }

    // 重置密码接口
    public static String resetPwd() {
        return JAVA_WEB_URL.concat("/user/resetPwd");
    }

    // 检测密码接口
    public static String checkPwd() {
        return JAVA_WEB_URL.concat("/user/confirmPayPwd");
    }

    //邀请对方加入游戏
    public static String invtdUser() {
        return IM_SERVER_URL.concat("/minigame/v1/invtdUser");
    }

    //同意游戏接口
    public static String acceptInvt() {
        return IM_SERVER_URL.concat("/minigame/v1/acceptInvt");
    }

    public static String getLuckyWheel() {
        return JAVA_WEB_URL.concat("/ttyy/newluckdraw/index.html");
    }

    /**
     * 小游戏url
     */
    public static String minigameUrl() {
        return IM_SERVER_URL.concat("/minigameh5");
    }

    public static String getKouHongUri() {
        return IM_SERVER_URL.concat("/xyapi/v1/testJump");
    }

    public static String getAgoraKeyUri() {
        return IM_SERVER_URL.concat("/agora/getKey");
    }

    /**
     * 房间红包
     */
    public static String sendRedPacket() {
        return IM_SERVER_URL.concat("/redPacket/send");
    }

    public static String getRedPacketHistory() {
        return IM_SERVER_URL.concat("/redPacket/record");
    }

    public static String receiveRedPacket() {
        return IM_SERVER_URL.concat("/redPacket/receive");
    }

    public static String getRoomHistoryRedPacket() {
        return IM_SERVER_URL.concat("/redPacket/list");
    }

    public static String getRedPacketDetail() {
        return IM_SERVER_URL.concat("/redPacket/detail");
    }

    public static String getSproutNewUserList() {
        return JAVA_WEB_URL.concat("/user/v2/newUserList");
    }

    public static String getSproutNearByUserList() {
        return JAVA_WEB_URL.concat("/user/v2/nearbyList");
    }

    public static String getSecurityReportUrl() {
        return JAVA_WEB_URL.concat("/client/security/saveInfo");
    }

    public static String getRecommAdUrl() {
        return JAVA_WEB_URL.concat("/client/advertisement");
    }

    public static String getBindThirdPlatformSmsCodeUrl() {
        return JAVA_WEB_URL.concat("/withDraw/getSms");
    }

    public static String getBindThirdPlatformUrl() {
        return JAVA_WEB_URL.concat("/withDraw/boundThird");
    }

    public static String getUnBindThirdPlatformUrl() {
        return JAVA_WEB_URL.concat("/withDraw/unBoundThird");
    }

    public static String getCheckSmsCodeUrl() {
        return JAVA_WEB_URL.concat("/withDraw/checkCode");
    }

    //取消游戏接口
    public static String cancelInvt() {
        return IM_SERVER_URL.concat("/minigame/v1/cancelInvtdUser");
    }

    //查询游戏结果接口
    public static String getGameReslt() {
        return IM_SERVER_URL.concat("/minigame/v1/getGameRes");
    }

    //获取游戏列表
    public static String getGames() {
        return IM_SERVER_URL.concat("/minigame/v1/getGames");
    }

    //发送贵族置顶广播
    public static String sendNoblePublicMsg() {
        return IM_SERVER_URL.concat("/vip/sendBroadcast");
    }

    //获取贵族置顶广播
    public static String getNoblePublicMsg() {
        return IM_SERVER_URL.concat("/vip/getBroadcast");
    }

    public static String getRealNameAuthUrl() {
        //return JAVA_WEB_URL.concat("/ttyy/real_name/index.html");//旧的
        //return JAVA_WEB_URL.concat("/ttyy/realname_new/index.html");//新的
        return JAVA_WEB_URL.concat("/ttyy/realname_new1/index.html");//新的
    }

    public static String getChanceMeetingUrl() {
        return JAVA_WEB_URL.concat("/room/linkMulti");
    }

    public static String getOnLineStatusSyncUrl() {
        return JAVA_WEB_URL.concat("/info/sync");
    }


    public static String getCheckRoomAttentionUrl() {
        return JAVA_WEB_URL.concat("/room/attention/checkAttentions");
    }

    public static String getRoomAttentionUrl() {
        return JAVA_WEB_URL.concat("/room/attention/attentions");
    }

    public static String getAttentionRoomListUrl() {
        return JAVA_WEB_URL.concat("/room/attention/getRoomAttentionByUid");
    }

    public static String getRoomCancelAttentionUrl() {
        return JAVA_WEB_URL.concat("/room/attention/delAttentions");
    }

    public static String getRoomContributionListUrl() {
        return JAVA_WEB_URL.concat("/roomctrb/queryByType");
    }

    public static String getLevelUrl() {
        return JAVA_WEB_URL.concat("/ttyy/level_new/index.html");
    }

    public static String getHelpUrl() {
        return "https://mp.weixin.qq.com/s/EAzHdNa6XyT7V0jaBiQmaQ";
    }

    public static String getChanceMeetingMsgAnimUrl() {
        return "https://img.pinjin88.com/pipeiliaotian.svga";
    }

    public static String getChanceMeetingBgAnimUrl() {
        return "https://img.pinjin88.com/pipeiliuxingquanping.svga";
    }

    public static String getNewUserList() {
        return JAVA_WEB_URL.concat("/user/newUserList");
    }

    public static String getCarGiveUrl() {
        return JAVA_WEB_URL.concat("/giftCar/give");
    }

    public static String getCarShopListUrl() {
        return JAVA_WEB_URL.concat("/giftCar/list");
    }

    public static String getHeadWearGiveUrl() {
        return JAVA_WEB_URL.concat("/headwear/give");
    }

    public static String getHeadWearShopListUrl() {
        return JAVA_WEB_URL.concat("/headwear/list");
    }

    public static String getHeadWearShopBuyUrl() {
        return JAVA_WEB_URL.concat("/headwear/list");
    }

    public static String getGiftPurseDrawUrl() {
        return JAVA_WEB_URL.concat("/user/giftPurse/draw");
    }

    public static String getInviteH5Url() {
        return JAVA_WEB_URL.concat("/ttyy/invitation_index/index.html");
    }

    public static String getPhotoSortUrl() {
        return JAVA_WEB_URL.concat("/photo/sortPhoto");
    }

    public static String getUserAgreementUrl() {
        return JAVA_WEB_URL.concat("/ttyy/agreement_tmxq/agreement.html");
    }

    public static String getPrivacyProtocolUrl() {
        return JAVA_WEB_URL.concat("/ttyy/agreement_tmxq/private.html");
    }

    public static String getBuyNobleUrl() {
        return JAVA_WEB_URL.concat("/vip/purse");
    }

    public static String getMyNobleUrl() {
        return JAVA_WEBVIEW_URL.concat("/ttyy/vip/index.html");
    }

    public static String getNobleDetailUrl() {
        return JAVA_WEBVIEW_URL.concat("/ttyy/vip/rule.html");
    }

    public static String getNobleMysteriousAvatarUrl() {
        return "https://img.pinjin88.com/shenmirentouxiang@3x.png";
    }

    public static String getNobleRoomVisiableUrl() {
        return JAVA_WEBVIEW_URL.concat("/vip/setInvisible");
    }

    public static String getRoomUserListUrl() {
        return JAVA_WEB_URL.concat("/room/manage/getUsersListByType");
    }

    public static String getUpdateUserRoomRoleUrl() {
        return JAVA_WEB_URL.concat("/room/setMemberRole");
    }

    public static String getAdmireUserUrl() {
        return JAVA_WEB_URL.concat("/user/admire");
    }

    public static String getUploadVoiceBgUrl() {
        return JAVA_WEB_URL.concat("/user/upload/background");
    }

    public static String getRecordAuthCardVoiceUrl() {
        //return JAVA_WEB_URL.concat("/ttyy/voiceAuthCard/index.html");
        return JAVA_WEB_URL.concat("/ttyy/voiceAuthCard_new/index.html");
    }

    public static String getVoiceAuthCardUrl() {
        //return JAVA_WEB_URL.concat("/ttyy/voiceAuthCard/draw.html");
        return JAVA_WEB_URL.concat("/ttyy/voiceAuthCard_new/draw.html");
    }

    public static String getVoiceAuthCardReportUrl() {
        return JAVA_WEB_URL.concat("/soundcard/v1/analysis");
    }

    public static String getRecordAuthCardVoiceBannerUrl() {
        return JAVA_WEB_URL.concat("/ttyy/voiceAuthCard");
    }

    //获取任务中心任务列表
    public static String getTaskList() {
        return JAVA_WEB_URL.concat("/pea/getMissionInfo");
    }

    //完成任务
    public static String finishTask() {
        return JAVA_WEB_URL.concat("/pea/complete");
    }

    //获取甜豆记录
    public static String getDouziHistory() {
        return JAVA_WEB_URL.concat("/pea/getRecordInfo");
    }


    public static final String shareH5LogoUrl = "https://img.pinjin88.com/tmxq_logo.png";

    //获取点赞记录
    public static String getLikeHistory() {
        return JAVA_WEB_URL.concat("/user/admireRecord");
    }

    //设置点赞记录已读
    public static String setLikeMsgListRead() {
        return JAVA_WEB_URL.concat("/user/setAdmireRead");
    }

    //获取点赞未读消息数量
    public static String getUnreadLikeMsgCount() {
        return JAVA_WEB_URL.concat("/user/getUnReadAdmireRecord");
    }

    /**
     * 获取消息中心未读数逻辑
     *
     * @return
     */
    public static String getUnreadMsgCount() {
        return JAVA_WEB_URL.concat("/userMsg/getUserUnReadMsg");
    }

    //甜豆抽奖地址
    public static String getDouziLuckyAward() {
        return JAVA_WEB_URL.concat("/ttyy/luckDraw_bean/index.html");
    }

    //获取勋章列表地址
    public static String getMedalList() {
        return JAVA_WEB_URL.concat("/title/getUserTitleList");
    }

    //佩戴勋章地址
    public static String setMedal() {
        return JAVA_WEB_URL.concat("/title/wearTitle");
    }

    //获取用户访客记录
    public static String visitorRecord() {
        return JAVA_WEB_URL.concat("/uservisitor/visitorRecord");
    }

    //获取我的足迹记录
    public static String getMyFooprint() {
        return JAVA_WEB_URL.concat("/room/attention/userFootprintList");
    }

    //获取挚友列表
    public static String getBosonFriends() {
        return JAVA_WEB_URL.concat("/bestfriend/bestFriendList");
    }

    //获取挚友申请列表
    public static String getBosonFriendApplys() {
        return JAVA_WEB_URL.concat("/bestfriend/bestFriendApplyList");
    }

    //获取挚友历史列表
    public static String getBosonFriendRecords() {
        return JAVA_WEB_URL.concat("//bestfriend/bestFriendRecordList");
    }

    //同意挚友申请
    public static String agreeBosonFriendApply() {
        return JAVA_WEB_URL.concat("/bestfriend/agreeApply");
    }

    //拒绝（删除）挚友申请
    public static String disagreeBosonFriendApply() {
        return JAVA_WEB_URL.concat("/room/attention/userFootprintList");
    }

    //解除挚友关系
    public static String unbindBosonFriend() {
        return JAVA_WEB_URL.concat("/bestfriend/relieveBestFriend");
    }

    /**
     * 连续签到和萌新大礼包共用接口
     *
     * @return
     */
    public static String getNewsBigGift() {
        return JAVA_WEB_URL.concat("/client/getNewsBigGift");
    }

    /**
     * 领取萌新大礼包接口
     *
     * @return
     */
    public static String receiveNewsBigGift() {
        return JAVA_WEB_URL.concat("/client/receiveNewsBigGift");
    }

    /**
     * 领取萌新大礼包接口
     *
     * @return
     */
    public static String receiveSignInGift() {
        return JAVA_WEB_URL.concat("/pea/receiveSignInGift");
    }


    /**
     * 甜豆签到任务
     *
     * @return
     */
    public static String signInMission() {
        return JAVA_WEB_URL.concat("/pea/signInMission");
    }


    public static String getOpscHomeData() {
        return JAVA_WEB_URL.concat("/home/v3/index");
    }

    //交友速配相关接口
    //获取交友速配基本信息
    public static String getQMInfo() {
        return JAVA_WEB_URL.concat("/userMatch/baseInfo");
    }

    //交友速配
    public static String quickMating() {
        return JAVA_WEB_URL.concat("/userMatch/enterMatch");
    }

    //取消交友速配
    public static String cancelQuickMating() {
        return JAVA_WEB_URL.concat("/userMatch/exitMatch");
    }

    //结束交友速配连麦
    public static String finishQuickMating() {
        return JAVA_WEB_URL.concat("/userMatch/finishMatch");
    }

    //交友速配互动
    public static String quickMatingAction() {
        return JAVA_WEB_URL.concat("/userMatch/operateMatch");
    }

    //交友速配续费
    public static String quickMatingRenew() {
        return JAVA_WEB_URL.concat("/userMatch/renewMatch");
    }

    //发现页相关接口
    //获取发现页数据
    public static String discover() {
        return JAVA_WEB_URL.concat("/home/v3/discover");
    }

    //获取附近页数据
    public static String nearby() {
        return JAVA_WEB_URL.concat("/user/v3/nearbyAllList");
    }


    //根据ID搜索在这个房间的某个用户
    public static String searchUserById() {
        return JAVA_WEB_URL.concat("/userroom/search");
    }

    //锁坑，开坑操作
    public static String getAlllockMicroPhone() {
        return IM_SERVER_URL.concat("/room/mic/lockPosBatch");
    }

    public static String getHisHomeRoom() {
        return JAVA_WEB_URL.concat("/room/nest");
    }


    //财富等级图片地址
    public static String getCFImgUrl(int cfLevel) {
        String levelStr = String.format("cf_%02d.png", cfLevel);
        return JAVA_IMG_URL.concat("/level/" + levelStr);
    }

    //魅力等级图片地址
    public static String getMLImgUrl(int cfLevel) {
        String levelStr = String.format("ml_%02d.png", cfLevel);
        return JAVA_IMG_URL.concat("/level/" + levelStr);
    }

    //成长等级图片地址
    public static String getCZImgUrl(int cfLevel) {
        String levelStr = String.format("cz_%02d.png", cfLevel);
        return JAVA_IMG_URL.concat("/level/" + levelStr);
    }

    //发布召集令
    public static String sendCallup() {
        return JAVA_WEB_URL.concat("/room/conveneRoom");
    }

    /**
     * 获取挚友玩法介绍H5页面URL
     *
     * @return
     */
    public static String getBosonFriendAbout() {
        return JAVA_WEB_URL.concat("/ttyy/bosonFriend/index.html");
    }

    /**
     * 获取房间消息提示列表
     *
     * @return
     */
    public static String getRoomTipsUrl() {
        return JAVA_WEB_URL.concat("/room/personal/getRoomTips");
    }

    /**
     * 获取房间消息提示列表
     *
     * @return
     */
    public static String getRoomTipsSendUrl() {
        return JAVA_WEB_URL.concat("/room/personal/sendTips");
    }

    public static String getApplyList() {
        return JAVA_WEB_URL.concat("/room/personal/mic/getApplyList");
    }

    public static String getEditApplyInfo() {
        return JAVA_WEB_URL.concat("/room/personal/mic/editApplyInfo");
    }

    public static String getRoomApplyInfo() {
        return JAVA_WEB_URL.concat("/room/personal/mic/getRoomApplyInfo");
    }

    public static String getConnAudioReq() {
        return JAVA_WEB_URL.concat("/room/personal/mic/upMic");
    }

    public static String getCallDownAudioConn() {
        return JAVA_WEB_URL.concat("/room/personal/mic/downMic");
    }

    public static String getReqAudioConn() {
        return JAVA_WEB_URL.concat("/room/personal/mic/applyConnect");
    }

    public static String getCancelAudioConnReq() {
        return JAVA_WEB_URL.concat("/room/personal/mic/cancelApplyConnect");
    }

    public static String getAudioConnSwitch() {
        return JAVA_WEB_URL.concat("/room/personal/mic/setConnect");
    }

    public static String getConveneUserList() {
        return JAVA_WEB_URL.concat("/room/conveneUserList");
    }

    public static String getCancelConvene() {
        return JAVA_WEB_URL.concat("/room/cancelConvene");
    }

    public static String getInRoomFromConvene() {
        return JAVA_WEB_URL.concat("/userroom/inRoomFromConvene");
    }


    // --------------------------------------- 竞拍房接口 start -----------------------------

    /**
     * 竞拍开始接口
     * @return
     */
    public static String startRoomAuctionUrl(){
        return JAVA_WEB_URL.concat("/room/auction/start");
    }

    /**
     * 竞拍结束接口
     * @return
     */
    public static String endRoomAuctionUrl(){
        return JAVA_WEB_URL.concat("/room/auction/end");
    }

    /**
     * 参加或者取消竞拍
     *  1参加2取消
     * @return
     */
    public static String joinRoomAuctionUrl(){
        return JAVA_WEB_URL.concat("/room/auction/join");
    }

    /**
     * 参加竞拍的列表
     * @return
     */
    public static String joinRoomAuctionListUrl(){
        return JAVA_WEB_URL.concat("/room/auction/list");
    }

    /**
     * 赠送竞拍优先卡
     * @return
     */
    public static String sendAuctionPriorityCardUrl(){
        return JAVA_WEB_URL.concat("/room/auction/sendCard");
    }

    // ---------------------------------------  竞拍房接口 end ------------------------------


    public static String getRecommendList() {
        return JAVA_WEB_URL.concat("/room/personal/getRecommendList");
    }

    public static String getReturnRoomList() {
        return JAVA_WEB_URL.concat("/room/getRecommendRoomList");
    }

    public static String getMultiRoomInfo() {
        return JAVA_WEB_URL.concat("/room/getRoomInfo");
    }

    public static String getRoomLevelAboutUrl() {
        return JAVA_WEBVIEW_URL.concat("/ttyy/roomLevel/index.html");
    }

    public static String getEmojiListUrl() {
        return JAVA_WEB_URL.concat("/privateChat/expressionList");
    }

    public static String getReportEmojiUrl() {
        return JAVA_WEB_URL.concat("/privateChat/sendExpression");
    }

    public static String getRandomTopicUrl() {
        return JAVA_WEB_URL.concat("/privateChat/randomTopic");
    }


}