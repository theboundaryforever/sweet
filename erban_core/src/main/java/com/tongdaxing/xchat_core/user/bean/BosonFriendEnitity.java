package com.tongdaxing.xchat_core.user.bean;

import java.io.Serializable;

import io.realm.RealmObject;


public class BosonFriendEnitity extends RealmObject implements Serializable {
    private String avatar;//": "https://img.pinjin88.com/Fl51AJoW45VOLX4A07yY4fbrnLxD?imageslim",
    private long createTime;//": 1557223405000,
    private long recordTime;//": 1557223405000,
    private int day;//": 0,
    private long fUid;//": 93000753,
    private int hour;//": 1,
    private int id;//": 11,
    private int level;//": 2,
    private String levelIcon;
    private String levelName;//": "情深挚友",
    private int minute;//": 27,
    private String nick;//": "小黎的守护神",
    private long uid;//": 93000746

    private int vipId;
    private int vipDate;
    private boolean isInvisible;

    public int getVipId() {
        return vipId;
    }

    public void setVipId(int vipId) {
        this.vipId = vipId;
    }

    public int getVipDate() {
        return vipDate;
    }

    public void setVipDate(int vipDate) {
        this.vipDate = vipDate;
    }

    public boolean isInvisible() {
        return isInvisible;
    }

    public void setInvisible(boolean invisible) {
        isInvisible = invisible;
    }

    public String getLevelIcon() {
        return levelIcon;
    }

    public void setLevelIcon(String levelIcon) {
        this.levelIcon = levelIcon;
    }

    public long getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(long recordTime) {
        this.recordTime = recordTime;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public long getfUid() {
        return fUid;
    }

    public void setfUid(long fUid) {
        this.fUid = fUid;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }
}
