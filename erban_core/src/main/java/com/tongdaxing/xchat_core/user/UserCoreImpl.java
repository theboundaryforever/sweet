package com.tongdaxing.xchat_core.user;

import android.support.annotation.Nullable;

import com.netease.nim.uikit.cache.NimUserInfoCache;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.ResponseCode;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.tongdaxing.erban.libcommon.coremanager.AbstractBaseCore;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.erban.libcommon.utils.config.SpEvent;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.AccountInfo;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.result.GiftWallListResult;
import com.tongdaxing.xchat_core.result.UserResult;
import com.tongdaxing.xchat_core.user.bean.GuideStatus;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

/**
 * Created by chenran on 2017/3/15.
 */

public class UserCoreImpl extends AbstractBaseCore implements IUserCore {
    public static final int TYPE_HOME = 1;
    public static final int TYPE_ATTENTION = 2;
    private static final String TAG = "UserCoreImpl";
    private IUserDbCore userDbCore;
    private UserInfo currentUserInfo;
    private Map<Long, UserInfo> mInfoCache = new ConcurrentHashMap(new HashMap<Long, UserInfo>());

    public UserCoreImpl() {
        super();
        userDbCore = CoreManager.getCore(IUserDbCore.class);
        CoreManager.addClient(this);
    }

    private void saveCache(long userId, UserInfo uInfo) {
        if (userId > 0 && uInfo != null) {
            mInfoCache.put(userId, uInfo);
        }
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogin(final AccountInfo accountInfo) {
        long uid = accountInfo.getUid();
        SpUtils.put(getContext(), SpEvent.cache_uid, uid + "");
        currentUserInfo = getCacheUserInfoByUid(accountInfo.getUid());
        if (currentUserInfo != null && (!StringUtil.isEmpty(currentUserInfo.getNick()) && !StringUtil.isEmpty(currentUserInfo.getAvatar()))) {
            updateCurrentUserInfo(currentUserInfo.getUid());
            return;
        }

        NimUserInfo nimUserInfo = NimUserInfoCache.getInstance().getUserInfo(accountInfo.getUid() + "");
        if (nimUserInfo == null) {
            NimUserInfoCache.getInstance().getUserInfoFromRemote(accountInfo.getUid() + "", new RequestCallbackWrapper<NimUserInfo>() {
                @Override
                public void onResult(int i, NimUserInfo nimUserInfo, Throwable throwable) {
                    //增加状态码判断，避免错误返回情况下跳转资料完善页面
                    if (ResponseCode.RES_SUCCESS == i) {
                        if (nimUserInfo == null || StringUtil.isEmpty(nimUserInfo.getName()) || StringUtil.isEmpty(nimUserInfo.getAvatar())) {
                            notifyClients(IUserClient.class, IUserClient.METHOD_ON_NEED_COMPLETE_INFO);
                        } else {
                            updateCurrentUserInfo(accountInfo.getUid());
                        }
                    } else {
                        updateCurrentUserInfo(accountInfo.getUid());
                    }
                }
            });
        } else {
            if (StringUtil.isEmpty(nimUserInfo.getName()) || StringUtil.isEmpty(nimUserInfo.getAvatar())) {
                notifyClients(IUserClient.class, IUserClient.METHOD_ON_NEED_COMPLETE_INFO);
            } else {
                updateCurrentUserInfo(accountInfo.getUid());
            }
        }

    }

    private void updateCurrentUserInfo(final long userId) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("queryUid", String.valueOf(userId));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");

        OkHttpManager.getInstance().getRequest(UriProvider.getUserInfo(), params, new OkHttpManager.MyCallBack<UserResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (currentUserInfo == null || StringUtil.isEmpty(currentUserInfo.getNick()) || StringUtil.isEmpty(currentUserInfo.getAvatar())) {
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_CURRENT_USERINFO_UPDATE_FAIL, e.getMessage());
                }
            }

            @Override
            public void onResponse(UserResult response) {
                if (response.isSuccess()) {
                    UserInfo userInfo = response.getData();
                    if (StringUtil.isEmpty(userInfo.getNick()) || StringUtil.isEmpty(userInfo.getAvatar())) {
                        notifyClients(IUserClient.class, IUserClient.METHOD_ON_NEED_COMPLETE_INFO);
                        return;
                    }
                    saveChanged(userId, response.getData());
                    currentUserInfo = response.getData();
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_CURRENT_USERINFO_UPDATE, response.getData());
                } else {
                    if (currentUserInfo == null || StringUtil.isEmpty(currentUserInfo.getNick()) || StringUtil.isEmpty(currentUserInfo.getAvatar())) {
                        notifyClients(IUserClient.class, IUserClient.METHOD_ON_CURRENT_USERINFO_UPDATE_FAIL, response.getMessage());
                    }
                }
            }
        });
    }

    @Override
    public void requestUserInfo(final long userId, int type) {
        if (userId <= 0) {
            return;
        }

        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("queryUid", String.valueOf(userId));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("type", "" + type);
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");

        OkHttpManager.getInstance().getRequest(UriProvider.getUserInfo(), params, new OkHttpManager.MyCallBack<UserResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_ERROR, e.getMessage());
            }

            @Override
            public void onResponse(UserResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        saveChanged(userId, response.getData());
                        notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO, response.getData());
                    } else {
                        notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_ERROR, response.getMessage());
                    }
                } else {
                    notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_ERROR, response.getMessage());
                }
            }
        });
    }

    @Override
    public void requestUserInfoNotNotify(final long userId, OkHttpManager.MyCallBack<UserInfo> callBack) {
        if (userId <= 0) {
            return;
        }

        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("queryUid", String.valueOf(userId));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");

        OkHttpManager.getInstance().getRequest(UriProvider.getUserInfo(), params, new OkHttpManager.MyCallBack<UserResult>() {
            @Override
            public void onError(Exception e) {
                if (callBack != null) {
                    callBack.onError(e);
                }
            }

            @Override
            public void onResponse(UserResult response) {
                if (response.isSuccess()) {
                    saveChanged(userId, response.getData());
                    if (callBack != null) {
                        callBack.onResponse(response.getData());
                    }
                } else {
                    if (callBack != null) {
                        callBack.onError(new Exception(response.getMessage()));
                    }
                }
            }
        });
    }

    private void saveChanged(long userId, UserInfo data) {
        saveCache(userId, data);
        if (null != userDbCore) {
            userDbCore.saveDetailUserInfo(data);
        }
    }

    @Override
    public void requestUserInfo(final long userId) {
        if (userId <= 0) {
            return;
        }

        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("queryUid", String.valueOf(userId));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");

        OkHttpManager.getInstance().getRequest(UriProvider.getUserInfo(), params, new OkHttpManager.MyCallBack<UserResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_ERROR, e.getMessage());
            }

            @Override
            public void onResponse(UserResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        saveChanged(userId, response.getData());
                        notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO, response.getData());
                    } else {
                        notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_ERROR, response.getMessage());
                    }
                } else {
                    notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_ERROR, response.getMessage());
                }
            }
        });
    }

    @Override
    public void updateGuideStatus(final int type) {
        LogUtils.d(TAG, "updateGuideStatus-type:" + type);
        Map<String, String> params = OkHttpManager.getDefaultParam();
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        params.put("uid", uid + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket() + "");
        params.put("type", String.valueOf(type));
        OkHttpManager.getInstance().postRequest(UriProvider.updateUserGuideStatus(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                LogUtils.d(TAG, "updateGuideStatus-onError");
                e.printStackTrace();

            }

            @Override
            public void onResponse(Json json) {
                LogUtils.d(TAG, "updateGuideStatus-onResponse-json:" + json);
                if (json.num("code") == 200) {
                    UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(uid);
                    if (null != userInfo) {
                        GuideStatus guideStatus = userInfo.getGuideState();
                        //1关注2照片3麦序4礼物5偶遇6游戏大厅{包含了两张}7拍卖房引导图(包含两张)
                        switch (type) {
                            case 1:
                                guideStatus.setType1("1");
                                break;
                            case 2:
                                guideStatus.setType2("1");
                                break;
                            case 3:
                                guideStatus.setType3("1");
                                break;
                            case 4:
                                guideStatus.setType4("1");
                                break;
                            case 5:
                                guideStatus.setType5("1");
                                break;
                            case 6:
                                guideStatus.setType6("1");
                                break;
                            case 7:
                                guideStatus.setType7("1");
                                break;

                            default:
                                break;
                        }
                        userInfo.setGuideState(guideStatus);
                        saveChanged(userInfo.getUid(), userInfo);
                    }

                }

            }
        });
    }

    @Nullable
    @Override
    public UserInfo getCacheUserInfoByUid(long userId) {
        return getCacheUserInfoByUid(userId, false);
    }

    @Override
    public UserInfo getCacheUserInfoByUid(long userId, boolean refresh) {
        if (userDbCore == null) {
            userDbCore = CoreManager.getCore(IUserDbCore.class);
        }

        if (userId == 0) {
            return null;
        }

        UserInfo userInfo = mInfoCache.get(userId);
        if (userInfo == null) {
            //尝试解决https://bugly.qq.com/v2/crash-reporting/crashes/23dbf7aab1/10600/report?pid=1
            if (null != userDbCore) {
                userInfo = userDbCore.queryDetailUserInfo(userId);
            }
        }

        if (userInfo == null || refresh || userInfo.getUid() == 0L) {
            requestUserInfo(userId);

        }
        return userInfo;
    }

    @Override
    public UserInfo getCacheUserInfoByUid(long userId, boolean refresh, int type) {
        if (userDbCore == null) {
            userDbCore = CoreManager.getCore(IUserDbCore.class);
        }

        if (userId == 0) {
            return null;
        }

        UserInfo userInfo = mInfoCache.get(userId);
        if (userInfo == null) {
            //尝试解决https://bugly.qq.com/v2/crash-reporting/crashes/23dbf7aab1/10600/report?pid=1
            if (null != userDbCore) {
                userInfo = userDbCore.queryDetailUserInfo(userId);
            }
        }

        if (userInfo == null || userInfo.getUid() == 0 || refresh) {
            requestUserInfo(userId, type);
        }
        return userInfo;
    }

    @Nullable
    @Override
    public UserInfo getCacheLoginUserInfo() {
        return getCacheUserInfoByUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }

    @Override
    public void requestCompleteUserInfo(final UserInfo userInfo, String shareChannel, String
            shareUid, String roomUid) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
        params.put("ticket", ticket);

        //解决uid可能为空，导致一直转圈的问题
        String cacheUid = "";
        try {
            cacheUid = (String) SpUtils.get(getContext(), SpEvent.cache_uid, "");
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!StringUtils.isEmpty(String.valueOf(userInfo.getUid()))) {
            params.put("uid", String.valueOf(userInfo.getUid()));
        } else if (!StringUtils.isEmpty(cacheUid)) {
            params.put("uid", cacheUid);
        } else {
            notifyClients(IUserClient.class, IUserClient.METHOD_ON_CURRENT_USER_INFO_COMPLETE_FAITH, "数据异常，请杀掉进程再次打开");
            return;
        }
        if (!StringUtils.isEmpty(userInfo.getBirthStr())) {
            params.put("birth", String.valueOf(userInfo.getBirthStr()));
        }
        if (!StringUtils.isEmpty(userInfo.getNick())) {
            params.put("nick", userInfo.getNick());
        }
        if (!StringUtils.isEmpty(userInfo.getAvatar())) {
            params.put("avatar", userInfo.getAvatar());
        }
        if (!StringUtils.isEmpty(userInfo.getCity())) {
            params.put("city", userInfo.getCity());
        }
        if (userInfo.getGender() > 0) {
            params.put("gender", String.valueOf(userInfo.getGender()));
        }
        if (!StringUtils.isEmpty(shareChannel)) {
            params.put("shareChannel", shareChannel);
        }
        if (!StringUtils.isEmpty(shareUid)) {
            params.put("shareUid", shareUid);
            SpUtils.put(getContext(), SpEvent.linkedMeShareUid, "");
        } else {
            String spShareUid = (String) SpUtils.get(getContext(), SpEvent.linkedMeShareUid, "");
            if (!StringUtils.isEmpty(spShareUid)) {
                params.put("shareUid", spShareUid);
                SpUtils.put(getContext(), SpEvent.linkedMeShareUid, "");
            }

        }
        if (!StringUtils.isEmpty(roomUid)) {
            if (isNumeric(roomUid)) {
                params.put("roomUid", roomUid);
            }
        }

        OkHttpManager.getInstance().getRequest(UriProvider.updateUserInfo(), params, new OkHttpManager.MyCallBack<UserResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IUserClient.class, IUserClient.METHOD_ON_CURRENT_USER_INFO_COMPLETE_FAITH, "网络异常");
            }

            @Override
            public void onResponse(UserResult response) {
                if (response != null && response.isSuccess()) {
                    UserInfo data = response.getData();
                    saveChanged(data.getUid(), data);
                    currentUserInfo = data;
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_CURRENT_USERINFO_UPDATE, response.getData());
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_CURRENT_USER_INFO_COMPLETE, response.getData());
                } else {
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_CURRENT_USER_INFO_COMPLETE_FAITH, response.getMessage() + "");
                }
            }
        });
    }

    public boolean isNumeric(String str) {
        Pattern pattern = Pattern.compile("[0-9]*");
        return pattern.matcher(str).matches();
    }

    @Override
    public void requestUpdateUserInfo(final UserInfo userInfo) {
        LogUtils.d(TAG, "requestCompleteUserInfo-userInfo:" + userInfo);
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
        params.put("ticket", ticket);
        if (!StringUtils.isEmpty(String.valueOf(userInfo.getUid()))) {
            params.put("uid", String.valueOf(userInfo.getUid()));
        } else {
            return;
        }
        if (!StringUtils.isEmpty(userInfo.getBirthStr())) {
            params.put("birth", String.valueOf(userInfo.getBirthStr()));
        }
        if (!StringUtils.isEmpty(userInfo.getNick())) {
            params.put("nick", userInfo.getNick());
        }
        if (!StringUtils.isEmpty(userInfo.getSignture())) {
            params.put("signture", userInfo.getSignture());
        }
        if (!StringUtils.isEmpty(userInfo.getUserVoice())) {
            params.put("userVoice", userInfo.getUserVoice());
        }
        if (userInfo.getVoiceDura() >= 0) {
            params.put("voiceDura", String.valueOf(userInfo.getVoiceDura()));
        }
        if (!StringUtils.isEmpty(userInfo.getAvatar())) {
            params.put("avatar", userInfo.getAvatar());
        }
        if (!StringUtils.isEmpty(userInfo.getRegion())) {
            params.put("region", userInfo.getRegion());
        }
        if (!StringUtils.isEmpty(userInfo.getCity())) {
            params.put("city", userInfo.getCity());
        }
        if (!StringUtils.isEmpty(userInfo.getUserDesc())) {
            params.put("userDesc", userInfo.getUserDesc());
        }
        if (userInfo.getGender() > 0) {
            params.put("gender", String.valueOf(userInfo.getGender()));
        }

        if (userInfo.getMessageRestriction() >= 0) {
            params.put("messageRestriction", String.valueOf(userInfo.getMessageRestriction()));
        }

        if (userInfo.getMessageRingRestriction() >= 0) {
            params.put("messageRingRestriction", String.valueOf(userInfo.getMessageRingRestriction()));
        }
        if (userInfo.getInNearby() >= 0) {
            params.put("inNearby", String.valueOf(userInfo.getInNearby()));
        }

        OkHttpManager.getInstance().getRequest(UriProvider.updateUserInfo(), params, new OkHttpManager.MyCallBack<UserResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_UPDAET_ERROR, e.getMessage());
            }

            @Override
            public void onResponse(UserResult response) {
                if (response != null && response.isSuccess()) {
                    UserInfo data = response.getData();
                    saveChanged(data.getUid(), data);
                    currentUserInfo = data;
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_CURRENT_USERINFO_UPDATE, response.getData());
                    notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_UPDAET, response.getData());
                } else {
                    notifyClients(IUserClient.class, IUserClient.METHOD_REQUEST_USER_INFO_UPDAET_ERROR, response.getMessage());
                }
            }
        });
    }

    @Override
    public void requestAddPhoto(String url) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("photoStr", url);
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        params.put("uid", uid + "");

        OkHttpManager.getInstance().postRequest(UriProvider.addPhoto(), params, new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IUserClient.class, IUserClient.METHOD_ON_REQUEST_ADD_PHOTO_FAITH, e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (response != null && response.isSuccess()) {
                    updateCurrentUserInfo(uid);
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_REQUEST_ADD_PHOTO);
                } else {
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_REQUEST_ADD_PHOTO_FAITH, response.getMessage());
                }
            }
        });
    }

    @Override
    public void requestDeletePhoto(long pid) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
        params.put("ticket", ticket);

        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        params.put("uid", uid + "");
        params.put("pid", pid + "");

        OkHttpManager.getInstance().postRequest(UriProvider.deletePhoto(), params, new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IUserClient.class, IUserClient.METHOD_ON_REQUEST_DELETE_PHOTO_FAITH, e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (response != null && response.isSuccess()) {
                    updateCurrentUserInfo(uid);
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_REQUEST_DELETE_PHOTO, pid);
                } else {
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_REQUEST_DELETE_PHOTO_FAITH, response.getMessage());
                }
            }
        });
    }

    @Override
    public void requestUserGiftWall(final long uid, final int orderType) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", uid + "");
        params.put("orderType", orderType + "");

        OkHttpManager.getInstance().getRequest(UriProvider.giftWall(), params, new OkHttpManager.MyCallBack<GiftWallListResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IUserClient.class, IUserClient.METHOD_ON_REQUEST_GIFT_WALL_FAIL, uid, orderType, e.getMessage());
            }

            @Override
            public void onResponse(GiftWallListResult response) {
                if (response != null && response.isSuccess()) {
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_REQUEST_GIFT_WALL, uid, orderType, response.getData());
                } else {
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_REQUEST_GIFT_WALL_FAIL, uid, orderType, response.getMessage());
                }
            }
        });
    }

    @Override
    public void sortPhoto(ArrayList<Long> pidList) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        params.put("uid", uid + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        if (null == pidList || pidList.size() == 0) {
            return;
        }

        //沟通有误，导致后端接收参数格式修改成了"1,2,3"这类格式
        StringBuffer sb = new StringBuffer();
        for (Long pid : pidList) {
            if (sb.length() > 0) {
                sb.append(",");
            }
            sb.append(pid.longValue());
        }
        params.put("pidList", sb.toString());

        OkHttpManager.getInstance().postRequest(UriProvider.getPhotoSortUrl(), params, new OkHttpManager.MyCallBack<GiftWallListResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IUserClient.class, IUserClient.METHOD_ON_NEED_COMPLETE_SORE_PHOTO, false, e.getMessage());
            }

            @Override
            public void onResponse(GiftWallListResult response) {
                if (response != null && response.isSuccess()) {
                    //需要注意这里会触发多个界面回调，可能导致多个接口接连被调用
                    updateCurrentUserInfo(uid);
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_NEED_COMPLETE_SORE_PHOTO, true, response.getData());
                } else {
                    notifyClients(IUserClient.class, IUserClient.METHOD_ON_NEED_COMPLETE_SORE_PHOTO, false, null == response ? "排序失败" : response.getMessage());
                }
            }
        });
    }


}
