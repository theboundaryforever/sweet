package com.tongdaxing.xchat_core.user;

import com.tongdaxing.erban.libcommon.coremanager.AbstractBaseCore;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.coremanager.IUserInfoCore;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.Map;

public class UserInfoCoreImpl extends AbstractBaseCore implements IUserInfoCore {


    private Json integerBooleanMap = new Json();

    @Override
    public Json getBannedMap() {
        return integerBooleanMap;
    }

    @Override
    public boolean checkHasBandPhone() {

        UserInfo cacheLoginUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (cacheLoginUserInfo == null) {
            return false;
        }
        String erbanNo = cacheLoginUserInfo.getErbanNo() + "";
        String phone = cacheLoginUserInfo.getPhone();
        return !erbanNo.equals(phone);
    }

    @Override
    public int getVersion() {
        UserInfo cacheLoginUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (cacheLoginUserInfo == null) {
            return 0;
        }
        return cacheLoginUserInfo.getExperLevel();
    }

    @Override
    public String getSensitiveWord() {
        String sensitiveWordData = CoreManager.getCore(VersionsCore.class).getSensitiveWordData();


        return sensitiveWordData;
    }

    private long requestBannedTime = 0;

    @Override
    public void checkBanned(boolean force) {

        long l = System.currentTimeMillis();
        //请求间距5分钟
        if (!force && l - requestBannedTime < 300 * 1000) {
            return;
        }

        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.getBannedType(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                if (null != e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponse(Json response) {
                if (response.num("code") == 200) {
                    long l = System.currentTimeMillis();
                    requestBannedTime = l;
                    Json data = response.json_ok("data");
//                    sensitiveWordData = data;
                    integerBooleanMap.set(IUserInfoCore.BANNED_ALL, data.boo("all"));
                    integerBooleanMap.set(IUserInfoCore.BANNED_ROOM, data.boo("room"));
                    integerBooleanMap.set(IUserInfoCore.BANNED_PUBLIC_ROOM, data.boo("broadcast"));
                    integerBooleanMap.set(IUserInfoCore.BANNED_P2P, data.boo("chat"));
                }
            }
        });
    }
}
