package com.tongdaxing.xchat_core.user;

import com.tongdaxing.erban.libcommon.coremanager.ICoreClient;
import com.tongdaxing.xchat_core.user.bean.CheckUpdataBean;

/**
 * Created by Administrator on 2017/7/12 0012.
 */

public interface VersionsCoreClient extends ICoreClient {

    String METHOD_GET_VERSION = "onGetVersion";
    String METHOD_GET_VERSION_ERROR = "onGetVersionError";
    String METHOD_VERSION_UPDATA_DIALOG = "onVersionUpdataDialog";
    String onGetChannelConfig = "onGetChannelConfig";

    public void onGetVersion(Boolean isBoolean);

    public void onGetVersionError(String error);

    public void onVersionUpdataDialog(CheckUpdataBean checkUpdataBean);

    public void onGetChannelConfig(boolean isSuccess, String msg, int homeIndex);
}
