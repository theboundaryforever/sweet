package com.tongdaxing.xchat_core.user;


import com.tongdaxing.erban.libcommon.coremanager.AbstractBaseCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import io.realm.Realm;
import io.realm.exceptions.RealmError;

/**
 * Created by chenran on 2017/3/15.
 */

public class UserDbCoreImpl extends AbstractBaseCore implements IUserDbCore {
    private Realm mRealm;

    public UserDbCoreImpl() {
        mRealm = Realm.getDefaultInstance();
    }

    @Override
    public void saveDetailUserInfo(UserInfo userInfo) {
        try {
            mRealm.beginTransaction();
            mRealm.copyToRealmOrUpdate(userInfo);
            mRealm.commitTransaction();
        }catch (RealmError | Exception ex){
            ex.printStackTrace();
        }

    }

    @Override
    public UserInfo queryDetailUserInfo(long uid) {
        UserInfo userInfo = mRealm.where(UserInfo.class).equalTo("uid", uid).findFirst();
        return null == userInfo ? new UserInfo() : Realm.getDefaultInstance().copyFromRealm(userInfo);
    }
}
