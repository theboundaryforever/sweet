package com.tongdaxing.xchat_core.user;

import com.tongdaxing.erban.libcommon.coremanager.AbstractBaseCore;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.result.AttentionListResult;
import com.tongdaxing.xchat_core.result.FansListResult;

import java.util.Map;

/**
 * Created by Administrator on 2017/7/5 0005.
 */

public class AttentionCoreImpl extends AbstractBaseCore implements AttentionCore {
    private static final String TAG = "AttentionCoreImpl";

    @Override
    public void getAttentionList(long uid, final int page, int pageSize) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(uid));
        param.put("pageSize", String.valueOf(pageSize));
        param.put("pageNo", String.valueOf(page));

        OkHttpManager.getInstance().getRequest(UriProvider.getAllFans(), param, new OkHttpManager.MyCallBack<AttentionListResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(AttentionCoreClient.class, AttentionCoreClient.METHOD_GET_ATTENTION_LIST_FAIL, e.getMessage(), page);
            }

            @Override
            public void onResponse(AttentionListResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(AttentionCoreClient.class, AttentionCoreClient.METHOD_GET_ATTENTION_LIST, response.getData(), page);
                    } else {
                        notifyClients(AttentionCoreClient.class, AttentionCoreClient.METHOD_GET_ATTENTION_LIST_FAIL, response.getMessage(), page);
                    }
                }
            }
        });
    }

    @Override
    public void getFansList(long uid, final int pageCount, int pageSize, final int pageType) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(uid));
        param.put("pageNo", String.valueOf(pageCount));
        param.put("pageSize", String.valueOf(pageSize));

        OkHttpManager.getInstance().getRequest(UriProvider.getFansList(), param, new OkHttpManager.MyCallBack<FansListResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(AttentionCoreClient.class, AttentionCoreClient.METHOD_GET_FANSLIST_FAIL, e.getMessage(), pageType, pageCount);
            }

            @Override
            public void onResponse(FansListResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(AttentionCoreClient.class, AttentionCoreClient.METHOD_GET_FANSLIST, response.getData(), pageType, pageCount);
                    } else {
                        notifyClients(AttentionCoreClient.class, AttentionCoreClient.METHOD_GET_FANSLIST_FAIL, response.getMessage(), pageType, pageCount);
                    }
                }
            }
        });
    }
}
