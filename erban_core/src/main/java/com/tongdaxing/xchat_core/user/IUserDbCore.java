package com.tongdaxing.xchat_core.user;


import com.tongdaxing.erban.libcommon.coremanager.IBaseCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

/**
 * Created by chenran on 2017/3/15.
 */

public interface IUserDbCore extends IBaseCore {

    /**
     * 保存用户详细信息
     * @param userInfo
     */
    public void saveDetailUserInfo(final UserInfo userInfo);

    /**
     * 查询用户详细信息
     * @param uid
     */
    public UserInfo queryDetailUserInfo(final long uid);


}
