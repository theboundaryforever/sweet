package com.tongdaxing.xchat_core.user;

import android.support.annotation.Nullable;

import com.tongdaxing.erban.libcommon.coremanager.IBaseCore;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.ArrayList;

/**
 * Created by chenran on 2017/3/15.
 */

public interface IUserCore extends IBaseCore {

    /**
     * 请求详细用户信息
     * 此方法不同的结果是返回callback，不用coreManager作通知
     */
    void requestUserInfoNotNotify(long userId, OkHttpManager.MyCallBack<UserInfo> callBack);

    /**
     * 请求详细用户信息
     * 回调onRequestDetailUserInfo
     *
     * @param userId
     */
    void requestUserInfo(long userId);

    /**
     * 请求详细用户信息 回调onRequestDetailUserInfo 新增-用户访问他人资料页的时候，需要上传type=1来记录
     *
     * @param userId
     * @param type
     */
    void requestUserInfo(long userId, int type);

    void updateGuideStatus(int type);

    /**
     * 通过uid查询缓存，同步接口，返回可能NULL
     *
     * @param userId
     * @return
     */
    @Nullable
    UserInfo getCacheUserInfoByUid(long userId);

    /**
     * 通过uid查询缓存，同步接口，返回可能NULL
     *
     * @param userId
     * @param refresh
     * @return
     */
    UserInfo getCacheUserInfoByUid(long userId, boolean refresh);

    /**
     * 通过uid查询缓存，同步接口，返回可能NULL
     * 新增-用户访问他人资料页的时候，需要上传type=1来记录
     *
     * @param userId
     * @param refresh
     * @param type
     * @return
     */
    UserInfo getCacheUserInfoByUid(long userId, boolean refresh, int type);

    /**
     * 通过uid查询缓存（内存）的当前登录用户的详细信息，同步接口，返回可能NULL
     *
     * @return
     */
    @Nullable
    UserInfo getCacheLoginUserInfo();


    /**
     * 信息不全登录后调
     *
     * @param userInfo
     */
    void requestCompleteUserInfo(UserInfo userInfo, String shareChannel, String shareUid, String roomUid);

    /**
     * 修改个人信息
     * 如果不需修改字段，传空或者""
     */
    void requestUpdateUserInfo(UserInfo userInfo);

    /**
     * 上传照片
     *
     * @param url
     */
    void requestAddPhoto(String url);

    /**
     * 删除照片
     *
     * @param pid
     */
    void requestDeletePhoto(long pid);

    /**
     * 排序类型,1收到的礼物数量多少排序,2礼物价格高低排序
     *
     * @param uid
     */
    void requestUserGiftWall(long uid, int orderType);

    /**
     * 个人相册排序
     *
     * @param pidList
     */
    void sortPhoto(ArrayList<Long> pidList);


}
