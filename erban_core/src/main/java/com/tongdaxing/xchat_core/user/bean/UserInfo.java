package com.tongdaxing.xchat_core.user.bean;


import com.tongdaxing.xchat_core.room.bean.CreateRoomBean;
import com.tongdaxing.xchat_core.utils.StarUtils;

import java.io.Serializable;
import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by chenran on 2017/3/8.
 */
public class UserInfo extends RealmObject implements Serializable {

    @PrimaryKey
    private long uid;
    //耳伴号
    private long erbanNo;
    // 昵称
    private String nick;
    //头像
    private String avatar;
    //性别 1:男 2：女 0 ：未知
    private int gender;
    //生日
    private long birth;
    //生日日期格式不存数据库
    private String birthStr;
    //签名
    private String signture;
    //声音展示文件
    private String userVoice;
    //声音时间
    private int voiceDura = -1;
    //关注数
    private long followNum;
    //粉丝数
    private long fansNum;
    //人气值
    private long fortune;
    //1普通账号，2官方账号，3机器账号
    private int defUser;
    //地区
    private String region;
    //城市
    private String city;
    //个人简介
    private String userDesc;
    //个人相册
    private RealmList<UserPhoto> privatePhoto;
    //财富等级
    private int experLevel;
    //魅力等级
    private int charmLevel;
    //原有业务逻辑协议：如果用户没有绑定手机，那么后台返回的phone字段就是erbanNo的内容
    private String phone;

    private String carUrl;

    private String carName;

    private String headwearUrl;

    private boolean hasVggPic;

    private boolean isBindPhone;

    private long createTime;

    private GuideStatus guideState;

    /**
     * 甜甜军团标识 1-标识为甜甜军团身份
     */
    private int externalAdmin;

    /**
     * 是否被封禁标识
     */
    private int isBlock;
    /**
     * 是否已经绑定qq
     */
    private boolean hasQQ;

    /**
     * 是否已经绑定微信
     */
    private boolean hasWx;

    /**
     * 是否被禁言标识
     */
    private int isMute;

    //修改过性别几次
    private int editGenderCount;

    /**
     * 是否开启了仅接收好友消息开关
     */
    private int messageRestriction = -1;

    private int inNearby = -1;

    private int messageRingRestriction = -1;

    //实名认证状态 : true--通过；false--不通过
    private boolean realNameAudit = false;

    private UserInfo loverUser;

    /**
     * 是否已经设置登录密码
     */
    private boolean isPassword;

    /**
     * 是否已经设置提现密码
     */
    private boolean isPayPassword;

    //勋章
    private String aliasName = "";
    private int aliasId = -1;
    private String aliasExpireTime = "";

    private RealmList<BosonFriendEnitity> bestFriendList;

    /**
     * 房间公屏-动态称号
     */
    private String alias;
    /**
     * 整型，贵族等级级别，0代表没有贵族等级，1-7代表相应的贵族级别
     */
    private int vipId;
    /**
     * 字符串类型，贵族级别描述，如“骑士”
     */
    private String vipName;
    /**
     * 贵族图标/勋章URL
     */
    private String vipIcon;

    private String vipMedal;
    /**
     * 整型，当前贵族有效期，以天为单位
     */
    private int vipDate;
    /**
     * 进房隐身开关，0关1开,更新接口传参时，以0/1为值传递
     */
    private boolean isInvisible = false;
    /**
     * 隐身模式的UID
     */
    private long invisibleUid;
    /**
     * 贵族专属聊天气泡url
     */
    private String vipBubble;

    /**
     * 声鉴卡--音色
     */
    private String timbre;
    private long admireNum;

    //我的声音背景图
    private String background;

    //-1无记录  0待审核 1审核通过 2审核不通过
    private int backgroundStatus;

    //指定用户显示单人直播房间开关
    private int isCreateSingleLive;
    //指定用户显示新的互动多人房开关 0 无权限  1.有权限
    private int multiRoom;

    private int growingLevel;

    //创建房间列表信息
    private RealmList<CreateRoomBean> powerRoom;

    public boolean isHasVggPic() {
        return hasVggPic;
    }

    public void setHasVggPic(boolean hasVggPic) {
        this.hasVggPic = hasVggPic;
    }

    public boolean isBindPhone() {
        return isBindPhone;
    }

    public void setBindPhone(boolean bindPhone) {
        isBindPhone = bindPhone;
    }


    public boolean isExternalAdmin() {
        return externalAdmin == 1;
    }

    public void setExternalAdmin(int externalAdmin) {
        this.externalAdmin = externalAdmin;
    }


    public boolean isBlock() {
        return isBlock == 1;
    }

    public void setBlock(int block) {
        isBlock = block;
    }

    public boolean isMute() {
        return isMute == 1;
    }

    public void setMute(int mute) {
        isMute = mute;
    }

    public boolean isHasQQ() {
        return hasQQ;
    }

    public void setHasQQ(boolean hasQQ) {
        this.hasQQ = hasQQ;
    }

    public boolean isHasWx() {
        return hasWx;
    }

    public void setHasWx(boolean hasWx) {
        this.hasWx = hasWx;
    }

    public int getInNearby() {
        return inNearby;
    }

    public void setInNearby(int inNearby) {
        this.inNearby = inNearby;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getMessageRingRestriction() {
        return messageRingRestriction;
    }

    public void setMessageRingRestriction(int messageRingRestriction) {
        this.messageRingRestriction = messageRingRestriction;
    }
    public boolean isRealNameAudit() {
        return realNameAudit;
    }

    public void setRealNameAudit(boolean realNameAudit) {
        this.realNameAudit = realNameAudit;
    }


    public UserInfo getLoverUser() {
        return loverUser;
    }

    public void setLoverUser(UserInfo loverUser) {
        this.loverUser = loverUser;
    }


    public RealmList<BosonFriendEnitity> getBestFriendList() {
        return bestFriendList;
    }

    public void setBestFriendList(RealmList<BosonFriendEnitity> bestFriendList) {
        this.bestFriendList = bestFriendList;
    }

    public int getAliasId() {
        return aliasId;
    }

    public void setAliasId(int aliasId) {
        this.aliasId = aliasId;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getAliasExpireTime() {
        return aliasExpireTime;
    }

    public void setAliasExpireTime(String aliasExpireTime) {
        this.aliasExpireTime = aliasExpireTime;
    }

    public int getEditGenderCount() {
        return editGenderCount;
    }

    public void setEditGenderCount(int editGenderCount) {
        this.editGenderCount = editGenderCount;
    }

    public int getMessageRestriction() {
        return messageRestriction;
    }

    public void setMessageRestriction(int messageRestriction) {
        this.messageRestriction = messageRestriction;
    }

    public boolean isPassword() {
        return isPassword;
    }

    public void setPassword(boolean password) {
        isPassword = password;
    }

    public boolean isPayPassword() {
        return isPayPassword;
    }

    public void setPayPassword(boolean payPassword) {
        isPayPassword = payPassword;
    }

    public void setGuideState(GuideStatus guideState) {
        this.guideState = guideState;
    }

    public GuideStatus getGuideState() {
        return guideState;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public String getHeadwearUrl() {
        return headwearUrl;
    }

    public void setHeadwearUrl(String headwearUrl) {
        this.headwearUrl = headwearUrl;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getCarUrl() {
        return carUrl;
    }

    public void setCarUrl(String carUrl) {
        this.carUrl = carUrl;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getExperLevel() {
        return experLevel;
    }

    public void setExperLevel(int experLevel) {
        this.experLevel = experLevel;
    }

    public int getCharmLevel() {
        return charmLevel;
    }

    public void setCharmLevel(int charmLevel) {
        this.charmLevel = charmLevel;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public long getErbanNo() {
        return erbanNo;
    }

    public void setErbanNo(long erbanNo) {
        this.erbanNo = erbanNo;
    }

    public String getNick() {
        if (null != nick && !nick.trim().isEmpty()) {
            //trim只是去除字符串首尾的空格
            return nick.replaceAll("\n", "").replace(" ", "");
        }
        return nick;
    }

    public void setNick(String nick) {
        if (null != nick && !nick.trim().isEmpty()) {
            this.nick = nick.replaceAll("\n", "").replace(" ", "");
        } else {
            this.nick = nick;
        }
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public long getBirth() {
        return birth;
    }

    public String getBirthStr() {
        return birthStr;
    }

    public void setBirthStr(String birthStr) {
        this.birthStr = birthStr;
    }

    public void setBirth(long birth) {
        this.birth = birth;
    }

    public String getStarStr() {
        return StarUtils.getConstellation(new Date(Long.valueOf(birth) / 1000));
    }


    public String getSignture() {
        return signture;
    }

    public void setSignture(String signture) {
        this.signture = signture;
    }

    public String getUserVoice() {
        return userVoice;
    }

    public void setUserVoice(String userVoice) {
        this.userVoice = userVoice;
    }

    public int getVoiceDura() {
        return voiceDura;
    }

    public void setVoiceDura(int voiceDura) {
        this.voiceDura = voiceDura;
    }

    public long getFollowNum() {
        return followNum;
    }

    public void setFollowNum(long followNum) {
        this.followNum = followNum;
    }

    public long getFansNum() {
        return fansNum;
    }

    public void setFansNum(long fansNum) {
        this.fansNum = fansNum;
    }

    public long getFortune() {
        return fortune;
    }

    public void setFortune(long fortune) {
        this.fortune = fortune;
    }

    public int getDefUser() {
        return defUser;
    }

    public void setDefUser(int defUser) {
        this.defUser = defUser;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getUserDesc() {
        return userDesc;
    }

    public void setUserDesc(String userDesc) {
        this.userDesc = userDesc;
    }

    public RealmList<UserPhoto> getPrivatePhoto() {
        return privatePhoto;
    }

    public void setPrivatePhoto(RealmList<UserPhoto> privatePhoto) {
        this.privatePhoto = privatePhoto;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public int isCreateSingleLive() {
        return isCreateSingleLive;
    }


    public int getGrowingLevel() {
        return growingLevel;
    }

    public void setGrowingLevel(int growingLevel) {
        this.growingLevel = growingLevel;
    }

    private RealmList<MedalBean> wearList;

    public RealmList<MedalBean> getWearList() {
        return wearList;
    }

    public void setWearList(RealmList<MedalBean> wearList) {
        this.wearList = wearList;
    }

    public int getBackgroundStatus() {
        return backgroundStatus;
    }

    public void setBackgroundStatus(int backgroundStatus) {
        this.backgroundStatus = backgroundStatus;
    }

    public RealmList<CreateRoomBean> getPowerRoom() {
        return powerRoom;
    }

    public void setPowerRoom(RealmList<CreateRoomBean> powerRoom) {
        this.powerRoom = powerRoom;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "uid=" + uid +
                ", erbanNo=" + erbanNo +
                ", nick='" + nick + '\'' +
                ", avatar='" + avatar + '\'' +
                ", gender=" + gender +
                ", birth=" + birth +
                ", birthStr='" + birthStr + '\'' +
                ", signture='" + signture + '\'' +
                ", userVoice='" + userVoice + '\'' +
                ", voiceDura=" + voiceDura +
                ", followNum=" + followNum +
                ", fansNum=" + fansNum +
                ", fortune=" + fortune +
                ", defUser=" + defUser +
                ", region='" + region + '\'' +
                ", city='" + city + '\'' +
                ", userDesc='" + userDesc + '\'' +
                ", privatePhoto=" + privatePhoto +
                ", experLevel=" + experLevel +
                ", charmLevel=" + charmLevel +
                ", phone='" + phone + '\'' +
                ", carUrl='" + carUrl + '\'' +
                ", carName='" + carName + '\'' +
                ", headwearUrl='" + headwearUrl + '\'' +
                ", hasVggPic=" + hasVggPic +
                ", isBindPhone=" + isBindPhone +
                ", createTime=" + createTime +
                ", guideState=" + guideState +
                ", externalAdmin=" + externalAdmin +
                ", isBlock=" + isBlock +
                ", hasQQ=" + hasQQ +
                ", hasWx=" + hasWx +
                ", isMute=" + isMute +
                ", editGenderCount=" + editGenderCount +
                ", messageRestriction=" + messageRestriction +
                ", inNearby=" + inNearby +
                ", messageRingRestriction=" + messageRingRestriction +
                ", realNameAudit=" + realNameAudit +
                ", loverUser=" + loverUser +
                ", isPassword=" + isPassword +
                ", isPayPassword=" + isPayPassword +
                ", aliasName='" + aliasName + '\'' +
                ", aliasId=" + aliasId +
                ", aliasExpireTime='" + aliasExpireTime + '\'' +
                ", bestFriendList=" + bestFriendList +
                ", alias='" + alias + '\'' +
                ", vipId=" + vipId +
                ", vipName='" + vipName + '\'' +
                ", vipIcon='" + vipIcon + '\'' +
                ", vipMedal='" + vipMedal + '\'' +
                ", vipDate=" + vipDate +
                ", isInvisible=" + isInvisible +
                ", invisibleUid=" + invisibleUid +
                ", vipBubble='" + vipBubble + '\'' +
                ", timbre='" + timbre + '\'' +
                ", admireNum=" + admireNum +
                ", background='" + background + '\'' +
                ", backgroundStatus=" + backgroundStatus +
                ", isCreateSingleLive=" + isCreateSingleLive +
                ", multiRoom=" + multiRoom +
                '}';
    }

    public void setCreateSingleLive(int createSingleLive) {
        isCreateSingleLive = createSingleLive;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getTimbre() {
        return timbre;
    }

    public void setTimbre(String timbre) {
        this.timbre = timbre;
    }

    public long getAdmireNum() {
        return admireNum;
    }

    public void setAdmireNum(long admireNum) {
        this.admireNum = admireNum;
    }

    public String getVipMedal() {
        return vipMedal;
    }

    public void setVipMedal(String vipMedal) {
        this.vipMedal = vipMedal;
    }

    public int getVipId() {
        return vipId;
    }

    public void setVipId(int vipId) {
        this.vipId = vipId;
    }

    public String getVipName() {
        return vipName;
    }

    public void setVipName(String vipName) {
        this.vipName = vipName;
    }

    public String getVipIcon() {
        return vipIcon;
    }

    public void setVipIcon(String vipIcon) {
        this.vipIcon = vipIcon;
    }

    public int getVipDate() {
        return vipDate;
    }

    public void setVipDate(int vipDate) {
        this.vipDate = vipDate;
    }

    public boolean getIsInvisible() {
        return isInvisible;
    }

    public void setIsInvisible(boolean isInvisible) {
        this.isInvisible = isInvisible;
    }

    public long getInvisibleUid() {
        return invisibleUid;
    }

    public void setInvisibleUid(long invisibleUid) {
        this.invisibleUid = invisibleUid;
    }

    public String getVipBubble() {
        return vipBubble;
    }

    public void setVipBubble(String vipBubble) {
        this.vipBubble = vipBubble;
    }

    public int getMultiRoom() {
        return multiRoom;
    }

    public void setMultiRoom(int multiRoom) {
        this.multiRoom = multiRoom;
    }
}

