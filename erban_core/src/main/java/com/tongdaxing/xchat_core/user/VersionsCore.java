package com.tongdaxing.xchat_core.user;

import com.tongdaxing.erban.libcommon.coremanager.IBaseCore;
import com.tongdaxing.erban.libcommon.utils.json.Json;

/**
 * Created by Administrator on 2017/7/12 0012.
 *
 */

public interface VersionsCore extends IBaseCore {

    String getSensitiveWordData();

    void checkVersion();

    int checkKick();

    void getConfig();

    void getChannelConfigure();

    Json getConfigData();

    void requestSensitiveWord();

    int getLastRequestHomeIndex();

    boolean isRequesting();

    boolean isRequestSuccess();

    boolean getMicInListOption();

    /**
     * 是否开启声网key动态获取
     *
     * @return
     */
    boolean getDynamicKeyOption();

    /**
     * 未认证或者未绑定手机账号用户，使用前是否需要认证或者绑定
     *
     * @return
     */
    boolean needAuthBeforeUse();

    /**
     * 偶遇，控制是否需要上传录音才能进一步使用开关
     *
     * @return
     */
    boolean needUploadAudioBeforeUse();

    /**
     * 是否开启房间礼物暴击
     *
     * @return
     */
    boolean isGiftComboSwitchOpen();
}
