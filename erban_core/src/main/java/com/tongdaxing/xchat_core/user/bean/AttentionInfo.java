package com.tongdaxing.xchat_core.user.bean;

import com.tongdaxing.xchat_core.room.bean.RoomInfo;

import java.util.List;

/**
 * Created by Administrator on 2017/7/8 0008.
 */

public class AttentionInfo {

    public String nick;

    public long fansNum;

    public long uid;

    public String avatar;

    public boolean valid;

    private int operatorStatus;

    private int type;

    private int gender;

    private String title;

    private RoomInfo userInRoom;

    public List<Long> uidList;

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public long getFansNum() {
        return fansNum;
    }

    public void setFansNum(long fansNum) {
        this.fansNum = fansNum;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
    public List<Long> getUidList() {
        return uidList;
    }

    public void setUidList(List<Long> uidList) {
        this.uidList = uidList;
    }

    public int getOperatorStatus() {
        return operatorStatus;
    }

    public void setOperatorStatus(int operatorStatus) {
        this.operatorStatus = operatorStatus;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public RoomInfo getUserInRoom() {
        return userInRoom;
    }

    public void setUserInRoom(RoomInfo userInRoom) {
        this.userInRoom = userInRoom;
    }
}
