package com.tongdaxing.xchat_core.user;

import com.tongdaxing.erban.libcommon.coremanager.AbstractBaseCore;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.erban.libcommon.utils.config.SpEvent;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.result.CheckUpdataResult;
import com.tongdaxing.xchat_core.user.bean.CheckUpdataBean;

import java.util.Map;


/**
 * Created by Administrator on 2017/7/12 0012.
 */

public class VersionsCoreImpl extends AbstractBaseCore implements VersionsCore {

    private final String TAG = VersionsCoreImpl.class.getSimpleName();
    private int checkKick = 0;

    /*
     * agora_app_certificate  声网秘钥
     * agora_app_id  声网动态key-appid
     * alipay  后台转账支付宝账号(1是主账号,其他是对应的子账户)
     * android_newest_version  android最新版本
     * audio_switch  录制开关（0关1开）
     * auditing_version  审核版本
     * auth_switch  实名开关（0关1开）
     * big_wheel_switch  房间大转盘
     * chat_room_index  公聊大厅位置
     * cur_gift_version  礼物版本
     * draw_act_switch  抽奖活动开关（1开2关闭）
     * draw_detonating_switch  大转盘开启暴走模式（1开0关）
     * dynamic_key_option  动态key开关
     * face_version  表情版本
     * game_top  首页列表
     * ggl_auditing_version  甜心live审核版本
     * ggl_newest_version  甜心live最新版本
     * gift_car_master_switch  座驾特效总开关（1开其他关）
     * green_room_index  绿色厅位置
     * homerecomm_sort_type  热门推荐排序方式
     * ios_limit_amount  内购限额
     * ios_limit_num  内购充值次数
     * iOS_newest_version  ios最新版本
     * is_exchange_awards  换金币抽奖开关（1开0关）
     * kick_waiting  踢出房间重进时间
     * lottery_box_big_gift  宝箱最大礼物名称
     * lottery_box_option  宝箱开关(1开0关)
     * lottery_box_option_level  宝箱等级
     * message_restriction_option  消息限制（0关1开）
     * mic_in_list_option  排麦开关（1开0关）
     * more_than_rmb  当天充值超过金额
     * newest_version  当前最新版本
     * new_year_open  必中甜字开关（1开0关）
     * pay_channel  支付渠道（1是pingxx，2是汇聚）
     * pea_draw_day_gold_max  甜豆抽奖每天金币限额
     * pic  闪屏图片
     * pingxx_wx  微信账号(1主账号)
     * pingxx_wx_option  微信切换开关(1开)
     * red_packet_switch  房间红包开关
     * sensitive_word  广播|公屏|私聊（逗号隔开）
     * show_limit  陪玩分类等级限制
     * sign_get_gold_min_level  签到兑换获得金币的最小等级
     * sign_in_card_gold  金卡ID
     * sign_in_card_white  白卡ID
     * splash_link  跳转链接
     * splash_pict  闪屏图片
     * splash_type  跳转类型， 1跳app页面，2跳聊天室，3跳h5页面
     * tarot_card_level  塔罗牌等级
     * timestamps  时间戳
     * tmxq_auditing_version  甜蜜星球审核版本
     * tmxq_newest_version  甜蜜星球最新版本
     * tmxq_pay_channel  甜蜜星球支付开关（0：微信支付，1：汇聚渠道）
     * ttq_auditing_version  甜甜圈审核版本
     * ttq_newest_version  甜甜圈最新版本
     * two_week_end  两周排名结束时间
     * two_week_start  两周排名开始时间
     * universal_card_max_num  万能卡的周期最大数量
     * universal_card_probability  万能卡的获得概率-- 0 - 100 整数
     * warning_system_send_day  限制注册天数
     * warning_system_send_num  限制消费金额
     * antiSpam 网易云盾开启发垃圾规则的等级限制
     * giftComboSwitch 新房间，礼物暴击，开关 0-关闭，1-开启
     */
    private Json configdata;
    private String sensitiveWordData;
    private int lastRequestHomeIndex = 0;
    private boolean isRequesting = false;
    private boolean isRequestSuccess = false;

    public VersionsCoreImpl() {
        String configStr = (String) SpUtils.get(getContext(), SpEvent.config_key, "");
        configdata = Json.parse(configStr);
        LogUtils.d(TAG, "VersionsCoreImpl-configStr:" + configStr);
    }

    @Override
    public String getSensitiveWordData() {
        return sensitiveWordData;
    }

    @Override
    public void checkVersion() {

        OkHttpManager.getInstance().getRequest(UriProvider.checkUpdate(),
                CommonParamUtil.getDefaultParam(), new OkHttpManager.MyCallBack<CheckUpdataResult>() {

                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                        notifyClients(AttentionCoreClient.class, VersionsCoreClient.METHOD_GET_VERSION_ERROR, e.getMessage());
                    }

                    @Override
                    public void onResponse(CheckUpdataResult response) {
                        if (null != response && response.isSuccess()) {
                            LogUtils.d(TAG, "checkVersion-response.data:" + response.getData());
                            CheckUpdataBean data = response.getData();
                            notifyClients(VersionsCoreClient.class, VersionsCoreClient.METHOD_VERSION_UPDATA_DIALOG, data);
                            if (data != null) {
                                checkKick = data.isKickWaiting();
                            }
                        }
                    }
                });
    }

    @Override
    public int checkKick() {
        return checkKick;
    }

    @Override
    public void getConfig() {
        LogUtils.d(TAG, "getConfig");
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        OkHttpManager.getInstance().getRequest(UriProvider.getConfigUrl(), requestParam, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Json response) {
                LogUtils.d(TAG, "VersionsCoreImpl-onResponse response:" + response);
                if (response.num("code") == 200) {
                    Json data = response.json_ok("data");
//                    if (data.num("timestamps") > configdata.num("timestamps")) {
                    configdata = data;
                    SpUtils.put(getContext(), SpEvent.config_key, data + "");
//                    }
                }
            }
        });
    }

    @Override
    public int getLastRequestHomeIndex() {
        return lastRequestHomeIndex;
    }

    @Override
    public boolean isRequesting() {
        return isRequesting;
    }

    @Override
    public boolean isRequestSuccess() {
        return isRequestSuccess;
    }

    @Override
    public void getChannelConfigure() {
        if (isRequesting) {
            return;
        }
        isRequestSuccess = false;
        isRequesting = true;
        Map<String, String> param = OkHttpManager.getDefaultParam();
        param.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        OkHttpManager.getInstance().getRequest(UriProvider.getChannelConfigure(), param, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                isRequesting = false;
                isRequestSuccess = false;
                notifyClients(VersionsCoreClient.class, VersionsCoreClient.onGetChannelConfig, false, e.getMessage(), lastRequestHomeIndex);
            }

            @Override
            public void onResponse(Json response) {
                LogUtils.d(TAG, "getChannelConfigure-->onResponse-response:" + response);
                isRequesting = false;
                isRequestSuccess = response.num("code") == 200;
                if (isRequestSuccess) {
                    Json json = response.json_ok("data");
                    lastRequestHomeIndex = json.num("homeIndex");
                }
                notifyClients(VersionsCoreClient.class, VersionsCoreClient.onGetChannelConfig, true, response.str("message"), lastRequestHomeIndex);
            }
        });
    }

    @Override
    public Json getConfigData() {
        return configdata;
    }

    @Override
    public boolean getMicInListOption() {
        return null != configdata && configdata.num("micInListOption") == 1;
    }

    @Override
    public boolean getDynamicKeyOption() {
        return null != configdata && configdata.num("dynamicKeyOption") == 1;
    }

    /**
     * 未认证或者未绑定手机账号用户，使用前是否需要认证或者绑定
     *
     * @return
     */
    @Override
    public boolean needAuthBeforeUse() {
        return null != configdata && configdata.num("authSwitch") == 1;
    }

    /**
     * 控制 上麦录音是否推流到旁路cdn 开关
     *
     * @return
     */
    @Override
    public boolean needUploadAudioBeforeUse() {
        return null != configdata && configdata.num("audioSwitch") == 1;
    }

    @Override
    public void requestSensitiveWord() {

        Map<String, String> params = CommonParamUtil.getDefaultParam();
        OkHttpManager.getInstance().getRequest(UriProvider.getSensitiveWord(), params, new OkHttpManager.MyCallBack<Json>() {

            @Override
            public void onError(Exception e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Json response) {
                if (response.num("code") == 200) {
                    String data = response.str("data");
                    sensitiveWordData = data;
                }
            }
        });
    }

    @Override
    public boolean isGiftComboSwitchOpen() {
        return null != configdata && configdata.num("giftComboSwitch") == 1;
    }
}
