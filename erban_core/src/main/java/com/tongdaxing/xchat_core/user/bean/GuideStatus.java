package com.tongdaxing.xchat_core.user.bean;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * 1关注2照片3麦序4礼物5偶遇6游戏大厅{包含了两张}7拍卖房引导图(包含两张)
 * {"1":"0","2":"0","3":"0","4":"0","5":"0","6":"0","7":"0"}
 */
public class GuideStatus extends RealmObject implements Serializable {

    private String type1;

    private String type2;

    private String type3;

    private String type4;

    private String type5;

    private String type6;

    private String type7;

    public String getType7() {
        return type7;
    }

    public void setType7(String type7) {
        this.type7 = type7;
    }

    public String getType6() {
        return type6;
    }

    public void setType6(String type6) {
        this.type6 = type6;
    }

    @Override
    public String toString() {
        return "GuideStatus{" +
                "type1='" + type1 + '\'' +
                ", type2='" + type2 + '\'' +
                ", type3='" + type3 + '\'' +
                ", type4='" + type4 + '\'' +
                ", type5='" + type5 + '\'' +
                ", type6='" + type6 + '\'' +
                ", type7='" + type7 + '\'' +
                '}';
    }

    public void setType1(String type1) {
        this.type1 = type1;
    }

    public String getType1() {
        return type1;
    }

    public void setType2(String type2) {
        this.type2 = type2;
    }

    public String getType2() {
        return type2;
    }

    public void setType3(String type3) {
        this.type3 = type3;
    }

    public String getType3() {
        return type3;
    }

    public void setType4(String type4) {
        this.type4 = type4;
    }

    public String getType4() {
        return type4;
    }

    public String getType5() {
        return type5;
    }

    public void setType5(String type5) {
        this.type5 = type5;
    }

}
