package com.tongdaxing.xchat_core.user.bean;

import java.io.Serializable;

import io.realm.RealmObject;

public class MedalBean extends RealmObject implements Serializable {
    private String alias;//": "https://img.pinjin88.com/xunzhang@3x.png",
    private long aliasExpireTime;//": 1562315098723,
    private int aliasId;//": 2,
    private String aliasName;//": "冠名"

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public long getAliasExpireTime() {
        return aliasExpireTime;
    }

    public void setAliasExpireTime(long aliasExpireTime) {
        this.aliasExpireTime = aliasExpireTime;
    }

    public int getAliasId() {
        return aliasId;
    }

    public void setAliasId(int aliasId) {
        this.aliasId = aliasId;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }
}
