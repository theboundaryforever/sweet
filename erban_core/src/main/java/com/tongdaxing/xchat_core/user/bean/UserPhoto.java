package com.tongdaxing.xchat_core.user.bean;

import android.text.TextUtils;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by chenran on 2017/7/25.
 */

public class UserPhoto extends RealmObject implements Serializable {

    @PrimaryKey
    private long pid;
    //图片地址
    private String photoUrl;

    //0待审核  1审核通过 2审核不通过
    private int photoStatus;

    public int getPhotoStatus() {
        return photoStatus;
    }

    public void setPhotoStatus(int photoStatus) {
        this.photoStatus = photoStatus;
    }

    public long getPid() {
        return pid;
    }

    public void setPid(long pid) {
        this.pid = pid;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public UserPhoto(long pid, String photoUrl) {
        this.pid = pid;
        this.photoUrl = photoUrl;
    }

    public UserPhoto() {

    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof UserPhoto)) {
            return false;
        }
        boolean isSameObj = false;
        UserPhoto other = (UserPhoto) obj;
        if (!TextUtils.isEmpty(other.getPhotoUrl()) && !TextUtils.isEmpty(this.getPhotoUrl())) {
            isSameObj = other.getPhotoUrl().equals(this.getPhotoUrl());
        } else if (other.pid != 0 && other.pid == this.pid) {
            isSameObj = true;
        }
        return isSameObj;
    }
}
