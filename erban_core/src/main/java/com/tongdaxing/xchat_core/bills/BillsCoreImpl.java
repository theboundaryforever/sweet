package com.tongdaxing.xchat_core.bills;

import com.tongdaxing.erban.libcommon.coremanager.AbstractBaseCore;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.result.ChargeResult;
import com.tongdaxing.xchat_core.result.ExpendResult;
import com.tongdaxing.xchat_core.result.IncomedResult;
import com.tongdaxing.xchat_core.result.RedBagResult;

import java.util.Map;

/**
 * Created by Seven on 2017/9/9.
 */

public class BillsCoreImpl extends AbstractBaseCore implements IBillsCore {
    private static final String TAG = "BillsCoreImpl";

    /**
     * 账单获取情况
     *
     * @param pageNo
     * @param pageSize
     * @param time
     * @param type     1：礼物支出记录 2：礼物收入记录 3：密聊记录 4：充值记录 5：提现记录
     */
    private void getBillRecode(int pageNo, int pageSize, long time, final int type) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("pageNo", String.valueOf(pageNo));
        param.put("pageSize", String.valueOf(pageSize));
        param.put("date", String.valueOf(time));
        param.put("type", String.valueOf(type));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());

        OkHttpManager.MyCallBack myCallBack = null;
        if (1 == type) {
            myCallBack = new OkHttpManager.MyCallBack<ExpendResult>() {
                @Override
                public void onError(Exception e) {
                    notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_EXPEND_BILLS_ERROR, e.getMessage());
                }

                @Override
                public void onResponse(ExpendResult response) {
                    if (response != null) {
                        if (response.isSuccess()) {
                            notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_EXPEND_BILLS, response.getData());
                        } else {
                            notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_EXPEND_BILLS_ERROR, response.getMessage());
                        }
                    }
                }
            };
        } else if (2 == type) {
            myCallBack = new OkHttpManager.MyCallBack<IncomedResult>() {
                @Override
                public void onError(Exception e) {
                    notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_INCOME_BILLS_ERROR, e.getMessage());
                }

                @Override
                public void onResponse(IncomedResult response) {
                    if (response != null) {
                        if (response.isSuccess()) {
                            notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_INCOME_BILLS, response.getData());
                        } else {
                            notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_INCOME_BILLS_ERROR, response.getMessage());
                        }
                    }
                }
            };
        } else if (3 == type) {
            myCallBack = new OkHttpManager.MyCallBack<IncomedResult>() {
                @Override
                public void onError(Exception e) {
                    notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_ORDER_INCOME_BILLS_ERROR, e.getMessage());
                }

                @Override
                public void onResponse(IncomedResult response) {
                    if (response != null) {
                        if (response.isSuccess()) {
                            notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_ORDER_INCOME_BILLS, response.getData());
                        } else {
                            notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_ORDER_INCOME_BILLS_ERROR, response.getMessage());
                        }
                    }
                }
            };
        } else if (4 == type) {
            myCallBack = new OkHttpManager.MyCallBack<ChargeResult>() {
                @Override
                public void onError(Exception e) {
                    notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_CHARGE_BILLS_ERROR, e.getMessage());
                }

                @Override
                public void onResponse(ChargeResult response) {
                    if (response != null) {
                        if (response.isSuccess()) {
                            notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_CHARGE_BILLS, response.getData());
                        } else {
                            notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_CHARGE_BILLS_ERROR, response.getMessage());
                        }
                    }
                }
            };
        } else if (5 == type) {
            myCallBack = new OkHttpManager.MyCallBack<IncomedResult>() {
                @Override
                public void onError(Exception e) {
                    notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_WIHTDRAW_BILLS_ERROR, e.getMessage());
                }

                @Override
                public void onResponse(IncomedResult response) {
                    if (response != null) {
                        if (response.isSuccess()) {
                            notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_WIHTDRAW_BILLS, response.getData());
                        } else {
                            notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_WIHTDRAW_BILLS_ERROR, response.getMessage());
                        }
                    }
                }
            };
        }

        if (null == myCallBack) {
            return;
        }

        OkHttpManager.getInstance().getRequest(UriProvider.getBillRecord(), param, myCallBack);
    }

    //礼物收入2
    @Override
    public void getGiftIncomeBills(int pageNo, int pageSize, long time) {
        getBillRecode(pageNo, pageSize, time, 2);
    }

    //礼物支出1
    @Override
    public void getGiftExpendBills(int pageNo, int pageSize, long time) {
        getBillRecode(pageNo, pageSize, time, 1);
    }

    //提现记录5
    @Override
    public void getWithdrawBills(int pageNo, int pageSize, long time) {
        getBillRecode(pageNo, pageSize, time, 5);
    }

    @Override
    public void getWithdrawRedBills(int pageNo, int pageSize, long time) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("pageNo", String.valueOf(pageNo));
        param.put("pageSize", String.valueOf(pageSize));
        param.put("date", String.valueOf(time));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());

        OkHttpManager.getInstance().getRequest(UriProvider.getPacketRecordDeposit(), param, new OkHttpManager.MyCallBack<RedBagResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_WITHDRAW_RED_BILLS_ERROR, e.getMessage());
            }

            @Override
            public void onResponse(RedBagResult response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_WITHDRAW_RED_BILLS, response.getData());
                    } else {
                        notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_WITHDRAW_RED_BILLS_ERROR, response.getMessage());
                    }
                }
            }
        });
    }

    //密聊记录：3
    @Override
    public void getChatBills(int pageNo, int pageSize, long time) {
        getBillRecode(pageNo, pageSize, time, 3);
    }


    //充值记录4
    @Override
    public void getChargeBills(int pageNo, int pageSize, long time) {
        getBillRecode(pageNo, pageSize, time, 4);
    }

    @Override
    public void getRedBagBills(int pageNo, int pageSize, long time) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("pageNo", String.valueOf(pageNo));
        param.put("pageSize", String.valueOf(pageSize));
        param.put("date", String.valueOf(time));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());

        OkHttpManager.getInstance().getRequest(UriProvider.getPacketRecord(), param, new OkHttpManager.MyCallBack<RedBagResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_RED_BAG_BILLS_ERROR, e.getMessage());
            }

            @Override
            public void onResponse(RedBagResult response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_RED_BAG_BILLS, response.getData());
                    } else {
                        notifyClients(IBillsCoreClient.class, IBillsCoreClient.METHOD_GET_RED_BAG_BILLS_ERROR, response.getMessage());
                    }
                }
            }
        });
    }
}
