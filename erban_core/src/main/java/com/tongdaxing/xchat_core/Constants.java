package com.tongdaxing.xchat_core;

import java.io.File;

/**
 * <p> 常量集合 </p>
 * Created by Administrator on 2017/11/9.
 */
public class Constants {
    public static final String ERBAN_DIR_NAME = "com.yuhuankj.tmxq";

    public static final String UMENG_KEY = "5cbdae393fc19515ec000ec8";
    //云信
    public static final String nimAppKey = "e655919f13b02388594d43451b801560";
    public static final String nimAppSecret = "1d6a1808087c";

    //bugly crash上报的appkey
    public static final String CRASH_REPORT_KEY = "25ab815fbc";
    public static final String CRASH_REPORT_KEY_DEBUG = "e5ed0920e0";
    /**
     * 百度统计
     */
    public static final String BAIDU_APPKEY = "41359bf78f";

    public static final String LOG_DIR = ERBAN_DIR_NAME + File.separator + "logs";
    public static final String CONFIG_DIR = ERBAN_DIR_NAME + File.separator + "config";
    public static final String VOICE_DIR = ERBAN_DIR_NAME + File.separator + "voice";
    public static final String CACHE_DIR = ERBAN_DIR_NAME + File.separator + "cache";
    public static final String HTTP_CACHE_DIR = ERBAN_DIR_NAME + File.separator + "http";
    public static final String IMAGE_CACHE_DIR = ERBAN_DIR_NAME + File.separator + "image";


    public static final String KEY_MAIN_POSITION = "key_main_position";

    public static final int RESULT_OK = 200;

    public static final int PAGE_START = 1;
    public static final int PAGE_SIZE = 10;
    public static final int PAGE_SIZE_20 = 20;
    public static final int PAGE_HOME_HOT_SIZE = 12;
    public static final int BILL_PAGE_SIZE = 50;
    /**
     * 一页房间人数
     */
    protected static final int ROOM_MEMBER_SIZE = 50;

    public static final String SHOP_TAG_NICK = "SHOP_TAG_NICK";
    public static final String SHOP_TAG_UID = "SHOP_TAG_UID";
    public static final String SHOP_TAB_INDEX = "SHOP_TAB_INDEX";
    public static final String USER_INFO_LIST_UID = "USER_INFO_LIST_UID";
    public static final String USER_INFO_LIST_TYPE = "USER_INFO_LIST_TYPE";
    public static final String HOME_TAB_INFO = "home_tab_info";
    public static final String KEY_USER_INFO = "key_user_info";
    public static final String KEY_HOME_DATA = "key_home_data";
    public static final String KEY_HOME_HOT_LIST = "key_home_hot_list";
    public static final String KEY_HOME_NO_HOT_LIST = "key_home_no_hot_list";

    public static final int FAN_MAIN_PAGE_TYPE = 100;
    public static final int FAN_NO_MAIN_PAGE_TYPE = 101;
    public static final String KEY_PAGE_TYPE = "page_type";
    public static final String KEY_MAIN_TAB_LIST = "main_tab_list";
    public static final String KEY_SEARCH_TAB_LIST = "search_tab_list";

    public static final String KEY_POSITION = "position";

    public static final String CHARGE_WX = "wx";
    public static final String CHARGE_ALIPAY = "alipay";
    public static final String CHARGE_WX_JOINPAY = "WEIXIN_APP";
    public static final String CHARGE_COINS = "gold";

    public static final int PAGE_TYPE_AV_ROOM_ACTIVITY = 100;
    public static final int PAGE_TYPE_USER_INFO_ACTIVITY = 101;
    public static final java.lang.String KEY_ROOM_IS_SHOW_ONLINE = "is_show_online";
    public static final String KEY_ROOM_INFO = "key_room_info";

    /**
     * 房间相关Key设置
     */
    public static final String ROOM_UPDATE_KEY_POSTION = "micPosition";
    public static final String ROOM_UPDATE_KEY_UID = "micUid";
    public static final String USER_GENDER = "gender";

    public static final String KEY_CHAT_ROOM_INFO_ROOM = "roomInfo";
    public static final String KEY_CHAT_ROOM_INFO_MIC = "micQueue";

    public static final String ROOM_UID = "ROOM_UID";
    public static final String ROOM_TYPE = "ROOM_TYPE";
    public static final String ROOM_FROM_MIC = "is_from_mic";
    public static final String ROOM_MEMBER_ID = "USER_ID";

    public static final String ROOM_FOLLOW_UID = "ROOM_FOLLOW_UID";
    public static final String TYPE = "TYPE";

    //大礼物
    public static final int SUPER_GIFT_SIZE = 999;

    public static final String ALIAS_ICON_URL = "alias_icon_url";
    public static final String MEDAL_URLS = "MEDAL_URLS";

    //广播广场消息体专用字段
    public static final String USER_EXPER_LEVEL = "experLevel";
    public static final String USER_NICK = "nick";
    public static final String USER_AGE= "age";
    //进房提示昵称字段
    public static final String USER_NICK_IN_ROOM = "user_nick";
    public static final String USER_AVATAR = "avatar";
    public static final String USER_CAR = "user_car";
    public static final String USER_CAR_NAME = "user_car_name";
    public static final String ROOM_ID = "room_id";
    public static final String USER_CHARM_LEVEL = "charmLevel";
    public static final String USER_UID = "uid";
    public static final String USER_NOBLE_BUBBLE = "vipBubble";
    public static final String USER_NOBLE_ICON = "vipIcon";
    public static final String USER_NOBLE_MEDAL = "vipMedal";
    public static final String USER_MEDAL_ID = "vipId";
    public static final String USER_MEDAL_DATE = "vipDate";
    public static final String USER_ERBAN_NO = "erbanNo";
    public static final String PUBLIC_CHAT_ROOM = "PUBLIC_CHAT_ROOM";

    public static final String headwearUrl = "headwearUrl";
    public static final String hasVggPic = "hasVggPic";
    public static final String IS_NEW_USER = "is_new_user";
    public static final String NOBLE_INVISIABLE_ENTER_ROOM = "isInvisible";
    public static final String NOBLE_INVISIABLE_UID = "invisibleUid";
}
