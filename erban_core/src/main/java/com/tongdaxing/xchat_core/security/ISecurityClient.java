package com.tongdaxing.xchat_core.security;

import com.tongdaxing.erban.libcommon.coremanager.ICoreClient;

public interface ISecurityClient extends ICoreClient {

    public static final String METHON_ON_SECURITY_REPORT = "onSecurityReport";

    void onSecurityReport(boolean isSuccess, String msg);
}
