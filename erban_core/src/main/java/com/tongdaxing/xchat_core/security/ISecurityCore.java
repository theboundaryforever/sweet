package com.tongdaxing.xchat_core.security;

import android.content.Context;

import com.tongdaxing.erban.libcommon.coremanager.IBaseCore;

/**
 * 甜甜军团相关业务处理
 */
public interface ISecurityCore extends IBaseCore {

    /**
     * 发起安全检测
     *
     * @param context
     */
    void securityCheck(Context context);
}



