package com.tongdaxing.xchat_core.security;

import android.content.Context;

import com.juxiao.safetychecker.SafetyChecker;
import com.juxiao.safetychecker.bean.SafetyCheckResultBean;
import com.tencent.bugly.crashreport.CrashReport;
import com.tongdaxing.erban.libcommon.coremanager.AbstractBaseCore;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.json.JsonParser;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.utils.ThreadUtil;

import java.util.Map;

public class SecurityCoreImpl extends AbstractBaseCore implements ISecurityCore {

    private final String TAG = SecurityCoreImpl.class.getSimpleName();

    /**
     * 发起安全检测
     */
    @Override
    public void securityCheck(final Context context) {
        ThreadUtil.getThreadPool().execute(new Runnable() {
            @Override
            public void run() {
                final SafetyCheckResultBean checkResult = SafetyChecker.getInstance().check(context.getApplicationContext());
                LogUtils.d(TAG, "securityCheck-checkResult.checkStatus:" + checkResult.getCheckStatus());
                if (checkResult.getCheckStatus() != 0) {//不安全的，上报
                    ThreadUtil.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //尝试验证下bugly几百条刚启动10秒以内就挂的crash是怎么来的
                            CrashReport.putUserData(context, "killBySecCheck", "true");
                            reportSecurityInfo(checkResult);
                        }
                    });
                }
            }
        });
    }

    private void reportSecurityInfo(SafetyCheckResultBean checkResult) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        params.put("uid", uid + "");
        params.put("msgId", System.currentTimeMillis() + "");
        params.put("content", JsonParser.toJson(checkResult));

        OkHttpManager.getInstance().postRequest(UriProvider.getSecurityReportUrl(), params, new OkHttpManager.MyCallBack<ServiceResult<Integer>>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(ServiceResult<Integer> response) {
//                if (200 == response.getCode()) {
//                    System.exit(0);
//                }
            }
        });
    }
}
