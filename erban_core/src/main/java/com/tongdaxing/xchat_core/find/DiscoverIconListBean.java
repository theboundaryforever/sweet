package com.tongdaxing.xchat_core.find;

import java.io.Serializable;

public class DiscoverIconListBean implements Serializable {

    /**
     * activity :
     * iosActivity : InviteShareViewController
     * params :
     * pic : https://img.pinjin88.com/icon_share.png
     * title : 分享邀请
     * url :
     */

    private String activity;
    private String iosActivity;
    private String params;
    private String pic;
    private String title;
    private String url;
    private String contentUrl;
    private String backgroundUrl;

    public String getContentUrl() {
        return contentUrl;
    }

    public void setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
    }

    public String getBackgroundUrl() {
        return backgroundUrl;
    }

    public void setBackgroundUrl(String backgroundUrl) {
        this.backgroundUrl = backgroundUrl;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getIosActivity() {
        return iosActivity;
    }

    public void setIosActivity(String iosActivity) {
        this.iosActivity = iosActivity;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "DiscoverIconListBean{" +
                "activity='" + activity + '\'' +
                ", iosActivity='" + iosActivity + '\'' +
                ", params='" + params + '\'' +
                ", pic='" + pic + '\'' +
                ", title='" + title + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
