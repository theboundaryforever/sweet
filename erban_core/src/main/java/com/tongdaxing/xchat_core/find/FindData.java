package com.tongdaxing.xchat_core.find;

import java.io.Serializable;
import java.util.List;

public class FindData implements Serializable {
    public List<FindInfo> advertiseList;
    public List<DiscoverIconListBean> discoverIconList;
}
