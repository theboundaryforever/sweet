package com.tongdaxing.xchat_core.find;

import com.tongdaxing.xchat_core.room.bean.RoomInfo;

import java.io.Serializable;

/**
 * Created by huangmeng1 on 2018/1/31.
 */

public class FindInfo implements Serializable {
    /**
     * advName : 测试啊
     * advIcon : http://img.tiantianyuyin.com/FvbRJRJgufTpntjatyfL8xx1yBQ5?imageslim
     * skipType : 3
     * skipUri : www.baidu.com
     */

    private String advName;
    private String advIcon;
    private String bigIcon;
    private int skipType;
    private String skipUri;

    private int roomType = RoomInfo.ROOMTYPE_HOME_PARTY;

    public int getRoomType() {
        return roomType;
    }

    public void setRoomType(int roomType) {
        this.roomType = roomType;
    }

    public String getBigIcon() {
        return bigIcon;
    }

    public void setBigIcon(String bigIcon) {
        this.bigIcon = bigIcon;
    }

    @Override
    public String toString() {
        return "FindInfo{" +
                "advName='" + advName + '\'' +
                ", advIcon='" + advIcon + '\'' +
                ", skipType=" + skipType +
                ", skipUri='" + skipUri + '\'' +
                ", bigIcon='" + bigIcon + '\'' +
                ", roomType='" + roomType + '\'' +
                '}';
    }

    public String getAdvName() {
        return advName;
    }

    public void setAdvName(String advName) {
        this.advName = advName;
    }

    public String getAdvIcon() {
        return advIcon;
    }

    public void setAdvIcon(String advIcon) {
        this.advIcon = advIcon;
    }

    public int getSkipType() {
        return skipType;
    }

    public void setSkipType(int skipType) {
        this.skipType = skipType;
    }

    public String getSkipUri() {
        return skipUri;
    }

    public void setSkipUri(String skipUri) {
        this.skipUri = skipUri;
    }
}
