package com.tongdaxing.xchat_core.room_red_packet;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.listener.HttpCallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RoomRedPacket;
import com.tongdaxing.xchat_core.bean.RoomRedPacketReceiveResult;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;

import java.util.List;
import java.util.Map;

/**
 * 房间红包model层
 */
public class RedPacketModel extends BaseMvpModel {

    /**
     * goldNum 金币数量
     * redPacketNum  红包数量
     * roomId 房间id
     * showAll 是否全房间显示（0不显示1显示）
     * showBroadcast 是否广播显示（0不显示1显示）
     * timing 定时时间
     * type 红包类型（1手气2定时）
     * uid 当前用户UID
     */
    public void sendRedPacket(String goldNum, String redPacketNum, long roomId, int showAll, int showBroadcast, int timing, int type, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("goldNum", goldNum);
        params.put("redPacketNum", redPacketNum);
        params.put("roomId", roomId + "");
        params.put("showAll", showAll + "");
        params.put("showBroadcast", showBroadcast + "");
        params.put("timing", timing + "");
        params.put("type", type + "");
        OkHttpManager.getInstance().postRequest(UriProvider.sendRedPacket(), params, callBack);
    }

    /**
     * 获取红包记录
     *
     * @param pageNum
     * @param pageSize
     * @param callBack
     */
    public void getRedPacketHistory(int pageNum, int pageSize, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("pageNum", pageNum + "");
        params.put("pageSize", pageSize + "");
        OkHttpManager.getInstance().postRequest(UriProvider.getRedPacketHistory(), params, callBack);
    }

    /**
     * @author liaoxy
     * @Description:获取红包详情
     * @date 2019/1/17 16:18
     */
    public void getRedPacketDetail(String redPacketId, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("redPacketId", redPacketId);
        OkHttpManager.getInstance().postRequest(UriProvider.getRedPacketDetail(), params, callBack);
    }

    /**
     * @author liaoxy
     * @Description:领取红包
     * @date 2019/1/17 16:20
     */
    public void receiveRedPacket(String redPacketId, long roomId, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("redPacketId", redPacketId);
        params.put("roomId", roomId + "");
        OkHttpManager.getInstance().postRequest(UriProvider.receiveRedPacket(), params, callBack);
    }

    public void getRoomHistoryRedPacket(long roomId, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomId", roomId + "");
        OkHttpManager.getInstance().postRequest(UriProvider.getRoomHistoryRedPacket(), params, callBack);
    }


    /**
     * 获取当前房间红包信息
     * @param roomId
     * @param callBack
     */
    public void getRoomRedPackageInfo(long roomId, CallBack<List<RoomRedPacket>> callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomId", roomId + "");
        OkHttpManager.getInstance().postRequest(UriProvider.getRoomHistoryRedPacket(), params, new OkHttpManager.MyCallBack<ServiceResult<List<RoomRedPacket>>>() {
            @Override
            public void onError(Exception e) {
                if (callBack != null) {
                    callBack.onFail(OkHttpManager.DEFAULT_CODE_ERROR,e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<List<RoomRedPacket>> response) {
                if (callBack != null) {
                    if (response != null) {
                        if (response.isSuccess()) {
                            callBack.onSuccess(response.getData());
                        } else {
                            callBack.onFail(response.getCode(),response.getMessage());
                        }
                    }else {
                        callBack.onFail(OkHttpManager.DEFAULT_CODE_ERROR,"数据异常！");
                    }
                }
            }
        });
    }


    /**
     * 获取领取房间红包信息
     * @param roomId
     * @param redPacketId
     * @param callBack
     */
    public void receiveRoomRedPackage(long roomId,String redPacketId,HttpCallBack<RoomRedPacketReceiveResult> callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("redPacketId", redPacketId);
        params.put("roomId", roomId + "");
        OkHttpManager.getInstance().postRequest(UriProvider.receiveRedPacket(), params,new OkHttpManager.MyCallBack<ServiceResult<RoomRedPacketReceiveResult>>() {
            @Override
            public void onError(Exception e) {
                if (callBack != null) {
                    callBack.onFail(OkHttpManager.DEFAULT_CODE_ERROR,e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<RoomRedPacketReceiveResult> response) {
                if (callBack != null) {
                    if (response != null) {
                        if (response.isSuccess()) {
                            callBack.onSuccess(response.getData());
                        } else {
                            callBack.onFail(response.getCode(),response.getMessage(),response.getData());
                        }
                    }else {
                        callBack.onFail(OkHttpManager.DEFAULT_CODE_ERROR,"数据异常！");
                    }
                }
            }
        });
    }
}
