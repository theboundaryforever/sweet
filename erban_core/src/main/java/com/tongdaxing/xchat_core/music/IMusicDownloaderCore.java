package com.tongdaxing.xchat_core.music;

import com.tongdaxing.erban.libcommon.coremanager.IBaseCore;
import com.tongdaxing.xchat_core.music.bean.HotMusicInfo;
import com.tongdaxing.xchat_core.player.bean.LocalMusicInfo;

import java.util.List;

/**
 * Created by weihaitao on 2018/11/09.
 *
 */

public interface IMusicDownloaderCore extends IBaseCore {

    /**
     * 添加热门曲库到下载队列
     * @param hotMusicInfo
     */
    void addHotMusicToDownQueue(HotMusicInfo hotMusicInfo);

    /**
     * 删除所有的下载中队列，以及对应的缓存信息<临时文件暂时不删除>
     */
    void deleteAllDownQueueAsWellMusicInfoAsTempFile();

    /**
     * 下载的歌曲被移除播放列表
     * @param uri
     */
    void deleteHotMusicDownloadingFromPlayList(String uri);

    /**
     * 检测HotMusicInfo是否处于下载队列中
     * @param uri
     */
    boolean checkHotMusicIsDownloading(String uri);

    /**
     * 查询下载中的LocalMusicIinfo队列的方法
     * @return
     */
    List<LocalMusicInfo> queryDownloadingMusicListInPlayList();

    boolean updateHotMusicInfoToLocal(List<HotMusicInfo> hotMusicInfos);
}
