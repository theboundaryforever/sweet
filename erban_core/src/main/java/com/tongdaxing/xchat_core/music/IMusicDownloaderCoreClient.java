package com.tongdaxing.xchat_core.music;

import com.tongdaxing.erban.libcommon.coremanager.ICoreClient;
import com.tongdaxing.xchat_core.music.bean.HotMusicInfo;
import com.tongdaxing.xchat_core.player.bean.LocalMusicInfo;

/**
 * Created by weihaitao on 2018/11/09.
 */

public interface IMusicDownloaderCoreClient extends ICoreClient {

    public static final String METHOD_ON_MUSIC_DOWNLOAD_PROGRESS_UPDATE = "onHotMusicDownloadProgressUpdate";

    /**
     * 下载进度通知
     * @param hotMusicInfo
     * @param hotMusicInfo
     * @param progress 当前下载进度
     */
    void onHotMusicDownloadProgressUpdate(LocalMusicInfo localMusicInfo, HotMusicInfo hotMusicInfo, int progress);


    public static final String METHOD_ON_MUSIC_DOWNLOAD_COMPLETED = "onHotMusicDownloadCompleted";

    /**
     * 下载完成通知--提示用户歌曲已经下载完成
     * @param hotMusicInfo
     */
    void onHotMusicDownloadCompleted(HotMusicInfo hotMusicInfo);

    public static final String METHOD_ON_MUSIC_DOWNLOAD_ERROR = "onHotMusicDownloadError";

    /**
     * 下载出错通知
     * @param msg
     * @param localMusicInfo
     * @param hotMusicInfo
     */
    void onHotMusicDownloadError(String msg, LocalMusicInfo localMusicInfo, HotMusicInfo hotMusicInfo);

    public static final String METHOD_ON_MUSIC_DOWNLOAD_COMPLE_INFO_UPDATED = "onHotMusicDownloadCompleInfoUpdated";

    /**
     * 下载的歌曲本地信息更新通知
     * @param localMusicInfo 更新后的localMusicInfo
     */
    void onHotMusicDownloadCompleInfoUpdated(LocalMusicInfo localMusicInfo, HotMusicInfo hotMusicInfo);

    public static final String METHOD_ON_HOT_UPDATE_LOCAL_COMPLEED = "onHotMusicInfoUpdateToLocalCompleted";

    /**
     * 热门曲库列表已经同步本地已经下载好的mp3信息了
     */
    void onHotMusicInfoUpdateToLocalCompleted();


}
