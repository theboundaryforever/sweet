package com.tongdaxing.xchat_core.music;

import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.tongdaxing.erban.libcommon.coremanager.AbstractBaseCore;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.http.DefaultRequestProcessor;
import com.tongdaxing.erban.libcommon.http_image.http.DownloadRequest;
import com.tongdaxing.erban.libcommon.http_image.http.ProgressInfo;
import com.tongdaxing.erban.libcommon.http_image.http.ProgressListener;
import com.tongdaxing.erban.libcommon.http_image.http.Request;
import com.tongdaxing.erban.libcommon.http_image.http.RequestError;
import com.tongdaxing.erban.libcommon.http_image.http.RequestProcessor;
import com.tongdaxing.erban.libcommon.http_image.http.ResponseErrorListener;
import com.tongdaxing.erban.libcommon.http_image.http.ResponseListener;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.file.BasicFileUtils;
import com.tongdaxing.xchat_core.music.bean.HotMusicInfo;
import com.tongdaxing.xchat_core.player.IPlayerCore;
import com.tongdaxing.xchat_core.player.IPlayerDbCore;
import com.tongdaxing.xchat_core.player.bean.LocalMusicInfo;
import com.tongdaxing.xchat_core.utils.MusicFileUtil;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by weihaitao on 2018/11/09.
 */

public class MusicDownloaderCoreImpl extends AbstractBaseCore implements IMusicDownloaderCore{

    private final String TAG = MusicDownloaderCoreImpl.class.getSimpleName();

    private Map<String, HotMusicInfo> downloadHotMusicInfoQueue = new HashMap<>();
    private Map<String, LocalMusicInfo> downloadLocalMusicInfoQueue = new HashMap<>();
    //可以扩展实现单个下载请求的停止、暂停、恢复下载
    private Map<String, DownloadRequest> downloadRequestMap = new HashMap<>();

    //可以扩展实现暂停所有下载、恢复所有下载、停止所有下载
    private RequestProcessor mCommonProcessor;
    private RequestProcessor.RequestFilter allRequestFilter = new RequestProcessor.RequestFilter() {
        @Override
        public boolean apply(Request<?> request) {
//            return null != request.getTag() && !TextUtils.isEmpty(request.getTag().toString());
            return true;
        }
    };

//    private long tempLocalId = 0;

    private ExecutorService downloadExService;
    private ExecutorService updateExService;

    public MusicDownloaderCoreImpl(){
        mCommonProcessor = new DefaultRequestProcessor(100, "Music_Down_Queue");
        mCommonProcessor.start();
//        tempLocalId = 0;

        //集中发生在Android7.0及以上的华为手机（EmotionUI_5.0及以上）的OOM
        //java.lang.OutOfMemoryError: pthread_create (1040KB stack) failed: Out of memory
        //这里的线程池 不能够一次性分配太多 测试中100个在HUA WEI FRD-AL00 Android 8的系统上可行
        //参考https://www.jianshu.com/p/e574f0ffdb42
//        downloadExService = Executors.newFixedThreadPool(100);
//        updateExService = Executors.newFixedThreadPool(100);
        //Executors各类型线程池的区别参考https://www.cnblogs.com/zhujiabin/p/5404771.html
        downloadExService = Executors.newCachedThreadPool();
        updateExService = Executors.newCachedThreadPool();
    }

    /**
     * 添加热门曲库到下载队列
     *
     * @param hotMusicInfo
     */
    @Override
    public void addHotMusicToDownQueue(final HotMusicInfo hotMusicInfo) {
        LogUtils.d(TAG,"addHotMusicToDownQueue-hotMusicInfo.id:"+hotMusicInfo.getId());
        MusicFileUtil.createMusicDownloadDir();
        //生成临时数据
        final LocalMusicInfo localMusicInfo = new LocalMusicInfo();
        localMusicInfo.setLocalUri(MusicFileUtil.getMusicDownloadFilePath(hotMusicInfo.getSingName(),hotMusicInfo.getId()));
        localMusicInfo.setSongId(hotMusicInfo.getId()+"");
        localMusicInfo.setRemoteUri(hotMusicInfo.getSingUrl());
//        tempLocalId-=1;
//        localMusicInfo.setLocalId(tempLocalId);
        List<String> artistNames = new ArrayList<String>();
        artistNames.add(hotMusicInfo.getSingerName());
        localMusicInfo.setArtistName(artistNames);
        //hot列表下载则默认添加进播放列表
        localMusicInfo.setInPlayerList(true);
        localMusicInfo.setSongName(hotMusicInfo.getSingName());
        //缓存下载队列以备更新下载进度
        downloadLocalMusicInfoQueue.put(hotMusicInfo.getSingUrl(),localMusicInfo);
        downloadHotMusicInfoQueue.put(hotMusicInfo.getSingUrl(),hotMusicInfo);
        //下载请求队列管理
        DownloadRequest downloadRequest = new DownloadRequest(localMusicInfo.getRemoteUri(),
                localMusicInfo.getLocalUri(), new ResponseListener<String>() {
            @Override
            public void onResponse(String response) {
                //response:/storage/emulated/0/tiantian/music/11.mp3 也即下载完成后 response为本地文件路径
                LogUtils.d(TAG, "onResponse-remoteUrl:" + localMusicInfo.getRemoteUri() + " response:" + response);
                onMusicDownloadRespone(response,localMusicInfo,hotMusicInfo);
            }
        }, new ResponseErrorListener() {
            @Override
            public void onErrorResponse(RequestError error) {
                if(null != error){
                    error.printStackTrace();
                    LogUtils.d(TAG, "onErrorResponse-remoteUrl:" + localMusicInfo.getRemoteUri() + " error:" + error.getErrorStr());
                    onMusicDownloadError(error,localMusicInfo,hotMusicInfo);
                }
            }
        }, new ProgressListener() {
            @Override
            public void onProgress(ProgressInfo info) {
                LogUtils.d(TAG, "onProgress-remoteUrl:" + localMusicInfo.getRemoteUri() + " progress:" + info.getProgress()+" total:"+info.getTotal());
                onMusicDownloadProgress(localMusicInfo,hotMusicInfo,info);
            }
        }, true);
        downloadRequest.getHeaders().putAll(CommonParamUtil.getDefaultHeaders(getContext()));
        downloadRequestMap.put(localMusicInfo.getRemoteUri(),downloadRequest);
        downloadRequest.setTag(hotMusicInfo.getSingUrl());
        //通知播放列表界面下载对立有更新
        CoreManager.getCore(IPlayerCore.class).addMusicToPlayerList(localMusicInfo);
        mCommonProcessor.add(downloadRequest);
    }

    /**
     * 下载出错
     * @param error
     * @param localMusicInfo
     * @param hotMusicInfo
     */
    private void onMusicDownloadError(RequestError error, LocalMusicInfo localMusicInfo, HotMusicInfo hotMusicInfo){
        DownloadRequest downloadRequest = downloadRequestMap.remove(localMusicInfo.getRemoteUri());
        downloadLocalMusicInfoQueue.remove(hotMusicInfo.getSingUrl());
        downloadHotMusicInfoQueue.remove(hotMusicInfo.getSingUrl());
        CoreManager.getCore(IPlayerCore.class).deleteMusicFromPlayerList(localMusicInfo);
        if(null != downloadRequest){
            //半路下载出错保存下载进度
            downloadRequest.cancel();
        }
        if(null != error){
            //通知播放列表移除item，通知热门曲库列表更新下载状态
            notifyClients(IMusicDownloaderCoreClient.class,IMusicDownloaderCoreClient.METHOD_ON_MUSIC_DOWNLOAD_ERROR,error.getErrorStr(),localMusicInfo,hotMusicInfo);
        }
    }

    /**
     * 下载完成
     * @param respone
     * @param localMusicInfo
     * @param hotMusicInfo
     */
    private void onMusicDownloadRespone(String respone, LocalMusicInfo localMusicInfo, HotMusicInfo hotMusicInfo){
        downloadRequestMap.remove(localMusicInfo.getRemoteUri());
        downloadLocalMusicInfoQueue.remove(hotMusicInfo.getSingUrl());
        downloadHotMusicInfoQueue.remove(hotMusicInfo.getSingUrl());
        downloadExService.execute(new ScanMusicMediaInfoRunnable(localMusicInfo,hotMusicInfo, true));
    }

    private class ScanMusicMediaInfoRunnable implements Runnable {

        private LocalMusicInfo localMusicInfo;
        private HotMusicInfo hotMusicInfo;
        private boolean downloadOrUpdate = false;

        public ScanMusicMediaInfoRunnable(LocalMusicInfo localMusicInfo,HotMusicInfo hotMusicInfo,boolean downloadOrUpdate){
            this.localMusicInfo = localMusicInfo;
            this.hotMusicInfo = hotMusicInfo;
            this.downloadOrUpdate = downloadOrUpdate;
        }

        @Override
        public void run() {
            LogUtils.d(TAG,"ScanMusicMediaInfoRunnable-run path:"+localMusicInfo.getLocalUri());
            MediaScannerConnection.scanFile(getContext(), new String[]{localMusicInfo.getLocalUri()}, MusicFileUtil.mimeTypes,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        @Override
                        public void onScanCompleted(String path, Uri uri) {
                            if (uri != null) {
                                Message msg = Message.obtain();
                                LocalMusicInfo musicInfo = MusicFileUtil.getLocalSongFromUri(getContext(),uri);
                                if(downloadOrUpdate){
                                    //realm中插入新下载的MP3扫描媒体数据
                                    musicInfo.setRemoteUri(localMusicInfo.getRemoteUri());
                                    musicInfo.setSongId(localMusicInfo.getSongId());
                                    musicInfo.setSongName(hotMusicInfo.getSingName());
                                    musicInfo.setArtistName(localMusicInfo.getArtistNames());
                                    musicInfo.setInPlayerList(localMusicInfo.isInPlayerList());
                                    //通知本地曲库列表界面 有新的歌曲下载到本地
                                    msg.what = Msg_What_Local_Music_Info_Add;
                                    msg.obj = musicInfo;
                                    Bundle bundle = new Bundle();
                                    bundle.putSerializable("oldMusicInfo",localMusicInfo);
                                    msg.setData(bundle);
                                    //通知热门曲库列表界面 有新的歌曲下载到本地,保证媒体信息被扫描并且被插入到数据库中，避免刚下载来不及扫描存储的瞬间用户点击播放按键无法播放的情况
                                    musicDownloadHandler.sendMessage(msg);
                                    msg = Message.obtain();
                                    msg.what = Msg_What_Hot_Music_Downloaded_Scaned;
                                    msg.obj = hotMusicInfo;
                                    musicDownloadHandler.sendMessage(msg);
                                }else{
                                    //为了避免数据库本身没有数据，或者有数据覆盖丢失localId durtion等数据
                                    localMusicInfo.setLocalId(musicInfo.getLocalId());
                                    localMusicInfo.setDuration(musicInfo.getDuration());
                                    localMusicInfo.setYear(musicInfo.getYear());
                                    msg.what = Msg_What_Hot_Music_Update_To_Local;
                                    Bundle bundle = new Bundle();
                                    bundle.putSerializable("localMusicInfo",localMusicInfo);
//                                    bundle.putSerializable("hotMusicInfo",hotMusicInfo);
                                    msg.setData(bundle);
                                    //UI线程更新数据到realm，同时需要更新到内存中的缓存中
                                    musicDownloadHandler.sendMessage(msg);
                                }
                            }
                        }
                    });
        }
    }

    private MusicDownloadHandler musicDownloadHandler = new MusicDownloadHandler(this);

    private static final int Msg_What_Local_Music_Info_Add = 0;
    private static final int Msg_What_Hot_Music_Downloaded_Scaned = 1;
    private static final int Msg_What_Hot_Music_Update_To_Local = 2;

    private class MusicDownloadHandler extends Handler {
        private WeakReference<MusicDownloaderCoreImpl> mWeakReference;

        MusicDownloadHandler(MusicDownloaderCoreImpl playerCore) {
            mWeakReference = new WeakReference<>(playerCore);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            MusicDownloaderCoreImpl playerCore = mWeakReference.get();
            if (playerCore == null){
                return;
            }
            switch (msg.what) {
                case Msg_What_Hot_Music_Update_To_Local:
                    LocalMusicInfo localMInfo = (LocalMusicInfo) msg.getData().getSerializable("localMusicInfo");
//                    HotMusicInfo hotMInfo = (HotMusicInfo) msg.getData().getSerializable("hotMusicInfo");
                    //更新到播放列表和本地列表
                    CoreManager.getCore(IPlayerCore.class).updateLocalMusicInfo(localMInfo);
                    hasUpdateCount+=1;
                    if(hasUpdateCount == lastHotMusicInfoNeedUpdateCount){
                        notifyClients(IMusicDownloaderCoreClient.class,IMusicDownloaderCoreClient.METHOD_ON_HOT_UPDATE_LOCAL_COMPLEED);
                        lastHotMusicInfoNeedUpdateCount = 0;
                        hasUpdateCount = 0;
                    }
                    break;
                case Msg_What_Hot_Music_Downloaded_Scaned:
                    HotMusicInfo hotMusicInfo = (HotMusicInfo) msg.obj;
                    notifyClients(IMusicDownloaderCoreClient.class,
                            IMusicDownloaderCoreClient.METHOD_ON_MUSIC_DOWNLOAD_COMPLETED,
                            hotMusicInfo);
                    break;
                case Msg_What_Local_Music_Info_Add:
                    LocalMusicInfo musicInfo = (LocalMusicInfo) msg.obj;
                    LocalMusicInfo oldMusicInfo = (LocalMusicInfo) msg.getData().getSerializable("oldMusicInfo");
                    CoreManager.getCore(IPlayerCore.class).replaceMusicInPlayerList(oldMusicInfo,musicInfo);
                    CoreManager.getCore(IPlayerDbCore.class).insertOrUpdateLocalMusicInfo(musicInfo);
                    notifyClients(IMusicDownloaderCoreClient.class,
                            IMusicDownloaderCoreClient.METHOD_ON_MUSIC_DOWNLOAD_COMPLE_INFO_UPDATED,
                            musicInfo);
                    break;
                default:
            }
        }
    }

    /**
     * 下载进度更新
     * @param info
     * @param localMusicInfo
     * @param hotMusicInfo
     */
    private void onMusicDownloadProgress(LocalMusicInfo localMusicInfo, HotMusicInfo hotMusicInfo,ProgressInfo info){
        int progress = (int)(info.getProgress()*100/info.getTotal());
        //通知界面更新下载进度
        notifyClients(IMusicDownloaderCoreClient.class,IMusicDownloaderCoreClient.METHOD_ON_MUSIC_DOWNLOAD_PROGRESS_UPDATE,
                localMusicInfo,hotMusicInfo,progress);
    }


    /**
     * 删除所有的下载中队列，以及对应的缓存信息<临时文件暂时不删除>
     */
    @Override
    public void deleteAllDownQueueAsWellMusicInfoAsTempFile() {

    }

    /**
     * 下载的歌曲被移除播放列表
     *
     * @param url
     */
    @Override
    public void deleteHotMusicDownloadingFromPlayList(String url) {
        LocalMusicInfo musicInfo = downloadLocalMusicInfoQueue.get(url);
        if(null != musicInfo){
            CoreManager.getCore(IPlayerCore.class).deleteMusicFromPlayerList(musicInfo);
            musicInfo.setInPlayerList(false);
        }
    }

    /**
     * 检测HotMusicInfo是否处于下载队列中
     *
     * @param uri
     */
    @Override
    public boolean checkHotMusicIsDownloading(String uri) {
        return downloadHotMusicInfoQueue.get(uri) != null;
    }

    /**
     * 查询下载中的LocalMusicIinfo队列的方法
     *
     * @return
     */
    @Override
    public List<LocalMusicInfo> queryDownloadingMusicListInPlayList() {
        List<LocalMusicInfo> inPlayerListLocalMusicInfos = new ArrayList<>();
        for(LocalMusicInfo musicInfo : downloadLocalMusicInfoQueue.values()){
            if(musicInfo.isInPlayerList()){
                inPlayerListLocalMusicInfos.add(musicInfo);
            }
        }
        return inPlayerListLocalMusicInfos;
    }

    private int lastHotMusicInfoNeedUpdateCount = 0;
    private int hasUpdateCount = 0;

    @Override
    public boolean updateHotMusicInfoToLocal(List<HotMusicInfo> hotMusicInfos) {
        boolean needWaitHotMusicInfoUpdateToLocal = false;
        LogUtils.d(TAG,"updateHotMusicInfoToLocal-needWaitHotMusicInfoUpdateToLocal1:"+needWaitHotMusicInfoUpdateToLocal);
        lastHotMusicInfoNeedUpdateCount = 0;
        hasUpdateCount = 0;
        List<HotMusicInfo> infos = new ArrayList<>();
        for(HotMusicInfo hotMusicInfo : hotMusicInfos){
            String path = MusicFileUtil.getMusicDownloadFilePath(hotMusicInfo.getSingName(),hotMusicInfo.getId());
            if(BasicFileUtils.isFileExisted(path) && checkIfNeedUpdateHotMusicInfoToLocal(hotMusicInfo)){
                if(!needWaitHotMusicInfoUpdateToLocal){
                    needWaitHotMusicInfoUpdateToLocal = true;
                }
                infos.add(hotMusicInfo);
            }
        }

        lastHotMusicInfoNeedUpdateCount = infos.size();
        LogUtils.d(TAG,"updateHotMusicInfoToLocal-needWaitHotMusicInfoUpdateToLocal2:"+needWaitHotMusicInfoUpdateToLocal);
        LogUtils.d(TAG,"updateHotMusicInfoToLocal-lastHotMusicInfoNeedUpdateCount:"+lastHotMusicInfoNeedUpdateCount);
        for(HotMusicInfo hotMusicInfo : infos){
            String path = MusicFileUtil.getMusicDownloadFilePath(hotMusicInfo.getSingName(),hotMusicInfo.getId());
            LocalMusicInfo localMusicInfo = CoreManager.getCore(IPlayerDbCore.class).gueryLocalMusicInfoByLocalUrl(path);
            if(localMusicInfo == null){
                localMusicInfo = new LocalMusicInfo();
                localMusicInfo.setLocalUri(MusicFileUtil.getMusicDownloadFilePath(hotMusicInfo.getSingName(),hotMusicInfo.getId()));
                List<String> artistNames = new ArrayList<String>();
                artistNames.add(hotMusicInfo.getSingerName());
                localMusicInfo.setArtistName(artistNames);
                localMusicInfo.setSongName(hotMusicInfo.getSingName());
//                tempLocalId-=1;
//                localMusicInfo.setLocalId(tempLocalId);
            }
            //已经扫描到数据库的，就更新songid和remoteuri即可
            localMusicInfo.setSongId(hotMusicInfo.getId()+"");
            localMusicInfo.setRemoteUri(hotMusicInfo.getSingUrl());
            updateExService.execute(new ScanMusicMediaInfoRunnable(localMusicInfo,hotMusicInfo,false));
        }
        return needWaitHotMusicInfoUpdateToLocal;
    }

    private boolean checkIfNeedUpdateHotMusicInfoToLocal(HotMusicInfo hotMusicInfo) {
        boolean needToUpdate = false;
        String path = MusicFileUtil.getMusicDownloadFilePath(hotMusicInfo.getSingName(),hotMusicInfo.getId());
        LocalMusicInfo musicInfo = CoreManager.getCore(IPlayerDbCore.class).gueryLocalMusicInfoByLocalUrl(path);
        if(null == musicInfo){
            needToUpdate = true;
        }else{
            //musicInfo不为空则为本地列表已经扫描的情况，那么肯定存在localId，更新数据库就有了相应的主键保障
            long songId = 0L;
            try {
                songId = Long.valueOf(musicInfo.getSongId());
                needToUpdate = false;
            } catch (NumberFormatException nfex){
                nfex.printStackTrace();
                needToUpdate = true;
            } catch (IllegalStateException ise){
                ise.printStackTrace();
            }
        }
        return needToUpdate;
    }
}
