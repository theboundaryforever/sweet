package com.tongdaxing.xchat_core.music.bean;

import java.io.Serializable;

public class HotMusicInfo implements Serializable{

    /**
     * createTime : 1541647596060
     * id : 0
     * singCount : 0
     * singName : string
     * singSize : 0
     * singStatus : 0
     * singType : 0
     * singUrl : string
     * singerName : string
     * updateTime : 1541647596060
     *
     * upUserId : 0
     * "userNo":3840915,
     * "userNick":"你别你要特别特别根据女虐楼欧诺"
     */

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public long getId() {
        return id;
    }

    public int getSingCount() {
        return singCount;
    }

    public void setSingCount(int singCount) {
        this.singCount = singCount;
    }

    public String getSingName() {
        return singName;
    }

    public void setSingName(String singName) {
        this.singName = singName;
    }

    public int getSingSize() {
        return singSize;
    }

    public void setSingSize(int singSize) {
        this.singSize = singSize;
    }

    public int getSingStatus() {
        return singStatus;
    }

    public void setSingStatus(int singStatus) {
        this.singStatus = singStatus;
    }

    public int getSingType() {
        return singType;
    }

    public void setSingType(int singType) {
        this.singType = singType;
    }

    public String getSingUrl() {
        return singUrl;
    }

    public void setSingUrl(String singUrl) {
        this.singUrl = singUrl;
    }

    public String getSingerName() {
        return singerName;
    }

    public void setSingerName(String singerName) {
        this.singerName = singerName;
    }

    public long getUpUserId() {
        return upUserId;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "HotMusicInfo{" +
                "createTime='" + createTime + '\'' +
                ", id=" + id +
                ", singCount=" + singCount +
                ", singName='" + singName + '\'' +
                ", singSize=" + singSize +
                ", singStatus=" + singStatus +
                ", singType=" + singType +
                ", singUrl='" + singUrl + '\'' +
                ", singerName='" + singerName + '\'' +
                ", upUserId=" + upUserId +
                ", updateTime='" + updateTime + '\'' +
                ", userAvatar='" + userAvatar + '\'' +
                '}';
    }

    private long createTime;
    private long id;

    public void setId(long id) {
        this.id = id;
    }

    public long getUserNo() {
        return userNo;
    }

    public void setUserNo(long userNo) {
        this.userNo = userNo;
    }

    public String getUserNick() {
        return userNick;
    }

    public void setUserNick(String userNick) {
        this.userNick = userNick;
    }

    public void setUpUserId(long upUserId) {
        this.upUserId = upUserId;
    }

    private long userNo;
    private String userNick;
    private int singCount;
    private String singName;
    private int singSize;
    private int singStatus;
    private int singType;
    private String singUrl;

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    private String userAvatar;
    private String singerName;
    private long upUserId;
    private long updateTime;

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof HotMusicInfo)) {
            return false;
        }
        HotMusicInfo other = (HotMusicInfo) obj;
        if (other.getId() == this.getId()) {
            return true;
        }
        return false;
    }
}
