package com.tongdaxing.xchat_core.initial;


import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.room.face.IFaceCore;

/**
 * @author xiaoyu
 * @date 2017/12/29
 */
public class InitPresenter extends AbstractMvpPresenter<IInitView> implements InitModel.OnInitResultListener {

    private final String TAG = InitPresenter.class.getSimpleName();

    public InitInfo getLocalSplashVo() {
        return InitModel.get().getCacheInitInfo();
    }

    public void init(boolean force) {
        InitModel.get().registerInitResultListener(this);
        InitModel.get().init(force);
    }

    @Override
    public void onInitResult(ServiceResult<InitInfo> initResult, Throwable throwable) {
        if (initResult != null && initResult.getData() != null) {
            InitInfo data = initResult.getData();
            LogUtils.d(TAG, "init-accept data:" + data);
            // 表情
            if (data != null && data.getFaceJson() != null) {
                CoreManager.getCore(IFaceCore.class).onReceiveOnlineFaceJson(data.getFaceJson().getJson());
            }
            // 闪屏
            if (data != null && data.getSplashVo() != null && getMvpView() != null) {
                getMvpView().onInitSuccess(data);
            }
        }
        if (null != throwable) {
            throwable.printStackTrace();
        }
        InitModel.get().unregisterInitResultListener(InitPresenter.this);
    }

    @Override
    public void onDestroyPresenter() {
        InitModel.get().unregisterInitResultListener(this);
        super.onDestroyPresenter();
    }
}
