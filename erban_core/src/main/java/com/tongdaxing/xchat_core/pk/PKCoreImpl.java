package com.tongdaxing.xchat_core.pk;

import com.tongdaxing.erban.libcommon.coremanager.AbstractBaseCore;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.LogUtil;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.pk.bean.PkVoteInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

public class PKCoreImpl extends AbstractBaseCore implements IPkCore {
    public static final String TAG = "PKCoreImpl";

    public PKCoreImpl() {
    }

    @Override
    public void savePK(long roomId, final PkVoteInfo info) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomId", roomId + "");
        params.put("pkType", info.getPkType() + "");
        params.put("targetUidList", info.getTargetUidList().toString());
        params.put("title", info.getTitle());
        params.put("expireSeconds", info.getExpireSeconds() + "");
        OkHttpManager.getInstance().postRequest(UriProvider.savePk(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                notifyClients(IPKCoreClient.class, IPKCoreClient.METHOD_ON_SAVE_PK_FAIL, e.getMessage());
            }

            @Override
            public void onResponse(Json response) {
                if (response.num("code") != 200) {
                    notifyClients(IPKCoreClient.class, IPKCoreClient.METHOD_ON_SAVE_PK_FAIL, response.str("message", "网络异常"));
                } else {
                    try {
                        JSONObject dataJson = response.getJSONObject("data");
                        long pkId = dataJson.getLong("id");
                        if (pkId > 0) {
                            info.setVoteId(pkId);
                            notifyClients(IPKCoreClient.class, IPKCoreClient.METHOD_ON_SAVE_PK, info);
                        } else {
                            notifyClients(IPKCoreClient.class, IPKCoreClient.METHOD_ON_SAVE_PK_FAIL, "PK id 异常！");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        notifyClients(IPKCoreClient.class, IPKCoreClient.METHOD_ON_SAVE_PK_FAIL, "PK id 异常！");
                    }
                }
            }
        });
    }


    @Override
    public void cancelPK(long roomId) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomId", roomId + "");
        OkHttpManager.getInstance().postRequest(UriProvider.cancelPk(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                LogUtil.d(TAG, "cancelPK--->onError");
            }

            @Override
            public void onResponse(Json response) {
                LogUtil.d(TAG, "cancelPK--->onResponse code:" + response.num("code"));
            }
        });
    }


    @Override
    public void getPkHistoryList(long roomId, int page) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomId", roomId + "");
        params.put("pageNum", page + "");
        params.put("pageSize", String.valueOf(Constants.PAGE_SIZE));
        OkHttpManager.getInstance().postRequest(UriProvider.getPkHistoryList(), params, new OkHttpManager.MyCallBack<ServiceResult<List<PkVoteInfo>>>() {
            @Override
            public void onError(Exception e) {
                notifyClients(IPKCoreClient.class, IPKCoreClient.METHOD_ON_PK_HISTORY_LIST_FAIL, e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<List<PkVoteInfo>> response) {
                if (response != null && response.isSuccess()) {
                    notifyClients(IPKCoreClient.class, IPKCoreClient.METHOD_ON_PK_HISTORY_LIST, response.getData());
                } else {
                    notifyClients(IPKCoreClient.class, IPKCoreClient.METHOD_ON_PK_HISTORY_LIST_FAIL, response.getMessage());
                }
            }
        });
    }

}
