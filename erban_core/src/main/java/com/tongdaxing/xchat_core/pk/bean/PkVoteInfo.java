package com.tongdaxing.xchat_core.pk.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 房间投票信息bean
 */
public class PkVoteInfo implements Serializable {
    public int vipId;
    public int vipDate;
    public boolean isInvisible;
    public int pkVipId;
    public int pkVipDate;
    public boolean pkIsInvisible;
    private long timestamps;
    private int duration;
    private int expireSeconds;
    private int pkType;
    private long uid;
    private String nick;
    private String avatar;
    private int voteCount;
    //发起人UID
    private long pkUid;
    private String pkNick;
    private String pkAvatar;
    //总PK礼物值/
    private int pkVoteCount;
    private long createTime;
    private long voteId;
    //PK赢得胜利人的UID
    private long opUid;

    //多人PK新增
    private long id;
    private String targetUidList;
    //新版本多人PK，多人列表
    private List<PkUser> pkList = new ArrayList<>();
    private String title = "快来参与投票吧〜";

    public static class PkUser implements Serializable {
        private long pkId;//": 104378924,
        private String uid;//": 93000816,
        private String nick;//": "刚刚的哈哈哈",
        private String avatar;//": "https://img.pinjin88.com/touxiangnan@3x.png",
        private long userNo;//": 1735077,
        private int voteCount;//": 0

        /**
         * 麦序位置
         */
        private int micPosition = -2;

        public int getMicPosition() {
            return micPosition;
        }

        public void setMicPosition(int micPosition) {
            this.micPosition = micPosition;
        }

        public long getPkId() {
            return pkId;
        }

        public void setPkId(long pkId) {
            this.pkId = pkId;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getNick() {
            return nick;
        }

        public void setNick(String nick) {
            this.nick = nick;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public long getUserNo() {
            return userNo;
        }

        public void setUserNo(long userNo) {
            this.userNo = userNo;
        }

        public int getVoteCount() {
            return voteCount;
        }

        public void setVoteCount(int voteCount) {
            this.voteCount = voteCount;
        }
    }

    public List<PkUser> getPkList() {
        return pkList;
    }

    public void setPkList(List<PkUser> pkList) {
        this.pkList = pkList;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTargetUidList() {
        return targetUidList;
    }

    public void setTargetUidList(String targetUidList) {
        this.targetUidList = targetUidList;
    }

    public int getVipId() {
        return vipId;
    }

    public void setVipId(int vipId) {
        this.vipId = vipId;
    }

    public int getVipDate() {
        return vipDate;
    }

    public void setVipDate(int vipDate) {
        this.vipDate = vipDate;
    }

    public boolean isInvisible() {
        return isInvisible;
    }

    public void setInvisible(boolean invisible) {
        isInvisible = invisible;
    }

    public int getPkVipId() {
        return pkVipId;
    }

    public void setPkVipId(int pkVipId) {
        this.pkVipId = pkVipId;
    }

    public int getPkVipDate() {
        return pkVipDate;
    }

    public void setPkVipDate(int pkVipDate) {
        this.pkVipDate = pkVipDate;
    }

    public boolean isPkIsInvisible() {
        return pkIsInvisible;
    }

    public void setPkIsInvisible(boolean pkIsInvisible) {
        this.pkIsInvisible = pkIsInvisible;
    }

    public long getTimestamps() {
        return timestamps;
    }

    public void setTimestamps(long timestamps) {
        this.timestamps = timestamps;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getPkType() {
        return pkType;
    }

    public void setPkType(int pkType) {
        this.pkType = pkType;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(int voteCount) {
        this.voteCount = voteCount;
    }

    public long getPkUid() {
        return pkUid;
    }

    public void setPkUid(long pkUid) {
        this.pkUid = pkUid;
    }

    public String getPkNick() {
        return pkNick;
    }

    public void setPkNick(String pkNick) {
        this.pkNick = pkNick;
    }

    public String getPkAvatar() {
        return pkAvatar;
    }

    public void setPkAvatar(String pkAvatar) {
        this.pkAvatar = pkAvatar;
    }

    public int getPkVoteCount() {
        return pkVoteCount;
    }

    public void setPkVoteCount(int pkVoteCount) {
        this.pkVoteCount = pkVoteCount;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public long getVoteId() {
        return id;
    }

    public void setVoteId(long voteId) {
        this.voteId = voteId;
    }

    public long getOpUid() {
        return opUid;
    }

    public void setOpUid(long opUid) {
        this.opUid = opUid;
    }

    public int getExpireSeconds() {
        return expireSeconds;
    }

    public void setExpireSeconds(int expireSeconds) {
        this.expireSeconds = expireSeconds;
    }

    public void setId(long id) {
        this.id = id;
    }
}
