package com.tongdaxing.xchat_core.pk;

import com.tongdaxing.erban.libcommon.coremanager.IBaseCore;
import com.tongdaxing.xchat_core.pk.bean.PkVoteInfo;

public interface IPkCore extends IBaseCore{


    /**
     * 发起保存一个PK
     */
    void savePK(long roomId,PkVoteInfo info);


    void getPkHistoryList(long roomId,int page);

    /**
     * 取消一个PK
     * @param roomId
     */
    void cancelPK(long roomId);
}
