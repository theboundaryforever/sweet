package com.tongdaxing.xchat_core.liveroom.im.model.bean;

import java.io.Serializable;

/**
 * @author weihaitao
 * @date 2019/5/21
 */
public class SingleAudioRoomEnitity extends GameRoomEnitity implements Serializable {

    /**
     * 房主昵称
     */
    private String nick;

    /**
     * 房间ID
     */
    private long roomId;

    /**
     * 房间是否开启，是否正在直播
     */
    private boolean valid;

    /**
     * 房间描述
     */
    private String roomDesc;

    /**
     * 房主是否在线
     */
    private boolean operateStatus;

    /**
     * 房间热度值
     */
    private long hotScore;

    //房间角标
    private String roomLabel;

    //性别 1:男 2：女 0 ：未知
    private int gender;

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getRoomLabel() {
        return roomLabel;
    }

    public void setRoomLabel(String roomLabel) {
        this.roomLabel = roomLabel;
    }

    public long getHotScore() {
        return hotScore;
    }

    public void setHotScore(long hotScore) {
        this.hotScore = hotScore;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getRoomDesc() {
        return roomDesc;
    }

    public void setRoomDesc(String roomDesc) {
        this.roomDesc = roomDesc;
    }


    public boolean isOperateStatus() {
        return operateStatus;
    }

    public void setOperateStatus(boolean operateStatus) {
        this.operateStatus = operateStatus;
    }

    @Override
    public String toString() {
        return "SingleAudioRoomEnitity{" +
                "title='" + title + '\'' +
                ", nick='" + nick + '\'' +
                ", uid=" + uid +
                ", roomId=" + roomId +
                ", valid=" + valid +
                ", avatar='" + avatar + '\'' +
                ", roomDesc='" + roomDesc + '\'' +
                ", type=" + type +
                ", operateStatus=" + operateStatus +
                ", hotScore=" + hotScore +
                ", avatar='" + avatar + '\'' +
                ", title='" + title + '\'' +
                ", onlineNum=" + onlineNum +
                ", newestTime=" + newestTime +
                ", uid=" + uid +
                ", badge='" + badge + '\'' +
                ", roomTag='" + roomTag + '\'' +
                ", tagPict='" + tagPict + '\'' +
                ", labelUrl='" + labelUrl + '\'' +
                ", type=" + type +
                ", gender=" + gender +
                '}';
    }
}
