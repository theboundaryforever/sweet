package com.tongdaxing.xchat_core.liveroom.im;

import com.tongdaxing.xchat_core.UriProvider;

/**
 * @author weihaitao
 * @date 2019/4/20
 */
public class IMUriProvider extends UriProvider {

    //查询房间黑名单列表
    public static String getRoomBlackListUrl() {
        return JAVA_WEB_URL.concat("/imroom/v1/fetchRoomBlackList");
    }

    //查询房间管理员
    public static String getRoomManagersUrl() {
        return JAVA_WEB_URL.concat("/imroom/v1/fetchRoomManagers");
    }

    /**
     * 申请上麦
     */
    public static String getApplyWaitQueueUrl() {
        return JAVA_WEB_URL.concat("/room/mic/applyWaitQueue");
    }

    /**
     * 邀请上麦（视频房）
     */
    public static String getInviteUpMicroOnVideoRoomUrl() {
        return JAVA_WEB_URL.concat("/room/mic/passmic");
    }

    /**
     * 邀请用户进房
     */
    public static String getInviteEnterRoomUrl() {
        return JAVA_WEB_URL.concat("/room/mic/requestNoRoomUser");
    }

    public static String getMarkBlackList() {
        return JAVA_WEB_URL.concat("/imroom/v1/markChatRoomBlackList");
    }

    public static String getKickMemberUrl() {
        return JAVA_WEB_URL.concat("/imroom/v1/kickMember");
    }

    /**
     * 检测用户
     */
    public static String getCheckPushAuth() {
        return JAVA_WEB_URL.concat("/imroom/v1/checkPushAuth");
    }

    public static String getMarkChatRoomManager() {
        return JAVA_WEB_URL.concat("/imroom/v1/markChatRoomManager");
    }

    //查询房间管理员
    public static String getFetchRoomMembersUrl() {
        return JAVA_WEB_URL.concat("/imroom/v1/fetchRoomMembers");
    }

    @Deprecated
    public static String getIMRoomInfoUrl() {
        return JAVA_WEB_URL.concat("/room/get");
    }

    //单人房-房主--开播
    public static String getStartPlayRoomUrl() {
        return JAVA_WEB_URL.concat("/room/personal/startPlayRoom");
    }

    //单人房-房主--停播
    public static String getStopPlayRoomUrl() {
        return JAVA_WEB_URL.concat("/room/personal/stopPlayRoom");
    }

    //单人房-房主-停播-开播信息统计界面
    public static String getRoomPlayInfoUrl() {
        return JAVA_WEB_URL.concat("/ttyy/room_statistics/index.html");
    }

    //单人房-为主播点赞
    public static String getRoomAdmireUrl() {
        return JAVA_WEB_URL.concat("/room/personal/admire");
    }

    //单人房-获取为主播点赞的剩余次数
    public static String getRoomAdmireCountUrl() {
        return JAVA_WEB_URL.concat("/room/personal/getAdmireCount");
    }
}
