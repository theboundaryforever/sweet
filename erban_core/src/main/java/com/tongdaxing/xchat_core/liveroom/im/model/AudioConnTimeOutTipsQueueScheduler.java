package com.tongdaxing.xchat_core.liveroom.im.model;

import android.os.Handler;
import android.os.Message;

import com.tongdaxing.erban.libcommon.utils.LogUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 用于主播打开语音连线弹框，点击[连接]按钮后，检测该用户5秒内是否已上麦，如果不在麦上，
 * 需要toast提示[对方已取消申请],并且刷新主播端连接记录列表
 * <p>
 * 0.map存储runnable，key为uid
 * 1.点击连线按钮后，接口调用成功，则直接add delay runnable
 * 2.add的时候，需要判断IM是否已经实时的将用户移到的麦位上，是就终止add runnable操作
 * 3.delay时间到了之后，需要判断用户是否已经在麦位上，否则toast并刷新主播端列表，是则什么也不干
 * 4.退房/task move等操作需要clear all runnable
 *
 * @author weihaitao
 * @date 2019/7/18
 */
public class AudioConnTimeOutTipsQueueScheduler {

    private static AudioConnTimeOutTipsQueueScheduler instance = null;
    private final String TAG = AudioConnTimeOutTipsQueueScheduler.class.getSimpleName();
    private Map<String, Runnable> runnableMap = new HashMap<>();
    private Handler mHandler = null;
    private OnConnTimeOutListener onConnTimeOutListener;

    private AudioConnTimeOutTipsQueueScheduler() {

    }

    public static AudioConnTimeOutTipsQueueScheduler getInstance() {

        if (null == instance) {
            instance = new AudioConnTimeOutTipsQueueScheduler();
        }

        return instance;
    }

    public void startDelayTipsRunnable(long uid) {
        LogUtils.d(TAG, "startDelayTipsRunnable uid:" + uid);
        try {
            if (RoomDataManager.get().isOnMic(uid)) {
                LogUtils.d(TAG, "startDelayTipsRunnable 用户已经在麦上了,任务终止");
                return;
            }

            if (null == runnableMap) {
                runnableMap = new HashMap<>();
            }

            if (null == mHandler) {
                mHandler = new Handler(new Handler.Callback() {
                    @Override
                    public boolean handleMessage(Message msg) {
                        if (null != onConnTimeOutListener) {
                            onConnTimeOutListener.onConnTimeOut();
                        }
                        return true;
                    }
                });
            }

            Runnable remRunnable = runnableMap.remove(String.valueOf(uid));
            if (null != remRunnable && null != mHandler) {
                mHandler.removeCallbacks(remRunnable);
            }

            Runnable timeOutTipRunnable = new Runnable() {
                @Override
                public void run() {
                    if (RoomDataManager.get().isOnMic(uid)) {
                        LogUtils.d(TAG, "startDelayTipsRunnable-->timeOutTipRunnable 用户已经在麦上了,toast终止");
                        return;
                    }
                    if (null != mHandler) {
                        mHandler.sendEmptyMessage(0);
                    }
                }
            };
            runnableMap.put(String.valueOf(uid), timeOutTipRunnable);
            mHandler.postDelayed(timeOutTipRunnable, 5000L);
            LogUtils.d(TAG, "startDelayTipsRunnable uid:" + uid + " 已经成功添加其超时上麦提示任务");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void stopDelayTipsRunnable(String uid) {
        LogUtils.d(TAG, "stopDelayTipsRunnable uid:" + uid);
        try {
            if (null != runnableMap) {
                Runnable remRunnable = runnableMap.remove(uid);
                if (null != remRunnable && null != mHandler) {
                    mHandler.removeCallbacks(remRunnable);
                    LogUtils.d(TAG, "stopDelayTipsRunnable uid:" + uid + " 已经成功移除其超时上麦提示任务");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void clear() {
        if (null != runnableMap) {
            runnableMap.clear();
            runnableMap = null;
        }
        if (null != mHandler) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }
        LogUtils.d(TAG, "clear set onConnTimeOutListener null.");
        onConnTimeOutListener = null;
        instance = null;
    }

    public void setOnConnTimeOutListener(OnConnTimeOutListener listener) {
        LogUtils.d(TAG, "setOnConnTimeOutListener");
        this.onConnTimeOutListener = listener;
    }

    public interface OnConnTimeOutListener {
        void onConnTimeOut();
    }

}
