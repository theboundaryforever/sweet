package com.tongdaxing.xchat_core.liveroom.im.model.bean;

/**
 * 房间人员进入消息
 */
public class IMRoomMemberComeInfo {
    private int experLevel;
    private String nickName;
    private String carImgUrl;
    private String carName;

    public int getExperLevel() {
        return experLevel;
    }

    public void setExperLevel(int experLevel) {
        this.experLevel = experLevel;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getCarImgUrl() {
        return carImgUrl;
    }

    public void setCarImgUrl(String carImgUrl) {
        this.carImgUrl = carImgUrl;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }
}
