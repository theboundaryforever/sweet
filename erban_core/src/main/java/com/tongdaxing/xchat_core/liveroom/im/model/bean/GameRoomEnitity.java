package com.tongdaxing.xchat_core.liveroom.im.model.bean;

import java.io.Serializable;

public class GameRoomEnitity implements Serializable {

    protected String avatar;//": "string",
    protected String title;//": "string",
    protected int onlineNum;//
    protected long newestTime;
    protected long uid;
    protected String roomPwd;
    protected String badge;
    protected String roomTag = "";
    protected String tagPict;
    protected String labelUrl;

    //房间类型
    protected int type = 0;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "GameRoomEnitity{" +
                "avatar='" + avatar + '\'' +
                ", title='" + title + '\'' +
                ", onlineNum=" + onlineNum +
                ", newestTime=" + newestTime +
                ", uid=" + uid +
                ", badge='" + badge + '\'' +
                ", roomTag='" + roomTag + '\'' +
                ", tagPict='" + tagPict + '\'' +
                ", labelUrl='" + labelUrl + '\'' +
                ", type=" + type +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GameRoomEnitity that = (GameRoomEnitity) o;

        return uid == that.uid;
    }

    public String getRoomPwd() {
        return roomPwd;
    }

    public void setRoomPwd(String roomPwd) {
        this.roomPwd = roomPwd;
    }

    @Override
    public int hashCode() {
        return (int) (uid ^ (uid >>> 32));
    }

    public String getLabelUrl() {
        return labelUrl;
    }

    public void setLabelUrl(String labelUrl) {
        this.labelUrl = labelUrl;
    }

    public String getRoomTag() {
        return roomTag;
    }

    public void setRoomTag(String roomTag) {
        this.roomTag = roomTag;
    }

    public String getTagPict() {
        return tagPict;
    }

    public void setTagPict(String tagPict) {
        this.tagPict = tagPict;
    }

    public String getBadge() {
        return badge;
    }

    public void setBadge(String badge) {
        this.badge = badge;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getOnlineNum() {
        return onlineNum;
    }

    public void setOnlineNum(int onlineNum) {
        this.onlineNum = onlineNum;
    }

    public long getNewestTime() {
        return newestTime;
    }

    public void setNewestTime(long newestTime) {
        this.newestTime = newestTime;
    }
}
