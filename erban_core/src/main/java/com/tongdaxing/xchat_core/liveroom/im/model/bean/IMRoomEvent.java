package com.tongdaxing.xchat_core.liveroom.im.model.bean;

import com.tongdaxing.xchat_core.bean.RoomRedPacket;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;
import com.tongdaxing.xchat_core.gift.MultiGiftReceiveInfo;
import com.tongdaxing.xchat_core.im.custom.bean.RoomCharmAttachment;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.room.auction.RoomAuctionBean;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.zego.zegoavkit2.soundlevel.ZegoSoundLevelInfo;

import java.util.List;

/**
 * 定义房间的操作
 *
 * @author xiaoyu
 * @date 2017/12/22
 */

public class IMRoomEvent extends RoomEvent {

    /**
     * 即构声浪状态
     */
    public static final int SPEAK_ZEGO_STATE_CHANGE = 33;
    public static final int CURRENT_SPEAK_STATE_CHANGE = 30;
    public static final int ZEGO_RESTART_CONNECTION_EVENT = 31;
    public static final int ZEGO_AUDIO_DEVICE_ERROR = 32;

    /**
     * 推拉流失败次数超过10次数据
     */
    public static final int PLAY_OR_PUBLISH_NETWORK_ERROR = 23;

    //新的重连消息
    public static final int ROOM_RECONNECT = 0x00000029;

    public static final int SOCKET_IM_RECONNECT_LOGIN_SUCCESS = 0x00000030;
    //通知邀请上麦（视频房）
    public static final int CODE_INVITE_MIC_ON_VIDEO_ROOM = 0x00000031;
    //通知邀请进房
    public static final int CODE_INVITE_ENTER_ROOM = 0x00000032;
    //有人申请上麦
    public static final int CODE_APPLY_WAIT_QUEUE_NOTICE = 0x00000033;


    //单个礼物消息
    public static final int ROOM_RECEIVE_GIFT_MSG = 0x00000040;
    //多个礼物
    public static final int ROOM_RECEIVE_MULTI_GIFT = 0x00000041;
    //全服消息
    public static final int ROOM_RECEIVE_SUPER_GIFT = 0x00000042;

    public static final int ROOM_RECEIVE_HOT_RANK_UPDATE = 0x00000043;

    //主播开播
    public static final int ROOM_RECEIVE_LIVE_START = 0x00000046;
    //主播停播
    public static final int ROOM_RECEIVE_LIVE_STOP = 0x00000047;

    /**
     * 播放房间点赞动画
     */
    public static final int ROOM_ADMIRE_ANIM = 0x00000048;

    //魅力值更新事件
    public static final int ROOM_MIC_CHARM_UPDATE = 0x00000049;

    //房间气泡通知事件
    public static final int ROOM_FLOATING_BUBBL_NOTIFY = 0x00000050;
    /**
     * 通知主播，语音连麦，申请数量有变，界面做相应的提示展示
     */
    public static final int ROOM_AUDIO_CONN_REQ_RECV_NEW = 0x00000051;

    /**
     * 通知主播，语音连麦连接被对方挂断了
     */
    public static final int ROOM_AUDIO_CONN_USER_CALL_DOWN_NOTIFY = 0x00000052;

    /**
     * 通知用户，语音连麦连接被对方挂断了
     */
    public static final int ROOM_AUDIO_CONN_ANCHOR_CALL_DOWN_NOTIFY = 0x00000053;

    /**
     * 通知用户，语音连麦申请，主播连接同意了
     */
    public static final int ROOM_AUDIO_CONN_REQ_CONN_BY_ANCHOR = 0x00000054;


    /**
     * 新多人房，主播发起召集令成功/召集令已召集人数更新通知
     */
    public static final int ROOM_CALL_UP_NUM_NOTIFY = 0x00000055;

    /**
     * 竞拍房的状态变化
     */
    public static final int ROOM_AUCTION_STATE_NOTIFY = 0x00000056;

    /**
     * 单人房，点赞，动画，对应的icon
     */
    private String admireAnimIconUrl = null;
    private int detonatingState = -1;
    /**
     * 语音连麦，单人房，主播或者用户主动挂断后的给端方的提示
     */
    private String audioConnOperaTips = "";

    private GiftReceiveInfo giftReceiveInfo;
    private MultiGiftReceiveInfo multiGiftReceiveInfo;

    public IMRoomEvent setGiftReceiveInfo(GiftReceiveInfo giftReceiveInfo) {
        this.giftReceiveInfo = giftReceiveInfo;
        return this;
    }

    public IMRoomEvent setMultiGiftReceiveInfo(MultiGiftReceiveInfo multiGiftReceiveInfo) {
        this.multiGiftReceiveInfo = multiGiftReceiveInfo;
        return this;
    }


    private String reasonMsg;
    private int reasonNo;
    private IMRoomQueueInfo imRoomQueueInfo;
    private IMRoomMessage mIMRoomMessage;

    private IMRoomMember chatRoomMember;

    /**
     * 当前用户的麦位
     */
    private int currentMicPosition = Integer.MIN_VALUE;

    private float currentMicStreamLevel = 0;

//    //即构的说话队列
    protected List<ZegoSoundLevelInfo> speakQueueMembersPosition;
    /**
     * 房间热度排行
     */
    private long hotRank = 0L;
    /**
     * 房间热度值
     */
    private long hotScore = 0L;
    private RoomCharmAttachment roomCharmInfo;

    public String getAdmireAnimIconUrl() {
        return admireAnimIconUrl;
    }

    public IMRoomEvent setAdmireAnimIconUrl(String admireAnimIconUrl) {
        this.admireAnimIconUrl = admireAnimIconUrl;
        return this;
    }

    private int auctionState = -1;
    private int auctionJoinCount = -1;
    private RoomAuctionBean roomAuctionBean;
    private RoomRedPacket roomRedPacket;
    private String redPacketId;

    public String getAudioConnOperaTips() {
        return audioConnOperaTips;
    }

    public IMRoomEvent setAudioConnOperaTips(String audioConnOperaTips) {
        this.audioConnOperaTips = audioConnOperaTips;
        return this;
    }

    public long getHotRank() {
        return hotRank;
    }

    public IMRoomEvent setHotRank(long hotRank) {
        this.hotRank = hotRank;
        return this;
    }

    public long getHotScore() {
        return hotScore;
    }

    public IMRoomEvent setHotScore(long hotScore) {
        this.hotScore = hotScore;
        return this;
    }

    @Override
    public IMRoomEvent setEvent(int event) {
        this.event = event;
        return this;
    }

    public IMRoomQueueInfo getImRoomQueueInfo() {
        return imRoomQueueInfo;
    }

    public IMRoomEvent setImRoomQueueInfo(IMRoomQueueInfo imRoomQueueInfo) {
        this.imRoomQueueInfo = imRoomQueueInfo;
        return this;
    }

    @Override
    public IMRoomEvent setMicPosition(int micPosition) {
        this.micPosition = micPosition;
        return this;
    }


    @Override
    public IMRoomEvent setPosState(int posState) {
        this.posState = posState;
        return this;
    }


    @Override
    public IMRoomEvent setAccount(String account) {
        this.account = account;
        return this;
    }


    @Override
    public IMRoomEvent setRoomInfo(RoomInfo roomInfo) {
        this.roomInfo = roomInfo;
        return this;
    }


    @Override
    public IMRoomEvent setSuccess(boolean success) {
        this.success = success;
        return this;
    }

    public IMRoomMessage getIMRoomMessage() {
        return mIMRoomMessage;
    }

    public IMRoomEvent setIMRoomMessage(IMRoomMessage chatRoomMessage) {
        mIMRoomMessage = chatRoomMessage;
        return this;
    }

    public IMRoomMember getIMRoomMember() {
        return chatRoomMember;
    }

    public IMRoomEvent setIMRoomMember(IMRoomMember chatRoomMember) {
        this.chatRoomMember = chatRoomMember;
        return this;
    }

    public String getReasonMsg() {
        return reasonMsg;
    }

    public IMRoomEvent setReasonMsg(String reasonMsg) {
        this.reasonMsg = reasonMsg;
        return this;
    }

    public int getReasonNo() {
        return reasonNo;
    }

    public IMRoomEvent setReasonNo(int reasonNo) {
        this.reasonNo = reasonNo;
        return this;
    }

    @Override
    public IMRoomEvent setCode(int code) {
        this.code = code;
        return this;
    }

    @Override
    public IMRoomEvent setMicPositionList(List<Integer> micPositionList) {
        this.micPositionList = micPositionList;
        return this;
    }

    public int getCurrentMicPosition() {
        return currentMicPosition;
    }

    public IMRoomEvent setCurrentMicPosition(int currentMicPosition) {
        this.currentMicPosition = currentMicPosition;
        return this;
    }

    public float getCurrentMicStreamLevel() {
        return currentMicStreamLevel;
    }

    public IMRoomEvent setCurrentMicStreamLevel(float currentMicStreamLevel) {
        this.currentMicStreamLevel = currentMicStreamLevel;
        return this;
    }

    public int getDetonatingState() {
        return detonatingState;
    }

    public IMRoomEvent setDetonatingState(int detonatingState) {
        this.detonatingState = detonatingState;
        return this;
    }

    public RoomCharmAttachment getRoomCharmInfo() {
        return roomCharmInfo;
    }

    public IMRoomEvent setRoomCharmInfo(RoomCharmAttachment roomCharmAttachment) {
        this.roomCharmInfo = roomCharmAttachment;
        return this;
    }

        public List<ZegoSoundLevelInfo> getSpeakQueueMembersPosition() {
        return speakQueueMembersPosition;
    }

    public IMRoomEvent setSpeakQueueMembersPosition(List<ZegoSoundLevelInfo> speakQueueMembersPosition) {
        this.speakQueueMembersPosition = speakQueueMembersPosition;
        return this;
    }

    public int getAuctionState() {
        return auctionState;
    }

    public IMRoomEvent setAuctionState(int auctionState) {
        this.auctionState = auctionState;
        return this;
    }

    public RoomAuctionBean getRoomAuctionBean() {
        return roomAuctionBean;
    }

    public IMRoomEvent setRoomAuctionBean(RoomAuctionBean roomAuctionBean) {
        this.roomAuctionBean = roomAuctionBean;
        return this;
    }

    public int getAuctionJoinCount() {
        return auctionJoinCount;
    }

    public IMRoomEvent setAuctionJoinCount(int auctionJoinCount) {
        this.auctionJoinCount = auctionJoinCount;
        return this;
    }

    public RoomRedPacket getRoomRedPacket() {
        return roomRedPacket;
    }

    public IMRoomEvent setRoomRedPacket(RoomRedPacket roomRedPacket) {
        this.roomRedPacket = roomRedPacket;
        return this;
    }

    public String getRedPacketId() {
        return redPacketId;
    }

    public IMRoomEvent setRedPacketId(String redPacketId) {
        this.redPacketId = redPacketId;
        return this;
    }
}
