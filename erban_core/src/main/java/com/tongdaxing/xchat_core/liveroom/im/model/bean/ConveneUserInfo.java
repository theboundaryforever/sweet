package com.tongdaxing.xchat_core.liveroom.im.model.bean;

import java.io.Serializable;

/**
 * @author weihaitao
 * @date 2019/7/16
 */
public class ConveneUserInfo implements Serializable {


    /**
     * 头像
     */
    private String avatar;

    /**
     * 性别， 1:男 2：女 0 ：未知
     */
    private int gender;

    /**
     * 是否已经关注TA
     */
    private boolean isFans;

    /**
     * 昵称
     */
    private String nick;

    /**
     * 成员ID
     */
    private long uid;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public boolean isFans() {
        return isFans;
    }

    public void setFans(boolean fans) {
        isFans = fans;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ConveneUserInfo)) {
            return false;
        }
        boolean isSameGift = false;
        ConveneUserInfo other = (ConveneUserInfo) obj;
        if (other.getUid() == this.getUid()) {
            isSameGift = true;
        }
        return isSameGift;
    }
}
