package com.tongdaxing.xchat_core.liveroom.im.model;

import android.text.TextUtils;
import android.util.SparseArray;

import com.netease.nim.uikit.common.util.string.StringUtil;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.im.IMProCallBack;
import com.tongdaxing.erban.libcommon.im.IMReportBean;
import com.tongdaxing.erban.libcommon.im.IMReportResult;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.JavaUtil;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.GiftAttachment;
import com.tongdaxing.xchat_core.liveroom.im.IMUriProvider;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.AudioConnPayInfoResp;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.AudioConnReqInfoResp;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.ConveneUserInfo;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomEvent;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMember;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMessage;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomOnlineMember;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomQueueInfo;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.MultiRoomInfo;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.SingleAudioRoomEnitity;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.pay.IPayCore;
import com.tongdaxing.xchat_core.result.RoomConsumeInfoListResult;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.queue.bean.RoomConsumeInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Nullable;

import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;

import static com.tongdaxing.erban.libcommon.coremanager.CoreManager.notifyClients;
import static com.tongdaxing.erban.libcommon.im.IMReportBean.CODE_NO_ERROR;
import static com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager.DEFAULT_CODE_ERROR;

/**
 * 房间model
 * 主要负责IM模块的请求方法管理
 *
 * @author zeda
 */
public class IMRoomModel extends BaseIMRoomModel {

    public static final int ERROR_CODE_AUDIO_CHANNEL_INIT = -1102;
    public static final int ERROR_CODE_DEFAULT = 0;

    public IMRoomModel() {

    }

    public void requestRoomInfo(long roomUid, int roomType, HttpRequestCallBack<RoomInfo> callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("roomType", String.valueOf(roomType));
        params.put("queryUid", String.valueOf(roomUid));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));

        OkHttpManager.getInstance().getRequest(UriProvider.getRoomInfo(), params, callBack);
    }

    /**
     * 获取房间管理员列表
     */
    public void getRoomManagers(OkHttpManager.MyCallBack<IMReportResult<List<IMRoomMember>>> myCallBack) {
        OkHttpManager.getInstance().postRequest(IMUriProvider.getRoomManagersUrl(),
                IMRoomMessageManager.get().getImDefaultParamsMap(), myCallBack);
    }

    /**
     * 获取房间黑名单列表
     */
    public void getRoomBlackList(long roomId, int limit, int start,
                                 OkHttpManager.MyCallBack<IMReportResult<List<IMRoomMember>>> myCallBack) {

        Map<String, String> params = IMRoomMessageManager.get().getImDefaultParamsMap();
        if (null == params) {
            return;
        }
        //后端接口暂时没用这两个参数做数据处理,一次返回所有的数据，没有分页操作
        params.put("limit", String.valueOf(limit));
        params.put("start", String.valueOf(start));
        OkHttpManager.getInstance().postRequest(IMUriProvider.getRoomBlackListUrl(),
                params, myCallBack);
    }

    /**
     * 通知服务端离开房间了
     */
    public void notifyServerOnRoomExit(int roomType) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomType", roomType + "");
        OkHttpManager.getInstance().postRequest(UriProvider.userRoomOut(), params, (OkHttpManager.MyCallBack) null);
    }

    /**
     * 获取已经收到的礼物消息
     */
    public List<IMRoomMessage> getGiftMsgList() {
        List<IMRoomMessage> chatRoomMessages = IMRoomMessageManager.get().messages;
        if (ListUtils.isListEmpty(chatRoomMessages)) {
            return null;
        }
        List<IMRoomMessage> messages = new ArrayList<>();
        for (int i = chatRoomMessages.size() - 1; i >= 0; i--) {
            if (chatRoomMessages.get(i).getAttachment() != null) {
                if ((chatRoomMessages.get(i).getAttachment()).getFirst()
                        == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT || (chatRoomMessages.get(i).getAttachment())
                        .getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT) {
                    messages.add(chatRoomMessages.get(i));
                }
            }
        }
        return messages;
    }

    /**
     * 当前uid是否能上这个麦位
     */
    private boolean isMeCanUpMic(int micPosition, String uid, boolean isInviteUpMic) {
        IMRoomQueueInfo roomQueueInfo = RoomDataManager.get().mMicQueueMemberMap.get(micPosition);
        if (roomQueueInfo != null && roomQueueInfo.mRoomMicInfo != null) {
            IMRoomMember chatRoomMember = roomQueueInfo.mChatRoomMember;
            //防止把房主挤掉了
            if (micPosition == RoomDataManager.MIC_POSITION_BY_OWNER && !RoomDataManager.get().isRoomOwner()) {
                return false;
            }
            //坑上没人 且 （没锁 或者 我是房主 或者 我是管理员 或者 我是被邀请上麦的）
            return chatRoomMember == null
                    && ((!roomQueueInfo.mRoomMicInfo.isMicLock() || RoomDataManager.get().isRoomOwner(uid)
                    || RoomDataManager.get().isRoomAdmin(uid)) || isInviteUpMic);
        }
        return false;
    }

    /**
     * 操作上麦
     *
     * @param micPosition   需要上的麦位
     * @param isInviteUpMic 是否是受邀请上麦
     * @param callBack      上麦结果回调
     */
    public void operateUpMicro(int micPosition, String uid, boolean isInviteUpMic, CallBack<String> callBack) {
        if (isMeCanUpMic(micPosition, uid, isInviteUpMic) && getServiceRoomInfo() != null) {
            //如果是被邀请上麦的话，上麦成功后不开麦
            RoomDataManager.get().mIsNeedOpenMic = !isInviteUpMic;
            ReUsedSocketManager.get().updateQueue(String.valueOf(getServiceRoomInfo().getRoomId()), micPosition,
                    JavaUtil.str2long(uid), new IMProCallBack() {
                        @Override
                        public void onSuccessPro(IMReportBean imReportBean) {
                            if (imReportBean == null || imReportBean.getReportData() == null) {
                                if (callBack != null) {
                                    callBack.onFail(-1, "数据异常！");
                                }
                                return;
                            }
                            if (imReportBean.getReportData().errno == CODE_NO_ERROR) {
                                if (callBack != null) {
                                    callBack.onSuccess("上麦成功");
                                }
                            } else {
                                if (callBack != null) {
                                    callBack.onFail(imReportBean.getReportData().errno,
                                            "上麦失败:" + imReportBean.getReportData().errmsg);
                                }
                            }
                        }

                        @Override
                        public void onError(int errorCode, String errorMsg) {
                            if (callBack != null) {
                                callBack.onFail(errorCode, "上麦失败:" + errorMsg);
                            }
                        }
                    });
        }
    }

    /**
     * 操作麦位开麦 或 关麦
     *
     * @param isOpen 是否开麦
     */
    public void operateMicroOpenOrClose(int micPosition, boolean isOpen) {
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (roomInfo != null && (RoomDataManager.get().isRoomAdmin() || RoomDataManager.get().isRoomOwner())) {
            Map<String, String> param = CommonParamUtil.getDefaultParam();
            param.put("position", String.valueOf(micPosition));
            param.put("state", String.valueOf(isOpen ? 0 : 1));
            param.put("roomUid", String.valueOf(roomInfo.getUid()));
            param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
            param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
            param.put("roomType", String.valueOf(roomInfo.getType()));
            OkHttpManager.getInstance().postRequest(UriProvider.operateMicroPhone(), param,
                    (HttpRequestCallBack) null);
        }
    }

    /**
     * 操作麦位锁麦 或 解锁麦
     *
     * @param isLock 是否锁麦
     */
    public void operateMicroLockAndUnLock(int micPosition, boolean isLock, HttpRequestCallBack<String> callBack) {
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (roomInfo != null && (RoomDataManager.get().isRoomAdmin() || RoomDataManager.get().isRoomOwner())) {
            Map<String, String> param = CommonParamUtil.getDefaultParam();
            param.put("position", String.valueOf(micPosition));
            param.put("state", String.valueOf(isLock ? 1 : 0));
            param.put("roomUid", String.valueOf(roomInfo.getUid()));
            param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
            param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
            param.put("roomType", String.valueOf(roomInfo.getType()));
            OkHttpManager.getInstance().postRequest(UriProvider.getlockMicroPhone(), param, callBack);
        } else if (callBack != null) {
            callBack.onFinish();
            callBack.onFailure(DEFAULT_CODE_ERROR, "");
        }
    }

    /**
     * 申请上麦
     */
    public void applyUpMicro(HttpRequestCallBack<Object> callBack) {
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (roomInfo != null) {
            Map<String, String> param = CommonParamUtil.getDefaultParam();
            param.put("roomid", String.valueOf(roomInfo.getRoomId()));
            param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
            param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
            OkHttpManager.getInstance().postRequest(IMUriProvider.getApplyWaitQueueUrl(), param, callBack);
        } else if (callBack != null) {
            callBack.onFinish();
            callBack.onFailure(DEFAULT_CODE_ERROR, "");
        }
    }

    /**
     * 操作开启房间
     *
     * @param roomType 房间类型
     * @param title    房间标题
     * @param roomDesc 房间描述
     * @param backPic  房间背景
     */
    public void operateRoomOpen(int roomType, @Nullable String title, @Nullable String roomPwd,
                                @Nullable String roomDesc, @Nullable String backPic, HttpRequestCallBack<RoomInfo> callBack) {
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomType", String.valueOf(roomType));

        if (!StringUtil.isEmpty(roomPwd)) {
            params.put("roomPwd", roomPwd);
        } else {
            params.put("roomPwd", "");
        }
        if (!StringUtil.isEmpty(title)) {
            params.put("title", title);
        } else {
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(uid);
            if (userInfo != null) {
                params.put("title", userInfo.getNick() + "的房间");
            }
        }
        if (!StringUtil.isEmpty(roomDesc)) {
            params.put("roomDesc", roomDesc);
        } else {
            params.put("roomDesc", "");
        }
        if (!StringUtil.isEmpty(backPic)) {
            params.put("backPic", backPic);
        } else {
            params.put("backPic", "");
        }
        OkHttpManager.getInstance().postRequest(UriProvider.openRoom(), params, callBack);
    }

    /**
     * 邀请上麦
     *
     * @param roomUserInfos 邀请的用户
     * @param isNeedPay     是否需要收费
     */
    public void inviteUpMicroOnVideoRoom(List<RoomConsumeInfo> roomUserInfos, boolean isNeedPay,
                                         HttpRequestCallBack<Object> callBack) {
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (roomInfo != null && !ListUtils.isListEmpty(roomUserInfos)) {
            Map<String, String> param = CommonParamUtil.getDefaultParam();
            StringBuilder inviteUserStrBuilder = new StringBuilder();
            for (int i = 0; i < roomUserInfos.size(); i++) {
                if (i == roomUserInfos.size() - 1) {
                    inviteUserStrBuilder.append(roomUserInfos.get(i).getUid());
                } else {
                    inviteUserStrBuilder.append(roomUserInfos.get(i).getUid()).append(",");
                }
            }
            param.put("micUid", inviteUserStrBuilder.toString());
            param.put("roomid", String.valueOf(roomInfo.getRoomId()));
            param.put("needPay", isNeedPay ? "1" : "0");
            param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
            param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
            OkHttpManager.getInstance().postRequest(IMUriProvider.getInviteUpMicroOnVideoRoomUrl(), param, callBack);
        } else if (callBack != null) {
            callBack.onFinish();
            callBack.onFailure(DEFAULT_CODE_ERROR, "邀请失败");
        }
    }

    /**
     * 邀请进房
     *
     * @param roomUserInfos 邀请的用户
     */
    public void inviteEnterRoom(List<RoomConsumeInfo> roomUserInfos, HttpRequestCallBack<Object> callBack) {
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (roomInfo != null && !ListUtils.isListEmpty(roomUserInfos)) {
            Map<String, String> param = CommonParamUtil.getDefaultParam();
            StringBuilder inviteUserStrBuilder = new StringBuilder();
            for (int i = 0; i < roomUserInfos.size(); i++) {
                if (i == roomUserInfos.size() - 1) {
                    inviteUserStrBuilder.append(roomUserInfos.get(i).getUid());
                } else {
                    inviteUserStrBuilder.append(roomUserInfos.get(i).getUid()).append(",");
                }
            }
            param.put("reqUid", inviteUserStrBuilder.toString());
            param.put("roomid", String.valueOf(roomInfo.getRoomId()));
            param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
            param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
            OkHttpManager.getInstance().postRequest(IMUriProvider.getInviteEnterRoomUrl(), param, callBack);
        } else if (callBack != null) {
            callBack.onFinish();
            callBack.onFailure(DEFAULT_CODE_ERROR, "邀请失败");
        }
    }

    /**
     * 获取房间用户在线列表信息
     */
    public void getRoomMembers(int index, int length, CallBack<List<IMRoomMember>> callBack) {
        Map<String, String> imDefaultParamsMap = IMRoomMessageManager.get().getImDefaultParamsMap();
        if (imDefaultParamsMap == null) {
            return;
        }
        imDefaultParamsMap.put("start", String.valueOf(index));
        imDefaultParamsMap.put("limit", String.valueOf(length));
        OkHttpManager.getInstance().postRequest(IMUriProvider.getFetchRoomMembersUrl(), imDefaultParamsMap, new OkHttpManager.MyCallBack<IMReportResult<List<IMRoomMember>>>() {
            @Override
            public void onError(Exception e) {
                onResponse(null);
            }

            @Override
            public void onResponse(IMReportResult<List<IMRoomMember>> response) {
                if (callBack != null) {
                    List<IMRoomMember> members = response == null || response.getData() == null ? new ArrayList<>() : response.getData();
                    members.add(null);
                    callBack.onSuccess(members);
                }
            }
        });
    }

    /**
     * 送礼物
     *
     * @param targetUid  单人时不能为0
     * @param targetUids 单人时为空、多人时不能为空
     * @param callBack   onSuccess为true代表需要刷新礼物列表
     */
    public void sendGift(int giftId, long targetUid, List<Long> targetUids, int giftNum, CallBack<Boolean> callBack) {
        if (getServiceRoomInfo() == null || (targetUid == 0 && ListUtils.isListEmpty(targetUids))) {
            if (callBack != null) {
                callBack.onFail(DEFAULT_CODE_ERROR, "送礼失败");
            }
        } else {
            Map<String, String> params = CommonParamUtil.getDefaultParam();
            params.put("giftId", giftId + "");
            params.put("uid", getCurrentUserId() + "");
            params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
            params.put("roomUid", getServiceRoomInfo().getUid() + "");
            params.put("giftNum", giftNum + "");
            params.put("roomType", String.valueOf(getServiceRoomInfo().getType()));

            String url;
            if (!ListUtils.isListEmpty(targetUids)) {
                url = UriProvider.sendWholeGiftV3();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < targetUids.size(); i++) {
                    long tUid = targetUids.get(i);
                    sb.append(tUid);
                    sb.append(",");
                }
                sb.deleteCharAt(sb.length() - 1);
                params.put("targetUids", sb.toString());
            } else {
                url = UriProvider.sendGiftV3();
                params.put("targetUid", targetUid + "");
                if (targetUid == getServiceRoomInfo().getUid()) {
                    params.put("type", "1");
                } else {
                    params.put("type", "3");
                }
            }
            OkHttpManager.getInstance().postRequest(url, params, new HttpRequestCallBack<GiftReceiveInfo>() {
                @Override
                public void onSuccess(String message, GiftReceiveInfo data) {
                    CoreManager.getCore(IPayCore.class).minusGold(data.getUseGiftPurseGold());
                    sendGiftMessage(!ListUtils.isListEmpty(targetUids), data);
                    if (callBack != null) {
                        GiftInfo giftInfo = CoreManager.getCore(IGiftCore.class).findGiftInfoById(data.getGiftId());
                        boolean needRefresh = false;
                        int giftNum = 0;
                        if (null != giftInfo) {
                            giftInfo.setUserGiftPurseNum(data.getUserGiftPurseNum());
                            needRefresh = true;
                            giftNum = data.getUserGiftPurseNum();
                        }
                        callBack.onSuccess(needRefresh);
                    }
                }

                @Override
                public void onFailure(int code, String msg) {
                    if (callBack != null) {
                        callBack.onFail(code, msg);
                    }
                }
            });
        }
    }

    /**
     * 发送礼物通知
     *
     * @param isMultiGift     是否是全麦礼物
     * @param giftReceiveInfo 礼物信息
     */
    private void sendGiftMessage(boolean isMultiGift, GiftReceiveInfo giftReceiveInfo) {
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (roomInfo == null || giftReceiveInfo == null) {
            return;
        }
        GiftAttachment giftAttachment = new GiftAttachment(isMultiGift ? CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT : CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT, CustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_GIFT);
        giftAttachment.setUid(getCurrentUserId() + "");
        giftAttachment.setGiftRecieveInfo(giftReceiveInfo);
        IMRoomMessage message = new IMRoomMessage();
        message.setRoomId(roomInfo.getRoomId() + "");
        message.setAttachment(giftAttachment);
        message.setImRoomMember(IMRoomMessageManager.get().getCurrentIMRoomMember());
        ReUsedSocketManager.get().sendCustomMessage(message.getRoomId() + "", message, new IMProCallBack() {
            @Override
            public void onSuccessPro(IMReportBean imReportBean) {
                if (imReportBean != null && imReportBean.getReportData() != null && imReportBean.getReportData().errno == 0) {
                    notifyClients(IMRoomCoreClient.class, IMRoomCoreClient.METHOD_ON_SEND_IMROOM_MESSAGE_SUCCESS, message);
                }
            }

            @Override
            public void onError(int errorCode, String errorMsg) {

            }
        });
    }

    /**
     * 获取microView的position
     */
    public int getMicroViewPositionByMicPosition(int micPosition) {
        return micPosition + 1;//麦位从-1开始
    }

    /**
     * 提交举报
     *
     * @param type       '类型 1. 用户 2. 房间',
     * @param targetUid  对方的uid（如果是房间，传房主UID）
     * @param reportType '举报类型 1.政治 2.色情 3.广告 4.人身攻击 ',
     */
    public void commitReport(int type, long targetUid, int reportType, HttpRequestCallBack<Object> callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("reportType", reportType + "");
        params.put("type", type + "");
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        params.put("phoneNo", userInfo != null ? userInfo.getPhone() : "");
        params.put("reportUid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("uid", String.valueOf(targetUid));
        OkHttpManager.getInstance().postRequest(UriProvider.reportUserUrl(), params, callBack);
    }

    /**
     * 请求当前房主信息
     */
    public void getRoomOwnerInfo(long uid) {
        CoreManager.getCore(IUserCore.class).requestUserInfo(uid, CoreManager.getCore(IAuthCore.class).getCurrentUid() == uid ? 0 : 1);
    }

    public void enterRoomSocket(RoomInfo roomInfo, IMProCallBack imProCallBack) {
//        ReUsedSocketManager.get().enterWithOpenChatRoom(roomUid, roomType, roomPwd, imProCallBack);
    }


    public void enterRoomSocket(long roomUid, int roomType, String roomPwd, IMProCallBack imProCallBack) {
        int reconnect = 0;//0表示正常进房
        ReUsedSocketManager.get().enterWithOpenChatRoom(roomUid, roomType, roomPwd, reconnect, imProCallBack);
    }

    /**
     * 退出房间--IM退房
     *
     * @param callBack 退出结果回调
     */
    public void exitRoom(CallBack<String> callBack) {
        RoomInfo currentRoom = RoomDataManager.get().getCurrentRoomInfo();
        if (currentRoom == null) {
            if (callBack != null) {
                callBack.onFail(0, "");
            }
            return;
        }
        int roomType = currentRoom.getType();
        exitRoomSocket(callBack, currentRoom.getRoomId());
        notifyServerOnRoomExit(roomType);
    }

    public void exitRoomSocket(final CallBack<String> callBack, long roomId) {
        //不处理socket是否退房成功 -- 先清除app内的
        RoomDataManager.get().release();
        IMRoomMessageManager.get().getIMRoomEventObservable()
                .onNext(new IMRoomEvent().setEvent(RoomEvent.ROOM_EXIT));
        ReUsedSocketManager.get().exitRoom(roomId, new IMProCallBack() {
            @Override
            public void onSuccessPro(IMReportBean imReportBean) {
                if (imReportBean.getReportData().errno == 0) {
                    if (callBack != null) {
                        callBack.onSuccess("");
                    }

                } else {
                    if (callBack != null) {
                        callBack.onFail(0, "");
                    }
                }
            }

            @Override
            public void onError(int errorCode, String errorMsg) {
                if (callBack != null) {
                    callBack.onFail(0, "");
                }
            }
        });
    }

    //-------------------对接服务器IM模块所对应的新逻辑----------------------------

    /**
     * 单人房-主播-开播
     *
     * @param roomId
     * @param callBack
     */
    public void startPlayRoom(long roomId, HttpRequestCallBack callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        if (params == null) {
            return;
        }
        IAuthCore iAuthCore = CoreManager.getCore(IAuthCore.class);
        long currentUid = iAuthCore.getCurrentUid();
        String ticket = iAuthCore.getTicket();
        params.put("roomId", roomId + "");
        params.put("uid", currentUid + "");
        params.put("ticket", ticket);
        OkHttpManager.getInstance().postRequest(IMUriProvider.getStartPlayRoomUrl(), params, callBack);
    }

    /**
     * 单人房-主播-停播
     *
     * @param roomId
     * @param callBack
     */
    public void stopPlayRoom(long roomId, HttpRequestCallBack callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        if (params == null) {
            return;
        }
        IAuthCore iAuthCore = CoreManager.getCore(IAuthCore.class);
        long currentUid = iAuthCore.getCurrentUid();
        String ticket = iAuthCore.getTicket();
        params.put("roomId", roomId + "");
        params.put("uid", currentUid + "");
        params.put("ticket", ticket);
        OkHttpManager.getInstance().postRequest(IMUriProvider.getStopPlayRoomUrl(), params, callBack);
    }

    /**
     * 为房主点赞
     *
     * @param roomId
     * @param callBack
     */
    public void admireRoomOwner(long roomId, int roomType, HttpRequestCallBack callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        if (params == null) {
            return;
        }
        IAuthCore iAuthCore = CoreManager.getCore(IAuthCore.class);
        long currentUid = iAuthCore.getCurrentUid();
        String ticket = iAuthCore.getTicket();
        params.put("roomId", roomId + "");
        params.put("roomType", roomType + "");
        params.put("uid", currentUid + "");
        params.put("ticket", ticket);
        OkHttpManager.getInstance().postRequest(IMUriProvider.getRoomAdmireUrl(), params, callBack);
    }

    /**
     * 获取剩余点赞次数
     *
     * @param roomId
     * @param roomType
     * @param callBack
     */
    public void getRoomAdmireCount(long roomId, int roomType, HttpRequestCallBack callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        if (params == null) {
            return;
        }
        IAuthCore iAuthCore = CoreManager.getCore(IAuthCore.class);
        long currentUid = iAuthCore.getCurrentUid();
        String ticket = iAuthCore.getTicket();
        params.put("roomId", roomId + "");
        params.put("roomType", roomType + "");
        params.put("uid", currentUid + "");
        params.put("ticket", ticket);
        OkHttpManager.getInstance().postRequest(IMUriProvider.getRoomAdmireCountUrl(), params, callBack);
    }

    public void getRoomMembers(int index, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> imDefaultParamsMap = IMRoomMessageManager.get().getImDefaultParamsMap();
        if (imDefaultParamsMap == null) {
            return;
        }

        imDefaultParamsMap.put("limit", "100");
        imDefaultParamsMap.put("start", String.valueOf(index));
        OkHttpManager.getInstance().postRequest(IMUriProvider.getFetchRoomMembersUrl(),
                imDefaultParamsMap, myCallBack);
    }


    public List<IMRoomOnlineMember> memberToOnlineMember(List<IMRoomMember> imChatRoomMembers,
                                                         boolean filterOnMic,
                                                         List<IMRoomOnlineMember> oldList) {
        Set<String> micUIds = new HashSet<>();
        //排除麦上的用户
        SparseArray<IMRoomQueueInfo> mMicQueueMemberMap = RoomDataManager.get().mMicQueueMemberMap;
        for (int i = 0; i < mMicQueueMemberMap.size(); i++) {
            IMRoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(i);
            if (roomQueueInfo.mChatRoomMember != null) {
                String account = roomQueueInfo.mChatRoomMember.getAccount();
                micUIds.add(account);
            }
        }
        Set<String> newListUids = new HashSet<>();
        List<IMRoomOnlineMember> imRoomOnlineMembers = new ArrayList<>();

        for (IMRoomMember member : imChatRoomMembers) {
            String account = member.getAccount();
            boolean onMic = micUIds.contains(account);
            //过滤麦上用户
            newListUids.add(account);
            if (onMic && filterOnMic) {
                continue;
            }
            imRoomOnlineMembers.add(new IMRoomOnlineMember(member, onMic, member.isManager(),
                    RoomDataManager.get().isRoomOwner(account)));
        }

        if (!ListUtils.isListEmpty(oldList)) {
            //排重
            List<IMRoomOnlineMember> repeatData = new ArrayList<>();
            for (IMRoomOnlineMember imRoomOnlineMember : oldList) {
                IMRoomMember chatRoomMember = imRoomOnlineMember.imRoomMember;
                if (chatRoomMember == null) {
                    continue;
                }
                String account = chatRoomMember.getAccount();
                if (newListUids.contains(account)) {
                    repeatData.add(imRoomOnlineMember);
                }
            }
            oldList.removeAll(repeatData);
            imRoomOnlineMembers.addAll(0, oldList);
        }
        //房主-管理-游客排序,用户身份有可能会变化，需要重新排序
        Collections.sort(imRoomOnlineMembers, new Comparator<IMRoomOnlineMember>() {
            @Override
            public int compare(IMRoomOnlineMember o1, IMRoomOnlineMember o2) {
                if (o1.isRoomOwer) {
                    return -1;
                }
                if (o2.isRoomOwer) {
                    return 1;
                }
                if (o1.isAdmin == o2.isAdmin) {
                    return -1;
                } else {
                    if (o1.isAdmin) {
                        return -1;
                    } else {
                        return 1;
                    }
                }
            }
        });
        return imRoomOnlineMembers;
    }

    public Single<List<IMRoomOnlineMember>> onMemberDownUpMic(final String account,
                                                              final boolean isUpMic,
                                                              final List<IMRoomOnlineMember> dataList) {
        if (TextUtils.isEmpty(account)) {
            return Single.error(new Throwable("account 不能为空"));
        }
        return Single.create(
                new SingleOnSubscribe<List<IMRoomOnlineMember>>() {
                    @Override
                    public void subscribe(SingleEmitter<List<IMRoomOnlineMember>> e) throws Exception {
                        if (ListUtils.isListEmpty(dataList)) {
                            e.onSuccess(dataList);
                        }
                        int size = dataList.size();
                        for (int i = 0; i < size; i++) {
                            IMRoomOnlineMember onlineChatMember = dataList.get(i);
                            if (onlineChatMember.imRoomMember != null
                                    && Objects.equals(onlineChatMember.imRoomMember.getAccount(), account)) {
                                onlineChatMember.isOnMic = isUpMic;
                            }
                        }
                        e.onSuccess(dataList);
                    }
                })
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<List<IMRoomOnlineMember>> onUpdateMemberManager(final String account,
                                                                  final boolean isRemoveManager,
                                                                  final List<IMRoomOnlineMember> dataList) {
        return Single.create(
                new SingleOnSubscribe<List<IMRoomOnlineMember>>() {
                    @Override
                    public void subscribe(SingleEmitter<List<IMRoomOnlineMember>> e) throws Exception {
                        if (ListUtils.isListEmpty(dataList)) {
                            e.onSuccess(dataList);
                        }
                        int size = dataList.size();
                        for (int i = 0; i < size; i++) {
                            IMRoomOnlineMember onlineChatMember = dataList.get(i);
                            if (onlineChatMember.imRoomMember != null
                                    && Objects.equals(onlineChatMember.imRoomMember.getAccount(), account)) {
                                onlineChatMember.isAdmin = !isRemoveManager;
                            }
                        }
                        e.onSuccess(dataList);
                    }
                })
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public void getRoomConsumeList(long roomUid, int roomType, int dataType,
                                   int type, OkHttpManager.MyCallBack<RoomConsumeInfoListResult> callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(roomUid));
        params.put("roomType", String.valueOf(roomType));
        params.put("dataType", String.valueOf(dataType));
        params.put("type", String.valueOf(type));
        OkHttpManager.getInstance().getRequest(UriProvider.getRoomContributionListUrl(), params, callBack);
    }

    public void getRoomTipsList(HttpRequestCallBack<List<String>> callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        OkHttpManager.getInstance().postRequest(UriProvider.getRoomTipsUrl(), params, callBack);
    }

    public void sendRoomTipsMsg(HttpRequestCallBack<Boolean> callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        OkHttpManager.getInstance().postRequest(UriProvider.getRoomTipsSendUrl(), params, callBack);
    }

    public void getRoomAudioConnReqList(long roomId, HttpRequestCallBack<AudioConnReqInfoResp> httpRequestCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("roomId", roomId + "");
        OkHttpManager.getInstance().postRequest(UriProvider.getApplyList(), params, httpRequestCallBack);
    }

    public void connAudioReq(long targetUid, long roomId, HttpRequestCallBack<String> httpRequestCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("targetUid", targetUid + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("roomId", roomId + "");
        OkHttpManager.getInstance().postRequest(UriProvider.getConnAudioReq(), params, httpRequestCallBack);
    }

    public void callDownAudioConn(long targetUid, long roomId, HttpRequestCallBack<String> httpRequestCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("targetUid", targetUid + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("roomId", roomId + "");
        OkHttpManager.getInstance().postRequest(UriProvider.getCallDownAudioConn(), params, httpRequestCallBack);
    }

    public void reqAudioConn(String applyContent, long goldNum, long roomId, HttpRequestCallBack<String> httpRequestCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("roomId", roomId + "");
        params.put("goldNum", goldNum + "");
        params.put("applyContent", applyContent);
        OkHttpManager.getInstance().postRequest(UriProvider.getReqAudioConn(), params, httpRequestCallBack);
    }

    public void cancelAudioConnReq(long roomId, HttpRequestCallBack<String> httpRequestCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("roomId", roomId + "");
        OkHttpManager.getInstance().postRequest(UriProvider.getCancelAudioConnReq(), params, httpRequestCallBack);
    }

    public void getRoomApplyInfo(long roomId, HttpRequestCallBack<AudioConnPayInfoResp> httpRequestCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("roomId", roomId + "");
        OkHttpManager.getInstance().postRequest(UriProvider.getRoomApplyInfo(), params, httpRequestCallBack);
    }

    public void editAudioConnReqReason(String applyContent, long roomId, HttpRequestCallBack<String> httpRequestCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("roomId", roomId + "");
        params.put("applyContent", applyContent);
        OkHttpManager.getInstance().postRequest(UriProvider.getEditApplyInfo(), params, httpRequestCallBack);
    }

    public void changeAudioConnSwitch(boolean openState, long roomId, HttpRequestCallBack<String> httpRequestCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("roomId", roomId + "");
        params.put("openState", openState ? "1" : "0");
        OkHttpManager.getInstance().postRequest(UriProvider.getAudioConnSwitch(), params, httpRequestCallBack);
    }

    /**
     * 获取召集令召集的成员列表
     *
     * @param pageNum
     * @param pageSize
     * @param roomId
     * @param httpRequestCallBack
     */
    public void getConveneUserList(int pageNum, int pageSize, long roomId, HttpRequestCallBack<List<ConveneUserInfo>> httpRequestCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("roomId", roomId + "");
        params.put("pageNum", pageNum + "");
        params.put("pageSize", pageSize + "");
        OkHttpManager.getInstance().postRequest(UriProvider.getConveneUserList(), params, httpRequestCallBack);
    }

    /**
     * 取消已经发布的召集令
     *
     * @param roomId
     * @param httpRequestCallBack
     */
    public void cancelConvene(long roomId, HttpRequestCallBack<String> httpRequestCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("roomId", roomId + "");
        OkHttpManager.getInstance().postRequest(UriProvider.getCancelConvene(), params, httpRequestCallBack);
    }

    /**
     * 召集令进房统计
     *
     * @param roomId
     * @param httpRequestCallBack
     */
    public void inRoomFromConvene(long roomId, HttpRequestCallBack<String> httpRequestCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("roomId", roomId + "");
        OkHttpManager.getInstance().postRequest(UriProvider.getInRoomFromConvene(), params, httpRequestCallBack);
    }

    /**
     * 单人房，推荐房间列表
     *
     * @param roomId
     * @param httpRequestCallBack
     */
    public void getRecommendList(long roomId, HttpRequestCallBack<List<SingleAudioRoomEnitity>> httpRequestCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("roomId", roomId + "");
        OkHttpManager.getInstance().postRequest(UriProvider.getRecommendList(), params, httpRequestCallBack);
    }

    /**
     * 新多人房房，退出房间前的引导房间列表
     *
     * @param roomId
     * @param httpRequestCallBack
     */
    public Call<ResponseBody> getReturnRoomList(long roomId, HttpRequestCallBack<List<SingleAudioRoomEnitity>> httpRequestCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("roomId", roomId + "");
        return OkHttpManager.getInstance().postRequest(UriProvider.getReturnRoomList(), params, httpRequestCallBack);
    }

    public void getMultiRoomInfo(long roomId, HttpRequestCallBack<MultiRoomInfo> httpRequestCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("roomId", roomId + "");
        OkHttpManager.getInstance().postRequest(UriProvider.getMultiRoomInfo(), params, httpRequestCallBack);
    }
}
