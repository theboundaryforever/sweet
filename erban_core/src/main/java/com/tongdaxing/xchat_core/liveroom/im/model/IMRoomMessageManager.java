package com.tongdaxing.xchat_core.liveroom.im.model;

import android.annotation.SuppressLint;
import android.text.TextUtils;
import android.util.SparseArray;

import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.chatroom.ChatRoomService;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.im.IMError;
import com.tongdaxing.erban.libcommon.im.IMHeartBeatDataListener;
import com.tongdaxing.erban.libcommon.im.IMKey;
import com.tongdaxing.erban.libcommon.im.IMNoticeMsgListener;
import com.tongdaxing.erban.libcommon.im.IMProCallBack;
import com.tongdaxing.erban.libcommon.im.IMReportBean;
import com.tongdaxing.erban.libcommon.im.IMReportRoute;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.ninepatchloader.NinePatchBitmapLoader;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.utils.json.JsonParser;
import com.tongdaxing.xchat_core.PreferencesUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RoomMicInfo;
import com.tongdaxing.xchat_core.bean.RoomRedPacket;
import com.tongdaxing.xchat_core.bean.RoomRedPacketFinish;
import com.tongdaxing.xchat_core.bean.attachmsg.RoomQueueMsgAttachment;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.gift.MultiGiftReceiveInfo;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCoreClient;
import com.tongdaxing.xchat_core.im.custom.bean.AuctionAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.AuctionJoinCountAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.CallUpAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.CallUpNumUpdateAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.DetonateGiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.FaceAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.GiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachParser;
import com.tongdaxing.xchat_core.im.custom.bean.LotteryBoxAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.MicInListAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.MultiGiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.PkCustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomCharmAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomHotRankUpdateAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomRedPacketAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomRedPacketFinishAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomRuleAttachment;
import com.tongdaxing.xchat_core.liveroom.im.IDisposableAddListener;
import com.tongdaxing.xchat_core.liveroom.im.IMSendCallBack;
import com.tongdaxing.xchat_core.liveroom.im.IMUriProvider;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomEvent;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMember;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMemberComeInfo;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMessage;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomQueueInfo;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.manager.RtcEngineManager;
import com.tongdaxing.xchat_core.noble.NobleBusinessManager;
import com.tongdaxing.xchat_core.pk.bean.PkVoteInfo;
import com.tongdaxing.xchat_core.room.auction.RoomAuctionBean;
import com.tongdaxing.xchat_core.room.bean.RoomAdditional;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.face.IFaceCore;
import com.tongdaxing.xchat_core.room.model.AvRoomModel;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import io.agora.rtc.Constants;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.processors.PublishProcessor;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

import static com.tongdaxing.erban.libcommon.coremanager.CoreManager.notifyClients;
import static com.tongdaxing.xchat_core.Constants.NOBLE_INVISIABLE_ENTER_ROOM;
import static com.tongdaxing.xchat_core.Constants.USER_MEDAL_DATE;
import static com.tongdaxing.xchat_core.Constants.USER_MEDAL_ID;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_INVITE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_KICK;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_CLOSE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_OPEN;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_CLOSE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_OPEN;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_ROOM_RED_PACKET_FINISH;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_SECOND_ROOM_ADMIRE_ANIM;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_SECOND_ROOM_ADMIRE_MSG;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_SECOND_ROOM_LIVE_START;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_SECOND_ROOM_LIVE_STOP;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_SUB_TYPE_FACE_SEND;

/**
 * <p>云信聊天室管理，一个全局的Model </p>
 *
 * @author jiahui
 * @date 2017/12/12
 */
public final class IMRoomMessageManager {

    private final String TAG = IMRoomMessageManager.class.getSimpleName();

    private static final Object SYNC_OBJECT = new Object();
    private static volatile IMRoomMessageManager sManager;
    private final AvRoomModel model;
    public List<IMRoomMessage> messages;
    private int beforeDisConnectionMuteStatus; // 0 默认状态 1是断网前静音，-1是断网前非静音
    private boolean imRoomConnection = true;//房间IM连接状态
    private PublishProcessor<IMRoomEvent> roomProcessor;
    private PublishSubject<IMRoomMessage> msgProcessor;

    private IMRoomMessageManager() {
        registerIMHeartBeatDataListener();
        registerInComingRoomMessage();
        messages = new ArrayList<>();
        model = new AvRoomModel();
    }

    public static IMRoomMessageManager get() {
        if (sManager == null) {
            synchronized (SYNC_OBJECT) {
                if (sManager == null) {
                    sManager = new IMRoomMessageManager();
                }
            }
        }
        return sManager;
    }

    /**
     * 注册socket心跳数据包基本请求数据监听器
     */
    private void registerIMHeartBeatDataListener() {
        ReUsedSocketManager.get().setImHeartBeatDataListener(new IMHeartBeatDataListener() {
            @Override
            public String getHeartBeatData() {
                Json json = new Json();
                RoomInfo cRoomInfo = BaseRoomServiceScheduler.getCurrentRoomInfo();
                if (null != cRoomInfo) {
                    json.set("room_id", cRoomInfo.getRoomId());
                }
                if (null != CoreManager.getCore(IAuthCore.class)) {
                    json.set("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid());
                }
                return json.toString();
            }
        });
    }

    private void registerInComingRoomMessage() {
        ReUsedSocketManager.get().setImNoticeMsgListener(new IMNoticeMsgListener() {

            @Override
            public void onNotice(Json json) {
                LogUtils.d(TAG, "onNotice-json:" + json);
                dealIMRoomMessage(json);
            }

            /**
             * 重连之后先登录im->进入房间成功回调调用的方法
             */
            @Override
            public void onDisConnectEnterRoomSuc() {
                LogUtil.d(RoomDataManager.TAG, "onReconnection");
                imRoomConnection = true;
                if (RoomDataManager.get().isOnMic(CoreManager.getCore(IAuthCore.class).getCurrentUid())) {
                    IMRoomQueueInfo imRoomQueueInfo = RoomDataManager.get().getRoomQueueMemberInfoMyself();
                    if (RoomDataManager.get().getCurrentRoomInfo() != null
                            && imRoomQueueInfo != null
                            && imRoomQueueInfo.mRoomMicInfo != null
                            && !imRoomQueueInfo.mRoomMicInfo.isMicMute()
                            && !imRoomQueueInfo.mRoomMicInfo.isMicLock()) {
                        //恢复断网前的静音状态
                        if (beforeDisConnectionMuteStatus == -1) {
                            RtcEngineManager.get().setMute(false);
                        }
                        if (beforeDisConnectionMuteStatus == 1) {
                            RtcEngineManager.get().setMute(true);
                        }
                    }
                }
                beforeDisConnectionMuteStatus = 0;
                noticeRoomConnect(RoomDataManager.get().getCurrentRoomInfo());
                RoomDataManager.get().setApplyUpMicro(false);//重连成功后可以再次申请上麦
            }

            @Override
            public void onDisConnectEnterRoomFail(int errorCode, String errorMsg) {
                if (errorCode == IMError.IM_USER_IN_ROOM_BLACK_LIST) {
                    // 提示被踢出的原因（聊天室已解散、被管理员踢出、被其他端踢出等)
                    noticeKickOutChatMember(errorCode, errorMsg, String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
                    // 清空缓存数据
                    RoomDataManager.get().release();
                }
            }

            @Override
            public void onDisConnection(boolean isCloseSelf) {
                imRoomConnection = false;
//                RtcEngineManager.get().leaveChannel();
                LogUtil.d(RoomDataManager.TAG, "onDisConnection");
                //异常断开im保存静音状态
                if (!isCloseSelf) {
                    beforeDisConnectionMuteStatus = RtcEngineManager.get().isMute() ? 1 : -1;
                    if (RoomDataManager.get().getCurrentRoomInfo() != null) {
                        RtcEngineManager.get().setMute(true);
                    }
                } else {
                    beforeDisConnectionMuteStatus = 0;
                }
            }

            @Override
            public void onLoginError(int err_code, String reason) {
                LogUtil.d(RoomDataManager.TAG, "onLoginError err_code:" + err_code + " reason:" + reason);
                if (err_code == IMError.IM_ERROR_LOGIN_AUTH_FAIL || err_code == IMError.IM_ERROR_GET_USER_INFO_FAIL) {
                    Json req_data = new Json();
                    req_data.set("errno", err_code);
                    req_data.set("errmsg", reason);
                    onKickOffLogin(req_data);
                    SingleToastUtil.showToast(reason);
                }
            }

            @Override
            public void onDisConntectIMLoginSuc() {
                LogUtil.d(RoomDataManager.TAG, "onDisConntectIMLoginSuc");
                noticeIMConnectLoginSuc();
            }
        });
    }


    //******************************************************** 接收各种消息的处理逻辑 **********************************************//

    private void dealIMRoomMessage(Json json) {
        if (json != null) {
            String route = json.str(IMKey.route);
            Json reqData = json.json(IMKey.req_data);
            if (!TextUtils.isEmpty(route) && reqData != null) {
                IMRoomMessage msg = new IMRoomMessage();
                msg.setRoute(route);
                msg.setRoomId(reqData.str(IMKey.room_id, "-1"));
                //TODO 后面如果业务需求需要接受全局的自定义消息时，需要另外开一个路由，不能同sendMessageReport、sendPublicMsgNotice混合
                if (IMReportRoute.sendMessageReport.equalsIgnoreCase(route) ||
                        IMReportRoute.sendPublicMsgNotice.equalsIgnoreCase(route)) {
                    //处理自定义消息
                    String custom = reqData.str(IMKey.custom);
                    LogUtils.d(TAG, "dealIMRoomMessage-custom:" + custom);

                    MsgAttachment msgAttachment = IMCustomAttachParser.parseCustomAttach(custom);
                    if (null == msgAttachment) {
                        return;
                    }
                    if (msgAttachment instanceof CustomAttachment) {
                        CustomAttachment customAttachment = (CustomAttachment) msgAttachment;
                        if (!(customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_FIRST_CALL_UP
                                && (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_CALL_UP
                                || customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_PUBLIC_CHAT_ROOM
                                || customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_PRIVATE_CHAT_MSG))
                                && RoomDataManager.get().getCurrentRoomInfo() == null) {
                            //增加getCurrentRoomInfo非空判断是为了避免，在进房拿历史消息时已经接收到其他消息，导致NPE
                            //这里需要避免召集令无法接收的问题
                            return;
                        }
                    }
                    if (reqData.has(IMKey.member)) {
                        parseCusttomMember(reqData, msg);
                    }
                    if (msgAttachment instanceof GiftAttachment) {
                        GiftAttachment giftAttachment = (GiftAttachment) msgAttachment;
                        msg.setAttachment(giftAttachment);
                        dealRoomCustomMessage(msg, giftAttachment);
                    } else if (msgAttachment instanceof MultiGiftAttachment) {
                        MultiGiftAttachment multiGiftAttachment = (MultiGiftAttachment) msgAttachment;
                        msg.setAttachment(multiGiftAttachment);
                        dealRoomCustomMessage(msg, multiGiftAttachment);
                    } else if (msgAttachment instanceof RoomQueueMsgAttachment) {
                        dealRoomCustomMessage(msg, (CustomAttachment) msgAttachment);
                    } else if (msgAttachment instanceof LotteryBoxAttachment) {
                        LotteryBoxAttachment lotteryBoxAttachment = (LotteryBoxAttachment) msgAttachment;
                        msg.setAttachment(lotteryBoxAttachment);
                        dealRoomCustomMessage(msg, lotteryBoxAttachment);
                    } else if (msgAttachment instanceof FaceAttachment) {
                        FaceAttachment customAttachment = (FaceAttachment) msgAttachment;
                        msg.setAttachment(customAttachment);
                        dealRoomCustomMessage(msg, customAttachment);
                    } else if (msgAttachment instanceof CustomAttachment) {
                        msg.setAttachment((CustomAttachment) msgAttachment);
                        dealRoomCustomMessage(msg, msg.getAttachment());
                    }
                }
                if (route.equals(IMReportRoute.kickoff)) {
                    //服务器提示将断开socket通知
                    onKickOffLogin(reqData);
                    return;
                }
                if (RoomDataManager.get().getCurrentRoomInfo() == null) {
                    //增加getCurrentRoomInfo非空判断是为了避免，在进房拿历史消息时已经接收到其他消息，导致NPE
                    //需要将kickoff排除
                    return;
                }
                switch (route) {
                    case IMReportRoute.sendTextReport:
                        //文本消息转发通知
                        dealWithTxtMessage(msg, reqData);
                        break;
                    case IMReportRoute.chatRoomMemberIn:
                        //用户进入聊天室通知
                        chatRoomMemberIn(msg, reqData);
                        break;
                    case IMReportRoute.chatRoomMemberExit:
                        //用户退出聊天室通知
                        chatRoomMemberExit(reqData);
                        break;
                    case IMReportRoute.QueueMemberUpdateNotice:
                    case IMReportRoute.AudioConnMicQueueUpdateNotice://单人房4坑位麦序更新
                        //队列成员被更新通知
                        chatRoomQueueChangeNotice(reqData);
                        break;
                    case IMReportRoute.ChatRoomMemberKicked:
                        //踢除特定成员通知
                        onKickedOutRoom(reqData);
                        break;
                    case IMReportRoute.AudioConnMicQueueMicUpdateNotice://更新坑位信息通知(单人房4坑位)
                    case IMReportRoute.QueueMicUpdateNotice://更新坑位信息通知
                        roomQueueMicUpdate(reqData);
                        break;
                    case IMReportRoute.ChatRoomInfoUpdated:
                        //聊天室信息更新通知
                        chatRoomInfoUpdate(reqData);
                        break;
                    case IMReportRoute.ChatRoomMemberBlackAdd:
                        //聊天室黑名单添加通知
                        //这里导致拉黑踢出不显示提示
                        addBlackList(reqData);
                        break;
                    case IMReportRoute.ChatRoomManagerAdd:
                        //聊天室管理员添加通知
                        addManagerMember(msg, reqData);
                        break;
                    case IMReportRoute.ChatRoomManagerRemove:
                        //聊天室管理员移除通知
                        dealWithRemoveManager(reqData);
                        break;
                    case IMReportRoute.passmicNotice:
                        noticeInviteUpMicOnVideo(reqData);
                        break;
                    case IMReportRoute.requestNoRoomUserNotice:
                        noticeInviteEnterRoom(reqData);
                        break;
                    case IMReportRoute.applyWaitQueueNotice:
                        noticeApplyWaitQueue(reqData);
                        break;
                    case IMReportRoute.ChatRoomMemberBlackRemove:
                    default:
                        break;
                }
            }
        }
    }

    /**
     * 处理房间IM自定义消息
     *
     * @param msg
     * @param customAttachment
     */
    private void dealRoomCustomMessage(IMRoomMessage msg, CustomAttachment customAttachment) {
        switch (customAttachment.getFirst()) {
            case CustomAttachment.CUSTOM_MSG_FIRST_AUDIO_MIC_CONN:
                if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_AUDIO_MIC_SWITCH_CHANGE) {
                    //麦序开关通知，暂时不用以event方式通知界面层
                    if (null != customAttachment.getData() && customAttachment.getData().containsKey("openState")) {
                        RoomDataManager.get().singleAudioAnchorConnSwitch = customAttachment.getData().getInteger("openState") == 1;
                    }
                }
                if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_AUDIO_CONN_REQ_NEW) {
                    //通知主播，语音连麦，申请列表数量有变
                    if (null != customAttachment.getData() && customAttachment.getData().containsKey("applyConnectCount")) {
                        RoomDataManager.get().singleAudioConnReqNum = customAttachment.getData().getLong("applyConnectCount");
                        getIMRoomEventObservable().onNext(new IMRoomEvent()
                                .setEvent(IMRoomEvent.ROOM_AUDIO_CONN_REQ_RECV_NEW));
                    }
                }
                if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_AUDIO_CONN_RECV_UP_MIC) {
                    //通知用户，主播连接申请
                    String message = "";
                    if (null != customAttachment.getData() && customAttachment.getData().containsKey("message")) {
                        message = customAttachment.getData().getString("message");
                    }
                    //关闭窗口，toast提示
                    getIMRoomEventObservable().onNext(new IMRoomEvent()
                            .setEvent(IMRoomEvent.ROOM_AUDIO_CONN_REQ_CONN_BY_ANCHOR)
                            .setAudioConnOperaTips(message));
                }
                if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_AUDIO_CONN_USER_CALL_DOWN) {
                    String message = "";
                    if (null != customAttachment.getData() && customAttachment.getData().containsKey("message")) {
                        message = customAttachment.getData().getString("message");
                    }
                    //通知主播，用户挂断连接
                    getIMRoomEventObservable().onNext(new IMRoomEvent()
                            .setEvent(IMRoomEvent.ROOM_AUDIO_CONN_USER_CALL_DOWN_NOTIFY)
                            .setAudioConnOperaTips(message));
                }
                if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_AUDIO_CONN_ANCHOR_CALL_DOWN) {
                    //通知用户，主播挂断连接
                    String message = "";
                    if (null != customAttachment.getData() && customAttachment.getData().containsKey("message")) {
                        message = customAttachment.getData().getString("message");
                    }
                    //通知用户，主播挂断连接
                    getIMRoomEventObservable().onNext(new IMRoomEvent()
                            .setEvent(IMRoomEvent.ROOM_AUDIO_CONN_ANCHOR_CALL_DOWN_NOTIFY)
                            .setAudioConnOperaTips(message));
                }
                break;
            case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE:
                //麦序列表相关自定义消息
                dealWithMicQueueMessage(customAttachment, msg);
                break;
            case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT:
                //单个礼物消息
                ArrayList<IMRoomMessage> messages1 = new ArrayList<>();
                messages1.add(msg);
                addMessages(msg);
                CoreManager.getCore(IGiftCore.class).onReceiveIMRoomMessages(messages1);
                break;
            case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT:
                //多个和全麦礼物
                ArrayList<IMRoomMessage> messages2 = new ArrayList<>();
                messages2.add(msg);
                addMessages(msg);
                CoreManager.getCore(IGiftCore.class).onReceiveIMRoomMessages(messages2);
                break;
            case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUPER_GIFT:
                //全服消息类型
                ArrayList<IMRoomMessage> messages3 = new ArrayList<>();
                messages3.add(msg);
                //全服消息不在公屏展示
//                addMessages(msg);
                CoreManager.getCore(IGiftCore.class).onReceiveIMRoomMessages(messages3);
                break;
            case CustomAttachment.CUSTOM_MSG_FIRST_PUBLIC_CHAT_ROOM:
                //这种写法，避免旧版本没有当前消息second类型，但是消息列表会出现空的消息,有新的second需要在这里新增
                if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_FIRST_PUBLIC_CHAT_ROOM
                        || customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_PUBLIC_ROOM_SEND
                        || customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_PUBLIC_ROOM_RECEIVE) {
                    addMessages(msg);
                    //分发礼物消息以播放礼物动画
                    CoreManager.getCore(IGiftCore.class).onReceiveIMRoomMessages(messages);
                }
                break;
            case CustomAttachment.CUSTOM_MSG_ROOM_RICH_TXT:
            case CustomAttachment.CUSTOM_MSG_ROOM_SYSTEM_RICH_TXT:
                //富文本
            case CustomAttachment.CUSTOM_MSG_ROOM_OPERA_TIPS:
                //分享、关注
            case CustomAttachment.CUSTOM_MSG_FIRST_ROOM_ATTENTION_TIPS:
                //关注房间公屏提示消息
            case CustomAttachment.CUSTOM_MSG_LOTTERY_BOX:
                //砸蛋
            case CustomAttachment.CUSTOM_MSG_TYPE_BURST_GIFT:
                //爆出礼物
                addMessages(msg);
                break;
            case CustomAttachment.CUSTOM_MSG_FIRST_ROOM_LIVE_NOTIFY:
                if (customAttachment.getSecond() == CUSTOM_MSG_SECOND_ROOM_LIVE_START) {
                    //2.开始计时,同时显示直播时长控件
                    if (null != RoomDataManager.get().getAdditional()) {
                        RoomAdditional roomAdditional = RoomDataManager.get().getAdditional();
                        roomAdditional.setPlayTime(0L);
                        roomAdditional.setPlayState(1);
                        RoomDataManager.get().setAdditional(roomAdditional);
                        RoomAdmireTimeCounter.getInstance().startCount();
                    }
                    //主播开播
                    getIMRoomEventObservable().onNext(new IMRoomEvent()
                            .setEvent(IMRoomEvent.ROOM_RECEIVE_LIVE_START));
                    //已开播（针对断线重连场景），则显示停播按钮，显示直播时长控件，并继续计时
                    RoomOwnerLiveTimeCounter.getInstance().setTimeCount(0L);
                    RoomOwnerLiveTimeCounter.getInstance().startCount();
                } else if (customAttachment.getSecond() == CUSTOM_MSG_SECOND_ROOM_LIVE_STOP) {
                    //2.停止计时,同时显示直播时长控件
                    if (null != RoomDataManager.get().getAdditional()) {
                        RoomAdditional roomAdditional = RoomDataManager.get().getAdditional();
                        roomAdditional.setPlayTime(0L);
                        roomAdditional.setPlayState(0);
                        RoomDataManager.get().setAdditional(roomAdditional);
                        RoomAdmireTimeCounter.getInstance().stopCount();
                    }
                    //主播停播
                    getIMRoomEventObservable().onNext(new IMRoomEvent()
                            .setEvent(IMRoomEvent.ROOM_RECEIVE_LIVE_STOP));
                    //未开播，则显示开播按钮，隐藏直播时长控件，关闭计时器
                    RoomOwnerLiveTimeCounter.getInstance().stopCount();
                    //停播，则停止所有计时
                    AudioConnTimeCounter.getInstance().release();
                }
                break;
            case CustomAttachment.CUSTOM_MSG_FIRST_ROOM_ADMIRE_NOTIFY:
                if (customAttachment.getSecond() == CUSTOM_MSG_SECOND_ROOM_ADMIRE_ANIM) {
                    getIMRoomEventObservable().onNext(new IMRoomEvent()
                            .setEvent(IMRoomEvent.ROOM_ADMIRE_ANIM)
                            .setAdmireAnimIconUrl(customAttachment.getData().getString("giftUrl")));
                }
                if (customAttachment.getSecond() == CUSTOM_MSG_SECOND_ROOM_ADMIRE_MSG) {
                    addMessages(msg);
                }
                break;
            case CustomAttachment.CUSTOM_MSG_ROOM_HOT_UPDATE:
                RoomHotRankUpdateAttachment roomHotRankUpdateAttachment = (RoomHotRankUpdateAttachment) customAttachment;
                getIMRoomEventObservable().onNext(new IMRoomEvent()
                        .setEvent(IMRoomEvent.ROOM_RECEIVE_HOT_RANK_UPDATE)
                        .setHotScore(roomHotRankUpdateAttachment.getHotScore())
                        .setHotRank(roomHotRankUpdateAttachment.getHotRank()));
                break;
            case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_FACE:
                if (customAttachment.getSecond() == CUSTOM_MSG_SUB_TYPE_FACE_SEND) {
                    CoreManager.getCore(IFaceCore.class).onImSendRoomMessageSuccess(msg);
                }
                break;
            case CustomAttachment.CUSTOM_MSG_FIRST_CALL_UP:
                if (customAttachment instanceof CallUpAttachment) {
                    //主播发布召集令，首页显示召集令详情
                    if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_CALL_UP) {
                        CallUpAttachment attachment = (CallUpAttachment) customAttachment;
                        IMNetEaseManager.get().getChatRoomEventObservable().onNext(new IMRoomEvent()
                                .setEvent(IMRoomEvent.CALL_UP_MSG).setCallUpBean(attachment.getDataInfo()));
                    }

                    //主播取消召集令
                    if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_CALL_UP_CANCEL_NOTIFY) {
                        RoomDataManager.get().conveneState = 0;
                        getIMRoomEventObservable().onNext(new IMRoomEvent()
                                .setEvent(IMRoomEvent.ROOM_CALL_UP_NUM_NOTIFY));
                    }
//
//                    //新多人房房间，召集令广播大厅消息，通过socket来通信
//                    if(customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_PUBLIC_CHAT_ROOM){
//                        CallUpAttachment attachment = (CallUpAttachment) customAttachment;
//                        if(null != attachment){
//                            ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
//                                    String.valueOf(PublicChatRoomController.getPublicChatRoomId()), attachment);
//                            IMNetEaseManager.get().addMessagesImmediately(message);
//                        }
//                    }
                }
                if (customAttachment instanceof CallUpNumUpdateAttachment) {
                    //召集令召集人数更新通知
                    if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_CALL_UP_NUM_NOTIFY) {
                        CallUpNumUpdateAttachment callUpNumUpdateAttachment = (CallUpNumUpdateAttachment) customAttachment;
                        if (null != callUpNumUpdateAttachment) {
                            RoomDataManager.get().conveneUserCount = callUpNumUpdateAttachment.getConveneUserCount();
                        }
                        getIMRoomEventObservable().onNext(new IMRoomEvent().setEvent(IMRoomEvent.ROOM_CALL_UP_NUM_NOTIFY));
                    }
                }
                break;

            case CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK:
                addMessages(msg);
                break;
            case CustomAttachment.CUSTOM_MSG_ROOM_CHARM:
                noticeRoomCharmUpadate((RoomCharmAttachment) customAttachment);
                break;
            case CustomAttachment.CUSTOM_MSG_ROOM_DETONATING_GIFT:
                if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_ROOM_DETONATING_GIFT_PROGRESS) {
                    DetonateGiftAttachment detonateGiftAttachment = (DetonateGiftAttachment) msg.getAttachment();
                    if (null != detonateGiftAttachment && RoomDataManager.get().getCurrentRoomInfo() != null) {
                        RoomDataManager.get().updateAdditionalDetonating(detonateGiftAttachment);
                        noticePopularGiftUpadate(detonateGiftAttachment.getDetonatingState());
                    }
                } else if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_ROOM_DETONATING_GIFT_NOTIFY) {
                    DetonateGiftAttachment detonateGiftAttachment = (DetonateGiftAttachment) msg.getAttachment();
                    if (null != detonateGiftAttachment && RoomDataManager.get().getCurrentRoomInfo() != null) {
                        //同一个房间才显示引爆球爆出效果以及更新信息
                        if (detonateGiftAttachment.getRoomUid() == RoomDataManager.get().getCurrentRoomInfo().getUid()
                                && detonateGiftAttachment.getRoomType() == RoomDataManager.get().getCurrentRoomInfo().getType()) {
                            RoomDataManager.get().updateAdditionalDetonating(detonateGiftAttachment);
                            noticePopularGiftUpadate(detonateGiftAttachment.getDetonatingState());
                        }
                    }
                    noticeRoomFloatingBubbleUpadate(msg);
                }
                break;
            case CustomAttachment.CUSTOM_MSG_FIRST_AUCTION:
                if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_AUCTION_START
                        || customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_AUCTION_UPDATE
                        || customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_AUCTION_END) {
                    if (customAttachment instanceof AuctionAttachment) {
                        AuctionAttachment auctionAttachment = (AuctionAttachment) customAttachment;
                        if (auctionAttachment.getRoomAuctionBean() != null && RoomDataManager.get().getCurrentRoomInfo() != null
                                && auctionAttachment.getRoomAuctionBean().getRoomId() == RoomDataManager.get().getCurrentRoomInfo().getRoomId()) {
                            if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_AUCTION_END) {
                                RoomDataManager.get().setAuction(null);
                            } else {
                                RoomDataManager.get().setAuction(auctionAttachment.getRoomAuctionBean());
                            }
                            noticeRoomAuctionNotice(customAttachment.getSecond(), auctionAttachment.getRoomAuctionBean());
                        }
                    }
                    if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_AUCTION_START
                            ||customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_AUCTION_END){
                        addMessages(msg);
                    }
                } else if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_AUCTION_PARTICIPATE_COUNT) {
                    if (customAttachment instanceof AuctionJoinCountAttachment) {
                        noticeRoomJoinAuctionCountNotice(customAttachment.getSecond(), ((AuctionJoinCountAttachment) customAttachment).getCount());
                    }
                }
                break;
            case CustomAttachment.CUSTOM_MSG_ROOM_LOVER_UP_MIC:
                if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_ROOM_LOVER_UP_MIC) {
                    noticeRoomFloatingBubbleUpadate(msg);
                }
                break;
            case CustomAttachment.CUSTOM_MSG_BOSON_FIRST:
                if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_BOSON_UP_MACRO) {
                    noticeRoomFloatingBubbleUpadate(msg);
                }
                break;
            case CustomAttachment.CUSTOM_MSG_ROOM_RED_PACKET:
                //发送红包消息
                if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_ROOM_RED_PACKET_RECEIVE) {
                    if (customAttachment instanceof RoomRedPacketAttachment) {
                        RoomRedPacketAttachment roomRedPacketAttachment = (RoomRedPacketAttachment) customAttachment;
                        if (RoomDataManager.get().getCurrentRoomInfo() != null && roomRedPacketAttachment.getDataInfo() != null) {
                            if (RoomDataManager.get().getCurrentRoomInfo().getRoomId() == roomRedPacketAttachment.getDataInfo().getRoomId()) {
                                roomRedPacketAttachment.getDataInfo().setRecieveTime(System.currentTimeMillis());
                                noticeRoomRedPackageRecieveNotice(roomRedPacketAttachment.getDataInfo());
                            }
                            if (roomRedPacketAttachment.getDataInfo().getShowAll() == 1){
                                noticeRoomFloatingBubbleUpadate(msg);
                            }
                        }
                    }
                }
                break;
            case CUSTOM_MSG_ROOM_RED_PACKET_FINISH:
                //红包完成消息
                if (customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_ROOM_RED_PACKET_RECEIVE_FINISH) {
                    if (customAttachment instanceof RoomRedPacketFinishAttachment) {
                        RoomRedPacketFinish redPacketInfo = ((RoomRedPacketFinishAttachment) customAttachment).getDataInfo();
                        if (redPacketInfo != null) {
                            noticeRoomRedPackageFinishNotice(redPacketInfo.getRedPacketId());
                        }
                    }
                    addMessages(msg);
                }
                break;
            default:
                break;
        }
    }

    /**
     * 解析消息相关的房间成员信息：发送者
     *
     * @param req_data
     * @param msg
     */
    public void parseCusttomMember(Json req_data, IMRoomMessage msg) {
        LogUtils.d(TAG, "parseCusttomMember-req_data:" + req_data);
        IMRoomMember member = null;
        try {
            member = JsonParser.parseJsonObject(req_data.json(IMKey.member).toString(), IMRoomMember.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (member != null) {
            msg.setImRoomMember(member);
        }
    }

    /**
     * 处理麦序相关的消息接收逻辑
     *
     * @param customAttachment
     * @param msg
     */
    private void dealWithMicQueueMessage(CustomAttachment customAttachment, IMRoomMessage msg) {
        RoomQueueMsgAttachment queueMsgAttachment = (RoomQueueMsgAttachment) customAttachment;
        msg.setAttachment(queueMsgAttachment);
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if (customAttachment.getSecond() == CUSTOM_MSG_HEADER_TYPE_QUEUE_INVITE) {
            //邀請上麥
            if (Objects.equals(queueMsgAttachment.uid, String.valueOf(uid))) {
                if (RoomDataManager.get().getCurrentRoomInfo() == null
                        || !String.valueOf(RoomDataManager.get().getCurrentRoomInfo().getRoomId()).equals(msg.getRoomId())) {
                    //阻止，IM下发邀请上麦通知时，用户已经完成退出房间的操作，或者已经切换到其他房间的情况，导致直接受邀上麦的问题
                    return;
                }
                noticeInviteUpMic(queueMsgAttachment.micPosition, queueMsgAttachment.uid);
                new IMRoomModel().operateUpMicro(queueMsgAttachment.micPosition, queueMsgAttachment.uid, true, null);
            }
        }

        if (customAttachment.getSecond() == CUSTOM_MSG_HEADER_TYPE_QUEUE_KICK) {
            //踢他下麥
            if (Objects.equals(queueMsgAttachment.uid, String.valueOf(uid))) {
                int micPosition = RoomDataManager.get().getMicPosition(uid);
                noticeKickDownMic(micPosition);
            }
        }

        //公屏消息开关/小礼物特效开关
        if ((customAttachment.getSecond() == CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_CLOSE)
                || (customAttachment.getSecond() == CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_OPEN)
                || (customAttachment.getSecond() == CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_OPEN)
                || (customAttachment.getSecond() == CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_CLOSE)) {
            addMessages(msg);
        }
    }

    /**
     * 处理排麦相关消息
     *
     * @param msg
     */
    private void dealWithMicInList(IMRoomMessage msg) {
        MicInListAttachment micInListAttachment = (MicInListAttachment) msg.getAttachment();
        String params = micInListAttachment.getParams();
        String key = Json.parse(params).str("key");
        SparseArray<IMRoomQueueInfo> mMicQueueMemberMap = RoomDataManager.get().mMicQueueMemberMap;
        if (!TextUtils.isEmpty(key) && mMicQueueMemberMap != null) {
            IMRoomQueueInfo IMRoomQueueInfo = mMicQueueMemberMap.get(Integer.parseInt(key));
            if (IMRoomQueueInfo != null) {
                if (!IMRoomQueueInfo.mRoomMicInfo.isMicLock()) {
                    micInListToUpMic(key);
                }
            }
        }
    }

    /**
     * 处理文本消息
     *
     * @param msg
     * @param req_data
     */
    private void dealWithTxtMessage(IMRoomMessage msg, Json req_data) {
        IMRoomMember member = null;
        try {
            member = JsonParser.parseJsonObject(req_data.json(IMKey.member).toString(), IMRoomMember.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (member == null) {
            return;
        }
        msg.setImRoomMember(member);
        msg.setContent(req_data.str(IMKey.content));
        addMessages(msg);
    }

    /**
     * 成员进入房间
     */
    private void chatRoomMemberIn(IMRoomMessage msg, Json req_data) {
        IMRoomMember member = null;
        try {
            member = JsonParser.parseJsonObject(req_data.json(IMKey.member).toString(), IMRoomMember.class);
            if (member != null) {
                int online_num = req_data.num("online_num");
                long timestamp = req_data.num_l("timestamp", 0);
                member.setOnlineNum(online_num);
                member.setTimestamp(timestamp);
                IMRoomMemberComeInfo memberComeInfo = new IMRoomMemberComeInfo();
                memberComeInfo.setNickName(member.getNick());
                memberComeInfo.setExperLevel(member.getExperLevel());
                memberComeInfo.setCarName(member.getCarName());
                memberComeInfo.setCarImgUrl(member.getCarUrl());
                //加到处理队列（因为有可能fragment还没初始化，还不能show，所以加到队列，由fragment处理后再remove）
                RoomDataManager.get().addMemberComeInfo(memberComeInfo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (member == null) {
            return;
        }
        msg.setImRoomMember(member);
        if (!TextUtils.isEmpty(member.getPointNineImg())) {
            NinePatchBitmapLoader.getInstance().loadNinePatchBitmap(null,
                    member.getPointNineImg(), 0);
        }
        addMessages(msg);
        noticeRoomMemberChange(true, member.getAccount(), member);
    }

    /**
     * 增加管理员
     *
     * @param message
     */
    private void addManagerMember(final IMRoomMessage message, Json req_data) {
        IMRoomMember member = null;
        try {
            member = JsonParser.parseJsonObject(req_data.json(IMKey.member).toString(), IMRoomMember.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (member != null) {
            message.setImRoomMember(member);
            RoomDataManager.get().addAdminMember(member);
            // 放在这里的原因是,只有管理员身份改变了才能发送通知
            noticeManagerChange(true, member.getAccount());
        }
    }

    /**
     * 处理移除管理员消息
     *
     * @param req_data
     */
    private void dealWithRemoveManager(Json req_data) {
        IMRoomMember member = null;
        try {
            member = JsonParser.parseJsonObject(req_data.json(IMKey.member).toString(), IMRoomMember.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (member != null) {
            removeManagerMember(member.getAccount());
        }
    }

    /**
     * 移除管理员
     */
    private void removeManagerMember(String account) {
        if (RoomDataManager.get().removeManagerMember(account)) {
            noticeManagerChange(false, account);
        }
    }

    /**
     * 加入黑名单
     *
     * @param req_data
     */
    private void addBlackList(Json req_data) {
        IMRoomMember member = null;
        try {
            member = JsonParser.parseJsonObject(req_data.json(IMKey.member).toString(), IMRoomMember.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (member != null && !RoomDataManager.get().isUserSelf(member.getAccount())) {
            noticeChatMemberBlackAdd(member.getAccount());
        }

    }

    /**
     * 退出房间处理
     */
    private void chatRoomMemberExit(Json req_data) {
        String account = req_data.str("uid");
        if (StringUtils.isNotEmpty(account)) {
            //用于判断退出数量
            IMRoomMember chatRoomMember = new IMRoomMember();
            chatRoomMember.setAccount(account);
            int online_num = req_data.num("online_num");
            long timestamp = req_data.num_l("timestamp", 0);
            chatRoomMember.setOnlineNum(online_num);
            chatRoomMember.setTimestamp(timestamp);
            noticeRoomMemberChange(false, account, chatRoomMember);

            if (null == RoomDataManager.get().getCurrentRoomInfo()) {
                return;
            }
            if (RoomDataManager.get().isOnMic(Long.valueOf(account))) {
                //在麦上的要退出麦
                LogUtil.d("nim_sdk", "chatRoomMemberExit     " + account);
                downMicroPhoneBySdk(RoomDataManager.get().getMicPosition(Long.valueOf(account)), null);
                SparseArray<IMRoomQueueInfo> mMicQueueMemberMap = RoomDataManager.get().mMicQueueMemberMap;
                int size = mMicQueueMemberMap.size();
                for (int i = 0; i < size; i++) {
                    IMRoomQueueInfo imRoomQueueInfo = mMicQueueMemberMap.valueAt(i);
                    if (imRoomQueueInfo.mChatRoomMember != null
                            && Objects.equals(imRoomQueueInfo.mChatRoomMember.getAccount(), account)) {
                        imRoomQueueInfo.mChatRoomMember = null;
                        LogUtil.d("remove  mChatRoomMember", 3 + "");
                        noticeDownMic(String.valueOf(mMicQueueMemberMap.keyAt(i)), account);
                        break;
                    }
                }
            }
            removeManagerMember(account);
        }
    }

    /**
     * 踢出房间操作
     *
     * @param req_data
     */
    private void onKickedOutRoom(Json req_data) {
        String account = req_data.str("uid");
        if (RoomDataManager.get().isUserSelf(account)) {//是踢自己
            int reason_no = req_data.num("reason_no");
            String reason_msg = req_data.str("reason_msg");
            // 提示被踢出的原因（聊天室已解散、被管理员踢出、被其他端踢出等
            noticeKickOutChatMember(reason_no, reason_msg, account);
            // 清空缓存数据
            RoomDataManager.get().release();
        }
    }

    /**
     * 踢出账号
     *
     * @param req_data
     */
    private void onKickOffLogin(Json req_data) {
//        chatRoomMemberExit(req_data);
        int reason_no = req_data.num("errno");
        String reason_msg = req_data.str("errmsg");
        // 提示被踢出的原因（聊天室已解散、被管理员踢出、被其他端踢出等
        noticeKickOutChatMember(reason_no, reason_msg, CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        // 清空缓存数据
        RoomDataManager.get().release();
        CoreManager.getCore(IAuthCore.class).logout();
        ReUsedSocketManager.get().destroy();
        PreferencesUtils.setFristQQ(true);
    }

    /**
     * 通知邀请上麦（视频房）
     */
    private void noticeInviteUpMicOnVideo(Json req_data) {

    }

    /**
     * 通知邀请进房（视频房）
     */
    private void noticeInviteEnterRoom(Json req_data) {

    }

    /**
     * 通知有人申请上麦（视频房）
     */
    private void noticeApplyWaitQueue(Json req_data) {

    }

    /**
     * 队列麦坑状态更新
     *
     * @param req_data
     */
    private void roomQueueMicUpdate(Json req_data) {
        String micInfo = req_data.str("mic_info");
        LogUtil.d(RoomDataManager.TAG, "roomQueueMicUpdate ---> position 坑位 " +
                "posState ---> 0：开锁，1：闭锁" +
                "micStat --->  0：开麦，1：闭麦" +
                "\n micInfo = " + micInfo);
        if (!TextUtils.isEmpty(micInfo)) {
            RoomMicInfo roomMicInfo = JsonParser.parseJsonToNormalObject(micInfo, RoomMicInfo.class);
            if (roomMicInfo != null) {
                int position = roomMicInfo.getPosition();
                IMRoomQueueInfo iMRoomQueueInfo = RoomDataManager.get().mMicQueueMemberMap.get(position);
                if (iMRoomQueueInfo != null) {
                    iMRoomQueueInfo.mRoomMicInfo = roomMicInfo;
                    if (RoomDataManager.get().getCurrentRoomInfo() != null && iMRoomQueueInfo.mChatRoomMember != null &&
                            RoomDataManager.get().isUserSelf(iMRoomQueueInfo.mChatRoomMember.getAccount())) {//如果变化的是我自己所在的麦位
                        RtcEngineManager.get().setRole(iMRoomQueueInfo.mRoomMicInfo.isMicMute() ? io.agora.rtc.Constants.CLIENT_ROLE_AUDIENCE : io.agora.rtc.Constants.CLIENT_ROLE_BROADCASTER);
                    }
                    noticeMicPosStateChange(position, iMRoomQueueInfo);
                }
            }
        }
    }

    /**
     * 房间信息更新
     *
     * @param req_data -
     */
    private void chatRoomInfoUpdate(Json req_data) {
        LogUtils.d(TAG, "chatRoomInfoUpdate-req_data:" + req_data);
        RoomInfo roomInfo = null;
        try {
            roomInfo = JsonParser.parseJsonToNormalObject(req_data.json("room_info").toString(), RoomInfo.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (roomInfo != null) {
            RoomDataManager.get().setCurrentRoomInfo(roomInfo);
            noticeRoomInfoUpdate(roomInfo);
        }
    }

    /**
     * 麦序更新
     *
     * @param req_data
     */
    private void chatRoomQueueChangeNotice(Json req_data) {
        if (req_data == null) {
            return;
        }
        //排麦的key是用uid，所以length大于2
        int key = req_data.num("key");
        boolean isMicInList = (key >= 10);
        Json content = req_data.json("value");
        switch (req_data.num("type")) {
            case 1:
                LogUtil.d(RoomDataManager.TAG, "chatRoomQueueChangeNotice ---> type(1：更新key 2：删除) = " + 1);
                if (isMicInList) {
                    addMicInList(key, content);
                }
                upMicroQueue(content, key);
                break;
            case 2:
                LogUtil.d(RoomDataManager.TAG, "chatRoomQueueChangeNotice ---> type(1：更新key 2：删除) = " + 2);
                if (isMicInList) {
                    removeMicInList(key + "");
                }
                downMicroQueue(key + "");
                break;
            default:
        }
    }


    //******************************************************** 消息发送方法 ******************************************************//

    /**
     * 上麦
     *
     * @param content --
     */
    private synchronized void upMicroQueue(Json content, final int micPosition) {
        final SparseArray<IMRoomQueueInfo> mMicQueueMemberMap = RoomDataManager.get().mMicQueueMemberMap;
        if (content != null) {
            IMRoomMember chatRoomMember = null;
            try {
                LogUtil.d(RoomDataManager.TAG, "upMicroQueue ---> content" + content.toString());
                chatRoomMember = JsonParser.parseJsonObject(content.toString(), IMRoomMember.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (chatRoomMember == null) {
                return;
            }
            IMRoomQueueInfo imRoomQueueInfo = mMicQueueMemberMap.get(micPosition);
            if (imRoomQueueInfo == null) {
                return;
            }
            int size = mMicQueueMemberMap.size();
            if (size > 0) {
                for (int j = 0; j < size; j++) {
                    IMRoomQueueInfo temp = mMicQueueMemberMap.valueAt(j);
                    if (temp.mChatRoomMember != null
                            && Objects.equals(temp.mChatRoomMember.getAccount(), chatRoomMember.getAccount())) {
                        //处理魅力值换坑问题
                        chatRoomMember.setCharmValue(temp.mChatRoomMember.getCharmValue());
                        chatRoomMember.setCharmHatUrl(temp.mChatRoomMember.getCharmHatUrl());
                        //处理同一个人换坑问题
                        temp.mChatRoomMember = null;
                    }
                }
            }
            IMRoomQueueInfo tempIMRoomQueueInfo = mMicQueueMemberMap.get(micPosition);
            if (tempIMRoomQueueInfo != null && tempIMRoomQueueInfo.mChatRoomMember != null
                    && !Objects.equals(tempIMRoomQueueInfo.mChatRoomMember.getAccount(), chatRoomMember.getAccount())) {
                //被挤下麦的情况
                noticeDownCrowdedMic(micPosition, tempIMRoomQueueInfo.mChatRoomMember.getAccount());
            }
            imRoomQueueInfo.mChatRoomMember = chatRoomMember;
            //重新更新队列，队列上是否还有自己
            if (!RoomDataManager.get().isOwnerOnMic()) {
                if (RoomDataManager.get().getCurrentRoomInfo() != null) {
                    //处理可能自己被挤下还能说话的情况
                    RtcEngineManager.get().setRole(Constants.CLIENT_ROLE_AUDIENCE);
                }
            }
            if (RoomDataManager.get().isUserSelf(chatRoomMember.getAccount())) {
                if (!imRoomQueueInfo.mRoomMicInfo.isMicMute()) {
                    if (RoomDataManager.get().getCurrentRoomInfo() != null) {
                        //开麦
                        RtcEngineManager.get().setRole(Constants.CLIENT_ROLE_BROADCASTER);
                    }
                    //不需要开麦的话闭麦
                    if (!RoomDataManager.get().mIsNeedOpenMic) {
                        //闭麦
                        if (RoomDataManager.get().getCurrentRoomInfo() != null) {
                            RtcEngineManager.get().setMute(true);
                        }
                        RoomDataManager.get().mIsNeedOpenMic = true;
                    }
                } else {
                    if (RoomDataManager.get().getCurrentRoomInfo() != null) {
                        RtcEngineManager.get().setRole(Constants.CLIENT_ROLE_AUDIENCE);
                    }
                }
            }
            micInListToDownMic(chatRoomMember.getAccount());
            noticeUpMic(micPosition, chatRoomMember.getAccount());
        } else {
            if (RoomDataManager.get().getCurrentRoomInfo() != null) {
                RtcEngineManager.get().setRole(Constants.CLIENT_ROLE_AUDIENCE);
            }
        }
    }

    /**
     * 下麦
     *
     * @param key --
     */
    private void downMicroQueue(String key) {
        LogUtil.d(RoomDataManager.TAG, "downMicroQueue ---> key(麦位) = " + key);
        SparseArray<IMRoomQueueInfo> mMicQueueMemberMap = RoomDataManager.get().mMicQueueMemberMap;
        if (!TextUtils.isEmpty(key) && mMicQueueMemberMap != null) {
            IMRoomQueueInfo IMRoomQueueInfo = mMicQueueMemberMap.get(Integer.parseInt(key));
            if (IMRoomQueueInfo != null && IMRoomQueueInfo.mChatRoomMember != null) {
                String account = IMRoomQueueInfo.mChatRoomMember.getAccount();
                if (RoomDataManager.get().isUserSelf(account)) {
                    if (RoomDataManager.get().getCurrentRoomInfo() != null) {
                        // 更新声网闭麦 ,发送状态信息
                        RtcEngineManager.get().setRole(Constants.CLIENT_ROLE_AUDIENCE);
                    }
//                    RtcEngineManager.get().setMute(false);
                    RoomDataManager.get().mIsNeedOpenMic = true;
                }
                //判断是否为房主，是则停止计时器
                if (RoomDataManager.get().isRoomOwner(account)) {
                    RoomOwnerLiveTimeCounter.getInstance().stopCount();
                }
                IMRoomQueueInfo.mChatRoomMember = null;
                // 通知界面更新麦序信息
                noticeDownMic(key, account);
                //排麦
                if (!IMRoomQueueInfo.mRoomMicInfo.isMicLock()) {
                    micInListToUpMic(key);
                }
            }
        }
    }

    /**
     * 解析进房时刻，公屏消息历史记录
     *
     * @return
     */
    private void getRoomHistoryMsgContent() {
        if (null != RoomDataManager.get().mRoomHistoryList && RoomDataManager.get().mRoomHistoryList.size() > 0
                && null != messages) {
            for (IMRoomMessage imRoomMessage : RoomDataManager.get().mRoomHistoryList) {
                noticeReceiverMessage(imRoomMessage);
                messages.add(imRoomMessage);
            }
        }
    }

    /**
     * 进入房间的提醒消息
     *
     * @return
     */
    private IMRoomMessage getFirstMessageContent() {
        String content = "为了营造健康和谐的直播环境，请不要发布违法、低俗、广告等违规内容，违反会被禁言哦";
        IMRoomMessage message = new IMRoomMessage();
        message.setRoute(IMReportRoute.ChatRoomTip);
        message.setContent(content);
        return message;
    }

    /**
     * 发送房间规则消息
     */
    public void sendRoomRulesMessage() {
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (roomInfo == null) {
            return;
        }
        if (StringUtils.isEmpty(roomInfo.getPlayInfo())) {
            return;
        }
        RoomRuleAttachment rules = new RoomRuleAttachment(CustomAttachment.CUSTOM_MSG_TYPE_RULE_FIRST,
                CustomAttachment.CUSTOM_MSG_TYPE_RULE_FIRST);
        rules.setRule(roomInfo.getPlayInfo());
        IMRoomMessage message = new IMRoomMessage();
        message.setRoute(IMReportRoute.sendMessageReport);
        message.setRoomId(roomInfo.getRoomId() + "");
        message.setAttachment(rules);
        messages.add(message);
        noticeReceiverMessageImmediately(message);
    }

    public void sendLotteryBoxMsg(GiftInfo giftInfo, int count, final CallBack<String> callBack) {
        if (giftInfo == null) {
            return;
        }
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        LotteryBoxAttachment lotteryBoxAttachment = new LotteryBoxAttachment(CustomAttachment.CUSTOM_MSG_LOTTERY_BOX, CustomAttachment.CUSTOM_MSG_LOTTERY_BOX);
        Json json = new Json();
        json.set("giftName", giftInfo.getGiftName());
        json.set("count", count);
        json.set("goldPrice", giftInfo.getGoldPrice());
        json.set("giftUrl", giftInfo.getGiftUrl());
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (null != userInfo) {
            if (null != roomInfo && roomInfo.getUid() != userInfo.getUid()) {
                json.set(com.tongdaxing.xchat_core.Constants.USER_NICK,
                        NobleBusinessManager.getNobleRoomNick(userInfo.getVipId(),
                                userInfo.getIsInvisible(), userInfo.getVipDate(), userInfo.getNick()));
                json.set(USER_MEDAL_ID, userInfo.getVipId());
                json.set(USER_MEDAL_DATE, userInfo.getVipDate());
                json.set(NOBLE_INVISIABLE_ENTER_ROOM, userInfo.getIsInvisible() ? 1 : 0);
            } else {
                json.set(com.tongdaxing.xchat_core.Constants.USER_NICK, userInfo.getNick());
            }

            lotteryBoxAttachment.setParams(json + "");

            IMRoomMessage message = new IMRoomMessage();
            if (null != roomInfo) {
                message.setRoomId(roomInfo.getRoomId() + "");
            }
            message.setAttachment(lotteryBoxAttachment);
            message.setImRoomMember(getCurrentIMRoomMember());
            ReUsedSocketManager.get().sendCustomMessage(message.getRoomId() + "", message, new IMSendCallBack() {

                @Override
                public void onSuccess(String data) {
                    LogUtil.d(RoomDataManager.TAG, "sendLotteryBoxMsg-->onSuccess data:" + data);
                    if (callBack != null) {
                        callBack.onSuccess(data);
                    }
                }

                @Override
                public void onError(int errorCode, String errorMsg) {
                    if (callBack != null) {
                        callBack.onFail(errorCode, errorMsg);
                    }
                }
            });
        }
    }

    /**
     * 发送文本信息
     *
     * @param message -
     */
    public void sendTextMsg(long roomId, String message, final CallBack<String> callBack) {
        if (TextUtils.isEmpty(message) || TextUtils.isEmpty(message.trim())) {
            return;
        }
        final IMRoomMessage imRoomMessage = new IMRoomMessage(String.valueOf(roomId), message);
        imRoomMessage.setRoute(IMReportRoute.sendTextReport);
        imRoomMessage.setImRoomMember(getCurrentIMRoomMember());
        ReUsedSocketManager.get().sendTxtMessage(roomId + "", imRoomMessage, new IMSendCallBack() {
            @Override
            public void onSuccess(String data) {
                LogUtil.d(RoomDataManager.TAG, "sendTextMsg-->onSuccess data:" + data);
                if (callBack != null) {
                    callBack.onSuccess(data);
                }
            }

            @Override
            public void onError(int errorCode, String errorMsg) {
                if (callBack != null) {
                    callBack.onFail(errorCode, errorMsg);
                }
            }
        });
    }

    private void sendMicInListNimMsg(final String key) {
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        MicInListAttachment micInListAttachment = new MicInListAttachment(
                CustomAttachment.CUSTOM_MSG_MIC_IN_LIST, CustomAttachment.CUSTOM_MSG_MIC_IN_LIST);
        Json json = new Json();
        json.set("key", key);
        micInListAttachment.setParams(json + "");
        final IMRoomMessage message = new IMRoomMessage();
        message.setRoomId(roomInfo.getRoomId() + "");
        message.setAttachment(micInListAttachment);
        message.setImRoomMember(getCurrentIMRoomMember());
        ReUsedSocketManager.get().sendCustomMessage(roomInfo.getRoomId() + "", message, new IMSendCallBack() {
            @Override
            public void onSuccess(String data) {
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        micInListToUpMic(key);
                    }
                }, 300);
            }

            @Override
            public void onError(int errorCode, String errorMsg) {

            }
        });
    }

    /**
     * 邀请上麦的自定义消息
     *
     * @param micUid   上麦用户uid
     * @param position 要上麦的位置
     */
    public void inviteMicroPhoneBySdk(final long micUid, final int position) {
        final RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        //上麦条件:游客不在麦上 或者是管理员，房主
        if ((!RoomDataManager.get().isOnMic(micUid)
                || RoomDataManager.get().isRoomOwner()
                || RoomDataManager.get().isRoomAdmin())
                && position != Integer.MIN_VALUE) {
            RoomQueueMsgAttachment queueMsgAttachment = new RoomQueueMsgAttachment(
                    CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE,
                    CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_INVITE);
            queueMsgAttachment.uid = String.valueOf(micUid);
            queueMsgAttachment.micPosition = position;
            IMRoomMessage message = new IMRoomMessage();
            message.setRoomId(roomInfo.getRoomId() + "");
            message.setAttachment(queueMsgAttachment);
            message.setImRoomMember(getCurrentIMRoomMember());
            ReUsedSocketManager.get().sendCustomMessage(roomInfo.getRoomId() + "", message, new IMSendCallBack() {
                @Override
                public void onSuccess(String data) {

                }

                @Override
                public void onError(int errorCode, String errorMsg) {

                }
            });
        }
    }

    /**
     * 踢人下麦
     *
     * @param micUid 被踢用户uid
     * @param roomId 房间ID
     */
    public void kickMicroPhoneBySdk(int micPosition, long micUid, long roomId) {
        IMRoomMessageManager.get().downMicroPhoneBySdk(micPosition, null);
        RoomQueueMsgAttachment queueMsgAttachment = new RoomQueueMsgAttachment(
                CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE,
                CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_KICK);
        queueMsgAttachment.uid = String.valueOf(micUid);
        IMRoomMessage message = new IMRoomMessage();
        message.setRoomId(String.valueOf(roomId));
        message.setAttachment(queueMsgAttachment);
        message.setImRoomMember(getCurrentIMRoomMember());
        ReUsedSocketManager.get().sendCustomMessage(roomId + "", message, new IMSendCallBack() {
            @Override
            public void onSuccess(String data) {

            }

            @Override
            public void onError(int errorCode, String errorMsg) {

            }
        });
    }

    /**
     * 发送开启和关闭屏蔽小礼物特效消息和屏蔽公屏消息
     *
     * @param uid
     * @param second
     * @param micIndex
     */
    public void systemNotificationBySdk(long uid, int second, int micIndex) {
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (roomInfo == null) {
            return;
        }
        long roomId = roomInfo.getRoomId();
        RoomQueueMsgAttachment queueMsgAttachment = new RoomQueueMsgAttachment(
                CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE,
                second);
        queueMsgAttachment.uid = String.valueOf(uid);
        if (micIndex != -1) {
            queueMsgAttachment.micPosition = micIndex;
        }
        final IMRoomMessage message = new IMRoomMessage();
        message.setRoomId(String.valueOf(roomId));
        message.setAttachment(queueMsgAttachment);
        message.setRoute(IMReportRoute.sendMessageReport);
        message.setImRoomMember(getCurrentIMRoomMember());
        ReUsedSocketManager.get().sendCustomMessage(String.valueOf(roomId), message, new IMSendCallBack() {
            @Override
            public void onSuccess(String data) {
                //IM服务器会向房间所有用户包括自己发送消息，因此不需要自行添加
//                addMessagesImmediately(message);
            }

            @Override
            public void onError(int errorCode, String errorMsg) {

            }
        });
    }

    /**
     * 发送开启和关闭屏蔽小礼物特效消息和屏蔽公屏消息
     *
     * @param second
     */
    public void sendRoomOperaTipMsgBySdk(int second) {
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();

        if (roomInfo == null) {
            return;
        }
        long roomId = roomInfo.getRoomId();
        CustomAttachment customAttachment = new CustomAttachment(
                CustomAttachment.CUSTOM_MSG_ROOM_OPERA_TIPS,
                second);
        final IMRoomMessage message = new IMRoomMessage();
        message.setRoomId(String.valueOf(roomId));
        message.setAttachment(customAttachment);
        message.setRoute(IMReportRoute.sendMessageReport);
        message.setImRoomMember(getCurrentIMRoomMember());
        ReUsedSocketManager.get().sendCustomMessage(String.valueOf(roomId), message, new IMSendCallBack() {
            @Override
            public void onSuccess(String data) {
                //IM服务器会向房间所有用户包括自己发送消息，因此不需要自行添加
//                addMessagesImmediately(message);
            }

            @Override
            public void onError(int errorCode, String errorMsg) {

            }
        });
    }

    public void addMicInList(int key, Json content) {

    }

    private void removeMicInList(String key) {
        RoomDataManager.get().removeMicListInfo(key);
        noticeMicInList();

    }

    /**
     * 进入聊天室
     */
    public void joinAvRoom() {
        RoomInfo curRoomInfo = RoomDataManager.get().getCurrentRoomInfo();
        RoomDataManager.get().setStartPlayFull(true);
        noticeEnterMessages();
        if (curRoomInfo != null) {
            long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
            OkHttpManager.MyCallBack myCallBack = new OkHttpManager.MyCallBack<Json>() {

                @Override
                public void onError(Exception e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Json json) {

                }
            };
            model.userRoomIn(String.valueOf(uid), curRoomInfo.getUid(), myCallBack, curRoomInfo.getType());
        }
    }

    public void addMessagesImmediately(IMRoomMessage msg) {
        if (messages.size() == 0) {
            getRoomHistoryMsgContent();
            IMRoomMessage firstMessageContent = getFirstMessageContent();
            messages.add(firstMessageContent);
            noticeReceiverMessageImmediately(firstMessageContent);
            sendRoomRulesMessage();
        }
        if (messages.size() > 1000) {
            messages.remove(0);
        }
        messages.add(msg);
        noticeReceiverMessageImmediately(msg);


    }

    private void addMessages(IMRoomMessage msg) {
        if (messages.size() == 0) {
            getRoomHistoryMsgContent();
            IMRoomMessage firstMessageContent = getFirstMessageContent();
            messages.add(firstMessageContent);
            noticeReceiverMessage(firstMessageContent);
            sendRoomRulesMessage();
        }
        if (messages.size() > 1000) {
            messages.remove(0);
        }
        messages.add(msg);
        noticeReceiverMessage(msg);
    }

    @Deprecated
    public Observable<List<IMRoomMessage>> getIMRoomMessageFlowable() {
        return getIMRoomMessagePublisher().toFlowable(BackpressureStrategy.BUFFER)
                .toObservable().buffer(200, TimeUnit.MILLISECONDS, 20)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public void subscribeIMRoomMessageFlowable(Consumer<List<IMRoomMessage>> chatMsg, IDisposableAddListener iDisposableAddListener) {
        Disposable disposable = getIMRoomMessageFlowable().subscribe(chatMsg);
        if (iDisposableAddListener != null) {
            iDisposableAddListener.addDisposable(disposable);
        }
    }

    private PublishSubject<IMRoomMessage> getIMRoomMessagePublisher() {
        if (msgProcessor == null) {
            synchronized (IMRoomMessageManager.class) {
                if (msgProcessor == null) {
                    msgProcessor = PublishSubject.create();
                }
            }
        }
        return msgProcessor;
    }

    @SuppressLint("CheckResult")
    public PublishProcessor<IMRoomEvent> getIMRoomEventObservable() {
        if (roomProcessor == null) {
            synchronized (IMRoomMessageManager.class) {
                if (roomProcessor == null) {
                    roomProcessor = PublishProcessor.create();
                }
            }
        }
        return roomProcessor;
    }

    public void subscribeIMRoomEventObservable(Consumer<IMRoomEvent> roomEvent, IDisposableAddListener iDisposableAddListener) {
        Disposable disposable = getIMRoomEventObservable().subscribe(roomEvent);
        if (iDisposableAddListener != null) {
            iDisposableAddListener.addDisposable(disposable);
        }
    }

    public void clear() {
        messages.clear();
        LogUtil.e(TAG, "清除房间消息....");
    }

    /**
     * @param account 被操作的用户uid
     * @param is_add  1添加，0移除
     */
    public void markBlackList(String account, boolean is_add, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = getImDefaultParamsMap();
        if (params == null) {
            return;
        }
        params.put("account", account);
        params.put("is_add", is_add ? "1" : "0");
        OkHttpManager.getInstance().postRequest(IMUriProvider.getMarkBlackList(), params, myCallBack);

    }

    public Map<String, String> getImDefaultParamsMap() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (roomInfo == null) {
            return null;
        }
        IAuthCore iAuthCore = CoreManager.getCore(IAuthCore.class);
        long currentUid = iAuthCore.getCurrentUid();
        String ticket = iAuthCore.getTicket();
        long roomId = roomInfo.getRoomId();
        params.put("room_id", roomId + "");
        params.put("uid", currentUid + "");
        params.put("ticket", ticket);
        return params;
    }


    /**
     * 踢出房间
     *
     * @param account    被踢出用户uid
     * @param myCallBack
     */
    public void kickMember(String account, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = getImDefaultParamsMap();
        if (params == null) {
            return;
        }
        params.put("account", account);
        OkHttpManager.getInstance().postRequest(IMUriProvider.getKickMemberUrl(), params, myCallBack);
    }

    /************************云信聊天室 普通操作(每个人都可以使用的) start******************************/


    private void micInListToUpMic(final String key) {
        LogUtil.d("micInListToUpMic", "key:" + key);

        //房主不不能上
        if ("-1".equals(key)) {
            return;
        }
        LogUtil.d("micInListToUpMic_!=-1", "key:" + key);
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (roomInfo == null) {
            return;
        }
        //那之前更新一次队列
        final Json json = RoomDataManager.get().getMicInListTopInfo();
        if (json == null) {
            return;
        }

        final String micInListTopKey = json.str("uid");

        LogUtil.d("micInListToUpMic", micInListTopKey);

        checkMicInListUpMicSuccess(micInListTopKey, roomInfo.getRoomId(), key);

        if (!micInListTopKey.equals(CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getUid() + "")) {
            return;
        }

        remveMicInlistOrUpMic(key, roomInfo, micInListTopKey);
    }

    private void remveMicInlistOrUpMic(final String key, RoomInfo roomInfo, final String micInListTopKey) {
        removeMicInList(micInListTopKey, roomInfo.getRoomId() + "", new RequestCallback() {
            @Override
            public void onSuccess(Object param) {
                //移除成功报上麦,判断是否自己,如果是自动上麦

                LogUtil.d("micInListToUpMic", 1 + "");
                CoreManager.notifyClients(IAVRoomCoreClient.class, IAVRoomCoreClient.onMicInListToUpMic, Integer.parseInt(key), micInListTopKey);
                CoreManager.notifyClients(IAVRoomCoreClient.class, IAVRoomCoreClient.micInListDismiss, "");

            }

            @Override
            public void onFailed(int code) {
                LogUtil.d("micInListLogUpMic", 2 + "code:" + code);
            }

            @Override
            public void onException(Throwable exception) {
                LogUtil.d("micInListLogUpMic", 3 + "");
            }
        });
    }

    private void checkMicInListUpMicSuccess(final String micInListTopKey, final long roomId, final String key) {
        String account = getFirstMicUid();

        long currentUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        boolean isKicker = (currentUid + "").equals(account);
        //如果是首个在麦上的人
        if (isKicker) {
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    Json json = RoomDataManager.get().getMicInListTopInfo();
                    if (json == null) {
                        return;
                    }

                    String topKey = json.str("uid", "null");
                    //1.5秒后判断排麦，首位跟换没有
                    if (micInListTopKey.equals(topKey)) {
                        IMRoomMessageManager.get().removeMicInList(
                                micInListTopKey, roomId + "", new RequestCallback() {
                                    @Override
                                    public void onSuccess(Object param) {
                                        //移除成功后通知别人上麦
                                        sendMicInListNimMsg(key);
                                    }

                                    @Override
                                    public void onFailed(int code) {
                                        LogUtil.d("micInListToUpMiconFailed", key);

                                    }

                                    @Override
                                    public void onException(Throwable exception) {
                                        LogUtil.d("micInListToUpMiconException", key);
                                    }
                                });
                        LogUtil.d("checkMicInListUpMicSuccess", "kick");

                    }
                }
            }, 1500);
        }
    }

    private String getFirstMicUid() {
        String account = "";
        SparseArray<IMRoomQueueInfo> mMicQueueMemberMap = RoomDataManager.get().mMicQueueMemberMap;

        for (int i = 0; i < mMicQueueMemberMap.size(); i++) {
            IMRoomQueueInfo IMRoomQueueInfo = mMicQueueMemberMap.valueAt(i);
            if (IMRoomQueueInfo == null) {
                continue;
            }
            IMRoomMember mChatRoomMember = IMRoomQueueInfo.mChatRoomMember;
            if (mChatRoomMember == null) {
                continue;
            }
            account = mChatRoomMember.getAccount();
            break;
        }
        return account;
    }

    public void removeMicInList(String key, String roomId, RequestCallback requestCallback) {
        NIMClient.getService(ChatRoomService.class).pollQueue(roomId, key).setCallback(requestCallback);

    }

    /**
     * 检测用户是否有推流权限
     */
    public void checkPushAuth(OkHttpManager.MyCallBack callBack) {

        Map<String, String> paramsMap = getImDefaultParamsMap();
        if (paramsMap == null) {
            return;
        }
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        paramsMap.put("room_id", String.valueOf(roomInfo.getRoomId()));
        paramsMap.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        paramsMap.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());

        OkHttpManager.getInstance().postRequest(IMUriProvider.getCheckPushAuth(), paramsMap, callBack);
    }

    private void micInListToDownMic(String key) {

        if (!RoomDataManager.get().checkInMicInlist()) {
            return;
        }
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        removeMicInList(key, roomInfo.getRoomId() + "", null);
    }

    /************************云信聊天室 普通操作 end******************************/

    /************************云信聊天室 房主/管理员操作 begin******************************/


    /**
     * 设置管理员
     *
     * @param is_add  1加，0移除
     * @param account 要设置的管理员id
     */
    public void markManager(final String account, final boolean is_add, final OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = getImDefaultParamsMap();
        if (params == null) {
            return;
        }
        params.put("is_add", is_add ? "1" : "0");
        params.put("account", account);
        OkHttpManager.getInstance().postRequest(IMUriProvider.getMarkChatRoomManager(), params, myCallBack);
    }

    /**
     * 下麦
     *
     * @param micPosition -
     * @param callBack    -
     */
    public void downMicroPhoneBySdk(int micPosition, final CallBack<String> callBack) {
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (roomInfo == null) {
            return;
        }

        if (micPosition < -1) {
            return;
        }
        //防止房主掉麦
        if (micPosition == -1) {
            String currentUid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            if (!RoomDataManager.get().isRoomOwner(currentUid)) {
                return;
            }
        }
        ReUsedSocketManager.get().pollQueue(String.valueOf(roomInfo.getRoomId()), micPosition, new IMSendCallBack() {
            @Override
            public void onSuccess(String data) {
                if (callBack != null) {
                    callBack.onSuccess("下麦成功");
                }
            }

            @Override
            public void onError(int errorCode, String errorMsg) {
                if (callBack != null) {
                    callBack.onFail(errorCode, "下麦失败:" + errorMsg);
                }
            }
        });
    }

    /**
     * 发送礼物通知
     *
     * @param giftReceiveInfo 礼物信息
     */
    public void sendSingleGiftMessage(GiftReceiveInfo giftReceiveInfo) {
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (roomInfo == null || giftReceiveInfo == null) {
            return;
        }
        long myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        GiftAttachment giftAttachment = new GiftAttachment(CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT, CustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_GIFT);
        giftAttachment.setUid(myUid + "");
        giftAttachment.setGiftRecieveInfo(giftReceiveInfo);
        IMRoomMessage message = new IMRoomMessage();
        message.setRoomId(roomInfo.getRoomId() + "");
        message.setAttachment(giftAttachment);
        message.setImRoomMember(IMRoomMessageManager.get().getCurrentIMRoomMember());
        ReUsedSocketManager.get().sendCustomMessage(message.getRoomId() + "", message, new IMProCallBack() {
            @Override
            public void onSuccessPro(IMReportBean imReportBean) {
                if (imReportBean != null && imReportBean.getReportData() != null && imReportBean.getReportData().errno == 0) {
                    notifyClients(IMRoomCoreClient.class, IMRoomCoreClient.METHOD_ON_SEND_IMROOM_MESSAGE_SUCCESS, message);
                }
            }

            @Override
            public void onError(int errorCode, String errorMsg) {

            }
        });
    }

    public void sendSingleGiftComboScreenMessage(GiftReceiveInfo giftReceiveInfo, long roomId) {
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (roomInfo == null || giftReceiveInfo == null) {
            return;
        }
        long myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        GiftAttachment giftAttachment = new GiftAttachment(CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT,
                CustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_GIFT_COMBO_SCREEN);
        giftAttachment.setUid(myUid + "");
        giftAttachment.setGiftRecieveInfo(giftReceiveInfo);
        IMRoomMessage message = new IMRoomMessage();
        message.setRoomId(String.valueOf(roomId));
        message.setAttachment(giftAttachment);

        message.setImRoomMember(IMRoomMessageManager.get().getCurrentIMRoomMember());
        ReUsedSocketManager.get().sendCustomMessage(message.getRoomId() + "", message, new IMProCallBack() {
            @Override
            public void onSuccessPro(IMReportBean imReportBean) {
                if (imReportBean != null && imReportBean.getReportData() != null && imReportBean.getReportData().errno == 0) {
                    notifyClients(IMRoomCoreClient.class, IMRoomCoreClient.METHOD_ON_SEND_IMROOM_MESSAGE_SUCCESS, message);
                }
            }

            @Override
            public void onError(int errorCode, String errorMsg) {

            }
        });
    }

    /**
     * 赠送全麦礼物
     *
     * @param giftReceiveInfo
     */
    public void sendMultiGiftComboScreenMessage(MultiGiftReceiveInfo giftReceiveInfo, long roomId) {
        if (giftReceiveInfo != null) {
            long myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
            MultiGiftAttachment giftAttachment = new MultiGiftAttachment(CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT,
                    CustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_MULTI_GIFT_COMBO_SCREEN);
            giftAttachment.setUid(myUid + "");
            giftAttachment.setMultiGiftAttachment(giftReceiveInfo);
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            giftAttachment.setCharmLevel(userInfo.getCharmLevel());
            giftAttachment.setExperLevel(userInfo.getExperLevel());
            IMRoomMessage message = new IMRoomMessage();
            message.setRoomId(String.valueOf(roomId));
            message.setAttachment(giftAttachment);
            message.setImRoomMember(IMRoomMessageManager.get().getCurrentIMRoomMember());
            ReUsedSocketManager.get().sendCustomMessage(message.getRoomId() + "", message, new IMProCallBack() {
                @Override
                public void onSuccessPro(IMReportBean imReportBean) {
                    if (imReportBean != null && imReportBean.getReportData() != null && imReportBean.getReportData().errno == 0) {
                        notifyClients(IMRoomCoreClient.class, IMRoomCoreClient.METHOD_ON_SEND_IMROOM_MESSAGE_SUCCESS, message);
                    }
                }

                @Override
                public void onError(int errorCode, String errorMsg) {

                }
            });
        }
    }

    /**
     * 赠送全麦礼物
     *
     * @param giftReceiveInfo
     */
    public void sendMultiGiftMessage(MultiGiftReceiveInfo giftReceiveInfo) {
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (roomInfo != null && giftReceiveInfo != null) {
            long myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
            MultiGiftAttachment giftAttachment = new MultiGiftAttachment(CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT, CustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_MULTI_GIFT);
            giftAttachment.setUid(myUid + "");
            giftAttachment.setMultiGiftAttachment(giftReceiveInfo);
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            giftAttachment.setCharmLevel(userInfo.getCharmLevel());
            giftAttachment.setExperLevel(userInfo.getExperLevel());
            IMRoomMessage message = new IMRoomMessage();
            message.setRoomId(roomInfo.getRoomId() + "");
            message.setAttachment(giftAttachment);
            message.setImRoomMember(IMRoomMessageManager.get().getCurrentIMRoomMember());
            ReUsedSocketManager.get().sendCustomMessage(message.getRoomId() + "", message, new IMProCallBack() {
                @Override
                public void onSuccessPro(IMReportBean imReportBean) {
                    if (imReportBean != null && imReportBean.getReportData() != null && imReportBean.getReportData().errno == 0) {
                        notifyClients(IMRoomCoreClient.class, IMRoomCoreClient.METHOD_ON_SEND_IMROOM_MESSAGE_SUCCESS, message);
                    }
                }

                @Override
                public void onError(int errorCode, String errorMsg) {

                }
            });
        }
    }

    /**
     * 发送Pk消息
     *
     * @param second
     * @param pkVoteInfo
     */
    public void sendPkStartMessage(int first, int second, PkVoteInfo pkVoteInfo) {
        if (RoomDataManager.get().getCurrentRoomInfo() == null) {
            return;
        }
        PkCustomAttachment pkCustomAttachment = new PkCustomAttachment(first, second);
        pkCustomAttachment.setPkVoteInfo(pkVoteInfo);
        IMRoomMessage message = new IMRoomMessage();
        message.setAttachment(pkCustomAttachment);
        ReUsedSocketManager.get().sendCustomMessage(RoomDataManager.get().getCurrentRoomInfo().getRoomId() + "", message, new IMProCallBack() {
            @Override
            public void onSuccessPro(IMReportBean imReportBean) {

            }

            @Override
            public void onError(int errorCode, String errorMsg) {

            }
        });
    }


    /**
     * 获取当前用户信息信息
     *
     * @return
     */
    public IMRoomMember getCurrentIMRoomMember() {
        IMRoomMember member = new IMRoomMember();
        if (RoomDataManager.get().mSelfRoomMember != null) {
            //传递需要的信息
            member = RoomDataManager.get().mSelfRoomMember;
        } else {
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            if (userInfo != null) {//暂时我们做处理
                member.setVipId(userInfo.getVipId());
                member.setVipMedal(userInfo.getVipMedal());
                member.setVipDate(userInfo.getVipDate());
                member.setInvisible(userInfo.getIsInvisible());
                member.setAlias(userInfo.getAlias());
            }
        }
        return member;
    }

    /**
     * -------------------------通知begin-------------------------------
     */
    private void noticeRoomGiftMsg(GiftReceiveInfo giftReceiveInfo) {
        LogUtil.d(RoomDataManager.TAG, "noticeRoomGiftMsg");
        getIMRoomEventObservable().onNext(new IMRoomEvent()
                .setEvent(IMRoomEvent.ROOM_RECEIVE_GIFT_MSG)
                .setGiftReceiveInfo(giftReceiveInfo));
    }

    private void noticeRoomMultiGiftMsg(MultiGiftReceiveInfo multiGiftReceiveInfo) {
        LogUtils.d(TAG, "noticeRoomMultiGiftMsg");
        getIMRoomEventObservable().onNext(new IMRoomEvent()
                .setEvent(IMRoomEvent.ROOM_RECEIVE_MULTI_GIFT)
                .setMultiGiftReceiveInfo(multiGiftReceiveInfo));
    }

    private void noticeRoomSuperGiftMsg(GiftReceiveInfo giftReceiveInfo) {
        LogUtils.d(TAG, "noticeRoomSuperGiftMsg");
        getIMRoomEventObservable().onNext(new IMRoomEvent()
                .setEvent(IMRoomEvent.ROOM_RECEIVE_SUPER_GIFT)
                .setGiftReceiveInfo(giftReceiveInfo));
    }

    private void noticeImNetReLogin(IMRoomQueueInfo IMRoomQueueInfo) {
        getIMRoomEventObservable().onNext(new IMRoomEvent()
                .setEvent(RoomEvent.ROOM_CHAT_RECONNECTION)
                .setImRoomQueueInfo(IMRoomQueueInfo));
    }

    private void noticeRoomMemberChange(boolean isMemberIn, String account, IMRoomMember imRoomMember) {
        IMRoomEvent roomEvent = new IMRoomEvent();
        //进入房间的时候把IMRoomMember传过去
        if (imRoomMember != null) {
            IMRoomMessage imRoomMessage = new IMRoomMessage();
            imRoomMessage.setImRoomMember(imRoomMember);
            roomEvent.setIMRoomMessage(imRoomMessage);
        }

        getIMRoomEventObservable().onNext(roomEvent
                .setAccount(account)
                .setEvent(isMemberIn ? RoomEvent.ROOM_MEMBER_IN : RoomEvent.ROOM_MEMBER_EXIT));
    }

    private void noticeManagerChange(boolean isAdd, String account) {
        getIMRoomEventObservable().onNext(new IMRoomEvent()
                .setEvent(isAdd ? RoomEvent.ROOM_MANAGER_ADD : RoomEvent.ROOM_MANAGER_REMOVE)
                .setAccount(account));
    }


    private void noticeReceiverMessageImmediately(IMRoomMessage IMRoomMessage) {
        getIMRoomEventObservable().onNext(new IMRoomEvent()
                .setEvent(RoomEvent.RECEIVE_MSG)
                .setIMRoomMessage(IMRoomMessage)
        );
    }

    private void noticeReceiverMessage(IMRoomMessage IMRoomMessage) {
        getIMRoomMessagePublisher().onNext(IMRoomMessage);
    }


    public void noticeSocketEnterRoomSucMessages() {
        getIMRoomEventObservable().onNext(new IMRoomEvent().setEvent(RoomEvent.SOCKET_ROOM_ENTER_SUC));
    }

    private void noticeEnterMessages() {
        getIMRoomEventObservable().onNext(new IMRoomEvent().setEvent(RoomEvent.ENTER_ROOM));
    }

    public void noticeKickOutChatMember(int reason_no, String reason_msg, String account) {
        getIMRoomEventObservable().onNext(new IMRoomEvent()
                .setEvent(RoomEvent.KICK_OUT_ROOM)
                .setReasonMsg(reason_msg)
                .setReasonNo(reason_no)
                .setAccount(account));
    }

    private void noticeKickDownMic(int position) {
        getIMRoomEventObservable().onNext(new IMRoomEvent()
                .setEvent(RoomEvent.KICK_DOWN_MIC)
                .setMicPosition(position));
    }

    private void noticeInviteUpMic(int position, String account) {
        getIMRoomEventObservable().onNext(new IMRoomEvent().setEvent(RoomEvent.INVITE_UP_MIC)
                .setAccount(account)
                .setMicPosition(position));
    }

    public void noticeDownMic(String position, String account) {
        getIMRoomEventObservable().onNext(new IMRoomEvent()
                .setEvent(RoomEvent.DOWN_MIC)
                .setAccount(account)
                .setMicPosition(Integer.valueOf(position)));
    }


    private void noticeMicPosStateChange(int position, IMRoomQueueInfo IMRoomQueueInfo) {
        getIMRoomEventObservable().onNext(new IMRoomEvent()
                .setEvent(RoomEvent.MIC_QUEUE_STATE_CHANGE)
                .setMicPosition(position)
                .setImRoomQueueInfo(IMRoomQueueInfo));
    }


    private void noticeChatMemberBlackAdd(String account) {
        getIMRoomEventObservable().onNext(new IMRoomEvent()
                .setEvent(RoomEvent.ADD_BLACK_LIST)
                .setAccount(account));
    }


    private void noticeUpMic(int position, String account) {
        getIMRoomEventObservable().onNext(new IMRoomEvent()
                .setEvent(RoomEvent.UP_MIC)
                .setMicPosition(position)
                .setAccount(account)
        );
    }

    private void noticeMicInList() {
        getIMRoomEventObservable().onNext(new IMRoomEvent()
                .setEvent(RoomEvent.MIC_IN_LIST_UPDATE)

        );
    }

    /**
     * 被挤下麦通知
     */
    private void noticeDownCrowdedMic(int position, String account) {
        getIMRoomEventObservable().onNext(new IMRoomEvent()
                .setEvent(RoomEvent.DOWN_CROWDED_MIC)
                .setMicPosition(position)
                .setAccount(account)
        );
    }

    /**
     * 人气礼物更新通知
     *
     * @param detonatingState
     */
    private void noticePopularGiftUpadate(int detonatingState) {
        getIMRoomEventObservable().onNext(new IMRoomEvent()
                .setEvent(IMRoomEvent.ROOM_DETONATE_GIFT_NOTIFY)
                .setDetonatingState(detonatingState)
        );
    }

    /**
     * 魅力值更新
     *
     * @param roomCharmAttachment
     */
    private void noticeRoomCharmUpadate(RoomCharmAttachment roomCharmAttachment) {
        getIMRoomEventObservable().onNext(new IMRoomEvent()
                .setEvent(IMRoomEvent.ROOM_MIC_CHARM_UPDATE)
                .setRoomCharmInfo(roomCharmAttachment)
        );
    }

    /**
     * 房间悬浮气泡通知事件
     * 目前有：爆出礼物、情侣上麦、挚友上麦和红包
     *
     * @param imRoomMessage
     */
    private void noticeRoomFloatingBubbleUpadate(IMRoomMessage imRoomMessage) {
        getIMRoomEventObservable().onNext(new IMRoomEvent()
                .setEvent(IMRoomEvent.ROOM_FLOATING_BUBBL_NOTIFY)
                .setIMRoomMessage(imRoomMessage)
        );
    }

    /**
     * 竞拍房的通知：开始竞拍，竞拍位信息更新，竞拍结束
     *
     * @param second
     */
    private void noticeRoomAuctionNotice(int second, RoomAuctionBean roomAuctionBean) {
        getIMRoomEventObservable().onNext(new IMRoomEvent()
                .setEvent(IMRoomEvent.ROOM_AUCTION_STATE_NOTIFY)
                .setRoomAuctionBean(roomAuctionBean)
                .setAuctionState(second));
    }

    /**
     * 竞拍房参加竞拍人数更新通知
     *
     * @param count
     */
    private void noticeRoomJoinAuctionCountNotice(int second, int count) {
        getIMRoomEventObservable().onNext(new IMRoomEvent()
                .setEvent(IMRoomEvent.ROOM_AUCTION_STATE_NOTIFY)
                .setAuctionState(second).setAuctionJoinCount(count));
    }

    /**
     * 房间红包消息接收通知
     *
     * @param redPacketInfo
     */
    private void noticeRoomRedPackageRecieveNotice(RoomRedPacket redPacketInfo) {
        getIMRoomEventObservable().onNext(new IMRoomEvent()
                .setEvent(IMRoomEvent.ROOM_RECEIVE_RED_PACKET)
                .setRoomRedPacket(redPacketInfo));
    }

    /**
     * 房间红包消息领取完通知
     *
     * @param redPacketId
     */
    private void noticeRoomRedPackageFinishNotice(String redPacketId) {
        getIMRoomEventObservable().onNext(new IMRoomEvent()
                .setEvent(IMRoomEvent.ROOM_RECEIVE_RED_PACKET_FINISH)
                .setRedPacketId(redPacketId));
    }

    public void noticeRoomInfoUpdate(RoomInfo roomInfo) {
        getIMRoomEventObservable().onNext(new IMRoomEvent().setEvent(RoomEvent.ROOM_INFO_UPDATE).setRoomInfo(roomInfo));
    }

    private void noticeRoomConnect(RoomInfo roomInfo) {
        getIMRoomEventObservable().onNext(new IMRoomEvent().setEvent(IMRoomEvent.ROOM_RECONNECT).setRoomInfo(roomInfo));
    }

    private void noticeIMConnectLoginSuc() {
        getIMRoomEventObservable().onNext(new IMRoomEvent().setEvent(IMRoomEvent.SOCKET_IM_RECONNECT_LOGIN_SUCCESS));
    }

    public void setBeforeDisConnectionMuteStatus(int beforeDisConnectionMuteStatus) {
        this.beforeDisConnectionMuteStatus = beforeDisConnectionMuteStatus;
    }

    public boolean isImRoomConnection() {
        return imRoomConnection;
    }

    public void setImRoomConnection(boolean imNetsConnection) {
        this.imRoomConnection = imNetsConnection;
    }
}
