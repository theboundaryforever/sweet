package com.tongdaxing.xchat_core.liveroom.im.model.bean;

import java.io.Serializable;

/**
 * 等待麦序的用户信息
 */
public class IMWaitQueuePersonInfo implements Serializable {
    private int manWaitQueueNum;
    private int womenWaitQueueNum;
    private int totalManNum;
    private int totalWomenNum;

    public IMWaitQueuePersonInfo() {

    }

    public IMWaitQueuePersonInfo(int manWaitQueueNum, int womenWaitQueueNum, int totalManNum, int totalWomenNum) {
        this.manWaitQueueNum = manWaitQueueNum;
        this.womenWaitQueueNum = womenWaitQueueNum;
        this.totalManNum = totalManNum;
        this.totalWomenNum = totalWomenNum;
    }

    public int getManWaitQueueNum() {
        return manWaitQueueNum;
    }

    public void setManWaitQueueNum(int manWaitQueueNum) {
        this.manWaitQueueNum = manWaitQueueNum;
    }

    public int getWomenWaitQueueNum() {
        return womenWaitQueueNum;
    }

    public void setWomenWaitQueueNum(int womenWaitQueueNum) {
        this.womenWaitQueueNum = womenWaitQueueNum;
    }

    public int getTotalManNum() {
        return totalManNum;
    }

    public void setTotalManNum(int totalManNum) {
        this.totalManNum = totalManNum;
    }

    public int getTotalWomenNum() {
        return totalWomenNum;
    }

    public void setTotalWomenNum(int totalWomenNum) {
        this.totalWomenNum = totalWomenNum;
    }
}
