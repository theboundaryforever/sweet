package com.tongdaxing.xchat_core.liveroom.im.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tongdaxing.erban.libcommon.utils.JavaUtil;

import java.io.Serializable;
import java.util.List;

/**
 * @author dell
 */
public class IMRoomMember implements Parcelable, Serializable {

    // 聊天室内的头像，预留字段，可从NimUserInfo中取avatar，可以由用户进聊天室时提交。
    public String avatar;
    // 成员账号
    private long account;
    public static final Creator<IMRoomMember> CREATOR = new Creator<IMRoomMember>() {
        @Override
        public IMRoomMember createFromParcel(Parcel in) {
            return new IMRoomMember(in);
        }

        @Override
        public IMRoomMember[] newArray(int size) {
            return new IMRoomMember[size];
        }
    };
    /// 进入聊天室时提交
    // 聊天室内的昵称字段，预留字段，可从NimUserInfo中取，也可以由用户进聊天室时提交。
    private String nick;
    private int gender;
    //头饰
    private String headwear_url;
    private String headwear_name;
    //座驾名称
    private String car_name;
    //座驾动画url
    private String car_url;
    //财富等级
    private int exper_level;
    //魅力等级
    private int charm_level;
    private long create_time;
    private int age;
    private String province;
    private String city;
    // 成员是否处于在线状态，仅特殊成员才可能离线，对游客/匿名用户而言只能是在线[仅成员在线时有效]
    private boolean is_online;
    // 是禁言用户
    private boolean is_mute;
    private boolean is_creator;
    //是否是萌新
    private boolean is_new_user;
    //是否为管理员
    private boolean is_manager;
    // 是否在黑名单中
    private boolean is_black_list;
    // 进入聊天室的时间点,对于离线成员该字段为空
    private long enter_time;
    //在线人数用于退出和进入消息
    private int online_num;
    //在线人数时间戳用与判断在线人数更新的及时性
    private long timestamp;
    private long erbanNo;

    /**
     * 房间公屏-动态称号
     */
    private String alias;
    /**
     * 整型，贵族等级级别，0代表没有贵族等级，1-7代表相应的贵族级别
     */
    private int vipId;
    /**
     * 字符串类型，贵族级别描述，如“骑士”
     */
    private String vipName;
    /**
     * 贵族图标/勋章URL
     */
    private String vipIcon;
    private String vipMedal;
    /**
     * 整型，当前贵族有效期，以天为单位
     */
    private int vipDate;
    /**
     * 进房隐身开关，0关1开,更新接口传参时，以0/1为值传递
     */
    private boolean isInvisible = false;
    /**
     * 管理图标
     */
    private String adminUrl;
    /**
     * 专属聊天气泡
     */
    private String pointNineImg;
    private List<String> iconList;
    private int charmValue;
    private String charmHatUrl;
    //麦上光圈
    String halo;
    private String project;
    private int day;

    public IMRoomMember() {
    }

    public IMRoomMember(ChatRoomMember member) {
        this();
        if (member != null) {
            setAccount(member.getAccount());
            setNick(member.getNick());
            setAvatar(member.getAvatar());
            setIsBlackList(member.isInBlackList());
            setIsMute(member.isMuted());
            setEnterTime(member.getEnterTime());
        }
    }

    protected IMRoomMember(Parcel in) {
        avatar = in.readString();
        account = in.readLong();
        erbanNo = in.readLong();
        nick = in.readString();
        gender = in.readInt();
        headwear_url = in.readString();
        headwear_name = in.readString();
        car_name = in.readString();
        car_url = in.readString();
        exper_level = in.readInt();
        charm_level = in.readInt();
        create_time = in.readLong();
        age = in.readInt();
        province = in.readString();
        city = in.readString();
        is_online = in.readByte() != 0;
        is_mute = in.readByte() != 0;
        is_creator = in.readByte() != 0;
        is_new_user = in.readByte() != 0;
        is_manager = in.readByte() != 0;
        is_black_list = in.readByte() != 0;
        enter_time = in.readLong();
        online_num = in.readInt();
        timestamp = in.readLong();
        pointNineImg = in.readString();
        alias = in.readString();
        vipId = in.readInt();
        vipName = in.readString();
        vipIcon = in.readString();
        vipMedal = in.readString();
        vipDate = in.readInt();
        isInvisible = in.readByte() != 0;
        adminUrl = in.readString();
        iconList = in.createStringArrayList();
        charmValue = in.readInt();
        charmHatUrl = in.readString();
        halo = in.readString();
        project = in.readString();
        day = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(avatar);
        dest.writeLong(account);
        dest.writeLong(erbanNo);
        dest.writeString(nick);
        dest.writeInt(gender);
        dest.writeString(headwear_url);
        dest.writeString(headwear_name);
        dest.writeString(car_name);
        dest.writeString(car_url);
        dest.writeInt(exper_level);
        dest.writeInt(charm_level);
        dest.writeLong(create_time);
        dest.writeInt(age);
        dest.writeString(province);
        dest.writeString(city);
        dest.writeByte((byte) (is_online ? 1 : 0));
        dest.writeByte((byte) (is_mute ? 1 : 0));
        dest.writeByte((byte) (is_creator ? 1 : 0));
        dest.writeByte((byte) (is_new_user ? 1 : 0));
        dest.writeByte((byte) (is_manager ? 1 : 0));
        dest.writeByte((byte) (is_black_list ? 1 : 0));
        dest.writeLong(enter_time);
        dest.writeInt(online_num);
        dest.writeLong(timestamp);
        dest.writeString(pointNineImg);
        dest.writeString(alias);
        dest.writeInt(vipId);
        dest.writeString(vipName);
        dest.writeString(vipIcon);
        dest.writeString(vipMedal);
        dest.writeInt(vipDate);
        dest.writeByte((byte) (isInvisible ? 1 : 0));
        dest.writeString(adminUrl);
        dest.writeStringList(iconList);
        dest.writeInt(charmValue);
        dest.writeString(charmHatUrl);
        dest.writeString(halo);
        dest.writeString(project);
        dest.writeInt(day);
    }

    public String getPointNineImg() {
        return pointNineImg;
    }

    public void setPointNineImg(String point_nine_img) {
        this.pointNineImg = point_nine_img;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public int getVipId() {
        return vipId;
    }

    public void setVipId(int vipId) {
        this.vipId = vipId;
    }

    public String getVipName() {
        return vipName;
    }

    public void setVipName(String vipName) {
        this.vipName = vipName;
    }

    public String getVipIcon() {
        return vipIcon;
    }

    public void setVipIcon(String vipIcon) {
        this.vipIcon = vipIcon;
    }

    public String getVipMedal() {
        return vipMedal;
    }

    public void setVipMedal(String vipMedal) {
        this.vipMedal = vipMedal;
    }

    public int getVipDate() {
        return vipDate;
    }

    public void setVipDate(int vipDate) {
        this.vipDate = vipDate;
    }

    public boolean isInvisible() {
        return isInvisible;
    }

    public void setInvisible(boolean invisible) {
        isInvisible = invisible;
    }

    public String getAdminUrl() {
        return adminUrl;
    }

    public void setAdminUrl(String adminUrl) {
        this.adminUrl = adminUrl;
    }

    public List<String> getIconList() {
        return iconList;
    }

    public void setIconList(List<String> iconList) {
        this.iconList = iconList;
    }

    public boolean isManager() {
        return is_manager;
    }

    public void setIsManager(boolean is_manager) {
        this.is_manager = is_manager;
    }

    /**
     * 获取成员帐号
     *
     * @return 成员account
     */
    public String getAccount() {
        return String.valueOf(account);
    }


    public long getLongAccount(){
        return account;
    }
    /**
     * 设置用户帐号
     *
     * @param account 用户帐号
     */
    public void setAccount(String account) {
        this.account = JavaUtil.str2long(account);
    }

    /**
     * 获取昵称
     * 可从NimUserInfo中取，也可以由用户进聊天室时提交。
     *
     * @return 昵称
     */
    public String getNick() {
        return nick;
    }

    /**
     * 设置成员昵称
     *
     * @param nick 昵称
     */
    public void setNick(String nick) {
        this.nick = nick;
    }

    /**
     * 获取头像
     * 可从NimUserInfo中取avatar，可以由用户进聊天室时提交。
     *
     * @return 头像
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * 设置成员头像
     *
     * @param avatar 头像
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     * 获取进入聊天室时间
     * 对于离线成员该字段为空
     *
     * @return 进入聊天室时间
     */
    public long getEnterTime() {
        return enter_time;
    }

    /**
     * 设置进入聊天室时间
     *
     * @param enter_time 进入聊天室时间
     */
    public void setEnterTime(long enter_time) {
        this.enter_time = enter_time;
    }

    /**
     * 判断用户是否在黑名单中
     *
     * @return 是否在黑名单中
     */
    public boolean isIsBlackList() {
        return is_black_list;
    }

    /**
     * 设置是否在黑名单中
     *
     * @param is_black_list 是否设置为黑名单
     */
    public void setIsBlackList(boolean is_black_list) {
        this.is_black_list = is_black_list;
    }

    /**
     * 判断用户是否被禁言
     *
     * @return 是否被禁言
     */
    public boolean isIsMute() {
        return is_mute;
    }

    /**
     * 设置是否禁言
     *
     * @param is_mute 是否禁言
     */
    public void setIsMute(boolean is_mute) {
        this.is_mute = is_mute;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getExperLevel() {
        return exper_level;
    }

    public void setExperLevel(int experLevel) {
        this.exper_level = experLevel;
    }

    public int getCharmLevel() {
        return charm_level;
    }

    public void setCharmLevel(int charmLevel) {
        this.charm_level = charmLevel;
    }

    public boolean isIsNewUser() {
        return is_new_user;
    }

    public String getCarName() {
        return car_name;
    }

    public void setCarName(String car_name) {
        this.car_name = car_name;
    }

    public boolean getIsNewUser() {
        return is_new_user;
    }

    public void setIsNewUser(boolean is_new_user) {
        this.is_new_user = is_new_user;
    }

    public String getHeadwearUrl() {
        return headwear_url;
    }

    public void setHeadwearUrl(String headwear_url) {
        this.headwear_url = headwear_url;
    }

    public String getCarUrl() {
        return car_url;
    }

    public void setCarUrl(String car_url) {
        this.car_url = car_url;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getOnlineNum() {
        return online_num < 0 ? 0 : online_num;
    }

    public void setOnlineNum(int online_num) {
        this.online_num = online_num;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getErbanNo() {
        return erbanNo;
    }

    public void setErbanNo(long erbanNo) {
        this.erbanNo = erbanNo;
    }

    public boolean isIs_online() {
        return is_online;
    }

    public void setIs_online(boolean is_online) {
        this.is_online = is_online;
    }

    public int getCharmValue() {
        return charmValue;
    }

    public void setCharmValue(int charmValue) {
        this.charmValue = charmValue;
    }

    public String getCharmHatUrl() {
        return charmHatUrl;
    }

    public void setCharmHatUrl(String charmHatUrl) {
        this.charmHatUrl = charmHatUrl;
    }

    public String getHalo() {
        return halo;
    }

    public void setHalo(String halo) {
        this.halo = halo;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public String toString() {
        return "{" +
                "avatar='" + avatar + '\'' +
                ", account=" + account +
                ", erbanNo=" + erbanNo +
                ", nick='" + nick + '\'' +
                ", gender=" + gender +
                ", headwear_url='" + headwear_url + '\'' +
                ", headwear_name='" + headwear_name + '\'' +
                ", car_name='" + car_name + '\'' +
                ", car_url='" + car_url + '\'' +
                ", exper_level=" + exper_level +
                ", charm_level=" + charm_level +
                ", create_time=" + create_time +
                ", age=" + age +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", is_online=" + is_online +
                ", is_mute=" + is_mute +
                ", is_creator=" + is_creator +
                ", is_new_user=" + is_new_user +
                ", is_manager=" + is_manager +
                ", is_black_list=" + is_black_list +
                ", enter_time=" + enter_time +
                ", online_num=" + online_num +
                ", timestamp=" + timestamp +
                ", pointNineImg='" + pointNineImg + '\'' +
                ", alias='" + alias + '\'' +
                ", vipId=" + vipId +
                ", vipName='" + vipName + '\'' +
                ", vipIcon='" + vipIcon + '\'' +
                ", vipMedal='" + vipMedal + '\'' +
                ", vipDate=" + vipDate +
                ", isInvisible=" + isInvisible +
                ", adminUrl='" + adminUrl + '\'' +
                '}';
    }

}
