package com.tongdaxing.xchat_core.liveroom.im.model;

import com.tongdaxing.erban.libcommon.coremanager.ICoreClient;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMessage;

/**
 * Created by zhouxiangfeng on 2017/5/20.
 *
 */
public interface IMRoomCoreClient extends ICoreClient {

    String METHOD_ON_SEND_IMROOM_MESSAGE_SUCCESS = "onSendIMRoomMessageSuccess";
    void onSendIMRoomMessageSuccess(IMRoomMessage message);

}
