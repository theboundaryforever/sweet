package com.tongdaxing.xchat_core.liveroom.im.model;

import android.os.Handler;
import android.os.Message;

import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomQueueInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * 房主直播时长计时器
 * <p>
 * 1.进入自己的房间
 * 2.切换房间
 * 3.退出房间
 * 4.最小化后关闭房间
 * 5.最小化后回到自己的房间
 * 6.最小化后直接退出登录
 *
 * @author weihaitao
 * @date 2019/4/26
 */
public class RoomOwnerLiveTimeCounter {

    private final String TAG = RoomOwnerLiveTimeCounter.class.getSimpleName();
    private static RoomOwnerLiveTimeCounter instance = null;
    /**
     * 是否开始房主直播时长计时
     */
    private boolean hasStarted = false;

    private List<OnOwnerLiveTimeUpdateListener> listeners = new ArrayList<>();
    private Handler mHandler;

    private long timeCount = 0L;

    private RoomOwnerLiveTimeCounter() {
    }

    public static RoomOwnerLiveTimeCounter getInstance() {

        if (null == instance) {
            instance = new RoomOwnerLiveTimeCounter();
        }

        return instance;
    }

    public void setTimeCount(long timeCount) {
        this.timeCount = timeCount;
    }

    public void startCount() {
        if (hasStarted) {
            LogUtils.e(TAG, "startCount-hasStarted is true，计时器已经启动");
            return;
        }
        if (null == listeners || listeners.size() == 0) {
            LogUtils.e(TAG, "startCount-listener is null，要计时器更新什么？！");
            return;
        }

        boolean isRoomAdminOnLine = false;

        try {
            //这里需要在对应map更新的情况下调用
            IMRoomQueueInfo roomQueueInfo = RoomDataManager.get().getRoomQueueMemberInfoByMicPosition(-1);
            isRoomAdminOnLine = null != roomQueueInfo && null != roomQueueInfo.mChatRoomMember;
        } catch (Exception e) {
            e.printStackTrace();
            //房主离线
        }
        if (!isRoomAdminOnLine) {
            LogUtils.e(TAG, "startCount 房主不在线，不启动计时器");
            return;
        }

        //判断开播状态
        if (RoomDataManager.get().getAdditional() == null) {
            LogUtils.e(TAG, "startCount 房主未开播，不启动计时");
            return;
        }

        if (RoomDataManager.get().getAdditional().getPlayState() != 1) {
            LogUtils.e(TAG, "startCount 房主未开播，不启动计时 mPlayState=" + RoomDataManager.get().getAdditional().getPlayState());
            return;
        }

        LogUtils.d(TAG, "startCount 开始计时");
        if (null == mHandler) {
            mHandler = new Handler(new Handler.Callback() {
                @Override
                public boolean handleMessage(Message msg) {
                    if (msg.what == 0) {
                        timeCount += 1;
                        if (null != listeners) {
                            for (OnOwnerLiveTimeUpdateListener listener : listeners) {
                                listener.onOwnerLiveTimeUpdated(timeCount);
                            }
                        }
                        mHandler.sendEmptyMessageDelayed(0, 1000L);
                        return true;
                    }
                    return false;
                }
            });
        }
        hasStarted = true;
        mHandler.sendEmptyMessageDelayed(0, 1000L);
    }

    public void release() {
        LogUtils.d(TAG, "release 结束计时");
        stopCount();
        if (null != listeners) {
            listeners.clear();
        }
    }

    public void stopCount() {
        if (null != mHandler) {
            mHandler.removeMessages(0);
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }
        hasStarted = false;
        //房主开播时长，每次都从开播事计时
        timeCount = 0L;
        LogUtils.d(TAG, "stopCount hasStarted:" + hasStarted + " timeCount:" + timeCount);
    }

    /**
     * 添加房主直播时长更新监听器，注意需要保证每次第一次进入房间的时候，初始化timeCount
     * @param listener
     */
    public void addListener(OnOwnerLiveTimeUpdateListener listener) {
        if (null != listeners && !listeners.contains(listener)) {
            listeners.add(listener);
            LogUtils.d(TAG, "addListener hasStarted:" + hasStarted);
        }
    }

    public void removeListener(OnOwnerLiveTimeUpdateListener listener) {
        if (null != listeners && listeners.contains(listener)) {
            LogUtils.d(TAG, "removeListener hasStarted:" + hasStarted);
            listeners.remove(listener);
        }
    }

    public interface OnOwnerLiveTimeUpdateListener {
        /**
         * 直播时长更新回调
         *
         * @param millCounts 秒为单位
         */
        void onOwnerLiveTimeUpdated(long millCounts);
    }
}
