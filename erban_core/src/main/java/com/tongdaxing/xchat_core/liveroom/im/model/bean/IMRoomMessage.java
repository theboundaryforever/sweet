package com.tongdaxing.xchat_core.liveroom.im.model.bean;

import com.tongdaxing.erban.libcommon.im.IMReportRoute;
import com.tongdaxing.erban.libcommon.widget.messageview.IMessage;
import com.tongdaxing.erban.libcommon.widget.messageview.RoomMsgHolerType;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;

/**
 * 新的消息实体
 */
public class IMRoomMessage extends IMessage {
    private String room_id;
    private String content;
    private String route;//路由协议替换msgType
    private IMRoomMember imRoomMember;
    private CustomAttachment attachment;
    private int msgShowType = -1;

    public IMRoomMessage() {
    }

    public IMRoomMessage(String room_id, String content) {
        this.room_id = room_id;
        this.content = content;
    }

    public CustomAttachment getAttachment() {
        return attachment;
    }

    public void setAttachment(CustomAttachment attachment) {
        this.attachment = attachment;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getContent() {
        return content;
    }


    public void setContent(String content) {
        this.content = content;
    }

    public IMRoomMember getImRoomMember() {
        return imRoomMember;
    }

    public void setImRoomMember(IMRoomMember imRoomMember) {
        this.imRoomMember = imRoomMember;
    }

    public String getRoomId() {
        return room_id;
    }

    public void setRoomId(String room_id) {
        this.room_id = room_id;
    }

    @Override
    public int getMsgShowType() {
        if ( msgShowType == -1) {//需要计算类型
            if (IMReportRoute.ChatRoomTip.equalsIgnoreCase(route)) {//提示消息  - 不显示等级
                msgShowType = RoomMsgHolerType.MSG_TXT;
            } else if (IMReportRoute.sendTextReport.equalsIgnoreCase(route)) {
                msgShowType = RoomMsgHolerType.MSG_CHAT_TXT;
            } else if (IMReportRoute.chatRoomMemberIn.equalsIgnoreCase(route)) {
                msgShowType = RoomMsgHolerType.MSG_CHAT_TXT;
            } else if (IMReportRoute.sendMessageReport.equalsIgnoreCase(route)) {
                if (attachment != null) {
                    if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_FIRST
                            || attachment.getFirst() == CustomAttachment.CUSTOM_MSG_TYPE_RULE_FIRST
                            || attachment.getFirst() == CustomAttachment.CUSTOM_MSG_ROOM_RICH_TXT) {
                        msgShowType = RoomMsgHolerType.MSG_TXT;
                    } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE
                            || attachment.getFirst() == CustomAttachment.CUSTOM_MSG_ROOM_OPERA_TIPS
                            || attachment.getFirst() == CustomAttachment.CUSTOM_MSG_ROOM_SYSTEM_RICH_TXT) {
                        msgShowType = RoomMsgHolerType.MSG_CHAT_TXT;
                    } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT
                            || attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT
                            || attachment.getFirst() == CustomAttachment.CUSTOM_MSG_LOTTERY_BOX
                            || attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_FACE) {
                        msgShowType = RoomMsgHolerType.MSG_GIFT;
                    } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_FIRST_ROOM_RED_PACKAGE) {//房间红包消息
                        if (attachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_ROOM_SEND_RED_PACKAGE
                                || attachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_ROOM_RECEIVE_RED_PACKAGE
                                || attachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_ROOM_RED_PACKAGE_FULL_SERVICE) {
                            msgShowType = RoomMsgHolerType.MSG_CHAT_TXT;
                        } else {
                            msgShowType = RoomMsgHolerType.MSG_TXT;
                        }
                    } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_FIRST_ROOM_ATTENTION_TIPS) {
                        if (attachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_ROOM_ATTENTION_TIPS) {
                            msgShowType = RoomMsgHolerType.MSG_CHAT_TXT;
                        } else if (attachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_ROOM_ATTENTION_GUIDE) {
                            msgShowType = RoomMsgHolerType.MSG_TXT_NO_BG;
                        } else {
                            msgShowType = RoomMsgHolerType.MSG_TXT;
                        }
                    } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_FIRST_ROOM_ADMIRE_NOTIFY) {
                        if (attachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_ROOM_ADMIRE_MSG) {
                            msgShowType = RoomMsgHolerType.MSG_CHAT_TXT;
                        } else {
                            msgShowType = RoomMsgHolerType.MSG_TXT;
                        }
                    } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK) {
                        msgShowType = RoomMsgHolerType.MSG_TXT;
                    } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_ROOM_ATTENTION) {
                        msgShowType = RoomMsgHolerType.MSG_TXT_NO_BG;
                    } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_FIRST_AUCTION){
                        msgShowType = RoomMsgHolerType.MSG_TXT;
                    }else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_ROOM_RED_PACKET_FINISH){
                        if (attachment.getSecond()  == CustomAttachment.CUSTOM_MSG_ROOM_RED_PACKET_RECEIVE_FINISH){
                            msgShowType = RoomMsgHolerType.MSG_TXT;
                        }else {
                            msgShowType = RoomMsgHolerType.MSG_TXT;
                        }
                    } else {
                        msgShowType = RoomMsgHolerType.MSG_TXT;
                    }
                }else {
                    msgShowType = RoomMsgHolerType.MSG_TXT;
                }
            } else {
                msgShowType = RoomMsgHolerType.MSG_TXT;
            }
            return msgShowType;
        } else {
            return msgShowType;
        }
    }
}
