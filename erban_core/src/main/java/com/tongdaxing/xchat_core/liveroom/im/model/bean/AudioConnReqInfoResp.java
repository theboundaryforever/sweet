package com.tongdaxing.xchat_core.liveroom.im.model.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @author weihaitao
 * @date 2019/7/16
 */
public class AudioConnReqInfoResp implements Serializable {

    /**
     * 开关状态1打开 0关闭
     */
    public int openState;

    /**
     * 申请列表
     */
    public List<AudioConnReqInfo> applyList;

    /**
     * 正在连线列表
     */
    public List<AudioConnReqInfo> micList;

    /**
     * 已结束列表
     */
    public List<AudioConnReqInfo> overList;
}
