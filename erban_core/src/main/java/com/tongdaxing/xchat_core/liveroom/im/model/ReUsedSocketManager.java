package com.tongdaxing.xchat_core.liveroom.im.model;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.im.ICommonListener;
import com.tongdaxing.erban.libcommon.im.IConnectListener;
import com.tongdaxing.erban.libcommon.im.IMCallBack;
import com.tongdaxing.erban.libcommon.im.IMError;
import com.tongdaxing.erban.libcommon.im.IMErrorBean;
import com.tongdaxing.erban.libcommon.im.IMHeartBeatDataListener;
import com.tongdaxing.erban.libcommon.im.IMKey;
import com.tongdaxing.erban.libcommon.im.IMModelFactory;
import com.tongdaxing.erban.libcommon.im.IMNoticeMsgListener;
import com.tongdaxing.erban.libcommon.im.IMProCallBack;
import com.tongdaxing.erban.libcommon.im.IMReportBean;
import com.tongdaxing.erban.libcommon.im.IMSendRoute;
import com.tongdaxing.erban.libcommon.im.SocketManager;
import com.tongdaxing.erban.libcommon.ninepatchloader.NinePatchBitmapLoader;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.utils.json.JsonParser;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RoomMicInfo;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.liveroom.im.IMSendCallBack;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMember;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMessage;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomQueueInfo;
import com.tongdaxing.xchat_core.room.auction.RoomAuctionBean;
import com.tongdaxing.xchat_core.room.bean.RoomAdditional;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;

import org.java_websocket.handshake.ServerHandshake;

import java.util.ArrayList;
import java.util.List;


/**
 *
 */
public class ReUsedSocketManager {

    public final static String TAG = "room_log ---> IM";
    private static ReUsedSocketManager reUsedSocketManager = null;
    private ICommonListener iCommonListener = null;
    private SocketManager socketManager = null;
    private IConnectListener connectListener;
    private IMNoticeMsgListener imNoticeMsgListener;

    private IMHeartBeatDataListener imHeartBeatDataListener;

    public void setImHeartBeatDataListener(IMHeartBeatDataListener imHeartBeatDataListener) {
        this.imHeartBeatDataListener = imHeartBeatDataListener;
    }

    public static ReUsedSocketManager get() {
        if (reUsedSocketManager == null) {
            synchronized (ReUsedSocketManager.class) {
                if (reUsedSocketManager == null) {
                    reUsedSocketManager = new ReUsedSocketManager();
                }
            }
        }
        return reUsedSocketManager;
    }

    private void imLogin(final long uid, final String ticket) {
        LogUtils.i(TAG, "imLogin ---> uid = " + uid);
        IMProCallBack imProCallBack = new IMProCallBack() {
            @Override
            public void onSuccessPro(IMReportBean imReportBean) {
                int errno = imReportBean.getReportData().errno;
                LogUtils.i("im_ticket", "im_login ---- >ticket = " + ticket + " errno = " + errno + " errorMsg = " + imReportBean.getReportData().errmsg);
                LogUtils.i(TAG, "imLogin ---> errno = " + errno);
                if (errno != 0) {
                    if (!ReUsedSocketManager.get().isConnect()) {
                        ReUsedSocketManager.get().connect(uid, connectListener, 3000); //登录失败而且socket断开状态重连
                    } else {
                        if (errno != IMError.IM_ERROR_LOGIN_AUTH_FAIL && errno != IMError.IM_ERROR_GET_USER_INFO_FAIL) {
                            imLogin(uid, ticket);
                        }
                        if (imNoticeMsgListener != null) {
                            imNoticeMsgListener.onLoginError(errno, imReportBean.getReportData().errmsg);
                        }
                    }
                } else {
                    if (imNoticeMsgListener != null) {
                        imNoticeMsgListener.onDisConntectIMLoginSuc();
                    }
                    onImLoginRenewEnterRoom();
                }
            }

            @Override
            public void onError(int errorCode, String errorMsg) {
                LogUtils.i("im_ticket", "im_login ---- >ticket = " + ticket + " errorCode = " + errorCode + " errorMsg = " + errorMsg);
                LogUtils.i(TAG, "imLogin ---> errorCode = " + errorCode);
            }
        };
        LogUtils.i("im_ticket", "im_login ---- >ticket = " + ticket);
        ReUsedSocketManager.get().send(IMModelFactory.get().createLoginModel(ticket, String.valueOf(uid)), imProCallBack);
    }

    /**
     * im登录之后如果再房间，需要重新进入房间
     */
    private void onImLoginRenewEnterRoom() {
        LogUtils.i(TAG, "onImLogin ---> enterRoom");
        if (RoomDataManager.get().getCurrentRoomInfo() != null) {
            long roomUid = RoomDataManager.get().getCurrentRoomInfo().getUid();
            int roomType = RoomDataManager.get().getCurrentRoomInfo().getType();
            int reconnect = 1;//1表示重连
            ReUsedSocketManager.get().enterWithOpenChatRoom(roomUid, roomType, null, reconnect, new IMProCallBack() {
                @Override
                public void onSuccessPro(IMReportBean imReportBean) {
                    LogUtils.i(TAG, "onImLogin ---> onReconnection");
                    if (imNoticeMsgListener != null) {
                        imNoticeMsgListener.onDisConnectEnterRoomSuc();
                    }
                }

                @Override
                public void onError(int errorCode, String errorMsg) {
                    LogUtils.i(TAG, "onImLogin ---> enterRoom error " + errorMsg);
                    if (imNoticeMsgListener != null) {
                        imNoticeMsgListener.onDisConnectEnterRoomFail(errorCode,errorMsg);
                    }
                }
            });
        }
    }

    private void setRoomHistoryMessage(List<Json> historyMessageList) {
        LogUtils.d(TAG, "setRoomHistoryMessage start");
        if (null == historyMessageList || historyMessageList.size() == 0) {
            return;
        }
        List<IMRoomMessage> imRoomMessageList = new ArrayList<>();
        IMRoomMessage imRoomMessage = null;
        String room_id = "-1";
        String content = "";
        String route = "";//路由协议替换msgType
        IMRoomMember member = null;
        CustomAttachment attachment = null;

        for (int i = 0; i < historyMessageList.size(); i++) {
            Json json = historyMessageList.get(i);
            try {
                route = json.str(IMKey.route);
                room_id = json.str(IMKey.room_id, "-1");
                member = JsonParser.parseJsonObject(json.json(IMKey.member).toString(), IMRoomMember.class);
                if (json.has(IMKey.custom)) {
                    attachment = JsonParser.parseJsonObject(json.json(IMKey.custom).toString(), CustomAttachment.class);
                }
                content = json.str(IMKey.content);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (member == null) {
                return;
            }
            imRoomMessage = new IMRoomMessage();
            imRoomMessage.setRoute(route);
            imRoomMessage.setRoomId(room_id);
            imRoomMessage.setImRoomMember(member);
            imRoomMessage.setContent(content);
            if (null != attachment) {
                imRoomMessage.setAttachment(attachment);
            }
            imRoomMessageList.add(imRoomMessage);
        }
        RoomDataManager.get().mRoomHistoryList = imRoomMessageList;
        LogUtils.d(TAG, "setRoomHistoryMessage end");
    }

    /**
     * 处理进入房间的socket返回信息
     * @param room_info
     * @param additional
     * @param room_usr
     * @param member
     * @param queueList
     */
    private void setServerMicInfo(String room_info,String additional, String room_usr,String member,String auction, List<Json> queueList) {
        RoomInfo extRoomInfo = JsonParser.parseJsonToNormalObject(room_info, RoomInfo.class);
        RoomAdditional roomAdditional  = JsonParser.parseJsonToNormalObject(additional,RoomAdditional.class);
        IMRoomMember roomOwer = JsonParser.parseJsonToNormalObject(room_usr, IMRoomMember.class);
        IMRoomMember chatRoomMember = JsonParser.parseJsonToNormalObject(member, IMRoomMember.class);
        if (chatRoomMember != null) {
            RoomDataManager.get().mSelfRoomMember = chatRoomMember;
            if (!TextUtils.isEmpty(chatRoomMember.getPointNineImg())) {
                NinePatchBitmapLoader.getInstance().loadNinePatchBitmap(null,
                        chatRoomMember.getPointNineImg(), 0);
            }
        }
        if (roomAdditional != null){
            RoomDataManager.get().setAdditional(roomAdditional);
            //更新主播剩余召集令次数
            RoomDataManager.get().conveneCount = roomAdditional.getConveneCount();
            //初始化房间召集令已召集人数
            RoomDataManager.get().conveneUserCount = roomAdditional.getConveneUserCount();
            //初始化房间召集令有效状态
            RoomDataManager.get().conveneState = roomAdditional.getConveneState();
            RoomOwnerLiveTimeCounter.getInstance().setTimeCount(roomAdditional.getPlayTime());
        }
        RoomDataManager.get().setmOwnerRoomMember(roomOwer);
        //同步房间信息-这里的同步可能导致com.tongdaxing.erban.liveroom.presenter.RoomFramePresenter.enterRoom直接return
        RoomDataManager.get().setCurrentRoomInfo(extRoomInfo);
        //竞拍房信息
        RoomAuctionBean roomAuction = null;
        if (StringUtils.isNotEmpty(auction)){
            roomAuction =  JsonParser.parseJsonToNormalObject(auction, RoomAuctionBean.class);
        }
        RoomDataManager.get().setAuction(roomAuction);

        //同步麦序
        for (int i = 0; i < queueList.size(); i++) {
            Json json = queueList.get(i);
            int key = json.num("key");
            Json value = json.json_ok("value");
            IMRoomMember imChatRoomMember = null;
            if (value.has("member")) {
                imChatRoomMember = JsonParser.parseJsonToNormalObject(value.str("member"), IMRoomMember.class);
            }
            RoomMicInfo micInfo = JsonParser.parseJsonToNormalObject(value.str("mic_info"), RoomMicInfo.class);
            IMRoomQueueInfo roomQueueInfo = new IMRoomQueueInfo(micInfo, imChatRoomMember);
            RoomDataManager.get().mMicQueueMemberMap.put(key, roomQueueInfo);
        }
    }


    //-------------------------------------------对外开放的方法-------------------------------------------------------------------

    /**
     * 注册断开回调函数  告诉业务层断开( code 告诉我们时超时导致还是网络导致断开还是手动断开)
     * 注册服务器单向推消息处理回调
     *
     * @param iCommonListener
     */
    public void setiCommonListener(ICommonListener iCommonListener) {
        this.iCommonListener = iCommonListener;
        if (iCommonListener != null && socketManager != null) {
            socketManager.setiCommonListener(iCommonListener);
        }
    }

    /**
     * 手动断开链接
     */
    public void disconnect() {
        if (socketManager != null) {
            socketManager.disconnect();
        }
    }

    public void destroy() {
        if (socketManager != null) {
            socketManager.destroy();
        }
    }


    /**
     * @return 链接状态
     */
    public boolean isConnect() {
        return socketManager != null && socketManager.isConnect();

    }


    /**
     * 链接socket
     */
    private void connect(long uid, IConnectListener iConnectListener, int delay) {
        if (socketManager != null) {
            socketManager.destroy();
        }
        socketManager = new SocketManager();
        if (null != imHeartBeatDataListener) {
            socketManager.setImHeartBeatDataListener(imHeartBeatDataListener);
        }
        try {
            if (iCommonListener != null) {
                socketManager.setiCommonListener(iCommonListener);
            }
            socketManager.connect(iConnectListener, delay);
        } catch (Exception e) {
            if (iConnectListener != null) {
                iConnectListener.onError(e);
            }
        }
    }

    public void setImNoticeMsgListener(IMNoticeMsgListener imNoticeMsgListener) {
        this.imNoticeMsgListener = imNoticeMsgListener;

    }

    /**
     * 对服务器发送消息
     *
     * @param content    发送内容
     * @param imCallBack 结果的回调
     */
    public void send(Json content, @NonNull IMCallBack imCallBack) {
        com.tongdaxing.erban.libcommon.utils.LogUtils.d("request_info_im_send", content.toString());
        if (socketManager != null) {
            socketManager.send(content.toString(), imCallBack);
        } else {
            imCallBack.onError(-1, IMError.IM_MS_SEND_ERROR + ":socketManager is null");
        }
    }


    /**
     * 登录回调之后初始化im，并登录
     */
    public void initIM() {
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        final String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
        final int reConnectTIme = 3000;
        connectListener = new IConnectListener() {
            @Override
            public void onSuccess(ServerHandshake serverHandshake) {
                LogUtils.i(TAG, "initIM ---> onOpen ---> HttpStatus = " + serverHandshake.getHttpStatus() + " HttpStatusMessage = " + serverHandshake.getHttpStatusMessage());
                imLogin(uid, ticket);
            }

            @Override
            public void onError(Exception e) {
                LogUtils.i(TAG, "initIM ---> onError ---> Exception = " + e.getMessage());
                LogUtils.i(TAG, e.getMessage());
                if (e.getMessage() == "Dubble connect!") {
                    return;
                }
                ReUsedSocketManager.get().connect(uid, connectListener, reConnectTIme); //断线重连延迟3秒执行
            }
        };
        connect(uid, connectListener, 0);
        setiCommonListener(new ICommonListener() {
            @Override
            public void onDisconnectCallBack(IMErrorBean err) {
                boolean isCloseSelf = err.getCloseReason() == SocketManager.CALL_BACK_CODE_SELFCLOSE;
                LogUtils.i(TAG, "initIM ---> onDisconnectCallBack ---> isCloseSelf = " + isCloseSelf + " err_code = " + err.getCode() + " reason = " + err.getReason());
                if (!isCloseSelf) { // 非手动关闭自动重连
                    ReUsedSocketManager.get().connect(uid, connectListener, reConnectTIme); // 断线重连延迟执行
                }
                if (imNoticeMsgListener != null) {
                    imNoticeMsgListener.onDisConnection(isCloseSelf);
                }

            }

            @Override
            public void onNoticeMessage(String message) {
                if (imNoticeMsgListener != null) {
                    imNoticeMsgListener.onNotice(Json.parse(message));
                }
            }
        });
    }

    /**
     * @param roomUid       房主UID
     * @param roomType      房间类型，参考RoomInfo
     * @param roomPwd       房间密码
     * @param reconnect     是否重连表示，0表示正常进房，1表示断线重连进房
     * @param imProCallBack
     */
    public void enterWithOpenChatRoom(long roomUid, int roomType, String roomPwd, int reconnect, final IMProCallBack imProCallBack) {
        String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
        ReUsedSocketManager.get().send(IMModelFactory.get().enterWithOpenChatRoom(roomUid, roomType, roomPwd, reconnect, ticket), new IMProCallBack() {
            @Override
            public void onSuccessPro(IMReportBean imReportBean) {
                com.tongdaxing.erban.libcommon.utils.LogUtils.d(TAG, "enterWithOpenChatRoom --->  onSuccessPro reconnect:" + reconnect);
                IMRoomMessageManager.get().setImRoomConnection(true);
                IMReportBean.ReportData reportData = imReportBean.getReportData();
                if (reportData.errno != 0) {
                    com.tongdaxing.erban.libcommon.utils.LogUtils.d(TAG, "enterWithOpenChatRoom ---> onSuccessPro --- > errno = " + reportData.errno);
                    imProCallBack.onError(reportData.errno, reportData.errmsg);
                    return;
                }
                Json data = reportData.data;
                //可能会解析错误
                try {
                    if (0 == reconnect) {
                        //仅正常进入房间流程才解析房间历史消息记录，断线重连不予理会
                        setRoomHistoryMessage(data.jlist("his_list"));
                    }
                    setServerMicInfo(data.str("room_info"), data.str("additional"),data.str("room_user"),data.str("member"),data.str("auction"), data.jlist("queue_list"));
                } catch (Exception e) {
                    imProCallBack.onError(-1101, "");
                    return;
                }
                imProCallBack.onSuccessPro(imReportBean);
            }

            @Override
            public void onError(int errorCode, String errorMsg) {
                com.tongdaxing.erban.libcommon.utils.LogUtils.d(TAG, "enterWithOpenChatRoom --->onError ---> errorCode = " + errorCode);
                imProCallBack.onError(errorCode, errorMsg);
            }
        });
    }

    /**
     * 更新麦序操作：加入队列  如果已经其他麦序位置上，会自动更新位置
     *
     * @param roomId
     * @param micPosition
     * @param uid
     * @param imCallBack
     */
    public void updateQueue(String roomId, int micPosition, long uid, final IMCallBack imCallBack) {
        LogUtils.i(TAG, "updateQueue ---> roomId = " + roomId + " --- micPosition = " + micPosition + " --- uid = " + uid);
        ReUsedSocketManager.get().send(IMModelFactory.get().createUpdateQueue(roomId, micPosition, uid), imCallBack);
    }

    /**
     * 退出房间
     *
     * @param roomId
     * @param imProCallBack
     */
    public void exitRoom(long roomId, IMProCallBack imProCallBack) {
        LogUtils.i(TAG, "exitRoom ---> roomId = " + roomId);
        send(IMModelFactory.get().createExitRoom(roomId), imProCallBack);
    }


    /**
     * 退出公聊房间
     *
     * @param roomId
     * @param imProCallBack
     */
    public void exitPublicRoom(long roomId, IMProCallBack imProCallBack) {
        LogUtils.i(TAG, "exitRoom ---> roomId = " + roomId);
        send(IMModelFactory.get().createExitPublicRoom(roomId), imProCallBack);
    }

    /**
     * 更新麦序操作：加入队列  如果已经其他麦序位置上，会自动更新位置
     *
     * @param roomId
     * @param micPosition
     * @param imSendCallBack
     */
    public void pollQueue(String roomId, int micPosition, final IMSendCallBack imSendCallBack) {
        LogUtils.i(TAG, "updateQueue ---> roomId = " + roomId + " --- micPosition = " + micPosition);
        send(IMModelFactory.get().createPollQueue(roomId, micPosition), imSendCallBack);
    }


    /**
     * 发送文本消息
     *
     * @param roomId
     * @param message
     * @param imSendCallBack
     */
    public void sendTxtMessage(String roomId, IMRoomMessage message, IMCallBack imSendCallBack) {
        Json json = new Json();
        json.set("room_id", roomId);
        json.set(IMKey.member, JsonParser.toJson(message.getImRoomMember()));
        if (StringUtils.isNotEmpty(message.getContent())) {
            json.set(IMKey.content, message.getContent());
        }
        send(IMModelFactory.get().createRequestData(IMSendRoute.sendText, json), imSendCallBack);
    }

    /**
     * 发送自定义消息
     *
     * @param roomId
     * @param message
     * @param imSendCallBack
     */
    public void sendCustomMessage(String roomId, IMRoomMessage message, IMCallBack imSendCallBack) {
        Json json = new Json();
        json.set("room_id", roomId);
        json.set(IMKey.custom, message.getAttachment().toJson(true));
        if (message.getImRoomMember() != null) {
            json.set(IMKey.member, JsonParser.toJson(message.getImRoomMember()));
        }
        send(IMModelFactory.get().createRequestData(IMSendRoute.sendMessage, json), imSendCallBack);
    }

    /**
     * 发送自定义消息
     *
     * @param roomId
     * @param message
     * @param imSendCallBack
     */
    public void sendPulicMessage(String roomId, IMRoomMessage message, IMCallBack imSendCallBack) {
        Json json = new Json();
        json.set("room_id", roomId);
        json.set(IMKey.custom, message.getAttachment().toJson(false));
//        if (message.getImChatRoomMember() != null)
//            json.set(IMKey.member, message.getImChatRoomMember());
        send(IMModelFactory.get().createRequestData(IMSendRoute.sendPublicMsg, json), imSendCallBack);
    }

    /**
     * 进入公聊大厅
     *
     * @param roomId
     * @param imSendCallBack
     */
    public void enterChatHallMessage(String roomId, IMCallBack imSendCallBack) {
        Json json = new Json();
        json.set("room_id", roomId);
        send(IMModelFactory.get().createRequestData(IMSendRoute.enterPublicRoom, json), imSendCallBack);
    }

}