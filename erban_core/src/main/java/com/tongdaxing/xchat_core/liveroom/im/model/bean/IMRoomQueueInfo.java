package com.tongdaxing.xchat_core.liveroom.im.model.bean;


import com.tongdaxing.xchat_core.bean.RoomMicInfo;

/**
 * <p>  房间麦序单个坑位信息实体，包含麦序状态，成员信息</p>
 *
 * @author jiahui
 * @date 2017/12/13
 */
public class IMRoomQueueInfo {
    /**
     * 坑位信息（是否所坑，开麦等）
     */
    public RoomMicInfo mRoomMicInfo;
    /**
     * 坑上人员信息
     */
    public IMRoomMember mChatRoomMember;
    /**
     * 当前成员的性别，本属于ChatRoomMember的
     */
    public int gender;
    public long time;
    //pk用到
    public boolean isSelect = true;

    private int value;
    private String hatUrl;

    public IMRoomQueueInfo(RoomMicInfo roomMicInfo, IMRoomMember chatRoomMember) {
        mRoomMicInfo = roomMicInfo;
        mChatRoomMember = chatRoomMember;
    }

    public RoomMicInfo getmRoomMicInfo() {
        return mRoomMicInfo;
    }

    public void setmRoomMicInfo(RoomMicInfo mRoomMicInfo) {
        this.mRoomMicInfo = mRoomMicInfo;
    }

    public IMRoomMember getmChatRoomMember() {
        return mChatRoomMember;
    }

    public void setmChatRoomMember(IMRoomMember mChatRoomMember) {
        this.mChatRoomMember = mChatRoomMember;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getHatUrl() {
        return hatUrl;
    }

    public void setHatUrl(String hatUrl) {
        this.hatUrl = hatUrl;
    }
}
