package com.tongdaxing.xchat_core.liveroom.im.model.bean;

import java.io.Serializable;

/**
 * @author weihaitao
 * @date 2019/7/16
 */
public class AudioConnPayInfoResp implements Serializable {

    /**
     * 金币余额
     */
    public double goldNum;

    /**
     * 最高出价
     */
    public long highest;

    /**
     * 最低出价
     */
    public long lowest;

}
