package com.tongdaxing.xchat_core.liveroom.im.model;

import android.os.Handler;
import android.os.Message;

import com.tongdaxing.xchat_core.room.bean.RoomInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * 房主点赞倒计时计时器
 * <p>
 * 1.逻辑限定在仅更新倒计时的情况，至于倒计时为0时，点赞次数刷新逻辑扔出去给外部处理
 * 2.初始倒计时时间依据ReUsedSocketManager#enterWithOpenChatRoom()给定的值，直接更新计时信息，重新开始计时
 * 3.
 *
 * @author weihaitao
 * @date 2019/4/26
 */
public class RoomAdmireTimeCounter {

    private static RoomAdmireTimeCounter instance = null;
    private final String TAG = RoomAdmireTimeCounter.class.getSimpleName();
    /**
     * 默认计时循环开始时，最大倒计时时间为5分钟
     */
    private final long MAX_COUNT_TIME = 300L;

    /**
     * 是否开始房主直播时长计时
     */
    public boolean hasStarted = false;

    private List<OnAdmireTimeUpdateListener> listeners = new ArrayList<>();
    private Handler mHandler;

    /**
     * 倒计时时间，秒
     */
    private long timeCount = 0L;

    private RoomAdmireTimeCounter() {
    }

    public static RoomAdmireTimeCounter getInstance() {

        if (null == instance) {
            instance = new RoomAdmireTimeCounter();
        }

        return instance;
    }

    public long getTimeCount() {
        return timeCount;
    }

    /**
     * 开启点赞倒计时
     */
    public void startCount() {
        //设置初始计时时长，秒为单位
        if (null == RoomDataManager.get().getAdditional()) {
//            LogUtils.e(TAG, "startCount additional为空，不启动计时器");
            return;
        }

        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (null == roomInfo) {
//            LogUtils.d(TAG, "startCount roomInfo为空，停止计时");
            stopCount();
            return;
        }

        if (roomInfo.getType() == RoomInfo.ROOMTYPE_SINGLE_AUDIO && RoomDataManager.get().isRoomOwner()) {
//            LogUtils.d(TAG, "startCount 单人房，点赞只针对非房主，停止计时");
            stopCount();
            return;
        }

        if (roomInfo.getType() == RoomInfo.ROOMTYPE_SINGLE_AUDIO && RoomDataManager.get().getAdditional().getPlayState() == 0) {
//            LogUtils.e(TAG, "startCount 单人房，房主未开播，停止计时");
            stopCount();
            return;
        }

        timeCount = RoomDataManager.get().getAdditional().getAdmireTime();
//        LogUtils.d(TAG, "startCount timeCount:" + timeCount);

        if (hasStarted) {
//            LogUtils.e(TAG, "startCount-hasStarted is true，计时器已经启动");
            return;
        }

        if (timeCount < 0L) {
//            LogUtils.e(TAG, "startCount-timeCount 值非法，不启动计时器");
            stopCount();
            return;
        }

        if (timeCount == 0) {
            timeCount = MAX_COUNT_TIME;
        }

        if (null == mHandler) {
            mHandler = new Handler(new Handler.Callback() {
                @Override
                public boolean handleMessage(Message msg) {
                    if (msg.what == 0) {
                        timeCount -= 1;
//                        LogUtils.d(TAG, "startCount-->handleMessage timeCount:" + timeCount);
                        if (null != listeners) {
                            for (OnAdmireTimeUpdateListener listener : listeners) {
                                if (0 == timeCount) {
                                    listener.onAdmireTimeOver();
                                } else {
                                    listener.onAdmireTimeUpdated(timeCount);
                                }
                            }
                        }
                        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
                        if (null == roomInfo) {
//                            LogUtils.d(TAG, "startCount-->handleMessage roomInfo为空，停止计时");
                            stopCount();
                            return true;
                        }

                        if (null == RoomDataManager.get().getAdditional()) {
//                            LogUtils.e(TAG, "startCount-->handleMessage additional为空，停止计时");
                            stopCount();
                            return true;
                        }

                        if (roomInfo.getType() == RoomInfo.ROOMTYPE_SINGLE_AUDIO && RoomDataManager.get().isRoomOwner()) {
//                            LogUtils.d(TAG, "startCount-->handleMessage 单人房，点赞只针对非房主，停止计时");
                            stopCount();
                            return true;
                        }

                        if (roomInfo.getType() == RoomInfo.ROOMTYPE_SINGLE_AUDIO && RoomDataManager.get().getAdditional().getPlayState() == 0) {
//                            LogUtils.e(TAG, "startCount-hasStarted is true，单人房，房主未开播，停止计时");
                            stopCount();
                            return true;
                        }

                        //一轮倒计时完成后开始下一轮
                        if (timeCount == 0) {
                            timeCount = MAX_COUNT_TIME;
                        }
                        mHandler.sendEmptyMessageDelayed(0, 1000L);
                        return true;
                    }
                    return false;
                }
            });
        }
        hasStarted = true;
//        LogUtils.d(TAG, "startCount 开始计时,timeCount:" + timeCount + " hasStarted:" + hasStarted);
        mHandler.sendEmptyMessageDelayed(0, 1000L);
    }

    public void release() {
//        LogUtils.d(TAG, "release 结束计时");
        stopCount();
        timeCount = 0L;
        if (null != listeners) {
            listeners.clear();
        }
    }

    public void stopCount() {
        if (null != mHandler) {
            mHandler.removeMessages(0);
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }
        hasStarted = false;
    }

    /**
     * 添加房主直播时长更新监听器，注意需要保证每次第一次进入房间的时候，初始化timeCount
     *
     * @param listener
     */
    public void addListener(OnAdmireTimeUpdateListener listener) {
        if (null != listeners && !listeners.contains(listener)) {
            listeners.add(listener);
//            LogUtils.d(TAG, "addListener hasStarted:" + hasStarted);
        }
    }

    public void removeListener(OnAdmireTimeUpdateListener listener) {
        if (null != listeners && listeners.contains(listener)) {
//            LogUtils.d(TAG, "removeListener hasStarted:" + hasStarted);
            listeners.remove(listener);
        }
    }

    public interface OnAdmireTimeUpdateListener {

        /**
         * 直播时长更新回调
         *
         * @param millCounts 秒为单位
         */
        void onAdmireTimeUpdated(long millCounts);

        /**
         * 一次计时循环结束，通知外部更新点赞信息，及界面展示情况
         */
        void onAdmireTimeOver();
    }
}
