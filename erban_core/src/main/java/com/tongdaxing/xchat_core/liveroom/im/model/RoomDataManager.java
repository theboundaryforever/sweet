package com.tongdaxing.xchat_core.liveroom.im.model;

import android.graphics.Point;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.SparseArray;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCoreClient;
import com.tongdaxing.xchat_core.im.custom.bean.DetonateGiftAttachment;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMember;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMemberComeInfo;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMessage;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomQueueInfo;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMWaitQueuePersonInfo;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.SingleAudioRoomEnitity;
import com.tongdaxing.xchat_core.manager.RtcEngineManager;
import com.tongdaxing.xchat_core.room.auction.RoomAuctionBean;
import com.tongdaxing.xchat_core.room.bean.RoomAdditional;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * <p> 全局变量，存储房间相关信息(周期与房间一样) </p>
 * <p>
 * 注意这个Manager，在http层面的数据结构保持同旧有房间的一致，仅在新房间类型IM那一套逻辑上的数据结构采用新的
 *
 * @author weihaitao
 * @date 2019年4月20日
 */
public final class RoomDataManager {

    public static final String TAG = "room_log ---> IM";

    public static final int MIC_POSITION_BY_OWNER = -1;//房主的麦位
    public static final int AUCTION_ROOM_HOST_POSITION = 0;//竞拍房主持位
    public static final int AUCTION_ROOM_POSITION = 1;//竞拍房拍卖位
    public static final long MSG_SEND_MAX_LENGTH = 500;//发送文本消息的最大长度
    public final static int MIC_FULL = -2;
    private static final Object SYNC_OBJECT = new Object();

    private static volatile RoomDataManager mInstance;
    private volatile RoomInfo mCurrentRoomInfo;
    //房间配置 --- 后台控制的用户不能修改的配置
    private RoomAdditional additional;
    //竞拍房间 -- 竞拍信息
    private RoomAuctionBean auction;

    /**
     * 是否已经关注该房间
     * 注意，进房初始化更新、执行相关操作后的更新操作
     */
    private boolean hasAttentionRoom = false;

    @Deprecated
    private boolean hasLikeRoomOwner = false;
    private boolean hasPkVoted = false;

    public boolean hasLikeRoomOwner() {
        return hasLikeRoomOwner;
    }

    public void setHasLikeRoomOwner(boolean hasLikeRoomOwner) {
        this.hasLikeRoomOwner = hasLikeRoomOwner;
    }

    /**
     * 自己的实体
     */
    public IMRoomMember mSelfRoomMember;

    /**
     * 房主的实体
     */
    public IMRoomMember mOwnerRoomMember;
    /**
     * 房间管理员集合
     */
    public List<IMRoomMember> mRoomManagerList;

    /**
     * 麦序位置信息：对应的位置，坑位信息（用户成员，坑位状态）
     */
    public SparseArray<IMRoomQueueInfo> mMicQueueMemberMap;

    /**
     * 进房前公屏的消息记录
     */
    public List<IMRoomMessage> mRoomHistoryList;

    /**
     * 记录每一个麦位的中心点位置
     */
    public SparseArray<Point> mMicPointMap;
    public SparseArray<Json> mMicInListMap;
    /**
     * 是否需要打开麦克风,用户自己的行为，不受房主管理员的管理
     * <p>
     * 这里应该被理解为是否主动开麦 --- 受邀上麦时默认不开麦 其他是开启的
     */
    public boolean mIsNeedOpenMic = true;
    //是否已经申请上麦
    private boolean isApplyUpMicro;

    /**
     * 需要处理的 房间人员进入 消息
     */
    private ConcurrentLinkedQueue<IMRoomMemberComeInfo> mMemberComeMsgQueue;
    private String room_rule = "";
    //是否开始播放全服
    private boolean isStartPlayFull = false;
    private long timestamp = 0;//房间人员进出时间戳
    //当前等待队列上麦的信息
    private IMWaitQueuePersonInfo IMWaitQueuePersonInfo;

    //防止最小化以及页面不可见的时候这时候的进场消息和特效是不能进入队列的
    private boolean isMinimize = false;//是否最小化 true 最小化

    /**
     * 单人房/新交友多人房，点赞数为0的情况下，记录倒计时状态的显示与隐藏，以解决房间最小化回到房间，倒计时重新显示的问题
     * 1.初始进房，置为true
     * 2.切换倒计时状态时，相应更改记录值
     * 3.退出房间，置为true
     */
    public boolean imRoomAdmireTimeCounterShow = true;

    /**
     * 单人房，进房后聊天消息提示框，是否显示标识，用于手动关闭后，最小化房间再次进来时的及时隐藏
     */
    public boolean singleAudioRoomMsgTipsViewShow = true;

    /**
     * 单人房，语音连麦，主播设置的语音连麦开关状态
     */
    public boolean singleAudioAnchorConnSwitch = true;

    /**
     * 单人房，语音连麦，最新的语音连麦请求数量
     */
    public long singleAudioConnReqNum = 0L;

    /**
     * 多人房，召集令召集人数
     */
    public long conveneUserCount = 0L;

    /**
     * 多人房，是否切换召集令控件为显示已召集人数状态
     */
    public int conveneState = 0;

    /**
     * 多人房，主播可发送召集令的剩余次数
     */
    public int conveneCount = 0;

    public boolean hasRequestRecommRoomList = false;

    //魅力值对应时间戳，防止
    public long charmTimestamps;

    public List<SingleAudioRoomEnitity> singleAudioRoomEnitities = new ArrayList<>();

    synchronized public List<SingleAudioRoomEnitity> getSingleAudioRoomEnitities() {
        return singleAudioRoomEnitities;
    }

    synchronized public void setSingleAudioRoomEnitities(List<SingleAudioRoomEnitity> singleAudioRoomEnitityList) {
        this.singleAudioRoomEnitities = singleAudioRoomEnitityList;
    }

    private TreeSet<Json> treeSet = new TreeSet<Json>(new Comparator<Json>() {
        @Override
        public int compare(Json o1, Json o2) {
            String time1 = o1.str("time");
            String time2 = o2.str("time");
            long l1 = Long.parseLong(time1);
            long l2 = Long.parseLong(time2);
            if (l1 > l2) {
                return 1;
            } else {
                return -1;
            }


        }
    });

    private RoomDataManager() {
        mRoomManagerList = new ArrayList<>();
        mMicQueueMemberMap = new SparseArray<>();
        mMicInListMap = new SparseArray<>();
        mMemberComeMsgQueue = new ConcurrentLinkedQueue<>();
        isApplyUpMicro = false;
    }

    public static RoomDataManager get() {
        if (mInstance == null) {
            synchronized (SYNC_OBJECT) {
                if (mInstance == null) {
                    mInstance = new RoomDataManager();
                }
            }
        }
        return mInstance;
    }

    public RoomInfo getCurrentRoomInfo() {
        return mCurrentRoomInfo;
    }

    public void setCurrentRoomInfo(RoomInfo mCurrentRoomInfo) {
        this.mCurrentRoomInfo = mCurrentRoomInfo;
    }

    public boolean hasAttentionRoom() {
        return hasAttentionRoom;
    }

    public void setHasAttentionRoom(boolean hasAttentionRoom) {
        this.hasAttentionRoom = hasAttentionRoom;
    }

    /**
     * 获取麦上队列信息
     *
     * @param micPosition 麦的位置
     * @return 对应队列信息
     */
    public IMRoomQueueInfo getRoomQueueMemberInfoByMicPosition(int micPosition) {
        if (micPosition >= mMicQueueMemberMap.size()) {
            return null;
        }
        return mMicQueueMemberMap.get(micPosition);
    }


    /**
     * 对应麦位是否有用户
     *
     * @param micPosition 麦的位置
     * @return 对应队列信息
     */
    public boolean hasMemberInMicPosition(int micPosition) {
        if (micPosition >= mMicQueueMemberMap.size()) {
            return false;
        }else {
            IMRoomQueueInfo imRoomQueueInfo = mMicQueueMemberMap.get(micPosition);
            if (imRoomQueueInfo == null || imRoomQueueInfo.getmChatRoomMember() == null){
                return false;
            }else {
                return true;
            }
        }
    }

    /**
     * 获取麦上的第一个异性（非房主）
     */
    @Deprecated
    public IMRoomMember getFirstOppositeSexOnMic() {
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo != null && mMicQueueMemberMap != null && mMicQueueMemberMap.size() > 1) {
            //这里要注意，过滤掉房主
            int oppositeSexGender = userInfo.getGender() == 1 ? 2 : 1;//异性性别
            for (int i = 1; i < mMicQueueMemberMap.size(); i++) {
                //这里要注意，过滤掉房主
                IMRoomQueueInfo IMRoomQueueInfo = mMicQueueMemberMap.valueAt(i);
                if (IMRoomQueueInfo.mChatRoomMember != null &&
                        Objects.equals(IMRoomQueueInfo.mChatRoomMember.getGender(), oppositeSexGender)) {
                    return IMRoomQueueInfo.mChatRoomMember;
                }
            }
        }
        return null;
    }

    /**
     * 获取自己在麦上队列信息
     *
     * @return 对应队列信息
     */
    public IMRoomQueueInfo getRoomQueueMemberInfoMyself() {
        return getRoomQueueMemberInfoByAccount(String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
    }

    /**
     * 获取麦上队列信息
     *
     * @param account 用户的id
     * @return 对应队列信息
     */
    public IMRoomQueueInfo getRoomQueueMemberInfoByAccount(String account) {
        if (TextUtils.isEmpty(account)) {
            return null;
        }
        //同步解决随便传递进来一个account值，都有返回值的RoomQueueInfo的业务bug
        IMRoomQueueInfo imRoomQueueInfo = null;
        for (int i = 0; i < mMicQueueMemberMap.size(); i++) {
            IMRoomQueueInfo imRoomQueueInfo1 = mMicQueueMemberMap.valueAt(i);
            if (imRoomQueueInfo1.mChatRoomMember != null &&
                    Objects.equals(imRoomQueueInfo1.mChatRoomMember.getAccount(), account)) {
                imRoomQueueInfo = imRoomQueueInfo1;
                break;
            }
        }
        return imRoomQueueInfo;
    }


    /**
     * 判断自己是否在拍卖位
     * @return
     */
    public boolean onAuctionMicMyself(){
        IMRoomQueueInfo imRoomQueueInfo = getRoomQueueMemberInfoByMicPosition(AUCTION_ROOM_POSITION);
        if (imRoomQueueInfo == null || imRoomQueueInfo.mChatRoomMember == null){
            return false;
        }else {
            return imRoomQueueInfo.mChatRoomMember.getLongAccount() == CoreManager.getCore(IAuthCore.class).getCurrentUid();
        }
    }

    public void addAdminMember(IMRoomMember chatRoomMember) {
        if (chatRoomMember == null || containsAdminMember(chatRoomMember.getAccount())) {
            return;
        }
        mRoomManagerList.add(chatRoomMember);
    }

    public boolean containsAdminMember(String uid) {
        for (IMRoomMember chatRoomMember : mRoomManagerList) {
            if (Objects.equals(chatRoomMember.getAccount(), String.valueOf(uid))) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断该用户是第一次进来还是切换房间
     *
     * @param roomUid 要进入房间的用户id
     */
    public boolean isFirstEnterRoomOrChangeOtherRoom(long roomUid, int roomType) {
        return mCurrentRoomInfo == null || mCurrentRoomInfo.getUid() != roomUid || mCurrentRoomInfo.getType() != roomType;
    }

    /***
     * 是否是房间创建者
     * @param currentUid
     * @return
     */
    public boolean isRoomOwner(String currentUid) {
        return mCurrentRoomInfo != null && Objects.equals(String.valueOf(mCurrentRoomInfo.getUid()), currentUid);
    }

    /***
     * 是否是房间创建者
     * @param currentUid
     * @return
     */
    public boolean isRoomOwner(long currentUid) {
        return mCurrentRoomInfo != null && mCurrentRoomInfo.getUid() == currentUid;
    }

    /**
     * 是否是自己
     */
    public boolean isUserSelf(String currentUid) {
        return Objects.equals(currentUid, String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
    }

    /**
     * 是否是自己
     */
    public boolean isUserSelf(long currentUid) {
        return currentUid == CoreManager.getCore(IAuthCore.class).getCurrentUid();
    }

    /***
     * 是否是房间创建者
     */
    public boolean isRoomOwner() {
        return mCurrentRoomInfo != null && isUserSelf(mCurrentRoomInfo.getUid());
    }

    /**
     * @return 是否做了移除
     */
    public boolean removeManagerMember(String account) {
        boolean isRemoved = false;
        if (ListUtils.isListEmpty(mRoomManagerList) || TextUtils.isEmpty(account)) {
            return isRemoved;
        }
        ListIterator<IMRoomMember> iterator = mRoomManagerList.listIterator();
        for (; iterator.hasNext(); ) {
            IMRoomMember chatRoomMember = iterator.next();
            if (Objects.equals(chatRoomMember.getAccount(), account)) {
                iterator.remove();
                isRemoved = true;
                break;
            }
        }
        if (RoomDataManager.get().isUserSelf(account) && mSelfRoomMember != null) {
            //自己是管理员被移除，恢复身份
            mSelfRoomMember.setIsManager(false);
        }
        return isRemoved;
    }

    public boolean isGuess() {
        return !isRoomAdmin() && !isRoomOwner();
    }

    public boolean isGuess(String account) {
        return !isRoomAdmin(account) && !isRoomOwner(account);
    }

    /**
     * 是否是房间管理员
     *
     * @return -
     */
    public boolean isRoomAdmin() {
        return isRoomAdmin(String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
    }

    public boolean isRoomAdmin(String account) {
        if (ListUtils.isListEmpty(mRoomManagerList)) {
            return false;
        }
        for (IMRoomMember chatRoomMember : mRoomManagerList) {
            if (Objects.equals(chatRoomMember.getAccount(), account)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断指定用户id是否在麦上
     *
     * @param myUid 用户id
     * @return -
     */
    public boolean isOnMic(long myUid) {
        return isOnMic(String.valueOf(myUid));
    }

    /**
     * 判断指定用户id是否在麦上
     *
     * @param myUid 用户id
     * @return -
     */
    public boolean isOnMic(String myUid) {
        int size = mMicQueueMemberMap.size();
        for (int i = 0; i < size; i++) {
            IMRoomQueueInfo IMRoomQueueInfo = mMicQueueMemberMap.valueAt(i);
            if (IMRoomQueueInfo != null && IMRoomQueueInfo.mChatRoomMember != null
                    && Objects.equals(IMRoomQueueInfo.mChatRoomMember.getAccount(), myUid)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断自己是否在麦上
     *
     * @return -
     */
    public boolean isOwnerOnMic() {
        return isOnMic(CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }

    /**
     * 根据用户id去获取当前用户在麦上的位置
     */
    public int getMicPosition(long uid) {
        int size = mMicQueueMemberMap.size();
        for (int i = 0; i < size; i++) {
            IMRoomQueueInfo IMRoomQueueInfo = mMicQueueMemberMap.valueAt(i);
            if (IMRoomQueueInfo != null && IMRoomQueueInfo.mChatRoomMember != null
                    && Objects.equals(IMRoomQueueInfo.mChatRoomMember.getAccount(), String.valueOf(uid))) {
                return mMicQueueMemberMap.keyAt(i);
            }
        }
        //判断是否房主
        if (isRoomOwner(uid)) {
            return -1;
        }
        return Integer.MIN_VALUE;
    }

    /**
     * 即构是已streamID表示用户唯一信息
     *
     * @param streamID 流的格式  s-初始化时传入的用户uid-时间戳
     */
    public int getMicPositionByStreamID(String streamID) {
        if (StringUtils.isEmpty(streamID)) {
            return Integer.MIN_VALUE;
        }
        for (int index = 0; index < mMicQueueMemberMap.size(); index++) {
            IMRoomQueueInfo IMRoomQueueInfo = mMicQueueMemberMap.valueAt(index);
            if (IMRoomQueueInfo != null && IMRoomQueueInfo.mChatRoomMember != null
                    && streamID.contains(IMRoomQueueInfo.mChatRoomMember.getAccount())) {
                return mMicQueueMemberMap.keyAt(index);
            }
        }
        //判断是否房主
        if (isRoomOwner(streamID)) {
            return -1;
        }
        return Integer.MIN_VALUE;
    }

    /**
     * 根据用户id去获取当前用户在麦上的位置
     *
     * @param currentUid -
     * @return -
     */
    public int getMicPosition(String currentUid) {
        return getMicPosition(Long.valueOf(currentUid));
    }

    /**
     * 获取房主的用户信息
     */
    public UserInfo getRoomOwnerUserInfo() {
        if (mCurrentRoomInfo != null) {
            return CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(mCurrentRoomInfo.getUid(), true);
        } else {
            return null;
        }
    }

    /**
     * 获取坑上没人的位置
     */
    public int findFreePosition() {
        int size;
        if (mMicQueueMemberMap != null && (size = mMicQueueMemberMap.size()) > 0) {
            for (int i = 0; i < size; i++) {
                int key = mMicQueueMemberMap.keyAt(i);
                IMRoomQueueInfo IMRoomQueueInfo = mMicQueueMemberMap.valueAt(i);
                if (IMRoomQueueInfo.mChatRoomMember == null) {
                    if (key == -1) {
                        continue;
                    }
                    return key;
                }
            }
        }
        return Integer.MIN_VALUE;
    }

    public void addMemberComeInfo(IMRoomMemberComeInfo memberComeInfo) {
        if (!isMinimize) {
            mMemberComeMsgQueue.offer(memberComeInfo);
        }
    }

    public IMRoomMemberComeInfo getAndRemoveFirstMemberComeInfo() {
        return mMemberComeMsgQueue.poll();
    }

    public int getMemberComeSize() {
        if (mMemberComeMsgQueue == null) {
            return 0;
        }
        return mMemberComeMsgQueue.size();
    }

    public void removeMicListInfo(String key) {
        if (TextUtils.isEmpty(key)) {
            return;
        }
        Integer keyInt = Integer.valueOf(key);

        removeMicListInfo(keyInt);
    }

    public void removeMicListInfo(int key) {

        mMicInListMap.remove(key);
        CoreManager.notifyClients(IAVRoomCoreClient.class, IAVRoomCoreClient.onMicInListChange);
    }

    public boolean checkInMicInlist() {
        UserInfo cacheLoginUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (cacheLoginUserInfo == null)
            return false;
        long uid = cacheLoginUserInfo.getUid();

        return mMicInListMap.get((int) uid) != null;

    }

    public Json getMicInListTopInfo() {

        if (mMicInListMap.size() < 1)
            return null;

        treeSet.clear();
        for (int i = 0; i < mMicInListMap.size(); i++) {
            treeSet.add(mMicInListMap.valueAt(i));
            LogUtils.d("micInListLog", "key:" + mMicInListMap.keyAt(i) + "   value:" + mMicInListMap.valueAt(i));
        }

        if (treeSet.size() > 0) {
            return treeSet.first();
        }
        return null;

    }

    /**
     * 找到空的麦位
     *
     * @return 返回-2的话代表满了
     */
    public int findEmptyMic() {
        if (mMicQueueMemberMap == null) {
            return MIC_FULL;
        }

        for (int i = 0; i < mMicQueueMemberMap.size(); i++) {
            IMRoomQueueInfo IMRoomQueueInfo = mMicQueueMemberMap.valueAt(i);
            int i1 = mMicQueueMemberMap.keyAt(i);
            if (IMRoomQueueInfo.mChatRoomMember == null && i1 != -1 && !IMRoomQueueInfo.mRoomMicInfo.isMicLock()) {
                return i1;
            }

        }
        return MIC_FULL;
    }


    public void release() {
        LogUtils.d(TAG, "release");
        IMRoomMessageManager.get().setImRoomConnection(false);
        IMRoomMessageManager.get().setBeforeDisConnectionMuteStatus(0);
        RtcEngineManager.get().leaveChannel();
        clear();
    }

    private void clear() {
        clearMembers();
        isApplyUpMicro = false;
        mCurrentRoomInfo = null;
        additional = null;
        auction = null;
        isMinimize = false;
        timestamp = 0;
        mMicInListMap.clear();
        mMicQueueMemberMap.clear();
        IMRoomMessageManager.get().clear();
        if (null != mRoomHistoryList) {
            mRoomHistoryList.clear();
            mRoomHistoryList = null;
        }
        hasPkVoted = false;
    }

    /**
     * 清除房间成员信息
     */
    private void clearMembers() {
        mSelfRoomMember = null;
        mOwnerRoomMember = null;
        mRoomManagerList.clear();
        mMemberComeMsgQueue.clear();
    }

    public @NonNull
    IMWaitQueuePersonInfo getIMWaitQueuePersonInfo() {
        if (IMWaitQueuePersonInfo == null) {
            IMWaitQueuePersonInfo = new IMWaitQueuePersonInfo(0, 0, 0, 0);
        }
        return IMWaitQueuePersonInfo;
    }

    public void setIMWaitQueuePersonInfo(IMWaitQueuePersonInfo IMWaitQueuePersonInfo) {
        this.IMWaitQueuePersonInfo = IMWaitQueuePersonInfo;
    }

    /**
     * 当前麦上是否有人（排除房主）
     */
    public boolean isHaveMemberOnMicro() {
        if (mMicQueueMemberMap != null && mMicQueueMemberMap.size() > 1) {//注意这里排除房主
            for (int i = 1; i < mMicQueueMemberMap.size(); i++) {//注意这里排除房主
                IMRoomQueueInfo queueInfo = mMicQueueMemberMap.valueAt(i);
                if (queueInfo != null && queueInfo.mChatRoomMember != null) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 获取房主默认信息
     */
    public IMRoomMember getRoomOwnerDefaultMemberInfo() {
        IMRoomMember member = null;
        if (RoomDataManager.get().mCurrentRoomInfo != null) {
            if (mOwnerRoomMember != null){
                return mOwnerRoomMember;
            }else {
                member = new IMRoomMember();
                member.setAccount(String.valueOf(RoomDataManager.get().mCurrentRoomInfo.getUid()));
                member.setNick("");
                member.setAvatar("");
            }
        }
        return member;
    }



    public String getRoomRule() {
        return room_rule;
    }

    public void setRoomRule(String room_rule) {
        this.room_rule = room_rule;
    }

    public boolean isMinimize() {
        return isMinimize;
    }

    public void setMinimize(boolean minimize) {
        isMinimize = minimize;
    }

    public boolean isStartPlayFull() {
        return isStartPlayFull;
    }

    public void setStartPlayFull(boolean startPlayFull) {
        isStartPlayFull = startPlayFull;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isApplyUpMicro() {
        return isApplyUpMicro;
    }

    public void setApplyUpMicro(boolean applyUpMicro) {
        isApplyUpMicro = applyUpMicro;
    }

    public RoomAdditional getAdditional() {
        return additional;
    }

    public void setAdditional(RoomAdditional additional) {
        this.additional = additional;
    }

    public void updateAdditionalDetonating(DetonateGiftAttachment detonateGiftAttachment){
        if (additional == null) {
            additional = new RoomAdditional();
        }
        additional.setDetonatingAvatar(detonateGiftAttachment.getDetonatingAvatar());
        additional.setDetonatingNick(detonateGiftAttachment.getDetonatingNick());
        additional.setDetonatingDuration(detonateGiftAttachment.getDetonatingDuration());
        additional.setDetonatingGiftName(detonateGiftAttachment.getDetonatingGiftName());
        additional.setDetonatingGiftUrl(detonateGiftAttachment.getDetonatingGiftUrl());
        additional.setDetonatingGifts(detonateGiftAttachment.getDetonatingGifts());
        additional.setDetonatingState(detonateGiftAttachment.getDetonatingState());
        additional.setDetonatingUid(detonateGiftAttachment.getDetonatingUid());
        additional.setDetonatingTime(detonateGiftAttachment.getDetonatingTime());
    }
    /**
     * 设置当前投票状态
     *
     * @param hasPkVoted
     */
    public void setHasPkVoted(boolean hasPkVoted) {
        this.hasPkVoted = hasPkVoted;
    }


    public boolean hasPkVoted() {
        return hasPkVoted;
    }

    public long getCharmTimestamps() {
        return charmTimestamps;
    }

    public void setCharmTimestamps(long charmTimestamps) {
        this.charmTimestamps = charmTimestamps;
    }

    /**
     * 单人房检测语音连麦4坑位是否有人占坑
     */
    public boolean checkAudioConnMicQueueExist() {
        //判断0-3坑位是否有member信息，有则表示有人，无则表示没人，有人则显示，没人则隐藏
        for (int position = 0; position < 4; position++) {
            IMRoomQueueInfo roomQueueInfo = RoomDataManager.get().getRoomQueueMemberInfoByMicPosition(position);
            if (null != roomQueueInfo && roomQueueInfo.mChatRoomMember != null) {
                return true;
            }
        }
        return false;
    }

    /**
     * 是否是当前拍卖人
     * @param account
     * @return
     */
    public boolean isCurrentAution(String account){
        if (auction == null || auction.getUid() <= 0){
            return false;
        }else {
            return String.valueOf(auction.getUid()).equals(account);
        }
    }

    /**
     * 自己是否是当前拍卖人
     * @return
     */
    public boolean myselfIsCurrentAution(){
        if (auction == null || auction.getUid() <= 0){
            return false;
        }else {
            return auction.getUid() == CoreManager.getCore(IAuthCore.class).getCurrentUid();
        }
    }

    public long getCurrentAuctionUid(){
        return auction == null?-1:auction.getUid();
    }

    public RoomAuctionBean getAuction() {
        return auction;
    }

    public void setAuction(RoomAuctionBean auction) {
        this.auction = auction;
    }

    public IMRoomMember getmOwnerRoomMember() {
        return mOwnerRoomMember;
    }

    public void setmOwnerRoomMember(IMRoomMember mOwnerRoomMember) {
        this.mOwnerRoomMember = mOwnerRoomMember;
    }
}
