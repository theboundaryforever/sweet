package com.tongdaxing.xchat_core.liveroom.im.model;

import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;

/**
 * 基础的房间model
 *
 * @author zeda
 */
public class BaseIMRoomModel extends BaseMvpModel {

    public RoomInfo getServiceRoomInfo() {
        return RoomDataManager.get().getCurrentRoomInfo();
    }

}
