package com.tongdaxing.xchat_core.liveroom.im.model;

import android.os.Message;
import android.view.View;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.AudioConnReqInfo;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

/**
 * 用于记录，语音连麦记录中，处于连麦的成员，已经连麦的时长
 * <p>
 * 1.以micTime+uid的方式，来唯一标识每一条连麦记录
 * 2.每次adapter notify的时候，判断处于连麦中，则start，判断处于已结束，则stop
 * 3.start的时候，判断是否已经有map映射，没有就添加并启动计时器，有就更新控件，控件设置tag为唯一标识
 * 4.stop的时候，判断是否已经有map映射，有就移除，并停止计时器
 * 5.AudioConnectListDialog每次隐藏的时候，停止并移除所有计时任务
 * 6.更新操作保持同步sync
 * 7.DaemonService#onTaskRemoved(android.content.Intent)方法执行5操作
 *
 * @author weihaitao
 * @date 2019/7/18
 */
public class AudioConnTimeCounter {

    private static AudioConnTimeCounter instance = null;
    private final String TAG = AudioConnTimeCounter.class.getSimpleName();
    /**
     * 以micTime+uid为key，控件为value
     */
    private Map<String, WeakReference<TextView>> mCountRecordMap = new HashMap<>();
    /**
     * 以micTime+uid为key，连麦时长(秒)为value
     */
    private Map<String, Long> mCountTimeMap = new HashMap<>();
    private android.os.Handler mHandler = null;
    private SimpleDateFormat sdf;
    /**
     * 1.release
     * 2.clear/empty
     */
    private boolean hasStartTimeCounter = false;

    private AudioConnTimeCounter() {

    }

    public static AudioConnTimeCounter getInstance() {

        if (null == instance) {
            instance = new AudioConnTimeCounter();
        }

        return instance;
    }

    public String getOperaKey(long micTime, long uid) {
        return String.valueOf(micTime).concat(String.valueOf(uid));
    }

    public void start(AudioConnReqInfo audioConnReqInfo, TextView tvConnTime) {
        String operaKey = getOperaKey(audioConnReqInfo.getMicTime(), audioConnReqInfo.getUid());
        tvConnTime.setTag(operaKey);
        if (null == mCountRecordMap) {
            mCountRecordMap = new HashMap<>();
        }
        if (null == mCountTimeMap) {
            mCountTimeMap = new HashMap<>();
        }
        synchronized (mCountRecordMap) {
            mCountRecordMap.put(operaKey, new WeakReference<>(tvConnTime));

            long micTimeValue = parseMicTime(audioConnReqInfo.getMicTime());
            tvConnTime.setText(parseTimeValue(micTimeValue));
            if (!mCountTimeMap.containsKey(operaKey)) {
                mCountTimeMap.put(operaKey, micTimeValue);
            }
            LogUtils.d(TAG, "start-operaKey:" + operaKey + " micTimeValue:" + micTimeValue + " hasStartTimeCounter:" + hasStartTimeCounter);
            if (hasStartTimeCounter) {
                return;
            }

            if (null == mHandler) {
                mHandler = new android.os.Handler(new android.os.Handler.Callback() {
                    @Override
                    public boolean handleMessage(Message msg) {
                        LogUtils.d(TAG, "start-->handleMessage ------------------------------->");
                        if (null == mCountRecordMap) {
                            mCountRecordMap = new HashMap<>();
                        }
                        if (null == mCountTimeMap) {
                            mCountTimeMap = new HashMap<>();
                        }

                        if (mCountRecordMap.size() == 0 || mCountTimeMap.size() == 0) {
                            if (null != mHandler) {
                                //直接停止计时刷新
                                mHandler.removeMessages(0);
                                mHandler.removeCallbacksAndMessages(null);
                            }

                            hasStartTimeCounter = false;
                            return true;
                        }

                        synchronized (mCountTimeMap) {
                            //先更新缓存的值
                            Iterator<String> iterator = mCountTimeMap.keySet().iterator();
                            Map<String, Long> tempMap = new HashMap<>();
                            while (iterator.hasNext()) {
                                String key = iterator.next();
                                long value = mCountTimeMap.get(key);
                                value += 1;
                                tempMap.put(key, value);
                            }

                            //后刷新界面展示
                            Iterator<String> tempIter = tempMap.keySet().iterator();
                            while (tempIter.hasNext()) {
                                String key = tempIter.next();
                                long value = tempMap.get(key);
                                if (!mCountTimeMap.containsKey(key)) {
                                    continue;
                                }
                                mCountTimeMap.put(key, value);

                                if (!mCountRecordMap.containsKey(key)) {
                                    continue;
                                }
                                WeakReference<TextView> weakReference = mCountRecordMap.get(key);
                                //严格判断，防止错位
                                if (null != weakReference && null != weakReference.get() && weakReference.get().getVisibility() == View.VISIBLE
                                        && null != weakReference.get().getTag() && weakReference.get().getTag().toString().equals(key)) {
                                    weakReference.get().setText(parseTimeValue(value));
                                    LogUtils.d(TAG, "start-->handleMessage key:" + key + " value:" + value);
                                }
                            }
                        }
                        if (null != mHandler) {
                            mHandler.sendEmptyMessageDelayed(0, 1000L);
                        }
                        return true;
                    }
                });
            }

            mHandler.sendEmptyMessageDelayed(0, 1000L);
            hasStartTimeCounter = true;
        }
    }

    public void stop(AudioConnReqInfo audioConnReqInfo, TextView tvConnTime) {
        String operaKey = getOperaKey(audioConnReqInfo.getMicTime(), audioConnReqInfo.getUid());
        if (null == mCountRecordMap) {
            mCountRecordMap = new HashMap<>();
        }
        if (null == mCountTimeMap) {
            mCountTimeMap = new HashMap<>();
        }
        synchronized (mCountRecordMap) {
            mCountRecordMap.remove(operaKey);
            mCountTimeMap.remove(operaKey);
        }

        if (mCountRecordMap.size() == 0 || mCountTimeMap.size() == 0) {
            if (null != mHandler) {
                //直接停止计时刷新
                mHandler.removeMessages(0);
                mHandler.removeCallbacksAndMessages(null);
            }

            hasStartTimeCounter = false;
        }
        LogUtils.d(TAG, "stop-operaKey:" + operaKey + " hasStartTimeCounter:" + hasStartTimeCounter);
    }

    public void release() {
        if (null != mCountTimeMap) {
            synchronized (mCountTimeMap) {
                mCountTimeMap.clear();
            }
        }
        if (null != mCountRecordMap) {
            mCountRecordMap.clear();
        }
        if (null != mHandler) {
            //直接停止计时刷新
            mHandler.removeMessages(0);
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }
        mCountRecordMap = null;
        mCountTimeMap = null;
        hasStartTimeCounter = false;
        LogUtils.d(TAG, "release hasStartTimeCounter:" + hasStartTimeCounter);
    }

    private long parseMicTime(long micTime) {
        return (System.currentTimeMillis() - micTime) / 1000;
    }

    /**
     * 将秒数转换成对应格式的时间字符串
     *
     * @param mseconds
     * @return
     */
    private String parseTimeValue(long mseconds) {
        if (null == sdf) {
            sdf = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
            //TimeZone时区，加上这句话，解决解析出的时间比真实时长多出8小时的问题
            sdf.setTimeZone(TimeZone.getTimeZone("GMT+0"));
        }
        Date date = new Date(mseconds * 1000);
        String str = sdf.format(date);
        LogUtils.d(TAG, "parseTimeValue mseconds:" + mseconds + "=" + str);
        return str;
    }

}
