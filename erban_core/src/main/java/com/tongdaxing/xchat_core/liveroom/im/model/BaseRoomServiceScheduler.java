package com.tongdaxing.xchat_core.liveroom.im.model;

import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.model.AvRoomModel;

/**
 * @author weihaitao
 * @date 2019/4/30
 */
public abstract class BaseRoomServiceScheduler {

    /**
     * 通用的退出房间逻辑
     * 这里两种房间类型都要做判断并执行对应的退出流程
     *
     * @param callBack
     */
    public static void exitRoom(CallBack<String> callBack) {
        if (null != AvRoomDataManager.get().mCurrentRoomInfo) {
            new AvRoomModel().exitRoom(callBack);
        }
        if (null != RoomDataManager.get().getCurrentRoomInfo()) {
            RoomDataManager.get().imRoomAdmireTimeCounterShow = true;
            RoomDataManager.get().singleAudioRoomMsgTipsViewShow = true;
            RoomDataManager.get().singleAudioConnReqNum = 0;
            RoomDataManager.get().hasRequestRecommRoomList = false;
            RoomOwnerLiveTimeCounter.getInstance().release();
            RoomAdmireTimeCounter.getInstance().release();
            AudioConnTimeOutTipsQueueScheduler.getInstance().clear();
            new IMRoomModel().exitRoom(callBack);
        }
    }

    /**
     * 检测当前用户是否处于房间之中
     * 用于需要用到录音的场景，如果处于房间之中，需要提示用户退出房间，用户点击确定按钮，要两种类型房间都检查并做退出处理
     *
     * @return
     */
    public static boolean checkUserInRoom() {
        return null != AvRoomDataManager.get().mCurrentRoomInfo || null != RoomDataManager.get().getCurrentRoomInfo();
    }

    public static RoomInfo getCurrentRoomInfo() {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (null == roomInfo) {
            roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        }
        return roomInfo;
    }

    public static RoomInfo getServerRoomInfo() {
        RoomInfo roomInfo = AvRoomDataManager.get().mServiceRoominfo;
        if (null == roomInfo) {
            roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        }
        return roomInfo;
    }

    public static void setServerRoomInfo(RoomInfo roomInfo) {
        RoomInfo tempInfo = AvRoomDataManager.get().mServiceRoominfo;
        if (null != tempInfo && null != roomInfo && roomInfo.getType() == tempInfo.getType()) {
            AvRoomDataManager.get().mServiceRoominfo = roomInfo;
        }
        tempInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (null != tempInfo && null != roomInfo && roomInfo.getType() == tempInfo.getType()) {
            RoomDataManager.get().setCurrentRoomInfo(roomInfo);
        }
    }
}
