package com.tongdaxing.xchat_core.liveroom.im.model.bean;

import android.support.annotation.NonNull;

import com.tongdaxing.erban.libcommon.utils.ListUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

/**
 * <p> 在线用户列表，包装 {@link IMRoomMember} </p>
 *
 * @author jiahui
 * @date 2017/12/20
 */
public class IMRoomOnlineMember implements Comparable<IMRoomOnlineMember> {
    public IMRoomMember imRoomMember;

    public boolean isOnMic;
    public boolean isAdmin;
    public boolean isRoomOwer;

    public IMRoomOnlineMember() {
    }

    public IMRoomOnlineMember(IMRoomMember imRoomMember) {
        this.imRoomMember = imRoomMember;
    }

    public IMRoomOnlineMember(IMRoomMember imRoomMember, boolean isOnMic, boolean isAdmin, boolean isRoomOwer) {
        this.imRoomMember = imRoomMember;
        this.isOnMic = isOnMic;
        this.isAdmin = isAdmin;
        this.isRoomOwer = isRoomOwer;
    }

    public static List<IMRoomOnlineMember> coverToOnlineChatMember(List<IMRoomMember> oldList, List<IMRoomMember> newList) {
        HashSet<IMRoomOnlineMember> treeSet = new HashSet<>();
        if (!ListUtils.isListEmpty(oldList)) {
            List<IMRoomOnlineMember> list = new ArrayList<>(oldList.size());
            for (IMRoomMember chatRoomMember : oldList) {
                list.add(new IMRoomOnlineMember(chatRoomMember));
            }
            treeSet.addAll(list);
        }
        if (!ListUtils.isListEmpty(newList)) {
            List<IMRoomOnlineMember> list = new ArrayList<>(newList.size());
            for (IMRoomMember chatRoomMember : newList) {
                list.add(new IMRoomOnlineMember(chatRoomMember));
            }
            treeSet.addAll(list);
        }
        ArrayList<IMRoomOnlineMember> list = new ArrayList<>(treeSet);
        Collections.sort(list);
        return list;
    }

    public static List<IMRoomMember> converOnlineToNormal(List<IMRoomOnlineMember> onlineChatMembers) {
        if (ListUtils.isListEmpty(onlineChatMembers)) {
            return null;
        }
        List<IMRoomMember> chatRoomMembers = new ArrayList<>();
        for (IMRoomOnlineMember temp : onlineChatMembers) {
            if (temp.imRoomMember != null) {
                chatRoomMembers.add(temp.imRoomMember);
            }
        }
        return chatRoomMembers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof IMRoomOnlineMember)) {
            return false;
        }

        IMRoomOnlineMember that = (IMRoomOnlineMember) o;
        return !(imRoomMember == null || that.imRoomMember == null)
                && Objects.equals(imRoomMember.getAccount(), that.imRoomMember.getAccount());
    }

    @Override
    public int hashCode() {
        return imRoomMember.getAccount().hashCode();
    }

    @Override
    public int compareTo(@NonNull IMRoomOnlineMember o) {
        if (imRoomMember == null) {
            return 1;
        }
        if (o.imRoomMember == null) {
            return -1;
        }
        return (int) (o.imRoomMember.getEnterTime() - imRoomMember.getEnterTime());
    }
}
