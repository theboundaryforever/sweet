package com.tongdaxing.xchat_core.liveroom.im;

import io.reactivex.disposables.Disposable;

public interface IDisposableAddListener {
    void addDisposable(Disposable disposable);
}
