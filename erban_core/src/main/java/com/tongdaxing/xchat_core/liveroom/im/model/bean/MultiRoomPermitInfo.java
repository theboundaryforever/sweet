package com.tongdaxing.xchat_core.liveroom.im.model.bean;

import java.io.Serializable;

/**
 * @author weihaitao
 * @date 2019/7/16
 */
public class MultiRoomPermitInfo implements Serializable {
    /**
     * url : String
     * name : String
     */
    private String permitUrl;
    private String permitName;

    public String getPermitUrl() {
        return permitUrl;
    }

    public void setPermitUrl(String permitUrl) {
        this.permitUrl = permitUrl;
    }

    public String getPermitName() {
        return permitName;
    }

    public void setPermitName(String permitName) {
        this.permitName = permitName;
    }

}
