package com.tongdaxing.xchat_core.liveroom.im.model.bean;

import com.tongdaxing.xchat_core.user.bean.GiftWallInfo;

import java.io.Serializable;
import java.util.List;

/**
 * @author weihaitao
 * @date 2019/7/16
 */
public class MultiRoomInfo implements Serializable {


    /**
     * activity : 0
     * admire : 0
     * avatar : string
     * erbanNo : 0
     * fans : 0
     * giftList : {}
     * nick : string
     * online : 0
     * onlineTime : 0
     * permitList : [0]
     * roomId : 0
     * roomLevel : 0
     * roomTitle : string
     * roomType : 0
     * uid : 0
     */

    private long activity;
    private long admire;
    private String avatar;
    private long erbanNo;
    private long fans;
    private List<GiftWallInfo> giftList;
    private String nick;
    private long online;
    private long onlineTime;
    private long roomId;
    private int roomLevel;
    private String roomLevelIcon;
    private String roomTitle;
    private int roomType;
    private long uid;
    private List<MultiRoomPermitInfo> permitList;

    public String getRoomLevelIcon() {
        return roomLevelIcon;
    }

    public void setRoomLevelIcon(String roomLevelIcon) {
        this.roomLevelIcon = roomLevelIcon;
    }

    public long getActivity() {
        return activity;
    }

    public void setActivity(long activity) {
        this.activity = activity;
    }

    public long getAdmire() {
        return admire;
    }

    public void setAdmire(long admire) {
        this.admire = admire;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public long getErbanNo() {
        return erbanNo;
    }

    public void setErbanNo(long erbanNo) {
        this.erbanNo = erbanNo;
    }

    public long getFans() {
        return fans;
    }

    public void setFans(long fans) {
        this.fans = fans;
    }

    public List<GiftWallInfo> getGiftList() {
        return giftList;
    }

    public void setGiftList(List<GiftWallInfo> giftList) {
        this.giftList = giftList;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public long getOnline() {
        return online;
    }

    public void setOnline(long online) {
        this.online = online;
    }

    public long getOnlineTime() {
        return onlineTime;
    }

    public void setOnlineTime(long onlineTime) {
        this.onlineTime = onlineTime;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public int getRoomLevel() {
        return roomLevel;
    }

    public void setRoomLevel(int roomLevel) {
        this.roomLevel = roomLevel;
    }

    public String getRoomTitle() {
        return roomTitle;
    }

    public void setRoomTitle(String roomTitle) {
        this.roomTitle = roomTitle;
    }

    public int getRoomType() {
        return roomType;
    }

    public void setRoomType(int roomType) {
        this.roomType = roomType;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public List<MultiRoomPermitInfo> getPermitList() {
        return permitList;
    }

    public void setPermitList(List<MultiRoomPermitInfo> permitList) {
        this.permitList = permitList;
    }
}
