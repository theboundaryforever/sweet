package com.tongdaxing.xchat_core.liveroom.im.model.bean;

import java.io.Serializable;

/**
 * @author weihaitao
 * @date 2019/7/16
 */
public class AudioConnReqInfo implements Serializable {

    /**
     * 主键ID
     */
    private int id;
    /**
     * 用户ID
     */
    private long uid;
    /**
     * 昵称
     */
    private String nick;
    /**
     * 性别
     */
    private int gender;
    /**
     * 用户头像
     */
    private String avatar;
    /**
     * 房间ID
     */
    private long roomId;
    /**
     * 申请金币
     */
    private long goldNum;
    /**
     * 申请内容
     */
    private String applyContent;
    /**
     * 申请时间
     */
    private long applyTime;
    /**
     * 连麦时间
     */
    private long micTime;
    /**
     * 结束时间
     */
    private long overTime;
    /**
     * 当前状态,1-等待中；2-连接中；3-已结束
     */
    private ApplyStatus applyStatus = ApplyStatus.Unknow;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public long getGoldNum() {
        return goldNum;
    }

    public void setGoldNum(long goldNum) {
        this.goldNum = goldNum;
    }

    public String getApplyContent() {
        return applyContent;
    }

    public void setApplyContent(String applyContent) {
        this.applyContent = applyContent;
    }

    public long getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(long applyTime) {
        this.applyTime = applyTime;
    }

    public long getMicTime() {
        return micTime;
    }

    public void setMicTime(long micTime) {
        this.micTime = micTime;
    }

    public long getOverTime() {
        return overTime;
    }

    public void setOverTime(long overTime) {
        this.overTime = overTime;
    }

    public ApplyStatus getApplyStatus() {
        return applyStatus;
    }

    public void setApplyStatus(ApplyStatus applyStatus) {
        this.applyStatus = applyStatus;
    }

    public enum ApplyStatus {
        Unknow,
        Reqing,
        Conning,
        Over
    }
}
