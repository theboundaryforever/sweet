package com.tongdaxing.xchat_core.room.auction;

import com.tongdaxing.erban.libcommon.base.BaseAppBean;

import java.util.List;

/**
 * 文件描述：房间信息返回的竞拍信息
 *
 * @auther：zwk
 * @data：2019/8/16
 */
public class RoomAuctionBean extends BaseAppBean {
    private int day;
    private String project;
    private long roomId;
    private long uid;
    private String avatar;
    private String nick;
    private int gender;
    private int count;//

    private List<AuctionUserBean> list;

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public List<AuctionUserBean> getList() {
        return list;
    }

    public void setList(List<AuctionUserBean> list) {
        this.list = list;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
