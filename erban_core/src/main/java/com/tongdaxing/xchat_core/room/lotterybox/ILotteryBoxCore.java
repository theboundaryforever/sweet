package com.tongdaxing.xchat_core.room.lotterybox;

import com.tongdaxing.erban.libcommon.coremanager.IBaseCore;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.json.Json;

/**
 * Created by Administrator on 2018/4/12.
 */

public interface ILotteryBoxCore extends IBaseCore {
    void lotteryRequest(String type, OkHttpManager.MyCallBack<Json> jsonMyCallBack);
}
