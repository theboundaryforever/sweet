package com.tongdaxing.xchat_core.room.view;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

import java.util.List;

/**
 * <p> </p>
 *
 * @author jiahui
 * @date 2017/12/19
 */
public interface IRoomBlackView extends IMvpBaseView {

    void onGetRoomBlackList(boolean isSuccess, String message, List<ChatRoomMember> chatRoomMemberList);

    void onRemoveUserFromBlackList(boolean isSuccess, String message, String account);
}
