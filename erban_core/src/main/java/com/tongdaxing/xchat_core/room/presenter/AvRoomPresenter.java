package com.tongdaxing.xchat_core.room.presenter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.SparseArray;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomInfo;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomResultData;
import com.netease.nimlib.sdk.util.Entry;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.PresenterEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RoomMicInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.RtcEngineManager;
import com.tongdaxing.xchat_core.manager.agora.AgoraEngineManager;
import com.tongdaxing.xchat_core.redpacket.bean.ActionDialogInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.model.AvRoomModel;
import com.tongdaxing.xchat_core.room.view.IAvRoomView;
import com.tongdaxing.xchat_core.user.VersionsCore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * <p>  </p>
 *
 * @author jiahui
 * @date 2017/12/11
 */
public class AvRoomPresenter extends AbstractMvpPresenter<IAvRoomView> {
    private static final String GET_ROOM_FROM_IMNET_ERROR = "-1101";
    private final AvRoomModel mAvRoomModel;
    private final String TAG = AvRoomPresenter.class.getSimpleName();
    private final Gson mGson;
    private Disposable mGetOnlineNumberDisposable;

    public AvRoomPresenter() {
        mAvRoomModel = new AvRoomModel();
        mGson = new Gson();
    }

    public AvRoomModel getmAvRoomModel() {
        return mAvRoomModel;
    }

    /**
     * 进入云信聊天室回调
     */
    public void enterRoom(final RoomInfo roomInfo, final boolean isFromMic) {
        if (roomInfo == null) {
            LogUtil.d(TAG, "enterRoom: enterRoom failed, because roomInfo is null");
            if (getMvpView() != null) {
                getMvpView().showFinishRoomView();
            }
            return;
        }
        LogUtil.d(TAG, "enterRoom: enterRoom roomId = " + roomInfo.getRoomId() + " ---> isFromMic = " + isFromMic);
        final RoomInfo currentRoom = BaseRoomServiceScheduler.getCurrentRoomInfo();
        //如果已经进入的房间要先退出房间
        if (currentRoom != null) {
            if (currentRoom.getUid() == roomInfo.getUid()) {
                return;
            }
            final int roomType = currentRoom.getType();
            final long roomId = currentRoom.getRoomId();
            LogUtil.d(TAG, "enterRoom: exitRoom roomId = " + roomId
                    + " ---> isFromMic = " + isFromMic
                    + " ---> roomType = " + roomType);
            //进入新的房间需要先退出房间
            CallBack callBack = new CallBack<String>() {
                @Override
                public void onSuccess(String data) {
                    LogUtil.d(TAG, "enterRoom: exitRoom onSuccess> roomId = " + roomId
                            + " ---> isFromMic = " + isFromMic
                            + " ---> roomType = " + roomType);
                    enterRoomAction(roomInfo, isFromMic);
                }

                @Override
                public void onFail(int code, String error) {
                    LogUtil.d(TAG, "enterRoom: exitRoom onFail> code = " + code + " ---> error = " + error);
                    dealEnterRoomError(new Throwable(code + " : " + error));
                }
            };
            BaseRoomServiceScheduler.exitRoom(callBack);
        } else {
            enterRoomAction(roomInfo, isFromMic);
        }
    }

    @SuppressWarnings("CheckResult")
    private void enterRoomAction(RoomInfo roomInfo, boolean isFromMic) {
        //我们自己服务端信息
        AvRoomDataManager.get().mCurrentRoomInfo = roomInfo;
        AvRoomDataManager.get().isShowLikeTa = false;
        AvRoomDataManager.get().mServiceRoominfo = roomInfo;
        final long roomId = roomInfo.getRoomId();
        LogUtil.d(TAG, "enterRoom: enterRoomAction> roomId = " + roomId + " ---> isFromMic = " + isFromMic);

        Observable<EnterChatRoomResultData> enterRoomObservable = mAvRoomModel.enterRoom(roomInfo.getRoomId(), 3, isFromMic);
        enterRoomObservable.flatMap((Function<EnterChatRoomResultData, ObservableSource<List<Entry<String, String>>>>) enterChatRoomResultData -> {
            LogUtil.d(TAG, "enterRoom: dealServerMicInfo-----> roomId = " + roomId);
            return dealServerMicInfo(enterChatRoomResultData);
        }).map(entries -> {
            LogUtil.d(TAG, "enterRoom: dealMicMemberFromIMNet-----> roomId = " + roomId);
            return dealMicMemberFromIMNet(entries);
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribe(roomQueueInfoSparseArray -> checkNeedDynamicGetAgoraKey(roomId),
                        throwable -> {
                            LogUtil.d(TAG, "enterRoom: error> throwable msg =" + throwable.getMessage() + " roomId = " + roomId);
                            dealEnterRoomError(throwable);
                        });
    }

    /**
     * 判断是否需要动态获取声网动态key
     */
    private void checkNeedDynamicGetAgoraKey(long roomId) {
        boolean dynamicKeyOption = CoreManager.getCore(VersionsCore.class).getDynamicKeyOption();
        LogUtil.d(TAG, "checkNeedDynamicGetAgoraKey: roomId = " + roomId + " ---> dynamicKeyOption = " + dynamicKeyOption);
        if (dynamicKeyOption) {
            getAgoreKeyFromServer(roomId);
        } else {
            long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
            AgoraEngineManager.get().startRtcEngine(uid, null, RoomInfo.ROOMTYPE_HOME_PARTY);
            if (getMvpView() != null) {
                getMvpView().enterRoomSuccess();
            }
        }
    }

    /**
     * 获取声网动态key
     *
     * @param roomId
     */
    private void getAgoreKeyFromServer(long roomId) {
        LogUtil.d(TAG, "getAgoreKeyFromServer: roomId = " + roomId + " ---> uid = " + CoreManager.getCore(IAuthCore.class).getCurrentUid());
        OkHttpManager.MyCallBack<Json> myCallBack = new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                dealEnterRoomError(new Throwable(e.getMessage()));

                LogUtil.d(TAG, "getAgoreKeyFromServer: onError msg = " + e.getMessage());
            }

            @Override
            public void onResponse(Json response) {
                boolean isRequestSuccess = response.num("code") == 200;
                String token = response.str("data");
                String message = response.str("message");
                LogUtil.d(TAG, "getAgoreKeyFromServer: response token =" + token + " ---> message = " + message);
                if (isRequestSuccess && !TextUtils.isEmpty(token)) {
                    long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
                    AgoraEngineManager.get().startRtcEngine(uid, token, RoomInfo.ROOMTYPE_HOME_PARTY);
                    if (getMvpView() != null) {
                        getMvpView().enterRoomSuccess();
                    }
                } else {
                    dealEnterRoomError(new Throwable(!TextUtils.isEmpty(message) ? message : ""));
                }
            }
        };

        mAvRoomModel.getRoomAgoraKey(roomId, myCallBack, null);
    }

    private void dealEnterRoomError(Throwable throwable) {
        throwable.printStackTrace();
        String error;
        LogUtil.d(TAG, "dealEnterRoomError: throwable msg =" + throwable.getMessage());
        if (throwable.getMessage() == null) {
            return;//这里观察到throwable.getMessage()==null，但是会走下一步subscribe.accept，找代码没见到有message==null的throwable，云信又不承认有这个问题，先这样给用户观察一下看看有没问题
        }
        switch (throwable.getMessage()) {
            case "414":
                error = "参数错误";
                if (getMvpView() != null) {
                    getMvpView().enterRoomFail(-1, error);
                }
                break;
            case "404":
            case "13002":
                if (getMvpView() != null) {
                    getMvpView().showFinishRoomView();
                }
                error = "聊天室不存在";
                break;
            case "403":
                error = "无权限";
                if (getMvpView() != null) {
                    getMvpView().enterRoomFail(-1, error);
                }
                break;
            case "500":
                error = "服务器内部错误";
                if (getMvpView() != null) {
                    getMvpView().enterRoomFail(-1, error);
                }
                break;
            case "13001":
                error = "IM主连接状态异常";
                if (getMvpView() != null) {
                    getMvpView().enterRoomFail(-1, error);
                }
                break;
            case "13003":
                error = "黑名单用户禁止进入聊天室";
                if (getMvpView() != null)
                    getMvpView().showBlackEnterRoomView();
                break;
            case GET_ROOM_FROM_IMNET_ERROR:
                exitRoom();
                if (getMvpView() != null) {
                    getMvpView().enterRoomFail(-1, "网络异常");
                }
                break;
            default:
                error = throwable.getMessage();
                if (getMvpView() != null) {
                    getMvpView().enterRoomFail(-1, error);
                }
                break;
        }
    }


    private void addInfoToMicInList(Entry<String, String> entry, JsonParser jsonParser) {
        JsonObject valueJsonObj = jsonParser.parse(entry.value).getAsJsonObject();
        if (valueJsonObj == null) {
            return;
        }
        Set<String> strings = valueJsonObj.keySet();
        Json json = new Json();
        for (String key : strings) {
            json.set(key, valueJsonObj.get(key).getAsString());
        }
        AvRoomDataManager.get().addMicInListInfo(entry.key, json);

    }

    /**
     * 处理网易云信坑位信息
     */
    private SparseArray<RoomQueueInfo> dealMicMemberFromIMNet(List<Entry<String, String>> entries) {
        if (!ListUtils.isListEmpty(entries)) {
            JsonParser jsonParser = new JsonParser();
            ChatRoomMember chatRoomMember;
            for (Entry<String, String> entry : entries) {
                LogUtil.d(TAG, "enterRoom: dealMicMemberFromIMNet key = " + entry.key + "   content = " + entry.value);
                if (entry.key != null && entry.key.length() > 2) {
                    addInfoToMicInList(entry, jsonParser);
                    continue;
                }
                if (entry.key != null) {
                    RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().mMicQueueMemberMap.get(Integer.parseInt(entry.key));
                    if (roomQueueInfo != null) {
                        JsonObject valueJsonObj = jsonParser.parse(entry.value).getAsJsonObject();
                        if (valueJsonObj != null) {
                            chatRoomMember = new ChatRoomMember();
                            if (valueJsonObj.has(Constants.USER_UID)) {
                                int uid = valueJsonObj.get(Constants.USER_UID).getAsInt();
                                chatRoomMember.setAccount(String.valueOf(uid));
                            }
                            if (valueJsonObj.has(Constants.USER_NICK)) {
                                chatRoomMember.setNick(valueJsonObj.get(Constants.USER_NICK).getAsString());
                            }
                            if (valueJsonObj.has(Constants.USER_AVATAR)) {
                                chatRoomMember.setAvatar(valueJsonObj.get(Constants.USER_AVATAR).getAsString());
                            }
                            if (valueJsonObj.has(Constants.USER_GENDER)) {
                                roomQueueInfo.gender = valueJsonObj.get(Constants.USER_GENDER).getAsInt();
                            }
                            Map<String, Object> stringStringMap = new HashMap<>();
                            if (valueJsonObj.has(Constants.headwearUrl)) {
                                stringStringMap.put(Constants.headwearUrl, valueJsonObj.get(Constants.headwearUrl).getAsString());
                            }
                            if (valueJsonObj.has(Constants.hasVggPic)) {
                                stringStringMap.put(Constants.hasVggPic, valueJsonObj.get(Constants.hasVggPic).getAsBoolean());
                            }
                            if (valueJsonObj.has(Constants.USER_EXPER_LEVEL)) {
                                stringStringMap.put(Constants.USER_EXPER_LEVEL, valueJsonObj.get(Constants.USER_EXPER_LEVEL).getAsInt());
                            }
                            if (valueJsonObj.has(Constants.USER_NICK_IN_ROOM)) {
                                stringStringMap.put(Constants.USER_NICK_IN_ROOM, valueJsonObj.get(Constants.USER_NICK_IN_ROOM).getAsString());
                            }
                            if (valueJsonObj.has(Constants.USER_CAR)) {
                                stringStringMap.put(Constants.USER_CAR, valueJsonObj.get(Constants.USER_CAR).getAsString());
                            }
                            if (valueJsonObj.has(Constants.USER_CAR_NAME)) {
                                stringStringMap.put(Constants.USER_CAR_NAME, valueJsonObj.get(Constants.USER_CAR_NAME).getAsString());
                            }
                            if (valueJsonObj.has(Constants.ROOM_ID)) {
                                stringStringMap.put(Constants.ROOM_ID, valueJsonObj.get(Constants.ROOM_ID).getAsString());
                            }
                            if (valueJsonObj.has(Constants.ROOM_FROM_MIC)) {
                                stringStringMap.put(Constants.ROOM_FROM_MIC, valueJsonObj.get(Constants.ROOM_FROM_MIC).getAsBoolean());
                            }
                            if (valueJsonObj.has(Constants.ALIAS_ICON_URL)) {
                                stringStringMap.put(Constants.ALIAS_ICON_URL, valueJsonObj.get(Constants.ALIAS_ICON_URL).getAsString());
                            }
                            if (valueJsonObj.has(Constants.USER_NOBLE_MEDAL)) {
                                stringStringMap.put(Constants.USER_NOBLE_MEDAL, valueJsonObj.get(Constants.USER_NOBLE_MEDAL).getAsString());
                            }
                            if (valueJsonObj.has(Constants.USER_MEDAL_ID)) {
                                stringStringMap.put(Constants.USER_MEDAL_ID, valueJsonObj.get(Constants.USER_MEDAL_ID).getAsInt());
                            }
                            if (valueJsonObj.has(Constants.USER_MEDAL_DATE)) {
                                stringStringMap.put(Constants.USER_MEDAL_DATE, valueJsonObj.get(Constants.USER_MEDAL_DATE).getAsInt());
                            }
                            if (valueJsonObj.has(Constants.NOBLE_INVISIABLE_ENTER_ROOM)) {
                                boolean operaSuc = false;
                                try {
                                    stringStringMap.put(Constants.NOBLE_INVISIABLE_ENTER_ROOM, valueJsonObj.get(Constants.NOBLE_INVISIABLE_ENTER_ROOM).getAsInt());
                                    operaSuc = true;
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                                if (!operaSuc) {
                                    try {
                                        stringStringMap.put(Constants.NOBLE_INVISIABLE_ENTER_ROOM, valueJsonObj.get(Constants.NOBLE_INVISIABLE_ENTER_ROOM).getAsBoolean() ? 1 : 0);
                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                    }
                                }
                            }

                            if (valueJsonObj.has(Constants.ROOM_MEMBER_ID)) {
                                stringStringMap.put(Constants.ROOM_MEMBER_ID, valueJsonObj.get(Constants.ROOM_MEMBER_ID).getAsLong());
                            }
                            if (valueJsonObj.has(Constants.IS_NEW_USER)) {
                                stringStringMap.put(Constants.IS_NEW_USER, valueJsonObj.get(Constants.IS_NEW_USER).getAsString());
                            }
                            chatRoomMember.setExtension(stringStringMap);
                            roomQueueInfo.mChatRoomMember = chatRoomMember;

                            //增加防炸房校验逻辑，同时开始接受此用户的推流
                            try {
                                RtcEngineManager.get().muteRemoteAudioStream(Integer.valueOf(chatRoomMember.getAccount()), false);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                        AvRoomDataManager.get().mMicQueueMemberMap.put(Integer.valueOf(entry.key), roomQueueInfo);
                    }
                }
            }
        }
        return AvRoomDataManager.get().mMicQueueMemberMap;
    }


    /**
     * 处理服务端坑位信息
     */
    @Nullable
    private ObservableSource<List<Entry<String, String>>> dealServerMicInfo(EnterChatRoomResultData enterChatRoomResultData) {
        AvRoomDataManager.get().mEnterChatRoomResultData = enterChatRoomResultData;
        if (enterChatRoomResultData == null) {
            return Observable.error(new Throwable(GET_ROOM_FROM_IMNET_ERROR));
        } else {
            final ChatRoomInfo roomInfo = enterChatRoomResultData.getRoomInfo();
            if (roomInfo == null) {
                return Observable.error(new Throwable(GET_ROOM_FROM_IMNET_ERROR));
            }
            AvRoomDataManager.get().mCurrentRoomInfo.onlineNum = roomInfo.getOnlineUserCount();
            Map<String, Object> extension = roomInfo.getExtension();
            if (extension != null) {
                String roomInfoStr = (String) extension.get(Constants.KEY_CHAT_ROOM_INFO_ROOM);
                if (!TextUtils.isEmpty(roomInfoStr)) {
                    RoomInfo extRoomInfo = mGson.fromJson(roomInfoStr, RoomInfo.class);
                    extRoomInfo.setRoomId(Long.valueOf(roomInfo.getRoomId()));
                    extRoomInfo.onlineNum = AvRoomDataManager.get().mCurrentRoomInfo.onlineNum;
                    //云信服务端信息
                    LogUtils.d("request_extRoomInfo" + extRoomInfo);
                    AvRoomDataManager.get().mCurrentRoomInfo = extRoomInfo;
                    AvRoomDataManager.get().isShowLikeTa = false;
                }
                //获取云信麦序相关信息
                String roomMicStr = (String) extension.get(Constants.KEY_CHAT_ROOM_INFO_MIC);
                if (!TextUtils.isEmpty(roomMicStr)) {
                    //初始化所有坑位
                    Map<String, String> micMapStr = mGson.fromJson(roomMicStr,
                            new TypeToken<Map<String, String>>() {
                            }.getType());

                    int micMaxIndex = AvRoomDataManager.ROOM_MAX_MIC_INDEX;
                    for (Map.Entry<String, String> entry : micMapStr.entrySet()) {
                        Integer key = Integer.valueOf(entry.getKey());
                        LogUtils.d(TAG, "dealServerMicInfo-key:" + key);
                        if (key > micMaxIndex) {
                            //避免key无序，不用break
                            LogUtils.d(TAG, "dealServerMicInfo-key:" + key + " continue");
                            continue;
                        }
                        LogUtils.d(TAG, "dealServerMicInfo-key:" + key + " put value:" + entry.getValue());
                        AvRoomDataManager.get().mMicQueueMemberMap.put(key,
                                new RoomQueueInfo(mGson.fromJson(entry.getValue(), RoomMicInfo.class), null));
                    }
                    return mAvRoomModel.queryRoomMicInfo(roomInfo.getRoomId());
                }
            }
            return Observable.error(new Throwable(GET_ROOM_FROM_IMNET_ERROR));
        }
    }

    public void exitRoom() {
        CallBack callBack = new CallBack<String>() {
            @Override
            public void onSuccess(String data) {
                if (getMvpView() != null) {
                    getMvpView().exitRoom(AvRoomDataManager.get().mCurrentRoomInfo);
                }
            }

            @Override
            public void onFail(int code, String error) {

            }
        };
        BaseRoomServiceScheduler.exitRoom(callBack);
    }


    public void requestRoomInfoFromService(String uId, int roomType) {
        LogUtil.d(TAG, "enterRoom: requestRoomInfoFromService> uId = " + uId + " roomType:" + roomType);
        if (TextUtils.isEmpty(uId)) {
            return;
        }

        OkHttpManager.MyCallBack myCallBack = new OkHttpManager.MyCallBack<ServiceResult<RoomInfo>>() {
            @Override
            public void onError(Exception e) {
                LogUtil.d(TAG, "enterRoom: onErrorResponse> error = " + e.getMessage());
                if (getMvpView() != null) {
                    getMvpView().requestRoomInfoFailView(-1, e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<RoomInfo> data) {
                if (getMvpView() == null) {
                    return;
                }
                if (null != data) {
                    LogUtil.d(TAG, "enterRoom: onResponse> msg = " + data.getMessage() + " ---> code = " + data.getCode() +
                            " ---> roomId =" + (data.getData() == null ? "0" : data.getData().getRoomId()));
                    if (data.isSuccess()) {
                        if (data.getData() == null || data.getData().getRoomId() == 0) {
                            getMvpView().showFinishRoomView();
                            return;
                        }
                        enterHomePartRoom(data.getData());
                    } else {
                        //2019年4月16日 11:41:15 之前版本依旧保持的服务器判断，新的业务场景改为 实名认证状态 本地判断，服务器接口内部逻辑会将实名认证判断逻辑去掉
                        getMvpView().requestRoomInfoFailView(data.getCode(), data.getErrorMessage());
                    }
                } else {
                    LogUtil.d(TAG, "enterRoom: onResponse> data = null");
                    getMvpView().requestRoomInfoFailView(-1, "进入房间失败");
                }
            }
        };

        mAvRoomModel.requestRoomInfoFromService(uId, roomType, myCallBack);
    }

    private void enterHomePartRoom(RoomInfo targetRoomInfo) {
        //退出已有音频房间逻辑
        //新旧房间都做判断退出逻辑
        RoomInfo currRoomInfo = BaseRoomServiceScheduler.getCurrentRoomInfo();
        if (currRoomInfo != null) {
            if (currRoomInfo.getUid() == targetRoomInfo.getUid()
                    && currRoomInfo.getType() == targetRoomInfo.getType()) {
                LogUtil.d(TAG, "enterHomePartRoom 回到所在房间，不做退房处理");
                return;
            }
            //进入新的房间需要先退出旧(可能新类型的单人音频房间，也可能是旧的轰趴类型房间)房间
            CallBack<String> callBack = new CallBack<String>() {
                @Override
                public void onSuccess(String data1) {
                    LogUtil.d(TAG, "enterHomePartRoom-->exitRoom--> onSuccess-->requestRoomInfoSuccessView");
                    if(null != getMvpView()){
                        getMvpView().requestRoomInfoSuccessView(targetRoomInfo);
                    }
                }

                @Override
                public void onFail(int code, String error) {
                    LogUtil.d(TAG, "enterHomePartRoom-->exitRoom--> onFail code:" + code + " error:" + error);
                    if(null != getMvpView()){
                        getMvpView().requestRoomInfoFailView(code, error);
                    }
                }
            };
            BaseRoomServiceScheduler.exitRoom(callBack);
        } else {
            if(null != getMvpView()){
                getMvpView().requestRoomInfoSuccessView(targetRoomInfo);
            }
        }
    }

    /**
     * 获取活动信息
     */
    public void getActionDialog(int type) {

        OkHttpManager.MyCallBack myCallBack = new OkHttpManager.MyCallBack<ServiceResult<List<ActionDialogInfo>>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().onGetActionDialogError(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<List<ActionDialogInfo>> data) {
                if (null != data && data.isSuccess()) {
                    if (getMvpView() != null && data.getData() != null) {
                        getMvpView().onGetActionDialog(data.getData());
                    }
                } else {
                    if (getMvpView() != null) {
                        getMvpView().onGetActionDialogError(data.getErrorMessage());
                    }
                }
            }
        };
        mAvRoomModel.getActionDialog(type, myCallBack);
    }

    /**
     * 获取房间内固定成员列表
     */
    public void getNormalChatMember() {
        LogUtils.d(TAG, "getNormalChatMember");
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null) {
            return;
        }
        long currentUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        mAvRoomModel.getNormalChatMember(String.valueOf(roomInfo.getRoomId()), currentUid);
    }

    private void startGetOnlineMemberNumberJob() {
        Observable.interval(10, 10, TimeUnit.SECONDS, Schedulers.io())
                .subscribe(new Observer<Long>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        mGetOnlineNumberDisposable = d;
                    }

                    @Override
                    public void onNext(Long aLong) {
                        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                        if (roomInfo == null) {
                            return;
                        }

                        Disposable disposable = mAvRoomModel.startGetOnlineMemberNumberJob(roomInfo.getRoomId())
                                .observeOn(AndroidSchedulers.mainThread())
                                .compose(AvRoomPresenter.this.bindUntilEvent(PresenterEvent.DESTROY))
                                .subscribe(new Consumer<ChatRoomInfo>() {
                                    @Override
                                    public void accept(ChatRoomInfo chatRoomInfo) throws Exception {
                                        if (chatRoomInfo == null) {
                                            return;
                                        }
                                        int onlineUserCount = chatRoomInfo.getOnlineUserCount();
                                        if (AvRoomDataManager.get().mCurrentRoomInfo != null) {
                                            AvRoomDataManager.get().mCurrentRoomInfo.onlineNum = onlineUserCount;
                                        }


                                        if (getMvpView() != null) {
                                            getMvpView().onRoomOnlineNumberSuccess(onlineUserCount);
                                        }
                                    }
                                });
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });

    }

    @Override
    public void onDestroyPresenter() {
        super.onDestroyPresenter();
        if (mGetOnlineNumberDisposable != null) {
            mGetOnlineNumberDisposable.dispose();
            mGetOnlineNumberDisposable = null;
        }
    }

    @Override
    public void onCreatePresenter(@Nullable Bundle saveState) {
        super.onCreatePresenter(saveState);
        startGetOnlineMemberNumberJob();
    }

    public boolean checkIsKick(long roomUid, int roomType) {
        return mAvRoomModel.checkIsKick(roomUid, roomType);
    }

    /**
     * 当前自己是否是第一次进入此房间
     */
    public boolean isFirstEnterRoomOrChangeOtherRoom(long roomUid, int roomType) {
        return AvRoomDataManager.get().isFirstEnterRoomOrChangeOtherRoom(roomUid, roomType);
    }

}
