package com.tongdaxing.xchat_core.room.publicchatroom;

import android.text.TextUtils;

import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nimlib.sdk.NIMChatRoomSDK;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.chatroom.ChatRoomMessageBuilder;
import com.netease.nimlib.sdk.chatroom.ChatRoomService;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomData;
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomResultData;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.msg.model.NIMAntiSpamOption;
import com.netease.nimlib.sdk.msg.model.QueryDirectionEnum;
import com.tongdaxing.erban.libcommon.coremanager.AbstractBaseCore;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.erban.libcommon.utils.TimeUtils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.PublicChatRoomAttachment;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.noble.NobleBusinessManager;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiConsumer;

import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_SUB_PUBLIC_CHAT_ROOM;


/**
 * Created by Administrator on 2018/3/20.
 */

public class PublicChatRoomController extends AbstractBaseCore {

    public static final long maxWaitTime = 60000;

    public static long devRoomId = 22141146;
    public static long formalRoomId = 22551544;
    public List<ChatRoomMessage> messages;
    public long cacheTime = 0;
    public static final String TAG = "public_chat_log";
    private long roomId = formalRoomId;
    private String cacheNameKey = "cacheNameKey";

    public PublicChatRoomController() {
        messages = new ArrayList<>();
        if (BasicConfig.isDebug) {
            roomId = devRoomId;
        }
    }

    public static long getPublicChatRoomId() {
        return BasicConfig.isDebug ? devRoomId : formalRoomId;
    }

    public static boolean filterOtherRoomMsg(ChatRoomMessage chatRoomMessage) {
        if (chatRoomMessage == null) {
            return true;
        }
        if (chatRoomMessage.getMsgType() != MsgTypeEnum.custom) {
            return true;
        }
        CustomAttachment attachment = (CustomAttachment) chatRoomMessage.getAttachment();
        if (attachment.getFirst() != CustomAttachment.CUSTOM_MSG_SUB_PUBLIC_CHAT_ROOM
                && (attachment.getFirst() != CustomAttachment.CUSTOM_MSG_FIRST_CALL_UP)) {
            return true;
        }
        //消息为空过滤
        MsgAttachment chatRoomMessageAttachment = chatRoomMessage.getAttachment();
        if (chatRoomMessageAttachment instanceof PublicChatRoomAttachment) {
            String msg = ((PublicChatRoomAttachment) chatRoomMessageAttachment).getMsg();
            if (TextUtils.isEmpty(msg)) {
                return true;
            }
        }
        return !chatRoomMessage.getSessionId().equals(String.valueOf(getPublicChatRoomId()));
    }

    public void enterRoom(final ActionCallBack actionCallBack) {
        LogUtil.d(TAG, "enterRoom");
        //适配云信乱发下麦消息的坑
        final EnterChatRoomData data = new EnterChatRoomData(String.valueOf(roomId));
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo != null) {
            //2.7.8版本开始，广播广场消息，头像、昵称、等级、贵族级别全部使用PublicChatRoomAttachment#params.put/get的方式,
            // 为了兼容旧版本，enterRoom方法内的传参逻辑保持不动
            Map<String, Object> stringIntegerMap = new HashMap<>();
            stringIntegerMap.put(Constants.PUBLIC_CHAT_ROOM, Constants.PUBLIC_CHAT_ROOM);
            stringIntegerMap.put(Constants.USER_EXPER_LEVEL, userInfo.getExperLevel());
            stringIntegerMap.put(Constants.USER_AVATAR, userInfo.getNick());
            stringIntegerMap.put(Constants.USER_CAR, userInfo.getCarUrl());
            stringIntegerMap.put(Constants.USER_CAR_NAME, userInfo.getCarName());
            stringIntegerMap.put(Constants.USER_NOBLE_MEDAL, userInfo.getVipMedal());
            stringIntegerMap.put(Constants.USER_MEDAL_ID, userInfo.getVipId());
            data.setNotifyExtension(stringIntegerMap);
            data.setExtension(stringIntegerMap);
        }
        NIMClient.getService(ChatRoomService.class).enterChatRoomEx(data, 3).setCallback(new RequestCallback<EnterChatRoomResultData>() {
            @Override
            public void onSuccess(EnterChatRoomResultData result) {
                actionCallBack.success();
                // 登录成功
                MsgTypeEnum[] typeEnums = new MsgTypeEnum[]{MsgTypeEnum.custom};
                LogUtil.d(TAG, "enterRoom-->onSuccess-->pullHostory roomId:" + roomId);
                NIMClient.getService(ChatRoomService.class)
                        .pullMessageHistoryExType(roomId + "", System.currentTimeMillis(), 20, QueryDirectionEnum.QUERY_OLD, typeEnums)
                        .setCallback(new RequestCallback<List<ChatRoomMessage>>() {
                            @Override
                            public void onSuccess(List<ChatRoomMessage> param) {
                                LogUtil.d(TAG, "enterRoom-->onSuccess-->pullHostory-->onSuccess");
                                Collections.reverse(param);
                                for (int i = 0; i < param.size(); i++) {
                                    ChatRoomMessage chatRoomMessage = param.get(i);
                                    MsgAttachment attachment = chatRoomMessage.getAttachment();
                                    if (attachment instanceof CustomAttachment) {
                                        CustomAttachment customAttachment = (CustomAttachment) attachment;
//                                        LogUtils.d(TAG, "enterRoom-->onSuccess-->pullHostory-->onSuccess i:"+i
//                                                +" first:"+customAttachment.getFirst()+" second:"+customAttachment.getSecond());
                                        if ((customAttachment).getFirst() == CustomAttachment.CUSTOM_MSG_SUB_PUBLIC_CHAT_ROOM
                                                || (customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_FIRST_CALL_UP &&
                                                customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_PUBLIC_CHAT_ROOM)) {
                                            onSendRoomMessageSuccess(chatRoomMessage);
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onFailed(int code) {
                                LogUtil.d(TAG, "enterRoom-->onSuccess-->pullHostory-->onFailed code:" + code);
                            }

                            @Override
                            public void onException(Throwable exception) {
                                exception.printStackTrace();
                                LogUtil.d(TAG, "enterRoom-->onSuccess-->pullHostory-->onException exc:" + exception.getMessage());
                            }
                        });
            }

            @Override
            public void onFailed(int code) {
                LogUtil.d(TAG, "enterRoom-->onFailed code:" + code);
                // 登录失败
                actionCallBack.error();
            }

            @Override
            public void onException(Throwable exception) {
                exception.printStackTrace();
                LogUtil.d(TAG, "enterRoom-->onException exc:" + exception.getMessage());
                // 错误
                actionCallBack.error();
            }
        });


    }


    public void leaveRoom() {
        LogUtil.d(TAG, "leaveRoom roomId:" + roomId);
        NIMChatRoomSDK.getChatRoomService().exitChatRoom(roomId + "");
    }

    public void inintCacheTime() {
        cacheTime = (long) SpUtils.get(getContext(), cacheNameKey, 0L);

    }


    public void refreshTime() {
        cacheTime = System.currentTimeMillis();
        SpUtils.put(getContext(), cacheNameKey, cacheTime);
    }

    public Disposable sendMsg(String content) {
//        LogUtils.d(TAG,"sendMsg content:"+content);
        if (TextUtils.isEmpty(content)) {
            return null;
        }
        //2.7.8版本开始，广播广场消息，头像、昵称、等级、贵族级别全部使用PublicChatRoomAttachment#params.put/get的方式,
        // 为了兼容旧版本，enterRoom方法内的传参逻辑保持不动
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (null == userInfo) {
            return null;
        }
        PublicChatRoomAttachment publicChatRoomAttachment = new PublicChatRoomAttachment(
                CUSTOM_MSG_SUB_PUBLIC_CHAT_ROOM, CUSTOM_MSG_SUB_PUBLIC_CHAT_ROOM);
        publicChatRoomAttachment.setMsg(content);
        Json json = new Json();
        json.set(Constants.USER_AVATAR, NobleBusinessManager.getNobleRoomAvatarUrl(
                userInfo.getVipId(), userInfo.getIsInvisible(), userInfo.getVipDate(), userInfo.getAvatar()));
        json.set(Constants.USER_UID, userInfo.getUid());
        json.set(Constants.USER_CHARM_LEVEL, userInfo.getCharmLevel());
        //2.7.8版本之后才塞的财富值等级，保持同IOS同步
        json.set(Constants.USER_EXPER_LEVEL, userInfo.getExperLevel());
        json.set(Constants.USER_NICK, NobleBusinessManager.getNobleRoomNick(userInfo.getVipId(),
                userInfo.getIsInvisible(), userInfo.getVipDate(), userInfo.getNick()));
        json.set(Constants.USER_NOBLE_MEDAL, userInfo.getVipMedal());
        json.set(Constants.USER_MEDAL_ID, userInfo.getVipId());
        json.set(Constants.USER_MEDAL_DATE, userInfo.getVipDate());
        json.set(Constants.NOBLE_INVISIABLE_ENTER_ROOM, userInfo.getIsInvisible() ? 1 : 0);
        json.set(Constants.NOBLE_INVISIABLE_UID, userInfo.getInvisibleUid());
        json.set(Constants.USER_GENDER, userInfo.getGender());
        json.set(Constants.USER_AGE, TimeUtils.getAgeByBirth(userInfo.getBirth()));
        publicChatRoomAttachment.setParams(json + "");
        ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                // 聊天室id
                roomId + "",
                // 自定义消息
                publicChatRoomAttachment
        );
        message.setContent(content);
        // 构造反垃圾对象
        NIMAntiSpamOption antiSpamOption = new NIMAntiSpamOption();
        Json jsonContent = new Json();
        jsonContent.set("type", 1);
        jsonContent.set("data", content);

        antiSpamOption.content = jsonContent + "";
        message.setNIMAntiSpamOption(antiSpamOption);
        return IMNetEaseManager.get().sendChatRoomMessage(message, false,
                IMNetEaseManager.get().checkAntiOptionEnable())
                .subscribe(new BiConsumer<ChatRoomMessage, Throwable>() {
                    @Override
                    public void accept(ChatRoomMessage chatRoomMessage,
                                       Throwable throwable) throws Exception {
//                        LogUtils.d(TAG,"sendMsg--accept chatRoomMessage is null:"+(null == chatRoomMessage));
                        if (chatRoomMessage != null) {
                            onSendRoomMessageSuccess(chatRoomMessage);
                            if (sendMsgCallBack != null) {
                                sendMsgCallBack.success();
                            }
                        }
                        if (null != throwable) {
                            throwable.printStackTrace();
                            LogUtil.d(TAG, "sendMsg-->accept exc:" + throwable.getMessage());
                        }
                    }
                });
    }

    private void onSendRoomMessageSuccess(ChatRoomMessage chatRoomMessage) {
//        LogUtils.d(PublicChatRoomController.TAG,"sendMsg-->onSendRoomMessageSuccess");
        IMNetEaseManager.get().addMessagesImmediately(chatRoomMessage);
    }

    public interface ActionCallBack {
        void success();

        void error();
    }

    private SendMsgCallBack sendMsgCallBack;

    public void setSendMsgCallBack(SendMsgCallBack sendMsgCallBack) {
        this.sendMsgCallBack = sendMsgCallBack;
    }

    public interface SendMsgCallBack {
        void success();

        void error();
    }
}
