package com.tongdaxing.xchat_core.room.presenter;

import android.util.SparseArray;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.model.RoomInviteModel;
import com.tongdaxing.xchat_core.room.view.IRoomInviteView;

import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;

/**
 * <p>  </p>
 *
 * @author jiahui
 * @date 2017/12/21
 */
public class RoomInvitePresenter extends AbstractMvpPresenter<IRoomInviteView> {

    private RoomInviteModel mRoomInviteModel;

    public RoomInvitePresenter() {
        mRoomInviteModel = new RoomInviteModel();
    }

    /**
     * 分页获取房间成员：第一页包含固定在线成员，游客50人，之后每一页获取游客50人
     *
     * @param page 页数
     * @param time 固定成员列表用updateTime,
     *             游客列表用进入enterTime，
     *             填0会使用当前服务器最新时间开始查询，即第一页，单位毫秒
     */
    public void requestChatMemberByPage(final int page, long time) {
        mRoomInviteModel.getPageMembers(page, time)
                .map(new Function<List<ChatRoomMember>, List<ChatRoomMember>>() {
                    @Override
                    public List<ChatRoomMember> apply(List<ChatRoomMember> chatRoomMemberList) throws Exception {
                        SparseArray<RoomQueueInfo> queueInfoSparseArray = AvRoomDataManager.get().mMicQueueMemberMap;
                        int size;
                        if (queueInfoSparseArray == null || (size = queueInfoSparseArray.size()) <= 0) {
                            return chatRoomMemberList;
                        }
                        //移除麦上人员
                        ChatRoomMember chatRoomMember;
                        ListIterator<ChatRoomMember> iterator = chatRoomMemberList.listIterator();
                        for (; iterator.hasNext(); ) {
                            chatRoomMember = iterator.next();
                            boolean remove = false;
                            for (int i = 0; i < size; i++) {
                                RoomQueueInfo roomQueueInfo = queueInfoSparseArray.valueAt(i);
                                //判断是否在麦上
                                if (roomQueueInfo.mChatRoomMember != null
                                        && Objects.equals(chatRoomMember.getAccount(), roomQueueInfo.mChatRoomMember.getAccount())) {
                                    remove = true;
                                    break;
                                }
                            }
                            //判断房主在线，但是不在麦位的情况，需要移除房主
                            if (!remove && AvRoomDataManager.get().isRoomOwner(chatRoomMember.getAccount())) {
                                remove = true;
                            }
                            if (remove) {
                                iterator.remove();
                            }
                        }
                        return chatRoomMemberList;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<ChatRoomMember>>() {
                    @Override
                    public void accept(List<ChatRoomMember> chatRoomMemberList) throws Exception {
                        if (getMvpView() != null) {
                            getMvpView().onRequestMemberByPageSuccess(chatRoomMemberList, page);
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        throwable.printStackTrace();
                        if (getMvpView() != null) {
                            getMvpView().onRequestChatMemberByPageFail(throwable.getMessage(), page);
                        }
                    }
                });
    }
}
