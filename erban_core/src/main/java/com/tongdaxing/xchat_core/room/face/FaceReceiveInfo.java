package com.tongdaxing.xchat_core.room.face;

import java.io.Serializable;
import java.util.List;

/**
 * @author chenran
 * @date 2017/9/12
 * 房间里面其中一个人发运气表情，其他人通过云信收到对应的运气表情的java bean
 */

public class FaceReceiveInfo implements Serializable {
    private long uid;
    private String nick;
    private int faceId;

    private String vipMedal;
    /**
     * 整型，当前贵族有效期，以天为单位
     */
    private int vipDate;
    /**
     * 进房隐身开关，0关1开,更新接口传参时，以0/1为值传递
     */
    private int isInvisible = 0;
    /**
     * 整型，贵族等级级别，0代表没有贵族等级，1-7代表相应的贵族级别
     */
    private int vipId;
    private String alias_icon_url;

    public int getVipDate() {
        return vipDate;
    }

    public void setVipDate(int vipDate) {
        this.vipDate = vipDate;
    }

    public int getIsInvisible() {
        return isInvisible;
    }

    public void setIsInvisible(int isInvisible) {
        this.isInvisible = isInvisible;
    }

    public int getVipId() {
        return vipId;
    }

    public void setVipId(int vipId) {
        this.vipId = vipId;
    }

    @Override
    public String toString() {
        return "FaceReceiveInfo{" +
                "uid=" + uid +
                ", nick='" + nick + '\'' +
                ", faceId=" + faceId +
                ", vipMedal='" + vipMedal + '\'' +
                ", vipDate=" + vipDate +
                ", isInvisible=" + isInvisible +
                ", vipId=" + vipId +
                ", alias_icon_url='" + alias_icon_url + '\'' +
                ", resultIndexes=" + resultIndexes +
                '}';
    }

    public String getAliasIconUrl() {
        return alias_icon_url;
    }

    public void setAliasIconUrl(String alias_icon_url) {
        this.alias_icon_url = alias_icon_url;
    }

    public String getVipMedal() {
        return vipMedal;
    }

    public void setVipMedal(String vipMedal) {
        this.vipMedal = vipMedal;
    }
    private List<Integer> resultIndexes;


    public List<Integer> getResultIndexes() {
        return resultIndexes;
    }

    public void setResultIndexes(List<Integer> resultIndexes) {
        this.resultIndexes = resultIndexes;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public int getFaceId() {
        return faceId;
    }

    public void setFaceId(int faceId) {
        this.faceId = faceId;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

}
