package com.tongdaxing.xchat_core.room.auction;

import com.tongdaxing.erban.libcommon.base.BaseAppBean;

/**
 * 文件描述：竞拍人的信息
 *
 * @auther：zwk
 * @data：2019/7/29
 */
public class AuctionUserBean extends BaseAppBean {
    private long uid;
    private String nick;
    private String goldNum;
    private String avatar;
    private int gender;

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getGoldNum() {
        return goldNum;
    }

    public void setGoldNum(String goldNum) {
        this.goldNum = goldNum;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }
}
