package com.tongdaxing.xchat_core.room.view;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;

import java.util.List;

/**
 * <p>  </p>
 *
 * @author jiahui
 * @date 2017/12/21
 */
public interface IRoomInviteView extends IHomePartyUserListView {

    void onRequestMemberByPageSuccess(List<ChatRoomMember> memberList, int page);

}
