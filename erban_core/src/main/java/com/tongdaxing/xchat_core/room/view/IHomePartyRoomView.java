package com.tongdaxing.xchat_core.room.view;

import android.util.SparseArray;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.bean.RoomMicInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.queue.bean.RoomConsumeInfo;

import java.util.List;

/**
 * <p> 轰趴房View层  </p>
 *
 * @author jiahui
 * @date 2017/12/8
 */
public interface IHomePartyRoomView extends IMvpBaseView {

    /**
     * 获取点击头像Button 列表
     *
     * @param micPosition    麦上位置
     * @param chatRoomMember 麦上位置
     * @param currentRoom    麦上位置
     * @param gender
     * @return
     */
    SparseArray<ButtonItem> getAvatarButtonItemList(int micPosition, ChatRoomMember chatRoomMember, RoomInfo currentRoom, int gender);

    /**
     * 点击麦上用户头像，显示操作对话框
     *
     * @param buttonItemList
     */
    void showMicAvatarClickDialog(List<ButtonItem> buttonItemList);

    /**
     * 点击麦上用户头像直接显示送礼物弹窗
     *
     */
    void showGiftDialog(ChatRoomMember chatRoomMember);

    /** 被踢下麦成功 */
    void kickDownMicroPhoneSuccess();

    /**
     * 麦上没人点击坑位处理
     *
     * @param roomMicInfo 坑位信息
     * @param micPosition
     * @param currentUid
     */
    void showOwnerClickDialog(RoomMicInfo roomMicInfo, int micPosition, long currentUid,boolean isOwner);

    /** 断网重连回调 */
    void chatRoomReConnectView();

    /** 房主点击自己头像 */
    void showOwnerSelfInfo(ChatRoomMember chatRoomMember);

    /** 房主自动上麦成功后通知刷新*/
    void notifyRefresh();

    /**
     * 更新相亲房间相亲选择状态
     */
    void updatePairInfoForMicQueueChanged();

    void onGetFortuneRankTop(boolean isSuccess, String message, List<RoomConsumeInfo> roomConsumeInfos);
}
