package com.tongdaxing.xchat_core.room.queue.bean;

/**
 * Created by chenran on 2017/10/25.
 */

public class MicMemberInfo {
    private long uid;
    private String avatar;
    private String nick;
    private int micPosition;
    private boolean isRoomOwnner;

    private int vipId = 0;
    private int vipDate = 0;
    private boolean isInvisible = false;

    public int getVipId() {
        return vipId;
    }

    public void setVipId(int vipId) {
        this.vipId = vipId;
    }

    public int getVipDate() {
        return vipDate;
    }

    public void setVipDate(int vipDate) {
        this.vipDate = vipDate;
    }

    //礼物用户选择中状态判断
    private boolean isSelect = false;

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    @Override
    public String toString() {
        return "MicMemberInfo{" +
                "uid=" + uid +
                ", avatar='" + avatar + '\'' +
                ", nick='" + nick + '\'' +
                ", micPosition=" + micPosition +
                ", isRoomOwnner=" + isRoomOwnner +
                ", vipId=" + vipId +
                ", vipDate=" + vipDate +
                ", isInvisible=" + isInvisible +
                '}';
    }

    public boolean isInvisible() {
        return isInvisible;
    }

    public void setInvisible(boolean invisible) {
        isInvisible = invisible;
    }

    public MicMemberInfo() {

    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getMicPosition() {
        return micPosition;
    }

    public void setMicPosition(int micPosition) {
        this.micPosition = micPosition;
    }

    public boolean isRoomOwnner() {
        return isRoomOwnner;
    }

    public void setRoomOwnner(boolean roomOwnner) {
        isRoomOwnner = roomOwnner;
    }
}
