package com.tongdaxing.xchat_core.room.presenter;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.noble.NobleBusinessManager;
import com.tongdaxing.xchat_core.room.bean.ServerUserMemberInfo;
import com.tongdaxing.xchat_core.room.model.HomePartyUserListModel;
import com.tongdaxing.xchat_core.room.model.RoomBaseModel;
import com.tongdaxing.xchat_core.room.view.IRoomBlackView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p> </p>
 *
 * @author jiahui
 * @date 2017/12/19
 */
public class RoomBlackPresenter extends AbstractMvpPresenter<IRoomBlackView> {

    private final String TAG = RoomBlackPresenter.class.getSimpleName();

    private final RoomBaseModel mRoomBaseModel;
    private final HomePartyUserListModel mHomePartyUserListMode;
    private List<ServerUserMemberInfo> oldList = new ArrayList<>();

    public RoomBlackPresenter() {
        mRoomBaseModel = new RoomBaseModel();
        mHomePartyUserListMode = new HomePartyUserListModel();
    }

    public void clearOldList() {
        oldList = new ArrayList<>();
    }

    public void queryRoomBlackList(int limit, long roomId, long endTime) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        //第一页
        params.put("endTime", String.valueOf(endTime));
        //最多展示1000条
        params.put("limit", String.valueOf(limit));
        params.put("roomId", String.valueOf(roomId));
        //1管理员管理 2黑名单管理 3房间在线列表
        params.put("type", "2");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");

        OkHttpManager.getInstance().postRequest(UriProvider.getRoomUserListUrl(), params,
                new OkHttpManager.MyCallBack<ServiceResult<List<ServerUserMemberInfo>>>() {
                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                        if (null != getMvpView()) {
                            getMvpView().onGetRoomBlackList(true, e.getMessage(), null);
                        }
                    }

                    @Override
                    public void onResponse(ServiceResult<List<ServerUserMemberInfo>> serviceResult) {
                        LogUtils.d(TAG, "getRoomOnLineUserList-onResponse-result:" + serviceResult);
                        if (null != serviceResult) {
                            long startTime = System.currentTimeMillis();
                            //这里可能要考虑将几个for循环合并，降低循环数据处理耗时
                            List<ChatRoomMember> managerList = new ArrayList<>();
                            if (serviceResult.isSuccess()) {
                                if (!ListUtils.isListEmpty(serviceResult.getData())) {
                                    //如果oldList.size>0，需要做去重处理，之后添加进oldList
                                    List<ServerUserMemberInfo> resultList = removeRepeatServerUserMemberInfo(oldList, serviceResult.getData());
                                    for (ServerUserMemberInfo serverUserMemberInfo : resultList) {
                                        if (serverUserMemberInfo.isBlacklisted()) {
                                            ChatRoomMember chatRoomMember = new ChatRoomMember();
                                            chatRoomMember.setRoomId(String.valueOf(serverUserMemberInfo.getRoomid()));
                                            chatRoomMember.setAccount(serverUserMemberInfo.getAccid());
                                            chatRoomMember.setMemberType(mHomePartyUserListMode.parseMemberTypeServer2Client(serverUserMemberInfo.getType()));
                                            chatRoomMember.setMemberLevel(serverUserMemberInfo.getLevel());
                                            chatRoomMember.setNick(NobleBusinessManager.getNobleRoomNick(
                                                    serverUserMemberInfo.getVipId(),
                                                    serverUserMemberInfo.isInvisible(),
                                                    serverUserMemberInfo.getVipDate(),
                                                    serverUserMemberInfo.getNick()));
                                            chatRoomMember.setAvatar(NobleBusinessManager.getNobleRoomAvatarUrl(
                                                    serverUserMemberInfo.getVipId(),
                                                    serverUserMemberInfo.isInvisible(),
                                                    serverUserMemberInfo.getVipDate(),
                                                    serverUserMemberInfo.getAvator()));
                                            Map<String, Object> extMap = mHomePartyUserListMode.parseMemberExtServer2Client(
                                                    serverUserMemberInfo.getExt(), serverUserMemberInfo.getVipId(), serverUserMemberInfo.getVipDate(),
                                                    serverUserMemberInfo.getIsInvisible() ? 1 : 0, serverUserMemberInfo.getInvisibleUid());
                                            chatRoomMember.setOnline(serverUserMemberInfo.isOnlineStat());
                                            chatRoomMember.setExtension(extMap);
                                            chatRoomMember.setInBlackList(serverUserMemberInfo.isBlacklisted());
                                            chatRoomMember.setMuted(serverUserMemberInfo.isMuted());
                                            chatRoomMember.setEnterTime(serverUserMemberInfo.getEnterTime());
                                            mHomePartyUserListMode.parseChatRoomMemberUpdateTime(extMap, chatRoomMember);
                                            chatRoomMember.setTempMuted(serverUserMemberInfo.isTempMuted());
                                            chatRoomMember.setTempMuteDuration(serverUserMemberInfo.getTempMuteTtl());
                                            managerList.add(chatRoomMember);
                                        }
                                    }
                                }
                            }
                            LogUtils.d(TAG, "getRoomOnLineUserList 解析接口返回数据，总耗时：" + (System.currentTimeMillis() - startTime));
                            if (null != getMvpView()) {
                                getMvpView().onGetRoomBlackList(true, serviceResult.getMessage(), managerList);
                            }
                        } else if (null != getMvpView()) {
                            getMvpView().onGetRoomBlackList(true, null == serviceResult ? null : serviceResult.getMessage(), null);
                        }
                    }
                });

    }


    private List<ServerUserMemberInfo> removeRepeatServerUserMemberInfo(List<ServerUserMemberInfo> oldList, List<ServerUserMemberInfo> newList) {
        //去重处理，避免服务器接口调用云信API时，拿不到ext的情况，导致time字段一直为0导致的数据重复

        long startTime = System.currentTimeMillis();
        if (ListUtils.isListEmpty(oldList)) {
            oldList.addAll(newList);
            LogUtils.d(TAG, "removeRepeatServerUserMemberInfo 数据去重处理，总耗时：" + (System.currentTimeMillis() - startTime));
            return newList;
        }
        List<ServerUserMemberInfo> removeList = new ArrayList<>();
        for (ServerUserMemberInfo newSUMInfo : newList) {
            int index = oldList.indexOf(newSUMInfo);
            if (-1 != index) {
                removeList.add(newSUMInfo);
            } else {
                oldList.add(newSUMInfo);
            }
        }
        for (ServerUserMemberInfo delSUMInfo : removeList) {
            newList.remove(delSUMInfo);
        }
        LogUtils.d(TAG, "removeRepeatServerUserMemberInfo 数据去重处理，总耗时：" + (System.currentTimeMillis() - startTime));
        return newList;
    }

    public void removeUserFromBlackList(long targetId, long roomId) {
        mRoomBaseModel.removeUserFromBlackList(targetId, roomId, new OkHttpManager.MyCallBack<ServiceResult<String>>() {

            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (getMvpView() != null) {
                    getMvpView().onRemoveUserFromBlackList(false, e.getMessage(),
                            String.valueOf(targetId));
                }
            }

            @Override
            public void onResponse(ServiceResult<String> result) {
                if (null == getMvpView()) {
                    return;
                }
                if (null != result && result.isSuccess()) {
                    getMvpView().onRemoveUserFromBlackList(true, result.getMessage(),
                            String.valueOf(targetId));
                } else {
                    getMvpView().onRemoveUserFromBlackList(false, null == result ?
                            null : result.getMessage(), String.valueOf(targetId));
                }
            }
        });
    }
}
