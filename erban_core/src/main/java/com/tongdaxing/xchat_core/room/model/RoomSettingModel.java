package com.tongdaxing.xchat_core.room.model;

import android.text.TextUtils;

import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;

import java.util.Map;

/**
 * <p> 房间设置 </p>
 *
 * @author jiahui
 * @date 2017/12/15
 */
public class RoomSettingModel extends BaseMvpModel {

    public RoomSettingModel() {

    }

    public void requestTagAll(String ticket, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("ticket", ticket);
        RoomInfo roomInfo = BaseRoomServiceScheduler.getCurrentRoomInfo();
        if (null != roomInfo) {
            requestParam.put("type", String.valueOf(roomInfo.getType()));
        }
        OkHttpManager.getInstance().postRequest(UriProvider.getRoomTagList(), requestParam, myCallBack);
    }

    /**
     * 更新房间设置信息
     *
     * @param title
     * @param desc
     * @param pwd
     * @param label 标签名字
     * @param tagId 标签id
     */
    public void updateRoomInfo(String title, String desc, String pwd, String label,
                               int tagId, long uid, String ticket, String backPic,
                               String backName, int giftEffectParams, int charmSwitch,
                               String roomNotice, String playInfo,
                               OkHttpManager.MyCallBack<ServiceResult<RoomInfo>> myCallBack) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        if (!TextUtils.isEmpty(title)) {
            requestParam.put("title", title);
        }
        if (!TextUtils.isEmpty(desc)) {
            requestParam.put("roomDesc", desc);
        }
        if (null != pwd) {
            requestParam.put("roomPwd", pwd);
        }
        if (!TextUtils.isEmpty(label)) {
            requestParam.put("roomTag", label);
        }

        requestParam.put("tagId", tagId + "");
        requestParam.put("uid", uid + "");
        requestParam.put("ticket", ticket);
        if (!TextUtils.isEmpty(backPic)) {
            requestParam.put("backPic", backPic);
        }
        if (!TextUtils.isEmpty(backName)) {
            requestParam.put("backName", backName);
        }
        if (!TextUtils.isEmpty(roomNotice)) {
            requestParam.put("roomNotice", roomNotice);
        }

        requestParam.put("giftEffectSwitch", giftEffectParams + "");
        requestParam.put("charmSwitch", charmSwitch + "");
        RoomInfo roomInfo = BaseRoomServiceScheduler.getCurrentRoomInfo();
        if (null != roomInfo) {
            requestParam.put("roomType", roomInfo.getType() + "");

            if (TextUtils.isEmpty(roomInfo.getPlayInfo()) || null == playInfo
                    || !playInfo.equals(roomInfo.getPlayInfo())) {
                requestParam.put("playInfo", null == playInfo ? "" : playInfo);
            }

            OkHttpManager.MyCallBack callBack = new OkHttpManager.MyCallBack<ServiceResult<RoomInfo>>() {
                @Override
                public void onError(Exception e) {
                    if (null != myCallBack) {
                        myCallBack.onError(e);
                    }
                }

                @Override
                public void onResponse(ServiceResult<RoomInfo> response) {
                    if (null != response && response.isSuccess()) {
                        BaseRoomServiceScheduler.setServerRoomInfo(response.getData());
                    }
                    if (null != myCallBack) {
                        myCallBack.onResponse(response);
                    }
                }
            };

            if (checkIsRoomOwner()) {
                OkHttpManager.getInstance().postRequest(UriProvider.updateRoomInfo(), requestParam, callBack);
            } else {
                requestParam.put("roomUid", roomInfo.getUid() + "");
                OkHttpManager.getInstance().postRequest(UriProvider.updateRoomInfoByAdimin(), requestParam, callBack);
            }
        }
    }

    public boolean checkIsRoomOwner() {
        RoomInfo roomInfo = BaseRoomServiceScheduler.getCurrentRoomInfo();
        boolean isRoomOwner = false;
        if (null == roomInfo) {
            return isRoomOwner;
        }
        if (roomInfo.getType() == RoomInfo.ROOMTYPE_HOME_PARTY) {
            isRoomOwner = AvRoomDataManager.get().isRoomOwner();
        } else {
            isRoomOwner = RoomDataManager.get().isRoomOwner();
        }
        return isRoomOwner;
    }


    /**
     * 更新公屏的开关状态
     *
     * @param publicChatSwitch
     * @param ticket
     * @param uid
     * @param callback
     */
    public void changeRoomPublicScreenState(final int publicChatSwitch, String ticket, final long uid, CallBack<String> callback) {
        if (BaseRoomServiceScheduler.getCurrentRoomInfo() == null) {
            return;
        }
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", uid + "");
        params.put("ticket", ticket);
        params.put("publicChatSwitch", publicChatSwitch + "");
        params.put("roomUid", String.valueOf(BaseRoomServiceScheduler.getCurrentRoomInfo().getUid()));
        params.put("roomType", String.valueOf(BaseRoomServiceScheduler.getCurrentRoomInfo().getType()));
        String shortUrl;
        if (checkIsRoomOwner()) {
            shortUrl = UriProvider.updateRoomInfo();
        } else {
            shortUrl = UriProvider.updateRoomInfoByAdimin();
        }
        OkHttpManager.getInstance().postRequest(shortUrl, params, new OkHttpManager.MyCallBack<Json>() {

            @Override
            public void onError(Exception e) {
                if (callback != null) {
                    callback.onFail(OkHttpManager.DEFAULT_CODE_ERROR, e.getMessage());
                }
            }

            @Override
            public void onResponse(Json response) {
                if (callback != null) {
                    if (response.num("code") == 200) {
                        callback.onSuccess(response.str("data"));
                    } else {
                        callback.onFail(response.num("code"), response.str("message"));
                    }
                }
            }
        });
    }
}
