package com.tongdaxing.xchat_core.room.model;

import android.os.Handler;
import android.text.TextUtils;
import android.util.SparseArray;

import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nimlib.sdk.AbortableFuture;
import com.netease.nimlib.sdk.NIMChatRoomSDK;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.chatroom.ChatRoomService;
import com.netease.nimlib.sdk.chatroom.constant.MemberQueryType;
import com.netease.nimlib.sdk.chatroom.constant.MemberType;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomData;
import com.netease.nimlib.sdk.chatroom.model.EnterChatRoomResultData;
import com.netease.nimlib.sdk.util.api.RequestResult;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.JavaUtil;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.erban.libcommon.utils.config.SpEvent;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.noble.NobleBusinessManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;

/**
 * <p> 房间网络数据操作 </p>
 *
 * @author jiahui
 * @date 2017/12/11
 */
public class AvRoomModel extends RoomBaseModel {

    public static final int PUBLIC_CHAT_ROOM_TYPE = 1;
    public static final String PUBLIC_CHAT_ROOM = "PUBLIC_CHAT_ROOM";
    private static final String ROOM_ID = "room_id";
    private final String TAG = AvRoomModel.class.getSimpleName();


    public AvRoomModel() {

    }

    /**
     * 进入云信聊天室
     *
     * @param roomId
     * @param retryCount 重试次数
     * @param isFromMic  是否来自一键连麦
     * @return
     */
    public Observable<EnterChatRoomResultData> enterRoom(final long roomId, final int retryCount, boolean isFromMic) {
        return enterRoom(roomId, retryCount, 0, isFromMic);
    }


    public Observable<EnterChatRoomResultData> enterRoom(final long roomId, final int retryCount, int type, boolean isFromMic) {
        final EnterChatRoomData enterChatRoomData = new EnterChatRoomData(String.valueOf(roomId));
        Map<String, Object> stringIntegerMap = new HashMap<>();
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (type == PUBLIC_CHAT_ROOM_TYPE) {
            stringIntegerMap.put(PUBLIC_CHAT_ROOM, PUBLIC_CHAT_ROOM);
        }
        if (userInfo != null) {
            stringIntegerMap.put(Constants.USER_EXPER_LEVEL, userInfo.getExperLevel());
            stringIntegerMap.put(Constants.USER_NICK_IN_ROOM, NobleBusinessManager.getNobleRoomNick(userInfo.getVipId(),
                    userInfo.getIsInvisible(), userInfo.getVipDate(), userInfo.getNick()));
            stringIntegerMap.put(Constants.USER_CAR, userInfo.getCarUrl());
            stringIntegerMap.put(Constants.USER_CAR_NAME, userInfo.getCarName());
            stringIntegerMap.put(Constants.ROOM_ID, roomId + "");
            stringIntegerMap.put(Constants.ROOM_FROM_MIC, isFromMic);
            stringIntegerMap.put(Constants.ALIAS_ICON_URL, userInfo.getAlias());
            stringIntegerMap.put(Constants.USER_NOBLE_MEDAL, userInfo.getVipMedal());
            stringIntegerMap.put(Constants.USER_MEDAL_ID, userInfo.getVipId());
            stringIntegerMap.put(Constants.USER_MEDAL_DATE, userInfo.getVipDate());
            stringIntegerMap.put(Constants.NOBLE_INVISIABLE_ENTER_ROOM, userInfo.getIsInvisible() ? 1 : 0);
            LogUtils.d(TAG, "enterRoom-alias:" + userInfo.getAlias());
            stringIntegerMap.put(Constants.ROOM_MEMBER_ID, userInfo.getUid());
            //少于3天都是新用户 -- 已废弃，最新业务修改为动态传递解析显示,保留兼容旧版本
            if (System.currentTimeMillis() - userInfo.getCreateTime() < (86400 * 3000)) {
                stringIntegerMap.put(Constants.IS_NEW_USER, "new");
                LogUtils.d(TAG, "enterRoom-time:" + (System.currentTimeMillis() - userInfo.getCreateTime()));
            }
        }

        enterChatRoomData.setNotifyExtension(stringIntegerMap);
        enterChatRoomData.setExtension(stringIntegerMap);
        ObservableOnSubscribe observableOnSubscribe = new ObservableOnSubscribe<EnterChatRoomResultData>() {
            @Override
            public void subscribe(ObservableEmitter<EnterChatRoomResultData> e) throws Exception {
                AbortableFuture<EnterChatRoomResultData> enterChatRoomEx =
                        NIMChatRoomSDK.getChatRoomService().enterChatRoomEx(enterChatRoomData, retryCount);
                RequestResult<EnterChatRoomResultData> requestResult = NIMClient.syncRequest(enterChatRoomEx);
                executeNIMClient("enterChatRoomEx", requestResult, e);
            }
        };

        return Observable.create(observableOnSubscribe)
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io());
    }


    /**
     * 离开聊天室（退出房间）
     */
    public void quiteRoom(String roomId) {
        LogUtil.d(TAG, "quiteRoom roomId:" + roomId);
        if (!TextUtils.isEmpty(roomId)) {
            LogUtil.d(TAG, "quiteRoom-->AvRoomDataManager.release");
            AvRoomDataManager.get().release();
            //云信的房间退出
            LogUtil.d(TAG, "quiteRoom-->exitChatRoom");
            NIMClient.getService(ChatRoomService.class).exitChatRoom(roomId);
        }
    }

    /**
     * 退出房间
     * 2.4.5后添加退出麦序的逻辑
     */
    public void exitRoom(final CallBack<String> callBack) {
        exitRoom(callBack, 200);
    }

    public void exitRoom(final CallBack<String> callBack, final int time) {
        final RoomInfo currentRoom = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoom == null) {
            if(null != callBack){
                callBack.onSuccess("成功退出");
            }
            return;
        }
        String account = CoreManager.getCore(IAuthCore.class).getCurrentUid() + "";
        boolean onMic = AvRoomDataManager.get().isOnMic(Long.valueOf(account));
        //如果在排麦队列又不在麦上则先移除队列在离开房间
        if (AvRoomDataManager.get().checkInMicInlist() && !onMic) {
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
            LogUtils.d("AvExitRoom", userInfo.getUid() + "     " + roomInfo.getRoomId());
            IMNetEaseManager.get().removeMicInList(userInfo.getUid() + "", roomInfo.getRoomId() + "", new RequestCallback() {
                @Override
                public void onSuccess(Object param) {
                    LogUtils.d("AvExitRoom", "onSuccess");
                    quitUserRoomDelay(time, callBack, currentRoom);
                }

                @Override
                public void onFailed(int code) {
                    LogUtils.d("AvExitRoom", "onFailed" + code);
                    quitUserRoomDelay(time, callBack, currentRoom);
                }

                @Override
                public void onException(Throwable exception) {
                    LogUtils.d("AvExitRoom", "onException");
                    quitUserRoomDelay(time, callBack, currentRoom);
                }
            });
            return;
        }


        //如果在麦上，退出之前自己下麦
        if (onMic) {
            //在麦上的要退出麦
//            LogUtils.d("nim_sdk", "chatRoomMemberExit     " + account);
            //延迟发送退出，
            IMNetEaseManager.get().downMicroPhoneBySdk(AvRoomDataManager.get().getMicPosition(Long.valueOf(account)), new CallBack<String>() {
                @Override
                public void onSuccess(String data) {
                    quitUserRoomDelay(time, callBack, currentRoom);
                }

                @Override
                public void onFail(int code, String error) {
                    quitUserRoomDelay(time, callBack, currentRoom);
                }
            });
            SparseArray<RoomQueueInfo> mMicQueueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;
            int size = mMicQueueMemberMap.size();
            for (int i = 0; i < size; i++) {
                RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(i);
                //房主的话不移除
                if (roomQueueInfo.mChatRoomMember != null && Objects.equals(roomQueueInfo.mChatRoomMember.getAccount(), account)) {
                    roomQueueInfo.mChatRoomMember = null;
                    IMNetEaseManager.get().noticeDownMic(String.valueOf(mMicQueueMemberMap.keyAt(i)), account);
                    break;
                }
            }
        } else {
            quitUserRoom(callBack, currentRoom);
            LogUtils.d(TAG, "exitRoom->quitUserRoom");
        }

    }

    private void quitUserRoomDelay(int time, final CallBack<String> callBack, final RoomInfo currentRoom) {
        if (time == 0) {
            quitUserRoom(callBack, currentRoom);
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    quitUserRoom(callBack, currentRoom);
                }
            }, time);
        }
    }

    private void quitUserRoom(CallBack<String> callBack, RoomInfo currentRoom) {
        String roomId = String.valueOf(currentRoom.getRoomId());
        LogUtils.d("AVRoomMsgQuit", roomId + "");
        quiteRoom(roomId);
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if (currentRoom.getUid() == uid) {
            if (currentRoom.getType() != RoomInfo.ROOMTYPE_HOME_PARTY) {
                quiteRoomForOurService(String.valueOf(uid), CoreManager.getCore(IAuthCore.class).getTicket(), currentRoom.getType());
            }
        }
        IMNetEaseManager.get().getChatRoomEventObservable().onNext(new RoomEvent().setEvent(RoomEvent.ROOM_EXIT));
        if (callBack != null) {
            callBack.onSuccess("成功");
        }

        LogUtils.d("quitUserRoom 离开房间id:" + uid);
        quitUserRoom(String.valueOf(uid), CoreManager.getCore(IAuthCore.class).getTicket(),
                currentRoom.getType());
    }

    /**
     * 通知服务端房间退出
     *
     * @param uId
     * @param ticket
     */
    public void quiteRoomForOurService(String uId, String ticket, int roomType) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", uId);
        requestParam.put("ticket", ticket);
        requestParam.put("roomType", String.valueOf(roomType));
        OkHttpManager.getInstance().postRequest(UriProvider.closeRoom(), requestParam, new OkHttpManager.MyCallBack<ServiceResult<String>>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                LogUtils.d("通知服务端退出房间失败");
            }

            @Override
            public void onResponse(ServiceResult<String> data) {
                if (data != null && data.isSuccess()) {
                    LogUtils.d("通知服务端退出房间成功:" + data);
                } else {
                    LogUtils.d("通知服务端退出房间失败:" + data.getErrorMessage() + "  " + "code:" + data.getCode());
                }
            }
        });
    }

    /**
     * 退出房间
     *
     * @param uId
     * @param ticket
     */
    public void quitUserRoom(String uId, String ticket, int roomType) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", uId);
        requestParam.put("ticket", ticket);
        requestParam.put("roomType", roomType + "");

        OkHttpManager.getInstance().postRequest(UriProvider.userRoomOut(), requestParam, new OkHttpManager.MyCallBack<ServiceResult<String>>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                LogUtils.d("quitUserRoom 通知服务端退出房间失败:" + e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<String> data) {
                if (data != null) {
                    if (data.isSuccess()) {
                        LogUtils.d("quitUserRoom 通知服务端退出房间成功:" + data);
                    } else {
                        LogUtils.d("quitUserRoom 通知服务端退出房间失败:" + data);
                    }
                }
            }
        });
    }


    /**
     * 获取活动信息
     */
    public void getActionDialog(int type, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        //1首页，2直播间
        requestParam.put("type", type + "");
        requestParam.put("ticket ", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.getRoomActivityList(), requestParam, myCallBack);
    }

    public void userRoomIn(String uid, long roomId, OkHttpManager.MyCallBack myCallBack, int roomType) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", uid);
        requestParam.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        requestParam.put("roomUid", roomId + "");
        requestParam.put("roomType", roomType + "");
        OkHttpManager.getInstance().postRequest(UriProvider.userRoomIn(), requestParam, myCallBack);
    }

    //---------------解决云信api接口返回数量限制，导致判断失败的场景----
    private int pageSize = 200;

    public void getNormalChatMember(String roomId, final long currentUid) {
        LogUtils.d(TAG, "getNormalChatMember-roomId:" + roomId + " currentUid:" + currentUid);
        RequestCallbackWrapper callbackWrapper = new RequestCallbackWrapper<List<ChatRoomMember>>() {
            @Override
            public void onResult(int i, List<ChatRoomMember> chatRoomMemberList, Throwable throwable) {
                if (ListUtils.isListEmpty(chatRoomMemberList)) {
                    return;
                }
                AvRoomDataManager.get().clearMembers();

                for (ChatRoomMember chatRoomMember : chatRoomMemberList) {
                    if (Objects.equals(chatRoomMember.getAccount(), String.valueOf(currentUid))) {
                        //自己
                        AvRoomDataManager.get().mOwnerMember = chatRoomMember;
                    }
                    if (chatRoomMember.getMemberType() == MemberType.ADMIN) {
                        AvRoomDataManager.get().addAdminMember(chatRoomMember);
                    }
                    if (chatRoomMember.getMemberType() == MemberType.CREATOR) {
                        AvRoomDataManager.get().mRoomCreateMember = chatRoomMember;
                    }
                }
                AvRoomDataManager.get().mRoomFixedMemberList.addAll(chatRoomMemberList);
                AvRoomDataManager.get().mRoomAllMemberList.addAll(chatRoomMemberList);
                LogUtils.d(TAG, "getNormalChatMember-onResult-进入房间获取固定成员成功,人数:" + chatRoomMemberList.size());
            }
        };
        //由于受到云信API的限制，会出现检索不到的情况,造成无法踢人，无法判断是否是管理员，是否在黑名单
//        NIMChatRoomSDK.getChatRoomService()
//                .fetchRoomMembers(roomId, MemberQueryType.NORMAL, 0, 500)
//                .setCallback(callbackWrapper);
        getAllRoomMembers(roomId, MemberQueryType.NORMAL, callbackWrapper);
    }

    /**
     * 根据类型获取房间所有成员 考虑外层改动小，进行如此封装
     *
     * @param roomId
     * @param memberQueryType
     * @param callback
     */
    public void getAllRoomMembers(final String roomId, final MemberQueryType memberQueryType,
                                  final RequestCallbackWrapper<List<ChatRoomMember>> callback) {

        //pageSize 云信支持的最大值为200
        NIMClient.getService(ChatRoomService.class)
                .fetchRoomMembers(roomId, memberQueryType, 0, pageSize).setCallback(new RequestCallbackWrapper<List<ChatRoomMember>>() {
            @Override
            public void onResult(int code, List<ChatRoomMember> result, Throwable exception) {
                if (code == 200) {
                    //判断是否还有下一页数据
                    if (result.size() == pageSize) {
                        ChatRoomMember last = result.get(pageSize - 1);
                        long nextTime = MemberQueryType.NORMAL == memberQueryType ? last.getUpdateTime() : last.getEnterTime();
                        getRoomMembersNext(roomId, nextTime, memberQueryType, callback, result);
                    } else {
                        callback.onResult(code, result, exception);
                    }
                } else {
                    callback.onResult(code, result, exception);
                }
            }
        });
    }

    /**
     * 获取分页数据
     *
     * @paramroomId
     * @paramtime
     * @parammemberQueryType
     * @paramcallback
     * @paramdata
     */
    private void getRoomMembersNext(final String roomId, long time,
                                    final MemberQueryType memberQueryType,
                                    final RequestCallbackWrapper<List<ChatRoomMember>> callback,
                                    final List<ChatRoomMember> data) {
        NIMClient.getService(ChatRoomService.class)
                .fetchRoomMembers(roomId, memberQueryType, time, pageSize)
                .setCallback(new RequestCallbackWrapper<List<ChatRoomMember>>() {
                    @Override
                    public void onResult(int code, List<ChatRoomMember> result, Throwable exception) {
                        if (code == 200) {
                            data.addAll(result);
                            //判断是否还有下一页数据
                            if (result.size() == pageSize) {
                                ChatRoomMember last = result.get(pageSize - 1);
                                long nextTime = MemberQueryType.NORMAL == memberQueryType ?
                                        last.getUpdateTime() : last.getEnterTime();
                                getRoomMembersNext(roomId, nextTime, memberQueryType, callback, data);
                            } else {
                                callback.onResult(code, data, exception);
                            }
                        } else {
                            //因为之前数据获取正常，当前页数据异常也当正常返回
                            callback.onResult(200, data, exception);
                        }
                    }
                });
    }

    //------------------------新旧房间，业务流程梳理---------------------------

    /**
     * 检查用户是否已被该房间踢出
     *
     * @param roomUid  房主UID
     * @param roomType 房间类型
     * @return
     */
    public boolean checkIsKick(long roomUid, int roomType) {
        int kickTime = CoreManager.getCore(VersionsCore.class).checkKick();
        if (kickTime < 1) {
            return false;
        }

        if (kickTime > 600) {
            kickTime = 600;
        }

        String kickInfo = (String) SpUtils.get(BasicConfig.INSTANCE.getAppContext(), SpEvent.onKickRoomInfo, "");
        Json json = Json.parse(kickInfo);
        String roomUidCache = json.str(SpEvent.roomUid);
        String roomTypeCache = json.str(SpEvent.roomType);
        String time = json.str(SpEvent.time);
        if (roomUidCache.equals(roomUid + "") && roomTypeCache.equals(String.valueOf(roomType))) {
            int i = kickTime * 1000;
            if (BasicConfig.isDebug && i > 10000) {
                i = 10000;
            }
            return System.currentTimeMillis() - JavaUtil.str2long(time) < i;
        }
        return false;
    }

    /**
     * 获取房间信息-请求服务器
     *
     * @param queryUid    房主UID
     * @param roomType
     * @param myCallBack1
     */
    public void requestRoomInfoFromService(String queryUid, int roomType,
                                           OkHttpManager.MyCallBack<ServiceResult<RoomInfo>> myCallBack1) {
        LogUtils.d(TAG, "requestRoomInfoFromService-queryUid:" + queryUid);
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        //要查询的房间房主UID
        requestParam.put("queryUid", queryUid + "");
        //对接IM后新增的房间类型字段
        requestParam.put("roomType", String.valueOf(roomType));
        requestParam.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        //当前用户UID
        requestParam.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        //进入房间的用户UID
        requestParam.put("visitorUid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        if (null != myCallBack1) {
            OkHttpManager.getInstance().getRequest(UriProvider.getRoomInfo(), requestParam, myCallBack1);
        }
    }

    /**
     * 获取声网动态key
     *
     * @param roomId
     * @param callBack1
     * @param callBack2 callBack1和callBack2任取其一
     */
    public void getRoomAgoraKey(long roomId, OkHttpManager.MyCallBack callBack1, HttpRequestCallBack callBack2) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("roomId", String.valueOf(roomId));
        if (null != callBack1) {
            OkHttpManager.getInstance().postRequest(UriProvider.getAgoraKeyUri(), params, callBack1);
        } else if (null != callBack2) {
            OkHttpManager.getInstance().postRequest(UriProvider.getAgoraKeyUri(), params, callBack2);
        }
    }

    public void closeRoomInfo(long uid, int roomType, OkHttpManager.MyCallBack callBack1) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomType", String.valueOf(roomType));
        OkHttpManager.getInstance().postRequest(UriProvider.closeRoom(), params, callBack1);
    }


    public void openRoom(final long uid, final int type, OkHttpManager.MyCallBack callBack1) {
        LogUtils.d(TAG, "openRoom-uid:" + uid + ", type:" + type);
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomType", String.valueOf(type));
        params.put("roomPwd", "");
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(uid);
        if (userInfo != null) {
            params.put("title", userInfo.getNick() + "的房间");
        }
        params.put("roomDesc", "");
        params.put("backPic", "");
        OkHttpManager.getInstance().postRequest(UriProvider.openRoom(), params, callBack1);
    }
}
