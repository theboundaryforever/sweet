package com.tongdaxing.xchat_core.room.view;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

/**
 * <p> </p>
 *
 * @author jiahui
 * @date 2017/12/19
 */
public interface IRoomMemberView extends IMvpBaseView {
    /**
     * 设置管理员成功
     *
     * @param chatRoomMember
     */
    void markManagerListSuccess(ChatRoomMember chatRoomMember);

    /**
     * 设置管理员失败
     *
     * @param code
     * @param error
     */
    void markManagerListFail(int code, String error);

    void onRemoveUserFromAdminList(boolean isSuccess, String message, String account);

}
