package com.tongdaxing.xchat_core.room.lotterybox;

import com.tongdaxing.erban.libcommon.coremanager.AbstractBaseCore;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;

import java.util.Map;

/**
 * Created by Administrator on 2018/4/12.
 */

public class LotteryBoxCoreImpl extends AbstractBaseCore implements ILotteryBoxCore {
    /**
     * @param type 1是一次，2是十次
     */
    @Override
    public void lotteryRequest(String type, OkHttpManager.MyCallBack<Json> jsonMyCallBack) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        IAuthCore core = CoreManager.getCore(IAuthCore.class);
        requestParam.put("uid", core.getCurrentUid() + "");
        requestParam.put("type", type);
        requestParam.put("ticket", core.getTicket());
        RoomInfo mCurrentRoomInfo = BaseRoomServiceScheduler.getCurrentRoomInfo();
        if (mCurrentRoomInfo != null) {
            requestParam.put("roomId", mCurrentRoomInfo.getRoomId() + "");
        }
        OkHttpManager.getInstance().postRequest(UriProvider.getGiftPurseDrawUrl(), requestParam, jsonMyCallBack);
    }
}
