package com.tongdaxing.xchat_core.room.bean;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * 文件描述：创建房间入口bean
 *
 * @auther：zwk
 * @data：2019/8/13
 */
public class CreateRoomBean extends RealmObject implements Serializable {
    private int type;
    private String name;
    private String url;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
