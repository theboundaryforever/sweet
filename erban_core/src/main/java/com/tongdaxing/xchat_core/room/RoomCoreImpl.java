package com.tongdaxing.xchat_core.room;

import android.os.Handler;

import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nimlib.sdk.chatroom.ChatRoomMessageBuilder;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.tongdaxing.erban.libcommon.coremanager.AbstractBaseCore;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.AccountInfo;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCore;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCoreClient;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.state.IPhoneCallStateClient;
import com.tongdaxing.xchat_core.im.state.PhoneCallStateCoreImpl;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomCoreClient;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.result.RoomConsumeInfoListResult;
import com.tongdaxing.xchat_core.result.RoomInfoResult;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiConsumer;

/**
 * Created by zhouxiangfeng on 2017/5/27.
 */

public class RoomCoreImpl extends AbstractBaseCore implements IRoomCore {

    private static final String TAG = "RoomCoreImpl";
    private static Handler handler = new Handler();
    private List<ChatRoomMessage> messages;
    private List<ChatRoomMessage> cacheMessages;

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            sendStatistics();
            handler.postDelayed(runnable, 60000);
        }
    };
    private boolean modifyMuteState;

    public RoomCoreImpl() {
        CoreManager.addClient(this);
        messages = new ArrayList<>();
    }

    private void sendStatistics() {
        RoomInfo cRoomInfo = BaseRoomServiceScheduler.getCurrentRoomInfo();
        if (cRoomInfo != null) {
            long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
            long roomUid = cRoomInfo.getUid();
            String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
            Map<String, String> params = CommonParamUtil.getDefaultParam();
            params.put("uid", String.valueOf(uid));
            params.put("roomUid", String.valueOf(roomUid));
            params.put("ticket", ticket);
            OkHttpManager.getInstance().postRequest(UriProvider.roomStatistics(), params, new OkHttpManager.MyCallBack<ServiceResult>() {
                @Override
                public void onError(Exception e) {
                    e.printStackTrace();

                }

                @Override
                public void onResponse(ServiceResult response) {
                }
            });
        }
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogin(AccountInfo accountInfo) {
        userRoomOut();
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogout() {
//        onRoomExited();
    }

    public void addMessages(ChatRoomMessage msg) {
        boolean needClear = false;

        if (messages == null) {
            messages = new ArrayList<>();
        }

        if (cacheMessages == null) {
            cacheMessages = new ArrayList<>();
        }

        if (messages != null && messages.size() >= 400) {
            messages.removeAll(cacheMessages);
            cacheMessages.clear();
            cacheMessages.addAll(messages);
            needClear = true;
        }

        messages.add(msg);

        if (cacheMessages.size() <= 200) {
            cacheMessages.add(msg);
        }
    }

    @CoreEvent(coreClientClass = IAVRoomCoreClient.class)
    public void onJoinAVRoom() {
        LogUtil.i(TAG, "onJoinAVRoom--->");
        RoomInfo cRoomInfo = BaseRoomServiceScheduler.getCurrentRoomInfo();
        notifyClients(com.tongdaxing.xchat_core.room.IRoomCoreClient.class,
                com.tongdaxing.xchat_core.room.IRoomCoreClient.METHOD_ON_ENTER, cRoomInfo);

        String content = "官方倡导绿色聊天，任何违法，低俗，暴力，敲诈等不良信息，官方将会采取封号处理。";
        ChatRoomMessage message = ChatRoomMessageBuilder.createTipMessage(content);
        message.setContent(content);
        addMessages(message);
        handler.removeMessages(0);
        handler.postDelayed(runnable, 60000);
        if (null != cRoomInfo) {
            userRoomIn(cRoomInfo.getUid());
        }
    }

    @CoreEvent(coreClientClass = IPhoneCallStateClient.class)
    public void onPhoneStateChanged(PhoneCallStateCoreImpl.PhoneCallStateEnum phoneCallStateEnum) {
        RoomInfo cRoomInfo = BaseRoomServiceScheduler.getCurrentRoomInfo();
        if (cRoomInfo != null) {
            if (phoneCallStateEnum != PhoneCallStateCoreImpl.PhoneCallStateEnum.IDLE) {
                boolean isAudience = CoreManager.getCore(IAVRoomCore.class).isAudienceRole();
                boolean isMute = CoreManager.getCore(IAVRoomCore.class).isMute();
                if (!isAudience && !isMute) {
                    CoreManager.getCore(IAVRoomCore.class).setMute(true);
                    modifyMuteState = true;
                } else {
                    modifyMuteState = false;
                }
            } else {
                if (modifyMuteState == true) {
                    CoreManager.getCore(IAVRoomCore.class).setMute(false);
                }
                modifyMuteState = false;
            }
        } else {
            modifyMuteState = false;
        }
    }

    @Override
    public void getRoomConsumeList(long roomUid, int roomType) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(roomUid));
        params.put("roomType", String.valueOf(roomType));
        OkHttpManager.getInstance().getRequest(UriProvider.getRoomConsumeList(), params, new OkHttpManager.MyCallBack<RoomConsumeInfoListResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(com.tongdaxing.xchat_core.room.IRoomCoreClient.class, com.tongdaxing.xchat_core.room.IRoomCoreClient.METHOD_ON_GET_ROOM_CONSUME_LIST_FAIL, e.getMessage());
            }

            @Override
            public void onResponse(RoomConsumeInfoListResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(com.tongdaxing.xchat_core.room.IRoomCoreClient.class, com.tongdaxing.xchat_core.room.IRoomCoreClient.METHOD_ON_GET_ROOM_CONSUME_LIST, response.getData());
                    } else {
                        notifyClients(com.tongdaxing.xchat_core.room.IRoomCoreClient.class, com.tongdaxing.xchat_core.room.IRoomCoreClient.METHOD_ON_GET_ROOM_CONSUME_LIST_FAIL, response.getMessage());
                    }
                }
            }
        });
    }

    @Override
    public void getUserRoom(long uid) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));

        OkHttpManager.getInstance().getRequest(UriProvider.getUserRoom(), params, new OkHttpManager.MyCallBack<RoomInfoResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(com.tongdaxing.xchat_core.room.IRoomCoreClient.class, com.tongdaxing.xchat_core.room.IRoomCoreClient.METHOD_ON_GET_USER_ROOM_FAIL, e.getMessage());
            }

            @Override
            public void onResponse(RoomInfoResult response) {
                if (response.isSuccess()) {
                    notifyClients(com.tongdaxing.xchat_core.room.IRoomCoreClient.class, com.tongdaxing.xchat_core.room.IRoomCoreClient.METHOD_ON_GET_USER_ROOM, response.getData());
                } else {
                    notifyClients(com.tongdaxing.xchat_core.room.IRoomCoreClient.class, com.tongdaxing.xchat_core.room.IRoomCoreClient.METHOD_ON_GET_USER_ROOM_FAIL, response.getMessage());
                }
            }
        });
    }

    @Override
    public void userRoomIn(long roomUid) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        params.put("uid", String.valueOf(uid));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomUid", String.valueOf(roomUid));
        RoomInfo cRoomInfo = BaseRoomServiceScheduler.getCurrentRoomInfo();
        if (null != cRoomInfo) {
            params.put("roomType", cRoomInfo.getType() + "");
        }

        OkHttpManager.getInstance().postRequest(UriProvider.userRoomIn(), params, new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(com.tongdaxing.xchat_core.room.IRoomCoreClient.class, com.tongdaxing.xchat_core.room.IRoomCoreClient.METHOD_ON_USER_ROOM_IN);
                    }
                }
            }
        });
    }

    @Override
    public void userRoomOut() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        params.put("uid", String.valueOf(uid));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        RoomInfo cRoomInfo = BaseRoomServiceScheduler.getCurrentRoomInfo();
        if (null != cRoomInfo) {
            params.put("roomType", cRoomInfo.getType() + "");
        }
        OkHttpManager.getInstance().postRequest(UriProvider.userRoomOut(), params, new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();

            }

            @Override
            public void onResponse(ServiceResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(com.tongdaxing.xchat_core.room.IRoomCoreClient.class, com.tongdaxing.xchat_core.room.IRoomCoreClient.METHOD_ON_USER_ROOM_OUT);
                    }
                }
            }
        });
    }

    @Override
    public Disposable sendMessage(final ChatRoomMessage message) {

        // 发送消息。如果需要关心发送结果，可设置回调函数。发送完成时，会收到回调。如果失败，会有具体的错误码。
        return IMNetEaseManager.get().sendChatRoomMessage(message, false).subscribe(new BiConsumer<ChatRoomMessage, Throwable>() {
            @Override
            public void accept(ChatRoomMessage chatRoomMessage, Throwable throwable) throws Exception {
                if (chatRoomMessage != null) {
                    notifyClients(IMRoomCoreClient.class, IRoomCoreClient.METHOD_ON_SEND_ROOM_MESSAGE_SUCCESS, message);
                    if (message.getMsgType() == MsgTypeEnum.custom) {
                        CustomAttachment attachment = (CustomAttachment) message.getAttachment();
                        if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT ||
                                attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_ROOM_TIP ||
                                attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT ||
                                attachment.getFirst() == CustomAttachment.CUSTOM_MSG_LOTTERY_BOX
                                || attachment.getFirst() == CustomAttachment.CUSTOM_MSG_MIC_IN_LIST
                                || attachment.getFirst() == CustomAttachment.CUSTOM_MSG_ROOM_LOVER_UP_MIC
                                || (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_BOSON_FIRST
                                    && attachment.getSecond() == CustomAttachment.CUSTOM_MSG_BOSON_UP_MACRO)) {
                            IMNetEaseManager.get().addMessagesImmediately(message);
                        }
                    }
                } else {
                    LogUtil.e(TAG, throwable.getMessage(), throwable);
                }
            }
        });
    }
}
