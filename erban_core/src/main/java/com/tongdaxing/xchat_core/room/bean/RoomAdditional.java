package com.tongdaxing.xchat_core.room.bean;

import java.util.List;

import static io.agora.rtc.Constants.AUDIO_PROFILE_MUSIC_STANDARD;
import static io.agora.rtc.Constants.AUDIO_SCENARIO_GAME_STREAMING;

/**
 * 文件描述：房间的额外扩展信息 -- 用于后台管理的动态参数 用户不可以修改的
 *
 * @auther：zwk
 * @data：2019/6/26
 */
public class RoomAdditional {
    /**
     * 0：默认的音频应用场景。  因为接口没有值时 -1会变成0 所以暂时不提供默认的音频，永远以1-5为范围
     * * 1：娱乐应用，需要频繁上下麦的场景。
     * * 2：教育应用，流畅度和稳定性优先。
     * * 3：游戏直播应用，需要外放游戏音效也直播出去的场景。
     * * 4：秀场应用，音质优先和更好的专业外设支持。
     * * 5：游戏开黑。
     */
    private int audioScenario = AUDIO_SCENARIO_GAME_STREAMING;
    /**
     * *  默认设置。通信模式下为 1，直播模式下为 2。 接口 1-3 对应 3-5  默认0
     * * 1：指定 32 KHz 采样率，语音编码, 单声道，编码码率约 18 Kbps。
     * * 2：指定 48 KHz采样率，音乐编码, 单声道，编码码率约 48 Kbps。
     * * 3：指定 48 KHz采样率，音乐编码, 双声道，编码码率约 56 Kbps。
     * * 4：指定 48 KHz 采样率，音乐编码, 单声道，编码码率约 128 Kbps。
     * * 5：指定 48 KHz采样率，音乐编码, 双声道，编码码率约 192 Kbps。
     */
    private int audioLevel = AUDIO_PROFILE_MUSIC_STANDARD;
    private boolean bigWheelSwitch;//房间大转盘开关
    private boolean redPacketSwitch;//房间红包开关
    private boolean lotteryBoxOption;//房间砸蛋宝箱开关
    private long hotScore;//房间热度排行
    private long hotRank;//房间热度值
    private int playState = 0;//单人音频房间-房主-开播状态（0未开播 1已开播）
    private long playTime = 0L;//单人音频房间-房主-开播时长（秒）
    private int admireCount;//为主播点赞剩余次数
    private long admireTime;//下一次点赞时长（秒）
    private int detonatingState = -1;// 引爆进度,-1为没权限
    private long detonatingUid;// 引爆人id
    private String detonatingNick;//引爆人昵称
    private String detonatingAvatar;//引爆人头像
    private int detonatingGifts;// 引爆礼物id
    private String detonatingGiftName;
    private String detonatingGiftUrl;

    private long detonatingTime;// 引爆时间
    private long detonatingDuration;// 引爆倒计时
    // 特有礼物
    private List<Integer> exclusiveGift;

    /**
     * 多人房，主播召集令已召集人数
     */
    private int conveneUserCount = 0;
    private int conveneCount;
    /**
     * 召集令状态，1表示有有效的房主发布的召集令
     */
    private int conveneState;

    private int isAdmire;//是否有点赞（1有 0没有）

    private int isConvene;//是否有召集令（1有 0没有）

    public int getConveneUserCount() {
        return conveneUserCount;
    }

    public void setConveneUserCount(int conveneUserCount) {
        this.conveneUserCount = conveneUserCount;
    }

    public int getConveneState() {
        return conveneState;
    }

    public void setConveneState(int conveneState) {
        this.conveneState = conveneState;
    }

    public int getConveneCount() {
        return conveneCount;
    }

    public void setConveneCount(int conveneCount) {
        this.conveneCount = conveneCount;
    }


    public int getPlayState() {
        return playState;
    }

    public void setPlayState(int mPlayState) {
        this.playState = mPlayState;
    }

    public long getPlayTime() {
        return playTime;
    }

    public void setPlayTime(long mPlayTime) {
        this.playTime = mPlayTime;
    }

    public int getAdmireCount() {
        return admireCount;
    }

    public void setAdmireCount(int mAdmireCount) {
        this.admireCount = mAdmireCount;
    }

    public long getAdmireTime() {
        return admireTime;
    }

    public void setAdmireTime(long admireTime) {
        this.admireTime = admireTime;
    }

    public void setmNextAdmireTime(long mNextAdmireTime) {
        this.admireTime = mNextAdmireTime;
    }

    public boolean isBigWheelSwitch() {
        return bigWheelSwitch;
    }

    public void setBigWheelSwitch(boolean bigWheelSwitch) {
        this.bigWheelSwitch = bigWheelSwitch;
    }

    public boolean isRedPacketSwitch() {
        return redPacketSwitch;
    }

    public void setRedPacketSwitch(boolean redPacketSwitch) {
        this.redPacketSwitch = redPacketSwitch;
    }

    public boolean isLotteryBoxOption() {
        return lotteryBoxOption;
    }

    public void setLotteryBoxOption(boolean lottery_box_option) {
        this.lotteryBoxOption = lottery_box_option;
    }

    public long getHotScore() {
        return hotScore;
    }

    public void setHotScore(long hotScore) {
        this.hotScore = hotScore;
    }

    public long getHotRank() {
        return hotRank;
    }

    public void setHotRank(long hotRank) {
        this.hotRank = hotRank;
    }

    public int getDetonatingState() {
        return detonatingState;
    }

    public void setDetonatingState(int detonatingState) {
        this.detonatingState = detonatingState;
    }

    public long getDetonatingUid() {
        return detonatingUid;
    }

    public void setDetonatingUid(long detonatingUid) {
        this.detonatingUid = detonatingUid;
    }

    public String getDetonatingNick() {
        return detonatingNick;
    }

    public void setDetonatingNick(String detonatingNick) {
        this.detonatingNick = detonatingNick;
    }

    public String getDetonatingAvatar() {
        return detonatingAvatar;
    }

    public void setDetonatingAvatar(String detonatingAvatar) {
        this.detonatingAvatar = detonatingAvatar;
    }

    public int getDetonatingGifts() {
        return detonatingGifts;
    }

    public void setDetonatingGifts(int detonatingGifts) {
        this.detonatingGifts = detonatingGifts;
    }

    public long getDetonatingTime() {
        return detonatingTime;
    }

    public void setDetonatingTime(long detonatingTime) {
        this.detonatingTime = detonatingTime;
    }

    public long getDetonatingDuration() {
        return detonatingDuration;
    }

    public void setDetonatingDuration(long detonatingDuration) {
        this.detonatingDuration = detonatingDuration;
    }

    public String getDetonatingGiftName() {
        return detonatingGiftName;
    }

    public void setDetonatingGiftName(String detonatingGiftName) {
        this.detonatingGiftName = detonatingGiftName;
    }

    public String getDetonatingGiftUrl() {
        return detonatingGiftUrl;
    }

    public void setDetonatingGiftUrl(String detonatingGiftUrl) {
        this.detonatingGiftUrl = detonatingGiftUrl;
    }

    public int getAudioScenario() {
        return audioScenario;
    }

    public void setAudioScenario(int audioScenario) {
        this.audioScenario = audioScenario;
    }

    public int getAudioLevel() {
        return audioLevel;
    }

    public void setAudioLevel(int audioLevel) {
        this.audioLevel = audioLevel;
    }

    public List<Integer> getExclusiveGift() {
        return exclusiveGift;
    }

    public void setExclusiveGift(List<Integer> exclusiveGift) {
        this.exclusiveGift = exclusiveGift;
    }

    public int getIsAdmire() {
        return isAdmire;
    }

    public void setIsAdmire(int isAdmire) {
        this.isAdmire = isAdmire;
    }

    public int getIsConvene() {
        return isConvene;
    }

    public void setIsConvene(int isConvene) {
        this.isConvene = isConvene;
    }
}
