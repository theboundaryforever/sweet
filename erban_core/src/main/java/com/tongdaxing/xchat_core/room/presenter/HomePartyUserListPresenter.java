package com.tongdaxing.xchat_core.room.presenter;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.room.bean.OnlineChatMember;
import com.tongdaxing.xchat_core.room.bean.ServerUserMemberInfo;
import com.tongdaxing.xchat_core.room.model.HomePartyUserListModel;
import com.tongdaxing.xchat_core.room.view.IHomePartyUserListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * <p>  </p>
 *
 * @author jiahui
 * @date 2017/12/8
 */
public class HomePartyUserListPresenter extends AbstractMvpPresenter<IHomePartyUserListView> {

    private final String TAG = HomePartyUserListPresenter.class.getSimpleName();

    private final HomePartyUserListModel mHomePartyUserListMode;

    public HomePartyUserListPresenter() {
        mHomePartyUserListMode = new HomePartyUserListModel();
    }

    /**
     * 查询房间在线用户列表
     *
     * @param endTime 单位毫秒，按时间倒序最后一个成员的时间戳,0表示系统当前时间
     * @param limit   返回条数，<=100
     * @param roomId  房间ID
     * @param page    页数
     * @param oldList    页数
     */
    public void getRoomOnLineUserList(final int page, long endTime, int limit, long roomId,
                                      final List<OnlineChatMember> oldList) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();

        params.put("endTime", String.valueOf(endTime));//第一页
        if (limit > 100) {
            limit = 100;
        }
        params.put("limit", String.valueOf(limit));//最多展示1000条
        params.put("roomId", String.valueOf(roomId));
        //1管理员管理 2黑名单管理 3房间在线列表
        params.put("type", "3");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        //接口返回的数据，字段可能些许对不上，这里需要关注我们用到的字段，
        // 该接口对应云信接口文档地址为：
        //https://dev.yunxin.163.com/docs/product/IM%E5%8D%B3%E6%97%B6%E9%80%9A%E8%AE%AF/
        // %E6%9C%8D%E5%8A%A1%E7%AB%AFAPI%E6%96%87%E6%A1%A3/%E8%81%8A%E5%A4%A9%E5%AE%A4?#
        // %E5%88%86%E9%A1%B5%E8%8E%B7%E5%8F%96%E6%88%90%E5%91%98%E5%88%97%E8%A1%A8

        //1.需要本地做转换参数名处理
        //2.第一页，不用做去重处理，第二页之后，就需要做去重处理
        //3.判断是否在麦上等的旧有逻辑

        OkHttpManager.getInstance().postRequest(UriProvider.getRoomUserListUrl(), params,
                new OkHttpManager.MyCallBack<ServiceResult<List<ServerUserMemberInfo>>>() {
                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                        if (null != getMvpView()) {
                            getMvpView().onGetOnLineUserList(false, e.getMessage(), page, null);
                        }
                    }

                    @Override
                    public void onResponse(ServiceResult<List<ServerUserMemberInfo>> serviceResult) {
                        LogUtils.d(TAG, "getRoomOnLineUserList-onResponse-result:" + serviceResult);
                        if (null != serviceResult && serviceResult.isSuccess()) {
                            long startTime = System.currentTimeMillis();
                            List<OnlineChatMember> onlineChatMemberList = new ArrayList<>();
                            //TODO 这里可能要考虑将几个for循环合并，降低循环数据处理耗时
                            if (page == Constants.PAGE_START) {
                                onlineChatMemberList = mHomePartyUserListMode.dealRoomChatMemberStatus(
                                        mHomePartyUserListMode.parseServerMemberInfo2OnLineChatMember(serviceResult.getData()));
                            } else {
                                if (!ListUtils.isListEmpty(serviceResult.getData())) {
                                    LogUtils.d(TAG, "getPageMembers-queryGuestList 第" + page + "页在线人数:"
                                            + serviceResult.getData().size());
                                    onlineChatMemberList = mHomePartyUserListMode.getChatRoomMemberList(
                                            mHomePartyUserListMode.parseServerMemberInfo2ChatRoomMember(serviceResult.getData()),
                                            oldList, page);
                                }
                            }
                            LogUtils.d(TAG, "getRoomOnLineUserList 解析接口返回数据，总耗时：" + (System.currentTimeMillis() - startTime));
                            if (null != getMvpView()) {
                                getMvpView().onGetOnLineUserList(true, serviceResult.getMessage(), page, onlineChatMemberList);
                            }
                        } else if (null != getMvpView()) {
                            getMvpView().onGetOnLineUserList(false, serviceResult == null ?
                                    null : serviceResult.getMessage(), page, null);
                        }


//                if(result.isSuccess()){
//                    List<OnlineChatMember> onlineChatMemberList = mHomePartyUserListMode.dealRoomChatMemberStatus(result.getData());
//                    if (null != getMvpView()) {
//                        getMvpView().onGetOnLineUserList(result.isSuccess(),result.getMessage(),page,onlineChatMemberList);
//                    }
//                }

                    }
                });
    }

    /**
     * 成员进来刷新在线列表
     *
     * @param account
     * @param onlineChatMembers
     */
    public Disposable onMemberInRefreshData(String account, List<OnlineChatMember> onlineChatMembers, final int page) {
        return mHomePartyUserListMode.onMemberInRefreshData(account, page, onlineChatMembers)
//                .compose(this.<List<OnlineChatMember>>bindUntilEvent(PresenterEvent.DESTROY))
                .subscribe(new Consumer<List<OnlineChatMember>>() {
                    @Override
                    public void accept(List<OnlineChatMember> onlineChatMembers) throws Exception {
                        if (getMvpView() != null) {
                            getMvpView().onRequestChatMemberByPageSuccess(onlineChatMembers, page);
                        }
                    }
                });

    }

    public Disposable onMemberDownUpMic(String account, boolean isUpMic, List<OnlineChatMember> dataList,
                                        final int page) {
        return mHomePartyUserListMode.onMemberDownUpMic(account, isUpMic, dataList)
                .subscribe(new Consumer<List<OnlineChatMember>>() {
                    @Override
                    public void accept(List<OnlineChatMember> onlineChatMembers) throws Exception {
                        if (getMvpView() != null) {
                            getMvpView().onRequestChatMemberByPageSuccess(onlineChatMembers, page);
                        }
                    }
                });
    }


    public Disposable onUpdateMemberManager(String account, List<OnlineChatMember> dataList,
                                            boolean isRemoveManager, final int page) {
        return mHomePartyUserListMode.onUpdateMemberManager(account, isRemoveManager, dataList)
                .subscribe(new Consumer<List<OnlineChatMember>>() {
                    @Override
                    public void accept(List<OnlineChatMember> onlineChatMembers) throws Exception {
                        if (getMvpView() != null) {
                            getMvpView().onRequestChatMemberByPageSuccess(onlineChatMembers, page);
                        }
                    }
                });
    }
}
