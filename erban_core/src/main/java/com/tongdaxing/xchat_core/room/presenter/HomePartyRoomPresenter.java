package com.tongdaxing.xchat_core.room.presenter;

import android.text.TextUtils;
import android.util.SparseArray;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.util.Entry;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RoomMicInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCoreClient;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomModel;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RtcEngineManager;
import com.tongdaxing.xchat_core.result.RoomConsumeInfoListResult;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.model.AvRoomModel;
import com.tongdaxing.xchat_core.room.model.HomePartyModel;
import com.tongdaxing.xchat_core.room.view.IHomePartyRoomView;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiConsumer;
import io.reactivex.functions.Consumer;

import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_LOCK_MIC;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_LOCK_MIC_ALL;

/**
 * <p>  </p>
 *
 * @author jiahui
 * @date 2017/12/8
 */
public class HomePartyRoomPresenter extends AbstractMvpPresenter<IHomePartyRoomView> {

    private final String TAG = HomePartyRoomPresenter.class.getSimpleName();
    private final HomePartyModel mHomePartyMode;
    private AvRoomModel mAvRoomModel;
    /**
     * 判断所坑服务端是否响应回来了
     */
    private boolean mIsLockMicPosResultSuccess = true;
    private boolean mIsUnLockMicPosResultSuccess = true;

    public HomePartyRoomPresenter() {
        mAvRoomModel = new AvRoomModel();
        mHomePartyMode = new HomePartyModel();
    }

    /**
     * 麦坑点击处理，麦上没人的时候
     *
     * @param micPosition    麦序位置
     * @param chatRoomMember 坑上的用户
     */
    public void microPhonePositionClick(final int micPosition, ChatRoomMember chatRoomMember) {
        final RoomInfo currentRoom = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoom == null) {
            return;
        }
        final String currentUid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        if (AvRoomDataManager.get().isRoomOwner(currentUid)) {
            onOwnerUpMicroClick(micPosition, currentRoom.getUid(), true);
        } else if (AvRoomDataManager.get().isRoomAdmin(currentUid)) {
            onOwnerUpMicroClick(micPosition, currentRoom.getUid(), false);
        } else {
            upMicroPhone(micPosition, currentUid, false);
        }
    }

    private void onOwnerUpMicroClick(final int micPosition, final long currentUid, boolean isOwner) {
        RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoByMicPosition(micPosition);
        if (roomQueueInfo == null) {
            return;
        }
        if (getMvpView() != null) {
            getMvpView().showOwnerClickDialog(roomQueueInfo.mRoomMicInfo, micPosition, currentUid, isOwner);
        }
    }

    /**
     * 排麦加入队列
     */
    public void addMicInList() {
        if (AvRoomDataManager.get().isOwnerOnMic()) {
            return;
        }

        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        int index = AvRoomDataManager.get().checkHasEmpteyMic();
        if (index == AvRoomDataManager.MIC_FULL) {
            RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
            if (null != roomInfo) {
                mHomePartyMode.updateQueueEx((int) userInfo.getUid(), roomInfo.getRoomId() + "", null, userInfo, 1);
            }
        } else {
            upMicroPhone(index, userInfo.getUid() + "", false);
            CoreManager.notifyClients(IAVRoomCoreClient.class, IAVRoomCoreClient.micInListDismiss, "");
        }


    }


    public void lockMicroPhone(final int micPosition, final long currentUid) {
        if (!mIsLockMicPosResultSuccess) {
            return;
        }
        mIsLockMicPosResultSuccess = false;

        OkHttpManager.MyCallBack myCallBack = new OkHttpManager.MyCallBack<ServiceResult<String>>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                LogUtils.d("用户%1$s锁坑失败: %2$s", String.valueOf(currentUid), e);
                mIsLockMicPosResultSuccess = true;
            }

            @Override
            public void onResponse(ServiceResult<String> data) {
                if (null != data && data.isSuccess()) {//解麦成功
                    LogUtils.d(TAG, "用户" + currentUid + "锁坑成功:" + data);
                    IMNetEaseManager.get().systemNotificationBySdk(-1,
                            CUSTOM_MSG_HEADER_TYPE_QUEUE_LOCK_MIC, micPosition,
                            0, 0, false, null);
                    mIsLockMicPosResultSuccess = true;
                } else {//解麦失败
                    if (null != data) {
                        LogUtils.d(TAG, "用户" + currentUid + "锁坑失败:" + data.getError());
                    }
                    mIsLockMicPosResultSuccess = true;
                }
            }
        };

        mHomePartyMode.lockMicroPhone(micPosition, String.valueOf(currentUid),
                CoreManager.getCore(IAuthCore.class).getTicket(), RoomInfo.ROOMTYPE_HOME_PARTY, myCallBack);
    }

    /**
     * 坑位释放锁
     */
    public void unLockMicroPhone(final int micPosition) {
        if (!mIsUnLockMicPosResultSuccess) {
            return;
        }
        mIsUnLockMicPosResultSuccess = false;
        RoomInfo currentRoom = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoom == null) {
            return;
        }
        final long roomUid = currentRoom.getUid();
        final String currentUid = String.valueOf(roomUid);

        if (AvRoomDataManager.get().isRoomAdmin() || AvRoomDataManager.get().isRoomOwner(currentUid)) {
            OkHttpManager.MyCallBack myCallBack = new OkHttpManager.MyCallBack<ServiceResult<String>>() {
                @Override
                public void onError(Exception e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(ServiceResult<String> data) {
                    if (null != data && data.isSuccess()) {//解麦成功
                        mIsUnLockMicPosResultSuccess = true;
                        IMNetEaseManager.get().systemNotificationBySdk(-1,
                                CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_UNLOCK_MIC, micPosition,
                                0, 0, false, null);
                    } else {//解麦失败
                        mIsUnLockMicPosResultSuccess = true;
                    }
                }
            };
            mHomePartyMode.unLockMicroPhone(micPosition, currentUid, CoreManager.getCore(IAuthCore.class).getTicket(),
                    currentRoom.getType(), myCallBack);
        }
    }

    /**
     * 全部坑位加锁
     */
    public void lockAllMicroPhone(final long currentUid) {
        if (!mIsLockMicPosResultSuccess) {
            return;
        }
        mIsLockMicPosResultSuccess = false;

        OkHttpManager.MyCallBack myCallBack = new OkHttpManager.MyCallBack<ServiceResult<String>>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                LogUtils.d("用户%1$s锁坑失败: %2$s", String.valueOf(currentUid), e);
                mIsLockMicPosResultSuccess = true;
            }

            @Override
            public void onResponse(ServiceResult<String> data) {
                if (null != data && data.isSuccess()) {//解麦成功
                    LogUtils.d(TAG, "用户" + currentUid + "锁坑成功:" + data);
                    IMNetEaseManager.get().systemNotificationBySdk(-1,
                            CUSTOM_MSG_HEADER_TYPE_QUEUE_LOCK_MIC_ALL, -1,
                            0, 0, false, null);
                    mIsLockMicPosResultSuccess = true;
                } else {//解麦失败
                    if (null != data) {
                        LogUtils.d(TAG, "用户" + currentUid + "锁坑失败:" + data.getError());
                    }
                    mIsLockMicPosResultSuccess = true;
                }
            }
        };

        mHomePartyMode.locAllkMicroPhone(String.valueOf(currentUid),
                CoreManager.getCore(IAuthCore.class).getTicket(), RoomInfo.ROOMTYPE_HOME_PARTY, myCallBack);
    }

    /**
     * 全部坑位释放锁
     */
    public void unLockAllMicroPhone() {
        if (!mIsUnLockMicPosResultSuccess) {
            return;
        }
        mIsUnLockMicPosResultSuccess = false;
        RoomInfo currentRoom = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoom == null) {
            return;
        }
        final long roomUid = currentRoom.getUid();
        final String currentUid = String.valueOf(roomUid);

        if (AvRoomDataManager.get().isRoomAdmin() || AvRoomDataManager.get().isRoomOwner(currentUid)) {
            OkHttpManager.MyCallBack myCallBack = new OkHttpManager.MyCallBack<ServiceResult<String>>() {
                @Override
                public void onError(Exception e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(ServiceResult<String> data) {
                    if (null != data && data.isSuccess()) {//解麦成功
                        mIsUnLockMicPosResultSuccess = true;
                        IMNetEaseManager.get().systemNotificationBySdk(-1,
                                CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_UNLOCK_MIC_ALL, -1,
                                0, 0, false, null);
                    } else {//解麦失败
                        mIsUnLockMicPosResultSuccess = true;
                    }
                }
            };
            mHomePartyMode.unLockcAllMicroPhone(currentUid, CoreManager.getCore(IAuthCore.class).getTicket(),
                    currentRoom.getType(), myCallBack);
        }
    }

    /**
     * 下麦
     *
     * @param micPosition
     * @param isKick      是否是主动的
     */
    public void downMicroPhone(int micPosition, final boolean isKick) {
        final RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null) {
            return;
        }
        final String currentUid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        LogUtils.d(TAG, "downMicroPhone currentUid:" + currentUid);
        mHomePartyMode.downMicroPhone(micPosition, new CallBack<String>() {
            @Override
            public void onSuccess(String data) {
                LogUtils.d(TAG, "downMicroPhone-onSuccess data:" + data);
                if (!isKick) {
                    //被踢了
                    if (getMvpView() != null) {
                        getMvpView().kickDownMicroPhoneSuccess();
                    }
                }
            }

            @Override
            public void onFail(int code, String error) {
                LogUtils.d(TAG, "downMicroPhone-onFail code:" + code + " error:" + error);
            }
        });
    }


    public void upMicroPhone(final int micPosition, final String uId, boolean isInviteUpMic) {
        upMicroPhone(micPosition, uId, isInviteUpMic, false);
    }

    /**
     * 上麦
     *
     * @param micPosition
     * @param uId
     * @param isInviteUpMic 是否是主动的，true：主动
     */
    public void upMicroPhone(final int micPosition, final String uId, boolean isInviteUpMic, boolean needCloseMic) {
        final RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null) {
            return;
        }
        LogUtils.d(TAG, "upMicroPhone micPosition:" + micPosition + " uId:" + uId
                + " isInviteUpMic:" + isInviteUpMic + " needCloseMic:" + needCloseMic);
        mHomePartyMode.upMicroPhone(micPosition, uId, String.valueOf(roomInfo.getRoomId()),
                isInviteUpMic, new CallBack<String>() {
                    @Override
                    public void onSuccess(String data) {
                        if (micPosition == -1) {
                            if (getMvpView() != null) {
                                getMvpView().notifyRefresh();
                            }
                        }
                        LogUtils.d(TAG, "upMicroPhone-onSuccess");
                    }

                    @Override
                    public void onFail(int code, String error) {
                        LogUtils.d(TAG, "upMicroPhone-onFail code:" + code + " error:" + error);
                    }
                }, needCloseMic);
    }

    /**
     * 邀请用户上麦
     *
     * @param micUid   要邀请的用户id
     * @param position 坑位
     */
    public Disposable inviteMicroPhone(final long micUid, final int position) {
        if (AvRoomDataManager.get().isOnMic(micUid)) {
            return null;
        }
        //如果点击的就是自己,那这里就是自己上麦
        if (AvRoomDataManager.get().isOwner(micUid)) {
            upMicroPhone(position, String.valueOf(micUid), true);
            return null;
        }
        return mHomePartyMode.inviteMicroPhone(micUid, position)
                .subscribe(new BiConsumer<ChatRoomMessage, Throwable>() {
                    @Override
                    public void accept(ChatRoomMessage chatRoomMessage,
                                       Throwable throwable) throws Exception {
                        if (throwable != null) {
                            LogUtils.d(TAG, "inviteMicroPhone-邀请用户" + micUid + "上麦失败!!!");
                        } else {
                            IMNetEaseManager.get().addMessagesImmediately(chatRoomMessage);
                            LogUtils.d(TAG, "inviteMicroPhone-邀请用户" + micUid + "上麦成功!!!");
                        }

                    }
                });
    }

    /**
     * 头像点击，人在麦上
     *
     * @param micPosition
     */
    public void avatarClick(int micPosition) {
        LogUtils.d("avatarClick", "micPosition:" + micPosition);
        final RoomInfo currentRoom = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoom == null) {
            return;
        }
        // 判断在不在麦上
        RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoByMicPosition(micPosition);
        if (roomQueueInfo == null) {
            return;
        }
        // 麦上的人员信息,麦上的坑位信息
        boolean pair = AvRoomDataManager.get().isPair();
        ChatRoomMember chatRoomMember = roomQueueInfo.mChatRoomMember;
        RoomMicInfo roomMicInfo = roomQueueInfo.mRoomMicInfo;
        if (chatRoomMember == null && micPosition == -1) {
            chatRoomMember = new ChatRoomMember();
            RoomInfo info = AvRoomDataManager.get().mCurrentRoomInfo;
            chatRoomMember.setNick("房主");
            chatRoomMember.setAvatar("");
            chatRoomMember.setAccount(info.getUid() + "");
        }
        if (chatRoomMember == null || roomMicInfo == null) {
            return;
        }
        String currentUid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        boolean isMySelf = Objects.equals(currentUid, chatRoomMember.getAccount());
        boolean isTargetRoomAdmin = AvRoomDataManager.get().isRoomAdmin(chatRoomMember.getAccount());
        boolean isTargetRoomOwner = AvRoomDataManager.get().isRoomOwner(chatRoomMember.getAccount());
        List<ButtonItem> buttonItems = new ArrayList<>();
        if (getMvpView() == null) {
            return;
        }
        SparseArray<ButtonItem> avatarButtonItemMap = getMvpView().getAvatarButtonItemList(micPosition,
                chatRoomMember, currentRoom, roomQueueInfo.gender);
        if (AvRoomDataManager.get().isRoomOwner()) {
            //房主操作
            if (!isMySelf) {
                //点击不是自己
                buttonItems.add(avatarButtonItemMap.get(0));
                buttonItems.add(avatarButtonItemMap.get(4));
                buttonItems.add(avatarButtonItemMap.get(2));
                if (roomMicInfo.isMicMute()) {
                    buttonItems.add(avatarButtonItemMap.get(6));
                } else {
                    buttonItems.add(avatarButtonItemMap.get(1));
                }

                buttonItems.add(avatarButtonItemMap.get(3));

                if (!isTargetRoomAdmin) {
                    buttonItems.add(avatarButtonItemMap.get(7));
                } else {
                    buttonItems.add(avatarButtonItemMap.get(8));
                }
                buttonItems.add(avatarButtonItemMap.get(9));
                if (getMvpView() != null) {
                    getMvpView().showMicAvatarClickDialog(buttonItems);
                }
            } else {
                // 查看资料
                // 下麦旁听
                // 解禁此座位
//                buttonItems.add(avatarButtonItemMap.get(4));
//                buttonItems.add(avatarButtonItemMap.get(5));
//                if (roomMicInfo.isMicMute()) {
//                    buttonItems.add(avatarButtonItemMap.get(6));
//                }
                if (getMvpView() != null) {
                    getMvpView().showOwnerSelfInfo(chatRoomMember);
                }
            }
        } else {
            if (AvRoomDataManager.get().isRoomAdmin()) {
                //管理员操作
                if (isMySelf) {
                    //点击自己
                    //查看资料
                    //下麦旁听
                    // 禁麦此座位/解禁此座位
                    buttonItems.add(avatarButtonItemMap.get(4));
                    buttonItems.add(avatarButtonItemMap.get(5));
                    if (roomMicInfo.isMicMute()) {
                        buttonItems.add(avatarButtonItemMap.get(6));
                    } else {
                        buttonItems.add(avatarButtonItemMap.get(1));
                    }
                } else {
                    //房主只显示送礼
                    if (isTargetRoomOwner) {
                        getMvpView().showGiftDialog(chatRoomMember);
                        return;
                    }

                    //送礼物
                    buttonItems.add(avatarButtonItemMap.get(0));
                    //查看资料
                    buttonItems.add(avatarButtonItemMap.get(4));
                    if (!isTargetRoomAdmin && !isTargetRoomOwner) {
                        //非房主或管理员
                        //抱他下麦
                        buttonItems.add(avatarButtonItemMap.get(2));
                        //禁麦/解麦操作
                        buttonItems.add(roomMicInfo.isMicMute() ? avatarButtonItemMap.get(6) : avatarButtonItemMap.get(1));
                        if (AvRoomDataManager.get().isOwnerOnMic() && pair && AvRoomDataManager.get().isShowLikeTa) {
                            buttonItems.add(avatarButtonItemMap.get(10));
                        }
                        //踢出房间
                        buttonItems.add(avatarButtonItemMap.get(3));
                        //加入黑名单
                        buttonItems.add(avatarButtonItemMap.get(9));
                    } else {
                        if (!roomMicInfo.isMicMute() && !isTargetRoomOwner) {
                            buttonItems.add(avatarButtonItemMap.get(1));
                        }
                        if (!isTargetRoomOwner) {
                            buttonItems.add(avatarButtonItemMap.get(2));
                            if (AvRoomDataManager.get().isOwnerOnMic() && pair && AvRoomDataManager.get().isShowLikeTa) {
                                buttonItems.add(avatarButtonItemMap.get(10));
                            }
                        }

                        // 对于管理员和房主,只有解麦功能
                        if (roomMicInfo.isMicMute()) {
                            buttonItems.add(avatarButtonItemMap.get(6));
                        }
                    }
                }
                if (getMvpView() != null) {
                    getMvpView().showMicAvatarClickDialog(buttonItems);
                }
            } else {
                //游客操作
                if (isMySelf) {
                    //查看资料
                    //下麦旁听
                    buttonItems.add(avatarButtonItemMap.get(4));
                    buttonItems.add(avatarButtonItemMap.get(5));
                    if (getMvpView() != null) {
                        getMvpView().showMicAvatarClickDialog(buttonItems);
                    }
                } else {
                    if (getMvpView() != null) {
                        if (pair && AvRoomDataManager.get().isOwnerOnMic() && !isTargetRoomOwner) {
                            buttonItems.add(avatarButtonItemMap.get(0));
                            buttonItems.add(avatarButtonItemMap.get(4));
                            if (AvRoomDataManager.get().isShowLikeTa) {
                                buttonItems.add(avatarButtonItemMap.get(10));
                            }
                            LogUtils.d("showGiftDialog", 2 + "");
                            getMvpView().showMicAvatarClickDialog(buttonItems);
                        } else {
                            getMvpView().showGiftDialog(chatRoomMember);
                        }
                    }
                }
            }
        }


    }

    /**
     * 开麦
     *
     * @param micPosition
     */
    public void openMicroPhone(final int micPosition) {
        final RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null) {
            return;
        }

        OkHttpManager.MyCallBack myCallBack = new OkHttpManager.MyCallBack<ServiceResult<String>>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                LogUtils.d("用户%1$s开麦失败: %2$s", String.valueOf(roomInfo.getUid()), e);
            }

            @Override
            public void onResponse(ServiceResult<String> data) {
                if (null != data && data.isSuccess()) {//解麦成功
                    IMNetEaseManager.get().systemNotificationBySdk(-1,
                            CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_OPEN_MIC,
                            micPosition, 0, 0, false, null);
                    LogUtils.d(TAG, "openMicroPhone 用户" + roomInfo.getUid() + "开麦成功: " + data);
                } else {//解麦失败
                    LogUtils.d(TAG, "openMicroPhone 用户" + roomInfo.getUid() + "开麦失败: " + (null == data ? "" : data.getError()));
                }
            }
        };

        mHomePartyMode.openMicroPhone(micPosition, roomInfo.getUid(), roomInfo.getType(), myCallBack);
    }

    /**
     * 闭麦
     *
     * @param micPosition
     */
    public void closeMicroPhone(final int micPosition) {
        final RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null) {
            return;
        }

        OkHttpManager.MyCallBack myCallBack = new OkHttpManager.MyCallBack<ServiceResult<String>>() {
            @Override
            public void onError(Exception e) {
                LogUtils.d("用户%1$s闭麦失败: %2$s", String.valueOf(roomInfo.getUid()), e);
            }

            @Override
            public void onResponse(ServiceResult<String> data) {
                if (null != data && data.isSuccess()) {//解麦成功
                    IMNetEaseManager.get().systemNotificationBySdk(-1,
                            CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_CLOSE_MIC, micPosition,
                            0, 0, false, null);
                    LogUtils.d(TAG, "closeMicroPhone 用户" + roomInfo.getUid() + "闭麦成功: " + data);
                } else {//解麦失败
                    LogUtils.d(TAG, "closeMicroPhone 用户" + roomInfo.getUid() + "闭麦失败: " + (null == data ? "" : data.getError()));
                }
            }
        };

        mHomePartyMode.closeMicroPhone(micPosition, roomInfo.getUid(), roomInfo.getType(), myCallBack);
    }

    /***
     * 发送房间消息
     * @param message
     */
    public Disposable sendTextMsg(String message) {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null || TextUtils.isEmpty(message)) {
            return null;
        }
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (null == userInfo) {
            return null;
        }

        return IMNetEaseManager.get().sendTextMsg(roomInfo.getRoomId(), message, userInfo.getVipIcon())
                .subscribe(new BiConsumer<ChatRoomMessage, Throwable>() {
                    @Override
                    public void accept(ChatRoomMessage chatRoomMessage,
                                       Throwable throwable) throws Exception {
                        if (throwable != null) {
                            LogUtils.d(TAG, "sendTextMsg 发送房间信息失败" + throwable.getMessage());
                        } else {
                            IMNetEaseManager.get().addMessagesImmediately(chatRoomMessage);
                            LogUtils.d(TAG, "sendTextMsg 发送房间信息成功" + chatRoomMessage);
                        }
                    }
                });
    }


    /**
     * 重连之后的逻辑
     *
     * @param queueInfo
     */
    public Disposable chatRoomReConnect(final RoomQueueInfo queueInfo) {
        final RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null) {
            return null;
        }
        LogUtils.d("micInListLogFetchQueue", "chatRoomReConnect1");

        return mHomePartyMode.queryRoomMicInfo(String.valueOf(roomInfo.getRoomId()))
                .delay(1, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<Entry<String, String>>>() {
                    @Override
                    public void accept(List<Entry<String, String>> entries) throws Exception {
                        boolean needUpMic = true;
                        if (!ListUtils.isListEmpty(entries)) {
                            JsonParser jsonParser = new JsonParser();
                            ChatRoomMember chatRoomMember;
                            AvRoomDataManager.get().mMicInListMap.clear();
                            int roomMicInfoPosition = -100;
                            if (queueInfo != null && queueInfo.mRoomMicInfo != null) {
                                roomMicInfoPosition = queueInfo.mRoomMicInfo.getPosition();
                            }
                            for (Entry<String, String> entry : entries) {
                                LogUtils.d("micInListLogFetchQueue", "key:" + entry.key + "   content:" + entry.value);
                                if (entry.key != null && entry.key.length() > 2) {
                                    addInfoToMicInList(entry, jsonParser);
                                    continue;
//                                    AvRoomDataManager.get().addMicInListInfo(entry.key, roomQueueInfo);
                                }
                                //判断是否有别人在麦位上
                                if ((roomMicInfoPosition + "").equals(entry.key)) {
                                    String oldQueueUid = queueInfo.mChatRoomMember.getAccount() + "";
                                    String queueUid = Json.parse(entry.value).str("uid");
                                    if (!oldQueueUid.equals(queueUid)) {
                                        needUpMic = false;
                                    }
                                }

                                RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().mMicQueueMemberMap.get(Integer.parseInt(entry.key));
                                if (roomQueueInfo != null) {
                                    JsonObject valueJsonObj = jsonParser.parse(entry.value).getAsJsonObject();
                                    if (valueJsonObj != null) {
                                        chatRoomMember = new ChatRoomMember();
                                        if (valueJsonObj.has("uid")) {
                                            int uid = valueJsonObj.get("uid").getAsInt();
                                            chatRoomMember.setAccount(String.valueOf(uid));
                                        }
                                        if (valueJsonObj.has("nick")) {
                                            chatRoomMember.setNick(valueJsonObj.get("nick").getAsString());
                                        }
                                        if (valueJsonObj.has("avatar")) {
                                            chatRoomMember.setAvatar(valueJsonObj.get("avatar").getAsString());
                                        }
                                        if (valueJsonObj.has("gender")) {
                                            roomQueueInfo.gender = valueJsonObj.get("gender").getAsInt();
                                        }
                                        if (valueJsonObj.has(Constants.headwearUrl)) {
                                            Map<String, Object> stringStringMap = new HashMap<>();
                                            stringStringMap.put(Constants.headwearUrl, valueJsonObj.get(Constants.headwearUrl).getAsString());
                                            if (valueJsonObj.has(Constants.hasVggPic)) {
                                                stringStringMap.put(Constants.hasVggPic, valueJsonObj.get(Constants.hasVggPic).getAsBoolean());
                                            }
                                            chatRoomMember.setExtension(stringStringMap);
                                        }
                                        roomQueueInfo.mChatRoomMember = chatRoomMember;
                                    }
                                    AvRoomDataManager.get().addRoomQueueInfo(entry.key, roomQueueInfo);
                                }
                            }
                        } else {
                            //麦上都没有人
                            AvRoomDataManager.get().resetMicMembers();
                        }
                        if (getMvpView() != null) {
                            getMvpView().chatRoomReConnectView();
                            LogUtils.d(TAG, "chatRoomReConnect updatePairInfoForMicQueueChanged MicroViewAdapter.updateMicUi");
                            getMvpView().updatePairInfoForMicQueueChanged();
                        }
                        //之前在麦上
                        if (queueInfo != null && queueInfo.mChatRoomMember != null && queueInfo.mRoomMicInfo != null) {
                            RoomQueueInfo roomQueueInfo = AvRoomDataManager.get()
                                    .getRoomQueueMemberInfoByMicPosition(queueInfo.mRoomMicInfo.getPosition());
                            //麦上没人
                            String account = queueInfo.mChatRoomMember.getAccount();
                            if (needUpMic && roomQueueInfo != null && (roomQueueInfo.mChatRoomMember == null ||
                                    Objects.equals(account, roomQueueInfo.mChatRoomMember.getAccount()))) {

                                roomQueueInfo.mChatRoomMember = null;
                                AvRoomDataManager.get().mIsNeedOpenMic = false;
                                upMicroPhone(queueInfo.mRoomMicInfo.getPosition(), account, true);
                            }
                            if (!needUpMic) {
                                if (AvRoomDataManager.get().isOwner(account)) {
                                    // 更新声网闭麦 ,发送状态信息
                                    //9/26
//                                    RtcEngineManager.get().setRole(Constants.CLIENT_ROLE_AUDIENCE);
//                                    AvRoomDataManager.get().mIsNeedOpenMic = false;
                                    RtcEngineManager.get().setMute(true);
                                    AvRoomDataManager.get().mIsNeedOpenMic = false;
                                }
                            }
                        }
                        LogUtils.d(TAG, "chatRoomReConnect 断网重连获取队列信息成功...." + entries);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        throwable.printStackTrace();
                        LogUtils.d(TAG, "chatRoomReConnect 断网重连获取队列信息失败....");
                    }
                });
    }


    private void addInfoToMicInList(Entry<String, String> entry, JsonParser jsonParser) {

        JsonObject valueJsonObj = jsonParser.parse(entry.value).getAsJsonObject();


        Set<String> strings = valueJsonObj.keySet();
        if (valueJsonObj == null) {
            return;
        }
        Json json = new Json();
        for (String key : strings) {
            json.set(key, valueJsonObj.get(key).getAsString());
        }
        AvRoomDataManager.get().addMicInListInfo(entry.key, json);

    }

    public void removeMicInList() {

        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        final RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (null == userInfo || null == roomInfo) {
            return;
        }
        IMNetEaseManager.get().removeMicInList(userInfo.getUid() + "", roomInfo.getRoomId() + "", new RequestCallback() {
            @Override
            public void onSuccess(Object param) {

            }

            @Override
            public void onFailed(int code) {

            }

            @Override
            public void onException(Throwable exception) {

            }
        });


    }

    public void updataQueueExBySdk(int micPosition, String roomId, String value) {
        mHomePartyMode.updataQueueExBySdk(micPosition, roomId, null, value, false);
    }

    //更新房间信息
    public void updateRoomInfo() {
        LogUtils.d(TAG, "updateRoomInfo");
        if (null == AvRoomDataManager.get().mCurrentRoomInfo) {
            return;
        }

        OkHttpManager.MyCallBack myCallBack = new OkHttpManager.MyCallBack<ServiceResult<RoomInfo>>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(ServiceResult<RoomInfo> data) {
                if (null != data && data.isSuccess() && data.getData() != null
                        && data.getData().getRoomId() > 0) {
                    AvRoomDataManager.get().mCurrentRoomInfo = data.getData();
                    AvRoomDataManager.get().mServiceRoominfo = data.getData();
                }
            }
        };
        mAvRoomModel.requestRoomInfoFromService(AvRoomDataManager.get().mCurrentRoomInfo.getUid() + "",
                AvRoomDataManager.get().mCurrentRoomInfo.getType(), myCallBack);
    }

    public void updateRoomInfoAndGiftInfo() {
        LogUtils.d(TAG, "updateRoomInfoAndGiftInfo");
        updateRoomInfo();
        CoreManager.getCore(IGiftCore.class).requestGiftInfos();
    }

    public void getFortuneRankTopData(long roomUid, int roomType) {
        OkHttpManager.MyCallBack<RoomConsumeInfoListResult> myCallBack =
                new OkHttpManager.MyCallBack<RoomConsumeInfoListResult>() {
                    @Override
                    public void onError(Exception e) {
                        LogUtils.d(TAG, "getFortuneRankTopData-onError");
                        e.printStackTrace();
                        if (null != getMvpView()) {
                            getMvpView().onGetFortuneRankTop(false, null, null);
                        }
                    }

                    @Override
                    public void onResponse(RoomConsumeInfoListResult result) {
                        LogUtils.d(TAG, "getFortuneRankTopData-onResponse result:" + result);
                        if (null != getMvpView()) {
                            getMvpView().onGetFortuneRankTop(result.isSuccess(), result.getMessage(), result.getData());
                        }
                    }
                };
        new IMRoomModel().getRoomConsumeList(roomUid, roomType, 1, 1, myCallBack);

    }
}
