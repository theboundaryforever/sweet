package com.tongdaxing.xchat_core.room.model;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;

import java.util.Map;

/**
 * <p> 轰趴房model层：数据获取 </p>
 *
 * @author jiahui
 * @date 2017/12/8
 */
public class HomePartyModel extends RoomBaseModel {

    public HomePartyModel() {
    }

    /**
     * 锁住全部麦坑
     *
     * @param roomUid
     * @param ticker
     */
    public void locAllkMicroPhone(String roomUid, String ticker, int roomType, OkHttpManager.MyCallBack myCallBack) {
        operateMicroPhone("1", roomUid, ticker, roomType, myCallBack);
    }

    /**
     * 释放该全部麦坑的锁
     */
    public void unLockcAllMicroPhone(String roomUid, String ticker, int roomType, OkHttpManager.MyCallBack myCallBack) {
        operateMicroPhone("0", roomUid, ticker, roomType, myCallBack);
    }

    /**
     * 锁住麦坑
     *
     * @param micPosition
     * @param roomUid
     * @param ticker
     */
    public void lockMicroPhone(int micPosition, String roomUid, String ticker, int roomType, OkHttpManager.MyCallBack myCallBack) {
        operateMicroPhone(micPosition, "1", roomUid, ticker, roomType, myCallBack);
    }

    /**
     * 释放该麦坑的锁
     *
     * @param micPosition
     */
    public void unLockMicroPhone(int micPosition, String roomUid, String ticker, int roomType, OkHttpManager.MyCallBack myCallBack) {
        operateMicroPhone(micPosition, "0", roomUid, ticker, roomType, myCallBack);
    }

    /**
     * 操作全部麦坑的锁
     *
     * @param state   1：锁，0：不锁
     * @param roomUid
     * @param ticker
     */
    private void operateMicroPhone(String state, String roomUid, String ticker,
                                   int roomType, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("state", state);
        requestParam.put("roomUid", roomUid);
        requestParam.put("ticket", ticker);
        requestParam.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        requestParam.put("roomType", roomType + "");

        OkHttpManager.getInstance().postRequest(UriProvider.getAlllockMicroPhone(), requestParam, myCallBack);
    }

    /**
     * 操作麦坑的锁
     *
     * @param micPosition
     * @param state       1：锁，0：不锁
     * @param roomUid
     * @param ticker
     */
    private void operateMicroPhone(int micPosition, String state, String roomUid, String ticker,
                                   int roomType, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("position", micPosition + "");
        requestParam.put("state", state);
        requestParam.put("roomUid", roomUid);
        requestParam.put("ticket", ticker);
        requestParam.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        requestParam.put("roomType", roomType + "");

        OkHttpManager.getInstance().postRequest(UriProvider.getlockMicroPhone(), requestParam, myCallBack);
    }

    /**
     * 开麦
     *
     * @param micPosition
     * @param roomUid
     */
    public void openMicroPhone(int micPosition, long roomUid, int roomType, OkHttpManager.MyCallBack myCallBack) {
        openOrCloseMicroPhone(micPosition, 0, roomUid, CoreManager.getCore(IAuthCore.class).getTicket(), roomType, myCallBack);
    }

    /**
     * 闭麦
     *
     * @param micPosition
     * @param roomUid
     */
    public void closeMicroPhone(int micPosition, long roomUid, int roomType, OkHttpManager.MyCallBack myCallBack) {
        openOrCloseMicroPhone(micPosition, 1, roomUid, CoreManager.getCore(IAuthCore.class).getTicket(), roomType, myCallBack);
    }

    /**
     * 开闭麦接口
     *
     * @param micPosition
     * @param state       1:闭麦，0：开麦
     * @param roomUid
     * @param ticket
     */
    private void openOrCloseMicroPhone(int micPosition, int state, long roomUid, String ticket,
                                       int roomType, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("position", micPosition + "");
        requestParam.put("state", state + "");
        requestParam.put("roomUid", roomUid + "");
        requestParam.put("ticket", ticket);
        requestParam.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        requestParam.put("roomType", roomType + "");

        OkHttpManager.getInstance().postRequest(UriProvider.operateMicroPhone(), requestParam, myCallBack);
    }
}
