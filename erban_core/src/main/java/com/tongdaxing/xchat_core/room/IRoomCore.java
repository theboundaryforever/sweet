package com.tongdaxing.xchat_core.room;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.tongdaxing.erban.libcommon.coremanager.IBaseCore;

import io.reactivex.disposables.Disposable;

/**
 * Created by chenran on 2017/2/16.
 */

public interface IRoomCore extends IBaseCore {

    void getRoomConsumeList(long roomUid, int roomType);

    void getUserRoom(long uid);

    void userRoomIn(long roomUid);

    void userRoomOut();

    /**
     * 发送房间消息
     */
    Disposable sendMessage(ChatRoomMessage message);
}
