package com.tongdaxing.xchat_core.room.view;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;

import java.util.List;

/**
 * <p> </p>
 *
 * @author jiahui
 * @date 2017/12/19
 */
public interface IRoomManagerView extends IRoomMemberView {

    void onGetRoomAdminList(boolean isSuccess, String message, List<ChatRoomMember> chatRoomMemberList);

}
