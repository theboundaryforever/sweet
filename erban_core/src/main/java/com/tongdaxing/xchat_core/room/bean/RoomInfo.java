package com.tongdaxing.xchat_core.room.bean;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;


/**
 * @author zhouxiangfeng
 * @date 2017/5/24
 */

public class RoomInfo implements Parcelable {

    //废旧的房间类型--拍卖房
    @Deprecated
    public static final int ROOMTYPE_AUCTION = 1;

    //废旧的房间类型--轻聊房
    @Deprecated
    public static final int ROOMTYPE_LIGHT_CHAT = 2;

    //保留的旧有房间类型
    public static final int ROOMTYPE_HOME_PARTY = 3;

    //废旧的ktv房间类型
    @Deprecated
    public static final int ROOMTYPE_KTV = 4;

    //新版本IM单人音频房间
    public static final int ROOMTYPE_SINGLE_AUDIO = 4;

    //新版本IM多人人音频房间
    public static final int ROOMTYPE_MULTI_AUDIO = 5;

    //新的竞拍房
    public static final int ROOMTYPE_NEW_AUCTION= 6;


    public static final Creator<RoomInfo> CREATOR = new Creator<RoomInfo>() {
        @Override
        public RoomInfo createFromParcel(Parcel in) {
            return new RoomInfo(in);
        }

        @Override
        public RoomInfo[] newArray(int size) {
            return new RoomInfo[size];
        }
    };
    public String title;
    public String roomPwd;

    public int getTagId() {
        return tagId;
    }

    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    public String getTagPict() {
        return tagPict;
    }

    public void setTagPict(String tagPict) {
        this.tagPict = tagPict;
    }

    public int tagId;
    public String tagPict;
    /**
     * 房间在线人数
     */
    public int onlineNum;
    //增加防炸房校验逻辑需要的参数
    //是否开启炸房检测
    private boolean isAlarmEnable = false;
    //下麦延迟检测炸房的间隔，避免误杀
    private long timeInterval = 0L;
    private long uid;
    /**
     * 官方账号与非官方账号
     */
    private int officeUser;
    private long roomId;
    private long roomNo;
    private int type;

    private int charmOpen;

    //房间玩法
    private String playInfo;
    private String roomNotice;
    private String roomDesc;
    private String backPic;
    private String backName;
    private int factor;
    private List<Integer> hideFace;
    private int giftEffectSwitch;
    private int publicChatSwitch;
    /**
     * 控制宝箱显示与否开关
     */
    private boolean lotteryBoxOption;
    private boolean redPacketSwitch;
    private boolean bigWheelSwitch;
    private int charmSwitch;
    //年度活动房间排行
    private String annualCeremonyUrl;
    //当前房间排行
    private Long ranking;
    //同前一名相差的分数
    private Double previousScore;
    //同下一名相差的分数
    private Double nextScore;
    // 引爆礼物id
    private int detonatingGifts;
    // 引爆起始时间，协议约定有效时间十五分钟
    private long detonatingTime;
    // 引爆进度,-1为隐藏
    private int detonatingState;
    //引爆人ID
    private long detonatingUid;
    //引爆协议有效期，以秒为单位
    private long detonatingDuration;
    //引爆人头像
    private String detonatingAvatar;
    //引爆人昵称
    private String detonatingNick;
    // 特有礼物
    private List<Integer> exclusiveGift;
    /**
     * 房间是否开启，是否正在直播
     */
    private boolean valid;
    /**
     * 1:房主在房间，2 :房主不在房间
     */
    private int operatorStatus;
    private String meetingName;
    /**
     * 1--牌照房，2--非牌照房，3--审核牌照房
     */
    private int isPermitRoom;
    /**
     * 房间标签名
     */
    private String roomTag;
    /**
     * 是使用即构，还是声网  1.声网 2.即构
     *
     */
    private int audioChannel;


    /**
     * 房主直播时长，单位秒
     */
    private long onlineDuration;

    public long erbanNo;

    private int conveneCount;

    /**
     * 房间分类标签ID，默认的0-交友
     * 22-男神、23-女神、24-陪玩、25-电台
     * 26-交友、27-新秀、28-男神新秀、29-女神新秀
     * 30-陪玩新秀、31-电台新秀、32-闲聊新秀
     */
    private int searchTagId;

    public int getSearchTagId() {
        return searchTagId;
    }

    public void setSearchTagId(int searchTagId) {
        this.searchTagId = searchTagId;
    }


    public long getOnlineDuration() {
        return onlineDuration;
    }

    public void setOnlineDuration(long onlineDuration) {
        this.onlineDuration = onlineDuration;
    }


    public int getConveneCount() {
        return conveneCount;
    }

    public void setConveneCount(int conveneCount) {
        this.conveneCount = conveneCount;
    }


    /**
     * 是否能够显示召集令
     *
     * @return
     */
    public boolean showCallUp() {
        //只有非牌照、类型为交友的房主,才显示召集令
        return (searchTagId == 26 || searchTagId == 0) && isPermitRoom == 2;
    }

    protected RoomInfo(Parcel in) {
        uid = in.readLong();
        officeUser = in.readInt();
        roomId = in.readLong();
//        ranking = in.readLong();
//        previousScore = in.readDouble();
//        nextScore = in.readDouble();
//        annualCeremonyUrl = in.readString();
        detonatingTime = in.readLong();
        detonatingUid = in.readLong();
        detonatingDuration = in.readLong();
        detonatingState = in.readInt();
        detonatingGifts = in.readInt();
        detonatingAvatar = in.readString();
        detonatingNick = in.readString();

        title = in.readString();
        type = in.readInt();

        backName = in.readString();
        roomDesc = in.readString();
        backPic = in.readString();

        valid = in.readByte() != 0;
        operatorStatus = in.readInt();
        meetingName = in.readString();
//        openTime = in.readLong();
        roomPwd = in.readString();
        roomTag = in.readString();
        tagId = in.readInt();
        tagPict = in.readString();
        onlineNum = in.readInt();

        isPermitRoom = in.readInt();
        roomNotice = in.readString();
        factor = in.readInt();
        giftEffectSwitch = in.readInt();
        publicChatSwitch = in.readInt();
        charmSwitch = in.readInt();
        charmOpen = in.readInt();

        playInfo = in.readString();
        erbanNo = in.readLong();
    }

    public int getOnlineNum() {
        return onlineNum;
    }

    public void setOnlineNum(int onlineNum) {
        this.onlineNum = onlineNum;
    }

    public int getAudioChannel() {
        return audioChannel;
    }

    public void setAudioChannel(int audioChannel) {
        this.audioChannel = audioChannel;
    }

    public boolean isAlarmEnable() {
        return isAlarmEnable;
    }

    public void setAlarmEnable(boolean alarmEnable) {
        isAlarmEnable = alarmEnable;
    }

    public long getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(long timeInterval) {
        this.timeInterval = timeInterval;
    }

    public int getCharmOpen() {
        return charmOpen;
    }

    public void setCharmOpen(int charmOpen) {
        this.charmOpen = charmOpen;
    }

    public String getBackName() {
        return backName;
    }

    public void setBackName(String backName) {
        this.backName = backName;
    }

    public boolean isLotteryBoxOption() {
        return lotteryBoxOption;
    }

    public void setLotteryBoxOption(boolean lotteryBoxOption) {
        this.lotteryBoxOption = lotteryBoxOption;
    }

    public long getRoomNo() {
        return roomNo;
    }

    public void setRoomNo(long roomNo) {
        this.roomNo = roomNo;
    }

    public boolean isBigWheelSwitch() {
        return bigWheelSwitch;
    }

    public void setBigWheelSwitch(boolean bigWheelSwitch) {
        this.bigWheelSwitch = bigWheelSwitch;
    }

    public boolean isRedPacketSwitch() {
        return redPacketSwitch;
    }

    public void setRedPacketSwitch(boolean redPacketSwitch) {
        this.redPacketSwitch = redPacketSwitch;
    }

    public String getAnnualCeremonyUrl() {
        return annualCeremonyUrl;
    }

    public void setAnnualCeremonyUrl(String annualCeremonyUrl) {
        this.annualCeremonyUrl = annualCeremonyUrl;
    }

    public Long getRanking() {
        return ranking;
    }

    public void setRanking(Long ranking) {
        this.ranking = ranking;
    }

    public Double getPreviousScore() {
        return previousScore;
    }

    public void setPreviousScore(Double previousScore) {
        this.previousScore = previousScore;
    }

    public Double getNextScore() {
        return nextScore;
    }

    public void setNextScore(Double nextScore) {
        this.nextScore = nextScore;
    }

    public int getDetonatingState() {
        return detonatingState;
    }

    public void setDetonatingState(int detonatingState) {
        this.detonatingState = detonatingState;
    }

    public int getDetonatingGifts() {
        return detonatingGifts;
    }

    public void setDetonatingGifts(int detonatingGifts) {
        this.detonatingGifts = detonatingGifts;
    }

    public long getDetonatingTime() {
        return detonatingTime;
    }

    public void setDetonatingTime(long detonatingTime) {
        this.detonatingTime = detonatingTime;
    }

    public long getDetonatingUid() {
        return detonatingUid;
    }

    public void setDetonatingUid(long detonatingUid) {
        this.detonatingUid = detonatingUid;
    }

    public long getDetonatingDuration() {
        return detonatingDuration;
    }

    public void setDetonatingDuration(long detonatingDuration) {
        this.detonatingDuration = detonatingDuration;
    }

    public List<Integer> getExclusiveGift() {
        return exclusiveGift;
    }

    public void setExclusiveGift(List<Integer> exclusiveGift) {
        this.exclusiveGift = exclusiveGift;
    }

    public String getDetonatingAvatar() {
        return detonatingAvatar;
    }

    public void setDetonatingAvatar(String detonatingAvatar) {
        this.detonatingAvatar = detonatingAvatar;
    }

    public String getDetonatingNick() {
        return detonatingNick;
    }

    public void setDetonatingNick(String detonatingNick) {
        this.detonatingNick = detonatingNick;
    }

    public int getCharmSwitch() {
        return charmSwitch;
    }

    public void setCharmSwitch(int charmSwitch) {
        this.charmSwitch = charmSwitch;
    }

    public int getGiftEffectSwitch() {
        return giftEffectSwitch;
    }

    public void setGiftEffectSwitch(int giftEffectSwitch) {
        this.giftEffectSwitch = giftEffectSwitch;
    }

    public int getPublicChatSwitch() {
        return publicChatSwitch;
    }


//    private long openTime;

    public void setPublicChatSwitch(int publicChatSwitch) {
        this.publicChatSwitch = publicChatSwitch;
    }

    public List<Integer> getHideFace() {
        return hideFace;
    }

    public void setHideFace(List<Integer> hideFace) {
        this.hideFace = hideFace;
    }

    public int getIsPermitRoom() {
        return isPermitRoom;

    }

    public void setIsPermitRoom(int isPermitRoom) {
        this.isPermitRoom = isPermitRoom;
    }

    public int getFactor() {
        return factor;
    }

    public void setFactor(int factor) {
        this.factor = factor;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(uid);
        dest.writeInt(officeUser);
        dest.writeLong(roomId);
        //参考https://blog.csdn.net/mad1989/article/details/36295879
        // Caused by: java.lang.RuntimeException:
        // Parcel android.os.Parcel@530cd29:
        // Unmarshalling unknown type code 6226023 at offset 408，
        // 另外，年度房间排行榜等这些需要服务器实时更新的数据，
        // 房间设置中并不是必须的，因此可以不做pracel读写操作，
        // 否则M15手机还是会抛如上错，因为房间设置界面，塞的是云信的数据，
        // 并不一定会有这些对象，null导致异常
//        if (null != ranking) {
//            dest.writeLong(ranking);
//        }
//        if (null != previousScore) {
//            dest.writeDouble(previousScore);
//        }
//        if (null != nextScore) {
//            dest.writeDouble(nextScore);
//        }
//        if (null != annualCeremonyUrl) {
//            dest.writeString(annualCeremonyUrl);
//        }

        dest.writeLong(detonatingTime);
        dest.writeLong(detonatingUid);
        dest.writeLong(detonatingDuration);
        dest.writeInt(detonatingState);
        dest.writeInt(detonatingGifts);
        dest.writeString(detonatingAvatar);
        dest.writeString(detonatingNick);

        dest.writeString(title);
        dest.writeInt(type);

        dest.writeString(backName);
        dest.writeString(roomDesc);
        dest.writeString(backPic);

        dest.writeByte((byte) (valid ? 1 : 0));
        dest.writeInt(operatorStatus);
        dest.writeString(meetingName);
        dest.writeString(roomPwd);
        dest.writeString(roomTag);

        dest.writeInt(tagId);
        dest.writeString(tagPict);
        dest.writeInt(onlineNum);
        dest.writeInt(isPermitRoom);
        dest.writeString(roomNotice);
        dest.writeInt(factor);
        dest.writeInt(giftEffectSwitch);
        dest.writeInt(publicChatSwitch);
        dest.writeInt(charmSwitch);
        dest.writeInt(charmOpen);
        dest.writeString(playInfo);
        dest.writeLong(erbanNo);

    }

    @Override
    public int describeContents() {
        return 0;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public int getOfficeUser() {
        return officeUser;
    }

    public void setOfficeUser(int officeUser) {
        this.officeUser = officeUser;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getRoomDesc() {
        if (null != roomDesc) {
            return roomDesc.replace("\n", "");
        }
        return roomDesc;
    }

    public void setRoomDesc(String roomDesc) {
        if (null != roomDesc) {
            this.roomDesc = roomDesc.replace("\n", "");
        } else {
            this.roomDesc = roomDesc;
        }

    }

    public String getBackPic() {
        return backPic;
    }

    public void setBackPic(String backPic) {
        this.backPic = backPic;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public int getOperatorStatus() {
        return operatorStatus;
    }

    public void setOperatorStatus(int operatorStatus) {
        this.operatorStatus = operatorStatus;
    }

    public String getMeetingName() {
        return meetingName;
    }

    public void setMeetingName(String meetingName) {
        this.meetingName = meetingName;
    }

//    public long getOpenTime() {
//        return openTime;
//    }
//
//    public void setOpenTime(long openTime) {
//        this.openTime = openTime;
//    }

    public String getRoomNotice() {
        return roomNotice;
    }

    public void setRoomNotice(String roomNotice) {
        this.roomNotice = roomNotice;
    }

    public String getRoomPwd() {
        return roomPwd;
    }

    public void setRoomPwd(String roomPwd) {
        this.roomPwd = roomPwd;
    }

    public String getRoomTag() {
        return roomTag;
    }

    public void setRoomTag(String roomTag) {
        this.roomTag = roomTag;
    }

    public String getPlayInfo() {
        return playInfo;
    }

    public void setPlayInfo(String playInfo) {
        this.playInfo = playInfo;
    }

    public long getErbanNo() {
        return erbanNo;
    }

    public void setErbanNo(long erbanNo) {
        this.erbanNo = erbanNo;
    }

    @Override
    public String toString() {
        return "RoomInfo{" +
                "title='" + title + '\'' +
                ", roomPwd='" + roomPwd + '\'' +
                ", tagId=" + tagId +
                ", tagPict='" + tagPict + '\'' +
                ", onlineNum=" + onlineNum +
                ", isAlarmEnable=" + isAlarmEnable +
                ", timeInterval=" + timeInterval +
                ", uid=" + uid +
                ", officeUser=" + officeUser +
                ", roomId=" + roomId +
                ", roomNo=" + roomNo +
                ", type=" + type +
                ", charmOpen=" + charmOpen +
                ", playInfo='" + playInfo + '\'' +
                ", roomNotice='" + roomNotice + '\'' +
                ", roomDesc='" + roomDesc + '\'' +
                ", backPic='" + backPic + '\'' +
                ", backName='" + backName + '\'' +
                ", factor=" + factor +
                ", hideFace=" + hideFace +
                ", giftEffectSwitch=" + giftEffectSwitch +
                ", publicChatSwitch=" + publicChatSwitch +
                ", lotteryBoxOption=" + lotteryBoxOption +
                ", redPacketSwitch=" + redPacketSwitch +
                ", bigWheelSwitch=" + bigWheelSwitch +
                ", charmSwitch=" + charmSwitch +
                ", annualCeremonyUrl='" + annualCeremonyUrl + '\'' +
                ", ranking=" + ranking +
                ", previousScore=" + previousScore +
                ", nextScore=" + nextScore +
                ", detonatingGifts=" + detonatingGifts +
                ", detonatingTime=" + detonatingTime +
                ", detonatingState=" + detonatingState +
                ", detonatingUid=" + detonatingUid +
                ", detonatingDuration=" + detonatingDuration +
                ", detonatingAvatar='" + detonatingAvatar + '\'' +
                ", detonatingNick='" + detonatingNick + '\'' +
                ", exclusiveGift=" + exclusiveGift +
                ", valid=" + valid +
                ", operatorStatus=" + operatorStatus +
                ", meetingName='" + meetingName + '\'' +
                ", isPermitRoom=" + isPermitRoom +
                ", roomTag='" + roomTag + '\'' +
                ", audioChannel=" + audioChannel +
                ", onlineDuration=" + onlineDuration +
                ", erbanNo=" + erbanNo +
                ", searchTagId=" + searchTagId +
                '}';
    }
}
