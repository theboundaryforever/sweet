package com.tongdaxing.xchat_core.room.bean;

import android.support.annotation.NonNull;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tongdaxing.erban.libcommon.utils.ListUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

/**
 * <p> 在线用户列表，包装 {@link ChatRoomMember} </p>
 *
 * @author jiahui
 * @date 2017/12/20
 */
public class OnlineChatMember implements Comparable<OnlineChatMember> {
    public ChatRoomMember chatRoomMember;

    public boolean isOnMic;
    public boolean isAdmin;
    public boolean isRoomOwer;

    public OnlineChatMember() {
    }

    public OnlineChatMember(ChatRoomMember chatRoomMember) {
        this.chatRoomMember = chatRoomMember;
    }

    public OnlineChatMember(ChatRoomMember chatRoomMember, boolean isOnMic, boolean isAdmin, boolean isRoomOwer) {
        this.chatRoomMember = chatRoomMember;
        this.isOnMic = isOnMic;
        this.isAdmin = isAdmin;
        this.isRoomOwer = isRoomOwer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OnlineChatMember)) {
            return false;
        }

        OnlineChatMember that = (OnlineChatMember) o;
        return !(chatRoomMember == null || that.chatRoomMember == null)
                && Objects.equals(chatRoomMember.getAccount(), that.chatRoomMember.getAccount());
    }

    @Override
    public int hashCode() {
        return chatRoomMember.getAccount().hashCode();
    }

    public static List<OnlineChatMember> coverToOnlineChatMember(List<ChatRoomMember> oldList, List<ChatRoomMember> newList) {
        HashSet<OnlineChatMember> treeSet = new HashSet<>();
        if (!ListUtils.isListEmpty(oldList)) {
            List<OnlineChatMember> list = new ArrayList<>(oldList.size());
            for (ChatRoomMember chatRoomMember : oldList) {
                list.add(new OnlineChatMember(chatRoomMember));
            }
            treeSet.addAll(list);
        }
        if (!ListUtils.isListEmpty(newList)) {
            List<OnlineChatMember> list = new ArrayList<>(newList.size());
            for (ChatRoomMember chatRoomMember : newList) {
                list.add(new OnlineChatMember(chatRoomMember));
            }
            treeSet.addAll(list);
        }
        ArrayList<OnlineChatMember> list = new ArrayList<>(treeSet);
        Collections.sort(list);
        return list;
    }

    public static List<ChatRoomMember> converOnlineToNormal(List<OnlineChatMember> onlineChatMembers) {
        if (ListUtils.isListEmpty(onlineChatMembers)) return null;
        List<ChatRoomMember> chatRoomMembers = new ArrayList<>();
        for (OnlineChatMember temp : onlineChatMembers) {
            if (temp.chatRoomMember == null) continue;
            chatRoomMembers.add(temp.chatRoomMember);
        }
        return chatRoomMembers;
    }

    @Override
    public int compareTo(@NonNull OnlineChatMember o) {
        if (chatRoomMember == null) return 1;
        if (o.chatRoomMember == null) return -1;
        return (int) (o.chatRoomMember.getEnterTime() - chatRoomMember.getEnterTime());
    }
}
