package com.tongdaxing.xchat_core.room.queue.bean;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;

/**
 * 坑上单个队列信息
 * Created by chenran on 2017/9/8.
 */

public class RoomQueueMemberInfo {
    private RoomQueueInfo roomQueueInfo;
    public ChatRoomMember chatRoomMember;

    public RoomQueueMemberInfo(RoomQueueInfo roomQueueInfo, ChatRoomMember chatRoomMember) {
        this.roomQueueInfo = roomQueueInfo;
        this.chatRoomMember = chatRoomMember;
    }

    public RoomQueueInfo getRoomQueueInfo() {
        return roomQueueInfo;
    }

    public void setRoomQueueInfo(RoomQueueInfo roomQueueInfo) {
        this.roomQueueInfo = roomQueueInfo;
    }

    public ChatRoomMember getChatRoomMember() {
        return chatRoomMember;
    }

    public void setChatRoomMember(ChatRoomMember chatRoomMember) {
        this.chatRoomMember = chatRoomMember;
    }
}
