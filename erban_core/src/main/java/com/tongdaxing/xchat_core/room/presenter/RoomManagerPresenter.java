package com.tongdaxing.xchat_core.room.presenter;

import com.netease.nimlib.sdk.chatroom.constant.MemberType;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.room.bean.ServerUserMemberInfo;
import com.tongdaxing.xchat_core.room.model.HomePartyUserListModel;
import com.tongdaxing.xchat_core.room.model.RoomBaseModel;
import com.tongdaxing.xchat_core.room.view.IRoomManagerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p> </p>
 *
 * @author jiahui
 * @date 2017/12/19
 */
public class RoomManagerPresenter extends AbstractMvpPresenter<IRoomManagerView> {

    private final String TAG = RoomManagerPresenter.class.getSimpleName();

    private final RoomBaseModel mRoomBaseModel;

    private final HomePartyUserListModel mHomePartyUserListMode;

    public RoomManagerPresenter() {
        mRoomBaseModel = new RoomBaseModel();
        mHomePartyUserListMode = new HomePartyUserListModel();
    }

    public void queryRoomAdminList(int limit, long roomId) {
        OkHttpManager.MyCallBack myCallBack = new OkHttpManager.MyCallBack<ServiceResult<List<ServerUserMemberInfo>>>() {
                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                        if (null != getMvpView()) {
                            getMvpView().onGetRoomAdminList(true, e.getMessage(), null);
                        }
                    }

                    @Override
                    public void onResponse(ServiceResult<List<ServerUserMemberInfo>> serviceResult) {
                        LogUtils.d(TAG, "getRoomOnLineUserList-onResponse-result:" + serviceResult);
                        if (null != serviceResult) {
                            long startTime = System.currentTimeMillis();
                            //这里可能要考虑将几个for循环合并，降低循环数据处理耗时
                            List<ChatRoomMember> managerList = new ArrayList<>();
                            if (serviceResult.isSuccess()) {
                                if (!ListUtils.isListEmpty(serviceResult.getData())) {
                                    for (ServerUserMemberInfo serverUserMemberInfo : serviceResult.getData()) {
                                        if (mHomePartyUserListMode.parseMemberTypeServer2Client(serverUserMemberInfo.getType()) == MemberType.ADMIN) {
                                            ChatRoomMember chatRoomMember = new ChatRoomMember();
                                            chatRoomMember.setRoomId(String.valueOf(serverUserMemberInfo.getRoomid()));
                                            chatRoomMember.setAccount(serverUserMemberInfo.getAccid());
                                            chatRoomMember.setMemberType(mHomePartyUserListMode.parseMemberTypeServer2Client(serverUserMemberInfo.getType()));
                                            chatRoomMember.setMemberLevel(serverUserMemberInfo.getLevel());
                                            chatRoomMember.setNick(serverUserMemberInfo.getNick());
                                            chatRoomMember.setAvatar(serverUserMemberInfo.getAvator());
                                            Map<String, Object> extMap = mHomePartyUserListMode.parseMemberExtServer2Client(
                                                    serverUserMemberInfo.getExt(), serverUserMemberInfo.getVipId(), serverUserMemberInfo.getVipDate(),
                                                    serverUserMemberInfo.getIsInvisible() ? 1 : 0, serverUserMemberInfo.getInvisibleUid());
                                            chatRoomMember.setExtension(extMap);
                                            chatRoomMember.setOnline(serverUserMemberInfo.isOnlineStat());
                                            chatRoomMember.setInBlackList(serverUserMemberInfo.isBlacklisted());
                                            chatRoomMember.setMuted(serverUserMemberInfo.isMuted());
                                            chatRoomMember.setEnterTime(serverUserMemberInfo.getEnterTime());
                                            mHomePartyUserListMode.parseChatRoomMemberUpdateTime(extMap, chatRoomMember);
                                            chatRoomMember.setTempMuted(serverUserMemberInfo.isTempMuted());
                                            chatRoomMember.setTempMuteDuration(serverUserMemberInfo.getTempMuteTtl());
                                            managerList.add(chatRoomMember);
                                        }
                                    }
                                }
                            }
                            LogUtils.d(TAG, "getRoomOnLineUserList 解析接口返回数据，总耗时：" + (System.currentTimeMillis() - startTime));
                            if (null != getMvpView()) {
                                getMvpView().onGetRoomAdminList(true, serviceResult.getMessage(), managerList);
                            }
                        } else if (null != getMvpView()) {
                            getMvpView().onGetRoomAdminList(true, null, null);
                        }
                    }
        };
        mRoomBaseModel.queryRoomAdminList(limit, roomId, myCallBack, null);
    }

    public void removeUserFromAdminList(long targetId, long roomId) {
        mRoomBaseModel.removeUserFromAdminList(targetId, roomId, new OkHttpManager.MyCallBack<ServiceResult<ServerUserMemberInfo>>() {

            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (getMvpView() != null) {
                    getMvpView().onRemoveUserFromAdminList(false, e.getMessage(), String.valueOf(targetId));
                }
            }

            @Override
            public void onResponse(ServiceResult<ServerUserMemberInfo> result) {
                if (null == getMvpView()) {
                    return;
                }
                if (null != result && result.isSuccess()) {
                    getMvpView().onRemoveUserFromAdminList(true, result.getMessage(), String.valueOf(targetId));
                } else {
                    getMvpView().onRemoveUserFromAdminList(false, null == result ? null : result.getMessage(), String.valueOf(targetId));
                }
            }
        });
    }
}
