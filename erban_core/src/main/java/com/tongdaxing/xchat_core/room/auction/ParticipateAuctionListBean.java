package com.tongdaxing.xchat_core.room.auction;

import com.tongdaxing.erban.libcommon.base.BaseAppBean;

import java.util.List;

/**
 * 文件描述：等待拍卖的列表信息实体
 *
 * @auther：zwk
 * @data：2019/7/29
 */
public class ParticipateAuctionListBean extends BaseAppBean {
    private int count;
    private boolean isJoin;
    private List<ParticipateAuctionBean> userList;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isJoin() {
        return isJoin;
    }

    public void setJoin(boolean join) {
        isJoin = join;
    }

    public List<ParticipateAuctionBean> getUserList() {
        return userList;
    }

    public void setUserList(List<ParticipateAuctionBean> userList) {
        this.userList = userList;
    }
}
