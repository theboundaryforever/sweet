package com.tongdaxing.xchat_core.room.model;

import android.text.TextUtils;

import com.alibaba.fastjson.JSONObject;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nimlib.sdk.NIMChatRoomSDK;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.chatroom.constant.MemberQueryType;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomInfo;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.util.Entry;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.exception.ErrorThrowable;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RoomMicInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.noble.NobleBusinessManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.bean.ServerUserMemberInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.schedulers.Schedulers;

/**
 * <p>房间相关操作model </p>
 *
 * @author jiahui
 * @date 2017/12/19
 */
public class RoomBaseModel extends BaseMvpModel {

    private final static String TAG = RoomBaseModel.class.getSimpleName();

    /**
     * 一页房间人数
     */
    protected static final int ROOM_MEMBER_SIZE = 50;


    /**
     * 获取固定成员列表(操作在子线程，需要在主线程操作的自己转)
     *
     * @param limit 请求数量
     */
    public Single<List<ChatRoomMember>> queryNormalList(int limit) {
        return queryNormalList(limit, 0);
    }


    /**
     * 获取固定成员列表(操作在子线程，需要在主线程操作的自己转)
     *
     * @param limit 请求数量
     * @param time  时间戳，用于分页加载
     */
    public Single<List<ChatRoomMember>> queryNormalList(int limit, long time) {
        return fetchRoomMembers(MemberQueryType.NORMAL, time, limit);
    }

    /**
     * 获取最新固定在线人数(操作在子线程，需要在主线程操作的自己转)
     *
     * @param limit 请求数量
     */
    public Single<List<ChatRoomMember>> queryOnlineList(int limit) {
        return fetchRoomMembers(MemberQueryType.ONLINE_NORMAL, 0, limit);
    }


    /**
     * 获取最新在线游客列表 (操作在子线程，需要在主线程操作的自己转)
     *
     * @param limit 请求数量
     */
    public Single<List<ChatRoomMember>> queryGuestList(int limit) {
        return queryGuestList(limit, 0);
    }

    /**
     * 获取在线游客列表 (操作在子线程，需要在主线程操作的自己转)
     *
     * @param limit 获取数量大小
     * @param time  从当前时间开始查找
     */
    public Single<List<ChatRoomMember>> queryGuestList(int limit, long time) {
        return fetchRoomMembers(MemberQueryType.GUEST, time, limit);
    }

    private Single<List<ChatRoomMember>> fetchRoomMembers(final MemberQueryType memberType,
                                                          final long time, final int limit) {
        final RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null) {
            return Single.error(new ErrorThrowable(ErrorThrowable.ROOM_INFO_NULL_ERROR));
        }
        return Single.create(
                new SingleOnSubscribe<List<ChatRoomMember>>() {
                    @Override
                    public void subscribe(SingleEmitter<List<ChatRoomMember>> e) throws Exception {
                        executeNIMClient("fetchRoomMembers", NIMClient.syncRequest(NIMChatRoomSDK.getChatRoomService()
                                .fetchRoomMembers(String.valueOf(roomInfo.getRoomId()), memberType, time, limit)), e);
                    }
                })
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io());
    }

    /**
     * 邀请上麦
     *
     * @param micUid   上麦用户uid
     * @param position
     * @return Single<ChatRoomMessage>
     */
    public Single<ChatRoomMessage> inviteMicroPhone(long micUid, int position) {
        return IMNetEaseManager.get().inviteMicroPhoneBySdk(micUid, position);
    }


    /**
     * 上麦
     *
     * @param micPosition
     * @param uId           要上麦的用户id
     * @param roomId
     * @param isInviteUpMic 是否是主动的
     * @param callBack
     */
    public void upMicroPhone(final int micPosition, final String uId, final String roomId,
                             boolean isInviteUpMic, final CallBack<String> callBack, boolean needCloseMic) {
        RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().mMicQueueMemberMap.get(micPosition);

        if (roomQueueInfo == null) {
            return;
        }
        ChatRoomMember chatRoomMember = roomQueueInfo.mChatRoomMember;
        final RoomMicInfo roomMicInfo = roomQueueInfo.mRoomMicInfo;

        //修复部分用户进入自己的房间无法上麦的问题
        final UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (micPosition == -1 && chatRoomMember != null && AvRoomDataManager.get().isRoomOwner()) {
            updateQueueEx(micPosition, roomId, callBack, userInfo);
        }

        //坑上没人且没锁
        if (roomMicInfo != null
                && ((!roomMicInfo.isMicLock() || AvRoomDataManager.get().isRoomOwner(uId)
                || AvRoomDataManager.get().isRoomAdmin(uId))
                || isInviteUpMic)
                && chatRoomMember == null) {
            if (AvRoomDataManager.get().isRoomOwner() != (micPosition == -1)) {
                return;
            }
            if (userInfo != null) {
                //先看下这个用户是否在麦上
                if (AvRoomDataManager.get().isOnMic(userInfo.getUid())) {
                    int position = AvRoomDataManager.get().getMicPosition(userInfo.getUid());
                    if (position == -1) {
                        return;
                    }

                    //下麦
                    downMicroPhone(position, new CallBack<String>() {
                        @Override
                        public void onSuccess(String data) {
                            updateQueueEx(micPosition, roomId, callBack, userInfo);
                        }

                        @Override
                        public void onFail(int code, String error) {
                            if (callBack != null) {
                                callBack.onFail(-1, "上麦导致下麦失败");
                            }
                        }
                    });
                } else {
                    updateQueueEx(micPosition, roomId, callBack, userInfo);
                }
            }
        }
    }

    public void downMicroPhone(int micPosition, final CallBack<String> callBack) {
        IMNetEaseManager.get().downMicroPhoneBySdk(micPosition, callBack);

    }

    public void updateQueueEx(int micPosition, String roomId, final CallBack<String> callBack, UserInfo userInfo) {
        updateQueueEx(micPosition, roomId, callBack, userInfo, -1);
    }


    public void updateQueueEx(int micPosition, String roomId, final CallBack<String> callBack, UserInfo userInfo, int type) {

        JSONObject contentJsonObj = new JSONObject();
        //大于0的是排麦的
        if (type > 0) {
            contentJsonObj.put("time", String.valueOf(System.currentTimeMillis()));
            contentJsonObj.put(Constants.USER_EXPER_LEVEL, userInfo.getExperLevel() + "");
            contentJsonObj.put(Constants.USER_ERBAN_NO, userInfo.getErbanNo() + "");
        }

        if (!TextUtils.isEmpty(userInfo.getHeadwearUrl())) {
            contentJsonObj.put(Constants.headwearUrl, userInfo.getHeadwearUrl());
            contentJsonObj.put(Constants.hasVggPic, userInfo.isHasVggPic());
        }

        contentJsonObj.put(Constants.USER_UID, String.valueOf(userInfo.getUid()));
        contentJsonObj.put(Constants.USER_NICK,
                NobleBusinessManager.getNobleRoomNick(userInfo.getVipId(), userInfo.getIsInvisible(),
                        userInfo.getVipDate(), userInfo.getNick()));
        contentJsonObj.put(Constants.USER_AVATAR,
                NobleBusinessManager.getNobleRoomAvatarUrl(userInfo.getVipId(), userInfo.getIsInvisible(),
                        userInfo.getVipDate(), userInfo.getAvatar()));
        contentJsonObj.put(Constants.USER_GENDER, userInfo.getGender());
        contentJsonObj.put(Constants.USER_MEDAL_ID, userInfo.getVipId());
        contentJsonObj.put(Constants.USER_MEDAL_DATE, userInfo.getVipDate());
        contentJsonObj.put(Constants.NOBLE_INVISIABLE_ENTER_ROOM, userInfo.getIsInvisible() ? 1 : 0);
        contentJsonObj.put(Constants.NOBLE_INVISIABLE_UID, userInfo.getInvisibleUid());
        String value = contentJsonObj.toJSONString();
        updataQueueExBySdk(micPosition, roomId, callBack, value);
    }


    public void updataQueueExBySdk(int micPosition, String roomId, final CallBack<String> callBack, String value) {
        updataQueueExBySdk(micPosition, roomId, callBack, value, true);
    }


    public void updataQueueExBySdk(int micPosition, String roomId, final CallBack<String> callBack, String value, boolean isTransient) {

        //防止房主掉麦
        if (micPosition == -1) {
            String currentUid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            if (!AvRoomDataManager.get().isRoomOwner(currentUid)) {
                return;
            }
        }

        LogUtils.d("micInListLogDpdataQueue", "getRoomId:" + roomId + "   key:" + micPosition);
        NIMChatRoomSDK.getChatRoomService()
                .updateQueueEx(roomId, String.valueOf(micPosition), value, isTransient)
                .setCallback(new RequestCallback<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        if (callBack != null) {
                            callBack.onSuccess("上麦成功");
                        }
                        LogUtils.d("micInListLogDpdataQueue", 1 + "");
                    }

                    @Override
                    public void onFailed(int i) {
                        if (callBack != null) {
                            callBack.onFail(-1, "上麦失败");
                        }
                        LogUtils.d("micInListLogDpdataQueue", 2 + "");
                    }

                    @Override
                    public void onException(Throwable throwable) {
                        if (callBack != null) {
                            callBack.onFail(-1, "上麦异常");
                        }
                        LogUtils.d("micInListLogDpdataQueue", 3 + "");
                    }
                });
    }

    /**
     * 获取房间队列信息
     *
     * @param roomId
     */
    public Observable<List<Entry<String, String>>> queryRoomMicInfo(final String roomId) {
        LogUtil.d(TAG, "queryRoomMicInfo roomId = " + roomId);
        return Observable.create(new ObservableOnSubscribe<List<Entry<String, String>>>() {
            @Override
            public void subscribe(ObservableEmitter<List<Entry<String, String>>> e) throws Exception {
                executeNIMClient("fetchQueue", NIMClient.syncRequest(NIMChatRoomSDK.getChatRoomService().fetchQueue(roomId)), e);
            }
        }).subscribeOn(Schedulers.io()).unsubscribeOn(Schedulers.io());
    }

    /**
     * 获取聊天室信息
     *
     * @param roomId 聊天室id
     * @return ChatRoomInfo 房间信息
     */
    public Observable<ChatRoomInfo> startGetOnlineMemberNumberJob(final long roomId) {
        return Observable.create(new ObservableOnSubscribe<ChatRoomInfo>() {
            @Override
            public void subscribe(ObservableEmitter<ChatRoomInfo> e) throws Exception {
                try {
                    executeNIMClient("fetchRoomInfo", NIMClient.syncRequest(NIMChatRoomSDK.getChatRoomService()
                            .fetchRoomInfo(String.valueOf(roomId))), e);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }).subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io());
    }

    public void addUserToAdminList(long targetUid, long roomId, OkHttpManager.MyCallBack<ServiceResult<ServerUserMemberInfo>> myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("targetUid", String.valueOf(targetUid));
        params.put("roomId", String.valueOf(roomId));
        //1：设置 2：取消设置
        params.put("operate", "1");
        //1: 管理员，2:普通等级用户，3:黑名单用户，4:禁言用户
        params.put("type", "1");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");

        OkHttpManager.getInstance().postRequest(UriProvider.getUpdateUserRoomRoleUrl(), params, myCallBack);
    }

    public void removeUserFromAdminList(long targetUid, long roomId, OkHttpManager.MyCallBack<ServiceResult<ServerUserMemberInfo>> myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("targetUid", String.valueOf(targetUid));
        params.put("roomId", String.valueOf(roomId));
        //1：设置 2：取消设置
        params.put("operate", "2");
        //1: 管理员，2:普通等级用户，3:黑名单用户，4:禁言用户
        params.put("type", "1");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");

        OkHttpManager.getInstance().postRequest(UriProvider.getUpdateUserRoomRoleUrl(), params, myCallBack);
    }

    public void removeUserFromBlackList(long targetUid, long roomId, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("targetUid", String.valueOf(targetUid));
        params.put("roomId", String.valueOf(roomId));
        //1：设置 2：取消设置
        params.put("operate", "2");
        //1: 管理员，2:普通等级用户，3:黑名单用户，4:禁言用户
        params.put("type", "3");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");

        OkHttpManager.getInstance().postRequest(UriProvider.getUpdateUserRoomRoleUrl(), params, myCallBack);
    }

    public void addUserToBlackList(long targetUid, long roomId, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("targetUid", String.valueOf(targetUid));
        params.put("roomId", String.valueOf(roomId));
        //1：设置 2：取消设置
        params.put("operate", "1");
        //1: 管理员，2:普通等级用户，3:黑名单用户，4:禁言用户
        params.put("type", "3");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");

        OkHttpManager.getInstance().postRequest(UriProvider.getUpdateUserRoomRoleUrl(), params, myCallBack);
    }

//-----------------------------相同业务场景不同实现，对应的接口--------------------------------

    /**
     * 查询房间成员
     *
     * @param limit     查询条数，接口限制最大100
     * @param roomId
     * @param callBack1
     * @param callBack2
     */
    public void                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               queryRoomAdminList(int limit, long roomId,
                                   OkHttpManager.MyCallBack<ServiceResult<List<ServerUserMemberInfo>>> callBack1,
                                   HttpRequestCallBack callBack2) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        //第一页
        params.put("endTime", "0");
        //最多展示1000条
        params.put("limit", String.valueOf(limit));
        params.put("roomId", String.valueOf(roomId));
        //1管理员管理 2黑名单管理 3房间在线列表
        params.put("type", "1");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");

        if (null != callBack1) {
            OkHttpManager.getInstance().postRequest(UriProvider.getRoomUserListUrl(), params, callBack1);
        } else if (null != callBack2) {
            OkHttpManager.getInstance().postRequest(UriProvider.getRoomUserListUrl(), params, callBack2);
        }
    }

    public void checkRoomAttention(long roomId, OkHttpManager.MyCallBack<ServiceResult<Boolean>> myCallBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        params.put("uid", uid + "");
        params.put("roomId", roomId + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket() + "");
        OkHttpManager.getInstance().postRequest(UriProvider.getCheckRoomAttentionUrl(), params, myCallBack);
    }

    public void doRoomAttention(long roomId, OkHttpManager.MyCallBack<Json> myCallBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        params.put("uid", uid + "");
        params.put("roomId", roomId + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket() + "");
        OkHttpManager.getInstance().postRequest(UriProvider.getRoomAttentionUrl(), params, myCallBack);
    }

    public void doRoomCancelAttention(long roomId, OkHttpManager.MyCallBack<Json> myCallBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        params.put("uid", uid + "");
        params.put("roomId", roomId + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket() + "");
        OkHttpManager.getInstance().postRequest(UriProvider.getRoomCancelAttentionUrl(), params, myCallBack);
    }
}
