package com.tongdaxing.xchat_core.room;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomKickOutEvent;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.tongdaxing.erban.libcommon.coremanager.ICoreClient;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.queue.bean.RoomConsumeInfo;

import java.util.List;

/**
 * Created by zhouxiangfeng on 2017/5/28.
 */

public interface IRoomCoreClient extends ICoreClient {

    String METHOD_ON_ENTER = "onEnter";
    void onEnter(RoomInfo roomInfo);

    String METHOD_ON_BE_KICK_OUT = "onBeKickOut";
    void onBeKickOut(ChatRoomKickOutEvent.ChatRoomKickOutReason reason);

    String METHOD_ON_UPDATE_ROOM_INFO = "onUpdateRoomInfo";
    void onUpdateRoomInfo(RoomInfo roomInfo);

    String METHOD_ON_UPDATE_ROOM_INFO_FAIL = "onUpdateRoomInfoFail";
    void onUpdateRoomInfoFail(String error);

    String METHOD_ON_GET_ROOM_CONSUME_LIST = "onGetRoomConsumeList";
    void onGetRoomConsumeList(List<RoomConsumeInfo> roomConsumeInfos);

    String METHOD_ON_GET_ROOM_CONSUME_LIST_FAIL = "onGetRoomConsumeListFail";
    void onGetRoomConsumeListFail(String msg);

    String METHOD_ON_GET_USER_ROOM = "onGetUserRoom";
    void onGetUserRoom(RoomInfo roomInfo);

    String METHOD_ON_GET_USER_ROOM_FAIL = "onGetUserRoomFail";
    void onGetUserRoomFail(String msg);

    String METHOD_ON_USER_ROOM_IN = "onUserRoomIn";
    void onUserRoomIn();

    String METHOD_ON_USER_ROOM_OUT = "onUserRoomOut";
    void onUserRoomOut();

    String METHOD_ON_CREATE_ROOM_CLICKED = "onCreateRoomClicked";
    void onCreateRoomClicked(long uid);

    String enterError = "enterError";
    void enterError();

    String METHOD_ON_SEND_ROOM_MESSAGE_SUCCESS = "onSendRoomMessageSuccess";
    void onSendRoomMessageSuccess(ChatRoomMessage message);
}
