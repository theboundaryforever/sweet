package com.tongdaxing.xchat_core.room.model;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.netease.nimlib.sdk.chatroom.constant.MemberType;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.room.bean.OnlineChatMember;
import com.tongdaxing.xchat_core.room.bean.ServerUserMemberInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * <p> 轰趴房用户列表网络处理 </p>
 *
 * @author jiahui
 * @date 2017/12/8
 */
public class HomePartyUserListModel extends RoomBaseModel {

    private final String TAG = HomePartyUserListModel.class.getSimpleName();

    public List<OnlineChatMember> getChatRoomMemberList(List<ChatRoomMember> chatRoomMemberList,
                                                        List<OnlineChatMember> oldList, int page) {

        if (!ListUtils.isListEmpty(chatRoomMemberList)) {
            List<ChatRoomMember> list = null;
            LogUtils.d(TAG, "getPageMembers-queryGuestList 第" + page + "页在线人数:"
                    + chatRoomMemberList.size());
            if (!ListUtils.isListEmpty(oldList)) {
                list = new ArrayList<>(oldList.size());
                for (OnlineChatMember temp : oldList) {
                    if (temp.chatRoomMember != null) {
                        list.add(temp.chatRoomMember);
                    }
                }
            }
            return getChatRoomMemberList(list, chatRoomMemberList);
        }
        return new ArrayList<>();
    }


    @NonNull
    private List<OnlineChatMember> getChatRoomMemberList(List<ChatRoomMember> oldList,
                                                         List<ChatRoomMember> newList) {
        long startTime = System.currentTimeMillis();
       /* try {
            Thread.sleep(12000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        //处理耗时的循环--去重操作
        List<OnlineChatMember> allMemberList = OnlineChatMember.coverToOnlineChatMember(oldList, newList);
        List<OnlineChatMember> part1MemberList = dealRoomChatMemberStatus(allMemberList);
        LogUtils.d(TAG, "getChatRoomMemberList 循环处理在线顺序列表耗时：" + (System.currentTimeMillis() - startTime));
        return part1MemberList;
    }

    /**
     * 将接口返回的数据结构转化为本地云信sdk接口返回的数据结构
     *
     * @param serverUserMemberInfos
     * @return
     */
    public List<ChatRoomMember> parseServerMemberInfo2ChatRoomMember(List<ServerUserMemberInfo> serverUserMemberInfos) {
        long startTime = System.currentTimeMillis();
        List<ChatRoomMember> chatRoomMembers = new ArrayList<>();
        if (null != serverUserMemberInfos) {
            for (ServerUserMemberInfo serverUserMemberInfo : serverUserMemberInfos) {
                ChatRoomMember chatRoomMember = new ChatRoomMember();
                chatRoomMember.setRoomId(String.valueOf(serverUserMemberInfo.getRoomid()));
                chatRoomMember.setAccount(serverUserMemberInfo.getAccid());
                chatRoomMember.setMemberType(parseMemberTypeServer2Client(serverUserMemberInfo.getType()));
                chatRoomMember.setMemberLevel(serverUserMemberInfo.getLevel());
                chatRoomMember.setNick(serverUserMemberInfo.getNick());
                chatRoomMember.setAvatar(serverUserMemberInfo.getAvator());
                Map<String, Object> extMap = parseMemberExtServer2Client(
                        serverUserMemberInfo.getExt(), serverUserMemberInfo.getVipId(), serverUserMemberInfo.getVipDate(),
                        serverUserMemberInfo.getIsInvisible() ? 1 : 0, serverUserMemberInfo.getInvisibleUid());
                chatRoomMember.setExtension(extMap);
                chatRoomMember.setOnline(serverUserMemberInfo.isOnlineStat());
                chatRoomMember.setInBlackList(serverUserMemberInfo.isBlacklisted());
                chatRoomMember.setMuted(serverUserMemberInfo.isMuted());
                chatRoomMember.setEnterTime(serverUserMemberInfo.getEnterTime());
                parseChatRoomMemberUpdateTime(extMap, chatRoomMember);
                chatRoomMember.setTempMuted(serverUserMemberInfo.isTempMuted());
                chatRoomMember.setTempMuteDuration(serverUserMemberInfo.getTempMuteTtl());
                chatRoomMembers.add(chatRoomMember);
            }
        }
        LogUtils.d(TAG, "parseServerMemberInfo2ChatRoomMember 循环处理在线列表耗时：" + (System.currentTimeMillis() - startTime));
        return chatRoomMembers;
    }

    /**
     * 将接口返回的数据结构转化为本地云信sdk接口返回的数据结构
     *
     * @param serverUserMemberInfos
     * @return
     */
    public List<OnlineChatMember> parseServerMemberInfo2OnLineChatMember(List<ServerUserMemberInfo> serverUserMemberInfos) {
        long startTime = System.currentTimeMillis();
        List<OnlineChatMember> onlineChatMemberList = new ArrayList<>();
        if (null != serverUserMemberInfos) {
            for (ServerUserMemberInfo serverUserMemberInfo : serverUserMemberInfos) {
                ChatRoomMember chatRoomMember = new ChatRoomMember();
                chatRoomMember.setRoomId(String.valueOf(serverUserMemberInfo.getRoomid()));
                chatRoomMember.setAccount(serverUserMemberInfo.getAccid());
                chatRoomMember.setMemberType(parseMemberTypeServer2Client(serverUserMemberInfo.getType()));
                chatRoomMember.setMemberLevel(serverUserMemberInfo.getLevel());
                chatRoomMember.setNick(serverUserMemberInfo.getNick());
                chatRoomMember.setAvatar(serverUserMemberInfo.getAvator());
                Map<String, Object> extMap = parseMemberExtServer2Client(
                        serverUserMemberInfo.getExt(), serverUserMemberInfo.getVipId(), serverUserMemberInfo.getVipDate(),
                        serverUserMemberInfo.getIsInvisible() ? 1 : 0, serverUserMemberInfo.getInvisibleUid());
                chatRoomMember.setExtension(extMap);
                chatRoomMember.setOnline(serverUserMemberInfo.isOnlineStat());
                chatRoomMember.setInBlackList(serverUserMemberInfo.isBlacklisted());
                chatRoomMember.setMuted(serverUserMemberInfo.isMuted());
                chatRoomMember.setEnterTime(serverUserMemberInfo.getEnterTime());
                parseChatRoomMemberUpdateTime(extMap, chatRoomMember);
                chatRoomMember.setTempMuted(serverUserMemberInfo.isTempMuted());
                chatRoomMember.setTempMuteDuration(serverUserMemberInfo.getTempMuteTtl());
                OnlineChatMember onlineChatMember = new OnlineChatMember(chatRoomMember);
                onlineChatMemberList.add(onlineChatMember);
            }
        }
        LogUtils.d(TAG, "parseServerMemberInfo2ChatRoomMember 循环处理在线列表耗时：" + (System.currentTimeMillis() - startTime));
        return onlineChatMemberList;
    }

    public Map<String, Object> parseMemberExtServer2Client(String ext, int vipId, int vipDate, int isInvisible, long invisibleUid) {
        LogUtils.d(TAG, "parseMemberExtServer2Client-ext:" + ext);
        Map<String, Object> extMap = new HashMap<>();
        if (!TextUtils.isEmpty(ext)) {
            try {
                JSONObject object = new JSONObject(ext);
                Iterator<String> keysIterator = object.keys();
                while (keysIterator.hasNext()) {
                    String key = keysIterator.next();
                    Object value = object.get(key);
                    extMap.put(key, value);
                    LogUtils.d(TAG, "parseMemberExtServer2Client-key:" + key + " value:" + value);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        extMap.put(Constants.USER_MEDAL_ID, vipId);
        extMap.put(Constants.USER_MEDAL_DATE, vipDate);
        extMap.put(Constants.NOBLE_INVISIABLE_ENTER_ROOM, isInvisible);
        extMap.put(Constants.NOBLE_INVISIABLE_UID, invisibleUid);

        return extMap;
    }

    public void parseChatRoomMemberUpdateTime(Map<String, Object> extMap, ChatRoomMember chatRoomMember) {
        if (null != extMap && extMap.containsKey("params")) {
            Object data = extMap.get("params");
            Json json = Json.parse(data + "");
            if (json.has("time")) {
                long updateTime = json.num_l("time");
                LogUtils.d(TAG, "parseChatRoomMemberUpdateTime-updateTime:" + updateTime);
                chatRoomMember.setUpdateTime(updateTime);
            }
        }
    }

    public MemberType parseMemberTypeServer2Client(String type) {
        MemberType memberType = MemberType.UNKNOWN;
        if (TextUtils.isEmpty(type)) {
            return memberType;
        }
        switch (type) {
            case ServerUserMemberInfo.MEMBER_TYPE_UNSET:
                memberType = MemberType.UNKNOWN;
                break;
            case ServerUserMemberInfo.MEMBER_TYPE_LIMITED:
                memberType = MemberType.LIMITED;
                break;
            case ServerUserMemberInfo.MEMBER_TYPE_COMMON:
                memberType = MemberType.NORMAL;
                break;
            case ServerUserMemberInfo.MEMBER_TYPE_CREATOR:
                memberType = MemberType.CREATOR;
                break;
            case ServerUserMemberInfo.MEMBER_TYPE_MANAGER:
                memberType = MemberType.ADMIN;
                break;
            case ServerUserMemberInfo.MEMBER_TYPE_TEMPORARY:
                memberType = MemberType.GUEST;
                break;
            default:
                break;
        }
        return memberType;
    }

    /**
     * 成员进来刷新在线列表
     *
     * @param account           进来成员的账号
     * @param onlineChatMembers 成员列表
     */
    public Single<List<OnlineChatMember>> onMemberInRefreshData(String account, int page,
                                                                final List<OnlineChatMember> onlineChatMembers) {
        if (TextUtils.isEmpty(account)) {
            return Single.error(new Throwable("account 不能为空"));
        }
        List<String> accounts = new ArrayList<>(1);
        accounts.add(account);
        return IMNetEaseManager.get().fetchRoomMembersByIds(accounts)
                .observeOn(Schedulers.io())
                .map(new Function<List<ChatRoomMember>, List<OnlineChatMember>>() {
                    @Override
                    public List<OnlineChatMember> apply(List<ChatRoomMember> chatRoomMemberList) throws Exception {
                        if (ListUtils.isListEmpty(chatRoomMemberList)) {
                            return onlineChatMembers;
                        }
                        return /*getChatRoomMemberList(chatRoomMemberList, OnlineChatMember.converOnlineToNormal(onlineChatMembers))*/onlineChatMembers;
                    }
                })
                .delay(page == Constants.PAGE_START ? 2 : 0, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<List<OnlineChatMember>> onMemberDownUpMic(final String account,
                                                            final boolean isUpMic,
                                                            final List<OnlineChatMember> dataList) {
        if (TextUtils.isEmpty(account)) return Single.error(new Throwable("account 不能为空"));
        return Single.create(
                new SingleOnSubscribe<List<OnlineChatMember>>() {
                    @Override
                    public void subscribe(SingleEmitter<List<OnlineChatMember>> e) throws Exception {
                        if (ListUtils.isListEmpty(dataList)) e.onSuccess(dataList);
                        int size = dataList.size();
                        for (int i = 0; i < size; i++) {
                            OnlineChatMember onlineChatMember = dataList.get(i);
                            if (onlineChatMember.chatRoomMember != null
                                    && Objects.equals(onlineChatMember.chatRoomMember.getAccount(), account)) {
                                onlineChatMember.isOnMic = isUpMic;
                            }
                        }
                        e.onSuccess(dataList);
                    }
                })
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<List<OnlineChatMember>> onUpdateMemberManager(final String account,
                                                                final boolean isRemoveManager,
                                                                final List<OnlineChatMember> dataList) {
        return Single.create(
                new SingleOnSubscribe<List<OnlineChatMember>>() {
                    @Override
                    public void subscribe(SingleEmitter<List<OnlineChatMember>> e) throws Exception {
                        if (ListUtils.isListEmpty(dataList)) e.onSuccess(dataList);
                        int size = dataList.size();
                        for (int i = 0; i < size; i++) {
                            OnlineChatMember onlineChatMember = dataList.get(i);
                            if (onlineChatMember.chatRoomMember != null
                                    && Objects.equals(onlineChatMember.chatRoomMember.getAccount(), account)) {
                                onlineChatMember.isAdmin = !isRemoveManager;
                            }
                        }
                        e.onSuccess(dataList);
                    }
                })
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * 处理接口返回在线用户列表的麦上、管理员等状态信息
     *
     * @param allMemberList 接口返回的在线用户列表
     * @return 处理后的在线用户列表数据
     */
    public List<OnlineChatMember> dealRoomChatMemberStatus(List<OnlineChatMember> allMemberList) {
        if (ListUtils.isListEmpty(allMemberList)) {
            return allMemberList;
        }
        long startTime = System.currentTimeMillis();

        List<OnlineChatMember> part1MemberList = new ArrayList<>();
        List<OnlineChatMember> limitMemberList = new ArrayList<>();
        List<OnlineChatMember> managerMemberList = new ArrayList<>();
        List<OnlineChatMember> normalMemberList = new ArrayList<>();
        List<OnlineChatMember> onMicMemberList = new ArrayList<>();
        List<OnlineChatMember> guestMemberList = new ArrayList<>();

        int size = AvRoomDataManager.get().mMicQueueMemberMap.size();
        boolean isRoomOwnerOnline = false;
        ChatRoomMember chatRoomMember;
        for (OnlineChatMember temp : allMemberList) {
            chatRoomMember = temp.chatRoomMember;
            if (chatRoomMember == null) {
                continue;
            }
            String account = chatRoomMember.getAccount();
            MemberType memberType = chatRoomMember.getMemberType();
            //自己
            if (AvRoomDataManager.get().isOwner(account)) {
                AvRoomDataManager.get().mOwnerMember = chatRoomMember;
            }
            //在麦上集合处理
            boolean isOnMic = false;
            for (int i = 0; i < size; i++) {
                RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().mMicQueueMemberMap.valueAt(i);
                if (roomQueueInfo.mChatRoomMember != null
                        && Objects.equals(roomQueueInfo.mChatRoomMember.getAccount(), account)) {
                    temp.isOnMic = true;
                    if (memberType == MemberType.CREATOR) {
                        isRoomOwnerOnline = true;
                        //房主在麦上
                        temp.isRoomOwer = true;
                        onMicMemberList.add(0, temp);
                    } else if (memberType == MemberType.ADMIN) {
                        //管理员在麦上
                        temp.isAdmin = true;
                        onMicMemberList.add(temp);
                    } else {
                        onMicMemberList.add(temp);
                    }
                    isOnMic = true;
                }
            }
            if (isOnMic) {
                continue;
            }

            //处理不再麦上的
            if (memberType == MemberType.ADMIN) {
                temp.isAdmin = true;
                managerMemberList.add(temp);
            } else if (memberType == MemberType.CREATOR) {
                isRoomOwnerOnline = chatRoomMember.isOnline();
                AvRoomDataManager.get().mRoomCreateMember = chatRoomMember;
            } else if (chatRoomMember.isInBlackList() || chatRoomMember.isMuted()) {
                limitMemberList.add(temp);
            } else if (memberType == MemberType.NORMAL) {
                normalMemberList.add(temp);
            } else if (memberType == MemberType.GUEST) {
                guestMemberList.add(temp);
            }
        }

        //房主
        if (isRoomOwnerOnline) {
            if (AvRoomDataManager.get().mRoomCreateMember != null) {
                if (!ListUtils.isListEmpty(onMicMemberList)
                        && !AvRoomDataManager.get().isRoomOwner(onMicMemberList.get(0).chatRoomMember.getAccount())) {
                    part1MemberList.add(0, new OnlineChatMember(AvRoomDataManager.get().mRoomCreateMember,
                            true, false, true));
                } else {
                    //处理麦上没有人的情况
                    part1MemberList.add(0, new OnlineChatMember(AvRoomDataManager.get().mRoomCreateMember,
                            true, false, true));
                }
            }
        }
        //上麦用户
        if (!ListUtils.isListEmpty(onMicMemberList)) {
            part1MemberList.addAll(onMicMemberList);
        }
        //管理员
        if (!ListUtils.isListEmpty(managerMemberList)) {
            part1MemberList.addAll(managerMemberList);
        }

        //固定在线普通成员
        if (!ListUtils.isListEmpty(normalMemberList)) {
            part1MemberList.addAll(normalMemberList);
        }

        //添加游客
        if (!ListUtils.isListEmpty(guestMemberList)) {
            part1MemberList.addAll(guestMemberList);
        }

        //利用LinkedHashSet不能添加重复数据并能保证添加顺序的特性做快速去重处理
        //参考：https://blog.csdn.net/u012156163/article/details/78338574、http://www.cnblogs.com/chenssy/p/3521565.html
        LinkedHashSet<OnlineChatMember> lhsInfoList = new LinkedHashSet<OnlineChatMember>(part1MemberList.size());
        lhsInfoList.addAll(part1MemberList);
        part1MemberList.clear();
        part1MemberList.addAll(lhsInfoList);

        LogUtils.d(TAG, "dealRoomChatMemberStatus 循环处理在线顺序列表耗时：" + (System.currentTimeMillis() - startTime));
        return part1MemberList;
    }

}
