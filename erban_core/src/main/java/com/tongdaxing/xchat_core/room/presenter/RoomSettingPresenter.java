package com.tongdaxing.xchat_core.room.presenter;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.model.RoomSettingModel;
import com.tongdaxing.xchat_core.room.view.IRoomSettingView;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.List;

import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_CLOSE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_OPEN;

/**
 * <p>  </p>
 *
 * @author jiahui
 * @date 2017/12/15
 */
public class RoomSettingPresenter extends AbstractMvpPresenter<IRoomSettingView> {
    private final RoomSettingModel model;

    public RoomSettingPresenter() {
        model = new RoomSettingModel();
    }

    public void requestTagAll() {
        OkHttpManager.MyCallBack myCallBack = new OkHttpManager.MyCallBack<ServiceResult<List<TabInfo>>>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (getMvpView() != null) {
                    getMvpView().onResultRequestTagAllFail(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<List<TabInfo>> data) {
                IRoomSettingView roomSettingView = getMvpView();
                if (roomSettingView == null) {
                    return;
                }
                if (null != data && data.isSuccess()) {
                    roomSettingView.onResultRequestTagAllSuccess(data.getData());
                } else {
                    roomSettingView.onResultRequestTagAllFail(data.getErrorMessage());
                }
            }
        };

        model.requestTagAll(CoreManager.getCore(IAuthCore.class).getTicket(), myCallBack);
    }

    /**
     * 更新房间设置信息
     *
     * @param title
     * @param desc
     * @param pwd
     * @param label            标签名字
     * @param tagId            标签id
     * @param backPic
     * @param giftEffectParams
     */
    public void updateRoomInfo(String title, String desc, String pwd, String label, int tagId,
                               String backPic, String backName, int giftEffectParams,
                               int charmSwitch, String roomNotice, String playInfo) {
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if ("".equals(title)) {
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(uid);
            if (userInfo != null) {
                title = userInfo.getNick() + "的房间";
            }
        }

        OkHttpManager.MyCallBack myCallBack = new OkHttpManager.MyCallBack<ServiceResult<RoomInfo>>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (getMvpView() != null) {
                    getMvpView().updateRoomInfoFail(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<RoomInfo> data) {
                IRoomSettingView roomSettingView = getMvpView();
                if (roomSettingView == null) {
                    return;
                }
                if (null == data) {
                    roomSettingView.updateRoomInfoFail("");
                    return;
                }
                if (data.isSuccess()) {
                    roomSettingView.updateRoomInfoSuccess(data.getData());
                } else {
                    roomSettingView.updateRoomInfoFail(data.getErrorMessage());
                }
            }
        };

        model.updateRoomInfo(title, desc, pwd, label, tagId, uid,
                CoreManager.getCore(IAuthCore.class).getTicket(), backPic, backName,
                giftEffectParams, charmSwitch, roomNotice, playInfo, myCallBack);
    }

    public boolean checkIsRoomOwner() {
        return model.checkIsRoomOwner();
    }

    /**
     * 礼物特效开启状态变更通知
     *
     * @param originalShowGiftEffect 原始状态
     * @param isShowGiftEffect       更改后的状态
     */
    public void notifyGiftEffectStatusChanged(boolean originalShowGiftEffect, boolean isShowGiftEffect) {
        //通过发送给MessageView，提示用户低价值礼物特效开启状态变更
        if (originalShowGiftEffect != isShowGiftEffect) {
            RoomInfo serverRoomInfo = BaseRoomServiceScheduler.getServerRoomInfo();
            if (serverRoomInfo.getType() == RoomInfo.ROOMTYPE_HOME_PARTY) {
                IMNetEaseManager.get().systemNotificationBySdk(-1, isShowGiftEffect ?
                                CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_OPEN :
                                CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_CLOSE,
                        -1, 0, 0, false, null);
            }
            if (serverRoomInfo.getType() == RoomInfo.ROOMTYPE_SINGLE_AUDIO ||serverRoomInfo.getType() == RoomInfo.ROOMTYPE_MULTI_AUDIO||serverRoomInfo.getType() == RoomInfo.ROOMTYPE_NEW_AUCTION) {
                IMRoomMessageManager.get().systemNotificationBySdk(
                        CoreManager.getCore(IAuthCore.class).getCurrentUid(),
                        isShowGiftEffect ? CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_OPEN :
                                CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_CLOSE, -1);
            }
        }
    }
}
