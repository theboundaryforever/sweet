package com.tongdaxing.xchat_core.room.bean;


import android.os.Parcel;
import android.os.Parcelable;


/**
 *  搜索-房间-推荐热门房间
 *
 * @author weihaitao
 * @date 2017/5/24
 */

public class SearchRoomPersonInfo implements Parcelable {


    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public long getErbanNo() {
        return erbanNo;
    }

    public void setErbanNo(long erbanNo) {
        this.erbanNo = erbanNo;
    }

    public int getFansNum() {
        return fansNum;
    }

    public void setFansNum(int fansNum) {
        this.fansNum = fansNum;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public boolean isHasPrettyErbanNo() {
        return hasPrettyErbanNo;
    }

    public void setHasPrettyErbanNo(boolean hasPrettyErbanNo) {
        this.hasPrettyErbanNo = hasPrettyErbanNo;
    }

    public boolean isFollow() {
        return isFollow;
    }

    public void setFollow(boolean follow) {
        isFollow = follow;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    /**
     * avatar : string
     * erbanNo : 0
     * fansNum : 0
     * gender : 0
     * hasPrettyErbanNo : true
     * isFollow : true
     * nick : string
     * roomId : 0
     * title : string
     * type : 0
     * uid : 0
     * valid : true
     */

    private String avatar;
    private long erbanNo;
    private int fansNum;
    private int gender;
    private boolean hasPrettyErbanNo;
    private boolean isFollow;
    private String nick;
    private long roomId;
    private String title;
    private int type;
    private long uid;
    private boolean valid;

    @Override
    public String toString() {
        return "SearchRoomPersonInfo{" +
                "avatar='" + avatar + '\'' +
                ", erbanNo=" + erbanNo +
                ", fansNum=" + fansNum +
                ", gender=" + gender +
                ", hasPrettyErbanNo=" + hasPrettyErbanNo +
                ", isFollow=" + isFollow +
                ", nick='" + nick + '\'' +
                ", roomId=" + roomId +
                ", title='" + title + '\'' +
                ", type=" + type +
                ", uid=" + uid +
                ", valid=" + valid +
                ", onlineNum=" + onlineNum +
                '}';
    }

    public int getOnlineNum() {
        return onlineNum;
    }

    public void setOnlineNum(int onlineNum) {
        this.onlineNum = onlineNum;
    }

    private int onlineNum;

    public SearchRoomPersonInfo(Parcel in) {
        if(null == in){
            return;
        }
        uid = in.readLong();
        roomId = in.readLong();
        erbanNo = in.readLong();
        valid = in.readByte() != 0;
        avatar = (String) in.readString();
        nick = (String) in.readString();
        title = (String) in.readString();
        fansNum = in.readInt();
        gender = in.readInt();
        type = in.readInt();
        hasPrettyErbanNo = in.readByte() != 0;
        isFollow = in.readByte() != 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(uid);
        dest.writeLong(roomId);
        dest.writeLong(erbanNo);
        dest.writeByte((byte) (valid ? 1 : 0));
        dest.writeByte((byte) (hasPrettyErbanNo ? 1 : 0));
        dest.writeByte((byte) (isFollow ? 1 : 0));
        dest.writeString(avatar);
        dest.writeString(nick);
        dest.writeString(title);
        dest.writeInt(fansNum);
        dest.writeInt(gender);
        dest.writeInt(type);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<SearchRoomPersonInfo> CREATOR = new Creator<SearchRoomPersonInfo>() {
        @Override
        public SearchRoomPersonInfo createFromParcel(Parcel in) {
            return new SearchRoomPersonInfo(in);
        }

        @Override
        public SearchRoomPersonInfo[] newArray(int size) {
            return new SearchRoomPersonInfo[size];
        }
    };
}
