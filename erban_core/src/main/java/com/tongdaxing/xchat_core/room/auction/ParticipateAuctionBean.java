package com.tongdaxing.xchat_core.room.auction;

import com.tongdaxing.erban.libcommon.base.BaseAppBean;

/**
 * 文件描述：参加拍卖的信息实体
 *
 * @auther：zwk
 * @data：2019/7/29
 */
public class ParticipateAuctionBean extends BaseAppBean {

    private String avatar;
    private String charmLevelPic;
    private int day;
    private String experLevelPic;
    private int gender;
    private String nick;
    private int cardNum;//
    private String project;
    private long roomId;
    private long uid;


    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getCharmLevelPic() {
        return charmLevelPic;
    }

    public void setCharmLevelPic(String charmLevelPic) {
        this.charmLevelPic = charmLevelPic;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public String getExperLevelPic() {
        return experLevelPic;
    }

    public void setExperLevelPic(String experLevelPic) {
        this.experLevelPic = experLevelPic;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getCardNum() {
        return cardNum;
    }

    public void setCardNum(int cardNum) {
        this.cardNum = cardNum;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }
}
