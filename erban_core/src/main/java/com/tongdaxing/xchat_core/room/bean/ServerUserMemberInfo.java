package com.tongdaxing.xchat_core.room.bean;

import android.text.TextUtils;

public class ServerUserMemberInfo {

    //未设置
    public static final String MEMBER_TYPE_UNSET = "UNSET";
    //受限用户，黑名单或禁言
    public static final String MEMBER_TYPE_LIMITED = "LIMITED";
    //普通固定成员
    public static final String MEMBER_TYPE_COMMON = "COMMON";
    //创建者
    public static final String MEMBER_TYPE_CREATOR = "CREATOR";
    //管理员
    public static final String MEMBER_TYPE_MANAGER = "MANAGER";
    //临时用户,非固定成员
    public static final String MEMBER_TYPE_TEMPORARY = "TEMPORARY";


    /**
     * roomid : 111
     * accid : abc
     * nick : abc
     * avator : http://nim.nos.netease.com/MTAxMTAwMg==/bmltYV8xNzg4NTA1NF8xNDU2Mjg0NDQ3MDcyX2E4NmYzNWI5LWRhYWEtNDRmNC05ZjU1LTJhMDUyMGE5MzQ4ZA==
     * ext : ext
     * type : MANAGER
     * level : 2
     * onlineStat : true
     * enterTime : 1487145487971
     * time : 1552990567607
     * blacklisted : true
     * muted : true
     * tempMuted : true
     * tempMuteTtl : 120
     * isRobot : true
     * robotExpirAt : 120
     */

    /**
     * type-角色类型：
     * UNSET（未设置），
     * LIMITED（受限用户，黑名单或禁言），
     * COMMON（普通固定成员），
     * CREATOR（创建者），
     * MANAGER（管理员），
     * TEMPORARY（临时用户,非固定成员）
     */

    private long roomid;
    private String accid;
    private String nick;
    private String avator;
    private String ext;
    private String type;
    private int level;
    private boolean onlineStat;

    private long time;

    private long enterTime;
    private int vipId;
    private int vipDate;
    private long invisibleUid;
    private boolean isInvisible;

    @Override
    public String toString() {
        return "ServerUserMemberInfo{" +
                "roomid=" + roomid +
                ", accid='" + accid + '\'' +
                ", nick='" + nick + '\'' +
                ", avator='" + avator + '\'' +
                ", ext='" + ext + '\'' +
                ", type='" + type + '\'' +
                ", level=" + level +
                ", onlineStat=" + onlineStat +
                ", enterTime=" + enterTime +
                ", time=" + time +
                ", blacklisted=" + blacklisted +
                ", muted=" + muted +
                ", tempMuted=" + tempMuted +
                ", tempMuteTtl=" + tempMuteTtl +
                ", isRobot=" + isRobot +
                ", robotExpirAt=" + robotExpirAt +
                ", vipId=" + vipId +
                ", vipDate=" + vipDate +
                ", invisibleUid=" + invisibleUid +
                ", isInvisible=" + isInvisible +
                '}';
    }
    private boolean blacklisted;
    private boolean muted;
    private boolean tempMuted;
    private int tempMuteTtl;
    private boolean isRobot;
    private int robotExpirAt;

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public boolean isInvisible() {
        return isInvisible;
    }

    public void setInvisible(boolean invisible) {
        isInvisible = invisible;
    }

    public int getVipId() {
        return vipId;
    }

    public void setVipId(int vipId) {
        this.vipId = vipId;
    }

    public int getVipDate() {
        return vipDate;
    }

    public void setVipDate(int vipDate) {
        this.vipDate = vipDate;
    }

    public long getInvisibleUid() {
        return invisibleUid;
    }

    public void setInvisibleUid(long invisibleUid) {
        this.invisibleUid = invisibleUid;
    }

    public boolean getIsInvisible() {
        return isInvisible;
    }

    public void setIsInvisible(boolean isInvisible) {
        this.isInvisible = isInvisible;
    }

    public long getRoomid() {
        return roomid;
    }

    public void setRoomid(long roomid) {
        this.roomid = roomid;
    }

    public String getAccid() {
        return accid;
    }

    public void setAccid(String accid) {
        this.accid = accid;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getAvator() {
        return avator;
    }

    public void setAvator(String avator) {
        this.avator = avator;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public boolean isOnlineStat() {
        return onlineStat;
    }

    public void setOnlineStat(boolean onlineStat) {
        this.onlineStat = onlineStat;
    }

    public long getEnterTime() {
        return enterTime;
    }

    public void setEnterTime(long enterTime) {
        this.enterTime = enterTime;
    }

    public boolean isBlacklisted() {
        return blacklisted;
    }

    public void setBlacklisted(boolean blacklisted) {
        this.blacklisted = blacklisted;
    }

    public boolean isMuted() {
        return muted;
    }

    public void setMuted(boolean muted) {
        this.muted = muted;
    }

    public boolean isTempMuted() {
        return tempMuted;
    }

    public void setTempMuted(boolean tempMuted) {
        this.tempMuted = tempMuted;
    }

    public int getTempMuteTtl() {
        return tempMuteTtl;
    }

    public void setTempMuteTtl(int tempMuteTtl) {
        this.tempMuteTtl = tempMuteTtl;
    }

    public boolean isIsRobot() {
        return isRobot;
    }

    public void setIsRobot(boolean isRobot) {
        this.isRobot = isRobot;
    }

    public int getRobotExpirAt() {
        return robotExpirAt;
    }

    public void setRobotExpirAt(int robotExpirAt) {
        this.robotExpirAt = robotExpirAt;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ServerUserMemberInfo)) {
            return false;
        }
        boolean isSameGift = false;
        ServerUserMemberInfo other = (ServerUserMemberInfo) obj;
        if (!TextUtils.isEmpty(other.getAccid()) && other.getAccid().equals(this.getAccid())) {
            isSameGift = true;
        }
        return isSameGift;
    }
}
