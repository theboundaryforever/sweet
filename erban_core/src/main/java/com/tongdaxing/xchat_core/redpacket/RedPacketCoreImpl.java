package com.tongdaxing.xchat_core.redpacket;

import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.tongdaxing.erban.libcommon.coremanager.AbstractBaseCore;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RedPacketAttachment;
import com.tongdaxing.xchat_core.im.message.IIMMessageCoreClient;
import com.tongdaxing.xchat_core.result.RedPacketResult;
import com.tongdaxing.xchat_core.result.WithdrawRedListResult;
import com.tongdaxing.xchat_core.result.WithdrawRedSucceedResult;

import java.util.List;
import java.util.Map;

/**
 * Created by ${Seven} on 2017/9/20.
 */

public class RedPacketCoreImpl extends AbstractBaseCore implements IRedPacketCore {
    public RedPacketCoreImpl() {
        CoreManager.addClient(this);
    }

    //获取红包页面数据
    @Override
    public void getRedPacketInfo() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));

        OkHttpManager.getInstance().getRequest(UriProvider.getRedPacket(), params, new OkHttpManager.MyCallBack<RedPacketResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IRedPacketCoreClient.class, IRedPacketCoreClient.METHOD_ON_GET_RED_INFO_ERROR, e.getMessage());
            }

            @Override
            public void onResponse(RedPacketResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IRedPacketCoreClient.class, IRedPacketCoreClient
                                .METHOD_ON_GET_RED_INFO, response.getData());
                    } else {
                        notifyClients(IRedPacketCoreClient.class, IRedPacketCoreClient
                                .METHOD_ON_GET_RED_INFO_ERROR, response.getMessage());
                    }
                }
            }
        });
    }

    @Override
    public void getRedList() {
        OkHttpManager.getInstance().getRequest(UriProvider.getRedBagList(),
                CommonParamUtil.getDefaultParam(),
                new OkHttpManager.MyCallBack<WithdrawRedListResult>() {
                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                        notifyClients(IRedPacketCoreClient.class, IRedPacketCoreClient.METHOD_ON_GET_RED_LIST_ERROR, e.getMessage());
                    }

                    @Override
                    public void onResponse(WithdrawRedListResult response) {
                        if (null != response) {
                            if (response.isSuccess()) {
                                notifyClients(IRedPacketCoreClient.class, IRedPacketCoreClient
                                        .METHOD_ON_GET_RED_LIST, response.getData());
                            } else {
                                notifyClients(IRedPacketCoreClient.class, IRedPacketCoreClient
                                        .METHOD_ON_GET_RED_LIST_ERROR, response.getMessage());
                            }
                        }
                    }
                });
    }

    @Override
    public void getRedWithdraw(long uid, int packetId) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));
        params.put("packetId", String.valueOf(packetId));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());

        OkHttpManager.getInstance().postRequest(UriProvider.getRedWithdraw(), params, new OkHttpManager.MyCallBack<WithdrawRedSucceedResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                notifyClients(IRedPacketCoreClient.class, IRedPacketCoreClient.METHOD_ON_GET_WITHDRAW_ERROR, e.getMessage());
            }

            @Override
            public void onResponse(WithdrawRedSucceedResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        notifyClients(IRedPacketCoreClient.class, IRedPacketCoreClient
                                .METHOD_ON_GET_WITHDRAW, response.getData());
                    } else {
                        notifyClients(IRedPacketCoreClient.class, IRedPacketCoreClient
                                .METHOD_ON_GET_WITHDRAW_ERROR, response.getMessage());
                    }
                }
            }
        });
    }

    @CoreEvent(coreClientClass = IIMMessageCoreClient.class)
    public void onReceivePersonalMessages(List<IMMessage> imMessages) {
        if (imMessages != null && imMessages.size() > 0) {
            for (IMMessage msg : imMessages) {
                if (msg.getMsgType() == MsgTypeEnum.custom) {
                    CustomAttachment attachment = (CustomAttachment) msg.getAttachment();
                    if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PACKET) {
                        RedPacketAttachment redPacketAttachment = (RedPacketAttachment) msg.getAttachment();
                        notifyClients(IRedPacketCoreClient.class, IRedPacketCoreClient.METHOD_ON_RECEIVE_NEW_PACKET, redPacketAttachment.getRedPacketInfo());
                    }
                }
            }
        }
    }
}
