package com.tongdaxing.xchat_core.redpacket;

import com.tongdaxing.erban.libcommon.coremanager.IBaseCore;

/**
 * Created by ${Seven} on 2017/9/20.
 */

public interface IRedPacketCore extends IBaseCore {
    //获取红包页面信息
    void getRedPacketInfo();

    //获取红包提现列表
    void getRedList();

    //发起红包提现
    void getRedWithdraw(long uid, int packetId);
}
