package cn.bingoogolapple.photopicker.widget;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class UserPhotoBean implements Parcelable {


    private long pid;
    //图片地址
    private String photoUrl;


    protected UserPhotoBean(Parcel in) {
        pid = in.readLong();
        photoUrl = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(pid);
        dest.writeString(photoUrl);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserPhotoBean> CREATOR = new Creator<UserPhotoBean>() {
        @Override
        public UserPhotoBean createFromParcel(Parcel in) {
            return new UserPhotoBean(in);
        }

        @Override
        public UserPhotoBean[] newArray(int size) {
            return new UserPhotoBean[size];
        }
    };

    public long getPid() {
        return pid;
    }

    public void setPid(long pid) {
        this.pid = pid;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public UserPhotoBean(long pid, String photoUrl) {
        this.pid = pid;
        this.photoUrl = photoUrl;
    }

    public UserPhotoBean() {

    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof UserPhotoBean)) {
            return false;
        }
        boolean isSameObj = false;
        UserPhotoBean other = (UserPhotoBean) obj;
        if (!TextUtils.isEmpty(other.getPhotoUrl()) && !TextUtils.isEmpty(this.getPhotoUrl()) ) {
            isSameObj = other.getPhotoUrl().equals(this.getPhotoUrl());
        }else if(other.pid != 0 && other.pid == this.pid){
            isSameObj = true;
        }
        return isSameObj;
    }


}
