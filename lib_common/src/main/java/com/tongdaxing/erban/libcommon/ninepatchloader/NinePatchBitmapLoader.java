package com.tongdaxing.erban.libcommon.ninepatchloader;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.NinePatchDrawable;
import android.text.TextUtils;
import android.view.View;

import com.jakewharton.disklrucache.DiskLruCache;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.ThreadUtil;
import com.tongdaxing.erban.libcommon.utils.codec.MD5Utils;
import com.tongdaxing.erban.libcommon.utils.file.StorageUtils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * .9图bitmap的三级缓存管理器
 * 1. 内存缓存，采用LruCache
 * 2. 磁盘缓存，采用DiskCache
 *
 * @author weihaitao
 * @date 2019/6/19
 */
public class NinePatchBitmapLoader {

    private static final String TAG = NinePatchBitmapLoader.class.getSimpleName();

    //默认磁盘缓存最大10M
    private static final int defaultDiskLruCacheSize = 1024 * 1024 * 10;

    //默认写入磁盘缓存的buffer大小
    private static final int defaultBufferSize = 8 * 1024;
    private static NinePatchBitmapLoader instance = null;
    /**
     * 内存缓存
     */
    private BitmapLruCache mBitmapLruCache;
    /**
     * 磁盘缓存
     */
    private DiskLruCache mDiskLruCache;
    private HashMap<String, WeakReference<View>> mViews = new HashMap<>();
    private LinkedList<String> mDownloadingUrls = new LinkedList<String>();

    private NinePatchBitmapLoader() {

    }

    public static NinePatchBitmapLoader getInstance() {
        if (null == instance) {
            instance = new NinePatchBitmapLoader();
        }

        return instance;
    }

    /**
     * 初始化方法
     *
     * @param diskCacheDir 磁盘缓存路径[我们这里不采用网络上的检测sd卡的方式，而是直接指定路径]
     */
    private void init(Context context, String diskCacheDir) {
        LogUtils.d(TAG, "init-diskCacheDir:" + diskCacheDir);
        mBitmapLruCache = new BitmapLruCache();
        try {
            mDiskLruCache = DiskLruCache.open(setDiskCacheDir(context, diskCacheDir),
                    1, 1, defaultDiskLruCacheSize);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 对外的初始化方法
     *
     * @param context
     */
    public void init(Context context) {
        LogUtils.d(TAG, "init");
        init(context, context.getPackageName().concat(File.separator).concat("cache"));
    }

    private File setDiskCacheDir(Context context, String rootDir) {
        File file = StorageUtils.getCacheDirectory(context, rootDir);
        if (file != null && !file.exists()) {
            boolean mkResult = file.mkdirs();
            LogUtils.d(TAG, "setDiskCacheDir-mkResult:" + mkResult);
        }
        return file;
    }

    /**
     * 加载点9图到view，注意这里可以通过外部view.setTag(URL)来防止错位
     *
     * @param view         可以为空，空表示提前加载的场景
     * @param url
     * @param defaultResId 如果内存缓存和磁盘缓存均没有bigmap缓存，那么需要另起线程下载，
     *                     由于基本用在房间MessageView中，故这类场景下直接加载默认图
     */
    public void loadNinePatchBitmap(View view, final String url, int defaultResId) {
        LogUtils.d(TAG, "loadNinePatchBitmap-url:" + url);
        if (TextUtils.isEmpty(url)) {
            return;
        }

        //先从内存缓存中获取
        if (null != mBitmapLruCache && null != mBitmapLruCache.get(url)) {
            if (null != view && GlideContextCheckUtil.checkContextUsable(view.getContext())) {
                Bitmap bmp = mBitmapLruCache.get(url);
                NinePatchDrawable ninePatchDrawable = NinePatchDrawableFactory.convertBitmap(
                        view.getContext().getResources(), bmp, null);
                view.setBackgroundDrawable(ninePatchDrawable);
                LogUtils.d(TAG, "loadNinePatchBitmap-内存中加载完毕");
            }
            return;
        }

        //再从磁盘缓存中获取
        if (null != mDiskLruCache) {
            try {
                String urlMd5 = MD5Utils.getMD5String(url);
                LogUtils.d(TAG, "loadNinePatchBitmap-urlMd5:" + urlMd5);
                if (null != mDiskLruCache && null != mDiskLruCache.get(urlMd5)) {
                    DiskLruCache.Snapshot snapshot = mDiskLruCache.get(urlMd5);
                    if (snapshot != null) {
                        Bitmap bitmap = BitmapFactory.decodeStream(snapshot.getInputStream(0));
                        // 存入LruCache缓存
                        if (null != mBitmapLruCache) {
                            LogUtils.d(TAG, "loadNinePatchBitmap 磁盘缓存中获取并写入内存缓存中");
                            mBitmapLruCache.put(url, bitmap);
                        }
                        if (null != view && GlideContextCheckUtil.checkContextUsable(view.getContext())) {
                            NinePatchDrawable ninePatchDrawable = NinePatchDrawableFactory.convertBitmap(
                                    view.getContext().getResources(), bitmap, null);
                            view.setBackgroundDrawable(ninePatchDrawable);
                            LogUtils.d(TAG, "loadNinePatchBitmap-磁盘缓存中加载完毕");
                        }
                        return;
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        //最后如果二级缓存都没有，则直接显示默认图
        if (null != view && GlideContextCheckUtil.checkContextUsable(view.getContext()) && defaultResId > 0) {
            view.setBackgroundDrawable(view.getContext().getResources().getDrawable(defaultResId));
            LogUtils.d(TAG, "loadNinePatchBitmap-准备网络下载，并加载默认图");
        }
        loadNinePatchBitmapFromServer(view, url);
    }

    /**
     * 下载并缓存点9图
     *
     * @param view
     * @param url
     */
    private void loadNinePatchBitmapFromServer(final View view, final String url) {
        LogUtils.d(TAG, "loadNinePatchBitmapFromServer-url:" + url);
        if (null != view) {
            view.setTag(url);
            mViews.put(url, new WeakReference<View>(view));
        }
        //而后另起线程下载
        if (null != mDownloadingUrls && !mDownloadingUrls.contains(url)) {
            LogUtils.d(TAG, "loadNinePatchBitmapFromServer-开启网络加载模式");
            mDownloadingUrls.add(url);
            ThreadUtil.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    Bitmap bitmap = null;
                    InputStream is = null;
                    BufferedInputStream in = null;
                    BufferedOutputStream out = null;
                    URL mURL = null;
                    URLConnection urlConnection = null;
                    DiskLruCache.Editor editor = null;
                    try {
                        mURL = new URL(url);
                        urlConnection = mURL.openConnection();
                        urlConnection.setConnectTimeout(5000);
                        urlConnection.setReadTimeout(5000);
                        is = urlConnection.getInputStream();
                        bitmap = BitmapFactory.decodeStream(is);

                        // 存入LruCache缓存
                        if (null != mBitmapLruCache) {
                            LogUtils.d(TAG, "loadNinePatchBitmap 网络获取并写入内存缓存中");
                            mBitmapLruCache.put(url, bitmap);
                        }

                        //存入磁盘缓存
                        if (null != mDiskLruCache) {
                            String urlMd5 = MD5Utils.getMD5String(url);
                            LogUtils.d(TAG, "loadNinePatchBitmap 网络获取并写入磁盘缓存中");
                            editor = mDiskLruCache.edit(urlMd5);
                            in = new BufferedInputStream(is, defaultBufferSize);
                            OutputStream outputStream = editor.newOutputStream(0);
                            out = new BufferedOutputStream(outputStream, defaultBufferSize);
                            int b;
                            while ((b = in.read()) != -1) {
                                out.write(b);
                            }
                            editor.commit();
                        }

                        if (null != mDownloadingUrls) {
                            LogUtils.d(TAG, "loadNinePatchBitmap 网络获取并移除状态");
                            mDownloadingUrls.remove(url);
                        }

                        //UI线程刷新
                        ThreadUtil.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (null != mBitmapLruCache && null != mBitmapLruCache.get(url)
                                        && null != mViews && null != mViews.get(url) && null != mViews.get(url).get()) {
                                    Bitmap bmp = mBitmapLruCache.get(url);
                                    View mView = mViews.get(url).get();
                                    if (url.equals(mView.getTag()) && GlideContextCheckUtil.checkContextUsable(mView.getContext())) {
                                        NinePatchDrawable ninePatchDrawable = NinePatchDrawableFactory.convertBitmap(
                                                mView.getContext().getResources(), bmp, null);
                                        mView.setBackgroundDrawable(ninePatchDrawable);
                                        LogUtils.d(TAG, "loadNinePatchBitmap 网络获取并重新从内存缓存中加载完毕");
                                    }
                                    mViews.remove(url);
                                }
                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                        if (null != editor) {
                            try {
                                editor.abort();
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }
                        }
                    } finally {
                        try {
                            mDownloadingUrls.remove(url);
                            if (out != null) {
                                out.close();
                            }
                            if (in != null) {
                                in.close();
                            }
                            if (null != is) {
                                is.close();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } else {
            LogUtils.d(TAG, "loadNinePatchBitmapFromServer-正在加载过程中");
        }
    }
}
