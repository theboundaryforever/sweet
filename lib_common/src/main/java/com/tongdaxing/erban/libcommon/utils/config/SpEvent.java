package com.tongdaxing.erban.libcommon.utils.config;

/**
 * Created by Administrator on 2018/3/7.
 */

public interface SpEvent {
    String linkedMeShareUid = "linkedMeShareUid";
    String linkedMeChannel = "linkedMeChannel";
    String onKickRoomInfo = "onKickRoomId";

    String roomUid = "roomUid";
    String roomType = "roomType";
    String time = "time";
    String cache_uid = "cache_uid";
    String not_hot_menu = "not_hot_menu";
    String config_key = "config_key";
    String search_friend_history = "search_friend_history";
    String search_room_history = "search_room_history";
    String search_hot_music_history = "search_hot_music_history";
    String search_room_hot = "search_room_hot";
    String share_fans_history = "share_fans_history";
    String sprout_permission_sure = "sprout_permission_sure";
    String has_set_alias = "has_set_alias";

    String cleckLoginTime = "cleckLoginTime";

    String shouldShowNoteGiftConfirmDialog = "should_show_note_gift_confirm_dialog";

    String messageIgnoreCache = "message_ignore_cache";
}
