package com.tongdaxing.erban.libcommon.utils;

import java.util.ArrayDeque;
import java.util.concurrent.Executor;

import static android.os.AsyncTask.THREAD_POOL_EXECUTOR;

/**
 * 原生rom的SerialExecutor上对run的crash做处理
 * （针对vivo的rom修改了AbsListView类导致crash，这里是crash的上层，在这里处理）
 */
public class SafeSerialExecutor implements Executor {
    final ArrayDeque<Runnable> mTasks = new ArrayDeque<Runnable>();
    Runnable mActive;

    public synchronized void execute(final Runnable r) {
        mTasks.offer(new Runnable() {
            public void run() {
                try {
                    r.run();
                } catch (Exception e) {//这里处理crash
                    e.printStackTrace();
                } finally {
                    scheduleNext();
                }
            }
        });
        if (mActive == null) {
            scheduleNext();
        }
    }

    protected synchronized void scheduleNext() {
        if ((mActive = mTasks.poll()) != null) {
            THREAD_POOL_EXECUTOR.execute(mActive);
        }
    }
}
