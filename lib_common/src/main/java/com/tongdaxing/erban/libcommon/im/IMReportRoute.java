package com.tongdaxing.erban.libcommon.im;

public interface IMReportRoute {
    //通知
    String ChatRoomMemberBlackRemove = "ChatRoomMemberBlackRemove";
    String ChatRoomMemberBlackAdd = "ChatRoomMemberBlackAdd";
    String chatRoomMemberIn = "chatRoomMemberIn";
    String ChatRoomTip = "ChatRoomTip";
    String kickoff = "kickoff";
    String chatRoomMemberExit = "chatRoomMemberExit";
    String ChatRoomInfoUpdated = "ChatRoomInfoUpdated";
    String ChatRoomMemberKicked = "ChatRoomMemberKicked";
    String QueueMemberUpdateNotice = "QueueMemberUpdateNotice";
    String AudioConnMicQueueUpdateNotice = "AudioConnMicQueueUpdateNotice";
    String QueueMicUpdateNotice = "QueueMicUpdateNotice";
    String AudioConnMicQueueMicUpdateNotice = "AudioConnMicQueueMicUpdateNotice";
    String sendMessageReport = "sendMessageReport";
    String updateQueue = "updateQueue";
    String ChatRoomManagerAdd = "ChatRoomManagerAdd";
    String ChatRoomManagerRemove = "ChatRoomManagerRemove";
    String sendTextReport = "sendTextReport";
    String enterPublicRoom = "enterPublicRoom";
    String sendPublicMsgNotice = "sendPublicMsgNotice";
    String sendPublicMsg = "sendPublicMsg";
    String passmicNotice = "passmicNotice";
    String requestNoRoomUserNotice = "requestNoRoomUserNotice";//邀请进房
    String applyWaitQueueNotice = "applyWaitQueueNotice";//申请上麦

}
