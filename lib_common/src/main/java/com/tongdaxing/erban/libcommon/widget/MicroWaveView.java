package com.tongdaxing.erban.libcommon.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.ColorInt;
import android.util.AttributeSet;
import android.view.View;

import com.juxiao.library_utils.DisplayUtils;
import com.tongdaxing.erban.libcommon.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by huangmeng1 on 2018/1/13.
 */

public class MicroWaveView extends View {
    private Paint paint;
    private List<Float> radus = new ArrayList<>();
    private List<Integer> alphaList = new ArrayList<>();
    private boolean isStarting = false;
    private int second;
    private float size = 56f;
    private float distance = 4;

    public MicroWaveView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public MicroWaveView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public MicroWaveView(Context context) {
        super(context);
        init(context, null);
    }

    private void init(Context context, AttributeSet attrs) {
        paint = new Paint();
        paint.setColor(0x55ffffff);
        distance = DisplayUtils.dip2px(context, 4);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.MicroWaveView);
        if (typedArray != null) {
            size = typedArray.getDimension(R.styleable.MicroWaveView_w_radius, 56);
            typedArray.recycle();
        }
        radus.add(size);
        alphaList.add(180);
    }

    public void setColor(@ColorInt int color){
        if (paint != null){
            paint.setColor(color);
        }
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!isStarting) return;
        second++;
        for (int i = 0; i < radus.size(); i++) {
            paint.setAlpha(alphaList.get(i));
            canvas.drawCircle(getWidth() / 2, getHeight() / 2, radus.get(i), paint);
            if (radus.get(i) <= (size + distance * 3)) {
                radus.set(i, radus.get(i) + 0.5f);
            }
            if (alphaList.get(i) > 10) {
                alphaList.set(i, alphaList.get(i) - 1);
            }
        }
        if (radus.get(0) == (size + distance * 2)) {
            radus.add(0, size);
            alphaList.add(0, 180);
        }
        if (radus.get(0) == size && radus.size() > 1) {
            radus.remove(1);
            alphaList.remove(1);
        }
        if (second >= size) {
            stop();
        }
        invalidate();
    }

    public void start() {
        second = 0;
        isStarting = true;
        invalidate();
    }

    public void stop() {
        radus.clear();
        alphaList.clear();
        alphaList.add(180);
        second = 0;
        radus.add(size);
        isStarting = false;
    }
}
