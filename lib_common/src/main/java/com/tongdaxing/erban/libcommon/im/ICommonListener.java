package com.tongdaxing.erban.libcommon.im;

public interface ICommonListener {
    void onDisconnectCallBack(IMErrorBean error);

    void onNoticeMessage(String message);
}
