package com.tongdaxing.erban.libcommon.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;

import com.tmxq.ndklib.JniUtils;
import com.tongdaxing.erban.libcommon.BuildConfig;
import com.tongdaxing.erban.libcommon.utils.codec.MD5Utils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by polo on 2018/6/27.
 */

public class SignUtils {

    private static final String TAG = SignUtils.class.getSimpleName();

    private static String signKey;

    static {
        //接口签名密钥 参数1-context，参数2 [1--debug 0--release]
        signKey = JniUtils.getDk(BasicConfig.INSTANCE.getAppContext(), BuildConfig.IS_DEBUG ? 1 : 0);
        LogUtils.d("SignUtils", "static signKey:" + signKey);
    }

    public static String getSign(String url, Map<String, String> params, String key, String t) {
        LogUtils.d(TAG, "getSign-url:" + url + " key:" + key + " t:" + t);
        Map<String, String> paramsMap = url2Map(url);
        if (params != null) {
            paramsMap.putAll(params);
        }
        if (t != null) {
            paramsMap.put("t", t);
        }

        StringBuffer signStringBuffer = new StringBuffer();
        if (paramsMap != null && paramsMap.size() > 0) {
            for (Map.Entry<String, String> entry : paramsMap.entrySet()) {
                //请求参数是在url地址中的如果encode后发送，需要decode解码在加密，和后台一致（最好应该是直接对参数加密后再在发送因为地址中取不到问题所以不能这样）
                String entryKey = entry.getKey();
                String entryValue = entry.getValue();
                LogUtils.d(TAG, "getSign-entryKey:" + entryKey + " entryValue:" + entryValue);
                //278及后续版本，去掉URLDecoder.decode操作
                String str = entryKey + "=" + entryValue;
                LogUtils.d(TAG, "getSign-str:" + str);
                signStringBuffer.append(str);
            }
        }
        signStringBuffer.append(key);
        LogUtils.d(TAG, "getSign-params:" + signStringBuffer.toString());

        String sign = "";
        try {
            sign = MD5Utils.getMD5String(signStringBuffer.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        LogUtils.d(TAG, "getSign-sign1:" + sign);
        if (sign.length() > 7) {
            sign = sign.substring(0, 7);
        }
        LogUtils.d(TAG, "getSign-sign2:" + sign);
        return sign;
    }


    public static String getSign(String url, Map<String, String> params, String t) {
        return getSign(url, params, signKey, t);

    }

    public static Map<String, String> url2Map(String param) {

        Map<String, String> map = new TreeMap<String, String>(new Comparator<String>() {

            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }

        });

        if (TextUtils.isEmpty(param)) {
            return map;
        }

        String[] urlparams = param.split("\\?");
        if (urlparams != null && urlparams.length == 2) {
            param = urlparams[1];
        } else {
            return map;
        }


        String[] params = param.split("&");
        for (int i = 0; i < params.length; i++) {
            String[] p = params[i].split("=");
            if (p.length == 2) {
                map.put(p[0], p[1]);
            } else if (p.length == 1) {
                map.put(p[0], "");
            }
        }
        return map;
    }

    /**
     * 获取当前包签名的hashcode
     */
    public static int getPackageSignatureHashCode(Context context, String packageName) {
        PackageManager pm = context.getPackageManager();
        List<PackageInfo> apps = pm.getInstalledPackages(PackageManager.GET_SIGNATURES);
        for (PackageInfo info : apps) {
            if (info.packageName.equals(packageName)) {
                return info.signatures[0].hashCode();
            }
        }
        return -1;
    }

}
