package com.tongdaxing.erban.libcommon.http_image.util;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;

import com.tongdaxing.erban.libcommon.utils.AppMetaDataUtil;
import com.tongdaxing.erban.libcommon.utils.NetworkUtils;
import com.tongdaxing.erban.libcommon.utils.SiemulatorCheckUtil;
import com.tongdaxing.erban.libcommon.utils.TelephonyUtils;
import com.tongdaxing.erban.libcommon.utils.VersionUtil;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CommonParamUtil {

    public static Map<String, String> getDefaultParam() {
        return getDefaultParam(new HashMap<String, String>());
    }

    public static Map<String, String> getDefaultParam(Map<String, String> hashMapParam) {
        if (hashMapParam == null) {
            hashMapParam = new HashMap<>();
        }
        hashMapParam.put("os", "android");
        hashMapParam.put("osVersion", Build.VERSION.RELEASE);
        hashMapParam.put("app", "TMXQ_AN");
        hashMapParam.put("ispType", String.valueOf(getIspType()));
        hashMapParam.put("netType", String.valueOf(getNetworkType()));
        hashMapParam.put("model", getPhoneModel());
        hashMapParam.put("appVersion", VersionUtil.getLocalName(BasicConfig.INSTANCE.getAppContext()));
        hashMapParam.put("appCode", VersionUtil.getVersionCode(BasicConfig.INSTANCE.getAppContext()) + "");
        hashMapParam.put("deviceId", DeviceUuidFactory.getDeviceId(BasicConfig.INSTANCE.getAppContext()));
        hashMapParam.put("channel", AppMetaDataUtil.getChannelID(BasicConfig.INSTANCE.getAppContext()));
        hashMapParam.put("imei", TelephonyUtils.getIMEI(BasicConfig.INSTANCE.getAppContext()));
        if(!TextUtils.isEmpty(SiemulatorCheckUtil.getCurrSimulatorEnv())){
            hashMapParam.put("simulator", SiemulatorCheckUtil.getCurrSimulatorEnv());
        }
        return hashMapParam;
    }


    public static JSONObject getCommmParams() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("os", "android");
            jsonObject.put("osVersion", Build.VERSION.RELEASE);
            jsonObject.put("app", "TMXQ_AN");
            jsonObject.put("ispType", String.valueOf(getIspType()));
            jsonObject.put("netType", String.valueOf(getNetworkType()));
            jsonObject.put("model", getPhoneModel());
            jsonObject.put("appVersion", VersionUtil.getLocalName(BasicConfig.INSTANCE.getAppContext()));
            jsonObject.put("appCode", VersionUtil.getVersionCode(BasicConfig.INSTANCE.getAppContext()) + "");
            jsonObject.put("deviceId", DeviceUuidFactory.getDeviceId(BasicConfig.INSTANCE.getAppContext()));
            jsonObject.put("channel", AppMetaDataUtil.getChannelID(BasicConfig.INSTANCE.getAppContext()));
            jsonObject.put("imei", TelephonyUtils.getIMEI(BasicConfig.INSTANCE.getAppContext()));
            if (!TextUtils.isEmpty(SiemulatorCheckUtil.getCurrSimulatorEnv())) {
                jsonObject.put("simulator", SiemulatorCheckUtil.getCurrSimulatorEnv());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jsonObject;
    }

    public static Map<String, String> getDefaultHeaders(Context context) {
        Map<String, String> param = new HashMap<String, String>(10);
        return param;
    }

    public static Map<String, String> getDefaultHeaders() {
        Map<String, String> param = new HashMap<String, String>(10);
        return param;
    }

    /**
     * 获取是否nettype字段
     *
     * @return
     */
    public static int getNetworkType() {
        if (NetworkUtils.getNetworkType(BasicConfig.INSTANCE.getAppContext()) == NetworkUtils.NET_WIFI) {
            return 2;
        } else {
            return 1;
        }
    }

    /**
     * 获取运营商字段
     *
     * @return
     */
    public static int getIspType() {
        String isp = NetworkUtils.getOperator(BasicConfig.INSTANCE.getAppContext());
        int ispType = 4;
        if (isp.equals(NetworkUtils.ChinaOperator.CMCC)) {
            ispType = 1;
        } else if (isp.equals(NetworkUtils.ChinaOperator.UNICOM)) {
            ispType = 2;
        } else if (isp.equals(NetworkUtils.ChinaOperator.CTL)) {
            ispType = 3;
        }

        return ispType;
    }

    /**
     * 获取手机型号
     *
     * @return
     */
    public static String getPhoneModel() {
        return Build.MODEL;
    }
}
