package com.tongdaxing.erban.libcommon.utils;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;

import java.io.File;
import java.io.FileFilter;

/**
 * 模拟器检测工具类
 */
public class SiemulatorCheckUtil {

    private static final String TAG = SiemulatorCheckUtil.class.getSimpleName();

    public static final String SIMULATOR_ZHENJI = "zhenji";
    public static final String SIMULATOR_XIAOYAO = "xiaoyao";
    public static final String SIMULATOR_YESHEN = "yeshen";
    public static final String SIMULATOR_TENCENT = "tencent";
    public static final String SIMULATOR_LEIDIAN = "leidian";
    public static final String SIMULATOR_MUMU = "mumu";
    public static final String SIMULATOR_TIANTIAN = "tiantian";
    public static final String SIMULATOR_BLUESTACKS = "bluestacks";
    public static final String SIMULATOR_HAIMA = "haima";

    public static String getCurrSimulatorEnv() {
        return currSimulatorEnv;
    }

    /**
     * 当前运行环境，标识是否为模拟器，及具体为哪种模拟器
     */
    private static String currSimulatorEnv = "";

    private static final int DEVICEINFO_UNKNOWN = 0;

    private static final FileFilter CPU_FILTER = new FileFilter() {
        @Override
        public boolean accept(File pathname) {
            String path = pathname.getName();
            //regex is slow, so checking char by char.
            if (path.startsWith("cpu")) {
                for (int i = 3; i < path.length(); i++) {
                    if (path.charAt(i) < '0' || path.charAt(i) > '9') {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
    };

    /**
     * 检测当前App运行环境是否为模拟器
     * <p>
     * 如果是模拟器，那么crashReport中设置对应参数，方便后续bugly上的bug跟踪定位
     *
     * @param context
     */
    public static String chechSiemulatorEnv(Context context) {

        LogUtils.d(TAG, "chechSiemulatorEnv-----------start------>");
        long startTime = System.currentTimeMillis();
        LogUtils.d(TAG, "chechSiemulatorEnv-Build.BRAND:" + Build.BRAND);
        LogUtils.d(TAG, "chechSiemulatorEnv-Build.FINGERPRINT:" + Build.FINGERPRINT);
        LogUtils.d(TAG, "chechSiemulatorEnv-Build.DEVICE:" + Build.DEVICE);
        LogUtils.d(TAG, "chechSiemulatorEnv-Build.MODEL :" + Build.MODEL);
        LogUtils.d(TAG, "chechSiemulatorEnv-Build.PRODUCT:" + Build.PRODUCT);
        LogUtils.d(TAG, "chechSiemulatorEnv-Build.MANUFACTURER:" + Build.MANUFACTURER);
        LogUtils.d(TAG, "chechSiemulatorEnv-Build.HARDWARE:" + Build.HARDWARE);

        //懒加载
        if (!TextUtils.isEmpty(currSimulatorEnv)) {
            LogUtils.d(TAG, "chechSiemulatorEnv-currSimulatorEnv:" + currSimulatorEnv);
            LogUtils.d(TAG, "chechSiemulatorEnv-----------end ,total time:" + (System.currentTimeMillis() - startTime));
            return currSimulatorEnv;
        }

        //逍遥模拟器  com.microvirt.launcher、com.microvirt.installer、com.microvirt.market、
        //          com.microvirt.tools、com.microvirt.guide、com.microvirt.download、
        //          om.microvirt.memuime
        //夜神模拟器  com.vphone.launcher、com.vphone.helper
        //腾讯手游助手 com.tencent.tinput
        //雷电模拟器  com.android.launcher3、com.android.flysilkworm
        //木木模拟器  com.netease.nemu_vinput.nemu、com.mumu.launcher、com.mumu.store
        //天天模拟器  com.kop.zkop、com.tiantian.ime
        //蓝叠模拟器  com.bluestacks.home、com.bluestacks.BstCommandProcessor
        //海马模拟器  me.haima.androidassist、com.example.android.softkeyboard、
        //          com.example.android.livecubes、me.haima.helpcenter、com.haimawan.push
        if (InstallUtil.isAppInstalled(context, "com.microvirt.launcher")
                || InstallUtil.isAppInstalled(context, "com.microvirt.market")) {
            currSimulatorEnv = SIMULATOR_XIAOYAO;
        } else if (InstallUtil.isAppInstalled(context, "com.vphone.helper")
                || InstallUtil.isAppInstalled(context, "com.vphone.launcher")) {
            currSimulatorEnv = SIMULATOR_YESHEN;
        } else if (InstallUtil.isAppInstalled(context, "com.tencent.tinput")) {
            currSimulatorEnv = SIMULATOR_TENCENT;
        } else if (InstallUtil.isAppInstalled(context, "com.android.launcher3")
                || InstallUtil.isAppInstalled(context, "com.android.flysilkworm")) {
            currSimulatorEnv = SIMULATOR_LEIDIAN;
        } else if (InstallUtil.isAppInstalled(context, "com.mumu.launcher")
                || InstallUtil.isAppInstalled(context, "com.mumu.store")) {
            currSimulatorEnv = SIMULATOR_MUMU;
        } else if (InstallUtil.isAppInstalled(context, "com.tiantian.ime")
                || InstallUtil.isAppInstalled(context, "com.kop.zkop")) {
            //测试基于3.2.0版本，3.2.6版本因模拟器启动失败，暂未测试
            currSimulatorEnv = SIMULATOR_TIANTIAN;
        } else if (!TextUtils.isEmpty(Build.FINGERPRINT) && Build.FINGERPRINT.toUpperCase().contains("KOT49H")) {
            //参考：https://blog.csdn.net/nihaoqiulinhe/article/details/71723945
            //下面这个方案，在最新版本上检测失效，不论开没开root权限，相关的包名应用获取不到
//            InstallUtil.isAppInstalled(context, "com.bluestacks.home")
//                    || InstallUtil.isAppInstalled(context, "com.bluestacks.BstCommandProcessor")
            currSimulatorEnv = SIMULATOR_BLUESTACKS;
        } else if (InstallUtil.isAppInstalled(context, "me.haima.androidassist")
                || InstallUtil.isAppInstalled(context, "com.example.android.softkeyboard")) {
            currSimulatorEnv = SIMULATOR_HAIMA;
        } else {
            currSimulatorEnv = SIMULATOR_ZHENJI;
        }
        LogUtils.d(TAG, "chechSiemulatorEnv-currSimulatorEnv:" + currSimulatorEnv);
        LogUtils.d(TAG, "chechSiemulatorEnv-----------end ,总耗时:" + (System.currentTimeMillis() - startTime));
        return currSimulatorEnv;
    }

    public static String getSiemulatorCrashReportValue() {
        if (TextUtils.isEmpty(currSimulatorEnv)) {
            return "真机|其他模拟器";
        }

        if (currSimulatorEnv.equals(SIMULATOR_XIAOYAO)) {
            return "逍遥模拟器";
        } else if (currSimulatorEnv.equals(SIMULATOR_YESHEN)) {
            return "夜神模拟器";
        } else if (currSimulatorEnv.equals(SIMULATOR_TENCENT)) {
            return "腾讯手游助手";
        } else if (currSimulatorEnv.equals(SIMULATOR_LEIDIAN)) {
            return "雷电模拟器";
        } else if (currSimulatorEnv.equals(SIMULATOR_MUMU)) {
            return "木木模拟器";
        } else if (currSimulatorEnv.equals(SIMULATOR_TIANTIAN)) {
            return "天天模拟器";
        } else if (currSimulatorEnv.equals(SIMULATOR_BLUESTACKS)) {
            return "蓝叠BS模拟器";
        } else if (currSimulatorEnv.equals(SIMULATOR_HAIMA)) {
            return "海马模拟器";
        } else {
            return "真机|其他模拟器";
        }
    }

    /**
     * 获取CPU核心数量
     * 参考：
     *
     * @return
     */
    public static int getNumberOfCPUCores() {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
            // Gingerbread doesn't support giving a single application access to both cores, but a
            // handful of devices (Atrix 4G and Droid X2 for example) were released with a dual-core
            // chipset and Gingerbread; that can let an app in the background run without impacting
            // the foreground application. But for our purposes, it makes them single core.
            return 1;
        }
        int cores;
        try {
            cores = new File("/sys/devices/system/cpu/").listFiles(CPU_FILTER).length;
        } catch (SecurityException e) {
            e.printStackTrace();
            cores = DEVICEINFO_UNKNOWN;
        } catch (NullPointerException e) {
            e.printStackTrace();
            cores = DEVICEINFO_UNKNOWN;
        } catch (Exception ex) {
            ex.printStackTrace();
            cores = DEVICEINFO_UNKNOWN;
        }
        return cores;
    }
}
