package com.tongdaxing.erban.libcommon.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author weihaitao
 * @date 2019/6/12
 */
public class SortList<E> {

    public void Sort(List<E> list, final String method, final String sort, final Class<?>... paramTypes) {
        Collections.sort(list, new Comparator<E>() {
            @Override
            public int compare(E e1, E e2) {
                int ret = 0;
                try {
                    Method m1 = e1.getClass().getMethod(method, paramTypes);
                    Method m2 = e2.getClass().getMethod(method, paramTypes);
                    if ("desc".equals(sort)) {
                        // 倒序
                        ret = m2.invoke((e2), paramTypes).toString()
                                .compareTo(m1.invoke((e1), paramTypes).toString());
                    } else {
                        // 正序
                        ret = m1.invoke((e1), paramTypes).toString()
                                .compareTo(m2.invoke((e2), paramTypes).toString());
                    }
                } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException ne) {
                    System.out.println(ne);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return ret;
            }
        });
    }

}
