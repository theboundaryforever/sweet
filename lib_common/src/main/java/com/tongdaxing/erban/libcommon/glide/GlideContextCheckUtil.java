package com.tongdaxing.erban.libcommon.glide;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Looper;
import android.support.v4.app.FragmentActivity;

import com.tongdaxing.erban.libcommon.utils.LogUtils;

public class GlideContextCheckUtil {

    private static final String TAG = GlideContextCheckUtil.class.getSimpleName();

    /**
     * 检查GlideApp.with(context)中的context是否可用，
     * 避免出现类似You cannot start a load for a destroyed activity这样的异常
     *
     * 分析参考:https://blog.csdn.net/loners_/article/details/73521968
     *
     * @param context
     * @return true可用，false不可用
     */
    public static boolean checkContextUsable(Context context){
        boolean usable = true;

        if (null != context && Looper.myLooper() == Looper.getMainLooper() && !(context instanceof Application)){
            if (context instanceof FragmentActivity) {
                FragmentActivity activity = (FragmentActivity)context;
                usable = !activity.isDestroyed();
            } else if (context instanceof Activity) {
                Activity activity = (Activity)context;
                usable = !activity.isDestroyed();
            } else if (context instanceof ContextWrapper) {
                ContextWrapper contextWrapper = ((ContextWrapper) context);
                Context newContext = contextWrapper.getBaseContext();
                usable =  checkContextUsable(newContext);
            }
        } else {
            usable = false;
        }
        LogUtils.d(TAG,"checkContextUsable-usable:"+usable);
        return usable;
    }


}
