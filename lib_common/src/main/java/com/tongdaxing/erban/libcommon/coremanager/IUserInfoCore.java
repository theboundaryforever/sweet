package com.tongdaxing.erban.libcommon.coremanager;

import com.tongdaxing.erban.libcommon.utils.json.Json;

public interface IUserInfoCore extends IBaseCore {

    String BANNED_ALL = "0";
    String BANNED_ROOM = "1";
    String BANNED_P2P = "2";
    String BANNED_PUBLIC_ROOM = "3";

    boolean checkHasBandPhone();

    int getVersion();

    String getSensitiveWord();

    Json getBannedMap();

    /**
     * @param force 是否强制刷新
     */
    void checkBanned(boolean force);
}
