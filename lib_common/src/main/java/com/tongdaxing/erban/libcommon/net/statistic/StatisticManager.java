package com.tongdaxing.erban.libcommon.net.statistic;

import android.content.Context;

import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.Map;

/**
 * <p> 统计接口管理 </p>
 *
 * @author jiahui
 * @date 2018/1/4
 */
public class StatisticManager {

    private final String TAG = StatisticManager.class.getSimpleName();

    private static final Object SYNC_OBJ = new Object();
    private volatile static StatisticManager mInstance;

    private StatisticManager() {
    }

    public static StatisticManager get() {
        if (mInstance == null) {
            synchronized (SYNC_OBJ) {
                if (mInstance == null) {
                    mInstance = new StatisticManager();
                }
            }
        }
        return mInstance;
    }

    public void onPause(Context context) {
        MobclickAgent.onPause(context);
    }

    public void onResume(Context context) {
        MobclickAgent.onResume(context);
    }

    public void onPageStart(String pageName) {
        MobclickAgent.onPageStart(pageName);
    }

    public void onPageEnd(String pageName) {
        MobclickAgent.onPageEnd(pageName);
    }

    /**
     * 计数事件--需要传参
     *
     * @param context
     * @param eventId   事件ID
     * @param arguments 参数集合
     */
    public void onEvent(Context context, String eventId, Map<String, String> arguments) {
        LogUtils.d(TAG, "onEvent-eventId:" + eventId);
        MobclickAgent.onEvent(context, eventId, arguments);
    }

    /**
     * 计数事件
     * @param context
     * @param eventId 事件ID
     * @param eventLabel 事件描述
     */
    public void onEvent(Context context, String eventId, String eventLabel) {
        LogUtils.d(TAG,"onEvent-eventId:"+eventId+" eventLabel:"+eventLabel);
        MobclickAgent.onEvent(context,eventId,eventLabel);
    }
}
