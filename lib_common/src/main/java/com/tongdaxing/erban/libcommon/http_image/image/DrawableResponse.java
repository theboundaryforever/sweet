package com.tongdaxing.erban.libcommon.http_image.image;

import android.graphics.drawable.Drawable;

/**
 * Drawable请求结果
 *
 * @author zhongyongsheng on 14-9-11.
 */
public class DrawableResponse {

    public Drawable drawable;
    public DrawableRequest request;

}
