package com.tongdaxing.erban.libcommon.im;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.tmxq.ndklib.JniUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtil;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.erban.libcommon.utils.ThreadUtil;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.erban.libcommon.utils.json.Json;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_6455;
import org.java_websocket.framing.CloseFrame;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.util.Hashtable;
import java.util.Iterator;

public class SocketManager {

    //线上IM环境
    private final static String PRODUCT_WS_URL = "wss://www.pinjin88.com:8828";
    //线下IM环境
    private final static String DEBUG_WS_URL = "ws://47.94.149.86:3016";

    //网络状态
    public static final int NET_TYPE_CONNECTED = 1;
    public static final int NET_TYPE_CONNECTING = 2;
    public static final int NET_TYPE_CLOSED = 0;
    public static final long TIMEOUT_TIME = 9000;
    //断开原因
    public static final int CALL_BACK_CODE_REASON_TIMEOUT = 408;
    public static final int CALL_BACK_CODE_DISCONNECT = 409;
    public static final int CALL_BACK_CODE_SELFCLOSE = 410; // 手动主动关闭
    //请求超时
    public static final int CHECK_CALL_BACK_TIMEOUT = 0;
    //private final String wsUrl = "ws://39.105.187.28:3006/";
    //private final String wssUrl = "wss://39.105.22.242:8816/";
    //链接回调
    public static final int IM_CONNECT_SUCCESS = 1;
    public static final int IM_CONNECT_ERROR = 2;
    //接受到IM信息通知
    public static final int IM_CONNECT_MSG = 3;
    //WebSocketClient断开
    public static final int IM_CONNECT_DISCONNECT = 4;
    public static final int IM_CODE_KICK_OFF = -100;
    public static final int IM_CODE_NO_NEED = -101;
    public static final int IM_CODE_HEARTBEAT = -102;
    public static final int CLOSE_HEART_BEAT_TIME_OUT = 777;
    public static final int CLOSE_STOP_SOCKET = 778;
    private static final long HEART_BEAT_RATE = 5 * 1000;//每隔5秒进行一次对长连接的心跳检测
    private static Hashtable<Integer, IMCallBack> callBackHashtable = new Hashtable<>();

    private final String TAG = SocketManager.class.getSimpleName();
    private Runnable connectRunnable = new Runnable() {
        @Override
        public void run() {
            ThreadUtil.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    status = NET_TYPE_CONNECTING;
                    try {
                        webSocketClient = new WebSocketClient(new URI(getWsRootUrl()), new Draft_6455(), null, 15000) {
                            @Override
                            public void onOpen(ServerHandshake handshakedata) {
                                log("onOpen");
                                onConnect(handshakedata);
                            }

                            @Override
                            public void onMessage(String message) {
                                LogUtils.d(TAG, "onMessage-message:" + message);
                                Json result = null;
                                try {
                                    Json json = Json.parse(message);
                                    message = JniUtils.decryptAes(BasicConfig.INSTANCE.getAppContext(), json.str("ed"));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                result = Json.parse(message);
                                onMsg(result == null ? -1 : result.num("id"), getImEventCode(result), result);
                                log("onNoticeMessage:" + message);
                            }

                            @Override
                            public void onClose(int code, String reason, boolean remote) {
                                status = NET_TYPE_CLOSED;
                                log("onClose   " + code + "  " + reason + " " + remote);
                                onDisconnect(code, reason, remote);
                            }

                            @Override
                            public void onError(Exception ex) {
                                status = NET_TYPE_CLOSED;
                                onErr(ex);
                                log("onError:" + ex);
                            }

                        };
                        webSocketClient.connect();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    //当前网络状态
    private int status = NET_TYPE_CLOSED;
    //socket断开的原因
    private int closeReason = CALL_BACK_CODE_DISCONNECT;
    private WebSocketClient webSocketClient = null;
    private IConnectListener iConnectListener;
    private ICommonListener iCommonListener;

    private IMHeartBeatDataListener imHeartBeatDataListener;

    public void setImHeartBeatDataListener(IMHeartBeatDataListener imHeartBeatDataListener) {
        this.imHeartBeatDataListener = imHeartBeatDataListener;
    }

    @SuppressLint("HandlerLeak")
    private Handler socketActionHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            //超时移除
            if (msg.what == CHECK_CALL_BACK_TIMEOUT) {
                int eventId = (Integer) msg.obj;
                IMCallBack remove = callBackHashtable.remove(eventId);
                if (remove != null) {
                    remove.onError(CALL_BACK_CODE_REASON_TIMEOUT, "timeout");
                }
            } else if (msg.what == IM_CONNECT_SUCCESS) {
                status = NET_TYPE_CONNECTED;
                if (iConnectListener != null) {
                    iConnectListener.onSuccess((ServerHandshake) msg.obj);
                    iConnectListener = null;
                }
            } else if (msg.what == IM_CONNECT_ERROR) {
                status = NET_TYPE_CLOSED;
                if (iConnectListener != null) {
                    iConnectListener.onError((Exception) msg.obj);
                    iConnectListener = null;
                }
            } else if (msg.what == IM_CONNECT_MSG) {
                String message = (String) msg.obj;
                //成功回调
                callBackResponse(msg.arg1, message);
            } else if (msg.what == IM_CONNECT_DISCONNECT) {
                status = NET_TYPE_CLOSED;
                clearMap();
                if (iCommonListener != null) {
                    iCommonListener.onDisconnectCallBack((IMErrorBean) msg.obj);
                }
            }
        }
    };
    private boolean hasStartHeartBeat = false;
    // 发送心跳包
    private Runnable heartBeatRunnable = new Runnable() {
        @Override
        public void run() {
//            if (System.currentTimeMillis() - sendTime >= HEART_BEAT_RATE) {
            if (isConnected()) {
                //链接状态发心跳包，报错就断开链接
                send(getHeartBeatData(), new IMCallBack() {
                    @Override
                    public void onSuccess(String data) {
                        log("onSuccess:data = " + data);
                        if (TextUtils.isEmpty(data)) {
                            heartBeatTimeOut();
                        }
                    }

                    @Override
                    public void onError(int errorCode, String errorMsg) {
                        log("onError:errorCode = " + errorCode + " , errorMsg = " + errorMsg);
                        heartBeatTimeOut();
                    }
                });
            } else {
                //重连写到业务逻辑
            }
//                sendTime = System.currentTimeMillis();
//            }
            //手动或者服务器断开要停掉
            socketActionHandler.postDelayed(this, HEART_BEAT_RATE);//每隔一定的时间，对长连接进行一次心跳检测
        }
    };

    public static String getWsRootUrl() {
        return BasicConfig.isDebug ? DEBUG_WS_URL : PRODUCT_WS_URL;
    }

    public SocketManager() {

    }

    private void log(String content) {
        LogUtil.d("socket_action", content);
    }

    private int getImEventCode(Json json) {
        String route = json.str(IMKey.route);
        if (route.equals(IMReportRoute.kickoff)) {
            return IM_CODE_KICK_OFF;
        }
        if (route.equals("notciefromsvr")) {
            return IM_CODE_NO_NEED;
        }
        if (route.equals(IMSendRoute.heartbeat)) {
            return IM_CODE_HEARTBEAT;
        }
        return 0;
    }

    private void onErr(Exception ex) {
        if (null == socketActionHandler) {
            return;
        }
        Message message = new Message();
        message.what = IM_CONNECT_ERROR;
        message.obj = ex;
        socketActionHandler.sendMessage(message);
    }

    private void onDisconnect(int code, String reason, boolean remote) {
        if (null == socketActionHandler) {
            return;
        }
        Message message = new Message();
        message.what = IM_CONNECT_DISCONNECT;
        IMErrorBean errorBean = new IMErrorBean();
        errorBean.setCode(code);
        errorBean.setReason(reason);
        errorBean.setRemote(remote);
        errorBean.setCloseReason(closeReason);
        message.obj = errorBean;
        socketActionHandler.sendMessage(message);
    }

    private void clearMap() {
        if (callBackHashtable == null || callBackHashtable.size() == 0) {
            return;
        }

        Iterator<IMCallBack> iterator = callBackHashtable.values().iterator();
        while (iterator.hasNext()) {
            log("send:clearMap timeout");
            iterator.next().onError(CALL_BACK_CODE_REASON_TIMEOUT, "网络异常，连接超时");
        }
        callBackHashtable.clear();
    }

    private void onConnect(ServerHandshake handshakedata) {
        if (null == socketActionHandler) {
            return;
        }
        Message message = new Message();
        message.what = IM_CONNECT_SUCCESS;
        message.obj = handshakedata;
        socketActionHandler.sendMessage(message);

        if (!hasStartHeartBeat) {
            hasStartHeartBeat = true;
            socketActionHandler.removeCallbacks(heartBeatRunnable);
            socketActionHandler.postDelayed(heartBeatRunnable, HEART_BEAT_RATE);  // 连接成功才启动心跳  只启动一次
        }
    }

    private void onMsg(int callBackId, int imEventCode, Json message) {
        if (status == NET_TYPE_CONNECTING || status == NET_TYPE_CLOSED) {
            return;
        }
        //im事件
        imEvent(imEventCode, message, callBackId);
    }

    private void imEvent(int imEventCode, final Json message, int callBackId) {
        if (imEventCode == IM_CODE_KICK_OFF) {
            log("kickoff");
            if (null != socketActionHandler) {
                //强制被踢的通知
                socketActionHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (iCommonListener != null) {
                            iCommonListener.onNoticeMessage(message == null ? "kickoff" : message.toString());
                        }
                    }
                });
            }
            disconnect();
            return;
        } else if (imEventCode == IM_CODE_NO_NEED) {
            return;
        }
        Message m = new Message();
        m.what = IM_CONNECT_MSG;
        m.obj = message.toString();
        m.arg1 = callBackId;
        if (null != socketActionHandler) {
            socketActionHandler.sendMessage(m);
        }
    }

    private void callBackResponse(int eventId, String message) {
        if (message == null) {
            return;
        }
        boolean isRes = message.contains("res_data");
        IMCallBack iimCallBack = null;
        //还有res_data是回调通知
        if (isRes) {
            iimCallBack = callBackHashtable.remove(eventId);
        }
        if (iimCallBack != null) {
            iimCallBack.onSuccess(message);
        } else {
            // 没找到处理的回调  说明可能是主动推消息需要外部处理(也可能超时后收到的消息 也外部处理)
            if (iCommonListener != null) {
                iCommonListener.onNoticeMessage(message);
            }
        }
    }

    private void heartBeatTimeOut() {
        log("heartBeatTimeOut");
        status = NET_TYPE_CLOSED;
        closeReason = CALL_BACK_CODE_REASON_TIMEOUT;
        closeSocket(CLOSE_HEART_BEAT_TIME_OUT);
    }

    private void closeSocket(int closeCode) {
        switch (closeCode) {
            case CLOSE_HEART_BEAT_TIME_OUT:
                break;
            case CLOSE_STOP_SOCKET:
                break;
            default:
        }
        try {
            log("closeSocket:closeCode = " + closeCode);
            if (webSocketClient != null) {
                webSocketClient.closeConnection(CloseFrame.ABNORMAL_CLOSE, "abnormal close");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getHeartBeatData() {
        Json reqData = null;
        if (null != imHeartBeatDataListener) {
            try {
                reqData = new Json(imHeartBeatDataListener.getHeartBeatData());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return IMModelFactory.get().createRequestData(IMSendRoute.heartbeat, reqData).toString();
    }

    /**
     * 注册断开回调函数  告诉业务层断开( code 告诉我们时超时导致还是网络导致断开还是手动断开)
     * 注册服务器单向推消息处理回调
     *
     * @param iCommonListener
     */
    public void setiCommonListener(ICommonListener iCommonListener) {
        this.iCommonListener = iCommonListener;
    }

    //-------------------------------------------对外开放的方法-------------------------------------------------------------------

    /**
     * 手动断开链接
     */
    public void disconnect() {
        log("disconnect");
        status = NET_TYPE_CLOSED;
        closeReason = CALL_BACK_CODE_SELFCLOSE;
        closeSocket(CLOSE_STOP_SOCKET);
    }

    public void destroy() {
        log("destroy");
        if (socketActionHandler != null) {
            socketActionHandler.removeCallbacksAndMessages(null);
        }
        status = NET_TYPE_CLOSED;
        closeReason = CALL_BACK_CODE_SELFCLOSE;
        clearMap();
        closeSocket(CLOSE_STOP_SOCKET);
    }

    /**
     * @return 是否已连接
     */
    public boolean isConnected() {
        return status == NET_TYPE_CONNECTED;
    }

    /**
     * @return 是否在连接中
     */
    public boolean isConnecting() {
        return status == NET_TYPE_CONNECTING;
    }

    private boolean connect(int delay) {
        log("connect");
        status = NET_TYPE_CONNECTING;
        //socketActionHandler.post(connectRunnable);
        this.closeSocket(CLOSE_STOP_SOCKET);
        if (null != socketActionHandler) {
            socketActionHandler.removeCallbacks(connectRunnable);
            socketActionHandler.postDelayed(connectRunnable, delay);
        }
        return true;
    }

    /**
     * 链接socket
     *
     * @param iConnectListener 链接结果回调
     * @return
     */
    public boolean connect(IConnectListener iConnectListener) {
        return this.connect(iConnectListener, 0);
    }

    /**
     * @return 链接状态
     */
    public boolean isConnect() {
        return status == NET_TYPE_CONNECTED;

    }

    /**
     * @param iConnectListener
     * @param delay            延迟connect 毫秒
     * @return
     */
    public boolean connect(IConnectListener iConnectListener, int delay) {
        if (status == NET_TYPE_CONNECTING || status == NET_TYPE_CONNECTED) {
            if (iConnectListener != null) {
                iConnectListener.onError(new Exception("Dubble connect!"));
            }
            return false;
        }
        closeReason = CALL_BACK_CODE_DISCONNECT;// 重置当前断开原因
        this.iConnectListener = iConnectListener;
        return connect(delay);
    }

    /**
     * 对服务器发送消息
     *
     * @param content    发送内容
     * @param imCallBack 结果的回调
     * @return
     */
    public void send(String content, @NonNull IMCallBack imCallBack) {
        LogUtils.d(TAG, "send-content:" + content);
        Json json = Json.parse(content);
        json.set("id", imCallBack.getCallbackId());
        json.set("timeFlag", System.currentTimeMillis());
        log("send:" + json.toString());
        String result = "";
        try {
            result = JniUtils.encryptAes(BasicConfig.INSTANCE.getAppContext(), json.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (StringUtils.isEmpty(result)) {
            LogUtil.d("send:result is empty");
            imCallBack.onError(-1, IMError.IM_MS_SEND_ERROR + ":result is empty");
            return;
        }
        Json sendContent = new Json();
        sendContent.set("ed", result);
        content = sendContent.toString();
        if (status == NET_TYPE_CONNECTED && webSocketClient != null) {
            if (!webSocketClient.isOpen()) {
                log("send:result is not open");
                imCallBack.onError(-1, IMError.IM_MS_SEND_ERROR + ":socket is not open");
                return;
            }
            try {
                //发送消息
                webSocketClient.send(content);
                //注册回调
            } catch (Exception e) {
                e.printStackTrace();
                log("send:" + e.getMessage());
                imCallBack.onError(-1, IMError.IM_MS_SEND_ERROR + "e.getMessage()");
                return;
            }
            if (null != callBackHashtable) {
                callBackHashtable.put(imCallBack.getCallbackId(), imCallBack);
            }
            //超时判断
            Message message = new Message();
            message.what = CHECK_CALL_BACK_TIMEOUT;
            message.obj = imCallBack.getCallbackId();
            if (null != socketActionHandler) {
                socketActionHandler.sendMessageDelayed(message, TIMEOUT_TIME);
            }
        } else {
            log("send:status != NET_TYPE_CONNECTED || webSocketClient == null    status = " + status);
            if (webSocketClient == null) {
                imCallBack.onError(-1, IMError.IM_MS_SEND_ERROR + ":webSocketClient is null");
            }else {
                imCallBack.onError(-1, IMError.IM_MS_SEND_ERROR + ":status = " + status);
            }
        }
    }
}
