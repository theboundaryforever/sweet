/**
 * Core对象工厂。使用getCore前需要注册对应core接口实现类
 * 此类是非线程安全的，必须在主线程调用
 */
package com.tongdaxing.erban.libcommon.coremanager;


import android.app.Application;
import android.content.Context;

import com.tongdaxing.erban.libcommon.BuildConfig;
import com.tongdaxing.erban.libcommon.http_image.image.ImageManager;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;

import java.io.File;
import java.lang.reflect.Field;
import java.util.HashMap;

/**
 * @author daixiang
 */
public class CoreFactory {

    private static final HashMap<Class<? extends IBaseCore>, IBaseCore> cores;
    private static final HashMap<Class<? extends IBaseCore>, Class<? extends AbstractBaseCore>> coreClasses;
    /**
     * 标识coreManager是否已经初始化完毕
     * 静态变量，如果设置了为true，那么在进程被后台杀死的情况下重新拉起，那么该值也会重新被创建为false
     */
    private static boolean hasCoreManagerInited = false;
    /**
     * 标识是否需要重新初始化coremanager，
     * 1.需要在CoreManager.init方法之前设置为false，只有这样在coremanagter的初始化过程中
     * 才不会重复去初始化陷入死循环
     * 2.在进程后台被杀，重新拉起后，由于这些静态变量被重新创建初始化，因此可以作为一个开关，来判断是否需要重新初始化
     */
    private static boolean shouldReInitCore = true;

    static {
        cores = new HashMap<>();
        coreClasses = new HashMap<>();
    }

    /**
     * 从工厂获取实现cls接口的对象实例
     * 该实例是使用registerCoreClass注册的实现类的对象
     *
     * @param cls 必须是core接口类，不能是core实现类，否则会抛出异常
     * @return 如果生成对象失败，返回null
     */
    public static <T extends IBaseCore> T getCore(Class<T> cls) {
        if (cls == null) {
            return null;
        }
        try {
            IBaseCore core = cores.get(cls);
            if (core == null) {
                Class<? extends AbstractBaseCore> implClass = coreClasses.get(cls);
                if (implClass == null) {
                    if (cls.isInterface()) {
                        core = checkIfNeedReInitCore(cls);
//                        throw new IllegalArgumentException("No registered core class for: " + cls.getName());
                    } else {
                        core = checkIfNeedReInitCore(cls);
//                        throw new IllegalArgumentException("Not interface core class for: " + cls.getName());
                    }
                } else {
                    core = implClass.newInstance();
                }

                if (core != null) {
                    cores.put(cls, core);
                    if (BuildConfig.IS_DEBUG) {
                    }
                }
            }
            return (T) core;
        } catch (Throwable e) {
        }
        return null;
    }

    /**
     * 注册某个接口实现类
     *
     * @param coreInterface
     * @param coreClass
     */
    public static void registerCoreClass(Class<? extends IBaseCore> coreInterface, Class<? extends AbstractBaseCore> coreClass) {

        if (coreInterface == null || coreClass == null) {
            return;
        }

        coreClasses.put(coreInterface, coreClass);
    }

//--------------------------------后台进程被杀后，最近任务列表重新拉起时必要的初始化逻辑-------------------------

    /**
     * 返回某个接口是否有注册实现类
     *
     * @param coreInterface
     * @return
     */
    public static boolean hasRegisteredCoreClass(Class<? extends IBaseCore> coreInterface) {
        if (coreInterface == null) {
            return false;
        } else {
            return coreClasses.containsKey(coreInterface);
        }
    }

    public static boolean isHasCoreManagerInited() {
        return hasCoreManagerInited;
    }

    /**
     * 设置是否coremanater初始化完毕，请务必在初始化模块coreManager的所有初始化工作都完毕后再调用该方法
     *
     * @param hasInited
     */
    public static void setHasCoreManagerInited(boolean hasInited) {
        hasCoreManagerInited = hasInited;
    }

    public static void doNotReInitCoreAction() {
        shouldReInitCore = false;
    }

    /**
     * 判断是否需要通过反射的方式重新初始化必要的coremanager等
     * @param cls
     * @param <T>
     * @return
     */
    public static <T extends IBaseCore> T checkIfNeedReInitCore(Class<T> cls) {
        IBaseCore core = null;
        if (!isHasCoreManagerInited() && shouldReInitCore) {
            long currTime = System.currentTimeMillis();
            try {
                Class clazz1 = Class.forName("com.yuhuankj.tmxq.utils.SplashInitUtils");
                clazz1.getMethod("initBasicConfig").invoke(null);

                Class clazz = Class.forName("com.tongdaxing.xchat_core.UriProvider");
                //需要区分线上还是线下
                if (BuildConfig.IS_DEBUG) {
                    clazz.getMethod("initTestUri").invoke(null);
                } else {
                    clazz.getMethod("initProductUri").invoke(null);
                }

                Field field = clazz.getField("JAVA_WEB_URL");
                clazz1.getMethod("initRxNet", String.class).invoke(null, (String) field.get(clazz));

                doNotReInitCoreAction();

                clazz1.getMethod("initCore").invoke(null);

                Application application = getApplicationUsingReflection();
                if (null != application) {
                    ImageManager.instance().init(BasicConfig.INSTANCE.getAppContext(),
                            application.getPackageName().concat(File.separator).concat("image"));

                    Class clazz2 = Class.forName("com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager");
                    Object obj = clazz2.getMethod("getInstance").invoke(null);
                    clazz2.getMethod("init", Context.class).invoke(obj, application.getApplicationContext());
                }

                setHasCoreManagerInited(true);
                core = getCore(cls);
                long endTime = System.currentTimeMillis() - currTime;
                return (T) core;
            } catch (Exception ex) {
                long endTime = System.currentTimeMillis() - currTime;
                ex.printStackTrace();
            }
        }

        return null;
    }

    private static Application getApplicationUsingReflection() throws Exception {
        return (Application) Class.forName("android.app.ActivityThread")
                .getMethod("currentApplication").invoke(null, (Object[]) null);
    }

}
