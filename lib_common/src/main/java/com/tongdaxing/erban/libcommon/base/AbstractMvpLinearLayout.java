package com.tongdaxing.erban.libcommon.base;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.tongdaxing.erban.libcommon.base.factory.BaseMvpProxy;
import com.tongdaxing.erban.libcommon.base.factory.PresenterMvpFactory;
import com.tongdaxing.erban.libcommon.base.factory.PresenterMvpFactoryImpl;
import com.tongdaxing.erban.libcommon.base.factory.PresenterProxyInterface;

/**
 * <p> 1. 子类的Presenter必须继承自AbstractMvpPresenter；
 * 2. 子类的View必须继承自IMvpBaseView
 * 3.View的声明周期流程，参考：
 * https://blog.csdn.net/SEU_Calvin/article/details/72855537
 * https://blog.csdn.net/sun_star1chen/article/details/44626433
 * </p>
 *
 * @author weihaitao
 * @date 2019年4月25日 18:35:26
 */
public abstract class AbstractMvpLinearLayout<V extends IMvpBaseView, P extends AbstractMvpPresenter<V>> extends LinearLayout
        implements PresenterProxyInterface<V, P> {

    protected final String TAG = getClass().getSimpleName();
    /**
     * 创建代理对象，传入默认的Presenter工厂
     */
    private BaseMvpProxy<V, P> mMvpProxy = new BaseMvpProxy<>(PresenterMvpFactoryImpl.<V, P>createFactory(getClass()));

    public AbstractMvpLinearLayout(Context context) {
        this(context, null);
    }

    public AbstractMvpLinearLayout(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AbstractMvpLinearLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mMvpProxy.onCreate((V) this);
        if (getViewLayoutId() != 0) {
            inflate(context, getViewLayoutId(), this);
        }
        initialView(context);
        initListener();
        initViewState();
    }

    protected abstract @LayoutRes
    int getViewLayoutId();

    protected abstract void initViewState();

    public abstract void initialView(Context context);

    protected abstract void initListener();

    /**
     * 异步初始化view的状态方法
     */
    public void asynInitViewState() {

    }

    @Override
    protected void onAttachedToWindow() {
        mMvpProxy.onResume();
        super.onAttachedToWindow();
    }

    @Override
    protected void onDetachedFromWindow() {
        mMvpProxy.onPause();
        mMvpProxy.onDestroy();
        super.onDetachedFromWindow();
    }


    @Override
    public PresenterMvpFactory<V, P> getPresenterFactory() {
        return mMvpProxy.getPresenterFactory();
    }

    @Override
    public void setPresenterFactory(PresenterMvpFactory<V, P> presenterFactory) {
        mMvpProxy.setPresenterFactory(presenterFactory);
    }

    @Override
    public P getMvpPresenter() {
        return mMvpProxy.getMvpPresenter();
    }

}
