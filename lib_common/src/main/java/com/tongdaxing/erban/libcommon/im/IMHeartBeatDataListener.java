package com.tongdaxing.erban.libcommon.im;

/**
 * @author weihaitao
 * @date 2019/5/16
 */
public interface IMHeartBeatDataListener {

    /**
     * 获取心跳包基本请求数据
     *
     * @return
     */
    String getHeartBeatData();
}
