package com.tongdaxing.erban.libcommon.http_image.http;

import android.os.Process;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author zhongyongsheng
 */
@SuppressWarnings("rawtypes")
public class CacheDispatcher extends Thread {

    private final BlockingQueue<Request> mCacheQueue;

    private final BlockingQueue<Request> mNetworkQueue;

    private volatile boolean mQuit = false;

    private RequestProcessor mRequestProcessor;

    public CacheDispatcher(
            BlockingQueue<Request> cacheQueue, BlockingQueue<Request> networkQueue, String name,
            RequestProcessor requestProcessor) {
        super(name + "CacheThread");
        mCacheQueue = cacheQueue;
        mNetworkQueue = networkQueue;
        mRequestProcessor = requestProcessor;
    }

    public void quit() {
        mQuit = true;
        interrupt();
    }

    @Override
    public void run() {
        Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

        while (true) {
            try {

                final Request request = mCacheQueue.take();

                waitIfPaused();

                if (request.isCanceled()) {
                    request.finish("Cache discard canceled");
                    continue;
                }

                request.getCache().initialize();

                Cache.Entry entry = request.getCache().get(request.getKey());
                if (entry == null) {
                    mNetworkQueue.put(request);
                    continue;
                }

                if (entry.isExpired()) {
                    //缓存过期, 去网络请求
                    request.setCacheEntry(entry);
                    mNetworkQueue.put(request);
                    continue;
                }

                request.parseDataToResponse(new ResponseData(entry.getData(), entry.getResponseHeaders()));

                if (!entry.refreshNeeded()) {
                    request.postResponse();
                } else {
                    request.setCacheEntry(entry);

                    request.getResponse().intermediate = true;

                    //返回成功结果,再去网络请求更新缓存
                    request.postResponse(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                mNetworkQueue.put(request);
                            } catch (InterruptedException e) {
                            }
                        }
                    });
                }

            } catch (InterruptedException e) {
                if (mQuit) {
                    return;
                }
                continue;
            } catch (Exception ee) {
                continue;
            } catch (Error e) {
            }
        }
    }

    private void waitIfPaused() {
        AtomicBoolean pause = mRequestProcessor.getPause();
        synchronized (pause) {
            if (pause.get()) {
                try {
                    pause.wait();
                } catch (InterruptedException e) {
                    return;
                }
            }
        }
    }
}
