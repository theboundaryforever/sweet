package com.tongdaxing.erban.libcommon.widget.messageview;

import android.support.annotation.LayoutRes;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 文件描述：房间多布局自定义ViewHolder的布局注解
 *
 * @auther：zwk
 * @data：2019/1/29
 */
@Retention(RUNTIME)
public @interface MsgHolderLayoutId {
    @LayoutRes int value();
}
