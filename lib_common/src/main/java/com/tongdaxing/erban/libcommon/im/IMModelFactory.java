package com.tongdaxing.erban.libcommon.im;


import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.utils.VersionUtil;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.erban.libcommon.utils.json.Json;

import java.util.Map;

public class IMModelFactory {

    private static IMModelFactory factory;

    public static IMModelFactory get() {
        if (factory == null) {
            synchronized (IMModelFactory.class) {
                if (factory == null) {
                    factory = new IMModelFactory();
                }
            }
        }
        return factory;
    }

    public static int getSendId() {
        return ++IMCallBack.callbackIndex;
    }

    public Json createRequestData(String route, Json requestData) {
        Json json = new Json();
        json.set(IMKey.route, route);

        if (requestData != null) {
            json.set("req_data", requestData);
        }
        return json;

    }

    public Json createLoginModel(String ticket, String uid) {
        Json json = new Json();
        json.set("ticket", ticket);
        json.set("uid", uid);
        //android为2
        json.set("page_name", 2);
        json.set("appVersion", VersionUtil.getLocalName(BasicConfig.INSTANCE.getAppContext()));
        json.set("appCode", VersionUtil.getVersionCode(BasicConfig.INSTANCE.getAppContext()) + "");
        return createRequestData(IMSendRoute.login, json);
    }

    /**
     * 新版的进入聊天室方式
     *
     * @param room_uid  房主uid
     * @param room_type
     * @param reconnect 是否是重连 1重连，0非重连
     * @return
     */
    public Json enterWithOpenChatRoom(long room_uid, int room_type, String room_PWD, int reconnect, String ticket) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        Json json = new Json();
        json.set("room_uid", room_uid);// 要进入的房主uid
        json.set("room_type", room_type);//  房间类型
        json.set("room_pwd", room_PWD);//  房间类型
        json.set("reconnect", reconnect);//
        json.set("ticket", ticket);
        json.set("os", params.get("os"));
        json.set("app_version", params.get("appVersion"));
        json.set("net_type", params.get("netType"));
        json.set("channel", params.get("channel"));
        json.set("device_id", params.get("deviceId"));
        json.set("os_version", params.get("osVersion"));
        json.set("model", params.get("model"));
        json.set("isp_type", params.get("ispType"));
        json.set("app_code", params.get("appCode"));
        return createRequestData(IMSendRoute.enterWithOpenChatRoom, json);
    }

    /**
     * 进入聊天室
     *
     * @param room_id   roomid
     * @param reconnect 是否是重连 1重连，0非重连
     * @return
     */
    public Json createJoinAvRoomModel(long room_id, int reconnect) {
        Json json = new Json();
        json.set("room_id", room_id);
        json.set("reconnect", reconnect);
        return createRequestData(IMSendRoute.enterChatRoom, json);
    }

    /**
     * 退出聊天室
     *
     * @param room_id
     * @return
     */
    public Json createExitRoom(long room_id) {
        Json json = new Json();
        json.set("room_id", room_id);
        return createRequestData(IMSendRoute.exitChatRoom, json);
    }

    /**
     * 退出公聊大厅
     *
     * @param room_id
     * @return
     */
    public Json createExitPublicRoom(long room_id) {
        Json json = new Json();
        json.set("room_id", room_id);
        return createRequestData(IMSendRoute.exitPublicRoom, json);
    }

    /**
     * 更新队列 -- 加入新的队列元素
     *
     * @param roomId   房间id
     * @param position 队列位置 -1 房主位  1-7 主播位
     * @param uid      加入队列用户uid
     * @return
     */
    public Json createUpdateQueue(String roomId, int position, long uid) {
        Json json = new Json();
        json.set("room_id", roomId);
        json.set("key", position);
        json.set("uid", uid);
        return createRequestData(IMSendRoute.updateQueue, json);
    }

    /**
     * 更新队列 -- 加入新的队列元素
     *
     * @param roomId   房间id
     * @param position 队列位置 -1 房主位  1-7 主播位
     * @return
     */
    public Json createPollQueue(String roomId, int position) {
        Json json = new Json();
        json.set("room_id", roomId);
        json.set("key", position);
        return createRequestData(IMSendRoute.pollQueue, json);
    }

}
