package com.tongdaxing.erban.libcommon.net.rxnet.callback;

import com.google.gson.internal.$Gson$Types;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public abstract class HttpRequestCallBack<T> {

    public Type mType;

    protected HttpRequestCallBack() {
        mType = getSuperclassTypeParameter(getClass());
    }

    private static Type getSuperclassTypeParameter(Class<?> subclass) {
        Type superclass = subclass.getGenericSuperclass();
        if (superclass instanceof Class) {
            throw new RuntimeException("Missing type parameter.");
        }
        ParameterizedType parameterized = (ParameterizedType) superclass;
        return $Gson$Types.canonicalize(parameterized.getActualTypeArguments()[0]);
    }

    /**
     * 需要在onSuccess或onFailure之前调用
     */
    public void onFinish() {

    }

    public abstract void onSuccess(String message, T response);

    public abstract void onFailure(int code, String msg);
}
