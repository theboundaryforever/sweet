package com.tongdaxing.erban.libcommon.net.rxnet;

/**
 * Created by fwhm on 2017/7/27.
 */

public class ServerException extends Exception {
    public ServerException(String message) {
        super(message);
    }
}
