package com.tongdaxing.erban.libcommon.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;

/**
 * Created with IntelliJ IDEA.
 * User: crid
 * Date: 9/3/13
 * Time: 9:23 AM
 * To change this template use File | Settings | File Templates.
 */
public class AppMetaDataUtil {

    /**
     * @param
     * @return 渠道名称
     * <p/>
     * IMPORTANT: 需要在AndroidManifest.xml 配置 <meta-data android:name="Channel_ID" android:value="official"/>
     */
    public static String getChannelID(Context context) {
        String channelID = BasicConfig.INSTANCE.getChannel();
     /*   try {
            if (context != null) {
                String pkgName = context.getPackageName();
                ApplicationInfo appInfo = context.getPackageManager().getApplicationInfo(pkgName, PackageManager.GET_META_DATA);
                if (appInfo != null && appInfo.metaData != null) {
                    Object channelIdObj = appInfo.metaData.get("UMENG_CHANNEL");
                    if (channelIdObj instanceof Integer) {
                        channelID = String.valueOf(channelIdObj);
                    } else {
                        channelID = channelIdObj.toString();
                    }
                }
            }
        } catch (Exception e) {
            MLog.error("AppMetaDataUtil.getChannelID(Context context)", e);
        }*/
        channelID = channelID == null ? "official" : channelID;
        return channelID;
    }

    public static String getSvnBuildVersion(Context context) {
        return getMetaString(context, "SvnBuildVersion");
    }

    public static String getMetaString(Context context, String key) {
        String value = "";
        try {
            if (context != null) {
                String pkgName = context.getPackageName();
                ApplicationInfo appInfo = context.getPackageManager().getApplicationInfo(pkgName, PackageManager.GET_META_DATA);
                value = appInfo.metaData.getString(key);
            }
        } catch (Exception e) {
        }

        return value;
    }

    public static String getUpdateId(Context context) {
        return getMetaString(context, "UpdateId");
    }
}
