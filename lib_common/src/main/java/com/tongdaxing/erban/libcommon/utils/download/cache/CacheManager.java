package com.tongdaxing.erban.libcommon.utils.download.cache;

import android.content.Context;

import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.erban.libcommon.utils.codec.MD5Utils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.erban.libcommon.utils.file.StorageUtils;
import com.tongdaxing.erban.libcommon.utils.json.JsonParser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class CacheManager {
    private static final String TAG = "CacheManager";
    //LruCache
    private static final int MEM_MAX_SIZE = 1 * 1024 * 1024;// MEM 1MB
    //DiskLruCache
    private static final int DISK_MAX_SIZE = 5 * 1024 * 1024;// SD 5MB
    private static final String CACHE_PATH = "cacheDir";
    private StringLruCache<String, String> mMemoryCache = null;
    private StringDiskCache mDiskCacke = null;

    private Context mContext = null;

    public CacheManager(String fileName) {
        mContext = BasicConfig.INSTANCE.getAppContext();

        //内存缓存
        mMemoryCache = new StringLruCache<String, String>(MEM_MAX_SIZE) {
            @Override
            protected int sizeOf(String key, String value) {
                int count = value.getBytes().length;
                return count;
            }

            @Override
            protected void entryRemoved(boolean evicted, String key,
                                        String oldValue, String newValue) {
                super.entryRemoved(evicted, key, oldValue, newValue);
            }
        };


        //sdcard或者内置存储
        File cacheDir = StorageUtils.getCacheDirectory(mContext, CACHE_PATH + File.separator + fileName);
        mDiskCacke = StringDiskCache.openCache(cacheDir, DISK_MAX_SIZE);
    }

//	public static CacheManager instance() 
//	{
//		if(mInstance==null) {
//			mInstance = new CacheManager();
//		}
//		return mInstance;
//	}


    public boolean putCache(String key, String json, long expire) {
        String md5Key = MD5Utils.getMD5String(key);
        if (mDiskCacke != null) {
            mDiskCacke.putText(md5Key, json);
        }
        if (mMemoryCache != null) {
            mMemoryCache.put(md5Key, json);
        }
        return false;
    }

    public String getCache(String key) throws NoSuchKeyException, IOException {
        //线程的优先级,设置低点对主线程
        //Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

        if (StringUtils.isBlank(key)) {
            return null;
        }
        String md5Key = MD5Utils.getMD5String(key);

        String json = "";
        if (mMemoryCache != null) {
            json = mMemoryCache.get(md5Key);
            if (json != null) {
                CacheClient.CachePacket packet = JsonParser.parseJsonObject(json, CacheClient.CachePacket.class);
                long expiredTime = System.currentTimeMillis() - packet.getHeader().getCreateTime();

                if (expiredTime > packet.getHeader().getExpired()) {
                    remove(key);
                    return null;
                }
                return json;
            }
        }
        //内存中没有
        if (mDiskCacke != null) {
            try {
                json = mDiskCacke.get(md5Key);
                if (json != null) {
                    mMemoryCache.put(md5Key, json);
                    return json;
                }
            } catch (final FileNotFoundException e) {
                throw new NoSuchKeyException(key, "no such key");
            } catch (final IOException e) {
                throw e;
            }
        }
        return null;
    }

    /**
     * 清空key值，释放sd和内存的数据
     *
     * @param key
     */
    public void remove(String key, String path) {
        if (StringUtils.isBlank(key)) {
            return;
        }
        String md5Key = MD5Utils.getMD5String(key);

        if (mMemoryCache != null) {
            mMemoryCache.remove(md5Key);
        }
        if (mDiskCacke != null) {
            //内存中没有
            mDiskCacke.clearCache(md5Key);
        }
    }

    /**
     * 清空key值，释放sd和内存的数据
     *
     * @param key
     */
    public void remove(String key) {
        if (StringUtils.isBlank(key)) {
            return;
        }
        String md5Key = MD5Utils.getMD5String(key);

        if (mMemoryCache != null) {
            mMemoryCache.remove(md5Key);
        }
        if (mDiskCacke != null) {
            //内存中没有
            mDiskCacke.clearCache(md5Key);
        }
    }

    /**
     * 清空anything
     */
    public void clear() {
        if (mMemoryCache != null) {
            mMemoryCache.evictAll();
        }
        if (mDiskCacke != null) {
            //内存中没有
            mDiskCacke.clearCache();
        }
    }
}
