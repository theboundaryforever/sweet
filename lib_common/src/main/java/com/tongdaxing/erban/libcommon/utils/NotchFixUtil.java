package com.tongdaxing.erban.libcommon.utils;


import android.content.Context;
import android.os.Build;
import android.text.TextUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * android齐柳海适配工具
 *
 * 目前分为Android O和Android P版本下的齐柳海
 * Android P谷歌官方有提供通用齐柳海适配方案
 * Android O由于官方未给出通用适配方案，需要手动判断机型，针对机型来做解决处理
 *
 * 参考链接
 * https://blog.csdn.net/nerv2013/article/details/82110196
 * 1.华为齐柳海适配:https://developer.huawei.com/consumer/cn/devservice/doc/50114
 * 2.小米:https://dev.mi.com/console/doc/detail?pId=1293
 * 3.Vivo:https://dev.vivo.com.cn/documentCenter/doc/103
 * 4.oppo:https://open.oppomobile.com/wiki/doc#id=10159
 * 5.锤子:https://resource.smartisan.com/resource/61263ed9599961d1191cc4381943b47a.pdf
 * 6.一加手机: 待查找
 * 7.其他手机:
 *
 * Android判断手机ROM
 * https://www.jianshu.com/p/ba9347a5a05a
 */
public class NotchFixUtil {

    private static final String TAG = NotchFixUtil.class.getSimpleName();

    //----------------------------------判断手机rom类型---------------------------------

    public static final String ROM_MIUI = "MIUI";
    public static final String ROM_EMUI = "EMUI";
    public static final String ROM_FLYME = "FLYME";
    public static final String ROM_OPPO = "OPPO";
    public static final String ROM_SMARTISAN = "SMARTISAN";
    public static final String ROM_VIVO = "VIVO";
    public static final String ROM_QIKU = "QIKU";

    private static final String KEY_VERSION_MIUI = "ro.miui.ui.version.name";
    private static final String KEY_VERSION_EMUI = "ro.build.version.emui";
    private static final String KEY_VERSION_OPPO = "ro.build.version.opporom";
    private static final String KEY_VERSION_SMARTISAN = "ro.smartisan.version";
    private static final String KEY_VERSION_VIVO = "ro.vivo.os.version";

    private static String sName;
    private static String sVersion;

    public static boolean isEmui() {
        return check(ROM_EMUI);
    }

    public static boolean isMiui() {
        return check(ROM_MIUI);
    }

    public static boolean isVivo() {
        return check(ROM_VIVO);
    }

    public static boolean isOppo() {
        return check(ROM_OPPO);
    }

    public static boolean isFlyme() {
        return check(ROM_FLYME);
    }

    public static boolean is360() {
        return check(ROM_QIKU) || check("360");
    }

    public static boolean isSmartisan() {
        return check(ROM_SMARTISAN);
    }

    public static String getName() {
        if (sName == null) {
            check("");
        }
        return sName;
    }

    public static String getVersion() {
        if (sVersion == null) {
            check("");
        }
        return sVersion;
    }

    public static boolean check(String rom) {
        if (sName != null) {
            return sName.equals(rom);
        }
        if (!TextUtils.isEmpty(sVersion = getProp(KEY_VERSION_MIUI))) {
            //ro.miui.ui.version.name返回V5表示是MIUI V5系统，V6表示MIUI V6系统
            sName = ROM_MIUI;
        } else if (!TextUtils.isEmpty(sVersion = getProp(KEY_VERSION_EMUI))) {
            sName = ROM_EMUI;
        } else if (!TextUtils.isEmpty(sVersion = getProp(KEY_VERSION_OPPO))) {
            sName = ROM_OPPO;
        } else if (!TextUtils.isEmpty(sVersion = getProp(KEY_VERSION_VIVO))) {
            sName = ROM_VIVO;
        } else if (!TextUtils.isEmpty(sVersion = getProp(KEY_VERSION_SMARTISAN))) {
            sName = ROM_SMARTISAN;
        } else {
            sVersion = Build.DISPLAY;
            if (sVersion.toUpperCase().contains(ROM_FLYME)) {
                sName = ROM_FLYME;
            } else {
                sVersion = Build.UNKNOWN;
                sName = Build.MANUFACTURER.toUpperCase();
            }
        }
        return sName.equals(rom);
    }

    public static String getProp(String name) {
        String line = null;
        BufferedReader input = null;
        try {
            Process p = Runtime.getRuntime().exec("getprop " + name);
            input = new BufferedReader(new InputStreamReader(p.getInputStream()), 1024);
            line = input.readLine();
            input.close();
        } catch (IOException ex) {
            LogUtils.e(TAG, "Unable to read prop " + name, ex);
            return null;
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        LogUtils.d(TAG,"getProp-line:"+line);
        return line;
    }

    //----华为手机 Android O 齐柳海适配----------------------------------------------------

    /**
     * 判断华为手机是否为刘海屏
     * @param context
     * @return true-是刘海屏, false-非刘海屏。
     */
    public static boolean hasHwNotchInScreen(Context context) {
        boolean ret = false;
        try {
            ClassLoader cl = context.getClassLoader();
            Class HwNotchSizeUtil = cl.loadClass("com.huawei.android.util.HwNotchSizeUtil");
            Method get = HwNotchSizeUtil.getMethod("hasNotchInScreen");
            ret = (boolean) get.invoke(HwNotchSizeUtil);
        } catch (ClassNotFoundException e) {
            LogUtils.e(TAG, "hasNotchInScreen ClassNotFoundException");
        } catch (NoSuchMethodException e) {
            LogUtils.e(TAG, "hasNotchInScreen NoSuchMethodException");
        } catch (Exception e) {
            LogUtils.e(TAG, "hasNotchInScreen Exception");
        } finally {
            LogUtils.d(TAG,"hasHwNotchInScreen-ret:"+ret);
            return ret;
        }
    }

    /**
     * 获取华为刘海屏手机的刘海尺寸：width、height
     * @param context
     * @return int[0]值为刘海宽度 int[1]值为刘海高度
     */
    public static int[] getNotchSize(Context context) {
        int[] ret = new int[]{0, 0};
        try {
            ClassLoader cl = context.getClassLoader();
            Class HwNotchSizeUtil = cl.loadClass("com.huawei.android.util.HwNotchSizeUtil");
            Method get = HwNotchSizeUtil.getMethod("getNotchSize");
            ret = (int[]) get.invoke(HwNotchSizeUtil);
        } catch (ClassNotFoundException e) {
            LogUtils.e(TAG, "getNotchSize ClassNotFoundException");
        } catch (NoSuchMethodException e) {
            LogUtils.e(TAG, "getNotchSize NoSuchMethodException");
        } catch (Exception e) {
            LogUtils.e(TAG, "getNotchSize Exception");
        } finally {
            LogUtils.d(TAG,"hasHwNotchInScreen-ret[0]:"+ret[0]+" ret[1]:"+ret[1]);
            return ret;
        }
    }

    //----小米手机 Android O齐柳海适配-------------------------------------------------

    public static boolean hasMiNotchInScreen(Context context){
        boolean result = false;
        String notch = "";
        try {
            ClassLoader cl = context.getClassLoader();
            Class<?> SystemProperties = cl.loadClass("android.os.SystemProperties");
            Method method = SystemProperties.getMethod("get", String.class);
            Object[] params = new Object[1];
            params[0] = "ro.miui.notch";
            notch = (String)method.invoke(SystemProperties, params);
            result =  "1".equals(notch);
        } catch (Exception e) {
            e.printStackTrace();
        }
        LogUtils.d(TAG,"hasMiNotchInScreen-result:"+result+" notch:"+notch);
        return result;
    }

    /**
     * 获取当前设备刘海宽度
     * MIUI 10 新增了获取刘海宽和高的方法，需升级至8.6.26开发版及以上版本
     * @param context
     * @return
     */
    public static int getMiNotchWidth(Context context){
        int width = 0;
        int resourceId = context.getResources().getIdentifier("notch_width", "dimen", "android");
        if (resourceId > 0) {
            width = context.getResources().getDimensionPixelSize(resourceId);
        }
        LogUtils.d(TAG,"getMiNotchWidth-width:"+width);
        return width;
    }

    /**
     * 获取当前设备刘海高度
     * MIUI 10 新增了获取刘海宽和高的方法，需升级至8.6.26开发版及以上版本
     * @param context
     * @return
     */
    public static int getMiNotchHeight(Context context){
        int height = 0;
        int resourceId = context.getResources().getIdentifier("notch_height", "dimen", "android");
        if (resourceId > 0) {
            height = context.getResources().getDimensionPixelSize(resourceId);
        }
        LogUtils.d(TAG,"getMiNotchHeight-height:"+height);
        return height;
    }

    /**
     * 小米刘海屏幕手机获取状态栏高度方法
     * 由于 Notch 设备的状态栏高度与正常机器不一样，因此在需要使用状态栏高度时，不建议写死一个值，而应该改为读取系统的值
     * @param context
     * @return
     */
    public static int getStatusBarHeight(Context context){
        int statusBarHeight = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            statusBarHeight = context.getResources().getDimensionPixelSize(resourceId);
        }
        LogUtils.d(TAG,"getStatusBarHeight-statusBarHeight0:"+statusBarHeight);
        if (statusBarHeight == 0) {
            try {
                Class<?> c = Class.forName("com.android.internal.R$dimen");
                Object o = c.newInstance();
                Field field = c.getField("status_bar_height");
                int x = (Integer) field.get(o);
                statusBarHeight = context.getResources().getDimensionPixelSize(x);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (statusBarHeight == 0) {
            statusBarHeight = DisplayUtility.dp2px(context,25);
        }

        LogUtils.d(TAG,"getStatusBarHeight-statusBarHeight1:"+statusBarHeight);
        return statusBarHeight;
    }

    //----Vivo & oppo Android O齐柳海适配-------------------------------------------------
    //Vivo Z1

    public static final int NOTCH_IN_SCREEN_VOIO=0x00000020;//是否有凹槽
    public static final int ROUNDED_IN_SCREEN_VOIO=0x00000008;//是否有圆角

    public static boolean hasVivoNotchInScreen(Context context){
        boolean ret = false;
        try {
            ClassLoader cl = context.getClassLoader();
            Class FtFeature = cl.loadClass("android.util.FtFeature");
            Method get = FtFeature.getMethod("isFeatureSupport",int.class);
            ret = (boolean) get.invoke(FtFeature,NOTCH_IN_SCREEN_VOIO);
        } catch (ClassNotFoundException e) {
            LogUtils.e(TAG, "hasNotchInScreen ClassNotFoundException");
        } catch (NoSuchMethodException e) {
            LogUtils.e(TAG, "hasNotchInScreen NoSuchMethodException");
        } catch (Exception e) {
            LogUtils.e(TAG, "hasNotchInScreen Exception");
        } finally {
            LogUtils.d(TAG,"hasVivoNotchInScreen-ret:"+ret);
            return ret;
        }
    }

    public static boolean hasOppoNotchInScreen(Context context){
        boolean hasNotchInOppo = context.getPackageManager().hasSystemFeature("com.oppo.feature.screen.heteromorphism");
        LogUtils.d(TAG,"hasNotchInOppo-hasNotchInOppo:"+hasNotchInOppo);
        return hasNotchInOppo;
    }


    //----坚果R1 Android O齐柳海适配-------------------------------------------------

    public static final int NOTCH_IN_SCREEN_Nut1=0x00000001;//坚果r1 异形屏

    public static boolean hasSmartNotchInScreen(Context context){
        boolean hasSmartNotch = false;
        try {
            ClassLoader cl = context.getClassLoader();
            Class FtFeature = cl.loadClass("smartisanos.api.DisplayUtilsSmt");
            Method get = FtFeature.getMethod("isFeatureSupport",int.class);
            hasSmartNotch = (boolean) get.invoke(FtFeature,NOTCH_IN_SCREEN_Nut1);
        } catch (ClassNotFoundException e) {
            LogUtils.e(TAG, "hasSmartNotchInScreen ClassNotFoundException");
        } catch (NoSuchMethodException e) {
            LogUtils.e(TAG, "hasSmartNotchInScreen NoSuchMethodException");
        } catch (Exception e) {
            LogUtils.e(TAG, "hasSmartNotchInScreen Exception");
        } finally {
            LogUtils.d(TAG,"hasSmartNotchInScreen-hasSmartNotch:"+hasSmartNotch);
            return hasSmartNotch;
        }
    }
}
