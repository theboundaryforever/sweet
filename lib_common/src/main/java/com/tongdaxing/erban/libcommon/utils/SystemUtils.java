package com.tongdaxing.erban.libcommon.utils;

import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import com.llew.huawei.verifier.LoadedApkHuaWei;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * <p> </p>
 *
 * @author jiahui
 * @date 2017/12/16
 */
public class SystemUtils {

    /**
     * 获取手机型号
     *
     * @return
     */
    public static String getPhoneModel() {
        return Build.MODEL;
    }

    /**
     * 获取是否nettype字段
     *
     * @return
     */
    public static int getNetworkType(Context context) {
        if (NetworkUtils.getNetworkType(context) == NetworkUtils.NET_WIFI) {
            return 2;
        } else {
            return 1;
        }
    }

    /**
     * 获取运营商字段
     *
     * @return
     */
    public static int getIspType(Context context) {
        String isp = NetworkUtils.getOperator(context);
        int ispType = 4;
        if (isp.equals(NetworkUtils.ChinaOperator.CMCC)) {
            ispType = 1;
        } else if (isp.equals(NetworkUtils.ChinaOperator.UNICOM)) {
            ispType = 2;
        } else if (isp.equals(NetworkUtils.ChinaOperator.CTL)) {
            ispType = 3;
        }

        return ispType;
    }

    /**
     * 参考: https://www.jianshu.com/p/89e2719be9c7
     * bugly: https://bugly.qq.com/v2/crash-reporting/crashes/23dbf7aab1/1171?pid=1&crashDataType=unSystemExit
     *
     * @closeFWDOrAddWatchTime true, 关闭FinalizerWatchdogDaemon，false，延长计时时间
     */
    public static void fixOppoTimeoutException(boolean closeFWDOrAddWatchTime) {
        //仅修改OPPo机型的
        if (VersionUtil.getManufacturer().equalsIgnoreCase(VersionUtil.OPPO) && Build.VERSION.SDK_INT >= 22 && Build.VERSION.SDK_INT <= 23) {
            if (closeFWDOrAddWatchTime) {
                try {
                    Class clazz = Class.forName("java.lang.Daemons$FinalizerWatchdogDaemon");
                    Method method = clazz.getSuperclass().getDeclaredMethod("stop");
                    method.setAccessible(true);
                    Field field = clazz.getDeclaredField("INSTANCE");
                    field.setAccessible(true);
                    method.invoke(field.get(null));
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    Class<?> c = Class.forName("java.lang.Daemons");
                    Field maxField = c.getDeclaredField("MAX_FINALIZE_NANOS");
                    maxField.setAccessible(true);
                    maxField.set(null, Long.MAX_VALUE);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 这里处理VIVO手机rom自定义AbsListView的crash问题
     * bugly上对应 https://bugly.qq.com/v2/crash-reporting/crashes/23dbf7aab1/40770?pid=1
     * <p>
     * Caused by:
     * java.lang.NullPointerException:Attempt to invoke interface method 'int java.util.List.size()' on a null object reference
     * android.widget.AbsListView$UpdateBottomFlagTask.isSuperFloatViewServiceRunning(AbsListView.java:7689)
     * android.widget.AbsListView$UpdateBottomFlagTask.doInBackground(AbsListView.java:7665)
     * android.os.AsyncTask$2.call(AsyncTask.java:292)
     * java.util.concurrent.FutureTask.run(FutureTask.java:237)
     * android.os.AsyncTask$SerialExecutor$1.run(AsyncTask.java:231)
     * java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1112)
     * java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:587)
     * java.lang.Thread.run(Thread.java:818)
     */
    public static void initVivoAbsListViewCrashHander() {
        try {
            if (Build.MANUFACTURER.toLowerCase().contains("vivo")) {//VIVO手机
                setFinalStatic(AsyncTask.class.getDeclaredField("SERIAL_EXECUTOR"), new SafeSerialExecutor());
                Field defaultField = AsyncTask.class.getDeclaredField("sDefaultExecutor");
                defaultField.setAccessible(true);
                defaultField.set(null, AsyncTask.SERIAL_EXECUTOR);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 反射修改final变量
     */
    private static void setFinalStatic(Field field, Object newValue) throws Exception {
        field.setAccessible(true);
        try {
            Field artField = Field.class.getDeclaredField("artField");
            artField.setAccessible(true);
            Object fieldValue = artField.get(field);
            Field accessFlagsFiled = fieldValue.getClass().getDeclaredField("accessFlags");
            accessFlagsFiled.setAccessible(true);
            accessFlagsFiled.setInt(fieldValue, field.getModifiers() & ~Modifier.FINAL);
        } catch (NoSuchFieldException e) {
            //没有artField的直接处理属性（<=4.4非art或者>=8.0优化过的art）
            Field accessFlagsFiled = field.getClass().getDeclaredField("accessFlags");
            accessFlagsFiled.setAccessible(true);
            accessFlagsFiled.setInt(field, field.getModifiers() & ~Modifier.FINAL);
        }
        field.set(null, newValue);
    }

    public static void initHuaweiVerifier(Application application) {
        if (Build.MANUFACTURER.equals("HUAWEI")) {
            if ("5.1".equals(android.os.Build.VERSION.RELEASE) || "5.1.1".equals(android.os.Build.VERSION.RELEASE)) {
                LoadedApkHuaWei.hookHuaWeiVerifier(application);
            }
        }
    }
}
