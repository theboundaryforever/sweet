package com.tongdaxing.erban.libcommon.http_image.image;

import android.graphics.drawable.BitmapDrawable;

/**
 * Image请求结果
 *
 * @author zhongyongsheng on 14-9-11.
 */
public class ImageResponse {

    public BitmapDrawable bitmapDrawable;
    public ImageRequest imageRequest;

}
