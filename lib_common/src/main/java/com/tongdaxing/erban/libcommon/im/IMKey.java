package com.tongdaxing.erban.libcommon.im;

public interface IMKey {
    String route = "route";
    String res_data = "res_data";
    String id = "id";
    String data = "data";
    String errno = "errno";
    String errmsg = "errmsg";
    String errdata = "errdata";
    String custom = "custom";
    String req_data = "req_data";
    String content = "content";//存放文本消息内容
    String member = "member";//存放消息的用户信息
    String mother = "mother";//存放消息的红娘用户信息
    String room_id = "room_id";
    String need_gold = "needGold";//邀请上麦时，需要收取多少音符
    String position = "position";//邀请上麦时，上麦的麦位
    String roomUid = "roomUid";//邀请进房时，房主uid
    String type = "type";//邀请进房时，房间类型
}
