package com.tongdaxing.erban.libcommon.net.rxnet;

/**
 * Created by huangmeng1 on 2018/1/17.
 */

public class AlreadyOpenExeption extends Exception {
    public AlreadyOpenExeption(String message) {
        super(message);
    }
}
