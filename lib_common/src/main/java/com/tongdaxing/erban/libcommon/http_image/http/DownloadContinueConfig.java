package com.tongdaxing.erban.libcommon.http_image.http;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Properties;

/**
 * 断点续传文件信息记录
 *
 * @author zhongyongsheng on 14-9-29.
 */
public class DownloadContinueConfig {

    private Properties properties = new Properties();
    private File mPath;

    public DownloadContinueConfig(String path) {
        this.mPath = new File(path);
    }

    public boolean exists() {
        boolean result = mPath.exists();
        return result;
    }

    public void create() throws IOException {
        mPath.createNewFile();
    }

    public void put(String key, String value) {
        properties.setProperty(key, value);
    }

    public String get(String key) {
        String value = properties.getProperty(key);
        return value;
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        try {
            String value = get(key);
            if (value != null) {
                return Boolean.valueOf(value);
            } else {
                return defaultValue;
            }
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public int getInt(String key, int defaultValue) {
        try {
            String value = get(key);
            if (value != null) {
                return Integer.valueOf(value);
            } else {
                return defaultValue;
            }
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public void load() throws IOException {
        InputStreamReader inputStreamReader = new InputStreamReader(
                new FileInputStream(mPath), "UTF-8");
        properties.load(inputStreamReader);
        try {
            inputStreamReader.close();
        } catch (IOException e) {
        }
    }

    public void save() throws IOException {
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
                new FileOutputStream(mPath), "UTF-8");
        properties.store(outputStreamWriter, null);
        try {
            outputStreamWriter.close();
        } catch (IOException e) {
        }
    }

    public boolean delete() {
        return mPath.delete();
    }
}
