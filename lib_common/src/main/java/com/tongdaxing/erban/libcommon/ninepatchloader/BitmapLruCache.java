package com.tongdaxing.erban.libcommon.ninepatchloader;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import java.io.Closeable;
import java.io.IOException;

/**
 * 参考:https://www.jianshu.com/p/ee7b943a6d41
 *
 * @author weihaitao
 * @date 2019/6/19
 */
public class BitmapLruCache extends LruCache<String, Bitmap> implements Closeable {

    public static final int defaultMaxSize = (int) (Runtime.getRuntime().totalMemory() / 8);

    public BitmapLruCache() {
        this(defaultMaxSize);
    }

    // maxSize 一般为可用最大内存的 1/8.
    public BitmapLruCache(int maxSize) {
        super(maxSize);
    }

    @Override
    protected int sizeOf(String key, Bitmap value) {
        // 重写获取某个节点的内存大小，不写默认返回1
        return value.getRowBytes() * value.getHeight();
    }

    // 某节点被移除后调用该函数
    @Override
    protected void entryRemoved(boolean evicted, String key, Bitmap oldValue, Bitmap newValue) {
        super.entryRemoved(evicted, key, oldValue, newValue);
    }

    /**
     * Closes this stream and releases any system resources associated
     * with it. If the stream is already closed then invoking this
     * method has no effect.
     *
     * <p> As noted in {@link AutoCloseable#close()}, cases where the
     * close may fail require careful attention. It is strongly advised
     * to relinquish the underlying resources and to internally
     * <em>mark</em> the {@code Closeable} as closed, prior to throwing
     * the {@code IOException}.
     *
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void close() throws IOException {

    }
}
