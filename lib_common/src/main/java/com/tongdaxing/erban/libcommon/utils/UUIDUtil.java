package com.tongdaxing.erban.libcommon.utils;

import java.util.UUID;

/**
 * Created by lijun on 2014/12/3.
 */
public class UUIDUtil {

    public static String getUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
}
