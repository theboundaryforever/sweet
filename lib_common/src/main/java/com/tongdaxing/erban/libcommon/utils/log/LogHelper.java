package com.tongdaxing.erban.libcommon.utils.log;


import android.content.Context;
import android.os.Environment;

import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by zwk on 16/8/20.
 */

public class LogHelper {
    private static final String DATA_PATH = Environment.getExternalStorageDirectory() + File.separator
            + BasicConfig.INSTANCE.getAppContext().getPackageName() + File.separator + "log";


    public String getCurrentLocalLogName() {
        DateFormat formatter = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
        return formatter.format(new Date());
    }

    public String getBeforeLocalLogName() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, -1);//往上推一天  30推三十天  365推一年
        DateFormat formatter = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
        return formatter.format(calendar.getTime());
    }

    public String getCurrentCrashLogName() {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return "crash-" + formatter.format(new Date()) + "-";
    }


    public  File getLogFile(Context context){
        return compressFile(getLogFileList(context));
    }

    private List<File> getLogFileList(Context context) {
        List<File> files = new ArrayList<File>();
        String logPath = DATA_PATH + File.separator + "";
        //获取本地日志文件
        String currentLogName = getCurrentLocalLogName();
        String beforeLogName = getBeforeLocalLogName();
        File localLog = new File(logPath + "local");
        if (localLog.exists()) {//获取本地文件中前一天日志和当前日期
            File[] logFiles = localLog.listFiles();
            if (null != logFiles && logFiles.length > 0) {
                for (File log : logFiles) {
                    if (log.getName().startsWith(currentLogName)
                            || log.getName().startsWith(beforeLogName)) {
                        files.add(log);
                    }
                }
            }
        }
        //获取本地crash日志文件
        String currentCrashName = getCurrentCrashLogName();
        File crashLog = new File(logPath + "crash");
        if (crashLog.exists()) {
            File[] crashFiles = crashLog.listFiles();
            if (null != crashFiles && crashFiles.length > 0) {
                for (File crash : crashFiles) {
                    if (crash.getName().startsWith(currentCrashName)) {
                        files.add(crash);
                    }
                }
            }
        }
        //获取声网日志文件文件
        File agoraLog = new File(logPath + "agora-rtc.log");
        if (agoraLog.exists()) {
            files.add(agoraLog);
        }
//        //获取即构日志文件
//        String zegoFileName = "zegoavlog";
//        if (context != null) {
//            File zegoPath = context.getExternalFilesDir(null);
//            if (zegoPath != null && zegoPath.exists()) {
//                File[] zegoFiles = zegoPath.listFiles();
//                if (null != zegoFiles && zegoFiles.length > 0) {
//                    for (File crash : zegoFiles) {
//                        if (crash.getName().startsWith(zegoFileName)) {
//                            files.add(crash);
//                        }
//                    }
//                }
//            }
//        }

        return files;
    }

    /**
     * 压缩文件
     *
     * @param files
     * @return
     */
    private File compressFile(List<File> files) {
        if (files == null || files.size() <= 0)
            return null;
        byte[] buffer = new byte[1024];
        try {
            String zipPath = DATA_PATH + File.separator + "logsZip.zip";
            File zipFile = new File(zipPath);
            if (zipFile.exists()) {
                zipFile.delete();
            }
            zipFile.createNewFile();
            FileOutputStream fos = new FileOutputStream(zipPath);
            ZipOutputStream zos = new ZipOutputStream(fos);
            for (File file : files) {
                if (file == null || !file.exists())
                    continue;
                ZipEntry ze = new ZipEntry(file.getName());
                zos.putNextEntry(ze);
                FileInputStream in = new FileInputStream(file);
                int len;
                while ((len = in.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }
                in.close();
            }
            zos.closeEntry();
            zos.close();
            return zipFile;
        } catch (Exception ex) {
            return null;
        }
    }

}
