package com.tongdaxing.erban.libcommon.im;


import com.tongdaxing.erban.libcommon.utils.json.Json;

public class IMReportBean extends IMMsgBean {

    public final static int CODE_NO_ERROR = 0;

    private ReportData reportData;

    public IMReportBean(String data) {
        super(data);
        reportData = new ReportData(resData);
    }

    public IMReportBean(Json data) {
        super(data);
        reportData = new ReportData(resData);
    }

    public ReportData getReportData() {
        return reportData;
    }

    public class ReportData {
        public int errno;
        public String errmsg;
        public Json data;
        public Json errdata;

        public ReportData(Json json) {
            if (json == null)
                return;
            errno = json.num(IMKey.errno);
            errmsg = json.str(IMKey.errmsg);
            errdata = json.json_ok(IMKey.errdata);
            data = json.json_ok(IMKey.data);
        }
    }
}
