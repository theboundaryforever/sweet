package com.tongdaxing.erban.libcommon.utils.file;

/**
 * Creator: 舒强睿
 * Date:2015/6/24
 * Time:18:40
 * <p/>
 * Description：存储设备不可用异常（sd卡无法识别、sd卡找不到..）
 */
public class TargetNotPreparedException extends CustomFileException {
}
