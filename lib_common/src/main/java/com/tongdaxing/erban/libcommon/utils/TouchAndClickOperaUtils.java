package com.tongdaxing.erban.libcommon.utils;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

/**
 * 手势滑动拖拽及点击监听工具类
 * 注意，目标组件父布局必须是相对布局
 *
 * 另外，参考https://blog.csdn.net/qq_23933981/article/details/53992891
 * -----------------------------------------------------------------
 *
 * 顾名思义，getParent就是获取view的父亲节点，而getRootView是寻找当前的view层次中处在最顶层的view，可理解为找出该view实例所在的view层次的根view。
 * 如果这个view文件只是一个activity.setContentView时所引用的一个view。
 *
 * 1.当view处于xml文件的根节点时，通过getParent到的view都是它身。
 * 2当view处于xml的非根节点时，通过getParent获得的是view的父亲节点。
 * 3.无论处于xml的根节点还是子节点，通过getRootView获得的都是当前Activity的DecorView
 * （关于DecorView，可参考http://blog.csdn.net/houliang120/article/details/51138087）。
 *
 * 区别于上面的情况，如果这个view处于Fregment中（猜测viewpager这些可能类似）
 * 1.当view处于xml文件的根节点时，通过getParent获得的是null（猜测这个可能是由于view处于Fregment中，而又无法获取父节点导致）。
 * 2.当view处于非根节点时，通过getParent获得的是它的父亲节点
 * 3.无论处于xml的根节点还是子节点，通过getRootView获取的都是它本身。
 * ---------------------
 *
 * Created by 魏海涛 2018年11月28日 11:01:56
 */
public class TouchAndClickOperaUtils {

    private final String TAG = TouchAndClickOperaUtils.class.getSimpleName();

    private View.OnTouchListener onTouchListener;
    private long lastDownTimeMillis;
    private int lastDownXTrans;
    private int lastDownYTrans;
    private int mWidthPixels;
    private int mHeightPixels;

    public void setMarginButtom(int marginButtom) {
        this.marginButtom = marginButtom;
    }

    private boolean isNeedCheckQuickClick = true;
    private int marginButtom = 0;
    private boolean hasPositionChanged = false;

    public TouchAndClickOperaUtils(Context context){

        mHeightPixels = context.getResources().getDisplayMetrics().heightPixels;
        mWidthPixels = context.getResources().getDisplayMetrics().widthPixels;

        onTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(null == targetView || null == targetView.getParent()){
                    return false;
                }
                final int x = (int) event.getRawX();
                final int y = (int) event.getRawY();
                LogUtils.d(TAG, "onTouch: x= " + x + "y=" + y);
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        if (ButtonUtils.isFastDoubleClick(v.getId())) {
                            break;
                        }
                        lastDownTimeMillis = System.currentTimeMillis();
                        ViewGroup.MarginLayoutParams params = null;
                        if(targetView.getParent() instanceof RelativeLayout){
                            params = (RelativeLayout.LayoutParams) targetView
                                    .getLayoutParams();
                        }else if(targetView.getParent() instanceof FrameLayout){
                            params = (FrameLayout.LayoutParams) targetView
                                    .getLayoutParams();
                        }
                        if (params != null) {
                            lastDownXTrans = x - params.leftMargin;
                            lastDownYTrans = y - params.topMargin;
                        }
                        break;
                    case MotionEvent.ACTION_MOVE:
                        ViewGroup.MarginLayoutParams layoutParams = null;
                        if(targetView.getParent() instanceof RelativeLayout){
                            layoutParams = (RelativeLayout.LayoutParams) targetView
                                    .getLayoutParams();
                        }else if(targetView.getParent() instanceof FrameLayout){
                            layoutParams = (FrameLayout.LayoutParams) targetView
                                    .getLayoutParams();
                        }
                        if (layoutParams != null) {
                            int width = layoutParams.width;
                            int height = layoutParams.height;

                            int xDistance = x - lastDownXTrans;
                            int yDistance = y - lastDownYTrans;

                            int outX = (mWidthPixels - width) - 10;
                            if (xDistance > outX) {
                                xDistance = outX;
                            }

                            int outY = mHeightPixels - height - marginButtom;
                            if (yDistance > outY) {
                                yDistance = outY;
                            }

                            if (yDistance < 100) {
                                yDistance = 100;
                            }
                            if (xDistance < 10) {
                                xDistance = 10;
                            }

                            layoutParams.leftMargin = xDistance;
                            layoutParams.topMargin = yDistance;
                            targetView.setLayoutParams(layoutParams);
                            hasPositionChanged = true;
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        if (null != listener) {
                            if (isNeedCheckQuickClick) {
                                if (System.currentTimeMillis() - lastDownTimeMillis < quickTimeInterval) {
                                    listener.onQuickClick();
                                }
                            } else {
                                listener.onQuickClick();
                            }
                        }
                        break;
                    default:
                        break;
                }
                targetView.getParent().requestLayout();
                return true;
            }
        };
    }

    public void setNeedCheckQuickClick(boolean needCheckQuickClick) {
        isNeedCheckQuickClick = needCheckQuickClick;
    }

    public void setQuickTimeInterval(int quickTimeInterval) {
        this.quickTimeInterval = quickTimeInterval;
    }

    /**
     * 视作快速点击事件的最小时间间隔
     */
    private int quickTimeInterval = 150;

    public void setTargetView(View targetView) {
        if (null != targetView) {
            if (targetView.getRootView() == null || !(targetView.getParent() instanceof RelativeLayout || targetView.getParent() instanceof FrameLayout)) {
                throw new RuntimeException("必须是非根节点，父布局必须是RelativeLayout|FrameLayout");
            }
            targetView.setOnTouchListener(onTouchListener);
        }
        this.targetView = targetView;
    }

    private View targetView;

    public boolean isPositionChanged() {
        return hasPositionChanged;
    }


    public void setListener(OnQuickClickListener listener) {
        this.listener = listener;
        LogUtils.d(TAG, "setListener");
    }

    private OnQuickClickListener listener ;

    public interface OnQuickClickListener {
        void onQuickClick();
    }
}
