package com.tongdaxing.erban.libcommon.im;


import org.java_websocket.handshake.ServerHandshake;

public interface IConnectListener {
    void onSuccess(ServerHandshake serverHandshake);

    void onError(Exception e);
}
