package com.tongdaxing.erban.libcommon.http_image.http;

/**
 * Http 请求错误的监听器
 *
 * @author zhongyongsheng
 */
public interface ResponseErrorListener {

    /**
     * 返回错误结果,异常类型如下:
     * <p/>
     *
     * @param error 错误结果
     */
    public void onErrorResponse(RequestError error);
}
