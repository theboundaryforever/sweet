package com.tongdaxing.erban.libcommon.im;

abstract public class IMCallBack {

    public static int callbackIndex = 0;
    private final int callbackId;

    public IMCallBack() {
        callbackId = callbackIndex++;
        //应该用不上
        if (callbackIndex == Integer.MAX_VALUE) {
            callbackIndex = 0;
        }
    }

    public abstract void onSuccess(String data);

    public abstract void onError(int errorCode, String errorMsg);

    public int getCallbackId() {
        return callbackId;
    }
}
