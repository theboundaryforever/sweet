package com.tongdaxing.erban.libcommon.utils.download.cache;

public interface ReturnCallback {

    public void onReturn(Object data) throws Exception;
}
