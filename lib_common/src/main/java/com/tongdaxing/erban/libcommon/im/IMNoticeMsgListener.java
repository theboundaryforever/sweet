package com.tongdaxing.erban.libcommon.im;

import com.tongdaxing.erban.libcommon.utils.json.Json;

/**
 * @author weihaitao
 * @date 2019/5/16
 */
public interface IMNoticeMsgListener {

    void onNotice(Json json);

    //断网重连进入聊天室成功
    void onDisConnectEnterRoomSuc();

    void onDisConnectEnterRoomFail(int errorCode, String errorMsg);

    void onDisConnection(boolean isCloseSelf);

    void onLoginError(int err_code, String reason);

    //断网重连登录IM成功
    void onDisConntectIMLoginSuc();

}
