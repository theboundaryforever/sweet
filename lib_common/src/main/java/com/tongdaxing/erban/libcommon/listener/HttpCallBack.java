package com.tongdaxing.erban.libcommon.listener;

/**
 * <p> 网络回调接口</p>
 *
 * @author jiahui
 * @date 2017/12/7
 */
public interface HttpCallBack<T> extends CallBack<T>{

    /**
     * 获取数据失败回调方法
     *
     * @param code  错误码
     * @param error 失败的信息
     * @param t 失败数据但是有结果
     */
    void onFail(int code, String error,T t);
}