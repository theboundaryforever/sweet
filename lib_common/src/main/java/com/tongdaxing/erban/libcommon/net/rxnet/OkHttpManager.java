package com.tongdaxing.erban.libcommon.net.rxnet;


import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.internal.$Gson$Types;
import com.tmxq.ndklib.JniUtils;
import com.tongdaxing.erban.libcommon.http_image.http.DefaultRequestProcessor;
import com.tongdaxing.erban.libcommon.http_image.http.DownloadRequest;
import com.tongdaxing.erban.libcommon.http_image.http.ProgressListener;
import com.tongdaxing.erban.libcommon.http_image.http.RequestProcessor;
import com.tongdaxing.erban.libcommon.http_image.http.ResponseErrorListener;
import com.tongdaxing.erban.libcommon.http_image.http.ResponseListener;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.RetrofitCallback;
import com.tongdaxing.erban.libcommon.utils.LogUtil;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SignUtils;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;

import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

import static com.tongdaxing.erban.libcommon.BuildConfig.IS_DEBUG;


/**
 * 创建者     polo
 * 创建时间   2017/8/19 17:13
 * 描述	      ${}
 * <p>
 * 更新者     $Author$
 * 更新时间   $Date$
 * 更新描述   ${}
 */

public class OkHttpManager implements RetrofitCallback.RetrofitResponseCallback {

    //是否测试模式，true--测试模式，测试模式下接口请求的参数均未加密，false-非测试模式，接口请求的参数均有加密操作
    private final static boolean isSignDebugModel = false;
    public static Handler mHandler = new Handler(Looper.getMainLooper());
    private static Context context;
    private static OkHttpManager mInstance;
    private String kp = "";
    private Gson mGson = new Gson();
    private RequestProcessor mCommonProcessor;
    public final static int DEFAULT_CODE_ERROR = -1;
    public final static String DEFAULT_MSG_ERROR = "数据异常，请稍后重试！";

    private OkHttpManager() {
        mCommonProcessor = new DefaultRequestProcessor(2, "Http_");
        mCommonProcessor.start();
    }

    public static Map<String, String> getDefaultParam() {
        return CommonParamUtil.getDefaultParam();
    }

    public static OkHttpManager getInstance() {
        if (mInstance == null) {
            synchronized (OkHttpManager.class) {
                if (mInstance == null) {
                    mInstance = new OkHttpManager();
                }
            }
        }
        return mInstance;
    }

    public void init(Context context) {
        OkHttpManager.context = context;
        kp = JniUtils.getDk(context, IS_DEBUG ? 1 : 0);
    }

    /**
     * 最新的请求方式 - 签名头
     *
     * @param url
     * @param params
     * @return
     */
    private Map<String, String> getSignHeader(String url, Map<String, String> headers, Map<String, String> params) {
        LogUtils.d("request_info", "url-->>\n" + url);
        //打印日志
        if (headers != null) {
            StringBuilder headersBuilder = new StringBuilder();
            for (Map.Entry<String, String> p : headers.entrySet()) {
                headersBuilder.append(p.getKey()).append("=").append(p.getValue()).append("  ");
            }
            LogUtils.d("request_info", "header-->>\n" + headersBuilder.toString());
        }
        if (params != null) {
            StringBuilder paramsBuilder = new StringBuilder();
            for (Map.Entry<String, String> p : params.entrySet()) {
                paramsBuilder.append(p.getKey()).append("=").append(p.getValue()).append("&");
            }
            LogUtils.d("request_info", "body-->>\n" + paramsBuilder.substring(0, paramsBuilder.length() - 1));
        }
        if (headers == null) {
            headers = new HashMap<>();
        }
        String time = System.currentTimeMillis() + "";
        //加上签名
        headers.put("t", time);
        LogUtils.d(OkHttpManager.class.getSimpleName(), "getSignHeader-->getSign");
        String sign = SignUtils.getSign(url, params, kp, time);
        headers.put("sn", sign);
        return headers;
    }

    /**
     * GET请求
     *
     * @param url
     * @param params
     * @param myCallBack
     */
    public void getRequest(String url, Map<String, String> params, final MyCallBack myCallBack) {
        getRequest(url, null, params, myCallBack);
    }

    /**
     * GET请求
     */
    public void getRequest(String url, Map<String, String> params, final HttpRequestCallBack callBack) {
        getRequest(url, null, params, null, callBack);
    }

    public void getRequest(String url, Map<String, String> headers, Map<String, String> params, final HttpRequestCallBack callBack) {
        getRequest(url, headers, params, null, callBack);
    }

    public void getRequest(String url, Map<String, String> headers, Map<String, String> params, final MyCallBack callBack) {
        getRequest(url, headers, params, callBack, null);
    }

    /**
     * GET请求 - 带有请求头的
     */
    public void getRequest(String url, Map<String, String> headers, Map<String, String> params, final MyCallBack myCallBack, HttpRequestCallBack callBack) {
        dealParamEmptyEx(params);
        LogUtils.d(OkHttpManager.class.getSimpleName(), "getRequest-->getSignHeader");
        Map<String, String> sgHeader = getSignHeader(url, headers, params);
        try {
            encryptParams(params);
        } catch (Exception e) {
            e.printStackTrace();
            onError(myCallBack, callBack, DEFAULT_CODE_ERROR, DEFAULT_MSG_ERROR);
        }
        if (callBack != null) {
            RxNet.create(RxNetService.class).getCallAddHeader(url, sgHeader, params).enqueue(new RetrofitCallback<ResponseBody>(callBack, this));
        } else if (myCallBack != null) {
            RxNet.create(RxNetService.class).getCallAddHeader(url, sgHeader, params).enqueue(new RetrofitCallback<ResponseBody>(myCallBack, this));
        }
    }

    /**
     * post请求
     */
    @Deprecated
    public void postRequest(String url, Map<String, String> params, final MyCallBack myCallBack) {
        postRequest(url, null, params, myCallBack, null);
    }

    /**
     * post请求
     */
    public Call<ResponseBody> postRequest(String url, Map<String, String> params, final HttpRequestCallBack callBack) {
        return postRequest(url, null, params, null, callBack);
    }

    /**
     * post请求
     */
    public void postRequest(String url, Map<String, String> headers, Map<String, String> params, final HttpRequestCallBack callBack) {
        postRequest(url, headers, params, null, callBack);
    }

    /**
     * post请求
     */
    public void postRequest(String url, Map<String, String> headers, Map<String, String> params, final MyCallBack callBack) {
        postRequest(url, headers, params, callBack, null);
    }

    /**
     * post请求 - 带有请求头的
     *
     * @param url
     * @param params
     * @param myCallBack
     */
    public Call<ResponseBody> postRequest(String url, Map<String, String> headers, Map<String, String> params, MyCallBack myCallBack, HttpRequestCallBack callBack) {
        dealParamEmptyEx(params);
        Map<String, String> sgHeader = getSignHeader(url, headers, params);
        try {
            encryptParams(params);
        } catch (Exception e) {
            e.printStackTrace();
            onError(myCallBack, callBack, DEFAULT_CODE_ERROR, DEFAULT_MSG_ERROR);
        }
        Call<ResponseBody> call = null;
        if (callBack != null) {
            call = RxNet.create(RxNetService.class).postCallAddHeader(url, sgHeader, params);
            call.enqueue(new RetrofitCallback<ResponseBody>(callBack, this));
        } else if (myCallBack != null) {
            call = RxNet.create(RxNetService.class).postCallAddHeader(url, sgHeader, params);
            call.enqueue(new RetrofitCallback<ResponseBody>(myCallBack, this));
        } else {
            call = RxNet.create(RxNetService.class).postCallAddHeader(url, sgHeader, params);
            call.enqueue(new RetrofitCallback<ResponseBody>((HttpRequestCallBack) null, this));
        }
        return call;
    }

    /**
     * 处理返回结果
     *
     * @param myCallBack
     */
    private void dealResponseResult(retrofit2.Call<ResponseBody> call, retrofit2.Response<ResponseBody> response, HttpRequestCallBack callBack, MyCallBack myCallBack) {
        if (myCallBack == null && callBack == null) {
            return;
        }
        if (call.request() != null && call.request().url() != null) {
            LogUtils.d("request_info", "start decryptParams-------------------->>>");
            LogUtils.d("request_info", "code-->>" + response.code() + "\nurl-->>" + call.request().url().toString());
        }
        if (response == null) {
            onError(myCallBack, callBack, DEFAULT_CODE_ERROR, DEFAULT_MSG_ERROR);
            return;
        }
        String string = "";
        if (response.body() != null) {
            try {
                string = decryptParams(response.body());
//                string = response.body().string();
                LogUtils.d("request_info", "response_body decryptResult-->>\n" + string);
            } catch (Exception e) {
                onError(myCallBack, callBack, DEFAULT_CODE_ERROR, e.getMessage());
            }
            int code = DEFAULT_CODE_ERROR;
            String message = "";
            Object data = null;
            if (myCallBack != null) {
                try {
                    if (myCallBack.mType.toString().equals(Json.class.toString())) {
                        data = Json.parse(string);
                    } else if (myCallBack.mType.toString().equals(String.class.toString())) {
                        data = string;
                    } else {
                        data = mGson.fromJson(string, myCallBack.mType);
                    }
                    onResponse(myCallBack, callBack, code, message, data);
                } catch (Exception e) {
                    onError(myCallBack, callBack, DEFAULT_CODE_ERROR, e.getMessage());
                }
            }
            if (callBack != null) {
                try {
                    JSONObject mResultJson = new JSONObject(string);
                    if (mResultJson.has("data") && !mResultJson.isNull("data")) {
                        String resultStr = mResultJson.getString("data");
                        if (callBack.mType.toString().equals(Json.class.toString())) {
                            data = Json.parse(resultStr);
                        } else if (callBack.mType.toString().equals(String.class.toString())) {
                            data = resultStr;
                        } else {
                            data = mGson.fromJson(resultStr, callBack.mType);
                        }
                    }
                    if (mResultJson.has("code") && !mResultJson.isNull("code")) {
                        code = mResultJson.getInt("code");
                    } else {
                        code = DEFAULT_CODE_ERROR;
                    }
                    if (mResultJson.has("message") && !mResultJson.isNull("message")) {
                        message = mResultJson.getString("message");
                    } else {
                        message = DEFAULT_MSG_ERROR;
                    }
                    onResponse(myCallBack, callBack, code, message, data);
                } catch (Exception e) {
                    onError(myCallBack, callBack, DEFAULT_CODE_ERROR, e.getMessage());
                }
            }
        } else {
            if (response.errorBody() != null) {
                try {
                    string = response.errorBody().string();
                    onError(myCallBack, callBack, DEFAULT_CODE_ERROR, string);
                } catch (IOException e) {
                    onError(myCallBack, callBack, DEFAULT_CODE_ERROR, DEFAULT_MSG_ERROR);
                }
            } else {
                onError(myCallBack, callBack, DEFAULT_CODE_ERROR, DEFAULT_MSG_ERROR);
            }
        }
        LogUtils.d("request_info", "<<-----------------------end decryptParams");
    }

    private void onResponse(MyCallBack myCallBack, HttpRequestCallBack callBack,
                            int code, String message, Object response) {
        if (myCallBack != null) {
            try {
                myCallBack.onResponse(response);
            } catch (Exception e) {
                e.printStackTrace();
                if (!TextUtils.isEmpty(e.getMessage())) {
                    LogUtils.w("OkHttpManager.onResponse", e.getMessage());
                }
            }
        }
        if (callBack != null) {
            try {
                callBack.onFinish();
            } catch (Exception e) {
                e.printStackTrace();
                if (!TextUtils.isEmpty(e.getMessage())) {
                    LogUtils.w("OkHttpManager.onResponse", e.getMessage());
                }
            }
            if (code == ServiceResult.SC_SUCCESS) {
                try {
                    callBack.onSuccess(message, response);
                } catch (Exception e) {
                    e.printStackTrace();
                    if (!TextUtils.isEmpty(e.getMessage())) {
                        LogUtils.w("OkHttpManager.onResponse", e.getMessage());
                    }
                }
            } else {
                try {
                    callBack.onFailure(code, message);
                } catch (Exception e) {
                    e.printStackTrace();
                    if (!TextUtils.isEmpty(e.getMessage())) {
                        LogUtils.w("OkHttpManager.onResponse", e.getMessage());
                    }
                }
            }
        }
    }

    private void onError(MyCallBack myCallBack, HttpRequestCallBack callBack, int code, String
            message) {
        if (myCallBack != null) {
            try {
                myCallBack.onError(new Exception(message));
            } catch (Exception e) {
                e.printStackTrace();
                if (!TextUtils.isEmpty(e.getMessage())) {
                    LogUtils.w("OkHttpManager.onError", e.getMessage());
                }
            }
        }
        if (callBack != null) {
            try {
                callBack.onFinish();
                callBack.onFailure(code, message);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 下载文件请求,调用者保证下载到本地文件的目录已经创建好
     *
     * @param url                 下载服务器路径
     * @param downloadFilePath    下载本地路径
     * @param successListener     成功回调
     * @param errorListener       失败回调
     * @param progressListener    进度回调
     * @param useContinueDownload 是否使用断点续传
     * @return
     */
    public DownloadRequest downloadRequest(String url,
                                           Map<String, String> headers,
                                           String downloadFilePath,
                                           ResponseListener<String> successListener,
                                           ResponseErrorListener errorListener,
                                           ProgressListener progressListener,
                                           boolean useContinueDownload) {
        if (url == null || downloadFilePath == null || successListener == null
                || errorListener == null || progressListener == null) {
            return null;
        }
        DownloadRequest req = new DownloadRequest(url, downloadFilePath, successListener,
                errorListener, progressListener, useContinueDownload);
        req.getHeaders().putAll(headers);
        mCommonProcessor.add(req);
        return req;
    }

    @Override
    public void onResponse(Call call, Response response, HttpRequestCallBack
            requestCallBack, OkHttpManager.MyCallBack myCallBack) {
        dealResponseResult(call, response, requestCallBack, myCallBack);
    }


    @Override
    public void onFailure(Call call, Throwable t, HttpRequestCallBack
            requestCallBack, OkHttpManager.MyCallBack myCallBack) {
        onError(myCallBack, requestCallBack, DEFAULT_CODE_ERROR, t == null ? DEFAULT_MSG_ERROR : t.getMessage());
    }

    public static abstract class MyCallBack<T> {
        Type mType;

        public MyCallBack() {
            mType = getSuperclassTypeParameter(getClass());
        }

        static Type getSuperclassTypeParameter(Class<?> subclass) {
            Type superclass = subclass.getGenericSuperclass();
            if (superclass instanceof Class) {
                throw new RuntimeException("Missing type parameter.");
            }
            ParameterizedType parameterized = (ParameterizedType) superclass;
            return $Gson$Types.canonicalize(parameterized.getActualTypeArguments()[0]);
        }

        public abstract void onError(Exception e);

        public abstract void onResponse(T response);
    }

    /**
     * 处理参数为null的时候导致的retrofit的异常崩溃
     */
    private void dealParamEmptyEx(Map<String, String> params) {
        if (params != null) {
            Set<String> keySet = params.keySet();
            for (String key : keySet) {
                if (StringUtils.isEmpty(params.get(key))) {
                    params.put(key, "");
                }
            }
        }
    }

    private void encryptParams(Map<String, String> params) throws Exception {
        if (isSignDebugModel) {
            return;
        }
        if (params != null && !params.isEmpty()) {
            StringBuilder paramsBuilder = new StringBuilder();
            for (Map.Entry<String, String> param : params.entrySet()) {
                paramsBuilder.append(param.getKey()).append("=").append(URLEncoder.encode(param.getValue(), "utf-8")).append("&");
            }
            String paramsStr = paramsBuilder.substring(0, paramsBuilder.length() - 1);//去掉最后一个&（最后一个必然会是&）
            LogUtil.d("request_info", "pre_encrypt_body-->>\n" + paramsStr);
            params.clear();
            params.put("ed", JniUtils.encryptAes(context, paramsStr));//只传加密后参数ed
        }
    }

    private String decryptParams(ResponseBody data) throws Exception {
        String bodyStr;
        Json bodyJson;
        try {
            bodyStr = data.string();
            LogUtil.d("request_info", "pre_decrypt_body-->>\n" + bodyStr);
            bodyJson = Json.parse(bodyStr);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("数据异常，请稍后重试！");
        }
        String string;
        if (bodyJson == null || !bodyJson.has("ed")) {
            string = bodyStr;
        } else {
            try {
                string = JniUtils.decryptAes(context, bodyJson.getString("ed"));
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception("数据异常，请稍后重试！");
            }
        }
        LogUtils.d("request_info", "pre_decrypt_string:" + string);
        return string;
    }

}