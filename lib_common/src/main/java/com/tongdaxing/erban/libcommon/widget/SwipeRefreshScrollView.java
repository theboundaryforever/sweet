package com.tongdaxing.erban.libcommon.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.widget.ScrollView;

import com.tongdaxing.erban.libcommon.utils.LogUtils;

/**
 * Created by LXL on 2017/05/03.
 */

public class SwipeRefreshScrollView extends ScrollView {

    private final String TAG = SwipeRefreshScrollView.class.getSimpleName();

    private int downX;
    private int downY;
    private int mTouchSlop;

    public boolean isScrollToBottom() {
        return isScrollToBottom;
    }

    public void setScrollToBottom(boolean scrollToBottom) {
        LogUtils.d(TAG, "setScrollToBottom-scrollToBottom:" + scrollToBottom);
        isScrollToBottom = scrollToBottom;
    }

    private boolean isScrollToBottom = false;//是不是滑动到了最低端 ；使用这个方法，解决了上拉加载的问题
    private OnScrollToBottomListener onScrollToBottom;

    public SwipeRefreshScrollView(Context context) {
        super(context);
        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    public SwipeRefreshScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    public SwipeRefreshScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    /**
     * @param scrollX  距离原点的X轴的距离
     * @param scrollY  距离原点的Y轴的距离
     * @param clampedX 当ScrollView滑动到左侧边界的时候值为true
     * @param clampedY 当ScrollView滑动到下边界的时候值为true
     */
    @Override
    protected void onOverScrolled(int scrollX, int scrollY, boolean clampedX, boolean clampedY) {
        super.onOverScrolled(scrollX, scrollY, clampedX, clampedY);
        LogUtils.d(TAG, "onOverScrolled-scrollX:" + scrollX + " scrollY:" + scrollY + " clampedX:" + clampedX + " clampedY:" + clampedY);
        if (clampedY && null != onScrollToBottom && isScrollToBottom()) {
            onScrollToBottom.onScrollBottomListener(clampedY);
        }
    }

    public void setOnScrollToBottomLintener(OnScrollToBottomListener listener) {
        onScrollToBottom = listener;
    }

    public interface OnScrollToBottomListener {
        public void onScrollBottomListener(boolean isBottom);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent e) {
        int action = e.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                setScrollToBottom(false);
                downX = (int) e.getRawX();
                downY = (int) e.getRawY();
                LogUtils.d(TAG, "MotionEvent.ACTION_DOWN-downY:" + downY);
                break;
            case MotionEvent.ACTION_MOVE:
                int moveY = (int) e.getRawY();
                LogUtils.d(TAG, "MotionEvent.ACTION_DOWN-moveY:" + moveY + " mTouchSlop:" + mTouchSlop);
                /****判断是向下滑动，才设置为true****/
                if (downY - moveY > 0) {
                    setScrollToBottom(true);
                } else {
                    setScrollToBottom(false);
                }
                if (Math.abs(moveY - downY) > mTouchSlop) {
                    return true;
                }
        }
        return super.onInterceptTouchEvent(e);
    }
}
