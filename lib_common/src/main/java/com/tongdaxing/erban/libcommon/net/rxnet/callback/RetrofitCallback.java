package com.tongdaxing.erban.libcommon.net.rxnet.callback;

import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 根据返回结果处理回调
 *
 * @param <T>
 */
public class RetrofitCallback<T> implements Callback<T> {
    private OkHttpManager.MyCallBack myCallBack;
    private HttpRequestCallBack requestCallBack;
    private RetrofitResponseCallback retrofitResponseCallback;

    public RetrofitCallback(OkHttpManager.MyCallBack myCallBack, RetrofitResponseCallback retrofitResponseCallback) {
        this.myCallBack = myCallBack;
        this.retrofitResponseCallback = retrofitResponseCallback;
    }

    public RetrofitCallback(HttpRequestCallBack requestCallBack, RetrofitResponseCallback retrofitResponseCallback) {
        this.requestCallBack = requestCallBack;
        this.retrofitResponseCallback = retrofitResponseCallback;
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (retrofitResponseCallback != null) {
            retrofitResponseCallback.onResponse(call, response, requestCallBack, myCallBack);
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        if (retrofitResponseCallback != null) {
            retrofitResponseCallback.onFailure(call, t, requestCallBack, myCallBack);
        }
    }

    public interface RetrofitResponseCallback<T> {
        void onResponse(Call<T> call, Response<T> response, HttpRequestCallBack requestCallBack, OkHttpManager.MyCallBack myCallBack);

        void onFailure(Call<T> call, Throwable t, HttpRequestCallBack requestCallBack, OkHttpManager.MyCallBack myCallBack);
    }
}
