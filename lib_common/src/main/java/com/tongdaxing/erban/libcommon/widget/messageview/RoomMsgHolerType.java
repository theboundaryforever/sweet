package com.tongdaxing.erban.libcommon.widget.messageview;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/1/29
 */
public interface RoomMsgHolerType {
    int MSG_DEFAULT_TXT = 0;
    int MSG_TXT = 1;
    int MSG_CHAT_TXT = 2;
    int MSG_GIFT = 3;
    int MSG_LOTTERY_BOX = 4;
    int MSG_TXT_NO_BG = 5;
}
