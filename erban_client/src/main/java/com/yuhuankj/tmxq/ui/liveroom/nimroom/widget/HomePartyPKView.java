package com.yuhuankj.tmxq.ui.liveroom.nimroom.widget;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.JavaUtil;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.NetworkUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.SortList;
import com.tongdaxing.erban.libcommon.utils.toast.ToastCompat;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.PkCustomAttachment;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.pk.IPKCoreClient;
import com.tongdaxing.xchat_core.pk.bean.PkVoteInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.adapter.PkUserFloatAdapter;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.gift.GiftDialog;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.reactivex.disposables.CompositeDisposable;

import static com.tongdaxing.xchat_core.pk.bean.PkVoteInfo.PkUser;

/**
 * PK 互动view
 * 存在全屏和最小化两种状态
 *
 * @author zwk 2018/7/3
 */
public class HomePartyPKView extends RelativeLayout implements View.OnClickListener, View.OnTouchListener {

    private final String TAG = HomePartyPKView.class.getSimpleName();

    private Context mContext;
    private RelativeLayout rlMinimize;//最小化布局
    private RelativeLayout rlFull;//大屏布局
    private ImageView ivMinimize;//最小化按钮
    private TextView tvMinCount;//最小化时的倒计时
    private TextView tvPkTimer;//最大化时的倒计时
    private PkVoteInfo pkVoteInfo;
    public boolean isFull = false;
    private boolean isShowing = false;

    private TextView tvPkName;
    private RecyclerView rcvUser;
    private TextView tvPkVote;
    private TextView tvPkType;
    private RelativeLayout rlEmply;
    //最小化时控件
    private ImageView imvPkUser1, imvPkUser2;
    private TextView tvPkCountTip;
    private PkUserFloatAdapter pkUserAdapter;
    private List<PkUser> datas;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public HomePartyPKView(Context context) {
        this(context, null);
    }

    public HomePartyPKView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HomePartyPKView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        initView(context);
        initListener();
    }

    private void initView(Context context) {
        inflate(context, R.layout.layout_home_party_pk, this);
        mWidthPixels = ScreenUtil.getScreenWidth(context);
        mHeightPixels = ScreenUtil.getScreenHeight(context);
        rlFull = findViewById(R.id.rl_pk_full_screen);
        tvPkTimer = findViewById(R.id.tvPkTimer);
        RelativeLayout.LayoutParams rl = (LayoutParams) rlFull.getLayoutParams();
        rl.width = 323 * mWidthPixels / 375;
//        rl.height = 204 * ScreenUtil.getScreenHeight(context)/667;
        rlFull.setLayoutParams(rl);
        rlMinimize = findViewById(R.id.rl_pk_minimize);
        ivMinimize = findViewById(R.id.iv_minimize);
        tvMinCount = findViewById(R.id.tv_pk_min_countdown);
        tvPkName = findViewById(R.id.tvPkName);
        rcvUser = findViewById(R.id.rcvUser);
        tvPkVote = findViewById(R.id.tvPkVote);
        tvPkType = findViewById(R.id.tvPkType);
        rlEmply = findViewById(R.id.rlEmply);
        //最小化时控件
        imvPkUser1 = findViewById(R.id.imvPkUser1);
        imvPkUser2 = findViewById(R.id.imvPkUser2);
        tvPkCountTip = findViewById(R.id.tvPkCountTip);

        GridLayoutManager layoutmanager = new GridLayoutManager(getContext(), 4);
        rcvUser.setLayoutManager(layoutmanager);
        datas = new ArrayList<>();
        pkUserAdapter = new PkUserFloatAdapter();
        pkUserAdapter.bindToRecyclerView(rcvUser);
        pkUserAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                try {
                    if (datas.get(position) == null || !tvPkVote.isEnabled()) {
                        return;
                    }
                    pkUserAdapter.setCurSelPos(position);
                    pkUserAdapter.setCurSelUid(JavaUtil.str2long(datas.get(position).getUid()));
                    pkUserAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        if (isFull) {
            if (rlMinimize.getVisibility() == View.VISIBLE) {
                rlMinimize.setVisibility(View.GONE);
            }
            if (rlFull.getVisibility() == View.GONE) {
                rlFull.setVisibility(View.VISIBLE);
            }
            disableClickOther(false);
        } else {
            disableClickOther(true);
        }
        rlEmply.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        rlFull.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        initData(false);
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            try {
                //倒计时剩余3和7秒分别请求
                if (countDuration == 3 || countDuration == 7) {
                    initData(true);
                }
                if (duration <= 0) {
                    handler.removeCallbacks(runnable);
                    resetState();
                    showPKWin();
                    return;
                }
                duration--;
                countDuration--;
                tvPkTimer.setText(duration + "S");
                tvMinCount.setText(duration + "S");
                handler.postDelayed(this, 1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void restorePkVoteStatus() {
        LogUtils.d(TAG, "restorePkVoteStatus hasPkVoted:" + AvRoomDataManager.get().hasPkVoted());
        if (AvRoomDataManager.get().hasPkVoted()) {
            tvPkVote.setText("已投票");
            tvPkVote.setEnabled(false);
            pkUserAdapter.setCurSelPos(-1);
            pkUserAdapter.setCurSelUid(0L);
            pkUserAdapter.notifyDataSetChanged();
        }
    }

    private void initListener() {
        rlMinimize.setOnClickListener(this);
        rlMinimize.setOnTouchListener(this);
        ivMinimize.setOnClickListener(this);
        tvPkVote.setOnClickListener(this);
    }

    private void disableClickOther(boolean isEnable) {
        if (isEnable) {
            rlEmply.setVisibility(GONE);
        } else {
            rlEmply.setVisibility(VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            //由全屏到最小化
            case R.id.iv_minimize:
                if (pkVoteInfo == null) {
                    resetState();
                    return;
                }
                isFull = false;
                if (rlFull.getVisibility() == View.VISIBLE) {
                    rlFull.setVisibility(View.GONE);
                }
                if (rlMinimize.getVisibility() == View.GONE) {
                    rlMinimize.setVisibility(View.VISIBLE);
                }
                disableClickOther(true);
                break;
            case R.id.tvPkVote:
                try {
                    int curSelPos = pkUserAdapter.getCurSelPos();
                    if (curSelPos != -1) {
                        if (pkVoteInfo.getPkType() == 2) {
                            //礼物pk
                            showGiftDialog(datas.get(curSelPos));
                        } else {
                            //投票pk
                            pkVote(datas.get(curSelPos).getUid() + "");
                        }
                    } else {
                        SingleToastUtil.showToast("请先选择要投票的用户");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
    }

    //倒计时
    Handler handler = new Handler();
    private int duration = 0;//真实的时间 -- 用于显示
    private int countDuration = 0;//用于倒计时 -- 如果延迟10秒依然出现问题则隐藏显示

    /**
     * 通过接口获取最新信息
     *
     * @param isVote true 投票操作  false初始化获取pk数据
     */
    private void initData(boolean isVote) {
        LogUtils.d(TAG, "initData isVote:" + isVote);
        if (!NetworkUtils.isNetworkAvailable(mContext)) {
            if (duration == 0) {
                resetState();
                restorePkVoteStatus();
            }
            return;
        }
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomId", (AvRoomDataManager.get().mCurrentRoomInfo == null ? 0 : AvRoomDataManager.get().mCurrentRoomInfo.getRoomId()) + "");
        OkHttpManager.getInstance().postRequest(UriProvider.getPkResult(), params, new OkHttpManager.MyCallBack<ServiceResult<PkVoteInfo>>() {
            @Override
            public void onError(Exception e) {
                LogUtils.d(TAG, "initData-->getPkResult-->onError duration:" + duration);
                e.printStackTrace();
                if (duration == 0) {
                    resetState();
                    restorePkVoteStatus();
                }
            }

            @Override
            public void onResponse(ServiceResult<PkVoteInfo> response) {
                LogUtils.d(TAG, "initData-->getPkResult-->onResponse isVote:" + isVote);
                if (isVote) {
                    if (response.getData() == null) {
                        return;
                    }
                    if (response.getData().getDuration() > 0) {
                        countDown(response.getData().getDuration());
                        setPkInfo(response.getData());
                        restorePkVoteStatus();
                    } else {
                        //屏蔽因为延迟导致的重复执行显示隐藏问题
                        if (!isShowing && pkVoteInfo != null && (rlFull.getVisibility() == View.VISIBLE
                                || rlMinimize.getVisibility() == View.VISIBLE)) {
                            dealWithPKEnd(response.getData());
                            AvRoomDataManager.get().setHasPkVoted(false);
                        }
                    }
                } else {
                    if (response != null && response.isSuccess()) {
                        if (response.getData() != null && response.getData().getDuration() > 0) {
                            setPkInfo(response.getData());
                            countDown(response.getData().getDuration());
                            restorePkVoteStatus();
                        } else {
                            resetState();
                            restorePkVoteStatus();
                        }
                    } else {
                        resetState();
                        restorePkVoteStatus();
                    }
                }
            }
        });
    }

    /**
     * 倒计时 + 延时10秒
     *
     * @param count
     */
    private void countDown(int count) {
        if (count <= 0) {
            handlerRelease();
            return;
        }
        countDuration = count + 10;
        duration = count;
        tvPkTimer.setText(duration + "S");
        tvMinCount.setText(duration + "S");
        if (handler != null) {
            try {
                handler.removeCallbacks(runnable);
                handler.postDelayed(runnable, 1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 释放handler
     */
    private void handlerRelease() {
        if (handler != null && runnable != null) {
            try {
                handler.removeCallbacks(runnable);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //给某用户投票
    private void pkVote(String voteUid) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomId", (AvRoomDataManager.get().mCurrentRoomInfo == null ?
                0 : AvRoomDataManager.get().mCurrentRoomInfo.getRoomId()) + "");
        params.put("voteUid", voteUid + "");
        OkHttpManager.getInstance().postRequest(UriProvider.sendPkVote(), params, new OkHttpManager.MyCallBack<ServiceResult<PkVoteInfo>>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                SingleToastUtil.showToast(mContext, "投票失败！", Toast.LENGTH_SHORT);
            }

            @Override
            public void onResponse(ServiceResult<PkVoteInfo> response) {
                if (response != null && response.getCode() == 200) {
                    IMNetEaseManager.get().sendPkNotificationBySdk(
                            CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK,
                            CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_VOTE, new PkVoteInfo());
                    tvPkVote.setText("已投票");
                    tvPkVote.setEnabled(false);
                    pkUserAdapter.setCurSelPos(-1);
                    pkUserAdapter.setCurSelUid(0L);
                    pkUserAdapter.notifyDataSetChanged();
                    AvRoomDataManager.get().setHasPkVoted(true);
                } else {
                    Toast.makeText(mContext, response.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * 礼物赠送投票
     */
    private void showGiftDialog(PkUser pkUser) {
        GiftDialog giftDialog = new GiftDialog(getContext(), Long.valueOf(pkUser.getUid()), false/*,
                pkUser.getNick(),
                pkUser.getAvatar(),
                pkUser.getVipId(),
                pkUser.getVipDate(),
                pkUser.isInvisible()*/);
        giftDialog.setRoomId(null == AvRoomDataManager.get().mCurrentRoomInfo ? 0L :
                AvRoomDataManager.get().mCurrentRoomInfo.getRoomId());
        giftDialog.setGiftDialogBtnClickListener(new GiftDialog.OnGiftDialogBtnClickListener() {
            @Override
            public void onRechargeBtnClick() {

            }

            @Override
            public void onSendGiftBtnClick(GiftInfo giftInfo, long uid, int number) {
                RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                if (currentRoomInfo == null) {
                    return;
                }
                CoreManager.getCore(IGiftCore.class).sendRoomGift(giftInfo.getGiftId(), uid,
                        currentRoomInfo.getUid(), number, giftInfo.getGoldPrice());
            }

            @Override
            public void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, int number) {

            }
        });
        giftDialog.show();
    }

    /**
     * 礼物赠送成功回调 -- 送给单人
     *
     * @param target
     */
    @CoreEvent(coreClientClass = IPKCoreClient.class)
    public void onPkGift(long target) {
        if (pkVoteInfo != null && !isShowing) {//如果正在执行结束动画将不执行礼物消息发送
            if (pkVoteInfo.getUid() == target || pkVoteInfo.getPkUid() == target) {
                IMNetEaseManager.get().sendPkNotificationBySdk(
                        CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK,
                        CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_VOTE, new PkVoteInfo());
            }
        }
    }

    /**
     * 礼物赠送成功回调 -- 送给全麦
     *
     * @param targetUids
     */
    @CoreEvent(coreClientClass = IPKCoreClient.class)
    public void onPkMultiGift(List<Long> targetUids) {
        if (pkVoteInfo != null && !isShowing) {
            IMNetEaseManager.get().sendPkNotificationBySdk(CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK,
                    CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_VOTE, new PkVoteInfo());
        }
    }

    @CoreEvent(coreClientClass = IPKCoreClient.class)
    public void onPkGiftFail(String error) {
        ToastCompat.makeText(mContext, error, ToastCompat.LENGTH_SHORT).show();
    }

    /**
     * 控制不同消息显示
     *
     * @param chatRoomMessage
     */
    private void dealWithEvent(ChatRoomMessage chatRoomMessage) {
        if (chatRoomMessage.getAttachment() instanceof PkCustomAttachment) {
            PkCustomAttachment pk = (PkCustomAttachment) chatRoomMessage.getAttachment();
            if (pk == null) {
                return;
            }
            if (pk.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_START || pk.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_START_NEW) {
                //操作者是自己
                if (pk.getPkVoteInfo() != null && AvRoomDataManager.get().isOwner(pk.getPkVoteInfo().getOpUid())) {
                    isFull = true;
                }
                setPkInfo(pk.getPkVoteInfo());
                initData(true);
            } else if (pk.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_VOTE) {
//                initData(true);
            } else if (pk.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_VOTE_NEW) {
                setPkInfo(pk.getPkVoteInfo());
            } else if (pk.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_END || pk.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_END_NEW) {
                //屏蔽因为延迟导致的重复执行显示隐藏问题
                if (!isShowing && pkVoteInfo != null) {
                    dealWithPKEnd(pk.getPkVoteInfo());
                    AvRoomDataManager.get().setHasPkVoted(false);
                }
            } else if (pk.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_CANCEL || pk.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_CANCEL_NEW) {
                resetState();
                AvRoomDataManager.get().setHasPkVoted(false);
            }
        }
    }

    /**
     * 处理PK结果
     *
     * @param info
     */
    private void dealWithPKEnd(PkVoteInfo info) {
        if (info != null) {
            handlerRelease();
            isFull = true;
            setPkInfo(info);
            tvPkVote.setVisibility(GONE);
            showPKWin();
        } else {
            if (duration == 0) {//数据异常倒计时结束隐藏
                resetState();
            }
        }
    }

    /**
     * 显示大窗口并且执行消失动画
     */
    private void showPKWin() {
        isShowing = true;
        rlFull.setVisibility(VISIBLE);
        disableClickOther(false);
        rlMinimize.setVisibility(GONE);
        tvPkTimer.setVisibility(GONE);
        tvPkVote.setVisibility(GONE);
        tvPkName.setText("投票结果");
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ScaleAnimation disappear = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, rlFull.getWidth() / 2, rlFull.getHeight() / 2);
                disappear.setDuration(500);
                disappear.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        resetState();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
                rlFull.startAnimation(disappear);
//                resetState();
                handler.removeCallbacks(this);
            }
        }, 5000);
    }

    /***
     * 重置view的初始状态
     */
    private void resetState() {
        //释放定时器
        handlerRelease();
        //隐藏布局
        rlFull.setVisibility(View.GONE);
        rlMinimize.setVisibility(View.GONE);
        disableClickOther(true);
        //重置默认状态
        pkVoteInfo = null;
        isFull = false;
        duration = 0;
        countDuration = 0;
        isShowing = false;
        tvPkVote.setVisibility(VISIBLE);
        tvPkTimer.setVisibility(VISIBLE);
        tvPkVote.setEnabled(true);
        tvPkVote.setText("投票");
        pkUserAdapter.setCurSelPos(-1);
        pkUserAdapter.setCurSelUid(0L);
    }

    /**
     * seekbar进度显示
     *
     * @param voteCount
     * @param pkVote
     * @return
     */
    private int getProgress(int voteCount, int pkVote) {
        if (voteCount == pkVote) {
            return 50;
        }
        return voteCount * 100 / (voteCount + pkVote == 0 ? 1 : voteCount + pkVote);
    }

    public void setPkInfo(PkVoteInfo info) {
        if (info == null) {
            return;
        }
        pkVoteInfo = info;
        datas.clear();
        //针对旧版本，2人PK的显示逻辑，getVoteCount左边票数，getPkVoteCount右边票数
        if ((info.getPkList() == null || info.getPkList().size() == 0) && !TextUtils.isEmpty(info.getPkNick())) {
            PkUser pkUser = new PkUser();
            pkUser.setUid(info.getUid() + "");
            pkUser.setAvatar(info.getAvatar());
            pkUser.setNick(info.getNick());
            pkUser.setVoteCount(info.getVoteCount());

            PkUser pkUser1 = new PkUser();
            pkUser1.setUid(info.getPkUid() + "");
            pkUser1.setAvatar(info.getPkAvatar());
            pkUser1.setNick(info.getPkNick());
            pkUser1.setVoteCount(info.getPkVoteCount());

            //获取麦位位置
            pkUser.setMicPosition(AvRoomDataManager.get().getMicPosition(pkUser.getUid()));
            pkUser1.setMicPosition(AvRoomDataManager.get().getMicPosition(pkUser1.getUid()));
            datas.add(pkUser);
            datas.add(pkUser1);
        } else {
            for (PkUser pkUser : info.getPkList()) {
                //获取麦位位置
                pkUser.setMicPosition(AvRoomDataManager.get().getMicPosition(pkUser.getUid()));
                datas.add(pkUser);
            }
        }

        //根据麦位麦序-->票数进行排序
        SortList<PkUser> sortList = new SortList<PkUser>();
        //正序排序--先按麦位顺序排序
        sortList.Sort(datas, "getMicPosition", "asc");
        //倒序排序--再按[2礼物价值/1投票人数]票数排序
        sortList.Sort(datas, "getVoteCount", "desc");

        tvPkName.setText(info.getTitle());
        //最小化时控件
        ImageLoadUtils.loadCircleImage(mContext, datas.get(0).getAvatar(), imvPkUser1, R.drawable.default_user_head);
        ImageLoadUtils.loadCircleImage(mContext, datas.get(1).getAvatar(), imvPkUser2, R.drawable.default_user_head);

        int index = 0;
        String pkCountStr = datas.size() + "";
        String pkCount = pkCountStr + "人正在PK";
        if (datas.size() > 2) {
            index = 1;
            pkCount = "等" + pkCountStr + "人正在PK";
        }
        SpannableStringBuilder ssb = new SpannableStringBuilder(pkCount);
        ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#10CFA6")), index, index + pkCountStr.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        tvPkCountTip.setText(ssb);

        pkUserAdapter.setPkType(pkVoteInfo.getPkType());
        pkUserAdapter.setNewData(datas);
        //1--投票人数，2--礼物价值
        tvPkType.setText(mContext.getResources().getString(pkVoteInfo.getPkType() == 2
                ? R.string.room_pk_type_tips_gift_price : R.string.room_pk_type_tips_person_num));
        if (isFull) {
            rlFull.setVisibility(View.VISIBLE);
            rlMinimize.setVisibility(View.GONE);
            disableClickOther(false);
        } else {
            rlFull.setVisibility(View.GONE);
            rlMinimize.setVisibility(View.VISIBLE);
            disableClickOther(true);
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        CoreManager.addClient(this);
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        compositeDisposable.add(IMNetEaseManager.get().getChatRoomMsgFlowable()
                .subscribe(messages -> {
                    if (messages.size() == 0) {
                        return;
                    }
                    for (ChatRoomMessage msg : messages) {
                        if (IMNetEaseManager.get().filterOtherRoomMsg(msg)) {
                            LogUtils.d(HomePartyPKView.class.getSimpleName(),
                                    "onAttachedToWindow - 其他房间信息_被拦截，roomId = " + msg.getSessionId());
                            continue;
                        }
                        if (msg.getAttachment() instanceof CustomAttachment) {
                            dealWithEvent(msg);
                        }
                    }
                }));
        compositeDisposable.add(IMNetEaseManager.get().getChatRoomEventObservable()
                .subscribe(roomEvent -> {
                    if (roomEvent == null ||
                            roomEvent.getEvent() != RoomEvent.RECEIVE_MSG) {
                        return;
                    }
                    ChatRoomMessage chatRoomMessage = roomEvent.getChatRoomMessage();
                    if (chatRoomMessage.getAttachment() instanceof CustomAttachment) {
                        dealWithEvent(chatRoomMessage);
                    }
                }));
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        CoreManager.removeClient(this);
        handlerRelease();
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
            compositeDisposable = null;
        }
    }

    public PkVoteInfo getPkVoteInfo() {
        return pkVoteInfo;
    }

    /**
     * 控制view的位置
     */
    private int mWidthPixels;
    private int mHeightPixels;
    long mDownTimeMillis = 0;
    int xDelta = 0;
    int yDelta;

    @Override
    public boolean performClick() {
        return super.performClick();
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        final int x = (int) event.getRawX();
        final int y = (int) event.getRawY();
//        LogUtils.d(TAG, "onTouch: x= " + x + "y=" + y);
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                mDownTimeMillis = System.currentTimeMillis();
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view
                        .getLayoutParams();
                xDelta = x - params.leftMargin;
                yDelta = y - params.topMargin;
                performClick();
//                LogUtils.d(TAG, "ACTION_DOWN: xDelta= " + xDelta + "yDelta=" + yDelta);
                break;
            case MotionEvent.ACTION_MOVE:
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view
                        .getLayoutParams();
                int width = layoutParams.width;
                int height = layoutParams.height;
                int xDistance = x - xDelta;
                int yDistance = y - yDelta;

                int outX = (mWidthPixels - width) - 10;
                if (xDistance > outX) {
                    xDistance = outX;
                }

                int outY = mHeightPixels - height;
                if (yDistance > outY) {
                    yDistance = outY;
                }

                if (yDistance < 100) {
                    yDistance = 100;
                }
                if (xDistance < 10) {
                    xDistance = 10;
                }

                layoutParams.leftMargin = xDistance;
                layoutParams.topMargin = yDistance;
                view.setLayoutParams(layoutParams);
                break;
            case MotionEvent.ACTION_UP:
                if (System.currentTimeMillis() - mDownTimeMillis < 150) {
                    if (pkVoteInfo != null) {
                        isFull = true;
                        if (rlMinimize.getVisibility() == View.VISIBLE) {
                            rlMinimize.setVisibility(View.GONE);
                        }
                        if (rlFull.getVisibility() == View.GONE) {
                            rlFull.setVisibility(View.VISIBLE);
                        }
                        disableClickOther(false);
                    } else {
                        resetState();
                    }
                }
                break;
            default:
                break;
        }
        return true;
    }
}
