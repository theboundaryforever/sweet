package com.yuhuankj.tmxq.ui.chancemeeting;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;

import java.util.Map;

public class EditVoicePresenter extends AbstractMvpPresenter<EditVoiceView> {

    private final String TAG = EditVoicePresenter.class.getSimpleName();

    public EditVoicePresenter() {
    }

    public void updateUserVoiceBg(String photoStr) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        //第一页
        params.put("photoStr", photoStr);
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().postRequest(UriProvider.getUploadVoiceBgUrl(), params,
                new OkHttpManager.MyCallBack<ServiceResult<String>>() {
                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                        if (null != getMvpView()) {
                            getMvpView().onVoiceBgUploaded(false, e.getMessage());
                        }
                    }

                    @Override
                    public void onResponse(ServiceResult<String> serviceResult) {
                        LogUtils.d(TAG, "getRoomOnLineUserList-onResponse-result:" + serviceResult);
                        if (null != getMvpView()) {
                            if (null != serviceResult) {
                                getMvpView().onVoiceBgUploaded(serviceResult.isSuccess(), serviceResult.getMessage());
                            } else {
                                getMvpView().onVoiceBgUploaded(false, null);
                            }
                        }
                    }
                });
    }

    public void exitRoom() {
        BaseRoomServiceScheduler.exitRoom(null);
    }

}
