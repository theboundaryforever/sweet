package com.yuhuankj.tmxq.ui.liveroom.imroom.room.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.ConveneUserInfo;

import java.util.List;

public interface IConeveUserListView extends IMvpBaseView {

    void refreshConeveUserList(List<ConveneUserInfo> conveneUserInfos);

    void showErrMsg(String msg);

    void finishAfterCancelCallUp();
}
