package com.yuhuankj.tmxq.ui.me.taskcenter.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.yuhuankj.tmxq.ui.me.taskcenter.model.TaskCenterEnitity;
import com.yuhuankj.tmxq.ui.signAward.model.SignAwardResult;

/**
 * @author weihaitao
 * @date 2019/5/21
 */
public interface ITaskCenterView extends IMvpBaseView {

    /**
     * 提示签到失败
     *
     * @param message
     */
    void showSignErr(String message);

    /**
     * 刷新领取状态为已领取
     */
    void refreshSignInAwardGotStatus(SignAwardResult result);

    /**
     * 刷新任务中心列表数据
     *
     * @param taskCenterEnitity
     */
    void refreshTaskList(TaskCenterEnitity taskCenterEnitity);

    /**
     * 显示萌新大礼包领取结果弹框
     */
    void showResultAfterSignSuc(SignAwardResult result);
}
