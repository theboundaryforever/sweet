package com.yuhuankj.tmxq.ui.me;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

/**
 * Created by MadisonRong on 04/01/2018.
 */

public interface IMeView extends IMvpBaseView {

    public void updateUserInfoUI(UserInfo userInfo);

    public void setVisitorCount(int count);
}
