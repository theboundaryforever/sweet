package com.yuhuankj.tmxq.ui.me.wallet;

import com.yuhuankj.tmxq.ui.me.charge.interfaces.IPayView;

/**
 * Created by MadisonRong on 08/01/2018.
 */

public interface IMyWalletView extends IPayView {

    public void handleClickByViewId(int id);
}
