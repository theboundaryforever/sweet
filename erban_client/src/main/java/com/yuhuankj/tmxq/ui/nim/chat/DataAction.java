package com.yuhuankj.tmxq.ui.nim.chat;

import com.netease.nim.uikit.session.actions.BaseAction;
import com.netease.nim.uikit.session.activity.P2PMessageActivity;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.LogUtil;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.agora.AgoraEngineManager;
import com.tongdaxing.xchat_core.result.RoomInfoResult;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.service.DaemonService;
import com.yuhuankj.tmxq.ui.MainActivity;
import com.yuhuankj.tmxq.ui.home.game.GameHomeModel;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;

import org.json.JSONException;

import java.util.Map;

import static com.tongdaxing.xchat_core.room.bean.RoomInfo.ROOMTYPE_HOME_PARTY;

/**
 * @author liaoxy
 * @Description:传入私聊页面的数据action，只传递逻辑不显示
 * @date 2019/1/10 19:40
 */
public class DataAction extends BaseAction {

    public DataAction() {
        super("", "");
    }

    @Override
    public void onClick() {
        if (jsonOut == null) {
            LogUtil.e("DataAction-->jsonOut is null");
            return;
        }
        try {
            int action = jsonOut.getInt("action");
            if (action == P2PMessageActivity.GET_RUNNING_INFO) {
                String uid = jsonOut.getString("uid");
                getUserRoom(uid);
            } else if (action == P2PMessageActivity.ENTER_RUNNING_ROOM) {
                long roomId = jsonOut.getLong("roomId");
                int roomType = ROOMTYPE_HOME_PARTY;
                if (jsonOut.has("roomType")) {
                    roomType = jsonOut.num("roomType");
                }
                if (null != getActivity()) {
                    RoomServiceScheduler.getInstance().enterRoom(getActivity(), roomId, roomType);
                }
            } else if (action == P2PMessageActivity.INVITATIOLN_TIMER) {
                boolean isContinue = jsonOut.getBoolean("isContinue");
                if (isContinue) {
                    MsgViewHolderMiniGameInvited.continueTimer();
                } else {
                    MsgViewHolderMiniGameInvited.pauseTimer();
                }
            } else if (action == P2PMessageActivity.ATTATION) {
                String likeUid = jsonOut.getString("likeUid");
                String attType = jsonOut.getString("attType");
                attention(likeUid, attType);
            } else if (action == P2PMessageActivity.IS_ATTATION) {
                String likeUid = jsonOut.getString("likeUid");
                isAttention(likeUid);
            } else if (action == P2PMessageActivity.INVITATIOLN_LINK_MACRO) {
                String linkUid = jsonOut.getString("linkUid");
                invitationLinkMacro(linkUid);
            } else if (action == P2PMessageActivity.CANCEL_LINK_MACRO) {
                String linkUid = jsonOut.getString("linkUid");
                cancelLinkMacro(linkUid);
            } else if (action == P2PMessageActivity.FINISH_LINK_MACRO) {
                String linkUid = jsonOut.getString("linkUid");
                String talkTime = jsonOut.getString("talkTime");
                finishLinkMacro(linkUid, talkTime);
            } else if (action == P2PMessageActivity.BACK_ACTION) {
                showFinishLinkMacroDlg();
            } else if (action == P2PMessageActivity.CLOSE_LINK_MACRO) {
                boolean isOpen = jsonOut.getBoolean("isOpen");
                closeOrOpenMacro(isOpen);
            } else if (action == P2PMessageActivity.LINK_MICRO_TIME_OUT) {
                String linkUid = jsonOut.getString("linkUid");
                int type = jsonOut.getInt("type");
                linkMacroTimeOut(linkUid, type);
            } else if (action == P2PMessageActivity.LINK_MICRO_EXIT) {
                MainActivity.exitLinkMacro();
            } else if (action == P2PMessageActivity.SHOW_LINK_MICRO_NOFITY) {
                if (null != getActivity()) {
                    DaemonService.start(getActivity(), "语音通话", "点击返回通话房间", "正在语音通话");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getUserRoom(String uid) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", uid);
        OkHttpManager.getInstance().getRequest(UriProvider.getUserRoom(), params, new OkHttpManager.MyCallBack<RoomInfoResult>() {
            @Override
            public void onError(Exception e) {
                LogUtil.e("DataAction-->onError " + e.getMessage());
            }

            @Override
            public void onResponse(RoomInfoResult response) {
                if (response.isSuccess()) {
                    try {
                        RoomInfo roomInfo = response.getData();
                        if (roomInfo == null) {
                            if (actionCallback != null) {
                                actionCallback.onActionSucceed(3, null);
                            }
                            return;
                        }
                        String roomInfoStr = String.format("聊天室(ID：%s) : %s", roomInfo.getRoomNo(), roomInfo.title);
                        try {
                            if (roomInfoStr.length() > 30) {
                                roomInfoStr = roomInfoStr.substring(0, 30) + "...";
                            }
                            Json json = new Json();
                            json.put("runningInfo", roomInfoStr);
                            json.put("roomId", roomInfo.getUid());
                            json.put("roomType", roomInfo.getType());
                            actionCallback.onActionSucceed(P2PMessageActivity.GET_RUNNING_INFO, json);
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                            if (actionCallback != null) {
                                actionCallback.onActionFailed(P2PMessageActivity.GET_RUNNING_INFO, null);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    /**
     * 关注或取消关注
     *
     * @param likeUid
     * @param type    1:关注 2:取消关注
     */
    private void attention(String likeUid, String type) {
        if (null == getActivity()) {
            return;
        }
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("type", type);
        params.put("likedUid", likeUid);
        DialogManager dialogManager = new DialogManager(getActivity());
        dialogManager.showProgressDialog(getActivity(), "请稍后...");
        OkHttpManager.getInstance().postRequest(UriProvider.praise(), params, new OkHttpManager.MyCallBack<ServiceResult<Object>>() {
            @Override
            public void onError(Exception e) {
                dialogManager.dismissDialog();
                if (actionCallback != null) {
                    actionCallback.onActionFailed(P2PMessageActivity.ATTATION, null);
                }
                SingleToastUtil.showToast(e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<Object> response) {
                dialogManager.dismissDialog();
                if (response.isSuccess()) {
                    if (actionCallback != null) {
                        actionCallback.onActionSucceed(P2PMessageActivity.ATTATION, null);
                    }
                } else {
                    SingleToastUtil.showToast(response.getErrorMessage());
                }

            }
        });
    }

    public void isAttention(final String isLikeUid) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("isLikeUid", isLikeUid);
        //DialogManager dialogManager = new DialogManager(getActivity());
        //dialogManager.showProgressDialog(getActivity(), "请稍后...");
        OkHttpManager.getInstance().getRequest(UriProvider.isLike(), params, new OkHttpManager.MyCallBack<ServiceResult<Object>>() {
            @Override
            public void onError(Exception e) {
                //dialogManager.dismissDialog();
                //SingleToastUtil.showToast(e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<Object> response) {
                //dialogManager.dismissDialog();
                if (actionCallback != null) {
                    try {
                        Json json = new Json();
                        json.put("isLike", response.getData());
                        actionCallback.onActionSucceed(P2PMessageActivity.IS_ATTATION, json);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void showFinishLinkMacroDlg() {
        if (null == getActivity()) {
            return;
        }
        DialogManager dialogManager = new DialogManager(getActivity());
        dialogManager.showOkCancelDialog("此时离开，会中断与对方的通话喔?", "同意", "取消", true, new DialogManager.AbsOkDialogListener() {
            @Override
            public void onOk() {
                AgoraEngineManager.get().exitLinkMacro();
                Json json = new Json();
                actionCallback.onActionSucceed(P2PMessageActivity.BACK_ACTION, json);
            }
        });
    }

    //邀请对方连麦
    private void invitationLinkMacro(String linkUid) {
        if (null == getActivity()) {
            return;
        }
        DialogManager dialogManager = new DialogManager(getActivity());
        dialogManager.showProgressDialog(getActivity(), "请稍后...");
        new GameHomeModel().invitationLinkMacro(linkUid, new OkHttpManager.MyCallBack<ServiceResult<Object>>() {
            @Override
            public void onError(Exception e) {
                dialogManager.dismissDialog();
                if (actionCallback != null) {
                    actionCallback.onActionFailed(P2PMessageActivity.INVITATIOLN_LINK_MACRO, null);
                }
                SingleToastUtil.showToast(e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<Object> response) {
                dialogManager.dismissDialog();
                if (response.isSuccess()) {
                    if (actionCallback != null) {
                        actionCallback.onActionSucceed(P2PMessageActivity.INVITATIOLN_LINK_MACRO, null);
                    }
                } else {
                    SingleToastUtil.showToast(response.getErrorMessage());
                }

            }
        });
    }

    //取消连麦
    private void cancelLinkMacro(String linkUid) {
        if (null == getActivity()) {
            return;
        }
        DialogManager dialogManager = new DialogManager(getActivity());
        dialogManager.showProgressDialog(getActivity(), "请稍后...");
        new GameHomeModel().cancelLinkMacro(linkUid, new OkHttpManager.MyCallBack<ServiceResult<Object>>() {
            @Override
            public void onError(Exception e) {
                dialogManager.dismissDialog();
                if (actionCallback != null) {
                    actionCallback.onActionFailed(P2PMessageActivity.CANCEL_LINK_MACRO, null);
                }
                SingleToastUtil.showToast(e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<Object> response) {
                dialogManager.dismissDialog();
                if (response.isSuccess()) {
                    if (actionCallback != null) {
                        actionCallback.onActionSucceed(P2PMessageActivity.CANCEL_LINK_MACRO, null);
                    }
                } else {
                    SingleToastUtil.showToast(response.getErrorMessage());
                }

            }
        });
    }

    //结束连麦
    private void finishLinkMacro(String linkUid, String talkTime) {
        if (null == getActivity()) {
            return;
        }
        DialogManager dialogManager = new DialogManager(getActivity());
        dialogManager.showProgressDialog(getActivity(), "请稍后...");
        new GameHomeModel().finishLinkMacro(linkUid, talkTime, new OkHttpManager.MyCallBack<ServiceResult<Object>>() {
            @Override
            public void onError(Exception e) {
                dialogManager.dismissDialog();
                if (actionCallback != null) {
                    actionCallback.onActionFailed(P2PMessageActivity.FINISH_LINK_MACRO, null);
                }
                SingleToastUtil.showToast(e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<Object> response) {
                dialogManager.dismissDialog();
                if (response.isSuccess()) {
                    if (actionCallback != null) {
                        actionCallback.onActionSucceed(P2PMessageActivity.FINISH_LINK_MACRO, null);
                    }
                } else {
                    SingleToastUtil.showToast(response.getErrorMessage());
                }

            }
        });
    }

    //连麦超时
    private void linkMacroTimeOut(String linkUid, int type) {
        new GameHomeModel().linkMacroException(linkUid, type, new OkHttpManager.MyCallBack<ServiceResult<Object>>() {
            @Override
            public void onError(Exception e) {
                if (actionCallback != null) {
                    actionCallback.onActionFailed(P2PMessageActivity.LINK_MICRO_TIME_OUT, null);
                }
            }

            @Override
            public void onResponse(ServiceResult<Object> response) {
                if (response.isSuccess()) {
                    if (actionCallback != null) {
                        actionCallback.onActionSucceed(P2PMessageActivity.LINK_MICRO_TIME_OUT, null);
                    }
                }
            }
        });
    }

    //闭麦或开麦
    private void closeOrOpenMacro(boolean isOpen) {
        if (AgoraEngineManager.get().closeOrOpenMacro(isOpen) == 0) {
            if (actionCallback != null) {
                actionCallback.onActionSucceed(P2PMessageActivity.CLOSE_LINK_MACRO, null);
            }
        } else {
            if (actionCallback != null) {
                actionCallback.onActionFailed(P2PMessageActivity.CLOSE_LINK_MACRO, null);
            }
        }
    }
}
