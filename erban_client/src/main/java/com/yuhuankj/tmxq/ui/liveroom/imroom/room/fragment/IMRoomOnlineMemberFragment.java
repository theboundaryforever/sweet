package com.yuhuankj.tmxq.ui.liveroom.imroom.room.fragment;

import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.netease.nim.uikit.common.util.sys.NetworkUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomEvent;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMember;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMessage;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomOnlineMember;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.fragment.BaseMvpFragment;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter.RoomOnlineMemberAdapter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter.IMRoomOnLineMemberPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IMRoomOnLineMemberView;
import com.yuhuankj.tmxq.ui.widget.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

import static com.yuhuankj.tmxq.ui.liveroom.imroom.room.dialog.IMRoomMemberListDialog.KEY_ISMIC;
import static com.yuhuankj.tmxq.ui.liveroom.imroom.room.dialog.IMRoomMemberListDialog.KEY_IS_ADMIN;
import static com.yuhuankj.tmxq.ui.liveroom.imroom.room.dialog.IMRoomMemberListDialog.KEY_IS_INVITE_UP_TO_AUDIO_CONN_MIC;

/**
 * <p>  在线用户列表 </p>
 *
 * @author Administrator
 * @date 2017/12/4
 */
@CreatePresenter(IMRoomOnLineMemberPresenter.class)
public class IMRoomOnlineMemberFragment extends BaseMvpFragment<IMRoomOnLineMemberView, IMRoomOnLineMemberPresenter>
        implements BaseQuickAdapter.OnItemClickListener, IMRoomOnLineMemberView {

    private final String TAG = IMRoomOnlineMemberFragment.class.getSimpleName();
    //是否过滤麦上用户
    public boolean isMic = false;
    public boolean isAdmin = false;
    private boolean isInviteUpToAudioConnMic = false;
    private RecyclerView mRecyclerView;
    private SmartRefreshLayout mRefreshLayout;
    private RoomOnlineMemberAdapter roomOnlineMemberAdapter;
    private int mPage = Constants.PAGE_START;
    private OnlineMemberItemClick onlineMemberItemClick;

    @Override
    public void onFindViews() {
        mRecyclerView = mView.findViewById(R.id.recycler_view);
        mRefreshLayout = mView.findViewById(R.id.refresh_layout);
        if (getArguments() != null) {
            isMic = getArguments().getBoolean(KEY_ISMIC, false);
            isAdmin = getArguments().getBoolean(KEY_IS_ADMIN,false);
            isInviteUpToAudioConnMic = getArguments().getBoolean(KEY_IS_INVITE_UP_TO_AUDIO_CONN_MIC, false);
        }
        mRefreshLayout.setEnableHeaderTranslationContent(false);
        mRecyclerView.setLayoutManager(new RecyclerViewNoBugLinearLayoutManager(mContext));
        roomOnlineMemberAdapter = new RoomOnlineMemberAdapter();
        mRecyclerView.setAdapter(roomOnlineMemberAdapter);
        mRecyclerView.addItemDecoration(
                new DividerItemDecoration(getContext(), OrientationHelper.VERTICAL,
                        2, R.color.app_bg));
        roomOnlineMemberAdapter.setOnItemClickListener(this);
        registerRoomEvent();
    }

    @Override
    public void onSetListener() {
        mRefreshLayout.setOnRefreshLoadmoreListener(new OnRefreshLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshLayout) {
                if (!NetworkUtil.isNetAvailable(mContext)) {
                    if (mRefreshLayout != null) {
                        mRefreshLayout.finishLoadmore();
                    }
                    return;
                }
                List<IMRoomOnlineMember> data = fitUser(roomOnlineMemberAdapter != null ? roomOnlineMemberAdapter.getData() : null);
                data = fitUserNoOnMic(data);
                if (ListUtils.isListEmpty(data)) {
                    if (mRefreshLayout != null) {
                        mRefreshLayout.finishLoadmore();
                    }
                    return;
                }
                loadData(data.size(), mPage + 1);
            }

            @Override
            public void onRefresh(RefreshLayout refreshLayout) {
                if (!NetworkUtil.isNetAvailable(mContext)) {
                    if (mRefreshLayout != null) {
                        mRefreshLayout.finishRefresh();
                    }
                    return;
                }
                firstLoad();
            }
        });
    }

    private List<IMRoomOnlineMember> fitUser(List<IMRoomOnlineMember> user) {
        if (!isMic) {
            return user;
        }
        if (ListUtils.isListEmpty(user)) {
            return user;
        }
        List<IMRoomOnlineMember> data = new ArrayList<>();
        for (int i = 0; i < user.size(); i++) {
            if (user.get(i).isOnMic) {
                data.add(user.get(i));
            }
        }
        return data;
    }

    private List<IMRoomOnlineMember> fitUserNoOnMic(List<IMRoomOnlineMember> user) {
        if (!isInviteUpToAudioConnMic) {
            return user;
        }
        if (ListUtils.isListEmpty(user)) {
            return user;
        }
        List<IMRoomOnlineMember> data = new ArrayList<>();
        for (int i = 0; i < user.size(); i++) {
            if (isAdmin){
                if (user.get(i).isAdmin && !user.get(i).isOnMic) {
                    data.add(user.get(i));
                }
            }else {
                if (!user.get(i).isOnMic) {
                    data.add(user.get(i));
                }
            }
        }
        return data;
    }

    @Override
    public void initiate() {

    }

    private void registerRoomEvent() {
        //在线列表，接收房间成员操作相关操作事件监听，及时刷新列表
        Consumer consumer = new Consumer<IMRoomEvent>() {
            @Override
            public void accept(IMRoomEvent roomEvent) throws Exception {
                if (roomEvent == null) {
                    return;
                }
                int event = roomEvent.getEvent();
                if (roomEvent.getEvent() == IMRoomEvent.ADD_BLACK_LIST ||
                        roomEvent.getEvent() == IMRoomEvent.DOWN_MIC ||
                        event == IMRoomEvent.ROOM_MEMBER_EXIT ||
                        roomEvent.getEvent() == IMRoomEvent.KICK_OUT_ROOM) {
                    if (roomEvent.getEvent() == IMRoomEvent.ADD_BLACK_LIST ||
                            roomEvent.getEvent() == IMRoomEvent.KICK_OUT_ROOM) {
                        if (!RoomDataManager.get().isRoomOwner()) {
                            addRoomMemberBlack();
                            return;
                        }
                    }
                    if (null == roomOnlineMemberAdapter || ListUtils.isListEmpty(roomOnlineMemberAdapter.getData())) {
                        return;
                    }
                    if (roomEvent.getEvent() == IMRoomEvent.DOWN_MIC) {
                        onRoomMemberDownUpMic(roomEvent.getAccount(), false, roomOnlineMemberAdapter.getData());
                        return;
                    }
                    ListIterator<IMRoomOnlineMember> iterator = roomOnlineMemberAdapter.getData().listIterator();
                    for (; iterator.hasNext(); ) {
                        IMRoomOnlineMember onlineChatMember = iterator.next();
                        if (onlineChatMember.imRoomMember != null && Objects.equals(
                                onlineChatMember.imRoomMember.getAccount(), roomEvent.getAccount())) {
                            iterator.remove();
                        }
                    }
                    roomOnlineMemberAdapter.notifyDataSetChanged();
                } else if (roomEvent.getEvent() == IMRoomEvent.ROOM_MANAGER_ADD
                        || roomEvent.getEvent() == IMRoomEvent.ROOM_MANAGER_REMOVE) {
                    if (roomOnlineMemberAdapter == null)
                        return;
                    onUpdateRoomMemberManager(roomEvent.getAccount(),
                            roomEvent.getEvent() == IMRoomEvent.ROOM_MANAGER_REMOVE,
                            roomOnlineMemberAdapter.getData());
                } else if (roomEvent.getEvent() == IMRoomEvent.UP_MIC) {
                    if (roomOnlineMemberAdapter == null) {
                        return;
                    }
                    onRoomMemberDownUpMic(roomEvent.getAccount(), true,
                            roomOnlineMemberAdapter.getData());
                } else if (event == IMRoomEvent.ROOM_MEMBER_IN) {
                    if (roomOnlineMemberAdapter == null) {
                        return;
                    }
                    onRoomMemberComingIn(roomEvent.getAccount(), roomOnlineMemberAdapter.getData(),
                            roomEvent.getIMRoomMessage());
                }
            }
        };
        Disposable mDisposable = IMRoomMessageManager.get().getIMRoomEventObservable().subscribe(consumer);
        if (null != mCompositeDisposable) {
            mCompositeDisposable.add(mDisposable);
        }
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.common_refresh_recycler_view;
    }

    @Override
    public void loadLazyIfYouWant() {
        super.loadLazyIfYouWant();
        LogUtils.d(TAG, "loadLazyIfYouWant");
        firstLoad();
    }

    public void firstLoad() {
        mPage = Constants.PAGE_START;
        loadData(0, Constants.PAGE_START);
    }

    private void loadData(int index, int page) {
        if (null != RoomDataManager.get().getCurrentRoomInfo()) {
            List<IMRoomOnlineMember> datas = roomOnlineMemberAdapter == null ? null : fitUser(roomOnlineMemberAdapter.getData());
            datas = fitUserNoOnMic(datas);
            getMvpPresenter().requestChatMemberByIndex(mPage, index, datas);
        }
    }

    @Override
    public void onRequestChatMemberByPageSuccess(List<IMRoomOnlineMember> chatRoomMemberList, int page) {
        dealOnLineUserListRespone(chatRoomMemberList, page);
    }

    private void dealOnLineUserListRespone(List<IMRoomOnlineMember> imRoomOnlineMembers, int page) {
        mPage = page;
        if (!ListUtils.isListEmpty(imRoomOnlineMembers)) {
            if (roomOnlineMemberAdapter != null) {
                List<IMRoomOnlineMember> datas = fitUser(imRoomOnlineMembers);
                datas = fitUserNoOnMic(datas);
                roomOnlineMemberAdapter.setNewData(datas);
            }
            if (mPage == Constants.PAGE_START) {
                if (mRefreshLayout != null) {
                    mRefreshLayout.finishRefresh();
                }
            } else {
                if (mRefreshLayout != null) {
                    mRefreshLayout.finishLoadmore(0);
                }
            }
            mPage++;
        } else {
            if (mPage == Constants.PAGE_START) {
                if (mRefreshLayout != null) {
                    mRefreshLayout.finishRefresh();
                }
            } else {
                if (mRefreshLayout != null) {
                    mRefreshLayout.finishLoadmore(0);
                }
            }
        }
    }

    @Override
    public void onRequestChatMemberByPageFail(String errorStr, int page) {
        LogUtils.d(TAG, "onRequestChatMemberByPageFail 获取到数据失败,page=" + page);
        dealGetOnLineUlserListError(page);
    }

    private void dealGetOnLineUlserListError(int page) {
        mPage = page;
        if (mPage == Constants.PAGE_START) {
            if (mRefreshLayout != null) {
                mRefreshLayout.finishRefresh();
            }
        } else {
            if (mRefreshLayout != null) {
                mRefreshLayout.finishLoadmore(0);
            }
        }
    }

    @Override
    public void onGetOnLineUserList(boolean isSuccess, String message, int page, List<IMRoomOnlineMember> onlineChatMembers) {
        LogUtils.d(TAG, "onGetOnLineUserList-isSuccess:" + isSuccess + " message:" + message);
        if (isSuccess) {
            dealOnLineUserListRespone(onlineChatMembers, page);
        } else {
            dealGetOnLineUlserListError(page);
            if (!TextUtils.isEmpty(message)) {
                toast(message);
            }
        }
    }

    public void setOnlineMemberItemClick(OnlineMemberItemClick onlineMemberItemClick) {
        this.onlineMemberItemClick = onlineMemberItemClick;
    }

    @Override
    public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int position) {
        RoomInfo currentRoom = RoomDataManager.get().getCurrentRoomInfo();
        if (currentRoom != null && roomOnlineMemberAdapter != null) {
            List<IMRoomOnlineMember> chatRoomMembers = roomOnlineMemberAdapter.getData();
            if (ListUtils.isListEmpty(chatRoomMembers) && chatRoomMembers.size() <= position) {
                return;
            }
            //自己的IM房间，暂时仅做在线用户列表，故点击事件交由调用者处理
            IMRoomMember chatRoomMember = chatRoomMembers.get(position).imRoomMember;
            if (onlineMemberItemClick != null) {
                onlineMemberItemClick.onItemClick(chatRoomMember);
            }
        }
    }

    public void onRoomMemberComingIn(String account, List<IMRoomOnlineMember> dataList, IMRoomMessage imRoomMessage) {
        getMvpPresenter().onMemberInRefreshData(account, dataList, mPage, imRoomMessage);
    }

    public void onRoomMemberDownUpMic(String account, boolean isUpMic, List<IMRoomOnlineMember> dataList) {
        Disposable disposable = getMvpPresenter().onMemberDownUpMic(account, isUpMic, dataList, mPage);
        if (null != disposable && null != mCompositeDisposable) {
            mCompositeDisposable.add(disposable);
        }
    }

    public void onUpdateRoomMemberManager(String account, boolean isRemoveManager, List<IMRoomOnlineMember> dataList) {
        Disposable disposable = getMvpPresenter().onUpdateMemberManager(account, dataList, isRemoveManager, mPage);
        if (null != disposable && null != mCompositeDisposable) {
            mCompositeDisposable.add(disposable);
        }
    }

    public void addRoomMemberBlack() {
    }

    public interface OnlineMemberItemClick {
        void onItemClick(IMRoomMember chatRoomMember);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mRefreshLayout != null) {
            mRefreshLayout.setOnRefreshListener(null);
            mRefreshLayout = null;
        }
        roomOnlineMemberAdapter = null;
        mRecyclerView = null;
        if (mView != null) {
            mView = null;
        }
    }
}
