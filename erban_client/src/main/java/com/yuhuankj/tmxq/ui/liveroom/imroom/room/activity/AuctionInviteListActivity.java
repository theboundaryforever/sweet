package com.yuhuankj.tmxq.ui.liveroom.imroom.room.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import com.juxiao.library_ui.widget.AppToolBar;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.room.auction.ParticipateAuctionBean;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter.AuctionInviteListAdapter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.dialog.AuctionSortListPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.dialog.IAuctionSortListView;

import java.util.List;

/**
 * 文件描述：等待被拍卖的用户排序列表
 *
 * @auther：zwk
 * @data：2019/8/1
 */
@CreatePresenter(AuctionSortListPresenter.class)
public class AuctionInviteListActivity extends BaseMvpActivity<IAuctionSortListView, AuctionSortListPresenter> implements IAuctionSortListView {
    private RecyclerView rvInviteList;
    private AuctionInviteListAdapter mAdapter;

    public static void start(Context context) {
        Intent intent = new Intent(context, AuctionInviteListActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auction_invite_list);
        rvInviteList = (RecyclerView) findViewById(R.id.rv_auction_invite_list);
        AppToolBar appToolBar = (AppToolBar) findViewById(R.id.atb_title);
        appToolBar.setOnLeftImgBtnClickListener(this::finish);
        mAdapter = new AuctionInviteListAdapter();
        rvInviteList.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener((adapter, view, position) -> {
            if (RoomDataManager.get().getAuction() != null && RoomDataManager.get().getAuction().getUid() > 0) {//判断是否有用户在拍卖位
                toast("当前有用户在拍卖中!");
            } else {
                if (mAdapter != null
                        && !ListUtils.isListEmpty(mAdapter.getData())
                        && mAdapter.getData().size() > position) {
                    getMvpPresenter().operateUpMicro(1, mAdapter.getData().get(position).getUid(), true);
                    finish();
                }
            }
        });
        showLoading();
        getMvpPresenter().getRoomAuctionList(RoomDataManager.get().getCurrentRoomInfo() != null ? RoomDataManager.get().getCurrentRoomInfo().getRoomId() : 0);

    }

    @Override
    public void showAuctionSortListView(List<ParticipateAuctionBean> datas) {
        hideStatus();
        if (ListUtils.isListEmpty(datas)) {
            showNoData("暂无用户参加拍卖哦～");
        } else {
            if (mAdapter != null) {
                mAdapter.setNewData(datas);
            }
        }
    }

    @Override
    public void showErrorAuctionSortListView() {
        hideStatus();
        showNetworkErr();
    }

    @Override
    public void inviteAuctionSucc() {
        toast("邀请拍卖位上麦成功");
        finish();
    }

    @Override
    public void showErrorToast(String error) {
        toast(error);
    }
}
