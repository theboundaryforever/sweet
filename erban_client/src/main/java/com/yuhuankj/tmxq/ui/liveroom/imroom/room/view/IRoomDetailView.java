package com.yuhuankj.tmxq.ui.liveroom.imroom.room.view;

import android.support.annotation.StringRes;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.redpacket.bean.ActionDialogInfo;

import java.util.List;

public interface IRoomDetailView extends IMvpBaseView {

    void refreshMicroView(int position, int roomEvent);

    void refreshMicBtnStatus();

    void showRechargeTipDialog();

    void startP2PSession(long friendUid);

    void toast(String msg);

    void toast(int resId);

    void hiddleRoomAttentionView();

    void showRoomAttentionStatus(boolean status);

    void onActivityFinish();

    void showReportDialog(int type, long uid);

    void showGiftDialogView(long uid, boolean isPersional);

    void showUserInfoDialogView(long uid);

    void openUserInfoActivity(long uid);

    void showShareDialogView();

    void showCommonPopupDialog(List<ButtonItem> buttonItems, @StringRes int strId);

    void openRoomInviteActivity(int micPosition);

    void openRoomSettingActivity();

    void showRoomActionBanner(List<ActionDialogInfo> actionDialogInfos);

    /**
     * 个人房-房主-停播操作，跳转开播统计界面
     */
    void openRoomLiveInfoH5View();

    void sendGiftUpdatePacketGiftCount(GiftInfo giftInfo);

    void sendGiftUpdateMinusBeanCount(int minusCount);

    void sendGiftUpdateMinusGoldCount(int minusCount);

    default void showAuctionSettingDialog(long adminUid,boolean isInvite){}
}
