package com.yuhuankj.tmxq.ui.liveroom.nimroom.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.netease.nim.uikit.common.util.sys.NetworkUtil;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.room.bean.OnlineChatMember;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.presenter.RoomInvitePresenter;
import com.tongdaxing.xchat_core.room.view.IRoomInviteView;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.adapter.RoomInviteAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p> 抱人上麦 </p>
 *
 * @author jiahui
 * @date 2017/12/21
 */
@CreatePresenter(RoomInvitePresenter.class)
public class RoomInviteActivity extends BaseMvpActivity<IRoomInviteView, RoomInvitePresenter>
        implements IRoomInviteView, RoomInviteAdapter.OnItemClickListener, RoomInviteAdapter.OnRoomOnlineNumberChangeListener {
    private RoomInviteAdapter mRoomInviteAdapter;
    private SmartRefreshLayout mRefreshLayout;
    private RecyclerView mRecyclerView;
    private TextView tvSearch;
    private ImageView imvClearText;
    private EditText edtSearch;

    private int mPage = Constants.PAGE_START;
    private int micPosition;

    public static void openActivity(FragmentActivity fragmentActivity, int micPosition) {
        Intent intent = new Intent(fragmentActivity, RoomInviteActivity.class);
        intent.putExtra(Constants.KEY_POSITION, micPosition);
        fragmentActivity.startActivityForResult(intent, 200);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_invite);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRefreshLayout = (SmartRefreshLayout) findViewById(R.id.refresh_layout);
        initTitleBar(getString(R.string.title_online));
        mTitleBar.setCommonBackgroundColor(Color.parseColor("#FAFAFA"));
        Intent intent = getIntent();
        if (intent != null)
            micPosition = intent.getIntExtra(Constants.KEY_POSITION, Integer.MIN_VALUE);

        tvSearch = (TextView) findViewById(R.id.tvSearch);
        imvClearText = (ImageView) findViewById(R.id.imvClearText);
        edtSearch = (EditText) findViewById(R.id.edtSearch);
        GrowingIO.getInstance().trackEditText(edtSearch);
        edtSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus && edtSearch.getText().length() > 0) {
                    imvClearText.setVisibility(View.VISIBLE);
                } else {
                    imvClearText.setVisibility(View.GONE);
                }
            }
        });
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edtSearch.getText().length() > 0) {
                    imvClearText.setVisibility(View.VISIBLE);
                } else {
                    imvClearText.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        tvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uidStr = edtSearch.getText().toString();
                if (TextUtils.isEmpty(uidStr)) {
                    toast("请输入用户ID");
                } else {
                    searchUserById(uidStr);
                }
            }
        });
        imvClearText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtSearch.setText("");
            }
        });

        mRoomInviteAdapter = new RoomInviteAdapter(this, this);
        mRecyclerView.setAdapter(mRoomInviteAdapter);
        mRoomInviteAdapter.setOnRoomOnlineNumberChangeListener(this);

        mRefreshLayout.setOnRefreshLoadmoreListener(new OnRefreshLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshLayout) {
                if (!NetworkUtil.isNetAvailable(RoomInviteActivity.this)) {
                    mRefreshLayout.finishLoadmore();
                    return;
                }
                List<ChatRoomMember> data = mRoomInviteAdapter.getChatRoomMemberList();
                if (ListUtils.isListEmpty(data)) {
                    mRefreshLayout.finishLoadmore();
                    return;
                }
                loadData(data.get(data.size() - 1).getEnterTime());
            }

            @Override
            public void onRefresh(RefreshLayout refreshLayout) {
                if (!NetworkUtil.isNetAvailable(RoomInviteActivity.this)) {
                    mRefreshLayout.finishRefresh();
                    return;
                }
                firstLoad();
            }
        });
        showLoading();
        firstLoad();
    }

    public void firstLoad() {
        mPage = Constants.PAGE_START;
        loadData(0);
    }

    private void loadData(long time) {
        getMvpPresenter().requestChatMemberByPage(mPage, time);
    }

    @Override
    public void onRequestChatMemberByPageSuccess(List<OnlineChatMember> memberList, int page) {

    }

    @Override
    public void onRequestChatMemberByPageFail(String errorStr, int page) {
        mPage = page;
        if (mPage == Constants.PAGE_START) {
            mRefreshLayout.finishRefresh(0);
            showNoData(getString(R.string.data_error));
        } else {
            mRefreshLayout.finishLoadmore(0);
        }
    }

    @Override
    public void onGetOnLineUserList(boolean isSuccess, String message, int page, List<OnlineChatMember> onlineChatMembers) {

    }

    @Override
    public void onRequestMemberByPageSuccess(List<ChatRoomMember> memberList, int page) {
        mPage = page;
        if (mPage == Constants.PAGE_START) {
            List<ChatRoomMember> chatRoomMemberList = mRoomInviteAdapter.getChatRoomMemberList();
            if (!ListUtils.isListEmpty(chatRoomMemberList))
                chatRoomMemberList.clear();
            mRefreshLayout.finishRefresh(0);
            if (ListUtils.isListEmpty(memberList)) {
                showNoData("暂无可抱用户");
            } else {
                hideStatus();
                mRoomInviteAdapter.addChatRoomMemberList(memberList);
            }
        } else {
            mRefreshLayout.finishLoadmore(0);
            if (!ListUtils.isListEmpty(memberList)) {
                mRoomInviteAdapter.addChatRoomMemberList(memberList);
            }
        }
    }

    //根据ID搜索在这个房间的某个用户
    public void searchUserById(String userNo) {
        RoomInfo roomInfo = RoomServiceScheduler.getCurrentRoomInfo();
        if (roomInfo == null)
            return;
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomId", String.valueOf(roomInfo.getRoomId()));
        params.put("userNo", userNo);
        OkHttpManager.getInstance().postRequest(UriProvider.searchUserById(), params, new HttpRequestCallBack<UserInfo>() {
            @Override
            public void onSuccess(String message, UserInfo response) {
                if (response == null || response.getUid() <= 0L) {
                    toast("该用户不在房间内");
                    mRoomInviteAdapter.getChatRoomMemberList().clear();
                    mRoomInviteAdapter.notifyDataSetChanged();
                    showNoData("暂无可抱用户");
                } else {
                    hideStatus();
                    ChatRoomMember member = new ChatRoomMember();
                    member.setNick(response.getNick());
                    member.setAvatar(response.getAvatar());
                    member.setAccount(String.valueOf(response.getUid()));
                    if (mRoomInviteAdapter.getChatRoomMemberList() != null) {
                        mRoomInviteAdapter.getChatRoomMemberList().clear();
                    } else {
                        mRoomInviteAdapter.addChatRoomMemberList(new ArrayList<>());
                    }
                    mRoomInviteAdapter.getChatRoomMemberList().add(member);
                    mRoomInviteAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                toast(msg);
            }
        });
    }

    @Override
    public void onClick(ChatRoomMember chatRoomMember) {
        if (chatRoomMember == null) return;
        Intent intent = new Intent();
        intent.putExtra("account", chatRoomMember.getAccount());
        intent.putExtra(Constants.KEY_POSITION, micPosition);
        setResult(100, intent);
        finish();
    }

    @Override
    public void onReloadDate() {
        super.onReloadDate();
        showLoading();
        firstLoad();
    }

    @Override
    public void onRoomOnlineNumberChange(int number) {
        if (number == 0) {
            showNoData();
        }
    }

    @Override
    protected void onDestroy() {
        if (mRoomInviteAdapter != null)
            mRoomInviteAdapter.onRelease();
        super.onDestroy();
    }
}
