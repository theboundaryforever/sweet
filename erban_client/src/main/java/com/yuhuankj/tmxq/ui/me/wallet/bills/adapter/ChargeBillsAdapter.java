package com.yuhuankj.tmxq.ui.me.wallet.bills.adapter;

import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.libcommon.utils.TimeUtils;
import com.tongdaxing.xchat_core.bills.bean.BillItemEntity;
import com.tongdaxing.xchat_core.bills.bean.ExpendInfo;
import com.yuhuankj.tmxq.R;

import java.util.List;

/**
 * 充值记录 ExpendInfo
 * Created by ${Seven} on 2017/9/15.
 */
public class ChargeBillsAdapter extends BillBaseAdapter {

    public ChargeBillsAdapter(List<BillItemEntity> billItemEntityList) {
        super(billItemEntityList);
        addItemType(BillItemEntity.ITEM_NORMAL, R.layout.list_charge_bills_item);
    }

    @Override
    public void convertNormal(BaseViewHolder baseViewHolder, BillItemEntity billItemEntity) {
        ExpendInfo expendInfo = billItemEntity.mChargeExpendInfo;
        if (expendInfo == null) {
            return;
        }
        baseViewHolder.setText(R.id.tv_gold, "充值" + expendInfo.getGoldNum() + "金币")
                .setText(R.id.tv_money, expendInfo.getShowStr())
                .setText(R.id.tv_charge_time, TimeUtils.getYearMonthDayHourMinuteSecond(expendInfo.getRecordTime()));
    }
}
