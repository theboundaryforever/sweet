package com.yuhuankj.tmxq.ui.login;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;

import java.util.HashMap;
import java.util.Map;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.wechat.friends.Wechat;

public class GetSmsCodeToBindThirdPlatformPresenter extends AbstractMvpPresenter<GetSmsCodeToBindThirdPlatformView> {

    private final String TAG = GetSmsCodeToBindThirdPlatformPresenter.class.getSimpleName();

    public GetSmsCodeToBindThirdPlatformPresenter() {

    }

    public void getSmsCode(String phone) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("phone ", phone);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.getBindThirdPlatformSmsCodeUrl(), params, new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (null != getMvpView()) {
                    getMvpView().onGetSmsCode(false, e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult response) {
                getMvpView().onGetSmsCode(200 == response.getCode(), response.getMessage());
            }
        });
    }

    public void getThirdPlatformAccessToken(int third_platform_type) {
        Platform wechat = ShareSDK.getPlatform(third_platform_type == 2 ? QQ.NAME : Wechat.NAME);
        if (!wechat.isClientValid()) {
            getMvpView().onThirdPlatformLogined(false, third_platform_type == 2 ? "未安装QQ" : "未安装微信",
                    third_platform_type, null, null, null);
            return;
        }
        if (wechat.isAuthValid()) {
            wechat.removeAccount(true);
        }

        wechat.setPlatformActionListener(new PlatformActionListener() {
            @Override
            public void onComplete(final Platform platform, int i, HashMap<String, Object> hashMap) {
                LogUtils.d(TAG, "onComplete-platform:" + platform + " i:" + i);
                if (i == Platform.ACTION_USER_INFOR) {
                    getMvpView().onThirdPlatformLogined(true, null, third_platform_type,
                            platform.getDb().getToken(), platform.getDb().getUserId(), platform.getDb().get("unionid"));
                }

            }

            @Override
            public void onError(Platform platform, int i, Throwable throwable) {
                LogUtils.d(TAG, "onError-platform:" + platform + " i:" + i);
                if (null != throwable) {
                    getMvpView().onThirdPlatformLogined(false, "绑定失败", third_platform_type, null, null, null);
                }
            }

            @Override
            public void onCancel(Platform platform, int i) {
                LogUtils.d(TAG, "onCancel-platform:" + platform + " i:" + i);
                getMvpView().onThirdPlatformLogined(false, "绑定取消", third_platform_type, null, null, null);
            }
        });
        wechat.SSOSetting(false);
        wechat.showUser(null);
    }

    public void bindThirdPlatform(String phone, int third_platform_type, String accessToken,
                                  String code, String openId, String unionId) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("phone", phone);
        params.put("accessToken", accessToken);
        params.put("code", code);
        params.put("openId", openId);
        params.put("unionId", unionId);
        params.put("type", third_platform_type + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().postRequest(UriProvider.getBindThirdPlatformUrl(), params, new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (null != getMvpView()) {
                    getMvpView().onBindThirdPlatform(false, e.getMessage(), third_platform_type);
                }
            }

            @Override
            public void onResponse(ServiceResult response) {
                getMvpView().onBindThirdPlatform(200 == response.getCode(), response.getMessage(), third_platform_type);
            }
        });
    }

    public void unBindThirdPlatform(int third_platform_type, String code) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("code", code);
        params.put("type", third_platform_type + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().postRequest(UriProvider.getUnBindThirdPlatformUrl(), params, new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (null != getMvpView()) {
                    getMvpView().onUnBindThirdPlatform(false, e.getMessage(), third_platform_type);
                }
            }

            @Override
            public void onResponse(ServiceResult response) {
                getMvpView().onUnBindThirdPlatform(200 == response.getCode(), response.getMessage(), third_platform_type);
            }
        });
    }

    public void checkCode(String code) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("code", code);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().postRequest(UriProvider.getCheckSmsCodeUrl(), params, new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (null != getMvpView()) {
                    getMvpView().onCheckSmsCode(false, e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult response) {
                getMvpView().onCheckSmsCode(200 == response.getCode(), response.getMessage());
            }
        });
    }

}
