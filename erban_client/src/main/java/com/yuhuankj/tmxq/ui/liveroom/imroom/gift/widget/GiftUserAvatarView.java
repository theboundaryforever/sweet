package com.yuhuankj.tmxq.ui.liveroom.imroom.gift.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;

import com.noober.background.view.BLTextView;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.imroom.gift.adapter.GiftTopAvatarAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * 文件描述：礼物弹框顶部用户头像列表View
 *
 * @auther：zwk
 * @data：2019/5/15
 */
public class GiftUserAvatarView extends LinearLayout implements GiftTopAvatarAdapter.OnCancelAllMicSelectListener {
    private BLTextView bltAllMic;
    private RecyclerView rvMicList;
    private GiftTopAvatarAdapter mAdapter;


    public GiftUserAvatarView(Context context) {
        this(context, null);
    }

    public GiftUserAvatarView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
        initListener();
    }

    private void initView(Context context) {
        setOrientation(HORIZONTAL);
        setGravity(Gravity.CENTER_VERTICAL);
        inflate(context, R.layout.view_gift_user_avatar, this);
        bltAllMic = findViewById(R.id.blt_gift_all_mic);
        rvMicList = findViewById(R.id.rv_gift_mic_list);
        rvMicList.setLayoutManager(new RecyclerViewNoBugLinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        mAdapter = new GiftTopAvatarAdapter();
        rvMicList.setAdapter(mAdapter);
    }

    private void initListener() {
        mAdapter.setOnCancelAllMicSelectListener(this);
        bltAllMic.setOnClickListener(v -> {
            bltAllMic.setEnabled(false);
            boolean isSelect = bltAllMic.isSelected();
            bltAllMic.setSelected(!isSelect);
            if (mAdapter != null && !ListUtils.isListEmpty(mAdapter.getData())) {
                for (int i = 0; i < mAdapter.getData().size(); i++) {
                    mAdapter.getData().get(i).setSelect(!isSelect);
                    mAdapter.notifyDataSetChanged();
                }
                mAdapter.setAllSelect(!isSelect);
            }
            bltAllMic.setEnabled(true);
        });
    }

    @Override
    public void onAllMicSelectChange(boolean isAllMic) {
        bltAllMic.setSelected(isAllMic);
    }

    /**
     * 单人送礼隐藏全麦开关
     *
     * @param isPersonal
     */
    public void setPersonalAvatar(boolean isPersonal) {
        bltAllMic.setVisibility(isPersonal ? View.GONE : View.VISIBLE);
    }

    /**
     * 设置全麦状态
     *
     * @param isAll
     */
    public void setAvatarAllMic(boolean isAll) {
        if (mAdapter != null) {
            mAdapter.setAllSelect(isAll);
            bltAllMic.setSelected(isAll);
        }
    }


    /**
     * 更新头像列表的信息
     *
     * @param datas
     */
    public void updateAllMicAvatar(List<MicMemberInfo> datas) {
        if (mAdapter != null) {
            mAdapter.setNewData(datas);
        }
    }


    /**
     * 获取当前选中的赠送礼物的对象
     *
     * @return
     */
    public List<MicMemberInfo> getSelectMicAvatar() {
        List<MicMemberInfo> micMemberInfos = null;
        if (mAdapter != null && !ListUtils.isListEmpty(mAdapter.getData())) {
            micMemberInfos = new ArrayList<>();
            for (int i = 0; i < mAdapter.getData().size(); i++) {
                MicMemberInfo micMemberInfo = mAdapter.getData().get(i);
                if (micMemberInfo.isSelect()) {
                    micMemberInfos.add(micMemberInfo);
                }
            }
        }
        return micMemberInfos;
    }

    public boolean isAllMic() {
        if (bltAllMic != null && bltAllMic.getVisibility() == View.VISIBLE) {
            return bltAllMic.isSelected();
        } else {
            return false;
        }
    }

    public void release(){
        if (mAdapter != null){
            mAdapter.setOnCancelAllMicSelectListener(null);
        }
    }
}
