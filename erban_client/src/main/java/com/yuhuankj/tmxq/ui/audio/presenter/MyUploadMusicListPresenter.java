package com.yuhuankj.tmxq.ui.audio.presenter;

import android.text.TextUtils;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.music.bean.HotMusicInfo;
import com.yuhuankj.tmxq.ui.audio.activity.MyUploadMusicView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>  </p>
 *
 * @author jiahui
 * @date 2017/12/8
 */
public class MyUploadMusicListPresenter extends AbstractMvpPresenter<MyUploadMusicView> {

    private final String TAG = MyUploadMusicListPresenter.class.getSimpleName();

    public void getUploadMusicList(int pageNum, int pageSize, String key) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        if (!TextUtils.isEmpty(key)) {
            params.put("key", key);//key不传默认所有热门歌曲
        }
        params.put("pageNum", pageNum + "");//第一页
        params.put("pageSize", pageSize + "");//最多展示1000条
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
//        OkHttpManager.getInstance().getRequest(UriProvider.getMyUploadMusicListUrl(), params, new OkHttpManager.MyCallBack<ServiceResult<HotMusicInfoResult>>() {
//            @Override
//            public void onError(Exception e) {
//                e.printStackTrace();
//                if(null != getMvpView()){
//                    getMvpView().onGetUploadMusicInfoList(false,e.getMessage(),null);
//                }
//            }
//
//            @Override
//            public void onResponse(ServiceResult<HotMusicInfoResult> response) {
//                if(null != getMvpView()){
//                    if (null != response && response.isSuccess()) {
//                        getMvpView().onGetUploadMusicInfoList(true,null,response.getData().list);
//                    }else{
//                        getMvpView().onGetUploadMusicInfoList(false,response.getMessage(),null);
//                    }
//                }
//
//            }
//        });

        OkHttpManager.getInstance().getRequest(UriProvider.getMyUploadMusicListUrl(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (null != getMvpView()) {
                    getMvpView().onGetUploadMusicInfoList(false, e.getMessage(), null);
                }
            }

            @Override
            public void onResponse(Json json) {
                LogUtils.d(TAG, "onResponse-json:" + json);
                if (null != getMvpView()) {
                    String mesg = json.str("message");
                    boolean isSuccess = json.num("code") == 200;
                    List<HotMusicInfo> infos = null;
                    try {
                        JSONObject data = json.getJSONObject("data");
                        if (null != data) {
                            JSONArray list = data.getJSONArray("list");
                            if (null != list && list.length() > 0) {
                                infos = new ArrayList<>();
                                for (int i = 0; i < list.length(); i++) {
                                    JSONObject object = list.getJSONObject(i);
                                    HotMusicInfo hotMusicInfo = new HotMusicInfo();
                                    hotMusicInfo.setId(object.getLong("id"));
                                    hotMusicInfo.setCreateTime(object.getLong("createTime"));
                                    hotMusicInfo.setUpdateTime(object.getLong("updateTime"));
                                    hotMusicInfo.setSingCount(object.getInt("singCount"));
                                    hotMusicInfo.setSingName(object.getString("singName"));
                                    hotMusicInfo.setSingerName(object.getString("singerName"));
                                    hotMusicInfo.setSingUrl(object.getString("singUrl"));
                                    hotMusicInfo.setSingType(object.getInt("singType"));
                                    hotMusicInfo.setSingSize(object.getInt("singSize"));
                                    if (object.has("upUserId")) {
                                        hotMusicInfo.setUpUserId(object.getInt("upUserId"));
                                    }
                                    if (object.has("userNo")) {
                                        hotMusicInfo.setUserNo(object.getLong("userNo"));
                                    }
                                    if (object.has("userNick")) {
                                        hotMusicInfo.setUserNick(object.getString("userNick"));
                                    }
                                    if (object.has("singStatus")) {
                                        hotMusicInfo.setSingStatus(object.getInt("singStatus"));
                                    }
                                    if (object.has("userAvatar")) {
                                        hotMusicInfo.setUserAvatar(object.getString("userAvatar"));
                                    }
                                    infos.add(hotMusicInfo);
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    getMvpView().onGetUploadMusicInfoList(isSuccess, mesg, infos);
                }
            }
        });
    }
}
