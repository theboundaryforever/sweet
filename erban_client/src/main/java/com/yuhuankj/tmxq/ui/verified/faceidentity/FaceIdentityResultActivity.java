package com.yuhuankj.tmxq.ui.verified.faceidentity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;

public class FaceIdentityResultActivity extends BaseActivity {
    private View vIconAnima;
    private ImageView imvIcon;
    private TextView tvUserTip;
    private TextView tvEnter;
    private String images;
    private String name = "甜甜用户";
    private String IDCard = "";
    private String phoneNo = "";
    private String token = "";
    private TranslateAnimation animation1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_face_identity_result);
        initView();
        initData();
    }

    private void initView() {
        imvIcon = (ImageView) findViewById(R.id.imvIcon);
        tvUserTip = (TextView) findViewById(R.id.tvUserTip);
        tvEnter = (TextView) findViewById(R.id.tvEnter);
        vIconAnima = findViewById(R.id.vIconAnima);
    }

    private void initData() {
        initTitleBar("审核身份");
        tvEnter.setOnClickListener(this);
        Intent intent = getIntent();
        if (intent == null) {
            SingleToastUtil.showToast("没有获取到用户身份数据");
            finish();
            return;
        }

        images = intent.getStringExtra("images");
        IDCard = intent.getStringExtra("IDCard");
        name = intent.getStringExtra("name");
        phoneNo = intent.getStringExtra("phoneNo");
        if (TextUtils.isEmpty(IDCard) || TextUtils.isEmpty(name) || TextUtils.isEmpty(phoneNo) || TextUtils.isEmpty(images)) {
            SingleToastUtil.showToast("没有获取到用户信息");
            finish();
            return;
        }
        //从控件位置移到控件下面
        animation1 = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_PARENT,
                0f, Animation.RELATIVE_TO_PARENT, 1.0f);
        animation1.setDuration(1000);
        animation1.setInterpolator(new LinearInterpolator());
        //设置动画结束之后是否保持动画开始时的状态
        animation1.setFillBefore(true);
        //设置重复模式
        animation1.setRepeatMode(AnimationSet.REVERSE);
        //设置重复次数
        animation1.setRepeatCount(AnimationSet.INFINITE);
        vIconAnima.startAnimation(animation1);

        tvEnter.postDelayed(new Runnable() {
            @Override
            public void run() {
                identity(images);
            }
        }, 2000);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view == tvEnter) {
            finish();
        }
    }

    private void notityWebChange() {
        Intent intent = new Intent(CommonWebViewActivity.ACTION_RELOAD_ORI_URL);
        intent.putExtra("url", UriProvider.getRealNameAuthUrl());
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    //认证
    private void identity(String megliveData) {
        new FaceIdentityModel().identity(megliveData, new OkHttpManager.MyCallBack<ServiceResult<String>>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                identResult(2, "认证失败");
            }

            @Override
            public void onResponse(ServiceResult<String> response) {
                if (response != null && response.isSuccess()) {
                    //验证成功
                    identResult(1, "恭喜你通过实名认证！");
                } else {
                    if (response != null && !TextUtils.isEmpty(response.getMessage())) {
                        identResult(2, response.getMessage());
                    } else {
                        identResult(2, "认证失败");
                    }
                }
            }
        });
    }

    //设置审核结果数据
    private void identResult(int reulst, String desc) {
        animation1.cancel();
        vIconAnima.setVisibility(View.GONE);
        tvEnter.setVisibility(View.VISIBLE);
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (reulst == 1) {
            notityWebChange();
            //验证成功
            mTitleBar.setTitle("确认身份");
            setTitle("确认身份");
            tvEnter.setText("结束认证");
            tvUserTip.setText("恭喜你通过实名认证！");
            imvIcon.setImageResource(R.drawable.ic_face_identity_shenhechenggong);
            if (userInfo != null) {
                userInfo.setRealNameAudit(true);
            }
        } else {
            mTitleBar.setTitle("审核身份");
            setTitle("审核身份");
            tvEnter.setText("重新认证");
            imvIcon.setImageResource(R.drawable.ic_face_identity_shenheshibai);
            tvUserTip.setText(desc);
            if (userInfo != null) {
                userInfo.setRealNameAudit(false);
            }
        }
    }
}
