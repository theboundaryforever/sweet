package com.yuhuankj.tmxq.ui.room.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.tongdaxing.xchat_core.home.TabInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.widget.itemdecotion.ScaleTransitionPagerTitleView;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.UIUtil;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p> 公共多个滑动tab样式 </p>
 *
 * @author Administrator
 * @date 2017/11/15
 */
public class CommonMagicIndicatorAdapter extends CommonNavigatorAdapter {

    private Context mContext;
    private List<TabInfo> mTitleList;
    private int mBottomMargin;
    private int padding;
    private int bottomLineColor=Color.parseColor("#0bcda8");
    private Map<Integer, SpannableStringBuilder> integerSpannableStringBuilderMap = new HashMap<>();

    public Map<Integer, SpannableStringBuilder> getIntegerSpannableStringBuilderMap() {
        return integerSpannableStringBuilderMap;
    }

    public List<TabInfo> getmTitleList() {
        return mTitleList;
    }

    public CommonMagicIndicatorAdapter(Context context, List<TabInfo> titleList, int bottomMargin) {
        mContext = context;
        mTitleList = titleList;
        mBottomMargin = bottomMargin;
    }

    public CommonMagicIndicatorAdapter(Context context, List<TabInfo> titleList, int bottomMargin, int padding) {
        mContext = context;
        mTitleList = titleList;
        mBottomMargin = bottomMargin;
        this.padding = padding;
    }

    private int size = 17;

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public int getCount() {
        return mTitleList == null ? 0 : mTitleList.size();
    }

    @Override
    public IPagerTitleView getTitleView(Context context, final int i) {
        ScaleTransitionPagerTitleView scaleTransitionPagerTitleView = new ScaleTransitionPagerTitleView(context);
        if (padding > 0) {
            scaleTransitionPagerTitleView.setPadding(padding, 0, padding, 0);
        }
        scaleTransitionPagerTitleView.setNormalColor(ContextCompat.getColor(mContext, R.color.color_666666));
        scaleTransitionPagerTitleView.setSelectedColor(ContextCompat.getColor(mContext, R.color.color_333333));
        scaleTransitionPagerTitleView.setMinScale(0.9f);
        scaleTransitionPagerTitleView.setTextSize(size);

        SpannableStringBuilder spannableStringBuilder = integerSpannableStringBuilderMap.get(i);
        if (spannableStringBuilder != null) {
            scaleTransitionPagerTitleView.setText(spannableStringBuilder);
        } else {
            scaleTransitionPagerTitleView.setText(mTitleList.get(i).getName());
        }


        scaleTransitionPagerTitleView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemSelectListener != null) {
                    mOnItemSelectListener.onItemSelect(i);
                }

            }
        });
        return scaleTransitionPagerTitleView;
    }

    public void setBottomLineColor(int bottomLineColor) {
        this.bottomLineColor = bottomLineColor;
    }

    @Override
    public IPagerIndicator getIndicator(Context context) {
        LinePagerIndicator indicator = new LinePagerIndicator(context);
        indicator.setMode(LinePagerIndicator.MODE_WRAP_CONTENT);
        indicator.setLineHeight(UIUtil.dip2px(mContext, 2.5));
        indicator.setRoundRadius(UIUtil.dip2px(mContext, 1.25));
        indicator.setLineWidth(UIUtil.dip2px(mContext, 27));
        indicator.setColors(bottomLineColor);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        lp.bottomMargin = mBottomMargin;

        indicator.setLayoutParams(lp);
        return indicator;
    }

    private OnItemSelectListener mOnItemSelectListener;

    public void setOnItemSelectListener(OnItemSelectListener onItemSelectListener) {
        mOnItemSelectListener = onItemSelectListener;
    }

    public interface OnItemSelectListener {
        void onItemSelect(int position);
    }
}