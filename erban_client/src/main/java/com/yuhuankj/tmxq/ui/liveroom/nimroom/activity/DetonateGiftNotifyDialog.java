package com.yuhuankj.tmxq.ui.liveroom.nimroom.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.widget.CircleImageView;

/**
 * @author chenran
 * @date 2017/10/4
 */

public class DetonateGiftNotifyDialog extends BaseActivity implements View.OnClickListener {

    private TextView tv_detonateNick;
    private TextView tv_detonateGift;
    private ImageView iv_detonateGift;
    private CircleImageView civ_detonateHead;
    private long uid;
    private long roomUid;
    private String nick;
    private String avatarUrl;
    private GiftInfo info;

    public static void start(Context context, long uid, long roomUid, String nick, String avatarUrl, int giftId) {
        Intent intent = new Intent(context, DetonateGiftNotifyDialog.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("uid", uid);
        intent.putExtra("roomUid", roomUid);
        intent.putExtra("nick", nick);
        intent.putExtra("avatarUrl", avatarUrl);
        intent.putExtra("giftId", giftId);
        context.startActivity(intent);
    }

    public static void startForResult(Fragment fragment, long uid, long roomUid, String nick, String avatarUrl,
                                      int giftId, int request) {
        Intent intent = new Intent(fragment.getActivity(), DetonateGiftNotifyDialog.class);
        /**
         * startActivityForResult官方解释
         * 请注意，此方法只应与定义为返回结果的Intent协议一起使用。
         * 在其他协议（例如Intent.ACTION_MAIN或Intent.ACTION_VIEW）中，您可能无法获得预期的结果。
         * 例如，如果您要启动的活动使用Intent.FLAG_ACTIVITY_NEW_TASK，
         * 则它将不会在您的任务中运行，因此您将立即收到取消结果。
         *
         * 言外之意，如果你想使用startActivityForResult，那么不要使用
         * intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
         * 否则在启动的时候就会立马接收到onActivityResult回调
         */
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("uid", uid);
        intent.putExtra("roomUid", roomUid);
        intent.putExtra("nick", nick);
        intent.putExtra("avatarUrl", avatarUrl);
        intent.putExtra("giftId", giftId);
        fragment.startActivityForResult(intent, request);
    }

    private long showTime = 0L;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detonate_gift_notify);
        //在setContentView前后设置均起不到作用,内容总是包裹居中显示
//        getWindow().setFlags(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        /*设置activity dialog的宽高与屏幕想等*/
        WindowManager windowManager = getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(lp);

        initData();
        initView();
        setListener();
    }

    private void initData() {
        showTime = System.currentTimeMillis();
        Intent intent = getIntent();
        if (null != intent) {
            uid = intent.getLongExtra("uid", 0L);
            roomUid = intent.getLongExtra("roomUid", 0L);
            nick = intent.getStringExtra("nick");
            avatarUrl = intent.getStringExtra("avatarUrl");
            info = CoreManager.getCore(IGiftCore.class).getGiftInfoByGiftId(intent.getIntExtra("giftId", 0));
        }
    }

    private void setListener() {
        findViewById(R.id.ll_blankToClose).setOnClickListener(this);
        tv_detonateGift.setOnClickListener(this);
    }

    private void initView() {
        tv_detonateNick = (TextView) findViewById(R.id.tv_detonateNick);
        tv_detonateNick.setText(nick);
        tv_detonateGift = (TextView) findViewById(R.id.tv_detonateGift);
        iv_detonateGift = (ImageView) findViewById(R.id.iv_detonateGift);
        if (null != info) {
            tv_detonateGift.setText(info.getGiftName());
            GlideApp.with(this).load(info.getGiftUrl()).dontAnimate().into(iv_detonateGift);
        }
        civ_detonateHead = (CircleImageView) findViewById(R.id.civ_detonateHead);
        if (!TextUtils.isEmpty(avatarUrl)) {
            GlideApp.with(this).load(avatarUrl).dontAnimate().into(civ_detonateHead);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_blankToClose:
                if (System.currentTimeMillis() - showTime >= 500L) {
                    finish();
                }
                break;
            case R.id.tv_detonateGift:
                Intent intent = new Intent();
                intent.putExtra("info", info);
                setResult(Activity.RESULT_OK, intent);
                finish();
                break;
            default:
                break;
        }
    }
}
