package com.yuhuankj.tmxq.ui.message.black;

import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

import java.util.List;

public interface BlackListView<T extends AbstractMvpPresenter> extends IMvpBaseView {

    void showBlackList(List<NimUserInfo> nimUserInfos);

    void showBlackListEmptyView();


}
