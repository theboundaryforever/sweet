package com.yuhuankj.tmxq.ui.audio.activity;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.music.bean.HotMusicInfo;

import java.util.List;

public interface SearchServerMusicView<T extends AbstractMvpPresenter> extends IMvpBaseView {
    void onGetSearchMusicList(boolean isSuccess, String msg, List<HotMusicInfo> microMatchPools);
}
