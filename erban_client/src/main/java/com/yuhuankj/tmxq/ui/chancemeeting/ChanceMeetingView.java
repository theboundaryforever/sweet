package com.yuhuankj.tmxq.ui.chancemeeting;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.yuhuankj.tmxq.ui.match.bean.MicroMatch;

import java.util.List;

public interface ChanceMeetingView<T extends AbstractMvpPresenter> extends IMvpBaseView {

    void onGetMeetingData(boolean isSuccess, String msg, List<MicroMatch> list);

    void onReportUserResponse(boolean isSuccess, String msg);

    void onAdmireOperaResponse(boolean isSuccess, String msg, long uid, boolean isAdmire);
}
