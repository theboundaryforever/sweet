package com.yuhuankj.tmxq.ui.webview;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

/**
 * <p> </p>
 *
 * @author weihaitao
 * @date 2019年7月2日 21:12:03
 */
public interface ISingleAudioRoomLiveInfoView extends IMvpBaseView {

    /**
     * 关闭当前统计界面
     */
    void finishLiveInfoView();

    /**
     * 提示停播操作失败信息
     *
     * @param msg
     */
    void showStopLiveErr(String msg);
}
