package com.yuhuankj.tmxq.ui.home.game;

import java.io.Serializable;

public class GameIconEnitity implements Serializable {

    private String activity;//": "string",
    private String backgroundUrl;//": "string",
    private String contentUrl;//": "string",
    private String iosActivity;//": "string",
    private String params;//": "string",
    private String pic;//": "string",
    private String title;//": "string",
    private String url;//": "string"

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getBackgroundUrl() {
        return backgroundUrl;
    }

    public void setBackgroundUrl(String backgroundUrl) {
        this.backgroundUrl = backgroundUrl;
    }

    public String getContentUrl() {
        return contentUrl;
    }

    public void setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
    }

    public String getIosActivity() {
        return iosActivity;
    }

    public void setIosActivity(String iosActivity) {
        this.iosActivity = iosActivity;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
