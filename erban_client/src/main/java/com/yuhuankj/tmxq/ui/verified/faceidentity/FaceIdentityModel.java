package com.yuhuankj.tmxq.ui.verified.faceidentity;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;

import java.util.Map;

/**
 * 实名认证model层
 */
public class FaceIdentityModel extends BaseMvpModel {

    //实名认证
    public void identity(String megliveData, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("megliveData", megliveData);
        OkHttpManager.getInstance().postRequest(UriProvider.getRealName(), params, callBack);
    }
}
