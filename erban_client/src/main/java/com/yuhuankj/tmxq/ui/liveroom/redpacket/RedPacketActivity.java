package com.yuhuankj.tmxq.ui.liveroom.redpacket;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.room_red_packet.RedPacketModel;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.widget.ButtonItemFactory;
import com.yuhuankj.tmxq.ui.me.charge.ChargeActivity;
import com.yuhuankj.tmxq.widget.TitleBar;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author liaoxy
 * @Description:红包设置页面
 * @date 2019/1/17 11:26
 */
public class RedPacketActivity extends BaseActivity {

    private LinearLayout llLucky, llTimer;
    private TextView tvLucky, tvTimer;
    private View vLucky, vTimer;
    private LinearLayout llLuckyMain, llTimerMain;
    private int pkTime = -1;
    private long roomId;//房间ID

    private EditText edtMoney;
    private EditText edtCount;
    private TextView tvSelectTime;
    private LinearLayout llShowBroadcast;
    private View vShowBroadcast;
    private LinearLayout llShowOther;
    private View vShowOther;
    private LinearLayout llResult;
    private TextView tvMoneyResult;
    private TextView tvSendRedPacket;
    private LinearLayout llSetTimer;
    private RelativeLayout rlCount;
    private RelativeLayout rlMoney;

    //根据选择不同的红包，重设不同的view
    private void resetViews(View view) {
        edtMoney = view.findViewById(R.id.edtMoney);
        GrowingIO.getInstance().trackEditText(edtMoney);
        edtCount = view.findViewById(R.id.edtCount);
        GrowingIO.getInstance().trackEditText(edtCount);
        tvSelectTime = view.findViewById(R.id.tvSelectTime);
        llShowBroadcast = view.findViewById(R.id.llShowBroadcast);
        vShowBroadcast = view.findViewById(R.id.vShowBroadcast);
        llShowOther = view.findViewById(R.id.llShowOther);
        vShowOther = view.findViewById(R.id.vShowOther);
        llResult = view.findViewById(R.id.llResult);
        tvMoneyResult = view.findViewById(R.id.tvMoneyResult);
        tvSendRedPacket = view.findViewById(R.id.tvSendRedPacket);
        llSetTimer = view.findViewById(R.id.llSetTimer);
        rlCount = view.findViewById(R.id.rlCount);
        rlMoney = view.findViewById(R.id.rlMoney);

        rlCount.setOnClickListener(this);
        rlMoney.setOnClickListener(this);
        edtMoney.setOnClickListener(this);
        edtCount.setOnClickListener(this);
        tvSendRedPacket.setOnClickListener(this);
        llShowBroadcast.setOnClickListener(this);
        llShowOther.setOnClickListener(this);
        tvSelectTime.setOnClickListener(this);

        edtMoney.setSelection(edtMoney.getText().length());
        edtCount.setSelection(edtCount.getText().length());

        edtMoney.addTextChangedListener(textWatcher);
        edtCount.addTextChangedListener(textWatcher);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_red_packet);
        initTitleBar("红包设置");
        initView();
        initData();
    }

    private void initView() {
        llLucky = (LinearLayout) findViewById(R.id.llLucky);
        llTimer = (LinearLayout) findViewById(R.id.llTimer);
        tvLucky = (TextView) findViewById(R.id.tvLucky);
        tvTimer = (TextView) findViewById(R.id.tvTimer);
        vLucky = findViewById(R.id.vLucky);
        vTimer = findViewById(R.id.vTimer);

        llLuckyMain = (LinearLayout) findViewById(R.id.llLuckyMain);
        llTimerMain = (LinearLayout) findViewById(R.id.llTimerMain);

        tvTimer.setSelected(false);
        vTimer.setVisibility(View.INVISIBLE);
        llTimerMain.setVisibility(View.GONE);
        tvLucky.setSelected(true);
        vLucky.setVisibility(View.VISIBLE);
        llLuckyMain.setVisibility(View.VISIBLE);
        resetViews(llLuckyMain);
        llSetTimer.setVisibility(View.GONE);

        llLuckyMain.findViewById(R.id.llShowBroadcast).setEnabled(false);
        llLuckyMain.findViewById(R.id.llShowOther).setEnabled(false);
        llTimerMain.findViewById(R.id.llShowBroadcast).setEnabled(false);
        llTimerMain.findViewById(R.id.llShowOther).setEnabled(false);

        llShowBroadcast.setEnabled(false);
        llShowOther.setEnabled(false);
        vShowBroadcast.setSelected(false);
        vShowOther.setSelected(false);
    }

    private void initData() {
        llLucky.setOnClickListener(this);
        llTimer.setOnClickListener(this);

        resetViews(llLuckyMain);

        Intent intent = getIntent();
        if (intent != null) {
            roomId = intent.getLongExtra("roomId", -1);
        }
        if (roomId == -1) {
            SingleToastUtil.showToast("房间ID不正确");
            finish();
        }
    }

    @Override
    public void initTitleBar(String title) {
        super.initTitleBar(title);
        TitleBar titleBar = (TitleBar) findViewById(R.id.title_bar);
        titleBar.setActionTextColor(R.color.text_color_primary);
        titleBar.addAction(new TitleBar.TextAction("记录") {
            @Override
            public void performAction(View view) {
                startActivity(new Intent(RedPacketActivity.this, RedPacketHistoryActivity.class));
            }
        });
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.llLucky:
                tvTimer.setSelected(false);
                vTimer.setVisibility(View.INVISIBLE);
                llTimerMain.setVisibility(View.GONE);
                tvLucky.setSelected(true);
                vLucky.setVisibility(View.VISIBLE);
                llLuckyMain.setVisibility(View.VISIBLE);
                resetViews(llLuckyMain);
                llSetTimer.setVisibility(View.GONE);
                break;
            case R.id.llTimer:
                tvTimer.setSelected(true);
                vTimer.setVisibility(View.VISIBLE);
                llTimerMain.setVisibility(View.VISIBLE);
                tvLucky.setSelected(false);
                vLucky.setVisibility(View.INVISIBLE);
                llLuckyMain.setVisibility(View.GONE);
                resetViews(llTimerMain);
                llSetTimer.setVisibility(View.VISIBLE);
                break;
            case R.id.rlMoney:
                edtMoney.requestFocus();
                edtMoney.callOnClick();
                showKeyboard(true);
                break;
            case R.id.rlCount:
                edtCount.requestFocus();
                edtCount.callOnClick();
                showKeyboard(true);
                break;
            case R.id.edtMoney:
                // edtMoney.setSelection(edtMoney.getText().length());
                break;
            case R.id.edtCount:
                // edtCount.setSelection(edtCount.getText().length());
                break;
            case R.id.llShowBroadcast:
                llShowBroadcast.setSelected(!llShowBroadcast.isSelected());
                break;
            case R.id.llShowOther:
                llShowOther.setSelected(!llShowOther.isSelected());
                break;
            case R.id.tvSelectTime:
                Map<String, Integer> jsonsTime = new LinkedHashMap<>();
                jsonsTime.put("30秒", 30);
                jsonsTime.put("60秒", 60);
                jsonsTime.put("90秒", 90);
                jsonsTime.put("180秒", 180);
                jsonsTime.put("5分钟", 300);
                jsonsTime.put("10分钟", 600);
                jsonsTime.put("20分钟", 1200);
                buildOptionDialog(jsonsTime, pkTime);
                break;
            case R.id.tvSendRedPacket:
                String moneyStr = edtMoney.getText().toString();
                if (TextUtils.isEmpty(moneyStr)) {
                    SingleToastUtil.showToast("红包最少应该塞入1块金币");
                    return;
                }
                int money = Integer.valueOf(moneyStr);
                if (money < 1 || money > 100000) {
                    SingleToastUtil.showToast("红包金额应该大于1块金币小于10万金币");
                    return;
                }
                String countStr = edtCount.getText().toString();
                if (TextUtils.isEmpty(countStr)) {
                    SingleToastUtil.showToast("红包个数最少为1个");
                    return;
                }
                int count = Integer.valueOf(countStr);
                if (count < 1 || count > 100) {
                    SingleToastUtil.showToast("红包个数应该大于1个小于100个");
                    return;
                }
                if (count > money) {
                    SingleToastUtil.showToast("红包个数不可大于红包金额");
                    return;
                }
                int isShowAll = vShowOther.isSelected() ? 1 : 0;
                int isBroadcast = vShowBroadcast.isSelected() ? 1 : 0;
                int type = llTimerMain.isShown() ? 2 : 1;
                if (type == 2 && pkTime < 0) {
                    SingleToastUtil.showToast("请设置定时红包时间");
                    return;
                }
                DialogManager dialogManager = new DialogManager(this);
                dialogManager.showProgressDialog(this, "请稍后...");
                new RedPacketModel().sendRedPacket(moneyStr, countStr, roomId, isShowAll, isBroadcast, pkTime, type, new OkHttpManager.MyCallBack<ServiceResult<Object>>() {
                    @Override
                    public void onError(Exception e) {
                        dialogManager.dismissDialog();
                        SingleToastUtil.showToast(e.getMessage());
                    }

                    @Override
                    public void onResponse(ServiceResult<Object> response) {
                        dialogManager.dismissDialog();
                        if (response.isSuccess()) {
                            SingleToastUtil.showToast("发送红包成功");
                            finish();
                        } else if (response.getCode() == 2103) {
                            getDialogManager().showOkCancelDialog("余额不足，是否充值?", "是", "否", true, new DialogManager.AbsOkDialogListener() {
                                @Override
                                public void onOk() {
                                    ChargeActivity.start(RedPacketActivity.this);
                                }
                            });
                        } else {
                            SingleToastUtil.showToast(RedPacketActivity.this, response.getMessage());
                        }
                    }
                });
                break;
        }
    }

    public void buildOptionDialog(Map<String, Integer> jsons, int type) {
        List<ButtonItem> buttonItems = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : jsons.entrySet()) {
            ButtonItem msgBlackListItem = ButtonItemFactory.createMsgBlackListItem(entry.getKey(), new ButtonItemFactory.OnItemClick() {
                @Override
                public void itemClick() {
                    tvSelectTime.setText(entry.getKey());
                    pkTime = entry.getValue();
                    isEnableSend();
                }
            });
            if (entry.getValue() == type) {
                msgBlackListItem.textColor = "#09CAA2";
            }
            buttonItems.add(msgBlackListItem);
        }
        getDialogManager().showCommonPopupDialog(buttonItems, getString(R.string.cancel));
    }

    //输入红包金额监听
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            isEnableSend();
        }
    };

    //是否可以发送
    private void isEnableSend() {
        String moneyStr = edtMoney.getText().toString();
        int money = 0;
        if (!TextUtils.isEmpty(moneyStr)) {
            money = Integer.valueOf(moneyStr);
            if (money >= 1000) {
                llShowBroadcast.setEnabled(true);
                if (money >= 10000) {
                    llShowOther.setEnabled(true);
                } else {
                    llShowOther.setEnabled(false);
                    llShowOther.setSelected(false);
                }
            } else {
                llShowBroadcast.setEnabled(false);
                llShowOther.setEnabled(false);
                llShowBroadcast.setSelected(false);
                llShowOther.setSelected(false);
            }
        } else {
            llShowBroadcast.setEnabled(false);
            llShowOther.setEnabled(false);
            llShowBroadcast.setSelected(false);
            llShowOther.setSelected(false);
        }
        String countStr = edtCount.getText().toString();
        int count = 0;
        if (!TextUtils.isEmpty(countStr)) {
            count = Integer.valueOf(countStr);
        }
        if (count > 100) {
            edtCount.setText(100 + "");
        }
        if (money > 100000) {
            edtMoney.setText(100000 + "");
        }
        tvMoneyResult.setText(money + "");
        boolean isEnable = false;
        if (money > 0 && money >= count) {
            isEnable = !llTimerMain.isShown() || tvSelectTime.getText().toString().length() > 0;
        }
        if (money < count) {
            SingleToastUtil.showToast("红包数量不可大于红包金额");
        }
        if (isEnable) {
            tvSendRedPacket.setEnabled(true);
        } else {
            tvSendRedPacket.setEnabled(false);
        }
        edtMoney.setSelection(edtMoney.getText().length());
        edtCount.setSelection(edtCount.getText().length());
    }
}
