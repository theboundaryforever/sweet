package com.yuhuankj.tmxq.ui.liveroom.imroom.room.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

public interface IMultiRoomPemitView extends IMvpBaseView {

    void refreshMultiRoomPemit();

}
