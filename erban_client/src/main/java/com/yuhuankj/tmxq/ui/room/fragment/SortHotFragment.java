package com.yuhuankj.tmxq.ui.room.fragment;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.netease.nim.uikit.common.ui.recyclerview.decoration.SpacingDecoration;
import com.netease.nim.uikit.common.util.sys.NetworkUtil;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.home.HomeRoom;
import com.tongdaxing.xchat_core.home.IHomeCore;
import com.tongdaxing.xchat_core.home.IHomeCoreClient;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.BR;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.bindadapter.BaseAdapter;
import com.yuhuankj.tmxq.base.fragment.BaseLazyFragment;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.UIUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * <p> 首页非Hot界面 </p>
 *
 * @author Administrator
 * @date 2017/11/15
 */
public class SortHotFragment extends BaseLazyFragment implements SwipeRefreshLayout.OnRefreshListener,
        BaseQuickAdapter.RequestLoadMoreListener, BaseQuickAdapter.OnItemClickListener {
    private static final String TAG = SortHotFragment.class.getSimpleName();
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private BaseAdapter<HomeRoom> mNotHotAdapter;
    private List<HomeRoom> mHomeRoomList;
    private int mCurrentPage = Constants.PAGE_START;
    private TabInfo mHomeTabInfo;
    private LinearLayoutManager layoutManager;


    public static SortHotFragment newInstance(TabInfo homeTabInfo) {
        SortHotFragment notHotFragment = new SortHotFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.HOME_TAB_INFO, homeTabInfo);
        notHotFragment.setArguments(bundle);
        return notHotFragment;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mHomeRoomList != null) {
            outState.putParcelableArrayList(Constants.KEY_HOME_NO_HOT_LIST, (ArrayList<? extends Parcelable>) mHomeRoomList);
        }
    }

    @Override
    protected void restoreState(@Nullable Bundle savedInstanceState) {
        super.restoreState(savedInstanceState);
        if (savedInstanceState != null) {
            mHomeRoomList = savedInstanceState.getParcelableArrayList(Constants.KEY_HOME_NO_HOT_LIST);
        }
    }


    @Override
    public void onFindViews() {
        mRecyclerView = mView.findViewById(R.id.recycler_view);
        mSwipeRefreshLayout = mView.findViewById(R.id.swipe_refresh);


    }

    @Override
    public void onSetListener() {
        layoutManager = new RecyclerViewNoBugLinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        int space = mContext.getResources().getDimensionPixelOffset(R.dimen.base_margin_left);
        mRecyclerView.addItemDecoration(new SpacingDecoration(space, space, true));
        mSwipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    protected void onInitArguments(Bundle bundle) {
        super.onInitArguments(bundle);
        if (bundle != null) {
            mHomeTabInfo = bundle.getParcelable(Constants.HOME_TAB_INFO);
        }
    }

    @Override
    public void initiate() {
        mNotHotAdapter = new BaseAdapter<>(R.layout.list_item_home_hot_normal_new, BR.homeHotBean);


        View view = new View(mContext);
        view.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, UIUtil.dip2px(mContext, 5)));
        mNotHotAdapter.addFooterView(view);
        mRecyclerView.setAdapter(mNotHotAdapter);
        mNotHotAdapter.setOnLoadMoreListener(this, mRecyclerView);
        mNotHotAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    protected void onLazyLoadData() {
        LogUtils.i(TAG, "懒加载数据...." + mHomeTabInfo.getName() + "----" + mHomeRoomList);
        if (!ListUtils.isListEmpty(mHomeRoomList)) {
            mNotHotAdapter.setNewData(mHomeRoomList);
            if (mHomeRoomList.size() < Constants.PAGE_SIZE) {
                mNotHotAdapter.setEnableLoadMore(false);
            }
        } else {
            mSwipeRefreshLayout.setRefreshing(true);
            setmCurrentPage(Constants.PAGE_START);
            loadData();
        }
    }


    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_sort_hot;
    }

    private void loadData() {
        if (mHomeTabInfo != null) {
            CoreManager.getCore(IHomeCore.class).getSortDataByTab(mHomeTabInfo.getId(), getmCurrentPage(), Constants.PAGE_SIZE);
        }
    }

    @CoreEvent(coreClientClass = IHomeCoreClient.class)
    public void onGetSortDataByTab(List<HomeRoom> homeRoomList, int tabType, int page) {
        setmCurrentPage(page);
        if (mHomeTabInfo != null && tabType == mHomeTabInfo.getId()) {
            LogUtils.i("liao", mHomeTabInfo.getName() + "---->数据加载回来了一次...." + getmCurrentPage());

            if (!ListUtils.isListEmpty(homeRoomList)) {

                if (getmCurrentPage() == Constants.PAGE_START) {
                    mSwipeRefreshLayout.setRefreshing(false);
                    hideStatus();
                    if (!ListUtils.isListEmpty(mHomeRoomList)) {
                        mHomeRoomList.clear();
                    }
                    mHomeRoomList = homeRoomList;
                    mNotHotAdapter.setNewData(mHomeRoomList);
                    //不够10个的话，不显示加载更多
                    if (homeRoomList.size() < Constants.PAGE_SIZE) {
                        mNotHotAdapter.setEnableLoadMore(false);
                    }
                } else {
                    mNotHotAdapter.addData(homeRoomList);
                    mNotHotAdapter.loadMoreComplete();
                }
                mCurrentPage++;
            } else {
                if (getmCurrentPage() == Constants.PAGE_START) {
                    mSwipeRefreshLayout.setRefreshing(false);
                    showNoData();
                } else {
                    mNotHotAdapter.loadMoreEnd(true);
                }
            }
        }
    }

    private void setmCurrentPage(int page) {
        mCurrentPage = page;
    }

    @CoreEvent(coreClientClass = IHomeCoreClient.class)
    public void onGetSortDataByTabFail(String error, int tabType, int page) {
        setmCurrentPage(page);
        if (mHomeTabInfo != null && tabType == mHomeTabInfo.getId()) {
            if (getmCurrentPage() == Constants.PAGE_START) {
                mSwipeRefreshLayout.setRefreshing(false);
                showNetworkErr();
            } else {
                mNotHotAdapter.loadMoreFail();
                toast(error);
            }
        }
    }

    private int getmCurrentPage() {
        return mCurrentPage;
    }


    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo userInfo) {
        LogUtils.i("liao", "用户更新加载数据....");
        setmCurrentPage(Constants.PAGE_START);
        loadData();
    }


    @Override
    public void onRefresh() {
        LogUtils.i("liao", "刷新加载数据");
        mNotHotAdapter.setEnableLoadMore(true);
        setmCurrentPage(Constants.PAGE_START);
        loadData();
    }

    @Override
    public void onLoadMoreRequested() {
        // 解决数据重复的问题
        if (getmCurrentPage() == Constants.PAGE_START) {
            return;
        }
        if (NetworkUtil.isNetAvailable(mContext)) {
            if (mHomeRoomList != null && mHomeRoomList.size() >= Constants.PAGE_SIZE) {
                LogUtils.i("liao", "加载更多加载数据");
                loadData();
            } else {
                mNotHotAdapter.setEnableLoadMore(false);
            }
        } else {
            mNotHotAdapter.loadMoreEnd(true);
        }
    }

    @Override
    public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
        if (ListUtils.isListEmpty(mHomeRoomList)) {
            return;
        }
        final HomeRoom homeRoom = mHomeRoomList.get(i);
        if (homeRoom == null) {
            return;
        }

        RoomServiceScheduler.getInstance().enterRoom(getContext(), homeRoom.getUid(), homeRoom.getType());
    }


    @Override
    public void onReloadData() {
        super.onReloadData();
        setmCurrentPage(Constants.PAGE_START);
        showLoading();
        loadData();
    }
}