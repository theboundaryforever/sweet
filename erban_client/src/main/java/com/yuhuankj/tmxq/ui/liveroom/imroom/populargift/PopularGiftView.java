package com.yuhuankj.tmxq.ui.liveroom.imroom.populargift;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.juxiao.library_utils.DisplayUtils;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tongdaxing.erban.libcommon.base.AbstractMvpRelativeLayout;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.TouchAndClickOperaUtils;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomEvent;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.R;

/**
 * 文件描述：人气礼物view（抽离逻辑）
 *
 * @auther：zwk
 * @data：2019/7/9
 */
@CreatePresenter(PopularGiftPresenter.class)
public class PopularGiftView extends AbstractMvpRelativeLayout<IMvpBaseView, PopularGiftPresenter> implements IMvpBaseView, View.OnClickListener {

    //    private WaterRippleProgressView waterProgress;/
    private SVGAImageView svgaImageView;
    private int currentProgress = -1;//当前的引爆进度
    private int viewRightMargin = 0;

    public void setViewRightMargin(int viewRightMargin) {
        this.viewRightMargin = viewRightMargin;
    }


    public PopularGiftView(Context context) {
        super(context);
    }

    public PopularGiftView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected int getViewLayoutId() {
        return R.layout.view_room_popular_gift;
    }

    @Override
    public void addCompositeDisposable() {
//        compositeDisposable.add(IMRoomMessageManager.get().getIMRoomMessageFlowable().subscribe(this::accept));
        compositeDisposable.add(IMRoomMessageManager.get().getIMRoomEventObservable().subscribe(this::accept));
    }

    @Override
    public void initialView(Context context) {
//        waterProgress = findViewById(R.id.wrpv_popular_progress);
        svgaImageView = findViewById(R.id.svga_popular_anim);
//        waterProgress.setColor(0xFF8256C9);
//        waterProgress.setTimeInner(300L);
//        waterProgress.setCycle(DisplayUtils.dip2px(context, 15));
//        waterProgress.setWaveHeight(DisplayUtils.dip2px(context, 10));

    }

    @Override
    protected void initListener() {
        setOnClickListener(this);
    }

    @Override
    protected void initViewState() {

    }

    public void initLayoutParams(Context context, int roomType) {
        if (roomType == RoomInfo.ROOMTYPE_MULTI_AUDIO) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) getLayoutParams();
            if (null != lp && null != context) {
                lp.leftMargin = DisplayUtils.getScreenWidth(context) - DisplayUtils.dip2px(context, 10 + 65);
                lp.topMargin = DisplayUtils.getScreenHeight(context) - DisplayUtils.dip2px(context, 105 + 85);
                setLayoutParams(lp);
            }
        } else {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) getLayoutParams();
            if (null != lp && null != context) {
                lp.leftMargin = DisplayUtils.getScreenWidth(context) - DisplayUtils.dip2px(context, 10 + 65);
                lp.topMargin = DisplayUtils.getScreenHeight(context) - DisplayUtils.dip2px(context, 165 + 85);
                setLayoutParams(lp);
            }
        }
    }

    public void addTouchEvent() {
        if (getContext() == null) {
            return;
        }
        TouchAndClickOperaUtils touchAndClickOperaUtils1 = new TouchAndClickOperaUtils(getContext());
        touchAndClickOperaUtils1.setTargetView(this);
        touchAndClickOperaUtils1.setMarginButtom(DisplayUtils.dip2px(getContext(), 20));
        touchAndClickOperaUtils1.setListener(this::showPoplularDialog);
    }

    private void accept(IMRoomEvent imRoomEvent) {
        if (imRoomEvent.getEvent() == IMRoomEvent.ROOM_DETONATE_GIFT_NOTIFY) {
            updatePopularGiftInfo(false, imRoomEvent.getDetonatingState());
        }
    }

    /**
     * 根据人气礼物信息更新人气球的状态
     *
     * @param isInitState     是否是第一次初始化状态
     * @param detonatingState
     */
    public void updatePopularGiftInfo(boolean isInitState, int detonatingState) {
        if (detonatingState <= -1) {//无人气礼物权限
            if (getVisibility() == View.VISIBLE) {
                setVisibility(View.GONE);
            }
        } else if (detonatingState < 100) {//显示进度状态
            if (getVisibility() == View.GONE) {
                setVisibility(View.VISIBLE);
            }
            startSvgaAnim(detonatingState);
        } else {//显示引爆状态
            CoreManager.getCore(IGiftCore.class).requestGiftInfos();
            if (getVisibility() == View.GONE) {
                setVisibility(View.VISIBLE);
            }
            startSvgaAnim(100);
            if (!isInitState && getContext() != null && RoomDataManager.get().getAdditional() != null) {
                PopularGiftResultDialog resultDialog = new PopularGiftResultDialog(getContext(), RoomDataManager.get().getAdditional());
                resultDialog.setOnPopularGiftResultClick(onPopularGiftResultClick);
                resultDialog.show();
            }
        }
    }


    /**
     * 播放引爆礼物的特效动画
     */
    private void startSvgaAnim(int progress) {
        if (svgaImageView == null || getContext() == null) {
            return;
        }
        if (progress <= 0){
            if (svgaImageView.isAnimating()) {
                svgaImageView.stopAnimation(true);
            }
            currentProgress = -1;
        }else {
            int relProgress = getRelProgress(progress);
            if (currentProgress < relProgress){
                currentProgress = relProgress;
                SVGAParser svgaParser = new SVGAParser(getContext());
                svgaParser.decodeFromAssets("popular_gift_" + currentProgress + ".svga", new SVGAParser.ParseCompletion() {
                    @Override
                    public void onComplete(SVGAVideoEntity svgaVideoEntity) {
                        if (null != svgaImageView && svgaVideoEntity != null) {
                            SVGADrawable svgaDrawable = new SVGADrawable(svgaVideoEntity);
                            svgaImageView.setImageDrawable(svgaDrawable);
//                            显示引爆动画,加载svga动画文件
//                                svgaImageView.setVisibility(View.VISIBLE);
                            svgaImageView.startAnimation();
                        }
                    }

                    @Override
                    public void onError() {

                    }
                });
            }
        }
    }

    /**
     * 获取真实进度
     * @param progress
     * @return
     */
    public int getRelProgress(int progress){
        int relProgress = progress/10;
        if (relProgress == 0){
            relProgress = 10;
        }else {
            relProgress= relProgress * 10;
        }
        return relProgress;
    }


    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        if (svgaImageView != null) {
            svgaImageView.stopAnimation(true);
            svgaImageView.clearAnimation();
        }
        currentProgress = -1;
    }

    @Override
    public void onClick(View v) {
        showPoplularDialog();
    }

    private void showPoplularDialog() {
        if (RoomDataManager.get().getAdditional() != null && RoomDataManager.get().getAdditional().getDetonatingState() >= 100) {
            PopularGiftResultDialog resultDialog = new PopularGiftResultDialog(getContext(), RoomDataManager.get().getAdditional());
            resultDialog.setOnPopularGiftResultClick(onPopularGiftResultClick);
            resultDialog.show();
        }
    }

    private OnPopularGiftResultClick onPopularGiftResultClick;

    public void setOnPopularGiftResultClick(OnPopularGiftResultClick onPopularGiftResultClick) {
        this.onPopularGiftResultClick = onPopularGiftResultClick;
    }

}
