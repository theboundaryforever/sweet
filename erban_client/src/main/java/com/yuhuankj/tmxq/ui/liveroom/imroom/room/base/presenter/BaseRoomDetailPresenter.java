package com.yuhuankj.tmxq.ui.liveroom.imroom.room.base.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.im.IMReportBean;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.JavaUtil;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RoomMicInfo;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.liveroom.im.model.AudioConnTimeOutTipsQueueScheduler;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomModel;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomAdmireTimeCounter;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomOwnerLiveTimeCounter;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMember;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMessage;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomQueueInfo;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMWaitQueuePersonInfo;
import com.tongdaxing.xchat_core.manager.RtcEngineManager;
import com.tongdaxing.xchat_core.redpacket.bean.ActionDialogInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.model.AvRoomModel;
import com.tongdaxing.xchat_core.room.model.RoomBaseModel;
import com.tongdaxing.xchat_core.room.model.RoomSettingModel;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.liveroom.imroom.gift.RoomGiftModel;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IRoomDetailView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.IMButtonItemFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_CLOSE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_OPEN;
import static com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager.MIC_POSITION_BY_OWNER;
import static com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager.MSG_SEND_MAX_LENGTH;

/**
 * 房间页面的presenter
 */
public class BaseRoomDetailPresenter<V extends IRoomDetailView> extends BaseRoomPresenter<V> {

    /**
     * 除RoomModel外，RoomDataManager、RtcEngineManager也属于model
     */
    protected IMRoomModel model;

    public boolean hasAttentionRoomAlready = false;

    //当前 麦位锁麦或解锁麦的操作 是否正在进行中
    private boolean isOperateMicroLockOrUnLockRequesting = false;

    private RoomGiftModel roomGiftModel;

    /**
     * 退出房间
     *
     * @param callBack 退出结果回调
     */
    public void exitRoom(CallBack<String> callBack) {
        RoomOwnerLiveTimeCounter.getInstance().release();
        RoomAdmireTimeCounter.getInstance().release();
        AudioConnTimeOutTipsQueueScheduler.getInstance().clear();
        RoomDataManager.get().imRoomAdmireTimeCounterShow = true;
        RoomDataManager.get().singleAudioRoomMsgTipsViewShow = true;
        RoomDataManager.get().singleAudioConnReqNum = 0;
        RoomDataManager.get().hasRequestRecommRoomList = false;
        model.exitRoom(callBack);
    }

    /**
     * 获取礼物消息列表
     */
    public List<IMRoomMessage> getGiftMsgList() {
        return model.getGiftMsgList();
    }

    /**
     * 保存最小化状态
     *
     * @param isMinimize 当前是否是最小化
     */
    public void saveMinimizeStatus(boolean isMinimize) {
        RoomDataManager.get().setMinimize(isMinimize);
    }

    /**
     * 我自己当前是否在麦上
     */
    public boolean isOnMicByMyself() {
        return RoomDataManager.get().isOnMic(CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }

    /**
     * 判断是否是我自己
     */
    public boolean isUserSelf(long uid) {
        return RoomDataManager.get().isUserSelf(uid);
    }

    /**
     * 此人当前是否在麦上
     */
    public boolean isOnMic(long uid) {
        return RoomDataManager.get().isOnMic(uid);
    }

    /**
     * 我自己当前是否是设置了禁听远程麦（静音）
     */
    public boolean isRemoteMuteByMyself() {
        return RtcEngineManager.get().isRemoteMute();
    }

    /**
     * 我自己当前是否禁麦状态
     */
    public boolean isMicMuteByMyself() {
        return RtcEngineManager.get().isMute();
    }

    /**
     * 我自己当前是否是观众身份
     */
    public boolean isAudienceRoleByMyself() {
        return RtcEngineManager.get().isAudienceRole();
    }

    /**
     * 检查并自动上麦（需要上麦的话）
     */
    public void checkAndAutoUpMic() {
        RoomMicInfo roomMicInfo = getRoomMicInfoByMyself();
        //如果我当前已经在麦位上
        if (roomMicInfo != null) {
            //不是禁麦的话，操作开麦
            setMicStatusByMyself(!roomMicInfo.isMicMute());
        } else if (isRoomOwnerByMyself()) {
            //当前我不在麦位上，但我是房主
            operateUpMicro(MIC_POSITION_BY_OWNER, String.valueOf(getCurrentUserId()), false);
        }
    }

    /**
     * 操作上麦
     *
     * @param micPosition   上的麦位
     * @param account       上麦的人
     * @param isInviteUpMic 上麦成功后是否自动开麦
     */
    public void operateUpMicro(int micPosition, String account, boolean isInviteUpMic) {
        model.operateUpMicro(micPosition, account, isInviteUpMic, new CallBack<String>() {
            @Override
            public void onSuccess(String data) {
            }

            @Override
            public void onFail(int code, String error) {
                if (getMvpView() != null) {
                    switch (code) {
                        case 2103:
                            getMvpView().showRechargeTipDialog();
                            break;
                        default:
                            getMvpView().toast(error);
                            break;
                    }
                }
            }
        });
    }


    /**
     * 邀请上麦
     *
     * @param micPosition 上的麦位
     * @param uid         上麦的人
     */
    public void inviteUpMicro(int micPosition, long uid) {
        IMRoomMessageManager.get().inviteMicroPhoneBySdk(uid, micPosition);
    }

    /**
     * 获取我当前在麦上的麦位信息
     *
     * @return 麦位信息 null代表我不在麦上
     */
    private RoomMicInfo getRoomMicInfoByMyself() {
        IMRoomQueueInfo queueInfo = RoomDataManager.get().getRoomQueueMemberInfoMyself();
        return queueInfo != null ? queueInfo.mRoomMicInfo : null;
    }

    /**
     * 我自己是否是房主
     */
    public boolean isRoomOwnerByMyself() {
        return RoomDataManager.get().isRoomOwner();
    }

    /**
     * 此人是否是房主
     */
    public boolean isRoomOwner(long uid) {
        return RoomDataManager.get().isRoomOwner(String.valueOf(uid));
    }

    /**
     * 我自己是否是管理员
     */
    public boolean isRoomAdminByMyself() {
        return RoomDataManager.get().isRoomAdmin();
    }

    /**
     * 此人是否是管理员
     */
    public boolean isRoomAdmin(long uid) {
        return RoomDataManager.get().isRoomAdmin(String.valueOf(uid));
    }

    /**
     * 操作添加/撤销管理员身份
     */
    public void operateAddOrRemoveManager(boolean isAdd, long uid) {
        IMRoomMessageManager.get().markManager(String.valueOf(uid), isAdd, null);
    }

    /**
     * 操作从黑名单中添加/删除
     */
    public void operateAddOrRemoveBlackList(boolean isAdd, long uid) {
        IMRoomMessageManager.get().markBlackList(String.valueOf(uid), isAdd, null);
    }

    /**
     * 获取房主默认信息
     */
    public IMRoomMember getRoomOwnerDefaultMemberInfo() {
        return RoomDataManager.get().getRoomOwnerDefaultMemberInfo();
    }

    /**
     * 操作踢用户出房间
     *
     * @param uid 被踢的用户uid
     */
    public void operateKickMemberToRoom(long uid) {
        IMRoomMessageManager.get().kickMember(String.valueOf(uid), new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().toast(e.getMessage());
                }
            }

            @Override
            public void onResponse(Json response) {
                if (getMvpView() != null) {
                    IMReportBean imReportBean = new IMReportBean(response);
                    if (imReportBean.getReportData().errno != 0) {
                        getMvpView().toast(imReportBean.getReportData().errmsg);
                    }
                }
            }
        });
    }

    /**
     * 设置我自己的麦位状态
     *
     * @param isOpen 是否开麦
     */
    private void setMicStatusByMyself(boolean isOpen) {
        RtcEngineManager.get().setRole(isOpen ? io.agora.rtc.Constants.CLIENT_ROLE_BROADCASTER :
                io.agora.rtc.Constants.CLIENT_ROLE_AUDIENCE);
    }

    /**
     * 获取当房间人员变化的时候记录的时间戳
     */
    public long getTimestampOnMemberChanged() {
        return RoomDataManager.get().getTimestamp();
    }

    /**
     * 当房间人员变化的时候记录一下变化的时间戳
     */
    public void setTimestampOnMemberChanged(long timestamp) {
        RoomDataManager.get().setTimestamp(timestamp);
    }

    /**
     * 操作麦位开麦 或 关麦
     *
     * @param isOpen 是否开麦
     */
    public void operateMicroOpenOrClose(int micPosition, boolean isOpen) {
        model.operateMicroOpenOrClose(micPosition, isOpen);
    }

    /**
     * 切换 闭麦/开麦 状态
     */
    public void switchMicroMuteStatus() {
        if (RoomDataManager.get().getCurrentRoomInfo() != null) {
            RtcEngineManager.get().setMute(!RtcEngineManager.get().isMute());
        }
    }

    /**
     * 操作麦位锁麦 或 解锁麦
     *
     * @param isLock 是否锁麦
     */
    public void operateMicroLockOrUnLock(int micPosition, boolean isLock) {
        if (isOperateMicroLockOrUnLockRequesting) {
            return;
        }
        isOperateMicroLockOrUnLockRequesting = true;
        model.operateMicroLockAndUnLock(micPosition, isLock, new HttpRequestCallBack<String>() {
            @Override
            public void onSuccess(String message, String response) {

            }

            @Override
            public void onFailure(int code, String msg) {

            }

            @Override
            public void onFinish() {
                isOperateMicroLockOrUnLockRequesting = false;
            }
        });
    }

    /**
     * 获取麦序队列信息（包含麦位信息 和 麦上人员信息）
     *
     * @return 返回null代表这个麦位是不存在的
     */
    public IMRoomQueueInfo getMicQueueInfoByMicPosition(int micPosition) {
        return RoomDataManager.get().getRoomQueueMemberInfoByMicPosition(micPosition);
    }

    /**
     * 获取我自己在麦序队列上的信息（包含麦位信息 和 麦上人员信息）
     *
     * @return 返回null代表我不在麦上
     */
    public IMRoomQueueInfo getMicQueueInfoByMicPositionByMyself() {
        return RoomDataManager.get().getRoomQueueMemberInfoMyself();
    }

    /**
     * 发送文本消息
     */
    public void sendTextMsg(String content) {
        RoomInfo roomInfo = getCurrRoomInfo();
        if (roomInfo == null || TextUtils.isEmpty(content)) {
            return;
        }

        if (roomInfo.getPublicChatSwitch() == 1) {
            //禁止了公屏
            getMvpView().toast("公屏消息已经被管理员禁止，请稍候再试");
        } else {
            if (content.length() > MSG_SEND_MAX_LENGTH) {
                //长度限制
                getMvpView().toast("当前文本长度" + content.length() + "已超过最大500限制");
            } else {
                //敏感词检测
                String sensitiveWordData = CoreManager.getCore(VersionsCore.class).getSensitiveWordData();
                if (!TextUtils.isEmpty(sensitiveWordData) && !TextUtils.isEmpty(content)
                        && content.replaceAll("\n", "").replace(" ", "").matches(sensitiveWordData)) {
                    getMvpView().toast(R.string.sensitive_word_data);
                } else {
                    IMRoomMessageManager.get().sendTextMsg(roomInfo.getRoomId(), content, null);
                }
            }
        }
    }

    /**
     * 下麦（自己下麦）
     *
     * @param microPosition 麦位
     */
    public void downMicro(int microPosition) {
        IMRoomMessageManager.get().downMicroPhoneBySdk(microPosition, null);
    }

    /**
     * 踢人下麦
     *
     * @param microPosition 麦位
     * @param targetUid     此人的uid
     */
    public void kickDownMicro(int microPosition, long targetUid) {
        if (RoomDataManager.get().getCurrentRoomInfo() != null) {
            IMRoomMessageManager.get().kickMicroPhoneBySdk(microPosition, targetUid, RoomDataManager.get().getCurrentRoomInfo().getRoomId());
        }
    }

    /**
     * 送礼物
     *
     * @param targetUid  单人时不能为0
     * @param targetUids 单人时为空、多人时不能为空
     */
    public void sendGift(int giftId, long targetUid, List<Long> targetUids, int giftNum) {
        model.sendGift(giftId, targetUid, targetUids, giftNum, new CallBack<Boolean>() {
            @Override
            public void onSuccess(Boolean data) {
            }

            @Override
            public void onFail(int code, String error) {
                if (getMvpView() != null) {
                    if (code == 2103) {
                        getMvpView().showRechargeTipDialog();
                    } else {
                        getMvpView().toast(error);
                    }
                }
            }
        });
    }

    /**
     * 设置等待麦序的信息（等待人数）
     */
    public void setWaitQueuePersonInfo(IMWaitQueuePersonInfo IMWaitQueuePersonInfo) {
        RoomDataManager.get().setIMWaitQueuePersonInfo(IMWaitQueuePersonInfo);
    }

    /**
     * 获取microView的position
     */
    public int getMicroViewPositionByMicPosition(int micPosition) {
        return model.getMicroViewPositionByMicPosition(micPosition);
    }

    /**
     * 根据uid获取所在的麦位
     *
     * @return 麦位
     */
    public int getMicPositionByUid(long uid) {
        return RoomDataManager.get().getMicPosition(uid);
    }

    /**
     * 提交举报
     *
     * @param type       '类型 1. 用户 2. 房间',
     * @param targetUid  对方的uid（如果是房间，传房主UID）
     * @param reportType '举报类型 1.政治 2.色情 3.广告 4.人身攻击 ',
     */
    public void commitReport(int type, long targetUid, int reportType) {
        model.commitReport(type, targetUid, reportType, new HttpRequestCallBack<Object>() {
            @Override
            public void onSuccess(String message, Object response) {
                if (getMvpView() != null) {
                    getMvpView().toast("举报成功，我们会尽快为您处理");
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().toast(msg);
                }
            }
        });
    }

    /**
     * 更新在线人数
     *
     * @param onlineNum 当前在线人数
     */
    public void refreshOnlineNum(int onlineNum) {
        RoomInfo currentRoomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (currentRoomInfo != null) {
            currentRoomInfo.setOnlineNum(onlineNum);
        }
    }


    /**
     * 获取房间的活动信息
     */
    public void getActivityInfo() {

    }

    /**
     * 检查是否已经关注了该房间
     */
    public void getCheckRoomAttention() {
        OkHttpManager.MyCallBack myCallBack = new OkHttpManager.MyCallBack<ServiceResult<Boolean>>() {
            @Override
            public void onError(Exception e) {
                LogUtils.d("BaseRoomDetailPresenter", "getCheckRoomAttention-onError");
                e.printStackTrace();
                if (null != getMvpView()) {
                    getMvpView().hiddleRoomAttentionView();
                }
            }

            @Override
            public void onResponse(ServiceResult<Boolean> response) {
                LogUtils.d("BaseRoomDetailPresenter", "getMeetingData-onResponse");
                if (null != getMvpView()) {
                    if (null == response) {
                        getMvpView().hiddleRoomAttentionView();
                        return;
                    }
                    if (!response.isSuccess()) {
                        getMvpView().hiddleRoomAttentionView();
                        return;
                    }
                    hasAttentionRoomAlready = response.getData();
                    getMvpView().showRoomAttentionStatus(hasAttentionRoomAlready);
                    RoomDataManager.get().setHasAttentionRoom(hasAttentionRoomAlready);
                }
            }
        };
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (null != roomInfo) {
            new RoomBaseModel().checkRoomAttention(roomInfo.getRoomId(), myCallBack);
        }
    }

    public void switchRoomAttentionStatus() {
        LogUtils.d("BaseRoomDetailPresenter", "switchRoomAttentionStatus-hasAttentionRoomAlready" + hasAttentionRoomAlready);
        if (hasAttentionRoomAlready) {
            doRoomCancelAttention();
        } else {
            doRoomAttention();
        }
    }

    /**
     * 关注房间
     */
    public void doRoomAttention() {
        OkHttpManager.MyCallBack myCallBack = new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                LogUtils.d("BaseRoomDetailPresenter", "doRoomAttention-onError");
                e.printStackTrace();
                if (null != getMvpView()) {
                    getMvpView().toast(e.getMessage());
                }
            }

            @Override
            public void onResponse(Json json) {
                LogUtils.d("BaseRoomDetailPresenter", "doRoomAttention-onResponse json:" + json);
                if (null != getMvpView()) {
                    if (json.num("code") == 200) {
                        hasAttentionRoomAlready = true;
                        RoomDataManager.get().setHasAttentionRoom(hasAttentionRoomAlready);
                        getMvpView().showRoomAttentionStatus(hasAttentionRoomAlready);
                    } else {
                        getMvpView().toast(json.str("message"));
                    }
                }
            }
        };
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (null != roomInfo) {
            new RoomBaseModel().doRoomAttention(roomInfo.getRoomId(), myCallBack);
        }
    }

    /**
     * 取消房间关注
     */
    public void doRoomCancelAttention() {
        OkHttpManager.MyCallBack myCallBack = new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                LogUtils.d("BaseRoomDetailPresenter", "doRoomCancelAttention-onError");
                e.printStackTrace();
                if (null != getMvpView()) {
                    getMvpView().toast(e.getMessage());
                }
            }

            @Override
            public void onResponse(Json json) {
                LogUtils.d("BaseRoomDetailPresenter", "doRoomCancelAttention-onResponse json:" + json);
                if (null != getMvpView()) {
                    if (json.num("code") == 200) {
                        hasAttentionRoomAlready = false;
                        getMvpView().showRoomAttentionStatus(hasAttentionRoomAlready);
                        RoomDataManager.get().setHasAttentionRoom(hasAttentionRoomAlready);
                    } else {
                        getMvpView().toast(json.str("message"));
                    }
                }
            }
        };
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (null != roomInfo) {
            new RoomBaseModel().doRoomCancelAttention(roomInfo.getRoomId(), myCallBack);
        }
    }


    /**
     * 处理麦位点击事件
     *
     * @param mContext
     * @param roomQueueInfo
     * @param micPosition
     */
    public void dealWithMicroClickEvent(Context mContext, IMRoomQueueInfo roomQueueInfo, int micPosition) {
        if (mContext == null) {
            return;
        }
        if (roomQueueInfo != null) {
            if (roomQueueInfo.mChatRoomMember == null && micPosition == RoomDataManager.MIC_POSITION_BY_OWNER) {
                //房主的话，创建一下默认信息
                roomQueueInfo.mChatRoomMember = getRoomOwnerDefaultMemberInfo();
            }
            if (roomQueueInfo.mChatRoomMember != null) {
                //麦位状态  禁麦和锁坑
                if (getCurrRoomInfo() == null) {
                    return;
                }
                String currentUid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
                boolean isMySelf = Objects.equals(currentUid, roomQueueInfo.mChatRoomMember.getAccount());
                boolean isTargetRoomAdmin = isRoomAdmin(Long.valueOf(roomQueueInfo.mChatRoomMember.getAccount()));
                boolean isTargetRoomOwner = isRoomOwner(Long.valueOf(roomQueueInfo.mChatRoomMember.getAccount()));
                List<ButtonItem> buttonItems = new ArrayList<>();
                //房主操作
                if (isRoomOwnerByMyself()) {
                    if (!isMySelf) {
                        //点击不是自己
                        buttonItems.add(new ButtonItem("送礼物",
                                () -> {
                                    if (getMvpView() != null && roomQueueInfo.mChatRoomMember != null) {
                                        getMvpView().showGiftDialogView(Long.valueOf(roomQueueInfo.mChatRoomMember.getAccount()), true);
                                    }
                                }));
                        buttonItems.add(new ButtonItem("查看资料",
                                () -> {
                                    if (getMvpView() != null && roomQueueInfo.mChatRoomMember != null) {
                                        getMvpView().openUserInfoActivity(Long.valueOf(roomQueueInfo.mChatRoomMember.getAccount()));
                                    }
                                }));
                        buttonItems.add(IMButtonItemFactory.createKickDownMicItem(roomQueueInfo.mChatRoomMember.getAccount()));
                        //禁麦/解麦操作
                        if (roomQueueInfo.mRoomMicInfo != null) {
                            buttonItems.add(roomQueueInfo.mRoomMicInfo.isMicMute() ?
                                    IMButtonItemFactory.createFreeMicItem(micPosition,
                                            () -> operateMicroOpenOrClose(micPosition, true)) :
                                    IMButtonItemFactory.createLockMicItem(micPosition,
                                            () -> operateMicroOpenOrClose(micPosition, false)));
                        }
                        buttonItems.add(IMButtonItemFactory.createKickOutRoomItem(mContext, roomQueueInfo.mChatRoomMember,
                                String.valueOf(getCurrRoomInfo().getRoomId()), roomQueueInfo.mChatRoomMember.getAccount()));
                        buttonItems.add(IMButtonItemFactory
                                .createMarkManagerListItem(String.valueOf(getCurrRoomInfo().getRoomId()),
                                        roomQueueInfo.mChatRoomMember.getAccount(), !isTargetRoomAdmin, roomQueueInfo.mChatRoomMember));
                        buttonItems.add(IMButtonItemFactory.createMarkBlackListItem(mContext, roomQueueInfo.mChatRoomMember,
                                String.valueOf(getCurrRoomInfo().getRoomId())));
                        if (getMvpView() != null) {
                            getMvpView().showCommonPopupDialog(buttonItems, R.string.cancel);
                        }
                    } else {
                        if (getMvpView() != null) {
                            getMvpView().openUserInfoActivity(JavaUtil.str2long(roomQueueInfo.mChatRoomMember.getAccount()));
                        }
                    }
                } else if (isRoomAdminByMyself()) {
                    //管理员操作
                    if (isMySelf) {
                        //点击自己
                        buttonItems.add(new ButtonItem("查看资料",
                                () -> {
                                    if (getMvpView() != null && roomQueueInfo.mChatRoomMember != null) {
                                        getMvpView().openUserInfoActivity(Long.valueOf(roomQueueInfo.mChatRoomMember.getAccount()));
                                    }
                                }));
                        //下麦旁听
                        buttonItems.add(IMButtonItemFactory.createDownMicItem());
                        //禁麦/解麦操作
                        if (roomQueueInfo.mRoomMicInfo != null) {
                            buttonItems.add(roomQueueInfo.mRoomMicInfo.isMicMute() ?
                                    IMButtonItemFactory.createFreeMicItem(micPosition,
                                            () -> operateMicroOpenOrClose(micPosition, true)) :
                                    IMButtonItemFactory.createLockMicItem(micPosition,
                                            () -> operateMicroOpenOrClose(micPosition, false)));
                        }
                    } else {
                        buttonItems.add(new ButtonItem("送礼物",
                                () -> {
                                    if (getMvpView() != null && roomQueueInfo.mChatRoomMember != null) {
                                        getMvpView().showGiftDialogView(Long.valueOf(roomQueueInfo.mChatRoomMember.getAccount()), true);
                                    }
                                }));
                        buttonItems.add(new ButtonItem("查看资料",
                                () -> {
                                    if (getMvpView() != null && roomQueueInfo.mChatRoomMember != null) {
                                        getMvpView().openUserInfoActivity(Long.valueOf(roomQueueInfo.mChatRoomMember.getAccount()));
                                    }
                                }));
                        //他非房主或管理员
                        if (!isTargetRoomAdmin && !isTargetRoomOwner) {
                            //抱他下麦
                            buttonItems.add(IMButtonItemFactory.createKickDownMicItem(roomQueueInfo.mChatRoomMember.getAccount()));
                            //禁麦/解麦操作
                            if (roomQueueInfo.mRoomMicInfo != null) {
                                buttonItems.add(roomQueueInfo.mRoomMicInfo.isMicMute() ?
                                        IMButtonItemFactory.createFreeMicItem(micPosition,
                                                () -> operateMicroOpenOrClose(micPosition, true)) :
                                        IMButtonItemFactory.createLockMicItem(micPosition,
                                                () -> operateMicroOpenOrClose(micPosition, false)));
                            }
                            //踢出房间
                            buttonItems.add(IMButtonItemFactory.createKickOutRoomItem(mContext, roomQueueInfo.mChatRoomMember,
                                    String.valueOf(getCurrRoomInfo().getRoomId()), roomQueueInfo.mChatRoomMember.getAccount()));
                            //加入黑名单
                            buttonItems.add(IMButtonItemFactory.createMarkBlackListItem(mContext, roomQueueInfo.mChatRoomMember,
                                    String.valueOf(getCurrRoomInfo().getRoomId())));
                        } else {
                            if (roomQueueInfo.mRoomMicInfo != null && !roomQueueInfo.mRoomMicInfo.isMicMute() && !isTargetRoomOwner) {
                                buttonItems.add(IMButtonItemFactory.createLockMicItem(micPosition,
                                        () -> operateMicroOpenOrClose(micPosition, false)));
                            }
                            if (!isTargetRoomOwner) {
                                buttonItems.add(IMButtonItemFactory.createKickDownMicItem(roomQueueInfo.mChatRoomMember.getAccount()));
                            }
                            if (roomQueueInfo.mRoomMicInfo != null && roomQueueInfo.mRoomMicInfo.isMicMute()) {
                                // 对于管理员和房主,只有解麦功能
                                buttonItems.add(IMButtonItemFactory.createFreeMicItem(micPosition,
                                        () -> operateMicroOpenOrClose(micPosition, true)));
                            }
                        }
                    }
                    if (getMvpView() != null && !ListUtils.isListEmpty(buttonItems)) {
                        getMvpView().showCommonPopupDialog(buttonItems, R.string.cancel);
                    }
                } else {
                    //游客操作
                    if (isMyself(roomQueueInfo.mChatRoomMember.getAccount())) {
                        buttonItems.add(new ButtonItem("查看资料",
                                () -> {
                                    if (getMvpView() != null && roomQueueInfo.mChatRoomMember != null) {
                                        getMvpView().showUserInfoDialogView(Long.valueOf(roomQueueInfo.mChatRoomMember.getAccount()));
                                    }
                                }));
                        buttonItems.add(IMButtonItemFactory.createDownMicItem());
                        //下麦旁听
                        if (getMvpView() != null) {
                            getMvpView().showCommonPopupDialog(buttonItems, R.string.cancel);
                        }
                    } else {
                        if (getMvpView() != null && roomQueueInfo.mChatRoomMember != null) {
                            getMvpView().showGiftDialogView(Long.valueOf(roomQueueInfo.mChatRoomMember.getAccount()), true);
                        }
                    }
                }
            } else {
                dealWithUpMicBtnEvent(mContext, roomQueueInfo, micPosition);
            }
        }
    }


    /**
     * 处理麦位没有用户信息的情况下的事件逻辑
     *
     * @param mContext
     * @param roomQueueInfo
     * @param micPosition
     */
    public void dealWithUpMicBtnEvent(Context mContext, IMRoomQueueInfo roomQueueInfo, int micPosition) {
        if (roomQueueInfo.mRoomMicInfo != null) {
            if (isRoomOwnerByMyself() || isRoomAdminByMyself()) {//管理员或者房主
                List<ButtonItem> buttonItems = new ArrayList<>(4);
                buttonItems.add(new ButtonItem(mContext.getString(R.string.embrace_up_mic),
                        () -> {
                            if (getMvpView() != null) {
                                getMvpView().openRoomInviteActivity(micPosition);
                            }
                        }));
                buttonItems.add(new ButtonItem(
                        roomQueueInfo.mRoomMicInfo.isMicMute() ? mContext.getString(R.string.no_forbid_mic)
                                : mContext.getString(R.string.forbid_mic), () -> {
                    operateMicroOpenOrClose(micPosition,
                            roomQueueInfo.mRoomMicInfo.isMicMute());
                }));
                buttonItems.add(
                        new ButtonItem(roomQueueInfo.mRoomMicInfo.isMicLock() ? mContext.getString(R.string.unlock_mic) :
                                mContext.getString(R.string.lock_mic), () -> operateMicroLockOrUnLock(micPosition,
                                !roomQueueInfo.mRoomMicInfo.isMicLock())));
                if (!isRoomOwnerByMyself()) {
                    buttonItems.add(new ButtonItem("移到此座位",
                            () -> operateUpMicro(micPosition, String.valueOf(getCurrentUserId()), false)));
                }
                if (getMvpView() != null) {
                    getMvpView().showCommonPopupDialog(buttonItems, R.string.cancel);
                }
            } else {
                //空坑位的情况，单人房同多人房，需要区别显示弹出菜单
                if (null != getCurrRoomInfo() && (getCurrRoomInfo().getType() == RoomInfo.ROOMTYPE_SINGLE_AUDIO || getCurrRoomInfo().getType() == RoomInfo.ROOMTYPE_NEW_AUCTION)) {
                    //单人房，普通用户点击空白麦位，不做处理
                    return;
                }
                //多人房间，则保持点击空白麦位自动上麦
                operateUpMicro(micPosition, String.valueOf(getCurrentUserId()), false);
            }
        }
    }


    /**
     * //TODO 这里应该可以合并上面的麦位点击事件
     * 树上用户和公屏位置的用户点击事件
     *
     * @param context
     * @param roomInfo
     * @param chatRoomMember
     */
    public void dealWithOnlinePeoleClickEvent(Context context, RoomInfo roomInfo, IMRoomMember chatRoomMember) {
        if (context == null || roomInfo == null || chatRoomMember == null) {
            return;
        }
        //在线列表，点击事件直接内部处理
        String account = chatRoomMember.getAccount();
        if (RoomDataManager.get().isRoomOwner(account) || RoomDataManager.get().isUserSelf(account)) {
            if (getMvpView() != null) {
                getMvpView().showUserInfoDialogView(Long.parseLong(account));
            }
        } else {
            if (TextUtils.isEmpty(account)) {
                return;
            }
            //公屏点击弹框
            final List<ButtonItem> buttonItems = new ArrayList<>();
            //礼物弹框--单人音频房间暂时不展示
            boolean isMyself = RoomDataManager.get().isUserSelf(account);
            boolean isTargetGuess = RoomDataManager.get().isGuess(account);
            boolean isTargetRoomAdmin = RoomDataManager.get().isRoomAdmin(account);
            boolean isTargetRoomOwner = RoomDataManager.get().isRoomOwner(account);
            boolean isTargetOnMic = RoomDataManager.get().isOnMic(account);
            boolean showInviteOnMicItem = roomInfo.getType() != RoomInfo.ROOMTYPE_SINGLE_AUDIO;
            boolean isCurrentAution = RoomDataManager.get().isCurrentAution(account);
            // 房主点击
            if (RoomDataManager.get().isRoomOwner()) {
                //目标用户为房间创建者，其实也就是用户自己
                if (isTargetRoomOwner) {
                    if (getMvpView() != null) {
                        getMvpView().openUserInfoActivity(JavaUtil.str2long(account));
                    }
                } else if (isTargetRoomAdmin || isTargetGuess) {
                    if (showInviteOnMicItem) {
                        //送礼物
                        buttonItems.add(new ButtonItem("送礼物",
                                () -> {
                                    if (getMvpView() != null) {
                                        getMvpView().showGiftDialogView(Long.valueOf(chatRoomMember.getAccount()), true);
                                    }
                                }));
//                        }
                    }
                    // 查看资料
                    buttonItems.add(new ButtonItem("查看资料",
                            () -> {
                                if (getMvpView() != null) {
                                    getMvpView().openUserInfoActivity(Long.valueOf(chatRoomMember.getAccount()));
                                }
                            }));
                    // 抱Ta上麦
                    if (!isTargetOnMic && showInviteOnMicItem) {
                        if (roomInfo.getType() != RoomInfo.ROOMTYPE_NEW_AUCTION) {
                            buttonItems.add(new ButtonItem(context.getString(R.string.embrace_up_mic),
                                    () -> {
                                        int freePosition = RoomDataManager.get().findFreePosition();
                                        if (freePosition == Integer.MIN_VALUE) {
                                            SingleToastUtil.showToast(
                                                    BasicConfig.INSTANCE.getAppContext().getString(R.string.no_empty_mic));
                                            return;
                                        }
                                        if (getMvpView() != null) {
                                            inviteUpMicro(freePosition, JavaUtil.str2long(account));
                                        }
                                    }));
                        } else {//竞拍房单独处理
                            if (isCurrentAution) {
                                buttonItems.add(new ButtonItem(context.getString(R.string.embrace_up_mic),
                                        () -> {
                                            if (RoomDataManager.get().hasMemberInMicPosition(RoomDataManager.AUCTION_ROOM_POSITION)) {
                                                SingleToastUtil.showToast("当前竞拍位已经有人哦！");
                                                return;
                                            }
                                            if (getMvpView() != null) {
                                                inviteUpMicro(RoomDataManager.AUCTION_ROOM_POSITION, JavaUtil.str2long(account));
                                            }
                                        }));
                            }
                        }
                    }
                    // 踢出房间
                    buttonItems.add(IMButtonItemFactory.createKickOutRoomItem(context, chatRoomMember,
                            String.valueOf(roomInfo.getRoomId()), account));
                    // 增加/移除管理员
                    buttonItems.add(IMButtonItemFactory.createMarkManagerListItem(String.valueOf(roomInfo.getRoomId()), account,
                            !isTargetRoomAdmin, chatRoomMember));
                    // 加入黑名单
                    buttonItems.add(IMButtonItemFactory.createMarkBlackListItem(context, chatRoomMember,
                            String.valueOf(roomInfo.getRoomId())));
                }
            } else if (RoomDataManager.get().isRoomAdmin()) {
                if (isMyself) {
                    if (getMvpView() != null) {
                        getMvpView().openUserInfoActivity(JavaUtil.str2long(account));
                    }
                } else if (isTargetRoomAdmin || isTargetRoomOwner) {
                    if (isTargetOnMic) {
                        if (getMvpView() != null) {
                            getMvpView().openUserInfoActivity(JavaUtil.str2long(account));
                        }
                    }
                    if (isTargetRoomOwner) {
                        //送礼物
                        buttonItems.add(new ButtonItem("送礼物",
                                () -> {
                                    if (getMvpView() != null) {
                                        getMvpView().showGiftDialogView(Long.valueOf(chatRoomMember.getAccount()), true);
                                    }
                                }));
                    }
                    // 查看资料
                    buttonItems.add(new ButtonItem("查看资料",
                            () -> {
                                if (getMvpView() != null) {
                                    getMvpView().openUserInfoActivity(Long.valueOf(chatRoomMember.getAccount()));
                                }
                            }));
                    // 抱Ta上麦
                    if (showInviteOnMicItem) {
                        if (roomInfo.getType() != RoomInfo.ROOMTYPE_NEW_AUCTION) {
                            buttonItems.add(new ButtonItem(context.getString(R.string.embrace_up_mic),
                                    () -> {
                                        int freePosition = RoomDataManager.get().findFreePosition();
                                        if (freePosition == Integer.MIN_VALUE) {
                                            SingleToastUtil.showToast(
                                                    BasicConfig.INSTANCE.getAppContext().getString(R.string.no_empty_mic));
                                            return;
                                        }
                                        if (getMvpView() != null) {
                                            inviteUpMicro(freePosition, JavaUtil.str2long(account));
                                        }
                                    }));
                        }else {
                            if (isCurrentAution) {
                                buttonItems.add(new ButtonItem(context.getString(R.string.embrace_up_mic),
                                        () -> {
                                            if (RoomDataManager.get().hasMemberInMicPosition(RoomDataManager.AUCTION_ROOM_POSITION)) {
                                                SingleToastUtil.showToast("当前竞拍位已经有人哦！");
                                                return;
                                            }
                                            if (getMvpView() != null) {
                                                inviteUpMicro(RoomDataManager.AUCTION_ROOM_POSITION, JavaUtil.str2long(account));
                                            }
                                        }));
                            }
                        }
                    }
                } else if (isTargetGuess) {
//                        if (showSendGiftBItem) {
                    //送礼物
                    buttonItems.add(new ButtonItem("送礼物",
                            () -> {
                                if (getMvpView() != null) {
                                    getMvpView().showGiftDialogView(Long.valueOf(chatRoomMember.getAccount()), true);
                                }
                            }));
//                        }
                    // 查看资料
                    buttonItems.add(new ButtonItem("查看资料",
                            () -> {
                                if (getMvpView() != null) {
                                    getMvpView().openUserInfoActivity(Long.valueOf(chatRoomMember.getAccount()));
                                }
                            }));
                    // 抱Ta上麦
                    if (!isTargetOnMic && showInviteOnMicItem) {
                        if (roomInfo.getType() != RoomInfo.ROOMTYPE_NEW_AUCTION) {
                            buttonItems.add(new ButtonItem(context.getString(R.string.embrace_up_mic),
                                    () -> {
                                        int freePosition = RoomDataManager.get().findFreePosition();
                                        if (freePosition == Integer.MIN_VALUE) {
                                            SingleToastUtil.showToast(
                                                    BasicConfig.INSTANCE.getAppContext().getString(R.string.no_empty_mic));
                                            return;
                                        }
                                        if (getMvpView() != null) {
                                            inviteUpMicro(freePosition, JavaUtil.str2long(account));
                                        }
                                    }));
                        } else {

                        }
                    }
                    // 踢出房间
                    buttonItems.add(IMButtonItemFactory.createKickOutRoomItem(context, chatRoomMember,
                            String.valueOf(roomInfo.getRoomId()), account));
                    // 加入黑名单
                    buttonItems.add(IMButtonItemFactory.createMarkBlackListItem(context, chatRoomMember,
                            String.valueOf(roomInfo.getRoomId())));
                }
            } else if (RoomDataManager.get().isGuess()) {
                if ((CoreManager.getCore(IAuthCore.class).getCurrentUid() + "").equals(account)) {
                    if (getMvpView() != null) {
                        getMvpView().showUserInfoDialogView(JavaUtil.str2long(account));
                    }
                } else {
                    if (getMvpView() != null) {
                        getMvpView().showGiftDialogView(Long.valueOf(chatRoomMember.getAccount()), true);
                    }
                }
            }
            if (getMvpView() != null && !ListUtils.isListEmpty(buttonItems)) {
                getMvpView().showCommonPopupDialog(buttonItems, R.string.cancel);
            }
        }
    }


    /**
     * 处理房间多功能按钮的
     */
    public void dealWithRoomMoreOperateEvent() {
        List<ButtonItem> buttonItems = new ArrayList<>();
        if (isRoomOwnerByMyself() || isRoomAdminByMyself()) {
            buttonItems.add(new ButtonItem("最小化", () -> {
                saveMinimizeStatus(true);
                if (getMvpView() != null) {
                    getMvpView().onActivityFinish();
                }
            }));
            buttonItems.add(new ButtonItem("房间设置", () -> {
                if (getMvpView() != null) {
                    getMvpView().openRoomSettingActivity();
                }
            }));
            if (null != getCurrRoomInfo()) {
                int giftEffectSwitch = getCurrRoomInfo().getGiftEffectSwitch();
                //1打开0关闭
                final boolean isShowGiftEffect = giftEffectSwitch == 1;
                String menuStr = isShowGiftEffect ? "打开低价值礼物特效" : "关闭低价值礼物特效";
                buttonItems.add(new ButtonItem(menuStr, () -> {
                    updateRoomGiftEffectSwitch(isShowGiftEffect);
                }));
                String publicScreen = getCurrRoomInfo().getPublicChatSwitch() == 0 ? "关闭房间公屏" : "开启房间公屏";
                buttonItems.add(new ButtonItem(publicScreen, () -> {
                    updateRoomPublicScreenState(getCurrRoomInfo().getPublicChatSwitch() == 0 ? 1 : 0);
                }));
            }
        } else if (!isRoomOwnerByMyself()) {
            buttonItems.add(new ButtonItem("举报房间", () -> {
                if (getCurrRoomInfo() != null && getMvpView() != null) {
                    getMvpView().showReportDialog(2, getCurrRoomInfo().getUid());
                }
            }));
            buttonItems.add(new ButtonItem("分享", () -> {
                if (getMvpView() != null) {
                    getMvpView().showShareDialogView();
                }
            }));
            buttonItems.add(new ButtonItem("最小化", () -> {
                saveMinimizeStatus(true);
                if (getMvpView() != null) {
                    getMvpView().onActivityFinish();
                }
            }));
        }
        buttonItems.add(new ButtonItem("退出房间", () -> {
            exitRoom(null);
            if (getMvpView() != null) {
                getMvpView().onActivityFinish();
            }
        }));
        if (getMvpView() != null && !ListUtils.isListEmpty(buttonItems)) {
            getMvpView().showCommonPopupDialog(buttonItems, R.string.cancel);
        }
    }


    /**
     * 礼物特效开关设置接口
     *
     * @param isShowGiftEffect
     */
    public void updateRoomGiftEffectSwitch(boolean isShowGiftEffect) {
        RoomInfo cRoomInfo = getCurrRoomInfo();
        if (null != cRoomInfo) {
            OkHttpManager.MyCallBack myCallBack = new OkHttpManager.MyCallBack<ServiceResult<RoomInfo>>() {
                @Override
                public void onError(Exception e) {
                    e.printStackTrace();

                }

                @Override
                public void onResponse(ServiceResult<RoomInfo> data) {
                    //低价值礼物特效，开关变更通知
                    IMRoomMessageManager.get().systemNotificationBySdk(
                            CoreManager.getCore(IAuthCore.class).getCurrentUid(),
                            isShowGiftEffect ? CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_OPEN :
                                    CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_CLOSE, -1);
                }
            };
            //调用接口更新
            updateRoomInfo(cRoomInfo.title, cRoomInfo.getRoomDesc(),
                    cRoomInfo.roomPwd, cRoomInfo.getRoomTag(), cRoomInfo.tagId,
                    cRoomInfo.getBackPic(), cRoomInfo.getBackName(),
                    isShowGiftEffect ? 0 : 1, cRoomInfo.getCharmSwitch(),
                    cRoomInfo.getRoomNotice(), cRoomInfo.getPlayInfo(), myCallBack);
        }
    }

    /**
     * 更新房间设置信息
     *
     * @param title
     * @param desc
     * @param pwd
     * @param label            标签名字
     * @param tagId            标签id
     * @param backPic
     * @param giftEffectParams
     */
    private void updateRoomInfo(String title, String desc, String pwd, String label, int tagId,
                                String backPic, String backName, int giftEffectParams,
                                int charmSwitch, String roomNotice, String playInfo,
                                OkHttpManager.MyCallBack<ServiceResult<RoomInfo>> myCallBack) {
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if ("".equals(title)) {
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(uid);
            if (userInfo != null) {
                title = userInfo.getNick() + "的房间";
            }
        }

        new RoomSettingModel().updateRoomInfo(title, desc, pwd, label, tagId, uid,
                CoreManager.getCore(IAuthCore.class).getTicket(), backPic, backName,
                giftEffectParams, charmSwitch, roomNotice, playInfo, myCallBack);

    }


    /**
     * 进房接口调用 -- 用于统计
     */
    public void reportUserRoomIn() {
        RoomInfo roomInfo = RoomServiceScheduler.getCurrentRoomInfo();
        if (roomInfo != null) {
            new AvRoomModel().userRoomIn(String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()), roomInfo.getRoomId(), new OkHttpManager.MyCallBack<Json>() {
                @Override
                public void onError(Exception e) {
                    if (null != e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onResponse(Json response) {
                }
            }, roomInfo.getType());
        }
    }

    /**
     * 更新公屏消息开关
     *
     * @param publicChatSwitch
     */
    public void updateRoomPublicScreenState(final int publicChatSwitch) {
        new RoomSettingModel().changeRoomPublicScreenState(publicChatSwitch, getCurrentUserTicket(), getCurrentUserId(), new CallBack<String>() {
            @Override
            public void onSuccess(String data) {
                if (publicChatSwitch == 0) {
                    IMRoomMessageManager.get().systemNotificationBySdk(getCurrentUserId(), CustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_OPEN, -1);
                } else {
                    IMRoomMessageManager.get().systemNotificationBySdk(getCurrentUserId(), CustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_CLOSE, -1);
                }
            }

            @Override
            public void onFail(int code, String error) {
                SingleToastUtil.showToast(error);
            }
        });
    }

    /**
     * 获取活动信息
     */
    public void getRoomActionList() {
        OkHttpManager.MyCallBack myCallBack = new OkHttpManager.MyCallBack<ServiceResult<List<ActionDialogInfo>>>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (!TextUtils.isEmpty(e.getMessage())) {
                    LogUtils.d(BaseRoomDetailPresenter.class.getSimpleName(), "getRoomActionList-->onError e.msg:" + e.getMessage());
                }
                if (getMvpView() != null) {
                    getMvpView().showRoomActionBanner(null);
                }
            }

            @Override
            public void onResponse(ServiceResult<List<ActionDialogInfo>> data) {
                if (null == getMvpView()) {
                    return;
                }
                if (null != data && data.isSuccess()) {
                    getMvpView().showRoomActionBanner(data.getData());
                } else {
                    getMvpView().showRoomActionBanner(null);
                }
            }
        };
        new AvRoomModel().getActionDialog(2, myCallBack);
    }

    //-----------------------------------礼物连击--------------------------------------------------

    public BaseRoomDetailPresenter() {
        super();
        model = new IMRoomModel();
        roomGiftModel = new RoomGiftModel();
    }

//    /**
//     * 新版本给单人赠送礼物
//     *
//     * @param giftInfo
//     * @param targetUid
//     * @param roomUid
//     * @param giftNum
//     */
//    public void sendSingleGift(GiftInfo giftInfo, long targetUid, long roomUid, long roomId, final int giftNum,
//                               long comboId, int comboRangStart, int comboRangEnd, CallBack<GiftReceiveInfo> callBack) {
//        roomGiftModel.sendRoomSingleRoomGift(giftInfo, targetUid, roomUid, roomId, giftNum, comboId, comboRangStart, comboRangEnd, new CallBack<GiftReceiveInfo>() {
//            @Override
//            public void onSuccess(GiftReceiveInfo data) {
//                if (getMvpView() != null) {
//                    if (data != null) {
//                        if (data.getIsPea() == 1) {
//                            //甜豆礼物
//                            CoreManager.getCore(IPayCore.class).minusDouzi(data.getUsePeaNum());
//                            getMvpView().sendGiftUpdateMinusBeanCount(data.getUsePeaNum());
//                        } else {
//                            //金币礼物
//                            CoreManager.getCore(IPayCore.class).minusGold(data.getUseGiftPurseGold());
//                            getMvpView().sendGiftUpdateMinusGoldCount(data.getUseGiftPurseGold());
//                        }
//                        if (giftInfo.getUserGiftPurseNum() != data.getUserGiftPurseNum()) {
//                            giftInfo.setUserGiftPurseNum(data.getUserGiftPurseNum());
//                            getMvpView().sendGiftUpdatePacketGiftCount(giftInfo);
//                        }
//                    }
//                }
//                if (null != callBack) {
//                    callBack.onSuccess(data);
//                }
//            }
//
//            @Override
//            public void onFail(int code, String error) {
//                if (!TextUtils.isEmpty(error)) {
//                    SingleToastUtil.showToast(error);
//                }
//                if (null != callBack) {
//                    callBack.onFail(code, error);
//                }
//            }
//        });
//    }
//
//
//    /**
//     * 新版本给全麦赠送礼物
//     *
//     * @param roomUid
//     * @param giftInfo
//     * @param micAvatar
//     * @param giftNum
//     */
//    public void sendRoomMultiGift(GiftInfo giftInfo, long roomUid, long roomId, List<MicMemberInfo> micAvatar, final int giftNum,
//                                  long comboId, int comboRangStart, int comboRangEnd, CallBack<MultiGiftReceiveInfo> callBack) {
//        roomGiftModel.sendRoomMultiGift(giftInfo, roomUid, roomId, micAvatar, giftNum, comboId, comboRangStart, comboRangEnd, new CallBack<MultiGiftReceiveInfo>() {
//            @Override
//            public void onSuccess(MultiGiftReceiveInfo data) {
//                if (getMvpView() != null) {
//                    if (data != null) {
//                        if (data.getIsPea() == 1) {
//                            //甜豆礼物
//                            CoreManager.getCore(IPayCore.class).minusDouzi(data.getUsePeaNum());
//                            getMvpView().sendGiftUpdateMinusBeanCount(data.getUsePeaNum());
//                        } else {
//                            //金币礼物
//                            CoreManager.getCore(IPayCore.class).minusGold(data.getUseGiftPurseGold());
//                            getMvpView().sendGiftUpdateMinusGoldCount(data.getUseGiftPurseGold());
//                        }
//                        if (giftInfo.getUserGiftPurseNum() != data.getUserGiftPurseNum()) {
//                            giftInfo.setUserGiftPurseNum(data.getUserGiftPurseNum());
//                            getMvpView().sendGiftUpdatePacketGiftCount(giftInfo);
//                        }
//                    }
//                }
//                if (null != callBack) {
//                    callBack.onSuccess(data);
//                }
//            }
//
//            @Override
//            public void onFail(int code, String error) {
//                if (!TextUtils.isEmpty(error)) {
//                    SingleToastUtil.showToast(error);
//                }
//                if (null != callBack) {
//                    callBack.onFail(code, error);
//                }
//            }
//        });
//    }
}
