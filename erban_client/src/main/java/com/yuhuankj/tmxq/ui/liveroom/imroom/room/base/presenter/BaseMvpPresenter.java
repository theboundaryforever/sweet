package com.yuhuankj.tmxq.ui.liveroom.imroom.room.base.presenter;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.xchat_core.auth.IAuthCore;

import java.util.Objects;

/**
 * 基础的presenter
 *
 * @author zeda
 */
public class BaseMvpPresenter<V extends IMvpBaseView> extends AbstractMvpPresenter<V> {

    public long getCurrentUserId() {
        return CoreManager.getCore(IAuthCore.class).getCurrentUid();
    }


    public String getCurrentUserTicket() {
        return CoreManager.getCore(IAuthCore.class).getTicket();
    }

    /**
     * 此uid是否是我自己
     */
    public boolean isMyself(String currentUid) {
        return Objects.equals(currentUid, String.valueOf(getCurrentUserId()));
    }
}
