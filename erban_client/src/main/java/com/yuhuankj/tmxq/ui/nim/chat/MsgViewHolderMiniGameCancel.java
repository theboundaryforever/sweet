package com.yuhuankj.tmxq.ui.nim.chat;

import com.netease.nim.uikit.common.ui.recyclerview.adapter.BaseMultiItemFetchLoadAdapter;
import com.netease.nim.uikit.session.viewholder.MsgViewHolderBase;
import com.tongdaxing.xchat_core.bean.MiniGameCancel;
import com.tongdaxing.xchat_core.im.custom.bean.MiniGameCancelAttachment;

/**
 * 小游戏取消消息
 */
public class MsgViewHolderMiniGameCancel extends MsgViewHolderBase {

    public MsgViewHolderMiniGameCancel(BaseMultiItemFetchLoadAdapter adapter) {
        super(adapter);
    }

    @Override
    protected int getContentResId() {
        return com.netease.nim.uikit.R.layout.view_minigame_msg_gone;
    }

    @Override
    protected void inflateContentView() {
    }

    @Override
    protected boolean isShowHeadImage() {
        return false;
    }

    @Override
    protected boolean isShowBubble() {
        return false;
    }

    @Override
    protected void bindContentView() {
        MiniGameCancel info = ((MiniGameCancelAttachment) message.getAttachment()).getDataInfo();
        if (info != null) {
            MsgViewHolderMiniGameInvited gameInvited = MsgViewHolderMiniGameInvited.get(info.getRoomid());
            if (gameInvited != null) {
                gameInvited.setEndStatus(MsgViewHolderMiniGameInvited.CANCEL, "游戏被取消", "失效");
            }
        }
    }
}
