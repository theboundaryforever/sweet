package com.yuhuankj.tmxq.ui.share;

import android.graphics.Color;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.makeramen.roundedimageview.RoundedImageView;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.widget.LevelView;

import java.util.List;


public class ShareFansAdapter extends BaseQuickAdapter<Json, ShareFansAdapter.ViewHolder> {
    private boolean moreOption = false;

    public Json sendHistory = new Json();

    public boolean isMoreOption() {
        return moreOption;
    }

    public void setMoreOption(boolean moreOption) {
        this.moreOption = moreOption;
        notifyDataSetChanged();
    }

    public ShareFansAdapter(@Nullable List<Json> data) {
        super(R.layout.list_item_share_fans, data);
    }

    @Override
    protected void convert(ViewHolder viewHolder, Json json) {

        ImageLoadUtils.loadImage(mContext, json.str("avatar"), viewHolder.imageView);
        String nick = json.str("nick");
        if (!TextUtils.isEmpty(nick) && nick.length() > 8) {
            nick = nick.substring(0, 8);
        }
        viewHolder.tvItemName.setText(nick);

        viewHolder.levelViewNewUserList.setExperLevel(json.num("experLevel"));

        int hasSend = sendHistory.num(json.str("uid"));
        boolean moreOption = this.moreOption && hasSend != 1;
        viewHolder.buInvite.setVisibility(moreOption ? View.GONE : View.VISIBLE);
        viewHolder.ivShareFansOption.setVisibility(!moreOption ? View.GONE : View.VISIBLE);
        if (hasSend == 1) {
            viewHolder.buInvite.setText("已邀请");
            viewHolder.buInvite.setTextColor(Color.parseColor("#666666"));
            viewHolder.buInvite.setBackground(null);
        } else {
            viewHolder.buInvite.setText("邀请");
            viewHolder.buInvite.setTextColor(Color.WHITE);
            viewHolder.buInvite.setBackgroundResource(R.drawable.shape_car_pay);
        }

        boolean select = json.boo("select");
        viewHolder.ivShareFansOption.setImageResource(select ? R.drawable.share_fans_option_select : R.drawable.share_fans_option_unselect);
        long uid = json.num_l("uid");
        viewHolder.buInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemAction != null) {
                    if (hasSend != 1)
                        itemAction.itemClickAction(uid);
                }

            }
        });
        viewHolder.ivShareFansOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                json.set("select", !json.boo("select"));
                notifyDataSetChanged();

            }
        });

    }


    public ItemAction itemAction;

    public interface ItemAction {
        void itemClickAction(long uid);
    }

    public class ViewHolder extends BaseViewHolder {

        //        @BindView(R.id.imageView)
        RoundedImageView imageView;
        //        @BindView(R.id.tv_item_name)
        TextView tvItemName;
        //        @BindView(R.id.level_view_new_user_list)
        LevelView levelViewNewUserList;
        //        @BindView(R.id.iv_new_user_item_sex)
//        ImageView ivNewUserItemSex;
//        @BindView(R.id.tv_new_user_item_age)
//        TextView tvNewUserItemAge;
//        @BindView(R.id.top_line)
//        View topLine;
//        @BindView(R.id.bu_invite)
        Button buInvite;
        //        @BindView(R.id.iv_share_fans_option)
        ImageView ivShareFansOption;
//        @BindView(R.id.container)
//        RelativeLayout container;

        public ViewHolder(View view) {
            super(view);
//            ButterKnife.bind(this, view);
            imageView = view.findViewById(R.id.imageView);
            tvItemName = view.findViewById(R.id.tv_item_name);
            buInvite = view.findViewById(R.id.bu_invite);
            ivShareFansOption = view.findViewById(R.id.iv_share_fans_option);
            levelViewNewUserList = view.findViewById(R.id.level_view_new_user_list);
        }
    }


}
