package com.yuhuankj.tmxq.ui.liveroom.imroom.room.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.ConveneUserInfo;
import com.tongdaxing.xchat_core.praise.IPraiseClient;
import com.tongdaxing.xchat_core.praise.IPraiseCore;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter.RoomCallUpUserAdapter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter.RoomCallUpUserListPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IConeveUserListView;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;

import java.util.List;

/**
 * 黑名单
 *
 * @author chenran
 * @date 2017/10/11
 */
@CreatePresenter(RoomCallUpUserListPresenter.class)
public class RoomCallUpUserListActivity extends BaseMvpActivity<IConeveUserListView, RoomCallUpUserListPresenter>
        implements IConeveUserListView, RoomCallUpUserAdapter.OnConeveUserClickListener {

    private final String TAG = RoomCallUpUserListActivity.class.getSimpleName();

    private TextView tvCancelCallUp;
    private RecyclerView rvConveneUser;
    private RoomCallUpUserAdapter roomCallUpUserAdapter;
    private int loadSize = 20;
    private int start = 0;
    private long lastOperaUid = 0L;
    private boolean isVisiable = false;
    private Runnable runnable;

    public static void start(Context context) {
        Intent intent = new Intent(context, RoomCallUpUserListActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_call_up_member_list);
        initTitleBar("已召集成员(0)");
        initView();
        LogUtils.d(TAG, "onCreate-loadData");
        getDialogManager().showProgressDialog(this, getResources().getString(R.string.network_loading));
        start = 1;
        //一次最多只能加载20条
        getMvpPresenter().getConveneUserList(start, loadSize);
    }

    private void initView() {
        tvCancelCallUp = (TextView) findViewById(R.id.tvCancelCallUp);
        rvConveneUser = (RecyclerView) findViewById(R.id.rvConveneUser);
        roomCallUpUserAdapter = new RoomCallUpUserAdapter();
        roomCallUpUserAdapter.setOnConeveUserClickListener(this);
        roomCallUpUserAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {

            @Override
            public void onLoadMoreRequested() {
                LogUtils.d(TAG, "initView-->onLoadMoreRequested-->getConveneUserList");
                start += 1;
                getDialogManager().showProgressDialog(RoomCallUpUserListActivity.this, getResources().getString(R.string.network_loading));
                getMvpPresenter().getConveneUserList(start, loadSize);
            }
        }, rvConveneUser);
        rvConveneUser.setAdapter(roomCallUpUserAdapter);
        roomCallUpUserAdapter.setEnableLoadMore(false);
        tvCancelCallUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialogManager().showOkCancelDialog(getResources().getString(R.string.room_call_up_tips_6),
                        getResources().getString(R.string.sure),
                        getResources().getString(R.string.cancel), new DialogManager.OkCancelDialogListener() {

                            @Override
                            public void onCancel() {
                            }

                            @Override
                            public void onOk() {
                                getDialogManager().showProgressDialog(RoomCallUpUserListActivity.this,
                                        RoomCallUpUserListActivity.this.getResources().getString(R.string.network_loading));
                                getMvpPresenter().cancelCallUp();
                            }
                        });
            }
        });
        tvCancelCallUp.setVisibility(RoomDataManager.get().isRoomOwner() ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isVisiable = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        isVisiable = false;
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraise(long likedUid) {
        List<ConveneUserInfo> conveneUserInfoList = roomCallUpUserAdapter.getData();
        if (!ListUtils.isListEmpty(conveneUserInfoList)) {
            ConveneUserInfo conveneUserInfo = new ConveneUserInfo();
            conveneUserInfo.setUid(likedUid);
            int index = conveneUserInfoList.indexOf(conveneUserInfo);
            if (-1 != index) {
                conveneUserInfo = conveneUserInfoList.get(index);
                conveneUserInfo.setFans(true);
                conveneUserInfoList.set(index, conveneUserInfo);
                if (null != runnable) {
                    rvConveneUser.removeCallbacks(runnable);
                }
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        roomCallUpUserAdapter.notifyItemChanged(index);
                    }
                };
                rvConveneUser.post(runnable);
            }
        }
        getDialogManager().dismissDialog();
        lastOperaUid = 0L;
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onCanceledPraise(long likedUid) {
        //现在依据valid字段来标识该用户是否在线，因此修改该字段的逻辑全部删除
        List<ConveneUserInfo> conveneUserInfoList = roomCallUpUserAdapter.getData();
        if (!ListUtils.isListEmpty(conveneUserInfoList)) {
            ConveneUserInfo conveneUserInfo = new ConveneUserInfo();
            conveneUserInfo.setUid(likedUid);
            int index = conveneUserInfoList.indexOf(conveneUserInfo);
            if (-1 != index) {
                conveneUserInfo = conveneUserInfoList.get(index);
                conveneUserInfo.setFans(false);
                conveneUserInfoList.set(index, conveneUserInfo);
                if (null != runnable) {
                    rvConveneUser.removeCallbacks(runnable);
                }
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        roomCallUpUserAdapter.notifyItemChanged(index);
                    }
                };
                rvConveneUser.post(runnable);
            }
        }
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraiseFaith(String error) {
        if (isVisiable && !TextUtils.isEmpty(error)) {
            toast(error);
        }
        getDialogManager().dismissDialog();
        lastOperaUid = 0L;
    }

    @Override
    public void onClickToOperaAttention(long uid) {
        lastOperaUid = uid;
        getDialogManager().showProgressDialog(this, getResources().getString(R.string.network_loading));
        CoreManager.getCore(IPraiseCore.class).praise(uid);
    }

    @Override
    public void onClickToViewUserInfo(long uid) {
        UserInfoActivity.start(this, uid);
    }

    @Override
    public void refreshConeveUserList(List<ConveneUserInfo> conveneUserInfos) {
        getDialogManager().dismissDialog();
        if (null != conveneUserInfos) {
            roomCallUpUserAdapter.loadMoreComplete();
            roomCallUpUserAdapter.setEnableLoadMore(true);
            if (conveneUserInfos.size() < loadSize) {
                roomCallUpUserAdapter.loadMoreEnd();
                roomCallUpUserAdapter.setEnableLoadMore(false);
            }
        }
        if (!ListUtils.isListEmpty(conveneUserInfos)) {
            if (null != runnable) {
                rvConveneUser.removeCallbacks(runnable);
            }
            runnable = new Runnable() {
                @Override
                public void run() {
                    if (0 == start) {
                        roomCallUpUserAdapter.setNewData(conveneUserInfos);
                    } else {
                        roomCallUpUserAdapter.addData(conveneUserInfos);
                    }
                    if (null != mTitleBar) {
                        if (null == roomCallUpUserAdapter.getData()) {
                            mTitleBar.setTitle("已召集成员(0)");
                        } else {
                            mTitleBar.setTitle("已召集成员(".concat(String.valueOf(roomCallUpUserAdapter.getData().size())).concat(")"));
                        }
                    }
                }
            };
            rvConveneUser.post(runnable);
        } else {
            start -= 1;
            if (start <= 0) {
                start = 0;
            }
        }
    }

    @Override
    public void showErrMsg(String msg) {
        start -= 1;
        if (start <= 0) {
            start = 0;
        }
        roomCallUpUserAdapter.loadMoreEnd();
        getDialogManager().dismissDialog();
        if (!TextUtils.isEmpty(msg)) {
            toast(msg);
        }
    }

    @Override
    public void finishAfterCancelCallUp() {
        getDialogManager().dismissDialog();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != rvConveneUser && null != runnable) {
            rvConveneUser.removeCallbacks(runnable);
        }
    }
}
