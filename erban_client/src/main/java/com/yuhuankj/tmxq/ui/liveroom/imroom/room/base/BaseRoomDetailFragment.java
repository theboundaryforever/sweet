package com.yuhuankj.tmxq.ui.liveroom.imroom.room.base;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.View;
import android.view.animation.Animation;
import android.widget.TextView;

import com.jude.rollviewpager.hintview.ColorPointHintView;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.im.IMReportRoute;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.ButtonUtils;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.codec.DESUtils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.ReUsedSocketManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomEvent;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMember;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMemberComeInfo;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMessage;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomQueueInfo;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.manager.RtcEngineManager;
import com.tongdaxing.xchat_core.pay.IPayCore;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_core.praise.IPraiseClient;
import com.tongdaxing.xchat_core.praise.IPraiseCore;
import com.tongdaxing.xchat_core.redpacket.bean.ActionDialogInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.face.FaceReceiveInfo;
import com.tongdaxing.xchat_core.room.face.IFaceCoreClient;
import com.tongdaxing.xchat_core.share.IShareCore;
import com.tongdaxing.xchat_core.share.IShareCoreClient;
import com.tongdaxing.xchat_core.utils.ChatUtil;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.base.fragment.BaseMvpFragment;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.thirdsdk.nim.SessionHelper;
import com.yuhuankj.tmxq.ui.liveroom.imroom.RoomFrameActivity;
import com.yuhuankj.tmxq.ui.liveroom.imroom.gift.IRoomGiftView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.gift.RoomGiftDialog;
import com.yuhuankj.tmxq.ui.liveroom.imroom.publicscreen.MessageView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.activity.IMRoomInviteActivity;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter.RoomBannerAdapter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.base.presenter.BaseRoomDetailPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.dialog.IMRoomMemberListDialog;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.fragment.IMRoomOnlineMemberFragment;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.listener.AnimationListener;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.listener.OnMicroItemClickListener;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IRoomDetailView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.AbstractInputMsgView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.AbstractMicroView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.ComingMsgView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.RoomAdmireView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.RoomBannerShapeHintView;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.activity.RoomSettingActivity;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.box.LotteryBoxDialog;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.callback.BottomViewListenerWrapper;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.callback.SoftKeyBoardListener;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.widget.AbstractBottomView;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.widget.GiftV2View;
import com.yuhuankj.tmxq.ui.me.charge.ChargeActivity;
import com.yuhuankj.tmxq.ui.share.ShareDialog;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;
import com.yuhuankj.tmxq.ui.widget.BigListDataDialog;
import com.yuhuankj.tmxq.ui.widget.UserInfoDialog;
import com.yuhuankj.tmxq.widget.Banner;
import com.zego.zegoavkit2.soundlevel.ZegoSoundLevelInfo;

import java.util.ArrayList;
import java.util.List;

import cn.sharesdk.framework.Platform;

import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_CLOSE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_OPEN;


/**
 * 房间fragment基类
 *
 * @author zeda
 */
@CreatePresenter(BaseRoomDetailPresenter.class)
public abstract class BaseRoomDetailFragment<V extends IRoomDetailView, P extends BaseRoomDetailPresenter<V>, T extends AbstractMicroView>
        extends BaseMvpFragment<V, P> implements IRoomDetailView, BottomViewListenerWrapper, IRoomGiftView, ComingMsgView.SvgaShowListener {

    /**
     * 是否第一次进入该房间
     */
    protected boolean isFristEnterRoom = true;
    /**
     * 防止外部新消息回调过来这边正在执行动画影响显示
     */
    private boolean isShowing;

    protected abstract @NonNull
    T getMicroView();

    protected abstract @NonNull
    MessageView getMessageView();

    protected abstract @NonNull
    GiftV2View getGiftView();

    protected abstract @NonNull
    AbstractBottomView getBottomView();

//    protected abstract RoomGiftComboSender getRoomGiftComboSenderView();
//
//    protected abstract ComboGiftView getComboGiftView();

    protected abstract TextView getFollowRoomView();

    protected abstract @NonNull
    ComingMsgView getComingMsgView();

    protected abstract RoomAdmireView getRoomAdmireView();

    protected RoomBannerAdapter roomBannerAdapter;

    protected abstract Banner getBanner();

    protected abstract @NonNull
    AbstractInputMsgView getInputMsgView();

    protected abstract @NonNull
    View getMoreOperateBtn();

    public abstract OnMicroItemClickListener getOnMicroItemClickListener();

    public abstract void showRoomMoreOperateDialog();

    protected abstract void refreshOnlineCount(int onlineNum);

    //声网声浪监听
    private void onSpeakQueueUpdate(List<Integer> micPositionList) {
        //返回的Integer -1 - 7对应：房主和1-8麦位有值代表有声波
        if (!ListUtils.isListEmpty(micPositionList)) {
            getMicroView().onSpeakQueueUpdate(micPositionList);
        }
    }

    //即构其他人的声浪监听
    private void onZegoSpeakQueueUpdate(List<ZegoSoundLevelInfo> speakers) {
        if (!ListUtils.isListEmpty(speakers)) {
            getMicroView().onZegoSpeakQueueUpdate(speakers);
        }
    }

    //即构自己的声浪监听
    private void onCurrentSpeakUpdate(int currentMicPosition, float currentMicStreamLevel) {
        getMicroView().onCurrentSpeakUpdate(currentMicPosition, currentMicStreamLevel);
    }

    private SoftKeyBoardListener.OnSoftKeyBoardChangeListener onSoftKeyBoardChangeListener;
    /**
     * 公屏消息内容点击事件监听
     */
    private MessageView.OnMsgContentClickListener onMsgContentClickListener = new MessageView.OnMsgContentClickListener() {
        @Override
        public void clickToOpenUserInfoDialog(long uid) {
            if (uid > 0) {
                showUserOperateDialog(uid);
            }
        }

        @Override
        public void onUserOperate(long uid, String nick) {
            showUserOperateDialog(uid, nick);
        }

        @Override
        public void clickToAttentionRoomOwner(boolean attentionOrCancel) {
            if (null == getMvpPresenter().getCurrRoomInfo()) {
                return;
            }
            //考虑到关注、取消关注，在个人资料页或者个人资料弹框中操作，需要影响到房间公屏消息，因此暂时保留coremanager的这一套逻辑
            if (attentionOrCancel) {
                CoreManager.getCore(IPraiseCore.class).praise(getMvpPresenter().getCurrRoomInfo().getUid());
            } else {
                CoreManager.getCore(IPraiseCore.class).cancelPraise(getMvpPresenter().getCurrRoomInfo().getUid());
            }
        }

        @Override
        public void clickToAttentionRoom(boolean attentionOrCancel) {
            //关注或者取消关注房间
            if (null == getMvpPresenter().getCurrRoomInfo()) {
                return;
            }
            if (attentionOrCancel) {
                getMvpPresenter().doRoomAttention();
            } else {
                getMvpPresenter().doRoomCancelAttention();
            }
        }
    };

    @Override
    public void onFindViews() {
//        if (null != getComboGiftView() && CoreManager.getCore(VersionsCore.class).isGiftComboSwitchOpen()) {
//            getGiftView().setComboGiftView(getComboGiftView());
//        }
    }

    @Override
    public void onSetListener() {
        //房间麦位点击事件
        getMicroView().setOnMicroItemClickListener(getOnMicroItemClickListener());

        //房间底部输入框发送按钮
        getInputMsgView().setOnSendBtnClickListener(inputContent -> {
            //发送消息
            getMvpPresenter().sendTextMsg(inputContent);
            getInputMsgView().setInputText("");
        });

        //房间底部操作栏
        getBottomView().setBottomViewListener(this);

        //房间更多操作按钮
        getMoreOperateBtn().setOnClickListener(v -> showRoomMoreOperateDialog());

        //关注房间
        if (null != getFollowRoomView()) {
            getFollowRoomView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getMvpPresenter().switchRoomAttentionStatus();

                    //个人房，右上角，关注按钮，点击统计
                    if (null != getActivity() && null != getMvpPresenter().getCurrRoomInfo()
                            && getMvpPresenter().getCurrRoomInfo().getType() == RoomInfo.ROOMTYPE_SINGLE_AUDIO
                            && !getMvpPresenter().hasAttentionRoomAlready) {
                        StatisticManager.get().onEvent(getActivity(),
                                StatisticModel.EVENT_ID_ROOM_ATTENTION_CLICK,
                                StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
                    }
                }
            });
        }

        if (getMessageView().getAdapter() != null) {
            getMessageView().getAdapter().setOnMsgContentClickListener(onMsgContentClickListener);
        }
        getComingMsgView().setSvgaListener(this);
        onSoftKeyBoardChangeListener = new SoftKeyBoardListener.OnSoftKeyBoardChangeListener() {
            @Override
            public void keyBoardShow(int height) {
                RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
                if (null != roomInfo && roomInfo.getType() == RoomInfo.ROOMTYPE_MULTI_AUDIO && null != getActivity()) {
                    height -= getActivity().getResources().getDimension(R.dimen.room_multi_indic_height);
                }
                if (null != getInputMsgView() && null != getInputMsgView().getOnSoftKeyBoardChangeListener()) {
                    getInputMsgView().getOnSoftKeyBoardChangeListener().keyBoardShow(height);
                }
                if (null != getMessageView() /*&& null != getMessageView().getOnSoftKeyBoardChangeListener()*/) {
                    getMessageView()./*getOnSoftKeyBoardChangeListener().*/keyBoardShow(height);
                }
            }

            @Override
            public void keyBoardHide(int height) {
                if (null != getInputMsgView() && null != getInputMsgView().getOnSoftKeyBoardChangeListener()) {
                    getInputMsgView().getOnSoftKeyBoardChangeListener().keyBoardHide(height);
                }
                if (null != getMessageView() /*&& null != getMessageView().getOnSoftKeyBoardChangeListener()*/) {
                    getMessageView()./*getOnSoftKeyBoardChangeListener().*/keyBoardHide(height);
                }
            }
        };
        SoftKeyBoardListener.setListener(getActivity(), onSoftKeyBoardChangeListener);

//        if (null != getRoomGiftComboSenderView() && CoreManager.getCore(VersionsCore.class).isGiftComboSwitchOpen()) {
//            getRoomGiftComboSenderView().setOnComboClickListener(new RoomGiftComboSender.OnComboClickListener() {
//                @Override
//                public void onComboClicked() {
//
//                    if (null == getRoomGiftComboSenderView().getGiftInfo()) {
//                        return;
//                    }
//                    if (null == RoomDataManager.get().getCurrentRoomInfo()) {
//                        return;
//                    }
//                    //连击延迟100对最后的统计消息发送的影响忽略
//                    ThreadUtil.runInUIThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            LogUtils.d(TAG, "onComboClicked-->run");
//                            List<MicMemberInfo> micAvatar = getRoomGiftComboSenderView().getMicAvatar();
//                            long targetUid = getRoomGiftComboSenderView().getTargetUid();
//                            if (null == micAvatar || micAvatar.size() == 0) {
//                                //单人赠送
//                                if (targetUid <= 0) {
//                                    return;
//                                }
//                                LogUtils.d(TAG, "onComboClicked-->run-->sendSingleGift");
//                                sendSingleGift(getRoomGiftComboSenderView().getGiftInfo(), targetUid,
//                                        RoomDataManager.get().getCurrentRoomInfo().getUid(),
//                                        RoomDataManager.get().getCurrentRoomInfo().getRoomId(),
//                                        getRoomGiftComboSenderView().getLastSendCount(), true);
//                            } else {
//                                LogUtils.d(TAG, "onComboClicked-->run-->sendRoomMultiGift");
//                                sendRoomMultiGift(getRoomGiftComboSenderView().getGiftInfo(),
//                                        RoomDataManager.get().getCurrentRoomInfo().getUid(),
//                                        RoomDataManager.get().getCurrentRoomInfo().getRoomId(),
//                                        micAvatar, getRoomGiftComboSenderView().getLastSendCount(),
//                                        true, !getRoomGiftComboSenderView().isMultiAndNoAllMic());
//                            }
//                        }
//                    }, 100L);
//                }
//            });

//        }
    }

//    public void hideComBoSenderViewAfterShowGiftDialog() {
//        if (null != getRoomGiftComboSenderView() && CoreManager.getCore(VersionsCore.class).isGiftComboSwitchOpen()) {
//            getRoomGiftComboSenderView().clear();
//            getRoomGiftComboSenderView().setVisibility(View.GONE);
//            ComboGiftPublicScreenMsgSender.getInstance().sendComboGiftScreenMsg();
//        }
//    }

    @Override
    public void initiate() {
        getMvpPresenter().saveMinimizeStatus(false);
        subscribeChatRoomEventObservable();
        subscribeChatRoomMsgObservable();
        //更新礼物信息
        CoreManager.getCore(IGiftCore.class).requestGiftInfos();
        //获取房间活动
        getMvpPresenter().getActivityInfo();
        if (getMvpPresenter().getCurrRoomInfo() != null) {//最小化或者房间信息不为空
            initRoomViewFromRoomInfo(getMvpPresenter().getCurrRoomInfo(), false, false, true);
            //查询是否已经关注房间
            if (!RoomDataManager.get().isRoomOwner()) {
                getMvpPresenter().getCheckRoomAttention();
            }
            dealUserComingMsg();
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (getMvpPresenter().getCurrRoomInfo() != null) {
            initRoomViewFromRoomInfo(getMvpPresenter().getCurrRoomInfo(), false, false, true);
            dealUserComingMsg();
        } else {
            toast("进房失败，请稍后重试");
            onActivityFinish();
        }
    }

    /**
     * socket连接成功显示房间的麦位和房间消息列表
     */
    protected abstract void onRoomInSucShowMicroAndMessageView();

    /**
     * 更新房间信息相关的view
     *
     * @param roomInfo
     */
    protected abstract void updateRoomInfoAboutView(RoomInfo roomInfo);

    /**
     * 声网或者其他第三方音视频SDK初始化成功显示底部功能布局
     */
    protected abstract void onRoomAudioInitSucShowBottomView();

    /**
     * 初始化房间状态
     *
     * @param roomInfo
     */
    public void initRoomViewFromRoomInfo(RoomInfo roomInfo, boolean roomInfoUpdate, boolean hasAnim, boolean refreshOnlineNum) {
        if (getActivity() instanceof RoomFrameActivity) {
            ((RoomFrameActivity) getActivity()).setupRoomBg(roomInfo, hasAnim);
        }
        if (!roomInfoUpdate) {
            onRoomInSucShowMicroAndMessageView();
        }
        if (refreshOnlineNum) {
            refreshOnlineCount(roomInfo.onlineNum);
        }
        updateRoomInfoAboutView(roomInfo);
        if (!roomInfoUpdate) {
            onRoomDynamicInitView();
        }
        refreshMicroView(-2, RoomEvent.NONE);
        refreshBottomView();
    }

    /**
     * 监听房间事件
     */
    public void subscribeChatRoomEventObservable() {
        IMRoomMessageManager.get().subscribeIMRoomEventObservable(this::receiveRoomEvent, this);
    }

    @Override
    public void onPause() {
        super.onPause();
        LogUtils.d(TAG, "onPause");
        if (null != onSoftKeyBoardChangeListener) {
            onSoftKeyBoardChangeListener.keyBoardHide(0);
        }
        getInputMsgView().clearFocusAndInput();
        //界面不可见时不处理特效和进场动画
        RoomDataManager.get().setMinimize(true);
    }

    /**
     * 房间异步配置的view初始化调用位置
     */
    protected void onRoomDynamicInitView() {
        //调用进房接口
        getMvpPresenter().reportUserRoomIn();
        //更新礼物
        CoreManager.getCore(IGiftCore.class).requestGiftInfos();
    }


    /**
     * 监听房间消息
     */
    public void subscribeChatRoomMsgObservable() {
        IMRoomMessageManager.get().subscribeIMRoomMessageFlowable(messages -> {
            if (messages.size() == 0) {
                return;
            }
            //是否有新的礼物消息更新标签
            for (int i = 0; i < messages.size(); i++) {
                IMRoomMessage chatRoomMessage = messages.get(i);
                if (chatRoomMessage == null) {
                    return;
                }
                if (IMReportRoute.sendMessageReport.equalsIgnoreCase(chatRoomMessage.getRoute())) {
                    CustomAttachment attachment = chatRoomMessage.getAttachment();
                    setupBottomViewSendMsgBtn(attachment);
                }
            }
        }, this);
    }

    /**
     * 刷新麦位
     *
     * @param micPosition 刷新的麦位 -2为刷新全部
     */
    @Override
    public void refreshMicroView(int micPosition, int roomEvent) {
        LogUtils.d(TAG, "refreshMicroView-micPosition:" + micPosition + " roomEvent:" + roomEvent);
        if (micPosition == -2) {
            getMicroView().notifyDataSetChanged(roomEvent);
        } else {
            getMicroView().notifyItemChanged(micPosition, roomEvent);
        }
    }

    /**
     * 刷新底部按钮
     */
    public void refreshBottomView() {
        getBottomView().updateBottomView(getMvpPresenter().isOnMicByMyself());
        refreshMicBtnStatus();

        refreshRemoteMuteBtnStatus();
    }

    /**
     * 刷新静音按钮状态
     */
    public void refreshRemoteMuteBtnStatus() {
        getBottomView().setRemoteMuteOpen(!getMvpPresenter().isRemoteMuteByMyself());
    }

    /**
     * 刷新底部麦位按钮状态
     */
    @Override
    public void refreshMicBtnStatus() {
        if (getMvpPresenter().isAudienceRoleByMyself()) {
            getBottomView().setMicBtnEnable(false);
            getBottomView().setMicBtnOpen(false);
        } else {
            if (getMvpPresenter().isMicMuteByMyself()) {
                getBottomView().setMicBtnEnable(true);
                getBottomView().setMicBtnOpen(false);
            } else {
                getBottomView().setMicBtnEnable(true);
                getBottomView().setMicBtnOpen(true);
            }
        }
    }

    /**
     * 显示充值提示弹框
     */
    @Override
    public void showRechargeTipDialog() {
        getDialogManager().showOkCancelDialog("余额不足，是否充值", true, new DialogManager.AbsOkDialogListener() {

            @Override
            public void onOk() {
                ChargeActivity.start(getActivity());
            }
        });
    }

    /**
     * 配置底部公屏输入框的发送按钮
     * (禁言)
     *
     * @param attachment 消息
     */
    private void setupBottomViewSendMsgBtn(CustomAttachment attachment) {
        if (attachment.getSecond() == CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_OPEN) {
            getBottomView().setInputMsgBtnEnable(true);
        } else if (attachment.getSecond() == CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_CLOSE) {
            getBottomView().setInputMsgBtnEnable(false);
        }
    }

    /**
     * 显示礼物弹框
     *
     * @param targetUid 此人的uid
     */
    public void showGiftDialog(long targetUid, boolean isPersonal) {
        if (!ButtonUtils.isFastDoubleClick()) {
            RoomGiftDialog roomGiftDialog = RoomGiftDialog.getInstance(targetUid, isPersonal);
//            roomGiftDialog.setOnClickToSendGiftListener(new RoomGiftDialog.OnClickToSendGiftListener() {
//                @Override
//                public void onSendRoomMultiGift(GiftInfo giftInfo, long roomUid, long roomId,
//                                                List<MicMemberInfo> micAvatar, int giftNum, boolean isAllMic) {
//                    BaseRoomDetailFragment.this.sendRoomMultiGift(giftInfo, roomUid, roomId, micAvatar,
//                            giftNum, false, isAllMic);
//                }
//
//                @Override
//                public void sendSingleGift(GiftInfo giftInfo, long targetUid, long roomUid, long roomId, int giftNum) {
//                    BaseRoomDetailFragment.this.sendSingleGift(giftInfo, targetUid, roomUid, roomId, giftNum, false);
//                }
//            });
            roomGiftDialog.show(getChildFragmentManager(), "giftDialog");
//            hideComBoSenderViewAfterShowGiftDialog();
        }
    }

//    public void sendRoomMultiGift(GiftInfo giftInfo, long roomUid, long roomId,
//                                  List<MicMemberInfo> micAvatar, int giftNum,
//                                  boolean clickByComBoBtn, boolean isAllMic) {
//        LogUtils.d(TAG, "sendRoomMultiGift roomId:" + roomId + " giftNum:" + giftNum + " clickByComBoBtn:" + clickByComBoBtn + " isAllMic:" + isAllMic);
//        if (isAllMic) {
//            onSendRoomMultiGift(giftInfo, roomUid, roomId, micAvatar, giftNum, clickByComBoBtn);
//        } else {
//            long forEndUid = micAvatar.get(micAvatar.size() - 1).getUid();
//            //首次发送，需要清空并更新缓存的接收方成员信息
//            if (!clickByComBoBtn && null != getRoomGiftComboSenderView() && CoreManager.getCore(VersionsCore.class).isGiftComboSwitchOpen()) {
//                LogUtils.d(TAG, "sendRoomMultiGift-->setMicAvatar");
//                getRoomGiftComboSenderView().setMicAvatar(micAvatar);
//                getRoomGiftComboSenderView().getUidComboInfos().clear();
//            }
//            for (MicMemberInfo micMemberInfo : micAvatar) {
//                sendMultiSingleGift(giftInfo, micMemberInfo, roomUid, roomId, giftNum,
//                        clickByComBoBtn, micMemberInfo.getUid() == forEndUid);
//            }
//            //这里因为sendSingleGift方法中接口请求回调更新的数据，因此这里立即调用restartTimeCountProgressAnim并不太合适
//        }
//    }
//
//    private void onSendRoomMultiGift(GiftInfo giftInfo, long roomUid, long roomId,
//                                     List<MicMemberInfo> micAvatar, int giftNum, boolean clickByComBoBtn) {
//        if (!clickByComBoBtn && CoreManager.getCore(VersionsCore.class).isGiftComboSwitchOpen()) {
//            //重置连送状态之前，将上次连送的公屏消息发出去
//            ComboGiftPublicScreenMsgSender.getInstance().sendComboGiftScreenMsg(roomId);
//            //先生成comBoId、comBoCount等数据
//            refreshComboSenderStatus(giftInfo, micAvatar, 0, giftNum);
//        }
//        long comboId = 0;
//        int comboRangStart = 0;
//        int comboRangEnd = giftNum;
//        if (null != getRoomGiftComboSenderView() && CoreManager.getCore(VersionsCore.class).isGiftComboSwitchOpen()) {
//            comboId = getRoomGiftComboSenderView().getComboId();
//            comboRangStart = getRoomGiftComboSenderView().getComboRangStart();
//            comboRangEnd = getRoomGiftComboSenderView().getComboRangEnd();
//        }
//        if (null != giftInfo) {
//            LogUtils.d(TAG, "onSendRoomMultiGift comboId:" + comboId + " comboRangStart:" + comboRangStart + " comboRangEnd:" + comboRangEnd + " giftId:" + giftInfo.getGiftId());
//        }
//
//        getMvpPresenter().sendRoomMultiGift(giftInfo, roomUid, roomId, micAvatar, giftNum, comboId, comboRangStart, comboRangEnd, new CallBack<MultiGiftReceiveInfo>() {
//            @Override
//            public void onSuccess(MultiGiftReceiveInfo data) {
//                //发送成功则显示连送按钮
//                if (!clickByComBoBtn && null != getRoomGiftComboSenderView() && CoreManager.getCore(VersionsCore.class).isGiftComboSwitchOpen()) {
//                    getRoomGiftComboSenderView().restartTimeCountProgressAnim();
//                }
//            }
//
//            @Override
//            public void onFail(int code, String error) {
//                sendGiftErrorToast(code, error);
//                if (null != getRoomGiftComboSenderView() && CoreManager.getCore(VersionsCore.class).isGiftComboSwitchOpen()) {
//                    getRoomGiftComboSenderView().clear();
//                    getRoomGiftComboSenderView().setVisibility(View.GONE);
//                }
//            }
//        });
//    }
//
//    public void sendSingleGift(GiftInfo giftInfo, long targetUid, long roomUid, long roomId,
//                               int giftNum, boolean clickByComBoBtn) {
//        //非连送按钮连击触发，且不为非全麦多人
//        if (!clickByComBoBtn && CoreManager.getCore(VersionsCore.class).isGiftComboSwitchOpen()) {
//            //重置连送状态之前，将上次连送的公屏消息发出去
//            ComboGiftPublicScreenMsgSender.getInstance().sendComboGiftScreenMsg(roomId);
//            refreshComboSenderStatus(giftInfo, null, targetUid, giftNum);
//        }
//        long comboId = 0;
//        int comboRangStart = 0;
//        int comboRangEnd = giftNum;
//        if (null != getRoomGiftComboSenderView() && CoreManager.getCore(VersionsCore.class).isGiftComboSwitchOpen()) {
//            comboId = getRoomGiftComboSenderView().getComboId();
//            comboRangStart = getRoomGiftComboSenderView().getComboRangStart();
//            comboRangEnd = getRoomGiftComboSenderView().getComboRangEnd();
//        }
//        if (null != giftInfo) {
//            LogUtils.d(TAG, "sendSingleGift comboId:" + comboId + " comboRangStart:"
//                    + comboRangStart + " comboRangEnd:" + comboRangEnd + " giftId:" + giftInfo.getGiftId());
//        }
//        getMvpPresenter().sendSingleGift(giftInfo, targetUid, roomUid, roomId, giftNum, comboId, comboRangStart, comboRangEnd, new CallBack<GiftReceiveInfo>() {
//            @Override
//            public void onSuccess(GiftReceiveInfo data) {
//                //发送成功则显示连送按钮
//                if (null != getRoomGiftComboSenderView() && CoreManager.getCore(VersionsCore.class).isGiftComboSwitchOpen()) {
//                    //不为非全麦多人，也即单人赠送，直接触发重新倒计时的逻辑
//                    getRoomGiftComboSenderView().restartTimeCountProgressAnim();
//                }
//            }
//
//            @Override
//            public void onFail(int code, String error) {
//                sendGiftErrorToast(code, error);
//                if (null != getRoomGiftComboSenderView() && CoreManager.getCore(VersionsCore.class).isGiftComboSwitchOpen()) {
//                    getRoomGiftComboSenderView().clear();
//                    getRoomGiftComboSenderView().setVisibility(View.GONE);
//                }
//            }
//        });
//    }


//    public void sendMultiSingleGift(GiftInfo giftInfo, MicMemberInfo micMemberInfo, long roomUid,
//                                    long roomId, int giftNum, boolean clickByComBoBtn, boolean isForEnd) {
//        LogUtils.d(TAG, "sendMultiSingleGift roomId:" + roomId + " giftNum:" + giftNum + " clickByComBoBtn:" + clickByComBoBtn + " isForEnd:" + isForEnd);
//        //非全麦多人，首次送礼，初始化comboId等数据
//        //非连送按钮连击触发，且不为非全麦多人
//        long comboId = 0;
//        int comboRangStart = 0;
//        int comboRangEnd = giftNum;
//
//        if (null != getRoomGiftComboSenderView() && CoreManager.getCore(VersionsCore.class).isGiftComboSwitchOpen()) {
//            if (!clickByComBoBtn) {
//                //重置连送状态之前，将上次连送的公屏消息发出去
//                ComboGiftPublicScreenMsgSender.getInstance().sendComboGiftScreenMsg(roomId);
//                getRoomGiftComboSenderView().initComBoInfoForMultiNoAllMic(giftInfo, giftNum);
//            }
//            ComboInfo comboInfo = getRoomGiftComboSenderView().genMultiNoAllMicComboId(micMemberInfo.getUid(), giftNum);
//            if (null != comboInfo) {
//                comboId = comboInfo.getComboId();
//                comboRangStart = comboInfo.getComboRangStart();
//                comboRangEnd = comboInfo.getComboRangEnd();
//            }
//        }
//
//        if (null != giftInfo) {
//            LogUtils.d(TAG, "sendMultiSingleGift uid:" + micMemberInfo.getUid() + " comboId:" + comboId + " comboRangStart:"
//                    + comboRangStart + " comboRangEnd:" + comboRangEnd + " giftId:" + giftInfo.getGiftId());
//        }
//        getMvpPresenter().sendSingleGift(giftInfo, micMemberInfo.getUid(),
//                roomUid, roomId, giftNum, comboId, comboRangStart, comboRangEnd, new CallBack<GiftReceiveInfo>() {
//                    @Override
//                    public void onSuccess(GiftReceiveInfo data) {
//                        //发送成功则显示连送按钮
//                        if (!clickByComBoBtn && isForEnd && null != getRoomGiftComboSenderView()
//                                && CoreManager.getCore(VersionsCore.class).isGiftComboSwitchOpen()) {
//                            //循环完毕后，才做显示连送按钮逻辑，
//                            getRoomGiftComboSenderView().restartTimeCountProgressAnim();
//                        }
//                    }
//
//                    @Override
//                    public void onFail(int code, String error) {
//                        sendGiftErrorToast(code, error);
//                        if (null != getRoomGiftComboSenderView() && CoreManager.getCore(VersionsCore.class).isGiftComboSwitchOpen()) {
//                            getRoomGiftComboSenderView().clear();
//                            getRoomGiftComboSenderView().setVisibility(View.GONE);
//                        }
//                    }
//                });
//    }
//
//    public void refreshComboSenderStatus(GiftInfo giftInfo, List<MicMemberInfo> micAvatar, long targetUid, int giftNum) {
//        if (null != getRoomGiftComboSenderView()) {
//            getRoomGiftComboSenderView().refreshComboSenderStatus(giftInfo, micAvatar, targetUid, giftNum);
//        }
//    }

    @Override
    public void sendGiftErrorToast(int code, String error) {
        if (!TextUtils.isEmpty(error)) {
            SingleToastUtil.showToast(error);
        }
    }

    @Override
    public void updateGiftBeanAndGoldCount(WalletInfo data) {
        CoreManager.getCore(IPayCore.class).setCurrentWalletInfo(data);
    }

    /**
     * 显示用户信息弹框
     *
     * @param uid 用户uid
     */
    public void showUserInfoDialog(long uid) {
        LogUtils.d(TAG, "showUserInfoDialog-uid:" + uid);
        new UserInfoDialog(getActivity(), uid).show();
    }

    /**
     * 显示对用户操作的弹框
     */
    public void showUserOperateDialog(long uid) {
        showUserInfoDialog(uid);
    }

    /**
     * 显示对用户操作的弹框
     */
    public void showUserOperateDialog(long uid, String nick) {
        if (getContext() != null) {
            IMRoomMember member = new IMRoomMember();
            member.setAccount(String.valueOf(uid));
            member.setNick(nick);
            getMvpPresenter().dealWithOnlinePeoleClickEvent(getContext(), RoomDataManager.get().getCurrentRoomInfo(), member);
        }
    }

    /**
     * 显示举报弹框
     *
     * @param type 1为举报用户 和 2为举报房间
     * @param uid  被举报人的uid，如果是房间，则传房主uid
     */
    @Override
    public void showReportDialog(int type, long uid) {
        List<ButtonItem> buttons = new ArrayList<>();
        ButtonItem button1 = new ButtonItem("政治敏感", () -> getMvpPresenter().commitReport(type, uid, 1));
        ButtonItem button2 = new ButtonItem("色情低俗", () -> getMvpPresenter().commitReport(type, uid, 2));
        ButtonItem button3 = new ButtonItem("广告骚扰", () -> getMvpPresenter().commitReport(type, uid, 3));
        ButtonItem button4 = new ButtonItem("人身攻击", () -> getMvpPresenter().commitReport(type, uid, 4));
        buttons.add(button1);
        buttons.add(button2);
        buttons.add(button3);
        buttons.add(button4);
        getDialogManager().showCommonPopupDialog(buttons, "取消");
    }

    /**
     * 打开私聊页面
     */
    @Override
    public void startP2PSession(long friendUid) {
        SessionHelper.startP2PSession(getContext(), String.valueOf(friendUid));
    }

    /**
     * 处理谁来了的逻辑
     */
    protected void dealUserComingMsg() {
        if (isShowing) {
            return;
        }
        IMRoomMemberComeInfo comeInfo = getMvpPresenter().getAndRemoveFirstMemberComeInfo();
        //大于等于10级，显示用户进来的弹幕
        if (comeInfo != null && comeInfo.getExperLevel() >= 10) {
            this.isShowing = true;
            //展示动画
            getComingMsgView().startAnimation(new AnimationListener() {

                @Override
                public void onAnimationStart(Animation animation) {
                    getComingMsgView().setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    getComingMsgView().setVisibility(View.INVISIBLE);
                    isShowing = false;
                    //队列中还有其他弹幕未处理完
                    if (getMvpPresenter().getMemberComeSize() > 0) {
                        //继续处理
                        dealUserComingMsg();
                    }
                }
            });
            //显示特效
            getComingMsgView().setupView(comeInfo);
        } else {
            isShowing = false;
        }
    }

    /**
     * 抱人上麦
     */
    public void holdUpMicro(long uid) {
        int freePosition = RoomDataManager.get().findFreePosition();
        if (freePosition == Integer.MIN_VALUE) {
            SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext().getString(R.string.no_empty_mic));
            return;
        }
        IMRoomMessageManager.get().inviteMicroPhoneBySdk(uid, freePosition);
    }

    /**
     * 显示在线用户的弹窗
     */
    public void showOnlineMemberDialog(IMRoomOnlineMemberFragment.OnlineMemberItemClick onlineItemClick) {
        if (getMvpPresenter().getCurrRoomInfo() != null) {
            IMRoomMemberListDialog imRoomMemberListDialog = IMRoomMemberListDialog.newOnlineUserListInstance(getActivity());
            imRoomMemberListDialog.setOnlineItemClick(onlineItemClick);
            imRoomMemberListDialog.show(getChildFragmentManager());
        }
    }

    /**
     * 显示土豪榜单
     */
    public void showContributionMemberListDialog() {
        if (getMvpPresenter().getCurrRoomInfo() != null) {
            BigListDataDialog bigListDataDialog = BigListDataDialog.newContributionListInstance(getActivity());
            bigListDataDialog.setSelectOptionAction(new BigListDataDialog.SelectOptionAction() {
                @Override
                public void optionClick() {
                    getDialogManager().showProgressDialog(mContext, mContext.getResources().getString(R.string.network_loading));
                }

                @Override
                public void onDataResponse() {
                    //请求结束前退出可能会导致奔溃，直接捕获没关系
                    try {
                        getDialogManager().dismissDialog();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
            bigListDataDialog.show(getChildFragmentManager());
        }
    }

    /**
     * 接收房间事件
     */
    public void receiveRoomEvent(IMRoomEvent roomEvent) {
        if (roomEvent == null) {
            return;
        }
        int event = roomEvent.getEvent();
        switch (event) {
            case IMRoomEvent.SOCKET_ROOM_ENTER_SUC://socket进房(包括socket重连进房)
                onRoomInSucShowMicroAndMessageView();
                refreshOnlineCount(getMvpPresenter().getCurrRoomInfo().onlineNum);
                updateRoomInfoAboutView(getMvpPresenter().getCurrRoomInfo());
                refreshMicroView(-2, RoomEvent.NONE);
                break;
            case IMRoomEvent.ENTER_ROOM://对应声网进channel流程
                getMvpPresenter().checkAndAutoUpMic();
                refreshBottomView();
                if (getActivity() instanceof RoomFrameActivity) {
                    ((RoomFrameActivity) getActivity()).setupRoomBg(getMvpPresenter().getCurrRoomInfo());
                }
                onRoomDynamicInitView();
                //查询是否已经关注房间
                if (!RoomDataManager.get().isRoomOwner()) {
                    getMvpPresenter().getCheckRoomAttention();
                }
                dealUserComingMsg();
                break;
            case IMRoomEvent.ROOM_MANAGER_ADD:
                //添加管理员权限
            case IMRoomEvent.ROOM_MANAGER_REMOVE:
                //移除管理员权限
                if (getMvpPresenter() != null && getMvpPresenter().isMyself(roomEvent.getAccount())) {
                    if (roomEvent.getEvent() == RoomEvent.ROOM_MANAGER_ADD) {
                        toast(R.string.set_room_manager);
                    } else if (roomEvent.getEvent() == RoomEvent.ROOM_MANAGER_REMOVE) {
                        toast(R.string.remove_room_manager);
                    }
//                    refreshMicroView(-2, event);
                    refreshBottomView();
                }
                break;
            case IMRoomEvent.ROOM_RECONNECT:
                //IM重连
                getMvpPresenter().checkAndAutoUpMic();
                refreshMicBtnStatus();
            case IMRoomEvent.ROOM_INFO_UPDATE:
                initRoomViewFromRoomInfo(getMvpPresenter().getCurrRoomInfo(), true, false, true);
                break;
            case IMRoomEvent.MIC_QUEUE_STATE_CHANGE:
                //因为没有记录切换上一个麦位的标记，单独刷新一个麦位会出现多个用户头像再麦位上
                refreshMicroView(-2, event);
                refreshMicBtnStatus();
                break;
            case IMRoomEvent.KICK_DOWN_MIC://
                toast(mContext.getResources().getString(R.string.kick_mic));
                break;
            case IMRoomEvent.DOWN_MIC:
                //下麦
            case IMRoomEvent.UP_MIC:
                //上麦
                refreshBottomView();
                refreshMicroView(-2, event);
                break;
            case IMRoomEvent.INVITE_UP_MIC:
                inviteMemberUpMicro(roomEvent.getMicPosition(),roomEvent.getAccount());
                break;
            case IMRoomEvent.ROOM_MIC_CHARM_UPDATE:
                getMicroView().updateCharmData(roomEvent.getRoomCharmInfo());
                break;
            //更新房间人数
            case IMRoomEvent.ROOM_MEMBER_IN:
            case IMRoomEvent.ROOM_MEMBER_EXIT:
                if (roomEvent.getIMRoomMessage() != null
                        && roomEvent.getIMRoomMessage().getImRoomMember() != null
                        && roomEvent.getIMRoomMessage().getImRoomMember().getTimestamp() > getMvpPresenter().getTimestampOnMemberChanged()) {
                    int onlineNum = roomEvent.getIMRoomMessage().getImRoomMember().getOnlineNum();
                    getMvpPresenter().refreshOnlineNum(onlineNum);
                    //当房间人员变化的时候记录一下变化的时间戳
                    getMvpPresenter().setTimestampOnMemberChanged(roomEvent.getIMRoomMessage().getImRoomMember().getTimestamp());
                    //刷新在线人数显示
                    refreshOnlineCount(onlineNum);
                    dealUserComingMsg();
                }
                break;
            case IMRoomEvent.RECEIVE_MSG:
                IMRoomMessage chatRoomMessage = roomEvent.getIMRoomMessage();
                if (chatRoomMessage != null && IMReportRoute.sendMessageReport.equalsIgnoreCase(chatRoomMessage.getRoute())) {
                    CustomAttachment attachment = chatRoomMessage.getAttachment();
                    LogUtil.d("RoomEvent.RECEIVE_MSG", "second = " + attachment.getSecond());
                    setupBottomViewSendMsgBtn(attachment);
                }
                break;
            case IMRoomEvent.SPEAK_STATE_CHANGE:
                onSpeakQueueUpdate(roomEvent.getMicPositionList());
                break;
            case IMRoomEvent.SPEAK_ZEGO_STATE_CHANGE:
                onZegoSpeakQueueUpdate(roomEvent.getSpeakQueueMembersPosition());
                break;
            case IMRoomEvent.CURRENT_SPEAK_STATE_CHANGE:
                onCurrentSpeakUpdate(roomEvent.getCurrentMicPosition(), roomEvent.getCurrentMicStreamLevel());
                break;
            case IMRoomEvent.RTC_ENGINE_NETWORK_BAD:
                toast("当前网络不稳定，请检查网络");
                break;
            case IMRoomEvent.RTC_ENGINE_NETWORK_CLOSE:
                toast("当前网络异常，与服务器断开连接，请检查网络");
                break;
            case IMRoomEvent.PLAY_OR_PUBLISH_NETWORK_ERROR:
                toast("当前网络异常，与服务器断开连接，请检查网络");
                break;
            case IMRoomEvent.ZEGO_AUDIO_DEVICE_ERROR:
                toast("当前麦克风可能被占用，请检查设备是否可用");
                break;
            default:
                break;
        }
    }


    /**
     * 房间邀请上麦通知回调消息处理
     * @param micPosition
     * @param account
     */
    public void inviteMemberUpMicro(int micPosition, String account) {
        getDialogManager().showOkBigTips(getString(R.string.tip_tips), getString(R.string.embrace_on_mic), true, null);
//        getMvpPresenter().operateUpMicro(micPosition, account, true);
        //因为最小化页面无回调问题导致这里上麦操作需要调整位置
    }

    /**
     * 返回按钮事件
     *
     * @return 是否拦截
     */
    public abstract boolean onBack();

    @Override
    public void onDestroyView() {
        LogUtil.d("BaseRoomDetailFragment","onDestroyView");
        releaseView();
        super.onDestroyView();
    }

    @Override
    public void hiddleRoomAttentionView() {
        if (null != getFollowRoomView()) {
            getFollowRoomView().setVisibility(View.GONE);
        }
    }

    @Override
    public void showRoomAttentionStatus(boolean status) {
        if (null != getFollowRoomView()) {
            getFollowRoomView().setVisibility(RoomDataManager.get().isRoomOwner() ? View.GONE : View.VISIBLE);
            if (status) {
                getFollowRoomView().setBackgroundColor(Color.TRANSPARENT);
            } else {
                getFollowRoomView().setBackgroundResource(R.drawable.bg_room_follow_room);
            }
            getFollowRoomView().setText(getResources().getString(status ? R.string.user_info_already_attention : R.string.user_info_attention));
        }
    }

    @Override
    public void onOpenMicBtnClick() {
        //没有im的时候不给操作
        if (!ReUsedSocketManager.get().isConnect()) {
            toast("网络已断开无法操作");
            return;
        }

        IMRoomQueueInfo roomQueueInfo = getMvpPresenter().getMicQueueInfoByMicPositionByMyself();
        if (roomQueueInfo == null || roomQueueInfo.mRoomMicInfo == null) {
            return;//我不在麦上
        }
        //我是主播，并且这个麦位不是被禁麦的
        if (!roomQueueInfo.mRoomMicInfo.isMicMute() && !getMvpPresenter().isAudienceRoleByMyself()) {
            //切换闭麦/开麦状态
            getMvpPresenter().switchMicroMuteStatus();
            refreshMicBtnStatus();
        }
    }

    @Override
    public void onSendMsgBtnClick() {
        if (ChatUtil.checkBanned()) {
            return;
        }
        getInputMsgView().setVisibility(View.VISIBLE);
        getInputMsgView().setInputText("");
        getInputMsgView().showKeyBoard();
    }

    @Override
    public void onSendFaceBtnClick() {

    }

    @Override
    public void onSendGiftBtnClick() {
        showGiftDialog(0, false);
    }

    @Override
    public void onRemoteMuteBtnClick() {
        RtcEngineManager.get().setRemoteMute(!RtcEngineManager.get().isRemoteMute());
        refreshRemoteMuteBtnStatus();
    }

    @Override
    public void onBuShowMicInList() {

    }

    @Override
    public void onPublicRoomMsgBtnClick() {
    }

    @Override
    public void onShareBtnClick() {
        showShareDialog();
    }

    protected ShareDialog.OnShareDialogItemClick onShareDialogItemClick = new ShareDialog.OnShareDialogItemClick() {

        @Override
        public void onSharePlatformClick(Platform platform) {
            RoomInfo currentRoomInfo = RoomDataManager.get().getCurrentRoomInfo();
            String shareName = platform.getName();
            if (currentRoomInfo != null) {
                CoreManager.getCore(IShareCore.class).shareRoom(platform, currentRoomInfo.getUid(),
                        currentRoomInfo.getTitle(), currentRoomInfo.getType());
            }
        }
    };

    protected void releaseView() {
        getMicroView().release();
        getMessageView().release();
        getGiftView().release();
        getInputMsgView().release();
        getBottomView().setBottomViewListener(null);
        if (null != onSoftKeyBoardChangeListener) {
            onSoftKeyBoardChangeListener = null;
        }
//        if (null != getRoomGiftComboSenderView()) {
//            getRoomGiftComboSenderView().setOnComboClickListener(null);
//            getRoomGiftComboSenderView().clear();
//            //将上次连送的公屏消息发出去
//            ComboGiftPublicScreenMsgSender.getInstance().sendComboGiftScreenMsg();
//        }
//        if (null != getComboGiftView()) {
//            getComboGiftView().clear();
//        }
    }

    protected void showShareDialog() {
        //显示分享弹框
        ShareDialog shareDialog = new ShareDialog(getActivity());
        shareDialog.setOnShareDialogItemClick(onShareDialogItemClick);
        shareDialog.setShowShareFans(true);
        shareDialog.show();
    }

    @Override
    public void openRoomSettingActivity() {
        if (null != getActivity()) {
            RoomSettingActivity.start(getActivity());
        }
    }

    protected void showLotteryBoxDialog() {
        LotteryBoxDialog lotteryBoxDialog = LotteryBoxDialog.newInstance();
        lotteryBoxDialog.show(getChildFragmentManager(), null);

        //房间-打开挖宝界面
        StatisticManager.get().onEvent(getActivity(),
                StatisticModel.EVENT_ID_ROOM_OPEN_BOX,
                StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
    }


    @Override
    public void showShareDialogView() {
        showShareDialog();
    }

    @Override
    public void showCommonPopupDialog(List<ButtonItem> buttonItems, int strId) {
        getDialogManager().showCommonPopupDialog(buttonItems, getString(strId));
    }


    @Override
    public void showUserInfoDialogView(long uid) {
        showUserInfoDialog(uid);
    }


    @Override
    public void openUserInfoActivity(long uid) {
        if (getActivity() != null) {
            UserInfoActivity.start(getActivity(), uid);
        }
    }

    @Override
    public void openRoomInviteActivity(int micPosition) {
        if (getActivity() != null) {
            IMRoomInviteActivity.openActivity(getActivity(), micPosition);
        }
    }

    @Override
    public void onActivityFinish() {
        if (getActivity() != null) {
            getActivity().finish();
        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMRoomConstant.REQUEST_CODE && resultCode == IMRoomConstant.ROOM_INVITE_RESULT_CODE) {
            if (data != null && data.getExtras() != null) {
                long account = data.getExtras().getLong("account", 0);
                if (account <= 0) {
                    return;
                }
                int micPosition = data.getExtras().getInt(Constants.KEY_POSITION, Integer.MIN_VALUE);
                if (micPosition == Integer.MIN_VALUE) {
                    return;
                }
                SparseArray<IMRoomQueueInfo> mMicQueueMemberMap = RoomDataManager.get().mMicQueueMemberMap;
                if (mMicQueueMemberMap != null && mMicQueueMemberMap.get(micPosition).mChatRoomMember != null) {
                    //该麦位有人了
                    toast("该麦位已经有人了");
                    return;
                }
                //抱人上麦
                getMvpPresenter().inviteUpMicro(micPosition, account);
            }
        }
    }

    @CoreEvent(coreClientClass = IShareCoreClient.class)
    public void onShareRoom() {
        toast("分享成功");
        IMRoomMessageManager.get().sendRoomOperaTipMsgBySdk(CustomAttachment.CUSTOM_MSG_ROOM_OPERA_TIPS_SHARE);
    }

    @CoreEvent(coreClientClass = IShareCoreClient.class)
    public void onShareRoomFail() {
        toast("分享失败，请重试");
    }

    @CoreEvent(coreClientClass = IShareCoreClient.class)
    public void onShareRoomCancel() {
        getDialogManager().dismissDialog();
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraise(long likedUid) {
        getDialogManager().dismissDialog();
        if (null != RoomDataManager.get().getCurrentRoomInfo()
                && likedUid == RoomDataManager.get().getCurrentRoomInfo().getUid()) {
            RoomDataManager.get().setHasLikeRoomOwner(true);
            if (RoomDataManager.get().getCurrentRoomInfo().getType() != RoomInfo.ROOMTYPE_SINGLE_AUDIO) {
                getMessageView().refreshLikeMsgStatus(true);
                IMRoomMessageManager.get().sendRoomOperaTipMsgBySdk(CustomAttachment.CUSTOM_MSG_ROOM_OPERA_TIPS_ATTENTION);
            }
        }
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraiseFaith(String error) {
        getDialogManager().dismissDialog();
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onCanceledPraise(long likedUid) {
        getDialogManager().dismissDialog();
        if (null != RoomDataManager.get().getCurrentRoomInfo()
                && likedUid == RoomDataManager.get().getCurrentRoomInfo().getUid()) {
            RoomDataManager.get().setHasLikeRoomOwner(false);
            if (RoomDataManager.get().getCurrentRoomInfo().getType() != RoomInfo.ROOMTYPE_SINGLE_AUDIO) {
                getMessageView().refreshLikeMsgStatus(true);
            }
        }
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onCanceledPraiseFaith(String error) {
        getDialogManager().dismissDialog();
    }

    @CoreEvent(coreClientClass = IFaceCoreClient.class)
    public void onReceiveFace(List<FaceReceiveInfo> faceReceiveInfos) {
        showFaceView(faceReceiveInfos);
    }

    public void showFaceView(List<FaceReceiveInfo> faceReceiveInfos) {
        getMicroView().showFaceView(faceReceiveInfos);
    }

    protected void onRoomBannerClick() {

    }

    @Override
    public void showRoomActionBanner(List<ActionDialogInfo> actionDialogInfos) {
        if (null != getBanner()) {
            if (!ListUtils.isListEmpty(actionDialogInfos)) {
                getBanner().setVisibility(View.VISIBLE);
                roomBannerAdapter = new RoomBannerAdapter(actionDialogInfos, getActivity());
                roomBannerAdapter.setOnRoomBannerClickListener(new RoomBannerAdapter.OnRoomBannerClickListener() {
                    @Override
                    public void onGameBannerClicked(ActionDialogInfo actionDialogInfo) {
                        LogUtils.d(TAG, "onGameBannerClicked-actionDialogInfo:" + actionDialogInfo);
                        if (null != getActivity()) {
                            CommonWebViewActivity.start(getActivity(), actionDialogInfo.getSkipUrl());
                            onRoomBannerClick();
                        }
                    }
                });
                getBanner().setAdapter(roomBannerAdapter);
                if (actionDialogInfos.size() > 1) {
                    if (null != getActivity()) {
                        getBanner().setHintView(new RoomBannerShapeHintView(getActivity()));
                    }
                    getBanner().setPlayDelay(3000);
                    getBanner().setAnimationDurtion(500);
                } else {
                    if (null != getActivity()) {
                        getBanner().setHintView(new ColorPointHintView(getActivity(), Color.TRANSPARENT, Color.TRANSPARENT));
                    }
                }
            } else {
                getBanner().setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void sendGiftUpdatePacketGiftCount(GiftInfo giftInfo) {

    }

    @Override
    public void sendGiftUpdateMinusBeanCount(int minusCount) {

    }

    @Override
    public void sendGiftUpdateMinusGoldCount(int minusCount) {

    }

    @Override
    public void onShow(String svgUrl) {
        try {
            getGiftView().giftEffectView.playRoomCarSvgaAnim(DESUtils.DESAndBase64DecryptRoomCar(svgUrl));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
