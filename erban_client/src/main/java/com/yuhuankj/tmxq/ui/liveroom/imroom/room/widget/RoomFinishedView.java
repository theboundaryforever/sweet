package com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.LinearLayout;

import com.yuhuankj.tmxq.R;

/**
 * 房间直播结束后的view
 *
 * @author zeda
 */
public class RoomFinishedView extends LinearLayout {

    private OnViewClickListener onViewClickListener;

    public RoomFinishedView(@NonNull Context context) {
        this(context, null);
    }

    public RoomFinishedView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoomFinishedView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        inflate(getContext(), R.layout.layout_room_live_finish, this);
        setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));
        setGravity(Gravity.CENTER_VERTICAL);
        setOrientation(VERTICAL);

        findViewById(R.id.back_btn).setOnClickListener(v -> {
            if (onViewClickListener != null) {
                onViewClickListener.onBackBtnClick();
            }
        });
    }

    public void setOnViewClickListener(OnViewClickListener onViewClickListener) {
        this.onViewClickListener = onViewClickListener;
    }

    public interface OnViewClickListener {
        void onBackBtnClick();
    }
}
