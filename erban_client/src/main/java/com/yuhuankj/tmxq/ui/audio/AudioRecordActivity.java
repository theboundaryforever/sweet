package com.yuhuankj.tmxq.ui.audio;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.netease.nimlib.sdk.media.record.AudioRecorder;
import com.netease.nimlib.sdk.media.record.IAudioRecordCallback;
import com.netease.nimlib.sdk.media.record.RecordType;
import com.tongdaxing.erban.libcommon.audio.AudioPlayer;
import com.tongdaxing.erban.libcommon.audio.OnPlayListener;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.LogUtil;
import com.tongdaxing.xchat_core.audio.AudioPlayAndRecordManager;
import com.tongdaxing.xchat_core.audio.AudioRecordPresenter;
import com.tongdaxing.xchat_core.audio.AudioRecordStatus;
import com.tongdaxing.xchat_core.audio.IAudioRecordView;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.file.IFileCore;
import com.tongdaxing.xchat_core.file.IFileCoreClient;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;

import java.io.File;

/**
 * @author zhouxiangfeng
 * @date 2017/5/25
 */
@CreatePresenter(AudioRecordPresenter.class)
public class AudioRecordActivity extends BaseMvpActivity<IAudioRecordView, AudioRecordPresenter>
        implements View.OnClickListener, IAudioRecordView {

    private static final String TAG = "AudioRecordActivity";

    public static final String AUDIO_FILE = "AUDIO_FILE";
    public static final String AUDIO_DURA = "AUDIO_DURA";

    private LinearLayout mLl_record;
    private TextView mTv_state;
    private ImageView mIv_retry_record;
    private ImageView mIv_record;
    private ImageView mIv_try_listen;
    private ImageView imgBack;
    private AudioRecorder audioRecorder;
    private String audioUrl;
    private AudioPlayer audioPlayer;
    private AudioPlayAndRecordManager audioManager;
    private Chronometer mTv_chronometer;
    private ImageView mIv_record_save;
    private AudioRecorder recorder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audiorecord);
        bindViews();
        onSetListner();
        init();
    }

    private void init() {
        audioManager = AudioPlayAndRecordManager.getInstance();
        audioPlayer = audioManager.getAudioPlayer(AudioRecordActivity.this, null, onPlayListener);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void onSetListner() {
        mLl_record.setOnClickListener(this);
        mIv_try_listen.setOnClickListener(this);
        mIv_retry_record.setOnClickListener(this);
        mIv_record_save.setOnClickListener(this);
        mIv_record.setOnTouchListener((v, event) -> {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
//                        toast("按下");
                    RoomInfo roomInfo = RoomServiceScheduler.getCurrentRoomInfo();
                    if (roomInfo != null) {
                        getDialogManager().showOkCancelDialog("当前正在房间无法录音，是否关闭房间？",
                                true, new DialogManager.OkCancelDialogListener() {
                                    @Override
                                    public void onCancel() {
                                    }

                                    @Override
                                    public void onOk() {
                                        getMvpPresenter().exitRoom();
                                    }
                                });
                    } else {
                        startVoice();
                    }
                    return true;
                case MotionEvent.ACTION_UP:
//                        toast("放开");
                    audioManager.stopRecord(false);
                    return true;
                default:
                    break;
            }
            return false;
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void startVoice() {
        if (audioState == AudioRecordStatus.STATE_RECORD_RECORDING) {
            toast("已经在录音...");
        } else if (audioState == AudioRecordStatus.STATE_RECORD_NORMAL) {
//            toast("开始录音");
            audioState = AudioRecordStatus.STATE_RECORD_RECORDING;
            startChronometer();
            recorder = audioManager.getAudioRecorder(AudioRecordActivity.this, onRecordCallback);
            audioManager.startRecord();
        }
    }

    private void startChronometer() {
        mTv_chronometer.setVisibility(View.VISIBLE);
        mTv_chronometer.setFormat(""); // Self dont show
        value = -1;
        mTv_chronometer.setOnChronometerTickListener(chronometerTickListener);
        mTv_chronometer.setBase(0);
        mTv_chronometer.start();
    }

    // End Of Content View Elements
    private void bindViews() {
        mLl_record = (LinearLayout) findViewById(R.id.ll_record);
        mTv_chronometer = (Chronometer) findViewById(R.id.tv_chronometer);
        mTv_state = (TextView) findViewById(R.id.tv_state);
        mIv_retry_record = (ImageView) findViewById(R.id.iv_retry_record);
        mIv_record = (ImageView) findViewById(R.id.iv_record);
        mIv_try_listen = (ImageView) findViewById(R.id.iv_try_listen);
        mIv_record_save = (ImageView) findViewById(R.id.iv_record_save);
        imgBack = (ImageView) findViewById(R.id.img_back);
    }

    private AudioRecordStatus audioState = AudioRecordStatus.STATE_RECORD_NORMAL;

    private File audioFile;

    private int audioDura;

    IAudioRecordCallback onRecordCallback = new IAudioRecordCallback() {
        @Override
        public void onRecordReady() {
            LogUtil.d(TAG, "onRecordReady");
        }

        @Override
        public void onRecordStart(File file, RecordType recordType) {
            LogUtil.d(TAG, "onRecordStart : " + file.getPath() + "type: " + recordType.name());
        }

        @Override
        public void onRecordSuccess(File file, long l, RecordType recordType) {
            double dura = (double) l / 1000;
            audioDura = (int) Math.round(dura);
            LogUtil.d(TAG, "onRecordSuccess : " + file.getPath() + "lenth  ：" + audioDura + "type : " + recordType.name());
            toast("录制完成");
            audioFile = file;
            audioState = AudioRecordStatus.STATE_RECORD_SUCCESS;
            showByState(audioState);
        }

        @Override
        public void onRecordFail() {
            LogUtil.d(TAG, "onRecordFail");
            toast("录制失败,录音时间过短");

            audioState = AudioRecordStatus.STATE_RECORD_NORMAL;
            showByState(audioState);
        }

        @Override
        public void onRecordCancel() {
            LogUtil.d(TAG, "onRecordCancel");
            audioState = AudioRecordStatus.STATE_RECORD_NORMAL;
            showByState(audioState);
        }

        @Override
        public void onRecordReachedMaxTime(int i) {
            LogUtil.d(TAG, "onRecordReachedMaxTime");
            double dura = (double) i / 1000;
            int max = (int) Math.round(dura);
            toast("录音时间过长");
        }
    };

    private void showByState(AudioRecordStatus state) {
        if (state == AudioRecordStatus.STATE_RECORD_NORMAL) {
            mIv_record.setVisibility(View.VISIBLE);
            mIv_record_save.setVisibility(View.GONE);
            mTv_state.setVisibility(View.VISIBLE);
            mTv_state.setText("长按录音");
            mTv_chronometer.setVisibility(View.GONE);
            mIv_retry_record.setVisibility(View.GONE);
            mIv_try_listen.setVisibility(View.GONE);
        } else if (state == AudioRecordStatus.STATE_RECORD_RECORDING) {
            mIv_record.setVisibility(View.VISIBLE);
            mIv_record_save.setVisibility(View.GONE);
            mTv_state.setVisibility(View.VISIBLE);
            mTv_state.setText("正在录音...");
            mTv_chronometer.setVisibility(View.VISIBLE);
            mIv_retry_record.setVisibility(View.GONE);
            mIv_try_listen.setVisibility(View.GONE);
        } else if (state == AudioRecordStatus.STATE_RECORD_SUCCESS) {
            mIv_record.setVisibility(View.GONE);
            mIv_record_save.setVisibility(View.VISIBLE);
            mTv_state.setVisibility(View.INVISIBLE);
            mIv_retry_record.setVisibility(View.VISIBLE);
            mTv_chronometer.setVisibility(View.GONE);
            mTv_chronometer.stop();
            mIv_try_listen.setVisibility(View.VISIBLE);
        }
    }

    OnPlayListener onPlayListener = new OnPlayListener() {

        @Override
        public void onPrepared() {
            LogUtil.d(TAG, "onPrepared");
            mIv_try_listen.setImageResource(R.drawable.icon_try_listen_pause);
        }

        @Override
        public void onCompletion() {
            LogUtil.d(TAG, "onCompletion");
            mIv_try_listen.setImageResource(R.drawable.icon_try_listen);
        }

        @Override
        public void onInterrupt() {
            LogUtil.d(TAG, "onInterrupt");
            mIv_try_listen.setImageResource(R.drawable.icon_try_listen_pause);
        }

        @Override
        public void onError(String s) {
            LogUtil.d(TAG, "onError :" + s);
            mIv_try_listen.setImageResource(R.drawable.icon_try_listen);
        }

        @Override
        public void onPlaying(long l) {
            LogUtil.d(TAG, "onPlaying :" + l);
            mIv_try_listen.setImageResource(R.drawable.icon_try_listen_pause);
        }
    };


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_record:

                break;
            case R.id.iv_retry_record:
                audioState = AudioRecordStatus.STATE_RECORD_NORMAL;
                showByState(audioState);
                if (null != recorder) {
                    recorder.destroyAudioRecorder();
                    recorder = null;
                }
                break;
            case R.id.iv_try_listen:
                if (!audioManager.isPlaying()) {
                    if (null != audioFile && audioFile.exists()) {
                        audioPlayer.setDataSource(audioFile.getPath());
                        audioManager.play();
                        mIv_try_listen.setImageResource(R.drawable.icon_try_listen_pause);
                    }
                } else {
                    audioManager.stopPlay();
                    mIv_try_listen.setImageResource(R.drawable.icon_try_listen);
                }
                break;
            case R.id.iv_record_save:
                if (null != audioFile) {
                    getDialogManager().showProgressDialog(AudioRecordActivity.this, "请稍后...");
                    CoreManager.getCore(IFileCore.class).upload(audioFile);
                }
                break;
            default:
        }
    }

    long value = -1;

    Chronometer.OnChronometerTickListener chronometerTickListener = new Chronometer.OnChronometerTickListener() {

        @Override
        public void onChronometerTick(Chronometer chronometer) {
            if (value == -1) {
                // chronometer.setBase(0); // the base time value
                value = chronometer.getBase();
            } else {
                value++; // timer add
            }
            if (value > 10) {
                audioManager.stopRecord(false);
                return;
            }

            String time;
            if (value < 10) {
                time = "00:0" + value;
            } else {
                time = "00:" + value;
            }
            chronometer.setText(time);
        }
    };

    @Override
    protected void onDestroy() {
        if (audioManager.isPlaying()) {
            audioManager.stopPlay();
        }
        if (onPlayListener != null) {
            onPlayListener = null;
        }
        if (audioPlayer != null) {
            audioPlayer.setOnPlayListener(null);
        }
        if (audioManager != null) {
            audioManager.release();
        }
        super.onDestroy();
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUpload(String url) {
        audioUrl = url;
        UserInfo user = new UserInfo();
        user.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        user.setUserVoice(audioUrl);
        user.setVoiceDura(audioDura);
        CoreManager.getCore(IUserCore.class).requestUpdateUserInfo(user);
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUploadFail() {
        toast("上传失败");
        getDialogManager().dismissDialog();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfoUpdate(UserInfo userInfo) {
        audioState = AudioRecordStatus.STATE_RECORD_NORMAL;
        showByState(audioState);
        getDialogManager().dismissDialog();

        Intent intent = new Intent();
        intent.putExtra(AUDIO_FILE, audioUrl);
        intent.putExtra(AUDIO_DURA, audioDura);
        setResult(RESULT_OK, intent);
        finish();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfoUpdateError(String error) {
        toast(error);
        getDialogManager().dismissDialog();
    }


}
