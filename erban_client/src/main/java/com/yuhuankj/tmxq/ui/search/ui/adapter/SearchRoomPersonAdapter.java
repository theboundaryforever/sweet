package com.yuhuankj.tmxq.ui.search.ui.adapter;

import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.xchat_core.room.bean.SearchRoomPersonInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.widget.SquareImageView;

/**
 * Created by chenran on 2017/10/3.
 */

public class SearchRoomPersonAdapter extends BaseQuickAdapter<SearchRoomPersonInfo,SearchRoomPersonAdapter.ViewHolder> {

    private Context context;

    public SearchRoomPersonAdapter(Context context) {
        super(R.layout.list_item_search);
        this.context = context;
    }

    @Override
    protected void convert(ViewHolder helper, final SearchRoomPersonInfo item) {
        helper.userName.setText(item.getNick());
        if (GlideContextCheckUtil.checkContextUsable(helper.avatar.getContext())) {
            ImageLoadUtils.loadSmallRoundBackground(helper.avatar.getContext(), item.getAvatar(), helper.avatar);
        }
        if (!item.isValid()) {
            helper.roomTitle.setVisibility(View.GONE);
        } else {
            helper.roomTitle.setVisibility(View.VISIBLE);
            helper.roomTitle.setText(item.getTitle());
        }

        helper.erbanNo.setText("ID:" + item.getErbanNo());

        helper.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClick != null) {
                    onItemClick.onItemClicked(item);
                }
            }
        });
    }

    private OnSearchAdapterItemClickListener onItemClick;

    public void setOnItemClick(OnSearchAdapterItemClickListener onItemClick){
        this.onItemClick = onItemClick;
    }

    static class ViewHolder extends BaseViewHolder {
        SquareImageView avatar;
        TextView userName;
        TextView roomTitle;
        TextView erbanNo;
        RelativeLayout container;

        public ViewHolder(View itemView) {
            super(itemView);
            avatar = itemView.findViewById(R.id.avatar);
            userName = itemView.findViewById(R.id.user_name);
            roomTitle = itemView.findViewById(R.id.room_title);
            erbanNo = itemView.findViewById(R.id.erban_no);
            container = itemView.findViewById(R.id.container);
        }
    }

    public interface OnSearchAdapterItemClickListener{
        void onItemClicked(SearchRoomPersonInfo homeRoom);
    }
}
