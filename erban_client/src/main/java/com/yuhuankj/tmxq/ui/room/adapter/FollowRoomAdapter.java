package com.yuhuankj.tmxq.ui.room.adapter;


import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FollowRoomAdapter extends BaseQuickAdapter<Json, FollowRoomAdapter.ViewHolder> {
    public FollowRoomAdapter() {
        super(R.layout.item_follow_room);
    }

    @Override
    protected void convert(ViewHolder viewHolder, Json json) {
        try {
            viewHolder.tvFollowListName.setText(json.str("title"));
            int onlineNum = json.num("onlineNum", 0);
            viewHolder.tvFollowListUserCount.setText(viewHolder.tvFollowListUserCount.getContext().getResources().getString(
                    R.string.follow_user_online_num, onlineNum));
            if (0 == onlineNum) {
                viewHolder.iv_enterRoom.setVisibility(View.INVISIBLE);
                if (GlideContextCheckUtil.checkContextUsable(viewHolder.iv_onLineAnim.getContext())) {
                    Glide.with(viewHolder.iv_onLineAnim.getContext()).load(R.drawable.icon_attention_room_online_zero).into(viewHolder.iv_onLineAnim);
                }
            } else {
                viewHolder.iv_enterRoom.setVisibility(View.VISIBLE);
                if (GlideContextCheckUtil.checkContextUsable(viewHolder.iv_onLineAnim.getContext())) {
                    Glide.with(viewHolder.iv_onLineAnim.getContext()).asGif().load(R.drawable.anim_follow_room_online).into(viewHolder.iv_onLineAnim);
                }
            }

            String avatar = json.str("avatar");
            if (!TextUtils.isEmpty(avatar) && GlideContextCheckUtil.checkContextUsable(viewHolder.ivFollowListUserIcon.getContext())) {
                ImageLoadUtils.loadCustomCornerImage(viewHolder.ivFollowListUserIcon.getContext(),
                        ImageLoadUtils.toThumbnailUrl(DisplayUtility.dp2px(viewHolder.ivFollowListUserIcon.getContext(), 76),
                                DisplayUtility.dp2px(viewHolder.ivFollowListUserIcon.getContext(), 76), avatar),
                        viewHolder.ivFollowListUserIcon, DisplayUtility.dp2px(viewHolder.ivFollowListUserIcon.getContext(), 15));
            }
            String roomPwd = json.str("roomPwd");
            if (TextUtils.isEmpty(roomPwd)) {
                viewHolder.vRoomLock.setVisibility(View.GONE);
            } else {
                viewHolder.vRoomLock.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    static class ViewHolder extends BaseViewHolder {
        //增加Nullable注解解决闪退:
        //Caused by: java.lang.IllegalStateException: Required view 'iv_follow_list_user_icon'
        // with ID 2131296888 for field 'ivFollowListUserIcon' was not found.
        // If this view is optional add '@Nullable' (fields) or '@Optional' (methods) annotation.
        @Nullable
        @BindView(R.id.ivFollowListUserIcon)
        ImageView ivFollowListUserIcon;
        @Nullable
        @BindView(R.id.tvFollowListUserCount)
        TextView tvFollowListUserCount;
        @Nullable
        @BindView(R.id.tvFollowListName)
        TextView tvFollowListName;
        @Nullable
        @BindView(R.id.iv_onLineAnim)
        ImageView iv_onLineAnim;
        @Nullable
        @BindView(R.id.iv_enterRoom)
        ImageView iv_enterRoom;
        @BindView(R.id.vRoomLock)
        View vRoomLock;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


}
