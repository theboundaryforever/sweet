package com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMemberComeInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.listener.AnimationListener;
import com.yuhuankj.tmxq.widget.LevelView;

/**
 * 创建者      Created by dell
 * 创建时间    2019/3/18
 * 描述        房间内谁来了的封装view
 *
 * @author dell
 */
public class ComingMsgView extends LinearLayout {

    private LevelView levelView;
    private TextView tvUserName, tvMsgCar;
    private SvgaShowListener listener;

    public ComingMsgView(Context context) {
        this(context, null);
    }

    public ComingMsgView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ComingMsgView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialView();
    }

    private void initialView() {
        setGravity(Gravity.CENTER_VERTICAL);
        inflate(getContext(), R.layout.layout_coming_msg, this);
        tvUserName = findViewById(R.id.tv_user_name);
        levelView = findViewById(R.id.level_view);
        tvMsgCar = findViewById(R.id.tv_msg_car);
    }

    public void setupView(IMRoomMemberComeInfo comeInfo) {
        if (comeInfo != null) {
            //展示用户的座驾动画
            if (StringUtils.isNotEmpty(comeInfo.getCarImgUrl()) && listener != null) {
                listener.onShow(comeInfo.getCarImgUrl());
            }
            //展示用户等级与背景
            int experLevel = comeInfo.getExperLevel();
            if (experLevel >= 10) {
                levelView.setExperLevel(experLevel);
                int enterAnimBgResId = 0;
                if (experLevel >= 91) {
                    enterAnimBgResId = R.drawable.ic_enter_room_anima_bg100;
                } else if (experLevel >= 81) {
                    enterAnimBgResId = R.drawable.ic_enter_room_anima_bg90;
                } else if (experLevel >= 71) {
                    enterAnimBgResId = R.drawable.ic_enter_room_anima_bg80;
                } else if (experLevel >= 61) {
                    enterAnimBgResId = R.drawable.ic_enter_room_anima_bg70;
                } else if (experLevel >= 51) {
                    enterAnimBgResId = R.drawable.ic_enter_room_anima_bg60;
                } else if (experLevel >= 41) {
                    enterAnimBgResId = R.drawable.ic_enter_room_anima_bg50;
                } else if (experLevel >= 31) {
                    enterAnimBgResId = R.drawable.ic_enter_room_anima_bg40;
                } else if (experLevel >= 21) {
                    enterAnimBgResId = R.drawable.ic_enter_room_anima_bg30;
                } else if (experLevel >= 11) {
                    enterAnimBgResId = R.drawable.ic_enter_room_anima_bg20;
                } else if (experLevel >= 10) {
                    enterAnimBgResId = R.drawable.ic_enter_room_anima_bg10;
                } else {
                    enterAnimBgResId = R.drawable.shape_left_white66;
                }
                setBackgroundResource(enterAnimBgResId);
                String nick = comeInfo.getNickName();
                if (!TextUtils.isEmpty(nick) && nick.length() > 6) {
                    nick = getContext().getResources().getString(R.string.nick_length_max_six, nick.substring(0, 6));
                }
                tvUserName.setText(nick);
                String carName = comeInfo.getCarName();
                tvMsgCar.setText(StringUtils.isNotEmpty(carName) ? getContext().getString(R.string.come_msg_tip_car,
                        carName) : getContext().getString(R.string.come_msg_tip_not_car));
            }
        }
    }

    public void setSvgaListener(SvgaShowListener listener) {
        this.listener = listener;
    }

    /**
     * 做弹幕动画
     */
    public void startAnimation(AnimationListener listener) {
        float inInterval = 0.16f;
        float inSpeed = 3f;
        float inValue = inInterval * inSpeed;

        float middleInterval = 0.95f;
        float middleSpeed = 0.06f;
        float middleValue = inValue + (middleInterval - inInterval) * middleSpeed;

        float outSpeed = 2.65f;
        TranslateAnimation animation = new TranslateAnimation(ScreenUtil.getDisplayWidth(), -getContext().getResources()
                .getDimensionPixelOffset(R.dimen.layout_come_msg_width), 0, 0);
        animation.setDuration(4000);
        animation.setInterpolator(v -> {
            if (v < inInterval) {
                return inSpeed * v;
            } else if (v < middleInterval) {
                return inValue + middleSpeed * (v - inInterval);
            } else {
                return middleValue + outSpeed * (v - middleInterval);
            }
        });
        animation.setAnimationListener(listener);
        startAnimation(animation);
    }

    public interface SvgaShowListener {

        /**
         * 是否展示用户的svg座驾动画
         */
        void onShow(String svgUrl);
    }
}
