package com.yuhuankj.tmxq.ui.quickmating;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;

import java.util.Map;

public class QuickMatingModel {

    //获取匹配基本信息
    public void getQMInfo(HttpRequestCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().postRequest(UriProvider.getQMInfo(), params, callBack);
    }

    //交友匹配接口
    //性别(1.男2.女 0.不限)
    public void quickMating(int gender, HttpRequestCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("gender", String.valueOf(gender));
        OkHttpManager.getInstance().postRequest(UriProvider.quickMating(), params, callBack);
    }

    //取消交友匹配接口
    public void cancelQuickMating(HttpRequestCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().postRequest(UriProvider.cancelQuickMating(), params, callBack);
    }

    //结束交友匹配连麦

    /**
     *
     * @param recordId
     * @param operate  1 手动挂断 2 时间结束
     * @param callBack
     */
    public void finishQuickMating(int recordId,int operate, HttpRequestCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("recordId", String.valueOf(recordId));
        params.put("operate", String.valueOf(operate));
        OkHttpManager.getInstance().postRequest(UriProvider.finishQuickMating(), params, callBack);
    }

    /**
     * 关注或取消关注
     *
     * @param likeUid
     * @param type    1:关注 2:取消关注
     */
    public void attention(long likeUid, int type, HttpRequestCallBack httpRequestCallBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("type", String.valueOf(type));
        params.put("source", "2");
        params.put("likedUid", String.valueOf(likeUid));
        OkHttpManager.getInstance().postRequest(UriProvider.praise(), params, httpRequestCallBack);
    }

    //交友互动接口 操作类型（1送花、2表情、3剪刀石头、4话题夹）
    public void quickMatingAction(int type, int recordId, String content, HttpRequestCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("type", String.valueOf(type));
        params.put("recordId", String.valueOf(recordId));
        params.put("content", content);
        OkHttpManager.getInstance().postRequest(UriProvider.quickMatingAction(), params, callBack);
    }

    //续费接口
    public void quickMatingRenew(int recordId, HttpRequestCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("recordId", String.valueOf(recordId));
        OkHttpManager.getInstance().postRequest(UriProvider.quickMatingRenew(), params, callBack);
    }

    //点赞接口，1点赞，2取消点赞
    public void like(long admireUid, int type, HttpRequestCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("admireUid", String.valueOf(admireUid));
        params.put("source", "2");
        params.put("type", String.valueOf(type));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket() + "");
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        OkHttpManager.getInstance().postRequest(UriProvider.getAdmireUserUrl(), params, callBack);
    }
}
