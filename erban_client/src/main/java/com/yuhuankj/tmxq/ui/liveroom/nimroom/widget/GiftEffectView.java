package com.yuhuankj.tmxq.ui.liveroom.nimroom.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.opensource.svgaplayer.SVGACallback;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGADynamicEntity;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tencent.bugly.crashreport.BuglyLog;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.JavaUtil;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.ResolutionUtils;
import com.tongdaxing.erban.libcommon.utils.RichTextUtil;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.me.noble.NobleBusinessManager;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.widget.CircleImageView;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.UIUtil;

import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by chenran on 2017/10/8.
 */

public class GiftEffectView extends RelativeLayout implements SVGACallback {

    private final String TAG = GiftEffectView.class.getSimpleName();

    private RelativeLayout container;
    private SVGAImageView svgaImageView;
    public View svgaBg;
    private ImageView giftLightBg;
    private ImageView giftImg;
    private ImageView imgBg;
    private CircleImageView benefactorAvatar;
    private CircleImageView receiverAvatar;
    private TextView benefactorNick;
    private TextView receiverNick;
    private TextView giftNumber;
    private TextView giftName;
    private GiftEffectListener giftEffectListener;
    private EffectHandler effectHandler;
    private boolean isAnim;
    private LinearLayout benfactor;
    private TextView giveText;
    private ImageView ivSend;
    private LinearLayout receiverContainer;
    private ImageView whiteLightBg;
    private TextView tvSupergiftInfo;

    private TextView tvRoomInfo;

    public interface GiftEffectListener {
        void onGiftEffectEnd();
    }

    public boolean isAnim() {
        return isAnim;
    }

    public GiftEffectView(Context context) {
        super(context);
        init(context);
    }

    public GiftEffectView(Context context, AttributeSet attr) {
        super(context, attr);
        init(context);
    }

    public GiftEffectView(Context context, AttributeSet attr, int i) {
        super(context, attr, i);
        init(context);
    }

    public void setGiftEffectListener(GiftEffectListener giftEffectListener) {
        this.giftEffectListener = giftEffectListener;
    }

    private Bitmap bitmap1, bitmap2;
    private SimpleTarget simpleTarget1 = new SimpleTarget<Bitmap>() {
        @Override
        public void onResourceReady(Bitmap resource, Transition transition) {
            bitmap1 = resource;
            LogUtils.d(TAG, "simpleTarget1-onResourceReady1");
            loadCircleBitmapToSVGAAnim();
        }
    };

    private SVGAVideoEntity lastVideoEntity;
    private int lastAnimFrame = 0;
    private SimpleTarget simpleTarget2 = new SimpleTarget<Bitmap>() {
        @Override
        public void onResourceReady(Bitmap resource, Transition transition) {
            bitmap2 = resource;
            LogUtils.d(TAG, "simpleTarget2-onResourceReady2");
            loadCircleBitmapToSVGAAnim();
        }
    };
    private SVGADrawable svgaDrawable;

    private int superGiftViewWidth = 0;

    private void init(Context context) {
        inflate(context, R.layout.layout_gift_effect, this);
        effectHandler = new EffectHandler(this);
        superGiftViewWidth = DisplayUtility.dp2px(BasicConfig.INSTANCE.getAppContext(), 375);
        container = findViewById(R.id.container);
        imgBg = findViewById(R.id.img_bg);
        giftLightBg = findViewById(R.id.gift_light_bg);
        giftImg = findViewById(R.id.gift_img);
        benefactorAvatar = findViewById(R.id.benefactor_avatar);
        receiverAvatar = findViewById(R.id.receiver_avatar);
        benefactorNick = findViewById(R.id.benefactor_nick);
        receiverNick = findViewById(R.id.receiver_nick);
        giftNumber = findViewById(R.id.gift_number);
        giftName = findViewById(R.id.gift_name);
        svgaImageView = findViewById(R.id.svga_imageview);

        benfactor = findViewById(R.id.benefactor_container);
        giveText = findViewById(R.id.give_text);
        ivSend = findViewById(R.id.iv_send);
        receiverContainer = findViewById(R.id.receiver_container);
        whiteLightBg = findViewById(R.id.white_light_bg);
        tvSupergiftInfo = findViewById(R.id.tv_super_gift_info);

        tvRoomInfo = findViewById(R.id.tvRoomInfo);

        svgaImageView.setCallback(this);
        svgaImageView.setClearsAfterStop(true);
        svgaImageView.setLoops(1);
        svgaBg = findViewById(R.id.svga_imageview_bg);
    }

    public void startGiftEffect(GiftReceiveInfo giftRecieveInfo) {
        LogUtils.d(TAG, "startGiftEffect giftRecieveInfo:" + giftRecieveInfo);
        if (null != giftRecieveInfo) {
            BuglyLog.d(TAG, "startGiftEffect-giftId:" + giftRecieveInfo.getGiftId());
        } else {
            LogUtils.e("startGiftEffect giftRecieveInfo is null");
            return;
        }

        this.isAnim = true;
        GiftInfo giftInfo = null;
        if (giftRecieveInfo.getRealGiftId() > 0) {
            giftInfo = CoreManager.getCore(IGiftCore.class).findGiftInfoById(giftRecieveInfo.getRealGiftId());
        } else {
            giftInfo = CoreManager.getCore(IGiftCore.class).findGiftInfoById(giftRecieveInfo.getGiftId());
        }

        if (giftInfo != null) {
            //如果有roomId是大礼物，全服特效
            //这个roomId其实是送礼物的人的uid
            //userNo是送礼物的房间  显示的ID号
            final String roomId = giftRecieveInfo.getRoomId();
            if (TextUtils.isEmpty(roomId)) {
                container.setOnClickListener(null);
                benfactor.setVisibility(VISIBLE);
//                giveText.setVisibility(VISIBLE);
                ivSend.setVisibility(VISIBLE);
                receiverContainer.setVisibility(VISIBLE);
                whiteLightBg.setVisibility(VISIBLE);
                tvSupergiftInfo.setVisibility(GONE);
            } else {
                container.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //即将要跳转的房间类型
                        int roomType = giftRecieveInfo.getRoomType();
                        LogUtils.d("startGiftEffect", "roomId:" + roomId + " roomType:" + roomType);
                        if (null != BaseRoomServiceScheduler.getCurrentRoomInfo() &&
                                BaseRoomServiceScheduler.getCurrentRoomInfo().getUid() == JavaUtil.str2long(roomId)
                                && roomType == BaseRoomServiceScheduler.getCurrentRoomInfo().getType()) {
                            //当前房间就不要跳了，避免需要走先finish后重新进房的流程
                            LogUtils.d("startGiftEffect", "当前房间不做跳转");
                            return;
                        }
                        RoomServiceScheduler.getInstance().enterOtherTypeRoomFromService(
                                getContext(), JavaUtil.str2long(roomId), roomType);
                    }
                });
                benfactor.setVisibility(INVISIBLE);
                giveText.setVisibility(INVISIBLE);
                ivSend.setVisibility(INVISIBLE);
                receiverContainer.setVisibility(INVISIBLE);
                whiteLightBg.setVisibility(INVISIBLE);
                tvSupergiftInfo.setVisibility(VISIBLE);

                String nick = giftRecieveInfo.getNick();
                if (!TextUtils.isEmpty(nick) && nick.length() > 10) {
                    nick = giveText.getResources().getString(R.string.nick_length_max_six, nick.substring(0, 10));
                }
                String targetNick = giftRecieveInfo.getTargetNick();
                if (!TextUtils.isEmpty(targetNick) && targetNick.length() > 10) {
                    targetNick = giveText.getResources().getString(R.string.nick_length_max_six, targetNick.substring(0, 10));
                }
                List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
                HashMap<String, Object> map;
                map = new HashMap<String, Object>();
                map.put(RichTextUtil.RICHTEXT_STRING, nick);
                map.put(RichTextUtil.RICHTEXT_COLOR, NobleBusinessManager.getNobleRoomNickColor(
                        giftRecieveInfo.getVipId(), giftRecieveInfo.getVipDate(),
                        giftRecieveInfo.isInvisible(), Color.BLACK, nick));
                list.add(map);

                map = new HashMap<String, Object>();
                map.put(RichTextUtil.RICHTEXT_STRING, "在");
                map.put(RichTextUtil.RICHTEXT_COLOR, Color.WHITE);
                list.add(map);

                map = new HashMap<String, Object>();
                map.put(RichTextUtil.RICHTEXT_STRING, "ID" + giftRecieveInfo.getUserNo());
                map.put(RichTextUtil.RICHTEXT_COLOR, Color.BLACK);
                list.add(map);

                map = new HashMap<String, Object>();
                map.put(RichTextUtil.RICHTEXT_STRING, "房间送给");
                map.put(RichTextUtil.RICHTEXT_COLOR, Color.WHITE);
                list.add(map);

                map = new HashMap<String, Object>();
                map.put(RichTextUtil.RICHTEXT_STRING, targetNick);
                map.put(RichTextUtil.RICHTEXT_COLOR, NobleBusinessManager.getNobleRoomNickColor(
                        giftRecieveInfo.getTargetVipId(), giftRecieveInfo.getTargetVipDate(),
                        giftRecieveInfo.isTargetIsInvisible(), Color.BLACK, targetNick));
                list.add(map);
                tvSupergiftInfo.setText(RichTextUtil.getSpannableStringFromList(list));
            }
            ImageLoadUtils.loadImage(benefactorAvatar.getContext(), giftRecieveInfo.getAvatar(), benefactorAvatar);
            ImageLoadUtils.loadImage(giftImg.getContext(), giftInfo.getGiftUrl(), giftImg);
            benefactorNick.setText(giftRecieveInfo.getNick());
            giftNumber.setText("x" + giftRecieveInfo.getGiftNum());
            if (TextUtils.isEmpty(roomId)) {
                giftNumber.setTextColor(Color.parseColor("#Fee800"));
            } else {
                giftNumber.setTextColor(Color.BLACK);
            }
            giftName.setText(giftInfo.getGiftName());
            container.setVisibility(VISIBLE);

            if (!StringUtil.isEmpty(giftRecieveInfo.getTargetAvatar()) && !StringUtil.isEmpty(giftRecieveInfo.getNick())) {
                ImageLoadUtils.loadAvatar(receiverAvatar.getContext(), giftRecieveInfo.getTargetAvatar(), receiverAvatar);
                receiverNick.setText(giftRecieveInfo.getTargetNick());
            } else {
                receiverAvatar.setImageResource(R.mipmap.ic_launcher);
                receiverNick.setText("全麦");
            }

            Animation operatingAnim = AnimationUtils.loadAnimation(getContext(), R.anim.light_bg_rotate_anim);
            LinearInterpolator lin = new LinearInterpolator();
            operatingAnim.setInterpolator(lin);
            giftLightBg.setAnimation(operatingAnim);

            final Point center = new Point();
            center.x = ResolutionUtils.getScreenWidth(getContext()) / 2;
            ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(container, "translationX",
                    -UIUtil.dip2px(getContext(), 400), center.x - superGiftViewWidth / 2).setDuration(500);
            objectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
            objectAnimator.start();

            ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(container, "alpha", 0.0F, 1.0F).setDuration(500);
            objectAnimator1.setInterpolator(new AccelerateDecelerateInterpolator());
            objectAnimator1.start();


            int totalCoin = giftInfo.getGoldPrice() * giftRecieveInfo.getGiftNum();
            if (giftRecieveInfo.getPersonCount() > 0) {
                totalCoin = giftInfo.getGoldPrice() * giftRecieveInfo.getGiftNum() * giftRecieveInfo.getPersonCount();
            }
            tvRoomInfo.setVisibility(GONE);
            if (totalCoin >= 30000) {
                RelativeLayout.LayoutParams params = (LayoutParams) container.getLayoutParams();
                params.height = ScreenUtil.dip2px(200);
                container.requestLayout();

                RelativeLayout.LayoutParams params1 = (LayoutParams) ivSend.getLayoutParams();
                params1.topMargin = ScreenUtil.dip2px(80);
                ivSend.requestLayout();

                benfactor.setVisibility(VISIBLE);
                ivSend.setVisibility(VISIBLE);
                receiverContainer.setVisibility(VISIBLE);
                whiteLightBg.setVisibility(VISIBLE);
                tvSupergiftInfo.setVisibility(GONE);
                if (TextUtils.isEmpty(roomId)) {
                    //如果当前房间是发送礼物的房间
                    imgBg.setImageResource(R.drawable.icon_gift_effect_bg_6);

                } else {
                    //如果是其它房间
                    String title = giftRecieveInfo.getTitle();
                    if (title == null) {
                        title = "";
                    }
                    if (title.length() > 4) {
                        title = title.substring(0, 4) + "...";
                    }
                    String ID = giftRecieveInfo.getUserNo();
                    if (ID == null) {
                        ID = "";
                    }
                    tvRoomInfo.setText(title + "(ID" + ID + ")");
                    tvRoomInfo.setVisibility(VISIBLE);
                    imgBg.setImageResource(R.drawable.icon_gift_effect_bg_5);
                }
            } else if (!TextUtils.isEmpty(roomId)) {
                imgBg.setImageResource(R.drawable.icon_gift_effect_bg_4);
            } else if (totalCoin >= 520 && totalCoin < 4999) {
                imgBg.setImageResource(R.drawable.icon_gift_effect_bg_1);
            } else if (totalCoin >= 4999 && totalCoin < 9999) {
                imgBg.setImageResource(R.drawable.icon_gift_effect_bg_2);
            } else if (totalCoin >= 9999 && totalCoin < 30000) {
                imgBg.setImageResource(R.drawable.icon_gift_effect_bg_3);
            }

            effectHandler.sendEmptyMessageDelayed(0, 6000);
            if (giftInfo.isHasEffect() && !StringUtil.isEmpty(giftInfo.getVggUrl()) && TextUtils.isEmpty(roomId)) {
                try {
                    drawSvgaEffect(giftInfo.getGiftId(), giftInfo.getVggUrl(),
                            giftRecieveInfo.getAvatar(), giftRecieveInfo.getTargetAvatar());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /***
     * 播放座驾特效（座驾特效和进房动画是同步处理的:避免卡段问题现在延迟200毫秒播放特效）
     * @param svga
     */
    public void playRoomCarSvgaAnim(String svga){
        Message message = new Message();
        message.what = 1;
        Bundle bundle = new Bundle();
        bundle.putString("svga",svga);
        message.setData(bundle);
        effectHandler.sendMessageDelayed(message,200);
    }

    /**
     * 礼物全屏特效
     *
     * @param url
     * @param giftId
     * @throws MalformedURLException
     */
    public void drawSvgaEffect(final int giftId, String url, String avatar, String targetAvatar) {
        try {
            SVGAParser parser = new SVGAParser(getContext());
            parser.decodeFromURL(new URL(url), new SVGAParser.ParseCompletion() {
                @Override
                public void onComplete(@NonNull SVGAVideoEntity videoItem) {
                    loadSvgaAnim(videoItem, giftId, avatar, targetAvatar);
                }

                @Override
                public void onError() {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadSvgaAnim(@NonNull SVGAVideoEntity videoItem, int giftId, String avatar, String targetAvatar) {
        LogUtils.d(TAG, "loadSvgaAnim-giftId:" + giftId + " avatar:" + avatar + " targetAvatar:" + targetAvatar);
        lastVideoEntity = videoItem;
        lastAnimFrame = 0;
        bitmap1 = null;
        bitmap2 = null;
        svgaImageView.setVisibility(View.VISIBLE);
        RequestOptions requestOptions = RequestOptions.bitmapTransform(new CircleCrop())
                .diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(false);
        if (giftId == 126) {
            if (null != svgaImageView && GlideContextCheckUtil.checkContextUsable(svgaImageView.getContext())) {
                Glide.with(svgaImageView.getContext()).asBitmap().load(avatar)
                        .apply(requestOptions)
                        .into(simpleTarget1);
                Glide.with(svgaImageView.getContext()).asBitmap().load(targetAvatar)
                        .apply(requestOptions)
                        .into(simpleTarget2);
            }
        } else {
            SVGADrawable drawable = new SVGADrawable(videoItem);
            svgaImageView.setImageDrawable(drawable);
            svgaImageView.startAnimation();
            svgaBg.setVisibility(VISIBLE);
            ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(svgaBg, "alpha", 0.0F, 2.0F).setDuration(800);
            objectAnimator1.setInterpolator(new AccelerateDecelerateInterpolator());
            objectAnimator1.start();
        }

    }

    private void loadCircleBitmapToSVGAAnim() {
        LogUtils.d(TAG, "loadCircleBitmapToSVGAAnim");
        if (null != bitmap1 && null != bitmap2 && null != lastVideoEntity) {
            if (null == svgaDrawable) {
                SVGADynamicEntity dynamicItem1 = new SVGADynamicEntity();
                svgaDrawable = new SVGADrawable(lastVideoEntity, dynamicItem1);
                svgaImageView.setImageDrawable(svgaDrawable);
            }
            SVGADynamicEntity dynamicItem2 = svgaDrawable.getDynamicItem();
            dynamicItem2.setDynamicImage(bitmap1, "nan");
            dynamicItem2.setDynamicImage(bitmap2, "nv");
            svgaImageView.startAnimation();
        }
    }

    private void deleteAnim() {
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(container, "translationX",
                container.getX(), ResolutionUtils.getScreenWidth(getContext())).setDuration(500);
        objectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        objectAnimator.start();

        ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(container, "alpha",
                1.0F, 0.0F).setDuration(500);
        objectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        objectAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                if (giftEffectListener != null) {
                    isAnim = false;
                    //隐藏container
                    container.setVisibility(View.GONE);
                    giftEffectListener.onGiftEffectEnd();
                }
            }
        });
        objectAnimator1.start();
    }

    private static class EffectHandler extends Handler {
        private WeakReference<GiftEffectView> effectViewWeakReference;

        public EffectHandler(GiftEffectView giftEffectView) {
            effectViewWeakReference = new WeakReference<>(giftEffectView);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (effectViewWeakReference != null && effectViewWeakReference.get() != null) {
                switch (msg.what) {
                    case 0:
                        effectViewWeakReference.get().deleteAnim();
                        break;
                    case 1:
                        String svga = msg.getData().getString("svga","");
                        if (StringUtils.isNotEmpty(svga)) {
                            effectViewWeakReference.get().drawSvgaEffect(0, svga, null, null);
                        }
                        break;
                }
            }
        }
    }

    public void release() {
        effectHandler.removeMessages(0);
        lastVideoEntity = null;
        lastAnimFrame = 0;
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onFinished() {
        lastVideoEntity = null;
        svgaBg.setVisibility(GONE);
        svgaImageView.setVisibility(View.GONE);
        bitmap1 = null;
        bitmap2 = null;
    }

    @Override
    public void onRepeat() {

    }

    @Override
    public void onStep(int i, double v) {
        lastAnimFrame = i;
    }
}
