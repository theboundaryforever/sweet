package com.yuhuankj.tmxq.ui.me.wallet.bills.adapter;

import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.libcommon.utils.TimeUtils;
import com.tongdaxing.xchat_core.bills.bean.BillItemEntity;
import com.tongdaxing.xchat_core.bills.bean.IncomeInfo;
import com.yuhuankj.tmxq.R;

import java.util.List;

/**
 * IncomeInfo
 * Created by Seven on 2017/9/17.
 */

public class WithdrawBillsAdapter extends BillBaseAdapter {

    public WithdrawBillsAdapter(List<BillItemEntity> billItemEntityList) {
        super(billItemEntityList);
        addItemType(BillItemEntity.ITEM_NORMAL, R.layout.list_withdraw_bills_item);
    }

    @Override
    public void convertNormal(BaseViewHolder baseViewHolder, BillItemEntity billItemEntity) {
        IncomeInfo incomeInfo = billItemEntity.mWithdrawInfo;
        if (incomeInfo == null) return;
        baseViewHolder.setText(R.id.tv_date, TimeUtils.getYearMonthDayHourMinuteSecond(incomeInfo.getRecordTime()))
                .setText(R.id.tv_diamondNum, "提现" + incomeInfo.getDiamondNum() + "钻石")
                .setText(R.id.tv_money, "+" + incomeInfo.getMoney() + "元");
    }
}
