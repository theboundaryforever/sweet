package com.yuhuankj.tmxq.ui.login;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.databinding.ActivityRegisterBinding;

/**
 * Created by zhouxiangfeng on 17/3/5.
 */

public class RegisterActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = "RegisterActivity";
    private String errorStr;
    private String phone;
    private String psw;
    private ActivityRegisterBinding registerBinding;
    private CodeDownTimer timer;
    private View vClearText;
    private TextView tvRegister;
    private TextView tvForgetPwd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerBinding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        initTitleBar("注册");
        mTitleBar.setCommonBackgroundColor(Color.WHITE);
        onFindViews();
        onSetListener();
    }


    public void onFindViews() {
        vClearText = findViewById(R.id.vClearText);
        registerBinding.tvRegister.setVisibility(View.GONE);
        registerBinding.tvForgetPwd.setText("已有账号，前往登录");
    }

    public void onSetListener() {
        registerBinding.setClick(this);
        vClearText.setOnClickListener(this);
        registerBinding.tvForgetPwd.setOnClickListener(this);
        registerBinding.etPhone.setOnFocusChangeListener(focusChangeListener);
        registerBinding.etPhone.addTextChangedListener(textWatcher);

        registerBinding.etPhone.requestFocus();
        showIME(registerBinding.etPhone);

        //这里从广播接收
        IntentFilter filter = new IntentFilter(ACTION_FINISH_REGISTER);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, filter);
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onRegisterFail(String error) {
        toast(error);
        getDialogManager().dismissDialog();
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onRegister() {
        CoreManager.getCore(IAuthCore.class).login(phone, psw);
        getDialogManager().dismissDialog();
        startActivity(new Intent(this, SupplyInfo1Activity.class));
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onSmsFail(String error) {
        toast(error);
        if (timer != null) {
            timer.cancel();
            timer.onFinish();
        }
        LogUtils.d(TAG, "onSmsFail error:" + error);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
        }
        getDialogManager().dismissDialog();
        if (broadcastReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        }
    }

    public static final String ACTION_FINISH_REGISTER = "ACTION_FINISH_REGISTER";
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_FINISH_REGISTER.equals(intent.getAction())) {
                finish();
            }
        }
    };

    @Override
    public void onClick(View v) {
        phone = registerBinding.etPhone.getText().toString();
        switch (v.getId()) {
            case R.id.btn_regist:
                psw = registerBinding.etPassword.getText().toString();
                String sms_code = registerBinding.etCode.getText().toString();
                if (!StringUtils.isEmpty(sms_code)) {
                    if (isOK(phone, psw)) {
                        getDialogManager().showProgressDialog(RegisterActivity.this, "正在注册...");
                        CoreManager.getCore(IAuthCore.class).register(phone, sms_code, psw);
                    } else {
                        toast(errorStr);
                    }
                } else {
                    toast("验证码不能为空");
                }
                break;
            case R.id.btn_get_code:
                if (phone.length() == 11) {
                    timer = new CodeDownTimer(registerBinding.btnGetCode, 60000, 1000);
                    timer.start();
                    CoreManager.getCore(IAuthCore.class).requestSMSCode(phone, 1);
                    registerBinding.etPhone.clearFocus();
                    registerBinding.etCode.requestFocus();
                } else {
                    toast("手机号码不正确");
                }
                break;
            case R.id.vClearText:
                registerBinding.etPhone.setText("");
                break;
            case R.id.tvForgetPwd:
                startActivity(new Intent(this, LoginPhoneActivity.class));
                break;
            default:
                break;
        }
    }

    private boolean isOK(String phone, String psw) {
        if (StringUtils.isEmpty(psw)) {
            errorStr = "密码不能为空";
            return false;
        }
        if (psw.length() < 6) {
            errorStr = "请输入6~16位的密码";
            return false;
        }
        if (StringUtils.isEmpty(phone)) {
            errorStr = "请填写手机号码！";
            return false;
        }
        return true;
    }

    private View.OnFocusChangeListener focusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (hasFocus) {
                String nickName = registerBinding.etPhone.getText().toString();
                if (TextUtils.isEmpty(nickName) || nickName.length() == 0) {
                    vClearText.setVisibility(View.GONE);
                } else {
                    vClearText.setVisibility(View.VISIBLE);
                }
            } else {
                vClearText.setVisibility(View.GONE);
            }
        }
    };

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String nickName = registerBinding.etPhone.getText().toString();
            if (TextUtils.isEmpty(nickName) || nickName.length() == 0) {
                vClearText.setVisibility(View.GONE);
            } else {
                vClearText.setVisibility(View.VISIBLE);
            }
        }
    };
}
