package com.yuhuankj.tmxq.ui.widget;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.SparseArray;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.utils.toast.ToastCompat;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCoreClient;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yanzhenjie.recyclerview.swipe.SwipeMenu;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuBridge;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuCreator;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuItem;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuItemClickListener;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuRecyclerView;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.adapter.MicInListAdapter;
import com.yuhuankj.tmxq.utils.Utils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/4/26.
 */

public class MicInListDialog extends BottomSheetDialog {

    private Context context;
    @BindView(R.id.tv_mic_in_list_dialog_title)
    TextView tvMicInListDialogTitle;
    @BindView(R.id.rv_mic_in_list_dialog)
    SwipeMenuRecyclerView rvMicInListDialog;
    @BindView(R.id.bu_mic_in_list_dialog_submit)
    Button buMicInListDialogSubmit;

    TreeSet<Json> treeSet;

    List<Json> jsons = new ArrayList<>();

    private MicInListAdapter adapter;
    public boolean isAdmin = false;
    public boolean isRoomOwner = false;


    public MicInListDialog(@NonNull Context context) {
        super(context, R.style.ErbanBottomSheetDialog);
        this.context = context;
        treeSet = new TreeSet<Json>(new Comparator<Json>() {
            @Override
            public int compare(Json o1, Json o2) {
                long l1 = o1.num_l("time");
                long l2 = o2.num_l("time");
                if (l1 > l2) {
                    return 1;
                } else {
                    return -1;
                }


            }
        });

    }

    boolean isFullScreen = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_mic_in_list);
        findViewById(R.id.ll_dialog_mic_in_list).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display d = windowManager.getDefaultDisplay();
        DisplayMetrics realDisplayMetrics = new DisplayMetrics();
        d.getRealMetrics(realDisplayMetrics);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = (isFullScreen ? context.getResources().getDisplayMetrics().heightPixels : realDisplayMetrics.heightPixels) -
                (Utils.hasSoftKeys(context) ? Utils.getNavigationBarHeight(context) : 0);
        getWindow().setAttributes(params);


        ButterKnife.bind(this);

        setCanceledOnTouchOutside(true);

        initDeleteMenu();

        refreshData();
        buMicInListDialogSubmit.setVisibility(isRoomOwner ? View.GONE : View.VISIBLE);
        buMicInListDialogSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iSubmitAction != null) {
                    iSubmitAction.onSubmitClick();
                }


            }
        });

        //初始化之后再开始监听事件
        CoreManager.addClient(this);
    }

    private void initDeleteMenu() {
        if (!isAdmin) {
            return;
        }
        SwipeMenuCreator swipeMenuCreator = new SwipeMenuCreator() {
            @Override
            public void onCreateMenu(SwipeMenu swipeLeftMenu, SwipeMenu swipeRightMenu, int viewType) {
                SwipeMenuItem deleteItem = new SwipeMenuItem(getContext());
                deleteItem.setText("删除");
                deleteItem.setTextColor(Color.WHITE);
                deleteItem.setWidth(300);
                deleteItem.setHeight(-1);
                deleteItem.setBackgroundColor(Color.parseColor("#fd2772"));
                swipeRightMenu.addMenuItem(deleteItem);
            }
        };
        rvMicInListDialog.setSwipeMenuItemClickListener(new SwipeMenuItemClickListener() {
            @Override
            public void onItemClick(SwipeMenuBridge menuBridge) {

                menuBridge.closeMenu();
                LogUtils.d("setSwipeMenuItemClickListener", menuBridge.getAdapterPosition() + "  " + menuBridge.getPosition() + "   " + menuBridge.getDirection());
                int adapterPosition = menuBridge.getAdapterPosition();
                List<Json> data = adapter.getData();
                Json json = data.get(adapterPosition);
                if (json == null) {
                    return;
                }
                String uid = json.str("uid");
                RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                if (roomInfo != null && !TextUtils.isEmpty(uid)) {
                    IMNetEaseManager.get().removeMicInList(uid, roomInfo.getRoomId() + "", null);
                }
            }
        });
        rvMicInListDialog.setSwipeMenuCreator(swipeMenuCreator);
    }

    public ISubmitAction iSubmitAction;

    public interface ISubmitAction {
        void onSubmitClick();
    }

    public void refreshData() {
        SparseArray<Json> mMicInListMap = AvRoomDataManager.get().mMicInListMap;

        boolean checkInMicInlist = AvRoomDataManager.get().checkInMicInlist();

        buMicInListDialogSubmit.setText(context.getResources().getString(
                checkInMicInlist ? R.string.room_mic_queue_cancel : R.string.room_mic_queue));

        treeSet.clear();
        for (int i = 0; i < mMicInListMap.size(); i++) {
            Json json = mMicInListMap.valueAt(i);
            treeSet.add(json);
        }

        jsons.clear();
        jsons.addAll(treeSet);
        tvMicInListDialogTitle.setText(context.getResources().getString(R.string.room_mic_queue_num, jsons.size()));
        rvMicInListDialog.setLayoutManager(new RecyclerViewNoBugLinearLayoutManager(context));
        if (adapter == null) {
            adapter = new MicInListAdapter(context, R.layout.item_mic_in_list, jsons);
            rvMicInListDialog.setAdapter(adapter);
            adapter.isAdmin = this.isAdmin;
        } else {
            adapter.isAdmin = this.isAdmin;
            adapter.setNewData(jsons);

        }

    }


    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        CoreManager.removeClient(this);
    }

    @CoreEvent(coreClientClass = IAVRoomCoreClient.class)
    public void onMicInListChange() {
        refreshData();
    }

    @CoreEvent(coreClientClass = IAVRoomCoreClient.class)
    public void micInListDismiss(String msg) {
        if (!TextUtils.isEmpty(msg)) {
            ToastCompat.makeText(getContext(), msg, ToastCompat.LENGTH_SHORT).show();
        }
        dismiss();
    }


}
