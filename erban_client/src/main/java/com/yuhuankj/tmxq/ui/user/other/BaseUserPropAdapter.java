package com.yuhuankj.tmxq.ui.user.other;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.yuhuankj.tmxq.R;

import java.util.List;

public abstract class BaseUserPropAdapter extends RecyclerView.Adapter<BaseUserPropAdapter.UserPropInfoHolder>  {


    public abstract void setUserPropInfoList(List<UserPropInfo> list);


    public class UserPropInfoHolder extends RecyclerView.ViewHolder {
        public ImageView giftPic;
        public TextView giftName;
        public TextView giftNum;

        public UserPropInfoHolder(View itemView) {
            super(itemView);
            giftPic = itemView.findViewById(R.id.gift_img);
            giftName = itemView.findViewById(R.id.gift_name);
            giftNum = itemView.findViewById(R.id.gift_num);
        }

    }
}
