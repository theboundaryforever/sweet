package com.yuhuankj.tmxq.ui.nim.recent;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;

import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.recent.RecentContactsCallback;
import com.netease.nim.uikit.recent.RecentContactsFragment;
import com.netease.nim.uikit.session.NIMSessionStatusManager;
import com.netease.nim.uikit.session.OfficSecrRedPointCountManager;
import com.netease.nim.uikit.session.activity.P2PMessageActivity;
import com.netease.nimlib.sdk.msg.MsgService;
import com.netease.nimlib.sdk.msg.attachment.AudioAttachment;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.RecentContact;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.MiniGameInvitedAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.NoticeAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RedPacketAttachment;
import com.tongdaxing.xchat_core.redpacket.bean.RedPacketInfoV2;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.fragment.BaseFragment;
import com.yuhuankj.tmxq.ui.nim.sysmsg.SysMsgActivity;

import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_LOTTERY;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_NOTICE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_OPEN_ROOM_NOTI;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PACKET;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_MINI_GAME;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_MINI_GAME_ON_INVITED_NOTIFY;

/**
 * 最近聊天列表
 *
 * @author chenran
 * @date 2017/9/18
 */
public class RecentListFragment extends BaseFragment {
    private RecentContactsFragment recentContactsFragment;

    private final String TAG = RecentListFragment.class.getSimpleName();

    @Override
    public void onFindViews() {
        recentContactsFragment = new RecentContactsFragment();
        recentContactsFragment.viewScene = RecentContactsFragment.ViewScene.MainMsgTab;
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.recent_container, recentContactsFragment).commitAllowingStateLoss();
    }

    @Override
    public void onSetListener() {

    }

    @Override
    public void initiate() {
        recentContactsFragment.setCallback(new RecentContactsCallback() {
            @Override
            public void onRecentContactsLoaded() {

            }

            @Override
            public void onUnreadCountChange(int unreadCount) {

            }

            @Override
            public void onItemClick(RecentContact recent) {
                if (recent.getSessionType() == SessionTypeEnum.Team) {
                    NimUIKit.startTeamSession(getActivity(), recent.getContactId());
                } else if (recent.getSessionType() == SessionTypeEnum.P2P) {
                    NimUIKit.startP2PSession(getActivity(), recent.getContactId());
                    if (OfficSecrRedPointCountManager.isOfficMsgAccount(recent.getContactId())) {
                        //如果是系统通知账号，跳转到系统通知页面
                        try {
                            startActivity(new Intent(getActivity(), SysMsgActivity.class));
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (getActivity() != null) {
                                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent(P2PMessageActivity.ACTION_FINISH));
                            }
                        }
                        OfficSecrRedPointCountManager.sysMsgNum = 0;
                    }
                }
                //新加点击后刷新，解决有时红点消失不了的问题
                recentContactsFragment.requestMessages(true);
                CoreManager.getCore(VersionsCore.class).requestSensitiveWord();
            }

            @Override
            public String getDigestOfAttachment(RecentContact recent, MsgAttachment attachment) {
                if (attachment instanceof CustomAttachment) {
                    CustomAttachment customAttachment = (CustomAttachment) attachment;
                    if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_OPEN_ROOM_NOTI) {
                        return "您关注的TA上线啦，快去围观吧~~~";
                    } else if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_GIFT) {
                        return "[礼物]";
                    } else if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_NOTICE) {
                        NoticeAttachment noticeAttachment = (NoticeAttachment) attachment;
                        return noticeAttachment.getTitle();
                    } else if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_PACKET) {
                        RedPacketAttachment redPacketAttachment = (RedPacketAttachment) attachment;
                        RedPacketInfoV2 redPacketInfo = redPacketAttachment.getRedPacketInfo();
                        if (redPacketInfo == null) {
                            return "您收到一个红包哦!";
                        }
                        return "您收到一个" + redPacketInfo.getPacketName() + "红包哦!";
                    } else if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_LOTTERY) {
                        return "恭喜您，获得抽奖机会";
                    } else if (customAttachment.getFirst() == CUSTOM_MSG_MINI_GAME) {
                        if (customAttachment.getSecond() == CUSTOM_MSG_MINI_GAME_ON_INVITED_NOTIFY) {
                            MiniGameInvitedAttachment miniGameInvitedAttachment = (MiniGameInvitedAttachment) attachment;
                            return miniGameInvitedAttachment.getDataInfo().getFromNick().concat("邀请你进入游戏");
                        }
                    }
                } else if (attachment instanceof AudioAttachment) {
                    return "[语音]";
                }
                return null;
            }

            @Override
            public String getDigestOfTipMsg(RecentContact recent) {
                return null;
            }
        });
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_recent_list;
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo userInfo) {
        recentContactsFragment.requestMessages(true);
    }

    private boolean mIsInitView;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIsInitView = true;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        LogUtils.d(TAG, "setUserVisibleHint-isVisibleToUser:" + isVisibleToUser + " mIsInitView:" + mIsInitView);
        if (!mIsInitView) {
            return;
        }
        if (isVisibleToUser) {
            LogUtils.d(TAG, "setUserVisibleHint-进入最近联系人列表界面");
            NIMSessionStatusManager.getInstance().updateChattingAccount(MsgService.MSG_CHATTING_ACCOUNT_ALL, SessionTypeEnum.None);
            if (recentContactsFragment != null) {
                recentContactsFragment.refreshList();
            }
        } else {
            LogUtils.d(TAG, "setUserVisibleHint-退出聊天界面或离开最近联系人列表界面");
            NIMSessionStatusManager.getInstance().updateChattingAccount(MsgService.MSG_CHATTING_ACCOUNT_NONE, SessionTypeEnum.None);
        }
    }

    public void setMsgViewHiddenToUser(boolean msgViewHiddenToUser) {
        if (null != recentContactsFragment) {
            recentContactsFragment.setMsgViewHiddenToUser(msgViewHiddenToUser);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIsInitView = false;
    }

    /**
     * 忽略未读消息（非全部已读）
     */
    public void ignoreUnreadMsg() {
        if (recentContactsFragment != null) {
            recentContactsFragment.ignoreUnreadMsg();
        }
    }
}
