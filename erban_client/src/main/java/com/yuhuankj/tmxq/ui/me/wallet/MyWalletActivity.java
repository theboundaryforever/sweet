package com.yuhuankj.tmxq.ui.me.wallet;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ButtonUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.ui.me.charge.ChargeActivity;
import com.yuhuankj.tmxq.ui.me.wallet.bills.TotalBillsActivity;
import com.yuhuankj.tmxq.ui.me.wallet.income.MyIncomeActivity;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;
import com.yuhuankj.tmxq.widget.TitleBar;

import java.util.Locale;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MadisonRong on 08/01/2018.
 */
@CreatePresenter(MyWalletPresenter.class)
public class MyWalletActivity extends BaseMvpActivity<IMyWalletView, MyWalletPresenter>
        implements IMyWalletView, View.OnClickListener {

    private static final String TAG = "MyWalletActivity";

    @BindView(R.id.tv_wallet_gold_num)
    TextView goldNumTextView;
    @BindView(R.id.rlCharge)
    RelativeLayout rlCharge;
    @BindView(R.id.rl_my_wallet_earnings)
    RelativeLayout earningsLayout;
    @BindString(R.string.my_wallet)
    String titleBarContent;
    @BindString(R.string.bill_title)
    String billTitle;
    @BindView(R.id.tvNormalMoney)
    TextView tvNormalMoney;
    @BindView(R.id.tvNobleMoney)
    TextView tvNobleMoney;
    @BindView(R.id.llNobleMoneyTip)
    LinearLayout llNobleMoneyTip;

    private TitleBar mTitleBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_wallet);
        ButterKnife.bind(this);
        initTitleBar(titleBarContent);
        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getMvpPresenter().loadWalletInfo();
    }

    @Override
    public void initTitleBar(String title) {
        mTitleBar = (TitleBar) findViewById(R.id.title_bar);
        if (mTitleBar != null) {
            mTitleBar.setTitle(title);
            mTitleBar.setImmersive(false);
            mTitleBar.setTitleColor(Color.WHITE);
            mTitleBar.setLeftImageResource(R.drawable.icon_white_back);
            mTitleBar.setLeftClickListener(v -> finish());

            mTitleBar.setActionTextColor(Color.WHITE);
            mTitleBar.setCommonBackgroundColor(Color.TRANSPARENT);
            mTitleBar.addAction(new TitleBar.TextAction(billTitle) {
                @Override
                public void performAction(View view) {
                    startActivity(new Intent(MyWalletActivity.this, TotalBillsActivity.class));
                }
            });
        }
    }

    private void initViews() {
        goldNumTextView.setOnClickListener(this);
        rlCharge.setOnClickListener(this);
        earningsLayout.setOnClickListener(this);
        llNobleMoneyTip.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick(view.getId())) {
            return;
        }
        getMvpPresenter().handleClick(view.getId());
    }

    @Override
    public void refreshUserWalletBalance(WalletInfo walletInfo) {
        goldNumTextView.setText(String.format(Locale.getDefault(), "%.2f", walletInfo.goldNum));
        tvNormalMoney.setText(String.format(Locale.getDefault(), "%.2f", walletInfo.chargeGoldNum));
        tvNobleMoney.setText(String.format(Locale.getDefault(), "%.2f", walletInfo.nobleGoldNum));
    }

    @Override
    public void getUserWalletInfoFail(String error) {
        toast(error);
        goldNumTextView.setText("0");
        goldNumTextView.setText("0");
        goldNumTextView.setText("0");
    }

    @Override
    public void handleClickByViewId(int id) {
        switch (id) {
            case R.id.rlCharge:
                startActivity(new Intent(this, ChargeActivity.class));
                break;

            case R.id.rl_my_wallet_earnings:
                startActivity(new Intent(this, MyIncomeActivity.class));
                break;

            case R.id.llNobleMoneyTip:
                CommonWebViewActivity.start(this, UriProvider.getNobleDetailUrl(), true);
                break;
            default:
                break;
        }
    }

    @Override
    public boolean isStatusBarDarkFont() {
        return false;
    }
}
