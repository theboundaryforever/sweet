package com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter;

import com.netease.nim.uikit.common.util.log.LogUtil;
import com.tongdaxing.erban.libcommon.im.IMError;
import com.tongdaxing.erban.libcommon.im.IMProCallBack;
import com.tongdaxing.erban.libcommon.im.IMReportBean;
import com.tongdaxing.erban.libcommon.im.IMReportResult;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.liveroom.im.model.AudioConnTimeOutTipsQueueScheduler;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomModel;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomAdmireTimeCounter;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomOwnerLiveTimeCounter;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMember;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.SingleAudioRoomEnitity;
import com.tongdaxing.xchat_core.manager.RtcEngineManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.model.AvRoomModel;
import com.tongdaxing.xchat_core.utils.StringUtils;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.base.presenter.BaseRoomPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IRoomView;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;

import static com.tongdaxing.xchat_core.liveroom.im.model.IMRoomModel.ERROR_CODE_AUDIO_CHANNEL_INIT;
import static com.tongdaxing.xchat_core.liveroom.im.model.IMRoomModel.ERROR_CODE_DEFAULT;

/**
 * 房间框架的presenter
 * 甜蜜星球后续新的房间类型可以考虑统一使用这个Presenter
 *
 * @author zeda
 */
public class RoomFramePresenter extends BaseRoomPresenter<IRoomView> {

    private final String TAG = RoomFramePresenter.class.getSimpleName();

    private IMRoomModel model;
    private AvRoomModel avRoomModel;

    public RoomFramePresenter() {
        super();
        model = new IMRoomModel();
        avRoomModel = new AvRoomModel();
    }

    private boolean joinChannel(String key) {
        com.juxiao.library_utils.log.LogUtil.i(TAG, "enterRoom ---> joinChannel");
        RoomInfo curRoomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (curRoomInfo != null) {
            com.juxiao.library_utils.log.LogUtil.i(TAG, "enterRoom ---> audioChannel = " + curRoomInfo.getAudioChannel());
            if (curRoomInfo.getAudioChannel() == 0) {
                return false;
            }
            RtcEngineManager.get().setAudioOrganization(curRoomInfo.getAudioChannel());
            return RtcEngineManager.get().startRtcEngine(getCurrentUserId(), key, curRoomInfo);
        } else {
            return false;
        }
    }

    public void enterRoom(long roomUid, int roomType, String roomPwd) {
        if (getMvpView() != null && StringUtils.isEmpty(roomPwd)) {
            getMvpView().showEnterRoomLoading();
        }
        model.enterRoomSocket(roomUid, roomType, roomPwd, new IMProCallBack() {

            @Override
            public void onSuccessPro(IMReportBean imReportBean) {
                if (getMvpView() != null) {
                    getMvpView().dismisEnterRoomLoading();
                    IMReportBean.ReportData reportData = imReportBean.getReportData();
                    Json data = reportData.data;
                    String agora = data.str("agora");
                    String key = null;
                    try {
                        Json json = new Json(agora);
                        key = json.str("key");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (key != null) {
                        if (joinChannel(key)) {
                            onEnterRoomSuccess();
                        } else {
                            onEnterRoomFail(ERROR_CODE_AUDIO_CHANNEL_INIT, "初始化失败");
                        }
                    } else {
                        onEnterRoomFail(ERROR_CODE_DEFAULT, "获取key失败");
                    }
                }
            }

            @Override
            public void onError(int errorCode, String errorMsg) {
                if (getMvpView() != null) {
                    getMvpView().dismisEnterRoomLoading();
                    onEnterRoomFail(errorCode, errorMsg);
                }
            }
        });
    }


    private void onEnterRoomSuccess() {
        LogUtils.d(TAG, "onEnterRoomSuccess");
        IMRoomMessageManager.get().noticeSocketEnterRoomSucMessages();
        refreshRoomManagerMember();
        RoomAdmireTimeCounter.getInstance().startCount();
        RoomDataManager.get().imRoomAdmireTimeCounterShow = true;
        RoomDataManager.get().singleAudioRoomMsgTipsViewShow = true;
        RoomDataManager.get().hasRequestRecommRoomList = false;
    }

    private void onEnterRoomFail(int code, String error) {
        LogUtils.d(TAG, "onEnterRoomFail-code:" + code + " error:" + error);
        if (getMvpView() == null) {
            return;
        }
        getMvpView().dismissDialog();
        switch (code) {
            case IMError.IM_ROOM_PWD://请输入房间密码
                getMvpView().showRoomPwdDialog(false);
                break;
            case IMError.IM_ROOM_RE_PWS://房间密码错误，请输入正确的密码
                getMvpView().showRoomPwdDialog(true);
                break;
            case IMError.USER_REAL_NAME_NEED_VERIFIED:
            case IMError.USER_REAL_NAME_AUDITING:
            case IMError.USER_REAL_NAME_NEED_PHONE:
            case IMError.USER_REAL_NAME_NEED_ROLE:
//                getMvpView().showRoomGuideDialog();
//                break;
            case IMError.IM_JSON_PARSE:
            case ERROR_CODE_AUDIO_CHANNEL_INIT:
            case IMError.IM_LOGIN_FAIL:
                //"im登录鉴权失败
            case IMError.IM_GET_USER_INFO_FAIL:
                //获取用户信息失败
            case IMError.IM_GET_ROOM_INFO_FAIL:
                //获取用户信息失败
            case IMError.IM_ROOM_SOCKET_ID_NOT_EXIST:
                //用户房间socketId不存在
                LogUtil.d(TAG, "onEnterRoomFail:code = " + code);
                BaseRoomServiceScheduler.exitRoom(null);
            case IMError.IM_USER_IN_ROOM_BLACK_LIST:
            case IMError.IM_ROOM_FINISHED:
                //房间已结束
            case IMError.IM_ROOM_NOT_EXIST:
                //房间不存在
            case IMError.IM_ROOM_HAS_FINISHED:
                //房间已经结束
            default:
                RoomDataManager.get().release();
                SingleToastUtil.showToast(error);
                getMvpView().finishActivity();
                break;

        }
    }

    /**
     * 刷新管理员列表
     */
    private void refreshRoomManagerMember() {
        LogUtils.d(TAG, "refreshRoomManagerMember");
        // 房间内查询管理员列表业务模块
        model.getRoomManagers(new OkHttpManager.MyCallBack<IMReportResult<List<IMRoomMember>>>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(IMReportResult<List<IMRoomMember>> response) {
                if (response != null && response.isSuccess() && response.getData() != null && !ListUtils.isListEmpty(response.getData())) {
                    RoomDataManager.get().mRoomManagerList = response.getData();
                }
            }
        });
    }

    /**
     * 检查是否已经被踢了
     *
     * @param roomUid
     * @param roomType
     * @return
     */
    public boolean checkIsKick(long roomUid, int roomType) {
        return avRoomModel.checkIsKick(roomUid, roomType);
    }

    /**
     * 当前自己是否是第一次进入此房间
     *
     * @param roomUid
     * @param roomType
     * @return
     */
    public boolean isFirstEnterRoomOrChangeOtherRoom(long roomUid, int roomType) {
        return RoomDataManager.get().isFirstEnterRoomOrChangeOtherRoom(roomUid, roomType);
    }


    /**
     * 刷新房主的用户信息
     */
    public void refreshRoomOwnerUserInfo() {
        RoomInfo roomInfo = getServerRoomInfo();
        if (roomInfo != null) {
            model.getRoomOwnerInfo(roomInfo.getUid());
        }
    }

    /**
     * 退出房间
     *
     * @param callBack 退出结果回调
     */
    public void exitRoom(CallBack<String> callBack) {
        RoomOwnerLiveTimeCounter.getInstance().release();
        RoomAdmireTimeCounter.getInstance().release();
        AudioConnTimeOutTipsQueueScheduler.getInstance().clear();
        RoomDataManager.get().imRoomAdmireTimeCounterShow = true;
        RoomDataManager.get().singleAudioRoomMsgTipsViewShow = true;
        RoomDataManager.get().singleAudioConnReqNum = 0;
        RoomDataManager.get().hasRequestRecommRoomList = false;
        model.exitRoom(callBack);
    }

    public Call<ResponseBody> getReturnRoomList() {
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (null == roomInfo) {
            return null;
        }
        LogUtils.d(TAG, "getReturnRoomList roomId:" + roomInfo.getRoomId());
        return model.getReturnRoomList(roomInfo.getRoomId(), new HttpRequestCallBack<List<SingleAudioRoomEnitity>>() {
            @Override
            public void onSuccess(String message, List<SingleAudioRoomEnitity> response) {
                LogUtils.d(TAG, "getReturnRoomList-->onSuccess message:" + message);
                if (null != getMvpView() && !ListUtils.isListEmpty(response)) {
                    LogUtils.d(TAG, "getReturnRoomList-->onSuccess list.size:" + response.size());
                    RoomDataManager.get().setSingleAudioRoomEnitities(response);
//                }else{
//                    /**
//                     * {
//                     * 	"abChannelType": 1,
//                     * 	"avatar": "https://pic.tiantianyuyin.com/FjEt5s1myGO0RX9V0xb3xWjSq1Ab?imageslim",
//                     * 	"backPic": "",
//                     * 	"calcSumDataIndex": 0,
//                     * 	"count": 0,
//                     * 	"erbanNo": 1450615,
//                     * 	"faceType": 0,
//                     * 	"factor": 0,
//                     * 	"gender": 1,
//                     * 	"hotRank": 99999,
//                     * 	"hotScore": 1000,
//                     * 	"isPermitRoom": 2,
//                     * 	"isRecom": 0,
//                     * 	"meetingName": "dc85061d287a4e6298b1e4091f8ad886",
//                     * 	"newestTime": 1565945080404,
//                     * 	"nick": "看一看太阳",
//                     * 	"officeUser": 1,
//                     * 	"onlineDuration": 28796,
//                     * 	"onlineNum": 2,
//                     * 	"openTime": 1565864844000,
//                     * 	"operatorStatus": 1,
//                     * 	"roomId": 11130,
//                     * 	"roomTag": "情感",
//                     * 	"searchTagId": 0,
//                     * 	"tagId": 38,
//                     * 	"tagPict": "https://img.pinjin88.com/qinggan@2x.png",
//                     * 	"title": "看一看太阳的房间",
//                     * 	"type": 4,
//                     * 	"uid": 92000417,
//                     * 	"valid": true
//                     * }
//                     */
//                    response = new ArrayList<>();
//                    for(int i = 0 ; i<8; i++){
//                        SingleAudioRoomEnitity singleAudioRoomEnitity = new SingleAudioRoomEnitity();
//                        singleAudioRoomEnitity.setType(RoomInfo.ROOMTYPE_SINGLE_AUDIO);
//                        singleAudioRoomEnitity.setTitle("看一看太阳的房间");
//                        singleAudioRoomEnitity.setAvatar("https://pic.tiantianyuyin.com/FjEt5s1myGO0RX9V0xb3xWjSq1Ab?imageslim");
//                        singleAudioRoomEnitity.setUid(92000417L);
//                        singleAudioRoomEnitity.setGender(1);
//                        singleAudioRoomEnitity.setOnlineNum(1314);
//                        singleAudioRoomEnitity.setRoomTag("情感");
//                        response.add(singleAudioRoomEnitity);
//                    }
//                    getMvpView().refreshRecommRoomList(response);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                LogUtils.d(TAG, "getReturnRoomList-->onFailure code:" + code + " msg:" + msg);
            }
        });
    }

}
