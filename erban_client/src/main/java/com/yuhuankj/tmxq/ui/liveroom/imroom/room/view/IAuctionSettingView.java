package com.yuhuankj.tmxq.ui.liveroom.imroom.room.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/7/30
 */
public interface IAuctionSettingView extends IMvpBaseView {
    void showResultToast(String result);
}
