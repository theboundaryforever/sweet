package com.yuhuankj.tmxq.ui.message.attention;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.praise.IPraiseClient;
import com.tongdaxing.xchat_core.user.AttentionCore;
import com.tongdaxing.xchat_core.user.AttentionCoreClient;
import com.tongdaxing.xchat_core.user.bean.AttentionInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * 关注列表
 */
public class AttentionListActivity extends BaseActivity {

    private RecyclerView mRecylcerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private AttentionListActivity mActivity;
    private AttentionListAdapter adapter;
    private List<AttentionInfo> mAttentionInfoList = new ArrayList<>();

    private int mPage = Constants.PAGE_START;

    public static void start(Context context) {
        Intent intent = new Intent(context, AttentionListActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_attention);
        setSwipeBackEnable(false);
        initTitleBar(getString(R.string.my_attention));
        if (null != mTitleBar) {
            mTitleBar.setCommonBackgroundColor(Color.WHITE);
        }
        initView();
        setListener();
        initData();
    }

    private void setListener() {
        swipeRefreshLayout.setOnRefreshListener(onRefreshLisetener);
        adapter = new AttentionListAdapter(mAttentionInfoList);
        adapter.setRylListener(new AttentionListAdapter.onClickListener() {
            @Override
            public void rylListeners(AttentionInfo attentionInfo) {
                if (90000000 == attentionInfo.getUid()) {
                    return;
                }
                UserInfoActivity.start(mActivity, attentionInfo.getUid());
            }

            @Override
            public void findHimListeners(AttentionInfo attentionInfo) {
                if (90000000 == attentionInfo.getUid()) {
                    return;
                }
                if (attentionInfo.getUserInRoom() != null) {
                    RoomServiceScheduler.getInstance().enterRoom(mActivity,
                            attentionInfo.getUserInRoom().getUid(),
                            attentionInfo.getUserInRoom().getType());
                }
            }
        });
        adapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                mPage++;
                onRefreshing();
            }
        }, mRecylcerView);
    }

    private void initData() {
        mRecylcerView.setAdapter(adapter);
        showLoading();
        onRefreshing();
    }

    private void initView() {
        mActivity = this;
        mRecylcerView = (RecyclerView) findViewById(R.id.recyclerView);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        mRecylcerView.setLayoutManager(new RecyclerViewNoBugLinearLayoutManager(mActivity));

    }

    SwipeRefreshLayout.OnRefreshListener onRefreshLisetener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            mPage = Constants.PAGE_START;
            onRefreshing();
        }
    };

    private void onRefreshing() {
        CoreManager.getCore(AttentionCore.class)
                .getAttentionList(CoreManager.getCore(IAuthCore.class).getCurrentUid(), mPage, Constants.PAGE_SIZE);
    }

    @CoreEvent(coreClientClass = AttentionCoreClient.class)
    public void onGetAttentionList(List<AttentionInfo> attentionInfoList, int page) {
        mPage = page;
        hideStatus();
        if (!ListUtils.isListEmpty(attentionInfoList)) {
            if (mPage == Constants.PAGE_START) {
                swipeRefreshLayout.setRefreshing(false);
                mAttentionInfoList.clear();
                adapter.setNewData(attentionInfoList);
                if (attentionInfoList.size() < Constants.PAGE_SIZE) {
                    adapter.setEnableLoadMore(false);
                }
            } else {
                adapter.loadMoreComplete();
                adapter.addData(attentionInfoList);
            }
        } else {
            if (mPage == Constants.PAGE_START) {
                adapter.setEmptyView(getEmptyView(mRecylcerView, getString(R.string.no_attention_text)));
                swipeRefreshLayout.setRefreshing(false);
            } else {
                adapter.loadMoreEnd(true);
            }

        }
    }

    @CoreEvent(coreClientClass = AttentionCoreClient.class)
    public void onGetAttentionListFail(String error, int page) {
        mPage = page;
        if (mPage == Constants.PAGE_START) {
            swipeRefreshLayout.setRefreshing(false);
            showNetworkErr();
        } else {
            adapter.loadMoreFail();
            toast(error);
        }
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onCanceledPraise(long uid) {
        List<AttentionInfo> data = adapter.getData();
        if (!ListUtils.isListEmpty(data)) {
            ListIterator<AttentionInfo> iterator = data.listIterator();
            for (; iterator.hasNext(); ) {
                AttentionInfo attentionInfo = iterator.next();
                if (attentionInfo.isValid() && attentionInfo.getUid() == uid) {
                    iterator.remove();
                }
            }
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public View.OnClickListener getLoadListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPage = Constants.PAGE_START;
                showLoading();
                onRefreshing();
            }
        };
    }
}


