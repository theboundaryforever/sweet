package com.yuhuankj.tmxq.ui.quickmating;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.BaseAppDialog;

import java.util.ArrayList;
import java.util.List;

public class RulerDialog extends BaseAppDialog implements View.OnClickListener {
    private LinearLayout llRuler;
    private RecyclerView rcvRuler;
    private TextView tvRulerEnter;
    private RulerAdapter rulerAdapter;
    private List<String> data = new ArrayList<>();

    public RulerDialog(Context context) {
        super(context);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.layout_quick_mating_tip;
    }

    @Override
    protected void initView() {
        llRuler = findViewById(R.id.llRuler);
        rcvRuler = findViewById(R.id.rcvRuler);
        tvRulerEnter = findViewById(R.id.tvRulerEnter);
    }

    @Override
    public void initListener() {
        tvRulerEnter.setOnClickListener(this);
    }

    @Override
    public void initViewState() {
        rcvRuler.setLayoutManager(new RecyclerViewNoBugLinearLayoutManager(getContext()));
        rulerAdapter = new RulerAdapter(data);
        rcvRuler.setAdapter(rulerAdapter);
        try {
            getWindow().getAttributes().width = ScreenUtil.getScreenWidth(getContext()) - ScreenUtil.dip2px(80);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == tvRulerEnter) {
            dismiss();
        }
    }

    public void setData(List datas) {
        data.clear();
        data.addAll(datas);
    }

    public class RulerAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

        public RulerAdapter(List data) {
            super(R.layout.item_quick_mating_tip, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, String item) {
            TextView tvContent = helper.getView(R.id.tvContent);
            tvContent.setText(item);
        }
    }
}
