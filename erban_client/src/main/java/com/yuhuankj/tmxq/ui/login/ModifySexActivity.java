package com.yuhuankj.tmxq.ui.login;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.RadioGroup;

import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.widget.TitleBar;

/**
 * @author liaoxy
 * @Description:修改性别页
 * @date 2019/4/11 14:04
 */
public class ModifySexActivity extends BaseActivity {

    private RadioGroup ragSex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_sex);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        ragSex = (RadioGroup) findViewById(R.id.ragSex);
    }

    private void initListener() {

    }

    private void initData() {
        initTitleBar("性别");
        mTitleBar.setActionTextColor(Color.parseColor("#1A1A1A"));
        mTitleBar.addAction(new TitleBar.TextAction("保存") {
            @Override
            public void performAction(View view) {
                Intent intent = new Intent();
                intent.putExtra("sex", ragSex.getCheckedRadioButtonId() == R.id.rabMan ? 1 : 2);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        mTitleBar.setCommonBackgroundColor(Color.WHITE);

        if (getIntent() != null) {
            int sex = getIntent().getIntExtra("sex", -1);
            if (sex != -1) {
                ragSex.check(sex == 1 ? R.id.rabMan : R.id.rabWoman);
            }
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfoUpdate(UserInfo userInfo) {
        getDialogManager().dismissDialog();
        SingleToastUtil.showToast("保存成功");
        finish();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfoUpdateError(String error) {
        getDialogManager().dismissDialog();
        if (!TextUtils.isEmpty(error)) {
            toast(error);
        }
    }
}
