package com.yuhuankj.tmxq.ui.liveroom.imroom.room.view;

import com.tongdaxing.xchat_core.room.queue.bean.RoomConsumeInfo;

import java.util.List;

/**
 * 文件描述：多人语聊房的MVP 的中间接口
 *
 * @auther：zwk
 * @data：2019/6/3
 */
public interface IMultiAudioView extends IRoomDetailView {

    void onGetFortuneRankTop(boolean isSuccess, String message, List<RoomConsumeInfo> roomConsumeInfos);
}
