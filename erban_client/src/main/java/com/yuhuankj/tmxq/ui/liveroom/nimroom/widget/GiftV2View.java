package com.yuhuankj.tmxq.ui.liveroom.nimroom.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.juxiao.library_utils.DisplayUtils;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.gift.IGiftCoreClient;
import com.tongdaxing.xchat_core.gift.MultiGiftReceiveInfo;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomEvent;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.ComboGiftView;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

/**
 * 礼物特效布局
 *
 * @author xiaoyu
 * @date 2017/12/20
 */

public class GiftV2View extends RelativeLayout implements GiftEffectView.GiftEffectListener {

    private final String TAG = GiftV2View.class.getSimpleName();

    public GiftEffectView giftEffectView;
    private int giftWidth;
    private int giftHeight;
    private List<GiftReceiveInfo> giftReceiveInfoList;
    private UiHandler handler;

    private Context context;
    private int mScreenWidth;
    private int mScreenHeight;
//    private int mGiftSendY;
    private RelativeLayout flSend;
//    private int resolutionScreen;
    private int pointeDistance = 50;

    private int roomType = RoomInfo.ROOMTYPE_HOME_PARTY;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private ComboGiftView comboGiftView;

    public void setRoomType(int roomType) {
        this.roomType = roomType;
    }

    public GiftV2View(Context context) {
        this(context, null);

    }

    public GiftV2View(Context context, AttributeSet attr) {
        this(context, attr, 0);
    }

    public GiftV2View(Context context, AttributeSet attr, int i) {
        super(context, attr, i);
        init(context);
    }

    public void setComboGiftView(ComboGiftView cgv) {
        this.comboGiftView = cgv;
    }

    private void init(Context context) {
        this.context = context;
        CoreManager.addClient(this);
        inflate(context,R.layout.layout_gift_v2_view, this);
        mScreenWidth = DisplayUtils.getScreenWidth(context);
        mScreenHeight = DisplayUtils.getScreenHeight(context);
//        resolutionScreen = ResolutionUtils.getScreenWidth(context);
        pointeDistance = DisplayUtils.dip2px(context,25);
//        mGiftSendY = DisplayUtils.dip2px(context, 90);
        giftWidth = DisplayUtils.dip2px(context, 80);
        giftHeight = DisplayUtils.dip2px(context, 80);

        giftReceiveInfoList = new ArrayList<>();
        giftEffectView = findViewById(R.id.gift_effect_view);
        flSend = findViewById(R.id.fl_gift_send);
        giftEffectView.setGiftEffectListener(this);
        handler = new UiHandler(this);
    }

    private void drawAnimation(List<GiftReceiveInfo> giftReceiveInfos) {
        if (giftReceiveInfos.size() > 0) {
            RoomInfo roomInfo = BaseRoomServiceScheduler.getCurrentRoomInfo();
            if (roomInfo != null) {
                setVisibility(VISIBLE);
                int totalCoin = 0;
                for (int i = 0; i < giftReceiveInfos.size(); i++) {
                    GiftReceiveInfo giftReceiveInfo = giftReceiveInfos.get(i);
                    SparseArray<Point> micViewPoint = new SparseArray<>();
                    if (roomType == RoomInfo.ROOMTYPE_HOME_PARTY) {
                        micViewPoint = AvRoomDataManager.get().mMicPointMap;
                    }
                    if (roomType == RoomInfo.ROOMTYPE_SINGLE_AUDIO ||roomType == RoomInfo.ROOMTYPE_MULTI_AUDIO
                            || roomType == RoomInfo.ROOMTYPE_NEW_AUCTION) {
                        micViewPoint = RoomDataManager.get().mMicPointMap;
                    }

                    LogUtils.d("GiftAnimation", "drawAnimation: get micPointMap=" + micViewPoint);
                    GiftInfo giftInfo = null;
                    if (giftReceiveInfo.getRealGiftId() > 0) {
                        giftInfo = CoreManager.getCore(IGiftCore.class).findGiftInfoById(giftReceiveInfo.getRealGiftId());
                    } else {
                        giftInfo = CoreManager.getCore(IGiftCore.class).findGiftInfoById(giftReceiveInfo.getGiftId());
                    }
                    // 算出发送者和接受者的位置
                    int senderPosition = -3;
                    int receivePosition = -3;


                    if (roomType == RoomInfo.ROOMTYPE_HOME_PARTY) {
                        senderPosition = AvRoomDataManager.get().getMicPosition(giftReceiveInfo.getUid());
                        receivePosition = AvRoomDataManager.get().getMicPosition(giftReceiveInfo.getTargetUid());
                    }
                    if (roomType == RoomInfo.ROOMTYPE_SINGLE_AUDIO || roomType == RoomInfo.ROOMTYPE_MULTI_AUDIO
                            || roomType == RoomInfo.ROOMTYPE_NEW_AUCTION) {
                        senderPosition = RoomDataManager.get().getMicPosition(giftReceiveInfo.getUid());
                        receivePosition = RoomDataManager.get().getMicPosition(giftReceiveInfo.getTargetUid());
                    }

                    LogUtils.d("GiftAnimation", "drawAnimation: receivePosition" + receivePosition);
                    Point senderPoint = micViewPoint.get(senderPosition);
                    Point receivePoint = micViewPoint.get(receivePosition);
                    //礼物送给树上的人
                    if (receivePosition == Integer.MIN_VALUE) {
                        receivePoint = micViewPoint.get(-2);
                    }
//                    if (senderPoint == null && roomInfo.getUid() == giftReceiveInfo.getUid()) {
//                        senderPoint = new Point();
//                        senderPoint.x = mScreenWidth / 2 - giftWidth / 2;
//                        senderPoint.y = mGiftSendY;
//                    }
//
//                    if (receivePoint == null && roomInfo.getUid() == giftReceiveInfo.getTargetUid()) {
//                        receivePoint = new Point();
//                        receivePoint.x = mScreenWidth / 2 - giftWidth / 2;
//                        receivePoint.y = mGiftSendY;
//                    }

                    if (receivePoint != null) {
                        //送出礼物到麦上的动画
                        if (totalCoin < 520 && roomInfo.getGiftEffectSwitch() == 1) {
                        } else {
                            drawGiftView(senderPoint, receivePoint, giftInfo.getGiftUrl());
                        }
                    }
                    totalCoin += giftInfo.getGoldPrice() * giftReceiveInfo.getGiftNum();
                }

                if (totalCoin >= 520) {
                    Message msg = Message.obtain();
                    msg.what = 0;
                    GiftReceiveInfo giftReceiveInfo = giftReceiveInfos.get(0);
                    giftReceiveInfo.setPersonCount(giftReceiveInfos.size());
                    msg.obj = giftReceiveInfo;
                    handler.sendMessageDelayed(msg, 200);
                }
            }
        }
    }

    private static class UiHandler extends Handler {
        private WeakReference<GiftV2View> giftV2ViewWeakReference;

        public UiHandler(GiftV2View giftV2View) {
            this.giftV2ViewWeakReference = new WeakReference<>(giftV2View);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            GiftV2View giftV2View = giftV2ViewWeakReference.get();
            if (giftV2View != null) {
                Activity activity = (Activity) giftV2View.context;
                if (activity != null && !activity.isDestroyed()) {
                    GiftReceiveInfo giftRecieveInfo = (GiftReceiveInfo) msg.obj;
                    if (giftRecieveInfo != null) {
                        giftV2View.drawGiftEffect(giftRecieveInfo);
                    }
                }
            }
        }
    }

    private void drawGiftEffect(GiftReceiveInfo giftReceiveInfo) {
        giftReceiveInfoList.add(giftReceiveInfo);
        if (!giftEffectView.isAnim()) {
            giftEffectView.startGiftEffect(giftReceiveInfo);
            giftReceiveInfoList.remove(0);
        }
    }

    private void drawAdmireAnim(String admireAnimIconUrl) {
        LogUtils.d(TAG, "drawAdmireAnim-admireAnimIconUrl:" + admireAnimIconUrl);
        if (TextUtils.isEmpty(admireAnimIconUrl)) {
            return;
        }
        RoomInfo roomInfo = BaseRoomServiceScheduler.getCurrentRoomInfo();
        if (null == roomInfo || roomInfo.getType() != RoomInfo.ROOMTYPE_SINGLE_AUDIO) {
            return;
        }
        setVisibility(VISIBLE);
        SparseArray<Point> micViewPoint = RoomDataManager.get().mMicPointMap;
        // 算出发送者和接受者的位置
        int senderPosition = RoomDataManager.get().getMicPosition(roomInfo.getUid());
        int receivePosition = RoomDataManager.get().getMicPosition(roomInfo.getUid());
        Point senderPoint = micViewPoint.get(senderPosition);
        Point receivePoint = micViewPoint.get(receivePosition);
        drawGiftView(null, receivePoint, admireAnimIconUrl);
    }

    private void drawGiftView(Point senderPoint, Point receivePoint, String giftUrl) {
        final Point center = new Point();
        center.x = mScreenWidth / 2;
        center.y = mScreenHeight / 2;
        final ImageView imageView = new ImageView(context);
        RelativeLayout.LayoutParams layoutParams;
        if (senderPoint == null) {
            senderPoint = new Point(mScreenWidth / 2 - giftWidth / 2, pointeDistance);
            layoutParams = new RelativeLayout.LayoutParams(giftWidth, giftWidth);
            layoutParams.leftMargin = mScreenWidth / 2 - giftWidth / 2;
            layoutParams.topMargin = pointeDistance;
        } else {
            layoutParams = new RelativeLayout.LayoutParams(giftWidth, giftWidth);
            layoutParams.leftMargin = senderPoint.x;
            layoutParams.topMargin = senderPoint.y;
        }

        imageView.setLayoutParams(layoutParams);
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        flSend.addView(imageView);
        ImageLoadUtils.loadImage(context, giftUrl, imageView);

        Keyframe kx0 = Keyframe.ofFloat(0f, 0);
        Keyframe kx1 = Keyframe.ofFloat(0.2f, center.x - senderPoint.x - giftWidth / 2);
        Keyframe kx2 = Keyframe.ofFloat(0.4f, center.x - senderPoint.x - giftWidth / 2);
        kx2.setInterpolator(new AccelerateDecelerateInterpolator());
        Keyframe kx3 = Keyframe.ofFloat(0.8f, center.x - senderPoint.x - giftWidth / 2);
        kx3.setInterpolator(new AccelerateDecelerateInterpolator());
        Keyframe kx4 = Keyframe.ofFloat(1f, receivePoint.x - senderPoint.x);
        kx4.setInterpolator(new AccelerateDecelerateInterpolator());

        Keyframe ky0 = Keyframe.ofFloat(0f, 0);
        Keyframe ky1 = Keyframe.ofFloat(0.2f, center.y - senderPoint.y - giftHeight / 2);
        Keyframe ky2 = Keyframe.ofFloat(0.4f, center.y - senderPoint.y - giftHeight / 2);
        ky2.setInterpolator(new AccelerateDecelerateInterpolator());
        Keyframe ky3 = Keyframe.ofFloat(0.8f, center.y - senderPoint.y - giftHeight / 2);
        ky3.setInterpolator(new AccelerateDecelerateInterpolator());
        Keyframe ky4 = Keyframe.ofFloat(1f, receivePoint.y - senderPoint.y);
        ky4.setInterpolator(new AccelerateDecelerateInterpolator());

        Keyframe ks0 = Keyframe.ofFloat(0f, 0.2f);
        Keyframe ks1 = Keyframe.ofFloat(0.2f, 1f);
        Keyframe ks2 = Keyframe.ofFloat(0.4f, 1.5f);
        Keyframe ks3 = Keyframe.ofFloat(0.6f, 2f);
        Keyframe ks4 = Keyframe.ofFloat(0.8f, 2f);
        Keyframe ks5 = Keyframe.ofFloat(1f, 0.2f);

        PropertyValuesHolder p0 = PropertyValuesHolder.ofKeyframe("translationX", kx0, kx1, kx2, kx3, kx4);
        PropertyValuesHolder p1 = PropertyValuesHolder.ofKeyframe("translationY", ky0, ky1, ky2, ky3, ky4);
        PropertyValuesHolder p2 = PropertyValuesHolder.ofKeyframe("scaleX", ks0, ks1, ks2, ks3, ks4, ks5);
        PropertyValuesHolder p3 = PropertyValuesHolder.ofKeyframe("scaleY", ks0, ks1, ks2, ks3, ks4, ks5);

        ObjectAnimator objectAnimator = ObjectAnimator.ofPropertyValuesHolder(imageView, p2, p3, p1, p0);
        objectAnimator.setDuration(4000);
        objectAnimator.start();

        objectAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                ViewGroup viewGroup = (ViewGroup) imageView.getParent();
                viewGroup.removeView(imageView);
            }
        });
    }

    public void release() {
        CoreManager.removeClient(this);
        giftReceiveInfoList.clear();
        giftEffectView.release();
        handler.removeCallbacksAndMessages(null);
        if (null != comboGiftView) {
            comboGiftView.clear();
            comboGiftView = null;
        }
    }

    @Override
    public void onGiftEffectEnd() {
        if (giftReceiveInfoList != null && giftReceiveInfoList.size() > 0) {
            giftEffectView.startGiftEffect(giftReceiveInfoList.get(0));
            giftReceiveInfoList.remove(0);
        } else {
            setVisibility(GONE);
        }
    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onRecieveGiftMsg(GiftReceiveInfo giftReceiveInfo) {
        //如果不是大礼物的传空适配ios
        if (giftReceiveInfo != null) {
            giftReceiveInfo.setRoomId("");
        }
        List<GiftReceiveInfo> giftReceiveInfos = new ArrayList<>();
        if (null != comboGiftView && giftReceiveInfo.getComboId() != 0L) {
            comboGiftView.addSingleGiftInfo(giftReceiveInfo);
        }
        giftReceiveInfos.add(giftReceiveInfo);
        drawAnimation(giftReceiveInfos);
    }

    /**
     * 大礼物的特效
     *
     * @param giftReceiveInfo
     */
    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onSuperGiftMsg(GiftReceiveInfo giftReceiveInfo) {
        LogUtils.d("IGiftCoreClient", "onSuperGiftMsg");
        setVisibility(VISIBLE);
        Message msg = Message.obtain();
        msg.what = 0;
        giftReceiveInfo.setPersonCount(1);
        msg.obj = giftReceiveInfo;
        handler.sendMessageDelayed(msg, 100);
    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onRecieveMultiGiftMsg(MultiGiftReceiveInfo multiGiftReceiveInfo) {
        if (multiGiftReceiveInfo != null) {
            List<Long> targetUids = multiGiftReceiveInfo.getTargetUids();
            List<GiftReceiveInfo> giftReceiveInfos = new ArrayList<>();
            for (Long targetUid : targetUids) {
                GiftReceiveInfo giftReceiveInfo = new GiftReceiveInfo();
                giftReceiveInfo.setUid(multiGiftReceiveInfo.getUid());
                giftReceiveInfo.setGiftNum(multiGiftReceiveInfo.getGiftNum());
                giftReceiveInfo.setTargetUid(targetUid);
                giftReceiveInfo.setNick(multiGiftReceiveInfo.getNick());
                giftReceiveInfo.setGiftId(multiGiftReceiveInfo.getGiftId());
                giftReceiveInfo.setRealGiftId(multiGiftReceiveInfo.getRealGiftId());
                giftReceiveInfo.setAvatar(multiGiftReceiveInfo.getAvatar());
                giftReceiveInfos.add(giftReceiveInfo);
            }
            drawAnimation(giftReceiveInfos);
            if (null != comboGiftView && multiGiftReceiveInfo.getComboId() != 0L) {
                comboGiftView.addMultiGiftInfo(multiGiftReceiveInfo);
            }
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (compositeDisposable == null) {
            return;
        }

        compositeDisposable.add(IMRoomMessageManager.get().getIMRoomEventObservable()
                .subscribe(roomEvent -> {
                    receiveRoomEvent(roomEvent);
                }));
    }

    /**
     * 接收房间事件
     */
    public void receiveRoomEvent(IMRoomEvent roomEvent) {
        if (roomEvent == null) {
            return;
        }
        int event = roomEvent.getEvent();
        switch (event) {
            case IMRoomEvent.ROOM_ADMIRE_ANIM:
                String admireAnimIconUrl = roomEvent.getAdmireAnimIconUrl();
                drawAdmireAnim(admireAnimIconUrl);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
            compositeDisposable = null;
        }
    }
}
