package com.yuhuankj.tmxq.ui.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_core.withdraw.IWithdrawCore;
import com.tongdaxing.xchat_core.withdraw.IWithdrawCoreClient;
import com.tongdaxing.xchat_core.withdraw.bean.WithdrawInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.ui.me.wallet.income.withdraw.BinderAlipayActivity;
import com.yuhuankj.tmxq.ui.widget.ChooseThirdPlatformDialog;
import com.yuhuankj.tmxq.widget.TitleBar;

/**
 * 第三方平台绑定信息展示界面
 */
public class ThirdPlatformBindingActivity extends BaseMvpActivity
        implements IMvpBaseView, View.OnClickListener, ChooseThirdPlatformDialog.OnThirdPlatformChooseListener {

    private final String TAG = ThirdPlatformBindingActivity.class.getSimpleName();
    private TextView tv_nick;
    private TextView tv_phone;
    private TextView tv_aliplay;
    private TextView tv_qqBindStatus;
    private TextView tv_wechatBindStatus;

    private UserInfo userInfo;
    private WithdrawInfo withdrawInfo;
    private boolean hasBindPhone = false;

    public static void start(Context context, WithdrawInfo withdrawInfo) {
        Intent intent = new Intent(context, ThirdPlatformBindingActivity.class);
        intent.putExtra("withdrawInfo", withdrawInfo);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third_platform_binding);
        initTitleBar(getResources().getString(R.string.setting_platform_binding));
        initView();
        initData();
    }

    private void initData() {
        userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (null != getIntent() && getIntent().hasExtra("withdrawInfo")) {
            withdrawInfo = (WithdrawInfo) getIntent().getSerializableExtra("withdrawInfo");
        }
        updateThirdPlatformBindStatus();
    }

    private void updateThirdPlatformBindStatus() {
        tv_nick.setText(null != userInfo ? userInfo.getNick() : null);
        tv_qqBindStatus.setText(getResources().getString(null != userInfo && userInfo.isHasQQ() ? R.string.third_platform_bind_binded : R.string.third_platform_bind_unbinded));
        tv_qqBindStatus.setCompoundDrawablesWithIntrinsicBounds(null, null, null != userInfo && userInfo.isHasQQ() ? null : getResources().getDrawable(R.drawable.arrow_right), null);
        tv_wechatBindStatus.setCompoundDrawablesWithIntrinsicBounds(null, null, null != userInfo && userInfo.isHasWx() ? null : getResources().getDrawable(R.drawable.arrow_right), null);
        tv_wechatBindStatus.setText(getResources().getString(null != userInfo && userInfo.isHasWx() ? R.string.third_platform_bind_binded : R.string.third_platform_bind_unbinded));
    }

    private void initView() {
        tv_nick = (TextView) findViewById(R.id.tv_nick);
        tv_phone = (TextView) findViewById(R.id.tv_phone);
        tv_aliplay = (TextView) findViewById(R.id.tv_alipay);
        tv_qqBindStatus = (TextView) findViewById(R.id.tv_qqBindStatus);
        tv_wechatBindStatus = (TextView) findViewById(R.id.tv_wechatBindStatus);
        findViewById(R.id.fl_bindPhone).setOnClickListener(this);
        findViewById(R.id.fl_bindAlipay).setOnClickListener(this);
        findViewById(R.id.rl_qq).setOnClickListener(this);
        findViewById(R.id.rl_wechat).setOnClickListener(this);
    }

    @Override
    public void initTitleBar(String title) {
        super.initTitleBar(title);
        if (mTitleBar != null) {
            mTitleBar.setLeftImageResource(R.drawable.arrow_left);
            mTitleBar.setLeftClickListener(v -> finish());
            mTitleBar.setActionTextColor(getResources().getColor(R.color.text_tertiary));
            mTitleBar.addAction(new TitleBar.TextAction(getResources().getString(R.string.third_platform_unbinding)) {
                @Override
                public void performAction(View view) {
                    ChooseThirdPlatformDialog dialog = new ChooseThirdPlatformDialog(ThirdPlatformBindingActivity.this, false);
                    dialog.setOnThirdPlatformChooseListener(ThirdPlatformBindingActivity.this);
                    dialog.show();
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        LogUtils.d(TAG, "onResume");
        userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo == null) {
            return;
        }
        getDialogManager().showProgressDialog(this, getResources().getString(R.string.loading));
        CoreManager.getCore(IAuthCore.class).isPhone(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        CoreManager.getCore(IWithdrawCore.class).getWithdrawUserInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fl_bindPhone:
                Intent intent = new Intent(this, BinderPhoneActivity.class);
                intent.putExtra(BinderPhoneActivity.hasBand, hasBindPhone ? BinderPhoneActivity.modifyBand : 0);
                startActivity(intent);
                break;

            case R.id.fl_bindAlipay:
                if (null != userInfo) {
                    if (!hasBindPhone) {
                        toast(R.string.third_platform_bind_phone_need);
                        return;
                    }
                    Intent aliPlayIntent = new Intent(this, BinderAlipayActivity.class);
                    startActivity(aliPlayIntent);
                }
                break;

            case R.id.rl_qq:
                if (null != userInfo && !userInfo.isHasQQ()) {
                    if (!hasBindPhone) {
                        toast(R.string.third_platform_bind_phone_need);
                        return;
                    }
                    //绑定第三方平台--qq
                    GetSmsCodeToBindThirdPlatformActivity.startForResult(this, request_code_bind_qq, true, request_code_bind_qq);
                }
                break;
            case R.id.rl_wechat:
                if (null != userInfo && !userInfo.isHasWx()) {
                    if (!hasBindPhone) {
                        toast(R.string.third_platform_bind_phone_need);
                        return;
                    }
                    //绑定第三方平台-微信
                    GetSmsCodeToBindThirdPlatformActivity.startForResult(this, request_code_bind_wx, true, request_code_bind_wx);
                }
                break;
            default:
                break;
        }
    }

    private final int request_code_bind_wx = 1;
    private final int request_code_bind_qq = 2;

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onIsPhone() {
        CoreManager.getCore(IUserCore.class).requestUserInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        hasBindPhone = true;
        updatePhoneBindStatus();
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onIsphoneFail(String error) {
        CoreManager.getCore(IUserCore.class).requestUserInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        hasBindPhone = false;
        updatePhoneBindStatus();
    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onGetWithdrawUserInfo(WithdrawInfo withdrawInfo) {
        updateAlipayBindStatus(withdrawInfo);
    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onGetWithdrawUserInfoFail(String error) {
        updateAlipayBindStatus(null);
    }

    private void updatePhoneBindStatus() {
        tv_phone.setText(hasBindPhone && !TextUtils.isEmpty(userInfo.getPhone()) ? userInfo.getPhone()
                : getResources().getString(R.string.third_platform_bind_unbinded));
    }

    private void updateAlipayBindStatus(WithdrawInfo withdrawInfo) {
        tv_aliplay.setText(withdrawInfo != null && !TextUtils.isEmpty(withdrawInfo.alipayAccount) && !withdrawInfo.alipayAccount.equals("null") ? withdrawInfo.alipayAccount
                : getResources().getString(R.string.third_platform_bind_unbinded));
    }

    //考虑到其他场景可能会修改本地缓存导致hasQq等修改为false，故每次onResume都重新获取一次接口最新数据
//    private boolean backAfterBindOrUnBindThirdPlatform = false;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogUtils.d(TAG, "onActivityResult-requestCode:" + requestCode + " resultCode:" + resultCode);
        if (request_code_bind_wx == requestCode || request_code_bind_qq == requestCode) {
//            backAfterBindOrUnBindThirdPlatform = true;
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfo(UserInfo info) {
        getDialogManager().dismissDialog();
        LogUtils.d(TAG, "onRequestUserInfo-info:" + info);
        if (info.getUid() == CoreManager.getCore(IAuthCore.class).getCurrentUid()) {
            userInfo = info;
            updateThirdPlatformBindStatus();
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfoError(String error) {
        getDialogManager().dismissDialog();
        if (!TextUtils.isEmpty(error)) {
            toast(error);
        }
    }

    @Override
    public void onThirdPlatformChoosed(ChooseThirdPlatformDialog.ThirdPlatformType third, boolean bindOrUnbind) {
        //绑定帐号界面仅仅存在解绑选择平台场景
        if (!bindOrUnbind) {
            if (third == ChooseThirdPlatformDialog.ThirdPlatformType.wechatPay && null != userInfo && !userInfo.isHasWx()) {
                toast(R.string.choose_unbind_third_platform_unbind_wx);
                return;
            }
            if (third == ChooseThirdPlatformDialog.ThirdPlatformType.qq && null != userInfo && !userInfo.isHasQQ()) {
                toast(R.string.choose_unbind_third_platform_unbind_qq);
                return;
            }

            //增加一步防御措施,当选择解除时，若未绑定手机，并且只绑定了QQ或者微信中的一种，则提示如下：“至少绑定QQ、微信、手机号中的一种作为登录账号”
            if (!hasBindPhone && null != userInfo && (userInfo.isHasWx() ^ userInfo.isHasQQ())) {
                toast(R.string.choose_unbind_third_platform_tips);
                return;
            }

            int requestCode = third == ChooseThirdPlatformDialog.ThirdPlatformType.wechatPay ? request_code_bind_wx : request_code_bind_qq;
            GetSmsCodeToBindThirdPlatformActivity.startForResult(this, requestCode, bindOrUnbind, requestCode);

        }
    }
}
