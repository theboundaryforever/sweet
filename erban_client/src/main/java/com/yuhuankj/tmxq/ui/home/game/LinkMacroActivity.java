package com.yuhuankj.tmxq.ui.home.game;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.session.activity.P2PMessageActivity;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.xchat_core.bean.GameLinkMacroInvitaion;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.widget.LinkMicroWaveView;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

/**
 * @author liaoxy
 * @Description:连麦接受页面
 * @date 2019/2/25 16:35
 */
public class LinkMacroActivity extends BaseActivity implements View.OnClickListener {

    private final String TAG = LinkMacroActivity.class.getSimpleName();
    private ImageView imvRefuse, imvAgree;
    private ImageView imvUser;
    private TextView tvUserName;
    private LinkMicroWaveView waveAnima;
    private LinearLayout llInfo;
    private GameHomeModel model;
    private GameLinkMacroInvitaion invitedInfo;

    public static void start(Context context, GameLinkMacroInvitaion invitedInfo) {
        Intent intent = new Intent(context, LinkMacroActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("data", invitedInfo);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_link_macro);
        setSwipeBackEnable(false);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        imvRefuse = (ImageView) findViewById(R.id.imvRefuse);
        imvAgree = (ImageView) findViewById(R.id.imvAgree);
        imvUser = (ImageView) findViewById(R.id.imvUser);
        tvUserName = (TextView) findViewById(R.id.tvUserName);
        waveAnima = (LinkMicroWaveView) findViewById(R.id.waveAnima);
        llInfo = findView(R.id.llInfo);
    }

    private void initListener() {
        imvRefuse.setOnClickListener(this);
        imvAgree.setOnClickListener(this);
    }

    private void initData() {
        model = new GameHomeModel();

        Intent intent = getIntent();
        if (intent == null) {
            finish();
            return;
        }
        invitedInfo = (GameLinkMacroInvitaion) intent.getSerializableExtra("data");
        if (invitedInfo == null) {
            finish();
        }
        tvUserName.setText(invitedInfo.getNick());
        ImageLoadUtils.loadImage(this, invitedInfo.getAvatar(), imvUser);
        waveAnima.post(new Runnable() {
            @Override
            public void run() {
                int[] userPoint = new int[2];
                imvUser.getLocationOnScreen(userPoint);
                int userCenterY = userPoint[1] + (imvUser.getHeight() / 2);
                int[] point = new int[2];
                waveAnima.getLocationOnScreen(point);
                int centerY = point[1] + (waveAnima.getHeight() / 2);
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) waveAnima.getLayoutParams();
                layoutParams.topMargin -= (centerY - userCenterY) + imvUser.getHeight();
                waveAnima.requestLayout();

                int startWidth = imvUser.getWidth() / 2;
                int stopWidth = llInfo.getHeight();
                waveAnima.setCustomRadus(startWidth);
                waveAnima.setStopRadus(stopWidth);
                waveAnima.setAddRadus(startWidth + 100);
                waveAnima.setDelRadus(stopWidth);
                waveAnima.setMiddleRadus(stopWidth + 20);
                waveAnima.start();
            }
        });
        IntentFilter filter = new IntentFilter(ACTION_CANCEL_LINK_MICRO);
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(broadcastReceiver, filter);
    }

    //取消通话广播动作
    public final static String ACTION_CANCEL_LINK_MICRO = "ACTION_CANCEL_LINK_MICRO";
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_CANCEL_LINK_MICRO.equals(intent.getAction())) {
                SingleToastUtil.showToast("对方已取消通话");
                waveAnima.stop();
                finish();
            }
        }
    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onClick(View v) {
        if (v == imvRefuse) {
            DialogManager dialogManager = new DialogManager(this);
            dialogManager.showProgressDialog(this, "请稍后...");
            model.refuseLinkMacro(invitedInfo.getUid() + "", new OkHttpManager.MyCallBack<ServiceResult<Object>>() {
                @Override
                public void onError(Exception e) {
                    dialogManager.dismissDialog();
                    SingleToastUtil.showToast(e.getMessage());
                }

                @Override
                public void onResponse(ServiceResult<Object> response) {
                    dialogManager.dismissDialog();
                    if (response.isSuccess()) {
                        finish();
                    } else {
                        SingleToastUtil.showToast(response.getErrorMessage());
                    }
                }
            });
        } else if (v == imvAgree) {
            DialogManager dialogManager = new DialogManager(this);
            dialogManager.showProgressDialog(this, "请稍后...");
            model.agreeLinkMacro(invitedInfo.getUid() + "", new OkHttpManager.MyCallBack<ServiceResult<Object>>() {
                @Override
                public void onError(Exception e) {
                    dialogManager.dismissDialog();
                    SingleToastUtil.showToast(e.getMessage());
                }

                @Override
                public void onResponse(ServiceResult<Object> response) {
                    dialogManager.dismissDialog();
                    if (response.isSuccess()) {
                        Bundle gotoP2pArg = new Bundle();
                        gotoP2pArg.putInt("linkMacro_status", P2PMessageActivity.AGREE_LINK_MACRO);
                        new GameHomeModel().enterLinkMacro(invitedInfo.getChannelId() + "");
                        //RtcEngineManager.get().joinLinkMacro(invitedInfo.getChannelId() + "", (int) uid);
                        NimUIKit.startP2PSession(LinkMacroActivity.this, invitedInfo.getUid() + "", gotoP2pArg);
                        finish();
                    } else {
                        SingleToastUtil.showToast(response.getErrorMessage());
                    }
                }
            });
        }
    }
}
