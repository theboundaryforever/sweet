package com.yuhuankj.tmxq.ui.me.noble;

import com.google.gson.JsonObject;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;

public interface OrderConfirmView<T extends AbstractMvpPresenter> extends IMvpBaseView {

    void onWalletInfoUpdateFail(String error);

    void onWalletInfoUpdate(WalletInfo walletInfo);

    void onBuyNoble(boolean isSuccess, JsonObject data, String msg);

}
