package com.yuhuankj.tmxq.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.microquation.linkedme.android.LinkedME;
import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.cache.NimUserInfoCache;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.netease.nim.uikit.contact.ContactEventListener;
import com.netease.nim.uikit.session.OfficSecrRedPointCountManager;
import com.netease.nim.uikit.session.SessionCustomization;
import com.netease.nim.uikit.session.SessionEventListener;
import com.netease.nim.uikit.session.actions.BaseAction;
import com.netease.nim.uikit.session.actions.ImageAction;
import com.netease.nim.uikit.session.activity.P2PMessageActivity;
import com.netease.nim.uikit.session.module.IShareFansCoreClient;
import com.netease.nim.uikit.session.module.input.InputPanel;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.NimIntent;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.StatusCode;
import com.netease.nimlib.sdk.auth.LoginInfo;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomKickOutEvent;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.netease.nimlib.sdk.msg.constant.MsgDirectionEnum;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.netease.nimlib.sdk.msg.model.RecentContact;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.tencent.bugly.crashreport.BuglyLog;
import com.tencent.bugly.crashreport.CrashReport;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.coremanager.IUserInfoCore;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.ChannelUtil;
import com.tongdaxing.erban.libcommon.utils.JavaUtil;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SiemulatorCheckUtil;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.erban.libcommon.utils.TouchAndClickOperaUtils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.erban.libcommon.utils.config.SpEvent;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.utils.pref.CommonPref;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.AccountInfo;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.auth.RealNameAuthStatusChecker;
import com.tongdaxing.xchat_core.bean.GameLinkMacroInvitaion;
import com.tongdaxing.xchat_core.bean.MiniGameCancel;
import com.tongdaxing.xchat_core.bean.MiniGameInvitedAcceptInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.bean.VisitorInfo;
import com.tongdaxing.xchat_core.bean.WebViewStyle;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.im.custom.bean.ChanceMeetingMsgAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.GameLinkMacroAgreeAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.GameLinkMacroBusyAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.GameLinkMacroCancelAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.GameLinkMacroFinishAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.GameLinkMacroInvitationAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.GameLinkMacroRefuseAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.GameLinkMacroTimeOutAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.GiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.LikeAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.LotteryAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.MiniGameCancelAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.MiniGameInvitedAcceptAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.MiniGameInvitedAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.MiniGameResultAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.MiniGameResultAttachmentGone;
import com.tongdaxing.xchat_core.im.custom.bean.NobleNotifyAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.NoticeAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.NotifyNoneAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.OpenRoomNotiAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.QmLikeAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RedPacketAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.ShareFansAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.SoundMatingMsgAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.VisitorAttachment;
import com.tongdaxing.xchat_core.im.login.IIMLoginClient;
import com.tongdaxing.xchat_core.im.message.IIMMessageCoreClient;
import com.tongdaxing.xchat_core.initial.InitInfo;
import com.tongdaxing.xchat_core.linked.ILinkedCoreClient;
import com.tongdaxing.xchat_core.linked.LinkedInfo;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomModel;
import com.tongdaxing.xchat_core.liveroom.im.model.ReUsedSocketManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomQueueInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.manager.RtcEngineManager;
import com.tongdaxing.xchat_core.manager.agora.AgoraEngineManager;
import com.tongdaxing.xchat_core.minigame.MiniGameModel;
import com.tongdaxing.xchat_core.player.IPlayerCore;
import com.tongdaxing.xchat_core.redpacket.IRedPacketCoreClient;
import com.tongdaxing.xchat_core.redpacket.bean.RedPacketInfoV2;
import com.tongdaxing.xchat_core.room.IRoomCoreClient;
import com.tongdaxing.xchat_core.room.bean.CreateRoomBean;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.security.ISecurityCore;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_core.user.VersionsCoreClient;
import com.tongdaxing.xchat_core.user.bean.CheckUpdataBean;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.XChatApplication;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.base.permission.PermissionActivity;
import com.yuhuankj.tmxq.constant.Extras;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.service.DaemonService;
import com.yuhuankj.tmxq.thirdsdk.nim.MainRedPointCountManager;
import com.yuhuankj.tmxq.thirdsdk.nim.MsgCenterRedPointStatusManager;
import com.yuhuankj.tmxq.thirdsdk.nim.RingPersonalMessageManager;
import com.yuhuankj.tmxq.thirdsdk.nim.SessionHelper;
import com.yuhuankj.tmxq.ui.find.FindHomeFragment;
import com.yuhuankj.tmxq.ui.home.fragment.NewChannelHomeFragment;
import com.yuhuankj.tmxq.ui.home.game.GameHomeModel;
import com.yuhuankj.tmxq.ui.home.game.LinkMacroActivity;
import com.yuhuankj.tmxq.ui.home.presenter.MainPresenter;
import com.yuhuankj.tmxq.ui.home.view.IMainView;
import com.yuhuankj.tmxq.ui.launch.middle.MiddleActivity;
import com.yuhuankj.tmxq.ui.liveroom.CreateRoomEnterDialog;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.AVRoomActivity;
import com.yuhuankj.tmxq.ui.login.BinderPhoneActivity;
import com.yuhuankj.tmxq.ui.login.LoginActivity;
import com.yuhuankj.tmxq.ui.login.SupplyInfo1Activity;
import com.yuhuankj.tmxq.ui.me.MeFragment;
import com.yuhuankj.tmxq.ui.message.MsgFragment;
import com.yuhuankj.tmxq.ui.message.UnreadMsgResult;
import com.yuhuankj.tmxq.ui.nim.actions.GiftAction;
import com.yuhuankj.tmxq.ui.nim.chat.DataAction;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderChanceMeeting;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderContent;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderGameLinkMacroAgree;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderGameLinkMacroBusy;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderGameLinkMacroCancel;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderGameLinkMacroFinish;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderGameLinkMacroInvitation;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderGameLinkMacroRefuse;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderGameLinkMacroTimeOut;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderGift;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderInvitationFans;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderLike;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderLottery;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderMiniGameCancel;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderMiniGameInvited;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderMiniGameInvitedAccept;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderMiniGameResult;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderMiniGameResultGone;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderNone;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderOnline;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderQmLike;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderRedPacket;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderSoundMating;
import com.yuhuankj.tmxq.ui.nim.game.GameEnitity;
import com.yuhuankj.tmxq.ui.nim.game.MiniGameAction;
import com.yuhuankj.tmxq.ui.ranklist.TabPageAdapter;
import com.yuhuankj.tmxq.ui.recommad.RecommAdDialog;
import com.yuhuankj.tmxq.ui.room.MainFragment;
import com.yuhuankj.tmxq.ui.signAward.model.RecommMicUserInfo;
import com.yuhuankj.tmxq.ui.signAward.model.SignInAwardInfo;
import com.yuhuankj.tmxq.ui.signAward.model.SignRecvGiftPkgInfo;
import com.yuhuankj.tmxq.ui.signAward.view.SignAwardDaysDialog;
import com.yuhuankj.tmxq.ui.signAward.view.SignAwardTipsDialog;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;
import com.yuhuankj.tmxq.ui.widget.RedPacketDialog;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.utils.SplashInitUtils;
import com.yuhuankj.tmxq.widget.CircleImageView;
import com.yuhuankj.tmxq.widget.MainTabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import cn.jpush.android.api.JPluginPlatformInterface;
import cn.jpush.android.api.JPushInterface;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * @author Administrator
 */
@CreatePresenter(MainPresenter.class)
public class MainActivity extends BaseMvpActivity<IMainView, MainPresenter>
        implements MainTabLayout.OnTabClickListener, IMainView {
    /**
     * 基本权限管理
     */
    private final String[] BASIC_PERMISSIONS = new String[]{
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.CAMERA,
            android.Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.RECORD_AUDIO
    };

    private static final String EXTRA_APP_QUIT = "APP_QUIT";
    private static final String TAG = "MainActivity";

    private RelativeLayout avatarLayout;
    private CircleImageView avatarImage;
    private TouchAndClickOperaUtils touchAndClickOperaUtils;

    private TextView tvName;
    private TextView tvId;

    private MainTabLayout mMainTabLayout;
    private int mCurrentMainPosition;
    private ViewGroup mViewGroup;
    private int mWidthPixels;
    private int mHeightPixels;

    private JPluginPlatformInterface pHuaweiPushInterface;
    private String extras;

    private boolean isRequestingGame = false;//是否正在请求游戏
    //游戏连麦成功开始时间
    public static long linkStartTime = 0;
    //当前是否在连麦
    public static boolean isLinkMicroing = false;
    private static boolean isLinkMicroSuding = false;
    private static String linkMicroChannelId = "";
    public static long linkMicroAccountId = -1;//连麦时对方账号ID

    private ViewPager vpFragment;
    private TabPageAdapter tabPageAdapter;
    private ArrayList<Fragment> fragments;

    private RingPersonalMessageManager ringPersonalMessageManager;
    private com.yuhuankj.tmxq.thirdsdk.nim.MainRedPointCountManager mainRedPointCountManager;
    private boolean isFirstLaunch = true;

    private String adData = "";//推荐广告信息

    public static void start(Context context) {
        start(context, null);
    }

    public static void start(Context context, Intent extras) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        if (extras != null) {
            intent.putExtras(extras);
        }
        context.startActivity(intent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(Constants.KEY_MAIN_POSITION, mCurrentMainPosition);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            mCurrentMainPosition = savedInstanceState.getInt(Constants.KEY_MAIN_POSITION);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mCurrentMainPosition = savedInstanceState.getInt(Constants.KEY_MAIN_POSITION);
        }
        setContentView(R.layout.activity_main);
        setSwipeBackEnable(false);

        initTitleBar(getString(R.string.app_name));
        if (!SplashInitUtils.getInstance().isHasInited()) {
            SplashInitUtils.getInstance().initIfNeed(getApplicationContext());
            getMvpPresenter().init(true);
        }

        initView();
        permission();

        //自动登录
        CoreManager.getCore(IAuthCore.class).autoLogin();
        MsgCenterRedPointStatusManager.getInstance().init();

        onParseIntent();
        updateRoomState(null, null);
        initP2PSessionCustomization();
        observRoomEvent();
        LogUtils.d(TAG, "onCreate-->checkUpdate request");
        checkUpdate();
        pHuaweiPushInterface = new JPluginPlatformInterface(this.getApplicationContext());
        hander.sendEmptyMessageDelayed(0, 5000L);
        //初始化连麦相关参数
        initLinkMicroData();
        //这里从广播接收
        IntentFilter filter = new IntentFilter(ACTION_OPEN_WEB);
        filter.addAction(ACTION_OPEN_P2P);
        filter.addAction(ACTION_FINISH);
        filter.addAction(ACTION_REFRESH_MSG_COUNT);
        filter.addAction(ACTION_SHOW_SIGNAWARDDIALOG);
        filter.addAction(ACTION_OPEN_FIND);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, filter);
        SiemulatorCheckUtil.chechSiemulatorEnv(this);
        registerPhoneStateListener();
    }

    private void initRoomMiniStatus() {
        LogUtils.d(TAG, "initRoomMiniStatus");
        RoomInfo roomInfo = RoomServiceScheduler.getCurrentRoomInfo();
        if (roomInfo != null) {
            if (roomInfo.getType() == RoomInfo.ROOMTYPE_HOME_PARTY) {
                LogUtils.d(TAG, "initRoomMiniStatus-->轰趴房，启动页过来，设置界面状态为最小化");
                AvRoomDataManager.get().setMinimize(true);
            } else {
                LogUtils.d(TAG, "initRoomMiniStatus-->新类型房间，启动页过来，设置界面状态为最小化");
                RoomDataManager.get().setMinimize(true);
            }
        }
    }

    private View.OnClickListener onCloseRoomClickedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            RoomInfo roomInfo = RoomServiceScheduler.getCurrentRoomInfo();
            //自己的房间才弹框提示，他人的房间直接关闭
            boolean isSelfRoom = null != roomInfo && null != CoreManager.getCore(IAuthCore.class)
                    && roomInfo.getUid() == CoreManager.getCore(IAuthCore.class).getCurrentUid();
            if (isSelfRoom) {
                getDialogManager().showOkCancelDialog("当前正在开播，是否要关闭直播？", true, new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onOk() {
                        BuglyLog.d(TAG, "onCloseRoom-->OK, close self room-->exitRoom");
                        getMvpPresenter().exitRoom();
                    }
                });
            } else {
                BuglyLog.d(TAG, "onCloseRoom-->OK, close others room-->exitRoom");
                getMvpPresenter().exitRoom();
            }
        }
    };

    public static final String ACTION_OPEN_WEB = "ACTION_OPEN_WEB";
    public static final String ACTION_OPEN_P2P = "ACTION_OPEN_P2P";
    public static final String ACTION_FINISH = "ACTION_FINISH";
    public static final String ACTION_REFRESH_MSG_COUNT = "ACTION_REFRESH_MSG_COUNT";
    public static final String ACTION_SHOW_SIGNAWARDDIALOG = "ACTION_SHOW_SIGNAWARDDIALOG";
    public static final String ACTION_OPEN_FIND = "ACTION_OPEN_FIND";//打开发现页
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_OPEN_WEB.equals(intent.getAction())) {
                //如果正在玩游戏，就跳转到游戏界面
                if (CommonWebViewActivity.isGameRunning) {
                    Intent resultIntent = new Intent(context, CommonWebViewActivity.class);
                    resultIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(resultIntent);
                }
            } else if (ACTION_OPEN_P2P.equals(intent.getAction())) {
                //如果正在连麦中则打开私聊页
                if (isLinkMicroing && linkMicroAccountId != -1) {
                    NimUIKit.startP2PSession(MainActivity.this, linkMicroAccountId + "");
                }
            } else if (ACTION_FINISH.equals(intent.getAction())) {
                finish();
            } else if (ACTION_REFRESH_MSG_COUNT.equals(intent.getAction())) {
                try {
                    MainRedPointCountManager.getInstance().updatePersonalMessageUnreadCound(mMainTabLayout);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (ACTION_SHOW_SIGNAWARDDIALOG.equals(intent.getAction())) {
                LogUtils.d(TAG, "ACTION_SHOW_SIGNAWARDDIALOG-->getNewsBigGift start SignAward Dialog");
                //完成注册后获取萌新大礼包、连续签到奖励
                getMvpPresenter().getNewsBigGift();
            } else if (ACTION_OPEN_FIND.equals(intent.getAction())) {
                start(context);
                openFindPage();
            }
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        //判断有没有被禁言
        CoreManager.getCore(IUserInfoCore.class).checkBanned(false);
        pHuaweiPushInterface.onStart(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            if (!ListUtils.isListEmpty(fragments)) {
                if (fragments.get(0) != null) {
                    (fragments.get(0)).onStop();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private SessionEventListener sessionEventListener = new SessionEventListener() {
        @Override
        public void onAvatarClicked(Context context, IMMessage message) {
            //官方帐号，私聊界面头像不可点击
            if (OfficSecrRedPointCountManager.isOfficSecrAccount(message.getFromAccount())) {
                return;
            }
            //如果是连麦中，且跳转的是自己就不让跳了
            if (isLinkMicroing) {
                long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
                if (message.getFromAccount().equals(uid + "")) {
                    return;
                }
            }
            //进入用户资料页
            UserInfoActivity.start(MainActivity.this, JavaUtil.str2long(message.getFromAccount()));
        }

        @Override
        public void onAvatarLongClicked(Context context, IMMessage message) {
        }
    };

    private void checkUpdate() {
        VersionsCore core = CoreManager.getCore(VersionsCore.class);
        LogUtils.d(TAG, "checkUpdate-->getConfig request");
        core.getConfig();
        LogUtils.d(TAG, "checkUpdate-->checkVersion request");
        core.checkVersion();
        LogUtils.d(TAG, "checkUpdate-->requestSensitiveWord request");
        core.requestSensitiveWord();
    }

    @CoreEvent(coreClientClass = VersionsCoreClient.class)
    public void onVersionUpdataDialog(CheckUpdataBean checkUpdataBean) {
        LogUtils.d(TAG, "onVersionUpdataDialog-checkUpdataBean:" + checkUpdataBean);
        if (checkUpdataBean == null) {
            return;
        }

        String updateVersion = checkUpdataBean.getUpdateVersion();
        if (TextUtils.isEmpty(updateVersion)) {
            return;
        }

        if (1 == checkUpdataBean.getStatus()) {
            return;
        }
        final boolean force = 3 == checkUpdataBean.getStatus();

        long l = (Long) SpUtils.get(MainActivity.this, "updataDialogDissTime", 0L);
        if (System.currentTimeMillis() - l < (86400 * 1000) && !force) {
            return;
        }

        AlertDialog alertDialog = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)
                .setTitle(R.string.update_new_version_tips)
                .setMessage(checkUpdataBean.getUpdateVersionDesc())
                .setPositiveButton(R.string.update, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String download_url = checkUpdataBean.getDownloadUrl();
                        if (TextUtils.isEmpty(download_url)) {
                            download_url = UriProvider.IM_SERVER_URL;
                        }
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(download_url));
                        startActivity(intent);
                        finish();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (force) {
                            finish();
                        } else {
                            SpUtils.put(MainActivity.this, "updataDialogDissTime", System.currentTimeMillis());
                        }
                    }
                }).create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(!force);
        alertDialog.show();
    }


    private void closeOpenRoomAnimation() {
//        avatarImage.clearAnimation();
        avatarLayout.setVisibility(View.GONE);
    }

    private Runnable showCallUpRunnable = null;

    private void observRoomEvent() {
        Disposable subscribe1 = IMNetEaseManager.get().getChatRoomEventObservable()
                .subscribe(new Consumer<RoomEvent>() {
                    @Override
                    public void accept(RoomEvent roomEvent) throws Exception {
                        if (roomEvent == null || roomEvent.getEvent() == RoomEvent.NONE) {
                            return;
                        }
                        int event = roomEvent.getEvent();
                        if (event == RoomEvent.ENTER_ROOM) {
                            BuglyLog.d(TAG, "NIM_ENTER_ROOM->onEnter");
                            onEnter(AvRoomDataManager.get().mCurrentRoomInfo);
                        } else if (event == RoomEvent.KICK_OUT_ROOM) {
                            ChatRoomKickOutEvent reason = roomEvent.getReason();
                            if (reason == null) {
                                return;
                            }
                            //加入黑名单，踢出房间回调
                            ChatRoomKickOutEvent.ChatRoomKickOutReason reasonReason = reason.getReason();
                            if (reasonReason == ChatRoomKickOutEvent.ChatRoomKickOutReason.BE_BLACKLISTED
                                    || reasonReason == ChatRoomKickOutEvent.ChatRoomKickOutReason.KICK_OUT_BY_MANAGER
                                    || reasonReason == ChatRoomKickOutEvent.ChatRoomKickOutReason.KICK_OUT_BY_CONFLICT_LOGIN
                                    || reasonReason == ChatRoomKickOutEvent.ChatRoomKickOutReason.CHAT_ROOM_INVALID) {
                                BuglyLog.d(TAG, "KICK_OUT_ROOM-->onRoomExited");
                                onRoomExited();
                            }
                        } else if (event == RoomEvent.ROOM_EXIT) {
                            BuglyLog.d(TAG, "NIM_ROOM_EXIT-->onRoomExited");
                            onRoomExited();
                        } else if (event == RoomEvent.ROOM_CHAT_RECONNECTION) {
                            updateRoomState(roomEvent.roomQueueInfo, null);
                        } else if (event == RoomEvent.CALL_UP_MSG) {
                            LogUtils.d(TAG, "observRoomEvent-->accept-->CALL_UP_MSG");
                            showCallUpDelay(roomEvent);
                        }
                    }
                });
        mCompositeDisposable.add(subscribe1);

        IMRoomMessageManager.get().subscribeIMRoomEventObservable(roomEvent -> {
            if (roomEvent == null || roomEvent.getEvent() == RoomEvent.NONE) {
                return;
            }
            int event = roomEvent.getEvent();
            if (event == RoomEvent.ENTER_ROOM) {
                BuglyLog.d(TAG, "IM->ENTER_ROOM->onEnter");
                onEnter(RoomDataManager.get().getCurrentRoomInfo());
            } else if (event == RoomEvent.KICK_OUT_ROOM) {
                BuglyLog.d(TAG, "IM->KICK_OUT_ROOM-->onRoomExited");
                if (!TextUtils.isEmpty(roomEvent.getReasonMsg())) {
                    toast(roomEvent.getReasonMsg());
                }
                onRoomExited();
            } else if (event == RoomEvent.ROOM_EXIT) {
                BuglyLog.d(TAG, "IM->ROOM_EXIT-->onRoomExited");
                onRoomExited();
            } else if (event == RoomEvent.ROOM_CHAT_RECONNECTION) {
                updateRoomState(null, roomEvent.getImRoomQueueInfo());
            } else {

            }
        }, this);
    }

    //所有用户，收到召集令后，延迟五秒显示
    private void showCallUpDelay(final RoomEvent roomEvent) {
        LogUtils.d(TAG, "showCallUpDelay");
        if (null != showCallUpRunnable) {
            LogUtils.d(TAG, "showCallUpDelay 此前已经接收到召集令消息，后续的不做展示处理");
            return;
        }
        if (null == showCallUpRunnable) {
            showCallUpRunnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        if (vpFragment.getCurrentItem() == 0 && BaseRoomServiceScheduler.getCurrentRoomInfo() == null) {
                            ((NewChannelHomeFragment) fragments.get(0)).showCallUpDlg(roomEvent.getCallUpBean());
                        }
                        vpFragment.removeCallbacks(showCallUpRunnable);
                        showCallUpRunnable = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
        }
        vpFragment.postDelayed(showCallUpRunnable, 5000L);

    }

    private void hideCallUp(){
        if (vpFragment.getCurrentItem() == 0 && null != fragments && fragments.get(0) != null
                && fragments.get(0) instanceof NewChannelHomeFragment) {
            ((NewChannelHomeFragment) fragments.get(0)).hideCallUp();
        }
    }

    private void initP2PSessionCustomization() {
        NimUIKit.registerMsgItemViewHolder(OpenRoomNotiAttachment.class, MsgViewHolderOnline.class);
        NimUIKit.registerMsgItemViewHolder(GiftAttachment.class, MsgViewHolderGift.class);
        NimUIKit.registerMsgItemViewHolder(NoticeAttachment.class, MsgViewHolderContent.class);
        NimUIKit.registerMsgItemViewHolder(ChanceMeetingMsgAttachment.class, MsgViewHolderChanceMeeting.class);
        NimUIKit.registerMsgItemViewHolder(RedPacketAttachment.class, MsgViewHolderRedPacket.class);
        NimUIKit.registerMsgItemViewHolder(LotteryAttachment.class, MsgViewHolderLottery.class);
        NimUIKit.registerMsgItemViewHolder(ShareFansAttachment.class, MsgViewHolderInvitationFans.class);
        NimUIKit.registerMsgItemViewHolder(MiniGameInvitedAttachment.class, MsgViewHolderMiniGameInvited.class);
        NimUIKit.registerMsgItemViewHolder(MiniGameInvitedAcceptAttachment.class, MsgViewHolderMiniGameInvitedAccept.class);
        NimUIKit.registerMsgItemViewHolder(MiniGameResultAttachment.class, MsgViewHolderMiniGameResult.class);
        NimUIKit.registerMsgItemViewHolder(MiniGameResultAttachmentGone.class, MsgViewHolderMiniGameResultGone.class);
        NimUIKit.registerMsgItemViewHolder(MiniGameCancelAttachment.class, MsgViewHolderMiniGameCancel.class);
        NimUIKit.registerMsgItemViewHolder(GameLinkMacroInvitationAttachment.class, MsgViewHolderGameLinkMacroInvitation.class);
        NimUIKit.registerMsgItemViewHolder(GameLinkMacroAgreeAttachment.class, MsgViewHolderGameLinkMacroAgree.class);
        NimUIKit.registerMsgItemViewHolder(NobleNotifyAttachment.class, MsgViewHolderNone.class);
        NimUIKit.registerMsgItemViewHolder(NotifyNoneAttachment.class, MsgViewHolderNone.class);
        NimUIKit.registerMsgItemViewHolder(GameLinkMacroRefuseAttachment.class, MsgViewHolderGameLinkMacroRefuse.class);
        NimUIKit.registerMsgItemViewHolder(GameLinkMacroCancelAttachment.class, MsgViewHolderGameLinkMacroCancel.class);
        NimUIKit.registerMsgItemViewHolder(GameLinkMacroFinishAttachment.class, MsgViewHolderGameLinkMacroFinish.class);
        NimUIKit.registerMsgItemViewHolder(GameLinkMacroTimeOutAttachment.class, MsgViewHolderGameLinkMacroTimeOut.class);
        NimUIKit.registerMsgItemViewHolder(GameLinkMacroBusyAttachment.class, MsgViewHolderGameLinkMacroBusy.class);
        NimUIKit.registerMsgItemViewHolder(LikeAttachment.class, MsgViewHolderLike.class);
        NimUIKit.registerMsgItemViewHolder(SoundMatingMsgAttachment.class, MsgViewHolderSoundMating.class);
        NimUIKit.registerMsgItemViewHolder(QmLikeAttachment.class, MsgViewHolderQmLike.class);

        NimUIKit.setSessionListener(sessionEventListener);
        NimUIKit.setContactEventListener(contactEventListener);
    }

    private void getGames() {
        if (!CoreManager.getCore(IAuthCore.class).isLogin()) {
            return;
        }
        isRequestingGame = true;
        new MiniGameModel().getGames(new OkHttpManager.MyCallBack<ServiceResult<List<GameEnitity>>>() {

            @Override
            public void onError(Exception e) {
                isRequestingGame = false;
                SessionCustomization sessionCustomization = new SessionCustomization();
                ArrayList<BaseAction> actions = new ArrayList<>();
                actions.add(new ImageAction());
                actions.add(new GiftAction());
                actions.add(new DataAction());
                sessionCustomization.actions = actions;
                sessionCustomization.withSticker = true;
                sessionCustomization.backgroundColor = Color.parseColor("#f3f5f8");
                NimUIKit.setCommonP2PSessionCustomization(sessionCustomization);
            }

            @Override
            public void onResponse(ServiceResult<List<GameEnitity>> response) {
                isRequestingGame = false;
                SessionCustomization sessionCustomization = new SessionCustomization();
                ArrayList<BaseAction> actions = new ArrayList<>();
                actions.add(new ImageAction());
                actions.add(new GiftAction());
                actions.add(new DataAction());
                if (response.isSuccess()) {
                    for (GameEnitity game : response.getData()) {
                        actions.add(new MiniGameAction(game.getImage(), game.getName(), game.getId() + "", game.getName(), game.getPlayers() + ""));
                    }
                }
                sessionCustomization.actions = actions;
                sessionCustomization.withSticker = true;
                sessionCustomization.backgroundColor = Color.parseColor("#f3f5f8");
                NimUIKit.setCommonP2PSessionCustomization(sessionCustomization);
            }
        });

    }

    private ContactEventListener contactEventListener = new ContactEventListener() {
        @Override
        public void onItemClick(Context context, String account) {
            NimUIKit.startP2PSession(context, account);
        }

        @Override
        public void onItemLongClick(Context context, String account) {
        }

        @Override
        public void onAvatarClick(Context context, String account) {
            UserInfoActivity.start(MainActivity.this, JavaUtil.str2long(account));
        }

        @Override
        public String getCustomMsgText(MsgAttachment msgAttachment) {
            if (msgAttachment == null) {
                return "";
            }
            if (msgAttachment instanceof CustomAttachment) {
                CustomAttachment customAttachment = (CustomAttachment) msgAttachment;
                int first = customAttachment.getFirst();
                int second = customAttachment.getSecond();
                switch (first) {
                    case CustomAttachment.CUSTOM_MSG_GAME_LINK_MACRO:
                        return "[通话]";
                    case CustomAttachment.CUSTOM_MSG_MINI_GAME:
                        return "[游戏]";
                    default:
                        break;
                }
            }
            return "...";
        }
    };

    @Override
    protected void onStop() {
        super.onStop();
        LogUtils.d(TAG, "onStop");
        pHuaweiPushInterface.onStop(this);
        try {
            //保存消息红点相关的数据到缓存中
            OfficSecrRedPointCountManager.save(this,
                    CoreManager.getCore(IAuthCore.class).getCurrentUid());
            MainRedPointCountManager.getInstance().saveU2UMsgCount(this,
                    CoreManager.getCore(IAuthCore.class).getCurrentUid());
        } catch (Exception e) {
            e.printStackTrace();
        }
        hideCallUp();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LogUtils.d(TAG, "onResume");
        LinkedME.getInstance().setImmediate(true);
        //如果没有取到游戏数据数据，则重新获取
        if (!isRequestingGame) {
            if (NimUIKit.commonP2PSessionCustomization == null
                    || NimUIKit.commonP2PSessionCustomization.actions == null
                    || NimUIKit.commonP2PSessionCustomization.actions.size() < 4) {
                getGames();
            }
        }
        //获取设备时间与正确时间差值
        if (MsgViewHolderMiniGameInvited.deviceTimeOffset == 0) {
            MsgViewHolderMiniGameInvited.getDeviceTimeOffset();
        }

        //如果当前已经回到首页，还在连麦，说明出现问题，直接退出连麦
        if (isLinkMicroing) {
            exitLinkMacro();
        }
        if (!isFirstLaunch) {
            MainRedPointCountManager.getInstance().updatePersonalMessageUnreadCound(mMainTabLayout);
        }
        if (isFirstLaunch) {
            isFirstLaunch = false;
        }
    }

    private void initView() {
        mHeightPixels = getResources().getDisplayMetrics().heightPixels;
        mWidthPixels = getResources().getDisplayMetrics().widthPixels;

        mViewGroup = (ViewGroup) findViewById(R.id.root);
        mViewGroup.requestLayout();

        mMainTabLayout = (MainTabLayout) findViewById(R.id.main_tab_layout);
        mMainTabLayout.setOnTabClickListener(this);
        mMainTabLayout.select(mCurrentMainPosition);

        avatarLayout = (RelativeLayout) findViewById(R.id.avatar_image_layout);
        avatarLayout.setVisibility(View.GONE);
        avatarImage = (CircleImageView) findViewById(R.id.avatar_image);
        tvName = (TextView) findViewById(R.id.tv_name);
        tvId = (TextView) findViewById(R.id.tv_id);
        findViewById(R.id.avatar_close_image).setOnClickListener(onCloseRoomClickedListener);
        initAvatarLayout();

        //改成可左右切换的样式
        vpFragment = (ViewPager) findViewById(R.id.vpFragment);
        fragments = new ArrayList<>();
//        String channelId = StatisticModel.getInstance().getChannelIfExisted(this);
//        if (!TextUtils.isEmpty(channelId) && channelId.endsWith("_new")) {
        fragments.add(new NewChannelHomeFragment());
//        } else {
//            fragments.add(new GameHomeFragment());
//        }
        fragments.add(new MainFragment());
        fragments.add(new FindHomeFragment());
        fragments.add(new MsgFragment());
        fragments.add(new MeFragment());
        tabPageAdapter = new TabPageAdapter(getSupportFragmentManager(), fragments);
        vpFragment.setAdapter(tabPageAdapter);
        vpFragment.setOffscreenPageLimit(5);

        vpFragment.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                LogUtils.d(TAG, "initView-->onPageSelected position:" + position);
                mMainTabLayout.select(position);
                hideCallUp();
            }
        });

        //[_first]渠道包默认跳发现页
        String channelId = ChannelUtil.getChannel(this);
        if (!TextUtils.isEmpty(channelId) && channelId.endsWith("_first")) {
            LogUtils.d(TAG, "initView-->go to find page");
            vpFragment.setCurrentItem(2);
        } else {
            vpFragment.setCurrentItem(0);
        }
    }

    /**
     * 房间入口按钮拖动逻辑
     */
    private void initAvatarLayout() {
        LogUtils.d(TAG, "initAvatarLayout");
        touchAndClickOperaUtils = new TouchAndClickOperaUtils(this);
        touchAndClickOperaUtils.setListener(new TouchAndClickOperaUtils.OnQuickClickListener() {
            @Override
            public void onQuickClick() {
                LogUtils.d(TAG, "initAvatarLayout-->onQuickClick");
                //区分当前所在的房间
                RoomInfo roomInfo = RoomServiceScheduler.getCurrentRoomInfo();
                LogUtils.d(TAG, "initAvatarLayout-->onQuickClick roomInfo is null :" + (null == roomInfo));
                if (null != roomInfo) {
                    RoomServiceScheduler.getInstance().enterRoom(MainActivity.this, roomInfo.getUid(), roomInfo.getType());
                }
            }
        });
        touchAndClickOperaUtils.setTargetView(avatarLayout);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) avatarLayout.getLayoutParams();
        int width1 = layoutParams.width;
        int height1 = layoutParams.height;
        ViewGroup.LayoutParams mMainTabLayoutLayoutParams = mMainTabLayout.getLayoutParams();
        int mMainTabHeight = mMainTabLayoutLayoutParams.height;
        touchAndClickOperaUtils.setMarginButtom(mMainTabHeight);
        int mh = mHeightPixels - height1 - mMainTabHeight;
        int mw = mWidthPixels - width1 - 10;

        layoutParams.leftMargin = mw;
        layoutParams.topMargin = mh;
        avatarLayout.setLayoutParams(layoutParams);
        mViewGroup.requestLayout();
    }

    private long startTime;
    private boolean shown;

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        onParseIntent();
        LogUtils.d(TAG, "onNewIntent-->initRoomMiniStatus");
        initRoomMiniStatus();
    }

    private void onParseIntent() {
        Intent intent = getIntent();
        if (intent.getIntExtra("curPage", -1) >= 0) {
            int curPage = intent.getIntExtra("curPage", -1);
            mMainTabLayout.select(curPage);
            return;
        }
        if (intent.hasExtra(NimIntent.EXTRA_NOTIFY_CONTENT)) {
            IMMessage message = (IMMessage) getIntent().getSerializableExtra(NimIntent.EXTRA_NOTIFY_CONTENT);
            switch (message.getSessionType()) {
                case P2P:
                    SessionHelper.startP2PSession(this, message.getSessionId());
                    break;
                case Team:
                    SessionHelper.startTeamSession(this, message.getSessionId());
                    break;
                default:
                    break;
            }
        } else if (intent.hasExtra(EXTRA_APP_QUIT)) {
            onLogout();
        } else if (intent.hasExtra(Extras.EXTRA_JUMP_P2P)) {
            Intent data = intent.getParcelableExtra(Extras.EXTRA_DATA);
            String account = data.getStringExtra(Extras.EXTRA_ACCOUNT);
            if (!TextUtils.isEmpty(account)) {
                SessionHelper.startP2PSession(this, account);
            }
        } else if (intent.hasExtra("url") && intent.hasExtra("type")) {
            startTime = System.currentTimeMillis();
        }
        parseJPushInfo();
    }

    /**
     * 解析极光推送内容
     */
    private void parseJPushInfo() {
        boolean isLogin = CoreManager.getCore(IAuthCore.class).isLogin();
        LogUtils.d(TAG, "parseJPushInfo-isLogin:" + isLogin);
        Intent intent = getIntent();
        //解析极光通知推送附带的数据内容
        if (isLogin && intent.hasExtra("extras")) {
            extras = intent.getStringExtra("extras");
            LogUtils.d(TAG, "parseJPushInfo-extras:" + extras);
            long currentUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
            if (!TextUtils.isEmpty(extras)) {
                try {
                    Json json = new Json(extras);
                    if (null != json) {
                        long roomUid = json.num_l("roomUid");
                        if (roomUid > 0) {
                            if (roomUid == currentUid) {
                                return;
                            }
                            //甜蜜星球V1.0.0.2及后续版本需要新增的字段
                            int roomType = RoomInfo.ROOMTYPE_HOME_PARTY;
                            if (json.has("roomType")) {
                                roomType = json.num("roomType");
                            }
                            //存在已知问题：进房瞬间IM还未登录成功或者断线，此时IM登录成功后，未能重新走enterRoom
                            // 比较容易复现的场景是极光通知唤起app后直接进房
                            RoomServiceScheduler.getInstance().enterRoom(MainActivity.this, roomUid, roomType);
                        } else {
                            autoJump(json.str("activity"), json.str("url"), json.json_ok("params"));
                        }
                        //登录成功更新个人资料后才进行跳转的话，清空intent中的携带数据，避免多次重复跳转
                        intent.putExtra("extras", "");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void permission() {
        checkPermission(new PermissionActivity.CheckPermListener() {
            @Override
            public void superPermission() {
            }
        }, R.string.ask_again_new, BASIC_PERMISSIONS);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LogUtils.d(TAG, "onDestroy");
        if (vpFragment != null) {
            vpFragment.clearOnPageChangeListeners();
        }
        sessionEventListener = null;
        contactEventListener = null;
        MsgCenterRedPointStatusManager.getInstance().release();
        NimUIKit.setSessionListener(null);
        NimUIKit.setContactEventListener(null);
        NimUIKit.setOnInputMsgSendableCheckListener(null);
        NimUIKit.setOnPrivateMsgAntiSpamOptionChangedListener(null);
        NimUIKit.onP2PSessionOpenListener = null;
        NimUIKit.onServerEmojiOperaListener = null;
        NimUIKit.onSendLinkMicroInviteMsgListener = null;
        if (null != hander) {
            hander.removeMessages(0);
        }
        MsgViewHolderMiniGameInvited.stop();

        if (broadcastReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        }
        if (null != ringPersonalMessageManager) {
            ringPersonalMessageManager.release();
        }
        MsgCenterRedPointStatusManager.getInstance().release();
        if (null != CoreManager.getCore(IPlayerCore.class)) {
            CoreManager.getCore(IPlayerCore.class).clearCurrLocalMusicInifo();
        }
        BaseRoomServiceScheduler.exitRoom(new CallBack<String>() {
            @Override
            public void onSuccess(String data) {

            }

            @Override
            public void onFail(int code, String error) {

            }
        });
        if (null != vpFragment && null != showCallUpRunnable) {
            vpFragment.removeCallbacks(showCallUpRunnable);
        }
    }

    private long lastClickBackKeyTime = 0L;

    /**
     * 双击返回键退出
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // 当用户准备退出应用时，是将应用压到后台，而不是正常的关闭activity
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        long timeInner = System.currentTimeMillis() - lastClickBackKeyTime;
        LogUtils.d(TAG, "onBackPressed-timeInner:" + timeInner);
        if (timeInner < 2000L) {
            //两次点击间隔小于2秒，直接退出
            LogUtils.d(TAG, "onBackPressed-两次点击间隔小于2秒，直接退出");
            moveTaskToBack(true);
        } else {
            lastClickBackKeyTime = System.currentTimeMillis();
            LogUtils.d(TAG, "onBackPressed-两次点击间隔大于等于2秒");
            toast(getResources().getString(R.string.main_back_key_press_tips));
        }
    }

    private void checkoutLinkedMe() {
        Intent intent = new Intent(this, MiddleActivity.class);
        startActivity(intent);
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogin(AccountInfo accountInfo) {

        NimUIKit.setOnInputMsgSendableCheckListener(new InputPanel.OnInputMsgSendableCheckListener() {
            @Override
            public boolean checkInputMsgSendable(Context context) {
                return RealNameAuthStatusChecker.getInstance().needBindPhoneFirst(context, getResources().getString(R.string.phone_bind_tips));
            }
        });
        NimUIKit.setOnPrivateMsgAntiSpamOptionChangedListener(new InputPanel.OnPrivateMsgAntiSpamOptionChangedListener() {
            @Override
            public boolean checkAntiOptionEnable() {
                return IMNetEaseManager.get().checkAntiOptionEnable();
            }
        });
        NimUIKit.onP2PSessionOpenListener = new NimUIKit.OnP2PSessionOpenListener() {
            @Override
            public void onP2PSessionOpened(String account) {
                MainRedPointCountManager.getInstance().delU2UMsgCount(account);
            }
        };
        NimUIKit.onServerEmojiOperaListener = new NimUIKit.OnServEmojiTopicInitListener() {
            @Override
            public void onNeedInitEmojiFromServer(long targetUid) {
                getMvpPresenter().getEmojiListFromServer(targetUid);
            }

            @Override
            public void onReportEmojiSent(long targetUid) {
                getMvpPresenter().reportEmojiSent(targetUid);
            }

            @Override
            public void getRandomTopic(long targetUid) {
                getMvpPresenter().getRandomTopic(targetUid);
            }
        };
        NimUIKit.onSendLinkMicroInviteMsgListener = new NimUIKit.OnSendLinkMicroInviteMsgListener() {
            @Override
            public boolean onSendLinkMicroInviteMsg() {
                return null != BaseRoomServiceScheduler.getCurrentRoomInfo();
            }
        };
        checkoutLinkedMe();
        VersionsCore core = CoreManager.getCore(VersionsCore.class);
        if (core != null) {
            core.getChannelConfigure();
        }

        if (accountInfo != null) {
            CrashReport.setUserId(accountInfo.getUid() + "");
            LogUtils.d(TAG, "onLogin-currentUid:" + accountInfo.getUid() + " check SignAward...");
            if (accountInfo.getUid() > 0L) {
                String alias = BasicConfig.isDebug ? "debug_user_" + accountInfo.getUid() : "user_" + accountInfo.getUid();
                String lastAlias = (String) SpUtils.get(this, SpEvent.has_set_alias, "");
                LogUtils.d(TAG, "onLogin-alias:" + alias + " lastAlias:" + lastAlias);
                //下面的if判断 可能导致覆盖安装会有问题
//                if (TextUtils.isEmpty(lastAlias) || !lastAlias.equals(alias)) {
                Random random = new Random(Integer.MAX_VALUE);
                int sequence = random.nextInt();
                //新的调用会覆盖之前的设置
                JPushInterface.setAlias(this, sequence, alias);
                SpUtils.put(this, SpEvent.has_set_alias, alias);
                LogUtils.d(TAG, "onLogin-alias:" + alias + " sequence:" + sequence);
//                }
                getGames();
            }

            //2019年5月22日 17:06:15 甜蜜星球1.0.0.2版本，推荐广告逻辑暂时屏蔽
//            getMvpPresenter().getRecommAdInfo();

            //获取萌新大礼包、连续签到奖励
            //如果是注册流程，那么这里接口调用会报错，提示用户需要先完善资料
            LogUtils.d(TAG, "onLogin-->getNewsBigGift,check SignAward...");
            getMvpPresenter().getNewsBigGift();
            getMvpPresenter().getUnreadMsgCount(new OkHttpManager.MyCallBack<ServiceResult<UnreadMsgResult>>() {

                @Override
                public void onError(Exception e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(ServiceResult<UnreadMsgResult> response) {
                    if (response != null && response.isSuccess() && null != response.getData()) {
                        UnreadMsgResult unreadMsgResult = response.getData();
                        MsgCenterRedPointStatusManager.getInstance().setShowSignInRedPoint(unreadMsgResult.getSignCount() > 0);
                    }
                }
            });
            if (null != XChatApplication.sdkOptions) {
                try {
                    NIMClient.updateStatusBarNotificationConfig(XChatApplication.sdkOptions.statusBarNotificationConfig);
                } catch (IllegalStateException iilse) {
                    iilse.printStackTrace();
                }
            }
            CoreManager.getCore(ISecurityCore.class).securityCheck(this);
            //登录成功后回到首页
            try {
                String channelId = ChannelUtil.getChannel(this);
                //[_first]渠道包默认跳发现页
                if (!TextUtils.isEmpty(channelId) && channelId.endsWith("_first")) {
                    mMainTabLayout.select(2);
                } else {
                    mMainTabLayout.select(0);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                //从缓存中恢复消息红点相关的数据
                //TODO 解决这里抛出的异常问题
                OfficSecrRedPointCountManager.restore(this, accountInfo.getUid());
                MainRedPointCountManager.getInstance().restoreU2UMsgCount(this, accountInfo.getUid());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onRequestTicketFail(String error) {
        toast(error);
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogout() {
        getMvpPresenter().exitRoom();
        ReUsedSocketManager.get().destroy();
        LoginActivity.start(MainActivity.this, LoginActivity.Enter_Type_Login);
        //进入登录页面
        StatisticManager.get().onEvent(this,
                StatisticModel.EVENT_ID_ENTER_LOGIN,
                StatisticModel.getInstance().getUMAnalyCommonMap(this));
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onNeedLogin() {
        LoginActivity.start(MainActivity.this, LoginActivity.Enter_Type_Login);
        //进入登录页面
        StatisticManager.get().onEvent(this,
                StatisticModel.EVENT_ID_ENTER_LOGIN,
                StatisticModel.getInstance().getUMAnalyCommonMap(this));
    }

    @CoreEvent(coreClientClass = IIMLoginClient.class)
    public void onImLoginSuccess(LoginInfo loginInfo) {
        //云信IM登录成功，会拉取一次未读消息
        MainRedPointCountManager.getInstance().updatePersonalMessageUnreadCound(mMainTabLayout);

        // 如果需要跳转,在这里实现
        if (!shown && (System.currentTimeMillis() - startTime) <= 2000 &&
                getIntent().hasExtra("url") && getIntent().hasExtra("type")) {
            shown = true;
            int type = getIntent().getIntExtra("type", 0);
            String url = getIntent().getStringExtra("url");
            if (type == 3) {
                Intent intent = new Intent(this, CommonWebViewActivity.class);
                intent.putExtra("url", url);
                startActivity(intent);
            } else if (type == 2) {
                //如果type=2表示打开的是房间，那么url实际上时roomUid的字符串格式值
                //那么就需要新增一个roomType
                int roomType = RoomInfo.ROOMTYPE_HOME_PARTY;
                if (getIntent().hasExtra("roomType")) {
                    roomType = getIntent().getIntExtra("roomType", roomType);
                }
                RoomServiceScheduler.getInstance().enterRoom(this, JavaUtil.str2long(url), roomType);
            }
        }
    }

    @CoreEvent(coreClientClass = IIMLoginClient.class)
    public void onImLoginFaith(String error) {
        toast(error);
        LoginActivity.start(MainActivity.this, LoginActivity.Enter_Type_Login);
        //进入登录页面
        StatisticManager.get().onEvent(this,
                StatisticModel.EVENT_ID_ENTER_LOGIN,
                StatisticModel.getInstance().getUMAnalyCommonMap(this));
    }

    @CoreEvent(coreClientClass = IIMLoginClient.class)
    public void onKickedOut(StatusCode code) {
        toast("您已被踢下线，若非正常行为，请及时修改密码");
        CoreManager.getCore(IAuthCore.class).logout();
    }

    @CoreEvent(coreClientClass = IIMMessageCoreClient.class)
    public void onReceiveRecentContactChanged(List<RecentContact> imMessages) {
        LogUtils.d(TAG, "onReceiveRecentContactChanged-size:" + (null == imMessages ? 0 : imMessages.size()));
        boolean updateMsgUnReadCount = MainRedPointCountManager.getInstance().filterRecentContactMessage(imMessages);
        LogUtils.d(TAG, "onReceiveRecentContactChanged-updateMsgUnReadCount:" + updateMsgUnReadCount);
        if (updateMsgUnReadCount) {
            MainRedPointCountManager.getInstance().updatePersonalMessageUnreadCound(mMainTabLayout);
        }
    }

    @CoreEvent(coreClientClass = IIMMessageCoreClient.class)
    public void onReceivePersonalMessages(List<IMMessage> imMessages) {
        LogUtils.d(TAG, "onReceivePersonalMessages-imMessage.size:" + (null == imMessages ? 0 : imMessages.size()));
        if (null == ringPersonalMessageManager) {
            ringPersonalMessageManager = new RingPersonalMessageManager();
        }

        boolean ringOnRecvPersonalMsg = MainRedPointCountManager.getInstance().filterCustomNotifyPersonalMessage(imMessages);
        if (ringOnRecvPersonalMsg) {
            ringPersonalMessageManager.ringOnRecvivePersonalMessage();
        }

        if (imMessages != null && imMessages.size() > 0) {
            for (IMMessage msg : imMessages) {
                LogUtils.d(TAG, "onReceivePersonalMessages-msg.getMsgType:" + msg.getMsgType() + " MsgTypeEnum.custom:" + MsgTypeEnum.custom);
                if (msg.getMsgType() == MsgTypeEnum.custom) {
                    CustomAttachment customAttachment = ((CustomAttachment) msg.getAttachment());
                    int first = customAttachment.getFirst();
                    int second = customAttachment.getSecond();
                    LogUtils.d(TAG, "onReceivePersonalMessages-first:" + first + " second:" + second);
                    //小游戏邀请的通知
                    if (first == CustomAttachment.CUSTOM_MSG_MINI_GAME && !msg.isRemoteRead()) {
                        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent("scrollToBottom"));
                        if (second == CustomAttachment.CUSTOM_MSG_MINI_GAME_ON_INVITED_NOTIFY) {
                            MsgViewHolderMiniGameInvited.addMsgTime(msg.getTime(), System.currentTimeMillis());
                        } else if (second == CustomAttachment.CUSTOM_MSG_MINI_GAME_ON_INVITED_ACCEPT_NOTIFY) {
                            //游戏邀请接受通知
                            MiniGameInvitedAcceptInfo invitedInfo = ((MiniGameInvitedAcceptAttachment) customAttachment).getDataInfo();
                            if (invitedInfo != null) {
                                int status;
                                if (invitedInfo.getAcceptInvt() == 1) {
                                    status = MsgViewHolderMiniGameInvited.AGREE;
                                } else {
                                    status = MsgViewHolderMiniGameInvited.REFUSE;
                                }
                                SpUtils.put(this, invitedInfo.getRoomid() + "", status);
                                //进入游戏
                                startGame(this, invitedInfo);
                            }
                        } else if (second == CustomAttachment.CUSTOM_MSG_MINI_GAME_ON_CANCEL_NOTIFY) {
                            //游戏邀请取消通知
                            MiniGameCancel invitedInfo = ((MiniGameCancelAttachment) customAttachment).getDataInfo();
                            if (invitedInfo != null) {
                                SpUtils.put(this, invitedInfo.getRoomid() + "", MsgViewHolderMiniGameInvited.CANCEL);
                            }
                        }
                    } else if (first == CustomAttachment.CUSTOM_MSG_GAME_LINK_MACRO) {
                        //如果消息发送的时间起过程30秒,则放弃处理
                        if (MsgViewHolderMiniGameInvited.getCurTimeMillis() - msg.getTime() > 30000) {
                            LogUtils.e("onReceivePersonalMessages-该连麦消息超时了");
                            continue;
                        }
                        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent("scrollToBottom"));
                        //游戏连麦
                        if (second == CustomAttachment.CUSTOM_MSG_GAME_LINK_MACRO_INVITATION) {
                            GameLinkMacroInvitaion info = ((GameLinkMacroInvitationAttachment) msg.getAttachment()).getDataInfo();
                            if (msg.getDirect() == MsgDirectionEnum.In) {
                                if (info != null) {
                                    if (isLinkMicroing) {
                                        //占线
                                        linkMicroBusy(info.getUid(), 2);
                                        continue;
                                    }
                                    LinkMacroActivity.start(MainActivity.this, info);
                                }
                            }
                            isLinkMicroing = true;
                        } else if (second == CustomAttachment.CUSTOM_MSG_GAME_LINK_MACRO_AGREE_GAME) {
                            GameLinkMacroInvitaion info = ((GameLinkMacroAgreeAttachment) msg.getAttachment()).getDataInfo();
                            isLinkMicroSuding = true;
                            isLinkMicroing = true;
                            linkStartTime = System.currentTimeMillis();
                            linkMicroChannelId = info.getChannelId() + "";
                            long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
                            //记录下对方的账号
                            if (info.getInvitationUid() == uid) {
                                linkMicroAccountId = info.getUid();
                            } else {
                                linkMicroAccountId = info.getInvitationUid();
                            }
                            //如果当前在房间内则退出房间
                            getMvpPresenter().exitRoom();
                            Intent intent = new Intent(AVRoomActivity.ACTION_EXIT_ROOM);
                            LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(intent);
                            //进入频道
                            new GameHomeModel().enterLinkMacro(info.getChannelId() + "");
                            //RtcEngineManager.get().joinLinkMacro(info.getChannelId() + "", (int) uid);
                        } else if (second == CustomAttachment.CUSTOM_MSG_GAME_LINK_MACRO_AGREE) {
                            GameLinkMacroInvitaion info = ((GameLinkMacroAgreeAttachment) msg.getAttachment()).getDataInfo();
                            isLinkMicroSuding = true;
                            isLinkMicroing = true;
                            linkStartTime = System.currentTimeMillis();
                            linkMicroChannelId = info.getChannelId() + "";
                            long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
                            //记录下对方的账号
                            if (info.getInvitationUid() == uid) {
                                linkMicroAccountId = info.getUid();
                            } else {
                                linkMicroAccountId = info.getInvitationUid();
                            }
                            //如果当前在房间内则退出房间
                            getMvpPresenter().exitRoom();
                            Intent intent = new Intent(AVRoomActivity.ACTION_EXIT_ROOM);
                            LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(intent);
                            //进入频道
                            new GameHomeModel().enterLinkMacro(info.getChannelId() + "");
                            //RtcEngineManager.get().joinLinkMacro(info.getChannelId() + "", (int) uid);
                            //发送连麦成功广播
                            Intent intent1 = new Intent(P2PMessageActivity.ACTION_LINK_MICROING);
                            LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(intent1);
                        } else if (second == CustomAttachment.CUSTOM_MSG_GAME_LINK_MACRO_REFRUSE) {
                            isLinkMicroing = false;
                            //发送连麦结束广播
                            Intent intent = new Intent(P2PMessageActivity.ACTION_LINK_MICRO_FINISH);
                            LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(intent);
                        } else if (second == CustomAttachment.CUSTOM_MSG_GAME_LINK_MACRO_CANCEL) {
                            isLinkMicroing = false;
                            //发送连麦取消广播
                            Intent intent = new Intent(LinkMacroActivity.ACTION_CANCEL_LINK_MICRO);
                            LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(intent);
                        } else if (second == CustomAttachment.CUSTOM_MSG_GAME_LINK_MACRO_FINISH) {
                            isLinkMicroing = false;
                            exitLinkMacro();
                            //DaemonService.stop(MainActivity.this);
                            //发送连麦结束广播
                            Intent intent = new Intent(P2PMessageActivity.ACTION_LINK_MICRO_FINISH);
                            LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(intent);
                        } else if (second == CustomAttachment.CUSTOM_MSG_GAME_LINK_MACRO_TIME_OUT) {
                            isLinkMicroing = false;
                            //发送连麦取消广播
                            Intent intent = new Intent(LinkMacroActivity.ACTION_CANCEL_LINK_MICRO);
                            LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(intent);
                            //发送连麦结束广播
                            Intent intent1 = new Intent(P2PMessageActivity.ACTION_LINK_MICRO_FINISH);
                            LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(intent1);
                        } else if (second == CustomAttachment.CUSTOM_MSG_GAME_LINK_MACRO_BUSY) {
                            if (msg.getDirect() == MsgDirectionEnum.Out || isLinkMicroSuding) {
                                continue;
                            }
                            isLinkMicroing = false;
                            //发送连麦结束广播
                            Intent intent1 = new Intent(P2PMessageActivity.ACTION_LINK_MICRO_FINISH);
                            LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(intent1);
                        }
                    } else if (first == CustomAttachment.CUSTOM_MSG_OFFICIAL_PRESENTATION_GIFT) {
                        //官方后台赠送礼物，刷新自己的礼物列表
                        LogUtils.d(TAG, "onReceivePersonalMessages-官方后台赠送礼物，刷新自己的礼物列表");
                        CoreManager.getCore(IGiftCore.class).requestGiftInfos();
                    } else if (first == CustomAttachment.CUSTOM_MSG_UPDATE_USER_NOBLE_INFO) {
                        LogUtils.d(TAG, "onReceivePersonalMessages-开通|续费贵族，到账更新信息");
                        if (null != msg.getAttachment()) {
                            NobleNotifyAttachment nobleNotifyAttachment = (NobleNotifyAttachment) msg.getAttachment();
                            LogUtils.d(TAG, "onReceivePersonalMessages-开通|续费贵族信息:" + nobleNotifyAttachment);
                        }
                        CoreManager.getCore(IUserCore.class).requestUserInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
                    } else if (first == CustomAttachment.CUSTOM_MSG_UPDATE_MUTE_INFO) {
                        LogUtils.d(TAG, "onReceivePersonalMessages-更新禁言信息");
                        CoreManager.getCore(IUserInfoCore.class).checkBanned(true);
                    } else if (first == CustomAttachment.CUSTOM_MSG_LIKE_FIRST || second == CustomAttachment.CUSTOM_MSG_LIKE_SECOND) {
                        //新点赞
                        try {
                            OfficSecrRedPointCountManager.addRedCountDelNum();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        MsgCenterRedPointStatusManager.getInstance().onRecvNewLikeNotify();
                    } else if (first == CustomAttachment.CUSTOM_MSG_NEW_FANS) {
                        //新粉丝
                        MsgCenterRedPointStatusManager.getInstance().onRecvNewFanNotify();
                    } else if (first == CustomAttachment.CUSTOM_MSG_NEW_FRIEND) {
                        //新好友
                        MsgCenterRedPointStatusManager.getInstance().onRecvNewFriendNotify();
                    } else if (first == CustomAttachment.CUSTOM_MSG_VISITOR_FIRST) {
                        //新访客通知
                        try {
                            VisitorInfo info = ((VisitorAttachment) msg.getAttachment()).getDataInfo();
                            if (fragments != null && fragments.size() >= 5) {
                                ((MeFragment) fragments.get(4)).setVisitorCount(info.getVisitorCount());
                            }
                            OfficSecrRedPointCountManager.addRedCountDelNum();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (first == CustomAttachment.CUSTOM_MSG_FIRST_CALL_UP) {
                        if (second == CustomAttachment.CUSTOM_MSG_SECOND_PRIVATE_CHAT_MSG) {
                            OfficSecrRedPointCountManager.addRedCountDelNum();
                        }
                    }

                    MainRedPointCountManager.getInstance().addU2UMsgCount(first, second, msg.getSessionId());
                    MainRedPointCountManager.getInstance().updatePersonalMessageUnreadCound(mMainTabLayout);
                }
            }
        }
    }

    //连麦占线
    private void linkMicroBusy(long linkUid, int type) {
        new GameHomeModel().linkMacroException(linkUid + "", type, new OkHttpManager.MyCallBack<ServiceResult<List<GameEnitity>>>() {
            @Override
            public void onError(Exception e) {
            }

            @Override
            public void onResponse(ServiceResult<List<GameEnitity>> response) {
            }
        });
    }


    //退出连麦
    public static void exitLinkMacro() {
        linkStartTime = 0;
        isLinkMicroSuding = false;
        isLinkMicroing = false;
        linkMicroChannelId = "";
        //退出连麦
        AgoraEngineManager.get().exitLinkMacro();
    }

    //初始化连麦相关数据
    public void initLinkMicroData() {
        linkStartTime = 0;
        isLinkMicroSuding = false;
        isLinkMicroing = false;
        linkMicroChannelId = "";
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onNeedCompleteInfo() {
        getDialogManager().dismissDialog();
        startActivity(new Intent(this, SupplyInfo1Activity.class));
        //进入注册后的完善个人资料页面
        StatisticManager.get().onEvent(this,
                StatisticModel.EVENT_ID_ENTER_ADD_USERINFO,
                StatisticModel.getInstance().getUMAnalyCommonMap(this));
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdateFail(String msg) {
        onLogout();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo userInfo) {
        LogUtils.d(TAG, "onCurrentUserInfoUpdate");
        CoreManager.getCore(IAuthCore.class).setThirdUserInfo(null);
        MainRedPointCountManager.getInstance().updatePersonalMessageUnreadCound(mMainTabLayout);

        if (null != userInfo) {
            CrashReport.putUserData(this, "erbanNo", userInfo.getErbanNo() + "");
        }
        CoreManager.getCore(ISecurityCore.class).securityCheck(this);
        parseJPushInfo();
    }

    @CoreEvent(coreClientClass = IRedPacketCoreClient.class)
    public void onReceiveNewPacket(final RedPacketInfoV2 redPacketInfo) {
        LogUtils.d(TAG, "onReceiveNewPacket-redPacketInfo:" + redPacketInfo);
        CommonPref.instance(this).putBoolean("redPacketPoint", true);
        if (redPacketInfo.isNeedAlert()) {
            RedPacketDialog.start(this, redPacketInfo);
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoCompleteFaith(String msg) {
        getDialogManager().dismissDialog();
        toast("用户信息加载失败，请检查网络后重新登录");
        CoreManager.getCore(IAuthCore.class).logout();
    }

    /**
     * 更新房间信息及麦序状态
     * TODO 参考音缘，将这一部分的逻辑，单独移动到独立的view中处理
     *
     * @param queueInfo
     * @param imRoomQueueInfo
     */
    private void updateRoomState(RoomQueueInfo queueInfo, IMRoomQueueInfo imRoomQueueInfo) {
        RoomInfo roomInfo = RoomServiceScheduler.getCurrentRoomInfo();
        if (roomInfo != null) {
            String roomId = roomInfo.getRoomId() + "";
            //房间最小化的时候重连判断是否在麦上，如果被踢会自动移除,
            // 由于目前单人音频房只有房主才可上麦，因此这里不做单人音频房的麦序状态判断
            boolean onMicBefore = queueInfo != null && queueInfo.mChatRoomMember != null
                    && queueInfo.mRoomMicInfo != null;
            if (onMicBefore && getMvpPresenter() != null && !IMNetEaseManager.get().inAvRoom
                    && roomInfo.getType() == RoomInfo.ROOMTYPE_HOME_PARTY) {
                Disposable disposable = getMvpPresenter().checkMicType(roomId, queueInfo, roomInfo.getType());
                if (null != mCompositeDisposable && null != disposable) {
                    mCompositeDisposable.add(disposable);
                }
            }
            if (roomInfo.getType() == RoomInfo.ROOMTYPE_HOME_PARTY) {
                //以下为房间标题、个人头像及动画加载逻辑，为共同部分
                String title = roomInfo.getTitle();
                if (!TextUtils.isEmpty(title) && title.length() > 7) {
                    title = getResources().getString(R.string.nick_length_max_six, title.substring(0, 7));
                }
                tvName.setText(title);
                UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(roomInfo.getUid());
                if (userInfo != null) {
//                    avatarLayout.clearAnimation();
                    avatarLayout.setVisibility(View.VISIBLE);
//                    Animation operatingAnim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.rotate_anim);
//                    LinearInterpolator lin = new LinearInterpolator();
//                    operatingAnim.setInterpolator(lin);
//                    avatarImage.startAnimation(operatingAnim);
                    tvId.setText("ID:".concat(String.valueOf(userInfo.getErbanNo())));
                    ImageLoadUtils.loadAvatar(MainActivity.this, userInfo.getAvatar(), avatarImage);
                } else {
                    NimUserInfo nimUserInfo = NimUserInfoCache.getInstance().getUserInfo(roomInfo.getUid() + "");
                    if (nimUserInfo == null) {
                        NimUserInfoCache.getInstance().getUserInfoFromRemote(roomInfo.getUid() + "", new RequestCallbackWrapper<NimUserInfo>() {
                            @Override
                            public void onResult(int i, NimUserInfo nimUserInfo, Throwable throwable) {
                                if (nimUserInfo != null) {
//                                    avatarLayout.clearAnimation();
                                    avatarLayout.setVisibility(View.VISIBLE);
//                                    Animation operatingAnim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.rotate_anim);
//                                    LinearInterpolator lin = new LinearInterpolator();
//                                    operatingAnim.setInterpolator(lin);
//                                    avatarImage.startAnimation(operatingAnim);
                                    ImageLoadUtils.loadAvatar(MainActivity.this, nimUserInfo.getAvatar(), avatarImage);
                                }
                            }
                        });
                    } else {
//                        avatarLayout.clearAnimation();
                        avatarLayout.setVisibility(View.VISIBLE);
//                        Animation operatingAnim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.rotate_anim);
//                        LinearInterpolator lin = new LinearInterpolator();
//                        operatingAnim.setInterpolator(lin);
//                        avatarImage.startAnimation(operatingAnim);
                        ImageLoadUtils.loadAvatar(MainActivity.this, nimUserInfo.getAvatar(), avatarImage);
                    }
                }
            }else {
                //以下为房间标题、个人头像及动画加载逻辑，为共同部分
                String title = roomInfo.getTitle();
                if (!TextUtils.isEmpty(title) && title.length() > 7) {
                    title = getResources().getString(R.string.nick_length_max_six, title.substring(0, 7));
                }
                tvName.setText(title);
                if (RoomDataManager.get().mOwnerRoomMember != null){
//                    avatarLayout.clearAnimation();
                    avatarLayout.setVisibility(View.VISIBLE);
//                    Animation operatingAnim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.rotate_anim);
//                    LinearInterpolator lin = new LinearInterpolator();
//                    operatingAnim.setInterpolator(lin);
//                    avatarImage.startAnimation(operatingAnim);
                    tvId.setText("ID:".concat(String.valueOf(RoomDataManager.get().mOwnerRoomMember.getErbanNo())));
                    ImageLoadUtils.loadAvatar(MainActivity.this, RoomDataManager.get().mOwnerRoomMember.getAvatar(), avatarImage);
                }else {
                    UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(roomInfo.getUid());
                    if (userInfo != null) {
//                        avatarLayout.clearAnimation();
                        avatarLayout.setVisibility(View.VISIBLE);
//                        Animation operatingAnim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.rotate_anim);
//                        LinearInterpolator lin = new LinearInterpolator();
//                        operatingAnim.setInterpolator(lin);
//                        avatarImage.startAnimation(operatingAnim);
                        tvId.setText("ID:".concat(String.valueOf(userInfo.getErbanNo())));
                        ImageLoadUtils.loadAvatar(MainActivity.this, userInfo.getAvatar(), avatarImage);
                    } else {
                        NimUserInfo nimUserInfo = NimUserInfoCache.getInstance().getUserInfo(roomInfo.getUid() + "");
                        if (nimUserInfo == null) {
                            NimUserInfoCache.getInstance().getUserInfoFromRemote(roomInfo.getUid() + "", new RequestCallbackWrapper<NimUserInfo>() {
                                @Override
                                public void onResult(int i, NimUserInfo nimUserInfo, Throwable throwable) {
                                    if (nimUserInfo != null) {
//                                        avatarLayout.clearAnimation();
                                        avatarLayout.setVisibility(View.VISIBLE);
//                                        Animation operatingAnim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.rotate_anim);
//                                        LinearInterpolator lin = new LinearInterpolator();
//                                        operatingAnim.setInterpolator(lin);
//                                        avatarImage.startAnimation(operatingAnim);
                                        ImageLoadUtils.loadAvatar(MainActivity.this, nimUserInfo.getAvatar(), avatarImage);
                                    }
                                }
                            });
                        } else {
//                            avatarLayout.clearAnimation();
                            avatarLayout.setVisibility(View.VISIBLE);
//                            Animation operatingAnim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.rotate_anim);
//                            LinearInterpolator lin = new LinearInterpolator();
//                            operatingAnim.setInterpolator(lin);
//                            avatarImage.startAnimation(operatingAnim);
                            ImageLoadUtils.loadAvatar(MainActivity.this, nimUserInfo.getAvatar(), avatarImage);
                        }
                    }
                }
            }
        }
    }

    public void onEnter(RoomInfo roomInfo) {
        LogUtils.d(TAG, "onEnter-roomInfo:" + roomInfo);
        LogUtils.d(TAG, "onEnter-cRoomInfo1:" + AvRoomDataManager.get().mCurrentRoomInfo);
        LogUtils.d(TAG, "onEnter-cRoomInfo2:" + RoomDataManager.get().getCurrentRoomInfo());
        updateRoomState(null, null);
        if (roomInfo != null && !TextUtils.isEmpty(roomInfo.getTitle())) {
            BuglyLog.d(TAG, "onEnter->DaemonService.start");
            DaemonService.start(this, roomInfo.getTitle(), "点击返回房间", "正在房间内");
        }
    }

    @CoreEvent(coreClientClass = IShareFansCoreClient.class)
    public void onShareFansJoin(long roomId, long uid, int roomType) {
        LogUtils.d(TAG, "onShareFansJoin-uid:" + uid + " roomType:" + roomType + " roomId:" + roomId);
        RoomServiceScheduler.getInstance().enterOtherTypeRoomFromService(this, uid, roomType);
        if (0 == roomId) {
            return;
        }
        new IMRoomModel().inRoomFromConvene(roomId, new HttpRequestCallBack<String>() {
            @Override
            public void onSuccess(String message, String response) {
                LogUtils.d(TAG, "onShareFansJoin-->inRoomFromConvene-->onSuccess message:" + message);
            }

            @Override
            public void onFailure(int code, String msg) {
                LogUtils.d(TAG, "onShareFansJoin-->inRoomFromConvene-->onFailure msg:" + msg + " code:" + code);
            }
        });
    }

    @CoreEvent(coreClientClass = ILinkedCoreClient.class)
    public void onLinkedInfoUpdate(LinkedInfo linkedInfo) {
        if (!StringUtil.isEmpty(linkedInfo.getRoomUid())) {
            RoomServiceScheduler.getInstance().enterRoom(this,
                    JavaUtil.str2long(linkedInfo.getRoomUid()), Integer.valueOf(linkedInfo.getRoomType()));
        }
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onTabClick(int position) {
        BuglyLog.d(TAG, "onTabClick-position:" + position);
        mCurrentMainPosition = position;
        if (0 == position) {
            //游戏大厅-首页入口点击
            StatisticManager.get().onEvent(this,
                    StatisticModel.EVENT_ID_MAIN_TAB_CHANCE_GAME_HOME,
                    StatisticModel.getInstance().getUMAnalyCommonMap(this));
        }
        if (vpFragment != null) {
            vpFragment.setCurrentItem(position, false);
        }
        if (3 == position) {
            final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
            if (0L == uid) {
                return;
            }
            if (null != NimUIKit.onServerEmojiOperaListener) {
                NimUIKit.onServerEmojiOperaListener.onNeedInitEmojiFromServer(uid);
            }
        }
    }

    @Override
    /**
     * 仅做关闭右下角面板和关闭房间服务 两块逻辑
     */
    public void onRoomExited() {
        BuglyLog.d(TAG, "onRoomExited->DaemonService.stop");
        closeOpenRoomAnimation();
        DaemonService.stop(MainActivity.this);
    }

    @Override
    public void onGetRecommAd(String data) {
        adData = data;
        if (CoreManager.getCore(IAuthCore.class).isLogin()) {
            RecommAdDialog.start(this, data);
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoComplete(UserInfo userInfo) {
        if (TextUtils.isEmpty(adData)) {
            getMvpPresenter().getRecommAdInfo();
            return;
        }
        if (hander != null) {
            hander.postDelayed(new Runnable() {
                @Override
                public void run() {
                    RecommAdDialog.start(MainActivity.this, adData);
                }
            }, 400);
        } else {
            RecommAdDialog.start(this, adData);
        }
    }

    @Override
    public void onInitSuccess(InitInfo data) {

    }

    /**
     * 创建homepart房间失败
     *
     * @param message 提示信息
     */
    @Override
    public void notifyHomePartRoomCreateFailed(String message) {
        LogUtils.d(TAG, "notifyHomePartRoomCreateFailed-message:" + message);
        getDialogManager().dismissDialog();
        if (!TextUtils.isEmpty(message)) {
            toast(message);
        }
    }

    /**
     * 进入房间需要实名认证
     */
    @Override
    public void notifyHomePartRoomNeedRealNameAuth(long queryUid, int roomType, RoomInfo roomInfo) {
        LogUtils.d(TAG, "notifyHomePartRoomNeedRealNameAuth-queryUid:" + queryUid + " roomType:" + roomType);
        getDialogManager().dismissDialog();
        getDialogManager().showOkCancelDialog(getResources().getString(R.string.real_name_auth_tips,
                getResources().getString(R.string.real_name_auth_tips_create_room)),
                getResources().getString(R.string.real_name_auth_accept),
                getResources().getString(R.string.cancel), new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {
                        getMvpPresenter().checkRoomUsable(queryUid, roomInfo);
                    }

                    @Override
                    public void onOk() {
                        CommonWebViewActivity.start(MainActivity.this, UriProvider.getRealNameAuthUrl());
                    }
                });
    }

    @Override
    public void enterHomePartRoom(long uid) {
        LogUtils.d(TAG, "enterHomePartRoom-uid:" + uid);
        getDialogManager().dismissDialog();
        RoomServiceScheduler.getInstance().enterRoom(MainActivity.this, uid, RoomInfo.ROOMTYPE_HOME_PARTY);
    }

    /**
     * 弹萌新大礼包界面，走领取萌新大礼包流程
     *
     * @param getDay
     * @param micRoomUserList
     * @param newsBigGiftMap
     */
    @Override
    public void showNewPkgDialog(int getDay, List<RecommMicUserInfo> micRoomUserList,
                                 Map<Integer, List<SignRecvGiftPkgInfo>> newsBigGiftMap) {
        SignAwardTipsDialog.start(this, getDay, micRoomUserList, newsBigGiftMap);
        overridePendingTransition(android.R.anim.fade_in, R.anim.fast_fade_out);
    }

    /**
     * 弹连续签到奖励弹框，走连续签到奖励流程
     *
     * @param signDay
     * @param signInAwardInfos
     */
    @Override
    public void showSignRecvDialog(int signDay, List<SignInAwardInfo> signInAwardInfos) {
        SignAwardDaysDialog.start(this, signDay, signInAwardInfos);
        overridePendingTransition(android.R.anim.fade_in, R.anim.fast_fade_out);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //JPush中调用HMS SDK解决错误的接口传入的requestCode为10001,开发者调用是请注意不要同样使用10001
        if (requestCode == JPluginPlatformInterface.JPLUGIN_REQUEST_CODE) {
            pHuaweiPushInterface.onActivityResult(this, requestCode, resultCode, data);
        }
    }

    //进入游戏
    private void startGame(Context context, MiniGameInvitedAcceptInfo invitedInfo) {
        if (invitedInfo.getAcceptInvt() == 1/* && msg.getDirect() == MsgDirectionEnum.In*/) {
            if (CommonWebViewActivity.isGameRunning) {
                LogUtils.e(TAG, "game is running");
                return;
            }
            long roomId = invitedInfo.getRoomid();
            String gameUrl = invitedInfo.getGameUrl();
            if (TextUtils.isEmpty(gameUrl)) {
                LogUtils.e(TAG, "gameUrl is null");
                return;
            }
            if (!MsgViewHolderMiniGameInvited.isExistInvit(roomId)) {
                LogUtils.e(TAG, "game invitation is null");
                return;
            }
            //进入游戏
            CommonWebViewActivity.isGameRunning = true;
            CommonWebViewActivity.start(context, gameUrl + "?room_id=" + roomId + "#/loading", WebViewStyle.NO_TITLE);
        }
    }

    //============================实名认证===============================
    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onNeedRealNameAuthChecked(Context context, String msg) {
        DialogManager dialogManager = new DialogManager(context);
        dialogManager.setCanceledOnClickOutside(false);
        dialogManager.showOkCancelDialog(msg,
                getResources().getString(R.string.real_name_auth_accept),
                getResources().getString(R.string.cancel),
                new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onOk() {
                        CommonWebViewActivity.start(MainActivity.this, UriProvider.getRealNameAuthUrl());
                    }
                });
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onNeedBindPhoneChecked(Context context, String msg) {
        DialogManager dialogManager = new DialogManager(context);
        dialogManager.setCanceledOnClickOutside(false);
        dialogManager.showOkCancelDialog(msg,
                getResources().getString(R.string.phone_bind_sure),
                getResources().getString(R.string.cancel),
                new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onOk() {
                        Intent intent = new Intent(MainActivity.this, BinderPhoneActivity.class);
                        intent.putExtra(BinderPhoneActivity.hasBand, 0);
                        startActivity(intent);
                    }
                });
    }

    //打开发现页
    public void openFindPage() {
        try {
            vpFragment.setCurrentItem(2, false);
            if (fragments != null && fragments.size() >= 3) {
                ((FindHomeFragment) fragments.get(2)).selectPage(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //---------------------------------用户在线状态同步---------------------------------
    //解决handler leak泄漏警告规范代码示例，参考https://blog.csdn.net/peceoqicka/article/details/42556365
    private Handler hander = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            getMvpPresenter().syncOnLineStatus();
            hander.sendEmptyMessageDelayed(0, 30000L);
            return true;
        }
    });

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onCreateRoomClicked(long uid) {

        //区分当前所在的房间
        RoomInfo roomInfo = RoomServiceScheduler.getCurrentRoomInfo();
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (null == userInfo) {
            return;
        }
        LogUtils.d(TAG, "onCreateRoomClicked roomInfo is null :" + (null == roomInfo));
        if (null != roomInfo && userInfo.getUid() == roomInfo.getUid()) {
            //仅自己的房间最小化后再次点击[我的]-[房间]，才会直接进房
            RoomServiceScheduler.getInstance().enterRoom(MainActivity.this, roomInfo.getUid(), roomInfo.getType());
            return;
        }


        if (!ListUtils.isListEmpty(userInfo.getPowerRoom())) {
            if (userInfo.getPowerRoom().size() == 1){
                CreateRoomBean createRoomBean = userInfo.getPowerRoom().get(0);
                if (createRoomBean == null) {
                    return;
                }
                if (!userInfo.isRealNameAudit()) {
                    getDialogManager().showOkCancelDialog(getResources().getString(R.string.real_name_auth_tips,getResources().getString(R.string.real_name_auth_tips_create_room)),
                            getResources().getString(R.string.real_name_auth_accept), getResources().getString(R.string.cancel), new DialogManager.OkCancelDialogListener() {
                                @Override
                                public void onCancel() {
                                    if (createRoomBean.getType() == RoomInfo.ROOMTYPE_HOME_PARTY){
                                        getDialogManager().showProgressDialog(MainActivity.this,getResources().getString(R.string.network_loading));
                                        getMvpPresenter().createHomePartRoom(uid, RoomInfo.ROOMTYPE_HOME_PARTY);
                                    }else {
                                        RoomServiceScheduler.getInstance().enterOtherTypeRoomFromService(MainActivity.this, uid, createRoomBean.getType());
                                    }
                                }

                                @Override
                                public void onOk() {
                                    CommonWebViewActivity.start(MainActivity.this, UriProvider.getRealNameAuthUrl());
                                }
                            });
                } else {
                    RoomServiceScheduler.getInstance().enterOtherTypeRoomFromService(MainActivity.this,uid,createRoomBean.getType());
                }
            }else {
                CreateRoomEnterDialog createRoomEnterDialog = new CreateRoomEnterDialog(this,uid);
                createRoomEnterDialog.setOnCreateRoomClickListener(new CreateRoomEnterDialog.OnCreateRoomClickListener() {
                    @Override
                    public void onClickCallback(int type, long uid) {
                        if (type == RoomInfo.ROOMTYPE_HOME_PARTY){
                            getDialogManager().showProgressDialog(MainActivity.this,getResources().getString(R.string.network_loading));
                            getMvpPresenter().createHomePartRoom(uid, RoomInfo.ROOMTYPE_HOME_PARTY);
                        }else {
                            RoomServiceScheduler.getInstance().enterOtherTypeRoomFromService(MainActivity.this, uid, type);
                        }
                    }

                    @Override
                    public void onRealNameAudit(int type, long uid) {
                        getDialogManager().showOkCancelDialog(getResources().getString(R.string.real_name_auth_tips,getResources().getString(R.string.real_name_auth_tips_create_room)),
                                getResources().getString(R.string.real_name_auth_accept), getResources().getString(R.string.cancel), new DialogManager.OkCancelDialogListener() {
                                    @Override
                                    public void onCancel() {
                                        if (type == RoomInfo.ROOMTYPE_HOME_PARTY){
                                            getDialogManager().showProgressDialog(MainActivity.this,getResources().getString(R.string.network_loading));
                                            getMvpPresenter().createHomePartRoom(uid, RoomInfo.ROOMTYPE_HOME_PARTY);
                                        }else {
                                            RoomServiceScheduler.getInstance().enterOtherTypeRoomFromService(MainActivity.this, uid, type);
                                        }
                                    }

                                    @Override
                                    public void onOk() {
                                        CommonWebViewActivity.start(MainActivity.this, UriProvider.getRealNameAuthUrl());
                                    }
                                });
                    }
                });
                createRoomEnterDialog.show();
            }
        }
    }

    private PhoneStateListener mPhoneStateListener;

    /**
     * 电话的监听，处理即构来电的时候提示推流和音乐播放
     */
    private void registerPhoneStateListener() {
        mPhoneStateListener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                super.onCallStateChanged(state, incomingNumber);
                switch (state) {
                    case TelephonyManager.CALL_STATE_IDLE:

                        break;
                    case TelephonyManager.CALL_STATE_RINGING:
                        stopZegoPlay();
                        break;
                    case TelephonyManager.CALL_STATE_OFFHOOK:
                        stopZegoPlay();
                        break;
                }
            }
        };
        TelephonyManager tm = (TelephonyManager) getSystemService(Service.TELEPHONY_SERVICE);
        tm.listen(mPhoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
    }

    private void stopZegoPlay() {
        if (BaseRoomServiceScheduler.getCurrentRoomInfo() != null && RtcEngineManager.get().getAudioOrganization() == RtcEngineManager.ZEGO) {
            IPlayerCore playerCore = CoreManager.getCore(IPlayerCore.class);
            if (playerCore != null) {
                playerCore.pause();
            }
            RtcEngineManager.get().setMute(true);
        }
    }
}
