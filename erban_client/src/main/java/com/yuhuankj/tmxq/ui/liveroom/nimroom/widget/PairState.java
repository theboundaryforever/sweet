package com.yuhuankj.tmxq.ui.liveroom.nimroom.widget;


import android.content.Context;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.utils.TimeUtils;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.R;

public class PairState extends LinearLayout {

    private View inflate;
    private TextView centerContent;
    private TextView pairStateAction;

    private final String pairStartContent = "心动选择中 ";
    private final String showPair = "公布结果";
    private CountDownTimer countDownTimer;
    private LinearLayout bg;
    private CountDownTimer startCountDownTimer;

    public PairState(Context context) {
        this(context, null);
    }

    public PairState(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inflate = View.inflate(context, R.layout.view_pair_state, this);
        bg = inflate.findViewById(R.id.ll_pair_state_bg);
        centerContent = inflate.findViewById(R.id.tv_pair_state_center_content);
        pairStateAction = inflate.findViewById(R.id.tv_pair_state_action);

        pairStateAction.setOnClickListener(v -> {
            if (iPairStateAction != null)
                iPairStateAction.onPairAction(isPairStart);
        });

    }

    /**
     * 当用户的身份改变时，房间信息改变时要通知其替换
     */
    public void initState() {
        RoomInfo mCurrentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (mCurrentRoomInfo != null) {
            setVisibility(mCurrentRoomInfo.tagId == 6 ? VISIBLE : INVISIBLE);
        }

        initPermission();
    }

    public void initPermission() {
        boolean b = AvRoomDataManager.get().isRoomAdmin() || AvRoomDataManager.get().isRoomOwner();
        bg.setBackgroundResource(b ? R.drawable.pair_state_bg_1 : R.drawable.pair_state_bg_2);

        pairStateAction.setVisibility(b ? VISIBLE : GONE);
    }

    public void startPair(long countDownTime) {
        isPairStart = true;

        if (startCountDownTimer != null) {
            pairStateAction.setEnabled(true);
            startCountDownTimer.cancel();
        }


        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
        countDownTimer = new CountDownTimer(countDownTime, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                if (!isPairStart)
                    return;
                String progresstime = TimeUtils.getProgresstime(millisUntilFinished);
                centerContent.setText(pairStartContent + progresstime);
            }

            @Override
            public void onFinish() {
                if (!isPairStart)
                    return;
                centerContent.setText(showPair);

            }
        };
        countDownTimer.start();

        pairStateAction.setText("提前结束");
    }


    private boolean isPairStart = false;

    public void stopPair(boolean needCountDown) {

        isPairStart = false;
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }

        if (null != centerContent) {
            centerContent.setText(needCountDown ? "公布结果" : "未开始");
        }

        if (needCountDown) {
            if (startCountDownTimer != null) {
                startCountDownTimer.cancel();
                startCountDownTimer = null;
            }
            pairStateAction.setEnabled(false);
            startCountDownTimer = new CountDownTimer(30 * 1000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    if (isPairStart) {
                        return;
                    }
                    String progresstime = TimeUtils.getProgresstime(millisUntilFinished);
                    pairStateAction.setText(progresstime + "");
                }

                @Override
                public void onFinish() {
                    if (isPairStart) {
                        return;
                    }
                    pairStateAction.setEnabled(true);
                    centerContent.setText("未开始");
                    pairStateAction.setText("开始相亲");

                }
            };
            startCountDownTimer.start();
        } else {
            pairStateAction.setText("开始相亲");
        }


    }

    IPairStateAction iPairStateAction;

    public void setiPairStateAction(IPairStateAction iPairStateAction) {
        this.iPairStateAction = iPairStateAction;
    }

    public interface IPairStateAction {
        void onPairAction(boolean isPairStart);
    }


}
