package com.yuhuankj.tmxq.ui.find;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.netease.nim.uikit.common.util.sys.NetworkUtil;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.ButtonUtils;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.JavaUtil;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.erban.libcommon.utils.config.SpEvent;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.utils.pref.ObjectPref;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.find.DiscoverIconListBean;
import com.tongdaxing.xchat_core.find.FindData;
import com.tongdaxing.xchat_core.find.FindInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.base.fragment.BaseLazyFragment;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.databinding.FragmentFindBinding;
import com.yuhuankj.tmxq.ui.find.mengxin.SproutNewActivity;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;
import com.yuhuankj.tmxq.ui.webview.VoiceAuthCardWebViewActivity;
import com.yuhuankj.tmxq.ui.widget.DividerItemDecoration;

import java.util.List;
import java.util.Map;

/**
 * Created by huangmeng1 on 2018/1/8.
 */

public class FindFragment extends BaseLazyFragment {
    public static final String TAG = "FindFragment";
    private final String SP_CACHE_FIND = "FIND_CACHE_DATA";
    private FragmentFindBinding findBinding;
    private FindBottomAdapter adapter;
    private FindTopAdapter findTopAdapter;
    private FindData lastFindData = null;

    private View headView;
    private ImageView iv_right1;
    private ImageView iv_right2;
    private ImageView iv_left1;
    private ImageView iv_leftBg;
    private Runnable refreshTopRViewDataRunnable = null;
    private Runnable refreshBottomRViewDataRunnable = null;

    private SwipeRefreshLayout swipeRefreshLayout;
    private SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            if (NetworkUtil.isNetAvailable(mContext)) {
                onLazyLoadData();
            } else {
                swipeRefreshLayout.setRefreshing(false);
            }
        }
    };

    @Override
    public void onFindViews() {
        swipeRefreshLayout=mView.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
        findBinding = DataBindingUtil.bind(mView);
        //dataBind就个坑爹玩意
        LinearLayoutManager linearLayoutManager = new RecyclerViewNoBugLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        findBinding.rvTopItem.setLayoutManager(linearLayoutManager);
        findBinding.rvTopItem.addItemDecoration(new DividerItemDecoration(linearLayoutManager.getOrientation(), Color.WHITE, mContext, 20));
        findBinding.rvTopItem.setNestedScrollingEnabled(false);
        findTopAdapter = new FindTopAdapter();
        findTopAdapter.iItemAction = new FindTopAdapter.IItemAction() {
            @Override
            public void onItemClick(DiscoverIconListBean bean) {
                autoJump(bean.getActivity(), bean.getUrl(), bean.getTitle(), Json.parse(bean.getParams()));
                LogUtils.d(TAG, "onItemClick-->top bean:" + bean);
                if (!TextUtils.isEmpty(bean.getActivity()) && bean.getActivity().equals("avroom.activity.PublicChatRoomActivity")) {
                    StatisticManager.get().onEvent(mContext,
                            StatisticModel.EVENT_ID_FIND_BROADCAST_MAKE_FRIEND,
                            StatisticModel.getInstance().getUMAnalyCommonMap(mContext));
                }
            }
        };
        headView = LayoutInflater.from(getActivity()).inflate(R.layout.list_item_find_top_head, null);
        findTopAdapter.addHeaderView(headView);
        iv_left1 = headView.findViewById(R.id.iv_left1);
        iv_leftBg = headView.findViewById(R.id.iv_leftBg);
        iv_left1.setOnClickListener(this);
        iv_leftBg.setOnClickListener(this);
        iv_right2 = headView.findViewById(R.id.iv_right2);
        iv_right2.setOnClickListener(this);
        iv_right1 = headView.findViewById(R.id.iv_right1);
        iv_right1.setOnClickListener(this);
        findBinding.rvTopItem.setAdapter(findTopAdapter);
        linearLayoutManager = new RecyclerViewNoBugLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        findBinding.recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new FindBottomAdapter(getActivity(), 790, 152);
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (ButtonUtils.isFastDoubleClick(view.getId())) {
                    return;
                }
                FindInfo findInfo = FindFragment.this.adapter.getData().get(position);
                LogUtils.d(TAG, "onItemClick-->bottom findInfo:" + findInfo);
                if (findInfo.getSkipType() == 3) {
                    if (findInfo.getSkipUri().endsWith(UriProvider.getRecordAuthCardVoiceBannerUrl())) {
                        //发现-banner-声鉴卡
                        Map<String, String> maps = StatisticModel.getInstance().getUMAnalyCommonMap(getActivity());
                        StatisticManager.get().onEvent(getActivity(), StatisticModel.EVENT_ID_FIND_BANNER_VOICEAUTHCARD, maps);

                        VoiceAuthCardWebViewActivity.start(getActivity(), false);
                    } else {
                        CommonWebViewActivity.start(getActivity(), findInfo.getSkipUri());
                    }

                } else if (findInfo.getSkipType() == 2) {
                    RoomServiceScheduler.getInstance().enterRoom(getActivity(),
                            JavaUtil.str2long(findInfo.getSkipUri()),
                            findInfo.getRoomType());
                }
            }
        });
        findBinding.recyclerView.addItemDecoration(new DividerItemDecoration(linearLayoutManager.getOrientation(), Color.WHITE, mContext, 20));
        findBinding.recyclerView.setAdapter(adapter);
    }

    @Override
    public void onSetListener() {
        findBinding.setClick(this);
    }

    @Override
    public void initiate() {
        LogUtils.d(TAG, "initiate");

    }

    private void refreshDataOnView(FindData data) {
        hideStatus();

        List<DiscoverIconListBean> beans = data.discoverIconList;
        if (null != beans && beans.size() >= 3) {
            DiscoverIconListBean left1Bean = beans.remove(0);
            iv_leftBg.setTag(left1Bean);
            iv_left1.setTag(left1Bean);
            GlideApp.with(mContext).load(left1Bean.getContentUrl()).dontAnimate()
//                    .placeholder(R.drawable.bg_default_cover_round_placehold_5)
                    .error(R.drawable.bg_default_cover_round_placehold_5)
                    .into(iv_left1);
            GlideApp.with(mContext).load(left1Bean.getBackgroundUrl()).dontAnimate()
//                    .placeholder(R.drawable.bg_default_cover_round_placehold_5)
                    .error(R.drawable.bg_default_cover_round_placehold_8)
                    .transforms(new CenterCrop(),
                            new RoundedCorners(DisplayUtility.dp2px(mContext,8)))
                    .into(iv_leftBg);

            left1Bean = beans.remove(0);
            iv_right1.setTag(left1Bean);
            GlideApp.with(mContext).load(left1Bean.getPic()).dontAnimate()
                    .error(R.drawable.bg_default_cover_round_placehold_5)
                    .into(iv_right1);

            left1Bean = beans.remove(0);
            iv_right2.setTag(left1Bean);
            GlideApp.with(mContext).load(left1Bean.getPic()).dontAnimate()
//                    .placeholder(R.drawable.bg_default_cover_round_placehold_5)
                    .error(R.drawable.bg_default_cover_round_placehold_5)
                    .into(iv_right2);
        }
        if (null == refreshTopRViewDataRunnable) {
            refreshTopRViewDataRunnable = new Runnable() {
                @Override
                public void run() {
                    findTopAdapter.setNewData(beans);
                }
            };
        }
        findBinding.rvTopItem.post(refreshTopRViewDataRunnable);
        if (null == refreshBottomRViewDataRunnable) {
            refreshBottomRViewDataRunnable = new Runnable() {
                @Override
                public void run() {
                    adapter.setNewData(data.advertiseList);
                }
            };
        }
        findBinding.recyclerView.post(refreshBottomRViewDataRunnable);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (null != refreshTopRViewDataRunnable) {
            findBinding.rvTopItem.removeCallbacks(refreshTopRViewDataRunnable);
        }
        if (null != refreshBottomRViewDataRunnable) {
            findBinding.recyclerView.removeCallbacks(refreshBottomRViewDataRunnable);
        }
    }

    @Override
    public void onReloadData() {
        super.onReloadData();
        initiate();
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_find;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_right2:
            case R.id.iv_left1:
            case R.id.iv_right1:
            case R.id.iv_leftBg:
                DiscoverIconListBean bean = (DiscoverIconListBean) view.getTag();
                if (null != bean) {
                    if (!TextUtils.isEmpty(bean.getActivity()) && bean.getActivity().equals("avroom.activity.PublicChatRoomActivity")) {
                        StatisticManager.get().onEvent(mContext,
                                StatisticModel.EVENT_ID_FIND_BROADCAST_MAKE_FRIEND,
                                StatisticModel.getInstance().getUMAnalyCommonMap(mContext));
                    }
                    if (!TextUtils.isEmpty(bean.getActivity()) && bean.getActivity().equals("ui.sprout.SproutNewActivity")) {
                        String content = (String) SpUtils.get(getActivity(), SpEvent.sprout_permission_sure, "");
                        if (!TextUtils.isEmpty(content) && content.equals("sprout_permission_sure")) {
                            SproutNewActivity.start(mContext);
                            //发现-进入萌新广场
                            StatisticManager.get().onEvent(getActivity(),
                                    StatisticModel.EVENT_ID_ENTER_SPROUT,
                                    StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
                        } else {
                            getDialogManager().showOkCancelWithTitleDialog(getResources().getString(R.string.sprout_permission_tips_title),
                                    getResources().getString(R.string.sprout_permission_tips),
                                    getResources().getString(R.string.sure),
                                    getResources().getString(R.string.cancel),
                                    new DialogManager.OkCancelDialogListener() {
                                        @Override
                                        public void onCancel() {
                                            SpUtils.remove(getActivity(), SpEvent.sprout_permission_sure);
                                        }

                                        @Override
                                        public void onOk() {
                                            SpUtils.put(getActivity(), SpEvent.sprout_permission_sure, "sprout_permission_sure");
                                            SproutNewActivity.start(mContext);
                                            //发现-进入萌新广场
                                            StatisticManager.get().onEvent(getActivity(),
                                                    StatisticModel.EVENT_ID_ENTER_SPROUT,
                                                    StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
                                        }
                                    });
                        }
                    } else {
                        autoJump(bean.getActivity(), bean.getUrl(), bean.getTitle(), Json.parse(bean.getParams()));
                    }

                    LogUtils.d(TAG, "onItemClick-activity:" + bean.getActivity() + " url:" + bean.getUrl());
                }
                break;
            default:
                super.onClick(view);
                break;
        }
    }

    @Override
    protected void onLazyLoadData() {
        //0.先加载本地缓存的数据
        if (null != getActivity()) {
            lastFindData = (FindData) ObjectPref.instance(getActivity().getApplicationContext()).readObject(SP_CACHE_FIND);
            if (null != lastFindData) {
                refreshDataOnView(lastFindData);
                adapter.setNeedDefaultPlaceHolderImg(null == lastFindData.advertiseList || lastFindData.advertiseList.size() == 0);
            }
        }

        //1.再去请求网络刷新数据
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        OkHttpManager.getInstance().getRequest(UriProvider.getFindInfo(), params, new OkHttpManager.MyCallBack<ServiceResult<FindData>>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (null == lastFindData) {
                    showNetworkErr();
                }
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onResponse(ServiceResult<FindData> response) {
                swipeRefreshLayout.setRefreshing(false);
                if (response.getCode() != 200 && null == lastFindData) {
                    showNetworkErr();
                    return;
                }
                FindData data = response.getData();
                if (data == null) {
                    return;
                }
                ObjectPref.instance(getActivity().getApplicationContext()).saveObject(SP_CACHE_FIND, data);
                refreshDataOnView(data);
            }
        });
    }
}
