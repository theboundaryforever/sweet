package com.yuhuankj.tmxq.ui.liveroom.imroom.room.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.fragment.IMRoomOnlineMemberFragment;

/**
 * Created by MadisonRong on 13/01/2018.
 */

public class IMRoomMemberListDialog extends AppCompatDialogFragment implements View.OnClickListener {

    public static final String TYPE_ONLINE_USER = "ONLINE_USER";
    public static final String KEY_TITLE = "KEY_TITLE";
    public static final String KEY_TYPE = "KEY_TYPE";
    public static final String KEY_ISMIC = "isMic";
    public static final String KEY_IS_ADMIN = "isAdmin";
    public static final String KEY_IS_INVITE_UP_TO_AUDIO_CONN_MIC = "isInviteUpToAudioConnMic";

    private String title;
    private String type;
    private IMRoomOnlineMemberFragment.OnlineMemberItemClick onlineItemClick;

    private FragmentTransaction fragmentTransaction;
    private IMRoomOnlineMemberFragment imRoomOnlineMemberFragment;

    public IMRoomMemberListDialog() {
    }

    public static IMRoomMemberListDialog newOnlineUserListInstance(Context context) {
        return newInstance(context.getString(R.string.online_user_text), TYPE_ONLINE_USER);
    }

    public static IMRoomMemberListDialog newInstance(String title, String type) {
        IMRoomMemberListDialog listDataDialog = new IMRoomMemberListDialog();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_TITLE, title);
        bundle.putString(KEY_TYPE, type);
        listDataDialog.setArguments(bundle);
        return listDataDialog;
    }

    public void setOnlineItemClick(IMRoomOnlineMemberFragment.OnlineMemberItemClick onlineItemClick) {
        this.onlineItemClick = onlineItemClick;
    }

    @Override
    public void show(FragmentManager fragmentManager, String tag) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(this, tag).addToBackStack(null);
        transaction.commitAllowingStateLoss();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            String titleArg = arguments.getString(KEY_TITLE);
            this.title = titleArg != null ? titleArg : "";
            String typeArg = arguments.getString(KEY_TYPE);
            this.type = typeArg != null ? typeArg : "";
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        // setup window and width
        View view = inflater.inflate(R.layout.dialog_list_data, window.findViewById(android.R.id.content), false);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        setCancelable(true);
        TextView titleTextView = view.findViewById(R.id.tv_list_data_title);
        titleTextView.setText(this.title);
        view.findViewById(R.id.iv_close_dialog).setOnClickListener(this);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!getActivity().isFinishing()) {
            switch (this.type) {
                case TYPE_ONLINE_USER:
                    if (null != fragmentTransaction && null != imRoomOnlineMemberFragment && imRoomOnlineMemberFragment.isVisible()) {
                        fragmentTransaction.hide(imRoomOnlineMemberFragment);
                    }

                    //参考：https://stackoverflow.com/questions/24561874/android-fragmenttransaction-commit-already-called
                    //解决因使用同一个fragmentTransaction多次commit抛java.lang.IllegalStateException: commit already called
                    fragmentTransaction = getChildFragmentManager().beginTransaction();

                    if (null == imRoomOnlineMemberFragment) {
                        imRoomOnlineMemberFragment = new IMRoomOnlineMemberFragment();
                        Bundle bundle = new Bundle();
                        bundle.putBoolean(KEY_ISMIC, false);
                        imRoomOnlineMemberFragment.setArguments(bundle);
                        fragmentTransaction.add(R.id.fl_container, imRoomOnlineMemberFragment,
                                IMRoomOnlineMemberFragment.class.getSimpleName());
                    }
                    if (onlineItemClick != null) {
                        imRoomOnlineMemberFragment.setOnlineMemberItemClick(chatRoomMember -> {
                            if (onlineItemClick != null) {
                                dismiss();
                                onlineItemClick.onItemClick(chatRoomMember);
                            }
                        });
                    }
                    fragmentTransaction.show(imRoomOnlineMemberFragment).commitAllowingStateLoss();
                    break;
                default:
                    break;
            }
        }
    }

    public void show(FragmentManager fragmentManager) {
        show(fragmentManager, this.type);
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (null != imRoomOnlineMemberFragment) {
            imRoomOnlineMemberFragment.setOnlineMemberItemClick(null);
            if (imRoomOnlineMemberFragment.isVisible() && null != fragmentTransaction) {
                fragmentTransaction.hide(imRoomOnlineMemberFragment);
            }
            imRoomOnlineMemberFragment = null;
            if (!getChildFragmentManager().getFragments().isEmpty()) {
                getChildFragmentManager().getFragments().clear();
            }
            fragmentTransaction = null;
        }
        if (null != onlineItemClick) {
            onlineItemClick = null;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_close_dialog:
                try {
                    dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
    }
}
