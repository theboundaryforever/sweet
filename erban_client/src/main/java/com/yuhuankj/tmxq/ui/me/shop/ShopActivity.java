package com.yuhuankj.tmxq.ui.me.shop;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.tongdaxing.xchat_core.home.TabInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.ui.room.adapter.CommonMagicIndicatorAdapter;
import com.yuhuankj.tmxq.widget.TitleBar;
import com.yuhuankj.tmxq.widget.magicindicator.MagicIndicator;
import com.yuhuankj.tmxq.widget.magicindicator.ViewPagerHelper;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.UIUtil;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.commonnavigator.CommonNavigator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.tongdaxing.xchat_core.Constants.SHOP_TAB_INDEX;
import static com.tongdaxing.xchat_core.Constants.SHOP_TAG_NICK;
import static com.tongdaxing.xchat_core.Constants.SHOP_TAG_UID;

/**
 * Created by Administrator on 2018/3/30.
 */

public class ShopActivity extends BaseActivity {
    @BindView(R.id.title_bar)
    TitleBar titleBar;
    @BindView(R.id.vp_shop)
    ViewPager vpShop;
    @BindView(R.id.shop_indicator)
    MagicIndicator shopIndicator;

    private CarAdapter carAdapter;
    private boolean showVgg = false;
    private String typeUrl = "";
    private List<Fragment> fragments;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int tabIndex = getIntent().getIntExtra(SHOP_TAB_INDEX, 0);

        long tagUid = getIntent().getLongExtra(SHOP_TAG_UID, 0);
        String tagNick = getIntent().getStringExtra(SHOP_TAG_NICK);


        setContentView(R.layout.activity_car);
        initTitleBar("道具商城");
        ButterKnife.bind(this);

        fragments = new ArrayList<>();


        ShopFragment headwearFragment = new ShopFragment();
        if (tagUid > 0) {
            headwearFragment.setTagNick(tagNick);
            headwearFragment.setTagUid(tagUid);
        }
        headwearFragment.setPrizeName(getResources().getString(R.string.head_wear));
        headwearFragment.setShortUrl("/headwear");
        headwearFragment.setPrizeParamsName("headwear");
        headwearFragment.setShowVgg(false);
        fragments.add(headwearFragment);

        ShopFragment carFragment = new ShopFragment();
        if (tagUid > 0) {
            carFragment.setTagNick(tagNick);
            carFragment.setTagUid(tagUid);
        }
        carFragment.setPrizeName(getResources().getString(R.string.car));
        carFragment.setShortUrl("/giftCar");
        carFragment.setPrizeParamsName("car");
        carFragment.setShowVgg(true);
        fragments.add(carFragment);

        FragmentPagerAdapter fragmentPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public int getCount() {
                return fragments.size();
            }

            @Override
            public Fragment getItem(int position) {
                return fragments.get(position);
            }
        };


        List<TabInfo> mTabInfoList = new ArrayList<>();
        mTabInfoList.add(new TabInfo(1, "头饰"));
        mTabInfoList.add(new TabInfo(1, "座驾"));
        CommonNavigator commonNavigator = new CommonNavigator(this);
        CommonMagicIndicatorAdapter magicIndicatorAdapter = new CommonMagicIndicatorAdapter(this,
                mTabInfoList, UIUtil.dip2px(this, 4));
        magicIndicatorAdapter.setOnItemSelectListener(new CommonMagicIndicatorAdapter.OnItemSelectListener() {
            @Override
            public void onItemSelect(int position) {
                vpShop.setCurrentItem(position);
            }
        });
        commonNavigator.setAdapter(magicIndicatorAdapter);
        commonNavigator.setAdjustMode(true);

        shopIndicator.setNavigator(commonNavigator);
        vpShop.setAdapter(fragmentPagerAdapter);
        if (fragments.size() > tabIndex) {
            vpShop.setCurrentItem(tabIndex);
        }
        ViewPagerHelper.bind(shopIndicator, vpShop);

    }


}
