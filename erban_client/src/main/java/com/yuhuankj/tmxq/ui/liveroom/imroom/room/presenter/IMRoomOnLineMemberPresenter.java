package com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.im.IMReportResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomModel;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMember;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMessage;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomOnlineMember;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IMRoomOnLineMemberView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * <p>  </p>
 *
 * @author jiahui
 * @date 2017/12/8
 */
public class IMRoomOnLineMemberPresenter extends AbstractMvpPresenter<IMRoomOnLineMemberView> {

    private final String TAG = IMRoomOnLineMemberPresenter.class.getSimpleName();

    private final IMRoomModel mIMRoomModel;

    public IMRoomOnLineMemberPresenter() {
        mIMRoomModel = new IMRoomModel();
    }

    /**
     * 分页获取房间成员：第一页包含队列成员，固定成员，游客50人，之后每一页获取游客50人
     *
     * @param page  页数
     * @param index 从哪个索引开始取
     */
    public void requestChatMemberByIndex(final int page, int index, final List<IMRoomOnlineMember> oldList) {

        mIMRoomModel.getRoomMembers(index, new OkHttpManager.MyCallBack<IMReportResult<List<IMRoomMember>>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().onRequestChatMemberByPageFail("网络异常", page);
                }
            }

            @Override
            public void onResponse(IMReportResult<List<IMRoomMember>> serviceResult) {
                if (getMvpView() != null) {
                    if (null == serviceResult) {
                        getMvpView().onRequestChatMemberByPageFail(
                                BasicConfig.INSTANCE.getAppContext().getString(R.string.network_error),
                                page);
                        return;
                    }
                    if (!serviceResult.isSuccess()) {
                        getMvpView().onRequestChatMemberByPageFail(serviceResult.getErrmsg(), page);
                        return;
                    }

                    List<IMRoomMember> imChatRoomMembers = serviceResult.getData();
                    if (ListUtils.isListEmpty(imChatRoomMembers)) {
                        getMvpView().onRequestChatMemberByPageFail("没有更多数据", page);
                    } else {
                        List<IMRoomOnlineMember> onlineChatMembers =
                                mIMRoomModel.memberToOnlineMember(imChatRoomMembers, false, oldList);
                        getMvpView().onRequestChatMemberByPageSuccess(onlineChatMembers, page);
                    }
                }
            }
        });
    }

    /**
     * 成员进来刷新在线列表
     */
    public void onMemberInRefreshData(String account, List<IMRoomOnlineMember> imRoomOnLineMembers,
                                      final int page, IMRoomMessage imRoomMessage) {
        if (imRoomMessage == null) {
            return;
        }
        IMRoomMember imChatRoomMember = imRoomMessage.getImRoomMember();
        if (imChatRoomMember == null) {
            return;
        }
        List<IMRoomMember> imChatRoomMembers = new ArrayList<>(1);
        imChatRoomMembers.add(imChatRoomMember);

        List<IMRoomOnlineMember> onlineChatMemberList = mIMRoomModel.memberToOnlineMember(imChatRoomMembers,
                false, imRoomOnLineMembers);
        if (getMvpView() != null) {
            getMvpView().onRequestChatMemberByPageSuccess(onlineChatMemberList, page);
        }
    }

    public Disposable onMemberDownUpMic(String account, boolean isUpMic, List<IMRoomOnlineMember> dataList, final int page) {
        return mIMRoomModel.onMemberDownUpMic(account, isUpMic, dataList).subscribe(
                new Consumer<List<IMRoomOnlineMember>>() {

                    @Override
                    public void accept(List<IMRoomOnlineMember> onlineChatMembers) throws Exception {
                        if (getMvpView() != null) {
                            getMvpView().onRequestChatMemberByPageSuccess(onlineChatMembers, page);
                        }
                    }
                });
    }


    public Disposable onUpdateMemberManager(String account, List<IMRoomOnlineMember> dataList,
                                            boolean isRemoveManager, final int page) {
        return mIMRoomModel.onUpdateMemberManager(account, isRemoveManager, dataList).subscribe(
                new Consumer<List<IMRoomOnlineMember>>() {

                    @Override
                    public void accept(List<IMRoomOnlineMember> onlineChatMembers) throws Exception {
                        if (getMvpView() != null) {
                            getMvpView().onRequestChatMemberByPageSuccess(onlineChatMembers, page);
                        }
                    }
                });
    }
}
