package com.yuhuankj.tmxq.ui.liveroom.nimroom.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.presenter.RoomSettingPresenter;
import com.tongdaxing.xchat_core.room.view.IRoomSettingView;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.widget.TitleBar;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/5/2.
 */
@CreatePresenter(RoomSettingPresenter.class)
public class RoomTopicActivity extends BaseMvpActivity<IRoomSettingView, RoomSettingPresenter>
        implements IRoomSettingView {
    @BindView(R.id.title_bar)
    TitleBar titleBar;
    @BindView(R.id.edt_topic_title)
    EditText edtTopicTitle;

    @BindView(R.id.edt_topic_content)
    EditText edtTopicContent;
    InputFilter enterBlankInputFilter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            if (source.toString().contentEquals("\n")) {
                return "";
            } else {
                return null;
            }
        }
    };

    public static void start(Context context) {
        Intent intent = new Intent(context, RoomTopicActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoomInfo roomInfo = BaseRoomServiceScheduler.getServerRoomInfo();
        setContentView(R.layout.room_topic_edit);
        ButterKnife.bind(this);

        String roomNotice = roomInfo.getRoomNotice();
        if (!TextUtils.isEmpty(roomNotice)) {
            edtTopicContent.setText(roomNotice);
        }
        edtTopicTitle.setText(roomInfo.getRoomDesc());
        try {
            edtTopicTitle.setSelection(edtTopicTitle.getText().toString().length());
        } catch (Exception e) {
            e.printStackTrace();
        }
        edtTopicTitle.setFilters(new InputFilter[]{enterBlankInputFilter});
        initTitleBar(getResources().getString(R.string.room_topic_title));
        TextView rightAction = new TextView(this);
        rightAction.setTextColor(Color.parseColor("#1a1a1a"));
        rightAction.setText(getResources().getString(R.string.save));
        rightAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String roomDesc = edtTopicTitle.getText().toString();
                String roomNotice = edtTopicContent.getText().toString();
                if (TextUtils.isEmpty(roomDesc)) {
                    toast(getResources().getString(R.string.room_topic_title_empty_tips));
                    return;
                }
                getMvpPresenter().updateRoomInfo(roomInfo.title, roomDesc,
                        roomInfo.getRoomPwd(), roomInfo.getRoomTag(), roomInfo.getTagId(),
                        roomInfo.getBackPic(), roomInfo.getBackName(), roomInfo.getGiftEffectSwitch(),
                        roomInfo.getCharmSwitch(), roomNotice, roomInfo.getPlayInfo());
            }
        });
        titleBar.mRightLayout.addView(rightAction);

        GrowingIO.getInstance().trackEditText(edtTopicTitle);
        GrowingIO.getInstance().trackEditText(edtTopicContent);
    }

    @Override
    public void updateRoomInfoSuccess(RoomInfo roomInfo) {
        toast(getResources().getString(R.string.room_topic_save_success));
        finish();
    }

    @Override
    public void updateRoomInfoFail(String error) {
        toast(error);
    }

    @Override
    public void onResultRequestTagAllSuccess(List<TabInfo> tabInfoList) {
        //忽略
    }

    @Override
    public void onResultRequestTagAllFail(String error) {
        //忽略
    }
}
