package com.yuhuankj.tmxq.ui.liveroom.imroom.gift.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;

import com.juxiao.library_utils.DisplayUtils;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.imroom.gift.listener.OnSendGiftCountListener;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/5/17
 */
public class SendGiftCountWindow extends PopupWindow implements View.OnClickListener {
    private OnSendGiftCountListener onSendGiftCountListener;

    public OnSendGiftCountListener getOnSendGiftCountListener() {
        return onSendGiftCountListener;
    }

    public void setOnSendGiftCountListener(OnSendGiftCountListener onSendGiftCountListener) {
        this.onSendGiftCountListener = onSendGiftCountListener;
    }

    public SendGiftCountWindow(Context context) {
        super(context);
        initView(context);
        initListener();
    }


    private void initView(Context context) {
        setContentView(LayoutInflater.from(context).inflate(R.layout.dialog_gift_number, null));
        setWidth(DisplayUtils.dip2px(context, 100));
        setHeight(DisplayUtils.dip2px(context, 215));
        setOutsideTouchable(true);
    }

    private void initListener() {
        getContentView().findViewById(R.id.number_1).setOnClickListener(this);
        getContentView().findViewById(R.id.number_10).setOnClickListener(this);
        getContentView().findViewById(R.id.number_38).setOnClickListener(this);
        getContentView().findViewById(R.id.number_66).setOnClickListener(this);
        getContentView().findViewById(R.id.number_188).setOnClickListener(this);
        getContentView().findViewById(R.id.number_520).setOnClickListener(this);
        getContentView().findViewById(R.id.number_1314).setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        int count = -1;
        switch (v.getId()) {
            case R.id.number_1:
                count = 1;
                break;
            case R.id.number_10:
                count = 10;
                break;
            case R.id.number_38:
                count = 38;
                break;
            case R.id.number_66:
                count = 66;
                break;
            case R.id.number_188:
                count = 188;
                break;
            case R.id.number_520:
                count = 520;
                break;
            case R.id.number_1314:
                count = 1314;
                break;
        }
        if (onSendGiftCountListener != null && count != -1) {
            onSendGiftCountListener.onSelectCount(count);
        }
    }

    public void showGiftCountAtAnchor(View giftNumLayout) {
        showAsDropDown(giftNumLayout);
    }
}
