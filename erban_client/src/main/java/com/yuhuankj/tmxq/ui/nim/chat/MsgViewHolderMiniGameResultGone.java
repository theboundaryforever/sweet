package com.yuhuankj.tmxq.ui.nim.chat;

import com.netease.nim.uikit.common.ui.recyclerview.adapter.BaseMultiItemFetchLoadAdapter;
import com.netease.nim.uikit.session.viewholder.MsgViewHolderBase;

/**
 * 小游戏结束消息
 */
public class MsgViewHolderMiniGameResultGone extends MsgViewHolderBase {

    public MsgViewHolderMiniGameResultGone(BaseMultiItemFetchLoadAdapter adapter) {
        super(adapter);
    }

    @Override
    protected boolean isShowHeadImage() {
        return false;
    }

    @Override
    protected boolean isShowBubble() {
        return false;
    }

    @Override
    protected int getContentResId() {
        return com.netease.nim.uikit.R.layout.view_minigame_msg_gone;
    }

    @Override
    protected void inflateContentView() {
    }

    @Override
    protected void bindContentView() {

    }

}
