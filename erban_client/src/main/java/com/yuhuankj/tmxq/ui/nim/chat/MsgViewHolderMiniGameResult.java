package com.yuhuankj.tmxq.ui.nim.chat;

import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.netease.nim.uikit.common.ui.recyclerview.adapter.BaseMultiItemFetchLoadAdapter;
import com.netease.nim.uikit.session.viewholder.MsgViewHolderBase;
import com.tongdaxing.erban.libcommon.utils.ResolutionUtils;
import com.tongdaxing.xchat_core.bean.MiniGameResult;
import com.tongdaxing.xchat_core.im.custom.bean.MiniGameResultAttachment;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

/**
 * 小游戏结束消息
 */
public class MsgViewHolderMiniGameResult extends MsgViewHolderBase {

    private RelativeLayout rlPlayerStatusBg;
    private ImageView imvGameBg;
    private ImageView imvImage;
    private TextView tvGameName;
    private MiniGameResult info;

    public MsgViewHolderMiniGameResult(BaseMultiItemFetchLoadAdapter adapter) {
        super(adapter);
    }

    @Override
    protected int getContentResId() {
        info = ((MiniGameResultAttachment) message.getAttachment()).getDataInfo();
        return R.layout.view_minigame_chat_result;
    }

    @Override
    protected void inflateContentView() {
        rlPlayerStatusBg = findViewById(R.id.rlPlayerStatusBg);
        imvGameBg = findViewById(R.id.imvGameBg);
        tvGameName = findViewById(R.id.tvGameName);
        imvImage = findViewById(R.id.imvImage);
    }

    @Override
    protected void bindContentView() {
        contentContainer.setPadding(0, 0, 0, 0);
        info = ((MiniGameResultAttachment) message.getAttachment()).getDataInfo();
        tvGameName.setText(info.getGameName());
        ImageLoadUtils.loadImage(context, info.getGameBgImage(), imvGameBg);
        ImageLoadUtils.loadImage(context, info.getOppAvatar(), imvImage);

        if (info.getResult() == 0) {//0:输
            rlPlayerStatusBg.setGravity(Gravity.CENTER_HORIZONTAL);
            ((RelativeLayout.LayoutParams) rlPlayerStatusBg.getLayoutParams()).topMargin = (int) ResolutionUtils.convertDpToPixel(10, context);
            rlPlayerStatusBg.setPadding((int) ResolutionUtils.convertDpToPixel(5, context), (int) ResolutionUtils.convertDpToPixel(6, context), 0, 0);
            setLayoutWidthAndHeight(imvImage, (int) ResolutionUtils.convertDpToPixel(55, context), (int) ResolutionUtils.convertDpToPixel(55, context));
            setLayoutWidthAndHeight(rlPlayerStatusBg, (int) ResolutionUtils.convertDpToPixel(100, context), (int) ResolutionUtils.convertDpToPixel(88, context));
            rlPlayerStatusBg.setBackgroundResource(R.drawable.ic_minigame_chat_shibai);
        } else if (info.getResult() == 1) { //1：赢
            rlPlayerStatusBg.setGravity(Gravity.CENTER);
            ((RelativeLayout.LayoutParams) rlPlayerStatusBg.getLayoutParams()).topMargin = 0;
            rlPlayerStatusBg.setPadding((int) ResolutionUtils.convertDpToPixel(2, context), 0, 0, 0);
            setLayoutWidthAndHeight(imvImage, (int) ResolutionUtils.convertDpToPixel(52, context), (int) ResolutionUtils.convertDpToPixel(52, context));
            setLayoutWidthAndHeight(rlPlayerStatusBg, (int) ResolutionUtils.convertDpToPixel(125, context), (int) ResolutionUtils.convertDpToPixel(104, context));
            rlPlayerStatusBg.setBackgroundResource(R.drawable.ic_minigame_chat_shengli);
        } else { //2:和
            rlPlayerStatusBg.setGravity(Gravity.CENTER_HORIZONTAL);
            ((RelativeLayout.LayoutParams) rlPlayerStatusBg.getLayoutParams()).topMargin = (int) ResolutionUtils.convertDpToPixel(10, context);
            rlPlayerStatusBg.setPadding((int) ResolutionUtils.convertDpToPixel(5, context), (int) ResolutionUtils.convertDpToPixel(6, context), 0, 0);
            setLayoutWidthAndHeight(imvImage, (int) ResolutionUtils.convertDpToPixel(55, context), (int) ResolutionUtils.convertDpToPixel(55, context));
            setLayoutWidthAndHeight(rlPlayerStatusBg, (int) ResolutionUtils.convertDpToPixel(100, context), (int) ResolutionUtils.convertDpToPixel(88, context));
            rlPlayerStatusBg.setBackgroundResource(R.drawable.ic_minigame_chat_heju);
        }
        rlPlayerStatusBg.requestLayout();
        rlPlayerStatusBg.invalidate();
    }

    /**
     * 设置一个布局的高度 by liaoxy
     */
    public static void setLayoutHeight(View view, int height) {
        ViewGroup.LayoutParams lp = view.getLayoutParams();
        if (lp == null) {
            return;
        }
        lp.height = height;
        view.setLayoutParams(lp);
        view.requestLayout();
    }

    /**
     * 设置一个布局的宽度 by liaoxy
     */
    public static void setLayoutWidthAndHeight(View view, int width, int height) {
        setLayoutWidth(view, width);
        setLayoutHeight(view, height);
    }

    /**
     * 设置一个布局的宽度 by liaoxy
     */
    public static void setLayoutWidth(View view, int width) {
        ViewGroup.LayoutParams lp = view.getLayoutParams();
        if (lp == null) {
            return;
        }
        lp.width = width;
        view.setLayoutParams(lp);
        view.requestLayout();
    }
}
