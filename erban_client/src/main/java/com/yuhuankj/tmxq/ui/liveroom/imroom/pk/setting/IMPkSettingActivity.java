package com.yuhuankj.tmxq.ui.liveroom.imroom.pk.setting;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.growingio.android.sdk.collection.GrowingIO;
import com.juxiao.library_ui.widget.AppToolBar;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomQueueInfo;
import com.tongdaxing.xchat_core.pk.bean.PkVoteInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter.IMPkUserAdapter;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.activity.PkHistoryActivity;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.widget.ButtonItemFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 文件描述：新版本IM的PK设置页面
 *
 * @auther：zwk
 * @data：2019/7/8
 */
@CreatePresenter(RoomPkSettingPresenter.class)
public class IMPkSettingActivity extends BaseMvpActivity<IRoomPkSettingView, RoomPkSettingPresenter>
        implements IRoomPkSettingView, View.OnClickListener, BaseQuickAdapter.OnItemClickListener{
    @BindView(R.id.atb_title)
    AppToolBar atbTitle;
    @BindView(R.id.tv_pk_type)
    TextView tvPkType;
    @BindView(R.id.rl_select_pk_type)
    RelativeLayout rlSelectPkType;
    @BindView(R.id.tv_pk_time)
    TextView tvPkTime;
    @BindView(R.id.rl_select_pk_time)
    RelativeLayout rlSelectPkTime;
    @BindView(R.id.bu_pk_submit)
    TextView buPkSubmit;
    @BindView(R.id.rcvUser)
    RecyclerView rcvUser;
    @BindView(R.id.edtPkName)
    EditText edtPkName;

    private int pkTime = 60;
    private int pkType = 1;

    private IMPkUserAdapter pkUserAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_im_pk_setting);
        ButterKnife.bind(this);
        atbTitle.setOnLeftImgBtnClickListener(this::finish);
        atbTitle.setOnRightTitleClickListener(() -> PkHistoryActivity.start(IMPkSettingActivity.this));
        rlSelectPkTime.setOnClickListener(this);
        rlSelectPkType.setOnClickListener(this);
        buPkSubmit.setOnClickListener(this);
        pkUserAdapter = new IMPkUserAdapter();
        pkUserAdapter.bindToRecyclerView(rcvUser);
        pkUserAdapter.setOnItemClickListener(this);
        getMvpPresenter().getRoomPKMemberListInfo();
        GrowingIO.getInstance().trackEditText(edtPkName);
    }

    @Override
    public void showPKMemberListView(List<IMRoomQueueInfo> datas) {
        if (pkUserAdapter != null) {
            pkUserAdapter.setNewData(datas);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_select_pk_type:
                buildOptionDialog(getMvpPresenter().getPkType(), pkType);
                break;
            case R.id.rl_select_pk_time:
                buildOptionDialog(getMvpPresenter().getPkTime(), pkTime);
                break;
            case R.id.bu_pk_submit:
                submit();
                break;
            default:
        }
    }

    private void submit() {
        getDialogManager().showProgressDialog(this, getString(R.string.network_loading));
        buPkSubmit.setEnabled(false);
        getMvpPresenter().saveRoomPkInfo(pkType, pkTime, edtPkName.getText().toString(), pkUserAdapter.getData());
        //房间-进入PK设置
        StatisticManager.get().onEvent(this,
                StatisticModel.EVENT_ID_ROOM_OPEN_PK,
                StatisticModel.getInstance().getUMAnalyCommonMap(this));
    }


    @Override
    public void savePKSucView(PkVoteInfo pkVoteInfo) {
        getDialogManager().dismissDialog();
        buPkSubmit.setEnabled(true);
//        IMRoomMessageManager.get().sendPkStartMessage(CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK,
//                CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_START_NEW, pkVoteInfo);
        toast("发起PK成功");
        finish();
    }

    @Override
    public void savePkError(String error) {
        buPkSubmit.setEnabled(true);
        getDialogManager().dismissDialog();
        toast(error);
    }

    public void buildOptionDialog(Map<String, Integer> jsons, int type) {
        List<ButtonItem> buttonItems = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : jsons.entrySet()) {
            ButtonItem msgBlackListItem = ButtonItemFactory.createMsgBlackListItem(entry.getKey(), new ButtonItemFactory.OnItemClick() {
                @Override
                public void itemClick() {
                    optionAction(entry.getValue(), entry.getKey());
                }
            });
            if (entry.getValue() == type) {
                msgBlackListItem.textColor = "#09CAA2";
            }
            buttonItems.add(msgBlackListItem);
        }
        getDialogManager().showCommonPopupDialog(buttonItems, getString(R.string.cancel));
    }

    private void optionAction(int type, String name) {
        if (type < 30) {
            pkType = type;
            if (type == 1) {
                tvPkType.setText("按投票人数PK ");
            } else {
                tvPkType.setText("按礼物价值PK ");
            }
        } else {
            pkTime = type;
            tvPkTime.setText(name + " ");
        }
    }

//
//    @Override
//    public void onGetOnLineUserList(boolean isSuccess, String message, int page, List<OnlineChatMember> onlineChatMembers) {
//        if (isSuccess) {
//            micUsers = new ArrayList<>();
//            datas = new ArrayList<>(Collections.nCopies(8, null));
//            Collections.copy(datas, micUsers);
//            pkUserAdapter.setNewData(datas);
//        } else if (!TextUtils.isEmpty(message)) {
//            toast(message);
//        } else {
//            toast("获取麦上用户失败");
//        }
//    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        if (pkUserAdapter != null && !ListUtils.isListEmpty(pkUserAdapter.getData()) && pkUserAdapter.getData().size() > position) {
            pkUserAdapter.getData().get(position).setSelect(!pkUserAdapter.getData().get(position).isSelect());
            adapter.notifyDataSetChanged();
        }
    }

}
