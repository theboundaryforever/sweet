package com.yuhuankj.tmxq.ui.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.praise.IPraiseClient;
import com.tongdaxing.xchat_core.praise.IPraiseCore;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_core.utils.StarUtils;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.me.noble.NobleBusinessManager;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.utils.Utils;
import com.yuhuankj.tmxq.widget.CircleImageView;
import com.yuhuankj.tmxq.widget.LevelView;

import java.util.Date;
import java.util.Locale;

/**
 * @author xiaoyu
 * @date 2017/12/13
 */

public class UserInfoDialog extends BottomSheetDialog implements View.OnClickListener {
    private static final String TAG = "UserInfoDialog";
    private Context context;
    private long uid;
    private boolean isAttention;
    private UserInfo userInfo;
    private TextView report;
    private ImageView closeImage;
    private CircleImageView avatar;
    private TextView nick;
    private TextView erbanId;
    private TextView constellation;
    private TextView usePageBtn;
    private TextView fansNumber;
    private TextView attentionBtn;
    private long myUid;
    private LevelView mLevelView;
    private ImageView ivUserNobleMedal;
    private ImageView imvMedal;

    public UserInfoDialog(Context context, long uid) {
        super(context, R.style.ErbanUserInfoDialog);
        this.context = context;
        this.uid = uid;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CoreManager.addClient(this);
        setContentView(R.layout.dialog_user_info);
        setCanceledOnTouchOutside(true);
        closeImage = findViewById(R.id.close_image);
        report = findViewById(R.id.report_text);
        avatar = findViewById(R.id.avatar);
        nick = findViewById(R.id.nick);
        erbanId = findViewById(R.id.tv_erban_id);
        constellation = findViewById(R.id.tv_constellation);
        usePageBtn = findViewById(R.id.user_info_page_btn);
        attentionBtn = findViewById(R.id.attention_btn);
        fansNumber = findViewById(R.id.fans_number);
        mLevelView = findViewById(R.id.level_info_user_dialog);
        ivUserNobleMedal = findViewById(R.id.ivUserNobleMedal);
        imvMedal = findViewById(R.id.imvMedal);
        usePageBtn.setOnClickListener(this);
        attentionBtn.setOnClickListener(this);
        closeImage.setOnClickListener(this);
        report.setOnClickListener(this);

        // set width
        Window window = getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display d = windowManager.getDefaultDisplay();
        DisplayMetrics realDisplayMetrics = new DisplayMetrics();
        d.getRealMetrics(realDisplayMetrics);
        params.height = (realDisplayMetrics.heightPixels) - (Utils.hasSoftKeys(context) ?
                Utils.getNavigationBarHeight(context) : 0);
        window.setAttributes(params);
//        window.setWindowAnimations(R.style.ErbanCommonWindowAnimationStyle);

        userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(uid, true);
        myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();

        if (uid == myUid) {
            findViewById(R.id.divider).setVisibility(View.GONE);
            findViewById(R.id.attention_btn).setVisibility(View.GONE);
        } else {
            CoreManager.getCore(IPraiseCore.class).isPraised(myUid, uid);
        }

        updateView();
    }

    private void updateView() {
        if (userInfo != null) {
            String avator = null;
            String nickStr = null;
            long erbanNo = 0L;
            int nickColor = 0;
            if (AvRoomDataManager.get().isRoomOwner(uid) || uid == CoreManager.getCore(IAuthCore.class).getCurrentUid()) {
                avator = userInfo.getAvatar();
                nickStr = userInfo.getNick();
                erbanNo = userInfo.getErbanNo();
                nickColor = context.getResources().getColor(R.color.text_1A1A1A);
            } else {
                avator = NobleBusinessManager.getNobleRoomAvatarUrl(
                        userInfo.getVipId(), userInfo.getIsInvisible(), userInfo.getVipDate(),
                        userInfo.getAvatar());
                erbanNo = NobleBusinessManager.getNobleUserInfoDialogErbanNo(userInfo.getVipId(),
                        userInfo.getIsInvisible(), userInfo.getVipDate(), userInfo.getInvisibleUid(),
                        userInfo.getErbanNo());
                nickStr = NobleBusinessManager.getNobleRoomNick(userInfo.getVipId(), userInfo.getIsInvisible(), userInfo.getVipDate(), userInfo.getNick());
                nickColor = NobleBusinessManager.getNobleRoomNickColor(userInfo.getVipId(),
                        userInfo.getVipDate(), userInfo.getIsInvisible(), context.getResources().getColor(R.color.text_1A1A1A), nickStr);
            }
            ImageLoadUtils.loadAvatar(getContext(), avator, avatar, true);
            if (!TextUtils.isEmpty(nickStr) && nickStr.length() > 6) {
                nickStr = getContext().getString(R.string.nick_length_max_six, nickStr.substring(0, 6));
            }
            nick.setText(nickStr);
            nick.setTextColor(nickColor);
            erbanId.setText(String.format(Locale.getDefault(),
                    getContext().getString(R.string.me_user_id), erbanNo));
            Drawable drawable;
            if (userInfo.getGender() == 1) {
                drawable = context.getResources().getDrawable(R.drawable.icon_man);
            } else {
                drawable = context.getResources().getDrawable(R.drawable.icon_female);
            }
            nick.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
            //设置星座
            String star = StarUtils.getConstellation(new Date(userInfo.getBirth()));
            constellation.setText(star);
            //设置粉丝数量
            fansNumber.setText(String.format(Locale.getDefault(),
                    getContext().getString(R.string.dialog_user_info_fans_number_text), userInfo.getFansNum()));

            mLevelView.setExperLevel(userInfo.getExperLevel());
            mLevelView.setCharmLevel(userInfo.getCharmLevel());

            if (!TextUtils.isEmpty(userInfo.getVipMedal())) {
                ivUserNobleMedal.setVisibility(View.VISIBLE);
                GlideApp.with(getContext()).load(userInfo.getVipMedal())
                        .dontAnimate().into(ivUserNobleMedal);
            } else {
                ivUserNobleMedal.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(userInfo.getAlias())) {
                imvMedal.setVisibility(View.VISIBLE);
                GlideApp.with(getContext()).load(userInfo.getAlias())
                        .dontAnimate().into(imvMedal);
            } else {
                imvMedal.setVisibility(View.GONE);
            }
        }
    }

    //------------------------------IPraiseClient--------------------------------
    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onIsLiked(Boolean islike, long uid) {
        isAttention = islike;
        if (islike) {
            attentionBtn.setText("已关注");
        } else {
            attentionBtn.setText("关注");
        }
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraise(long likedUid) {
        isAttention = true;
        attentionBtn.setText("已关注");
        SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), "关注成功，相互关注可成为好友哦！", Toast.LENGTH_SHORT);
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onCanceledPraise(long uid) {
        isAttention = false;
        attentionBtn.setText("关注");
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfo(UserInfo info) {
        if (info.getUid() == uid) {
            this.userInfo = info;
            updateView();
        }
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        CoreManager.removeClient(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.report_text:
                Log.i(TAG, "onClick: click report text");
                break;

            case R.id.attention_btn:
                if (userInfo != null) {
                    if (isAttention) {
                        CoreManager.getCore(IPraiseCore.class).cancelPraise(userInfo.getUid());
                    } else {
                        //user_info_dialog_invisiable_atten_tips
                        if (NobleBusinessManager.isNobleMysteryMan(userInfo.getVipId(),
                                userInfo.getIsInvisible(), userInfo.getVipDate())) {
                            SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(),
                                    BasicConfig.INSTANCE.getAppContext().getResources().getString(R.string.user_info_dialog_invisiable_atten_tips),
                                    Toast.LENGTH_SHORT);
                            return;
                        }

                        CoreManager.getCore(IPraiseCore.class).praise(userInfo.getUid());
                    }
                }
                break;
            case R.id.close_image:
                dismiss();
                break;
            case R.id.user_info_page_btn:
                if (null != userInfo) {
                    long targetUid = 0L;
                    if (AvRoomDataManager.get().isRoomOwner(uid) || uid == CoreManager.getCore(IAuthCore.class).getCurrentUid()) {
                        targetUid = uid;
                    } else {
                        targetUid = NobleBusinessManager.getNobleUserUID(
                                userInfo.getVipId(), userInfo.getVipDate(),
                                userInfo.getIsInvisible(), userInfo.getInvisibleUid(), uid);
                    }
                    UserInfoActivity.start(context, targetUid);
                }

                dismiss();
                break;
            default:
        }
    }
}
