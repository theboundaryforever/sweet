package com.yuhuankj.tmxq.ui.liveroom.imroom.gift.listener;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/5/17
 */
public interface OnSendGiftCountListener {
    void onSelectCount(int giftCount);
}
