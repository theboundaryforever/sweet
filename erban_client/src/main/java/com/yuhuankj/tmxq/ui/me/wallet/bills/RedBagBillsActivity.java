package com.yuhuankj.tmxq.ui.me.wallet.bills;

import android.os.Bundle;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.bills.IBillsCore;
import com.tongdaxing.xchat_core.bills.IBillsCoreClient;
import com.tongdaxing.xchat_core.bills.bean.BillItemEntity;
import com.tongdaxing.xchat_core.bills.bean.RedBagInfo;
import com.tongdaxing.xchat_core.bills.bean.RedBagListInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.me.wallet.bills.adapter.RedBagBillsAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 红包记录
 */
public class RedBagBillsActivity extends BillBaseActivity {

    private RedBagBillsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initTitleBar(getString(R.string.bill_red));
    }


    protected void initData() {
        super.initData();
        adapter = new RedBagBillsAdapter(mBillItemEntityList);
        adapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                mCurrentCounter++;
                loadData();
            }
        }, mRecyclerView);
        RecyclerViewNoBugLinearLayoutManager manager = new RecyclerViewNoBugLinearLayoutManager(mActivity);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setAdapter(adapter);
        showLoading();
        loadData();
    }

    protected void loadData() {
        CoreManager.getCore(IBillsCore.class).getRedBagBills(mCurrentCounter, PAGE_SIZE, time);
    }

    private String time1 = "";

    @CoreEvent(coreClientClass = IBillsCoreClient.class)
    public void onGetRedBagBills(RedBagListInfo data) {
        mRefreshLayout.setRefreshing(false);
        if (null != data) {
            if (mCurrentCounter == Constants.PAGE_START) {
                hideStatus();
                mBillItemEntityList.clear();
                adapter.setNewData(mBillItemEntityList);
            } else {
                adapter.loadMoreComplete();
            }
            BillItemEntity billItemEntity;
            List<Map<String, List<RedBagInfo>>> billList = data.getBillList();
            if (!billList.isEmpty()) {
                tvTime.setVisibility(View.GONE);
                int size = mBillItemEntityList.size();
                List<BillItemEntity> billItemEntities = new ArrayList<>();
                for (int i = 0; i < billList.size(); i++) {
                    Map<String, List<RedBagInfo>> map = billList.get(i);
                    for (String key : map.keySet()) {
                        // key ---日期    value：list集合记录
                        List<RedBagInfo> redBagInfos = map.get(key);
                        if (ListUtils.isListEmpty(redBagInfos)) continue;
                        //正常item
                        for (RedBagInfo temp : redBagInfos) {
                            billItemEntity = new BillItemEntity(BillItemEntity.ITEM_NORMAL);
                            billItemEntity.mRedBagInfo = temp;
                            billItemEntity.time = key;  //目的是为了比较
                            billItemEntities.add(billItemEntity);
                        }
                    }
                }
                if (billItemEntities.size() < Constants.BILL_PAGE_SIZE && mCurrentCounter == Constants.PAGE_START) {
                    adapter.setEnableLoadMore(false);
                }
                adapter.addData(billItemEntities);
            } else {
                if (mCurrentCounter == 1) {
                    showNoData(getResources().getString(R.string.bill_no_data_text));
                } else {
                    adapter.loadMoreEnd(true);
                }
            }
        }
    }

    @CoreEvent(coreClientClass = IBillsCoreClient.class)
    public void onGetRedBagBillsError(String error) {
        if (mCurrentCounter == Constants.PAGE_START) {
            showNetworkErr();
        } else {
            adapter.loadMoreFail();
        }
    }
}
