package com.yuhuankj.tmxq.ui.user.bosonFriend;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.libcommon.utils.TimeUtils;
import com.tongdaxing.xchat_core.user.bean.BosonFriendEnitity;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

public class BosonFriendAdapter extends BaseQuickAdapter<BosonFriendEnitity, BaseViewHolder> {

    public BosonFriendAdapter(List data) {
        super(R.layout.item_boson_friend, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, BosonFriendEnitity item) {
        ImageView imvPortrait = helper.getView(R.id.imvPortrait);
        ImageView imvHeadwear = helper.getView(R.id.imvHeadwear);
        TextView tvUserName = helper.getView(R.id.tvUserName);
        TextView tvDate = helper.getView(R.id.tvDate);
        TextView tvDays = helper.getView(R.id.tvDays);
        TextView tvRemove = helper.getView(R.id.tvRemove);
        helper.addOnClickListener(R.id.tvRemove);

        if (isSelf) {
            tvRemove.setVisibility(View.VISIBLE);
        } else {
            tvRemove.setVisibility(View.GONE);
        }
        try {
            ImageLoadUtils.loadCircleImage(mContext, item.getAvatar(), imvPortrait, R.drawable.ic_bosonfriend_level_default);
            ImageLoadUtils.loadCircleImage(mContext, item.getLevelIcon(), imvHeadwear, R.drawable.ic_bosonfriend_level_0);
            tvUserName.setText(item.getNick());
            tvDate.setText(TimeUtils.getDateTimeString(item.getCreateTime(), "yyyy.M.d") + "至今");
            tvDays.setText(item.getDay() + "天" + item.getHour() + "时");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isSelf = false;

    public void setSelf(boolean isSelf) {
        this.isSelf = isSelf;
    }
}