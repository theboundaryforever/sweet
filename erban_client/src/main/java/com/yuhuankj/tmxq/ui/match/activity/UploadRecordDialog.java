package com.yuhuankj.tmxq.ui.match.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatDialog;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.netease.nimlib.sdk.media.record.AudioRecorder;
import com.netease.nimlib.sdk.media.record.IAudioRecordCallback;
import com.netease.nimlib.sdk.media.record.RecordType;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.tongdaxing.erban.libcommon.audio.AudioPlayer;
import com.tongdaxing.erban.libcommon.audio.OnPlayListener;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.audio.AudioPlayAndRecordManager;
import com.tongdaxing.xchat_core.audio.AudioRecordStatus;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.file.IFileCore;
import com.tongdaxing.xchat_core.file.IFileCoreClient;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_core.utils.ThreadUtil;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.utils.Utils;

import java.io.File;
import java.util.Map;

/**
 * 一键连麦-上传录音弹框
 */
public class UploadRecordDialog extends AppCompatDialog implements View.OnClickListener {

    private final String TAG = UploadRecordDialog.class.getSimpleName();
    private FragmentActivity oontext;

    private Chronometer cnt_recordTimer;
    long time = -1;

    private ImageView iv_record;
    private ImageView iv_listen;
    private TextView tv_delete;
    private ImageView iv_done;
    private TextView tv_listen;
    private TextView tv_uploadTips;
    private TextView tv_record;

    private TextView tv_done;
    private TextView tv_changeNexOne;
    private TextView tv_words;

    private AudioRecordStatus audioRecordStatus = AudioRecordStatus.STATE_RECORD_NORMAL;
    private AudioPlayAndRecordManager audioManager;
    private AudioPlayer audioPlayer;
    private AudioRecorder audioRecorder;
    private String audioUrl;
    private final int maxDuration = 30;
    private final int minDuration = 6;
    private final long middleDuration = 10;
    private long lastRecordStartTime = 0L;
    private long lastRecordDuration = 0L;
    private boolean hasUserUpFinger = false;
    private boolean hasRecordOverMaxDuration = false;
    private boolean hasRerecorded = false;

    private SVGAImageView svgaiv_recordAnim;
    private SVGAParser svgaParser;
    private SVGADrawable svgaDrawable;

    private int audioDura;
    private File audioFile;
    private UserInfo userInfo;
    IAudioRecordCallback onRecordCallback = new IAudioRecordCallback() {
        @Override
        public void onRecordReady() {
            LogUtils.d(TAG, "onRecordReady-hasUserUpFinger:" + hasUserUpFinger + " lastRecordDuration:" + lastRecordDuration);
            hasRerecorded = false;
            audioUrl = null;
            changeDeleteBtnUsable(hasRerecorded);
        }

        @Override
        public void onRecordStart(File file, RecordType recordType) {
            LogUtils.d(TAG, "onRecordStart-filepath:" + file.getPath() + " type:" + recordType.name());
            LogUtils.d(TAG, "onRecordStart-hasUserUpFinger:" + hasUserUpFinger + " lastRecordDuration:" + lastRecordDuration);
            if (hasUserUpFinger && lastRecordDuration < minDuration * 1000) {
                if (null != audioManager) {
                    audioManager.stopRecord(true);
                }
            }
        }

        @Override
        public void onRecordSuccess(File file, long l, RecordType recordType) {
            LogUtils.d(TAG, "onRecordSuccess l:" + l + " recordType:" + recordType.name());
            hasRerecorded = true;
            double dura = (double) l / 1000;
            audioDura = (int) Math.round(dura);
            time = audioDura;
            updateRecordTimeOnView();
            LogUtils.d(TAG, "onRecordSuccess filepath:" + file.getPath() + " lenth:" + audioDura + " type:" + recordType.name());
            SingleToastUtil.showToast(oontext.getResources().getString(hasRecordOverMaxDuration ? R.string.record_over_max_duration : R.string.record_done));
            audioFile = file;
            audioRecordStatus = AudioRecordStatus.STATE_RECORD_SUCCESS;
            showByState(audioRecordStatus);
//            svgaiv_recordAnim.stopAnimation(true);
            svgaiv_recordAnim.setImageDrawable(oontext.getResources().getDrawable(R.drawable.icon_upload_record_play_default));
        }

        @Override
        public void onRecordFail() {
            LogUtils.d(TAG, "onRecordFail");
            SingleToastUtil.showToast("录制失败");
            audioRecordStatus = AudioRecordStatus.STATE_RECORD_NORMAL;
            showByState(audioRecordStatus);
//            svgaiv_recordAnim.stopAnimation(true);
            svgaiv_recordAnim.setImageDrawable(oontext.getResources().getDrawable(R.drawable.icon_upload_record_play_default));
        }

        @Override
        public void onRecordCancel() {
            LogUtils.d(TAG, "onRecordCancel");
            audioRecordStatus = AudioRecordStatus.STATE_RECORD_NORMAL;
            showByState(audioRecordStatus);
//            svgaiv_recordAnim.stopAnimation(true);
            svgaiv_recordAnim.setImageDrawable(svgaiv_recordAnim.getContext().getResources().getDrawable(R.drawable.icon_upload_record_play_default));
            if (lastRecordDuration < minDuration * 1000 && null != oontext) {
                SingleToastUtil.showToast(oontext.getResources().getString(R.string.record_no_enough_min_duration));
            }
        }

        @Override
        public void onRecordReachedMaxTime(int i) {
            LogUtils.d(TAG, "onRecordReachedMaxTim-i:" + i);
//            svgaiv_recordAnim.stopAnimation(true);
            svgaiv_recordAnim.setImageDrawable(oontext.getResources().getDrawable(R.drawable.icon_upload_record_play_default));
        }
    };
    private DialogManager dialogManager = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_upload_record);
        initParams();
        initView();
        changeVoiceTextSample();
    }

    /**
     * 录音按钮长按监听器
     */
    View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    hasUserUpFinger = false;
                    //按下的时候先判断当前是否处在房间内

                    if (!BaseRoomServiceScheduler.checkUserInRoom()) {
                        startRecord();
                        LogUtils.d(TAG, "onTouch-ACTION_DOWN lastRecordStartTime:" + lastRecordStartTime);
                        return true;
                    }

                    if (null != dialogManager) {
                        dialogManager.showOkCancelDialog(
                                getContext().getString(R.string.record_need_exit_avroom),
                                true, okCancelDialogListener);
                    }
                    return true;
                case MotionEvent.ACTION_UP:
                    hasUserUpFinger = true;
                    lastRecordDuration = System.currentTimeMillis() - lastRecordStartTime;
                    LogUtils.d(TAG, "onTouch-ACTION_UP lastRecordDuration:" + lastRecordDuration + " hasUserUpFinger:" + hasUserUpFinger);
                    //这里有可能抬手触发了up事件后，录音回调还没回调到onRecordReady，也即recorder.isRecording()返回false导致操作中止
                    audioManager.stopRecord(lastRecordDuration < minDuration * 1000);
                    return true;
                default:
                    return false;
            }
        }
    };
    DialogManager.OkCancelDialogListener okCancelDialogListener = null;

    public UploadRecordDialog(FragmentActivity context) {
        super(context, R.style.ErbanBottomSheetDialog);
        this.oontext = context;

        if (oontext instanceof BaseMvpActivity) {
            dialogManager = ((BaseMvpActivity) oontext).getDialogManager();
        } else if (oontext instanceof BaseActivity) {
            dialogManager = ((BaseActivity) oontext).getDialogManager();
        }
    }

    @Override
    public void show() {
        super.show();
        hasRerecorded = false;
        CoreManager.addClient(this);
        audioManager = AudioPlayAndRecordManager.getInstance();
        audioPlayer = audioManager.getAudioPlayer(oontext, null, onPlayListener);
        audioRecorder = audioManager.getAudioRecorder(oontext, onRecordCallback);
        //svga动画播放效果不太符合预期，暂时使用gif来
        okCancelDialogListener = new DialogManager.OkCancelDialogListener() {
            @Override
            public void onCancel() {
            }

            @Override
            public void onOk() {
                if (null != onNeedExitRoomListener) {
                    onNeedExitRoomListener.onNeedExitRoom();
                }
            }
        };

        UserInfo userSelfInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (null != userSelfInfo) {
            userInfo = userSelfInfo;
        }
        if (null != userInfo) {
            String userVoiceUrl = userInfo.getUserVoice();
            int dur = userInfo.getVoiceDura();
            LogUtils.d(TAG, "show-userVoiceUrl:" + userVoiceUrl + " dur:" + dur);
            if (!TextUtils.isEmpty(userVoiceUrl) && dur > 0) {
                audioUrl = userVoiceUrl;
                audioRecordStatus = AudioRecordStatus.STATE_RECORD_EXISTED;
                time = dur;
                updateRecordTimeOnView();
                showByState(audioRecordStatus);
            } else {
                audioRecordStatus = AudioRecordStatus.STATE_RECORD_NORMAL;
                time = 0;
                updateRecordTimeOnView();
                showByState(audioRecordStatus);
            }
        }
    }

    /**
     * 开始录音计时
     */
    private void startChronometer() {
        cnt_recordTimer.setVisibility(View.VISIBLE);
        cnt_recordTimer.setText("00:00");
        time = -1;
        cnt_recordTimer.setOnChronometerTickListener(chronometerTickListener);
        cnt_recordTimer.setBase(0);
        cnt_recordTimer.start();
    }

    Chronometer.OnChronometerTickListener chronometerTickListener = new Chronometer.OnChronometerTickListener() {

        @Override
        public void onChronometerTick(Chronometer chronometer) {
            if (time == -1) {
                // chronometer.setBase(0); // the base time value
                time = chronometer.getBase();
            } else {
                time++; // timer add
                if (time > maxDuration) {
                    hasRecordOverMaxDuration = true;
                    audioManager.stopRecord(false);
                    cnt_recordTimer.stop();
                    time = maxDuration;
                }
                updateRecordTimeOnView();
            }
        }
    };

    private void updateRecordTimeOnView() {
        String timeVaule;
        if (time < middleDuration) {
            timeVaule = "00:0".concat(String.valueOf(time));
        } else {
            timeVaule = "00:".concat(String.valueOf(time));
        }
        cnt_recordTimer.setText(timeVaule);
    }

    private void initParams() {
        setCancelable(true);
        setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                LogUtils.d(TAG, "onCancel");
            }
        });
        setCanceledOnTouchOutside(false);
        WindowManager windowManager = (WindowManager) oontext.getSystemService(Context.WINDOW_SERVICE);
        Display d = windowManager.getDefaultDisplay();
        DisplayMetrics realDisplayMetrics = new DisplayMetrics();
        d.getRealMetrics(realDisplayMetrics);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.BOTTOM;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = oontext.getResources().getDisplayMetrics().heightPixels - Utils.getNavBarHeight(oontext);
        getWindow().setAttributes(params);
    }

    private void initView() {
        svgaiv_recordAnim = findViewById(R.id.svgaiv_recordAnim);
        svgaiv_recordAnim.setImageDrawable(oontext.getResources().getDrawable(R.drawable.icon_upload_record_play_default));
        cnt_recordTimer = findViewById(R.id.cnt_recordTimer);
        iv_listen = findViewById(R.id.iv_listen);
        tv_uploadTips = findViewById(R.id.tv_uploadTips);
        tv_record = findViewById(R.id.tv_record);
        tv_listen = findViewById(R.id.tv_listen);
        iv_record = findViewById(R.id.iv_record);
        iv_done = findViewById(R.id.iv_done);
        tv_delete = findViewById(R.id.tv_delete);
        tv_done = findViewById(R.id.tv_done);
        tv_changeNexOne = findViewById(R.id.tv_changeNexOne);
        tv_words = findViewById(R.id.tv_words);
        iv_record.setOnTouchListener(onTouchListener);
        iv_listen.setOnClickListener(this);
        iv_done.setOnClickListener(this);
        tv_changeNexOne.setOnClickListener(this);
        tv_done.setOnClickListener(this);
        tv_delete.setOnClickListener(this);
        findViewById(R.id.v_blank).setOnClickListener(this);
        changeDeleteBtnUsable(false);
    }

    private void showByState(AudioRecordStatus state) {
        if (state == AudioRecordStatus.STATE_RECORD_NORMAL) {
            //重置,或者默认开始状态，仅仅展示长按录音
            iv_listen.setVisibility(View.GONE);
            tv_listen.setVisibility(View.GONE);
            tv_uploadTips.setVisibility(View.GONE);
            iv_done.setVisibility(View.GONE);
            tv_done.setVisibility(View.GONE);
            //音频时长重置为0
            cnt_recordTimer.setText("00:00");
            cnt_recordTimer.stop();
            tv_record.setText(oontext.getResources().getString(R.string.press_to_record));
            changeDeleteBtnUsable(hasRerecorded);
        } else if (state == AudioRecordStatus.STATE_RECORD_RECORDING) {
            //正在录音，更新时长计时
            iv_listen.setVisibility(View.GONE);
            tv_listen.setVisibility(View.GONE);
            tv_uploadTips.setVisibility(View.GONE);
            iv_done.setVisibility(View.GONE);
            tv_done.setVisibility(View.GONE);
            tv_record.setText(oontext.getResources().getString(R.string.up_move_to_cancel));
        } else if (state == AudioRecordStatus.STATE_RECORD_SUCCESS) {
            //录音完成，展示删除和试听按钮
            iv_listen.setVisibility(View.VISIBLE);
            tv_listen.setVisibility(View.VISIBLE);
            tv_uploadTips.setVisibility(View.VISIBLE);
            iv_done.setVisibility(View.VISIBLE);
            tv_done.setVisibility(View.VISIBLE);
            tv_record.setText(oontext.getResources().getString(R.string.record_again));
            cnt_recordTimer.stop();
        } else if (state == AudioRecordStatus.STATE_RECORD_EXISTED) {
            iv_listen.setVisibility(View.VISIBLE);
            tv_listen.setVisibility(View.VISIBLE);
            tv_uploadTips.setVisibility(View.VISIBLE);
            iv_done.setVisibility(View.GONE);
            tv_done.setVisibility(View.GONE);
            tv_record.setText(oontext.getResources().getString(R.string.record_again));
            cnt_recordTimer.stop();
            changeDeleteBtnUsable(true);
        }
    }

    private boolean isDismissDialogAfterUpdateUserInfo = false;

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.v_blank:
                //正在上传，对话框应该不可取消,
                // 正在试听,资源直接释放即可，
                // 正在录音，不用理会
                dismiss();
                break;
            case R.id.tv_changeNexOne:
                changeVoiceTextSample();

                //上传录音--换一换
                StatisticManager.get().onEvent(oontext,
                        StatisticModel.EVENT_ID_AUDIO_UPLOAD_CHANGE_ANOTHER,
                        StatisticModel.getInstance().getUMAnalyCommonMap(oontext));
                break;
            case R.id.tv_done:
            case R.id.iv_done:
                if (null != userInfo && !TextUtils.isEmpty(userInfo.getUserVoice())
                        && null != oontext && userInfo.getVoiceDura() > 0) {
                    if (null != dialogManager) {
                        dialogManager.showOkCancelDialog(oontext.getResources().getString(R.string.edit_voice_save_tips6),
                                oontext.getResources().getString(R.string.ok), oontext.getResources().getString(R.string.cancel),
                                true, new DialogManager.AbsOkDialogListener() {
                                    @Override
                                    public void onOk() {
                                        doneRecord();
                                        //上传录音--上传
                                        StatisticManager.get().onEvent(oontext,
                                                StatisticModel.EVENT_ID_AUDIO_UPLOAD_UPLOAD_CLICK,
                                                StatisticModel.getInstance().getUMAnalyCommonMap(oontext));
                                    }
                                });
                    }
                } else {
                    doneRecord();
                    //上传录音--上传
                    StatisticManager.get().onEvent(oontext,
                            StatisticModel.EVENT_ID_AUDIO_UPLOAD_UPLOAD_CLICK,
                            StatisticModel.getInstance().getUMAnalyCommonMap(oontext));
                }
                break;
            case R.id.iv_listen:
                tryToListen();

                //上传录音--试听
                StatisticManager.get().onEvent(oontext,
                        StatisticModel.EVENT_ID_AUDIO_UPLOAD_LISTEN_CLICK,
                        StatisticModel.getInstance().getUMAnalyCommonMap(oontext));
                break;
            case R.id.tv_delete:
                if (null != dialogManager) {
                    dialogManager.showOkCancelDialog(
                            getContext().getResources().getString(R.string.upload_record_delete), true,
                            new DialogManager.AbsOkDialogListener() {
                                @Override
                                public void onOk() {
                                    deleteLastRecord(true);
                                }
                            });
                }


                //上传录音--删除
                StatisticManager.get().onEvent(oontext,
                        StatisticModel.EVENT_ID_AUDIO_UPLOAD_DELETE_CLICK,
                        StatisticModel.getInstance().getUMAnalyCommonMap(oontext));
                break;
            default:
                break;
        }
    }

    private void changeVoiceTextSample() {
//        oontext.getDialogManager().showProgressDialog(oontext, context.getString(R.string.network_loading), false);
        Map<String, String> params = OkHttpManager.getDefaultParam();
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        params.put("uid", uid + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket() + "");
        OkHttpManager.getInstance().getRequest(UriProvider.getVoiceText(), params, new OkHttpManager.MyCallBack<ServiceResult<String>>() {
            @Override
            public void onError(Exception e) {
                LogUtils.d(TAG, "changeVoiceTextSample-onError");
                e.printStackTrace();
//                if(null != oontext){
//                    oontext.getDialogManager().dismissDialog();
//                }
            }

            @Override
            public void onResponse(ServiceResult<String> response) {
                LogUtils.d(TAG, "changeVoiceTextSample-onResponse-str:" + response.getData());
//                if(null != oontext){
//                    oontext.getDialogManager().dismissDialog();
//                }
                if (200 == response.getCode() && !TextUtils.isEmpty(response.getData())) {
                    tv_words.setText(response.getData());
                }
            }
        });
    }

    private void doneRecord() {
        //判断文件是否存在，存在则上传，不关上传结果，直接 隐藏对话框
        isDismissDialogAfterUpdateUserInfo = true;
        //录音时长大于0且已经重新录制过
        if (null != audioFile && hasRerecorded && audioDura > 0) {
            if (null != dialogManager) {
                dialogManager.showProgressDialog(oontext, oontext.getString(R.string.network_loading), false);
            }
            hasRerecorded = false;
            CoreManager.getCore(IFileCore.class).upload(audioFile);
        } else {
            dismiss();
        }
    }

    OnPlayListener onPlayListener = new OnPlayListener() {

        @Override
        public void onPrepared() {
            LogUtils.d(TAG, "onPrepared");
            //准备开始试听,图标状态不管
            iv_listen.setImageDrawable(iv_listen.getContext().getResources().getDrawable(R.mipmap.icon_micro_match_listen));
        }

        @Override
        public void onCompletion() {
            LogUtils.d(TAG, "onCompletion");
            //试听结束,更新试听按钮为试听
            iv_listen.setImageDrawable(iv_listen.getContext().getResources().getDrawable(R.mipmap.icon_micro_match_listen));
//            svgaiv_recordAnim.stopAnimation(true);
            svgaiv_recordAnim.setImageDrawable(oontext.getResources().getDrawable(R.drawable.icon_upload_record_play_default));
        }

        @Override
        public void onInterrupt() {
            LogUtils.d(TAG, "onInterrupt");
            //试听被中断
            iv_listen.setImageDrawable(iv_listen.getContext().getResources().getDrawable(R.mipmap.icon_micro_match_listen));
//            svgaiv_recordAnim.stopAnimation(true);
            svgaiv_recordAnim.setImageDrawable(oontext.getResources().getDrawable(R.drawable.icon_upload_record_play_default));
        }

        @Override
        public void onError(String s) {
            LogUtils.d(TAG, "onError :" + s);
            //试听出错
            iv_listen.setImageDrawable(iv_listen.getContext().getResources().getDrawable(R.mipmap.icon_micro_match_listen));
//            svgaiv_recordAnim.stopAnimation(true);
            svgaiv_recordAnim.setImageDrawable(oontext.getResources().getDrawable(R.drawable.icon_upload_record_play_default));
        }

        @Override
        public void onPlaying(long l) {
            LogUtils.d(TAG, "onPlaying :" + l);
            iv_listen.setImageDrawable(iv_listen.getContext().getResources().getDrawable(R.mipmap.icon_micro_match_listen_pause));
        }
    };

    /**
     * 删除刚才的录音文件
     */
    private void deleteLastRecord(boolean isUpdateUserInfo) {
        //上传完毕更新本地数据
        if (isUpdateUserInfo && !hasRerecorded) {
            UserInfo user = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            if (null != user && !TextUtils.isEmpty(user.getUserVoice()) && !TextUtils.isEmpty(audioUrl) && user.getUserVoice().equals(audioUrl) && user.getVoiceDura() > 0) {
                user.setUserVoice(null);
                user.setVoiceDura(0);
                CoreManager.getCore(IUserCore.class).requestUpdateUserInfo(user);
                audioUrl = null;
            }
        }
        hasRerecorded = false;
        audioRecordStatus = AudioRecordStatus.STATE_RECORD_NORMAL;
        showByState(audioRecordStatus);

        if (null != audioFile && audioFile.exists()) {
            ThreadUtil.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    audioFile.delete();
                    audioFile = null;
                }
            });
        }
    }

    /**
     * 试听刚才的录音文件
     */
    private void tryToListen() {
        if (!audioManager.isPlaying()) {
            if (null != audioFile && audioFile.exists()) {
                LogUtils.d(TAG, "onClick-play audioFilePath: " + audioFile.getPath());
                audioPlayer.setDataSource(audioFile.getPath());
                audioManager.play();
//                if(null != svgaDrawable){
//                    svgaiv_recordAnim.setImageDrawable(svgaDrawable);
//                }
//                svgaiv_recordAnim.startAnimation();
                Glide.with(oontext).asGif().load(R.drawable.upload_record_play_anim).into(svgaiv_recordAnim);
                iv_listen.setImageResource(R.mipmap.icon_micro_match_listen_pause);
            } else if (!TextUtils.isEmpty(audioUrl)) {
                LogUtils.d(TAG, "onClick-play audioUrl: " + audioUrl);
                audioPlayer.setDataSource(audioUrl);
                audioManager.play();
//                if(null != svgaDrawable){
//                    svgaiv_recordAnim.setImageDrawable(svgaDrawable);
//                }
//                svgaiv_recordAnim.startAnimation();
                Glide.with(oontext).asGif().load(R.drawable.upload_record_play_anim).into(svgaiv_recordAnim);
                iv_listen.setImageResource(R.mipmap.icon_micro_match_listen_pause);
            }
        } else {
            audioManager.stopPlay();
//            svgaiv_recordAnim.stopAnimation(true);
            svgaiv_recordAnim.setImageDrawable(oontext.getResources().getDrawable(R.drawable.icon_upload_record_play_default));
            iv_listen.setImageResource(R.mipmap.icon_micro_match_listen);
        }
    }

    @Override
    public void dismiss() {
        LogUtils.d(TAG, "dismiss");
        release();
        CoreManager.removeClient(this);
        onNeedExitRoomListener = null;
        okCancelDialogListener = null;
        super.dismiss();
    }

    private OnNeedExitRoomListener onNeedExitRoomListener;

    /**
     * 资源释放
     */
    private void release() {
        LogUtils.d(TAG, "release");
        if (audioManager != null) {
            //audioManager.release();会停止player播放器并设置listener为null
            audioManager.release();
            if (audioPlayer != null) {
                audioPlayer = null;
            }
            audioManager = null;
        }
//        svgaiv_recordAnim.stopAnimation(true);
    }

    public void setOnNeedExitRoomListener(OnNeedExitRoomListener listener) {
        this.onNeedExitRoomListener = listener;
    }

    private void changeDeleteBtnUsable(boolean usuable) {
        tv_delete.setVisibility(usuable ? View.VISIBLE : View.GONE);
    }

    private void startRecord() {
        if (audioRecordStatus == AudioRecordStatus.STATE_RECORD_RECORDING) {
            SingleToastUtil.showToast("已经在录音...");
            return;
        } else if (audioRecordStatus == AudioRecordStatus.STATE_RECORD_SUCCESS) {
            //重置界面，删除录音文件
            deleteLastRecord(false);
        }
        lastRecordStartTime = System.currentTimeMillis();
        SingleToastUtil.showToast("开始录音");
        tv_record.setText(oontext.getResources().getString(R.string.up_move_to_cancel));
        audioRecordStatus = AudioRecordStatus.STATE_RECORD_RECORDING;
        //开始显示计时
        startChronometer();
        if (null == audioRecorder) {
            audioRecorder = audioManager.getAudioRecorder(oontext, onRecordCallback);
        }
        audioManager.startRecord();
//        if(null != svgaDrawable){
//            svgaiv_recordAnim.setImageDrawable(svgaDrawable);
//        }
//        svgaiv_recordAnim.startAnimation();
        Glide.with(oontext).asGif().load(R.drawable.upload_record_play_anim).into(svgaiv_recordAnim);
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUpload(String url) {
        LogUtils.d(TAG, "onUpload-audioUrl:" + url + " audioDura:" + audioDura);
        audioUrl = url;
        //上传完毕更新本地数据
        if (null != userInfo) {
            userInfo.setUserVoice(audioUrl);
            userInfo.setVoiceDura(audioDura);
            CoreManager.getCore(IUserCore.class).requestUpdateUserInfo(userInfo);
        }
    }


    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUploadFail() {
        SingleToastUtil.showToast("上传失败");
        if (null != dialogManager) {
            dialogManager.dismissDialog();
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfoUpdate(UserInfo userInfo) {
        LogUtils.d(TAG, "onRequestUserInfoUpdate-userInfo:" + userInfo);
//        audioRecordStatus = AudioRecordStatus.STATE_RECORD_NORMAL;
//        showByState(audioRecordStatus);
        if (null != dialogManager) {
            dialogManager.dismissDialog();
        }
        if (isDismissDialogAfterUpdateUserInfo) {
            dismiss();
            isDismissDialogAfterUpdateUserInfo = false;
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfoUpdateError(String error) {
        LogUtils.d(TAG, "onRequestUserInfoUpdateError-error:" + error);
        SingleToastUtil.showToast(error);
        if (null != dialogManager) {
            dialogManager.dismissDialog();
        }
    }

    /**
     * 监听房间冲突事件，提示用户需要先退出房间才能够录音
     */
    public interface OnNeedExitRoomListener {
        void onNeedExitRoom();
    }
}
