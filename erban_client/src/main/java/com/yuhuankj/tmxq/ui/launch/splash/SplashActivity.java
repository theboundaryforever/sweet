package com.yuhuankj.tmxq.ui.launch.splash;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.xchat_core.home.IHomeCoreClient;
import com.tongdaxing.xchat_core.initial.IInitView;
import com.tongdaxing.xchat_core.initial.InitInfo;
import com.tongdaxing.xchat_core.initial.InitPresenter;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.security.ISecurityCore;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.XChatApplication;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.ui.MainActivity;
import com.yuhuankj.tmxq.utils.SplashInitUtils;

import org.json.JSONException;
import org.json.JSONObject;

import cn.jpush.android.api.JPushInterface;
import io.agora.rtc.RtcEngine;

import static com.yuhuankj.tmxq.thirdsdk.jpush.JpushActionBroadcast.JPushExtras;

/**
 * @author xiaoyu
 * @date 2017/12/30
 */
@CreatePresenter(InitPresenter.class)
public class SplashActivity extends BaseMvpActivity<IInitView, InitPresenter> implements IInitView, View.OnClickListener {

    private static final String TAG = SplashActivity.class.getSimpleName();
    private ImageView ivActivity;
    private InitInfo mLocalSplashVo;
    public static long startTime = 0L;
    private final static long MIN_WAIT_TIME_DEFAULT = 0;//默认最小需等待多少毫秒（没有广告图的情况下）
    private final static long MIN_WAIT_TIME_LENGTH = 1500;//默认最小需等待多少毫秒（有广告图的情况下）
    private long minWaitTime = MIN_WAIT_TIME_LENGTH;//最小需等待多少毫秒

    //跳转main的参数
    //如果是点击广告图打开就有以下参数
    private String link = null;
    private int type = 0;
    private int roomType = RoomInfo.ROOMTYPE_HOME_PARTY;

    //如果是推送打开就有以下参数
    private String extras = "";

    //是否已经初始化完成（初始化完成才能跳转到首页）
    private boolean isInitComplete = false;
    //是否广告页最小显示时间完成
    private boolean isAdComplete = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startTime = System.currentTimeMillis();
        LogUtils.d("runTest", "onCreate-startTime:"+startTime+" applicationInitTime:"+(startTime - XChatApplication.appStartTime));
        setContentView(R.layout.activity_splash);
        ivActivity = (ImageView) findViewById(R.id.iv_activity);
        showSplash();
        ivActivity.setOnClickListener(this);
        setSwipeBackEnable(false);
        //解析极光推送sdk附带数据内容
        extras = parseJpushPluginOpenExtra();
        if (TextUtils.isEmpty(extras)) {
            extras = parseJpushOpenExtra();
        }
        LogUtils.d(TAG, " onCreate extras:" + extras);
        if (savedInstanceState != null) {
            // 从堆栈恢复，不再重复解析之前的intent
            setIntent(new Intent());
        }
        long initStartTime = System.currentTimeMillis();
        LogUtils.d("runTest", TAG + " initiate,initStartTime:" + initStartTime);
        initiate();
        LogUtils.d("runTest", TAG + " initiate,total time" + (System.currentTimeMillis() - initStartTime));
        LogUtils.d("runTest", TAG + " rtc sdk version: " + RtcEngine.getSdkVersion());
//        checkFirstOpen();
        isInitComplete = true;
        LogUtils.d("runTest", TAG + " isInitComplete " + isInitComplete);
        LogUtils.d("runTest", TAG + " isAdComplete " + isAdComplete);
        //如果广告页最小显示时间已经完成，则SDK初始化后直接可以进入主页
        if (isAdComplete) {
            nextMain();
        }
    }


    private void checkFirstOpen() {
        //first_open 没有用户暂时
        String first_open_key = "first_open";
        String isFirst = (String) SpUtils.get(this, first_open_key, "");
        if (TextUtils.isEmpty(isFirst)) {
            String first_open_value = "open";
            SpUtils.put(this, first_open_key, first_open_value);
        }
        //安全检测目前仅上报不强杀，因此启动页也没必要检测
        CoreManager.getCore(ISecurityCore.class).securityCheck(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    protected void initiate() {
        if (!SplashInitUtils.getInstance().isHasInited()) {
            SplashInitUtils.getInstance().initIfNeed(getApplicationContext());
            getMvpPresenter().init(true);
        }
    }

    private void showSplash() {
        LogUtils.d("runTest", TAG + " showSplash");
        // 不过期的，并且已经下载出来图片的闪屏页数据
        mLocalSplashVo = getMvpPresenter().getLocalSplashVo();
        if (mLocalSplashVo != null && mLocalSplashVo.getSplashVo() != null
                && !TextUtils.isEmpty(mLocalSplashVo.getSplashVo().getPict())) {
            GlideApp.with(this)
                    .load(mLocalSplashVo.getSplashVo().getPict())
                    .into(ivActivity);
        } else {
            // 其他情况显示的是默认的图片，不用这里设，已经设置style里面了
            minWaitTime = 10;
        }
        try {
            ivActivity.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (isInitComplete) {
                        nextMain();
                    }
                    isAdComplete = true;
                }
            }, minWaitTime);
        } catch (Exception e) {
            if (isInitComplete) {
                nextMain();
            }
            isAdComplete = true;
        }
    }

    private void nextMain() {
        LogUtils.d("runTest", TAG + " nextMain");
        //注意这里不一定是主线程，不要做UI操作，如果要做，先改为保证是主线程
        Intent intent = new Intent();
        if (type != 0 && !TextUtils.isEmpty(link)) {
            intent.putExtra("url", link);
            intent.putExtra("type", type);
            intent.putExtra("roomType", roomType);
        }
        intent.putExtra("extras", extras);
        MainActivity.start(this, intent);
        LogUtils.d("runTest", TAG + " nextMain-->finish,total time1: " + (System.currentTimeMillis() - startTime));
        LogUtils.d("runTest", TAG + " nextMain-->finish,total time2: " + (System.currentTimeMillis() - XChatApplication.appStartTime));
        finish();
    }

    @CoreEvent(coreClientClass = IHomeCoreClient.class)
    public void onGetHomeTabListFail(String error) {
        LogUtils.i(TAG, "启动获取首页Tab失败......");
    }

    @Override
    public void onInitSuccess(InitInfo data) {
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_activity) {
            if (mLocalSplashVo == null || mLocalSplashVo.getSplashVo() == null) {
                return;
            }
            link = mLocalSplashVo.getSplashVo().getLink();
            type = mLocalSplashVo.getSplashVo().getType();
            roomType = mLocalSplashVo.getSplashVo().getRoomType();
            LogUtils.d(TAG, "onClick-link:" + link + "type:" + type + " roomType:" + roomType);
            if (TextUtils.isEmpty(link) || type == 0) {
                return;
            }
            if (isInitComplete) {
                //初始化完成才可以立即跳到下个页面，否则等初始化完成自动跳转就好
                nextMain();
            }
        }
    }

    //----------------------------极光厂商通道推送----------------------------------------

    /**
     * 消息Id
     **/
    private static final String KEY_MSGID = "msg_id";
    /**
     * 该通知的下发通道
     **/
    private static final String KEY_WHICH_PUSH_SDK = "rom_type";
    /**
     * 通知标题
     **/
    private static final String KEY_TITLE = "n_title";
    /**
     * 通知内容
     **/
    private static final String KEY_CONTENT = "n_content";
    /**
     * 通知附加字段
     **/
    private static final String KEY_EXTRAS = "n_extras";

    /**
     * 小米和魅族、VIVO是通过极光回调自己判断如何跳转
     *
     * @return
     */
    private String parseJpushOpenExtra() {
        Intent intent = getIntent();
        if (null != intent && intent.getExtras() != null) {
            extras = intent.getExtras().getString(JPushExtras);
        }
        LogUtils.d(TAG, "parseJpushOpenExtra-extras:" + extras);
        return extras;
    }

    /**
     * 解析极光推送厂商通道打开的情况
     * 华为、oppo、fcm是需要服务端配置uri_Action 和 uri_activity告知客户端如何跳转的,
     * 	配置了，则走厂商通道
     * 	未配置，则走普通通道
     */
    private String parseJpushPluginOpenExtra() {
        LogUtils.d(TAG, "parseJpushPluginOpenExtra");
        /**
         * {
         * 	"msg_id":"123456",
         * 	"n_content":"this is content",
         * 	"n_extras":{"key1":"value1","key2":"value2"},
         * 	"n_title":"this is title",
         * 	"rom_type":0
         *
         * JSON 内容字段说明：
         *
         * 字段|取值类型|描述
         * ---|---|---
         * msg_id|String|通过此key获取到通知的msgid
         * n_title|String|通过此key获取到通知标题
         * n_content|String|通过此key获取到通知内容
         * n_extras|String|通过此key获取到通知附加字段
         * rom_type| byte|通过此key获取到下发通知的平台。
         *      得到值说明：byte类型的整数， 0为极光，1为小米，2为华为，3为魅族，4为oppo，8为FCM。
         * }
         */
        Intent intent = getIntent();
        if (null != intent) {
            String data = null;
            //获取华为平台附带的jpush信息
            if (getIntent().getData() != null) {
                data = getIntent().getData().toString();
            }

            //获取fcm或oppo平台附带的jpush信息
            if (TextUtils.isEmpty(data) && intent.getExtras() != null) {
                data = getIntent().getExtras().getString("JMessageExtra");
            }

            LogUtils.d(TAG, "parseJpushPluginOpenExtra-data:" + data);
            if (TextUtils.isEmpty(data)) {
                return "";
            }
            try {
                JSONObject jsonObject = new JSONObject(data);
                String msgId = jsonObject.optString(KEY_MSGID);
                byte whichPushSDK = (byte) jsonObject.optInt(KEY_WHICH_PUSH_SDK);
                String title = jsonObject.optString(KEY_TITLE);
                String content = jsonObject.optString(KEY_CONTENT);
                String extras = jsonObject.optString(KEY_EXTRAS);
                StringBuilder sb = new StringBuilder();
                sb.append("msgId:");
                sb.append(msgId);
                sb.append("\n");
                sb.append("title:");
                sb.append(title);
                sb.append("\n");
                sb.append("content:");
                sb.append(content);
                sb.append("\n");
                sb.append("extras:");
                sb.append(extras);
                sb.append("\n");
                sb.append("platform:");
                sb.append(getPushSDKName(whichPushSDK));
                LogUtils.d(TAG, "parseJpushPluginOpenExtra-msgContent:" + sb.toString());
                //上报点击事件
                JPushInterface.reportNotificationOpened(this, msgId, whichPushSDK);
                return extras;
            } catch (JSONException e) {
                e.printStackTrace();
                LogUtils.w(TAG, "parse notification error");
            }
        }
        return "";
    }

    private String getPushSDKName(byte whichPushSDK) {
        String name;
        switch (whichPushSDK) {
            case 0:
                name = "jpush";
                break;
            case 1:
                name = "xiaomi";
                break;
            case 2:
                name = "huawei";
                break;
            case 3:
                name = "meizu";
                break;
            case 4:
                name = "oppo";
                break;
            case 8:
                name = "fcm";
                break;
            default:
                name = "jpush";
        }
        return name;
    }
}
