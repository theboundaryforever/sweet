package com.yuhuankj.tmxq.ui.me.noble;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;

import java.util.Map;

/**
 * @author liaoxy
 * @Description:贵族model层
 * @date 2019/3/11 11:23
 */
public class NobleModel {

    //发送贵族广播
    public void sendNoblePublicMsg(String message, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("message", message);
        OkHttpManager.getInstance().postRequest(UriProvider.sendNoblePublicMsg(), params, callBack);
    }

    //获取置顶广播
    public void getNoblePublicMsg(OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().postRequest(UriProvider.getNoblePublicMsg(), params, callBack);
    }
}
