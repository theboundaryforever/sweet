package com.yuhuankj.tmxq.ui.audio.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;

import com.tongdaxing.xchat_core.home.TabInfo;
import com.yuhuankj.tmxq.ui.audio.activity.MusicPlayListFragment;
import com.yuhuankj.tmxq.ui.audio.activity.MusicServerListFragment;

import java.util.List;

/**
 * <p> 我的曲库、热门曲库等 </p>
 * Created by Administrator on 2017/11/15.
 */
public class MusicLibraryAdapter extends FragmentPagerAdapter {
    private SparseArray<Fragment> mFragmentList;
    private List<TabInfo> mTitleList;

    public MusicLibraryAdapter(FragmentManager fm, List<TabInfo> titleList) {
        super(fm);
        this.mTitleList = titleList;
        mFragmentList = new SparseArray<>();
        mFragmentList.put(0, new MusicPlayListFragment());
        mFragmentList.put(1, new MusicServerListFragment());
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mTitleList == null ? 0 : mTitleList.size();
    }
}