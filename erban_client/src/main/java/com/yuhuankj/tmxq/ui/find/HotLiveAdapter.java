package com.yuhuankj.tmxq.ui.find;

import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.find.bean.FindHotRoomEnitity;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

public class HotLiveAdapter extends BaseQuickAdapter<FindHotRoomEnitity, BaseViewHolder> {

    private int imgWidthAndHeight = 0;

    public HotLiveAdapter(List<FindHotRoomEnitity> data) {
        super(R.layout.item_discovery, data);
        int screenW = ScreenUtil.getDisplayWidth();
        imgWidthAndHeight = (screenW - (DisplayUtility.dp2px(mContext, 15) * 2) - (DisplayUtility.dp2px(mContext, 15) * 2)) / 3;
    }

    @Override
    protected void convert(BaseViewHolder helper, FindHotRoomEnitity item) {
        ImageView imvRoom = helper.getView(R.id.imvRoom);
        RelativeLayout.LayoutParams imvIconLp = (RelativeLayout.LayoutParams) imvRoom.getLayoutParams();
        imvIconLp.width = imgWidthAndHeight;
        imvIconLp.height = imgWidthAndHeight;
        imvRoom.setLayoutParams(imvIconLp);

        TextView tvRoomName = helper.getView(R.id.tvRoomName);
        TextView tvOnLineCount = helper.getView(R.id.tvOnLineCount);
        ImageView imvRunning = helper.getView(R.id.imvRunning);
        GlideApp.with(mContext).asGif().load(R.drawable.anim_room_running_white).diskCacheStrategy(DiskCacheStrategy.ALL).into(imvRunning);
        tvRoomName.setText(item.getTitle());
        tvOnLineCount.setText(item.getOnlineNum() + "人");
        ImageLoadUtils.loadRoundImage(mContext,item.getAvatar(), imvRoom, ScreenUtil.dip2px(11),0);

    }
}