package com.yuhuankj.tmxq.ui.liveroom.imroom.room.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.yuhuankj.tmxq.R;

/**
 * 新版房间的密码框输入
 *
 * @author zwk
 * @date 2019/7/5
 */
public class InputPwdDialog extends DialogFragment implements View.OnClickListener {
    private View mRootView;
    private TextView mTvTitle;
    private EditText mInputText;
    private TextView mFailText;
    private TextView mConfirmText;
    private TextView mCancelText;

    private String mTitle;
    private String mOk;
    private String mCancel;
    private boolean showFailText = false;
    private OnDialogBtnClickListener mOnDialogBtnClickListener;

    public static InputPwdDialog newInstance(String title, String okLabel, String cancelLabel, boolean showFailText) {
        InputPwdDialog fragment = new InputPwdDialog();
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putString("okLabel", okLabel);
        bundle.putString("cancelLabel", cancelLabel);
        bundle.putBoolean("showFailText", showFailText);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static InputPwdDialog newInstance(String title, String okLabel, String cancelLabel, String resultCode) {
        InputPwdDialog fragment = new InputPwdDialog();
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putString("okLabel", okLabel);
        bundle.putString("cancelLabel", cancelLabel);
        bundle.putString("resultCode", resultCode);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            mTitle = arguments.getString("title");
            mOk = arguments.getString("okLabel");
            mCancel = arguments.getString("cancelLabel");
            showFailText = arguments.getBoolean("showFailText");
        }
        setCancelable(false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.layout_pic_login_dialog, container);
        mTvTitle = (TextView) mRootView.findViewById(R.id.pic_login_title);
        mInputText = (EditText) mRootView.findViewById(R.id.pic_login_input);
        GrowingIO.getInstance().trackEditText(mInputText);
        mFailText = (TextView) mRootView.findViewById(R.id.pic_login_fail_msg);
        mConfirmText = (TextView) mRootView.findViewById(R.id.btn_confirm);
        mCancelText = (TextView) mRootView.findViewById(R.id.btn_cancel);
        return mRootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mInputText.requestFocus();
        Window window = getDialog().getWindow();
        if (window != null) {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }

        mTvTitle.setText(mTitle);
        mCancelText.setText(mCancel);
        mConfirmText.setText(mOk);
        mFailText.setVisibility(showFailText ? View.VISIBLE : View.GONE);
        mConfirmText.setOnClickListener(this);
        mCancelText.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_confirm:
                if (mOnDialogBtnClickListener != null) {
                    mOnDialogBtnClickListener.onBtnConfirm(mInputText.getText().toString());
                }
                break;
            case R.id.btn_cancel:
                if (mOnDialogBtnClickListener != null) {
                    mOnDialogBtnClickListener.onBtnCancel();
                }
                break;
            default:
        }
    }

    public void setOnDialogBtnClickListener(OnDialogBtnClickListener onDialogBtnClickListener) {
        mOnDialogBtnClickListener = onDialogBtnClickListener;
    }

    @Override
    public int show(FragmentTransaction transaction, String tag) {
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(this, tag).addToBackStack(null);
        return transaction.commitAllowingStateLoss();

    }

    public interface OnDialogBtnClickListener {
        void onBtnConfirm(String inputPwd);

        void onBtnCancel();
    }
}
