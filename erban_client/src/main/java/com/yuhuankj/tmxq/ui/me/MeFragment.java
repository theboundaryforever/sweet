package com.yuhuankj.tmxq.ui.me;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.room.IRoomCoreClient;
import com.tongdaxing.xchat_core.user.bean.MedalBean;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.fragment.BaseMvpFragment;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.MainActivity;
import com.yuhuankj.tmxq.ui.me.wallet.bills.widget.BillItemView;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.widget.LevelView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by MadisonRong on 04/01/2018.
 */
@CreatePresenter(MePresenter.class)
public class MeFragment extends BaseMvpFragment<IMeView, MePresenter>
        implements View.OnClickListener, IMeView {
    public static final String TAG = "MeFragment";

    @BindView(R.id.tv_user_name)
    TextView mTvUserName;
    @BindView(R.id.ivUserMedal)
    ImageView ivUserMedal;
    @BindView(R.id.tv_user_id)
    TextView mTvUserId;
    @BindView(R.id.iv_user_head)
    RoundedImageView mIvUserHead;
    @BindView(R.id.tv_user_attentions)
    TextView mTvUserAttentions;
    @BindView(R.id.tv_user_attention_text)
    TextView mTvUserAttentionText;
    @BindView(R.id.tv_user_fans)
    TextView mTvUserFans;
    @BindView(R.id.tvAdmireNum)
    TextView tvAdmireNum;
    @BindView(R.id.tvAdmireNumTips)
    TextView tvAdmireNumTips;
    @BindView(R.id.tv_user_fan_text)
    TextView mTvFansText;
    @BindView(R.id.bivMyVisitor)
    BillItemView bivMyVisitor;
    @BindView(R.id.me_item_setting)
    BillItemView mMeItemSetting;
    @BindView(R.id.me_item_charge)
    BillItemView mMeItemCharge;
    @BindView(R.id.level_info)
    LevelView mLevelView;
    @BindView(R.id.rlUserInfo)
    View rlUserInfo;

    @BindView(R.id.me_item_level)
    BillItemView mMeItemLevel;
    @BindView(R.id.me_car)
    BillItemView meCar;
    @BindView(R.id.bivInvite)
    BillItemView bivInvite;
    @BindView(R.id.bivRealAuth)
    BillItemView bivRealAuth;
    @BindView(R.id.me_noble)
    BillItemView me_noble;
    Unbinder unbinder;
    @BindView(R.id.tv_authStatus)
    TextView tv_authStatus;
    @BindView(R.id.llWallet)
    LinearLayout llWallet;
    @BindView(R.id.llTask)
    LinearLayout llTask;
    @BindView(R.id.llRoom)
    LinearLayout llRoom;
    @BindView(R.id.llPublicMsg)
    LinearLayout llPublicMsg;
    @BindView(R.id.bivMyMedal)
    BillItemView bivMyMedal;
    @BindView(R.id.tvLevel)
    TextView tvLevel;
    @BindView(R.id.imvMedal1)
    ImageView imvMedal1;
    @BindView(R.id.imvMedal2)
    ImageView imvMedal2;
    @BindView(R.id.tvNewVisitorCount)
    TextView tvNewVisitorCount;
    @BindView(R.id.tvBecomeVip)
    TextView tvBecomeVip;

    @Override
    public void onFindViews() {
    }

    @Override
    public void onSetListener() {
        mTvUserAttentions.setOnClickListener(this);
        mTvUserAttentionText.setOnClickListener(this);
        mTvUserFans.setOnClickListener(this);
        mTvFansText.setOnClickListener(this);
        tvAdmireNum.setOnClickListener(this);
        tvAdmireNumTips.setOnClickListener(this);
        bivInvite.setOnClickListener(this);
        bivRealAuth.setOnClickListener(this);
        rlUserInfo.setOnClickListener(this);
        mIvUserHead.setOnClickListener(this);
        bivMyMedal.setOnClickListener(this);

        bivMyVisitor.setOnClickListener(this);
        mMeItemSetting.setOnClickListener(this);
        mMeItemCharge.setOnClickListener(this);
        mMeItemLevel.setOnClickListener(this);
        meCar.setOnClickListener(this);
        me_noble.setOnClickListener(this);

        llWallet.setOnClickListener(this);
        llTask.setOnClickListener(this);
        llPublicMsg.setOnClickListener(this);
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CoreManager.notifyClients(IRoomCoreClient.class,
                        IRoomCoreClient.METHOD_ON_CREATE_ROOM_CLICKED,
                        CoreManager.getCore(IAuthCore.class).getCurrentUid());
                StatisticManager.get().onEvent(getActivity(),
                        StatisticModel.EVENT_ID_ME_MINE_ROOM,
                        StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
            }
        };
        llRoom.setOnClickListener(onClickListener);
    }

    @Override
    public void initiate() {
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_me;
    }

    @Override
    public void onResume() {
        super.onResume();
        getMvpPresenter().initUserData();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.llPublicMsg) {
            if (getActivity() instanceof MainActivity) {
                ((MainActivity) getActivity()).openFindPage();
            }
            return;
        }

        getMvpPresenter().clickBy(view.getId(), getContext());
    }

    @Override
    public void updateUserInfoUI(UserInfo userInfo) {
        if (userInfo != null) {
            mTvUserName.setText(userInfo.getNick());
            mTvUserId.setText(getString(R.string.me_user_id, userInfo.getErbanNo()));
            ImageLoadUtils.loadAvatar(mContext, userInfo.getAvatar(), mIvUserHead, false);
            mTvUserAttentions.setText(String.valueOf(userInfo.getFollowNum()));
            mTvUserFans.setText(String.valueOf(userInfo.getFansNum()));
            tvAdmireNum.setText(String.valueOf(userInfo.getAdmireNum()));
//            mLevelView.setLevelImageViewHeight(DisplayUtility.dp2px(getActivity(), 15));
//            int charmLevel = userInfo.getCharmLevel();
//            mLevelView.setCharmLevel(charmLevel);
//            int experLevel = userInfo.getExperLevel();
//            mLevelView.setExperLevel(experLevel);
//
//            if (charmLevel > 0 || experLevel > 0) {
//                mLevelView.setVisibility(View.VISIBLE);
//            } else {
//                mLevelView.setVisibility(View.GONE);
//            }

//            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mLevelView.getLayoutParams();

            if (!TextUtils.isEmpty(userInfo.getVipMedal())) {
//                lp.leftMargin = DisplayUtility.dp2px(getActivity(), 16);
//                mLevelView.setLayoutParams(lp);
//                mLevelView.setPadding(DisplayUtility.dp2px(getActivity(), 16), 0,
//                        DisplayUtility.dp2px(getActivity(), 7), 0);

                ivUserMedal.setVisibility(View.VISIBLE);
                GlideApp.with(this).load(userInfo.getVipMedal())
                        .dontAnimate().into(ivUserMedal);
                tvBecomeVip.setVisibility(View.INVISIBLE);

            } else {
//                lp.leftMargin = DisplayUtility.dp2px(getActivity(), 5);
//                mLevelView.setLayoutParams(lp);
//                mLevelView.setPadding(DisplayUtility.dp2px(getActivity(), 5), 0,
//                        DisplayUtility.dp2px(getActivity(), 7), 0);

                ivUserMedal.setVisibility(View.GONE);
                tvBecomeVip.setVisibility(View.VISIBLE);
            }
            tv_authStatus.setText(getResources().getString(userInfo.isRealNameAudit() ?
                    R.string.real_name_auth_status_certified : R.string.real_name_auth_status_uncertified));

            int experLevel = userInfo.getExperLevel();
            tvLevel.setVisibility(View.VISIBLE);
            tvLevel.setText("Lv." + experLevel);

            imvMedal1.setVisibility(View.GONE);
            imvMedal2.setVisibility(View.GONE);
            if (userInfo.getWearList() != null) {
                List<MedalBean> medalBeans = new ArrayList<>();
                for (MedalBean bean : userInfo.getWearList()) {
                    if (bean.getAliasId() > 0) {
                        medalBeans.add(bean);
                    }
                }
                if (medalBeans.size() == 1) {
                    imvMedal1.setVisibility(View.VISIBLE);
                    ImageLoadUtils.loadImage(getContext(), medalBeans.get(0).getAlias(), imvMedal1);
                } else if (medalBeans.size() == 2) {
                    imvMedal1.setVisibility(View.VISIBLE);
                    ImageLoadUtils.loadImage(getContext(), medalBeans.get(0).getAlias(), imvMedal1);
                    imvMedal2.setVisibility(View.VISIBLE);
                    ImageLoadUtils.loadImage(getContext(), medalBeans.get(1).getAlias(), imvMedal2);
                }
            }

//            if (!TextUtils.isEmpty(userInfo.getAlias())) {
//                imvMedal.setVisibility(View.VISIBLE);
//                GlideApp.with(this).load(userInfo.getAlias()).dontAnimate().into(imvMedal);
//            } else {
//                imvMedal.setVisibility(View.GONE);
//            }
        } else {
            mTvUserName.setText("");
            mTvUserId.setText(getString(R.string.me_user_id_null));
            mTvUserAttentions.setText("0");
            mTvUserFans.setText("0");
            tvAdmireNum.setText("0");
            //mLevelView.setVisibility(View.GONE);
//            mLevelView.setExperLevel(0);
//            mLevelView.setCharmLevel(0);
            ivUserMedal.setVisibility(View.GONE);
            tvLevel.setVisibility(View.INVISIBLE);
        }
//        mTvUserName.setOnClickListener(v -> startActivity(new Intent(mContext, KtvRoomListActivity.class)));
//        mLevelView.setOnClickListener(v -> startActivity(new Intent(mContext, AudienceKtvDemoActivity.class)));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    //设置新访客数量
    @Override
    public void setVisitorCount(int count) {
        if (count <= 0) {
            tvNewVisitorCount.setVisibility(View.GONE);
        } else {
            tvNewVisitorCount.setVisibility(View.VISIBLE);
            tvNewVisitorCount.setText("+" + count);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
