package com.yuhuankj.tmxq.ui.quickmating;

import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.xchat_core.im.custom.bean.QuickMatingBean;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.base.fragment.BaseFragment;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.chancemeeting.ChanceMeetingActivity;
import com.yuhuankj.tmxq.ui.me.taskcenter.view.TaskCenterActivity;
import com.yuhuankj.tmxq.ui.quickmating.bean.QMInfoBean;
import com.yuhuankj.tmxq.ui.quickmating.bean.RoomBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class QuickMatingFragment extends BaseFragment {
    private ImageView imvBack;
    private View vQuestion;
    private TextView tvDouziCount;
    private RelativeLayout rlCloseInfo;
    private LinearLayout rlOpenInfo;
    private TextView tvOpen;
    private TextView tvOpenTime;
    private TextView tvOpenStatus;
    private TextView tvTimes;
    private TextView tvMating;
    private TextView tvMatingPoint;
    private SVGAImageView svQuickMating;
    private TextView tvWantContinueMating;
    //性别选择弹窗UI
    private RelativeLayout rlOpen;
    private ImageView imvFilter;

    private QuickMatingModel qmModel;
    private QMInfoBean qmInfoBean;
    private boolean isMating = false;//是否正在匹配中
    private int pointCount = 1;//匹配中动画点的数量
    private RulerDialog rulerDialog;
    private GenderDialog genderDialog;
    private boolean isBuy = false;//是否是买的次数

    @Override
    public int getRootLayoutId() {
        return R.layout.activity_quick_mating;
    }

    @Override
    public void onFindViews() {
        imvBack = mView.findViewById(R.id.imvBack);
        vQuestion = mView.findViewById(R.id.vQuestion);
        tvDouziCount = mView.findViewById(R.id.tvDouziCount);
        tvOpenTime = mView.findViewById(R.id.tvOpenTime);
        tvOpenStatus = mView.findViewById(R.id.tvOpenStatus);
        tvTimes = mView.findViewById(R.id.tvTimes);
        tvMating = mView.findViewById(R.id.tvMating);
        tvMatingPoint = mView.findViewById(R.id.tvMatingPoint);
        rlCloseInfo = mView.findViewById(R.id.rlCloseInfo);
        rlOpenInfo = mView.findViewById(R.id.rlOpenInfo);
        tvOpen = mView.findViewById(R.id.tvOpen);
        svQuickMating = mView.findViewById(R.id.svQuickMating);
        tvWantContinueMating = mView.findViewById(R.id.tvWantContinueMating);

        rlOpen = mView.findViewById(R.id.rlOpen);
        imvFilter = mView.findViewById(R.id.imvFilter);

    }

    @Override
    public void onSetListener() {
        imvBack.setOnClickListener(this);
        vQuestion.setOnClickListener(this);
        tvDouziCount.setOnClickListener(this);
        tvWantContinueMating.setOnClickListener(this);
        rlOpen.setOnClickListener(this);
        imvFilter.setOnClickListener(this);
    }

    @Override
    public void initiate() {
        qmModel = new QuickMatingModel();

        ViewGroup.LayoutParams params = svQuickMating.getLayoutParams();
        params.height = ScreenUtil.getScreenWidth(getActivity()) - ScreenUtil.dip2px(82);
        params.width = ScreenUtil.getScreenWidth(getActivity()) - ScreenUtil.dip2px(82);
        svQuickMating.requestLayout();

        startQMAnimator();
        getQmInfo();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            if (rulerDialog != null && rulerDialog.isShowing()) {
                rulerDialog.dismiss();
            }
            if (genderDialog != null && genderDialog.isShowing()) {
                genderDialog.dismiss();
            }
        }
    }

    //获取交友速配所需基本信息
    private void getQmInfo() {
        qmModel.getQMInfo(new HttpRequestCallBack<QMInfoBean>() {

            @Override
            public void onSuccess(String message, QMInfoBean response) {
                setData(response);
            }

            @Override
            public void onFailure(int code, String msg) {
                toast(msg);
            }
        });
    }

    //开始匹配
    public void startQuickMatingIf(int gender) {
        if (qmInfoBean.getUserFreeMatchCount() > 0) {
            startQuickMating(gender);
            return;
        }
        int buyCostPea = qmInfoBean.getUserMatchConfig().getBuyCostPea();
        int maxBuyCount = qmInfoBean.getUserMatchConfig().getMaxBuyCount();
        String buyTimeTitle = String.format("确定花费%d甜豆？", buyCostPea);
        String buyTimeTip = String.format("%d甜豆=1次机会，每天%d次\n匹配不成功，不扣除甜豆", buyCostPea, maxBuyCount);
        DialogManager dialogManager = new DialogManager(getContext());
        dialogManager.showOkCancelTitleMsgCenterDialog(buyTimeTitle, buyTimeTip,
                Color.parseColor("#FF4F8D"), new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onOk() {
                        isBuy = true;
                        startQuickMating(gender);
                    }
                });

    }

    //开始匹配
    public void startQuickMating(int gender) {
        isMating = true;
        setData(qmInfoBean);
        qmModel.quickMating(gender, new HttpRequestCallBack<RoomBean>() {

            @Override
            public void onSuccess(String message, RoomBean response) {
                getHomeActivity().setRoomBean(response);
                rlOpen.getHandler().removeCallbacksAndMessages(null);
                rlOpen.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isMating) {
                            qmModel.cancelQuickMating(new HttpRequestCallBack<QuickMatingBean>() {
                                @Override
                                public void onSuccess(String message, QuickMatingBean response) {
                                    isMating = false;
                                    rlCloseInfo.setVisibility(View.GONE);
                                    rlOpenInfo.setVisibility(View.VISIBLE);
                                    tvTimes.setVisibility(View.GONE);
                                    tvMating.setVisibility(View.VISIBLE);
                                    tvMatingPoint.setVisibility(View.GONE);
                                    tvDouziCount.setVisibility(View.VISIBLE);
                                    if (getActivity() != null) {
                                        tvWantContinueMating.setVisibility(View.VISIBLE);
                                        tvTimes.setVisibility(View.VISIBLE);
                                        int type = new Random().nextInt(10);
                                        if (type % 2 == 0) {
                                            tvMating.setText("当前在线人数都在匹配中，先去\n别的房间逛逛吧~");
                                            tvOpen.setText("到处逛逛");
                                        } else {
                                            tvMating.setText("当前在线人数都在匹配中，先去\n偶遇几个好声音吧~");
                                            tvOpen.setText("偶遇好声音");
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(int code, String msg) {
                                    toast(msg);
                                }
                            });
                        }
                    }
                }, 10000);
            }

            @Override
            public void onFailure(int code, String msg) {
                if (code == 2802) {//如果已经在匹配中则取消匹配
                    cancelQuickMating();
                }
                toast(msg);
                isMating = false;
                setData(qmInfoBean);
            }
        });
    }

    //取消交友速配
    private void cancelQuickMating() {
        qmModel.cancelQuickMating(new HttpRequestCallBack<QuickMatingBean>() {

            @Override
            public void onSuccess(String message, QuickMatingBean response) {
                isMating = false;
                setData(qmInfoBean);
            }

            @Override
            public void onFailure(int code, String msg) {
                toast(msg);
                isMating = false;
                setData(qmInfoBean);
            }
        });
    }

    //设置页面信息
    private void setData(QMInfoBean qmInfoBean) {
        if (qmInfoBean == null) {
            toast("获取匹配信息失败");
            return;
        }
        tvDouziCount.setText(qmInfoBean.getPeaNum() + "");
        if (qmInfoBean.getUserMatchConfig() == null) {
            toast("获取匹配信息失败");
            return;
        }
        tvWantContinueMating.setVisibility(View.GONE);
        this.qmInfoBean = qmInfoBean;
        if (isMating) {
            tvOpen.setText("取消匹配");
            tvMating.setText("正在为你努力匹配中");
            tvDouziCount.setVisibility(View.INVISIBLE);
            rlCloseInfo.setVisibility(View.GONE);
            rlOpenInfo.setVisibility(View.VISIBLE);
            tvTimes.setVisibility(View.GONE);
            tvMating.setVisibility(View.VISIBLE);
            tvMatingPoint.setVisibility(View.VISIBLE);
            startMatingAnima();
        } else {
            closeMatingAnima();
            tvDouziCount.setVisibility(View.VISIBLE);
            tvTimes.setVisibility(View.VISIBLE);
            tvMating.setVisibility(View.GONE);
            tvMatingPoint.setVisibility(View.GONE);
            rlCloseInfo.setVisibility(View.VISIBLE);
            rlOpenInfo.setVisibility(View.GONE);
            if (qmInfoBean.getIsOpen() == 0) {
                String openTime = String.format("开放时间：%s ~ %s", qmInfoBean.getUserMatchConfig().getOpenTime(), qmInfoBean.getUserMatchConfig().getCloseTime());
                openTime = openTime.replaceAll("-", ":");
                tvOpenTime.setVisibility(View.VISIBLE);
                tvOpenTime.setText(openTime);
                tvOpenStatus.setText("暂未开放");
            } else if (qmInfoBean.getIsOpen() == 1) {
                tvOpenTime.setVisibility(View.GONE);
                tvOpenStatus.setText("开始匹配");
            }
        }
        updateTimes();
    }

    private Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            if (pointCount == 1) {
                tvMatingPoint.setText(".");
                pointCount++;
            } else if (pointCount == 2) {
                tvMatingPoint.setText("..");
                pointCount++;
            } else {
                tvMatingPoint.setText("...");
                pointCount = 1;
            }
            handler.sendEmptyMessageDelayed(1, 300);
        }
    };

    //启动正在匹配中动画
    private void startMatingAnima() {
        handler.removeMessages(1);
        handler.sendEmptyMessage(1);
    }

    private void closeMatingAnima() {
        handler.removeMessages(1);
        tvMatingPoint.setText("...");
    }

    public void updateTimes() {
        int hasMatchCount = qmInfoBean.getUserFreeMatchCount();
//        if (hasMatchCount <= 0) {
//            hasMatchCount = qmInfoBean.getUserBuyMatchCount();
//        }
        String matchCountStr = hasMatchCount + "";
        SpannableString spannableString = new SpannableString("今日剩余" + matchCountStr + "次");
        spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#FFFD33")), 4, 4 + matchCountStr.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvTimes.setText(spannableString);
    }

    //更新甜豆数量
    private void updateDouziCount(int peaNum) {
        if (qmInfoBean == null || getActivity() == null) {
            return;
        }
        int douziCount = qmInfoBean.getPeaNum() + peaNum;
        if (douziCount < 0) {
            douziCount = 0;
        }
        qmInfoBean.setPeaNum(douziCount);
        ((QuickMatingHomeActivity) getActivity()).updateDouziCount(qmInfoBean);
    }

    //更新甜豆数量
    public void updateDouziCount() {
        if (qmInfoBean == null) {
            return;
        }
        tvDouziCount.setText(qmInfoBean.getPeaNum() + "");
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view == imvBack) {
            if (getActivity() != null) {
                getActivity().finish();
            }
        } else if (view == vQuestion) {
            //统计-交友速配-规则
            StatisticManager.get().onEvent(getContext(),
                    StatisticModel.EVENT_ID_CLICK_QM_RULER,
                    StatisticModel.getInstance().getUMAnalyCommonMap(getContext()));
            //服务端有就用服务端的，没有就用本地写死的
            if (qmInfoBean != null && qmInfoBean.getRuleList() != null) {
                rulerDialog = new RulerDialog(getContext());
                rulerDialog.setData(qmInfoBean.getRuleList());
                rulerDialog.show();
                return;
            }
            List<String> data = new ArrayList<>();
            data.add("开放时间：每天19:00~次日2:00");
            data.add("每天系统会赠送3次连麦机会，使用完赠送次数后可以用甜豆购买(50甜豆=1次机会)");
            data.add("每次匹配免费限时聊天4分钟，如果双方都关注了对方，就可以无限畅聊");
            data.add("若对方并没有关注你，你又不想结束聊天，可以用甜豆续费哦");
            rulerDialog = new RulerDialog(getContext());
            rulerDialog.setData(data);
            rulerDialog.show();
        } else if (view == imvFilter) {
            genderDialog = new GenderDialog(getContext(), getHomeActivity().gender);
            genderDialog.setOnGenderListener(new GenderDialog.OnGenderListener() {
                @Override
                public void onGender(int gender) {
                    getHomeActivity().gender = gender;
                }
            });
            genderDialog.show();
        } else if (view == rlOpen) {
            if (isMating) {
                //统计-交友速配-取消匹配
                StatisticManager.get().onEvent(getContext(),
                        StatisticModel.EVENT_ID_CLICK_QM_CANCEL_MATCHING,
                        StatisticModel.getInstance().getUMAnalyCommonMap(getContext()));
                cancelQuickMating();
                return;
            }
            if (AvRoomDataManager.get().mCurrentRoomInfo != null) {
                toast("在房间内无法进行速配功能");
                return;
            }
            if (tvOpen.isShown()) {
                String openStr = tvOpen.getText().toString();
                if (openStr.equals("到处逛逛")) {
                    if (getActivity() == null) {
                        return;
                    }
                    //统计-交友速配-到处逛逛
                    StatisticManager.get().onEvent(getContext(),
                            StatisticModel.EVENT_ID_CLICK_QM_WALK_AROUND,
                            StatisticModel.getInstance().getUMAnalyCommonMap(getContext()));
                    ((QuickMatingHomeActivity) getActivity()).randomEnterRoom();
                } else if (openStr.equals("偶遇好声音")) {
                    //统计-交友速配-偶遇好声音
                    StatisticManager.get().onEvent(getContext(),
                            StatisticModel.EVENT_ID_CLICK_QM_CHANGE_MEETING,
                            StatisticModel.getInstance().getUMAnalyCommonMap(getContext()));
                    startActivity(new Intent(getContext(), ChanceMeetingActivity.class));
                }
            } else {
                if (qmInfoBean != null && qmInfoBean.getIsOpen() == 1) {
                    //统计-交友速配-开始匹配
                    StatisticManager.get().onEvent(getContext(),
                            StatisticModel.EVENT_ID_CLICK_QM_START_MATCHING,
                            StatisticModel.getInstance().getUMAnalyCommonMap(getContext()));
                    startQuickMatingIf(getHomeActivity().gender);
                }
            }
        } else if (view == tvDouziCount) {
            startActivity(new Intent(getContext(), TaskCenterActivity.class));
        } else if (view == tvWantContinueMating) {
            //统计-交友速配-想要继续匹配
            StatisticManager.get().onEvent(getContext(),
                    StatisticModel.EVENT_ID_CLICK_QM_WANT_CONTINUE_MATCHING,
                    StatisticModel.getInstance().getUMAnalyCommonMap(getContext()));
            startQuickMatingIf(getHomeActivity().gender);
        }
    }

    //进入连麦页
    public void enterLinkMicro(QuickMatingBean quickMatingBean) {
        if (getActivity() == null) {
            return;
        }
        if (isBuy) {
            //减去豆子和匹配次数
            int buyPeaCount = qmInfoBean.getUserMatchConfig().getBuyCostPea();
            updateDouziCount(-buyPeaCount);
            getHomeActivity().updateTimes(qmInfoBean, -1);
        }
        isMating = false;
        setData(qmInfoBean);
        ((QuickMatingHomeActivity) getActivity()).showQuickMatingLinking(qmInfoBean, quickMatingBean);
    }

    //开启顶部的匹配动画
    protected void startQMAnimator() {
        if (getActivity() == null) {
            return;
        }
        new SVGAParser(getActivity()).parse("quickmatinglinking.svga", new SVGAParser.ParseCompletion() {
            @Override
            public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {
                svQuickMating.setVisibility(View.VISIBLE);
                SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);
                svQuickMating.setImageDrawable(drawable);
                svQuickMating.startAnimation();
            }

            @Override
            public void onError() {

            }
        });
        ImageView imvLiuxing1 = mView.findViewById(R.id.imvLiuxing1);
        ImageView imvLiuxing2 = mView.findViewById(R.id.imvLiuxing2);
        ImageView imvLiuxing3 = mView.findViewById(R.id.imvLiuxing3);
        TranslateAnimation translate1 = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 1, Animation.RELATIVE_TO_SELF,
                -10F, Animation.RELATIVE_TO_SELF, -1f,
                Animation.RELATIVE_TO_SELF, 12f);
        translate1.setDuration(2300);
        translate1.setRepeatCount(-1);
        TranslateAnimation translate2 = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF,
                -10F, Animation.RELATIVE_TO_SELF, -1f,
                Animation.RELATIVE_TO_SELF, 12f);
        translate2.setDuration(2300);
        translate2.setRepeatCount(-1);
        TranslateAnimation translate3 = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 1, Animation.RELATIVE_TO_SELF,
                -10F, Animation.RELATIVE_TO_SELF, -1f,
                Animation.RELATIVE_TO_SELF, 12f);
        translate3.setDuration(2800);
        translate3.setRepeatCount(-1);
        imvLiuxing1.startAnimation(translate1);
        imvLiuxing1.setVisibility(View.VISIBLE);
        imvLiuxing2.postDelayed(new Runnable() {
            @Override
            public void run() {
                imvLiuxing2.startAnimation(translate2);
                imvLiuxing2.setVisibility(View.VISIBLE);
            }
        }, 1000);
        imvLiuxing3.postDelayed(new Runnable() {
            @Override
            public void run() {
                imvLiuxing3.startAnimation(translate3);
                imvLiuxing3.setVisibility(View.VISIBLE);
            }
        }, 2000);
    }

    public QuickMatingHomeActivity getHomeActivity() {
        return ((QuickMatingHomeActivity) getActivity());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
            handler = null;
        }
    }
}
