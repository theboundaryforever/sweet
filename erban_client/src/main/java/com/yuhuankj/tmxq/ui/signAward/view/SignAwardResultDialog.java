package com.yuhuankj.tmxq.ui.signAward.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.noober.background.view.BLTextView;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.ui.signAward.model.SignAwardResult;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import butterknife.ButterKnife;

/**
 * 连续签到天数弹框
 *
 * @author weihaitao
 * @date 2019/5/22
 */
public class SignAwardResultDialog extends BaseMvpActivity
        implements View.OnClickListener, IMvpBaseView {


    private ImageView mIvPkgRecv;
    private BLTextView bltvTitle;
    private TextView mTvTips;
    private ImageView mIvClose;

    private SignAwardResult result;

    public static void start(Context context, SignAwardResult result) {
        Intent intent = new Intent(context, SignAwardResultDialog.class);
        intent.putExtra("result", result);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_award_result);
        ButterKnife.bind(this);
        initData();
        initView();
        setListener();
    }

    private void initData() {
        Intent intent = getIntent();
        if (intent.hasExtra("result")) {
            result = (SignAwardResult) intent.getSerializableExtra("result");
        }
        if (null == result) {
            finish();
        }
    }

    private void initView() {
        mIvPkgRecv = (ImageView) findViewById(R.id.ivPkgRecv);
        bltvTitle = (BLTextView) findViewById(R.id.bltvTitle);
        mTvTips = (TextView) findViewById(R.id.tvTips);
        mIvClose = (ImageView) findViewById(R.id.ivClose);

        bltvTitle.setText(getResources().getString(R.string.sign_award_result_title, result.getSignName()));
        mTvTips.setText(result.getSignDesc());
        int imgSize = DisplayUtility.dp2px(this, 235);
        ImageLoadUtils.loadImage(this, ImageLoadUtils.toThumbnailUrl(imgSize, imgSize, result.getSignPic()), mIvPkgRecv);
    }

    private void setListener() {
        mIvClose.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivClose:
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}
