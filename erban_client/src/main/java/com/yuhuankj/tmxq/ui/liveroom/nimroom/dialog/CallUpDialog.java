package com.yuhuankj.tmxq.ui.liveroom.nimroom.dialog;

import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomEvent;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.BaseAppDialog;
import com.yuhuankj.tmxq.constant.StatisticModel;

import java.util.Map;

import static com.yuhuankj.tmxq.base.dialog.AppDialogConstant.STYLE_CENTER_BACK_DARK;

/**
 * @author liaoxy
 * @Description:召集令弹窗
 * @date 2019/7/1 11:42
 */
public class CallUpDialog extends BaseAppDialog implements View.OnClickListener {

    private final String TAG = CallUpDialog.class.getSimpleName();

    private EditText edtContent;
    private TextView tvCancel;
    private TextView tvSend;
    private TextView tvCallUpCountTips;
    private long roomId;
    private int times;
    long sy = 0L;

    private TextWatcher textWatcher;

    public CallUpDialog(Context context, long roomId) {
        super(context);
        this.roomId = roomId;
    }

    @Override
    public int onDialogStyle() {
        return STYLE_CENTER_BACK_DARK;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_room_callup;
    }

    @Override
    protected void initView() {
        edtContent = findViewById(R.id.edtContent);
        GrowingIO.getInstance().trackEditText(edtContent);
        tvCancel = findViewById(R.id.tvCancel);
        tvCallUpCountTips = findViewById(R.id.tvCallUpCountTips);
        tvSend = findViewById(R.id.tvSend);
        edtContent.setSelection(edtContent.getText().toString().length());
    }

    @Override
    public void initListener() {
        tvCancel.setOnClickListener(this);
        tvSend.setOnClickListener(this);
        RoomInfo roomInfo = BaseRoomServiceScheduler.getCurrentRoomInfo();
        if (null == roomInfo || roomInfo.getType() == RoomInfo.ROOMTYPE_SINGLE_AUDIO) {
            dismiss();
            return;
        }
        if (roomInfo.getType() == RoomInfo.ROOMTYPE_HOME_PARTY) {
            times = AvRoomDataManager.get().mServiceRoominfo.getConveneCount();
        } else if (roomInfo.getType() == RoomInfo.ROOMTYPE_MULTI_AUDIO) {
            times = RoomDataManager.get().conveneCount;
        }

        if (times < 0) {
            times = 0;
        }
        refreshCallUpCount();
        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                edtContent.removeTextChangedListener(textWatcher);
                String str = edtContent.getText().toString();
                if (!TextUtils.isEmpty(str)) {
                    str = str.replaceAll("\n", "").replaceAll(" ", "");
                    edtContent.setText(str);
                    edtContent.setSelection(edtContent.getText().length());
                    edtContent.addTextChangedListener(textWatcher);
                }
            }
        };
        edtContent.addTextChangedListener(textWatcher);
    }

    private void refreshCallUpCount() {
        LogUtils.d(TAG, "refreshCallUpCount-times:" + times);
        String roomCallUpNumStr = String.valueOf(times);
        String tips = getContext().getResources().getString(R.string.room_call_up_tips_7, roomCallUpNumStr);
        SpannableStringBuilder ssb = new SpannableStringBuilder(tips);
        int firstIndex = tips.indexOf(roomCallUpNumStr);
        ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#CCCCCC")), 0, firstIndex, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#4FD7B6")),
                firstIndex, firstIndex + roomCallUpNumStr.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#CCCCCC")),
                firstIndex + roomCallUpNumStr.length(), tips.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        tvCallUpCountTips.setText(ssb);
    }

    @Override
    public void initViewState() {
        if (getWindow() != null) {
            getWindow().getAttributes().width = (int) (ScreenUtil.getScreenWidth(getContext()) * 0.86f);
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    @Override
    public void onClick(View v) {
        if (v == tvSend) {
            String content = edtContent.getText().toString();
            if (TextUtils.isEmpty(content)) {
                SingleToastUtil.showToast("请输入召集内容");
                return;
            }
            sendCallup(roomId, content, new HttpRequestCallBack<Integer>() {
                @Override
                public void onSuccess(String message, Integer response) {
                    LogUtils.d(TAG, "sendCallup-->onSuccess message:" + message + " response:" + response);
                    try {
                        SingleToastUtil.showToast("发送成功");

                        RoomInfo roomInfo = BaseRoomServiceScheduler.getCurrentRoomInfo();
                        if (null == roomInfo || roomInfo.getType() == RoomInfo.ROOMTYPE_SINGLE_AUDIO) {
                            dismiss();
                            return;
                        }
                        if (roomInfo.getType() == RoomInfo.ROOMTYPE_HOME_PARTY) {
                            AvRoomDataManager.get().mServiceRoominfo.setConveneCount(response);
                        } else if (roomInfo.getType() == RoomInfo.ROOMTYPE_MULTI_AUDIO) {
                            //召集令剩余使用次数
                            RoomDataManager.get().conveneCount = response;
                            //切换召集令入口控件显示状态
                            RoomDataManager.get().conveneUserCount = 0L;
                            RoomDataManager.get().conveneState = 1;
                            IMRoomMessageManager.get().getIMRoomEventObservable().onNext(new IMRoomEvent()
                                    .setEvent(IMRoomEvent.ROOM_CALL_UP_NUM_NOTIFY));
                        }
                        //交友方，召集令成功发布，统计
                        if (null != getContext()) {
                            StatisticManager.get().onEvent(getContext(),
                                    StatisticModel.EVENT_ID_ROOM_CALL_UP_SEND_SUC,
                                    StatisticModel.getInstance().getUMAnalyCommonMap(getContext()));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int code, String msg) {
                    if (!TextUtils.isEmpty(msg)) {
                        SingleToastUtil.showToast(msg);
                    } else {
                        SingleToastUtil.showToast("发送召集令失败");
                    }

                }
            });
        }
        dismiss();
    }

    //发送召集令
    public void sendCallup(long roomId, String context, HttpRequestCallBack<Integer> callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomId", String.valueOf(roomId));
        params.put("context", context);
        OkHttpManager.getInstance().postRequest(UriProvider.sendCallup(), params, callBack);
    }
}
