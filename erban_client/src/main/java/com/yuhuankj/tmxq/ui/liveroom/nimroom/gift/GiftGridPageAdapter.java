package com.yuhuankj.tmxq.ui.liveroom.nimroom.gift;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.juxiao.library_utils.DisplayUtils;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.yuhuankj.tmxq.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 文件描述：礼物列表分页布局页面
 *
 * @auther：zwk
 * @data：2019/5/10
 */
public class GiftGridPageAdapter extends RecyclerView.Adapter<GiftGridPageAdapter.GiftHolder> {
    private Context mContext;
    private List<GiftInfo> mDatas;
    public static int currentSelect = 0;
    private int screenWith = 0;

    public GiftGridPageAdapter(Context mContext) {
        this.mContext = mContext;
        if (this.mDatas == null) {
            this.mDatas = new ArrayList<>();
        }
        screenWith = DisplayUtils.getScreenWidth(mContext);
    }

    public void setmDatas(List<GiftInfo> mDatas) {
        this.mDatas = mDatas;
        notifyDataSetChanged();
    }

    public List<GiftInfo> getmDatas() {
        return mDatas;
    }

    @NonNull
    @Override
    public GiftHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        GiftHolder holder = new GiftHolder(LayoutInflater.from(mContext).inflate(R.layout.item_gift_page_list, viewGroup, false));
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull GiftHolder giftHolder, int i) {
        GiftListItemAdapter adapter = new GiftListItemAdapter(mContext, mDatas, i);
        giftHolder.rvGiftPageItem.setAdapter(adapter);
        adapter.setOnItemClickListener(new GiftListItemAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position, GiftInfo giftInfo) {
                if (mOnItemClickListener != null) {
                    currentSelect = position;
                    mOnItemClickListener.onItemClick(giftInfo,position);
                    notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return (int) Math.ceil(mDatas.size() * 1.0 / GiftListItemAdapter.PAGE_SIZE);
    }

    public void setIndex(int position) {
        currentSelect = position;
    }

    public void setGiftInfoList(List<GiftInfo> giftInfoList) {
        this.mDatas = giftInfoList;
        notifyDataSetChanged();
    }

    class GiftHolder extends RecyclerView.ViewHolder {
        private RecyclerView rvGiftPageItem;

        public GiftHolder(@NonNull View itemView) {
            super(itemView);
            rvGiftPageItem = itemView.findViewById(R.id.rv_gift_page_list);
        }
    }

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }


    public interface OnItemClickListener {
        void onItemClick(GiftInfo giftInfo,int position);
    }
}
