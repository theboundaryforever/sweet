package com.yuhuankj.tmxq.ui.nim.sysmsg;

import java.io.Serializable;

public class SysMsgEnitity implements Serializable {
    private String    avatar;//": "https://img.pinjin88.com/Fl51AJoW45VOLX4A07yY4fbrnLxD?imageslim",
    private long    createTime;//": 1557217542000,
    private long    fUid;//": 93000746,
    private int    id;//": 3,
    private int    level;//": 2,
    private String    levelName;//": "情深挚友",
    private String    message;//": "xxx想和你成为情深挚友挚友～！",
    private String    nick;//":
    private int    status;//": 0未处理,1同意 2不同意
    private long    uid;//": 93000753

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public long getfUid() {
        return fUid;
    }

    public void setfUid(long fUid) {
        this.fUid = fUid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }
}
