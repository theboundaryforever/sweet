package com.yuhuankj.tmxq.ui.chancemeeting;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.jph.takephoto.app.TakePhoto;
import com.jph.takephoto.app.TakePhotoActivity;
import com.jph.takephoto.app.TakePhotoImpl;
import com.jph.takephoto.compress.CompressConfig;
import com.jph.takephoto.model.CropOptions;
import com.jph.takephoto.model.TResult;
import com.jph.takephoto.permission.TakePhotoInvocationHandler;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.libcommon.audio.AudioPlayer;
import com.tongdaxing.erban.libcommon.audio.OnPlayListener;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.file.JXFileUtils;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.audio.AudioPlayAndRecordManager;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.file.IFileCore;
import com.tongdaxing.xchat_core.file.IFileCoreClient;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.permission.PermissionActivity;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.match.activity.UploadRecordDialog;
import com.yuhuankj.tmxq.ui.user.other.ShowPhotoActivity;
import com.yuhuankj.tmxq.ui.webview.VoiceAuthCardWebViewActivity;
import com.yuhuankj.tmxq.widget.RoundImageView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 我的声音编辑界面
 *
 * @author weihaitao
 * @date 2019/1/4
 */
@CreatePresenter(EditVoicePresenter.class)
public class EditVoiceActivity extends TakePhotoActivity<EditVoiceView, EditVoicePresenter>
        implements EditVoiceView, View.OnClickListener, UploadRecordDialog.OnNeedExitRoomListener {

    public static final String TAG = EditVoiceActivity.class.getSimpleName();
    private static final String CAMERA_PREFIX = "voicebg_";
    PermissionActivity.CheckPermListener checkPermissionListener = new PermissionActivity.CheckPermListener() {
        @Override
        public void superPermission() {
            takePhoto();
        }
    };
    private UserInfo userInfo;
    private String lastUploadImgUrl = null;
    private TextView tvSave;
    //添加我的声音背景图
    private View rlAddVoiceBg;
    private ImageView ivAddVoiceBg;
    //已上传我的声音背景图
    private RoundImageView rivVoiceBg;
    private TextView tvUpdateVoiceBg;
    private View rlImgInfo;
    //我的音色
    private TextView tvVoiceType;
    //我的录音
    private TextView tvMyoice;
    private View vButtomDiv;
    private View llListenerVoice;
    private ImageView ivVoicePlayStatus;
    OnPlayListener onPlayListener = new OnPlayListener() {

        @Override
        public void onPrepared() {
            LogUtils.d(TAG, "onPrepared");
            //准备开始试听,图标状态不管
            ivVoicePlayStatus.setImageDrawable(getResources().getDrawable(R.mipmap.icon_userinfo_voice_pause));
        }

        @Override
        public void onCompletion() {
            LogUtils.d(TAG, "onCompletion");
            //试听结束,更新试听按钮为试听
            ivVoicePlayStatus.setImageDrawable(getResources().getDrawable(R.mipmap.icon_userinfo_voice_play));
        }

        @Override
        public void onInterrupt() {
            LogUtils.d(TAG, "onInterrupt");
            //试听被中断
            ivVoicePlayStatus.setImageDrawable(getResources().getDrawable(R.mipmap.icon_userinfo_voice_play));
        }

        @Override
        public void onError(String s) {
            LogUtils.d(TAG, "onError :" + s);
            //试听出错
            ivVoicePlayStatus.setImageDrawable(getResources().getDrawable(R.mipmap.icon_userinfo_voice_play));
        }

        @Override
        public void onPlaying(long l) {
            LogUtils.d(TAG, "onPlaying :" + l);

        }
    };
    private TextView tvVoicePlayDur;
    private int imgWidth = 0;
    private UploadRecordDialog uploadRecordDialog;
    private AudioPlayAndRecordManager audioManager;
    private AudioPlayer audioPlayer;

    private boolean isVoiceBgUploading = false;

    public static void start(Context context) {
        context.startActivity(new Intent(context, EditVoiceActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtils.d(TAG, "onCreate");
        setContentView(R.layout.activity_edit_voice);
        onFindViews();
        onSetListener();
        userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        refreshVoiceCardView();
    }

    private void onFindViews() {
        LogUtils.d(TAG, "onFindViews");
        initTitleBar(getResources().getString(R.string.edit));
        mTitleBar.setTitleColor(Color.parseColor("#1A1A1A"));
        mTitleBar.setLeftImageResource(R.drawable.icon_arrow_left_black);
        mTitleBar.setBackgroundColor(Color.TRANSPARENT);
        mTitleBar.setImmersive(false);
        tvSave = (TextView) findViewById(R.id.tvSave);
        tvSave.setOnClickListener(this);

        //添加我的声音背景图
        rlAddVoiceBg = findViewById(R.id.rlAddVoiceBg);
        ivAddVoiceBg = (ImageView) findViewById(R.id.ivAddVoiceBg);

        //已上传我的声音背景图
        rivVoiceBg = (RoundImageView) findViewById(R.id.rivVoiceBg);
        imgWidth = DisplayUtility.getScreenWidth(this) - DisplayUtility.dp2px(this, 44);
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) rivVoiceBg.getLayoutParams();
        lp.width = imgWidth;
        lp.height = imgWidth;
        rivVoiceBg.setLayoutParams(lp);

        tvUpdateVoiceBg = (TextView) findViewById(R.id.tvUpdateVoiceBg);
        rlImgInfo = findViewById(R.id.rlImgInfo);

        //我的音色
        tvVoiceType = (TextView) findViewById(R.id.tvVoiceType);

        //我的录音
        tvMyoice = (TextView) findViewById(R.id.tvMyoice);
        vButtomDiv = findViewById(R.id.vButtomDiv);
        llListenerVoice = findViewById(R.id.llListenerVoice);
        ivVoicePlayStatus = (ImageView) findViewById(R.id.ivVoicePlayStatus);
        tvVoicePlayDur = (TextView) findViewById(R.id.tvVoicePlayDur);
    }

    private void onSetListener() {
        LogUtils.d(TAG, "onSetListener");
        ivAddVoiceBg.setOnClickListener(this);
        findViewById(R.id.tvUpdateVoiceBg).setOnClickListener(this);
        tvMyoice.setOnClickListener(this);
        ivVoicePlayStatus.setOnClickListener(this);
        tvVoiceType.setOnClickListener(this);
        rivVoiceBg.setOnClickListener(this);
    }

    private void refreshVoiceCardView() {
        if (null != userInfo) {
            if (!TextUtils.isEmpty(lastUploadImgUrl)) {
                //2.用户上传过了我的声音背景图
                rlAddVoiceBg.setVisibility(View.GONE);
                rlImgInfo.setVisibility(View.VISIBLE);

                GlideApp.with(this)
                        .load(lastUploadImgUrl)
//                        .placeholder(getResources().getDrawable(R.drawable.icon_follow_user_empty))
                        .error(getResources().getDrawable(R.drawable.icon_follow_user_empty))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .dontAnimate()
                        .into(rivVoiceBg);
            } else if (!TextUtils.isEmpty(userInfo.getBackground())) {
                //2.用户上传过了我的声音背景图
                rlAddVoiceBg.setVisibility(View.GONE);
                rlImgInfo.setVisibility(View.VISIBLE);

                GlideApp.with(this)
                        .load(userInfo.getBackground())
                        .placeholder(getResources().getDrawable(R.drawable.icon_follow_user_empty))
                        .error(getResources().getDrawable(R.drawable.icon_follow_user_empty))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .dontAnimate()
                        .into(rivVoiceBg);
            } else {
                //1.用户未上传我的声音背景图
                rlAddVoiceBg.setVisibility(View.VISIBLE);
                rlImgInfo.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(userInfo.getUserVoice()) && userInfo.getVoiceDura() > 0) {
                //3.用户上传过录音
                tvMyoice.setText(null);

                //刷新录音数据
                llListenerVoice.setVisibility(View.VISIBLE);
                vButtomDiv.setVisibility(View.GONE);

                int dura = userInfo.getVoiceDura();
                String duraStr = null;
                if (dura >= 10) {
                    duraStr = "00:".concat(String.valueOf(dura));
                } else if (dura > 0) {
                    duraStr = "00:0".concat(String.valueOf(dura));
                }
                tvVoicePlayDur.setText(duraStr);
            } else {
                //4.用户未上传过录音
                tvMyoice.setText(getResources().getString(R.string.edit_my_voice_un_add));

                llListenerVoice.setVisibility(View.GONE);
                vButtomDiv.setVisibility(View.VISIBLE);
                tvVoicePlayDur.setText(null);
            }

            //音色同偶遇我的声音是两个独立的业务流程，需要区分开来
            if (TextUtils.isEmpty(userInfo.getTimbre())) {
                tvVoiceType.setText(getResources().getString(R.string.edit_void_type_unknow));
                tvVoiceType.setTextColor(Color.parseColor("#CECECE"));
            } else {
                tvVoiceType.setText(userInfo.getTimbre());
                tvVoiceType.setTextColor(Color.parseColor("#1CC9A4"));
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        releaseAudioRes();
        LogUtils.d(TAG, "onDestroy");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rivVoiceBg:
                if (null != userInfo) {
                    Intent intent = new Intent(this, ShowPhotoActivity.class);
                    intent.putExtra("position", 0);
                    intent.putExtra("userId", userInfo.getUid());
                    intent.putExtra("showUserPhotoOrVoiceBg", false);
                    startActivity(intent);
                }
                break;
            case R.id.tvVoiceType:
                //声音编辑页-声鉴卡
                Map<String, String> maps = StatisticModel.getInstance().getUMAnalyCommonMap(this);
                StatisticManager.get().onEvent(this, StatisticModel.EVENT_ID_VOICEEDIT_ENTER_VOICEAUTHCARD, maps);
                VoiceAuthCardWebViewActivity.start(this, false);
                break;
            case R.id.tvMyoice:
                showUploadRecordDialog();
                break;
            case R.id.ivVoicePlayStatus:
                tryToListen();
                break;
            case R.id.tvUpdateVoiceBg:
            case R.id.ivAddVoiceBg:
                uploadNewVoiceBg();
                break;
            case R.id.tvSave:
                saveVoiceInfo();
                break;
            default:
                break;
        }
    }

    private void saveVoiceInfo() {

        if (null == userInfo) {
            return;
        }

        if (TextUtils.isEmpty(lastUploadImgUrl) && TextUtils.isEmpty(userInfo.getBackground()) &&
                (TextUtils.isEmpty(userInfo.getUserVoice()) || userInfo.getVoiceDura() <= 0)) {
            toast(R.string.edit_voice_save_tips3);
            return;
        }

        if (TextUtils.isEmpty(lastUploadImgUrl) && TextUtils.isEmpty(userInfo.getBackground())) {
            toast(R.string.edit_voice_save_tips1);
            return;
        }
        if (TextUtils.isEmpty(userInfo.getUserVoice()) || userInfo.getVoiceDura() <= 0) {
            toast(R.string.edit_voice_save_tips2);
            return;
        }

        //上传成功，更新房间背景
        if (!TextUtils.isEmpty(lastUploadImgUrl)) {
            getDialogManager().showProgressDialog(this, getResources().getString(R.string.network_loading));
            getMvpPresenter().updateUserVoiceBg(lastUploadImgUrl);
        } else {
            finish();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        final UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        boolean result = null != userInfo && (TextUtils.isEmpty(userInfo.getUserVoice()) || userInfo.getVoiceDura() <= 0);
        LogUtils.d(TAG, "onResume result:" + result);

    }

    @Override
    public void onPause() {
        super.onPause();
        LogUtils.d(TAG, "onPause");
        if (audioManager != null) {
            //audioManager.release();会停止player播放器并设置listener为null
            audioManager.release();
        }
    }

    //----------------------上传声音背景图------------------------------
    private void uploadNewVoiceBg() {
        ButtonItem buttonItem = new ButtonItem("拍照上传", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                checkPermissionAndStartCamera();
            }
        });
        ButtonItem buttonItem1 = new ButtonItem("本地相册", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                String mCameraCapturingName = CAMERA_PREFIX + System.currentTimeMillis() + ".jpg";
                File cameraOutFile = JXFileUtils.getTempFile(EditVoiceActivity.this, mCameraCapturingName);
                if (!cameraOutFile.getParentFile().exists()) {
                    cameraOutFile.getParentFile().mkdirs();
                }
                Uri uri = Uri.fromFile(cameraOutFile);
                CompressConfig compressConfig = new CompressConfig.Builder().create();
                getTakePhoto().onEnableCompress(compressConfig, true);
                CropOptions options = new CropOptions.Builder().setWithOwnCrop(true).create();
                getTakePhoto().onPickFromGalleryWithCrop(uri, options);
            }
        });
        List<ButtonItem> buttonItems = new ArrayList<>();
        buttonItems.add(buttonItem);
        buttonItems.add(buttonItem1);
        getDialogManager().showCommonPopupDialog(buttonItems, "取消", false);
    }

    /**
     * 获取TakePhoto实例
     *
     * @return
     */
    @Override
    public TakePhoto getTakePhoto() {
        if (takePhoto == null) {
            takePhoto = (TakePhoto) TakePhotoInvocationHandler.of(this).bind(new TakePhotoImpl(this, this));
        }
        return takePhoto;
    }

    private void checkPermissionAndStartCamera() {
        //低版本授权检查
        checkPermission(checkPermissionListener, R.string.ask_camera, android.Manifest.permission.CAMERA);
    }

    private void takePhoto() {
        String mCameraCapturingName = CAMERA_PREFIX + System.currentTimeMillis() + ".jpg";
        File cameraOutFile = JXFileUtils.getTempFile(this, mCameraCapturingName);
        if (!cameraOutFile.getParentFile().exists()) {
            cameraOutFile.getParentFile().mkdirs();
        }
        Uri uri = Uri.fromFile(cameraOutFile);
        CompressConfig compressConfig = new CompressConfig.Builder().create();
        getTakePhoto().onEnableCompress(compressConfig, false);
        CropOptions options = new CropOptions.Builder().setWithOwnCrop(true).create();
        getTakePhoto().onPickFromCaptureWithCrop(uri, options);
    }

    @Override
    public void takeSuccess(TResult result) {
        getDialogManager().showProgressDialog(this, getResources().getString(R.string.network_loading));
        isVoiceBgUploading = true;
        CoreManager.getCore(IFileCore.class).upload(new File(result.getImage().getCompressPath()));
    }

    @Override
    public void takeFail(TResult result, String msg) {
        getDialogManager().dismissDialog();
        if (!TextUtils.isEmpty(msg)) {
            toast(msg);
        }
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUpload(String url) {
        LogUtils.d(TAG, "onUpload-url:" + url + " isVoiceBgUploading:" + isVoiceBgUploading);
        if (!isVoiceBgUploading) {
            LogUtils.d(TAG, "onUpload-上传录音弹框的上传操作触发回调，忽略");
            return;
        }
        isVoiceBgUploading = false;
        lastUploadImgUrl = url;
        if (!TextUtils.isEmpty(lastUploadImgUrl)) {
            GlideApp.with(this)
                    .load(lastUploadImgUrl)
//                        .placeholder(getResources().getDrawable(R.drawable.icon_follow_user_empty))
                    .error(getResources().getDrawable(R.drawable.icon_follow_user_empty))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontAnimate()
                    .into(rivVoiceBg);
        } else if (null != userInfo && !TextUtils.isEmpty(userInfo.getBackground())) {
            GlideApp.with(this)
                    .load(lastUploadImgUrl)
                    .error(getResources().getDrawable(R.drawable.icon_follow_user_empty))
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(rivVoiceBg);
        }

        rlAddVoiceBg.setVisibility(View.GONE);
        rlImgInfo.setVisibility(View.VISIBLE);
        getDialogManager().dismissDialog();
    }

    @Override
    public void onVoiceBgUploaded(boolean isSuccess, String message) {
        LogUtils.d(TAG, "onVoiceBgUploaded-isSuccess:" + isSuccess + " message:" + message);
        if (isSuccess) {
            getDialogManager().dismissDialog();
            CoreManager.getCore(IUserCore.class).requestUserInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            toast(R.string.edit_voice_save_tips4);
            finish();
        } else {
            getDialogManager().dismissDialog();
            if (!TextUtils.isEmpty(message)) {
                toast(message);
            }
        }
    }

    //------------------------播放我的声音---------------------------
    private void showUploadRecordDialog() {
        if (null == uploadRecordDialog) {
            uploadRecordDialog = new UploadRecordDialog(this);
            uploadRecordDialog.setOnNeedExitRoomListener(this);
        }
        uploadRecordDialog.show();
    }

    @Override
    public void onNeedExitRoom() {
        LogUtils.d(TAG, "onNeedExitRoom");
        getMvpPresenter().exitRoom();
    }

    private void releaseAudioRes() {
        if (audioManager != null) {
            //audioManager.release();会停止player播放器并设置listener为null
            audioManager.release();
            if (audioPlayer != null) {
                audioPlayer = null;
            }
            audioManager = null;
        }
    }

    /**
     * 试听刚才的录音文件
     */
    private void tryToListen() {
        if (null == audioManager) {
            audioManager = AudioPlayAndRecordManager.getInstance();
        }

        if (!audioManager.isPlaying()) {
            if (null != audioPlayer) {
                audioPlayer.stop();
                audioPlayer.setOnPlayListener(null);
                audioPlayer = null;
            }
            if (null != userInfo) {
                //-->修复 Use of stream types is deprecated for operations other than volume control，
                // See the documentation of setAudioStreamType() for what to use
                // instead with android.media.AudioAttributes to qualify your playback use case
                // 也即 "点击 录音时长 弹出上传录音界面，再点击其它区域 关闭 上传页面，然后点击 播放 按钮  —— 一直播放，无法停止"--->
                audioPlayer = audioManager.getAudioPlayer(this, null, onPlayListener);
                //<---修复代码分割线，实际含义为每个界面每次播放都要停止上次的audioPlayer并重新初始化保证场景唯一--
                audioPlayer.setDataSource(userInfo.getUserVoice());
                audioManager.play();
            }
        } else {
            audioManager.stopPlay();
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo info) {
        LogUtils.d(TAG, "onCurrentUserInfoUpdate-info:" + info);
        if (null != userInfo && info.getUid() == userInfo.getUid()) {
            userInfo = info;
            refreshVoiceCardView();
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfo(UserInfo info) {
        LogUtils.d(TAG, "onRequestUserInfo-info:" + info);
        if (null != userInfo && info.getUid() == userInfo.getUid()) {
            userInfo = info;
            refreshVoiceCardView();
        }
    }
}
