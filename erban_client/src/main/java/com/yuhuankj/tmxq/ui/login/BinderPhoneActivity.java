package com.yuhuankj.tmxq.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.growingio.android.sdk.collection.GrowingIO;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.erban.libcommon.utils.toast.ToastCompat;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.ui.user.password.ModifyPwdActivity;
import com.yuhuankj.tmxq.ui.user.password.SetingPwdActivity;

import java.text.NumberFormat;

/*
    copy绑定支付宝页面，控件ID没有改变 详情请看布局
 */
public class BinderPhoneActivity extends BaseActivity {
    private EditText etAlipayAccount;
    private EditText etSmsCode;
    private Button btnGetCode;
    private Button btnBinder;
    private Button btnBinderRquest;
    private TextWatcher textWatcher;

    public static final String hasBand = "hasBand";
    public static final int modifyBand = 1;
    private boolean isModifyBand = false;
    private TextInputLayout mTilPhone;
    private TextInputLayout mTilSms;
    private int mHasBandType;
    private CodeDownTimer mTimer;
    private String mSubmitPhone;
    //绑定手机后的跳转页面
    private int type = 0;//1:登录密码 2：支付密码
    private UserInfo userInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent() == null) {
            SingleToastUtil.showToast("传入数据为空");
            return;
        }
        userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        mHasBandType = getIntent().getIntExtra(hasBand, 0);
        type = getIntent().getIntExtra("type", 0);
        if (mHasBandType == modifyBand) {
            isModifyBand = true;
        }
        setContentView(R.layout.activity_binder_phone);
        initTitleBar(isModifyBand ? "更换绑定手机号码" : "绑定手机号码");
        initView();
        initData();
        setListener();

        if (mHasBandType == modifyBand) {
            if (userInfo != null && !TextUtils.isEmpty(userInfo.getPhone())) {
                etAlipayAccount.setText(userInfo.getPhone());
                etAlipayAccount.setEnabled(false);
            }
            setMode(isModifyBand);
        }
    }

    private void initView() {
        mTilPhone = (TextInputLayout) findViewById(R.id.til_phone);
        mTilSms = (TextInputLayout) findViewById(R.id.til_sms);
        etAlipayAccount = (EditText) findViewById(R.id.et_phone);
        GrowingIO.getInstance().trackEditText(etAlipayAccount);
        etSmsCode = (EditText) findViewById(R.id.et_smscode);
        GrowingIO.getInstance().trackEditText(etSmsCode);
        btnGetCode = (Button) findViewById(R.id.btn_get_code);
        btnBinder = (Button) findViewById(R.id.btn_binder);
        btnBinderRquest = (Button) findViewById(R.id.btn_binder_request);
    }

    private void setMode(boolean isModifyBand) {
        mTilPhone.setHint(isModifyBand ? "请输入已绑定的手机号码" : "请输入新绑定的手机号码");
        mTitleBar.setTitle(isModifyBand ? "更换绑定手机号码" : "绑定手机号码");
        btnBinder.setText(isModifyBand ? "确认替换" : "确认绑定");
    }

    private void initData() {

    }

    private void setListener() {
        //获取绑定手机验证码
        btnGetCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = etAlipayAccount.getText().toString().trim();
                Number parse = null;
                try {
                    parse = NumberFormat.getIntegerInstance().parse(phoneNumber);
                } catch (Exception e) {
                    Log.e("bind phone", e.getMessage(), e);
                }
                if (parse == null || parse.intValue() == 0 || phoneNumber.length() != 11) {
                    ToastCompat.makeText(BasicConfig.INSTANCE.getAppContext(), "请输入正确的手机号码", ToastCompat.LENGTH_SHORT).show();
                    return;
                }
                mTimer = new CodeDownTimer(btnGetCode, 60000, 1000);
                mTimer.start();

                if (mHasBandType == modifyBand) {
                    if (isModifyBand) {
                        //3是绑定新的手机
                        CoreManager.getCore(IAuthCore.class).getModifyPhoneSMSCode(phoneNumber, "3");
                    } else {
                        //2是验证旧的手机
                        CoreManager.getCore(IAuthCore.class).getModifyPhoneSMSCode(phoneNumber, "2");
                    }
                } else {
                    CoreManager.getCore(IAuthCore.class).getSMSCode(phoneNumber);
                }
            }
        });

        //请求绑定手机号码
        btnBinderRquest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSubmitPhone = etAlipayAccount.getText().toString();
                getDialogManager().showProgressDialog(BinderPhoneActivity.this, "正在验证请稍后...");
                if (mHasBandType == modifyBand) {
                    if (isModifyBand) {
                        CoreManager.getCore(IAuthCore.class).ModifyBinderPhone(CoreManager.getCore(IAuthCore.class).getCurrentUid(),
                                mSubmitPhone, etSmsCode.getText().toString(), UriProvider.modifyBinderPhone());
                    } else {
                        CoreManager.getCore(IAuthCore.class).ModifyBinderPhone(CoreManager.getCore(IAuthCore.class).getCurrentUid(),
                                mSubmitPhone, etSmsCode.getText().toString(), UriProvider.modifyBinderNewPhone());
                    }

                } else {
                    CoreManager.getCore(IAuthCore.class).BinderPhone(CoreManager.getCore(IAuthCore.class).getCurrentUid(),
                            mSubmitPhone, etSmsCode.getText().toString()
                    );
                }

            }
        });
        //输入框监听改变
        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (etAlipayAccount.getText() != null && etAlipayAccount.getText().length() > 0
                        && etSmsCode.getText() != null && etSmsCode.getText().length() > 0) {
                    btnBinder.setVisibility(View.GONE);
                    btnBinderRquest.setVisibility(View.VISIBLE);
                } else {
                    btnBinder.setVisibility(View.VISIBLE);
                    btnBinderRquest.setVisibility(View.GONE);
                }
            }
        };

        etAlipayAccount.addTextChangedListener(textWatcher);
        etSmsCode.addTextChangedListener(textWatcher);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != mTimer) {
            mTimer.cancel();
            mTimer = null;
        }
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onBinderPhone() {
        getDialogManager().dismissDialog();
        if (!TextUtils.isEmpty(mSubmitPhone)) {
            CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().setPhone(mSubmitPhone);
        }
        toast("绑定成功");
        if (userInfo != null) {
            userInfo.setBindPhone(true);
        }
        if (type != 0 && userInfo != null) {
            Intent intent = null;
            if (type == 2) {
                if (userInfo.isPayPassword()) {
                    intent = new Intent(this, ModifyPwdActivity.class);
                } else {
                    intent = new Intent(this, SetingPwdActivity.class);
                }
            } else {
                if (userInfo.isPassword()) {
                    intent = new Intent(this, ModifyPwdActivity.class);
                } else {
                    intent = new Intent(this, SetingPwdActivity.class);
                }
            }
            intent.putExtra("type", type);
            startActivity(intent);
        }
        finish();
    }


    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onBinderPhoneFail(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onMoidfyOnBiner() {
        if (!TextUtils.isEmpty(mSubmitPhone)) {
            CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().setPhone(mSubmitPhone);
        }
        getDialogManager().dismissDialog();
        if (isModifyBand) {
            isModifyBand = false;
            setMode(false);
            etSmsCode.setText("");
            etAlipayAccount.setText("");
            etAlipayAccount.setEnabled(true);
            etAlipayAccount.setFocusable(true);
            etAlipayAccount.requestFocus();
            if (null != mTimer) {
                mTimer.cancel();
                mTimer.onFinish();
            }
        } else {
            toast("绑定成功");
            finish();
        }


    }


    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onMoidfyOnBinerFail(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onGetSMSCodeFail(String error) {
        if (null != mTimer) {
            mTimer.cancel();
            mTimer.onFinish();
        }
        toast(error);
    }
}
