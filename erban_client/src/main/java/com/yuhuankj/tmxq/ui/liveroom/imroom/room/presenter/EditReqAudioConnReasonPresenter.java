package com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter;

import android.text.TextUtils;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomModel;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IEditReqAudioConnReasonView;

/**
 * 房间页面的presenter
 */
public class EditReqAudioConnReasonPresenter<I extends IMvpBaseView> extends AbstractMvpPresenter<IEditReqAudioConnReasonView> {

    private final String TAG = EditReqAudioConnReasonPresenter.class.getSimpleName();

    private IMRoomModel imRoomModel;

    public EditReqAudioConnReasonPresenter() {
        imRoomModel = new IMRoomModel();
    }

    public void editAudioConnReqReason(String reason) {
        LogUtils.d(TAG, "editAudioConnReqReason reason:" + reason);
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (null == roomInfo) {
            return;
        }
        imRoomModel.editAudioConnReqReason(reason, roomInfo.getRoomId(), new HttpRequestCallBack<String>() {
            @Override
            public void onSuccess(String message, String response) {
                LogUtils.d(TAG, "editAudioConnReqReason-->onSuccess message:" + message + " response:" + response);
                if (null != getMvpView()) {
                    //提示
                    if (TextUtils.isEmpty(response)) {
                        getMvpView().toast(response);
                    }
                    //刷新列表
                    getMvpView().finishForResult();
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                LogUtils.d(TAG, "editAudioConnReqReason-->onFailure code:" + code + " msg:" + msg);
                if (null == getMvpView()) {
                    return;
                }

                if (!TextUtils.isEmpty(msg)) {
                    getMvpView().toast(msg);
                }
            }
        });
    }

}
