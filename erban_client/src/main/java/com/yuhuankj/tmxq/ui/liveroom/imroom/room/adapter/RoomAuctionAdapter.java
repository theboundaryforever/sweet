package com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.room.auction.AuctionUserBean;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.listener.OnRoomAuctionClickListener;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 文件描述：新的竞拍房间的竞拍位adapter
 *
 * @auther：zwk
 * @data：2019/6/6
 */
public class RoomAuctionAdapter extends RecyclerView.Adapter<RoomAuctionAdapter.AuctionHolder> {
    private Context mContext;
    private List<AuctionUserBean> datas;
    private OnRoomAuctionClickListener onRoomAuctionClickListener;

    public RoomAuctionAdapter(Context mContext) {
        this.mContext = mContext;
        this.datas = new ArrayList<>();
    }

    @NonNull
    @Override
    public AuctionHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new AuctionHolder(LayoutInflater.from(mContext).inflate(R.layout.item_rv_auciton_position, viewGroup, false));
    }

    @Override
    public int getItemCount() {
        if (RoomDataManager.get().getAuction() == null || ListUtils.isListEmpty(RoomDataManager.get().getAuction().getList())) {
            return 1;
        } else {
            if (RoomDataManager.get().getAuction().getList().size() <= 3) {
                return RoomDataManager.get().getAuction().getList().size();
            } else {
                return 3;
            }
        }
    }


    @Override
    public void onBindViewHolder(@NonNull AuctionHolder holder, int position) {
        if (RoomDataManager.get().getAuction() == null || ListUtils.isListEmpty(RoomDataManager.get().getAuction().getList())) {
            holder.ivAuctionAvatar.setImageResource(R.drawable.ic_auction_position_mic);
            holder.ivAuctionLogo.setImageResource(R.drawable.ic_auction_position_0);
            holder.ivAuctionGender.setVisibility(View.GONE);
            holder.tvAuctionGoldNum.setVisibility(View.INVISIBLE);
            holder.tvAuctionNick.setText("首席竞拍位");
            holder.rlAuction.setOnClickListener(v -> {
                if (onRoomAuctionClickListener!=null) {
                    onRoomAuctionClickListener.onAucitonUserClick(null);
                }
            });
        } else {
            AuctionUserBean auctionUserBean = null;//位置控制 -- 2 1 3（第一名中间 两边分别是第二名和第三名）
            int realPosition = position;
            if (RoomDataManager.get().getAuction().getList().size()>=3){
                if (position == 0) {
                    realPosition = 1;
                    auctionUserBean = RoomDataManager.get().getAuction().getList().get(1);
                }else if (position == 1){
                    realPosition = 0;
                    auctionUserBean = RoomDataManager.get().getAuction().getList().get(0);
                }else {
                    realPosition = 2;
                    auctionUserBean = RoomDataManager.get().getAuction().getList().get(2);
                }
            }else {
                auctionUserBean = RoomDataManager.get().getAuction().getList().get(position);
            }
            if (auctionUserBean == null) {
                holder.ivAuctionAvatar.setImageResource(R.drawable.ic_auction_position_mic);
                holder.ivAuctionLogo.setImageResource(R.drawable.ic_auction_position_0);
                holder.ivAuctionGender.setVisibility(View.GONE);
                holder.tvAuctionGoldNum.setVisibility(View.INVISIBLE);
                holder.tvAuctionNick.setText("首席竞拍位");
            } else {
                ImageLoadUtils.loadCircleImage(mContext, auctionUserBean.getAvatar(), holder.ivAuctionAvatar, R.drawable.ic_default_avatar);
                if (realPosition == 0) {
                    holder.ivAuctionLogo.setImageResource(R.drawable.ic_auction_position_1);
                } else if (realPosition == 1) {
                    holder.ivAuctionLogo.setImageResource(R.drawable.ic_auction_position_2);
                } else {
                    holder.ivAuctionLogo.setImageResource(R.drawable.ic_auction_position_3);
                }
                holder.ivAuctionGender.setVisibility(View.VISIBLE);
                holder.ivAuctionGender.setImageResource(auctionUserBean.getGender() == 1 ? R.drawable.icon_man : R.drawable.icon_female);
                holder.tvAuctionNick.setText(StringUtils.limitStr(mContext,auctionUserBean.getNick(),4));
                holder.tvAuctionGoldNum.setVisibility(View.VISIBLE);
                holder.tvAuctionGoldNum.setText(auctionUserBean.getGoldNum() + "金币");
            }
            AuctionUserBean finalAuctionUserBean = auctionUserBean;
            holder.rlAuction.setOnClickListener(v -> {
                if (onRoomAuctionClickListener!=null) {
                    onRoomAuctionClickListener.onAucitonUserClick(finalAuctionUserBean);
                }
            });
        }

    }

    public void setOnRoomAuctionClickListener(OnRoomAuctionClickListener onRoomAuctionClickListener) {
        this.onRoomAuctionClickListener = onRoomAuctionClickListener;
    }

    public void setDatas(List<AuctionUserBean> datas) {
        this.datas = datas;
        notifyDataSetChanged();
    }

    public void addDatas(AuctionUserBean data) {
        if (data == null) {
            return;
        }
        int size = this.datas.size();
        this.datas.add(data);
        if (size > 0) {
            notifyItemChanged(size - 1, 1);
        } else {
            notifyDataSetChanged();
        }
    }

    class AuctionHolder extends RecyclerView.ViewHolder {
        private RelativeLayout rlAuction;
        private ImageView ivAuctionAvatar;
        private ImageView ivAuctionLogo;
        private ImageView ivAuctionGender;
        private TextView tvAuctionNick;
        private TextView tvAuctionGoldNum;


        public AuctionHolder(@NonNull View itemView) {
            super(itemView);
            rlAuction = itemView.findViewById(R.id.rl_room_auction_position);
            ivAuctionAvatar = itemView.findViewById(R.id.iv_room_auction_avatar);
            ivAuctionLogo = itemView.findViewById(R.id.iv_room_auction_logo);
            ivAuctionGender = itemView.findViewById(R.id.iv_room_auction_gender);
            tvAuctionNick = itemView.findViewById(R.id.tv_room_auction_nick);
            tvAuctionGoldNum = itemView.findViewById(R.id.tv_room_auction_gold_num);
        }
    }
}
