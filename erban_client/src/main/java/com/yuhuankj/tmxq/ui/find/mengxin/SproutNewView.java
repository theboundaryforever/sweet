package com.yuhuankj.tmxq.ui.find.mengxin;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

import java.util.List;

public interface SproutNewView<T extends AbstractMvpPresenter> extends IMvpBaseView {


    /**
     * 萌新 接口回调
     *
     * @param success
     * @param message
     * @param rooms
     */
    void onGetSproutNewList(boolean success, String message, List<T> rooms, int currPageNum);
}
