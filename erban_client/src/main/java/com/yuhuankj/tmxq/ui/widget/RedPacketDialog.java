package com.yuhuankj.tmxq.ui.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tongdaxing.xchat_core.redpacket.bean.RedPacketInfoV2;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.utils.UIHelper;

/**
 * @author chenran
 * @date 2017/10/4
 */

public class RedPacketDialog extends BaseActivity implements View.OnClickListener{

    private ImageView openRed;
    private ImageView imgClose;
    private ImageView checkDetail;
    private TextView redMoney;
    private TextView source;
    private RelativeLayout openRedPacket;
    private RelativeLayout checkRedPacket;
    private RedPacketInfoV2 redPacketInfo;

    private ObjectAnimator mObjectAnimator;

    public static void start(Context context, RedPacketInfoV2 redPacketInfoV2) {
        Intent intent = new Intent(context, RedPacketDialog.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("redPacketInfo", redPacketInfoV2);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.red_packet_dialog);
        redPacketInfo = (RedPacketInfoV2) getIntent().getSerializableExtra("redPacketInfo");
        initView();
        setListener();
    }

    private void setListener() {
        imgClose.setOnClickListener(this);
        openRed.setOnClickListener(this);
        checkDetail.setOnClickListener(this);
    }

    private void initView() {
        openRedPacket = (RelativeLayout) findViewById(R.id.open_red_packet);
        checkRedPacket = (RelativeLayout) findViewById(R.id.check_red_packet);
        openRed = (ImageView) findViewById(R.id.img_open);
        imgClose = (ImageView) findViewById(R.id.img_close);
        redMoney = (TextView) findViewById(R.id.tv_red_money);
        checkDetail = (ImageView) findViewById(R.id.tv_look);
        source = (TextView) findViewById(R.id.source);

        source.setText("收到" + redPacketInfo.getPacketName() + "红包");
        redMoney.setText(String.valueOf(redPacketInfo.getPacketNum()));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_open:
                startRedPacketAnim(v);
                break;
            case R.id.img_close:
                finish();
                break;
            case R.id.tv_look:
                UIHelper.showWalletAct(this);
                finish();
                break;
            default:
        }
    }

    private void startRedPacketAnim(View v) {
        v.setOnClickListener(null);
        mObjectAnimator = ObjectAnimator.ofFloat(v, "rotationX", 0, 360, 720, 1080, 1540, 1900);
        mObjectAnimator.setDuration(1000);
        mObjectAnimator.setInterpolator(new AccelerateInterpolator());
        mObjectAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                openRedPacket.setVisibility(View.INVISIBLE);
                checkRedPacket.setVisibility(View.VISIBLE);
            }
        });
        mObjectAnimator.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mObjectAnimator != null) {
            if (mObjectAnimator.isRunning()) {
                mObjectAnimator.cancel();
            }
            mObjectAnimator = null;
        }
    }
}
