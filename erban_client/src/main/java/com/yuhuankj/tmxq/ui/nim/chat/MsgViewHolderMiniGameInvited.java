package com.yuhuankj.tmxq.ui.nim.chat;

import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.netease.nim.uikit.common.ui.recyclerview.adapter.BaseMultiItemFetchLoadAdapter;
import com.netease.nim.uikit.session.viewholder.MsgViewHolderBase;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.xchat_core.bean.MiniGameInvitedInfo;
import com.tongdaxing.xchat_core.im.custom.bean.MiniGameInvitedAttachment;
import com.tongdaxing.xchat_core.minigame.MiniGameModel;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 小游戏邀请通知
 */
public class MsgViewHolderMiniGameInvited extends MsgViewHolderBase {
    private String TAG = "MsgViewHolderMiniGameInvited";
    private ImageView imvGame;
    private TextView tvGameName;
    private TextView tvDownTimer;
    private TextView tvStatusTip;

    private TextView tvRefuse;
    private TextView tvAgree;

    private MiniGameModel miniGameModel;
    private DialogManager dialogManager;

    private MiniGameInvitedInfo invitedInfo;

    private boolean isReceive = true;
    //0:等待接受 1,超时，2，被拒绝，3,被取消，4，同意，5，失效
    public static final int WATING = 0;
    public static final int TIME_OUT = 1;
    public static final int REFUSE = 2;
    public static final int CANCEL = 3;
    public static final int AGREE = 4;
    public static final int DISABLE = 5;
    //0:等待接受 1,超时，2，被拒绝，3,被取消，4，同意，5，游戏完成，6，失效
    private static Map<Long, Integer> mapMsgStatus = new HashMap<>();
    private static Map<Long, WeakReference<MsgViewHolderMiniGameInvited>> mapMsg = new HashMap<>();
    private static Map<Long, WeakReference<MsgViewHolderMiniGameInvited>> mapInvitationMsg = new HashMap<>();
    private static Map<Long, Long> mapMsgReceiveTime = new HashMap<>();//记录下消息收到时的时间
    private static List<Long> removeInvitationList = new ArrayList<>();//需要删除的邀请列表
    private static int KEY_TIME = R.id.tv_time;
    private static int MSG_UPDATE_TIMER = 1;//更新倒计时消息

    private static final int DURING = 60000;//倒计时60秒

    //本机时间和网络时间的差值,如果本机时间不正确，用来矫正时间
    public static long deviceTimeOffset = 0;
    private static Thread timeThread;//获取网络时间的子线程

    //获取本机时间和网络时间的差值
    public static void getDeviceTimeOffset() {
        if (timeThread != null && timeThread.isAlive()) {
            return;
        }
        timeThread = new Thread(new Runnable() {
            @Override
            public void run() {
                String webUrl = "https://www.baidu.com/";
                try {
                    URL url = new URL(webUrl);
                    URLConnection uc = url.openConnection();
                    uc.setReadTimeout(5000);
                    uc.setConnectTimeout(5000);
                    uc.connect();
                    long correctTime = uc.getDate();
                    if (correctTime > 0) {
                        long localTime = System.currentTimeMillis();
                        deviceTimeOffset = correctTime - localTime;
                    }
                } catch (Exception e) {
                    timeThread = null;
                    //e.printStackTrace();
                }
            }
        });
        timeThread.start();
    }

    //获取当前时间
    public static long getCurTimeMillis() {
        return System.currentTimeMillis() + deviceTimeOffset;
    }

    public static void stop() {
        if (timeThread != null) {
            try {
                timeThread.interrupt();
                timeThread = null;
            } catch (Exception e) {
                // e.printStackTrace();
            }
        }
    }


    public static void clear() {
        mapMsgStatus.clear();
        mapMsg.clear();
        mapInvitationMsg.clear();
    }

    public static void addMsgTime(long key, long time) {
        mapMsgReceiveTime.put(key, time);
    }

    public static void continueTimer() {
        handler.removeMessages(MSG_UPDATE_TIMER);
        handler.sendEmptyMessageDelayed(MSG_UPDATE_TIMER, 1000);
    }

    public static void pauseTimer() {
        handler.removeMessages(MSG_UPDATE_TIMER);
    }

    private static Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            if (!mapMsg.isEmpty()) {
                for (Map.Entry<Long, WeakReference<MsgViewHolderMiniGameInvited>> map : mapMsg.entrySet()) {
                    try {
                        if (!removeInvitationList.contains(map.getKey())) {
                            map.getValue().get().updateTimer();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (removeInvitationList != null && removeInvitationList.size() > 0) {
                    for (long key : removeInvitationList) {
                        mapMsg.remove(key);
                    }
                    removeInvitationList.clear();
                }
                handler.removeMessages(MSG_UPDATE_TIMER);
                handler.sendEmptyMessageDelayed(MSG_UPDATE_TIMER, 1000);
            } else {
                handler.removeMessages(MSG_UPDATE_TIMER);
            }
        }
    };

    public MsgViewHolderMiniGameInvited(BaseMultiItemFetchLoadAdapter adapter) {
        super(adapter);
        miniGameModel = new MiniGameModel();
    }

    @Override
    protected int getContentResId() {
        return R.layout.view_minigame_chat;
    }

    @Override
    protected void inflateContentView() {
        imvGame = findViewById(R.id.imvGame);
        tvGameName = findViewById(R.id.tvGameName);
        tvDownTimer = findViewById(R.id.tvDownTimer);
        tvStatusTip = findViewById(R.id.tvStatusTip);

        tvRefuse = findViewById(R.id.tvRefuse);
        tvAgree = findViewById(R.id.tvAgree);
    }

    //更新倒计时
    public void updateTimer() {
        if (tvDownTimer.getTag() != null && (long) tvDownTimer.getTag() == message.getTime()) {
            long curTime = System.currentTimeMillis() + deviceTimeOffset;
            if (curTime - message.getTime() >= DURING) {
                setEndStatus(TIME_OUT, "超时", "失效");
                return;
            }
            int KEY_TIME = (int) message.getTime();
            long surplus = -1;
            if (tvDownTimer.getTag(KEY_TIME) == null) {
//                long lastTime = message.getTime();
//                if (mapMsgReceiveTime.containsKey(lastTime)) {
//                    long offsetTime = System.currentTimeMillis() - mapMsgReceiveTime.get(lastTime);
//                    lastTime += offsetTime;
//                }
                long lastTime = curTime;
                surplus = lastTime - message.getTime();
                surplus -= (surplus % 1000);
                surplus = (DURING - surplus) / 1000;//剩余时长
                String curText = tvDownTimer.getText().toString();
                if (curText.contains("60") && surplus == 60) {
                    surplus = 59;
                }
                tvDownTimer.setTag(KEY_TIME, surplus);
            } else {
                surplus = (long) tvDownTimer.getTag(KEY_TIME);
                tvDownTimer.setTag(KEY_TIME, surplus - 1);
                surplus -= 1;
            }
            if (surplus <= 0) {
                setEndStatus(TIME_OUT, "超时", "失效");
            } else {
                if (surplus <= 60) {
                    tvDownTimer.setText(surplus + "s");
                } else {
                    setEndStatus(TIME_OUT, "超时", "失效");
                }
            }
        }
    }

    public static boolean isExistInvit(long roomId) {
        return mapInvitationMsg.containsKey(roomId);
    }

    public static MsgViewHolderMiniGameInvited get(long roomId) {
        if (mapMsg == null || mapMsg.get(roomId) == null || mapMsg.get(roomId).get() == null) {
            return null;
        }
        return mapMsg.get(roomId).get();
    }

    public long getTime() {
        return message.getTime();
    }


    //设置邀请消息结束时显示
    public void setEndStatus(int status, String statusTip, String topTip) {
        mapMsgStatus.put(invitedInfo.getRoomid(), status);
        SpUtils.put(context, invitedInfo.getRoomid() + "", status);
        if (status != WATING) {
            //添加到移除列表
            removeInvitationList.add(invitedInfo.getRoomid());
            //mapMsg.remove(invitedInfo.getRoomid());
            mapMsgReceiveTime.remove(invitedInfo.getRoomid());
        }
        tvAgree.setVisibility(View.INVISIBLE);
        tvRefuse.setVisibility(View.INVISIBLE);
        tvStatusTip.setVisibility(View.VISIBLE);
        tvStatusTip.setTextColor(Color.parseColor("#FF5B5B"));
        tvStatusTip.setText(statusTip);
        tvDownTimer.setText(topTip);
    }

    @Override
    protected void bindContentView() {
        invitedInfo = ((MiniGameInvitedAttachment) message.getAttachment()).getDataInfo();
        contentContainer.setPadding(0, 0, 0, 0);
        contentContainer.setBackground(null);
        ImageLoadUtils.loadBannerRoundBg(context, invitedInfo.getGameBgImage(), imvGame);
        tvGameName.setText(invitedInfo.getGameName());

        isReceive = isReceivedMessage();
        Integer status = mapMsgStatus.get(invitedInfo.getRoomid());
        if (status == null) {
            status = (Integer) SpUtils.get(context, invitedInfo.getRoomid() + "", 0);
            if (status == null) {
                status = 0;
            }
        }
        tvDownTimer.setTag(message.getTime());
        //0:等待接受 1,超时，2，被拒绝，3,被取消，4，同意，5，失效
        if (status == WATING) {
            long curTime = System.currentTimeMillis() + deviceTimeOffset;
            long surplus = curTime - message.getTime();
            if (surplus > DURING) {//超过DURING为超时
                setEndStatus(TIME_OUT, "超时", "失效");
            } else {
                String lastStr = tvDownTimer.getText().toString();
                if (lastStr.equals("失效")) {
                    tvDownTimer.setText("60s");
                }
                tvStatusTip.setTextColor(Color.parseColor("#333333"));
                mapMsg.put(invitedInfo.getRoomid(), new WeakReference<>(this));
                mapInvitationMsg.put(invitedInfo.getRoomid(), new WeakReference<>(this));
                if (isReceive) {
                    tvRefuse.setVisibility(View.VISIBLE);
                    tvAgree.setVisibility(View.VISIBLE);
                    tvStatusTip.setVisibility(View.INVISIBLE);
                    tvStatusTip.setText("等待接受");
                    tvAgree.setBackgroundResource(R.drawable.bg_minigame_win_pop_btn_green);
                    tvAgree.setText("同意");
                    tvRefuse.setText("拒绝");
                    tvRefuse.setOnClickListener(onClickListener);
                    tvAgree.setOnClickListener(onClickListener);
                } else {
                    tvRefuse.setVisibility(View.INVISIBLE);
                    tvAgree.setVisibility(View.VISIBLE);
                    tvStatusTip.setVisibility(View.VISIBLE);
                    tvStatusTip.setText("等待对方接受");
                    tvAgree.setBackgroundResource(R.drawable.bg_minigame_win_pop_btn_red);
                    tvAgree.setText("取消");
                    //取消游戏邀请
                    tvAgree.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (dialogManager == null) {
                                dialogManager = new DialogManager(context);
                            }
                            dialogManager.showProgressDialog(context, "请稍后...");
                            miniGameModel.cancelInvited(invitedInfo.getRoomid() + "", new OkHttpManager.MyCallBack<ServiceResult<Object>>() {
                                @Override
                                public void onError(Exception e) {
                                    dialogManager.dismissDialog();
                                    SingleToastUtil.showToast(context, "操作失败");
                                }

                                @Override
                                public void onResponse(ServiceResult<Object> response) {
                                    dialogManager.dismissDialog();
                                    if (response.getCode() != 200) {
                                        SingleToastUtil.showToast(context, response.getMessage());
                                    } else {
                                        setEndStatus(CANCEL, "游戏被取消", "失效");
                                    }
                                }
                            });
                        }
                    });
                }
                continueTimer();
            }
        } else if (status == REFUSE) {//2，被拒绝
            setEndStatus(REFUSE, "游戏被拒绝", "失效");
        } else if (status == CANCEL) {//3,被取消
            setEndStatus(CANCEL, "游戏被取消", "失效");
        } else if (status == AGREE) {//4，同意
            setEndStatus(AGREE, "已同意", "失效");
        } else if (status == DISABLE) {//5，失效
            setEndStatus(DISABLE, "失效", "失效");
        } else {//超时
            setEndStatus(TIME_OUT, "超时", "失效");
        }
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int isAgree = -1;
            if (view.getId() == R.id.tvRefuse) {
                isAgree = 0;
            } else if (view.getId() == R.id.tvAgree) {
                isAgree = 1;
            }
            if (dialogManager == null) {
                dialogManager = new DialogManager(context);
            }
            dialogManager.showProgressDialog(context, "请稍后...");
            int finalIsAgree = isAgree;
            miniGameModel.acceptInvited(invitedInfo.getRoomid(), isAgree, new OkHttpManager.MyCallBack<ServiceResult<Object>>() {
                @Override
                public void onError(Exception e) {
                    dialogManager.dismissDialog();
                    SingleToastUtil.showToast(context, "操作失败");
                }

                @Override
                public void onResponse(ServiceResult<Object> response) {
                    dialogManager.dismissDialog();
                    if (response.getCode() != 200) {
                        SingleToastUtil.showToast(context, response.getMessage());
                    } else {
                        if (finalIsAgree == 1) {//同意邀请则进入房间
                            setEndStatus(AGREE, "已同意", "失效");
                        } else {
                            setEndStatus(REFUSE, "游戏被拒绝", "失效");
                        }
                    }
                }
            });
        }
    };
}
