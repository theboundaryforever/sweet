package com.yuhuankj.tmxq.ui.recommad;

import com.tongdaxing.erban.libcommon.coremanager.ICoreClient;

/**
 * Created by Administrator on 2017/7/12 0012.
 */

public interface IRecommAdCoreClient extends ICoreClient {

    String METHOD_ON_RECOMM_AD_SHOW_STATUS_CHANGED = "onRecommAdShowStatusChanged";

    void onRecommAdShowStatusChanged(boolean isShow);
}
