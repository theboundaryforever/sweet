package com.yuhuankj.tmxq.ui.liveroom.imroom.redpackage;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.bean.RoomRedPacket;
import com.tongdaxing.xchat_core.bean.RoomRedPacketReceiveResult;

import java.util.List;

/**
 * 文件描述：房间红包view接口层
 *
 * @auther：zwk
 * @data：2019/8/23
 */
public interface IRoomRedPackageView extends IMvpBaseView {
    void initStateAndShowRedPackageView(List<RoomRedPacket> roomRedPackets);

    void showReceiveSucAnimAndDialog(RoomRedPacketReceiveResult data);

    void showNoRedPackageDialog(RoomRedPacketReceiveResult data);

    void showFailToast(String error);

}
