package com.yuhuankj.tmxq.ui.message;

import java.io.Serializable;

/**
 * @author weihaitao
 * @date 2019/6/17
 */
public class UnreadMsgResult implements Serializable {

    /**
     * 点赞消息未读数
     */
    private int admireCount;
    /**
     * 粉丝消息未读数
     */
    private int fansCount;
    /**
     * 签到消息未读数
     */
    private int signCount;

    public int getAdmireCount() {
        return admireCount;
    }

    public void setAdmireCount(int admireCount) {
        this.admireCount = admireCount;
    }

    public int getFansCount() {
        return fansCount;
    }

    public void setFansCount(int fansCount) {
        this.fansCount = fansCount;
    }

    public int getSignCount() {
        return signCount;
    }

    public void setSignCount(int signCount) {
        this.signCount = signCount;
    }


}
