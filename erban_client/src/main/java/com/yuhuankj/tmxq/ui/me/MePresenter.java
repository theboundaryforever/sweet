package com.yuhuankj.tmxq.ui.me;

import android.content.Context;
import android.content.Intent;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.utils.ButtonUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.user.bean.MedalBean;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.me.charge.ChargeActivity;
import com.yuhuankj.tmxq.ui.me.medal.MedalActivity;
import com.yuhuankj.tmxq.ui.me.shop.ShopActivity;
import com.yuhuankj.tmxq.ui.me.taskcenter.view.TaskCenterActivity;
import com.yuhuankj.tmxq.ui.me.visitor.VisitorActivity;
import com.yuhuankj.tmxq.ui.message.attention.AttentionListActivity;
import com.yuhuankj.tmxq.ui.message.fans.FansListActivity;
import com.yuhuankj.tmxq.ui.message.like.LikeMeListActivity;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;
import com.yuhuankj.tmxq.utils.UIHelper;

import java.util.ArrayList;

/**
 * Created by MadisonRong on 04/01/2018.
 */

public class MePresenter extends AbstractMvpPresenter<IMeView> {

    private UserMvpModel userMvpModel;
    private UserInfo mUserInfo;

    public MePresenter() {
        userMvpModel = new UserMvpModel();
    }

    public void initUserData() {
        mUserInfo = userMvpModel.getUserDate();
        setupUserData();
    }

    public void setupUserData() {
        if (mUserInfo == null) {
//            MLog.error(this, "用户信息不存在！");
            return;
        }
        getMvpView().updateUserInfoUI(mUserInfo);
    }

    public void clickBy(int viewId, Context context) {
        if (ButtonUtils.isFastDoubleClick(viewId)) {
            return;
        }
        switch (viewId) {
            case R.id.me_noble:
                CommonWebViewActivity.start(context, UriProvider.getMyNobleUrl(), true);
                break;
            case R.id.rlUserInfo:
                if (mUserInfo != null) {
                    UserInfoActivity.start(context, mUserInfo.getUid());
                }
                break;
            case R.id.tv_user_attentions:
            case R.id.tv_user_attention_text:
                context.startActivity(new Intent(context, AttentionListActivity.class));
                break;
            case R.id.tv_user_fans:
            case R.id.tv_user_fan_text:
                context.startActivity(new Intent(context, FansListActivity.class));
                break;
            case R.id.me_item_setting:
                UIHelper.showSettingAct(context);
                break;
            case R.id.me_item_charge:
                context.startActivity(new Intent(context, ChargeActivity.class));
                break;
            case R.id.bivMyVisitor:
                context.startActivity(new Intent(context, VisitorActivity.class));
                getMvpView().setVisitorCount(0);
                break;
            case R.id.me_item_level:
                CommonWebViewActivity.start(context, UriProvider.getLevelUrl());
                break;
            case R.id.me_car:
                context.startActivity(new Intent(context, ShopActivity.class));
                break;
            case R.id.bivInvite:
                CommonWebViewActivity.start(context, UriProvider.getInviteH5Url());
                break;
            case R.id.bivRealAuth:
                CommonWebViewActivity.start(context, UriProvider.getRealNameAuthUrl());
                //context.startActivity(new Intent(context, FaceIdentityActivity.class));
                break;
            case R.id.tvAdmireNum:
            case R.id.tvAdmireNumTips:
                LikeMeListActivity.start(context);
                break;
            case R.id.bivMyMedal:
                try {
                    ArrayList<MedalBean> medalBeans = new ArrayList<>(mUserInfo.getWearList());
                    Intent intent = new Intent(context, MedalActivity.class);
                    intent.putExtra("uid", mUserInfo.getUid());
                    intent.putExtra("userFortuneLevel", mUserInfo.getExperLevel());
                    intent.putExtra("curSetMedals", medalBeans);
                    context.startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.llWallet:
                UIHelper.showWalletAct(context);
                break;
            case R.id.llTask:
                context.startActivity(new Intent(context, TaskCenterActivity.class));
                break;
            case R.id.iv_user_head:
                //UIHelper.showUserInfoModifyAct(context, CoreManager.getCore(IAuthCore.class).getCurrentUid());
                if (mUserInfo != null) {
                    UserInfoActivity.start(context, mUserInfo.getUid());
                }
                break;
            default:
                break;
        }
    }
}
