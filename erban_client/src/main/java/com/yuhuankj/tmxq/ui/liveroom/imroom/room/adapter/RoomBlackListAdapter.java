package com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMember;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.bindadapter.BaseAdapter;
import com.yuhuankj.tmxq.base.bindadapter.BindingViewHolder;
import com.yuhuankj.tmxq.databinding.ListItemLiveroomBlackBinding;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.UIUtil;

/**
 * Created by huangmeng1 on 2018/1/18.
 */

public class RoomBlackListAdapter extends BaseAdapter<IMRoomMember> {
    private RoomBlackDelete roomBlackDelete;

    public RoomBlackListAdapter(int layoutResId, int brid) {
        super(layoutResId, brid);
    }

    public void setRoomBlackDelete(RoomBlackDelete roomBlackDelete) {
        this.roomBlackDelete = roomBlackDelete;
    }

    @Override
    protected void convert(BindingViewHolder helper, IMRoomMember item) {
        super.convert(helper, item);
        ListItemLiveroomBlackBinding binding = (ListItemLiveroomBlackBinding) helper.getBinding();

        // 黑名单新增数据
        binding.tvMarkAdmin.setText(null);
        binding.tvMarkTime.setText(null);
        String nick = item.getNick();
        if (!TextUtils.isEmpty(nick) && nick.length() > 6) {
            nick = binding.tvUserName.getContext().getResources().getString(R.string.nick_length_max_six, nick.substring(0, 6));
        }
        binding.tvUserName.setText(nick);
        ViewGroup.LayoutParams layoutParams = binding.container.getLayoutParams();
        layoutParams.width = UIUtil.getScreenWidth(mContext);
        binding.container.setLayoutParams(layoutParams);
        binding.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (roomBlackDelete != null) {
                    roomBlackDelete.onDeleteClick(item);
                }
            }
        });

    }

    public interface RoomBlackDelete {
        void onDeleteClick(IMRoomMember imRoomMember);
    }
}
