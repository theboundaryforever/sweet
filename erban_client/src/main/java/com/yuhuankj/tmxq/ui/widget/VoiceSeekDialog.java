package com.yuhuankj.tmxq.ui.widget;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.xchat_core.player.IPlayerCore;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.Utils;

/**
 * Created by chenran on 2017/12/18.
 */

public class VoiceSeekDialog extends BottomSheetDialog implements SeekBar.OnSeekBarChangeListener{
    private Context context;
    private SeekBar musicVoiceSeek;
    private SeekBar voiceSeek;
    private TextView musicVoiceNum;
    private TextView voiceNum;

    public VoiceSeekDialog(@NonNull Context context) {
        super(context, R.style.ErbanBottomSheetDialog);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_voice_seek);
        setCanceledOnTouchOutside(true);

        musicVoiceSeek = findViewById(R.id.music_voice_seek);
        musicVoiceSeek.setMax(100);
        musicVoiceSeek.setProgress(CoreManager.getCore(IPlayerCore.class).getCurrentVolume());
        musicVoiceSeek.setOnSeekBarChangeListener(this);

        voiceSeek = findViewById(R.id.voice_seek);
        voiceSeek.setMax(100);
        voiceSeek.setProgress(CoreManager.getCore(IPlayerCore.class).getCurrentRecordingVolume());
        voiceSeek.setOnSeekBarChangeListener(this);

        musicVoiceNum = findViewById(R.id.music_voice_number);
        voiceNum = findViewById(R.id.voice_number);

        musicVoiceNum.setText(CoreManager.getCore(IPlayerCore.class).getCurrentVolume()+"%");
        voiceNum.setText(CoreManager.getCore(IPlayerCore.class).getCurrentRecordingVolume()+"%");

        FrameLayout bottomSheet = findViewById(android.support.design.R.id.design_bottom_sheet);
        if (bottomSheet != null) {
            BottomSheetBehavior.from(bottomSheet).setSkipCollapsed(false);
            BottomSheetBehavior.from(bottomSheet).setPeekHeight(
                    (int) context.getResources().getDimension(R.dimen.dialog_gift_height) +
                            (Utils.hasSoftKeys(context) ? Utils.getNavigationBarHeight(context) : 0));
        }
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = context.getResources().getDisplayMetrics().heightPixels - (Utils.hasSoftKeys(context) ? Utils.getNavigationBarHeight(context) : 0);
        getWindow().setAttributes(params);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (seekBar == musicVoiceSeek) {
            CoreManager.getCore(IPlayerCore.class).seekVolume(progress);
            musicVoiceNum.setText(progress+"%");
        } else {
            CoreManager.getCore(IPlayerCore.class).seekRecordingVolume(progress);
            voiceNum.setText(progress+"%");
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
