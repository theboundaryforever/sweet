package com.yuhuankj.tmxq.ui.share;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.netease.nim.uikit.common.util.sys.ClipboardUtil;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.Utils;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;

/**
 * @author xiaoyu
 * @date 2017/12/13
 */

public class ShareDialog extends BottomSheetDialog implements View.OnClickListener {
    private Context context;
    private WindowManager windowManager;
    private int width;
    private TextView tvName;
    private TextView tvWeixin;
    private TextView tvWeixinpy;
    private TextView tvQq;
    private TextView tvQqZone;
    private TextView tvCancel;
    private TextView tv_share_copy;
    private static final String TAG = "ShareDialog";
    private OnShareDialogItemClick onShareDialogItemClick;
    private TextView tvShareFriend;

    public void setShowShareFans(boolean showShareFans) {
        this.showShareFans = showShareFans;
    }

    public boolean showShareFans = false;

    public ShareDialog(Context context) {
        super(context, R.style.ErbanBottomSheetDialog);
        this.context = context;
    }

    public void setOnShareDialogItemClick(OnShareDialogItemClick onShareDialogItemClick) {
        this.onShareDialogItemClick = onShareDialogItemClick;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_share);
        setCanceledOnTouchOutside(true);
        tvName = findViewById(R.id.tv_title);
        tvWeixin = findViewById(R.id.tv_weixin);
        tvWeixinpy = findViewById(R.id.tv_weixinpy);
        tvQq = findViewById(R.id.tv_qq);
        tvQqZone = findViewById(R.id.tv_qq_zone);
        tvCancel = findViewById(R.id.tv_cancel);
        tv_share_copy = findViewById(R.id.tv_share_copy);
        tvShareFriend = findViewById(R.id.tv_share_friend);
        findViewById(R.id.ll_2).setVisibility(showShareFans ? View.VISIBLE : View.GONE);

        tvShareFriend.setOnClickListener(this);
        tvWeixin.setOnClickListener(this);
        tvWeixinpy.setOnClickListener(this);
        tvQq.setOnClickListener(this);
        tvQqZone.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        tv_share_copy.setOnClickListener(this);
        FrameLayout bottomSheet = findViewById(android.support.design.R.id.design_bottom_sheet);
        if (bottomSheet != null) {
            BottomSheetBehavior.from(bottomSheet).setSkipCollapsed(false);
            if (showShareFans) {
                BottomSheetBehavior.from(bottomSheet).setPeekHeight(
                        (int) context.getResources().getDimension(R.dimen.dialog_share_height_2));
            } else {
                BottomSheetBehavior.from(bottomSheet).setPeekHeight(
                        (int) context.getResources().getDimension(R.dimen.dialog_share_height));
            }

        }
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = context.getResources().getDisplayMetrics().heightPixels - (Utils.hasSoftKeys(context) ? Utils.getNavigationBarHeight(context) : 0);
        getWindow().setAttributes(params);
    }

    public void setName(String name) {
        if (!StringUtils.isEmpty(name)) {
            tvName.setText(name);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_share_copy:
                RoomInfo currentRoomInfo = BaseRoomServiceScheduler.getCurrentRoomInfo();
                if (currentRoomInfo != null) {
                    String url = new StringBuilder(UriProvider.JAVA_WEB_TMXQ_URL).append("/ttyy/share_tmxq/share.html?shareUid=")
                            .append(CoreManager.getCore(IAuthCore.class).getCurrentUid())
                            .append("&uid=").append(currentRoomInfo.getUid())
                            .append("&roomType=").append(currentRoomInfo.getType()).toString();
                    LogUtils.d(TAG,"onClick-url:"+url);
                    ClipboardUtil.clipboardCopyText(context,url);
                    SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(),
                            R.string.share_dialog_method_copy_link_tips,
                            Toast.LENGTH_SHORT);
                }
                break;
            case R.id.tv_weixin:
                if (onShareDialogItemClick != null) {
                    onShareDialogItemClick.onSharePlatformClick(ShareSDK.getPlatform(Wechat.NAME));
                }
                break;
            case R.id.tv_weixinpy:
                if (onShareDialogItemClick != null) {
                    onShareDialogItemClick.onSharePlatformClick(ShareSDK.getPlatform(WechatMoments.NAME));
                }
                break;
            case R.id.tv_qq:
                if (onShareDialogItemClick != null) {
                    onShareDialogItemClick.onSharePlatformClick(ShareSDK.getPlatform(QQ.NAME));
                }
                break;
            case R.id.tv_qq_zone:
                if (onShareDialogItemClick != null) {
                    onShareDialogItemClick.onSharePlatformClick(ShareSDK.getPlatform(QZone.NAME));
                }
                break;
            case R.id.tv_share_friend:
                context.startActivity(new Intent(context, ShareFansActivity.class));
                break;
            case R.id.tv_cancel:

                break;
            default:
                break;
        }
        dismiss();
    }

    public interface OnShareDialogItemClick {
        void onSharePlatformClick(Platform platform);
    }
}
