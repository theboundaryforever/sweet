package com.yuhuankj.tmxq.ui.message.like;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;

import java.util.Map;

public class LikeModel {

    //获取点赞消息列表
    public void getLikeMsgList(int pageNum, int pageSize, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("pageSize", String.valueOf(pageSize));
        params.put("pageNum", String.valueOf(pageNum));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().postRequest(UriProvider.getLikeHistory(), params, callBack);
    }


    //设置点赞消息已读
    public void setLikeMsgListRead(OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        //params.put("timeStamp", String.valueOf(time));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().postRequest(UriProvider.setLikeMsgListRead(), params, callBack);
    }

    //获取点赞未读消息数量
    public void getUnreadLikeMsgCount(OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().postRequest(UriProvider.getUnreadLikeMsgCount(), params, callBack);
    }
}
