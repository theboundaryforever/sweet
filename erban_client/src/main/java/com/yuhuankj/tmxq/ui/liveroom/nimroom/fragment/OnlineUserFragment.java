package com.yuhuankj.tmxq.ui.liveroom.nimroom.fragment;

import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.netease.nim.uikit.common.util.sys.NetworkUtil;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.JavaUtil;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.OnlineChatMember;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.presenter.HomePartyUserListPresenter;
import com.tongdaxing.xchat_core.room.view.IHomePartyUserListView;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.base.fragment.BaseMvpFragment;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.adapter.OnlineUserAdapter;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.widget.ButtonItemFactory;
import com.yuhuankj.tmxq.ui.widget.DividerItemDecoration;
import com.yuhuankj.tmxq.ui.widget.UserInfoDialog;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.Disposable;

/**
 * <p>  在线用户列表 </p>
 *
 * @author Administrator
 * @date 2017/12/4
 */
@CreatePresenter(HomePartyUserListPresenter.class)
public class OnlineUserFragment extends BaseMvpFragment<IHomePartyUserListView, HomePartyUserListPresenter>
        implements BaseQuickAdapter.OnItemClickListener, IHomePartyUserListView, OnlineUserAdapter.OnRoomOnlineNumberChangeListener {

    private final String TAG = OnlineUserFragment.class.getSimpleName();
    private RecyclerView mRecyclerView;
    private SmartRefreshLayout mRefreshLayout;

    private OnlineUserAdapter mOnlineUserAdapter;
    private int mPage = Constants.PAGE_START;
    public boolean isMic = false;//是否在麦上public

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mRefreshLayout != null) {
            mRefreshLayout.setOnRefreshListener(null);
            mRefreshLayout = null;
        }
        if (mView != null) {
            mView = null;
        }
        mRecyclerView = null;
        if (mOnlineUserAdapter != null) {
            mOnlineUserAdapter.release();
            mOnlineUserAdapter = null;
        }
    }

    @Override
    public void onFindViews() {
        mRecyclerView = mView.findViewById(R.id.recycler_view);
        mRefreshLayout = mView.findViewById(R.id.refresh_layout);
        if (getArguments() != null) {
            isMic = getArguments().getBoolean("isMic", false);
        }
        mRefreshLayout.setEnableHeaderTranslationContent(false);
        mRecyclerView.setLayoutManager(new RecyclerViewNoBugLinearLayoutManager(mContext));
        mOnlineUserAdapter = new OnlineUserAdapter();
        mRecyclerView.setAdapter(mOnlineUserAdapter);
        mRecyclerView.addItemDecoration(
                new DividerItemDecoration(getContext(), OrientationHelper.VERTICAL,
                        2, R.color.app_bg));
        mOnlineUserAdapter.setOnItemClickListener(this);
        mOnlineUserAdapter.setListener(this);
    }

    @Override
    public void onSetListener() {
        mRefreshLayout.setOnRefreshLoadmoreListener(new OnRefreshLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshLayout) {
                if (!NetworkUtil.isNetAvailable(mContext)) {
                    if (mRefreshLayout != null) {
                        mRefreshLayout.finishLoadmore();
                    }
                    return;
                }
                List<OnlineChatMember> data = fitUser(mOnlineUserAdapter != null ? mOnlineUserAdapter.getData() : null);
                if (ListUtils.isListEmpty(data)) {
                    if (mRefreshLayout != null) {
                        mRefreshLayout.finishLoadmore();
                    }
                    return;
                }
                loadData(data.get(data.size() - 1).chatRoomMember.getEnterTime());
            }

            @Override
            public void onRefresh(RefreshLayout refreshLayout) {
                if (!NetworkUtil.isNetAvailable(mContext)) {
                    if (mRefreshLayout != null) {
                        mRefreshLayout.finishRefresh();
                    }
                    return;
                }
                firstLoad();
            }
        });
    }

    private List<OnlineChatMember> fitUser(List<OnlineChatMember> user) {
        if (!isMic) {
            return user;
        }
        if (ListUtils.isListEmpty(user)) {
            return user;
        }
        List<OnlineChatMember> data = new ArrayList<>();
        for (int i = 0; i < user.size(); i++) {
            if (user.get(i).isOnMic) {
                data.add(user.get(i));
            }
        }
        return data;
    }

    @Override
    public void initiate() {
    }


    @Override
    public int getRootLayoutId() {
        return R.layout.common_refresh_recycler_view;
    }


    @Override
    public void loadLazyIfYouWant() {
        super.loadLazyIfYouWant();
        LogUtils.d(TAG, "loadLazyIfYouWant");
        firstLoad();
    }

    public void firstLoad() {
        mPage = Constants.PAGE_START;
        loadData(0);
    }

    private void loadData(long time) {
        if (null != AvRoomDataManager.get().mCurrentRoomInfo) {
            getMvpPresenter().getRoomOnLineUserList(mPage, time, 101,
                    AvRoomDataManager.get().mCurrentRoomInfo.getRoomId(),
                    mOnlineUserAdapter == null ? null : fitUser(mOnlineUserAdapter.getData()));
        }
    }

    @Override
    public void onRequestChatMemberByPageSuccess(List<OnlineChatMember> chatRoomMemberList, int page) {
        dealOnLineUserListRespone(chatRoomMemberList, page);
    }

    private void dealOnLineUserListRespone(List<OnlineChatMember> chatRoomMemberList, int page) {
        mPage = page;
        if (!ListUtils.isListEmpty(chatRoomMemberList)) {
            if (mOnlineUserAdapter != null) {
                mOnlineUserAdapter.setNewData(fitUser(chatRoomMemberList));
            }
            if (mPage == Constants.PAGE_START) {
                if (mRefreshLayout != null) {
                    mRefreshLayout.finishRefresh();
                }
            } else {
                if (mRefreshLayout != null) {
                    mRefreshLayout.finishLoadmore(0);
                }
            }
            mPage++;
        } else {
            if (mPage == Constants.PAGE_START) {
                if (mRefreshLayout != null) {
                    mRefreshLayout.finishRefresh();
                }
            } else {
                if (mRefreshLayout != null) {
                    mRefreshLayout.finishLoadmore(0);
                }
            }
        }
    }

    @Override
    public void onRequestChatMemberByPageFail(String errorStr, int page) {
        LogUtils.d(TAG, "onRequestChatMemberByPageFail 获取到数据失败,page=" + page);
        dealGetOnLineUlserListError(page);
    }

    private void dealGetOnLineUlserListError(int page) {
        mPage = page;
        if (mPage == Constants.PAGE_START) {
            if (mRefreshLayout != null) {
                mRefreshLayout.finishRefresh();
            }
        } else {
            if (mRefreshLayout != null) {
                mRefreshLayout.finishLoadmore(0);
            }
        }
    }

    @Override
    public void onGetOnLineUserList(boolean isSuccess, String message, int page, List<OnlineChatMember> onlineChatMembers) {
        LogUtils.d(TAG, "onGetOnLineUserList-isSuccess:" + isSuccess + " message:" + message);
        if (isSuccess) {
            dealOnLineUserListRespone(onlineChatMembers, page);
        } else {
            dealGetOnLineUlserListError(page);
            if (!TextUtils.isEmpty(message)) {
                toast(message);
            }
        }
    }

    private OnlineItemClick onlineItemClick;

    public void setOnlineItemClick(OnlineItemClick onlineItemClick) {
        this.onlineItemClick = onlineItemClick;
    }


    public interface OnlineItemClick {
        void onItemClick(ChatRoomMember chatRoomMember);
    }

    @Override
    public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int position) {
        RoomInfo currentRoom = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoom != null && mOnlineUserAdapter != null) {
            List<OnlineChatMember> chatRoomMembers = mOnlineUserAdapter.getData();
            if (ListUtils.isListEmpty(chatRoomMembers) || position >= chatRoomMembers.size()) {
                return;
            }
            ChatRoomMember chatRoomMember = chatRoomMembers.get(position).chatRoomMember;
            //麦上用户列表，用于PK选择用户界面，点击事件交给调用者处理
            if (onlineItemClick != null) {
                onlineItemClick.onItemClick(chatRoomMember);
                return;
            }
            //在线列表，点击事件直接内部处理
            if (chatRoomMember != null) {
                String account = chatRoomMember.getAccount();
                if (AvRoomDataManager.get().isRoomOwner(account) || AvRoomDataManager.get().isOwner(account)) {
                    new UserInfoDialog(mContext, JavaUtil.str2long(account)).show();
                } else {
                    if (TextUtils.isEmpty(account)) {
                        return;
                    }
                    //公屏点击弹框
                    final List<ButtonItem> buttonItems = new ArrayList<>();
                    //礼物弹框
                    List<ButtonItem> items = ButtonItemFactory.createAllRoomPublicScreenButtonItems(mContext, chatRoomMember);
                    if (items == null) {
//                        new UserInfoDialog(mContext, JavaUtil.str2long(account)).show();
                        return;
                    }
                    buttonItems.addAll(items);
                    ((BaseMvpActivity) mContext).getDialogManager().showCommonPopupDialog(buttonItems, "取消");
                }
            }
        }
    }

    @Override
    public void onMemberIn(String account, List<OnlineChatMember> dataList) {
        Disposable disposable = getMvpPresenter().onMemberInRefreshData(account, dataList, mPage);
        if (null != disposable && null != mCompositeDisposable) {
            mCompositeDisposable.add(disposable);
        }
    }

    @Override
    public void onMemberDownUpMic(String account, boolean isUpMic, List<OnlineChatMember> dataList) {
        Disposable disposable = getMvpPresenter().onMemberDownUpMic(account, isUpMic, dataList, mPage);
        if (null != disposable && null != mCompositeDisposable) {
            mCompositeDisposable.add(disposable);
        }
    }

    @Override
    public void onUpdateMemberManager(String account, boolean isRemoveManager, List<OnlineChatMember> dataList) {
        Disposable disposable = getMvpPresenter().onUpdateMemberManager(account, dataList, isRemoveManager, mPage);
        if (null != disposable && null != mCompositeDisposable) {
            mCompositeDisposable.add(disposable);
        }
    }

    @Override
    public void addMemberBlack() {
    }
}
