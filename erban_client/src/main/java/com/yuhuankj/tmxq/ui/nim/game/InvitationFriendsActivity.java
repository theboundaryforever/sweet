package com.yuhuankj.tmxq.ui.nim.game;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.netease.nim.uikit.NimUIKit;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.friend.FriendService;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liaoxy
 * @Description:小游戏邀请页
 * @date 2019/1/3 11:39
 */
public class InvitationFriendsActivity extends BaseActivity {
    private List<String> friends;
    private LinearLayout llEmply;
    private RecyclerView rcvFriends;
    private BaseQuickAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_minigame_invitation_friends);
        initTitleBar("邀请好友");
        initView();
        initData();
    }

    private void initView() {
        rcvFriends = (RecyclerView) findViewById(R.id.rcvFriends);
        llEmply = (LinearLayout) findViewById(R.id.llEmply);
    }

    private void initData() {
        //mTitleBar.setBackgroundColor(Color.parseColor("#FAFAFA"));
        LinearLayoutManager layoutmanager = new RecyclerViewNoBugLinearLayoutManager(this);
        layoutmanager.setOrientation(LinearLayoutManager.VERTICAL);
        rcvFriends.setLayoutManager(layoutmanager);
        friends = new ArrayList<>();

        List<String> tempFriends = NIMClient.getService(FriendService.class).getFriendAccounts();
        if (tempFriends != null) {
            friends.addAll(tempFriends);
        }
        if(friends.size()==0){
            llEmply.setVisibility(View.VISIBLE);
            return;
        }
        llEmply.setVisibility(View.GONE);
        adapter = new InvitationAdapter(R.layout.item_minigame_invitation_friends, friends);
        rcvFriends.setAdapter(adapter);
        adapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                if (view.getId() == R.id.tvInvitation) {
                    if (friends != null && position >= 0 && position < friends.size()) {
                        Bundle arg = new Bundle();
                        arg.putString("showAction", "games");
                        NimUIKit.startP2PSession(InvitationFriendsActivity.this, friends.get(position), arg);
                    } else {
                        SingleToastUtil.showToast("好友数据不正确");
                    }
                }
            }
        });
    }
}
