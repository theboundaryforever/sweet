package com.yuhuankj.tmxq.ui.nim.chat;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.common.ui.recyclerview.adapter.BaseMultiItemFetchLoadAdapter;
import com.netease.nim.uikit.glide.GlideApp;
import com.netease.nim.uikit.session.viewholder.MsgViewHolderBase;
import com.netease.nimlib.sdk.uinfo.model.UserInfo;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.xchat_core.im.custom.bean.ChanceMeetingMsgAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.QmLikeAttachment;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.widget.CircleImageView;

import java.text.SimpleDateFormat;
import java.util.Date;


public class MsgViewHolderQmLike extends MsgViewHolderBase implements View.OnClickListener {

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private CircleImageView civSender;
    private CircleImageView civReceiver;
    private TextView tvMsgTime;
    private TextView tvContent;

    public MsgViewHolderQmLike(BaseMultiItemFetchLoadAdapter adapter) {
        super(adapter);
        setMiddleItem(true);
        setShowHeadImage(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.civSender:
                UserInfoActivity.start(context, Long.valueOf(message.getFromAccount()));
                break;
            case R.id.civReceiver:
                ChanceMeetingMsgAttachment attachment = (ChanceMeetingMsgAttachment) message.getAttachment();
                if (null != attachment) {
                    UserInfoActivity.start(context, attachment.getTargetUid());
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected int getContentResId() {
        return R.layout.layout_msg_view_holder_chance_meeting;
    }

    @Override
    protected void inflateContentView() {
        civSender = findViewById(R.id.civSender);
        civReceiver = findViewById(R.id.civReceiver);
        tvMsgTime = findViewById(R.id.tvMsgTime);
        tvContent = findViewById(R.id.tvContent);
    }

    @Override
    protected void bindContentView() {
        QmLikeAttachment attachment = (QmLikeAttachment) message.getAttachment();
        final UserInfo userInfo = NimUIKit.getUserInfoProvider().getUserInfo(message.getFromAccount());
        if (null != attachment && attachment.getDataInfo() != null) {
            long sendTime = attachment.getDataInfo().getAdmireDate();
            String sendTimeStr = simpleDateFormat.format(new Date(sendTime));
            tvMsgTime.setText(sendTimeStr);
            tvContent.setText(attachment.getDataInfo().getContent());
            if (null != userInfo && !TextUtils.isEmpty(userInfo.getAvatar())) {
                GlideApp.with(context)
                        .load(ImageLoadUtils.toThumbnailUrl(DisplayUtility.dp2px(context, 48), DisplayUtility.dp2px(context, 48), userInfo.getAvatar()))
                        .placeholder(R.drawable.default_user_head)
                        .into(civSender);
            }
            if (attachment.getDataInfo() != null && !TextUtils.isEmpty(attachment.getDataInfo().getTargetAvatar())) {
                GlideApp.with(context)
                        .load(ImageLoadUtils.toThumbnailUrl(DisplayUtility.dp2px(context, 48), DisplayUtility.dp2px(context, 48), attachment.getDataInfo().getTargetAvatar()))
                        .placeholder(R.drawable.default_user_head)
                        .into(civReceiver);
            }
        }
    }
}
