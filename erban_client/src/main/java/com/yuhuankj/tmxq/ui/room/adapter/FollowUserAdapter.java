package com.yuhuankj.tmxq.ui.room.adapter;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FollowUserAdapter extends BaseQuickAdapter<Json, FollowUserAdapter.ViewHolder> {
    public FollowUserAdapter() {
        super(R.layout.item_follow_user);
    }

    @Override
    protected void convert(ViewHolder viewHolder, Json json) {
        viewHolder.tvFollowListName.setText(json.str("nick"));
        String userInRoomName = null;
        if (json.has("userInRoom")) {
            userInRoomName = json.json_ok("userInRoom").str("title");
            viewHolder.tvOnlineStatus.setVisibility(View.VISIBLE);
        }else{
            viewHolder.tvOnlineStatus.setVisibility(View.GONE);
        }
        viewHolder.tvFollowUserInRoomName.setText(userInRoomName);

        String avatar = json.str("avatar");
        if (!TextUtils.isEmpty(avatar) && GlideContextCheckUtil.checkContextUsable(viewHolder.ivFollowListUserIcon.getContext())) {
            ImageLoadUtils.loadCustomCornerImage(viewHolder.ivFollowListUserIcon.getContext(),
                    ImageLoadUtils.toThumbnailUrl(DisplayUtility.dp2px(viewHolder.ivFollowListUserIcon.getContext(), 100),
                            DisplayUtility.dp2px(viewHolder.ivFollowListUserIcon.getContext(), 100), avatar),
                    viewHolder.ivFollowListUserIcon, DisplayUtility.dp2px(viewHolder.ivFollowListUserIcon.getContext(), 16));
        }
    }


    static class ViewHolder extends BaseViewHolder {
        //增加Nullable注解解决闪退:
        //Caused by: java.lang.IllegalStateException: Required view 'iv_follow_list_user_icon'
        // with ID 2131296888 for field 'ivFollowListUserIcon' was not found.
        // If this view is optional add '@Nullable' (fields) or '@Optional' (methods) annotation.
        @Nullable
        @BindView(R.id.ivFollowListUserIcon)
        ImageView ivFollowListUserIcon;
        @Nullable
        @BindView(R.id.tvFollowListName)
        TextView tvFollowListName;
        @Nullable
        @BindView(R.id.tvFollowUserInRoomName)
        TextView tvFollowUserInRoomName;
        @Nullable
        @BindView(R.id.tvOnlineStatus)
        TextView tvOnlineStatus;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


}
