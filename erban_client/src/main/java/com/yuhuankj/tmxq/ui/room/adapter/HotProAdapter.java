package com.yuhuankj.tmxq.ui.room.adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.juxiao.library_utils.DisplayUtils;
import com.netease.nim.uikit.glide.GlideApp;
import com.scwang.smartrefresh.layout.util.DensityUtil;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.MainActivity;
import com.yuhuankj.tmxq.ui.home.activity.SortListActivity;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HotProAdapter extends BaseQuickAdapter<Json, BaseViewHolder> {

    public static final int Recommend = 1;
    public static final int SortMenu = 2;
    public static final int PublicChatRoomType = 3;
    private final SortMenuAdapter sortMenuAdapter;
    private HotRecommendAdapter hotRecommendAdapter;
    public boolean showNewUserTitle = true;
    public boolean showRecommendBg = true;
    private List<Json> chatList;
    private ObjectAnimator animator;
    private Context context;
    int tagHeight = 28;

    public void setHotRoomType(HotRoomType hotRoomType) {
        this.hotRoomType = hotRoomType;
    }

    private HotRoomType hotRoomType = HotRoomType.unknow;

    public enum HotRoomType {
        unknow,
        recommend_room,
        hot_room
    }

    public void setHotRecommendAdapter(HotRecommendAdapter hotRecommendAdapter) {
        this.hotRecommendAdapter = hotRecommendAdapter;
    }

    public HotProAdapter(Context context,HotRecommendAdapter hotRecommendAdapter, SortMenuAdapter sortMenuAdapter) {
        super(R.layout.list_item_home_hot_normal_new, new ArrayList<>());
        this.context = context;
        this.hotRecommendAdapter = hotRecommendAdapter;
        this.sortMenuAdapter = sortMenuAdapter;
        this.tagHeight = DisplayUtils.dip2px(context,14);
    }

    @Override
    protected void convert(BaseViewHolder helper, Json item) {

    }


    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        position = position - getHeaderLayoutCount();
        if (position < 0) {
            return;
        }
        if (position >= getData().size()) {
            return;
        }

        Json json = getData().get(position);
        if (holder instanceof HotItemViewHolder) {
            setHotData((HotItemViewHolder) holder, json);
        } else if (holder instanceof RecommendViewHolder) {
            setRecommendVData((RecommendViewHolder) holder, json);
            ((RecommendViewHolder) holder).recyclerView.setVisibility(showRecommendBg ? View.VISIBLE : View.GONE);
        } else if (holder instanceof SortMenuViewHolder) {
            ((SortMenuViewHolder) holder).newUserBg.setVisibility(showNewUserTitle ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == Recommend) {
            return new RecommendViewHolder(LayoutInflater.from(mContext).inflate(R.layout.head_recommend, parent, false));
        } else if (viewType == SortMenu) {
            return new SortMenuViewHolder(LayoutInflater.from(mContext).inflate(R.layout.list_item_home_hot_sort_menu, parent, false));
        } else if (viewType == 0) {
            //list_item_home_hot_normal_pro.xml
            return new HotItemViewHolder(LayoutInflater.from(context).inflate(R.layout.item_room_new, parent, false));
        } else if (viewType == PublicChatRoomType) {
            return new PublicChatRoomViewHolder(LayoutInflater.from(mContext).inflate(R.layout.list_item_home_public_chat_room, parent, false));
        }
        return super.onCreateViewHolder(parent, viewType);
    }


    @Override
    public int getItemViewType(int position) {
        int itemViewType = super.getItemViewType(position);
        if (itemViewType == 0) {
            int index = position - getHeaderLayoutCount();
            if (index < 0) {
                return itemViewType;
            }
            Json json = getData().get(index);
            int viewType = json.num("viewType");
            if (viewType == 1) {
                return Recommend;
            } else if (viewType == 2) {
                return SortMenu;
            } else if (viewType == 3) {
                return PublicChatRoomType;
            }
        }
        return itemViewType;
    }

    public void setChatList(List<Json> chatList) {
        this.chatList = chatList;
    }


    public class PublicChatRoomViewHolder extends BaseViewHolder {

        @BindView(R.id.iv_msg_user_icon)
        ImageView ivMsgUserIcon;
        @BindView(R.id.tv_msg_content)
        TextView tvMsgContent;
        @BindView(R.id.ll_chat_room_user_info)
        LinearLayout llChatRoomUserInfo;

        private int animationRepeatTime = 0;

        public PublicChatRoomViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LocalBroadcastManager.getInstance(view.getContext()).sendBroadcast(new Intent(MainActivity.ACTION_OPEN_FIND));
                }
            });
            if (chatList != null && chatList.size() > 0) {
                Json json = chatList.get(0);
                setPublicChatRoomInfo(json);
            } else {
                return;
            }
            if (animator != null) {
                animator.removeAllListeners();
                animator.cancel();
            }

            animator = ObjectAnimator.ofFloat(llChatRoomUserInfo, "alpha", 0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 0f);
            animator.setDuration(3000);//时间1s
            animator.setRepeatCount(-1);
            animator.start();
            animator.addListener(new AnimatorListenerAdapter() {


                @Override
                public void onAnimationRepeat(Animator animation) {
                    super.onAnimationRepeat(animation);

                    if (chatList != null && chatList.size() > 0) {
                        Json json = chatList.get(++animationRepeatTime % chatList.size());
                        setPublicChatRoomInfo(json);
                    }
                }

            });
        }

        private void setPublicChatRoomInfo(Json json) {
            try {
                Context context = ivMsgUserIcon.getContext();
                if (context != null) {
                    ImageLoadUtils.loadCircleImage(context, json.str("fromAvator"), ivMsgUserIcon, R.drawable.default_user_head);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            tvMsgContent.setText(json.json_ok("attach").json_ok("data").str("msg"));
        }
    }

    public class RecommendViewHolder extends BaseViewHolder {

        RecyclerView recyclerView;

        public RecommendViewHolder(View view) {
            super(view);

            recyclerView = view.findViewById(R.id.rv_recommend);

            LinearLayoutManager horizontalManager = new RecyclerViewNoBugLinearLayoutManager(mContext);
            horizontalManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            recyclerView.setLayoutManager(horizontalManager);
            if (hotRecommendAdapter != null) {
                recyclerView.setAdapter(hotRecommendAdapter);
                view.setVisibility(View.VISIBLE);
            } else {
                view.setVisibility(View.GONE);
            }
            recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
                int mSpace = 30;

                @Override
                public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                    super.getItemOffsets(outRect, view, parent, state);

                    outRect.right = mSpace;
                    if (parent.getChildAdapterPosition(view) == 0) {
                        outRect.left = DensityUtil.dp2px(16);
                    } else {
                        outRect.left = mSpace;
                    }
                }
            });
            //移除焦点
            recyclerView.setFocusableInTouchMode(false);
            recyclerView.requestFocus();
        }
    }

    public class SortMenuViewHolder extends BaseViewHolder {

        @BindView(R.id.tv_new_more)
        TextView tvListItemMore;
        @BindView(R.id.gv_list_item_sort)
        GridView gvListItemSort;
        @BindView(R.id.rl_new_user_title_bg)
        View newUserBg;
        @BindView(R.id.tv_hot_more)
        View tvHotMore;

        public SortMenuViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            if (sortMenuAdapter != null) {
                gvListItemSort.setAdapter(sortMenuAdapter);
            }

            tvListItemMore.setOnClickListener(v -> SortListActivity.start(mContext, 27, "新秀"));
            tvHotMore.setOnClickListener(more -> SortListActivity.start(mContext, -1));
        }
    }

    public class HotItemViewHolder extends BaseViewHolder {
        View view;
        @BindView(R.id.tvRoomName)
        TextView tvRoomName;

        @BindView(R.id.imvRunning)
        ImageView imvRunning;

        @BindView(R.id.tvRoomInfo)
        TextView tvRoomInfo;

        @BindView(R.id.tvOnLineCount)
        TextView tvOnLineCount;

        @BindView(R.id.imvIcon)
        ImageView imvIcon;

        @BindView(R.id.imvMark)
        ImageView imvMark;

        @BindView(R.id.imvTag)
        ImageView imvTag;

        public HotItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            this.view = view;
        }
    }

    private void setRecommendVData(RecommendViewHolder holder, Json json) {

    }

    private void setHotData(HotItemViewHolder holder, Json json) {
        if (GlideContextCheckUtil.checkContextUsable(mContext)) {
            ImageLoadUtils.loadBannerRoundBg(mContext, json.str("avatar"),
                    holder.imvIcon, DisplayUtility.dp2px(mContext, 8));
        }

        holder.tvRoomName.setText(json.str("title"));
        holder.tvOnLineCount.setText(json.str("onlineNum"));
        holder.tvRoomInfo.setText(json.str("roomTag"));
        ImageLoadUtils.loadImageWidthLimitView(mContext, json.str("badge"), holder.imvTag,tagHeight);
        GlideApp.with(mContext).asGif().load(R.drawable.anim_game_home_room_running).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.imvRunning);
        long uid = json.num_l("uid");
        int type = RoomInfo.ROOMTYPE_HOME_PARTY;
        if (json.has("type")) {
            type = json.num("type");
        }
        final int roomType = type;
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RoomServiceScheduler.getInstance().enterRoom(mContext, uid, roomType);
                if (hotRoomType == HotRoomType.recommend_room) {
                    //热门推荐-剔除前面两个大推荐位后的
                    LogUtils.d(TAG, "onClick-热门推荐-剔除前面两个大推荐位后的");
                    StatisticManager.get().onEvent(mContext,
                            StatisticModel.EVENT_ID_HOT_RECOMMEND_ROOM,
                            StatisticModel.getInstance().getUMAnalyCommonMap(mContext));
                } else if (hotRoomType == HotRoomType.hot_room) {
                    //火热房间-非绿色房间
                    LogUtils.d(TAG, "onClick-火热房间-非绿色房间");
                    StatisticManager.get().onEvent(mContext,
                            StatisticModel.EVENT_ID_HOT_ROOM,
                            StatisticModel.getInstance().getUMAnalyCommonMap(mContext));
                }
            }
        });
    }
}
