package com.yuhuankj.tmxq.ui.audio.activity;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

public interface MusicLibraryView<T extends AbstractMvpPresenter> extends IMvpBaseView {

    void onMusicPlayReport(boolean isSuccess, String msg);
}
