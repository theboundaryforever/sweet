package com.yuhuankj.tmxq.ui.nim.game;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.netease.nim.uikit.common.util.sys.NetworkUtil;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.xchat_core.bean.WebViewStyle;
import com.tongdaxing.xchat_core.minigame.MiniGameModel;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liaoxy
 * @Description:小游戏列表页
 * @date 2019/1/3 11:39
 */
public class InvitationActivity extends BaseActivity {
    private List<GameEnitity> games;
    private RecyclerView rcvGame;
    private GameAdapter adapter;
    private RelativeLayout rlInvitation;
    private SwipeRefreshLayout sfRoot;
    private MiniGameModel miniGameModel;
    private String TAG = "InvitationActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_minigame_invitation);
        initTitleBar("一起玩");
        initView();
        initData();
    }

    private void initView() {
        rcvGame = (RecyclerView) findViewById(R.id.rcvGame);
        rlInvitation = (RelativeLayout) findViewById(R.id.rlInvitation);
        rlInvitation.setOnClickListener(this);
        sfRoot = (SwipeRefreshLayout) findViewById(R.id.sfRoot);
        sfRoot.setOnRefreshListener(onRefreshListener);
    }

    private void initData() {
        GridLayoutManager layoutmanager = new GridLayoutManager(this, 3);
        rcvGame.setLayoutManager(layoutmanager);
        games = new ArrayList<>();

        miniGameModel = new MiniGameModel();
        miniGameModel.getGames(new OkHttpManager.MyCallBack<ServiceResult<List<GameEnitity>>>() {

            @Override
            public void onError(Exception e) {
                SingleToastUtil.showToast("获取游戏列表失败");
            }

            @Override
            public void onResponse(ServiceResult<List<GameEnitity>> response) {
                if (!response.isSuccess()) {
                    SingleToastUtil.showToast(response.getErrorMessage());
                    return;
                }
                games.clear();
                games.addAll(response.getData());
                adapter.notifyDataSetChanged();
            }
        });

        adapter = new GameAdapter(R.layout.item_minigame_invitation, games);
        adapter.setOnItemClickListener(onItemClickListener);
        rcvGame.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view == rlInvitation) {
            startActivity(new Intent(this, InvitationFriendsActivity.class));
            //邀请好友
            StatisticManager.get().onEvent(this,
                    StatisticModel.EVENT_ID_PLAY_TOGETHER_GAMELIST_INVITE,
                    StatisticModel.getInstance().getUMAnalyCommonMap(this));
        }
    }

    private BaseQuickAdapter.OnItemClickListener onItemClickListener = new BaseQuickAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
            if (games != null && games.size() > position) {
                if (CommonWebViewActivity.isGameRunning) {
                    LogUtils.e(TAG, "game is running");
                    return;
                }
                if (position < 0 || position >= games.size()) {
                    LogUtils.e(TAG, "game size  is error");
                    return;
                }
                if (games.get(position) == null) {
                    SingleToastUtil.showToast("game is null");
                    return;
                }
                String url = games.get(position).getUrl();
                if (TextUtils.isEmpty(url)) {
                    SingleToastUtil.showToast("游戏链接无效");
                    return;
                }
                CommonWebViewActivity.isGameRunning = true;
                CommonWebViewActivity.start(InvitationActivity.this, url + "#/loading", WebViewStyle.NO_TITLE);
                if(0 == position){
                    //进入游戏
                    StatisticManager.get().onEvent(InvitationActivity.this,
                            StatisticModel.EVENT_ID_PLAY_TOGETHER_GAMELIST_1,
                            StatisticModel.getInstance().getUMAnalyCommonMap(InvitationActivity.this));
                }else if(1 == position){
                    //进入游戏
                    StatisticManager.get().onEvent(InvitationActivity.this,
                            StatisticModel.EVENT_ID_PLAY_TOGETHER_GAMELIST_2,
                            StatisticModel.getInstance().getUMAnalyCommonMap(InvitationActivity.this));
                }else if (2 == position){
                    //进入游戏
                    StatisticManager.get().onEvent(InvitationActivity.this,
                            StatisticModel.EVENT_ID_PLAY_TOGETHER_GAMELIST_3,
                            StatisticModel.getInstance().getUMAnalyCommonMap(InvitationActivity.this));
                }


            }
        }
    };

    private SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            if (NetworkUtil.isNetAvailable(getApplicationContext())) {
                miniGameModel.getGames(new OkHttpManager.MyCallBack<ServiceResult<List<GameEnitity>>>() {

                    @Override
                    public void onError(Exception e) {
                        sfRoot.setRefreshing(false);
                        SingleToastUtil.showToast("获取游戏列表失败");
                    }

                    @Override
                    public void onResponse(ServiceResult<List<GameEnitity>> response) {
                        sfRoot.setRefreshing(false);
                        if (!response.isSuccess()) {
                            SingleToastUtil.showToast(response.getErrorMessage());
                            return;
                        }
                        games.clear();
                        games.addAll(response.getData());
                        adapter.notifyDataSetChanged();
                    }
                });
            } else {
                sfRoot.setRefreshing(false);
            }
        }
    };
}
