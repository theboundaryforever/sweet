package com.yuhuankj.tmxq.ui.audio.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.juxiao.library_utils.DisplayUtils;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.TouchAndClickOperaUtils;
import com.tongdaxing.xchat_core.player.IPlayerCore;
import com.tongdaxing.xchat_core.player.IPlayerCoreClient;
import com.tongdaxing.xchat_core.player.bean.LocalMusicInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.audio.activity.MusicLibraryActivity;

import java.util.List;

import static com.tongdaxing.xchat_core.player.IPlayerCore.PLAY_STATUS_MUSIC_LIST_EMPTY;

/**
 * 音乐播放入口
 * Created by chenran on 2017/10/28.
 */

public class MusicPlayerView extends FrameLayout implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {
    private LinearLayout ll_music_library;
    private RelativeLayout musicBoxLayout;
    private ImageView packUp;
    private ImageView musicListMore;
    private ImageView musicPlayPause;
    private ImageView nextBtn;
    private SeekBar volumeSeekBar;
    private TextView musicName;
    private String imageBg;
    private View rootView;

    private TouchAndClickOperaUtils touchAndClickOperaUtils;

    private int roomType = RoomInfo.ROOMTYPE_HOME_PARTY;

    public void setRoomType(int roomType) {
        this.roomType = roomType;
        //单人音频房间需要重置布局属性
        resetMusicBtnLayoutParams(roomType);
    }

    public MusicPlayerView(Context context) {
        super(context);
        init(context);
    }

    public MusicPlayerView(Context context, AttributeSet attr) {
        super(context, attr);
        init(context);
    }

    public MusicPlayerView(Context context, AttributeSet attr, int i) {
        super(context, attr, i);
        init(context);
    }

    private void init(Context context) {
        touchAndClickOperaUtils = new TouchAndClickOperaUtils(context);
        CoreManager.addClient(this);
        LayoutInflater.from(getContext()).inflate(R.layout.layout_music_player_view, this, true);
        rootView = findViewById(R.id.fl_root);
        rootView.setOnClickListener(this);
        rootView.setClickable(false);

        ll_music_library = findViewById(R.id.ll_music_library);
        ll_music_library.setOnClickListener(this);
        resetMusicBtnLayoutParams(roomType);

        packUp = findViewById(R.id.pack_up);
        packUp.setOnClickListener(this);
        musicBoxLayout = findViewById(R.id.music_box_layout);
        touchAndClickOperaUtils.setTargetView(musicBoxLayout);
        touchAndClickOperaUtils.setMarginButtom(DisplayUtils.dip2px(musicBoxLayout.getContext(), 40));
        musicListMore = findViewById(R.id.music_list_more);
        musicListMore.setOnClickListener(this);

        musicPlayPause = findViewById(R.id.music_play_pause);
        musicPlayPause.setOnClickListener(this);
        volumeSeekBar = findViewById(R.id.voice_seek);
        volumeSeekBar.setMax(100);
        try {
            volumeSeekBar.setProgress(CoreManager.getCore(IPlayerCore.class).getCurrentVolume());
        } catch (Exception e) {
            e.printStackTrace();
        }
        volumeSeekBar.setOnSeekBarChangeListener(this);
        musicName = findViewById(R.id.music_name);
        nextBtn = findViewById(R.id.music_play_next);
        nextBtn.setOnClickListener(this);
        updateView();
    }

    private void resetMusicBtnLayoutParams(int roomType) {
        if (ll_music_library == null) {
            return;
        }
        //单人音频房间需要重置布局属性
        if (roomType == RoomInfo.ROOMTYPE_SINGLE_AUDIO) {
            LayoutParams lp = (LayoutParams) ll_music_library.getLayoutParams();
            lp.topMargin = DisplayUtils.dip2px(getContext(), 100);
            lp.rightMargin = DisplayUtils.dip2px(getContext(), 18);
            ll_music_library.setLayoutParams(lp);
        } else if (roomType == RoomInfo.ROOMTYPE_MULTI_AUDIO || roomType == RoomInfo.ROOMTYPE_NEW_AUCTION) {
            LayoutParams lp = (LayoutParams) ll_music_library.getLayoutParams();
            lp.topMargin = DisplayUtils.dip2px(getContext(), 85);
            lp.rightMargin = DisplayUtils.dip2px(getContext(), 90);
            ll_music_library.setLayoutParams(lp);
        }
    }

    public void setImageBg(String imageBg) {
        this.imageBg = imageBg;
    }

    public void showFlagInAnim() {
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(musicBoxLayout, "translationX",
                (DisplayUtils.getScreenWidth(getContext()) - DisplayUtils.dip2px(getContext(), 287)) / 2,
                DisplayUtils.dip2px(getContext(), 287)).setDuration(150);
        objectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        objectAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                musicBoxLayout.setVisibility(GONE);
            }
        });
        objectAnimator.start();
    }

    private void showBoxInAnim() {
        ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(musicBoxLayout, "translationX",
                DisplayUtils.getScreenWidth(getContext()),
                (DisplayUtils.getScreenWidth(getContext()) - DisplayUtils.dip2px(getContext(), 287)) / 2).setDuration(150);
        objectAnimator1.setInterpolator(new AccelerateDecelerateInterpolator());
        objectAnimator1.setStartDelay(150);
        objectAnimator1.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                musicBoxLayout.setVisibility(VISIBLE);
            }
        });
        objectAnimator1.start();
    }

    public void updateVoiceValue() {
        volumeSeekBar.setProgress(CoreManager.getCore(IPlayerCore.class).getCurrentVolume());
    }

    public void release() {
        CoreManager.removeClient(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_music_library:
                rootView.setClickable(true);
                showBoxInAnim();
                //房间-打开播放音乐
                StatisticManager.get().onEvent(getContext(),
                        StatisticModel.EVENT_ID_ROOM_OPEN_MUSIC_CONTROL_PANEL,
                        StatisticModel.getInstance().getUMAnalyCommonMap(getContext()));
                break;
            case R.id.fl_root:
                rootView.setClickable(false);
                showFlagInAnim();
                break;
            case R.id.music_list_more:
                MusicLibraryActivity.start(getContext(), imageBg);
                break;
            case R.id.music_play_pause:
                List<LocalMusicInfo> localMusicInfoList = CoreManager.getCore(IPlayerCore.class).getPlayerListMusicInfos();
                if (localMusicInfoList != null && localMusicInfoList.size() > 0) {
                    int state = CoreManager.getCore(IPlayerCore.class).getState();
                    if (state == IPlayerCore.STATE_PLAY) {
                        CoreManager.getCore(IPlayerCore.class).pause();
                    } else if (state == IPlayerCore.STATE_PAUSE) {
                        CoreManager.getCore(IPlayerCore.class).play(CoreManager.getCore(IPlayerCore.class).getCurrLocalMusicInfo());
                    } else {
                        int result = CoreManager.getCore(IPlayerCore.class).play(null);
                        if (result < 0) {
                            if (result == PLAY_STATUS_MUSIC_LIST_EMPTY) {
                                ((BaseMvpActivity) getContext()).toast(R.string.music_play_failed_list_empty);
                            } else {
                                ((BaseMvpActivity) getContext()).toast(R.string.music_play_failed_file_format_error);
                            }
                        }
                    }
                } else {
                    MusicLibraryActivity.start(getContext(), imageBg);
                }
                break;
            case R.id.music_play_next:
                List<LocalMusicInfo> localMusicInfoList1 = CoreManager.getCore(IPlayerCore.class).getPlayerListMusicInfos();
                if (localMusicInfoList1 != null && localMusicInfoList1.size() > 0) {
                    int result = CoreManager.getCore(IPlayerCore.class).playNext();
                    if (result < 0) {
                        if (result == -3) {
                            ((BaseMvpActivity) getContext()).toast(R.string.music_play_failed_list_empty);
                        } else {
                            ((BaseMvpActivity) getContext()).toast(R.string.music_play_failed_file_format_error);
                        }
                    }
                } else {
                    MusicLibraryActivity.start(getContext(), imageBg);
                }
                break;
            default:
        }
    }

    private void updateView() {
        if (CoreManager.getCore(IPlayerCore.class) != null) {
            LocalMusicInfo current = CoreManager.getCore(IPlayerCore.class).getCurrLocalMusicInfo();
            if (current != null && CoreManager.getCore(IPlayerCore.class).getState() != IPlayerCore.STATE_STOP) {
                musicName.setText(current.getSongName());
                int state = CoreManager.getCore(IPlayerCore.class).getState();
                if (state == IPlayerCore.STATE_PLAY) {
                    musicPlayPause.setImageResource(R.drawable.icon_music_player_play);
                } else {
                    musicPlayPause.setImageResource(R.drawable.icon_music_player_pause);
                }
            } else {
                musicName.setText(musicName.getContext().getResources().getString(R.string.music_play_empty));
                musicPlayPause.setImageResource(R.drawable.icon_music_player_pause);
            }
        }
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicPlaying(LocalMusicInfo localMusicInfo) {
        updateView();
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicPause(LocalMusicInfo localMusicInfo) {
        updateView();

    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicStop() {
        updateView();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        CoreManager.getCore(IPlayerCore.class).seekVolume(progress);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
