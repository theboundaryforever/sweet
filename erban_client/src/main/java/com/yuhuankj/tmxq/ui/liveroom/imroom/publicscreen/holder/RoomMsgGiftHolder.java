package com.yuhuankj.tmxq.ui.liveroom.imroom.publicscreen.holder;

import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;

import com.juxiao.library_utils.DisplayUtils;
import com.netease.nim.uikit.common.ui.widget.UrlImageSpan;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.gift.MultiGiftReceiveInfo;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.FaceAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.GiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.LotteryBoxAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.MultiGiftAttachment;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMessage;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.face.FaceInfo;
import com.tongdaxing.xchat_core.room.face.FaceReceiveInfo;
import com.tongdaxing.xchat_core.room.face.IFaceCore;
import com.tongdaxing.xchat_core.utils.BaseConstant;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.imroom.publicscreen.base.RoomMsgBaseHolder;
import com.yuhuankj.tmxq.utils.StringUtil;

import java.util.List;

/**
 * 文件描述：礼物消息holder类
 *
 * @auther：zwk
 * @data：2019/5/7
 */
public class RoomMsgGiftHolder extends RoomMsgBaseChatHolder {
    int giftWidth = 50;

    public RoomMsgGiftHolder(View itemView) {
        super(itemView);
        if (itemView != null && itemView.getContext() != null) {
            giftWidth = DisplayUtils.dip2px(itemView.getContext(), 22);
        }
    }

    @Override
    protected void onDataTransformContent(RoomMsgBaseHolder holder, IMRoomMessage message, int position, TextView etvContent) {
        if (message.getAttachment() instanceof GiftAttachment) {
            setMsgHeaderGift(etvContent, (GiftAttachment) message.getAttachment());
        } else if (message.getAttachment() instanceof MultiGiftAttachment) {
            setMsgMultiGift(etvContent, (MultiGiftAttachment) message.getAttachment());
        } else if (message.getAttachment() instanceof LotteryBoxAttachment) {
            setLotteryInfo((LotteryBoxAttachment) message.getAttachment(), etvContent);
        } else if (message.getAttachment() instanceof FaceAttachment) {
            setMsgRoomFace(etvContent, (FaceAttachment) message.getAttachment());
        } else {
            etvContent.setText("不支持的礼物消息类型");
        }
    }

    /**
     * 设置赠送单个礼物消息样式
     *
     * @param tvContent
     * @param attachment
     */
    private void setMsgHeaderGift(TextView tvContent, GiftAttachment attachment) {
        GiftReceiveInfo giftRecieveInfo = attachment.getGiftRecieveInfo();
        if (giftRecieveInfo != null) {
            String nick = StringUtil.limitStr(tvContent.getContext(), giftRecieveInfo.getNick(), 4);
            String targetNick = giftRecieveInfo.getTargetNick();
            RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
            if (RoomDataManager.get().isRoomOwner(giftRecieveInfo.getTargetUid()) && null != roomInfo
                    && roomInfo.getType() == RoomInfo.ROOMTYPE_SINGLE_AUDIO) {
                targetNick = "主播";
            }
            targetNick = StringUtil.limitStr(tvContent.getContext(), targetNick, 4);

            String content = nick.concat(" 送给 ").concat(targetNick + " ");
            SpannableStringBuilder builder = new SpannableStringBuilder(content);
            builder.setSpan(new ForegroundColorSpan(0xFFFFDA81),
                    0, content.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);

            //插入小图标占位符
            builder.append(BaseConstant.GIFT_PLACEHOLDER);
            GiftInfo giftInfo = CoreManager.getCore(IGiftCore.class).findGiftInfoById(giftRecieveInfo.getGiftId());
            UrlImageSpan imageSpan = new UrlImageSpan(tvContent.getContext(), R.drawable.ic_room_gift_msg_holder, giftInfo != null ? giftInfo.getGiftUrl() : "", tvContent);
            imageSpan.setImgWidth(giftWidth);
            imageSpan.setImgHeight(giftWidth);
            builder.setSpan(imageSpan, builder.toString().length() - BaseConstant.GIFT_PLACEHOLDER.length(),
                    builder.toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if (attachment.getSecond() == CustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_GIFT) {
                builder.append(" ×").append(String.valueOf(giftRecieveInfo.getGiftNum()));
            } else {
                builder.append(" ×").append(String.valueOf(giftRecieveInfo.getComboRangEnd()));
            }

            builder.setSpan(new ForegroundColorSpan(0xFFFFDA81),
                    0, content.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            tvContent.setText(builder);
        } else {
            tvContent.setText("不支持的礼物消息类型");
        }
    }


    /**
     * 设置全麦礼物消息样式
     *
     * @param tvContent
     * @param attachment
     */
    private void setMsgMultiGift(TextView tvContent, MultiGiftAttachment attachment) {
        MultiGiftReceiveInfo multiGiftRecieveInfo = attachment.getMultiGiftRecieveInfo();
        if (multiGiftRecieveInfo != null) {
            String nick = StringUtil.limitStr(tvContent.getContext(), multiGiftRecieveInfo.getNick(), 5);
            String content = nick.concat(" 全麦送出 ");
            SpannableStringBuilder builder = new SpannableStringBuilder(content);
            //插入小图标占位符
            builder.append(BaseConstant.GIFT_PLACEHOLDER);
            GiftInfo giftInfo = CoreManager.getCore(IGiftCore.class).findGiftInfoById(multiGiftRecieveInfo.getGiftId());
            UrlImageSpan imageSpan = new UrlImageSpan(tvContent.getContext(), R.drawable.ic_room_gift_msg_holder, giftInfo != null ? giftInfo.getGiftUrl() : "", tvContent);
            imageSpan.setImgWidth(giftWidth);
            imageSpan.setImgHeight(giftWidth);
            builder.setSpan(imageSpan, builder.toString().length() - BaseConstant.GIFT_PLACEHOLDER.length(),
                    builder.toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if (attachment.getSecond() == CustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_MULTI_GIFT) {
                builder.append(" ×").append(String.valueOf(multiGiftRecieveInfo.getGiftNum()));
            } else {
                builder.append(" ×").append(String.valueOf(multiGiftRecieveInfo.getComboRangEnd()));
            }

            builder.setSpan(new ForegroundColorSpan(0xFFFFDA81),
                    0, content.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            tvContent.setText(builder);
        } else {
            tvContent.setText("不支持的礼物消息类型");
        }
    }


    /***
     * 宝箱抽奖
     */
    private void setLotteryInfo(LotteryBoxAttachment attachment, TextView tvContent) {
        //恭喜萌新小可爱抽出瞌睡熊礼物 x66
        String content = "恭喜".concat(attachment.getNick()).concat("抽出")
                .concat(attachment.getGiftName().concat("礼物 "));
        SpannableStringBuilder builder = new SpannableStringBuilder(content);
        //插入小图标占位符
        builder.append(BaseConstant.GIFT_PLACEHOLDER);
        UrlImageSpan imageSpan = new UrlImageSpan(tvContent.getContext(), R.drawable.ic_room_gift_msg_holder, attachment.getGiftUrl(), tvContent);
        imageSpan.setImgWidth(giftWidth);
        imageSpan.setImgHeight(giftWidth);
        builder.setSpan(imageSpan, builder.toString().length() - BaseConstant.GIFT_PLACEHOLDER.length(),
                builder.toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(" ×").append(String.valueOf(attachment.getCount()));
        builder.setSpan(new ForegroundColorSpan(0xFFFFDA81),
                0, content.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        tvContent.setText(builder);
    }


    /**
     * 设置房间有表情的消息
     *
     * @param tvContent
     * @param faceAttachment
     */
    private void setMsgRoomFace(TextView tvContent, FaceAttachment faceAttachment) {
        List<FaceReceiveInfo> faceReceiveInfos = faceAttachment.getFaceReceiveInfos();
        if (!ListUtils.isListEmpty(faceReceiveInfos)) {
            String content = "出";
            FaceReceiveInfo faceReceiveInfo = faceReceiveInfos.get(0);
            List<Integer> resultIndexes = faceReceiveInfos.get(0).getResultIndexes();
            FaceInfo faceInfo = CoreManager.getCore(IFaceCore.class).findFaceInfoById(faceReceiveInfo.getFaceId());
            if (faceReceiveInfo.getResultIndexes().size() <= 0 || faceInfo == null) {
                return;
            }
            SpannableStringBuilder builder = new SpannableStringBuilder(content);
            for (Integer index : resultIndexes) {
                //插入小图标占位符
                builder.append(BaseConstant.GIFT_PLACEHOLDER);
                String faceUrl = faceInfo.getFacePath(index);
                UrlImageSpan imageSpan = new UrlImageSpan(tvContent.getContext(), R.drawable.ic_room_gift_msg_holder, faceUrl != null ? faceUrl : "", tvContent);
                imageSpan.setImgWidth(giftWidth);
                imageSpan.setImgHeight(giftWidth);
                builder.setSpan(imageSpan, builder.toString().length() - BaseConstant.GIFT_PLACEHOLDER.length(),
                        builder.toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            builder.setSpan(new ForegroundColorSpan(0xFFFFDA81),
                    0, content.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            tvContent.setText(builder);
        } else {
            tvContent.setText("不支持的礼物消息类型");
        }
    }
}
