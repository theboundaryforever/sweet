package com.yuhuankj.tmxq.ui.home.game;

import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.nim.game.GameEnitity;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

/**
 * @author liaoxy
 * @Description:一起玩横向滚动数据adapter
 * @date 2019/2/15 10:11
 */
public class PlayTogetherAdapter extends BaseQuickAdapter<GameEnitity, BaseViewHolder> {

    private int imgWidthAndHeight = 0;

    public PlayTogetherAdapter(List<GameEnitity> data) {
        super(R.layout.item_game_home_games, data);
        int screenW = ScreenUtil.getDisplayWidth();
        imgWidthAndHeight = (screenW - (DisplayUtility.dp2px(mContext, 15) * 2) - (DisplayUtility.dp2px(mContext, 10) * 2)) / 3;
        LogUtils.d(TAG,"PlayTogetherAdapter-imgWidthAndHeight:"+imgWidthAndHeight);
    }

    @Override
    protected void convert(BaseViewHolder helper, GameEnitity item) {
        ImageView imvIcon = helper.getView(R.id.imvIcon);
        RelativeLayout.LayoutParams imvIconLp = (RelativeLayout.LayoutParams) imvIcon.getLayoutParams();
        imvIconLp.width = imgWidthAndHeight;
        imvIconLp.height = imgWidthAndHeight;
        imvIcon.setLayoutParams(imvIconLp);

        TextView tvName = helper.getView(R.id.tvName);
        TextView tvOnLineCount = helper.getView(R.id.tvOnLineCount);
        tvName.setText(item.getName());
        tvOnLineCount.setText(mContext.getResources().getString(R.string.game_play_online_tips, item.getPlayers()));
        LogUtils.d(TAG,"convert-bgName:"+item.getName()+" bgUrl:"+item.getBgImage());
        ImageLoadUtils.loadBannerRoundBackground(mContext, ImageLoadUtils.toThumbnailUrl(imgWidthAndHeight, imgWidthAndHeight, item.getBgImage()), imvIcon);
        helper.addOnClickListener(R.id.imvIcon);
    }
}