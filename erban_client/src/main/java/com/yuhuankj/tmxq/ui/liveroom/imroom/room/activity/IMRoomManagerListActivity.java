package com.yuhuankj.tmxq.ui.liveroom.imroom.room.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMember;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter.RoomManagerListAdapter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter.IMRoomManagerPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IRoomBlackListView;

import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

/**
 * 房间管理员列表
 *
 * @author chenran
 * @date 2017/10/11
 */
@CreatePresenter(IMRoomManagerPresenter.class)
public class IMRoomManagerListActivity extends BaseMvpActivity<IRoomBlackListView, IMRoomManagerPresenter>
        implements RoomManagerListAdapter.OnRoomManagerListOperationClickListener, IRoomBlackListView {
    private TextView count;
    private RecyclerView recyclerView;
    private RoomManagerListAdapter normalListAdapter;

    public static void start(Context context) {
        Intent intent = new Intent(context, IMRoomManagerListActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_manager_list);
        initTitleBar("管理员");
        initView();

        showLoading();
        loadData();
    }

    private void loadData() {
        RoomInfo roomInfo = RoomServiceScheduler.getCurrentRoomInfo();
        if (null != roomInfo) {
            getMvpPresenter().queryRoomManagerList(100, roomInfo.getRoomId());
        }
    }

    private void initView() {
        count = (TextView) findViewById(R.id.count);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        normalListAdapter = new RoomManagerListAdapter(this);
        normalListAdapter.setListOperationClickListener(this);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerView.setAdapter(normalListAdapter);
    }

    @Override
    public void onRemoveOperationClick(final IMRoomMember imRoomMember) {
        getDialogManager().showOkCancelDialog("是否将" + imRoomMember.getNick() + "移除管理员列表？",
                true, new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onOk() {
                        RoomInfo roomInfo = RoomServiceScheduler.getCurrentRoomInfo();
                        if (roomInfo != null) {
                            getMvpPresenter().removeUserFromManagerList(
                                    imRoomMember.getAccount(), roomInfo.getRoomId());
                        }
                    }
                });
    }

    @Override
    public View.OnClickListener getLoadListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoading();
                loadData();
            }
        };
    }

    private void dealRemoveAdminResponse(String account) {
        if (TextUtils.isEmpty(account)) {
            return;
        }
        List<IMRoomMember> list = normalListAdapter.getRoomMemberList();
        if (!ListUtils.isListEmpty(list)) {
            hideStatus();
            ListIterator<IMRoomMember> iterator = list.listIterator();
            for (; iterator.hasNext(); ) {
                if (Objects.equals(iterator.next().getAccount(), account)) {
                    iterator.remove();
                }
            }
            normalListAdapter.notifyDataSetChanged();
            count.setText("管理员" + list.size() + "人");
            if (list.size() == 0) {
                showNoData("暂没有设置管理员");
            }
        } else {
            showNoData("暂没有设置管理员");
            count.setText("管理员0人");
        }
        toast("操作成功");
    }

    @Override
    public void showRoomBlackList(List<IMRoomMember> imRoomMembers) {
        hideStatus();
        if (imRoomMembers != null && imRoomMembers.size() > 0) {
            normalListAdapter.setRoomMemberList(imRoomMembers);
            normalListAdapter.notifyDataSetChanged();
            count.setText("管理员" + imRoomMembers.size() + "人");
        } else {
            showNoData("暂没有设置管理员");
            count.setText("管理员0人");
        }
    }

    @Override
    public void removeUserFromBlackList(String account) {
        dealRemoveAdminResponse(account);
    }

    @Override
    public void showErrMsg(String message) {
        if (!TextUtils.isEmpty(message)) {
            toast(message);
        }
    }

    @Override
    public void showListEmptyView() {
        showNetworkErr();
    }
}
