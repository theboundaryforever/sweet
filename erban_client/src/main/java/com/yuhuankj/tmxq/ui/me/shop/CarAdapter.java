package com.yuhuankj.tmxq.ui.me.shop;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.juxiao.library_utils.DisplayUtils;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/3/30.
 */

public class CarAdapter extends BaseQuickAdapter<Json, CarAdapter.ViewHolder> {

    private final Context context;
    private int selectIndex = -1;
    //判断是否有选择过
    public String selectId = "";
    public boolean isShowSvga = true;
    public String prizeParamasName = "";
    public int carHeight = 170;


    public CarAdapter(Context context) {
        super(R.layout.item_car_select);
        this.context = context;
        carHeight = DisplayUtils.dip2px(context,85);
    }

    public void setVggAction(VggAction vggAction) {
        this.vggAction = vggAction;
    }

    private VggAction vggAction;

    public interface VggAction {
        void showVgg(String url);
    }

    private PayAction payAction;

    public void setPayAction(PayAction payAction) {
        this.payAction = payAction;
    }

    public interface PayAction {
        //type 1 是购买，2是续费
        void buyCar(int type, String carId, String carName, int nobleId);
    }


    @Override
    public void setNewData(@Nullable List<Json> data) {
        super.setNewData(data);
        for (int i = 0; i < data.size(); i++) {
            Json json = data.get(i);
            if (json.num("isPurse") == 2) {
                selectIndex = i;
                break;
            }
        }
        selectId = "";
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        Json item = getData().get(position);

        int isPurse = item.num("isPurse", 0);
        String vggUrl = item.str("vggUrl");

        holder.tvCarType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPurse == 0) {

                    if (vggAction != null) {
                        vggAction.showVgg(vggUrl);
                    }
                } else if (isPurse == 1) {
                    selectId = item.str(prizeParamasName + "Id");
                    if (selectIndex > -1)
                        getData().get(selectIndex).set("isPurse", 1);
                    selectIndex = position;
                    item.set("isPurse", 2);

                } else if (isPurse == 2) {
                    item.set("isPurse", 1);
                    selectId = "-1";

                }
                notifyDataSetChanged();
            }
        });
    }

    @Override
    protected void convert(ViewHolder helper, Json item) {

        String carName = item.str(prizeParamasName + "Name");
        helper.tvCarName.setText(carName);

        ImageLoadUtils.loadImageWidthLimitView(context, item.str("picUrl"), helper.ivCarStyle,carHeight);
        //0未购买，1购买未选中，2选中
        int isPurse = item.num("isPurse", 0);
        helper.tvCarTime.setText(isPurse == 0 ? item.str("effectiveTime") + "天" : item.str("daysRemaining") + "天");
        String vggUrl = item.str("vggUrl");

        helper.ivCarStyle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPurse != 0) {
                    if (vggAction != null) {
                        if (isShowSvga) {
                            vggAction.showVgg(vggUrl);
                        }

                    }
                }
            }
        });
        if (item.str("goldPrice").equals("0")) {
            helper.tvCarTime.setVisibility(View.GONE);
            helper.buCarBuy.setVisibility(View.GONE);
            helper.tvCarPrice.setVisibility(View.GONE);
        } else {
            helper.tvCarTime.setVisibility(View.VISIBLE);
            helper.buCarBuy.setVisibility(View.VISIBLE);
            helper.tvCarPrice.setVisibility(View.VISIBLE);
        }


        try {
            //如果是甜豆抽奖获得的礼物则要显示日期
            if (item.has("isPea") && item.getInt("isPea") == 1) {
                helper.tvCarTime.setVisibility(View.VISIBLE);
            }
            //如果是萌新礼包等其他业务场景赠送的礼物，则要显示有效天数
            //type==1 表示赠送的礼物
            if (item.has("type") && item.getInt("type") == 1) {
                helper.tvCarTime.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }

        int nobleId = item.num("vipId");
        if (nobleId > 0 && item.has("vipIcon")) {
            helper.imvNobleIcon.setVisibility(View.VISIBLE);
            ImageLoadUtils.loadImage(context, item.str("vipIcon"), helper.imvNobleIcon);
        } else {
            helper.imvNobleIcon.setVisibility(View.GONE);
        }

        String carId = item.str(prizeParamasName + "Id");
        helper.buCarBuy.setBackgroundResource(R.drawable.shape_car_pay);
        if (isPurse == 0) {
            if (isShowSvga) {
                helper.tvCarType.setVisibility(View.VISIBLE);
                setDrawableInTv(helper.tvCarType, 0, " 点击试驾", Color.parseColor("#23CFB1"));
            } else {
                helper.tvCarType.setVisibility(View.GONE);
            }
        } else if (isPurse == 1) {
            helper.tvCarType.setVisibility(View.VISIBLE);
            setDrawableInTv(helper.tvCarType, R.mipmap.car_option_0, " 点击使用", Color.parseColor("#999999"));
        } else if (isPurse == 2) {
            helper.tvCarType.setVisibility(View.VISIBLE);
            setDrawableInTv(helper.tvCarType, R.mipmap.car_option_1, " 正在使用", Color.parseColor("#999999"));
        }

        helper.buCarBuy.setText(isPurse == 0 ? "购买" : "继续购买");

        helper.tvCarPrice.setText(item.str("goldPrice"));

        helper.buCarBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (payAction == null)
                    return;
                try {
                    UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
                    if (userInfo != null && userInfo.getVipId() < nobleId) {
                        //等级太小无法购买
                        SingleToastUtil.showToast("你的等级不足，不可以购买/续费该礼物");
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (isPurse == 0) {
                    payAction.buyCar(1, carId, carName, nobleId);
                } else if (isPurse == 1 || isPurse == 2) {
                    payAction.buyCar(2, carId, carName, nobleId);
                }
            }
        });

        helper.buCarBuy.setText(isPurse == 0 ? "购买" : "续期");
    }

    static class ViewHolder extends BaseViewHolder {
        @BindView(R.id.tv_car_time)
        TextView tvCarTime;
        @BindView(R.id.tv_car_type)
        TextView tvCarType;
        @BindView(R.id.iv_car_style)
        ImageView ivCarStyle;
        @BindView(R.id.tv_car_price)
        TextView tvCarPrice;
        @BindView(R.id.tv_car_name)
        TextView tvCarName;
        @BindView(R.id.bu_car_buy)
        Button buCarBuy;
        @BindView(R.id.imvNobleIcon)
        ImageView imvNobleIcon;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    private void setDrawableInTv(TextView textView, int id, String content, int color) {
        Drawable drawable = null;
        if (id != 0) {
            drawable = mContext.getResources().getDrawable(id);
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        }

        textView.setCompoundDrawables(drawable, null, null, null);
        textView.setText(content);
        textView.setTextColor(color);
    }
}
