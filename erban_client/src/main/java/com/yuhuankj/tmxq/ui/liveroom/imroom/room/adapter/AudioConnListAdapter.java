package com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter;

import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.xchat_core.liveroom.im.model.AudioConnTimeCounter;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.AudioConnReqInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

/**
 * Created by huangmeng1 on 2018/1/18.
 */

public class AudioConnListAdapter extends BaseQuickAdapter<AudioConnReqInfo, BaseViewHolder> {

    private final String TAG = AudioConnListAdapter.class.getSimpleName();
    private int imvIconWidthAndHeight;
    private boolean isAnchor = false;
    private long selfUid = 0L;
    private OnAudioConnItemClickListener onAudioConnItemClickListener;

    public AudioConnListAdapter() {
        super(R.layout.item_rv_audio_conn_list);
        imvIconWidthAndHeight = DisplayUtility.dp2px(mContext, 30);
    }

    public void setAnchor(boolean anchor) {
        isAnchor = anchor;
    }

    public void setSelfUid(long selfUid) {
        this.selfUid = selfUid;
    }

    public void setOnAudioConnItemClickListener(OnAudioConnItemClickListener listener) {
        this.onAudioConnItemClickListener = listener;
    }

    /**
     * Implement this method and use the helper to adapt the view to the given item.
     *
     * @param helper A fully initialized helper.
     * @param item   The item that needs to be displayed.
     */
    @Override
    protected void convert(BaseViewHolder helper, AudioConnReqInfo item) {

        TextView tvReqIndex = helper.getView(R.id.tvReqIndex);
        //暂时以id来排序，如果后端id跟前端概念不一致，则直接按照position来显示
        int position = getData().indexOf(item) + 1;
        tvReqIndex.setText(position >= 10 ? String.valueOf(position) : "0".concat(String.valueOf(position)));
        ImageView ivReqAvatar = helper.getView(R.id.ivReqAvatar);
        //如果是粉丝，则状态有等待中、连麦中、已结束
        //如果是主播，则状态有连接，连接中动效，挂断，已结束
        TextView tvConnStatus = helper.getView(R.id.tvConnStatus);
        tvConnStatus.setVisibility(View.GONE);
        tvConnStatus.setText(null);
        tvConnStatus.setTextColor(Color.parseColor("#999999"));

        TextView tvReqNick = helper.getView(R.id.tvReqNick);
        TextView tvReqReason = helper.getView(R.id.tvReqReason);
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) tvReqReason.getLayoutParams();

        TextView tvConnTime = helper.getView(R.id.tvConnTime);
        tvConnTime.setTag(null);
        tvConnTime.setText(null);
        tvConnTime.setVisibility(View.GONE);

        TextView tvConnAudio = helper.getView(R.id.tvConnAudio);
        tvConnAudio.setVisibility(View.GONE);
        tvConnAudio.setOnClickListener(null);

        TextView tvDownAudio = helper.getView(R.id.tvDownAudio);
        tvDownAudio.setVisibility(View.GONE);
        tvDownAudio.setOnClickListener(null);

        ImageView ivConningAnim = helper.getView(R.id.ivConningAnim);
        ivConningAnim.setVisibility(View.GONE);

        if (GlideContextCheckUtil.checkContextUsable(mContext)) {
            lp.bottomMargin = DisplayUtility.dp2px(mContext, 10);

            GlideApp.with(mContext).load(ImageLoadUtils.toThumbnailUrl(imvIconWidthAndHeight,
                    imvIconWidthAndHeight, item.getAvatar()))
                    .dontAnimate()
                    .error(R.drawable.bg_default_cover_round_placehold_15)
                    .transforms(new CircleCrop())
                    .into(ivReqAvatar);

            if (item.getApplyStatus() == AudioConnReqInfo.ApplyStatus.Conning) {
                //处于连麦中
                if (isAnchor) {
                    //对于主播，显示挂断
                    tvDownAudio.setVisibility(View.VISIBLE);
                    tvDownAudio.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (null != onAudioConnItemClickListener) {
                                onAudioConnItemClickListener.onCallDownConnClick(item.getUid());
                            }
                        }
                    });
                } else {
                    //对于非主播，显示连麦中
                    tvConnStatus.setVisibility(View.VISIBLE);
                    tvConnStatus.setText(mContext.getResources().getString(R.string.audio_connect_status_conning));
                    tvConnStatus.setTextColor(Color.parseColor("#FF9563"));
                }

                //只有处于“连麦中” 并且在麦上的用户和主播显示,也即用户只能看到自己的，主播能看到所有连麦中的
                if (item.getUid() == selfUid || RoomDataManager.get().isRoomOwner()) {
                    tvConnTime.setVisibility(View.VISIBLE);
                    lp.bottomMargin = DisplayUtility.dp2px(mContext, 4);
                    AudioConnTimeCounter.getInstance().start(item, tvConnTime);
                }

            } else if (item.getApplyStatus() == AudioConnReqInfo.ApplyStatus.Reqing) {
                //申请请求未连接状态
                if (isAnchor) {
                    //对于主播，显示连接
                    tvConnAudio.setVisibility(View.VISIBLE);
                    tvConnAudio.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (null != onAudioConnItemClickListener) {
                                onAudioConnItemClickListener.onRecvConnReqClick(item.getUid());
                            }
                            //主播点击连接按钮，到下一次刷新列表中间的请求过程，显示连接中动画特效
                            ivConningAnim.setVisibility(View.VISIBLE);
                            if (GlideContextCheckUtil.checkContextUsable(mContext)) {
                                GlideApp.with(mContext).asGif().load(R.drawable.anim_audio_conning).into(ivConningAnim);
                            }
                            tvConnAudio.setVisibility(View.GONE);
                        }
                    });
                } else {
                    //对于非主播，显示等待中
                    tvConnStatus.setVisibility(View.VISIBLE);
                    tvConnStatus.setText(mContext.getResources().getString(R.string.audio_connect_status_waiting));
                    //#1CC9A4--自己的，#999999-别人的
                    tvConnStatus.setTextColor(Color.parseColor(item.getUid() == selfUid ? "#1CC9A4" : "#999999"));
                }
                AudioConnTimeCounter.getInstance().stop(item, tvConnTime);
            } else if (item.getApplyStatus() == AudioConnReqInfo.ApplyStatus.Over) {
                //已结束,统一显示为已结束
                tvConnStatus.setVisibility(View.VISIBLE);
                tvConnStatus.setText(mContext.getResources().getString(R.string.audio_connect_status_over));
                tvConnStatus.setTextColor(Color.parseColor("#999999"));
                AudioConnTimeCounter.getInstance().stop(item, tvConnTime);
            }

        } else {
            tvConnStatus.setText(null);
            ivReqAvatar.setImageDrawable(null);
            AudioConnTimeCounter.getInstance().stop(item, tvConnTime);
        }
        tvReqNick.setText(item.getNick());
        tvReqReason.setText(item.getApplyContent());
        tvReqReason.setLayoutParams(lp);
        helper.getView(R.id.rlConnRecord).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onAudioConnItemClickListener) {
                    onAudioConnItemClickListener.onShowUserInfoDialogClick(item.getUid());
                }
            }
        });
    }

    public interface OnAudioConnItemClickListener {

        /**
         * 点击[连接]，接受申请
         */
        void onRecvConnReqClick(long targetUid);

        /**
         * 挂断连麦
         */
        void onCallDownConnClick(long targetUid);

        /**
         * 点击其他区域，显示申请人的个人资料弹框
         */
        void onShowUserInfoDialogClick(long uid);

    }
}
