package com.yuhuankj.tmxq.ui.room.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.home.activity.SortListActivity;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

public class SortMenuAdapter extends BaseAdapter {


    public List<Json> jsons;

    public SortMenuAdapter(List<Json> jsons) {

        this.jsons = jsons;
    }

    public void setJsons(List<Json> jsons) {
        this.jsons = jsons;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        int i = jsons == null ? 0 : jsons.size();
        if (i > 6) {
            i = 6;
        }
        return i;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Json json = jsons.get(position);
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_hot_sort_menu, parent, false);
        ImageView icon = inflate.findViewById(R.id.iv_item_hot_sort_option_icon);
        TextView title = inflate.findViewById(R.id.tv_item_hot_sort_option_title);
        title.setText(json.str("name"));
        ImageLoadUtils.loadImage(parent.getContext(), json.str("pict"), icon);
        inflate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SortListActivity.start(parent.getContext(), position);
            }
        });
        return inflate;
    }
}
