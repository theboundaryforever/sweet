package com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tongdaxing.erban.libcommon.base.AbstractMvpLinearLayout;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter.RoomMsgTipsListAdapter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter.RoomMsgTipsPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IRoomMsgTipsView;

import java.util.List;

/**
 * 创建者      魏海涛
 * 创建时间    2019年4月25日 18:12:44
 * 描述        房间内右下角点赞模块的封装view
 *
 * @author dell
 */
@CreatePresenter(RoomMsgTipsPresenter.class)
public class RoomMsgTipsView extends AbstractMvpLinearLayout<IRoomMsgTipsView, RoomMsgTipsPresenter<IRoomMsgTipsView>>
        implements IRoomMsgTipsView, View.OnClickListener {

    private final String TAG = RoomMsgTipsView.class.getSimpleName();

    private RecyclerView rvMsgTips;
    private RoomMsgTipsListAdapter roomMsgTipsListAdapter;
    private ImageView ivCloseMsgTips;
    private OnRoomMsgTipsClickListener onRoomMsgTipsClickListener;
    private OnRoomMsgTipsViewVisibilityChangeListener onRoomMsgTipsViewVisibilityChangeListener;

    public RoomMsgTipsView(Context context) {
        this(context, null);
    }

    public RoomMsgTipsView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoomMsgTipsView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getViewLayoutId() {
        return R.layout.layout_room_msg_tips;
    }

    @Override
    public void initialView(Context context) {
        rvMsgTips = findViewById(R.id.rvMsgTips);
        ivCloseMsgTips = findViewById(R.id.ivCloseMsgTips);
        setVisibility(View.GONE);
        if (null == roomMsgTipsListAdapter) {
            roomMsgTipsListAdapter = new RoomMsgTipsListAdapter();
        }
        rvMsgTips.setAdapter(roomMsgTipsListAdapter);
    }

    @Override
    protected void initListener() {
        ivCloseMsgTips.setOnClickListener(this);
        roomMsgTipsListAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                getMvpPresenter().reportRoomMsgTipsSend();
                if (null != onRoomMsgTipsClickListener && null != adapter.getItem(position)) {
                    onRoomMsgTipsClickListener.onRoomMsgTipsClick((String) adapter.getItem(position));
                }
            }
        });
    }

    @Override
    protected void initViewState() {

    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        LogUtils.d(TAG, "onAttachedToWindow");
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        LogUtils.d(TAG, "onDetachedFromWindow");
        release();
    }

    public void getRoomMsgTipsList() {
        getMvpPresenter().getRoomMsgTipsList();
    }

    private void release() {
        onRoomMsgTipsClickListener = null;
        onRoomMsgTipsViewVisibilityChangeListener = null;
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivCloseMsgTips:
                setVisibility(View.GONE);
                RoomDataManager.get().singleAudioRoomMsgTipsViewShow = false;
                break;
            default:
                break;
        }
    }

    @Override
    public void refreshMsgTipsView(List<String> roomTipsList) {
        if (!ListUtils.isListEmpty(roomTipsList) && RoomDataManager.get().singleAudioRoomMsgTipsViewShow
                && null != roomMsgTipsListAdapter) {
            setVisibility(View.VISIBLE);
            roomMsgTipsListAdapter.replaceData(roomTipsList);
        }
    }

    @Override
    public void refreshMsgTipsShowStatus(boolean show) {
        setVisibility(show ? View.VISIBLE : GONE);
        RoomDataManager.get().singleAudioRoomMsgTipsViewShow = show;
    }

    public void setOnRoomMsgTipsClickListener(OnRoomMsgTipsClickListener listener) {
        this.onRoomMsgTipsClickListener = listener;
    }

    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        if (null != onRoomMsgTipsViewVisibilityChangeListener) {
            onRoomMsgTipsViewVisibilityChangeListener.onRoomMsgTipsViewVisibilityChanged(visibility);
        }
    }

    public void setOnRoomMsgTipsViewVisibilityChangeListener(OnRoomMsgTipsViewVisibilityChangeListener listener) {
        this.onRoomMsgTipsViewVisibilityChangeListener = listener;
    }

    public interface OnRoomMsgTipsClickListener {
        void onRoomMsgTipsClick(String msgTips);
    }

    public interface OnRoomMsgTipsViewVisibilityChangeListener {
        void onRoomMsgTipsViewVisibilityChanged(int visibility);
    }
}
