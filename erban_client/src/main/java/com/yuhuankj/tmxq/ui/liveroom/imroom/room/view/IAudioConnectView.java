package com.yuhuankj.tmxq.ui.liveroom.imroom.room.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.AudioConnReqInfo;

public interface IAudioConnectView extends IMvpBaseView {

    void refreshAudioConnReqList();

    void toast(String msg);

    void refreshOperaStatus(AudioConnReqInfo.ApplyStatus applyStatus);

    void refreshConnSwitchStatus();
}
