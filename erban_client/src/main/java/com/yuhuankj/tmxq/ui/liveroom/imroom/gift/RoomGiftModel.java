package com.yuhuankj.tmxq.ui.liveroom.imroom.gift;

import android.text.TextUtils;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.JavaUtil;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;
import com.tongdaxing.xchat_core.gift.GiftType;
import com.tongdaxing.xchat_core.gift.MultiGiftReceiveInfo;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMember;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomQueueInfo;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_core.result.GiftRecieveInfoResult;
import com.tongdaxing.xchat_core.result.MultiGiftRecieveInfoResult;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/5/15
 */
public class RoomGiftModel extends BaseMvpModel {

    /**
     * 获取礼物分类的tab信息列表
     *
     * @return
     */
    public List<TabInfo> getTabInfoList() {
        List<TabInfo> tabInfoList = new ArrayList<>();
        tabInfoList.add(new TabInfo(-2, "包裹"));
        tabInfoList.add(new TabInfo(-1, "普通礼物"));
        tabInfoList.add(new TabInfo(0, "神奇礼盒"));
        tabInfoList.add(new TabInfo(1, "贵族礼物"));
        tabInfoList.add(new TabInfo(2, "挚友水晶"));
        return tabInfoList;
    }

    /**
     * 获取礼物列表对应分类的Fragment的list
     *
     * @return
     */
    public List<RoomGiftListFragment> getGiftTabFragments() {
        List<RoomGiftListFragment> fragments = new ArrayList<>();
        fragments.add(RoomGiftListFragment.getInstance(GiftType.Package));
        fragments.add(RoomGiftListFragment.getInstance(GiftType.Normal));
        fragments.add(RoomGiftListFragment.getInstance(GiftType.Box));
        fragments.add(RoomGiftListFragment.getInstance(GiftType.Exclusive));
        fragments.add(RoomGiftListFragment.getInstance(GiftType.BosomFriend));
        return fragments;
    }


    /**
     * 获取礼物列表对应分类的Fragment的list
     *
     * @return
     */
    public int getGiftTypePosition(GiftType giftType) {
        if (giftType == GiftType.Package) {
            return 0;
        } else if (giftType == GiftType.Normal) {
            return 1;
        } else if (giftType == GiftType.Box) {
            return 2;
        } else if (giftType == GiftType.Exclusive) {
            return 3;
        } else if (giftType == GiftType.BosomFriend) {
            return 4;
        } else {
            return 1;
        }
    }


    public List<MicMemberInfo> getTopUserAvatarList(boolean isPersonal, IMRoomMember chatRoomMember) {
        return getTopUserAvatarList(isPersonal, JavaUtil.str2long(chatRoomMember.getAccount()), chatRoomMember.getVipId(), chatRoomMember.getVipDate(), chatRoomMember.isInvisible());
    }

    /**
     * 根据当前用户信息获取需要显示送礼对象的信息列表
     *
     * @param isPersonal
     * @param uid
     * @param vipId
     * @param vipDate
     * @param isInvisible
     * @return
     */
    public List<MicMemberInfo> getTopUserAvatarList(boolean isPersonal, long uid, int vipId, int vipDate, boolean isInvisible) {
        List<MicMemberInfo> micMemberInfos = new ArrayList<>();
        if (isPersonal) {//单人送礼物的情况 -- 只能给一个人送礼 不需要显示麦位  --- 用于私聊的情况
            MicMemberInfo personalMemberInfo = new MicMemberInfo();
            if (uid > 0) {
                if (!RoomDataManager.get().isUserSelf(uid)) {
                    personalMemberInfo.setUid(uid);
                    personalMemberInfo.setMicPosition(-2);
                    personalMemberInfo.setVipId(vipId);
                    personalMemberInfo.setVipDate(vipDate);
                    personalMemberInfo.setInvisible(isInvisible);
                    personalMemberInfo.setSelect(true);
                    IMRoomQueueInfo myMicInfo = RoomDataManager.get().getRoomQueueMemberInfoByAccount(String.valueOf(uid));
                    if (myMicInfo != null && myMicInfo.mChatRoomMember != null) {
                        personalMemberInfo.setNick(myMicInfo.mChatRoomMember.getNick());
                        personalMemberInfo.setAvatar(myMicInfo.mChatRoomMember.getAvatar());
                    } else {
                        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(uid, false);
                        if (userInfo == null) {
                            personalMemberInfo.setAvatar("");
                            personalMemberInfo.setNick("");
                        } else {
                            personalMemberInfo.setAvatar(userInfo.getAvatar());
                            personalMemberInfo.setNick(userInfo.getNick());
                        }
                    }
                    micMemberInfos.add(personalMemberInfo);
                }
            } else {
                IMRoomMember defRoomMember = RoomDataManager.get().getRoomOwnerDefaultMemberInfo();
                if (defRoomMember != null && !RoomDataManager.get().isUserSelf(defRoomMember.getAccount())) {
                    personalMemberInfo.setRoomOwnner(true);
                    personalMemberInfo.setUid(Long.valueOf(defRoomMember.getAccount()));
                    personalMemberInfo.setNick(defRoomMember.getNick());
                    personalMemberInfo.setAvatar(defRoomMember.getAvatar());
                    personalMemberInfo.setVipId(defRoomMember.getVipId());
                    personalMemberInfo.setVipDate(defRoomMember.getVipDate());
                    personalMemberInfo.setInvisible(defRoomMember.isInvisible());
                    personalMemberInfo.setMicPosition(-1);
                    personalMemberInfo.setSelect(uid <= 0);
                    micMemberInfos.add(personalMemberInfo);
                }
            }
        } else {//多人送礼物  -- 两种情况 选中人不在麦位上则单独显示一个人情况，如果再麦位上则显示其他人一起的情况，只是默认选中当前传入的人，如果uid = 0 则按照麦位顺序默认选中
            boolean uidIsMic = false;
            boolean hasRoomOwner = false;
            List<MicMemberInfo> copyMicMemberInfos = new ArrayList<>();
            for (int i = 0; i < RoomDataManager.get().mMicQueueMemberMap.size(); i++) {
                IMRoomQueueInfo roomQueueInfo = RoomDataManager.get().mMicQueueMemberMap.get(RoomDataManager.get().mMicQueueMemberMap.keyAt(i));
                if (null == roomQueueInfo) {
                    continue;
                }
                IMRoomMember imRoomMember = roomQueueInfo.mChatRoomMember;
                if (imRoomMember == null) {
                    continue;
                }
                if (TextUtils.isEmpty(imRoomMember.getAccount())
                        || RoomDataManager.get().isUserSelf(imRoomMember.getAccount())) {
                    continue;
                }
                MicMemberInfo micMemberInfo = new MicMemberInfo();
                // 当前麦位有这个uid的人
                if (imRoomMember.getAccount().equals(String.valueOf(uid))) {
                    uidIsMic = true;
                    micMemberInfo.setSelect(true);
                }
                if (RoomDataManager.get().isRoomOwner(imRoomMember.getAccount())) {
                    hasRoomOwner = true;
                    micMemberInfo.setRoomOwnner(true);
                }
                micMemberInfo.setNick(imRoomMember.getNick());
                micMemberInfo.setAvatar(imRoomMember.getAvatar());
                micMemberInfo.setMicPosition(RoomDataManager.get().mMicQueueMemberMap.keyAt(i));
                micMemberInfo.setUid(JavaUtil.str2long(imRoomMember.getAccount()));
                copyMicMemberInfos.add(micMemberInfo);
            }
            if (!hasRoomOwner) {//这里会过滤掉自己是房主的情况所以下面排除
                IMRoomMember defRoomMember = RoomDataManager.get().getRoomOwnerDefaultMemberInfo();
                if (defRoomMember != null && !RoomDataManager.get().isUserSelf(defRoomMember.getAccount())) {
                    MicMemberInfo micMemberInfo = new MicMemberInfo();
                    micMemberInfo.setRoomOwnner(true);
                    micMemberInfo.setUid(Long.valueOf(defRoomMember.getAccount()));
                    micMemberInfo.setNick(defRoomMember.getNick());
                    micMemberInfo.setAvatar(defRoomMember.getAvatar());
                    micMemberInfo.setVipId(defRoomMember.getVipId());
                    micMemberInfo.setVipDate(defRoomMember.getVipDate());
                    micMemberInfo.setInvisible(defRoomMember.isInvisible());
                    micMemberInfo.setMicPosition(-1);
                    micMemberInfo.setSelect(uid <= 0);
                    micMemberInfos.add(micMemberInfo);
                }
            }
            if (uid > 0 && !uidIsMic) {//独立不在麦位的用户
                MicMemberInfo personalMemberInfo = new MicMemberInfo();
                personalMemberInfo.setUid(uid);
                personalMemberInfo.setMicPosition(-2);
                personalMemberInfo.setVipId(vipId);
                personalMemberInfo.setVipDate(vipDate);
                personalMemberInfo.setInvisible(isInvisible);
                UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(uid, false);
                if (userInfo == null) {
                    personalMemberInfo.setAvatar("");
                    personalMemberInfo.setNick("");
                } else {
                    personalMemberInfo.setAvatar(userInfo.getAvatar());
                    personalMemberInfo.setNick(userInfo.getNick());
                }
                personalMemberInfo.setSelect(true);
                micMemberInfos.add(personalMemberInfo);
            } else {
                micMemberInfos.addAll(copyMicMemberInfos);
            }
        }
        return micMemberInfos;
    }


    /**
     * 赠送单个礼物接口
     *
     * @param giftInfo
     * @param targetUid
     * @param roomUid
     * @param giftNum
     */
    public void sendRoomSingleRoomGift(GiftInfo giftInfo, final long targetUid, long roomUid, final int giftNum, CallBack<GiftReceiveInfo> callback) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
        requestParam.put("giftId", giftInfo.getGiftId() + "");
        requestParam.put("targetUid", targetUid + "");
        requestParam.put("uid", uid + "");
        requestParam.put("ticket", ticket);
        requestParam.put("roomUid", roomUid + "");
        requestParam.put("giftNum", giftNum + "");
        if (targetUid == roomUid) {
            requestParam.put("type", "1");
        } else {
            requestParam.put("type", "3");
        }
        RoomInfo cRoomInfo = BaseRoomServiceScheduler.getCurrentRoomInfo();
        if (null != cRoomInfo) {
            requestParam.put("roomType", cRoomInfo.getType() + "");
        }
        OkHttpManager.getInstance().postRequest(UriProvider.sendGiftV3(), requestParam, new OkHttpManager.MyCallBack<GiftRecieveInfoResult>() {
            @Override
            public void onError(Exception e) {
                if (callback != null) {
                    callback.onFail(-1, e.getMessage());
                }
            }

            @Override
            public void onResponse(GiftRecieveInfoResult response) {
                if (response.isSuccess()){
                    if (response.getData() != null) {
//                        notifyClients(IPKCoreClient.class, IPKCoreClient.METHOD_ON_PK_GIFT, targetUid);
                        IMRoomMessageManager.get().sendSingleGiftMessage(response.getData());
                    }
                    if (callback != null) {
                        callback.onSuccess(response.getData());
                    }
                }else {
                    if (callback != null) {
                        callback.onFail(response.getCode(), response.getMessage());
                    }
                }
            }
        });
    }


    /**
     * 赠送全麦礼物
     * @param giftInfo
     * @param micAvatar
     * @param roomUid
     * @param giftNum
     */
    public void sendRoomMultiGift(GiftInfo giftInfo, long roomUid,  List<MicMemberInfo> micAvatar,final int giftNum,CallBack<MultiGiftReceiveInfo> callback) {
        if (ListUtils.isListEmpty(micAvatar)) {
            return;
        }
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
        requestParam.put("giftId", giftInfo.getGiftId() + "");
        requestParam.put("uid", uid + "");
        requestParam.put("ticket", ticket);
        requestParam.put("roomUid", roomUid + "");
        requestParam.put("giftNum", giftNum + "");
        RoomInfo cRoomInfo = BaseRoomServiceScheduler.getCurrentRoomInfo();
        if (null != cRoomInfo) {
            requestParam.put("roomType", cRoomInfo.getType() + "");
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < micAvatar.size(); i++) {
            sb.append(micAvatar.get(i).getUid());
            sb.append(",");
        }
        sb.deleteCharAt(sb.length() - 1);
        requestParam.put("targetUids", sb.toString());
        OkHttpManager.getInstance().postRequest(UriProvider.sendWholeGiftV3(), requestParam, new OkHttpManager.MyCallBack<MultiGiftRecieveInfoResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (callback != null){
                    callback.onFail(-1,e.getMessage());
                }
            }

            @Override
            public void onResponse(MultiGiftRecieveInfoResult response) {
                if (response.isSuccess()){
                    if (response.getData() != null) {
//                       notifyClients(IPKCoreClient.class, IPKCoreClient.METHOD_ON_PK_MULTI_GIFT, targetUids);
                        IMRoomMessageManager.get().sendMultiGiftMessage(response.getData());
                    }
                    if (callback != null) {
                        callback.onSuccess(response.getData());
                    }
                }else {
                    if (callback != null) {
                        callback.onFail(response.getCode(), response.getMessage());
                    }
                }
            }
        });
    }


}
