package com.yuhuankj.tmxq.ui.me.wallet.bills;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.SparseLongArray;
import android.view.View;
import android.widget.ImageView;

import com.jzxiang.pickerview.TimePickerDialog;
import com.jzxiang.pickerview.data.Type;
import com.jzxiang.pickerview.listener.OnDateSetListener;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.ui.me.wallet.bills.adapter.WithdrawAdapter;
import com.yuhuankj.tmxq.ui.me.wallet.bills.event.DateInfoEvent;
import com.yuhuankj.tmxq.ui.room.adapter.CommonMagicIndicatorAdapter;
import com.yuhuankj.tmxq.widget.TitleBar;
import com.yuhuankj.tmxq.widget.magicindicator.MagicIndicator;
import com.yuhuankj.tmxq.widget.magicindicator.ViewPagerHelper;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.commonnavigator.CommonNavigator;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * 提现账单记录
 */
public class WithdrawBillsActivity extends BaseActivity implements View.OnClickListener, OnDateSetListener, CommonMagicIndicatorAdapter.OnItemSelectListener {

    private TitleBar mTitleBar;
    private MagicIndicator mMagicIndicator;
    private ViewPager mViewPager;

   // private TextView mTvDay;
    private ImageView mIvToday, mIvDay;
    //    private TimePickerDialog mDialogYearMonthDay;
    private TimePickerDialog.Builder mDialogYearMonthDayBuild;
    private int mPosition;
    private long time = System.currentTimeMillis();
    private SparseLongArray mTodayMap = new SparseLongArray(2);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_withdraw_bills);
        initView();
        initData();
        setListener();
    }

    private void initView() {
        mMagicIndicator = (MagicIndicator) findViewById(R.id.magic_indicator2);
        mViewPager = (ViewPager) findViewById(R.id.vPager);
       // mTvDay = (TextView) findViewById(R.id.tv_date);
        mIvToday = (ImageView) findViewById(R.id.iv_today_select);
        mIvDay = (ImageView) findViewById(R.id.tv_selector_date);
    }


    private void initData() {
        initTitleBar(getString(R.string.bill_withdraw));
        mViewPager.setAdapter(new WithdrawAdapter(getSupportFragmentManager()));
        initMagicIndicator2();
        //初始化时间
        mTodayMap.put(0, time);
        mTodayMap.put(1, time);
        setDate();
        mDialogYearMonthDayBuild = new TimePickerDialog.Builder()
                .setType(Type.YEAR_MONTH_DAY)
                .setTitleStringId("日期选择")
                .setThemeColor(getResources().getColor(R.color.line_background))
                .setWheelItemTextNormalColor(getResources().getColor(R.color
                        .timetimepicker_default_text_color))
                .setWheelItemTextSelectorColor(getResources().getColor(R.color.black))
                .setCallBack(this)
        ;
    }

    private void setListener() {
        mIvDay.setOnClickListener(this);
        mIvToday.setOnClickListener(this);
    }

    private void initMagicIndicator2() {
        final String[] titles = getResources().getStringArray(R.array.bill_withdraw_titles);
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdjustMode(true);
        List<TabInfo> tabInfoList = new ArrayList<>(2);
        for (int i = 0; i < titles.length; i++) {
            tabInfoList.add(new TabInfo(i, titles[i]));
        }
        CommonMagicIndicatorAdapter adapter = new CommonMagicIndicatorAdapter(this, tabInfoList, 0);
        adapter.setOnItemSelectListener(this);
        commonNavigator.setAdapter(adapter);
        mMagicIndicator.setNavigator(commonNavigator);
        ViewPagerHelper.bind(mMagicIndicator, mViewPager);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mPosition = position;
                setDate();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_today_select:
                time = System.currentTimeMillis();
                mTodayMap.put(mPosition, time);
                setDate();
                EventBus.getDefault().post(new DateInfoEvent(time, mPosition));
                break;
            case R.id.tv_selector_date:
                mDialogYearMonthDayBuild.build().show(getSupportFragmentManager(), "year_month_day");
                break;
            default:
        }
    }


    private void setDate() {
       // mTvDay.setText(TimeUtils.getDateTimeString(mTodayMap.get(mPosition), "yyyy-MM-dd"));
    }

    @Override
    public void onDateSet(TimePickerDialog timePickerView, long millseconds) {
        this.time = millseconds;
        mTodayMap.put(mPosition, time);
        setDate();
        EventBus.getDefault().post(new DateInfoEvent(millseconds, mPosition));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mDialogYearMonthDayBuild != null) {
            mDialogYearMonthDayBuild.setCallBack(null);
            mDialogYearMonthDayBuild = null;
        }
    }

    @Override
    public void onItemSelect(int position) {
        mViewPager.setCurrentItem(position);
    }
}
