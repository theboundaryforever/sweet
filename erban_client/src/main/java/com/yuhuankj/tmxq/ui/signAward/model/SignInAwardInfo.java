package com.yuhuankj.tmxq.ui.signAward.model;

import java.io.Serializable;

/**
 * 连续签到奖励及任务中心奖励
 *
 * @author weihaitao
 * @date 2019/5/22
 */
public class SignInAwardInfo implements Serializable {

    /**
     * 主键ID
     */
    private int id;
    /**
     * 任务名称
     */
    private String missionName;
    /**
     * 任务图标
     */
    private String missionIcon;

    /**
     * 任务甜豆收益
     */
    private int peaNum;
    /**
     * 排列顺序 1-7
     */
    private int seq;

    /**
     * 赠品ID
     */
    private int freebiesId;

    /**
     * 赠品类型：0甜豆 1神秘宝箱 2勋章
     */
    private int freebiesType;

    /**
     * 赠品名称
     * 2019年5月23日 15:19:57
     * 甜蜜星球1.0.0.2版本，首页连续签到奖励弹框，直接展示该字段
     * 任务中心，则通过"+".concat(String.valueOf(peaNum))的格式来展示
     */
    private String freebiesName;

    /**
     * 任务类型：0，删除；1，新手任务；2，每日任务；3，签到任务
     */
    private int missionType;

    /**
     * 任务状态，1，未完成；2，已完成; 3，已领取
     */
    private int missionStatus;

    /**
     * 任务作用域：1，审核可见；2，审核不可见；
     */
    private int missionScope;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMissionName() {
        return missionName;
    }

    public void setMissionName(String missionName) {
        this.missionName = missionName;
    }

    public String getMissionIcon() {
        return missionIcon;
    }

    public void setMissionIcon(String missionIcon) {
        this.missionIcon = missionIcon;
    }

    public int getPeaNum() {
        return peaNum;
    }

    public void setPeaNum(int peaNum) {
        this.peaNum = peaNum;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public int getFreebiesId() {
        return freebiesId;
    }

    public void setFreebiesId(int freebiesId) {
        this.freebiesId = freebiesId;
    }

    public int getFreebiesType() {
        return freebiesType;
    }

    public void setFreebiesType(int freebiesType) {
        this.freebiesType = freebiesType;
    }

    public String getFreebiesName() {
        return freebiesName;
    }

    public void setFreebiesName(String freebiesName) {
        this.freebiesName = freebiesName;
    }

    public int getMissionType() {
        return missionType;
    }

    public void setMissionType(int missionType) {
        this.missionType = missionType;
    }

    public int getMissionStatus() {
        return missionStatus;
    }

    public void setMissionStatus(int missionStatus) {
        this.missionStatus = missionStatus;
    }

    public int getMissionScope() {
        return missionScope;
    }

    public void setMissionScope(int missionScope) {
        this.missionScope = missionScope;
    }
}
