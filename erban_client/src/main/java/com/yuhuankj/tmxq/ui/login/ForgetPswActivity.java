package com.yuhuankj.tmxq.ui.login;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.databinding.ActivityRegisterBinding;

/**
 * Created by zhouxiangfeng on 17/3/5.
 */
public class ForgetPswActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = "RegisterActivity";
    private String errorStr;
    private ActivityRegisterBinding forgetPswBinding;
    private CodeDownTimer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        forgetPswBinding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        initTitleBar("重置密码");
        onFindViews();
        onSetListener();
    }

    public void onFindViews() {
        forgetPswBinding.btnRegist.setText("重置密码");
        forgetPswBinding.rlActionTip.setVisibility(View.GONE);
    }

    public void onSetListener() {
        forgetPswBinding.setClick(this);
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onSmsFail(String error) {
        toast(error);
        LogUtils.d(TAG, "onSmsFail error:" + error);
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onModifyPsw() {
        toast("重置密码成功！");
        finish();
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onModifyPswFail(String error) {
        toast(error);
        LogUtils.d(TAG, "onModifyPswFail error:" + error);
    }

    @Override
    public void onClick(View v) {
        String phone = forgetPswBinding.etPhone.getText().toString();
        switch (v.getId()) {
            case R.id.btn_regist:
                String psw = forgetPswBinding.etPassword.getText().toString();
                String sms_code = forgetPswBinding.etCode.getText().toString();
                if (!StringUtils.isEmpty(sms_code)) {
                    if (isOK(phone, psw)) {
                        CoreManager.getCore(IAuthCore.class).requestResetPsw(phone, sms_code, psw);
                    } else {
                        toast(errorStr);
                    }
                } else {
                    toast("验证码不能为空！");
                }
                break;
            case R.id.btn_get_code:
                if (phone.length() == 11) {
                    timer = new CodeDownTimer(forgetPswBinding.btnGetCode, 60000, 1000);
                    timer.start();
                    CoreManager.getCore(IAuthCore.class).requestSMSCode(phone, 3);
                } else {
                    toast("手机号码不正确");
                }
                break;
        }
    }

    private boolean isOK(String phone, String psw) {
        if (StringUtils.isEmpty(psw)) {
            errorStr = "密码不能为空";
            return false;
        }
        if (psw.length() < 6) {
            errorStr = "请输入6~16位的密码";
            return false;
        }
        if (StringUtils.isEmpty(phone)) {
            errorStr = "请填写手机号码！";
            return false;
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
        }
    }
}
