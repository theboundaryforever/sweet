package com.yuhuankj.tmxq.ui.find.mengxin;

import java.io.Serializable;
import java.util.List;

import io.realm.RealmObject;

public class SproutUserInRoomInfo extends RealmObject implements Serializable {


    /**
     * uid : 90000939
     * officeUser : 1
     * roomId : 57782066
     * title : u大家觉得就放假减肥
     * type : 3
     * meetingName : 8edbb1bc37004f80bf14d715513824fe
     * valid : true
     * operatorStatus : 1
     * roomDesc : 还回家
     * roomNotice : 新还珠格格
     * backPic : 6
     * openTime : 1540345105000
     * onlineNum : 97
     * abChannelType : 1
     * roomPwd :
     * roomTag : 听歌
     * calcSumDataIndex : 0
     * tagId : 11
     * tagPict : https://img.pinjin88.com/tag_yinyue2.png
     * isPermitRoom : 2
     * isExceptionClose : false
     * isRecom : 0
     * count : 0
     * hideFace : [21,22,23,24]
     * giftEffectSwitch : 0
     * publicChatSwitch : 0
     * charmSwitch : 0
     * charmOpen : 0
     * playInfo : 坎坎坷坷vbh 兔兔哈哈宝贝
     * factor : 0
     * detonatingState : 0
     * exceptionClose : false
     */

    private long uid;
    private int officeUser;
    private long roomId;
    private String title;
    private int type;
    private String meetingName;
    private boolean valid;
    private int operatorStatus;
    private String roomDesc;
    private String roomNotice;
    private String backPic;
    private long openTime;
    private int onlineNum;
    private int abChannelType;
    private String roomPwd;
    private String roomTag;
    private int calcSumDataIndex;
    private int tagId;
    private String tagPict;
    private int isPermitRoom;
    private boolean isExceptionClose;
    private int isRecom;
    private int count;
    private int giftEffectSwitch;
    private int publicChatSwitch;
    private int charmSwitch;
    private int charmOpen;
    private String playInfo;
    private int factor;
    private int detonatingState;
    private boolean exceptionClose;
    private List<Integer> hideFace;

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public int getOfficeUser() {
        return officeUser;
    }

    public void setOfficeUser(int officeUser) {
        this.officeUser = officeUser;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMeetingName() {
        return meetingName;
    }

    public void setMeetingName(String meetingName) {
        this.meetingName = meetingName;
    }

    public boolean getValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public int getOperatorStatus() {
        return operatorStatus;
    }

    public void setOperatorStatus(int operatorStatus) {
        this.operatorStatus = operatorStatus;
    }

    public String getRoomDesc() {
        return roomDesc;
    }

    public void setRoomDesc(String roomDesc) {
        this.roomDesc = roomDesc;
    }

    public String getRoomNotice() {
        return roomNotice;
    }

    public void setRoomNotice(String roomNotice) {
        this.roomNotice = roomNotice;
    }

    public String getBackPic() {
        return backPic;
    }

    public void setBackPic(String backPic) {
        this.backPic = backPic;
    }

    public long getOpenTime() {
        return openTime;
    }

    public void setOpenTime(long openTime) {
        this.openTime = openTime;
    }

    public int getOnlineNum() {
        return onlineNum;
    }

    public void setOnlineNum(int onlineNum) {
        this.onlineNum = onlineNum;
    }

    public int getAbChannelType() {
        return abChannelType;
    }

    public void setAbChannelType(int abChannelType) {
        this.abChannelType = abChannelType;
    }

    public String getRoomPwd() {
        return roomPwd;
    }

    public void setRoomPwd(String roomPwd) {
        this.roomPwd = roomPwd;
    }

    public String getRoomTag() {
        return roomTag;
    }

    public void setRoomTag(String roomTag) {
        this.roomTag = roomTag;
    }

    public int getCalcSumDataIndex() {
        return calcSumDataIndex;
    }

    public void setCalcSumDataIndex(int calcSumDataIndex) {
        this.calcSumDataIndex = calcSumDataIndex;
    }

    public int getTagId() {
        return tagId;
    }

    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    public String getTagPict() {
        return tagPict;
    }

    public void setTagPict(String tagPict) {
        this.tagPict = tagPict;
    }

    public int getIsPermitRoom() {
        return isPermitRoom;
    }

    public void setIsPermitRoom(int isPermitRoom) {
        this.isPermitRoom = isPermitRoom;
    }

    public boolean isIsExceptionClose() {
        return isExceptionClose;
    }

    public void setIsExceptionClose(boolean isExceptionClose) {
        this.isExceptionClose = isExceptionClose;
    }

    public int getIsRecom() {
        return isRecom;
    }

    public void setIsRecom(int isRecom) {
        this.isRecom = isRecom;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getGiftEffectSwitch() {
        return giftEffectSwitch;
    }

    public void setGiftEffectSwitch(int giftEffectSwitch) {
        this.giftEffectSwitch = giftEffectSwitch;
    }

    public int getPublicChatSwitch() {
        return publicChatSwitch;
    }

    public void setPublicChatSwitch(int publicChatSwitch) {
        this.publicChatSwitch = publicChatSwitch;
    }

    public int getCharmSwitch() {
        return charmSwitch;
    }

    public void setCharmSwitch(int charmSwitch) {
        this.charmSwitch = charmSwitch;
    }

    public int getCharmOpen() {
        return charmOpen;
    }

    public void setCharmOpen(int charmOpen) {
        this.charmOpen = charmOpen;
    }

    public String getPlayInfo() {
        return playInfo;
    }

    public void setPlayInfo(String playInfo) {
        this.playInfo = playInfo;
    }

    public int getFactor() {
        return factor;
    }

    public void setFactor(int factor) {
        this.factor = factor;
    }

    public int getDetonatingState() {
        return detonatingState;
    }

    public void setDetonatingState(int detonatingState) {
        this.detonatingState = detonatingState;
    }

    public boolean isExceptionClose() {
        return exceptionClose;
    }

    public void setExceptionClose(boolean exceptionClose) {
        this.exceptionClose = exceptionClose;
    }

    public List<Integer> getHideFace() {
        return hideFace;
    }

    public void setHideFace(List<Integer> hideFace) {
        this.hideFace = hideFace;
    }

    @Override
    public String toString() {
        return "SproutUserInRoomInfo{" +
                "uid=" + uid +
                ", officeUser=" + officeUser +
                ", roomId=" + roomId +
                ", title='" + title + '\'' +
                ", type=" + type +
                ", meetingName='" + meetingName + '\'' +
                ", valid=" + valid +
                ", operatorStatus=" + operatorStatus +
                ", roomDesc='" + roomDesc + '\'' +
                ", roomNotice='" + roomNotice + '\'' +
                ", backPic='" + backPic + '\'' +
                ", openTime=" + openTime +
                ", onlineNum=" + onlineNum +
                ", abChannelType=" + abChannelType +
                ", roomPwd='" + roomPwd + '\'' +
                ", roomTag='" + roomTag + '\'' +
                ", calcSumDataIndex=" + calcSumDataIndex +
                ", tagId=" + tagId +
                ", tagPict='" + tagPict + '\'' +
                ", isPermitRoom=" + isPermitRoom +
                ", isExceptionClose=" + isExceptionClose +
                ", isRecom=" + isRecom +
                ", count=" + count +
                ", giftEffectSwitch=" + giftEffectSwitch +
                ", publicChatSwitch=" + publicChatSwitch +
                ", charmSwitch=" + charmSwitch +
                ", charmOpen=" + charmOpen +
                ", playInfo='" + playInfo + '\'' +
                ", factor=" + factor +
                ", detonatingState=" + detonatingState +
                ", exceptionClose=" + exceptionClose +
                ", hideFace=" + hideFace +
                '}';
    }
}
