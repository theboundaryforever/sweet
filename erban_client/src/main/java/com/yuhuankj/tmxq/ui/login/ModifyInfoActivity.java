package com.yuhuankj.tmxq.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.View;
import android.widget.EditText;

import com.growingio.android.sdk.collection.GrowingIO;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.widget.TitleBar;


/**
 * Created by zhouxiangfeng on 2017/5/13.
 */

public class ModifyInfoActivity extends BaseActivity {
    private EditText etEditText;
    private EditText etEditTextNick;
    public static final String CONTENT = "content";
    public static final String CONTENTNICK = "contentNick";
    private CoordinatorLayout layout_coordinator;
    private String title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_info);
        onFindViews();
        onSetListener();
        init();
        initData();
    }

    private void initData() {
        long currentUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        UserInfo userInfos = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(currentUid);
        if (!StringUtil.isEmpty(title) && userInfos != null) {
            if (title.equals("个性签名")) {
                etEditText.setText(userInfos.getUserDesc());
            } else {
                etEditTextNick.setText(userInfos.getNick());
                etEditTextNick.setFilters(new InputFilter[]{enterBlankInputFilter});
            }
            etEditTextNick.setSelection(etEditTextNick.getText().length());
        }
    }

    private void init() {
        title = getIntent().getStringExtra("title");
        if (title.equals("个性签名")) {
            etEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(60)});
        } else if (title.equals("昵称")) {
            etEditTextNick.setVisibility(View.VISIBLE);
            etEditText.setVisibility(View.GONE);
            etEditTextNick.setFilters(new InputFilter[]{new InputFilter.LengthFilter(15)});
        }
        initTitleBar(title);
    }

    InputFilter enterBlankInputFilter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            if (source.equals(" ") || source.toString().contentEquals("\n")) {
                return "";
            } else {
                return null;
            }
        }
    };

    @Override
    public void initTitleBar(String title) {
        super.initTitleBar(title);
        TitleBar titleBar = (TitleBar) findViewById(R.id.title_bar);
        titleBar.setActionTextColor(R.color.text_color_primary);
        titleBar.addAction(new TitleBar.TextAction(getResources().getString(R.string.save)) {
            @Override
            public void performAction(View view) {
                String content = etEditText.getText().toString();
                String contentNick = etEditTextNick.getText().toString();
                //修改个人介绍
                if (!content.trim().isEmpty()) {
                    Intent intent = new Intent();
                    intent.putExtra(CONTENT, content);
                    setResult(RESULT_OK, intent);
                    finish();
                } else if (!contentNick.trim().isEmpty()) {
                    Intent intent = new Intent();
                    intent.putExtra(CONTENTNICK, contentNick);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    Snackbar.make(layout_coordinator, getResources().getString(R.string.user_info_modify_input_empty), Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void onSetListener() {

    }

    private void onFindViews() {
        etEditText = (EditText) findViewById(R.id.et_content);
        GrowingIO.getInstance().trackEditText(etEditText);
        etEditTextNick = (EditText) findViewById(R.id.et_content_nick);
        GrowingIO.getInstance().trackEditText(etEditTextNick);
        layout_coordinator = (CoordinatorLayout) findViewById(R.id.layout_coordinator);
    }

    public boolean isValid() {
        if (etEditText.length() > 60) {
            return false;
        }
        return true;
    }
}
