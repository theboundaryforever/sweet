package com.yuhuankj.tmxq.ui.widget;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.Utils;

/**
 * @author xiaoyu
 * @date 2017/12/13
 */

public class ChooseThirdPlatformDialog extends BottomSheetDialog implements View.OnClickListener {
    private Context context;
    private WindowManager windowManager;

    private TextView tv_title;
    private TextView tv_desc;
    private TextView tv_cancel;
    private TextView tv_wechat;
    private TextView tv_alipay;
    private ImageView iv_recommThirdPlatform;
    private ImageView iv_wechat;
    private ImageView iv_alipay;
    private boolean bindOrUnbind = true;

    private static final String TAG = "ChooseThirdPlatformDialog";
    private OnThirdPlatformChooseListener onThirdPlatformChooseListener;

    public ChooseThirdPlatformDialog(Context context, boolean bindOrUnbind) {
        super(context, R.style.ErbanBottomSheetDialog);
        this.context = context;
        this.bindOrUnbind = bindOrUnbind;
    }

    public void setOnThirdPlatformChooseListener(OnThirdPlatformChooseListener onThirdPlatformChooseListener) {
        this.onThirdPlatformChooseListener = onThirdPlatformChooseListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_third_platform_choose);
        setCanceledOnTouchOutside(true);

        initView();

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = context.getResources().getDisplayMetrics().heightPixels - (Utils.hasSoftKeys(context) ? Utils.getNavigationBarHeight(context) : 0);
        getWindow().setAttributes(params);
    }

    private void initView() {
        tv_title = findViewById(R.id.tv_title);
        tv_desc = findViewById(R.id.tv_desc);
        tv_cancel = findViewById(R.id.tv_cancel);
        tv_wechat = findViewById(R.id.tv_wechat);
        tv_alipay = findViewById(R.id.tv_alipay);
        iv_recommThirdPlatform = findViewById(R.id.iv_recommThirdPlatform);
        iv_wechat = findViewById(R.id.iv_wechat);
        iv_alipay = findViewById(R.id.iv_alipay);

        tv_title.setText(context.getResources().getString(bindOrUnbind ? R.string.third_platform_choose_title : R.string.choose_unbind_third_platform_title));
        tv_desc.setText(context.getResources().getString(bindOrUnbind ? R.string.third_platform_choose_tips : R.string.choose_unbind_third_platform_desc));
        tv_alipay.setText(context.getResources().getString(bindOrUnbind ? R.string.third_platform_alipay : R.string.third_platform_wechat));
        tv_wechat.setText(context.getResources().getString(bindOrUnbind ? R.string.third_platform_wechat_recom : R.string.third_platform_qq));
        iv_alipay.setImageDrawable(context.getResources().getDrawable(bindOrUnbind ? R.mipmap.icon_third_platform_alipay : R.mipmap.icon_third_platform_wechat));
        iv_wechat.setImageDrawable(context.getResources().getDrawable(bindOrUnbind ? R.mipmap.icon_third_platform_wechat : R.mipmap.icon_third_platform_qq));
        iv_recommThirdPlatform.setVisibility(bindOrUnbind ? View.VISIBLE : View.GONE);

        tv_wechat.setOnClickListener(this);
        tv_alipay.setOnClickListener(this);
        iv_alipay.setOnClickListener(this);
        iv_wechat.setOnClickListener(this);
        tv_cancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_cancel:
                dismiss();
                break;
            case R.id.iv_alipay:
            case R.id.tv_alipay:
                if (null != onThirdPlatformChooseListener) {
                    onThirdPlatformChooseListener.onThirdPlatformChoosed(bindOrUnbind ? ThirdPlatformType.alipay : ThirdPlatformType.wechatPay, bindOrUnbind);
                }
                dismiss();
                break;
            case R.id.tv_wechat:
            case R.id.iv_wechat:
                if (null != onThirdPlatformChooseListener) {
                    onThirdPlatformChooseListener.onThirdPlatformChoosed(bindOrUnbind ? ThirdPlatformType.wechatPay : ThirdPlatformType.qq, bindOrUnbind);
                }
                break;
            default:
                break;
        }
        dismiss();
    }

    public enum ThirdPlatformType {
        wechatPay,//微信支付
        alipay,//支付宝支付
        qq,
    }

    public interface OnThirdPlatformChooseListener {
        void onThirdPlatformChoosed(ThirdPlatformType third, boolean bindOrUnbind);
    }
}
