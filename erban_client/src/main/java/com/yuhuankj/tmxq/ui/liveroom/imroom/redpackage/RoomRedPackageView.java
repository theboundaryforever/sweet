package com.yuhuankj.tmxq.ui.liveroom.imroom.redpackage;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opensource.svgaplayer.SVGACallback;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tongdaxing.erban.libcommon.base.AbstractMvpRelativeLayout;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.xchat_core.bean.RoomRedPacket;
import com.tongdaxing.xchat_core.bean.RoomRedPacketReceiveResult;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomEvent;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderMiniGameInvited;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.net.URL;
import java.util.List;

/**
 * 文件描述：房间红包View
 *
 * @auther：zwk
 * @data：2019/8/23
 */
@CreatePresenter(RoomRedPackagePresenter.class)
public class RoomRedPackageView extends AbstractMvpRelativeLayout<IRoomRedPackageView, RoomRedPackagePresenter> implements IRoomRedPackageView {
    private SVGAImageView svRedPackageRain;
    private RelativeLayout rlRedPackage;
    private TextView tvUserName;
    private TextView tvCountdown;
    private RoomRedPacket currentRedPacket;//当前洪波
    //倒计时
    Handler handler = null;
    private int countDuration;
    private RelativeLayout rlRedPacketOpen;
    private RelativeLayout rlRedPacketOpenGet;
    private RelativeLayout rlRedPacketOpenNo;

    public RoomRedPackageView(Context context) {
        super(context);
    }

    public RoomRedPackageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected int getViewLayoutId() {
        return R.layout.view_room_red_package;
    }

    @Override
    public void initialView(Context context) {
        rlRedPackage = findViewById(R.id.rl_red_package_content);
        svRedPackageRain = findViewById(R.id.sv_red_package_rain);
        tvUserName = findViewById(R.id.tv_send_user_nick);
        tvCountdown = findViewById(R.id.tv_send_countdown);
//
        rlRedPacketOpen = findViewById(R.id.rlRedPacketOpen);
        rlRedPacketOpenGet = findViewById(R.id.rlRedPacketOpenGet);
        rlRedPacketOpenNo = findViewById(R.id.rlRedPacketOpenNo);
    }

    @Override
    protected void initListener() {
        rlRedPackage.setOnClickListener(v -> {
            rlRedPackage.setEnabled(false);
            if (RoomDataManager.get().getCurrentRoomInfo() != null && currentRedPacket != null) {
                getMvpPresenter().receiveRedPacket(RoomDataManager.get().getCurrentRoomInfo().getRoomId(), currentRedPacket.getRedPacketId());
            } else {
                rlRedPackage.setEnabled(true);
            }
        });
        rlRedPacketOpen.setOnClickListener(view -> {
            Animation animation = new AlphaAnimation(1F, 0F);
            animation.setDuration(500);
            rlRedPacketOpen.startAnimation(animation);
            rlRedPacketOpen.setVisibility(View.GONE);
        });
    }

    @Override
    protected void initViewState() {
        setEnabled(false);
    }

    public void getRedPackageInfo() {
        if (RoomDataManager.get().getCurrentRoomInfo() != null) {
            getMvpPresenter().getRoomRedPacketInfo(RoomDataManager.get().getCurrentRoomInfo().getRoomId());
        }
    }

    @Override
    public void addCompositeDisposable() {
        super.addCompositeDisposable();
        compositeDisposable.add(IMRoomMessageManager.get().getIMRoomEventObservable().subscribe(this::accept));
    }

    private void accept(IMRoomEvent imRoomEvent) {
        switch (imRoomEvent.getEvent()) {
            case IMRoomEvent.ROOM_RECEIVE_RED_PACKET:
                if (imRoomEvent.getRoomRedPacket() != null) {
                    currentRedPacket = imRoomEvent.getRoomRedPacket();
                    initRedPackageView(false);
                }
                break;

            case IMRoomEvent.ROOM_RECEIVE_RED_PACKET_FINISH:
                if (currentRedPacket != null
                        && StringUtils.isNotEmpty(currentRedPacket.getRedPacketId())
                        && currentRedPacket.getRedPacketId().equals(imRoomEvent.getRedPacketId())) {
                    Animation animation = new AlphaAnimation(1F, 0F);
                    animation.setDuration(1000);
                    rlRedPackage.startAnimation(animation);
                    rlRedPackage.setVisibility(View.GONE);
                    if (handler != null) {
                        handler.removeCallbacksAndMessages(null);
                    }
                }
                break;
        }
    }

    @Override
    public void initStateAndShowRedPackageView(List<RoomRedPacket> roomRedPackets) {
        if (!ListUtils.isListEmpty(roomRedPackets)) {
            currentRedPacket = roomRedPackets.get(0);
            initRedPackageView(true);
        }else {
            rlRedPackage.setVisibility(View.GONE);
            if (svRedPackageRain.isAnimating()) {
                svRedPackageRain.stopAnimation(true);
            }
            svRedPackageRain.setVisibility(View.GONE);
            if (handler != null) {
                handler.removeCallbacksAndMessages(null);
            }
        }
    }


    /**
     * 初始化红包状态
     * @param isHttpRequest
     */
    public void initRedPackageView(boolean isHttpRequest) {
        if (currentRedPacket != null) {
            //显示右下角红包View
            String nick = currentRedPacket.getNick();
            rlRedPackage.setVisibility(View.VISIBLE);
            tvUserName.setText(nick);
            if (currentRedPacket.getType() == 1) {//手气红包
                tvCountdown.setVisibility(View.GONE);
                if (!isHttpRequest) {//非网络请求的
                    //播放红包雨动画
                    playRedPacketRainSvgaAnim();
                }
            } else {
                tvCountdown.setVisibility(View.VISIBLE);
                long distance =(MsgViewHolderMiniGameInvited.getCurTimeMillis()-currentRedPacket.getRedPacketTime()) / 1000;
                int cutTime = Math.round((float) distance);
                if (cutTime >= currentRedPacket.getTiming()) {//红包倒计时已过
                    rlRedPackage.setEnabled(true);
                    tvCountdown.setVisibility(View.GONE);
                } else {
                    rlRedPackage.setEnabled(false);
                    countDown(currentRedPacket.getTiming() - cutTime);
                }
            }
        } else {
            rlRedPackage.setVisibility(View.GONE);
            if (svRedPackageRain.isAnimating()) {
                svRedPackageRain.stopAnimation(true);
            }
            svRedPackageRain.setVisibility(View.GONE);
            if (handler != null) {
                handler.removeCallbacksAndMessages(null);
            }
        }
    }

    /**
     * 红包倒计时
     *
     * @param count
     */
    private void countDown(int count) {
        if (handler == null) {
            handler = new Handler();
        }
        if (count <= 0) {
            handler.removeCallbacks(runnable);
            return;
        }
        countDuration = count;
        tvCountdown.setText(countDuration + "S");
        if (handler != null) {
            try {
                handler.removeCallbacks(runnable);
                handler.postDelayed(runnable, 1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            try {
                if (countDuration <= 0) {
                    tvCountdown.setVisibility(View.GONE);
                    rlRedPackage.setEnabled(true);
                    //播放红包雨动画
                    playRedPacketRainSvgaAnim();
                    handler.removeCallbacks(runnable);
                    return;
                }
                countDuration--;
                tvCountdown.setText(countDuration + "S");
                handler.postDelayed(this, 1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void showReceiveSucAnimAndDialog(RoomRedPacketReceiveResult data) {
        rlRedPackage.setEnabled(true);
//        rlRedPacketOpen.setVisibility(View.VISIBLE);
        rlRedPacketOpenNo.setVisibility(View.GONE);
        rlRedPacketOpenGet.setVisibility(View.VISIBLE);
        ImageView imvHeadImg = rlRedPacketOpenGet.findViewById(R.id.imvHeadImg);
        TextView tvName = rlRedPacketOpenGet.findViewById(R.id.tvName);
        TextView tvGetCount = rlRedPacketOpenGet.findViewById(R.id.tvGetCount);
        tvGetCount.setText(String.format("恭喜，你抢到了%d金币", data.getGoldNum()));
        ImageLoadUtils.loadCircleImage(getContext(), data.getAvatar(), imvHeadImg, R.drawable.ic_default_avatar);
        tvName.setText(data.getNick());
        playRedPacketOpenSvgaAnim(data, "red_packet_get.svga");
    }

    @Override
    public void showNoRedPackageDialog(RoomRedPacketReceiveResult data) {
        rlRedPackage.setEnabled(true);
        rlRedPacketOpen.setVisibility(View.VISIBLE);
        rlRedPacketOpenGet.setVisibility(View.GONE);
        rlRedPacketOpenNo.setVisibility(View.VISIBLE);
        ImageView imvHeadImg = rlRedPacketOpenNo.findViewById(R.id.imvHeadImg);
        TextView tvName = rlRedPacketOpenNo.findViewById(R.id.tvName);
        tvName.setText(data.getNick());
        ImageLoadUtils.loadCircleImage(getContext(), data.getAvatar(), imvHeadImg, R.drawable.ic_default_avatar);
        Animation animation = new AlphaAnimation(0F, 1F);
        animation.setDuration(700);
        rlRedPacketOpenNo.startAnimation(animation);
        rlRedPacketOpenNo.setVisibility(View.VISIBLE);
    }

    @Override
    public void showFailToast(String error) {
        rlRedPackage.setEnabled(true);
        SingleToastUtil.showToast(error);
    }


    /**
     * 播放红包雨动画
     */
    private void playRedPacketRainSvgaAnim() {
        if (currentRedPacket == null || currentRedPacket.getGoldNum() < 10000
                || StringUtils.isEmpty(currentRedPacket.getRedEnvelopedRain())) {
            return;
        }
        parseSvgaUrl(currentRedPacket.getRedEnvelopedRain());
        svRedPackageRain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rlRedPackage.setEnabled(false);
                svRedPackageRain.stopAnimation(true);
                svRedPackageRain.setVisibility(View.GONE);
                if (RoomDataManager.get().getCurrentRoomInfo() != null) {
                    getMvpPresenter().receiveRedPacket(RoomDataManager.get().getCurrentRoomInfo().getRoomId(), currentRedPacket.getRedPacketId());
                } else {
                    rlRedPackage.setEnabled(true);
                }
            }
        });
        svRedPackageRain.setCallback(new SVGACallback() {
            @Override
            public void onPause() {

            }

            @Override
            public void onFinished() {
            }

            @Override
            public void onRepeat() {
                svRedPackageRain.stopAnimation(true);
                svRedPackageRain.setVisibility(View.GONE);
            }

            @Override
            public void onStep(int i, double v) {

            }
        });
    }


    /**
     * 播放红包打开动画
     *
     * @param data
     * @param savgUrl
     */
    private void playRedPacketOpenSvgaAnim(RoomRedPacketReceiveResult data, String savgUrl) {
        if (StringUtils.isEmpty(savgUrl)) {
            return;
        }
        parseSvgaUrl(savgUrl);
        svRedPackageRain.setCallback(new SVGACallback() {
            @Override
            public void onPause() {

            }

            @Override
            public void onFinished() {
                Animation animation = new AlphaAnimation(0F, 1F);
                animation.setDuration(700);
                rlRedPacketOpen.startAnimation(animation);
                rlRedPacketOpen.setVisibility(View.VISIBLE);
            }

            @Override
            public void onRepeat() {
                svRedPackageRain.stopAnimation(true);
                svRedPackageRain.setVisibility(View.GONE);
            }

            @Override
            public void onStep(int i, double v) {

            }
        });
    }

    /**
     * 解析svga：本地和URL两种
     *
     * @param svgaUrl
     */
    private void parseSvgaUrl(String svgaUrl) {
        SVGAParser.ParseCompletion parseCompletion = new SVGAParser.ParseCompletion() {
            @Override
            public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {
                svRedPackageRain.setVisibility(View.VISIBLE);
                SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);
                svRedPackageRain.setImageDrawable(drawable);
                svRedPackageRain.startAnimation();
            }

            @Override
            public void onError() {
            }
        };
        SVGAParser svgaParser = new SVGAParser(getContext());
        try {
            if (svgaUrl.startsWith("http")) {
                URL url = new URL(svgaUrl);
                svgaParser.decodeFromURL(url, parseCompletion);
            } else {
                svgaParser.decodeFromAssets(svgaUrl, parseCompletion);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
