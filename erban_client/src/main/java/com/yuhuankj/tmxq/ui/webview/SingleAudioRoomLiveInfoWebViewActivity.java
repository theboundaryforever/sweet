package com.yuhuankj.tmxq.ui.webview;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.liveroom.im.IMUriProvider;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.R;

/**
 * 上传声鉴卡录音webview
 */
@CreatePresenter(SingleAudioRoomLiveInfoPresenter.class)
public class SingleAudioRoomLiveInfoWebViewActivity
        extends CommonWebViewActivity<ISingleAudioRoomLiveInfoView, SingleAudioRoomLiveInfoPresenter>
        implements ISingleAudioRoomLiveInfoView {

    private SingleAudioRoomLiveInfoJSInterface jsInterface;
    private ImageView ivLiveInfoPre;

    public static void start(Context context, boolean showTitle) {
        Intent intent = new Intent(context, SingleAudioRoomLiveInfoWebViewActivity.class);
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (null == roomInfo) {
            return;
        }
        intent.putExtra("url", IMUriProvider.getRoomPlayInfoUrl().concat("?roomId=")
                .concat(String.valueOf(roomInfo.getRoomId())));
        intent.putExtra("showTitle", showTitle);
        context.startActivity(intent);
    }

    @Override
    protected void initView() {
        super.initView();
        ivLiveInfoPre = (ImageView) findViewById(R.id.ivLiveInfoPre);
        ivLiveInfoPre.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPageLoadFinished(WebView view, String url) {
        super.onPageLoadFinished(view, url);
        LogUtils.d(TAG, "onPageLoadFinished hasTitleRecived:" + hasTitleRecived);
        if (hasTitleRecived) {
            ivLiveInfoPre.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TAG = SingleAudioRoomLiveInfoWebViewActivity.class.getSimpleName();
        jsInterface = new SingleAudioRoomLiveInfoJSInterface(webView, this);
        webView.removeJavascriptInterface("androidJsObj");
        webView.addJavascriptInterface(jsInterface, "androidJsObj");
    }

    @Override
    public boolean isStatusBarDarkFont() {
        return false;
    }

    /**
     * js调用，通知客户端调用服务器的停播接口
     */
    public void stopPlayRoom() {
        getDialogManager().showProgressDialog(this, getResources().getString(R.string.network_loading));
        getMvpPresenter().stopPlayRoom();
    }

    /**
     * 关闭当前统计界面
     */
    @Override
    public void finishLiveInfoView() {
        getDialogManager().dismissDialog();
        finish();
    }

    @Override
    public void showStopLiveErr(String msg) {
        getDialogManager().dismissDialog();
        if (!TextUtils.isEmpty(msg)) {
            toast(msg);
        }
    }
}
