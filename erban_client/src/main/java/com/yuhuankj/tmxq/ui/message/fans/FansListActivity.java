package com.yuhuankj.tmxq.ui.message.fans;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import com.tongdaxing.xchat_core.Constants;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;

/**
 * 粉丝列表
 */
public class FansListActivity extends BaseActivity {

    public static void start(Context context) {
        context.startActivity(new Intent(context, FansListActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fans);
        initTitleBar(getString(R.string.my_fan));
        if(null != mTitleBar){
            mTitleBar.setCommonBackgroundColor(Color.WHITE);
        }

        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container,  FansListFragment.newInstance(Constants.FAN_NO_MAIN_PAGE_TYPE), FansListFragment.class.getSimpleName())
                .commitAllowingStateLoss();


    }
}
