package com.yuhuankj.tmxq.ui.me.medal;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.MedalBean;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author liaoxy
 * @Description:勋章页
 * @date 2019/4/19 9:42
 */
public class MedalActivity extends BaseActivity {

    private View vBack;
    private RecyclerView rcvContent;
    private MedalAdapter medalAdapter;
    private List<MedalEntity> datas;
    private RelativeLayout rlMedalDetail;
    private ImageView imvMyMedal1;
    private ImageView imvMyMedal2;
    private LinearLayout llEmply;
    private TextView tvSelect;
    private int curIndexId1 = -1;
    private int curIndexId2 = -1;
    private long userId = -1;
    private int userFortuneLevel = -1;
    private boolean isSelect = false;
    private UserInfo userInfo;
    private TextView tvTitle;
    private TextView tvMedalTip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medal);
        initView();
        initData();
    }

    private void initView() {
        vBack = findViewById(R.id.vBack);
        rcvContent = (RecyclerView) findViewById(R.id.rcvContent);
        rlMedalDetail = (RelativeLayout) findViewById(R.id.rlMedalDetail);
        imvMyMedal1 = (ImageView) findViewById(R.id.imvMyMedal1);
        imvMyMedal2 = (ImageView) findViewById(R.id.imvMyMedal2);
        tvSelect = (TextView) findViewById(R.id.tvSelect);
        llEmply = (LinearLayout) findViewById(R.id.llEmply);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvMedalTip = (TextView) findViewById(R.id.tvMedalTip);
        tvSelect.setOnClickListener(this);
        vBack.setOnClickListener(this);
        rlMedalDetail.setOnClickListener(this);
    }

    private void initData() {
        if (null == CoreManager.getCore(IUserCore.class)) {
            toast("用户信息为空");
            finish();
            return;
        }
        userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo == null) {
            toast("用户信息为空");
            finish();
            return;
        }
        Intent intent = getIntent();
        if (intent == null) {
            toast("用户信息为空");
            finish();
            return;
        }
        userId = intent.getLongExtra("uid", -1);
        userFortuneLevel = intent.getIntExtra("userFortuneLevel", -1);
        List<MedalBean> curSetMedals = (ArrayList<MedalBean>) intent.getSerializableExtra("curSetMedals");
        if (userId == -1) {
            toast("用户信息为空");
            finish();
        }
        if (userId == userInfo.getUid()) {
            //如果是查看自己勋章
            tvTitle.setText("我的勋章");
            tvSelect.setVisibility(View.VISIBLE);
            curSetMedals = userInfo.getWearList();
        } else {
            //如果是查看他人勋章
            tvTitle.setText("勋章列表");
            tvSelect.setVisibility(View.INVISIBLE);
            tvMedalTip.setText("ta暂时还没有获得勋章～");
        }

        if (curSetMedals == null || curSetMedals.size() == 0) {
            imvMyMedal1.setImageResource(R.drawable.ic_medal_default);
        } else if (curSetMedals.size() == 1) {
            curIndexId1 = curSetMedals.get(0).getAliasId();
            ImageLoadUtils.loadImage(this, curSetMedals.get(0).getAlias(), imvMyMedal1, R.drawable.ic_medal_default);
        } else {
            curIndexId1 = curSetMedals.get(0).getAliasId();
            curIndexId2 = curSetMedals.get(1).getAliasId();
            ImageLoadUtils.loadImage(this, curSetMedals.get(0).getAlias(), imvMyMedal1, R.drawable.ic_medal_default);
            ImageLoadUtils.loadImage(this, curSetMedals.get(1).getAlias(), imvMyMedal2, R.drawable.ic_medal_default);
        }

        int useableCount = 1;
        if (userFortuneLevel >= 30) {
            //财富等级大于30级可佩戴两个勋章
            imvMyMedal2.setVisibility(View.VISIBLE);
            useableCount = 2;
        } else {
            if (curIndexId2 > 0) {
                curIndexId1 = curIndexId2;
            }
        }

        GridLayoutManager manager = new GridLayoutManager(this, 2);
        rcvContent.setLayoutManager(manager);

        datas = new ArrayList<>();
        medalAdapter = new MedalAdapter(datas, useableCount);
        medalAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                try {
                    if (isSelect) {
                        int curId = datas.get(position).getTitleId();
                        if (curId == curIndexId1) {
                            curIndexId1 = -1;
                            imvMyMedal1.setImageResource(R.drawable.ic_medal_default);
                            medalAdapter.setCurSelect(true, Arrays.asList(curIndexId1, curIndexId2));
                        } else if (curId == curIndexId2) {
                            curIndexId2 = -1;
                            imvMyMedal2.setImageResource(R.drawable.ic_medal_default);
                            medalAdapter.setCurSelect(true, Arrays.asList(curIndexId1, curIndexId2));
                        } else {
                            tvSelect.setText("保存");
                            if (curIndexId1 <= 0) {
                                curIndexId1 = datas.get(position).getTitleId();
                                ImageLoadUtils.loadImage(MedalActivity.this, datas.get(position).getPicture(), imvMyMedal1, R.drawable.ic_medal_default);
                            } else if (curIndexId2 <= 0 && imvMyMedal2.isShown()) {
                                curIndexId2 = datas.get(position).getTitleId();
                                ImageLoadUtils.loadImage(MedalActivity.this, datas.get(position).getPicture(), imvMyMedal2, R.drawable.ic_medal_default);
                            } else {
//                                if (imvMyMedal2.isShown()) {
//                                    toast("最多只能佩戴两个勋章");
//                                } else {
//                                    curIndexId1 = datas.get(position).getTitleId();
//                                    ImageLoadUtils.loadImage(MedalActivity.this, datas.get(position).getPicture(), imvMyMedal1, R.drawable.ic_medal_default);
//                                }
                            }
                            medalAdapter.setCurSelect(true, Arrays.asList(curIndexId1, curIndexId2));
                        }
                    } else {
                        showMedalDetail(datas.get(position));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        medalAdapter.bindToRecyclerView(rcvContent);

        getMedalList();
    }

    //获取勋章列表
    private void getMedalList() {
        getDialogManager().showProgressDialog(this, "请稍候...");
        new MedalModel().getMedalList(userId, new OkHttpManager.MyCallBack<ServiceResult<List<MedalEntity>>>() {
            @Override
            public void onError(Exception e) {
                getDialogManager().dismissDialog();
                e.printStackTrace();
                SingleToastUtil.showToast("获取勋章列表失败");
            }

            @Override
            public void onResponse(ServiceResult<List<MedalEntity>> response) {
                getDialogManager().dismissDialog();
                if (response == null) {
                    SingleToastUtil.showToast("获取勋章列表失败");
                } else {
                    if (response.isSuccess() && response.getData() != null) {
                        if (response.getData().size() == 0) {
                            llEmply.setVisibility(View.VISIBLE);
                            tvSelect.setEnabled(false);
                        } else {
                            llEmply.setVisibility(View.GONE);
                            tvSelect.setEnabled(true);
                        }
                        datas.clear();
                        datas.addAll(response.getData());
                        medalAdapter.notifyDataSetChanged();
                    } else if (TextUtils.isEmpty(response.getMessage())) {
                        SingleToastUtil.showToast(response.getMessage());
                    } else {
                        SingleToastUtil.showToast("获取勋章列表失败");
                    }
                }
            }
        });
    }

    //佩戴勋章
    private void setMedal(List<Integer> ids) {
        getDialogManager().showProgressDialog(this, "请稍候...");
        new MedalModel().setMedal(ids, new OkHttpManager.MyCallBack<ServiceResult<Object>>() {
            @Override
            public void onError(Exception e) {
                getDialogManager().dismissDialog();
                e.printStackTrace();
                SingleToastUtil.showToast("佩戴勋章失败");
            }

            @Override
            public void onResponse(ServiceResult<Object> response) {
                getDialogManager().dismissDialog();
                if (response == null) {
                    SingleToastUtil.showToast("佩戴勋章失败");
                } else {
                    if (response.isSuccess()) {
                        SingleToastUtil.showToast("保存成功");
                        tvSelect.setText("佩戴勋章");
                        medalAdapter.setCurSelect(false, Arrays.asList(curIndexId1, curIndexId2));
                        isSelect = false;
                        CoreManager.getCore(IUserCore.class).requestUserInfo(userInfo.getUid());
                    } else {
                        SingleToastUtil.showToast("佩戴勋章失败");
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view == vBack) {
            finish();
        } else if (view == rlMedalDetail) {
            rlMedalDetail.setVisibility(View.GONE);
        } else if (view == tvSelect) {
            if (isSelect) {
                try {
                    if (curIndexId1 == 0 && curIndexId2 == 0) {
                        SingleToastUtil.showToast("请选择要佩戴的勋章");
                        return;
                    }
                    setMedal(Arrays.asList(curIndexId1, curIndexId2));
                } catch (Exception e) {
                    e.printStackTrace();
                    SingleToastUtil.showToast("佩戴勋章失败");
                }
            } else {
                isSelect = true;
                tvSelect.setText("保存");
                medalAdapter.setCurSelect(true, Arrays.asList(curIndexId1, curIndexId2));
            }
        }
    }

    //显示勋章详情
    private void showMedalDetail(MedalEntity medal) {
        if (medal == null) {
            return;
        }
        TextView tvMedalDetailName = rlMedalDetail.findViewById(R.id.tvMedalDetailName);
        TextView tvMedalDetailGetStyle = rlMedalDetail.findViewById(R.id.tvMedalDetailGetStyle);
        TextView tvMedalDetailValidateTime = rlMedalDetail.findViewById(R.id.tvMedalDetailValidateTime);
        ImageView imvMyMedal = rlMedalDetail.findViewById(R.id.imvMyMedal);

        if (TextUtils.isEmpty(medal.getPicture())) {
            imvMyMedal.setImageResource(R.drawable.ic_medal_default);
        } else {
            ImageLoadUtils.loadImage(this, medal.getPicture(), imvMyMedal, R.drawable.ic_medal_default);
        }
        try {
            tvMedalDetailName.setText(medal.getName());
            tvMedalDetailGetStyle.setText(medal.getConditions());
            tvMedalDetailValidateTime.setText("有效期至" + getTime(medal.getEndTime(), "yyyy-MM-dd"));
            rlMedalDetail.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getTime(long timeInMillis, String format) {
        if (timeInMillis == 0) {
            return "";
        }
        DecimalFormat decimal = new DecimalFormat("0.0000000");
        String endZeroStr = decimal.format(timeInMillis).replace(".", "").substring(0, 13);
        timeInMillis = Long.valueOf(endZeroStr);
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(new Date(timeInMillis));
    }

    @Override
    public boolean isStatusBarDarkFont() {
        return false;
    }
}
