package com.yuhuankj.tmxq.ui.nim.chat;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.netease.nim.uikit.common.ui.recyclerview.adapter.BaseMultiItemFetchLoadAdapter;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.netease.nim.uikit.session.viewholder.MsgViewHolderBase;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.ButtonUtils;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.xchat_core.im.custom.bean.NoticeAttachment;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

/**
 * Created by chenran on 2017/9/21.
 */

public class MsgViewHolderContent extends MsgViewHolderBase implements View.OnClickListener{

    private ImageView bg;
    private TextView title;
    private TextView desc;
    private LinearLayout llContainer;

    public MsgViewHolderContent(BaseMultiItemFetchLoadAdapter adapter) {
        super(adapter);
        setShowHeadImage(false);
        setMiddleItem(true);
    }

    @Override
    protected boolean isShowBubble() {
        return false;
    }

    @Override
    protected int getContentResId() {
        return R.layout.layout_msg_view_holder_content;
    }

    @Override
    protected void inflateContentView() {
        bg = findViewById(R.id.bg_image);
        title = findViewById(R.id.title);
        desc = findViewById(R.id.desc);
        llContainer = findViewById(R.id.llContainer);
    }

    @Override
    protected void bindContentView() {
        NoticeAttachment attachment = (NoticeAttachment) message.getAttachment();
        if (!StringUtil.isEmpty(attachment.getPicUrl())) {
            bg.setVisibility(View.VISIBLE);
            ImageLoadUtils.loadImage(bg.getContext(), attachment.getPicUrl(), bg, R.drawable.bg_default_cover_round_placehold_5);
        } else {
            bg.setVisibility(View.GONE);
        }

        title.setText(attachment.getTitle());
        desc.setText(attachment.getDesc());
        llContainer.setOnClickListener(this);
        contentContainer.setBackground(null);
        if (isReceivedMessage()) {
            llContainer.setBackgroundResource(com.netease.nim.uikit.R.drawable.nim_message_content_left_bg);
        } else {
            llContainer.setBackgroundResource(com.netease.nim.uikit.R.drawable.nim_message_content_right_bg);
        }

        if (GlideContextCheckUtil.checkContextUsable(llContainer.getContext())) {
            if (llContainer.getLayoutParams() instanceof FrameLayout.LayoutParams) {
                FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) llContainer.getLayoutParams();
                lp.width = DisplayUtility.getScreenWidth(llContainer.getContext()) - DisplayUtility.dp2px(llContainer.getContext(), 30);
                lp.height = FrameLayout.LayoutParams.WRAP_CONTENT;
                llContainer.setLayoutParams(lp);
            }
        }

    }

    @Override
    public void onClick(View v) {
        if (ButtonUtils.isFastDoubleClick(v.getId())) {
            return;
        }
        NoticeAttachment attachment = (NoticeAttachment) message.getAttachment();
        if (!StringUtil.isEmpty(attachment.getWebUrl())) {
            CommonWebViewActivity.start(context, attachment.getWebUrl());
        }
    }
}
