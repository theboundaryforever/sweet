package com.yuhuankj.tmxq.ui.login;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aigestudio.wheelpicker.widgets.WheelDatePicker;
import com.aigestudio.wheelpicker.widgets.WheelDayPicker;
import com.aigestudio.wheelpicker.widgets.WheelMonthPicker;
import com.aigestudio.wheelpicker.widgets.WheelYearPicker;
import com.jph.takephoto.app.TakePhotoActivity;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.auth.ThirdUserInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.linked.ILinkedCore;
import com.tongdaxing.xchat_core.linked.LinkedInfo;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.MainActivity;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * @author liaoxy
 * @Description:资料完善页2
 * @date 2019/4/10 19:22
 */
public class SupplyInfo2Activity extends TakePhotoActivity implements IMvpBaseView, View.OnClickListener {
    private WheelDatePicker wheelDate;
    private WheelYearPicker wheelPickerYear;
    private WheelMonthPicker wheelPickerMonth;
    private WheelDayPicker wheelPickerDay;
    private TextView tvEnter;
    private UserInfo userInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_supply2);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        wheelDate = (WheelDatePicker) findViewById(R.id.wheelDate);
        tvEnter = (TextView) findViewById(R.id.tvEnter);

        wheelPickerYear = wheelDate.findViewById(R.id.wheel_date_picker_year);
        wheelPickerMonth = wheelDate.findViewById(R.id.wheel_date_picker_month);
        wheelPickerDay = wheelDate.findViewById(R.id.wheel_date_picker_day);

        LinearLayout.LayoutParams monthParams = (LinearLayout.LayoutParams) wheelPickerMonth.getLayoutParams();
        monthParams.leftMargin = ScreenUtil.dip2px(25);
        wheelPickerMonth.requestLayout();
        LinearLayout.LayoutParams dayParams = (LinearLayout.LayoutParams) wheelPickerDay.getLayoutParams();
        dayParams.leftMargin = ScreenUtil.dip2px(40);
        wheelPickerDay.requestLayout();
    }

    private void initListener() {
        tvEnter.setOnClickListener(this);
    }

    private void initData() {
        initTitleBar("");
        mTitleBar.setCommonBackgroundColor(Color.WHITE);

        Intent intent = getIntent();
        if (intent == null) {
            toast("用户信息为空");
            finish();
            return;
        }
        userInfo = new UserInfo();
        if (intent.hasExtra("gender")) {
            userInfo.setGender(intent.getIntExtra("gender", 1));
        }
        ThirdUserInfo thirdUserInfo = CoreManager.getCore(IAuthCore.class).getThirdUserInfo();
        if (thirdUserInfo != null && !TextUtils.isEmpty(thirdUserInfo.getUserIcon())) {
            userInfo.setAvatar(thirdUserInfo.getUserIcon());
            userInfo.setNick(thirdUserInfo.getUserName());
        }
        wheelDate.setVisibleItemCount(5);
        wheelDate.setItemTextSize(ScreenUtil.sp2px(27));
        wheelDate.setItemTextColor(Color.parseColor("#FFDFDCDC"));
        wheelDate.setSelectedItemTextColor(Color.parseColor("#FF333333"));
        wheelDate.setAtmospheric(true);
        wheelDate.setYearStart(1970);
        wheelDate.setYearEnd(2019);
        wheelDate.setYearAndMonth(1997, 1);
        wheelDate.setSelectedDay(1);
        wheelDate.setCyclic(true);
    }

    @Override
    public void onClick(View view) {
        if (view == tvEnter) {
            getDialogManager().showProgressDialog(this, "请稍候...");
            LinkedInfo linkedInfo = CoreManager.getCore(ILinkedCore.class).getLinkedInfo();
            String channel = "";
            String roomUid = "";
            String uid = "";
            if (linkedInfo != null) {
                channel = linkedInfo.getChannel();
                roomUid = linkedInfo.getRoomUid();
                uid = linkedInfo.getUid();
            }
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-d", Locale.getDefault());
            String dateStr = formatter.format(wheelDate.getCurrentDate());
            LogUtils.e("dateStr==" + dateStr);
            userInfo.setBirthStr(dateStr);
            userInfo.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            CoreManager.getCore(IUserCore.class).requestCompleteUserInfo(userInfo, channel, uid, roomUid);
            //点击保存
            StatisticManager.get().onEvent(this,
                    StatisticModel.EVENT_ID_CLICK_PRESERVE,
                    StatisticModel.getInstance().getUMAnalyCommonMap(this));
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoComplete(UserInfo userInfo) {
        getDialogManager().dismissDialog();
        //完善资料以后重新加载礼物列表,因为完善资料会送礼物给用户
        CoreManager.getCore(IGiftCore.class).requestGiftInfos();
        CoreManager.getCore(IAuthCore.class).setThirdUserInfo(null);
        CoreManager.getCore(IUserCore.class).requestUserInfo(userInfo.getUid());
        //通知SupplyInfo1Activity,注册流程走完，结束页面
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(RegisterActivity.ACTION_FINISH_REGISTER));
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(SupplyInfo1Activity.ACTION_FINISH_SUPPLY1));
        //通知主界面，注册流程走完，需要刷新萌新大礼包接口数据
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(MainActivity.ACTION_SHOW_SIGNAWARDDIALOG));
        finish();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoCompleteFaith(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }
}
