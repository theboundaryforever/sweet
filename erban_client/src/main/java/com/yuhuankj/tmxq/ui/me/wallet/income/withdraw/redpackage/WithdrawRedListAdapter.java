package com.yuhuankj.tmxq.ui.me.wallet.income.withdraw.redpackage;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.redpacket.bean.WithdrawRedListInfo;
import com.yuhuankj.tmxq.R;

/**
 * <p> 红包提现展示列表 </p>
 * Created by Administrator on 2017/11/20.
 */
public class WithdrawRedListAdapter extends BaseQuickAdapter<WithdrawRedListInfo, BaseViewHolder> {
    public WithdrawRedListAdapter() {
        super(R.layout.withdraw_item);
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, WithdrawRedListInfo withdrwaListInfo) {
        if (withdrwaListInfo == null) return;

        baseViewHolder.setText(R.id.list_name, "¥"+withdrwaListInfo.getPacketNum());

        LinearLayout layout = baseViewHolder.getView(R.id.select_withdraw);
        TextView money=baseViewHolder.getView(R.id.list_name);
        baseViewHolder.getView(R.id.text1).setVisibility(View.GONE);
        baseViewHolder.getView(R.id.text2).setVisibility(View.GONE);
        baseViewHolder.getView(R.id.tv_jewel).setVisibility(View.GONE);
        if (withdrwaListInfo.isSelected){
            layout.setBackgroundResource(R.drawable.img_bg_rise_select);
            money.setTextColor(BasicConfig.INSTANCE.getAppContext().getResources().getColor(R.color.color_FF4D4D));
        }else {
            layout.setBackgroundResource(R.drawable.img_bg_rise_noselect);
            money.setTextColor(BasicConfig.INSTANCE.getAppContext().getResources().getColor(R.color.black));
        }
    }
}
