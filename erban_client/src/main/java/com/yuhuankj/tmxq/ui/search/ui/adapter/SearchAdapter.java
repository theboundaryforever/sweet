package com.yuhuankj.tmxq.ui.search.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;

import com.tongdaxing.xchat_core.home.TabInfo;
import com.yuhuankj.tmxq.ui.search.ui.fragment.SearchFriendFragment;
import com.yuhuankj.tmxq.ui.search.ui.fragment.SearchRoomFragment;

import java.util.List;

/**
 * <p> 主页adapter  热门等 </p>
 * Created by Administrator on 2017/11/15.
 */
public class SearchAdapter extends FragmentPagerAdapter {
    private SparseArray<Fragment> mFragmentList;
    private List<TabInfo> mTitleList;

    public SearchAdapter(FragmentManager fm, List<TabInfo> titleList) {
        super(fm);
        this.mTitleList = titleList;
        mFragmentList = new SparseArray<>();
        mFragmentList.put(0, new SearchRoomFragment());
        mFragmentList.put(1, new SearchFriendFragment());
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mTitleList == null ? 0 : mTitleList.size();
    }
}