package com.yuhuankj.tmxq.ui.signAward.model;

import java.io.Serializable;

/**
 * 连续签到接口返回实体
 *
 * @author weihaitao
 * @date 2019/5/22
 */
public class SignAwardResult implements Serializable {

    /**
     * 连续签到天数
     */
    private int signDay;

    /**
     * 签到描述语
     */
    private String signDesc;

    /**
     * 签到物品名称
     */
    private String signName;

    /**
     * 签到物品图片
     */
    private String signPic;

    public int getSignDay() {
        return signDay;
    }

    public void setSignDay(int signDay) {
        this.signDay = signDay;
    }

    public String getSignDesc() {
        return signDesc;
    }

    public void setSignDesc(String signDesc) {
        this.signDesc = signDesc;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public String getSignPic() {
        return signPic;
    }

    public void setSignPic(String signPic) {
        this.signPic = signPic;
    }
}
