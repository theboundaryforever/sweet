package com.yuhuankj.tmxq.ui.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.erban.libcommon.utils.toast.ToastCompat;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;

import java.text.NumberFormat;

/**
 * 第三方平台绑定信息展示界面
 */
@CreatePresenter(GetSmsCodeToBindThirdPlatformPresenter.class)
public class GetSmsCodeToBindThirdPlatformActivity extends BaseMvpActivity<GetSmsCodeToBindThirdPlatformView, GetSmsCodeToBindThirdPlatformPresenter>
        implements GetSmsCodeToBindThirdPlatformView {

    private final String TAG = GetSmsCodeToBindThirdPlatformActivity.class.getSimpleName();

    private TextView tv_bindWxTips;
    private EditText et_phone;
    private EditText et_smscode;
    private Button btnGetCode;
    private Button btn_sureBind;
    private TextWatcher textWatcher;
    private CodeDownTimer mTimer;
    private int third_platform_type = 0;//1--wx,2--qq
    private boolean bindOrUnbind;

    public static void start(Context context, int third_platform_type, boolean bindOrUnbind) {
        Intent intent = new Intent(context, GetSmsCodeToBindThirdPlatformActivity.class);
        intent.putExtra("third_platform_type", third_platform_type);
        intent.putExtra("bindOrUnbind", bindOrUnbind);
        context.startActivity(intent);
    }

    public static void startForResult(FragmentActivity activity, int third_platform_type, boolean bindOrUnbind, int requestCode) {
        Intent intent = new Intent(activity, GetSmsCodeToBindThirdPlatformActivity.class);
        intent.putExtra("third_platform_type", third_platform_type);
        intent.putExtra("bindOrUnbind", bindOrUnbind);
        activity.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bind_thirdplatform_code);
        initView();
        initData();
        initTitleBar(getResources().getString(third_platform_type == 2 ?
                bindOrUnbind ? R.string.bind_sms_code_title_bind_qq : R.string.bind_sms_code_title_unbind_qq :
                bindOrUnbind ? R.string.bind_sms_code_title_bind_wx : R.string.bind_sms_code_title_unbind_wx));
        btn_sureBind.setText(getResources().getString(bindOrUnbind ? R.string.bind_sms_code_sure_bind : R.string.bind_sms_code_sure_unbind));
        setListener();

    }

    private void initView() {
        tv_bindWxTips = (TextView) findViewById(R.id.tv_bindWxTips);
        et_phone = (EditText) findViewById(R.id.et_phone);
        GrowingIO.getInstance().trackEditText(et_phone);
        et_smscode = (EditText) findViewById(R.id.et_smscode);
        GrowingIO.getInstance().trackEditText(et_smscode);
        btnGetCode = (Button) findViewById(R.id.btn_get_code);
        btn_sureBind = (Button) findViewById(R.id.btn_sureBind);
    }

    private void initData() {
        third_platform_type = getIntent().getIntExtra("third_platform_type", 1);
        bindOrUnbind = getIntent().getBooleanExtra("bindOrUnbind", true);
        tv_bindWxTips.setVisibility(2 == third_platform_type ? View.GONE : View.VISIBLE);
        if (null != CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(CoreManager.getCore(IAuthCore.class).getCurrentUid())) {
            et_phone.setText(CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(CoreManager.getCore(IAuthCore.class).getCurrentUid()).getPhone());
            et_phone.setEnabled(false);
        }
    }

    private void setListener() {
        //获取绑定手机验证码
        btnGetCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = et_phone.getText().toString().trim();
                Number parse = null;
                try {
                    parse = NumberFormat.getIntegerInstance().parse(phoneNumber);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (parse == null || parse.intValue() == 0 || phoneNumber.length() != 11) {
                    ToastCompat.makeText(BasicConfig.INSTANCE.getAppContext(), R.string.bind_sms_code_format_tips, ToastCompat.LENGTH_SHORT).show();
                    return;
                }
                mTimer = new CodeDownTimer(btnGetCode, 60000, 1000);
                mTimer.start();

                //发起获取验证码请求
                getMvpPresenter().getSmsCode(et_phone.getText().toString());
            }
        });

        //绑定验证手机
        btn_sureBind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialogManager().showProgressDialog(GetSmsCodeToBindThirdPlatformActivity.this, getResources().getString(R.string.loading));
                getMvpPresenter().checkCode(et_smscode.getText().toString());
            }
        });
        //输入框监听改变
        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean usuable = et_phone.getText() != null && et_phone.getText().length() > 0
                        && et_smscode.getText() != null && et_smscode.getText().length() > 0;
                btn_sureBind.setBackgroundDrawable(getResources().getDrawable(usuable ? R.drawable.shape_green_gradient_5dp : R.drawable.shape_gray_5dp));
                btn_sureBind.setTextColor(getResources().getColor(usuable ? R.color.colorPrimaryDark : R.color.white));
                btn_sureBind.setClickable(usuable);
                btn_sureBind.setFocusable(usuable);
                btn_sureBind.setEnabled(usuable);
            }
        };

        et_phone.addTextChangedListener(textWatcher);
        et_smscode.addTextChangedListener(textWatcher);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getDialogManager().dismissDialog();
        if (null != mTimer) {
            mTimer.cancel();
            mTimer = null;
        }
    }

    @Override
    public void onGetSmsCode(boolean isSuccess, String message) {
        if (!isSuccess && !TextUtils.isEmpty(message)) {
            toast(message);
        }
    }

    @Override
    public void onBindThirdPlatform(boolean isSuccess, String message, int third_platform_type) {
        getDialogManager().dismissDialog();
        if (isSuccess) {
            Intent intent = new Intent();
            intent.putExtra("third_platform_type", third_platform_type);
            setResult(Activity.RESULT_OK);
            this.finish();
        } else if (!TextUtils.isEmpty(message)) {
            toast(message);
        }
    }

    @Override
    public void onUnBindThirdPlatform(boolean isSuccess, String message, int third_platform_type) {
        getDialogManager().dismissDialog();
        if (isSuccess) {
            setResult(Activity.RESULT_OK);
            this.finish();
        } else if (!TextUtils.isEmpty(message)) {
            toast(message);
        }
    }

    @Override
    public void onCheckSmsCode(boolean isSuccess, String message) {
        if (isSuccess) {
            if (bindOrUnbind) {
                getMvpPresenter().getThirdPlatformAccessToken(third_platform_type);
            } else {
                getMvpPresenter().unBindThirdPlatform(third_platform_type, et_smscode.getText().toString());
            }
        } else {
            getDialogManager().dismissDialog();
            if (!TextUtils.isEmpty(message)) {
                toast(message);
            }
        }
    }

    @Override
    public void onThirdPlatformLogined(boolean isSuccess, String message, int third_platform_type,
                                       String accessToken, String openId, String unionId) {
        if (isSuccess) {
            getMvpPresenter().bindThirdPlatform(et_phone.getText().toString(), third_platform_type, accessToken, et_smscode.getText().toString(), openId, unionId);
        } else {
            getDialogManager().dismissDialog();
            if (!TextUtils.isEmpty(message)) {
                toast(message);
            }
        }
    }
}
