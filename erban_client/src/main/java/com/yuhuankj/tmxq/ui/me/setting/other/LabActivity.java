package com.yuhuankj.tmxq.ui.me.setting.other;

import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.erban.libcommon.utils.pref.CommonPref;
import com.tongdaxing.xchat_core.Env;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.XChatApplication;
import com.yuhuankj.tmxq.base.activity.BaseActivity;

/**
 * Created by chenran on 2017/10/16.
 */

public class LabActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lab);

        int enviroment = CommonPref.instance(BasicConfig.INSTANCE.getAppContext()).getInt("enviroment");
        //根据ID找到RadioGroup实例
        RadioGroup group = (RadioGroup) this.findViewById(R.id.radioGroup);
        RadioButton button = (RadioButton) findViewById(R.id.product);
        RadioButton button1 = (RadioButton) findViewById(R.id.test);
        if (enviroment == 0) {
            button.setChecked(true);
        } else {
            button1.setChecked(true);
        }
        //绑定一个匿名监听器
        group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup arg0, int arg1) {
                // TODO Auto-generated method stub
                //获取变更后的选中项的ID
                if (arg1 == R.id.test) {
                    CommonPref.instance(BasicConfig.INSTANCE.getAppContext()).putInt("enviroment", 1);
                } else {
                    CommonPref.instance(BasicConfig.INSTANCE.getAppContext()).putInt("enviroment", 0);
                }
                Env.instance().init();
                XChatApplication.initRxNet(BasicConfig.INSTANCE.getAppContext(), UriProvider.JAVA_WEB_URL);
                CoreManager.getCore(IAuthCore.class).logout();
                finish();
            }
        });
    }
}
