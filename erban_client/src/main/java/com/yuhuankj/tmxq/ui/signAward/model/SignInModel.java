package com.yuhuankj.tmxq.ui.signAward.model;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;

import java.util.Map;

/**
 * @author weihaitao
 * @date 2019/5/23
 */
public class SignInModel {

    /**
     * 获取萌新大礼包和连续签到奖励
     *
     * @param callBack
     */
    public void getNewsBigGift(HttpRequestCallBack<SignAwardResponse> callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if (0L == uid) {
            return;
        }
        params.put("uid", uid + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket() + "");
        OkHttpManager.getInstance().getRequest(UriProvider.getNewsBigGift(), params, callBack);
    }

    /**
     * 领取萌新大礼包接口
     *
     * @param getDay   领取天数 1、第一天 2、第二天 3、第三天 必填
     * @param micUid   麦上用户ID
     * @param roomUid  房主UID
     * @param roomId   房间ID
     * @param roomType 房间类型
     * @param callBack
     */
    public void receiveNewsBigGift(int getDay, long micUid, long roomId, int roomType,
                                   long roomUid, HttpRequestCallBack<RecvNewsGiftPkgResult> callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if (0L == uid) {
            return;
        }
        params.put("getDay", getDay + "");
        if (micUid > 0) {
            params.put("micUid", micUid + "");
        }
        if (roomId > 0) {
            params.put("roomId", roomId + "");
        }
        if (roomType > 0) {
            params.put("roomType", roomType + "");
        }
        if (roomUid > 0) {
            params.put("roomUid", roomUid + "");
        }
        params.put("uid", uid + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket() + "");
        OkHttpManager.getInstance().postRequest(UriProvider.receiveNewsBigGift(), params, callBack);
    }

    /**
     * 获取萌新大礼包和连续签到奖励
     *
     * @param callBack
     */
    public void signInMission(HttpRequestCallBack<SignAwardResult> callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if (0L == uid) {
            return;
        }
        params.put("uid", uid + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket() + "");
        OkHttpManager.getInstance().postRequest(UriProvider.signInMission(), params, callBack);
    }

    /**
     * 获取萌新大礼包和连续签到奖励
     *
     * @param callBack
     */
    public void receiveSignInGift(HttpRequestCallBack<SignAwardResult> callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if (0L == uid) {
            return;
        }
        params.put("uid", uid + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket() + "");
        OkHttpManager.getInstance().postRequest(UriProvider.receiveSignInGift(), params, callBack);
    }
}
