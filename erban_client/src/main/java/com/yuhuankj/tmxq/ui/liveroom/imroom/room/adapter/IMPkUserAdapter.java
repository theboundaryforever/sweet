package com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomQueueInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

/**
 * 文件描述：新的IM的PK功能：麦位用户选择的adapter
 *
 * @auther：zwk
 * @data：2019/7/8
 */
public class IMPkUserAdapter extends BaseQuickAdapter<IMRoomQueueInfo, BaseViewHolder> {

    public IMPkUserAdapter() {
        super(R.layout.item_pk_user);
    }

    @Override
    public int getItemCount() {
        return 8;
    }

    @Override
    protected void convert(BaseViewHolder helper, IMRoomQueueInfo item) {
        ImageView imvUser = helper.getView(R.id.imvUser);
        View vSelect = helper.getView(R.id.vSelect);
        TextView tvUserNick = helper.getView(R.id.tvUserNick);
        if (item == null || item.mChatRoomMember == null) {
            imvUser.setImageResource(R.drawable.ic_pk_user_emply);
            tvUserNick.setText("");
            vSelect.setVisibility(View.INVISIBLE);
        }else {
            ImageLoadUtils.loadCircleImage(mContext, item.mChatRoomMember.getAvatar(), imvUser, R.drawable.ic_default_avator_circle);
            tvUserNick.setText(item.mChatRoomMember.getNick());
            if (item.isSelect()) {
                vSelect.setVisibility(View.VISIBLE);
            } else {
                vSelect.setVisibility(View.INVISIBLE);
            }
        }
    }
}
