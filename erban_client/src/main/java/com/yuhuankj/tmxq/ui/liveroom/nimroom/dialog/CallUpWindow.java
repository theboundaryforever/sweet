package com.yuhuankj.tmxq.ui.liveroom.nimroom.dialog;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.tongdaxing.xchat_core.im.custom.bean.CallUpBean;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.AVRoomActivity;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

public class CallUpWindow extends PopupWindow {

    private View vMain;
    private TextView tvRefuse;
    private CountDownTimer timer;
    private CallUpBean callUpBean;

    public CallUpWindow(Context context, CallUpBean callUpBean) {
        super(context);
        this.callUpBean = callUpBean;
        vMain = LayoutInflater.from(context).inflate(R.layout.layout_callup, null);
        setContentView(vMain);
        initData(context);
    }

    private void initData(Context context) {
        setWidth(ScreenUtil.getScreenWidth(context));
        setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setAnimationStyle(R.style.CallUpAnimation);

        ImageView imvAvatar = vMain.findViewById(R.id.imvAvatar);
        TextView tvName = vMain.findViewById(R.id.tvName);
        TextView tvContent = vMain.findViewById(R.id.tvContent);
        TextView tvAgree = vMain.findViewById(R.id.tvAgree);
        tvRefuse = vMain.findViewById(R.id.tvRefuse);

        if (callUpBean != null) {
            ImageLoadUtils.loadCircleImage(imvAvatar.getContext(), callUpBean.getAvatar(), imvAvatar, R.drawable.ic_default_avatar);
            tvName.setText(callUpBean.getNick());
            tvContent.setText(callUpBean.getMessage());
        }

        tvAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AVRoomActivity.start(tvAgree.getContext(), callUpBean.getRoomUid());
            }
        });

        tvRefuse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss() {
                if (timer != null) {
                    timer.cancel();
                    timer = null;
                }
            }
        });
    }

    @Override
    public void showAtLocation(View parent, int gravity, int x, int y) {
        super.showAtLocation(parent, gravity, x, y);
        if (callUpBean == null) {
            dismiss();
            return;
        }
        timer = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                String timeStr = String.format("拒绝(%d)", millisUntilFinished / 1000);
                tvRefuse.setText(timeStr);
            }

            @Override
            public void onFinish() {
                dismiss();
            }
        };
        timer.start();
    }
}
