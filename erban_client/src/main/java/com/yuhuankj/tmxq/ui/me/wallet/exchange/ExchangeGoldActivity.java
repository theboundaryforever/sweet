package com.yuhuankj.tmxq.ui.me.wallet.exchange;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.RealNameAuthStatusChecker;
import com.tongdaxing.xchat_core.pay.bean.ExchangeAwardInfo;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.ui.user.password.PaypwdDialog;
import com.yuhuankj.tmxq.ui.widget.ExchangeAwardsDialog;
import com.yuhuankj.tmxq.ui.widget.ExchangeVerificationDialog;

import java.util.Locale;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MadisonRong on 09/01/2018.
 */
@CreatePresenter(ExchangeGoldPresenter.class)
public class ExchangeGoldActivity extends BaseMvpActivity<IExchangeGoldView, ExchangeGoldPresenter>
        implements IExchangeGoldView, View.OnClickListener, TextWatcher {

    @BindView(R.id.et_exchange_gold_diamond_amount)
    EditText inputEditText;
    @BindView(R.id.tv_exchange_gold_diamond_balance)
    TextView diamondBalanceTextView;
    @BindView(R.id.tv_exchange_gold_result)
    TextView resultTextView;
    @BindView(R.id.tv_exchange_gold_gold_balance)
    TextView goldBalanceTextView;
    @BindView(R.id.btn_exchange_gold_confirm)
    Button confirmButton;
    @BindString(R.string.exchange_gold_title)
    String titleContent;
    @BindString(R.string.exchange_gold_loading)
    String loading;
    private ExchangeVerificationDialog exchangeVerificationDialog;
    private UserInfo userInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exchange_gold);
        ButterKnife.bind(this);
        initTitleBar(titleContent);
        initListeners();

        GrowingIO.getInstance().trackEditText(inputEditText);
    }

    @Override
    protected void onResume() {
        super.onResume();
        userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        getMvpPresenter().refreshWalletInfo(false);
    }

    private void initListeners() {
        confirmButton.setOnClickListener(this);
        inputEditText.addTextChangedListener(this);
    }

    @Override
    public void onClick(View view) {
        if (userInfo == null) {
            SingleToastUtil.showToast("用户数据为空");
            return;
        }
        String input = inputEditText.getText().toString();
        if (StringUtil.isEmpty(input)) {
            SingleToastUtil.showToast(getResources().getString(R.string.exchange_gold_error_empty_input));
            return;
        }

        int value = Integer.parseInt(input);
        if (value % 10 != 0) {
            SingleToastUtil.showToast(getResources().getString(R.string.exchange_gold_error_is_not_ten_multiple));
            return;
        }

        if (getMvpPresenter().getWalletInfo() == null) {
            SingleToastUtil.showToast("钱包数据为空");
            return;
        }
        if (value > getMvpPresenter().getWalletInfo().getDiamondNum()) {
            SingleToastUtil.showToast(getResources().getString(R.string.exchange_gold_error_diamond_less));
            return;
        }
        //只有部分牌照才需要绑定手机号，这里不强制要求绑定手机号了
//        if (!userInfo.isBindPhone()) {
//            startActivity(new Intent(this, BinderPhoneActivity.class));
//            return;
//        }
        if (!userInfo.isPayPassword()) {
            //没有设置提现密码按以前的逻辑走
            getMvpPresenter().confirmToExchangeGold(input);
        } else {
            //设置了提现密码，要判断提现密码的正确性
//            Intent intent = new Intent(this, SetingPwdActivity.class);
//            intent.putExtra("type", 2);
//            startActivity(intent);
            PaypwdDialog paypwdDialog = new PaypwdDialog(ExchangeGoldActivity.this, "确认兑换", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getMvpPresenter().confirmToExchangeGold(input);
                }
            });
            paypwdDialog.show();
        }
    }

    @Override
    public void refreshUserWalletBalance(WalletInfo walletInfo) {
        diamondBalanceTextView.setText(String.format(Locale.getDefault(), "%.1f", walletInfo.diamondNum));
//        goldBalanceTextView.setText(String.format(Locale.getDefault(), "%.1f", walletInfo.goldNum));
        goldBalanceTextView.setText(String.format(Locale.getDefault(), "%.1f", walletInfo.goldNum));
    }

    @Override
    public void getUserWalletInfoFail(String error) {
        toast(error);
        diamondBalanceTextView.setText("0");
        goldBalanceTextView.setText("0");
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (inputEditText.getText().toString().equals("0")) {
            inputEditText.setText("");
        }
        String str = inputEditText.getText().toString();
        getMvpPresenter().calculateResult(str);
    }

    @Override
    public void toastForError(int errorResId) {
        toast(errorResId);
    }

    @Override
    public void displayResult(String result) {
        resultTextView.setText(result);
    }

    @Override
    public void requestExchangeGold(int value) {
        getDialogManager().showProgressDialog(ExchangeGoldActivity.this, loading);
        getMvpPresenter().exchangeGold(String.valueOf(value));
    }

    @Override
    public void requestExchangeGold(int value, String sms) {
        getDialogManager().showProgressDialog(ExchangeGoldActivity.this, loading);
        getMvpPresenter().exchangeGold(String.valueOf(value), sms);
    }

    @Override
    public void exchangeGold(WalletInfo walletInfo) {
        getDialogManager().dismissDialog();
        toast(R.string.exchange_gold_success);
        inputEditText.setText("");
        resultTextView.setText("0");
        if (exchangeVerificationDialog != null)
            exchangeVerificationDialog.dismiss();
    }

    @Override
    public void exchangeGoldFail(int code, String error) {
        getDialogManager().dismissDialog();
        if (code == RealNameAuthStatusChecker.RESULT_FAILED_NEED_REAL_NAME_AUTH) {
            CoreManager.notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_NEED_REAL_NAME_AUTH, this,
                    getResources().getString(R.string.real_name_auth_tips,
                            getResources().getString(R.string.real_name_auth_tips_exchange)));

            return;
        }

        //需要验证账号的
        if (code == 401) {
            if (exchangeVerificationDialog != null)
                exchangeVerificationDialog.dismiss();

            String phone = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getPhone();
            if (TextUtils.isEmpty(phone) || phone.length() != 11) {
                toast(R.string.third_platform_bind_phone_need);
                return;
            }

            exchangeVerificationDialog = ExchangeVerificationDialog.newInstance();
            exchangeVerificationDialog.iOnSubmit = new ExchangeVerificationDialog.IOnSubmit() {
                @Override
                public void onSubmit(String sms) {
                    String str = inputEditText.getText().toString();
                    getMvpPresenter().confirmToExchangeGold(str, sms);
                }
            };
            exchangeVerificationDialog.show(getSupportFragmentManager(), null);

            return;
        }
        if (!TextUtils.isEmpty(error)) {
            toast(error);
        }
    }

    @Override
    public void showAward(ExchangeAwardInfo data) {

        String drawMsg = data.getDrawMsg();
        String drawUrl = data.getDrawUrl();

        if (TextUtils.isEmpty(drawMsg) || TextUtils.isEmpty(drawUrl)) {
            return;
        }
        ExchangeAwardsDialog exchangeAwardsDialog = ExchangeAwardsDialog.newInstance();
        exchangeAwardsDialog.setData(data);
        exchangeAwardsDialog.show(getSupportFragmentManager(), null);
    }
}
