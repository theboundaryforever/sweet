package com.yuhuankj.tmxq.ui.me.medal;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;

import java.util.List;
import java.util.Map;

/**
 * 勋章model层
 */
public class MedalModel extends BaseMvpModel {

    //获取勋章列表
    public void getMedalList(long queryUid, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("queryUid", String.valueOf(queryUid));
        OkHttpManager.getInstance().postRequest(UriProvider.getMedalList(), params, callBack);
    }

    //佩戴勋章
    public void setMedal(List<Integer> titleIds, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < titleIds.size(); i++) {
            long targetUid = titleIds.get(i);
            sb.append(targetUid);
            sb.append(",");
        }
        sb.deleteCharAt(sb.length() - 1);
        params.put("titleIds", sb.toString());
        OkHttpManager.getInstance().postRequest(UriProvider.setMedal(), params, callBack);
    }
}
