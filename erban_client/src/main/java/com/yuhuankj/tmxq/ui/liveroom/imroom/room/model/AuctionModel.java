package com.yuhuankj.tmxq.ui.liveroom.imroom.room.model;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_core.room.auction.ParticipateAuctionBean;
import com.tongdaxing.xchat_core.room.auction.ParticipateAuctionListBean;

import java.util.Map;

/**
 * 文件描述：竞拍房的
 *
 * @auther：zwk
 * @data：2019/7/30
 */
public class AuctionModel extends BaseMvpModel {


    /**
     * 获取当前房间等待参加竞拍用户列表
     *
     * @param roomId
     * @param myCallBack
     */
    public void getRoomAuctionList(long roomId,CallBack<ParticipateAuctionListBean> myCallBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomId",String.valueOf(roomId));
        OkHttpManager.getInstance().postRequest(UriProvider.joinRoomAuctionListUrl(), params, new OkHttpManager.MyCallBack<ServiceResult<ParticipateAuctionListBean>>() {
            @Override
            public void onError(Exception e) {
                if (myCallBack != null) {
                    myCallBack.onFail(OkHttpManager.DEFAULT_CODE_ERROR, e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<ParticipateAuctionListBean> response) {
                if (myCallBack != null) {
                    if (response != null) {
                        if (response.isSuccess()) {
                            myCallBack.onSuccess(response.getData());
                        } else {
                            myCallBack.onFail(response.getCode(), response.getMessage());
                        }
                    } else {
                        myCallBack.onFail(OkHttpManager.DEFAULT_CODE_ERROR, "数据异常");
                    }
                }
            }
        });
    }

    /**
     * 获取当前房间等待参加竞拍用户列表
     *
     * @param roomId
     * @param myCallBack
     */
    public void getRoomAuctionList(long roomId,long uid,CallBack<ParticipateAuctionListBean> myCallBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(uid));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomId",String.valueOf(roomId));
        OkHttpManager.getInstance().postRequest(UriProvider.joinRoomAuctionListUrl(), params, new OkHttpManager.MyCallBack<ServiceResult<ParticipateAuctionListBean>>() {
            @Override
            public void onError(Exception e) {
                if (myCallBack != null) {
                    myCallBack.onFail(OkHttpManager.DEFAULT_CODE_ERROR, e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<ParticipateAuctionListBean> response) {
                if (myCallBack != null) {
                    if (response != null) {
                        if (response.isSuccess()) {
                            myCallBack.onSuccess(response.getData());
                        } else {
                            myCallBack.onFail(response.getCode(), response.getMessage());
                        }
                    } else {
                        myCallBack.onFail(OkHttpManager.DEFAULT_CODE_ERROR, "数据异常");
                    }
                }
            }
        });
    }

    /**
     * 开始/取消竞拍
     *
     * @param type 1参加2取消
     * @param roomId
     * @param day
     * @param project
     * @param myCallBack
     */
    public void joinOrCancelRoomAuction(int type,long roomId, int day,String project, CallBack<ParticipateAuctionBean> myCallBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("type", String.valueOf(type));
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomId", String.valueOf(roomId));
        params.put("day", String.valueOf(day));
        params.put("project", project);
        OkHttpManager.getInstance().postRequest(UriProvider.joinRoomAuctionUrl(), params, new OkHttpManager.MyCallBack<ServiceResult<ParticipateAuctionBean>>() {
            @Override
            public void onError(Exception e) {
                if (myCallBack != null) {
                    myCallBack.onFail(OkHttpManager.DEFAULT_CODE_ERROR, e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<ParticipateAuctionBean> response) {
                if (myCallBack != null) {
                    if (response != null) {
                        if (response.isSuccess()) {
                            myCallBack.onSuccess(response.getData());
                        } else {
                            myCallBack.onFail(response.getCode(), response.getMessage());
                        }
                    } else {
                        myCallBack.onFail(OkHttpManager.DEFAULT_CODE_ERROR, "数据异常");
                    }
                }
            }
        });
    }


    /**
     * 开始竞拍
     *
     * @param roomId
     * @param auctionUid
     * @param myCallBack
     */
    public void startRoomAuction(long roomId, long auctionUid, CallBack<ParticipateAuctionBean> myCallBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomId", String.valueOf(roomId));
        params.put("auctionUid", String.valueOf(auctionUid));
        OkHttpManager.getInstance().postRequest(UriProvider.startRoomAuctionUrl(), params, new OkHttpManager.MyCallBack<ServiceResult<ParticipateAuctionBean>>() {
            @Override
            public void onError(Exception e) {
                if (myCallBack != null) {
                    myCallBack.onFail(OkHttpManager.DEFAULT_CODE_ERROR, e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<ParticipateAuctionBean> response) {
                if (myCallBack != null) {
                    if (response != null) {
                        if (response.isSuccess()) {
                            myCallBack.onSuccess(response.getData());
                        } else {
                            myCallBack.onFail(response.getCode(), response.getMessage());
                        }
                    } else {
                        myCallBack.onFail(OkHttpManager.DEFAULT_CODE_ERROR, "数据异常");
                    }
                }
            }
        });
    }


    /**
     * 结束竞拍
     * @param roomId
     * @param auctionUid
     * @param myCallBack
     */
    public void endRoomAuction(long roomId,long auctionUid,CallBack<ParticipateAuctionBean> myCallBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomId", String.valueOf(roomId));
        params.put("auctionUid", String.valueOf(auctionUid));
        OkHttpManager.getInstance().postRequest(UriProvider.endRoomAuctionUrl(), params, new OkHttpManager.MyCallBack<ServiceResult<ParticipateAuctionBean>>() {
            @Override
            public void onError(Exception e) {
                if (myCallBack != null) {
                    myCallBack.onFail(OkHttpManager.DEFAULT_CODE_ERROR, e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<ParticipateAuctionBean> response) {
                if (myCallBack != null) {
                    if (response != null) {
                        if (response.isSuccess()) {
                            myCallBack.onSuccess(response.getData());
                        } else {
                            myCallBack.onFail(response.getCode(), response.getMessage());
                        }
                    } else {
                        myCallBack.onFail(OkHttpManager.DEFAULT_CODE_ERROR, "数据异常");
                    }
                }
            }
        });
    }


    /**
     * 竞拍房赠送优先卡接口
     * @param roomId
     * @param hostUid
     * @param myCallBack
     */
    public void sendAuctionPriorityCard(long roomId,long hostUid,int num,CallBack<String> myCallBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomId", String.valueOf(roomId));
        if (hostUid > 0) {
            params.put("hostUid", String.valueOf(hostUid));
        }
        params.put("num", String.valueOf(num));
        OkHttpManager.getInstance().postRequest(UriProvider.sendAuctionPriorityCardUrl(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                if (myCallBack != null) {
                    myCallBack.onFail(OkHttpManager.DEFAULT_CODE_ERROR, e.getMessage());
                }
            }

            @Override
            public void onResponse(Json response) {
                if (myCallBack != null) {
                    if (response != null) {
                        int code = response.num("code");
                        String message = response.str("message");
                        if (response.num("code") == 200) {
                            myCallBack.onSuccess(message);
                        } else {
                            myCallBack.onFail(code,message);
                        }
                    } else {
                        myCallBack.onFail(OkHttpManager.DEFAULT_CODE_ERROR, "数据异常");
                    }
                }
            }
        });
    }


}
