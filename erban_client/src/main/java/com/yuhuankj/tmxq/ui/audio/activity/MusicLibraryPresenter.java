package com.yuhuankj.tmxq.ui.audio.activity;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.player.bean.LocalMusicInfo;

import java.util.Map;


public class MusicLibraryPresenter extends AbstractMvpPresenter<MusicLibraryView> {

    private final String TAG = MusicLibraryPresenter.class.getSimpleName();


    public MusicLibraryPresenter() {
    }

    public void reportMusicPlay(LocalMusicInfo localMusicInfo){
        long songId = 0;
        try {
            songId = Long.valueOf(localMusicInfo.getSongId());
            //上报
            Map params = CommonParamUtil.getDefaultParam();
            params.put("singId", songId+"");
            params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
            params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid()+"");

            OkHttpManager.getInstance().postRequest(UriProvider.reportMusicPlayUrl(), params,
                    new OkHttpManager.MyCallBack<Json>() {
                        @Override
                        public void onError(Exception e) {
                            e.printStackTrace();
                            if(null != getMvpView()){
                                getMvpView().onMusicPlayReport(false,e.getMessage());
                            }
                        }

                        @Override
                        public void onResponse(Json json) {
                            if(null != getMvpView() && null != json){
                                getMvpView().onMusicPlayReport(json.num("code") == 200,json.str("message"));
                            }
                        }
                    });
        }catch (NumberFormatException nfe){
            nfe.printStackTrace();
        }catch (IllegalStateException ise){
            ise.printStackTrace();
        }
    }

}
