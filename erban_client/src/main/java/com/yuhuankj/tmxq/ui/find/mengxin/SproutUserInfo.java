package com.yuhuankj.tmxq.ui.find.mengxin;

import com.tongdaxing.xchat_core.user.bean.UserInfo;

public class SproutUserInfo extends UserInfo {

    public long getStar() {
        return star;
    }

    public void setStar(long star) {
        this.star = star;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public SproutUserInRoomInfo getUserInRoom() {
        return userInRoom;
    }

    public void setUserInRoom(SproutUserInRoomInfo userInRoom) {
        this.userInRoom = userInRoom;
    }

    /**
     * "code": 200,
     * "data": [{
     * "uid": 90000986,
     * "erbanNo": 9132013,
     * "phone": "9132013",
     * "birth": 867686400000,
     * "star": null,
     * "nick": "周紫卉",
     * "email": null,
     * "signture": null,
     * "userVoice": null,
     * "followNum": 0,
     * "fansNum": 0,
     * "fortune": null,
     * "gender": 2,
     * "avatar": "https://img.pinjin88.com/FnFnU4SseiN0MK8wmiNnDSxKSTtw?imageslim",
     * "age": 21,
     * "experLevel": 0,
     * "charmLevel": 0,
     * "city": "中国",
     * "userInRoom": null
     * }],
     * "message": "success"
     */

    private long star;
    private String email;
    private int age;
    private SproutUserInRoomInfo userInRoom;

    @Override
    public String toString() {
        return "SproutUserInfo{" +
                "star=" + star +
                ", email='" + email + '\'' +
                ", age=" + age +
                ", userInRoom=" + userInRoom +
                '}';
    }
}
