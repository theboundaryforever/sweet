package com.yuhuankj.tmxq.ui.room.fragment;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.netease.nim.uikit.common.ui.recyclerview.decoration.SpacingDecoration;
import com.netease.nim.uikit.common.util.sys.NetworkUtil;
import com.tencent.bugly.crashreport.BuglyLog;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.home.HomeRoom;
import com.tongdaxing.xchat_core.home.NotHotData;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_core.result.NotHotResult;
import com.tongdaxing.xchat_core.room.IRoomCoreClient;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.fragment.BaseLazyFragment;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.room.adapter.NotHotProAdapter;
import com.yuhuankj.tmxq.ui.room.adapter.RoomTagTypeAdapter;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.UIUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p> 首页非Hot界面 </p>
 *
 * @author Administrator
 * @date 2017/11/15
 */
public class NotHotFragment extends BaseLazyFragment implements SwipeRefreshLayout.OnRefreshListener,
        BaseQuickAdapter.RequestLoadMoreListener, BaseQuickAdapter.OnItemClickListener {
    private static final String TAG = NotHotFragment.class.getSimpleName();
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private NotHotProAdapter mNotHotAdapter;
    private List<HomeRoom> mHomeRoomList;
    private int mCurrentPage = Constants.PAGE_START;
    private TabInfo mHomeTabInfo;
    private LinearLayoutManager layoutManager;
    private boolean showTopMenu = false;
    //private SkeletonScreen skeletonScreen;//加载状态view
    private RecyclerView rvRoomType;
    private RelativeLayout rlMyRoom;
    private LinearLayout llNoData;
    private RoomTagTypeAdapter tagTypeAdapter;
    private int tagId = -1;

    public void setShowTopMenu(boolean showTopMenu) {
        this.showTopMenu = showTopMenu;
    }

    public static NotHotFragment newInstance(TabInfo homeTabInfo) {
        NotHotFragment notHotFragment = new NotHotFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.HOME_TAB_INFO, homeTabInfo);
        notHotFragment.setArguments(bundle);
        return notHotFragment;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mHomeRoomList != null) {
            outState.putParcelableArrayList(Constants.KEY_HOME_NO_HOT_LIST, (ArrayList<? extends Parcelable>) mHomeRoomList);
        }
    }

    @Override
    protected void restoreState(@Nullable Bundle savedInstanceState) {
        super.restoreState(savedInstanceState);
        if (savedInstanceState != null) {
            mHomeRoomList = savedInstanceState.getParcelableArrayList(Constants.KEY_HOME_NO_HOT_LIST);
        }
    }

    @Override
    public void onFindViews() {
        mRecyclerView = mView.findViewById(R.id.recycler_view);
        mSwipeRefreshLayout = mView.findViewById(R.id.swipe_refresh);
        llNoData = mView.findViewById(R.id.llNoData);
    }

    @Override
    public void onSetListener() {
        layoutManager = new RecyclerViewNoBugLinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        int space = mContext.getResources().getDimensionPixelOffset(R.dimen.base_margin_left);
        mRecyclerView.addItemDecoration(new SpacingDecoration(space, space, true));
        mSwipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    protected void onInitArguments(Bundle bundle) {
        super.onInitArguments(bundle);
        if (bundle != null) {
            mHomeTabInfo = bundle.getParcelable(Constants.HOME_TAB_INFO);
            if (mHomeTabInfo != null) {
                tagId = mHomeTabInfo.getId();
            }
        }
    }

    @Override
    public void initiate() {
        mNotHotAdapter = new NotHotProAdapter(new ArrayList<>());
        mRecyclerView.setAdapter(mNotHotAdapter);

        View view = new View(mContext);
        view.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, UIUtil.dip2px(mContext, 5)));
        mNotHotAdapter.addFooterView(view);

        mNotHotAdapter.setOnLoadMoreListener(this, mRecyclerView);
        mNotHotAdapter.setOnItemClickListener(this);

        initHeader();
    }

    private void initHeader() {
        View vHeader = LayoutInflater.from(getActivity()).inflate(R.layout.header_not_hot_new, null);
        mNotHotAdapter.addHeaderView(vHeader);

        rvRoomType = vHeader.findViewById(R.id.rvRoomType);
        LinearLayoutManager layoutManager = new RecyclerViewNoBugLinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvRoomType.setLayoutManager(layoutManager);
        tagTypeAdapter = new RoomTagTypeAdapter(new ArrayList<>());
        tagTypeAdapter.bindToRecyclerView(rvRoomType);
        tagTypeAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                try {
                    TabInfo tabInfo = tagTypeAdapter.getData().get(position);
                    setmCurrentPage(Constants.PAGE_START);
                    loadData(tabInfo.getId());
                    tagTypeAdapter.setCurPosition(position);
                } catch (Exception e) {
                    e.printStackTrace();
                    SingleToastUtil.showToast("获取数据失败");
                }
            }
        });

        rlMyRoom = vHeader.findViewById(R.id.rlMyRoom);

//        skeletonScreen = Skeleton.bind(mRecyclerView)
//                .adapter(mNotHotAdapter)
//                .load(R.layout.item_game_home_live_room_loading)
//                .show();
    }

    @Override
    protected void onLazyLoadData() {
        LogUtils.d(TAG, "onLazyLoadData-tabName:" + mHomeTabInfo.getName());
        if (!ListUtils.isListEmpty(mHomeRoomList)) {
            mNotHotAdapter.setNewData(mHomeRoomList);
            if (mHomeRoomList.size() < Constants.PAGE_SIZE) {
                mNotHotAdapter.setEnableLoadMore(false);
            }
        } else {
            mSwipeRefreshLayout.setRefreshing(true);
            if (mHomeTabInfo != null) {
                setmCurrentPage(Constants.PAGE_START);
                loadData(mHomeTabInfo.getId());
            }
        }
    }


    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_not_hot;
    }

    private void loadData(int tagId) {
        BuglyLog.d(TAG, "setBannerData-tagId:" + tagId);
        this.tagId = tagId;
        Map<String, String> param = OkHttpManager.getDefaultParam();
        int currentPage = getmCurrentPage();
        param.put("tagId", String.valueOf(tagId));
        param.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        param.put("pageNum", String.valueOf(currentPage));
        param.put("pageSize", String.valueOf(Constants.PAGE_SIZE));
        OkHttpManager.getInstance().getRequest(UriProvider.getNotHotPageData(), param, new OkHttpManager.MyCallBack<NotHotResult>() {
            @Override
            public void onError(Exception e) {
//                    try {
//                        skeletonScreen.hide();
//                    } catch (Exception e1) {
//                        e1.printStackTrace();
//                    }
                e.printStackTrace();
                onGetHomeDataFail("网络异常", tagId, currentPage);
            }

            @Override
            public void onResponse(NotHotResult response) {
//                    try {
//                        skeletonScreen.hide();
//                    } catch (Exception e1) {
//                        e1.printStackTrace();
//                    }
                if (response.isSuccess()) {
                    onGetHomeData(response.getData(), tagId, currentPage);
                } else {
                    onGetHomeDataFail(response.getMessage(), tagId, currentPage);
                }
            }
        });
    }

    public void onGetHomeData(NotHotData notHotData, int tabType, int page) {
        BuglyLog.d(TAG, "setRoomList-tabType:" + tabType + " page:" + page);
        setmCurrentPage(page);
        //设置顶部分类标签
        if (notHotData.tagList != null && notHotData.tagList.size() > 0) {
            tagTypeAdapter.setNewData(notHotData.tagList);
            rlMyRoom.setVisibility(View.VISIBLE);
            rlMyRoom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //跳转到自己的房间
                    CoreManager.notifyClients(IRoomCoreClient.class,
                            IRoomCoreClient.METHOD_ON_CREATE_ROOM_CLICKED,
                            CoreManager.getCore(IAuthCore.class).getCurrentUid());
                    //首页-交友-我的房间
                    StatisticManager.get().onEvent(getActivity(),
                            StatisticModel.EVENT_ID_MAIN_TAB_MYHOME,
                            StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
                }
            });
        }
        List<HomeRoom> homeRoomList = notHotData.tagRoomList;
        setRoomList(homeRoomList);
    }

    private void setRoomList(List<HomeRoom> homeRoomList) {
        LogUtils.d(TAG, "setRoomList-tabName:" + mHomeTabInfo.getName() + "---->数据加载回来了一次...." + getmCurrentPage());
        if (null != homeRoomList && homeRoomList.size() > 0) {
            llNoData.setVisibility(View.GONE);
            if (getmCurrentPage() == Constants.PAGE_START) {
                mSwipeRefreshLayout.setRefreshing(false);
                hideStatus();
                if (!ListUtils.isListEmpty(mHomeRoomList)) {
                    mHomeRoomList.clear();
                }
                mHomeRoomList = homeRoomList;
                mNotHotAdapter.setNewData(mHomeRoomList);
                //下拉刷新后返回的数据，不够10个的话，禁用上拉加载更多的功能
                mNotHotAdapter.setEnableLoadMore(homeRoomList.size() >= Constants.PAGE_SIZE);
            } else {
                //mHomeRoomList默认代表第一页的数据，在上拉的时候，如果第一页的数据就不够10个，上拉加载更多操作立马返回
                mNotHotAdapter.addData(homeRoomList);
                mNotHotAdapter.loadMoreComplete();
            }
            mCurrentPage++;
        } else {
            if (getmCurrentPage() == Constants.PAGE_START) {
                if (!ListUtils.isListEmpty(mHomeRoomList)) {
                    mHomeRoomList.clear();
                }
                mHomeRoomList = new ArrayList<>();
                mNotHotAdapter.setNewData(mHomeRoomList);
                mSwipeRefreshLayout.setRefreshing(false);

                if (tagTypeAdapter == null || tagTypeAdapter.getData().size() == 0) {
                    llNoData.setVisibility(View.GONE);
                    showNoData();
                } else {
                    llNoData.setVisibility(View.VISIBLE);
                }
            } else {
                mNotHotAdapter.loadMoreEnd(true);
            }
        }
        mRecyclerView.smoothScrollBy(0, 1);
        mRecyclerView.smoothScrollBy(0, -1);
    }

    private void setmCurrentPage(int page) {
        mCurrentPage = page;
    }


    public void onGetHomeDataFail(String error, int tabType, int page) {
        LogUtils.d(TAG, "onGetHomeDataFail-error:" + error + " tabType:" + tabType + " page:" + page);
        setmCurrentPage(page);
        if (mHomeTabInfo != null) {
            if (getmCurrentPage() == Constants.PAGE_START) {
                mSwipeRefreshLayout.setRefreshing(false);
                showNetworkErr();
            } else {
                mNotHotAdapter.loadMoreFail();
                toast(error);
            }
        }
    }

    private int getmCurrentPage() {
        return mCurrentPage;
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo userInfo) {
        LogUtils.d(TAG, "onCurrentUserInfoUpdate");
        setmCurrentPage(Constants.PAGE_START);
//        loadData();
    }

    @Override
    public void onRefresh() {
        BuglyLog.d(TAG, "onRefresh");
        mNotHotAdapter.setEnableLoadMore(true);
        setmCurrentPage(Constants.PAGE_START);
        setmCurrentPage(Constants.PAGE_START);
        loadData(tagId);
    }

    @Override
    public void onLoadMoreRequested() {
        LogUtils.d(TAG, "onLoadMoreRequested-currPage:" + getmCurrentPage());
        // 解决数据重复的问题
        if (getmCurrentPage() == Constants.PAGE_START) {
            return;
        }
        if (NetworkUtil.isNetAvailable(mContext)) {
            if (mHomeRoomList != null && mHomeRoomList.size() >= Constants.PAGE_SIZE) {
                BuglyLog.d(TAG, "onLoadMoreRequested-加载更多数据");
                loadData(tagId);
            } else {
                mNotHotAdapter.setEnableLoadMore(false);
            }
        } else {
            mNotHotAdapter.loadMoreEnd(true);
        }
    }

    @Override
    public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
        if (ListUtils.isListEmpty(mHomeRoomList)) {
            return;
        }
        final HomeRoom homeRoom = mHomeRoomList.get(i);
        if (homeRoom == null) {
            return;
        }

        RoomServiceScheduler.getInstance().enterRoom(getContext(), homeRoom.getUid(), homeRoom.getType());
    }


    @Override
    public void onReloadData() {
        super.onReloadData();
        BuglyLog.d(TAG, "onReloadData");
        setmCurrentPage(Constants.PAGE_START);
        showLoading();
        loadData(tagId);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}