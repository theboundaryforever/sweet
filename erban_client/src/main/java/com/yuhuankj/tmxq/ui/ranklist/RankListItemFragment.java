package com.yuhuankj.tmxq.ui.ranklist;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tencent.bugly.crashreport.BuglyLog;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.fragment.BaseFragment;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author liaoxy
 * @Description:排行榜
 * @date 2019/3/5 17:13
 */
public class RankListItemFragment extends BaseFragment {
    private static final String TAG = RankListItemFragment.class.getSimpleName();
    private RecyclerView rvUser;
    private RankListAdapter rankListAdapter;
    private View vHeader;
    private int type = 2;//默认财富榜
    private int dateType = 1;
    private List<RankListEnitity> allRanks;
    private boolean isFirst = true;//第一次滚动到默认位置

    public static RankListItemFragment newInstance(int type, int dateType) {
        RankListItemFragment rankListFragment = new RankListItemFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        bundle.putInt("dateType", dateType);
        rankListFragment.setArguments(bundle);
        return rankListFragment;
    }

    @Override
    protected void onInitArguments(Bundle bundle) {
        super.onInitArguments(bundle);
        if (bundle != null) {
            type = bundle.getInt("type", 2);
            dateType = bundle.getInt("dateType", 1);
        }
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.layout_ranklist;
    }

    @Override
    public void onFindViews() {
        rvUser = mView.findViewById(R.id.rvUser);
    }

    @Override
    public void onSetListener() {

    }

    @Override
    public void initiate() {
        LinearLayoutManager layoutManager = new RecyclerViewNoBugLinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvUser.setLayoutManager(layoutManager);

        rankListAdapter = new RankListAdapter(getContext(),new ArrayList<>(), this);
        rankListAdapter.setType(type);
        rvUser.setAdapter(rankListAdapter);

        if (type == 1) {
            //魅力榜
            vHeader = LayoutInflater.from(getActivity()).inflate(R.layout.layout_ranklist_header, null);
        } else {
            //财富榜
            vHeader = LayoutInflater.from(getActivity()).inflate(R.layout.layout_ranklist_header_fortune, null);
        }
        rankListAdapter.addHeaderView(vHeader);

//        ViewGroup.LayoutParams layoutParams = vHeader.getLayoutParams();
//        layoutParams.height = (int) (DisplayUtils.getScreenHeight(getActivity()) * 0.48f);
//        vHeader.requestLayout();

        rankListAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                try {
                    UserInfoActivity.start(getContext(), rankListAdapter.getData().get(position).getUid());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        RankListActivity rankListActivity = (RankListActivity) getActivity();
        RankListFragment rankListFragment = (RankListFragment) getParentFragment();
        rvUser.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                try {
                    scrollFmtY = rankListFragment.setTabOffsetY(dy, true);
                    scrollAcyY = rankListActivity.setTabOffsetY(dy, true);
                    LogUtils.e("scrollY == " + dy);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (!isReqedData) {
            getData(dateType);
        }
    }

    private boolean isReqedData = false;//是否请求过数据了
    private int scrollAcyY = 0;
    private int scrollFmtY = 0;

    public void setOffsetY() {
        try {
            if (isFirst) {
                scrollAcyY = 0;
                scrollFmtY = 0;
                isFirst = false;
            }
            RankListActivity rankListActivity = (RankListActivity) getActivity();
            RankListFragment rankListFragment = (RankListFragment) getParentFragment();
            rankListFragment.setTabOffsetY(scrollFmtY, false);
            rankListActivity.setTabOffsetY(scrollAcyY, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (rankListAdapter == null) {
                isReqedData = false;
            } else if (allRanks != null && allRanks.size() == 0) {
                getData(dateType);
            }
        }
    }

    //获取列表数据
    private void getData(int datetype) {
        BuglyLog.d(TAG, "getData-datetype:" + datetype);
        allRanks = null;
        //DialogManager dialogManager = new DialogManager(getActivity());
        //dialogManager.showProgressDialog(getActivity(), "请稍后...");
        new RankListModel().getRankList(type, datetype, new OkHttpManager.MyCallBack<ServiceResult<RankList>>() {
            @Override
            public void onError(Exception e) {
                //dialogManager.dismissDialog();
                LogUtils.e(e.getMessage());
                SingleToastUtil.showToast("获取排行榜数据失败");
                rankListAdapter.setNewData(new ArrayList<>());
                setWinerList(new ArrayList<>());
            }

            @Override
            public void onResponse(ServiceResult<RankList> response) {
                // dialogManager.dismissDialog();
                if (response == null) {
                    rankListAdapter.setNewData(new ArrayList<>());
                    setWinerList(new ArrayList<>());
                    SingleToastUtil.showToast("获取排行榜数据失败");
                    return;
                }
                if (response.isSuccess()) {
                    if (response.getData() != null) {
                        allRanks = response.getData().getRankVoList();
                        if (allRanks == null || allRanks.size() == 0) {
                            BuglyLog.d(TAG, "setNewData0");
                            rankListAdapter.setNewData(new ArrayList<>());
                            setWinerList(new ArrayList<>());
                            return;
                        }
                        if (allRanks.size() > 3) {
                            List<RankListEnitity> winerList = allRanks.subList(0, 3);
                            setWinerList(winerList);
                            List<RankListEnitity> otherRanks = allRanks.subList(3, allRanks.size());
                            BuglyLog.d(TAG, "setNewData1");
                            rankListAdapter.setNewData(otherRanks);
                        } else {
                            BuglyLog.d(TAG, "setNewData2");
                            rankListAdapter.setNewData(new ArrayList<>());
                            setWinerList(allRanks);
                        }
                    } else {
                        rankListAdapter.setNewData(new ArrayList<>());
                        setWinerList(new ArrayList<>());
                    }
                } else {
                    SingleToastUtil.showToast(response.getErrorMessage());
                    BuglyLog.d(TAG, "setNewData3");
                    rankListAdapter.setNewData(new ArrayList<>());
                    setWinerList(new ArrayList<>());
                }
            }
        });
    }

    //获取与上一个差值
    public String getLastOffset(int position) {
        try {
            RankListEnitity item = allRanks.get(position);
            RankListEnitity lastItem = allRanks.get(position - 1);
            long offset = lastItem.getTotalNum() - item.getTotalNum();
            if (offset > 10000) {
                long w = offset / 10000;
                double y = offset % 10000;
                double x = y / 10000d;
                String result = String.format("%.1f", w + x);
                return result + "W";
            }
            return String.valueOf(offset);
        } catch (Exception e) {
            // e.printStackTrace();
            return "";
        }
    }

    private void setWinerList(List<RankListEnitity> list) {
        LinearLayout llFirst = vHeader.findViewById(R.id.llFirst);
        LinearLayout llSecond = vHeader.findViewById(R.id.llSecond);
        LinearLayout llThird = vHeader.findViewById(R.id.llThird);
        llFirst.setVisibility(View.INVISIBLE);
        llSecond.setVisibility(View.INVISIBLE);
        llThird.setVisibility(View.INVISIBLE);
        if (list == null || list.size() == 0) {
            SingleToastUtil.showToast("暂无数据");
            return;
        }
        RankListEnitity first = list.get(0);
        llFirst.setVisibility(View.VISIBLE);
        ImageView imvFirst = vHeader.findViewById(R.id.imvFirst);
        ImageView imvLevelFirst = vHeader.findViewById(R.id.imvLevelFirst);
        TextView tvUserNameFirst = vHeader.findViewById(R.id.tvUserNameFirst);
        TextView tvLastOffsetFirst = vHeader.findViewById(R.id.tvLastOffsetFirst);
        ImageView imvNobleIconFirst = vHeader.findViewById(R.id.imvNobleIconFirst);

        if (!TextUtils.isEmpty(first.getVipMedal())) {
            imvNobleIconFirst.setVisibility(View.VISIBLE);
            ImageLoadUtils.loadImage(mContext, first.getVipMedal(), imvNobleIconFirst);
        } else {
            imvNobleIconFirst.setVisibility(View.GONE);
        }
        ImageLoadUtils.loadImage(mContext, first.getAvatar(), imvFirst);
        tvUserNameFirst.setText(first.getNick());
        try {
            //imvLevelFirst.setImageResource(getLevelIconId(first));
            ImageLoadUtils.loadImage(mContext, getLevelIconUrl(first), imvLevelFirst);
        } catch (Exception e) {
            e.printStackTrace();
        }

        imvFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    UserInfoActivity.start(getContext(), first.getUid());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (list.size() > 1) {
            RankListEnitity second = list.get(1);
            llSecond.setVisibility(View.VISIBLE);
            ImageView imvSecond = vHeader.findViewById(R.id.imvSecond);
            ImageView imvLevelSecond = vHeader.findViewById(R.id.imvLevelSecond);
            TextView tvUserNameSecond = vHeader.findViewById(R.id.tvUserNameSecond);
            TextView tvLastOffsetSecond = vHeader.findViewById(R.id.tvLastOffsetSecond);
            ImageView imvNobleIconSecond = vHeader.findViewById(R.id.imvNobleIconSecond);

            if (!TextUtils.isEmpty(second.getVipMedal())) {
                imvNobleIconSecond.setVisibility(View.VISIBLE);
                ImageLoadUtils.loadImage(mContext, second.getVipMedal(), imvNobleIconSecond);
            } else {
                imvNobleIconSecond.setVisibility(View.GONE);
            }
            ImageLoadUtils.loadImage(mContext, second.getAvatar(), imvSecond);
            tvUserNameSecond.setText(second.getNick());
            tvLastOffsetSecond.setText("距上一名" + getLastOffset(1));
            try {
                //imvLevelSecond.setImageResource(getLevelIconId(second));
                ImageLoadUtils.loadImage(mContext, getLevelIconUrl(second), imvLevelSecond);
            } catch (Exception e) {
                e.printStackTrace();
            }

            imvSecond.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        UserInfoActivity.start(getContext(), second.getUid());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        if (list.size() > 2) {
            RankListEnitity third = list.get(2);
            llThird.setVisibility(View.VISIBLE);
            ImageView imvThird = vHeader.findViewById(R.id.imvThird);
            ImageView imvLevelThird = vHeader.findViewById(R.id.imvLevelThird);
            TextView tvUserNameThird = vHeader.findViewById(R.id.tvUserNameThird);
            TextView tvLastOffsetThird = vHeader.findViewById(R.id.tvLastOffsetThird);
            ImageView imvNobleIconThird = vHeader.findViewById(R.id.imvNobleIconThird);

            if (!TextUtils.isEmpty(third.getVipMedal())) {
                imvNobleIconThird.setVisibility(View.VISIBLE);
                ImageLoadUtils.loadImage(mContext, third.getVipMedal(), imvNobleIconThird);
            } else {
                imvNobleIconThird.setVisibility(View.GONE);
            }
            ImageLoadUtils.loadImage(mContext, third.getAvatar(), imvThird);
            tvUserNameThird.setText(third.getNick());
            tvLastOffsetThird.setText("距上一名" + getLastOffset(2));
            try {
                //imvLevelThird.setImageResource(getLevelIconId(third));
                ImageLoadUtils.loadImage(mContext, getLevelIconUrl(third), imvLevelThird);
            } catch (Exception e) {
                e.printStackTrace();
            }

            imvThird.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        UserInfoActivity.start(getContext(), third.getUid());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    //获取等级图标
    private String getLevelIconUrl(RankListEnitity rankList) {
        String drawableStr = "";
        try {
            if (type == 1) {
                //魅力榜
                int level = rankList.getCharmLevel();
                drawableStr = UriProvider.getMLImgUrl(level);
            } else {
                //财富榜
                int level = rankList.getExperLevel();
                drawableStr = UriProvider.getCFImgUrl(level);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return drawableStr;
    }

    public class RankList implements Serializable {
        private List<RankListEnitity> rankVoList = new ArrayList<>();

        public List<RankListEnitity> getRankVoList() {
            return rankVoList;
        }

        public void setRankVoList(List<RankListEnitity> rankVoList) {
            this.rankVoList = rankVoList;
        }
    }
}