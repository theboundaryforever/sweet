package com.yuhuankj.tmxq.ui.home.game;

import com.tongdaxing.xchat_core.room.bean.RoomInfo;

import java.io.Serializable;

public class GameBannerEnitity implements Serializable {

    private int   bannerId;//": 209,
    private String   bannerName;//": "甜蜜星球抖音视频征集令",
    private String   bannerPic;//": "https://img.pinjin88.com/Fp4gtl_MAHu-geG81PkGQeTTgGQc?imageslim",
    private int   isNewUser;//": 0,
    private int   seqNo;//": 1,
    private int   skipType;//": 3,
    private String   skipUri;//": "https://mp.weixin.qq.com/s/ddCvr8ss_9X5gQlt7LsxuA"

    //2019年5月24日 14:46:38 后端还未添加 为了保持流程正常，客户端自有
    private int roomType = RoomInfo.ROOMTYPE_HOME_PARTY;

    public int getRoomType() {
        return roomType;
    }

    public void setRoomType(int roomType) {
        this.roomType = roomType;
    }

    public int getBannerId() {
        return bannerId;
    }

    public void setBannerId(int bannerId) {
        this.bannerId = bannerId;
    }

    public String getBannerName() {
        return bannerName;
    }

    public void setBannerName(String bannerName) {
        this.bannerName = bannerName;
    }

    public String getBannerPic() {
        return bannerPic;
    }

    public void setBannerPic(String bannerPic) {
        this.bannerPic = bannerPic;
    }

    public int getIsNewUser() {
        return isNewUser;
    }

    public void setIsNewUser(int isNewUser) {
        this.isNewUser = isNewUser;
    }

    public int getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    public int getSkipType() {
        return skipType;
    }

    public void setSkipType(int skipType) {
        this.skipType = skipType;
    }

    public String getSkipUri() {
        return skipUri;
    }

    public void setSkipUri(String skipUri) {
        this.skipUri = skipUri;
    }
}
