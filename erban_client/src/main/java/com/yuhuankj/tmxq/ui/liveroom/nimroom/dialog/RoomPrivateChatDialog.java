package com.yuhuankj.tmxq.ui.liveroom.nimroom.dialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.session.SessionCustomization;
import com.netease.nim.uikit.session.audio.P2pMicroLinkHelper;
import com.netease.nim.uikit.session.audio.P2pMicroLinkObservable;
import com.netease.nim.uikit.session.constant.Extras;
import com.netease.nim.uikit.session.fragment.MessageFragment;
import com.netease.nim.uikit.uinfo.UserInfoHelper;
import com.netease.nim.uikit.uinfo.UserInfoObservable;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.friend.FriendService;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.NotchFixUtil;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.BaseDialogFragment;
import com.yuhuankj.tmxq.utils.Utils;

import java.util.List;

/**
 * 房间私聊界面
 */
public class RoomPrivateChatDialog extends BaseDialogFragment implements View.OnClickListener {

    protected String sessionId;
    private MessageFragment messageFragment;
    private SessionCustomization customization;
    private UserInfoObservable.UserInfoObserver uinfoObserver;
    private P2pMicroLinkObservable.P2pMicroLinkObserver p2pMicroLinkObserver;
    private TextView tvToolbarTitle;
    private ImageView ivCloss;

    private LinearLayout msgFragmentContainer;

    private boolean isMyFriend = false;

    private final String TAG = RoomPrivateChatDialog.class.getSimpleName();

    public static RoomPrivateChatDialog newInstance(String sessionId) {
        RoomPrivateChatDialog privateChat = new RoomPrivateChatDialog();
        Bundle bundle = new Bundle();
        bundle.putString(Extras.EXTRA_ACCOUNT, sessionId);
        privateChat.setArguments(bundle);
        return privateChat;
    }

    public RoomPrivateChatDialog() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            sessionId = savedInstanceState.getString(Extras.EXTRA_ACCOUNT, sessionId);
        } else {
            if (getArguments() != null) {
                sessionId = getArguments().getString(Extras.EXTRA_ACCOUNT, "");
            }
        }
//        getActivity().registerReceiver(imageIntentResultReceiver,new IntentFilter(ImageIntentAction));
        LogUtils.d(TAG, "onCreate-sessionId:" + sessionId);
        //避免在私聊界面，避免特定场景(如打开召集令消息进入房间后)，仍旧收到同一用户的消息，首页底部未读红点提示逻辑未能清楚的问题
        if (null != NimUIKit.onP2PSessionOpenListener) {
            NimUIKit.onP2PSessionOpenListener.onP2PSessionOpened(sessionId);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        // setup window and width
        View view = inflater.inflate(R.layout.dialog_room_private_chat, window.findViewById(android.R.id.content), false);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.BOTTOM);
        setCancelable(true);

        if (null != getDialog()) {
            getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && null != event && event.getAction() == KeyEvent.ACTION_DOWN) {
                        LogUtils.d(TAG, "onCreateView-->setOnKeyListener-->onKey-->dismissSelf");
                        dismissSelf();
                        return true;
                    }
                    return false;
                }
            });
        }
        return view;
    }

    private void dismissSelf() {
        if (messageFragment != null && messageFragment.isLinkMacroing()) {
            try {
                LogUtils.d(TAG, "dismissSelf-->cancelAction");
                messageFragment.cancelAction();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            LogUtils.d(TAG, "dismissSelf-->dismiss");
            dismiss();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvToolbarTitle = view.findViewById(R.id.tv_list_data_title);
        ivCloss = view.findViewById(R.id.iv_close_dialog);
        ivCloss.setOnClickListener(this);
        if (NotchFixUtil.check(NotchFixUtil.ROM_VIVO) && android.os.Build.VERSION.SDK_INT >= 28) {
            view.findViewById(R.id.v_navbar).getLayoutParams().height = 0;
        } else {
            view.findViewById(R.id.v_navbar).getLayoutParams().height = Utils.getNavBarHeight(getActivity());
        }

        messageFragment = new MessageFragment();
        Bundle arguments = getArguments();
//        if (NimUIKit.commonP2PSessionCustomization != null) {
//            customization = NimUIKit.commonP2PSessionCustomization;
//            //清空action中Context的引用，以防内存泄露
//            if (customization != null && customization.actions != null) {
//                for (BaseAction action : customization.actions) {
//                    action.setContainer(null);
//                }
//            } else {
//                customization = new SessionCustomization();
//                ArrayList<BaseAction> actions = new ArrayList<>();
//                actions.add(new ImageAction());
//                actions.add(new GiftAction());
//                actions.add(new DataAction());
//                customization.actions = actions;
//                customization.withSticker = true;
//            }
//        } else {
//            customization = new SessionCustomization();
//            ArrayList<BaseAction> actions = new ArrayList<>();
//            actions.add(new ImageAction());
//            actions.add(new GiftAction());
//            actions.add(new DataAction());
//            customization.actions = actions;
//            customization.withSticker = true;
//        }
        //    customization = new SessionCustomization();
        // ArrayList<BaseAction> actions = new ArrayList<>();
        //因为房间内私聊界面发送图片消息，发送后界面没有响应更新，处理逻辑还有些问题，故跟产品讨论暂时隐藏
        //todo:找最新的方案解决房间内图片拍摄流程走不通的问题
//        actions.add(new ImageAction());
        //  actions.add(new GiftAction());
        //  customization.actions = actions;
        //  customization.withSticker = true;
//        arguments.putSerializable(Extras.EXTRA_CUSTOMIZATION, customization);
        arguments.putSerializable(Extras.EXTRA_TYPE, SessionTypeEnum.P2P);
        arguments.putBoolean(MessageFragment.Extras_Show_In_Room, true);
        messageFragment.setArguments(arguments);
        messageFragment.setContainerId(R.id.msg_fragment_container);
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fl_room_msg_container, messageFragment).commitAllowingStateLoss();
        requestBuddyInfo();
        registerObservers(true);
        isMyFriend = NIMClient.getService(FriendService.class).isMyFriend(sessionId);
        LogUtils.d(TAG, "onViewCreated isMyFriend:" + isMyFriend);
        messageFragment.setMyFriend(isMyFriend);
    }

    private void registerObservers(boolean register) {
        if (register) {
            registerUserInfoObserver();
        } else {
            unregisterUserInfoObserver();
        }

    }

    private void registerUserInfoObserver() {
        if (uinfoObserver == null) {
            uinfoObserver = new UserInfoObservable.UserInfoObserver() {
                @Override
                public void onUserInfoChanged(List<String> accounts) {
                    if (accounts.contains(sessionId)) {
                        requestBuddyInfo();
                    }
                }
            };
        }

        UserInfoHelper.registerObserver(uinfoObserver);

        if (p2pMicroLinkObserver == null) {
            p2pMicroLinkObserver = new P2pMicroLinkObservable.P2pMicroLinkObserver() {
                @Override
                public void onDismissAfterEndLinkMicro() {
                    dismiss();
                }
            };
        }

        P2pMicroLinkHelper.registerObserver(p2pMicroLinkObserver);
    }

    private void unregisterUserInfoObserver() {
        if (uinfoObserver != null) {
            UserInfoHelper.unregisterObserver(uinfoObserver);
        }
        if (p2pMicroLinkObserver != null) {
            P2pMicroLinkHelper.unregisterObserver(p2pMicroLinkObserver);
        }
    }

    private void requestBuddyInfo() {
        // 显示自己的textview并且居中
        String userTitleName = UserInfoHelper.getUserTitleName(sessionId, SessionTypeEnum.P2P);
        if (tvToolbarTitle != null) {
            tvToolbarTitle.setText(userTitleName);
        }
    }


    public void dealImageIntentResult(int requestCode, int resultCode, Intent data) {
        LogUtils.d(TAG, "dealImageIntentResult-requestCode:" + requestCode + " resultCode:" + resultCode + " data:" + data);
        if (null != data) {
            if (data.hasExtra("data")) {
                LogUtils.d(TAG, "dealImageIntentResult-data.data:" + data);
            }
        }
        if (messageFragment != null) {
            messageFragment.onActivityResult(requestCode, resultCode, data);
        }

        if (customization != null) {
            customization.onActivityResult(getActivity(), requestCode, resultCode, data);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogUtils.d(TAG, "onActivityResult requestCode:" + requestCode + " resultCode:" + resultCode);
        dealImageIntentResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        registerObservers(false);
        if (null != getDialog()) {
            getDialog().setOnKeyListener(null);
        }
    }

    @Override
    public void dismiss() {
        if (null != getDialog()) {
            getDialog().setOnKeyListener(null);
        }
        super.dismiss();
    }

    @Override
    public void onClick(View v) {
        dismissSelf();
    }
}
