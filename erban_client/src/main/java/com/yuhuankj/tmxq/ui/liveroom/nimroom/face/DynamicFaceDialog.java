package com.yuhuankj.tmxq.ui.liveroom.nimroom.face;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.erban.libcommon.utils.toast.ToastCompat;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.face.FaceInfo;
import com.tongdaxing.xchat_core.room.face.IFaceCore;
import com.tongdaxing.xchat_core.room.face.IFaceCoreClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.me.noble.NobleBusinessManager;
import com.yuhuankj.tmxq.utils.Utils;

import java.util.ArrayList;
import java.util.List;


/**
 * @author xiaoyu
 * @date 2017/12/8
 */

public class DynamicFaceDialog extends BottomSheetDialog
        implements DynamicFaceAdapter.OnFaceItemClickListener, View.OnClickListener {
    private static final String TAG = "DynamicFaceDialog";
    private Context context;

    private TextView tvNormalFace;
    private TextView tvNobleFace;
    private int type = 1;//表情类型
    private LinearLayout llIndicator;

    public DynamicFaceDialog(Context context) {
        super(context, R.style.ErbanBottomSheetDialog);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setCanceledOnTouchOutside(true);
        setCancelable(true);
        setContentView(R.layout.dialog_bottom_face);
        CoreManager.addClient(this);
        if (getWindow() != null) {
            WindowManager.LayoutParams params = getWindow().getAttributes();
            params.width = WindowManager.LayoutParams.MATCH_PARENT;
            params.height = WindowManager.LayoutParams.MATCH_PARENT;
            getWindow().setAttributes(params);
        }
        tvNormalFace = findViewById(R.id.tvNormalFace);
        tvNobleFace = findViewById(R.id.tvNobleFace);
        tvNormalFace.setOnClickListener(this);
        tvNobleFace.setOnClickListener(this);

        try {
            long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
            type = (int) SpUtils.get(getContext(), uid + "_open_face_type", type);
        } catch (Exception e) {
            e.printStackTrace();
        }
        init(findViewById(R.id.rl_dynamic_face_dialog_root), type);
    }

    private void init(View root, int type) {
        this.type = type;
        try {
            long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
            SpUtils.put(getContext(), uid + "_open_face_type", type);
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<FaceInfo> faceInfos = CoreManager.getCore(IFaceCore.class).getFaceInfos(type);
        if (faceInfos == null || faceInfos.size() == 0) {
            LogUtils.d(TAG, "init-表情准备中");
            ToastCompat.makeText(context, "表情准备中...", ToastCompat.LENGTH_SHORT).show();
            return;
        }

        ViewPager vpMenu = root.findViewById(R.id.viewpager);
        List<View> pagerView = new ArrayList<>();
        List<List<FaceInfo>> lists = resolveData(faceInfos);
        int size = lists.size();
        for (int i = 0; i < size; i++) {
            View view = LayoutInflater.from(context).inflate(R.layout.layout_face_grid_view, vpMenu, false);
            GridView gridView = view.findViewById(R.id.gridView);
            DynamicFaceAdapter adapter = new DynamicFaceAdapter(context, lists.get(i));
            adapter.setOnFaceItemClickListener(this);
            gridView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            pagerView.add(view);
        }

        MyViewPagerAdapter pagerAdapter = new MyViewPagerAdapter(pagerView);
        vpMenu.setAdapter(pagerAdapter);
        pagerAdapter.notifyDataSetChanged();

        // 准备indicator
        llIndicator = root.findViewById(R.id.ll_dynamic_face_dialog_indicator);
        llIndicator.removeAllViews();
        int indicatorDotLength = Utils.dip2px(context, 6);
        int indicatorDotSpace = Utils.dip2px(context, 5);
        for (int i = 0; i < size; i++) {
            View view = new View(context);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(indicatorDotLength, indicatorDotLength);
            if (i != 0) {
                params.leftMargin = indicatorDotSpace;
            }
            view.setLayoutParams(params);
            view.setBackgroundResource(R.drawable.bg_dynamic_face_dialog_selector);
            llIndicator.addView(view);
        }
        try {
            if (type == 2) {
                selectNobleFace();
            } else {
                selectNormalFace();
            }
            vpMenu.setCurrentItem(type - 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        setIndicator(0);
        vpMenu.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                setIndicator(position);
            }
        });
    }

    private void setIndicator(int position) {
        if (llIndicator == null) {
            return;
        }
        for (int i = 0; i < llIndicator.getChildCount(); i++) {
            if (i == position) {
                llIndicator.getChildAt(i).setSelected(true);
            } else {
                llIndicator.getChildAt(i).setSelected(false);
            }
        }

    }

    @Override
    public void onClick(View v) {
        if (v == tvNormalFace) {
            selectNormalFace();
            init(findViewById(R.id.rl_dynamic_face_dialog_root), 1);
        } else if (v == tvNobleFace) {
            selectNobleFace();
            init(findViewById(R.id.rl_dynamic_face_dialog_root), 2);
        }
    }

    private void selectNormalFace() {
        tvNormalFace.setTextColor(Color.parseColor("#10CFA6"));
        tvNobleFace.setTextColor(Color.parseColor("#777878"));
    }

    private void selectNobleFace() {
        tvNobleFace.setTextColor(Color.parseColor("#10CFA6"));
        tvNormalFace.setTextColor(Color.parseColor("#777878"));
    }

    @CoreEvent(coreClientClass = IFaceCoreClient.class)
    public void onUnzipSuccess() {
        LogUtils.d(TAG, "onUnzipSuccess");
        init(findViewById(R.id.rl_dynamic_face_dialog_root), type);
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        CoreManager.removeClient(this);
    }

    private static final int IMAGE_COUNT_PER_PAGE = 15;
    private static final int TYPE_NORMAL_FACE = 0;
    private static final int TYPE_LUCKY_FACE = 1;

    private List<List<FaceInfo>> resolveData(List<FaceInfo> faceInfos) {
        List<Integer> hideFace = AvRoomDataManager.get().getHideFaceList();
        int size = faceInfos.size();
        // 最少要有两页
        List<List<FaceInfo>> tmp = new ArrayList<>();
        tmp.add(new ArrayList<FaceInfo>());
        tmp.add(new ArrayList<FaceInfo>());
        List<List<FaceInfo>> results = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            FaceInfo faceInfo = faceInfos.get(i);
            //表情拦截
            if (hideFace != null && hideFace.size() > 0 && hideFace.contains(faceInfo.getId())) {
                continue;
            }
            int resultCount = faceInfo.getResultCount();
            if (resultCount > 0) {
                // 运气表情
                tmp.get(TYPE_LUCKY_FACE).add(faceInfos.get(i));
            } else {
                // 普通表情
                tmp.get(TYPE_NORMAL_FACE).add(faceInfos.get(i));
            }
        }
        // 分页
        results.add(new ArrayList<FaceInfo>());
        for (int i = 0; i < size; i++) {
            // 需要多页来显示
            if (results.get(results.size() - 1).size() == IMAGE_COUNT_PER_PAGE) {
                results.add(new ArrayList<FaceInfo>());
            }
            if (tmp.get(TYPE_NORMAL_FACE).size() > 0) {
                results.get(results.size() - 1).add(tmp.get(TYPE_NORMAL_FACE).remove(0));
            } else if (tmp.get(TYPE_LUCKY_FACE).size() > 0) {
                results.get(results.size() - 1).add(tmp.get(TYPE_LUCKY_FACE).remove(0));
            }
        }
        return results;
    }

    @Override
    public void onFaceItemClick(FaceInfo faceInfo) {
        if (faceInfo != null && !CoreManager.getCore(IFaceCore.class).isShowingFace()) {
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
//            if (context != null && userInfo != null && faceInfo.getNobleId() > userInfo.getVipId()) {
//                ToastCompat.makeText(context, "男爵及以上贵族才可以使用贵族表情", ToastCompat.LENGTH_SHORT).show();
//                return;
//            }
            if (context != null && userInfo != null && faceInfo.isNobleFace() && !NobleBusinessManager.canSendNobleEmoji(userInfo.getVipId())) {
                ToastCompat.makeText(context, "男爵及以上贵族才可以使用贵族表情", ToastCompat.LENGTH_SHORT).show();
                return;
            }
            CoreManager.getCore(IFaceCore.class).sendFace(faceInfo);
            dismiss();
        }
    }

    public class MyViewPagerAdapter extends PagerAdapter {

        private List<View> mLists;

        MyViewPagerAdapter(List<View> array) {
            this.mLists = array;
        }

        @Override
        public int getCount() {
            return mLists.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            container.addView(mLists.get(position));
            return mLists.get(position);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

    }

}
