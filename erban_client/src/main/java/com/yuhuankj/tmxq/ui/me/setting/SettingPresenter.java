package com.yuhuankj.tmxq.ui.me.setting;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.match.bean.MicroMatch;

import java.util.List;
import java.util.Map;

public class SettingPresenter extends AbstractMvpPresenter<SettingView> {

    private final String TAG = SettingPresenter.class.getSimpleName();

    public void changeNobleEnterRoomVisiableStatus(boolean isVisiable) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        params.put("uid", uid + "");
        params.put("open", Boolean.valueOf(isVisiable).toString());
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket() + "");
        OkHttpManager.getInstance().postRequest(UriProvider.getNobleRoomVisiableUrl(), params,
                new OkHttpManager.MyCallBack<ServiceResult<List<MicroMatch>>>() {
                    @Override
                    public void onError(Exception e) {
                        LogUtils.d(TAG, "changeNobleEnterRoomVisiableStatus-onError");
                        e.printStackTrace();
                        if (null != getMvpView()) {
                            getMvpView().onChangeNobleEnterRoomVisiableStatus(false,
                                    BasicConfig.INSTANCE.getAppContext().getResources().getString(
                                            R.string.network_error_retry));
                        }
                    }

                    @Override
                    public void onResponse(ServiceResult<List<MicroMatch>> response) {
                        LogUtils.d(TAG, "changeNobleEnterRoomVisiableStatus-onResponse");
                        if (null != getMvpView()) {
                            getMvpView().onChangeNobleEnterRoomVisiableStatus(200 == response.getCode(), response.getMessage());
                        }
                    }
                });
    }

    public void exitRoom() {
        BaseRoomServiceScheduler.exitRoom(null);
    }

}
