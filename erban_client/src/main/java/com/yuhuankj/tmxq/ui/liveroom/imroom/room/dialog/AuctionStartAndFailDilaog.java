package com.yuhuankj.tmxq.ui.liveroom.imroom.room.dialog;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.room.auction.RoomAuctionBean;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.BaseAppDialog;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

/**
 * 文件描述：拍卖开始弹框提示
 *
 * @auther：zwk
 * @data：2019/8/1
 */
public class AuctionStartAndFailDilaog extends BaseAppDialog {
    private boolean isFailed = false;
    private ImageView ivAvatar;
    private ImageView ivGender;
    private TextView tvNick;
    private TextView tvInfo;
    private RoomAuctionBean roomAuctionBean;

    public AuctionStartAndFailDilaog(Context context) {
        super(context);
    }

    public AuctionStartAndFailDilaog(Context context, RoomAuctionBean roomAuctionBean, boolean isFailed) {
        super(context);
        this.roomAuctionBean = roomAuctionBean;
        this.isFailed = isFailed;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_auction_start;
    }

    @Override
    protected void initView() {
        ivAvatar = findViewById(R.id.iv_auction_user_avatar);
        ivGender = findViewById(R.id.iv_auction_user_gender);
        tvNick = findViewById(R.id.tv_auction_user_nick);
        tvInfo = findViewById(R.id.tv_auction_info);
        if (isFailed) {
            findViewById(R.id.tv_auction_start).setVisibility(View.GONE);
            findViewById(R.id.iv_auction_start_back).setVisibility(View.GONE);
            findViewById(R.id.tv_auction_fail).setVisibility(View.VISIBLE);
            findViewById(R.id.tv_auction_fail2).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void initListener() {

    }

    @Override
    public void initViewState() {
        if (isFailed) {
            if (roomAuctionBean == null || roomAuctionBean.getUid() <= 0) {
                dismiss();
            } else {
                ImageLoadUtils.loadCircleImage(getContext(), roomAuctionBean.getAvatar(), ivAvatar, R.drawable.ic_default_avatar);
                ivGender.setImageResource(roomAuctionBean.getGender() == 1 ? R.drawable.icon_man : R.drawable.icon_female);
                tvNick.setText(roomAuctionBean.getNick());
                tvInfo.setText("拍卖：" + roomAuctionBean.getProject() + " | " + roomAuctionBean.getDay() + "天");
            }
        } else {
            if (RoomDataManager.get().getAuction() == null || RoomDataManager.get().getAuction().getUid() <= 0) {
                dismiss();
            } else {
                RoomAuctionBean roomAuction = RoomDataManager.get().getAuction();
                ImageLoadUtils.loadCircleImage(getContext(), roomAuction.getAvatar(), ivAvatar, R.drawable.ic_default_avatar);
                ivGender.setImageResource(roomAuction.getGender() == 1 ? R.drawable.icon_man : R.drawable.icon_female);
                tvNick.setText(roomAuction.getNick());
                tvInfo.setText("拍卖：" + roomAuction.getProject() + " | " + roomAuction.getDay() + "天");
            }
        }
    }
}
