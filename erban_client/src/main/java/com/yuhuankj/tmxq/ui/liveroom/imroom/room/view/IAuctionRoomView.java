package com.yuhuankj.tmxq.ui.liveroom.imroom.room.view;

import com.tongdaxing.xchat_core.room.queue.bean.RoomConsumeInfo;

import java.util.List;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/7/26
 */
public interface IAuctionRoomView extends IRoomDetailView {
    void onGetFortuneRankTop(boolean isSuccess, String message, List<RoomConsumeInfo> roomConsumeInfos);
    void onUpdateAuctionOperateBtn();
    void showAuctionSortDialog();
    void isAdminJoinAuction(boolean join,long uid,boolean isInvite);
}
