package com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.xchat_core.room.auction.ParticipateAuctionBean;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.widget.LevelView;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/7/30
 */
public class AuctionSortListAdapter extends BaseQuickAdapter<ParticipateAuctionBean, BaseViewHolder> {

    public AuctionSortListAdapter() {
        super(R.layout.item_rv_auction_sort_list);
    }

    @Override
    protected void convert(BaseViewHolder helper, ParticipateAuctionBean item) {
        int position = helper.getAdapterPosition() + 1;
        helper.setText(R.id.tv_auction_sort_nick, item.getNick())
                .setText(R.id.iv_auction_sort_num, position > 9 ? position + "" : "0" + position)
                .setImageResource(R.id.iv_auction_sort_gender, item.getGender() == 1 ? R.drawable.icon_man : R.drawable.icon_female);
        TextView tvCard = helper.getView(R.id.tv_auction_sort_priority);
        ImageView ivCard =  helper.getView(R.id.iv_auction_sort_priority);
        if (item.getCardNum() > 0){
            tvCard.setVisibility(View.VISIBLE);
            ivCard.setVisibility(View.VISIBLE);
            tvCard.setText("x"+item.getCardNum());
        }else {
            tvCard.setVisibility(View.GONE);
            ivCard.setVisibility(View.GONE);
            tvCard.setText("");
        }
        ImageLoadUtils.loadCircleImage(mContext, item.getAvatar(), helper.getView(R.id.iv_auction_sort_avatar), R.drawable.ic_default_avatar);
        LevelView levelView = helper.getView(R.id.lv_auction_sort_level);
        levelView.setCharmLevel(item.getCharmLevelPic());
        levelView.setExperLevel(item.getExperLevelPic());
    }
}
