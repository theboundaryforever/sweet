package com.yuhuankj.tmxq.ui.liveroom.imroom.publicscreen.holder;

import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.widget.TextView;

import com.juxiao.library_utils.DisplayUtils;
import com.netease.nim.uikit.common.ui.widget.UrlImageSpan;
import com.tongdaxing.erban.libcommon.im.IMReportRoute;
import com.tongdaxing.erban.libcommon.widget.messageview.MsgHolderLayoutId;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMessage;
import com.tongdaxing.xchat_core.utils.BaseConstant;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.imroom.publicscreen.base.RoomMsgBaseHolder;

/**
 * 文件描述：背景图片透明的文本消息，用于不需要默认聊天气泡(透明看不见的气泡)的情况
 *
 * @auther：zwk
 * @data：2019/5/7
 */
@MsgHolderLayoutId(value = R.layout.item_room_msg_txt_no_bg)
public class RoomMsgTxtNoBgHolder extends RoomMsgBaseHolder<IMRoomMessage> {

    public RoomMsgTxtNoBgHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void onDataTransformView(RoomMsgBaseHolder holder, IMRoomMessage message, int position) {
        TextView tvContent = (TextView) holder.getView(R.id.etv_content);
        if (IMReportRoute.sendMessageReport.equalsIgnoreCase(message.getRoute())) {
            if (message.getAttachment() != null) {
                if (message.getAttachment().getFirst() == CustomAttachment.CUSTOM_MSG_ROOM_ATTENTION) {
                    //提示关注房主消息类型
                    setAttentionRoomOwnerMsg(tvContent);
                } else if (message.getAttachment().getFirst() == CustomAttachment.CUSTOM_MSG_FIRST_ROOM_ATTENTION_TIPS
                        && message.getAttachment().getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_ROOM_ATTENTION_GUIDE) {
                    //提示关注房间消息类型
                    setAttentionRoomMsg(tvContent);
                } else {
                    tvContent.setText("不支持消息类型");
                }
            } else {
                tvContent.setText("不支持消息类型");
            }
        } else {
            tvContent.setText("不支持消息类型");
        }
    }

    @Override
    protected int getItemClickLayoutId() {
        return R.id.etv_content;
    }

    @Override
    protected int getItemUserClickLayoutId() {
        return 0;
    }


    private int attentionWidth = 0;
    private int attentionHeight = 0;
    /**
     * 显示自定义的关注房主消息
     */
    private void setAttentionRoomOwnerMsg(TextView tvContent) {
        SpannableStringBuilder builder = new SpannableStringBuilder("");
        //插入小图标占位符
        builder.append(BaseConstant.GIFT_PLACEHOLDER);
        UrlImageSpan imageSpan = new UrlImageSpan(tvContent.getContext(), RoomDataManager.get().hasLikeRoomOwner() ?
                R.drawable.ic_liveroom_msg_attention_already : R.drawable.ic_liveroom_msg_attention);
        if (attentionWidth <= 0) {
            attentionWidth = DisplayUtils.dip2px(tvContent.getContext(), 181);
        }
        if (attentionHeight <= 0){
            attentionHeight = DisplayUtils.dip2px(tvContent.getContext(), 166);
        }
        imageSpan.setImgWidth(attentionWidth);
        imageSpan.setImgHeight(attentionHeight);
        builder.setSpan(imageSpan, builder.toString().length() - BaseConstant.GIFT_PLACEHOLDER.length(),
                builder.toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvContent.setText(builder);
        tvContent.setPadding(0, 0, 0, 0);
    }

    /**
     * 显示自定义的关注房间消息
     */
    private void setAttentionRoomMsg(TextView tvContent) {
        SpannableStringBuilder builder = new SpannableStringBuilder("");
        //插入小图标占位符
        builder.append(BaseConstant.GIFT_PLACEHOLDER);
        UrlImageSpan imageSpan = new UrlImageSpan(tvContent.getContext(), RoomDataManager.get().hasAttentionRoom() ?
                R.drawable.ic_liveroom_msg_attention_already : R.drawable.ic_liveroom_msg_attention);
        if (attentionWidth <= 0) {
            attentionWidth = DisplayUtils.dip2px(tvContent.getContext(), 181);
        }
        if (attentionHeight <= 0){
            attentionHeight = DisplayUtils.dip2px(tvContent.getContext(), 166);
        }
        imageSpan.setImgWidth(attentionWidth);
        imageSpan.setImgHeight(attentionHeight);
        builder.setSpan(imageSpan, builder.toString().length() - BaseConstant.GIFT_PLACEHOLDER.length(),
                builder.toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvContent.setText(builder);
        tvContent.setPadding(0, 0, 0, 0);
    }
}
