package com.yuhuankj.tmxq.ui.signAward.model;

import java.io.Serializable;

/**
 * 萌新大礼包
 *
 * @author weihaitao
 * @date 2019/5/22
 */
public class SignRecvGiftPkgInfo implements Serializable {

    /**
     * 礼物数量
     */
    private int giftCount;

    /**
     * 礼物ID
     */
    private int giftId;

    /**
     * 奖品名称
     */
    private String giftName;

    /**
     * 奖品图片URL
     */
    private String giftPicUrl;

    /**
     * 礼物类型（1、礼物 2、座驾 3、头饰 4、甜豆）
     */
    private int type;

    public int getGiftCount() {
        return giftCount;
    }

    public void setGiftCount(int giftCount) {
        this.giftCount = giftCount;
    }

    public int getGiftId() {
        return giftId;
    }

    public void setGiftId(int giftId) {
        this.giftId = giftId;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public String getGiftPicUrl() {
        return giftPicUrl;
    }

    public void setGiftPicUrl(String giftPicUrl) {
        this.giftPicUrl = giftPicUrl;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
