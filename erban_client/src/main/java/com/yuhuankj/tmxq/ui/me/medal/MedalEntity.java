package com.yuhuankj.tmxq.ui.me.medal;

import java.io.Serializable;

/**
 * @author liaoxy
 * @Description:勋章实体类
 * @date 2019/4/19 10:57
 */
public class MedalEntity implements Serializable {

    private String conditions;//": "新注册用户自动获得",
    private long createTime;//": 1555664961000,
    private long endTime;//": 1556269761000,
    private String name;//": "萌新",
    private String picture;//": "https://img.pinjin88.com/xunzhang2@3x.png",
    private long startTime;//": 1555664961000,
    private int titleId;//": 1,
    private long uid;//": 92000684
    private int id;//": 92000684

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public int getTitleId() {
        return titleId;
    }

    public void setTitleId(int titleId) {
        this.titleId = titleId;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }
}