package com.yuhuankj.tmxq.ui.widget.address;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.aigestudio.wheelpicker.WheelPicker;

import java.util.ArrayList;
import java.util.List;


/**
 * WheelAreaPicker
 * Created by Administrator on 2016/9/14 0014.
 */
public class WheelCityPicker extends LinearLayout {
    private static final float ITEM_TEXT_SIZE = 18;
    private static final String SELECTED_ITEM_COLOR = "#353535";
    private static final int PROVINCE_INITIAL_INDEX = 0;

    private Context mContext;

    private List<AddressHelper.Province> mProvinceList;
    private List<AddressHelper.City> mCityList;
    private List<String> mProvinceName, mCityName;

    private LayoutParams mLayoutParams;

    private WheelPicker mWPProvince, mWPCity, mWPArea;

    public WheelCityPicker(Context context, AttributeSet attrs) {
        super(context, attrs);

        initLayoutParams();

        initView(context);

        mProvinceList = AddressHelper.getInstance(context).getProvinces();

        obtainProvinceData();

        addListenerToWheelPicker();
    }

    private void initLayoutParams() {
        mLayoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mLayoutParams.setMargins(5, 5, 5, 5);
        mLayoutParams.width = 0;
    }

    private void initView(Context context) {
        setOrientation(HORIZONTAL);

        mContext = context;

        mProvinceName = new ArrayList<>();
        mCityName = new ArrayList<>();

        mWPProvince = new WheelPicker(context);
        mWPCity = new WheelPicker(context);
        mWPArea = new WheelPicker(context);

        initWheelPicker(mWPProvince, 1);
        initWheelPicker(mWPCity, 1.5f);
        initWheelPicker(mWPArea, 1.5f);
    }

    public WheelPicker getmWPCity() {
        return mWPCity;
    }

    public WheelPicker getmWPProvince() {
        return mWPProvince;
    }

    private void initWheelPicker(WheelPicker wheelPicker, float weight) {
        mLayoutParams.weight = weight;
        wheelPicker.setVisibleItemCount(7);
        wheelPicker.setItemTextSize(dip2px(mContext, ITEM_TEXT_SIZE));
        wheelPicker.setSelectedItemTextColor(Color.parseColor(SELECTED_ITEM_COLOR));
        wheelPicker.setCurved(true);
        wheelPicker.setLayoutParams(mLayoutParams);
        addView(wheelPicker);
    }

    private void obtainProvinceData() {
        for (AddressHelper.Province province : mProvinceList) {
            mProvinceName.add(province.getProvinceName());
        }
        mWPProvince.setData(mProvinceName);
        setCityAndAreaData(PROVINCE_INITIAL_INDEX);
    }

    public void setCurCity(String curCity) {
        if (TextUtils.isEmpty(curCity)) {
            return;
        }
        int proIndex = -1;
        int cityIndex = -1;
        for (AddressHelper.Province province : mProvinceList) {
            List<AddressHelper.City> mCityList = province.getCitys();
            for (AddressHelper.City city : mCityList) {
                if (curCity.contains(city.getCityName()) || city.getCityName().contains(curCity)) {
                    proIndex = mProvinceList.indexOf(province);
                    cityIndex = mCityList.indexOf(city);
                    break;
                }
            }
        }
        if (proIndex != -1 && cityIndex != -1) {
            mWPProvince.setSelectedItemPosition(proIndex);
            setCityAndAreaData(proIndex);
            mWPCity.setSelectedItemPosition(cityIndex);
        }
    }

    private void addListenerToWheelPicker() {
        //监听省份的滑轮,根据省份的滑轮滑动的数据来设置市跟地区的滑轮数据
        mWPProvince.setOnItemSelectedListener(new WheelPicker.OnItemSelectedListener() {
            @Override
            public void onItemSelected(WheelPicker picker, Object data, int position) {
                //获得该省所有城市的集合
                mCityList = mProvinceList.get(position).getCitys();
                setCityAndAreaData(position);
            }
        });

        mWPCity.setOnItemSelectedListener(new WheelPicker.OnItemSelectedListener() {
            @Override
            public void onItemSelected(WheelPicker picker, Object data, int position) {
                //获取城市对应的城区的名字
                mWPArea.setData(mCityList.get(position).getCountys());
            }
        });
    }

    private void setCityAndAreaData(int position) {
        //获得该省所有城市的集合
        mCityList = mProvinceList.get(position).getCitys();
        //获取所有city的名字
        //重置先前的城市集合数据
        mCityName.clear();
        for (AddressHelper.City city : mCityList)
            mCityName.add(city.getCityName());
        mWPCity.setData(mCityName);
        mWPCity.setSelectedItemPosition(0);
        //获取第一个城市对应的城区的名字
        //重置先前的城区集合的数据
        mWPArea.setData(mCityList.get(0).getCountys());
        mWPArea.setSelectedItemPosition(0);
    }


    public String getProvince() {
        return mProvinceList.get(mWPProvince.getCurrentItemPosition()).getProvinceName();
    }


    public String getCity() {
        return mCityList.get(mWPCity.getCurrentItemPosition()).getCityName();
    }


    public String getArea() {
        return mCityList.get(mWPCity.getCurrentItemPosition()).getCountys().get(mWPArea.getCurrentItemPosition()).getCounty_name();
    }


    public void hideArea() {
        this.removeViewAt(2);
    }

    private int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

}
