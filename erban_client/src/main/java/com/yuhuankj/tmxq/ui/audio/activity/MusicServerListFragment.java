package com.yuhuankj.tmxq.ui.audio.activity;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.music.IMusicDownloaderCore;
import com.tongdaxing.xchat_core.music.IMusicDownloaderCoreClient;
import com.tongdaxing.xchat_core.music.bean.HotMusicInfo;
import com.tongdaxing.xchat_core.player.IPlayerCoreClient;
import com.tongdaxing.xchat_core.player.bean.LocalMusicInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.fragment.BaseMvpFragment;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.audio.adapter.MusicPlayListAdapter;
import com.yuhuankj.tmxq.ui.audio.adapter.MusicServerListAdapter;
import com.yuhuankj.tmxq.ui.audio.presenter.MusicServerListPresenter;
import com.yuhuankj.tmxq.ui.widget.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;
@CreatePresenter(MusicServerListPresenter.class)
public class MusicServerListFragment extends BaseMvpFragment<MusicServerListView, MusicServerListPresenter>
        implements MusicServerListView,View.OnClickListener, MusicServerListAdapter.OnHotMusicItemClickListener, MusicPlayListAdapter.OnPlayErrorListener, SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView rv_musicList;
    private View ll_searchEmpty;

    private SwipeRefreshLayout srl_refresh;

    private MusicServerListAdapter adapter;
    private List<HotMusicInfo> hotMusicInfoList = new ArrayList<>();

    /**
     * 数据懒加载
     */
    @Override
    public void loadLazyIfYouWant() {
        if(null == hotMusicInfoList || hotMusicInfoList.size() == 0){
            getDialogManager().showProgressDialog(getActivity(),getResources().getString(R.string.network_loading));
            getMvpPresenter().searchServerMusic(null);
        }else{
            refreshHotMusicListView();
        }
    }

    private void refreshHotMusicListView() {
        boolean musicListEmpty = null == hotMusicInfoList || hotMusicInfoList.size() == 0;
        srl_refresh.setEnabled(!musicListEmpty);
        if(!musicListEmpty){
            adapter.setHotMusicInfos(hotMusicInfoList);
            adapter.notifyDataSetChanged();
        }
        ll_searchEmpty.setVisibility(musicListEmpty ? View.VISIBLE : View.GONE);
        rv_musicList.setVisibility(musicListEmpty ? View.GONE : View.VISIBLE);
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_server_music_library;
    }

    @Override
    public void onFindViews() {
        srl_refresh = mView.findViewById(R.id.srl_refresh);
        rv_musicList = mView.findViewById(R.id.rv_musicList);
        ll_searchEmpty = mView.findViewById(R.id.ll_searchEmpty);
        adapter = new MusicServerListAdapter(getActivity());
        adapter.setHotMusicInfos(hotMusicInfoList);
        adapter.setOnHotMusicItemClickListener(this);
        adapter.setOnPlayErrorListener(this);
        LinearLayoutManager linearLayoutManager = new RecyclerViewNoBugLinearLayoutManager(getActivity());
        rv_musicList.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(), linearLayoutManager.getOrientation(),
                1, R.color.color_1Affffff);
        dividerItemDecoration.setIngoreLastDividerItem(true);
        rv_musicList.addItemDecoration(dividerItemDecoration);
        rv_musicList.setAdapter(adapter);
        mView.findViewById(R.id.ll_searchHotMusic).setOnClickListener(this);
        mView.findViewById(R.id.tv_search).setVisibility(View.VISIBLE);
    }

    @Override
    public void onSetListener() {
        adapter.setOnPlayErrorListener(this);
        adapter.setOnHotMusicItemClickListener(this);
        srl_refresh.setOnRefreshListener(this);
        srl_refresh.setEnabled(true);
    }

    @Override
    public void initiate() {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ll_searchHotMusic:
                SearchServerMusicActivity.start(getContext(),null);
                //房间-搜索音乐
                StatisticManager.get().onEvent(getContext(),
                        StatisticModel.EVENT_ID_ROOM_HOT_MUSIC_SEARCH,
                        StatisticModel.getInstance().getUMAnalyCommonMap(getContext()));
                break;
            default:
                break;
        }
    }

    @Override
    public void onGetHotMusicInfoList(boolean isSuccess, String msg, List list) {
        LogUtils.d(TAG,"onGetHotMusicInfoList-isSuccess:"+isSuccess+" msg:"+msg+" list:"+list);
        mIsLoaded = isSuccess;
        srl_refresh.setRefreshing(false);
        if(!isSuccess && !TextUtils.isEmpty(msg)){
            toast(msg);
        }
        hotMusicInfoList = list;
        if(null != list && list.size()>0 && !CoreManager.getCore(IMusicDownloaderCore.class).updateHotMusicInfoToLocal(list)){
            getDialogManager().dismissDialog();
            refreshHotMusicListView();
        }
    }

    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient.class)
    public void onHotMusicInfoUpdateToLocalCompleted() {
        getDialogManager().dismissDialog();
        if(null != hotMusicInfoList && hotMusicInfoList.size()>0){
            refreshHotMusicListView();
        }
    }


    @Override
    public void onHotMusicClickToDown(HotMusicInfo hotMusicInfo) {
        CoreManager.getCore(IMusicDownloaderCore.class).addHotMusicToDownQueue(hotMusicInfo);
        //房间-热门音乐下载
        StatisticManager.get().onEvent(getContext(),
                StatisticModel.EVENT_ID_ROOM_HOT_MUSIC_DOWNLOAD,
                StatisticModel.getInstance().getUMAnalyCommonMap(getContext()));
    }

    @Override
    public void onPlayError(String msg) {
        if(!TextUtils.isEmpty(msg)){
            toast(msg);
        }
    }

    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient.class)
    public void onHotMusicDownloadProgressUpdate(LocalMusicInfo localMusicInfo, HotMusicInfo hotMusicInfo, int progress) {
        int index = hotMusicInfoList.indexOf(hotMusicInfo);
        if(-1 != index){
            try {
                adapter.notifyItemChanged(index,progress);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient.class)
    public void onHotMusicDownloadError(String msg, LocalMusicInfo localMusicInfo, HotMusicInfo hotMusicInfo) {
        int index = hotMusicInfoList.indexOf(hotMusicInfo);
        if(-1 != index){
            try {
                adapter.notifyItemChanged(index,-1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient.class)
    public void onHotMusicDownloadCompleted(HotMusicInfo hotMusicInfo) {
        int index = hotMusicInfoList.indexOf(hotMusicInfo);
        if(-1 != index){
            try {
                adapter.notifyItemChanged(index,101);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Called when a swipe gesture triggers a refresh.
     */
    @Override
    public void onRefresh() {
        getMvpPresenter().searchServerMusic(null);
    }

    private HotMusicInfo currHotInfo;

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicPlaying(LocalMusicInfo localMusicInfo) {
//        notifyLastMusicStopPlay();
//        changeMusicPlayStatus(localMusicInfo);
        adapter.notifyDataSetChanged();
    }

    private void notifyLastMusicStopPlay() {
        if(null != currHotInfo){
            int index = hotMusicInfoList.indexOf(currHotInfo);
            if(-1 != index){
                adapter.notifyItemChanged(index);
            }
        }
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicPause(LocalMusicInfo localMusicInfo) {
//        changeMusicPlayStatus(localMusicInfo);
        adapter.notifyDataSetChanged();
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicStop() {
        adapter.notifyDataSetChanged();
    }

    private void changeMusicPlayStatus(LocalMusicInfo localMusicInfo){
        try {
            int songId = Integer.valueOf(localMusicInfo.getSongId());
            HotMusicInfo tempInfo = new HotMusicInfo();
            tempInfo.setId(songId);
            if(songId>0){
                int index = hotMusicInfoList.indexOf(tempInfo);
                if(-1 != index){
                    adapter.notifyItemChanged(index);
                    currHotInfo = hotMusicInfoList.get(index);
                }
            }
        } catch (Exception nfex){
            nfex.printStackTrace();
        }
    }
}
