package com.yuhuankj.tmxq.ui.find;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * 发现页model层
 */
public class FindModel extends BaseMvpModel {

    //获取发现页数据
    public void discover(HttpRequestCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.discover(), params, callBack);
    }

    //获取附近页数据
    public Call<ResponseBody> nearby(int gender, int pageNum, int pageSize, HttpRequestCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("pageNum", String.valueOf(pageNum));
        params.put("pageSize", String.valueOf(pageSize));
        params.put("gender", String.valueOf(gender));
        return OkHttpManager.getInstance().postRequest(UriProvider.nearby(), params, callBack);
    }
}
