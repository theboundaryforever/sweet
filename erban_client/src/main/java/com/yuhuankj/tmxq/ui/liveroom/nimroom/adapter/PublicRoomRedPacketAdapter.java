package com.yuhuankj.tmxq.ui.liveroom.nimroom.adapter;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.xchat_core.bean.RoomRedPacket;
import com.yuhuankj.tmxq.R;

import java.util.List;

/**
 * @author liaoxy
 * @Description:广播红包页adapter
 * @date 2019/1/17 15:54
 */
public class PublicRoomRedPacketAdapter extends BaseQuickAdapter<RoomRedPacket, BaseViewHolder> {

    public PublicRoomRedPacketAdapter(List data) {
        super(R.layout.view_red_packet_public_room, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, RoomRedPacket item) {
        RelativeLayout rlRedPacket = helper.getView(R.id.rlRedPacket);
        TextView tvName = helper.getView(R.id.tvName);
        TextView tvTimer = helper.getView(R.id.tvTimer);
        tvName.setText(item.getNick());
        if (item.getType() == 1) {
            //拼手气红包
            tvTimer.setVisibility(View.GONE);
        } else {
            long syTime = item.getUseabelTime();
            if (syTime <= 0) {
                //超时
                tvTimer.setVisibility(View.GONE);
            } else {
                tvTimer.setVisibility(View.VISIBLE);
                tvTimer.setText(syTime + "s");
            }
        }
    }
}