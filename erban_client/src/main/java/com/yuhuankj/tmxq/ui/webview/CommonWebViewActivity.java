package com.yuhuankj.tmxq.ui.webview;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.WindowManager;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.gson.JsonSyntaxException;
import com.jph.takephoto.app.TakePhotoActivity;
import com.jph.takephoto.compress.CompressConfig;
import com.jph.takephoto.model.TResult;
import com.juxiao.library_utils.DisplayUtils;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.netease.nim.uikit.session.activity.P2PMessageActivity;
import com.tencent.bugly.crashreport.BuglyLog;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.ButtonUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.file.JXFileUtils;
import com.tongdaxing.erban.libcommon.utils.json.JsonParser;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.bean.WebViewStyle;
import com.tongdaxing.xchat_core.common.ICommonClient;
import com.tongdaxing.xchat_core.count.IChargeClient;
import com.tongdaxing.xchat_core.file.IFileCore;
import com.tongdaxing.xchat_core.file.IFileCoreClient;
import com.tongdaxing.xchat_core.liveroom.im.IMUriProvider;
import com.tongdaxing.xchat_core.manager.agora.AgoraEngineManager;
import com.tongdaxing.xchat_core.redpacket.IRedPacketCoreClient;
import com.tongdaxing.xchat_core.redpacket.bean.WebViewInfo;
import com.tongdaxing.xchat_core.share.IShareCore;
import com.yuhuankj.tmxq.BuildConfig;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.share.ShareDialog;
import com.yuhuankj.tmxq.widget.TitleBar;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cn.sharesdk.framework.Platform;


public class CommonWebViewActivity<V extends IMvpBaseView, P extends AbstractMvpPresenter<V>> extends
        TakePhotoActivity<V, P> implements ShareDialog.OnShareDialogItemClick ,IMvpBaseView{
    private static final String CAMERA_PREFIX = "picture_";
    public static final String ACTION_RELOAD = "ACTION_RELOAD";//重新加载广播事件
    public static final String ACTION_RELOAD_ORI_URL = "ACTION_RELOAD_ORI_URL";//重新加载原始页
    public static final String ACTION_BACK = "ACTION_BACK";//页面返回
    private static final String POSITION = "position";
    private static final String JS_NAME = "androidJsObj";
    public static boolean isGameRunning = false;//是否正在游戏中
    public String TAG = CommonWebViewActivity.class.getSimpleName();
    public Handler mHandler = new Handler();
    public TitleBar mTitleBar;
    private View fl_title;
    protected WebView webView;
    private JSInterface jsInterface;
    private ProgressBar mProgressBar;
    private CommonWebViewActivity mActivity;
    private WebChromeClient wvcc;
    private ImageView imgShare;
    private View vTitleBar;
    protected View flRoot;

    private WebViewInfo webViewInfo = null;
    private String url, data;
    private boolean showTitle;
    private int mPosition;
    private int mProgress;
    private boolean isFirstLoad = true;
    public boolean isBackFromOrderConfirmView = false;
    public long roomId = 0;

    protected boolean hasTitleRecived = false;

    private Runnable mProgressRunnable = new Runnable() {
        @Override
        public void run() {
            if (mProgress < 96) {
                mProgress += 3;
                mProgressBar.setProgress(mProgress);
                mHandler.postDelayed(mProgressRunnable, 10);
            }
        }
    };
    //检查游戏加载资源超时
    private Runnable mCheckGameLoadTimeoutRunnable = new Runnable() {
        @Override
        public void run() {
            if (isGameRunning && jsInterface != null && jsInterface.isLoadingShowing()) {
                SingleToastUtil.showToast("加载游戏超时");
                finish();
            }
        }
    };

    public static void start(Context context, String url, boolean showTitle) {
        Intent intent = new Intent(context, CommonWebViewActivity.class);
        intent.putExtra("url", url);
        intent.putExtra("showTitle", showTitle);
        context.startActivity(intent);
    }

    public static void start(Context context, String url, boolean showTitle, long roomId) {
        Intent intent = new Intent(context, CommonWebViewActivity.class);
        intent.putExtra("url", url);
        intent.putExtra("showTitle", showTitle);
        intent.putExtra("roomId", roomId);
        context.startActivity(intent);
    }

    public static void start(Context context, String url, WebViewStyle style) {
        Intent intent = new Intent(context, CommonWebViewActivity.class);
        intent.putExtra("url", url);
        intent.putExtra("style", style);
        context.startActivity(intent);
    }

    public static void start(Context context, String url, long roomId) {
        Intent intent = new Intent(context, CommonWebViewActivity.class);
        intent.putExtra("url", url);
        intent.putExtra("roomId", roomId);
        context.startActivity(intent);
    }

    public static void start(Context context, String url) {
        Intent intent = new Intent(context, CommonWebViewActivity.class);
        intent.putExtra("url", url);
        context.startActivity(intent);
    }

    public static void startForHtmlData(Context context, String data) {
        Intent intent = new Intent(context, CommonWebViewActivity.class);
        intent.putExtra("data", data);
        context.startActivity(intent);
    }

    /**
     * 排行榜专用
     */
    public static void start(Context context, String url, int position) {
        Intent intent = new Intent(context, CommonWebViewActivity.class);
        intent.putExtra("url", url);
        intent.putExtra(POSITION, position);
        context.startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (null != webView) {
                        webView.evaluateJavascript("onStart()", new ValueCallback<String>() {
                            @Override
                            public void onReceiveValue(String value) {
                                LogUtils.d(TAG, "onStart-onReceiveValue-value:" + value);
                            }
                        });
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startCheckGameLoadTime() {
        mHandler.postDelayed(mCheckGameLoadTimeoutRunnable, 30000);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        url = intent.getStringExtra("url");
        data = intent.getStringExtra("data");
        setContentView(R.layout.activity_common_web_view);
        initTitleBar("");
        initView();
        BuglyLog.d(TAG, "onCreate-url:" + url);
        WebViewStyle style = (WebViewStyle) intent.getSerializableExtra("style");
        if (style != null) {
            switch (style) {
                case NO_TITLE:
                    mTitleBar.setVisibility(View.GONE);
                    break;
                case NO_TITLE_BUT_STATUS_BAR:
                    mTitleBar.setVisibility(View.GONE);

                    break;
                case FULL_SCREEN:
                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
                    break;
                case NO_TITLE_AND_FULL_SCREEN:
                    mTitleBar.setVisibility(View.GONE);
                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
                    break;
                default:
                    break;
            }
        }
        showTitle = intent.getBooleanExtra("showTitle", true);
        roomId = intent.getLongExtra("roomId", 0L);
        fl_title.setVisibility(showTitle ? View.VISIBLE : View.GONE);
        vTitleBar.setVisibility(showTitle ? View.VISIBLE : View.GONE);
        mPosition = intent.getIntExtra(POSITION, 0) + 1;
        mActivity = this;
        setListener();
        showWebView(url);
    }

    private void setListener() {
        imgShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick(v.getId())) {
                    return;
                }
                if (!TextUtils.isEmpty(url) && url.equals(UriProvider.getMyNobleUrl())) {
                    //贵族-关于贵族统计
                    Map<String, String> maps = StatisticModel.getInstance().getUMAnalyCommonMap(CommonWebViewActivity.this);
                    StatisticManager.get().onEvent(CommonWebViewActivity.this,
                            StatisticModel.EVENT_ID_ABOUT_NOBLE, maps);
                    CommonWebViewActivity.start(CommonWebViewActivity.this, UriProvider.getNobleDetailUrl(), true);
                    return;
                }
                ShareDialog shareDialog = new ShareDialog(mActivity);
                shareDialog.setOnShareDialogItemClick(CommonWebViewActivity.this);
                shareDialog.show();
            }
        });
    }

    protected void initView() {
        flRoot = findViewById(R.id.flRoot);
        flRoot.setBackgroundColor(Color.WHITE);
        vTitleBar = findViewById(R.id.vTitleBar);
        webView = (WebView) findViewById(R.id.webview);
        mTitleBar = (TitleBar) findViewById(R.id.title);
        fl_title = findViewById(R.id.fl_title);
        imgShare = (ImageView) findViewById(R.id.img_share);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);

        if (isGameRunning) {
            changeGameDisplay();
        }

        if (!TextUtils.isEmpty(url) && url.equals(UriProvider.getMyNobleUrl())) {
            imgShare.setVisibility(View.VISIBLE);
            imgShare.setImageDrawable(getResources().getDrawable(R.drawable.ic_webview_noble_more));
        }
    }

    public void changeGameDisplay() {
        setSwipeBackEnable(false);
        int statusBarHeight = DisplayUtils.getStatusBarHeight(this);
        View view = findViewById(R.id.vStatus);
        view.getLayoutParams().height = statusBarHeight;
        view.requestLayout();
        view.setVisibility(View.VISIBLE);
        vTitleBar.setVisibility(View.GONE);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initData() {
        mHandler.post(mProgressRunnable);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setUseWideViewPort(true);
        jsInterface = new JSInterface(webView, this);
        jsInterface.setPosition(mPosition);
        webView.addJavascriptInterface(jsInterface, JS_NAME);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { // http 与 https 混合的页面
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                LogUtils.d(TAG, "onReceivedSslError-error:" + error);
//                super.onReceivedSslError(view, handler, error); //一定要去掉
                // handler.cancel();// Android默认的处理方式
                handler.proceed();// 接受所有网站的证书
                // handleMessage(Message msg);// 进行其他处理
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                LogUtils.d(TAG, "onReceivedSslError-url:" + url);
                // 如下方案可在非微信内部WebView的H5页面中调出微信支付
                if (url.startsWith("weixin://wap/pay?") || url.startsWith("alipays://platformapi/startApp?")) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(url));
                    startActivity(intent);
                    return true;
                }
                return super.shouldOverrideUrlLoading(view, request);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                LogUtils.d(TAG, "onReceivedSslError-url:" + url);
                // 如下方案可在非微信内部WebView的H5页面中调出微信支付
                if (url.startsWith("weixin://wap/pay?") || url.startsWith("alipays://platformapi/startApp?")) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(url));
                    startActivity(intent);
                    return true;
                }
                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                //游戏中默认显示loading
                if (jsInterface != null && isGameRunning) {
                    jsInterface.showLoading("");
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                mProgressBar.setProgress(100);
                mProgressBar.setVisibility(View.GONE);

                //甜豆翻牌，标题栏需要浮在页面上
                boolean isLuckDraw = !TextUtils.isEmpty(url) && url.endsWith("/ttyy/luckDraw_bean/index.html");
                if (isLuckDraw) {
                    fl_title.setBackgroundColor(Color.TRANSPARENT);
                    if (hasTitleRecived) {
                        mTitleBar.setTitleColor(isLuckDraw ? Color.WHITE : getResources().getColor(R.color.back_font));
                        mTitleBar.setLeftImageResource(isLuckDraw ? R.drawable.icon_white_back : R.drawable.arrow_left);
                        mTitleBar.setCommonBackgroundColor(isLuckDraw ? Color.TRANSPARENT : Color.WHITE);
                        flRoot.setBackgroundColor(Color.WHITE);
                        fl_title.setBackgroundColor(Color.parseColor("#4022BE"));
                    }
                    vTitleBar.setVisibility(View.GONE);
                }

                onPageLoadFinished(view, url);

                LogUtils.d(TAG, "onPageFinished-url:" + url);
                try {
                    webView.evaluateJavascript("shareInfo()", new ValueCallback<String>() {
                        @Override
                        public void onReceiveValue(String value) {
                            LogUtils.d(TAG, "onPageFinished-->onReceiveValue webViewInfo:" + webViewInfo);
                            if (!StringUtil.isEmpty(value) || !value.equals("null")) {
                                String jsonData = value.replace("\\", "");
                                if (jsonData.indexOf("\"") == 0) {
                                    //去掉第一个 "
                                    jsonData = jsonData.substring(1);
                                }
                                if (jsonData.lastIndexOf("\"") == (jsonData.length() - 1)) {
                                    jsonData = jsonData.substring(0, jsonData.length() - 1);
                                }
                                try {
                                    LogUtils.d(TAG, "onPageFinished-->onReceiveValue jsonData:" + jsonData);
                                    webViewInfo = JsonParser.parseJsonToNormalObject(jsonData, WebViewInfo.class);
                                    LogUtils.d("onReceiveValue", webViewInfo + "");
                                    if (webViewInfo != null) {
                                        imgShare.setVisibility(View.VISIBLE);
                                    }
                                } catch (JsonSyntaxException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    LogUtils.d(TAG, "onPageFinished-onReceiveValue webViewInfo:" + webViewInfo + " error:" + e.getMessage());
                }
                super.onPageFinished(view, url);
            }
        });

        //获取webviewtitle作为titlebar的title
        wvcc = new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                LogUtils.d(TAG, "onReceivedTitle-title:" + title);
                //webview控件在Android6.0及以上有一个bug,那就是onReceivedTitle()会调用两次,一次为网页的url,
                // 一次为网页真正的title,故这里需要做一个过滤
                if (!TextUtils.isEmpty(url) && !TextUtils.isEmpty(title) && !url.contains(title)) {
                    mTitleBar.setTitle(title);
                    hasTitleRecived = true;
                }

                if (!TextUtils.isEmpty(title) && title.contains(IMUriProvider.getRoomPlayInfoUrl())) {
                    hasTitleRecived = true;
                }
            }

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                LogUtils.d(TAG, "onProgressChanged-newProgress:" + newProgress);
                if (jsInterface != null && jsInterface.isLoadingShowing()) {
                    jsInterface.setProgress(newProgress);
                }
            }
        };
        // 设置setWebChromeClient对象
        webView.setWebChromeClient(wvcc);
        if (mTitleBar != null) {
            mTitleBar.setImmersive(false);
            mTitleBar.setCommonBackgroundColor(Color.WHITE);
            mTitleBar.setTitleColor(getResources().getColor(R.color.back_font));
            mTitleBar.setLeftImageResource(R.drawable.arrow_left);
            mTitleBar.setLeftClickListener(v -> onLeftClickListener());
        }
        // 设置Webview的user-agent
        webView.getSettings().setUserAgentString(webView.getSettings().getUserAgentString() + " tiantianAppAndroid");
        webView.getSettings().setMediaPlaybackRequiresUserGesture(false);

        //这里从广播接收一些外部事件
        IntentFilter filter = new IntentFilter(ACTION_RELOAD_ORI_URL);
        filter.addAction(ACTION_RELOAD);
        filter.addAction(ACTION_BACK);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, filter);
    }

    public void onPageLoadFinished(WebView view, String url) {
        LogUtils.d(TAG, "onPageLoadFinished url:" + url);
    }

    public void showWebView(String url) {
        LogUtils.d(TAG, "showWebView url:" + url);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(BuildConfig.DEBUG);
        }
        if (TextUtils.isEmpty(data)) {
            if (!TextUtils.isEmpty(url)) {
                webView.loadUrl(url);
            }
        } else {
            webView.loadData(data, "text/html", "utf-8");
        }
        initData();
    }

    //调用系统按键返回上一层
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (isGameRunning && jsInterface != null && jsInterface.isLoadingShowing()) {
                getDialogManager().showOkCancelDialog("主动退出会被处罚扣分喔", "是", "否", true, new DialogManager.AbsOkDialogListener() {
                    @Override
                    public void onOk() {
                        //退出连麦
                        AgoraEngineManager.get().exitLinkMacro();
                        Intent intent = new Intent(P2PMessageActivity.ACTION_LINK_MICRO_FINISH);
                        LocalBroadcastManager.getInstance(CommonWebViewActivity.this).sendBroadcast(intent);
                        finish();
                    }
                });
                return true;
            }
            if (webView.canGoBack()) {
                webView.goBack();
                return true;
            } else {
                return super.onKeyDown(keyCode, event);
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onSharePlatformClick(Platform platform) {
        if (webViewInfo != null) {
            CoreManager.getCore(IShareCore.class).shareH5(webViewInfo, platform);
            LogUtils.d("onSharePlatformClick", webViewInfo + "");
        }
    }

    @CoreEvent(coreClientClass = IRedPacketCoreClient.class)
    public void onShareWebView() {
        toast("分享成功");
    }

    @CoreEvent(coreClientClass = IRedPacketCoreClient.class)
    public void onShareWebViewError() {
        toast("分享失败，请重试");
    }

    @CoreEvent(coreClientClass = IRedPacketCoreClient.class)
    public void onShareWebViewCanle() {
        toast("取消分享");
    }

    @CoreEvent(coreClientClass = IRedPacketCoreClient.class)
    public void onShareViewRedError(String error) {
        toast(error);
    }

    @CoreEvent(coreClientClass = ICommonClient.class)
    public void onRecieveNeedRefreshWebView() {
        if (!StringUtil.isEmpty(url)) {
            showWebView(url);
        }
    }

    @CoreEvent(coreClientClass = IChargeClient.class)
    public void chargeAction(String action) {
        LogUtils.d(TAG, "chargeAction action:" + action);
        try {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    webView.evaluateJavascript("getStatus()", new ValueCallback<String>() {
                        @Override
                        public void onReceiveValue(String value) {
                            LogUtils.d("chargeAction-onReceiveValue", "value:" + value);
                        }
                    });
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //重写左上角返回键按钮点击
    @Override
    protected void onLeftClickListener() {
        if (jsInterface != null && jsInterface.isInterceptBack()) {
            jsInterface.userReturn(webView);
        } else {
            if (webView.canGoBack()) {
                webView.goBack();
            } else {
                finish();
            }
        }
    }

    public void close() {
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (webView != null) {
            webView.onPause();
        }
        StatisticManager.get().onPause(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (webView != null) {
            webView.onResume();
        }

        //这里判断url后缀等于我的贵族url后缀，则每次界面可见就刷新界面
        LogUtils.d(TAG, "onResume-isFirstLoad:" + isFirstLoad + " isBackFromOrderConfirmView:" + isBackFromOrderConfirmView);
        if (!isFirstLoad && !TextUtils.isEmpty(url)
                && url.equals(UriProvider.getMyNobleUrl())
                && isBackFromOrderConfirmView) {
            webView.reload();
            isBackFromOrderConfirmView = false;
        }
        isFirstLoad = false;

        StatisticManager.get().onResume(this);
    }

    @Override
    protected void onDestroy() {
        isGameRunning = false;
        if (jsInterface != null) {
            jsInterface.setLoadingShowing(false);
        }
        if (mHandler != null) {
            mHandler.removeCallbacks(mProgressRunnable);
            if (mCheckGameLoadTimeoutRunnable != null) {
                mHandler.removeCallbacks(mCheckGameLoadTimeoutRunnable);
                mCheckGameLoadTimeoutRunnable = null;
            }
            mHandler.removeCallbacksAndMessages(null);
            mProgressRunnable = null;
            mHandler = null;
        }
        super.onDestroy();
        if (webView != null) {
            webView.setWebViewClient(null);
            webView.setWebChromeClient(null);
            webView.removeJavascriptInterface(JS_NAME);
            //https://blog.csdn.net/u013085697/article/details/53259116  单纯移除webview依然会有ContentViewCore引起webview的mContent泄露
            ViewParent parent = webView.getParent();
            if (parent != null) {
                ((ViewGroup) parent).removeView(webView);
            }
            webView.removeAllViews();
            webView.destroy();
            webView = null;
        }
        if (broadcastReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        }


    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_RELOAD.equals(intent.getAction())) {
                webView.reload();
            }
            if (ACTION_BACK.equals(intent.getAction())) {
                if (webView.canGoBack()) {
                    webView.goBack();
                }
            } else if (ACTION_RELOAD_ORI_URL.equals(intent.getAction())) {
                try {
                    String url = intent.getStringExtra("url");
                    if (TextUtils.isEmpty(url)) {
                        return;
                    }
                    webView.loadUrl(url);
                    webView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (webView != null) {
                                webView.clearHistory();
                            }
                        }
                    }, 1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };

    @Override
    public void onBackPressed() {
        if (jsInterface != null && jsInterface.isInterceptBack()) {
            jsInterface.userReturn(webView);
        } else {
            super.onBackPressed();
        }
    }

    public void showImageChooser() {
        //图片压缩配置 暂时写死 长或宽不超过当前屏幕的长或宽、图片大小不超过2M
        CompressConfig compressConfig = new CompressConfig.Builder().setMaxPixel(DisplayUtils.getScreenHeight(this)).setMaxSize(2048 * 1024).create();
        getTakePhoto().onEnableCompress(compressConfig, true);

        ButtonItem buttonItem = new ButtonItem("拍照上传", () ->
                //授权检查
                checkPermission(this::takePhoto, R.string.ask_camera, android.Manifest.permission.CAMERA));
        ButtonItem buttonItem1 = new ButtonItem("本地相册", () -> {
            getTakePhoto().onPickFromGallery();

        });
        List<ButtonItem> buttonItems = new ArrayList<>();
        buttonItems.add(buttonItem);
        buttonItems.add(buttonItem1);
        getDialogManager().showCommonPopupDialog(buttonItems, "取消", false);
    }

    private void takePhoto() {
        String mCameraCapturingName = CAMERA_PREFIX + System.currentTimeMillis() + ".jpg";
        File cameraOutFile = JXFileUtils.getTempFile(CommonWebViewActivity.this, mCameraCapturingName);
        if (!cameraOutFile.getParentFile().exists()) {
            cameraOutFile.getParentFile().mkdirs();
        }
        Uri uri = Uri.fromFile(cameraOutFile);
        getTakePhoto().onPickFromCapture(uri);
    }

    @Override
    public void takeSuccess(TResult result) {
        getDialogManager().showProgressDialog(CommonWebViewActivity.this, "请稍后");
        CoreManager.getCore(IFileCore.class).upload(new File(result.getImage().getCompressPath()));
    }

    @Override
    public void takeFail(TResult result, String msg) {
        toast(msg);
        if (jsInterface != null) {
            jsInterface.onImageChooserResult("");
        }
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUpload(String url) {
        getDialogManager().dismissDialog();
        if (jsInterface != null) {
            jsInterface.onImageChooserResult(url);
        }
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUploadFail() {
        toast("上传失败");
        getDialogManager().dismissDialog();
        if (jsInterface != null) {
            jsInterface.onImageChooserResult("");
        }
    }

    @Override
    public boolean isStatusBarDarkFont() {
        return TextUtils.isEmpty(url) || !url.endsWith("/ttyy/luckDraw_bean/index.html");
    }

    @Override
    public void initImmersionBar(boolean statusBarDarkFont) {
        super.initImmersionBar(statusBarDarkFont);
        LogUtils.d(TAG, "initImmersionBar statusBarDarkFont:" + statusBarDarkFont + " url:" + url);
        //主要针对小游戏兼容适配
        if (statusBarDarkFont && null != vTitleBar && !TextUtils.isEmpty(url) && !url.endsWith("/ttyy/luckDraw_bean/index.html")) {
            vTitleBar.setVisibility(View.GONE);
        }
    }
}