package com.yuhuankj.tmxq.ui.liveroom.redpacket;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author liaoxy
 * @Description:红包明细实体类
 * @date 2019/1/17 15:55
 */
public class RedPacketDetailEnitity implements Serializable {
    private int redPacketNum;//": 1,
    private int redPacketReceiveNum;//": 1,
    private int redPacketGold;//": 1,
    private ArrayList<RedPacketDetaiItemlEnitity> list = new ArrayList<>();//":

    public int getRedPacketNum() {
        return redPacketNum;
    }

    public void setRedPacketNum(int redPacketNum) {
        this.redPacketNum = redPacketNum;
    }

    public int getRedPacketReceiveNum() {
        return redPacketReceiveNum;
    }

    public void setRedPacketReceiveNum(int redPacketReceiveNum) {
        this.redPacketReceiveNum = redPacketReceiveNum;
    }

    public int getRedPacketGold() {
        return redPacketGold;
    }

    public void setRedPacketGold(int redPacketGold) {
        this.redPacketGold = redPacketGold;
    }

    public ArrayList<RedPacketDetaiItemlEnitity> getList() {
        return list;
    }

    public void setList(ArrayList<RedPacketDetaiItemlEnitity> list) {
        this.list = list;
    }
}
