package com.yuhuankj.tmxq.ui.audio.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.tongdaxing.xchat_core.home.TabInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.UIUtil;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.commonnavigator.titles.ColorTransitionPagerTitleView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p> 公共多个滑动tab样式 </p>
 *
 * @author Administrator
 * @date 2017/11/15
 */
public class MusicLibraryMagicIndicatorAdapter extends CommonNavigatorAdapter {

    private Context mContext;
    private List<TabInfo> mTitleList;
    private int indicatorBottomMargin,indicatorTopMarginTitle;
    private int titleTxtViewLeftRightPadding;
    private Map<Integer, SpannableStringBuilder> integerSpannableStringBuilderMap = new HashMap<>();

    public Map<Integer, SpannableStringBuilder> getIntegerSpannableStringBuilderMap() {
        return integerSpannableStringBuilderMap;
    }

    public List<TabInfo> getTitleList() {
        return mTitleList;
    }

    public MusicLibraryMagicIndicatorAdapter(Context context, List<TabInfo> titleList, int indicatorBottomMargin,
                                             int indicatorTopMarginTitle, int titleTxtViewLeftRightPadding) {
        mContext = context;
        mTitleList = titleList;
        this.indicatorBottomMargin = indicatorBottomMargin;
        this.indicatorTopMarginTitle = indicatorTopMarginTitle;
        this.titleTxtViewLeftRightPadding = titleTxtViewLeftRightPadding;
    }

    private int size = 16;

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public int getCount() {
        return mTitleList == null ? 0 : mTitleList.size();
    }

    @Override
    public IPagerTitleView getTitleView(Context context, final int i) {
        ColorTransitionPagerTitleView colorTransitionPagerTitleView = new ColorTransitionPagerTitleView(context);
        if (titleTxtViewLeftRightPadding > 0) {
            colorTransitionPagerTitleView.setPadding(titleTxtViewLeftRightPadding, 0, titleTxtViewLeftRightPadding, 0);
        }
        colorTransitionPagerTitleView.setNormalColor(ContextCompat.getColor(mContext, R.color.color_787879));
        colorTransitionPagerTitleView.setSelectedColor(ContextCompat.getColor(mContext, R.color.color_10CFA6));
        colorTransitionPagerTitleView.setTextSize(size);

        SpannableStringBuilder spannableStringBuilder = integerSpannableStringBuilderMap.get(i);
        if (spannableStringBuilder != null) {
            colorTransitionPagerTitleView.setText(spannableStringBuilder);
        } else {
            colorTransitionPagerTitleView.setText(mTitleList.get(i).getName());
        }


        colorTransitionPagerTitleView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemSelectListener != null) {
                    mOnItemSelectListener.onItemSelect(i);
                }
            }
        });
        return colorTransitionPagerTitleView;
    }


    @Override
    public IPagerIndicator getIndicator(Context context) {
        LinePagerIndicator indicator = new LinePagerIndicator(context);
        indicator.setMode(LinePagerIndicator.MODE_WRAP_CONTENT);
        indicator.setLineHeight(UIUtil.dip2px(mContext, 1));
        indicator.setRoundRadius(UIUtil.dip2px(mContext, 1));
        indicator.setColors(mContext.getResources().getColor(R.color.color_10CFA6));
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        lp.bottomMargin = indicatorBottomMargin;
        lp.topMargin = indicatorTopMarginTitle;
        indicator.setLayoutParams(lp);
        return indicator;
    }

    private OnItemSelectListener mOnItemSelectListener;

    public void setOnItemSelectListener(OnItemSelectListener onItemSelectListener) {
        mOnItemSelectListener = onItemSelectListener;
    }

    public interface OnItemSelectListener {
        void onItemSelect(int position);
    }
}