package com.yuhuankj.tmxq.ui.search.interfaces;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.room.bean.SearchRoomPersonInfo;

import java.util.List;

public interface SearchRoomView<T extends AbstractMvpPresenter> extends IMvpBaseView {

    /**
     * 搜索房间 关键字搜索结果返回
     * @param success
     * @param message
     * @param rooms
     */
    void onRoomSearched(boolean success, String message, List<SearchRoomPersonInfo> rooms);

    /**
     * 搜索房间 关键字搜索结果返回
     * @param success
     * @param message
     * @param rooms
     */
    void onRoomRecommend(boolean success, String message, List<T> rooms);
}
