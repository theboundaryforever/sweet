package com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.im.IMReportResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomModel;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMember;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IRoomBlackListView;

import java.util.List;

/**
 * <p> </p>
 *
 * @author jiahui
 * @date 2017/12/19
 */
public class IMRoomManagerPresenter extends AbstractMvpPresenter<IRoomBlackListView> {

    private final String TAG = IMRoomManagerPresenter.class.getSimpleName();

    public IMRoomManagerPresenter() {
    }

    public void queryRoomManagerList(int limit, long roomId) {
        OkHttpManager.MyCallBack myCallBack = new OkHttpManager.MyCallBack<IMReportResult<List<IMRoomMember>>>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (null != getMvpView()) {
                    getMvpView().showErrMsg(e.getMessage());
                }
            }

            @Override
            public void onResponse(IMReportResult<List<IMRoomMember>> imReportResult) {
                LogUtils.d(TAG, "getRoomOnLineUserList-onResponse-imReportResult:" + imReportResult);
                if (null == imReportResult) {
                    getMvpView().showErrMsg(BasicConfig.INSTANCE.getAppContext().getResources().getString(R.string.network_error));
                    return;
                }
                if (imReportResult.isSuccess()) {
                    if (!ListUtils.isListEmpty(imReportResult.getData())) {
                        RoomDataManager.get().mRoomManagerList = imReportResult.getData();
                    }
                    getMvpView().showRoomBlackList(imReportResult.getData());
                } else {
                    getMvpView().showListEmptyView();
                }
            }
        };
        new IMRoomModel().getRoomManagers(myCallBack);
    }

    public void removeUserFromManagerList(final String account, long roomId) {
        OkHttpManager.MyCallBack myCallBack = new OkHttpManager.MyCallBack<IMReportResult>() {

            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (getMvpView() != null) {
                    getMvpView().showErrMsg(e.getMessage());
                }
            }

            @Override
            public void onResponse(IMReportResult imReportResult) {
                if (null == imReportResult) {
                    getMvpView().showErrMsg(BasicConfig.INSTANCE.getAppContext().getResources().getString(R.string.network_error));
                    return;
                }
                if (imReportResult.isSuccess()) {
                    getMvpView().removeUserFromBlackList(account);
                } else {
                    getMvpView().showErrMsg(imReportResult.getErrmsg());
                }
            }
        };

        IMRoomMessageManager.get().markManager(account, false, myCallBack);
    }
}
