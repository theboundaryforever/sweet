package com.yuhuankj.tmxq.ui.launch.guide;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.tongdaxing.xchat_core.PreferencesUtils;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.ui.MainActivity;


public class UserGuideActivity extends BaseActivity {


    protected static final String TAG = "UserGuide";
    private ViewPager mViewPage;
    private ImageView enterHome;
    private GuideAdapter adapter;

    public static void start(Context context) {
        Intent intent = new Intent(context, UserGuideActivity.class);
        context.startActivity(intent);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_guide);
        initView();
    }

    private void initView() {
       mViewPage = (ViewPager) findViewById(R.id.view_page);
        enterHome = (ImageView) findViewById(R.id.enter_home);
        adapter = new GuideAdapter(this);
        mViewPage.setAdapter(new GuideAdapter(this));
        mViewPage.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position == adapter.getCount()-1){
                    enterHome.setVisibility(View.VISIBLE);
                }else{
                    enterHome.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        enterHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferencesUtils.setFristUser(false);
                MainActivity.start(UserGuideActivity.this);
                finish();
            }
        });

    }


}
