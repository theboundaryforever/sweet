package com.yuhuankj.tmxq.ui.me.noble;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.pingplusplus.android.Pingpp;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.constant.StatisticModel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * 购买贵族-订单确认页
 * <p>
 * 1.创建订单
 * 2.拉起支付流程 Pingpp.createPayment(this, data);
 * 3.购买成功，界面提示，关闭
 */
@CreatePresenter(OrderConfirmPresenter.class)
public class OrderConfirmActivity extends BaseMvpActivity<OrderConfirmView, OrderConfirmPresenter>
        implements OrderConfirmView, View.OnClickListener {

    private final String TAG = OrderConfirmActivity.class.getSimpleName();

    private WalletInfo walletInfo;

    /**
     * 开通
     */
    private static final int ORDER_TYPE_OPEN = 1;
    /**
     * 续费
     */
    private static final int ORDER_TYPE_RENEWALS = 2;

    private String nbLevelName = null;
    private int timeDays = 0;
    private int coins = 0;
    private String reward = null;
    private int type = 0;
    private int vipId = 0;
    private long roomId = 0L;

    private TextView tvOrderTips;
    private TextView tvNobleLevel;
    private TextView tvNobleTime;
    private TextView tvNobleCoins;
    private TextView tvBuyNoble;
    private ImageView ivNobleMedal;

    private FrameLayout flCoinMethod;
    private TextView tvCoins;
    private ImageView ivCoinMethod;

    private FrameLayout flALiMethod;
    private ImageView ivALiMethod;

    private FrameLayout flWXMethod;
    private ImageView ivWXMethod;

    /**
     * H5拉起原生订单确认页
     *
     * @param context
     * @param nbLevelName
     * @param timeDays
     * @param coins
     * @param reward
     * @param type
     * @param roomId
     */
    public static void startFromH5(Context context, int vipId, String nbLevelName, int timeDays,
                                   int coins, String reward, int type, long roomId) {
        Intent intent = new Intent(context, OrderConfirmActivity.class);
        intent.putExtra("nbLevelName", nbLevelName);
        intent.putExtra("timeDays", timeDays);
        intent.putExtra("coins", coins);
        intent.putExtra("reward", reward);
        intent.putExtra("type", type);
        intent.putExtra("vipId", vipId);
        intent.putExtra("roomId", roomId);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_confirm);
        initData();
        initView();
        getDialogManager().showProgressDialog(this, getResources().getString(R.string.network_loading));
        getMvpPresenter().refreshWalletInfo(true);
    }

    private void initData() {
        Intent intent = getIntent();
        if (null != intent) {
            nbLevelName = intent.getStringExtra("nbLevelName");
            timeDays = intent.getIntExtra("timeDays", 0);
            coins = intent.getIntExtra("coins", 0);
            reward = intent.getStringExtra("reward");
            type = intent.getIntExtra("type", 0);
            vipId = intent.getIntExtra("vipId", 0);
            roomId = intent.getLongExtra("roomId", 0L);
        }

        //传值错误则界面直接返回
        if ((ORDER_TYPE_OPEN != type && ORDER_TYPE_RENEWALS != type)) {
            finish();
        }
    }

    private void initView() {
        LogUtils.d(TAG, "initView");
        initTitleBar(getResources().getString(R.string.order_confirm));
        if (null != mTitleBar) {
            mTitleBar.setLeftImageResource(R.drawable.ic_arrow_back);
            mTitleBar.setCommonBackgroundColor(Color.TRANSPARENT);
            mTitleBar.setTitleColor(Color.WHITE);
        }

        tvBuyNoble = (TextView) findViewById(R.id.tvBuyNoble);
        tvBuyNoble.setOnClickListener(this);
        tvOrderTips = (TextView) findViewById(R.id.tvOrderTips);
        tvNobleLevel = (TextView) findViewById(R.id.tvNobleLevel);
        tvNobleTime = (TextView) findViewById(R.id.tvNobleTime);
        tvNobleCoins = (TextView) findViewById(R.id.tvNobleCoins);
        tvCoins = (TextView) findViewById(R.id.tvCoins);
        ivNobleMedal = (ImageView) findViewById(R.id.ivNobleMedal);
        ivCoinMethod = (ImageView) findViewById(R.id.ivCoinMethod);
        ivWXMethod = (ImageView) findViewById(R.id.ivWXMethod);
        ivALiMethod = (ImageView) findViewById(R.id.ivALiMethod);
        flCoinMethod = (FrameLayout) findViewById(R.id.flCoinMethod);
        flCoinMethod.setOnClickListener(this);
        flALiMethod = (FrameLayout) findViewById(R.id.flALiMethod);
        flALiMethod.setOnClickListener(this);
        flWXMethod = (FrameLayout) findViewById(R.id.flWXMethod);
        flWXMethod.setOnClickListener(this);

        tvNobleLevel.setText(getResources().getString(R.string.order_noble_level_tips, nbLevelName));
        tvNobleTime.setText(getResources().getString(R.string.order_noble_time_tips, timeDays));
        updateCostInfo();
        int index;
        int medalIconResId = NobleBusinessManager.getNobleOrderMedalResId(vipId);
        if (medalIconResId > 0) {
            ivNobleMedal.setImageDrawable(getResources().getDrawable(medalIconResId));
            ivNobleMedal.setVisibility(View.VISIBLE);
        } else {
            ivNobleMedal.setVisibility(View.INVISIBLE);
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String time = dateFormat.format(new Date(System.currentTimeMillis()));
        String tips = getResources().getString(R.string.order_noble_coins_tips, reward, time);
        String rewardTips = getResources().getString(1 == type ? R.string.order_open_noble_reward_coins_tips :
                R.string.order_renew_noble_reward_coins_tips, tips);
        SpannableStringBuilder ssb1 = new SpannableStringBuilder(rewardTips);
        index = rewardTips.indexOf(tips);
        ssb1.setSpan(new ForegroundColorSpan(Color.parseColor("#B4B4B4")), 0,
                index, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        ssb1.setSpan(new ForegroundColorSpan(Color.parseColor("#D7AC69")), index,
                index + tips.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        ssb1.setSpan(new ForegroundColorSpan(Color.parseColor("#B4B4B4")), index + tips.length(),
                rewardTips.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        tvOrderTips.setText(ssb1);
    }

    private void updateCostInfo() {
        String coinsNeed = null;
        int cost = 0;
        if (0 == currPayMethod) {
            cost = coins;
            coinsNeed = getResources().getString(R.string.order_noble_coins_need_tips, cost);
        } else {
            cost = coins / 10;
            coinsNeed = getResources().getString(R.string.order_noble_money_need_tips, cost);
        }

        SpannableStringBuilder ssb = new SpannableStringBuilder(coinsNeed);
        int costIndex = coinsNeed.indexOf(String.valueOf(cost));
        int costLength = String.valueOf(cost).length();
        ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#ffffff")), 0,
                costIndex, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#D7AC69")), costIndex,
                costIndex + costLength, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#ffffff")), costIndex + costLength,
                coinsNeed.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        tvNobleCoins.setText(ssb);
    }

    private int currPayMethod = 0;

    private void updatePayMethodSelected() {
        ivCoinMethod.setImageDrawable(getResources().getDrawable(currPayMethod == 0 ?
                R.drawable.ic_order_confirm_method_selected : R.drawable.ic_order_confirm_method_unselected));
        ivALiMethod.setImageDrawable(getResources().getDrawable(currPayMethod == 1 ?
                R.drawable.ic_order_confirm_method_selected : R.drawable.ic_order_confirm_method_unselected));
        ivWXMethod.setImageDrawable(getResources().getDrawable(currPayMethod == 2 ?
                R.drawable.ic_order_confirm_method_selected : R.drawable.ic_order_confirm_method_unselected));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.flCoinMethod:
                currPayMethod = 0;
                updatePayMethodSelected();
                updateCostInfo();
                if (null != walletInfo && walletInfo.getChargeGoldNum() < coins) {
                    toast(R.string.noble_pay_gold_not_enought);
                }
                break;
            case R.id.flALiMethod:
                currPayMethod = 1;
                updatePayMethodSelected();
                updateCostInfo();
                break;
            case R.id.flWXMethod:
                currPayMethod = 2;
                updatePayMethodSelected();
                updateCostInfo();
                break;
            case R.id.tvBuyNoble:
                if (0 == currPayMethod) {
                    if (null != walletInfo && walletInfo.getChargeGoldNum() < coins) {
                        toast(R.string.noble_pay_gold_not_enought);
                        return;
                    }
                    buyNoble();
                } else {
                    bugNoble(currPayMethod);
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        LogUtils.d(TAG, "onActivityResult-requestCode:" + requestCode + " resultCode:" + resultCode);
        getDialogManager().dismissDialog();
        //支付页面返回处理
        if (requestCode == Pingpp.REQUEST_CODE_PAYMENT) {
            if (data != null && data.getExtras() != null) {
                String result = data.getExtras().getString("pay_result");
                LogUtils.d(TAG, "onActivityResult-result:" + result);
                if (result != null) {

                    /* 处理返回值
                     * "success" - 支付成功
                     * "fail"    - 支付失败
                     * "cancel"  - 取消支付
                     * "invalid" - 支付插件未安装（一般是微信客户端未安装的情况）
                     * "unknown" - app进程异常被杀死(一般是低内存状态下,app进程被杀死)
                     */
                    // 错误信息
                    String errorMsg = data.getExtras().getString("error_msg");
                    String extraMsg = data.getExtras().getString("extra_msg");
                    LogUtils.d(TAG, "onActivityResult-errorMsg:" + errorMsg + "extraMsg:" + extraMsg);
                    if ("success".equals(result)) {
                        //贵族-贵族支付统计
                        Map<String, String> maps = StatisticModel.getInstance().getUMAnalyCommonMap(OrderConfirmActivity.this);
                        maps.put("result", "faild");
                        maps.put("vipId", String.valueOf(vipId));
                        maps.put("payType", currPayMethod == 1 ? "Alipay" : "Wechat");
                        StatisticManager.get().onEvent(OrderConfirmActivity.this,
                                StatisticModel.EVENT_ID_PAYFOR_NOBLE, maps);
                        toast(R.string.pay_success);
                        finish();
                    } else if ("cancel".equals(result)) {
                        toast(R.string.pay_canceled);
                    } else {
                        toast(R.string.pay_failed);
                        //贵族-贵族支付统计
                        Map<String, String> maps = StatisticModel.getInstance().getUMAnalyCommonMap(OrderConfirmActivity.this);
                        maps.put("result", "faild");
                        maps.put("payType", currPayMethod == 1 ? "Alipay" : "Wechat");
                        maps.put("vipId", String.valueOf(vipId));
                        StatisticManager.get().onEvent(OrderConfirmActivity.this,
                                StatisticModel.EVENT_ID_PAYFOR_NOBLE, maps);
                    }
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getDialogManager().dismissDialog();
    }

    @Override
    public void onWalletInfoUpdateFail(String error) {
        getDialogManager().dismissDialog();
        if (!TextUtils.isEmpty(error)) {
            toast(error);
        }
    }

    @Override
    public void onWalletInfoUpdate(WalletInfo walletInfo) {
        this.walletInfo = walletInfo;
        getDialogManager().dismissDialog();
        if (null != walletInfo) {
            tvCoins.setText(getResources().getString(R.string.order_coins_tips, Double.valueOf(walletInfo.getChargeGoldNum()).intValue()));
        }

    }

    @Override
    public void onBuyNoble(boolean isSuccess, JsonObject data, String msg) {
        LogUtils.d(TAG, "onBuyNoble-isSuccess:" + isSuccess + " data:" + data + " msg:" + msg);
        if (!isSuccess) {
            if (!TextUtils.isEmpty(msg)) {
                try {
                    getDialogManager().dismissDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                toast(msg);
                return;
            }
        } else {
            if (null != data) {
                //走的微信|支付宝支付方式
                Pingpp.createPayment(this, data.toString());
            } else {
                //贵族-贵族支付统计
                Map<String, String> maps = StatisticModel.getInstance().getUMAnalyCommonMap(OrderConfirmActivity.this);
                maps.put("result", "success");
                maps.put("vipId", String.valueOf(vipId));
                maps.put("payType", "Coin");
                StatisticManager.get().onEvent(OrderConfirmActivity.this,
                        StatisticModel.EVENT_ID_PAYFOR_NOBLE, maps);
                getDialogManager().dismissDialog();
                toast(R.string.pay_success);
                finish();
            }
        }
    }

    /**
     * 金币购买/开通贵族服务
     */
    private void buyNoble() {
        if (null == walletInfo || walletInfo.getChargeGoldNum() < coins) {
            toast(R.string.noble_pay_gold_not_enought);
            return;
        }
        getDialogManager().showOkCancelDialog(
                getResources().getString(R.string.noble_pay_confirm_tips_coins,
                        String.valueOf(coins), nbLevelName),
                true, new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onOk() {
                        getDialogManager().showProgressDialog(OrderConfirmActivity.this, getResources().getString(R.string.network_loading));
                        getMvpPresenter().buyNoble(vipId, Constants.CHARGE_COINS, type, roomId);
                    }
                });
    }

    /**
     * 充值方式购买/开通贵族服务
     *
     * @param channelType
     */
    private void bugNoble(final int channelType) {
        getDialogManager().showOkCancelDialog(
                getResources().getString(R.string.noble_pay_confirm_tips_money,
                        String.valueOf(coins / 10), nbLevelName),
                true, new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onOk() {
                        String payChannel = null;
                        if (1 == channelType) {
                            payChannel = Constants.CHARGE_ALIPAY;
                        }
                        if (2 == channelType) {
                            payChannel = Constants.CHARGE_WX;
                        }

                        if (!TextUtils.isEmpty(payChannel)) {
                            getDialogManager().showProgressDialog(OrderConfirmActivity.this, getResources().getString(R.string.network_loading));
                            getMvpPresenter().buyNoble(vipId, payChannel, type, roomId);
                        }
                    }
                });
    }
}
