package com.yuhuankj.tmxq.ui.liveroom.nimroom.bean;

/**
 * Created by Administrator on 2018/3/23.
 */

public class ChatSelectBgBean {
    public ChatSelectBgBean() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int vipId;

    public int id;
    public String name;
    public String picUrl;
    public String vipIcon = null;

    public ChatSelectBgBean(int id, String picUrl, String name, int vipId, String vipIcon) {
        this.id = id;
        this.name = name;
        this.picUrl = picUrl;
        this.vipId = vipId;
        this.vipIcon = vipIcon;
    }

    public int getVipId() {
        return vipId;
    }

    public void setVipId(int vipId) {
        this.vipId = vipId;
    }

    public String getVipIcon() {
        return vipIcon;
    }

    public void setVipIcon(String vipIcon) {
        this.vipIcon = vipIcon;
    }
}
