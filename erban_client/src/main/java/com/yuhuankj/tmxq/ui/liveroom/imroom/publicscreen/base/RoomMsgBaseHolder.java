package com.yuhuankj.tmxq.ui.liveroom.imroom.publicscreen.base;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.IdRes;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.widget.messageview.IMessage;

/**
 * 文件描述：房间消息多类型holder基类
 *
 * @auther：zwk
 * @data：2019/1/28
 */
public abstract class RoomMsgBaseHolder<T extends IMessage> extends RecyclerView.ViewHolder {
    private SparseArray<View> mViews;
    private View mConvertView;

    public RoomMsgBaseHolder(View itemView) {
        super(itemView);
        mConvertView = itemView;
        mViews = new SparseArray<>();
    }

    public abstract void onDataTransformView(RoomMsgBaseHolder holder, T message, int position);

    /**
     * 通过viewId获取控件
     *
     * @param viewId
     * @return
     */
    public <T extends View> T getView(int viewId) {
        View view = mViews.get(viewId);
        if (view == null) {
            view = mConvertView.findViewById(viewId);
            mViews.put(viewId, view);
        }
        return (T) view;
    }

    public View getConvertView() {
        return mConvertView;
    }


    /****以下为辅助方法*****/

    /**
     * 设置TextView的值
     *
     * @param viewId
     * @param text
     * @return
     */
    public void setText(int viewId, String text) {
        TextView tv = getView(viewId);
        tv.setText(text);
    }

    public void setImageResource(int viewId, int resId) {
        ImageView view = getView(viewId);
        view.setImageResource(resId);
    }

//    public ItemViewHolder setImageUrl(int viewId, String url) {
//        ImageView view = getView(viewId);
//        ImageLoader.Builder builder = new ImageLoader.Builder();
//        ImageLoader img = builder.url(url)
//                .imgView(view).strategy(ImageLoaderUtil.LOAD_STRATEGY_ONLY_WIFI).build();
//        imageLoaderUtil.loadImage(mContext, img);
//        return this;
//    }

    public void setImageBitmap(int viewId, Bitmap bitmap) {
        ImageView view = getView(viewId);
        view.setImageBitmap(bitmap);
    }

    public void setImageDrawable(int viewId, Drawable drawable) {
        ImageView view = getView(viewId);
        view.setImageDrawable(drawable);
    }

    /**
     * 关于事件的
     */
    public void setOnClickListener(int viewId, View.OnClickListener listener) {
        View view = getView(viewId);
        if (view != null) {
            view.setOnClickListener(listener);
        }
    }

    /**
     * 关于item事件的
     */
    public void setOnItemClickListener(View.OnClickListener listener) {
        if (getItemClickLayoutId() != 0) {
            setOnClickListener(getItemClickLayoutId(), listener);
        }
    }

    public void setOnItemUseClickListener(View.OnClickListener listener) {
        if (getItemUserClickLayoutId() != 0) {
            setOnClickListener(getItemUserClickLayoutId(), listener);
        }
    }

    protected abstract @IdRes int getItemClickLayoutId();

    protected abstract @IdRes
    int getItemUserClickLayoutId();
}
