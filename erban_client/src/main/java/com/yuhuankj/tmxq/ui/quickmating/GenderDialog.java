package com.yuhuankj.tmxq.ui.quickmating;

import android.content.Context;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.BaseAppDialog;

import static com.yuhuankj.tmxq.base.dialog.AppDialogConstant.STYLE_BOTTOM_BACK_DARK;

public class GenderDialog extends BaseAppDialog implements View.OnClickListener {
    private RadioGroup ragGender;
    private TextView tvEnter;
    private OnGenderListener onGenderListener;
    private int gender = 0;

    public GenderDialog(Context context, int gender) {
        super(context);
        this.gender = gender;
    }

    @Override
    public int onDialogStyle() {
        return STYLE_BOTTOM_BACK_DARK;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.layout_quick_mating_sex;
    }

    @Override
    protected void initView() {
        ragGender = findViewById(R.id.ragGender);
        tvEnter = findViewById(R.id.tvEnter);
    }

    @Override
    public void initListener() {
        tvEnter.setOnClickListener(this);
    }

    @Override
    public void initViewState() {
        if (getWindow() != null) {
            getWindow().getAttributes().width = ScreenUtil.getScreenWidth(getContext());
        }
        if (gender == 1) {
            ragGender.check(R.id.rabMan);
        } else if (gender == 2) {
            ragGender.check(R.id.rabWoman);
        } else {
            ragGender.check(R.id.rabNoLimit);
        }
    }

    public void setOnGenderListener(OnGenderListener onGenderListener) {
        this.onGenderListener = onGenderListener;
    }

    @Override
    public void onClick(View v) {
        if (v == tvEnter) {
            if (onGenderListener != null) {
                int checkedId = ragGender.getCheckedRadioButtonId();
                int gender = 0;
                if (checkedId == R.id.rabMan) {
                    gender = 1;
                } else if (checkedId == R.id.rabWoman) {
                    gender = 2;
                }
                onGenderListener.onGender(gender);
            }
            dismiss();
        }
    }

    public interface OnGenderListener {
        void onGender(int gender);
    }
}
