package com.yuhuankj.tmxq.ui.me.wallet.exchange;

import android.text.TextUtils;

import com.netease.nim.uikit.common.util.string.StringUtil;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.pay.IPayCore;
import com.tongdaxing.xchat_core.pay.bean.ExchangeAwardInfoResult;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.me.charge.presenter.PayPresenter;

import java.util.Map;

/**
 * Created by MadisonRong on 09/01/2018.
 */

public class ExchangeGoldPresenter extends PayPresenter<IExchangeGoldView> {

    public void calculateResult(String input) {
        if (null == getMvpView()) {
            return;
        }
        if (!StringUtil.isEmpty(input) && isNumeric(input)) {
            int value = Integer.parseInt(input);
            if (isTenMultiple(value)) {
//                int result = (int) (value * 1.2);
                getMvpView().displayResult(value + "");
            } else {
                getMvpView().displayResult(0 + "");
            }
        } else {
            getMvpView().displayResult(0 + "");
        }
    }

    public void confirmToExchangeGold(String input) {
        confirmToExchangeGold(input, "");
    }

    public void confirmToExchangeGold(String input, String sms) {
        if (StringUtil.isEmpty(input)) {
            getMvpView().toastForError(R.string.exchange_gold_error_empty_input);
            return;
        }

        int value = Integer.parseInt(input);
        if (!isTenMultiple(value)) {
            getMvpView().toastForError(R.string.exchange_gold_error_is_not_ten_multiple);
            return;
        }

        if (walletInfo == null) {
            return;
        }
        if (value > walletInfo.getDiamondNum()) {
            getMvpView().toastForError(R.string.exchange_gold_error_diamond_less);
            return;
        }
        if (TextUtils.isEmpty(sms))
            getMvpView().requestExchangeGold(value);
        else
            getMvpView().requestExchangeGold(value, sms);
    }


    public void exchangeGold(String diamondNum) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        requestParam.put("diamondNum", diamondNum);
        requestParam.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());

        OkHttpManager.getInstance().postRequest(UriProvider.getExchangeGold(), requestParam, new OkHttpManager.MyCallBack<ExchangeAwardInfoResult>() {

            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (getMvpView() != null) {
                    getMvpView().exchangeGoldFail(-1, e.getMessage());
                }
            }

            @Override
            public void onResponse(ExchangeAwardInfoResult data) {
                if (null != data && data.isSuccess() && data.getData() != null) {
                    walletInfo = data.getData();
                    if (getMvpView() != null) {
                        getMvpView().exchangeGold(data.getData());
                        getMvpView().refreshUserWalletBalance(data.getData());
                        getMvpView().showAward(data.getData());
                    }
                    CoreManager.getCore(IPayCore.class).setCurrentWalletInfo(data.getData());
                } else {
                    if (getMvpView() != null && data != null)
                        getMvpView().exchangeGoldFail(data.getCode(), data.getErrorMessage());
                }
            }
        });
    }

    public void exchangeGold(String diamondNum, String sms) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        requestParam.put("diamondNum", diamondNum);
        requestParam.put("smsCode", sms);
        requestParam.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());

        OkHttpManager.getInstance().postRequest(UriProvider.getExchangeGold(), requestParam, new OkHttpManager.MyCallBack<ExchangeAwardInfoResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                getMvpView().exchangeGoldFail(-1, e.getMessage());
            }

            @Override
            public void onResponse(ExchangeAwardInfoResult response) {
                if (null != response && response.isSuccess()) {
                    walletInfo = response.getData();
                    getMvpView().exchangeGold(response.getData());
                    getMvpView().refreshUserWalletBalance(response.getData());
                    getMvpView().showAward(response.getData());
                    CoreManager.getCore(IPayCore.class).setCurrentWalletInfo(response.getData());
                } else {
                    getMvpView().exchangeGoldFail(response.getCode(), response.getErrorMessage());
                }
            }
        });
    }

    private boolean isTenMultiple(int number) {
        int value = number % 10;
        return value == 0;
    }

    private boolean isNumeric(String str) {
        for (int i = str.length(); --i >= 0; ) {
            if (!Character.isDigit(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }
}
