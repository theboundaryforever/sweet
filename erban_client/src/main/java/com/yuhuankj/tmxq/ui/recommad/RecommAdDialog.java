package com.yuhuankj.tmxq.ui.recommad;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.ButtonUtils;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;

import org.json.JSONException;
import org.json.JSONObject;

import static com.tongdaxing.xchat_core.room.bean.RoomInfo.ROOMTYPE_HOME_PARTY;
import static com.tongdaxing.xchat_core.room.bean.RoomInfo.ROOMTYPE_SINGLE_AUDIO;

/**
 * @author chenran
 * @date 2017/10/4
 */

public class RecommAdDialog extends BaseActivity implements View.OnClickListener {

    private final String TAG = RecommAdDialog.class.getSimpleName();

    private ImageView iv_recommAd;

    private String data;

    public static void start(Context context, String data) {
        Intent intent = new Intent(context, RecommAdDialog.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("data", data);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recomm_advert);
        CoreManager.notifyClients(IRecommAdCoreClient.class, IRecommAdCoreClient.METHOD_ON_RECOMM_AD_SHOW_STATUS_CHANGED, true);
        initData();
        initView();
        setListener();
    }

    private void setListener() {
        findViewById(R.id.ll_blankToClose).setOnClickListener(this);
        findViewById(R.id.iv_close).setOnClickListener(this);
        iv_recommAd.setOnClickListener(this);
    }

    private void initView() {
        iv_recommAd = (ImageView) findViewById(R.id.iv_recommAd);
        if (!TextUtils.isEmpty(imgUrl)) {
            GlideApp.with(this)
                    .load(imgUrl)
                    .dontAnimate()
                    .transforms(new CenterCrop(), new RoundedCorners(DisplayUtility.dp2px(this, 15)))
                    .into(iv_recommAd);
        } else {
            CoreManager.notifyClients(IRecommAdCoreClient.class, IRecommAdCoreClient.METHOD_ON_RECOMM_AD_SHOW_STATUS_CHANGED, false);
            finish();
        }
    }

    //调用系统按键返回上一层
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            CoreManager.notifyClients(IRecommAdCoreClient.class, IRecommAdCoreClient.METHOD_ON_RECOMM_AD_SHOW_STATUS_CHANGED, false);
            finish();
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        if (ButtonUtils.isFastDoubleClick(v.getId())) {
            return;
        }
        switch (v.getId()) {
            case R.id.iv_close:
            case R.id.ll_blankToClose:
                break;
            case R.id.iv_recommAd:
                onClickToJump();
                break;
            default:
                break;
        }
        CoreManager.notifyClients(IRecommAdCoreClient.class, IRecommAdCoreClient.METHOD_ON_RECOMM_AD_SHOW_STATUS_CHANGED, false);
        finish();
    }

    private static String url = null;
    private static String imgUrl = null;
    private static String activity_android = null;
    private static JSONObject paramsJSONObject = null;

    //   "{\"imgUrl\":\"https://www.baidu.com/\",
//      \"string\":\"推荐广告提示文本\",
//      \"activity_ios\":\"com.tiantian.mobile.UserInfoActivity\",
//      \"activity_android\":\"com.yuhuankj.tmxq.ui.user.other.UserInfoActivity\",
//      \"params\":\"{
//          \\\"value\\\":999999,\\\"key\\\":\\\"userId\\\"
//       }\",
//      \"url\":\"https://www.baidu.com/\"
//  }"
    private void initData() {
        data = getIntent().getStringExtra("data");
        LogUtils.d(TAG, "initData-data:" + data);
        try {
            if (!TextUtils.isEmpty(data)) {
                JSONObject object = new JSONObject(data);
                if (null != object) {
                    if (object.has("imgUrl")) {
                        imgUrl = object.getString("imgUrl");
                    }
                    if (object.has("url")) {
                        url = object.getString("url");
                    }
                    if (object.has("params")) {
                        paramsJSONObject = new JSONObject(object.getString("params"));
                    }
                    if (object.has("activity_android")) {
                        activity_android = object.getString("activity_android");
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void onClickToJump() {
        LogUtils.d(TAG, "onClickToJump-url:" + url + " activity_android:" + activity_android);
        if (!TextUtils.isEmpty(url)) {
            //跳转h5
            CommonWebViewActivity.start(this, url);
        } else if (!TextUtils.isEmpty(activity_android)) {
            try {
                String key = null;
                if (paramsJSONObject.has("key")) {
                    key = paramsJSONObject.getString("key");
                }
                Object object = null;
                if (paramsJSONObject.has("value")) {
                    object = paramsJSONObject.get("value");
                }
                //跳转房间
                if (activity_android.endsWith("AVRoomActivity") && null != object &&
                        !TextUtils.isEmpty(key) && key.equals("roomUid")) {
                    long roomUid = Long.valueOf((String) object);
                    RoomServiceScheduler.getInstance().enterRoom(this, roomUid, ROOMTYPE_HOME_PARTY);
                } else if (activity_android.endsWith("RoomFrameActivity") && null != object &&
                        !TextUtils.isEmpty(key) && key.equals("roomUid")) {
                    long roomUid = Long.valueOf((String) object);
                    RoomServiceScheduler.getInstance().enterRoom(this, roomUid, ROOMTYPE_SINGLE_AUDIO);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
