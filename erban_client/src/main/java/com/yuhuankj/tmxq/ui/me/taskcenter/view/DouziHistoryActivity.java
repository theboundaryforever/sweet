package com.yuhuankj.tmxq.ui.me.taskcenter.view;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.Constants;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.ui.me.taskcenter.model.DouziHistoryEnitity;
import com.yuhuankj.tmxq.ui.me.taskcenter.model.TaskCenterModel;
import com.yuhuankj.tmxq.ui.me.taskcenter.view.adapter.DouziHistoryAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liaoxy
 * @Description:甜豆记录页面
 * @date 2019/4/12 10:57
 */
public class DouziHistoryActivity extends BaseActivity {
    private List<DouziHistoryEnitity> datas;
    private RecyclerView rcvHistory;
    private BaseQuickAdapter adapter;
    private RelativeLayout rlNoData;
    private int page = Constants.PAGE_START;//当前获取第几页数据
    private boolean isReqing = false;//是否正在请求
    private TextView tvEmplyTip;
    private boolean isAllLoaded = false;//数据全部加载完成

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_red_packet_history);
        initTitleBar("甜豆记录");
        initView();
        initData();
    }

    private void initView() {
        rcvHistory = (RecyclerView) findViewById(R.id.rcvRedPacket);
        rlNoData = (RelativeLayout) findViewById(R.id.rlNoData);
        tvEmplyTip = (TextView) findViewById(R.id.tvEmplyTip);
    }

    private void initData() {
        tvEmplyTip.setText("你还没有甜豆使用记录");
        mTitleBar.setCommonBackgroundColor(Color.WHITE);
        LinearLayoutManager layoutmanager = new RecyclerViewNoBugLinearLayoutManager(this);
        layoutmanager.setOrientation(LinearLayoutManager.VERTICAL);
        rcvHistory.setLayoutManager(layoutmanager);
        datas = new ArrayList<>();

        adapter = new DouziHistoryAdapter(datas);
        rcvHistory.setAdapter(adapter);
        rcvHistory.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                //静止没有滚动
                //public static final int SCROLL_STATE_IDLE = 0;
                //正在被外部拖拽,一般为用户正在用手指滚动
                //public static final int SCROLL_STATE_DRAGGING = 1;
                //自动滚动
                //public static final int SCROLL_STATE_SETTLING = 2;

                //只有滚动条静止的时候才刷新
                if (newState == 0 && !isAllLoaded) {
                    if (!rcvHistory.canScrollVertically(1) && datas.size() > 5) {
                        //滑动到底部
                        getData();
                    }
                }
            }
        });
        getData();
    }

    private void getData() {
        if (isReqing) {
            return;
        }
        isReqing = true;
        DialogManager dialogManager = new DialogManager(this);
        dialogManager.showProgressDialog(this, "请稍后...");
        new TaskCenterModel().getDouziHistory(page, Constants.PAGE_SIZE, new OkHttpManager.MyCallBack<ServiceResult<List<DouziHistoryEnitity>>>() {
            @Override
            public void onError(Exception e) {
                dialogManager.dismissDialog();
                SingleToastUtil.showToast(e.getMessage());
                isReqing = false;
            }

            @Override
            public void onResponse(ServiceResult<List<DouziHistoryEnitity>> response) {
                dialogManager.dismissDialog();
                if (response.isSuccess()) {
                    List<DouziHistoryEnitity> temps = response.getData();
                    if (temps != null && temps.size() > 0) {
                        if (page == Constants.PAGE_START) {
                            datas.clear();
                        }
                        datas.addAll(temps);
                        adapter.notifyDataSetChanged();
                        page++;
                    } else {
                        if (datas.size() == 0) {
                            rlNoData.setVisibility(View.VISIBLE);
                        } else {
                            SingleToastUtil.showToast("数据已全部加载完成");
                            isAllLoaded = true;
                        }
                    }
                } else {
                    SingleToastUtil.showToast(DouziHistoryActivity.this, response.getMessage());
                }
                isReqing = false;
            }
        });
    }
}
