package com.yuhuankj.tmxq.ui.liveroom.imroom.room.base;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/6/24
 */
public interface IMRoomConstant {
    int REQUEST_CODE = 200;
    int ROOM_INVITE_RESULT_CODE = 100;
}
