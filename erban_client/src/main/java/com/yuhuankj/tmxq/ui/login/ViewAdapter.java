package com.yuhuankj.tmxq.ui.login;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * @Description: ViewPager的通用适配器
 * @author liaoxy
 * @date 2016-5-27 下午3:02:12
 */
public class ViewAdapter extends PagerAdapter {

	private ArrayList<View> views; // 界面列表

	public ViewAdapter(ArrayList<View> views) {
		this.views = views;
	}

	@Override
	public int getCount() {
		if (views != null) {
			return views.size();
		}
		return 0;
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		container.addView(views.get(position), 0);
		return views.get(position);
	}

	// 判断是否由对象生成界面
	@Override
	public boolean isViewFromObject(View view, Object arg1) {
		return (view == arg1);
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView(views.get(position));
	}

}