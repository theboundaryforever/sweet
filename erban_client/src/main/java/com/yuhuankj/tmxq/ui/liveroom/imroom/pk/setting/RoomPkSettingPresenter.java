package com.yuhuankj.tmxq.ui.liveroom.imroom.pk.setting;

import android.text.TextUtils;
import android.util.SparseArray;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomQueueInfo;
import com.tongdaxing.xchat_core.pk.bean.PkVoteInfo;
import com.yuhuankj.tmxq.ui.liveroom.imroom.pk.RoomPKModel;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 文件描述：新的IM多人PK设置页面P层
 *
 * @auther：zwk
 * @data：2019/7/8
 */
public class RoomPkSettingPresenter extends AbstractMvpPresenter<IRoomPkSettingView> {
    private RoomPKModel roomPkModel;

    public RoomPkSettingPresenter() {
        roomPkModel = new RoomPKModel();
    }

    /**
     * PK类型
     *
     * @return
     */
    public Map<String, Integer> getPkType() {
        Map<String, Integer> jsonsType = new LinkedHashMap<>();
        jsonsType.put("按投票人数PK(1人=1pk值）", 1);
        jsonsType.put("按礼物价值PK(1金币=1pk值）", 2);
        return jsonsType;
    }


    /**
     * 获取PK时间列表
     *
     * @return
     */
    public Map<String, Integer> getPkTime() {
        Map<String, Integer> jsonsTime = new LinkedHashMap<>();
        jsonsTime.put("30秒", 30);
        jsonsTime.put("60秒", 60);
        jsonsTime.put("90秒", 90);
        jsonsTime.put("180秒", 180);
        jsonsTime.put("5分钟", 300);
        jsonsTime.put("10分钟", 600);
        jsonsTime.put("20分钟", 1200);
        return jsonsTime;
    }

    /**
     * 获取可以参与PK的麦位上的人信息
     */
    public void getRoomPKMemberListInfo() {
        if (RoomDataManager.get().mMicQueueMemberMap != null && RoomDataManager.get().mMicQueueMemberMap.size() > 0) {
            SparseArray<IMRoomQueueInfo> imMicQueue = RoomDataManager.get().mMicQueueMemberMap.clone();
            List<IMRoomQueueInfo> data = new ArrayList<>();
            for (int i = 0; i < imMicQueue.size(); i++) {
                IMRoomQueueInfo roomQueueInfo = imMicQueue.get(i);
                if (roomQueueInfo != null) {
                    //默认全选
                    roomQueueInfo.setSelect(true);
                    data.add(roomQueueInfo);
                }
            }
            if (getMvpView() != null) {
                getMvpView().showPKMemberListView(data);
            }
        } else {
            if (getMvpView() != null) {
                getMvpView().showPKMemberListView(null);
            }
        }
    }


    /**
     * 发起新的PK
     *
     * @param pkType
     * @param pkTime
     * @param pkTitle
     * @param members
     */
    public void saveRoomPkInfo(int pkType, int pkTime, String pkTitle, List<IMRoomQueueInfo> members) {
        if (members == null) {
            if (getMvpView() != null) {
                getMvpView().savePkError("麦上暂时没有可以选择的PK成员〜");
            }
            return;
        }
        StringBuilder sb = new StringBuilder();
        List<PkVoteInfo.PkUser> selMembers = new ArrayList<>();
        for (IMRoomQueueInfo member : members) {
            if (member != null && member.isSelect && member.mChatRoomMember != null) {
                PkVoteInfo.PkUser pkUser = new PkVoteInfo.PkUser();
                pkUser.setAvatar(member.mChatRoomMember.getAvatar());
                pkUser.setNick(member.mChatRoomMember.getNick());
                pkUser.setUid(member.mChatRoomMember.getAccount());
                selMembers.add(pkUser);
                String targetUid = member.mChatRoomMember.getAccount();
                sb.append(targetUid).append(",");
            }
        }
        if (selMembers.size() < 2) {
            if (getMvpView() != null) {
                getMvpView().savePkError("请选择PK成员〜");
            }
            return;
        }
        sb.deleteCharAt(sb.length() - 1);
        pkTitle = pkTitle.replaceAll(" ", "");
        if (TextUtils.isEmpty(pkTitle)) {
            pkTitle = "快来参与投票吧〜";
        }
        PkVoteInfo info = new PkVoteInfo();
        //旧版本PK所需参数
        info.setOpUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        info.setUid(Long.valueOf(selMembers.get(0).getUid()));
        info.setNick(selMembers.get(0).getNick());
        info.setAvatar(selMembers.get(0).getAvatar());
        info.setPkUid(Long.valueOf(selMembers.get(1).getUid()));
        info.setPkNick(selMembers.get(1).getNick());
        info.setPkAvatar(selMembers.get(1).getAvatar());
        //新的PK
        info.setTitle(pkTitle);
        info.setPkType(pkType);
        info.setDuration(pkTime);
        info.setPkList(selMembers);
        info.setTargetUidList(sb.toString());
        info.setExpireSeconds(pkTime);
        roomPkModel.savePk(RoomDataManager.get().getCurrentRoomInfo() == null ? 0 : RoomDataManager.get().getCurrentRoomInfo().getRoomId(), info, new CallBack<PkVoteInfo>() {
            @Override
            public void onSuccess(PkVoteInfo data) {
                if (getMvpView() != null) {
                    getMvpView().savePKSucView(data);
                }
            }

            @Override
            public void onFail(int code, String error) {
                if (getMvpView() != null) {
                    getMvpView().savePkError(error);
                }
            }
        });
//            CoreManager.getCore(IPkCore.class).savePK(AvRoomDataManager.get().mCurrentRoomInfo == null ? 0 : AvRoomDataManager.get().mCurrentRoomInfo.getRoomId(), info);
    }
}
