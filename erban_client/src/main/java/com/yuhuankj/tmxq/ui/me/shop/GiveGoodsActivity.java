package com.yuhuankj.tmxq.ui.me.shop;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.base.fragment.BaseListFragment;
import com.yuhuankj.tmxq.ui.room.adapter.CommonMagicIndicatorAdapter;
import com.yuhuankj.tmxq.widget.TitleBar;
import com.yuhuankj.tmxq.widget.magicindicator.MagicIndicator;
import com.yuhuankj.tmxq.widget.magicindicator.ViewPagerHelper;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.UIUtil;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.commonnavigator.CommonNavigator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GiveGoodsActivity extends BaseActivity {

    public static final int CAR = 0;
    public static final int HEADWEAR = 1;
    @BindView(R.id.title_bar)
    TitleBar titleBar;
    @BindView(R.id.give_goods_indicator)
    MagicIndicator giveGoodsIndicator;
    @BindView(R.id.vp_give_goods)
    ViewPager vpGiveGoods;
    private ArrayList<Fragment> fragments;
    private String carName;
    private String goodsId;
    private int type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_give_goods);
        ButterKnife.bind(this);
        carName = getIntent().getStringExtra("carName");
        goodsId = getIntent().getStringExtra("goodsId");
        type = getIntent().getIntExtra("type", 0);


        initTitleBar("赠送");
        ButterKnife.bind(this);

        fragments = new ArrayList<>();

        BaseListFragment baseListFragment = new BaseListFragment();
        GiveGoodsAdapter shareFansAdapter = new GiveGoodsAdapter(new ArrayList<>());
        shareFansAdapter.itemAction = new GiveGoodsAdapter.ItemAction() {
            @Override
            public void itemClickAction(String uid, String userName) {
                showEnsureDialog(uid, userName);
            }
        };
        baseListFragment.setEmptyStr("没有关注的用户");
        baseListFragment.setPageNoParmasName("pageNo");
        baseListFragment.setShortUrl(UriProvider.getAllFans());
        baseListFragment.setAdapter(shareFansAdapter);
        baseListFragment.setDataFilter(new BaseListFragment.IDataFilter() {
            @Override
            public List<Json> dataFilter(Json json) {
                return json.jlist("data");
            }
        });


        FriendListGiftFragment friendListGiftFragment = new FriendListGiftFragment();
        friendListGiftFragment.iGiveAction = new FriendListGiftAdapter.IGiveAction() {
            @Override
            public void onGiveEvent(String uid, String userName) {
                showEnsureDialog(uid, userName);
            }
        };
        fragments.add(friendListGiftFragment);
        fragments.add(baseListFragment);
        FragmentPagerAdapter fragmentPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public int getCount() {
                return fragments.size();
            }

            @Override
            public Fragment getItem(int position) {
                return fragments.get(position);
            }
        };


        List<TabInfo> mTabInfoList = new ArrayList<>();
        mTabInfoList.add(new TabInfo(1, "好友"));
        mTabInfoList.add(new TabInfo(1, "关注"));
        CommonNavigator commonNavigator = new CommonNavigator(this);
        CommonMagicIndicatorAdapter magicIndicatorAdapter = new CommonMagicIndicatorAdapter(this,
                mTabInfoList, UIUtil.dip2px(this, 4));
        magicIndicatorAdapter.setOnItemSelectListener(new CommonMagicIndicatorAdapter.OnItemSelectListener() {
            @Override
            public void onItemSelect(int position) {
                vpGiveGoods.setCurrentItem(position);
            }
        });
        commonNavigator.setAdapter(magicIndicatorAdapter);
        commonNavigator.setAdjustMode(true);

        giveGoodsIndicator.setNavigator(commonNavigator);
        vpGiveGoods.setAdapter(fragmentPagerAdapter);
        ViewPagerHelper.bind(giveGoodsIndicator, vpGiveGoods);
    }


    private void showEnsureDialog(String uid, String userName) {
        getDialogManager().showOkCancelDialog(getResources().getString(R.string.bug_then_give_sure_tips, carName, userName), true,
                new DialogManager.AbsOkDialogListener() {
                    @Override
                    public void onOk() {
                        requestGift(uid);
                    }
                });
    }

    private void requestGift(String uid) {

        if (TextUtils.isEmpty(uid) || TextUtils.isEmpty(goodsId)) {
            toast(getResources().getString(R.string.params_error));
            return;
        }

        Map<String, String> param = OkHttpManager.getDefaultParam();
        param.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        param.put("targetUid", uid);

        String shortUrl;
        if (type == CAR) {
            shortUrl = UriProvider.getCarGiveUrl();
            param.put("carId", goodsId);
        } else {
            shortUrl = UriProvider.getHeadWearGiveUrl();
            param.put("headwearId", goodsId);
        }

        OkHttpManager.getInstance().postRequest(shortUrl, param, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                toast(getResources().getString(R.string.network_error));
            }

            @Override
            public void onResponse(Json json) {
                if (json.num("code") == 200) {
                    toast(getResources().getString(R.string.give_success));
                    finish();
                } else {
                    try {
                        String msg = json.getString("message");
                        if (!TextUtils.isEmpty(msg)) {
                            toast(msg);
                            return;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    toast("操作失败!");
                }
            }
        });
    }

}
