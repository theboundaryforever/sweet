package com.yuhuankj.tmxq.ui.room.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jude.rollviewpager.adapter.StaticPagerAdapter;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.ButtonUtils;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;
import com.yuhuankj.tmxq.ui.webview.VoiceAuthCardWebViewActivity;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 * @date 2017/8/7
 */

public class BannerAdapter extends StaticPagerAdapter {

    private final String TAG = BannerAdapter.class.getSimpleName();

    private Context context;
    private List<Json> bannerInfoList;
    private LayoutInflater mInflater;

    private int screenWidth = 0;
    private int bannerHeight = 0;

    public BannerAdapter(List<Json> homeItem, Context context) {
        this.context = context;
        this.bannerInfoList = homeItem;
        mInflater = LayoutInflater.from(context);
        screenWidth = DisplayUtility.getScreenWidth(context);
        bannerHeight = screenWidth / 3;
    }

    @Override
    public View getView(ViewGroup container, int position) {
        final Json bannerInfo = bannerInfoList.get(position);
        ImageView imgBanner = (ImageView) mInflater.inflate(R.layout.banner_page_item, container, false);
        ViewGroup.LayoutParams lp = imgBanner.getLayoutParams();
        lp.height = bannerHeight;
        lp.width = screenWidth;

        if(GlideContextCheckUtil.checkContextUsable(context)){
            ImageLoadUtils.loadBannerRoundBg(context,
                    ImageLoadUtils.toThumbnailUrl(screenWidth,bannerHeight,bannerInfo.str("bannerPic")), imgBanner);
        }
        imgBanner.setTag(position);
        imgBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick(v.getId())) {
                    return;
                }
                Integer posit = (Integer) v.getTag();
                if (null != posit) {
                    LogUtils.d(TAG, "onClick-posit:" + posit);
                    StatisticManager.get().onEvent(context,
                            StatisticModel.getInstance().getMainHotBannerClickEventId(position),
                            StatisticModel.getInstance().getUMAnalyCommonMap(context));
                }
                if (bannerInfo.num("skipType") == 3) {
                    if (bannerInfo.str("skipUri").endsWith(UriProvider.getRecordAuthCardVoiceBannerUrl())) {
                        //房间页-banner-声鉴卡
                        Map<String, String> maps = StatisticModel.getInstance().getUMAnalyCommonMap(context);
                        StatisticManager.get().onEvent(context, StatisticModel.EVENT_ID_HOT_BANNER_VOICEAUTHCARD, maps);

                        VoiceAuthCardWebViewActivity.start(context, false);
                    } else {
                        CommonWebViewActivity.start(context, bannerInfo.str("skipUri"));
                    }
                } else if (bannerInfo.num("skipType") == 2) {
                    int roomType = RoomInfo.ROOMTYPE_HOME_PARTY;
                    if (bannerInfo.has("roomType")) {
                        roomType = bannerInfo.num("roomType");
                    }
                    RoomServiceScheduler.getInstance().enterRoom(context,
                            bannerInfo.num_l("skipUri"), roomType);
                }
            }
        });
        return imgBanner;
    }

    @Override
    public int getCount() {
        if (bannerInfoList == null) {
            return 0;
        } else {
            return bannerInfoList.size();
        }
    }

    private View.OnClickListener onBannerItemClickListener;

    public void setOnBannerItemClickListener(View.OnClickListener onBannerItemClickListener) {
        this.onBannerItemClickListener = onBannerItemClickListener;
    }

}
