package com.yuhuankj.tmxq.ui.room.interfaces;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.utils.json.Json;

import java.util.List;

public interface FollowView<T extends AbstractMvpPresenter> extends IMvpBaseView {

    /**
     * 获取关注房间列表
     *
     * @param isSuccess
     * @param data
     */
    void onGetAttentionRoomList(boolean isSuccess, List<Json> data);

    /**
     * 获取推荐房间列表
     *
     * @param isSuccess
     * @param data
     */
    void onGetOppositeRoom(boolean isSuccess, List<Json> data);

    /**
     * 获取关注用户列表
     *
     * @param isSuccess
     * @param data
     */
    void onGetFollowUserList(boolean isSuccess, List<Json> data);

    /**
     * 获取我的足迹列表
     *
     * @param isSuccess
     * @param data
     */
    void onMyFooprint(boolean isSuccess, List<Json> data);
}
