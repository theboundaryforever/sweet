package com.yuhuankj.tmxq.ui.liveroom.nimroom.checkmicro;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;

import java.util.Map;

public class CheckMicroQueuePresenter extends AbstractMvpPresenter<CheckMicroQueueView> {

    private final String TAG = CheckMicroQueuePresenter.class.getSimpleName();

    public CheckMicroQueuePresenter() {

    }


    public void requestCheckMicroQueue(long roomUid, int roomType) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("roomUid", roomUid + "");
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid()+"");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomType", roomType + "");
        OkHttpManager.getInstance().postRequest(UriProvider.checkMicroQueueUrl(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                getMvpView().onRequestCheckMicroQueue(false,e.getMessage());
            }

            @Override
            public void onResponse(Json json) {
                getMvpView().onRequestCheckMicroQueue(200 == json.num("code"),json.str("data"));
            }
        });

    }

    public void requestCleanMicroQueue(long roomUid, int roomType) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("roomUid", roomUid + "");
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid()+"");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomType", roomType + "");
        OkHttpManager.getInstance().postRequest(UriProvider.handleMicroQueueUrl(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                getMvpView().onRequestCleanMicroQueue(false,e.getMessage());
            }

            @Override
            public void onResponse(Json json) {
                getMvpView().onRequestCleanMicroQueue(200 == json.num("code"),json.str("message"));
            }
        });

    }

}
