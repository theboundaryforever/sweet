package com.yuhuankj.tmxq.ui.home.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.ethanhua.skeleton.Skeleton;
import com.ethanhua.skeleton.SkeletonScreen;
import com.jude.rollviewpager.hintview.ColorPointHintView;
import com.tencent.bugly.crashreport.BuglyLog;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.AccountInfo;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.im.custom.bean.CallUpBean;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomModel;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.GameRoomEnitity;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.SingleAudioRoomEnitity;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.fragment.BaseMvpFragment;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.thirdsdk.nim.MsgCenterRedPointStatusManager;
import com.yuhuankj.tmxq.ui.home.game.GameBannerEnitity;
import com.yuhuankj.tmxq.ui.home.game.GameHomeLiveRoomAdapter;
import com.yuhuankj.tmxq.ui.home.model.NewChannelHomeResponse;
import com.yuhuankj.tmxq.ui.home.presenter.NewChannelHomePresenter;
import com.yuhuankj.tmxq.ui.home.view.INewChannelHomeView;
import com.yuhuankj.tmxq.ui.home.view.adapter.HomeSingleAudioRoomAdapter;
import com.yuhuankj.tmxq.ui.home.view.adapter.OppoHomeBannerAdapter;
import com.yuhuankj.tmxq.ui.home.view.widget.NewChannelHomeBannerShapeHintView;
import com.yuhuankj.tmxq.ui.home.view.widget.NewChannelHomeTopView;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.listener.AnimationListener;
import com.yuhuankj.tmxq.ui.me.taskcenter.view.TaskCenterActivity;
import com.yuhuankj.tmxq.ui.message.UnreadMsgResult;
import com.yuhuankj.tmxq.ui.ranklist.RankListActivity;
import com.yuhuankj.tmxq.ui.search.SearchActivity;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;
import com.yuhuankj.tmxq.ui.webview.VoiceAuthCardWebViewActivity;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.widget.Banner;

import java.util.ArrayList;
import java.util.List;

/**
 * @author weihaitao
 * @date 2019/5/21
 */
@CreatePresenter(NewChannelHomePresenter.class)
public class NewChannelHomeFragment extends BaseMvpFragment<INewChannelHomeView, NewChannelHomePresenter> implements INewChannelHomeView {

    public String TAG = NewChannelHomeFragment.class.getSimpleName();

    private NewChannelHomeTopView ohtvTopNav;
    private Banner bnContent;
    private OppoHomeBannerAdapter oppoHomeBannerAdapter;
    private RecyclerView rvSingleRoom;
    private TextView tvSingleRoomTips;
    private HomeSingleAudioRoomAdapter homeSingleAudioRoomAdapter;
    private GameHomeLiveRoomAdapter roomAdapter;
    private RecyclerView rcvOppoRoom;
    private SwipeRefreshLayout swipeRefresh;
    private View vHeader;

    private boolean isHideLoad = false;//是否已经隐藏了加载布局
    private SkeletonScreen skeletonScreen;//加载状态view

    private boolean isRequesting = false;
    private int nextPageNum = Constants.PAGE_START;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_oppo_game_home;
    }

    @Override
    public void onFindViews() {
        ohtvTopNav = mView.findViewById(R.id.ohtvTopNav);
        swipeRefresh = mView.findViewById(R.id.swipe_refresh);
        swipeRefresh.setEnabled(false);
        vHeader = LayoutInflater.from(getActivity()).inflate(R.layout.layout_oppo_game_home_head, null);
        bnContent = vHeader.findViewById(R.id.bnContent);
        initBanner();
        rvSingleRoom = vHeader.findViewById(R.id.rvSingleRoom);
        tvSingleRoomTips = vHeader.findViewById(R.id.tvSingleRoomTips);
        homeSingleAudioRoomAdapter = new HomeSingleAudioRoomAdapter(getContext(),new ArrayList<>());
        rvSingleRoom.setLayoutManager(new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false));
        rvSingleRoom.setAdapter(homeSingleAudioRoomAdapter);

        roomAdapter = new GameHomeLiveRoomAdapter(new ArrayList<>());
        roomAdapter.addHeaderView(vHeader);
        rcvOppoRoom = mView.findViewById(R.id.rcvOppoRoom);
        rcvOppoRoom.setLayoutManager(new RecyclerViewNoBugLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rcvOppoRoom.setAdapter(roomAdapter);
        skeletonScreen = Skeleton.bind(rcvOppoRoom)
                .adapter(roomAdapter)
                .load(R.layout.fragment_game_home_oppo_loading)
                .show();
    }

    /**
     * 隐藏Skeleton
     */
    private void hideSkeleton() {
        try {
            if (!isHideLoad) {
                skeletonScreen.hide();
            }
            isHideLoad = true;
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    /**
     * 重置banner布局参数，高宽等比例 按照设计稿的比例是346:90
     */
    private void initBanner() {
        int screenWidth = DisplayUtility.getScreenWidth(getActivity());
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) bnContent.getLayoutParams();
        lp.width = screenWidth - DisplayUtility.dp2px(getActivity(), 30);
        lp.height = lp.width * 90 / 346;
        lp.gravity = Gravity.CENTER_HORIZONTAL;
        bnContent.setLayoutParams(lp);

        oppoHomeBannerAdapter = new OppoHomeBannerAdapter(null, getActivity());
        bnContent.setAdapter(oppoHomeBannerAdapter);
    }

    //召集令定时器
    private CountDownTimer timer;

    @Override
    public void initiate() {
        LogUtils.d(TAG, "initiate-->loadData");
        loadData();
    }

    @Override
    public void loadLazyIfYouWant() {
        super.loadLazyIfYouWant();
        LogUtils.d(TAG, "loadLazyIfYouWant-->loadData");
        loadData();
    }

    @Override
    public void onSetListener() {

        ohtvTopNav.setHomeTopClickListener(new NewChannelHomeTopView.OppoHomeTopClickListener() {
            @Override
            public void onLeftClick() {
                startActivity(new Intent(getActivity(), RankListActivity.class));
            }

            @Override
            public void onSearchClick() {
                SearchActivity.start(getActivity());
            }

            @Override
            public void onRightClick() {
                startActivity(new Intent(getContext(), TaskCenterActivity.class));

                if (null != getActivity()) {
                    StatisticManager.get().onEvent(getActivity(), StatisticModel.EVENT_ID_NEW_HOME_GOTO_TASK_CENTER,
                            StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
                }
            }
        });

        oppoHomeBannerAdapter.setOnGameBannerClickListener(new OppoHomeBannerAdapter.OnGameBannerClickListener() {
            @Override
            public void onGameBannerClicked(GameBannerEnitity bannerEnitity) {
                if (bannerEnitity.getSkipType() == 3) {
                    if (bannerEnitity.getSkipUri().endsWith(UriProvider.getRecordAuthCardVoiceBannerUrl())) {
                        VoiceAuthCardWebViewActivity.start(getActivity(), false);
                    } else {
                        CommonWebViewActivity.start(getActivity(), bannerEnitity.getSkipUri());
                    }
                } else if (bannerEnitity.getSkipType() == 2) {
                    try {
                        RoomServiceScheduler.getInstance().enterRoom(getActivity(),
                                Long.valueOf(bannerEnitity.getSkipUri()), bannerEnitity.getRoomType());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        homeSingleAudioRoomAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                List<SingleAudioRoomEnitity> datas = adapter.getData();
                if (null != datas && datas.size() > position) {
                    SingleAudioRoomEnitity enitity = datas.get(position);
                    RoomServiceScheduler.getInstance().enterRoom(getActivity(), enitity.getUid(), enitity.getType());

                    //首页-直播间列表-点击事件
                    StatisticManager.get().onEvent(getActivity(),
                            StatisticModel.EVENT_ID_HOME_LIST_LIVEROOM_CLICK,
                            StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
                }
            }
        });
//        homeSingleAudioRoomAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
//            @Override
//            public void onLoadMoreRequested() {
//                getMvpPresenter().getOpscHomeData(nextPageNum, Constants.PAGE_SIZE_20);
//            }
//        },rvSingleRoom);

        roomAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                if (roomAdapter.getData().size() > position && null != getActivity()) {
                    GameRoomEnitity gameRoomEnitity = roomAdapter.getData().get(position);
                    RoomServiceScheduler.getInstance().enterRoom(getActivity(),
                            gameRoomEnitity.getUid(), gameRoomEnitity.getType());

                    //首页-聊天室列表-点击事件
                    StatisticManager.get().onEvent(getActivity(),
                            StatisticModel.EVENT_ID_HOME_LIST_CHATROOM_CLICK,
                            StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
                }
            }
        });

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                if (isRequesting) {
                    stopRefresh();
                } else {
                    loadData();
                }
            }
        });

        roomAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {

            @Override
            public void onLoadMoreRequested() {
                isRequesting = true;
                getMvpPresenter().getOpscHomeData(nextPageNum + 1, Constants.PAGE_SIZE_20);
            }
        });
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogin(AccountInfo accountInfo) {
        LogUtils.d(TAG, "onLogin-->loadData");
        if (null != accountInfo && accountInfo.getUid() > 0L) {
            loadData();
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo userInfo) {
        LogUtils.d(TAG, "onCurrentUserInfoUpdate-->loadData");
        loadData();
    }

    /**
     * 停止刷新,并在首次懒加载刷新结束时，判断是否需要展示新手引导页
     */
    private void stopRefresh() {
        swipeRefresh.setRefreshing(false);
    }

    private void loadData() {
        LogUtils.d(TAG, "loadData-isRequesting:" + isRequesting);
        if (!isRequesting) {
            isRequesting = true;
            nextPageNum = Constants.PAGE_START;
            getMvpPresenter().getOpscHomeData(nextPageNum, Constants.PAGE_SIZE_20);
            getMvpPresenter().getUnreadMsgCount(new OkHttpManager.MyCallBack<ServiceResult<UnreadMsgResult>>() {

                @Override
                public void onError(Exception e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(ServiceResult<UnreadMsgResult> response) {
                    if (response != null && response.isSuccess() && null != response.getData()) {
                        UnreadMsgResult unreadMsgResult = response.getData();
                        MsgCenterRedPointStatusManager.getInstance().setShowSignInRedPoint(unreadMsgResult.getSignCount() > 0);
                    }
                }
            });
        }
    }

    /**
     * 显示首页接口错误状态
     *
     * @param message
     */
    @Override
    public void showOpscHomeDataErr(String message) {
        LogUtils.d(TAG, "showOpscHomeDataErr-message:" + message);
        swipeRefresh.setEnabled(false);
        isRequesting = false;
        stopRefresh();
        hideSkeleton();
        //显示网络出错状态 --> StatusLayout
        showNetworkErr();
    }

    /**
     * 底部聊天室列表添加新数据,对应上拉加载更多操作,如果上拉调用接口出错，无需提示用户，因此也直接采用这个回调
     *
     * @param canLoadMore 是否可继续上拉加载更多
     * @param listRoom    数据列表
     */
    @Override
    public void showNewRoomList(boolean canLoadMore, List<GameRoomEnitity> listRoom) {
        isRequesting = false;
        if (canLoadMore) {
            //仍旧可以上拉加载更多
            nextPageNum += 1;
            if (null != roomAdapter) {
                roomAdapter.loadMoreComplete();
                if (null != listRoom) {
                    roomAdapter.addData(listRoom);
                }
            }
        } else {
            //不能再加载更多，可能是接口报错，可能是数据量不够
            if (null != roomAdapter) {
                if (null != listRoom) {
                    roomAdapter.loadMoreEnd();
                    roomAdapter.addData(listRoom);
                } else {
                    roomAdapter.loadMoreFail();
                }
            }
        }

    }

    private void setBannerData(List<GameBannerEnitity> list) {
        BuglyLog.d(TAG, "setBannerData");
        oppoHomeBannerAdapter.setBannerInfoList(list);
        oppoHomeBannerAdapter.notifyDataSetChanged();
        if (list.size() > 1) {
            bnContent.setHintView(new NewChannelHomeBannerShapeHintView(getActivity()));
            bnContent.setPlayDelay(3000);
            bnContent.setAnimationDurtion(500);
        } else {
            bnContent.setHintView(new ColorPointHintView(getContext(), Color.TRANSPARENT, Color.TRANSPARENT));
        }
    }

    /**
     * 刷新opsc市场首页数据,对应下拉刷新操作
     *
     * @param response
     */
    @Override
    public void refreshOpscHomeData(NewChannelHomeResponse response) {
        LogUtils.d(TAG, "refreshOpscHomeData");
        setBannerData(response.getBanners());
        homeSingleAudioRoomAdapter.replaceData(response.getHotRooms());
        //直播间列表数据为空的情况先，隐藏对应模块
        if (null == response.getHotRooms() || response.getHotRooms().size() == 0) {
            tvSingleRoomTips.setVisibility(View.GONE);
            rvSingleRoom.setVisibility(View.GONE);
        } else {
            tvSingleRoomTips.setVisibility(View.VISIBLE);
            rvSingleRoom.setVisibility(View.VISIBLE);
        }

        roomAdapter.replaceData(response.getListRoom());
        roomAdapter.loadMoreComplete();
        swipeRefresh.setEnabled(true);
        isRequesting = false;
        stopRefresh();
        hideSkeleton();
    }

    private Runnable reportInRoomByCallUpRunnable = null;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        if (null != reportInRoomByCallUpRunnable) {
            swipeRefresh.removeCallbacks(reportInRoomByCallUpRunnable);
            reportInRoomByCallUpRunnable = null;
        }
    }

    //显示召集令
    public void showCallUpDlg(CallUpBean callUpBean) {
        if (callUpBean == null) {
            return;
        }
        //1.0.3.0版本开始，召集令需求变更为对所有渠道包都可见
//        String channelId = ChannelUtil.getChannel(getContext());
//        if (!TextUtils.isEmpty(channelId) && !channelId.endsWith("_new")) {
//            return;
//        }

        LinearLayout llCallUp = getView().findViewById(R.id.llCallUp);
        ImageView imvAvatar = llCallUp.findViewById(R.id.imvAvatar);
        TextView tvName = llCallUp.findViewById(R.id.tvName);
        TextView tvContent = llCallUp.findViewById(R.id.tvContent);
        TextView tvAgree = llCallUp.findViewById(R.id.tvAgree);
        TextView tvRefuse = llCallUp.findViewById(R.id.tvRefuse);
        llCallUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        ImageLoadUtils.loadCircleImage(imvAvatar.getContext(), callUpBean.getAvatar(), imvAvatar, R.drawable.ic_default_avatar);
        tvName.setText(callUpBean.getTitle());
        tvContent.setText(callUpBean.getMessage());

        tvAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideCallUp(llCallUp);
                LogUtils.d(TAG, "showCallUpDlg-->enterRoom uid:" + callUpBean.getRoomUid() + " type:" + callUpBean.getType());
                if (null != getContext()) {
                    RoomServiceScheduler.getInstance().enterRoom(getContext(), callUpBean.getRoomUid(), callUpBean.getType());

                    //召集令进房统计,延迟1000毫秒统计上报
                    if (null != reportInRoomByCallUpRunnable) {
                        swipeRefresh.removeCallbacks(reportInRoomByCallUpRunnable);
                        reportInRoomByCallUpRunnable = null;
                    }
                    reportInRoomByCallUpRunnable = new Runnable() {
                        @Override
                        public void run() {
                            new IMRoomModel().inRoomFromConvene(callUpBean.getRoomId(), new HttpRequestCallBack<String>() {
                                @Override
                                public void onSuccess(String message, String response) {
                                    LogUtils.d(TAG, "showCallUpDlg-->inRoomFromConvene-->onSuccess message:" + message);
                                }

                                @Override
                                public void onFailure(int code, String msg) {
                                    LogUtils.d(TAG, "showCallUpDlg-->inRoomFromConvene-->onFailure msg:" + msg + " code:" + code);
                                }
                            });
                        }
                    };
                    swipeRefresh.postDelayed(reportInRoomByCallUpRunnable, 1000L);
                }

                if (null != getActivity()) {
                    //首页-召集令-接受
                    StatisticManager.get().onEvent(getActivity(),
                            StatisticModel.EVENT_ID_HOME_CALL_UP_INVITE_ACCEPT,
                            StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
                }
            }
        });

        tvRefuse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideCallUp(llCallUp);

                if (null != getActivity()) {
                    //首页-召集令-拒绝
                    StatisticManager.get().onEvent(getActivity(),
                            StatisticModel.EVENT_ID_HOME_CALL_UP_INVITE_REFUSE,
                            StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
                }
            }
        });

        ImageView ivGender = llCallUp.findViewById(R.id.ivGender);
        ivGender.setImageDrawable(mContext.getResources().getDrawable(callUpBean.getGender() == 1 ? R.drawable.icon_man : R.drawable.icon_women));

        showCallUp(llCallUp, tvRefuse);
    }

    //显示召集令
    private void showCallUp(LinearLayout llCallUp, TextView tvRefuse) {
        TranslateAnimation showAnima = new TranslateAnimation(Animation.RELATIVE_TO_SELF
                , 0, Animation.RELATIVE_TO_SELF, 0
                , Animation.RELATIVE_TO_SELF, -1f
                , Animation.RELATIVE_TO_SELF, 0);
        showAnima.setDuration(1000);
        showAnima.setAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                super.onAnimationStart(animation);
                llCallUp.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                super.onAnimationEnd(animation);
                if (timer != null) {
                    timer.cancel();
                }
                timer = new CountDownTimer(60000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        String timeStr = String.format("拒绝(%d)", millisUntilFinished / 1000);
                        tvRefuse.setText(timeStr);
                    }

                    @Override
                    public void onFinish() {
                        hideCallUp(llCallUp);
                    }
                };
                timer.start();
            }
        });
        llCallUp.startAnimation(showAnima);
    }

    public void hideCallUp() {
        if (null != getView() && null != getView().findViewById(R.id.llCallUp)) {
            hideCallUp(getView().findViewById(R.id.llCallUp));
        }
    }

    //隐藏召集令
    private void hideCallUp(LinearLayout llCallUp) {
        if (null == llCallUp || llCallUp.getVisibility() == View.GONE) {
            return;
        }
        TranslateAnimation hideAnima = new TranslateAnimation(Animation.RELATIVE_TO_SELF
                , 0, Animation.RELATIVE_TO_SELF, 0
                , Animation.RELATIVE_TO_SELF, 0f
                , Animation.RELATIVE_TO_SELF, -1f);
        hideAnima.setDuration(1000);
        hideAnima.setAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
                super.onAnimationEnd(animation);
                llCallUp.setVisibility(View.GONE);
                if (timer != null) {
                    timer.cancel();
                    timer = null;
                }
            }
        });
        llCallUp.startAnimation(hideAnima);
    }
}
