package com.yuhuankj.tmxq.ui.me.wallet.income.withdraw.redpackage;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.redpacket.IRedPacketCore;
import com.tongdaxing.xchat_core.redpacket.IRedPacketCoreClient;
import com.tongdaxing.xchat_core.redpacket.bean.RedPacketInfo;
import com.tongdaxing.xchat_core.redpacket.bean.WithdrawRedListInfo;
import com.tongdaxing.xchat_core.redpacket.bean.WithdrawRedSucceedInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_core.withdraw.IWithdrawCore;
import com.tongdaxing.xchat_core.withdraw.IWithdrawCoreClient;
import com.tongdaxing.xchat_core.withdraw.bean.WithdrawInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.ui.login.BinderPhoneActivity;
import com.yuhuankj.tmxq.ui.me.wallet.income.withdraw.BinderAlipayActivity;

import java.util.List;

/**
 * 红包提现
 */
public class RedPacketWithdrawActivity extends BaseActivity implements View.OnClickListener {

    private TextView redNum;
    private RelativeLayout binder;
    private RelativeLayout binderSucceed;
    private TextView alipayAccount;
    private TextView alipayName;
    private RecyclerView recyclerView;
    private Button btnWithdraw;
    private Button unBtnWithdraw;
    private WithdrawInfo mWithdrawInfo;
    private WithdrawRedListAdapter mRedListAdapter;
    private WithdrawRedListInfo mSelectRedInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_red_packet_withdraw);
        initTitleBar(getString(R.string.red_packet_withdraw));
        initView();
        initData();
        setListener();
    }

    private void initView() {
        redNum = (TextView) findViewById(R.id.tv_red_num);
        binder = (RelativeLayout) findViewById(R.id.rly_binder);
        binderSucceed = (RelativeLayout) findViewById(R.id.rly_binder_succeed);
        alipayAccount = (TextView) findViewById(R.id.tv_user_zhifubao);
        alipayName = (TextView) findViewById(R.id.tv_user_zhifubao_name);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        btnWithdraw = (Button) findViewById(R.id.btn_withdraw);
        unBtnWithdraw = (Button) findViewById(R.id.btn_withdraw_un);
    }


    private void initData() {
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        mRedListAdapter = new WithdrawRedListAdapter();
        recyclerView.setAdapter(mRedListAdapter);
        mRedListAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int position) {
                List<WithdrawRedListInfo> list = mRedListAdapter.getData();
                if (ListUtils.isListEmpty(list)) return;
                mSelectRedInfo = list.get(position);
                int size = list.size();
                for (int i = 0; i < size; i++) {
                    list.get(i).isSelected = position == i;
                }
                mRedListAdapter.notifyDataSetChanged();
                redPackWithdraw();
            }
        });
        loadAlipayInfo();
        loadingData();
        loadingListData();
    }

    private void loadAlipayInfo() {
        CoreManager.getCore(IWithdrawCore.class).getWithdrawUserInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onGetWithdrawUserInfo(WithdrawInfo withdrawInfo) {
        if (withdrawInfo != null) {
            mWithdrawInfo = withdrawInfo;
            if (TextUtils.isEmpty(withdrawInfo.alipayAccount) || withdrawInfo.alipayAccount.equals("null")) {
                binder.setVisibility(View.VISIBLE);
                binderSucceed.setVisibility(View.GONE);
            } else {
                binder.setVisibility(View.GONE);
                binderSucceed.setVisibility(View.VISIBLE);
                alipayAccount.setText(withdrawInfo.alipayAccount);
                alipayName.setText(withdrawInfo.alipayAccountName);
            }
        }
    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onGetWithdrawUserInfoFail(String error) {
        toast(error);
    }

    private void loadingListData() {
        CoreManager.getCore(IRedPacketCore.class).getRedList();
    }

    @CoreEvent(coreClientClass = IRedPacketCoreClient.class)
    public void onGetRedList(List<WithdrawRedListInfo> withdrawRedListInfos) {
        if (!withdrawRedListInfos.isEmpty()) {
            mRedListAdapter.setNewData(withdrawRedListInfos);
        }
    }

    @CoreEvent(coreClientClass = IRedPacketCoreClient.class)
    public void onGetRedListError(String error) {
        toast(error);
    }

    private void loadingData() {
        CoreManager.getCore(IRedPacketCore.class).getRedPacketInfo();
    }

    private RedPacketInfo redPacketInfos;

    @CoreEvent(coreClientClass = IRedPacketCoreClient.class)
    public void onGetRedInfo(RedPacketInfo redPacketInfo) {
        if (null != redPacketInfo) {
            redPacketInfos = redPacketInfo;
            Double packetNum = redPacketInfo.getPacketNum();
            packetNum = (double) Math.round(packetNum * 100) / 100;
            redNum.setText(String.valueOf(packetNum));
        }
    }

    @CoreEvent(coreClientClass = IRedPacketCoreClient.class)
    public void onGetRedInfoError(String error) {
        toast(error);
    }


    private void setListener() {
        binder.setOnClickListener(this);
        binderSucceed.setOnClickListener(this);
    }

    private Intent intent;
    private Bundle mBundle;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            //未绑定支付宝时可点---绑定支付宝账号
            case R.id.rly_binder:
                UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
                if (userInfo != null && !userInfo.isBindPhone()) {
                    Intent intent = new Intent(this, BinderPhoneActivity.class);
                    startActivity(intent);
                    return;
                }
                intent = new Intent(RedPacketWithdrawActivity.this, BinderAlipayActivity.class);
                mBundle = new Bundle();
                mBundle.putSerializable("withdrawInfo", mWithdrawInfo);
                intent.putExtras(mBundle);
                startActivity(intent);
                break;
            //绑定成功支付宝后可点---更改支付宝账号
            case R.id.rly_binder_succeed:
                intent = new Intent(RedPacketWithdrawActivity.this, BinderAlipayActivity.class);
                mBundle = new Bundle();
                mBundle.putSerializable("withdrawInfo", mWithdrawInfo);
                intent.putExtras(mBundle);
                startActivity(intent);
                break;
        }
    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onBinderAlipay() {
        toast("绑定成功");
        loadAlipayInfo();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void redPackWithdraw() {
        if (mSelectRedInfo == null) {
            toast("兑换失败");
            return;
        }
        if (isWithdraw()) {
            btnWithdraw.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //有账号才能提现
                    if (!isAlipayValid()) {
                        toast(R.string.bind_your_alipay);
                        return;
                    }
                    //发起兑换
                    getDialogManager().showOkCancelDialog(
                            getString(R.string.withdraw_dialog_notice,
                                    mSelectRedInfo.getPacketNum(), mSelectRedInfo.getPacketNum()),
                            true, new DialogManager.OkCancelDialogListener() {
                                @Override
                                public void onCancel() {
                                    getDialogManager().dismissDialog();
                                }

                                @Override
                                public void onOk() {
                                    getDialogManager().dismissDialog();
                                    CoreManager.getCore(IRedPacketCore.class).getRedWithdraw(
                                            CoreManager.getCore(IAuthCore.class).getCurrentUid(),
                                            mSelectRedInfo.getPacketId());

                                }
                            });
                }
            });
        }
    }


    public boolean isWithdraw() {
        if (!(mWithdrawInfo != null && mWithdrawInfo.isNotBoundPhone)) {
            if (mSelectRedInfo != null && redPacketInfos != null) {
                //用户的钻石余额 > 选中金额的钻石数时
                if (redPacketInfos.getPacketNum() >= mSelectRedInfo.getPacketNum()) {
                    unBtnWithdraw.setVisibility(View.GONE);
                    btnWithdraw.setVisibility(View.VISIBLE);
                    return true;
                } else {
                    unBtnWithdraw.setVisibility(View.VISIBLE);
                    btnWithdraw.setVisibility(View.GONE);
                    return true;
                }
            }
        } else {
            return false;
        }
        //如果选中position不为空的时候
        return false;
    }

    private boolean isAlipayValid() {
        return mWithdrawInfo != null && !TextUtils.isEmpty(mWithdrawInfo.alipayAccount);
    }

    @CoreEvent(coreClientClass = IRedPacketCoreClient.class)
    public void onGetWithdraw(WithdrawRedSucceedInfo succeedInfo) {
        toast("提现成功");
        if (null != succeedInfo) {
            Double packetNum = succeedInfo.getPacketNum();
            packetNum = (double) Math.round(packetNum * 100) / 100;
            redNum.setText(String.valueOf(packetNum));
        }
    }

    @CoreEvent(coreClientClass = IRedPacketCoreClient.class)
    public void onGetWithdrawError(String error) {
        toast(error);
    }

}
