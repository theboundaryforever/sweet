package com.yuhuankj.tmxq.ui.user.other;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.jude.rollviewpager.adapter.StaticPagerAdapter;
import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.cache.NimUserInfoCache;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.netease.nim.uikit.glide.GlideApp;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomKickOutEvent;
import com.netease.nimlib.sdk.friend.FriendService;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.tongdaxing.erban.libcommon.audio.AudioPlayer;
import com.tongdaxing.erban.libcommon.audio.OnPlayListener;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.audio.AudioPlayAndRecordManager;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCore;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCoreClient;
import com.tongdaxing.xchat_core.legion.ILegionClient;
import com.tongdaxing.xchat_core.legion.ILegionCore;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.praise.IPraiseClient;
import com.tongdaxing.xchat_core.praise.IPraiseCore;
import com.tongdaxing.xchat_core.room.IRoomCore;
import com.tongdaxing.xchat_core.room.IRoomCoreClient;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.publicchatroom.PublicChatRoomController;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_core.user.bean.GiftWallInfo;
import com.tongdaxing.xchat_core.user.bean.MedalBean;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_core.user.bean.UserPhoto;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.databinding.ActivityUserInfoBinding;
import com.yuhuankj.tmxq.ui.MainActivity;
import com.yuhuankj.tmxq.ui.launch.guide.NoviceGuideDialog;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.widget.ButtonItemFactory;
import com.yuhuankj.tmxq.ui.login.ViewAdapter;
import com.yuhuankj.tmxq.ui.match.activity.UploadRecordDialog;
import com.yuhuankj.tmxq.ui.me.medal.MedalActivity;
import com.yuhuankj.tmxq.ui.message.attention.AttentionListActivity;
import com.yuhuankj.tmxq.ui.message.fans.FansListActivity;
import com.yuhuankj.tmxq.ui.message.like.LikeMeListActivity;
import com.yuhuankj.tmxq.ui.user.bosonFriend.BosonFriendActivity;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.utils.UIHelper;
import com.yuhuankj.tmxq.widget.ObservableScrollView;
import com.yuhuankj.tmxq.widget.UserInfoColorPointHintView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.reactivex.disposables.Disposable;

/**
 * 用户中心
 *
 * @author zhouxiangfeng
 * @date 2017/5/24
 */
public class UserInfoActivity extends BaseMvpActivity
        implements View.OnClickListener, ObservableScrollView.ScrollViewListener,
        UploadRecordDialog.OnNeedExitRoomListener, IMvpBaseView {

    private static final String TAG = UserInfoActivity.class.getSimpleName();
    private ActivityUserInfoBinding infoBinding;
    OnPlayListener onPlayListener = new OnPlayListener() {

        @Override
        public void onPrepared() {
            LogUtils.d(TAG, "onPrepared");
            //准备开始试听,图标状态不管
        }

        @Override
        public void onCompletion() {
            LogUtils.d(TAG, "onCompletion");
            //试听结束,更新试听按钮为试听
            infoBinding.imvVoice.setImageDrawable(getResources().getDrawable(R.drawable.anim_user_info_voice));
        }

        @Override
        public void onInterrupt() {
            LogUtils.d(TAG, "onInterrupt");
            //试听被中断
            infoBinding.imvVoice.setImageDrawable(getResources().getDrawable(R.drawable.anim_user_info_voice));
        }

        @Override
        public void onError(String s) {
            LogUtils.d(TAG, "onError :" + s);
            //试听出错
            infoBinding.imvVoice.setImageDrawable(getResources().getDrawable(R.drawable.anim_user_info_voice));
        }

        @Override
        public void onPlaying(long l) {
            LogUtils.d(TAG, "onPlaying :" + l);

        }
    };
    private UserInfo userInfo;
    private long userId;
    private boolean autoShowUploadAudioDialog;
    private RoomInfo mRoomInfo;
    private boolean hasInitData = false;
    private int flag = 0;
    private List<UserPropInfo> propList = new ArrayList<>();
    private List<GiftWallInfo> giftWallListOrderByCount = new ArrayList<>();
    private List<GiftWallInfo> giftWallListOrderByPrice = new ArrayList<>();
    private UserPropAdapter propAdapter;
    private BasePropertyAdapter giftWallAdapter;
    private NoviceGuideDialog userInfoGuideDialog = null;

    private long currUserInRoomUId = 0L;
    private int currUserInRoomType = RoomInfo.ROOMTYPE_HOME_PARTY;

    private BannerAdapter bannerAdapter;
    private RecyclerView rvPropList, rvGiftList;
    private LinearLayout llGiftEmpty, llPropEmpty;

    //挚友相关
    private RelativeLayout rlFriend1, rlFriend2, rlFriend3;
    private ImageView imvFriendHeadwear1, imvFriendHeadwear2, imvFriendHeadwear3;
    private ImageView imvFriend1, imvFriend2, imvFriend3;

    //----------------------------------------上传录音--------------------------------------
    private UploadRecordDialog uploadRecordDialog;
    private AudioPlayAndRecordManager audioManager;
    private AudioPlayer audioPlayer;

    private int enterSource = 0;//从哪个页面进入此页面 1：从附近页面进入,2:从交友速配完成页进入

//----------------------------右上角更多（举报、禁言、封禁等业务逻辑）---------------------------------

    public static void start(Context context, long userId, int enterSource) {
        Intent intent = new Intent(context, UserInfoActivity.class);
        intent.putExtra("userId", userId);
        intent.putExtra("enterSource", enterSource);
        context.startActivity(intent);
    }

    public static void start(Context context, long userId) {
        Intent intent = new Intent(context, UserInfoActivity.class);
        intent.putExtra("userId", userId);
        context.startActivity(intent);
    }

    public static void start(Context context, long userId, boolean autoShowUploadAudioDialog) {
        Intent intent = new Intent(context, UserInfoActivity.class);
        intent.putExtra("userId", userId);
        intent.putExtra("autoShowUploadAudioDialog", autoShowUploadAudioDialog);
        context.startActivity(intent);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        userId = intent.getLongExtra("userId", 0L);
        enterSource = intent.getIntExtra("enterSource", 0);
        userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(userId, true);
        hasInitData = false;
        initData(userInfo);
        refreshStatusData();
        checkAutoShowUploadRecordDialog(intent);
    }

    private void checkAutoShowUploadRecordDialog(Intent intent) {
        autoShowUploadAudioDialog = intent.getBooleanExtra("autoShowUploadAudioDialog", false);
        LogUtils.d(TAG, "checkAutoShowUploadRecordDialog-autoShowUploadAudioDialog:" + autoShowUploadAudioDialog);
        if (autoShowUploadAudioDialog) {
            showUploadRecordDialog();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        infoBinding = DataBindingUtil.setContentView(this, R.layout.activity_user_info);
        infoBinding.setClick(this);
        infoBinding.ivMore.setOnClickListener(this);
        //----------相册-------------------------------------------
        //infoBinding.photoRecyclerView.setLayoutManager(new RecyclerViewNoBugLinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        infoBinding.scrollView.setScrollViewListener(svScrollViewListener);

        userId = getIntent().getLongExtra("userId", 0L);
        enterSource = getIntent().getIntExtra("enterSource", 0);
        userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(userId, true, CoreManager.getCore(IAuthCore.class).getCurrentUid() == userId ? 0 : 1);

        propAdapter = new UserPropAdapter(this);
        propAdapter.setUserPropInfoList(propList);

        infoBinding.vpContent.addOnPageChangeListener(pageChangeListener);
        // 初始化引导图片列表
        ArrayList<View> views = new ArrayList<>();
        View vProp = LayoutInflater.from(this).inflate(R.layout.layout_userinfo_prop, null);
        View vGift = LayoutInflater.from(this).inflate(R.layout.layout_userinfo_gift, null);

        rvPropList = vProp.findViewById(R.id.rvPropList);
        llPropEmpty = vProp.findViewById(R.id.llPropEmpty);
        rvGiftList = vGift.findViewById(R.id.rvGiftList);
        llGiftEmpty = vGift.findViewById(R.id.llGiftEmpty);

        //默认显示空礼物和空道具提示界面
        llPropEmpty.setVisibility(View.VISIBLE);
        rvPropList.setVisibility(View.GONE);
        llGiftEmpty.setVisibility(View.VISIBLE);
        rvGiftList.setVisibility(View.GONE);

        rvPropList.setOnTouchListener(rvTouchListener);
        rvGiftList.setOnTouchListener(rvTouchListener);

        curRView = rvPropList;

        rvPropList.setFocusableInTouchMode(false);
        rvPropList.setFocusable(false);
        rvGiftList.setFocusableInTouchMode(false);
        rvGiftList.setFocusable(false);

        views.add(vProp);
        views.add(vGift);
        ViewAdapter vpAdapter = new ViewAdapter(views);
        infoBinding.vpContent.setAdapter(vpAdapter);

        rvPropList.setNestedScrollingEnabled(false);
        rvPropList.setAdapter(propAdapter);
        rvPropList.setLayoutManager(new GridLayoutManager(this, 4));

        giftWallAdapter = new GiftWallAdapter(this);
        giftWallAdapter.setGiftWallInfoList(new ArrayList<>());
        rvGiftList.setNestedScrollingEnabled(false);
        rvGiftList.setAdapter(giftWallAdapter);
        rvGiftList.setLayoutManager(new GridLayoutManager(this, 4));
        infoBinding.llFindTa.setVisibility(View.GONE);

        rlFriend1 = (RelativeLayout) findViewById(R.id.rlFriend1);
        rlFriend2 = (RelativeLayout) findViewById(R.id.rlFriend2);
        rlFriend3 = (RelativeLayout) findViewById(R.id.rlFriend3);
        imvFriendHeadwear1 = (ImageView) findViewById(R.id.imvFriendHeadwear1);
        imvFriendHeadwear2 = (ImageView) findViewById(R.id.imvFriendHeadwear2);
        imvFriendHeadwear3 = (ImageView) findViewById(R.id.imvFriendHeadwear3);
        imvFriend1 = (ImageView) findViewById(R.id.imvFriend1);
        imvFriend2 = (ImageView) findViewById(R.id.imvFriend2);
        imvFriend3 = (ImageView) findViewById(R.id.imvFriend3);

        initData(userInfo);
        //单独抽出来一个方法来更新关注状态、礼物墙信息，以避免强刷个人数据会导致多次刷新礼物列表
        refreshStatusData();
        Disposable disposable = IMNetEaseManager.get().getChatRoomEventObservable().subscribe(roomEvent -> {
            if (null != roomEvent) {
                switch (roomEvent.getEvent()) {
                    case RoomEvent.KICK_OUT_ROOM:
                        ChatRoomKickOutEvent reason = roomEvent.getReason();
                        if (reason != null) {
                            ChatRoomKickOutEvent.ChatRoomKickOutReason kickOutReason = reason.getReason();
                            if (kickOutReason == ChatRoomKickOutEvent.ChatRoomKickOutReason.CHAT_ROOM_INVALID) {
                                infoBinding.llTaHome.setVisibility(View.GONE);
                            }
                        }
                        break;
                    default:
                }
            }
        });
        mCompositeDisposable.add(disposable);
        infoBinding.attentionText.post(new Runnable() {
            @Override
            public void run() {
                checkShouldShowGuideView(CoreManager.getCore(IAuthCore.class).getCurrentUid() == userId && !autoShowUploadAudioDialog);
            }
        });

        checkAutoShowUploadRecordDialog(getIntent());

        findViewById(R.id.llAdmire).setOnClickListener(this);
    }

    private void initData(UserInfo userInfo) {
        if (!hasInitData && null != userInfo) {
            hasInitData = true;
            //头像等基本信息
            infoBinding.setUserInfo(userInfo);
            String userNick = userInfo.getNick();
            if (!TextUtils.isEmpty(userNick) && userNick.length() > 8) {
                userNick = getResources().getString(R.string.nick_length_max_six, userNick.substring(0, 6));
            }
            infoBinding.tvUserName.setText(userNick);
            if (!TextUtils.isEmpty(userInfo.getVipMedal())) {
                infoBinding.imvUserMedal.setVisibility(View.VISIBLE);
                GlideApp.with(this).load(userInfo.getVipMedal())
                        .dontAnimate().into(infoBinding.imvUserMedal);
            } else {
                infoBinding.imvUserMedal.setVisibility(View.GONE);
            }
            ImageView imvLoverAnima = (ImageView) findViewById(R.id.imvLoverAnima);
            UserInfo loverUserInfo = userInfo.getLoverUser();
            if (null == loverUserInfo/* || userInfo.getUid() == CoreManager.getCore(IAuthCore.class).getCurrentUid()*/) {
                infoBinding.rlLover.setVisibility(View.GONE);
                imvLoverAnima.setVisibility(View.GONE);
            } else {
                infoBinding.rlLover.setVisibility(View.VISIBLE);
                ImageLoadUtils.loadCircleImage(this, loverUserInfo.getAvatar(), infoBinding.imvLover, R.drawable.default_user_head);
                imvLoverAnima.setVisibility(View.VISIBLE);
                imvLoverAnima.setImageResource(R.drawable.anima_lover);
                AnimationDrawable animationDrawable = (AnimationDrawable) imvLoverAnima.getDrawable();
                animationDrawable.start();
            }
            //等级
//            if (userInfo.getExperLevel() <= 0 && userInfo.getCharmLevel() <= 0) {
//                infoBinding.lvUserLevel.setVisibility(View.GONE);
//            } else {
//                infoBinding.lvUserLevel.setVisibility(View.VISIBLE);
//                infoBinding.lvUserLevel.setExperLevel(userInfo.getExperLevel());
//                infoBinding.lvUserLevel.setCharmLevel(userInfo.getCharmLevel());
//            }
            if (userInfo.getExperLevel() > 0) {
                infoBinding.imvFortuneLevel.setVisibility(View.VISIBLE);
                ImageLoadUtils.loadImage(this, UriProvider.getCFImgUrl(userInfo.getExperLevel()), infoBinding.imvFortuneLevel);
            } else {
                infoBinding.imvFortuneLevel.setVisibility(View.GONE);
            }
            if (userInfo.getGrowingLevel() > 0) {
                infoBinding.imvGrowingLevel.setVisibility(View.VISIBLE);
                ImageLoadUtils.loadImage(this, UriProvider.getCZImgUrl(userInfo.getGrowingLevel()), infoBinding.imvGrowingLevel);
            } else {
                infoBinding.imvGrowingLevel.setVisibility(View.GONE);
            }
            if (userInfo.getCharmLevel() > 0) {
                infoBinding.imvCharmLevel.setVisibility(View.VISIBLE);
                ImageLoadUtils.loadImage(this, UriProvider.getMLImgUrl(userInfo.getCharmLevel()), infoBinding.imvCharmLevel);
            } else {
                infoBinding.imvCharmLevel.setVisibility(View.GONE);
            }
            //取出头像和相册
            List<String> images = new ArrayList<>();
            int showPhotoSize = userInfo.getGrowingLevel() >= 20 ? 6 : 4;
            images.add(userInfo.getAvatar());
            if (userInfo.getPrivatePhoto() != null && userInfo.getPrivatePhoto().size() > 0) {
                for (UserPhoto photo : userInfo.getPrivatePhoto()) {
                    if (photo.getPhotoStatus() == 1) {
                        //取审核通过的照片展示
                        images.add(photo.getPhotoUrl());
                        if (images.size() > showPhotoSize) {
                            break;
                        }
                    }
                }
            }
            bannerAdapter = new BannerAdapter(images, this);
            infoBinding.bnImages.setAdapter(bannerAdapter);
            bannerAdapter.notifyDataSetChanged();
            if (images.size() > 1) {
                infoBinding.bnImages.setHintView(new UserInfoColorPointHintView(this, Color.parseColor("#FFFFFF"), Color.parseColor("#66000000")));
                infoBinding.bnImages.setPlayDelay(3000);
                infoBinding.bnImages.setAnimationDurtion(500);
            } else {
                infoBinding.bnImages.setHintView(new UserInfoColorPointHintView(this, Color.TRANSPARENT, Color.TRANSPARENT));
            }
            if (CoreManager.getCore(IAuthCore.class).getCurrentUid() == userId) {
                infoBinding.setIsUserSelf(true);
                infoBinding.llAttention.setOnClickListener(this);
                infoBinding.llFans.setOnClickListener(this);
                infoBinding.llTryListen.setOnClickListener(this);
                //隐藏自己的个人资料页上传录音入口
                infoBinding.llTryListen.setVisibility(View.VISIBLE);
                if (!TextUtils.isEmpty(userInfo.getUserVoice()) && userInfo.getVoiceDura() > 0) {
                    infoBinding.tvVoice.setText(getResources().getString(R.string.userinfo_voice_dura, userInfo.getVoiceDura()));
                    infoBinding.imvVoice.setVisibility(View.VISIBLE);
                } else {
                    infoBinding.imvVoice.setVisibility(View.GONE);
                    infoBinding.tvVoice.setText(getResources().getString(R.string.userinfo_voice_tips));
                    infoBinding.llTryListen.setVisibility(View.GONE);
                }
                //如果是自己就不显示他的窝和去找ta
                infoBinding.llTaHome.setVisibility(View.GONE);
                infoBinding.llFindTa.setVisibility(View.GONE);
            } else {
                infoBinding.setIsUserSelf(false);
                if (!TextUtils.isEmpty(userInfo.getUserVoice()) && userInfo.getVoiceDura() > 0) {
                    infoBinding.llTryListen.setVisibility(View.VISIBLE);
                    infoBinding.llTryListen.setOnClickListener(this);
                    infoBinding.tvVoice.setText(getResources().getString(R.string.userinfo_voice_dura, userInfo.getVoiceDura()));
                    infoBinding.imvVoice.setVisibility(View.VISIBLE);
                } else {
                    infoBinding.llTryListen.setVisibility(View.GONE);
                }
                requestRoomInfo(userInfo);
            }
            infoBinding.tvErbanId.setText(getResources().getString(R.string.user_info_erban_id, String.valueOf(userInfo.getErbanNo())));

            infoBinding.imvMedal1.setVisibility(View.GONE);
            infoBinding.imvCurMedal1.setVisibility(View.GONE);
            infoBinding.imvMedal2.setVisibility(View.GONE);
            infoBinding.imvCurMedal2.setVisibility(View.GONE);
            if (userInfo.getWearList() != null) {
                List<MedalBean> medalBeans = new ArrayList<>();
                for (MedalBean bean : userInfo.getWearList()) {
                    if (bean.getAliasId() > 0) {
                        medalBeans.add(bean);
                    }
                }
                if (medalBeans.size() == 1) {
                    infoBinding.imvMedal1.setVisibility(View.VISIBLE);
                    infoBinding.imvCurMedal1.setVisibility(View.VISIBLE);
                    ImageLoadUtils.loadImage(this, medalBeans.get(0).getAlias(), infoBinding.imvMedal1);
                    ImageLoadUtils.loadImage(this, medalBeans.get(0).getAlias(), infoBinding.imvCurMedal1);
                } else if (medalBeans.size() == 2) {
                    infoBinding.imvMedal1.setVisibility(View.VISIBLE);
                    infoBinding.imvCurMedal2.setVisibility(View.VISIBLE);
                    ImageLoadUtils.loadImage(this, medalBeans.get(0).getAlias(), infoBinding.imvMedal1);
                    ImageLoadUtils.loadImage(this, medalBeans.get(0).getAlias(), infoBinding.imvCurMedal2);

                    infoBinding.imvMedal2.setVisibility(View.VISIBLE);
                    infoBinding.imvCurMedal1.setVisibility(View.VISIBLE);
                    ImageLoadUtils.loadImage(this, medalBeans.get(1).getAlias(), infoBinding.imvMedal2);
                    ImageLoadUtils.loadImage(this, medalBeans.get(1).getAlias(), infoBinding.imvCurMedal1);
                }
            }
//            if (!TextUtils.isEmpty(userInfo.getAlias())) {
//                infoBinding.imvMedal.setVisibility(View.VISIBLE);
//                infoBinding.imvCurMedal1.setVisibility(View.VISIBLE);
//                ImageLoadUtils.loadImage(this, userInfo.getAlias(), infoBinding.imvMedal);
//                ImageLoadUtils.loadImage(this, userInfo.getAlias(), infoBinding.imvCurMedal1);
//            } else {
//                infoBinding.imvMedal.setVisibility(View.GONE);
//                infoBinding.imvCurMedal1.setVisibility(View.GONE);
//            }
            infoBinding.rlMedal.setOnClickListener(this);

            if (!TextUtils.isEmpty(userInfo.getUserDesc())) {
                infoBinding.tvSignature.setText(userInfo.getUserDesc());
            }

            try {
                if (userInfo.getBestFriendList() != null && userInfo.getBestFriendList().size() > 0) {
                    int size = userInfo.getBestFriendList().size();
                    if (size >= 1) {
                        rlFriend1.setVisibility(View.VISIBLE);
                        ImageLoadUtils.loadCircleImage(this, userInfo.getBestFriendList().get(0).getAvatar(), imvFriend1, R.drawable.ic_bosonfriend_level_default);
                        ImageLoadUtils.loadImage(this, userInfo.getBestFriendList().get(0).getLevelIcon(), imvFriendHeadwear1, R.drawable.ic_bosonfriend_level_0);
                    }
                    if (size >= 2) {
                        rlFriend2.setVisibility(View.VISIBLE);
                        ImageLoadUtils.loadCircleImage(this, userInfo.getBestFriendList().get(1).getAvatar(), imvFriend2, R.drawable.ic_bosonfriend_level_default);
                        ImageLoadUtils.loadImage(this, userInfo.getBestFriendList().get(1).getLevelIcon(), imvFriendHeadwear2, R.drawable.ic_bosonfriend_level_0);
                    }
                    if (size >= 3) {
                        rlFriend3.setVisibility(View.VISIBLE);
                        ImageLoadUtils.loadCircleImage(this, userInfo.getBestFriendList().get(2).getAvatar(), imvFriend3, R.drawable.ic_bosonfriend_level_default);
                        ImageLoadUtils.loadImage(this, userInfo.getBestFriendList().get(2).getLevelIcon(), imvFriendHeadwear3, R.drawable.ic_bosonfriend_level_0);
                    }
                } else {

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void refreshStatusData() {
        //查询是否已经关注
        if (CoreManager.getCore(IAuthCore.class).getCurrentUid() != userId) {
            CoreManager.getCore(IPraiseCore.class).isPraised(CoreManager.getCore(IAuthCore.class).getCurrentUid(), userId);

            //查找对方当前的房间信息
            CoreManager.getCore(IRoomCore.class).getUserRoom(userId);
        }

        //更新礼物
        CoreManager.getCore(IUserCore.class).requestUserGiftWall(userId, 1);
        CoreManager.getCore(IUserCore.class).requestUserGiftWall(userId, 2);
        //查询座驾 头饰
        requestPropData();

        CoreManager.getCore(VersionsCore.class).requestSensitiveWord();

        //获取用户当前所在房间信息
        currUserInRoomUId = 0L;
        infoBinding.llFindTa.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llTryListen:
                if (CoreManager.getCore(IAuthCore.class).getCurrentUid() == userId) {
                    if (!TextUtils.isEmpty(userInfo.getUserVoice()) && userInfo.getVoiceDura() > 0) {
                        tryToListen();
                    } else {
                        showUploadRecordDialog();
                        //个人资料-上传录音--点击
                        StatisticManager.get().onEvent(this,
                                StatisticModel.EVENT_ID_USERINFO_AUDIO_UPLOAD_CLICK,
                                StatisticModel.getInstance().getUMAnalyCommonMap(this));
                    }

                } else {
                    tryToListen();
                }
                break;
            case R.id.tv_voice:
                showUploadRecordDialog();
                //个人资料-上传录音--点击
                StatisticManager.get().onEvent(this,
                        StatisticModel.EVENT_ID_USERINFO_AUDIO_UPLOAD_CLICK,
                        StatisticModel.getInstance().getUMAnalyCommonMap(this));
                break;
            case R.id.iv_more:
                showMoreMenuDialog();
                break;

            case R.id.tv_edit:
                UIHelper.showUserInfoModifyAct(UserInfoActivity.this, userId);
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.ll_attention:
                startActivity(new Intent(this, AttentionListActivity.class));
                break;
            case R.id.ll_fans:
                startActivity(new Intent(this, FansListActivity.class));
                break;
            case R.id.llTaHome:
                if (mRoomInfo != null) {
                    if (MainActivity.isLinkMicroing) {
                        SingleToastUtil.showToast("连麦中不能进入房间");
                        return;
                    }

                    RoomInfo current = RoomServiceScheduler.getCurrentRoomInfo();
                    if (null != current && mRoomInfo != null && current.getType() == mRoomInfo.getType()
                            && current.getUid() == mRoomInfo.getUid()) {
                        toast(R.string.already_in_same_room);
                        return;
                    }

                    RoomServiceScheduler.getInstance().enterRoom(this, mRoomInfo.getUid(), mRoomInfo.getType());
                    //资料-Ta的窝
                    StatisticManager.get().onEvent(this,
                            StatisticModel.EVENT_ID_USERINFO_HISHOME,
                            StatisticModel.getInstance().getUMAnalyCommonMap(this));
                }
                break;
            case R.id.ll_send_msg:
                //连麦中点私聊直接关闭资料页,上一页就是私聊页
                if (MainActivity.isLinkMicroing) {
                    finish();
                    return;
                }
                boolean isFriend = NIMClient.getService(FriendService.class).isMyFriend(userId + "");
                if (null != userInfo && userInfo.getMessageRestriction() == 1 && !isFriend) {
                    toast(R.string.only_recv_friend_msg_tips);
                    return;
                }
                if (enterSource == 1) {
                    //从附近进入主页再进入私聊统计
                    Map<String, String> params = StatisticModel.getInstance().getUMAnalyCommonMap(this);
                    StatisticManager.get().onEvent(this,
                            StatisticModel.EVENT_ID_CLICK_NEARBY_P2PCHAT, params);
                } else if (enterSource == 2) {
                    //从交友速配进入主页再进入私聊统计
                    Map<String, String> params = StatisticModel.getInstance().getUMAnalyCommonMap(this);
                    StatisticManager.get().onEvent(this,
                            StatisticModel.EVENT_ID_CLICK_QM_USERPAGE_CHAT, params);
                }
                NimUserInfo nimUserInfo = NimUserInfoCache.getInstance().getUserInfo(userId + "");
                if (nimUserInfo != null) {
                    NimUIKit.startP2PSession(this, userId + "");
                    //资料-私聊
                    StatisticManager.get().onEvent(UserInfoActivity.this,
                            StatisticModel.EVENT_ID_USERINFO_MESSAGE,
                            StatisticModel.getInstance().getUMAnalyCommonMap(UserInfoActivity.this));
                } else {
                    NimUserInfoCache.getInstance().getUserInfoFromRemote(userId + "", new RequestCallbackWrapper<NimUserInfo>() {
                        @Override
                        public void onResult(int i, NimUserInfo nimUserInfo, Throwable throwable) {
                            if (i == 200) {
                                NimUIKit.startP2PSession(UserInfoActivity.this, userId + "");
                                //资料-私聊
                                StatisticManager.get().onEvent(UserInfoActivity.this,
                                        StatisticModel.EVENT_ID_USERINFO_MESSAGE,
                                        StatisticModel.getInstance().getUMAnalyCommonMap(UserInfoActivity.this));
                            } else {
                                toast(getResources().getString(R.string.network_error_retry));
                            }
                        }
                    });
                }
                break;
            case R.id.ll_go_attention:
                getDialogManager().showProgressDialog(UserInfoActivity.this, getResources().getString(R.string.network_loading));
                CoreManager.getCore(IPraiseCore.class).userInfoPraise(userId);
                //资料-关注
                StatisticManager.get().onEvent(this,
                        StatisticModel.EVENT_ID_USERINFO_ATTENTION,
                        StatisticModel.getInstance().getUMAnalyCommonMap(this));
                break;
            case R.id.ll_cancel_attention:
                boolean isMyFriend = CoreManager.getCore(IIMFriendCore.class).isMyFriend(userId + "");
                String tip = getString(isMyFriend ? R.string.fan_friend_cancel_tips : R.string.fan_cancel_tips);
                getDialogManager().showOkCancelDialog(tip, true, new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {
                        getDialogManager().dismissDialog();
                    }

                    @Override
                    public void onOk() {
                        getDialogManager().dismissDialog();
                        getDialogManager().showProgressDialog(UserInfoActivity.this, getString(R.string.network_loading));
                        CoreManager.getCore(IPraiseCore.class).cancelPraise(userId);
                    }
                });
                break;
            case R.id.llFindTa:
                RoomInfo current = RoomServiceScheduler.getCurrentRoomInfo();
                if (currUserInRoomUId > 0) {
                    if (current != null && current.getUid() == currUserInRoomUId && current.getType() == currUserInRoomType) {
                        toast(R.string.already_in_same_room);
                        return;
                    }

                    if (MainActivity.isLinkMicroing) {
                        SingleToastUtil.showToast("连麦中不能进入房间");
                        return;
                    }

                    RoomServiceScheduler.getInstance().enterRoom(this, currUserInRoomUId,
                            currUserInRoomType);
                } else {
                    toast(R.string.user_not_in_room);
                }
                //资料-去找Ta
                StatisticManager.get().onEvent(this,
                        StatisticModel.EVENT_ID_USERINFO_FINDTA,
                        StatisticModel.getInstance().getUMAnalyCommonMap(this));
                break;
//            case R.id.tv_sort:
//                if (null != giftWallListOrderByCount && giftWallListOrderByCount.size() > 0
//                        && null != giftWallListOrderByPrice && giftWallListOrderByPrice.size() > 0) {
//                    List<GiftWallInfo> giftWallInfos = new ArrayList<>();
//                    if (infoBinding.tvSort.getText().equals(getResources().getString(R.string.user_info_gift_sort_price))) {
//                        infoBinding.tvSort.setText(getResources().getString(R.string.user_info_gift_sort_num));
//                        giftWallInfos = giftWallListOrderByPrice;
//                    } else {
//                        infoBinding.tvSort.setText(getResources().getString(R.string.user_info_gift_sort_price));
//                        giftWallInfos = giftWallListOrderByCount;
//                    }
//                    giftWallAdapter.setGiftWallInfoList(giftWallInfos);
//                    giftWallAdapter.notifyDataSetChanged();
//                }
//                break;
            case R.id.rlLover:
                if (userInfo != null && null != userInfo.getLoverUser()) {
                    UserInfoActivity.start(this, userInfo.getLoverUser().getUid());
                }
                break;
            case R.id.rlMedal:
                try {
                    ArrayList<MedalBean> medalBeans = new ArrayList<>(userInfo.getWearList());
                    Intent intent = new Intent(this, MedalActivity.class);
                    intent.putExtra("uid", userInfo.getUid());
                    intent.putExtra("userFortuneLevel", userInfo.getExperLevel());
                    intent.putExtra("curSetMedals", medalBeans);
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.llPropTitle:
                infoBinding.vpContent.setCurrentItem(0, true);
                break;
            case R.id.llGiftTitle:
                infoBinding.vpContent.setCurrentItem(1, true);
                break;
            case R.id.rlBosonFriend:
                Intent intent = new Intent(this, BosonFriendActivity.class);
                intent.putExtra("uid", userId);
                startActivity(intent);
                break;
            case R.id.llAdmire:
                if (userId != CoreManager.getCore(IAuthCore.class).getCurrentUid()) {
                    return;
                }
                LikeMeListActivity.start(this);
                break;
            default:
                break;
        }
    }

//-----------------------------查询该用户房间信息------------------------------

    private void showUploadRecordDialog() {
        if (CoreManager.getCore(IAuthCore.class).getCurrentUid() == userId) {
            if (null == uploadRecordDialog) {
                uploadRecordDialog = new UploadRecordDialog(UserInfoActivity.this);
                uploadRecordDialog.setOnNeedExitRoomListener(UserInfoActivity.this);
            }
            uploadRecordDialog.show();
        }
    }

    private void showMoreMenuDialog() {
        boolean hasBlacked = CoreManager.getCore(IIMFriendCore.class).isUserInBlackList(String.valueOf(userId));
        List<ButtonItem> buttonItems = new ArrayList<>();
        buttonItems.add(ButtonItemFactory.createReportItem(UserInfoActivity.this,
                getResources().getString(R.string.user_info_report), 1, userId));

        UserInfo selfUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        //是否为甜甜军团
        if (null != userInfo && null != selfUserInfo && selfUserInfo.isExternalAdmin()) {
            //用户是否已经被封禁
            ButtonItem.OnClickListener blockOrUnblockListener = new ButtonItem.OnClickListener() {
                @Override
                public void onClick() {
                    if (userInfo.isBlock()) {
                        //被封禁了，调用解封接口
                        blockUser(false, userId, 0);
                    } else {
                        //未被封禁，显示封禁多久dialog
                        List<ButtonItem> buttons = new ArrayList<>();
                        buttons.add(new ButtonItem(getResources().getString(R.string.user_info_block_day),
                                new ButtonItem.OnClickListener() {
                                    @Override
                                    public void onClick() {
                                        blockUser(true, userId, 1);
                                    }
                                }));
                        buttons.add(new ButtonItem(getResources().getString(R.string.user_info_block_week),
                                new ButtonItem.OnClickListener() {
                                    @Override
                                    public void onClick() {
                                        blockUser(true, userId, 2);
                                    }
                                }));
                        buttons.add(new ButtonItem(getResources().getString(R.string.user_info_block_ever),
                                new ButtonItem.OnClickListener() {
                                    @Override
                                    public void onClick() {
                                        blockUser(true, userId, 3);
                                    }
                                }));
                        getDialogManager().showCommonPopupDialog(buttons, getResources().getString(R.string.cancel));

                    }
                }
            };
            buttonItems.add(new ButtonItem(getResources().getString(userInfo.isBlock() ?
                    R.string.user_info_unblock : R.string.user_info_block), blockOrUnblockListener));

            //用户是否已经被禁言
            ButtonItem.OnClickListener forbiddListener = new ButtonItem.OnClickListener() {
                @Override
                public void onClick() {
                    if (userInfo.isMute()) {
                        //被禁言了，调用解除禁言接口
                        forbiddenUser(false, userId, 0);
                    } else {
                        //未被禁言，显示禁言多久dialog
                        List<ButtonItem> buttons = new ArrayList<>();
                        buttons.add(new ButtonItem(getResources().getString(R.string.forbidden_words_broadcast),
                                new ButtonItem.OnClickListener() {
                                    @Override
                                    public void onClick() {
                                        forbiddenUser(true, userId, 3);
                                    }
                                }));
                        buttons.add(new ButtonItem(getResources().getString(R.string.forbidden_words_private),
                                new ButtonItem.OnClickListener() {
                                    @Override
                                    public void onClick() {
                                        forbiddenUser(true, userId, 2);
                                    }
                                }));
                        buttons.add(new ButtonItem(getResources().getString(R.string.forbidden_words_room),
                                new ButtonItem.OnClickListener() {
                                    @Override
                                    public void onClick() {
                                        forbiddenUser(true, userId, 1);
                                    }
                                }));
                        getDialogManager().showCommonPopupDialog(buttons, getResources().getString(R.string.cancel));
                    }
                }
            };
            buttonItems.add(new ButtonItem(getResources().getString(userInfo.isMute() ?
                    R.string.user_info_forbidden_words_cancel : R.string.user_info_forbidden_words), forbiddListener));
        }

        ButtonItemFactory.OnItemClick onItemClick = new ButtonItemFactory.OnItemClick() {
            @Override
            public void itemClick() {
                showDefriendDialog();
            }
        };
        buttonItems.add(ButtonItemFactory.createMsgBlackListItem(
                getString(hasBlacked ? R.string.user_info_cancel_black : R.string.user_info_black), onItemClick));
        getDialogManager().showCommonPopupDialog(buttonItems, getString(R.string.cancel));
    }

    private void blockUser(boolean block, long targetUserId, int timeType) {
        getDialogManager().showProgressDialog(this, getResources().getString(R.string.network_loading));
        if (block) {
            CoreManager.getCore(ILegionCore.class).blockUser(targetUserId, timeType);
        } else {
            CoreManager.getCore(ILegionCore.class).unBlockUser(targetUserId);
        }
    }

//------------------关注与否更新操作----------------------------------------------

    private void forbiddenUser(boolean forbid, long targetUserId, int timeType) {
        getDialogManager().showProgressDialog(this, getResources().getString(R.string.network_loading));
        if (forbid) {
            CoreManager.getCore(ILegionCore.class).forbiddenWordUser(targetUserId, timeType);
        } else {
            CoreManager.getCore(ILegionCore.class).cancelForbiddenWordUser(targetUserId);
        }
    }

    @CoreEvent(coreClientClass = ILegionClient.class)
    public void onBlockUser(boolean isSuccess, String msg) {
        toast(msg);
        if (isSuccess) {
            CoreManager.getCore(IUserCore.class).requestUserInfo(userId);
        } else {
            getDialogManager().dismissDialog();
        }
    }

    @CoreEvent(coreClientClass = ILegionClient.class)
    public void onUnBlockUser(boolean isSuccess, String msg) {
        toast(msg);
        if (isSuccess) {
            CoreManager.getCore(IUserCore.class).requestUserInfo(userId);
        } else {
            getDialogManager().dismissDialog();
        }
    }

    @CoreEvent(coreClientClass = ILegionClient.class)
    public void onForbiddenWordUser(boolean isSuccess, String msg) {
        toast(msg);
        if (isSuccess) {
            CoreManager.getCore(IUserCore.class).requestUserInfo(userId);
        } else {
            getDialogManager().dismissDialog();
        }
    }

    @CoreEvent(coreClientClass = ILegionClient.class)
    public void onCancelForbiddenWordUser(boolean isSuccess, String msg) {
        toast(msg);
        if (isSuccess) {
            CoreManager.getCore(IUserCore.class).requestUserInfo(userId);
        } else {
            getDialogManager().dismissDialog();
            toast(msg);
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfo(UserInfo info) {
        if (info.getUid() == userId) {
            getDialogManager().dismissDialog();
            userInfo = info;
            hasInitData = false;
            initData(userInfo);
        }
    }

//------------------座驾、头饰更新操作----------------------------------------------

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo info) {
        if (info.getUid() == userId) {
            userInfo = info;
            hasInitData = false;
            initData(userInfo);
        }
    }

    private boolean isReachPinned = false;//是否到达固定道具和礼物标题栏的位置了
    private boolean isCanPinned = false;//是否可以固定道具和礼物标题栏了
    private int topbarBottomY = 0;//顶部标题栏底端的Y坐标
    private RecyclerView curRView;

    private ObservableScrollView.ScrollViewListener svScrollViewListener = new ObservableScrollView.ScrollViewListener() {

        @Override
        public void onScrollChanged(NestedScrollView view, int x, int y, int oldx, int oldy) {
            if (flag == 0 && oldy > 300) {
                flag = 1;
                infoBinding.title.setVisibility(View.VISIBLE);
                if (userInfo != null) {
                    infoBinding.title.setText(userInfo.getNick());
                }
                infoBinding.rlNav.setBackgroundResource(R.color.colorPrimaryDark);
                infoBinding.ivBack.setImageResource(R.drawable.icon_arrow_left_black);
                infoBinding.tvEdit.setTextColor(getResources().getColor(R.color.black));
                infoBinding.ivMore.setImageResource(R.drawable.user_info_menu_black);
                initImmersionBar(true);
            } else if (flag == 1 && oldy <= 300) {
                flag = 0;
                infoBinding.title.setVisibility(View.GONE);
                infoBinding.rlNav.setBackgroundResource(R.color.transparent);
                infoBinding.ivBack.setImageResource(R.drawable.icon_nav_back);
                infoBinding.tvEdit.setTextColor(getResources().getColor(R.color.white));
                infoBinding.ivMore.setImageResource(R.drawable.user_info_menu);
                initImmersionBar(false);
            }
            int[] posView = new int[2];
            infoBinding.llVpTitle.getLocationOnScreen(posView);
            isReachPinned = topbarBottomY > posView[1];
            isCanPinned = isReachPinned;
            if (!isCanScrollUp(curRView) && !isCanScrollDown(curRView)) {
                isCanPinned = false;
            }
            //  infoBinding.vpContent.setGetEvent(isCanPinned);
            LogUtils.e("liaoxy", "isCanPinned is " + isCanPinned);
        }
    };

    private View.OnTouchListener rvTouchListener = new View.OnTouchListener() {
        private float y1, y2;

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            LogUtils.e("liaoxy", "event.getAction() == " + event.getAction()
                    + " isCanScrollDown(curRView)==" + isCanScrollDown(curRView) + " isCanPinned =" + isCanPinned + " isReachPinned ==" + isReachPinned);
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                y1 = event.getY();
                if (isCanPinned) {
                    infoBinding.vpContent.setGetEvent(true);
                }
                //下面这种是另一种滚动方式，viewPager优先滚动
                //infoBinding.vpContent.setGetEvent(true);
            } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
                y2 = event.getY();
                if (y1 - y2 > 5) {
                    //向上滑
                    if (!isCanPinned) {
                        infoBinding.vpContent.setGetEvent(false);
                    } else {
                        infoBinding.vpContent.setGetEvent(true);
                    }
                    //下面这种是另一种滚动方式，viewPager优先滚动
//                    if (!isCanScrollUp(curRView)) {
//                        infoBinding.vpContent.setGetEvent(false);
//                    }
                } else if (y2 - y1 > 5) {
                    //向下滑
                    if (!isCanScrollDown(curRView)) {
                        infoBinding.vpContent.setGetEvent(false);
                    }
                }
                y1 = y2;
            }
            return false;
        }
    };

    private ViewPager.SimpleOnPageChangeListener pageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
        @Override
        public void onPageSelected(int position) {
            if (position == 0) {
                curRView = rvPropList;
                infoBinding.tvPropTitle.setTextColor(Color.parseColor("#333333"));
                infoBinding.vProp.setVisibility(View.VISIBLE);
                infoBinding.tvGiftTitle.setTextColor(Color.parseColor("#666666"));
                infoBinding.vGift.setVisibility(View.INVISIBLE);
            } else if (position == 1) {
                curRView = rvGiftList;
                infoBinding.tvGiftTitle.setTextColor(Color.parseColor("#333333"));
                infoBinding.vGift.setVisibility(View.VISIBLE);
                infoBinding.tvPropTitle.setTextColor(Color.parseColor("#666666"));
                infoBinding.vProp.setVisibility(View.INVISIBLE);
            }
            if (isReachPinned && (isCanScrollUp(curRView) || isCanScrollDown(curRView))) {
                isCanPinned = true;
            }
            //下面这种是另一种滚动方式，viewPager优先滚动
//            if (isCanScrollUp(curRView) || isCanScrollDown(curRView)) {
//                if (isReachPinned) {
//                    isCanPinned = true;
//                }
//                infoBinding.vpContent.setGetEvent(true);
//            } else {
//                infoBinding.vpContent.setGetEvent(false);
//            }
        }
    };

    //是否能向上滑动
    private boolean isCanScrollUp(RecyclerView recyclerView) {
        return recyclerView.canScrollVertically(1);
    }

    //是否能向下滑动
    private boolean isCanScrollDown(RecyclerView recyclerView) {
        return recyclerView.canScrollVertically(-1);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        int pinnedTitleHeight = infoBinding.llVpTitle.getHeight();
        int topbarHeight = infoBinding.rlNav.getHeight();

        topbarBottomY = ScreenUtil.getStatusBarHeight(this) + topbarHeight;

        int visibleHeight = infoBinding.vVisibleHeight.getHeight();

        int bottomHeight = infoBinding.llBottom.getHeight();
        ViewGroup.LayoutParams params = infoBinding.vpContent.getLayoutParams();
        params.height = visibleHeight - pinnedTitleHeight - topbarHeight;
        infoBinding.vpContent.setPadding(0, 0, 0, bottomHeight - ScreenUtil.dip2px(10));
        infoBinding.vpContent.requestLayout();

        super.onWindowFocusChanged(hasFocus);
    }

    /**
     * 获取房间信息
     *
     * @param userInfo
     */
    private void requestRoomInfo(UserInfo userInfo) {
        if (null == userInfo) {
            return;
        }
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("targetUid", String.valueOf(userInfo.getUid()));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        param.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        OkHttpManager.getInstance().getRequest(UriProvider.getHisHomeRoom(),
                param, new OkHttpManager.MyCallBack<ServiceResult<RoomInfo>>() {

                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onResponse(ServiceResult<RoomInfo> response) {
                        if (response != null && response.isSuccess()) {
                            RoomInfo roomInfo = response.getData();
                            if (roomInfo == null) {
                                //该用户还未开房间
                                infoBinding.llTaHome.setVisibility(View.GONE);
                                return;
                            }
                            mRoomInfo = roomInfo;
                            try {
                                boolean isMySelf = (userInfo.getUid() == CoreManager.getCore(IAuthCore.class).getCurrentUid());
                                boolean roomValid = roomInfo.getType() == RoomInfo.ROOMTYPE_HOME_PARTY || roomInfo.getType() == RoomInfo.ROOMTYPE_SINGLE_AUDIO || roomInfo.getType() == RoomInfo.ROOMTYPE_MULTI_AUDIO|| roomInfo.getType() == RoomInfo.ROOMTYPE_NEW_AUCTION;
                                infoBinding.llTaHome.setVisibility((roomValid && !isMySelf) ? View.VISIBLE : View.GONE);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
    }

//------------------礼物更新操作----------------------------------------------

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onGetUserRoom(RoomInfo roomInfo) {
        currUserInRoomUId = roomInfo.getUid();
        currUserInRoomType = roomInfo.getType();
        if (currUserInRoomUId == (BasicConfig.isDebug ? PublicChatRoomController.devRoomId : PublicChatRoomController.formalRoomId)) {
            //不再房间，不展示去找TA
            return;
        }
        if (roomInfo.getUid() > 0) {
            infoBinding.llFindTa.setVisibility(View.VISIBLE);
        }
    }

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onGetUserRoomFail(String msg) {
        currUserInRoomUId = 0L;
        infoBinding.llFindTa.setVisibility(View.GONE);
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onIsLiked(boolean islike, long uid) {
        infoBinding.setIsLike(islike);
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onIsLikedFail(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onCanceledPraise(long likedUid) {
        toast(R.string.fan_cancel_success);
        getDialogManager().dismissDialog();
        infoBinding.setIsLike(false);
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onCanceledPraiseFaith(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

//----------------------新手引导-----------------------------------------

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraise(long likedUid) {
        getDialogManager().dismissDialog();
        toast(R.string.fan_success);
        infoBinding.setIsLike(true);
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraiseFaith(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    private void requestPropData() {
        propList.clear();
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(userId));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.getCarListUrl(), param, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                LogUtils.d(TAG, "requestPropData getCarListUrl onError:" + e.getMessage());
                e.printStackTrace();
            }

            @Override
            public void onResponse(Json response) {
                LogUtils.d(TAG, "requestPropData getCarListUrl onResponse json:" + response);
                if (200 == response.num("code")) {
                    List<Json> data = response.jlist("data");
                    if (null != data && data.size() > 0) {
                        for (Json json : data) {
                            propList.add(new UserPropInfo(json.str("picUrl"), json.str("carName")));
                        }
                        propAdapter.setUserPropInfoList(propList);
                        rvPropList.setVisibility(View.VISIBLE);
                        llPropEmpty.setVisibility(View.GONE);
                    }

                    if (!ListUtils.isListEmpty(propList)) {
                        infoBinding.tvPropTitle.setText("道具 ".concat(String.valueOf(propList.size())));
                    } else {
                        infoBinding.tvPropTitle.setText("道具");
                    }

                }
            }
        });

        param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(userId));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.getHeadWearListUrl(), param, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                LogUtils.d(TAG, "requestPropData getHeadWearListUrl onError:" + e.getMessage());
                e.printStackTrace();
            }

            @Override
            public void onResponse(Json response) {
                LogUtils.d(TAG, "requestPropData getHeadWearListUrl onResponse json:" + response);
                if (200 == response.num("code")) {
                    List<Json> data = response.jlist("data");
                    if (null != data && data.size() > 0) {
                        for (Json json : data) {
                            propList.add(new UserPropInfo(json.str("picUrl"), json.str("headwearName")));
                        }
                        propAdapter.setUserPropInfoList(propList);
                        rvPropList.setVisibility(View.VISIBLE);
                        llPropEmpty.setVisibility(View.GONE);
                    }

                    if (!ListUtils.isListEmpty(propList)) {
                        infoBinding.tvPropTitle.setText("道具 ".concat(String.valueOf(propList.size())));
                    } else {
                        infoBinding.tvPropTitle.setText("道具");
                    }
                }
            }
        });
    }

//------------------------------拉黑和取消拉黑操作-----------------------------------------

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestGiftWall(long uid, int orderType, List<
            GiftWallInfo> giftWallInfoList) {
        LogUtils.d(TAG, "onRequestGiftWall-uid:" + uid + " orderType:" + orderType);
        if (null != giftWallInfoList) {
            LogUtils.d(TAG, "onRequestGiftWall-giftWallInfoList.length:" + giftWallInfoList.size());
        }

        if (1 == orderType) {
            giftWallListOrderByCount = giftWallInfoList;
            boolean hasGift = null != giftWallInfoList && giftWallInfoList.size() > 0;
            llGiftEmpty.setVisibility(hasGift ? View.GONE : View.VISIBLE);
            rvGiftList.setVisibility(hasGift ? View.VISIBLE : View.GONE);
            if (hasGift) {
                infoBinding.tvGiftTitle.setText("礼物 ".concat(String.valueOf(giftWallInfoList.size())));
            } else {
                infoBinding.tvGiftTitle.setText("礼物");
            }
            giftWallAdapter.setGiftWallInfoList(giftWallInfoList);
            giftWallAdapter.notifyDataSetChanged();
        } else if (2 == orderType) {
            giftWallListOrderByPrice = giftWallInfoList;
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestGiftWallFail(long uid, int orderType, String msg) {
        LogUtils.d(TAG, "onRequestGiftWall-uid:" + uid + " orderType:" + orderType + " msg:" + msg);
        toast(msg);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getDialogManager().dismissDialog();
        releaseAudioRes();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (audioManager != null) {
            //audioManager.release();会停止player播放器并设置listener为null
            audioManager.release();
        }
    }

    public void checkShouldShowGuideView(boolean isUserSelf) {
        //判断是否需要显示新手引导
//        UserInfo uinfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
//        if (null != uinfo && null != uinfo.getGuideState()) {
//            if (isUserSelf) {
//                if ("0".equals(uinfo.getGuideState().getType2())) {
//                    showGuideView(isUserSelf);
//                }
//            } else {
//                if ("0".equals(uinfo.getGuideState().getType1())) {
//                    showGuideView(isUserSelf);
//                }
//            }
//        }
    }

    /**
     * 显示新手引导
     *
     * @param isUserSelf
     */
    public void showGuideView(boolean isUserSelf) {
        LogUtils.d(TAG, "showGuideView-isUserSelf:" + isUserSelf);
        if (userInfoGuideDialog == null) {
            userInfoGuideDialog = new NoviceGuideDialog(this,
                    isUserSelf ? NoviceGuideDialog.GuideType.USERINFO_SELF
                            : NoviceGuideDialog.GuideType.USERINFO_OTHER, null);
        }

        userInfoGuideDialog.show();

    }

    /**
     * 展示拉黑操作对话框
     */
    private void showDefriendDialog() {
        boolean hasBlacked = CoreManager.getCore(IIMFriendCore.class).isUserInBlackList(String.valueOf(userId));
        LogUtils.d(TAG, "showDefriendDialog-userId:" + userId + " hasBlacked:" + hasBlacked);
        DialogManager.AbsOkDialogListener cancelBlackListener = new DialogManager.AbsOkDialogListener() {
            @Override
            public void onOk() {
                CoreManager.getCore(IIMFriendCore.class).removeFromBlackList(String.valueOf(userId));
            }
        };
        DialogManager.AbsOkDialogListener blackListener = new DialogManager.AbsOkDialogListener() {
            @Override
            public void onOk() {
                CoreManager.getCore(IIMFriendCore.class).addToBlackList(String.valueOf(userId));
            }
        };
        getDialogManager().showOkCancelDialog(
                getResources().getString(hasBlacked ? R.string.user_info_cancel_black_tips : R.string.user_info_black_tips),
                true, hasBlacked ? cancelBlackListener : blackListener);
    }

    @CoreEvent(coreClientClass = IIMFriendCoreClient.class)
    public void removeBlackListSuccess() {
        toast("已取消拉黑");
    }

    @CoreEvent(coreClientClass = IIMFriendCoreClient.class)
    public void addBlackListSuccess() {
        toast("已拉黑");
    }

    @Override
    public void onNeedExitRoom() {
        LogUtils.d(TAG, "onNeedExitRoom");
        BaseRoomServiceScheduler.exitRoom(null);
    }

    private void releaseAudioRes() {
        if (audioManager != null) {
            //audioManager.release();会停止player播放器并设置listener为null
            audioManager.release();
            if (audioPlayer != null) {
                audioPlayer = null;
            }
            audioManager = null;
        }
    }

    /**
     * 试听刚才的录音文件
     */
    private void tryToListen() {
        if (null == audioManager) {
            audioManager = AudioPlayAndRecordManager.getInstance();
        }

        if (!audioManager.isPlaying()) {
            if (null != audioPlayer) {
                audioPlayer.stop();
                audioPlayer.setOnPlayListener(null);
                audioPlayer = null;
            }
            if (null != userInfo) {
                //-->修复 Use of stream types is deprecated for operations other than volume control，
                // See the documentation of setAudioStreamType() for what to use
                // instead with android.media.AudioAttributes to qualify your playback use case
                // 也即 "点击 录音时长 弹出上传录音界面，再点击其它区域 关闭 上传页面，然后点击 播放 按钮  —— 一直播放，无法停止"--->
                audioPlayer = audioManager.getAudioPlayer(this, null, onPlayListener);
                //<---修复代码分割线，实际含义为每个界面每次播放都要停止上次的audioPlayer并重新初始化保证场景唯一--
                audioPlayer.setDataSource(userInfo.getUserVoice());
                audioManager.play();
                Glide.with(this).asGif().load(R.drawable.anim_user_info_voice).into(infoBinding.imvVoice);
            }
        } else {
            audioManager.stopPlay();
            infoBinding.imvVoice.setImageDrawable(getResources().getDrawable(R.mipmap.icon_userinfo_voice));
        }
    }

    @Override
    public void onScrollChanged(NestedScrollView view, int x, int y, int oldx, int oldy) {

    }

    public class BannerAdapter extends StaticPagerAdapter {
        private Context context;
        private List<String> bannerInfoList;
        private LayoutInflater mInflater;

        public BannerAdapter(List<String> bannerInfoList, Context context) {
            this.context = context;
            this.bannerInfoList = bannerInfoList;
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(ViewGroup container, int position) {
            final String bannerEnitity = bannerInfoList.get(position);
            ImageView imgBanner = (ImageView) mInflater.inflate(R.layout.banner_page_item_userinfo, container, false);
            ImageLoadUtils.loadImage(context, bannerEnitity, imgBanner);
            return imgBanner;
        }

        @Override
        public int getCount() {
            if (bannerInfoList == null) {
                return 0;
            } else {
                return bannerInfoList.size();
            }
        }
    }

    @Override
    public boolean isStatusBarDarkFont() {
        return false;
    }
}
