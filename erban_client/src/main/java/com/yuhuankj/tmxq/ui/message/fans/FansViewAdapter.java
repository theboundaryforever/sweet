package com.yuhuankj.tmxq.ui.message.fans;

import android.view.View;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCore;
import com.tongdaxing.xchat_core.user.bean.FansInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

/**
 * Created by chenran on 2017/10/2.
 */

public class FansViewAdapter extends BaseQuickAdapter<FansInfo, BaseViewHolder> {

    private OnItemClickListener onItemClickListener;
    int imgWidthHeight = 0;

    public interface OnItemClickListener {
        void onItemClick(FansInfo fansInfo);

        void onAttentionBtnClick(FansInfo fansInfo);
    }

    public void setRylListener(OnItemClickListener onClickListener) {
        onItemClickListener = onClickListener;
    }

    public FansViewAdapter(List<FansInfo> fansInfoList) {
        super(R.layout.fans_list_item, fansInfoList);
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, final FansInfo fansInfo) {
        if (fansInfo == null) {
            return;
        }
        baseViewHolder.setText(R.id.tv_userName, fansInfo.getNick())
                .setVisible(R.id.tv_state, fansInfo.isValid())
                .setVisible(R.id.view_line, baseViewHolder.getLayoutPosition() != getItemCount() - 1)
                .setVisible(R.id.attention_img, !CoreManager.getCore(IIMFriendCore.class).isMyFriend(fansInfo.getUid() + ""))
                .setOnClickListener(R.id.rly, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onItemClickListener != null) {
                            onItemClickListener.onItemClick(fansInfo);
                        }
                    }
                })
                .setOnClickListener(R.id.attention_img, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onItemClickListener != null) {
                            onItemClickListener.onAttentionBtnClick(fansInfo);
                        }
                    }
                })
        ;
        ImageView civAvatar = baseViewHolder.getView(R.id.civAvatar);
        if (GlideContextCheckUtil.checkContextUsable(civAvatar.getContext())) {
            if (0 == imgWidthHeight) {
                imgWidthHeight = DisplayUtility.dp2px(civAvatar.getContext(), 60);
            }
            ImageLoadUtils.loadBannerRoundBgWithOutPlaceHolder(civAvatar.getContext(),
                    ImageLoadUtils.toThumbnailUrl(imgWidthHeight, imgWidthHeight, fansInfo.getAvatar()),
                    civAvatar,
                    imgWidthHeight,
                    R.drawable.bg_default_cover_round_placehold_size60);
        }
    }
}
