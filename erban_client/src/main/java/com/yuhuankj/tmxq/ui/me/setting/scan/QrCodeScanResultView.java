package com.yuhuankj.tmxq.ui.me.setting.scan;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

public interface QrCodeScanResultView<T extends AbstractMvpPresenter> extends IMvpBaseView {

    void onQrCodeLogin(boolean isSuccess, String msg);

}
