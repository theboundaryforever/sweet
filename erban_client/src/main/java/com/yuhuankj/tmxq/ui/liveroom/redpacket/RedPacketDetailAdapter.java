package com.yuhuankj.tmxq.ui.liveroom.redpacket;

import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.netease.nim.uikit.common.util.sys.TimeUtil;
import com.yuhuankj.tmxq.R;

import java.util.List;

/**
 * @author liaoxy
 * @Description:红包记录详情页adapter
 * @date 2019/1/17 15:54
 */
public class RedPacketDetailAdapter extends BaseQuickAdapter<RedPacketDetaiItemlEnitity, BaseViewHolder> {

    public RedPacketDetailAdapter(List data) {
        super(R.layout.item_red_packet_detail, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, RedPacketDetaiItemlEnitity item) {
        ImageView imvPortrait = helper.getView(R.id.imvPortrait);
        TextView tvNick = helper.getView(R.id.tvNick);
        TextView tvTime = helper.getView(R.id.tvTime);
        TextView tvStatus = helper.getView(R.id.tvStatus);

        Glide.with(mContext).load(item.getAvatar()).into(imvPortrait);
        tvNick.setText(item.getNick());
        try {
            String timeStr = TimeUtil.getDateTimeString(Long.valueOf(item.getReceiveDate()), "yyyy.MM.dd HH:mm:ss");
            tvTime.setText(timeStr);
        } catch (Exception e) {
            e.printStackTrace();
            tvTime.setText(item.getReceiveDate());
        }
        tvStatus.setText(item.getGoldNum() + "金币");
    }
}