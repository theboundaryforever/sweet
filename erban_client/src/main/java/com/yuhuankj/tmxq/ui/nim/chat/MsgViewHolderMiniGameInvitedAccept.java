package com.yuhuankj.tmxq.ui.nim.chat;

import com.netease.nim.uikit.common.ui.recyclerview.adapter.BaseMultiItemFetchLoadAdapter;
import com.netease.nim.uikit.session.viewholder.MsgViewHolderBase;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.bean.MiniGameInvitedAcceptInfo;
import com.tongdaxing.xchat_core.im.custom.bean.MiniGameInvitedAcceptAttachment;

/**
 * 小游戏邀请接受通知
 */
public class MsgViewHolderMiniGameInvitedAccept extends MsgViewHolderBase {

    private static final String TAG = "MsgViewHolderMiniGameInvitedAccept";

    public MsgViewHolderMiniGameInvitedAccept(BaseMultiItemFetchLoadAdapter adapter) {
        super(adapter);
    }

    @Override
    protected boolean isShowHeadImage() {
        return false;
    }

    @Override
    protected boolean isShowBubble() {
        return false;
    }

    @Override
    protected int getContentResId() {
        return com.netease.nim.uikit.R.layout.view_minigame_msg_gone;
    }

    @Override
    protected void inflateContentView() {
    }

    @Override
    protected void bindContentView() {
        MiniGameInvitedAcceptInfo info = ((MiniGameInvitedAcceptAttachment) message.getAttachment()).getDataInfo();
        if (info == null) {
            LogUtils.e(TAG, "info is null");
            return;
        }
        int acceptResult = info.getAcceptInvt();
        MsgViewHolderMiniGameInvited gameInvited = MsgViewHolderMiniGameInvited.get(info.getRoomid());
        if (!isReceivedMessage()) {//我是发送方
            if (acceptResult == 0) {//拒绝邀请
                if (gameInvited != null) {
                    gameInvited.setEndStatus(MsgViewHolderMiniGameInvited.REFUSE, "游戏被拒绝", "失效");
                }
            } else {
                if (gameInvited != null) {
                    gameInvited.setEndStatus(MsgViewHolderMiniGameInvited.AGREE, "已同意", "失效");
                }
            }
        } else {
            if (acceptResult == 0) {//拒绝邀请
                if (gameInvited != null) {
                    gameInvited.setEndStatus(MsgViewHolderMiniGameInvited.REFUSE, "游戏被拒绝", "失效");
                }
            } else {
                if (gameInvited != null) {
                    gameInvited.setEndStatus(MsgViewHolderMiniGameInvited.AGREE, "已同意", "失效");
                }
            }
        }
    }
}
