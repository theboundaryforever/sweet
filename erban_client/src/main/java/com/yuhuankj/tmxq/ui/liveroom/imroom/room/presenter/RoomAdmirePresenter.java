package com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomModel;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomAdditional;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.base.presenter.BaseMvpPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IRoomAdmireView;

/**
 * 房间页面的presenter
 */
public class RoomAdmirePresenter<I extends IMvpBaseView> extends BaseMvpPresenter<IRoomAdmireView> {

    private final String TAG = RoomAdmirePresenter.class.getSimpleName();

    private IMRoomModel imRoomModel;

    public RoomAdmirePresenter() {
        imRoomModel = new IMRoomModel();
    }

    /**
     * 为房主点赞
     */
    public void admireRoomOwner() {
        LogUtils.d(TAG, "admireRoomOwner");
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (null == roomInfo) {
            return;
        }
        imRoomModel.admireRoomOwner(roomInfo.getRoomId(), roomInfo.getType(), new HttpRequestCallBack<String>() {
            @Override
            public void onSuccess(String message, String response) {
                LogUtils.d(TAG, "admireRoomOwner--onSuccess message:" + message + " response:" + response);
                RoomAdditional roomAdditional = RoomDataManager.get().getAdditional();
                if (null != roomAdditional) {
                    //点赞成功，本地次数减1
                    int mAdmireCount = roomAdditional.getAdmireCount();
                    mAdmireCount -= 1;
                    if (mAdmireCount < 0) {
                        mAdmireCount = 0;
                    }
                    roomAdditional.setAdmireCount(mAdmireCount);
                    RoomDataManager.get().setAdditional(roomAdditional);
                }

                //刷新界面
                if (null != getMvpView()) {
                    getMvpView().updateRoomAdmireStatus();
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                LogUtils.d(TAG, "admireRoomOwner--onFailure code:" + code + " msg" + msg);
                if (code == 3003) {
                    //剩余点赞次数不够
                    RoomAdditional roomAdditional = RoomDataManager.get().getAdditional();
                    if (null != roomAdditional) {
                        roomAdditional.setAdmireCount(0);
                        RoomDataManager.get().setAdditional(roomAdditional);
                    }
                    //刷新界面
                    if (null != getMvpView()) {
                        getMvpView().updateRoomAdmireStatus();
                    }
                }
            }
        });
    }

    /**
     * 获取房间点赞信息
     */
    public void getRoomAdmireInfo() {
        LogUtils.d(TAG, "getRoomAdmireInfo");
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (null == roomInfo) {
            return;
        }
        imRoomModel.getRoomAdmireCount(roomInfo.getRoomId(), roomInfo.getType(), new HttpRequestCallBack<Integer>() {
            @Override
            public void onSuccess(String message, Integer response) {
                LogUtils.d(TAG, "getRoomAdmireCount--onSuccess message:" + message + " response:" + response);
                //更新本地数据，并刷新界面展示
                RoomAdditional roomAdditional = RoomDataManager.get().getAdditional();
                if (null != roomAdditional) {
                    roomAdditional.setAdmireCount(response);
                    RoomDataManager.get().setAdditional(roomAdditional);
                }
                if (null != getMvpView()) {
                    getMvpView().updateRoomAdmireStatus();
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                LogUtils.d(TAG, "getRoomAdmireCount--onFailure code:" + code + " msg" + msg);
            }
        });
    }

}
