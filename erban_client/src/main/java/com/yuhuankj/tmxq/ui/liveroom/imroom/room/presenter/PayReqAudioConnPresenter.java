package com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter;

import android.text.TextUtils;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomModel;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.AudioConnPayInfoResp;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IPayReqAudioConnView;

/**
 * 房间页面的presenter
 */
public class PayReqAudioConnPresenter<I extends IMvpBaseView> extends AbstractMvpPresenter<IPayReqAudioConnView> {

    private final String TAG = PayReqAudioConnPresenter.class.getSimpleName();

    private IMRoomModel imRoomModel;

    public PayReqAudioConnPresenter() {
        imRoomModel = new IMRoomModel();
    }

    public void getRoomApplyInfo() {
        LogUtils.d(TAG, "getRoomApplyInfo");
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (null == roomInfo) {
            return;
        }

        imRoomModel.getRoomApplyInfo(roomInfo.getRoomId(), new HttpRequestCallBack<AudioConnPayInfoResp>() {

            @Override
            public void onSuccess(String message, AudioConnPayInfoResp response) {
                LogUtils.d(TAG, "getRoomApplyInfo-->onSuccess message:" + message
                        + " highest:" + response.highest + " lowest:" + response.lowest
                        + " goldNum:" + response.goldNum);
                if (null == getMvpView()) {
                    return;
                }
                getMvpView().refreshAudioConnPayHistoryInfo(response.highest, response.lowest);
                getMvpView().refreshGoldNum(response.goldNum);
                getMvpView().toast("");
            }

            @Override
            public void onFailure(int code, String msg) {
                LogUtils.d(TAG, "getRoomApplyInfo-->onFailure code:" + code + " msg:" + code);
                if (null == getMvpView()) {
                    return;
                }
                getMvpView().toast(msg);
            }
        });
    }

    public void reqAudioConn(String applyContent, long goldNum) {
        LogUtils.d(TAG, "reqAudioConn-applyContent:" + applyContent + " goldNum:" + goldNum);
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (null == roomInfo) {
            return;
        }
        imRoomModel.reqAudioConn(applyContent, goldNum, roomInfo.getRoomId(), new HttpRequestCallBack<String>() {
            @Override
            public void onSuccess(String message, String response) {
                LogUtils.d(TAG, "reqAudioConn-->onSuccess message:" + message + " response:" + response);
                if (null != getMvpView()) {
                    //提示
                    if (TextUtils.isEmpty(response)) {
                        getMvpView().toast(response);
                    }
                    getMvpView().finishForResult();
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                LogUtils.d(TAG, "reqAudioConn-->onFailure code:" + code + " msg:" + msg);
                if (null == getMvpView()) {
                    return;
                }

                if (!TextUtils.isEmpty(msg)) {
                    getMvpView().toast(msg);
                }
            }
        });
    }
}
