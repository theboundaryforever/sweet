package com.yuhuankj.tmxq.ui.home.model;

import com.tongdaxing.xchat_core.liveroom.im.model.bean.GameRoomEnitity;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.SingleAudioRoomEnitity;
import com.yuhuankj.tmxq.ui.home.game.GameBannerEnitity;

import java.io.Serializable;
import java.util.List;

/**
 * @author weihaitao
 * @date 2019/5/21
 */
public class NewChannelHomeResponse implements Serializable {

    /**
     * 顶部banner
     */
    private List<GameBannerEnitity> banners;

    /**
     * 中部单人音频直播间列表
     */
    private List<SingleAudioRoomEnitity> hotRooms;

    /**
     * 底部聊天室列表
     */
    private List<GameRoomEnitity> listRoom;

    public List<GameBannerEnitity> getBanners() {
        return banners;
    }

    public void setBanners(List<GameBannerEnitity> banners) {
        this.banners = banners;
    }

    public List<SingleAudioRoomEnitity> getHotRooms() {
        return hotRooms;
    }

    public void setHotRooms(List<SingleAudioRoomEnitity> hotRooms) {
        this.hotRooms = hotRooms;
    }

    public List<GameRoomEnitity> getListRoom() {
        return listRoom;
    }

    public void setListRoom(List<GameRoomEnitity> listRoom) {
        this.listRoom = listRoom;
    }
}
