package com.yuhuankj.tmxq.ui.liveroom.nimroom.adapter;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tongdaxing.erban.libcommon.utils.TimeUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.bindadapter.BaseAdapter;
import com.yuhuankj.tmxq.base.bindadapter.BindingViewHolder;
import com.yuhuankj.tmxq.databinding.ListItemRoomBlackBinding;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.callback.DataKey;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.UIUtil;

import java.util.Map;

/**
 * Created by huangmeng1 on 2018/1/18.
 */

public class RoomBlackAdapter extends BaseAdapter<ChatRoomMember> {
    public RoomBlackAdapter(int layoutResId, int brid) {
        super(layoutResId, brid);
    }

    private RoomBlackDelete roomBlackDelete;

    public void setRoomBlackDelete(RoomBlackDelete roomBlackDelete) {
        this.roomBlackDelete = roomBlackDelete;
    }

    @Override
    protected void convert(BindingViewHolder helper, ChatRoomMember item) {
        super.convert(helper, item);
        ListItemRoomBlackBinding binding = (ListItemRoomBlackBinding) helper.getBinding();
        // 黑名单新增数据
        long time = -1;
        String adminName = "";
        Map<String, Object> extension = item.getExtension();
        if (extension != null) {
            Object data = extension.get(DataKey.params);
            Json json = Json.parse(data + "");
            time = json.num_l(DataKey.time);
            adminName = json.str(DataKey.adminName);
            if (!TextUtils.isEmpty(adminName) && adminName.length() > 6) {
                adminName = binding.tvMarkAdmin.getContext().getResources().getString(R.string.nick_length_max_six, adminName.substring(0, 6));
            }
            if (!TextUtils.isEmpty(adminName)) {
                binding.tvMarkAdmin.setText(binding.tvMarkAdmin.getContext().getResources().getString(R.string.black_list_admin_name, adminName));
            }
            if (time > 0) {
                binding.tvMarkTime.setText(TimeUtils.getTimeStringFromMillis(time));
            }

        }
        String nick = item.getNick();
        if (!TextUtils.isEmpty(nick) && nick.length() > 6) {
            nick = binding.tvUserName.getContext().getResources().getString(R.string.nick_length_max_six, nick.substring(0, 6));
        }
        binding.tvUserName.setText(nick);
        ViewGroup.LayoutParams layoutParams = binding.container.getLayoutParams();
        layoutParams.width = UIUtil.getScreenWidth(mContext);
        binding.container.setLayoutParams(layoutParams);
        binding.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (roomBlackDelete != null) {
                    roomBlackDelete.onDeleteClick(item);
                }
            }
        });

    }

    public interface RoomBlackDelete {
        void onDeleteClick(ChatRoomMember chatRoomMember);
    }
}
