package com.yuhuankj.tmxq.ui.user.bosonFriend;

import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.libcommon.utils.TimeUtils;
import com.tongdaxing.xchat_core.user.bean.BosonFriendEnitity;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

public class BosonFriendHistoryAdapter extends BaseQuickAdapter<BosonFriendEnitity, BaseViewHolder> {

    public BosonFriendHistoryAdapter(List data) {
        super(R.layout.item_boson_friend_history, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, BosonFriendEnitity item) {
        ImageView imvPortrait = helper.getView(R.id.imvPortrait);
        ImageView imvHeadwear = helper.getView(R.id.imvHeadwear);
        TextView tvUserName = helper.getView(R.id.tvUserName);
        TextView tvDays = helper.getView(R.id.tvDays);
        TextView tvTimeRange = helper.getView(R.id.tvTimeRange);

        try {
            ImageLoadUtils.loadCircleImage(mContext, item.getAvatar(), imvPortrait, R.drawable.default_user_head);
            ImageLoadUtils.loadCircleImage(mContext, item.getLevelIcon(), imvHeadwear, R.drawable.ic_bosonfriend_level_0);
            tvUserName.setText(item.getNick());
            tvTimeRange.setText(TimeUtils.getDateTimeString(item.getCreateTime(), "yyyy.M.d") + "至" + TimeUtils.getDateTimeString(item.getRecordTime(), "yyyy.M.d"));
            tvDays.setText(item.getDay() + "天" + item.getHour() + "时");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}