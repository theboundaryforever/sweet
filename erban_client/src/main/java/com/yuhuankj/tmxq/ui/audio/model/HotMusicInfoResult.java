package com.yuhuankj.tmxq.ui.audio.model;

import com.tongdaxing.xchat_core.music.bean.HotMusicInfo;

import java.util.List;

public class HotMusicInfoResult {

    public int count;
    public List<HotMusicInfo> list;
}
