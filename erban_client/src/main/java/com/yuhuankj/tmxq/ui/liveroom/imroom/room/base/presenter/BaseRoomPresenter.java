package com.yuhuankj.tmxq.ui.liveroom.imroom.room.base.presenter;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomAdmireTimeCounter;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomOwnerLiveTimeCounter;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMemberComeInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;

/**
 * 基础的房间presenter
 *
 * @author zeda
 */
public abstract class BaseRoomPresenter<V extends IMvpBaseView> extends BaseMvpPresenter<V> {

    /**
     * 获取当前房间的房间信息
     */
    public RoomInfo getServerRoomInfo() {
        return RoomDataManager.get().getCurrentRoomInfo();
    }

    /**
     * 获取当前房间的房间信息
     */
    public RoomInfo getCurrRoomInfo() {
        return RoomDataManager.get().getCurrentRoomInfo();
    }

    /**
     * 获取进入房间的用户信息
     */
    public IMRoomMemberComeInfo getAndRemoveFirstMemberComeInfo() {
        return RoomDataManager.get().getAndRemoveFirstMemberComeInfo();
    }

    /**
     * 获取进入房间用户的size
     */
    public int getMemberComeSize() {
        return RoomDataManager.get().getMemberComeSize();
    }

    /**
     * 释放房间数据
     */
    public void releaseRoomData() {
        RoomDataManager.get().release();
        RoomOwnerLiveTimeCounter.getInstance().release();
        RoomAdmireTimeCounter.getInstance().release();
    }
}
