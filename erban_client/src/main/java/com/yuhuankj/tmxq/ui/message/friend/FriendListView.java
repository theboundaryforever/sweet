package com.yuhuankj.tmxq.ui.message.friend;

import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

import java.util.List;

public interface FriendListView<T extends AbstractMvpPresenter> extends IMvpBaseView {

    void showFriendList(List<NimUserInfo> nimUserInfos);

    void showFriendListEmptyView();


}
