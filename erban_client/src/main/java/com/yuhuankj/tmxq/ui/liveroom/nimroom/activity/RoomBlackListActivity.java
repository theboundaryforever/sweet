package com.yuhuankj.tmxq.ui.liveroom.nimroom.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.presenter.RoomBlackPresenter;
import com.tongdaxing.xchat_core.room.view.IRoomBlackView;
import com.yuhuankj.tmxq.BR;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.adapter.RoomBlackAdapter;

import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

/**
 * 黑名单
 *
 * @author chenran
 * @date 2017/10/11
 */
@CreatePresenter(RoomBlackPresenter.class)
public class RoomBlackListActivity extends BaseMvpActivity<IRoomBlackView, RoomBlackPresenter>
        implements IRoomBlackView, RoomBlackAdapter.RoomBlackDelete {

    private final String TAG = RoomBlackListActivity.class.getSimpleName();

    private TextView count;
    private RecyclerView recyclerView;
    private RoomBlackAdapter normalListAdapter;
    private long lastUserUpdateTime = 0;
    private int loadSize = 100;


    public static void start(Context context) {
        Intent intent = new Intent(context, RoomBlackListActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_black_list);
        initTitleBar("黑名单");
        initView();

        showLoading();
        LogUtils.d(TAG, "onCreate-loadData");
        lastUserUpdateTime = 0;
        getMvpPresenter().clearOldList();
        loadData();
    }

    private void loadData() {
        LogUtils.d(TAG, "loadData-lastUserUpdateTime:" + lastUserUpdateTime);
        //一次最多只能加载200条
        RoomInfo roomInfo = RoomServiceScheduler.getCurrentRoomInfo();
        if (null != roomInfo) {
            getMvpPresenter().queryRoomBlackList(loadSize, roomInfo.getRoomId(), lastUserUpdateTime);
        }
    }

    private void initView() {
        count = (TextView) findViewById(R.id.count);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        normalListAdapter = new RoomBlackAdapter(R.layout.list_item_room_black, BR.userInfo);
        normalListAdapter.setRoomBlackDelete(this);
        normalListAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                LogUtils.d(TAG, "onLoadMoreRequested-loadData");
                loadData();
            }
        }, recyclerView);
        recyclerView.setLayoutManager(new RecyclerViewNoBugLinearLayoutManager(this));
        recyclerView.setAdapter(normalListAdapter);
    }

    @Override
    public void onDeleteClick(final ChatRoomMember chatRoomMember) {
        if (null == chatRoomMember) {
            return;
        }
        getDialogManager().showOkCancelDialog(
                "是否将" + chatRoomMember.getNick() + "移除黑名单列表？",
                true,
                new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onOk() {
                        RoomInfo roomInfo = RoomServiceScheduler.getCurrentRoomInfo();
                        if (roomInfo != null) {
                            getMvpPresenter().removeUserFromBlackList(Long.valueOf(
                                    chatRoomMember.getAccount()), roomInfo.getRoomId());
                        }
                    }
                });
    }

    @Override
    public View.OnClickListener getLoadListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtils.d(TAG, "onClick-loadData");
                showLoading();
                loadData();
            }
        };
    }

    private void dealRoomBlackListResponse(List<ChatRoomMember> chatRoomMemberList) {
        hideStatus();
        if (chatRoomMemberList != null) {
            normalListAdapter.loadMoreComplete();
            if (chatRoomMemberList.size() < loadSize) {
                normalListAdapter.loadMoreEnd();
            }
        }
        if (chatRoomMemberList != null && chatRoomMemberList.size() > 0) {
            if (normalListAdapter.getData() != null && normalListAdapter.getData().size() > 0) {
                normalListAdapter.addData(chatRoomMemberList);
            } else {
                normalListAdapter.setNewData(chatRoomMemberList);
            }

            count.setText("黑名单" + normalListAdapter.getData().size() + "人");
            //根据最后的时间戳分页加载
            lastUserUpdateTime = chatRoomMemberList.get(chatRoomMemberList.size() - 1).getUpdateTime();
        } else if (normalListAdapter.getData() == null || normalListAdapter.getData().size() < 1) {
            showNoData("暂没有设置黑名单");
            count.setText("黑名单0人");
        }

        LogUtils.d(TAG, "dealRoomBlackListResponse-lastUserUpdateTime:" + lastUserUpdateTime);
    }


    private void dealRemoveBlackUserResponse(String account) {
        if (TextUtils.isEmpty(account)) {
            return;
        }
        List<ChatRoomMember> normalList = normalListAdapter.getData();
        if (!ListUtils.isListEmpty(normalList)) {
            hideStatus();
            ListIterator<ChatRoomMember> iterator = normalList.listIterator();
            for (; iterator.hasNext(); ) {
                if (Objects.equals(iterator.next().getAccount(), account)) {
                    iterator.remove();
                }
            }
            normalListAdapter.notifyDataSetChanged();
            count.setText("黑名单" + normalList.size() + "人");
            if (normalList.size() == 0) {
                showNoData("暂没有设置黑名单");
            }
        } else {
            showNoData("暂没有设置黑名单");
            count.setText("黑名单0人");
        }
        toast("操作成功");
    }

    @Override
    public void onGetRoomBlackList(boolean isSuccess, String message, List<ChatRoomMember> chatRoomMemberList) {
        if (isSuccess) {
            dealRoomBlackListResponse(chatRoomMemberList);
        } else {
            //        toast("操作失败，请重试");
        }
    }

    @Override
    public void onRemoveUserFromBlackList(boolean isSuccess, String message, String account) {
        if (isSuccess) {
            dealRemoveBlackUserResponse(account);
        } else {
            //        toast("操作失败，请重试");
            if (!TextUtils.isEmpty(message)) {
                toast(message);
            }
        }
    }
}
