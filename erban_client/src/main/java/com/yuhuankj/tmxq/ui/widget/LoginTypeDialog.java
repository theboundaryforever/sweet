package com.yuhuankj.tmxq.ui.widget;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.login.LoginPhoneActivity;
import com.yuhuankj.tmxq.ui.login.RegisterActivity;

/**
 * @author liaoxy
 * @Description:手机登录时登录注册选择框
 * @date 2019/4/10 18:34
 */
public class LoginTypeDialog extends AppCompatDialog implements View.OnClickListener {
    private Activity activity;
    private TextView tvLogin;
    private TextView tvRegister;

    public LoginTypeDialog(Activity activity) {
        super(activity, R.style.dialog);
        this.activity = activity;
        init();
    }

    private void init() {
        View view = LayoutInflater.from(activity).inflate(R.layout.dialog_login_type, null, false);
        setContentView(view);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = ScreenUtil.getScreenWidth(activity) - ScreenUtil.dip2px(17 * 2);
        params.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
        getWindow().setAttributes(params);
        setCanceledOnTouchOutside(true);

        tvLogin = view.findViewById(R.id.tvLogin);
        tvRegister = view.findViewById(R.id.tvRegister);

        tvLogin.setOnClickListener(this);
        tvRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == tvLogin) {
            activity.startActivity(new Intent(activity, LoginPhoneActivity.class));
            //点击登录
            StatisticManager.get().onEvent(getContext(),
                    StatisticModel.EVENT_ID_CLICK_LOGIN,
                    StatisticModel.getInstance().getUMAnalyCommonMap(getContext()));
        } else if (view == tvRegister) {
            activity.startActivity(new Intent(activity, RegisterActivity.class));
            //点击注册
            StatisticManager.get().onEvent(getContext(),
                    StatisticModel.EVENT_ID_CLICK_REGISTE,
                    StatisticModel.getInstance().getUMAnalyCommonMap(getContext()));
        }
        dismiss();
    }

    @Override
    public void show() {
        try {
            super.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dismiss() {
        try {
            super.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
