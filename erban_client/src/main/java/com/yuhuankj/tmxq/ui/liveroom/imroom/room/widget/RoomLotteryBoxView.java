package com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import com.tongdaxing.erban.libcommon.utils.TouchAndClickOperaUtils;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;

/**
 * 创建者      魏海涛
 * 创建时间    2019年4月25日 18:12:44
 * 描述        房间内房间公告界面封装view
 *
 * @author dell
 */
@SuppressLint("AppCompatCustomView")
public class RoomLotteryBoxView extends ImageView {

    private final String TAG = RoomLotteryBoxView.class.getSimpleName();

    private TouchAndClickOperaUtils touchAndClickOperaUtils1;
    private TouchAndClickOperaUtils.OnQuickClickListener onQuickClickListener;

    public RoomLotteryBoxView(Context context) {
        this(context, null);
    }

    public RoomLotteryBoxView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoomLotteryBoxView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setOnQuickClickListener(TouchAndClickOperaUtils.OnQuickClickListener onQuickClickListener) {
        this.onQuickClickListener = onQuickClickListener;
    }

    public void initialView() {
        if (RoomDataManager.get().getAdditional() != null) {
            if (RoomDataManager.get().getAdditional().isLotteryBoxOption()) {
                setVisibility(View.VISIBLE);
                initRoomLotteryBox();
            } else {
                setVisibility(View.GONE);
            }
        }
    }

    /**
     * 宝箱图标控件初始化
     */
    private void initRoomLotteryBox() {
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onQuickClickListener) {
                    onQuickClickListener.onQuickClick();
                }
            }
        });
    }

    public void release() {
        if (null != touchAndClickOperaUtils1) {
            touchAndClickOperaUtils1.setListener(null);
        }
        if (null != onQuickClickListener) {
            onQuickClickListener = null;
        }
        setOnClickListener(null);
    }
}
