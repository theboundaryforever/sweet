package com.yuhuankj.tmxq.ui.liveroom.imroom.room.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewStub;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.noober.background.view.BLTextView;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.gift.ComboGiftPublicScreenMsgSender;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftType;
import com.tongdaxing.xchat_core.liveroom.im.model.AudioConnTimeOutTipsQueueScheduler;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomOwnerLiveTimeCounter;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomEvent;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomQueueInfo;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.face.FaceReceiveInfo;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.audio.widget.MusicPlayerView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.gift.RoomGiftDialog;
import com.yuhuankj.tmxq.ui.liveroom.imroom.publicscreen.MessageView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.base.BaseRoomDetailFragment;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.dialog.AudioConnectListDialog;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.listener.OnMicroItemClickListener;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter.SingleAudioRoomPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IRoomDetailView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.ComingMsgView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.InputMsgView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.RecommRoomListView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.RoomAdmireView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.RoomHeatView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.RoomLotteryBoxView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.RoomMsgTipsView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.RoomNoticeView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.SingleAudioConnMicroView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.SingleAudioOwneMicrorView;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.widget.GiftV2View;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.widget.SingleBottomView;
import com.yuhuankj.tmxq.ui.webview.SingleAudioRoomLiveInfoWebViewActivity;
import com.yuhuankj.tmxq.ui.widget.RoomMoreOpearDialog;
import com.yuhuankj.tmxq.widget.Banner;

import java.util.List;


/**
 * 个人语音房的房间fragment
 *
 * @author zeda
 */
@CreatePresenter(SingleAudioRoomPresenter.class)
public class SingleAudioRoomFragment extends BaseRoomDetailFragment<IRoomDetailView,
        SingleAudioRoomPresenter, SingleAudioOwneMicrorView> implements IRoomDetailView,
        SingleAudioOwneMicrorView.OnSingleOwnerMicroViewClickListener, RoomHeatView.RoomHeatClickListener {

    private SingleAudioOwneMicrorView saomvOwnerMicInfo;
    private SingleAudioConnMicroView sacmvConnMicroView;
    private MessageView messageView;
    private GiftV2View giftView;
    private SingleBottomView bvOperaMenu;
    //    private ImageView roomBgIv;
    private InputMsgView inputMsgView;
    private View moreOperateBtn;
    private ViewStub mVsMusicPlayer;
    private MusicPlayerView mMusicPlayerView;
    private ComingMsgView cmvMsgView;
    private TextView tvFollowRoom;
    private RoomNoticeView rnvNotice;
    private RoomHeatView rhvVaule;
    private RoomLotteryBoxView rlbvLotteryBox;
    private Banner bannerActivity;
    private BLTextView tvLiveOpera;

    private RoomAdmireView ravAdmire;
    private AudioConnectListDialog audioConnectListDialog;

    private RecommRoomListView rrlvRecommRoomList;

    private RoomMsgTipsView rmtvMsgTipsList;

//    private RoomGiftComboSender rgcsComboView;
//    private ComboGiftView cgvGiftAnim;

    public static SingleAudioRoomFragment newInstance(long roomUid) {
        SingleAudioRoomFragment roomDetailFragment = new SingleAudioRoomFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(Constants.ROOM_UID, roomUid);
        roomDetailFragment.setArguments(bundle);
        return roomDetailFragment;
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_single_audio_room;
    }

    @Override
    public void onFindViews() {
        saomvOwnerMicInfo = mView.findViewById(R.id.saomvOwnerMicInfo);
        sacmvConnMicroView = mView.findViewById(R.id.sacmvConnMicroView);
        messageView = mView.findViewById(R.id.message_view);
        messageView.initMsgData();
        messageView.setAutoUpWhenSoftKeyShow(true);
        giftView = mView.findViewById(R.id.gift_view);
        giftView.setRoomType(RoomInfo.ROOMTYPE_SINGLE_AUDIO);
        bvOperaMenu = mView.findViewById(R.id.bvOperaMenu);
        bvOperaMenu.initState();
//        roomBgIv = mView.findViewById(R.id.iv_room_bg);
        inputMsgView = mView.findViewById(R.id.input_layout);
        moreOperateBtn = mView.findViewById(R.id.iv_room_operate);
        mVsMusicPlayer = mView.findViewById(R.id.vs_music_player);
        cmvMsgView = mView.findViewById(R.id.cmv_msg);
        tvFollowRoom = mView.findViewById(R.id.tvFollowRoom);
        rnvNotice = mView.findViewById(R.id.rnvNotice);
        rhvVaule = mView.findViewById(R.id.rhvVaule);
        rlbvLotteryBox = mView.findViewById(R.id.rlbvLotteryBox);
        bannerActivity = mView.findViewById(R.id.banner_activity);
        tvLiveOpera = mView.findViewById(R.id.tvLiveOpera);
        ravAdmire = mView.findViewById(R.id.ravAdmire);
        rmtvMsgTipsList = mView.findViewById(R.id.rmtvMsgTipsList);
        rrlvRecommRoomList = mView.findViewById(R.id.rrlvRecommRoomList);
//        rgcsComboView = mView.findViewById(R.id.rgcsComboView);
//        cgvGiftAnim = mView.findViewById(R.id.cgvGiftAnim);
//        if (CoreManager.getCore(VersionsCore.class).isGiftComboSwitchOpen()) {
//            cgvGiftAnim.setVisibility(View.VISIBLE);
//        } else {
//            cgvGiftAnim.setVisibility(View.GONE);
//        }
        super.onFindViews();
    }

    @Override
    public void onSetListener() {
        super.onSetListener();
        saomvOwnerMicInfo.setOnSingleOwnerMicroViewClickListener(this);
        sacmvConnMicroView.setOnVisibilityChangeListener(new SingleAudioConnMicroView.OnVisibilityChangeListener() {
            @Override
            public void onVisibilityChanged(int visibility) {
                if (null == messageView) {
                    return;
                }
                refreshMessageViewLp(visibility);
            }
        });
        sacmvConnMicroView.setOnMicroItemClickListener(getOnMicroItemClickListener());
        rhvVaule.setRoomHeatClickListener(this);
        rlbvLotteryBox.setOnQuickClickListener(this::showLotteryBoxDialog);
        tvLiveOpera.setOnClickListener(v -> doRoomOwnerLiveOpera());
        rmtvMsgTipsList.setOnRoomMsgTipsClickListener(new RoomMsgTipsView.OnRoomMsgTipsClickListener() {
            @Override
            public void onRoomMsgTipsClick(String msgTips) {
                getMvpPresenter().sendTextMsg(msgTips);
            }
        });
        rmtvMsgTipsList.setOnRoomMsgTipsViewVisibilityChangeListener(new RoomMsgTipsView.OnRoomMsgTipsViewVisibilityChangeListener() {
            @Override
            public void onRoomMsgTipsViewVisibilityChanged(int visibility) {
                if (null == messageView || null == ravAdmire || null == rlbvLotteryBox || null == getActivity()) {
                    return;
                }
                RelativeLayout.LayoutParams messageViewLp = (RelativeLayout.LayoutParams) messageView.getLayoutParams();
                RelativeLayout.LayoutParams ravAdmireLp = (RelativeLayout.LayoutParams) ravAdmire.getLayoutParams();
                RelativeLayout.LayoutParams rlbvLotteryBoxLp = (RelativeLayout.LayoutParams) rlbvLotteryBox.getLayoutParams();
                if (null == messageViewLp || null == ravAdmireLp || null == rlbvLotteryBoxLp) {
                    return;
                }

                if (visibility == View.VISIBLE) {
                    messageViewLp.addRule(RelativeLayout.ABOVE, R.id.rmtvMsgTipsList);
                    ravAdmireLp.bottomMargin = DisplayUtility.dp2px(getActivity(), 165);
                    rlbvLotteryBoxLp.bottomMargin = DisplayUtility.dp2px(getActivity(), 95);
                } else if (visibility == View.GONE) {
                    messageViewLp.addRule(RelativeLayout.ABOVE, R.id.bvOperaMenu);
                    ravAdmireLp.bottomMargin = DisplayUtility.dp2px(getActivity(), 130);
                    rlbvLotteryBoxLp.bottomMargin = DisplayUtility.dp2px(getActivity(), 60);
                }

                messageView.setLayoutParams(messageViewLp);
                ravAdmire.setLayoutParams(ravAdmireLp);
                rlbvLotteryBox.setLayoutParams(rlbvLotteryBoxLp);
            }
        });
        AudioConnTimeOutTipsQueueScheduler.getInstance().setOnConnTimeOutListener(new AudioConnTimeOutTipsQueueScheduler.OnConnTimeOutListener() {
            @Override
            public void onConnTimeOut() {
                toast(R.string.audio_connect_status_conn_timeout);
                if (null != audioConnectListDialog && audioConnectListDialog.isVisible()) {
                    audioConnectListDialog.getAudioConnReqList(true);
                }
            }
        });
    }

    private void refreshMessageViewLp(int visibility) {
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) messageView.getLayoutParams();
        if (null == lp) {
            return;
        }
        if (visibility == View.VISIBLE) {
            lp.addRule(RelativeLayout.BELOW, R.id.sacmvConnMicroView);
        } else {
            lp.addRule(RelativeLayout.BELOW, R.id.rhvVaule);
        }
        messageView.setLayoutParams(lp);
    }

    /**
     * 房主操作，开播、停播
     */
    public void doRoomOwnerLiveOpera() {
        getMvpPresenter().doRoomOwnerLiveOpera();
    }

    @NonNull
    @Override
    protected SingleAudioOwneMicrorView getMicroView() {
        return saomvOwnerMicInfo;
    }

    @NonNull
    @Override
    protected MessageView getMessageView() {
        return messageView;
    }

    @NonNull
    @Override
    protected GiftV2View getGiftView() {
        return giftView;
    }

    @NonNull
    @Override
    protected SingleBottomView getBottomView() {
        return bvOperaMenu;
    }

//    @Override
//    protected RoomGiftComboSender getRoomGiftComboSenderView() {
//        return rgcsComboView;
//    }

//    @Override
//    protected ComboGiftView getComboGiftView() {
//        return cgvGiftAnim;
//    }

    @Override
    protected TextView getFollowRoomView() {
        return tvFollowRoom;
    }

    @NonNull
    @Override
    protected ComingMsgView getComingMsgView() {
        return cmvMsgView;
    }

    @Override
    protected RoomAdmireView getRoomAdmireView() {
        return ravAdmire;
    }

    @Override
    protected Banner getBanner() {
        return bannerActivity;
    }

//    @NonNull
//    @Override
//    protected ImageView getRoomBgView() {
//        return roomBgIv;
//    }

    @NonNull
    @Override
    protected InputMsgView getInputMsgView() {
        return inputMsgView;
    }

    @NonNull
    @Override
    protected View getMoreOperateBtn() {
        return moreOperateBtn;
    }

    @Override
    public void receiveRoomEvent(IMRoomEvent roomEvent) {
        super.receiveRoomEvent(roomEvent);
        if (roomEvent != null && getMvpPresenter() != null) {
            int event = roomEvent.getEvent();
            switch (event) {
                case RoomEvent.ENTER_ROOM:
                    //[自己]进入房间
                case RoomEvent.ROOM_INFO_UPDATE:
                    //房间信息更新
                case RoomEvent.ROOM_MEMBER_IN:
                    //成员进入
                case RoomEvent.ROOM_MEMBER_EXIT:
                    //成员退出
                case RoomEvent.DOWN_MIC:
                    //下麦
                case RoomEvent.UP_MIC:
                    //上麦
                    onShowMusicPlayView();
                    break;
                case IMRoomEvent.ROOM_RECEIVE_HOT_RANK_UPDATE:
                    LogUtils.d(TAG, "receiveRoomEvent-房间热度值、排行信息更新");
                    //房间热度值、排行信息更新
                    if (null != rhvVaule) {
                        rhvVaule.refreshRoomHeatData(roomEvent.getHotRank(), roomEvent.getHotScore());
                    }
                    if (RoomDataManager.get().getAdditional() != null) {
                        RoomDataManager.get().getAdditional().setHotRank(roomEvent.getHotRank());
                        RoomDataManager.get().getAdditional().setHotScore(roomEvent.getHotScore());
                    }
                    break;
                case IMRoomEvent.ROOM_RECEIVE_LIVE_START:
                    LogUtils.d(TAG, "receiveRoomEvent-房主开播通知");
                    //1.如果是房主，那么按钮文案改为[停播]
                    if (RoomDataManager.get().isRoomOwner() && null != getActivity()) {
                        tvLiveOpera.setText(getActivity().getResources().getString(R.string.room_live_opera_stop));
                    }
                    ravAdmire.updateRoomAdmireStatus();
                    break;
                case IMRoomEvent.ROOM_RECEIVE_LIVE_STOP:
                    LogUtils.d(TAG, "receiveRoomEvent-房主停播通知");
                    //1.如果是房主，那么按钮文案改为[开播]
                    if (RoomDataManager.get().isRoomOwner() && null != getActivity()) {
                        tvLiveOpera.setText(getActivity().getResources().getString(R.string.room_live_opera_start));
                    }
                    ravAdmire.updateRoomAdmireStatus();
                    break;
                case IMRoomEvent.ROOM_AUDIO_CONN_REQ_RECV_NEW:
                    bvOperaMenu.updateAnchorRecvAudioConnCount(RoomDataManager.get().singleAudioConnReqNum);
                    break;
                case IMRoomEvent.ROOM_AUDIO_CONN_USER_CALL_DOWN_NOTIFY:
                case IMRoomEvent.ROOM_AUDIO_CONN_ANCHOR_CALL_DOWN_NOTIFY:
                    if (!TextUtils.isEmpty(roomEvent.getAudioConnOperaTips())) {
                        toast(roomEvent.getAudioConnOperaTips());
                    }
                    break;
                case IMRoomEvent.ROOM_AUDIO_CONN_REQ_CONN_BY_ANCHOR:
                    if (!TextUtils.isEmpty(roomEvent.getAudioConnOperaTips())) {
                        toast(roomEvent.getAudioConnOperaTips());
                    }
                    break;
                default:
                    break;
            }
        }
    }


    /**
     * 刷新在线人数
     */
    @Override
    protected void refreshOnlineCount(int onlineCount) {
        if (null != saomvOwnerMicInfo) {
            saomvOwnerMicInfo.updateOnLineNum(onlineCount);
        }
    }

    /**
     * 麦位点击监听
     */
    @Override
    public OnMicroItemClickListener getOnMicroItemClickListener() {
        return new OnMicroItemClickListener() {
            @Override
            public void onUpMicBtnClick(int position) {

            }

            @Override
            public void onMicroBtnClickListener(IMRoomQueueInfo roomQueueInfo, int micPosition) {
                getMvpPresenter().dealWithMicroClickEvent(getContext(), roomQueueInfo, micPosition);
            }
        };
    }

    /**
     * 显示房间更多功能弹框
     */
    @Override
    public void showRoomMoreOperateDialog() {
        getMvpPresenter().dealWithRoomMoreOperateEvent();
    }

    /**
     * 修改单人语音房的送礼
     *
     * @param targetUid  此人的uid
     * @param isPersonal
     */
    @Override
    public void showGiftDialog(long targetUid, boolean isPersonal) {
        super.showGiftDialog(targetUid, true);
    }

    private void onShowMusicPlayView() {
        //获取自己是否在麦上
        boolean isOnMic = getMvpPresenter().isOnMicByMyself();
        // 更新播放器界面
        RoomInfo currRoomInfo = getMvpPresenter().getCurrRoomInfo();
        if (currRoomInfo == null) {
            return;
        }
        if (mMusicPlayerView == null && isOnMic) {
            mMusicPlayerView = (MusicPlayerView) mVsMusicPlayer.inflate();
        }
        if (mMusicPlayerView != null) {
            mMusicPlayerView.setVisibility(isOnMic ? View.VISIBLE : View.GONE);
            mMusicPlayerView.setImageBg(currRoomInfo.getBackPic());
            mMusicPlayerView.setRoomType(currRoomInfo.getType());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //更新播放器状态
        if (mMusicPlayerView != null) {
            mMusicPlayerView.updateVoiceValue();
        }
    }

    @Override
    protected void releaseView() {
        super.releaseView();
        //释放音乐播放器的view
        if (mMusicPlayerView != null) {
            mMusicPlayerView.release();
        }
        if (null != rnvNotice) {
            rnvNotice.clearAnimation();
        }
        if (null != rhvVaule) {
            rhvVaule.release();
        }
        if (null != sacmvConnMicroView) {
            sacmvConnMicroView.release();
        }
    }

    @Override
    public boolean onBack() {
        boolean isIntercept = null != rrlvRecommRoomList && rrlvRecommRoomList.getVisibility() == View.VISIBLE;
        LogUtils.d(TAG, "onBackPressed-isIntercept:" + isIntercept);
        return isIntercept;
    }

    /**
     * 房间公告被点击
     */
    @Override
    public void showRoomNoticeInOutAnim() {
        if (rnvNotice.getVisibility() == View.VISIBLE) {
            rnvNotice.playOutAnim();
        } else {
            rnvNotice.playInAnim();
        }
    }

    /**
     * 点击跳转房间在线用户列表
     */
    @Override
    public void onOnlineMemberListClicked() {
        showOnlineMemberDialog(imRoomMember -> getMvpPresenter().dealWithOnlinePeoleClickEvent(getContext(), getMvpPresenter().getCurrRoomInfo(), imRoomMember));
    }

    /**
     * 点击打开土豪榜
     */
    @Override
    public void onShowRoomHeatRankList() {
        //同之前房间共享榜榜单界面
        showContributionMemberListDialog();
    }

    @Override
    public void initiate() {
        super.initiate();
        //初始化播放器的状态
        onShowMusicPlayView();
        getMvpPresenter().resetTimeInRoom();
        getMvpPresenter().getRoomActionList();
    }

    @Override
    protected void onRoomInSucShowMicroAndMessageView() {

    }

    @Override
    protected void updateRoomInfoAboutView(RoomInfo roomInfo) {
        ComboGiftPublicScreenMsgSender.getInstance().setCurrRoomId(roomInfo.getRoomId());

        //1.刚进来的时候，需要判断是否是房主，并且是否为单人房，两者都满足，才能处理开播按钮的显示逻辑
        if (null != roomInfo && roomInfo.getType() == RoomInfo.ROOMTYPE_SINGLE_AUDIO) {
            if (null != tvLiveOpera) {
                tvLiveOpera.setVisibility(RoomDataManager.get().isRoomOwner() ? View.VISIBLE : View.GONE);
            }

            if (null == RoomDataManager.get().getAdditional()) {
                return;
            }

            if (null != getActivity() && null != tvLiveOpera) {
                //2.对于单人房，判断开播状态，显示相应提示文案
                tvLiveOpera.setText(getActivity().getResources().getString(
                        RoomDataManager.get().getAdditional().getPlayState() == 1 ? R.string.room_live_opera_stop : R.string.room_live_opera_start));
            }

            if (RoomDataManager.get().getAdditional().getPlayState() == 1) {
                //已开播（针对断线重连场景），则显示停播按钮，显示直播时长控件，并继续计时
                RoomOwnerLiveTimeCounter.getInstance().startCount();
            } else {
                //未开播，则显示开播按钮，隐藏直播时长控件，关闭计时器
                RoomOwnerLiveTimeCounter.getInstance().stopCount();
            }
            if (null != ravAdmire) {
                ravAdmire.initRoomAdmireView();
            }
        }
        if (null != rmtvMsgTipsList) {
            rmtvMsgTipsList.getRoomMsgTipsList();
        }
        if (null != messageView) {
            refreshMessageViewLp(messageView.getVisibility());
        }
    }

    @Override
    protected void onRoomAudioInitSucShowBottomView() {

    }

    @Override
    public void showGiftDialogView(long targetUid, boolean isPersonal) {
        showGiftDialog(targetUid, isPersonal);
    }

    @Override
    protected void onRoomBannerClick() {
        if (null == getActivity()) {
            return;
        }
        //个人房，活动banner，点击
        StatisticManager.get().onEvent(getActivity(),
                StatisticModel.EVENT_ID_ROOM_BANNER_CLICK,
                StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
    }

    /**
     * 个人房-房主-停播操作，跳转开播统计界面
     */
    @Override
    public void openRoomLiveInfoH5View() {
        if (null != getActivity()) {
            SingleAudioRoomLiveInfoWebViewActivity.start(getActivity(), false);
        }
    }

    @Override
    protected void onRoomDynamicInitView() {
        super.onRoomDynamicInitView();
        if (null != rlbvLotteryBox) {
            rlbvLotteryBox.initialView();
        }
        if (RoomDataManager.get().getAdditional() != null && null != rhvVaule) {
            rhvVaule.refreshRoomHeatData(RoomDataManager.get().getAdditional().getHotRank(), RoomDataManager.get().getAdditional().getHotScore());
        }
        //房主自己隐藏关注房间控件
        tvFollowRoom.setVisibility(RoomDataManager.get().isRoomOwner() ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (null != rlbvLotteryBox) {
            rlbvLotteryBox.release();
        }
        if (null != roomBannerAdapter) {
            roomBannerAdapter.setOnRoomBannerClickListener(null);
        }
        AudioConnTimeOutTipsQueueScheduler.getInstance().clear();
    }

    @Override
    public void onRoomMoreOperaClick() {
        //测试炸房校验逻辑的测试代码,备用测试
//        RtcEngineManager.get().setRole(RtcEngineManager.get().isAudienceRole ? io.agora.rtc.Constants.CLIENT_ROLE_BROADCASTER : io.agora.rtc.Constants.CLIENT_ROLE_AUDIENCE);
        RoomMoreOpearDialog roomMoreOpearDialog = new RoomMoreOpearDialog(getActivity());
        roomMoreOpearDialog.setOnLockyWheelGiftBtnClickListener(new RoomMoreOpearDialog.OnLockyWheelGiftBtnClickListener() {
            @Override
            public void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, long targetUid, int number) {
                //先用旧的
            }

            @Override
            public void showRoomGiftDialog(long uid) {
                RoomGiftDialog roomGiftDialog = RoomGiftDialog.getInstance(uid, GiftType.Package, true);
//                roomGiftDialog.setOnClickToSendGiftListener(new RoomGiftDialog.OnClickToSendGiftListener() {
//                    @Override
//                    public void onSendRoomMultiGift(GiftInfo giftInfo, long roomUid, long roomId,
//                                                    List<MicMemberInfo> micAvatar, int giftNum, boolean isAllMic) {
//                        SingleAudioRoomFragment.this.sendRoomMultiGift(giftInfo, roomUid, roomId, micAvatar, giftNum, false, isAllMic);
//                    }
//
//                    @Override
//                    public void sendSingleGift(GiftInfo giftInfo, long targetUid, long roomUid, long roomId, int giftNum) {
//                        SingleAudioRoomFragment.this.sendSingleGift(giftInfo, targetUid, roomUid, roomId, giftNum, false);
//                    }
//                });
                roomGiftDialog.show(getChildFragmentManager(), RoomGiftDialog.class.getSimpleName());
            }
        });
        roomMoreOpearDialog.show();
//        hideComBoSenderViewAfterShowGiftDialog();
    }

    /**
     * 非主播，点击底部语音连接按钮
     */
    @Override
    public void onFansReqAudioConnClick() {
        showAudioReqConnDialog();
    }

    /**
     * 主播，点击底部语音连接按钮
     */
    @Override
    public void onAnchorAudioConnRecvClick() {
        showAudioReqConnDialog();
    }

    private void showAudioReqConnDialog() {
        audioConnectListDialog = new AudioConnectListDialog();
        audioConnectListDialog.show(getChildFragmentManager(), "audioConnectListDialog");
    }

    @Override
    public void showRoomAttentionStatus(boolean status) {
        super.showRoomAttentionStatus(status);
        if (null != tvFollowRoom) {
//            tvFollowRoom.setVisibility(status ? View.GONE : View.VISIBLE);
        }
        if (null != messageView) {
            messageView.refreshRoomAttentionMsgStatus(status);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (null != audioConnectListDialog && audioConnectListDialog.isVisible()) {
            audioConnectListDialog.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void showFaceView(List<FaceReceiveInfo> faceReceiveInfos) {
        super.showFaceView(faceReceiveInfos);
        sacmvConnMicroView.showFaceView(faceReceiveInfos);
    }
}
