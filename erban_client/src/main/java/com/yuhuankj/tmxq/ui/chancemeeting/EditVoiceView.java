package com.yuhuankj.tmxq.ui.chancemeeting;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

public interface EditVoiceView<T extends AbstractMvpPresenter> extends IMvpBaseView {

    void onVoiceBgUploaded(boolean isSuccess, String message);
}
