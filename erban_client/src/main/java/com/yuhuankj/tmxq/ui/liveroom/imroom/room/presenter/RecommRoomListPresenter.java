package com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomModel;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.SingleAudioRoomEnitity;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IRecommRoomListView;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * 房间页面的presenter
 */
public class RecommRoomListPresenter<I extends IMvpBaseView> extends AbstractMvpPresenter<IRecommRoomListView> {

    private final String TAG = RecommRoomListPresenter.class.getSimpleName();

    private IMRoomModel imRoomModel;

    public RecommRoomListPresenter() {
        imRoomModel = new IMRoomModel();
    }

    public void getRecommRoomList() {
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (null == roomInfo) {
            return;
        }
        LogUtils.d(TAG, "getRecommRoomList roomId:" + roomInfo.getRoomId());
        imRoomModel.getRecommendList(roomInfo.getRoomId(), new HttpRequestCallBack<List<SingleAudioRoomEnitity>>() {
            @Override
            public void onSuccess(String message, List<SingleAudioRoomEnitity> response) {
                LogUtils.d(TAG, "getRecommRoomList-->onSuccess message:" + message);
                if (null != getMvpView() && !ListUtils.isListEmpty(response)) {
                    LogUtils.d(TAG, "getRecommRoomList-->onSuccess list.size:" + response.size());
                    getMvpView().refreshRecommRoomList(response);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                LogUtils.d(TAG, "getRecommRoomList-->onFailure code:" + code + " msg:" + msg);
            }
        });
    }

    public Call<ResponseBody> getReturnRoomList() {
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (null == roomInfo) {
            return null;
        }
        LogUtils.d(TAG, "getReturnRoomList roomId:" + roomInfo.getRoomId());
        return imRoomModel.getReturnRoomList(roomInfo.getRoomId(), new HttpRequestCallBack<List<SingleAudioRoomEnitity>>() {
            @Override
            public void onSuccess(String message, List<SingleAudioRoomEnitity> response) {
                LogUtils.d(TAG, "getReturnRoomList-->onSuccess message:" + message);
                if (null != getMvpView() && !ListUtils.isListEmpty(response)) {
                    LogUtils.d(TAG, "getReturnRoomList-->onSuccess list.size:" + response.size());
                    RoomDataManager.get().setSingleAudioRoomEnitities(response);
                    getMvpView().refreshRecommRoomList(response);
//                }else{
//                    /**
//                     * {
//                     * 	"abChannelType": 1,
//                     * 	"avatar": "https://pic.tiantianyuyin.com/FjEt5s1myGO0RX9V0xb3xWjSq1Ab?imageslim",
//                     * 	"backPic": "",
//                     * 	"calcSumDataIndex": 0,
//                     * 	"count": 0,
//                     * 	"erbanNo": 1450615,
//                     * 	"faceType": 0,
//                     * 	"factor": 0,
//                     * 	"gender": 1,
//                     * 	"hotRank": 99999,
//                     * 	"hotScore": 1000,
//                     * 	"isPermitRoom": 2,
//                     * 	"isRecom": 0,
//                     * 	"meetingName": "dc85061d287a4e6298b1e4091f8ad886",
//                     * 	"newestTime": 1565945080404,
//                     * 	"nick": "看一看太阳",
//                     * 	"officeUser": 1,
//                     * 	"onlineDuration": 28796,
//                     * 	"onlineNum": 2,
//                     * 	"openTime": 1565864844000,
//                     * 	"operatorStatus": 1,
//                     * 	"roomId": 11130,
//                     * 	"roomTag": "情感",
//                     * 	"searchTagId": 0,
//                     * 	"tagId": 38,
//                     * 	"tagPict": "https://img.pinjin88.com/qinggan@2x.png",
//                     * 	"title": "看一看太阳的房间",
//                     * 	"type": 4,
//                     * 	"uid": 92000417,
//                     * 	"valid": true
//                     * }
//                     */
//                    response = new ArrayList<>();
//                    for(int i = 0 ; i<8; i++){
//                        SingleAudioRoomEnitity singleAudioRoomEnitity = new SingleAudioRoomEnitity();
//                        singleAudioRoomEnitity.setType(RoomInfo.ROOMTYPE_SINGLE_AUDIO);
//                        singleAudioRoomEnitity.setTitle("看一看太阳的房间");
//                        singleAudioRoomEnitity.setAvatar("https://pic.tiantianyuyin.com/FjEt5s1myGO0RX9V0xb3xWjSq1Ab?imageslim");
//                        singleAudioRoomEnitity.setUid(92000417L);
//                        singleAudioRoomEnitity.setGender(1);
//                        singleAudioRoomEnitity.setOnlineNum(1314);
//                        singleAudioRoomEnitity.setRoomTag("情感");
//                        response.add(singleAudioRoomEnitity);
//                    }
//                    getMvpView().refreshRecommRoomList(response);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                LogUtils.d(TAG, "getReturnRoomList-->onFailure code:" + code + " msg:" + msg);
            }
        });
    }
}
