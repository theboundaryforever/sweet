package com.yuhuankj.tmxq.ui.audio.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.dinuscxj.progressbar.CircleProgressBar;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.file.BasicFileUtils;
import com.tongdaxing.xchat_core.player.IPlayerCore;
import com.tongdaxing.xchat_core.player.bean.LocalMusicInfo;
import com.yuhuankj.tmxq.R;

import java.util.List;

import static com.tongdaxing.xchat_core.player.IPlayerCore.PLAY_STATUS_MUSIC_LIST_EMPTY;

/**
 * Created by weihaitao on 2018年11月19日 12:33:06.
 */

public class MusicPlayListAdapter extends RecyclerView.Adapter<MusicPlayListAdapter.ViewHolder>{
    private Context context;
    private List<LocalMusicInfo> localMusicInfos;

    public MusicPlayListAdapter(Context context) {
        this.context = context;
    }

    public void setLocalMusicInfos(List<LocalMusicInfo> localMusicInfos) {
        this.localMusicInfos = localMusicInfos;
    }

    @Override
    public MusicPlayListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(context).inflate(R.layout.list_item_play_music,parent,false);
        return new MusicPlayListAdapter.ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(MusicPlayListAdapter.ViewHolder holder, int position, List<Object> payloads) {
        if(null != payloads && payloads.size() > 0 && payloads.get(0) instanceof Integer){
            holder.iv_playStatus.setVisibility(View.GONE);
            holder.fl_downProgress.setVisibility(View.VISIBLE);
            holder.cpb_download.setProgress((int)payloads.get(0));
        }else{
            onBindViewHolder(holder, position);
        }
    }

    @Override
    public void onBindViewHolder(MusicPlayListAdapter.ViewHolder holder, int position) {
        final LocalMusicInfo localMusicInfo = localMusicInfos.get(position);
        holder.tv_musicName.setText(localMusicInfo.getSongName());
        LocalMusicInfo currMusicInfo = CoreManager.getCore(IPlayerCore.class).getCurrLocalMusicInfo();
        boolean isCurrPlayMusic = null != currMusicInfo && currMusicInfo.getLocalUri().equals(localMusicInfo.getLocalUri())
                && CoreManager.getCore(IPlayerCore.class).getState() != IPlayerCore.STATE_STOP;
        boolean hasMusicFileDowned = BasicFileUtils.isFileExisted(localMusicInfo.getLocalUri());
        List<String> artistNames = localMusicInfo.getArtistNames();
        if(null != artistNames && artistNames.size()>0){
            holder.tv_singerName.setText(artistNames.get(0));
        }else{
            holder.tv_singerName.setText(context.getResources().getString(R.string.music_library_item_singer_unknow));
        }
        if(!hasMusicFileDowned){
            holder.iv_playStatus.setVisibility(View.GONE);
            holder.fl_downProgress.setVisibility(View.VISIBLE);
            holder.cpb_download.setProgress(1);
        }else{
            holder.iv_playStatus.setVisibility(View.VISIBLE);
            holder.fl_downProgress.setVisibility(View.GONE);
            if(isCurrPlayMusic){
                int state = CoreManager.getCore(IPlayerCore.class).getState();
                holder.iv_playStatus.setImageDrawable(context.getResources().getDrawable(state == IPlayerCore.STATE_PLAY ? R.mipmap.icon_music_item_play_to_pause : R.mipmap.icon_music_item_pause_to_play));
            }else{
                holder.iv_playStatus.setImageDrawable(context.getResources().getDrawable(R.mipmap.icon_music_item_to_play));
            }
        }

        holder.iv_playStatus.setTag(localMusicInfo);
        holder.iv_playStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocalMusicInfo info = (LocalMusicInfo) v.getTag();
                if(null == info){
                    return;
                }
                boolean currMusicPlay = CoreManager.getCore(IPlayerCore.class).getCurrLocalMusicLocalId() ==  info.getLocalId();
                if(currMusicPlay){
                    int state = CoreManager.getCore(IPlayerCore.class).getState();
                    if (state == IPlayerCore.STATE_PLAY) {
                        CoreManager.getCore(IPlayerCore.class).pause();
                    } else if (state == IPlayerCore.STATE_PAUSE) {
                        LocalMusicInfo current = CoreManager.getCore(IPlayerCore.class).getCurrLocalMusicInfo();
                        int result = CoreManager.getCore(IPlayerCore.class).play(current);
                        if (result < 0) {
                            if(null != onPlayErrorListener){
                                onPlayErrorListener.onPlayError(
                                        context.getResources().getString(result == PLAY_STATUS_MUSIC_LIST_EMPTY ?
                                                R.string.music_play_failed_list_empty :
                                                R.string.music_play_failed_file_format_error));
                            }
                        }
                    } else {
                        int result = CoreManager.getCore(IPlayerCore.class).playNext();
                        if (result < 0) {
                            if(null != onPlayErrorListener){
                                onPlayErrorListener.onPlayError(
                                        context.getResources().getString(result == PLAY_STATUS_MUSIC_LIST_EMPTY ?
                                                R.string.music_play_failed_list_empty :
                                                R.string.music_play_failed_file_format_error));
                            }
                        }
                    }
                }else{
                    int result = CoreManager.getCore(IPlayerCore.class).play(localMusicInfo);
                    if (result < 0) {
                        if(null != onPlayErrorListener){
                            onPlayErrorListener.onPlayError(
                                    context.getResources().getString(result == PLAY_STATUS_MUSIC_LIST_EMPTY ?
                                            R.string.music_play_failed_list_empty :
                                            R.string.music_play_failed_file_format_error));
                        }
                    }
                }

            }
        });
        holder.container.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                if(null != onLongClickListener){
                    onLongClickListener.onMusicListItemLongClicked(localMusicInfo);
                }
                return true;
            }
        });

    }

    @Override
    public int getItemCount() {
        if (localMusicInfos == null) {
            return 0;
        } else {
            return localMusicInfos.size();
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        ImageView iv_playStatus;
        CircleProgressBar cpb_download;
        TextView tv_musicName;
        TextView tv_singerName;
        FrameLayout container;
        FrameLayout fl_downProgress;

        public ViewHolder(View itemView) {
            super(itemView);
            container = (FrameLayout) itemView.findViewById(R.id.container);
            fl_downProgress = (FrameLayout) itemView.findViewById(R.id.fl_downProgress);
            iv_playStatus = (ImageView) itemView.findViewById(R.id.iv_playStatus);
            cpb_download = (CircleProgressBar) itemView.findViewById(R.id.cpb_download);
            tv_musicName = (TextView) itemView.findViewById(R.id.tv_musicName);
            tv_singerName = (TextView) itemView.findViewById(R.id.tv_singerName);
        }
    }

    public void setOnPlayErrorListener(OnPlayErrorListener onPlayErrorListener) {
        this.onPlayErrorListener = onPlayErrorListener;
    }

    private OnPlayErrorListener onPlayErrorListener;

    public interface OnPlayErrorListener{
        void onPlayError(String msg);
    }

    public void setOnLongClickListener(OnMusicListItemLongClickListener onLongClickListener) {
        this.onLongClickListener = onLongClickListener;
    }

    private OnMusicListItemLongClickListener onLongClickListener;

    public interface OnMusicListItemLongClickListener{
        void onMusicListItemLongClicked(LocalMusicInfo localMusicInfo);
    }

}
