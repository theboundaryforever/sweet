package com.yuhuankj.tmxq.ui.liveroom.imroom.room.dialog;

import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.room.auction.ParticipateAuctionBean;
import com.tongdaxing.xchat_core.room.auction.ParticipateAuctionListBean;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.base.presenter.BaseMvpPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.model.AuctionModel;

/**
 * 文件描述：竞拍房 - 参与竞拍用户弹框P层
 *
 * @auther：zwk
 * @data：2019/7/30
 */
public class AuctionSortListPresenter extends BaseMvpPresenter<IAuctionSortListView> {
    private AuctionModel auctionModel;

    public AuctionSortListPresenter() {
        this.auctionModel = new AuctionModel();
    }

    /**
     * 获取参与竞拍用户列表
     *
     * @param roomId
     */
    public void getRoomAuctionList(long roomId) {
        auctionModel.getRoomAuctionList(roomId, new CallBack<ParticipateAuctionListBean>() {
            @Override
            public void onSuccess(ParticipateAuctionListBean data) {
                if (getMvpView() != null) {
                    if (data != null) {
                        getMvpView().showSortListCount(data.getCount());
                        getMvpView().isShowPriorityCardView(data.isJoin());
                        getMvpView().isShowParticipateBtn(data.isJoin());
                        getMvpView().showAuctionSortListView(data.getUserList());
                    } else {
                        getMvpView().showErrorAuctionSortListView();
                    }
                }
            }

            @Override
            public void onFail(int code, String error) {
                if (getMvpView() != null) {
                    getMvpView().showErrorAuctionSortListView();
                }
            }
        });
    }

    /**
     * 获取参与竞拍用户列表
     *
     * @param roomId
     */
    public void cancelRoomAuction(long roomId, int day, String project) {
        auctionModel.joinOrCancelRoomAuction(2, roomId, day, project, new CallBack<ParticipateAuctionBean>() {
            @Override
            public void onSuccess(ParticipateAuctionBean data) {
                if (getMvpView() != null) {
                    if (data != null) {
                        getMvpView().showErrorToast("取消竞拍成功");
                        if (RoomDataManager.get().onAuctionMicMyself()) {
                            getMvpView().downMicro();
                        }
                        getMvpView().updateSortListInfo();
                    } else {
                        getMvpView().showErrorToast("取消异常");
                    }
                }
            }

            @Override
            public void onFail(int code, String error) {
                if (getMvpView() != null) {
                    getMvpView().showErrorToast(error);
                }
            }
        });
    }


    /**
     * 邀请竞拍上麦
     *
     * @param micPosition   上的麦位
     * @param account       上麦的人
     * @param isInviteUpMic 上麦成功后是否自动开麦
     */
    public void operateUpMicro(int micPosition, long account, boolean isInviteUpMic) {
        IMRoomMessageManager.get().inviteMicroPhoneBySdk(account,micPosition);
    }
}
