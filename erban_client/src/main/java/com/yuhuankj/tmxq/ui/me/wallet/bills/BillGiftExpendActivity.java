package com.yuhuankj.tmxq.ui.me.wallet.bills;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.bills.IBillsCore;
import com.tongdaxing.xchat_core.bills.IBillsCoreClient;
import com.tongdaxing.xchat_core.bills.bean.BillItemEntity;
import com.tongdaxing.xchat_core.bills.bean.ExpendInfo;
import com.tongdaxing.xchat_core.bills.bean.ExpendListInfo;
import com.tongdaxing.xchat_core.pay.IPayCore;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.me.charge.ChargeActivity;
import com.yuhuankj.tmxq.ui.me.wallet.bills.adapter.GiftExpendAdapter;
import com.yuhuankj.tmxq.widget.TitleBar;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>账单礼物支出界面  </p>
 *
 * @author Administrator
 * @date 2017/11/6
 */
public class BillGiftExpendActivity extends BillBaseActivity {
    private GiftExpendAdapter adapter;
    private TextView mGoldNum;

    @Override
    protected int getLayout() {
        return R.layout.activity_bills_gift;
    }

    @Override
    protected void initView() {
        super.initView();
        mGoldNum = (TextView) findViewById(R.id.tv_gold_num);
    }


    @Override
    protected void initData() {
        super.initData();
        initTitleBar(getString(R.string.bill_gift_expend));

        adapter = new GiftExpendAdapter(mBillItemEntityList);
        adapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                mCurrentCounter++;
                loadData();
            }
        }, mRecyclerView);
        RecyclerViewNoBugLinearLayoutManager manager = new RecyclerViewNoBugLinearLayoutManager(mActivity);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setAdapter(adapter);
        showLoading();
        onLoadGoldNum();
        loadData();
    }

    private void onLoadGoldNum() {
        WalletInfo walletInfo = CoreManager.getCore(IPayCore.class).getCurrentWalletInfo();
        if (null != walletInfo) {
            mGoldNum.setText(getString(R.string.bill_gift_gold_num, walletInfo.getGoldNum()));
        }
    }

    @Override
    protected void loadData() {
        CoreManager.getCore(IBillsCore.class).getGiftExpendBills(mCurrentCounter, PAGE_SIZE, time);
    }

    @CoreEvent(coreClientClass = IBillsCoreClient.class)
    public void onGetExpendBills(ExpendListInfo data) {
        mRefreshLayout.setRefreshing(false);
        if (null != data) {
            if (mCurrentCounter == Constants.PAGE_START) {
                hideStatus();
                mBillItemEntityList.clear();
                adapter.setNewData(mBillItemEntityList);
            } else {
                adapter.loadMoreComplete();
            }
            List<Map<String, List<ExpendInfo>>> billList = data.getBillList();
            if (!billList.isEmpty()) {
                tvTime.setVisibility(View.GONE);
                List<BillItemEntity> billItemEntities = new ArrayList<>();
                int size = mBillItemEntityList.size();
                BillItemEntity billItemEntity;
                for (int i = 0; i < billList.size(); i++) {
                    Map<String, List<ExpendInfo>> map = billList.get(i);
                    for (String key : map.keySet()) {
                        // key ---日期    value：list集合记录
                        List<ExpendInfo> expendInfos = map.get(key);
                        if (ListUtils.isListEmpty(expendInfos)) continue;
                        //正常item
                        for (ExpendInfo temp : expendInfos) {
                            billItemEntity = new BillItemEntity(BillItemEntity.ITEM_NORMAL);
                            //目的是为了比较
                            billItemEntity.time = key;
                            billItemEntity.mGiftExpendInfo = temp;
                            billItemEntities.add(billItemEntity);
                        }
                    }
                }
                if (billItemEntities.size() < Constants.BILL_PAGE_SIZE && mCurrentCounter == Constants.PAGE_START) {
                    adapter.setEnableLoadMore(false);
                }

                adapter.addData(billItemEntities);
            } else {
                if (mCurrentCounter == Constants.PAGE_START) {
                    showNoData(getResources().getString(R.string.bill_no_data_text));
                } else {
                    adapter.loadMoreEnd(true);
                }
            }
        }
    }

    @CoreEvent(coreClientClass = IBillsCoreClient.class)
    public void onGetExpendBillsError(String error) {
        if (mCurrentCounter == Constants.PAGE_START) {
            showNetworkErr();
        } else {
            adapter.loadMoreFail();
        }
    }

    @Override
    public void initTitleBar(String title) {
        mTitleBar = (TitleBar) findViewById(R.id.title_bar);
        if (mTitleBar != null) {
            mTitleBar.setTitle(title);
            mTitleBar.setImmersive(false);
            mTitleBar.setTitleColor(getResources().getColor(R.color.back_font));
            mTitleBar.setLeftImageResource(R.drawable.arrow_left);
            mTitleBar.setLeftClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
        mTitleBar.setActionTextColor(getResources().getColor(R.color.text_tertiary));
        mTitleBar.addAction(new TitleBar.TextAction("充值") {
            @Override
            public void performAction(View view) {
                startActivity(new Intent(mActivity, ChargeActivity.class));
            }
        });
    }
}
