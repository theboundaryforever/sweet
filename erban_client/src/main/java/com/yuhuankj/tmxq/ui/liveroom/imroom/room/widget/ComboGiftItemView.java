package com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.juxiao.library_utils.DisplayUtils;
import com.tongdaxing.erban.libcommon.base.AbstractMvpRelativeLayout;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.gift.ComboGiftVoInfo;
import com.tongdaxing.xchat_core.gift.ComboRang;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.widget.CircleImageView;

/**
 * Created by Administrator on 2019/4/7.
 */

public class ComboGiftItemView extends AbstractMvpRelativeLayout implements IMvpBaseView {
    public final static String TAG = ComboGiftItemView.class.getSimpleName();
    private final static boolean isPrintDebugLog = true;
    /**
     * 停留动画的最长事件
     */
    private final long HOLD_ANIM_MAX_TIME = 3 * 1000L;
    private Context mContext;
    private CircleImageView iv_avatar;
    private TextView tvSenderNick;
    private TextView tvRecvDesc;
    private ImageView iv_gift;
    private LinearLayout llComboNum;
    private ImageView vComboX;
    private ComboGiftVoInfo comboGiftVoInfo;
    private int currComboCount = 0;
    private int avatarAndGiftImgSize = 0;
    private int giftNumImgWidth = 0;
    private int giftNumImgHeight = 0;
    private OnOutAnimEndListener onOutAnimEndListener;
    private Runnable mAnimationEndRunnable = new Runnable() {
        @Override
        public void run() {
            printLog("playHoldinggAnim-->onAnimationEnd-->playOutAnim  comboId:" + comboGiftVoInfo.getItemComboId());
            playOutAnim();
        }
    };

    public ComboGiftItemView(Context context) {
        super(context, null);
    }

    public ComboGiftItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs, 0);
    }

    public ComboGiftItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getViewLayoutId() {
        return R.layout.item_combo_gift_layout;
    }

    @Override
    public void initialView(Context context) {
        this.mContext = context;
        avatarAndGiftImgSize = DisplayUtils.dip2px(mContext, 30);
        iv_avatar = findViewById(R.id.iv_avatar);
        tvSenderNick = findViewById(R.id.tvSenderNick);
        tvRecvDesc = findViewById(R.id.tvRecvDesc);
        iv_gift = findViewById(R.id.iv_gift);
        llComboNum = findViewById(R.id.llComboNum);
        vComboX = findViewById(R.id.vComboX);
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initViewState() {

    }

    public void playInAnim() {
        printLog("playInAnim-->initComboGiftVoInfo  comboId:" + comboGiftVoInfo.getItemComboId());
        initComboGiftVoInfo();
        comboGiftVoInfo.setGiftAnimStatus(ComboGiftVoInfo.GiftAnimStatus.PrepareAndPlaying);
        AnimationSet animation = (AnimationSet) AnimationUtils.loadAnimation(mContext, R.anim.anim_common_bottom_dialog_in);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //播放完入场动画之后，就可以循环播礼物数字动画了
                printLog("playInAnim-->onAnimationEnd-->playNextComboNumAnim  comboId:" + comboGiftVoInfo.getItemComboId());
                playNextComboNumAnim();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        printLog("playInAnim startInAnim");
        animation.setRepeatCount(0);
        startAnimation(animation);
    }

    public void initComboGiftVoInfo() {
        if (comboGiftVoInfo != null) {
            ImageLoadUtils.loadCircleImage(mContext, ImageLoadUtils.toThumbnailUrl(avatarAndGiftImgSize, avatarAndGiftImgSize, comboGiftVoInfo.getAvatar()),
                    iv_avatar, R.drawable.ic_default_avatar);
            tvSenderNick.setText(comboGiftVoInfo.getNick());
            tvRecvDesc.setText(comboGiftVoInfo.getContent());
            ImageLoadUtils.loadCircleImage(mContext, ImageLoadUtils.toThumbnailUrl(avatarAndGiftImgSize, avatarAndGiftImgSize, comboGiftVoInfo.getGiftUrl()),
                    iv_gift, R.drawable.ic_default_avatar);

            llComboNum.setVisibility(View.INVISIBLE);
            printLog("initComboGiftVoInfo-->loadFirstComboNumImgRes comboId:" + comboGiftVoInfo.getItemComboId());
            loadFirstComboNumImgRes();
        }
    }

    /**
     * 加载第一个连击数字图片资源，不播放动画
     */
    private void loadFirstComboNumImgRes() {
        if (comboGiftVoInfo != null) {
            ComboRang comboRang = comboGiftVoInfo.getFirstComboRang();
            if (null != comboRang) {
                int comboRangStart = comboRang.getComboRangStart();
                int comboRangEnd = comboRang.getComboRangEnd();
                printLog("loadFirstComboNumImgRes-->resetComboNumImg comboId:" + comboGiftVoInfo.getItemComboId());
                resetComboNumImg(comboRangStart, comboRangEnd);
            }
        }
    }

    /**
     * 加载并播放下一个连击数字动画
     *
     * @return
     */
    public void playNextComboNumAnim() {
        removeCallbacks(mAnimationEndRunnable);
        boolean hasNextNumAnim = false;
        if (comboGiftVoInfo != null) {
            ComboRang comboRang = comboGiftVoInfo.removeFirstComboRang();
            if (null != comboRang) {
                int comboRangStart = comboRang.getComboRangStart();
                int comboRangEnd = comboRang.getComboRangEnd();
                hasNextNumAnim = resetComboNumImg(comboRangStart, comboRangEnd);
                printLog("playNextComboNumAnim  comboId:" + comboGiftVoInfo.getItemComboId() + " hasNextNumAnim:" + hasNextNumAnim);
                if (hasNextNumAnim) {
                    printLog("playNextComboNumAnim-->playComboNumAnim  comboId:" + comboGiftVoInfo.getItemComboId());
                    playComboNumAnim();
                }
            } else {
                //如果一直播，播到comboGiftVoInfo的rang队列没有item，那么即可开始播放停留动画
                printLog("playNextComboNumAnim-->playHoldinggAnim  comboId:" + comboGiftVoInfo.getItemComboId());
                playHoldinggAnim();
            }
        }
    }

    /**
     * 播放出场动画
     */
    private void playOutAnim() {
        printLog("playOutAnim  comboId:" + comboGiftVoInfo.getItemComboId());
        comboGiftVoInfo.setGiftAnimStatus(ComboGiftVoInfo.GiftAnimStatus.Vanishing);
        AnimationSet animation = (AnimationSet) AnimationUtils.loadAnimation(mContext, R.anim.anim_common_bottom_dialog_out);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                setVisibility(INVISIBLE);
                comboGiftVoInfo.setGiftAnimStatus(ComboGiftVoInfo.GiftAnimStatus.Disappear);
                if (null != onOutAnimEndListener) {
                    printLog("playOutAnim-->onAnimationEnd-->onOutAnimEnd  comboId:" + comboGiftVoInfo.getItemComboId());
                    onOutAnimEndListener.onOutAnimEnd(ComboGiftItemView.this);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        animation.setRepeatCount(0);
        startAnimation(animation);
    }

    /**
     * 开启 停留计时
     */
    private void playHoldinggAnim() {
        printLog("playHoldinggAnim  comboId:" + comboGiftVoInfo.getItemComboId());
        comboGiftVoInfo.setGiftAnimStatus(ComboGiftVoInfo.GiftAnimStatus.Holding);
        //如果有，移除之前设置的定时任务
        removeCallbacks(mAnimationEndRunnable);
        //发起新的定时任务
        postDelayed(mAnimationEndRunnable, HOLD_ANIM_MAX_TIME);
    }

    private void printLog(String logContent) {
        if (isPrintDebugLog) {
            LogUtils.d(TAG, logContent);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (null != comboGiftVoInfo) {
            printLog("onDetachedFromWindow-->clear  comboId:" + comboGiftVoInfo.getItemComboId());
        }
        clear();
    }

    public void clear() {
        if (null != comboGiftVoInfo) {
            printLog("clear  comboId:" + comboGiftVoInfo.getItemComboId());
        }
        onOutAnimEndListener = null;
        comboGiftVoInfo = null;
        //移除停留动画相关
        removeCallbacks(mAnimationEndRunnable);

        //移除数字动画相关
        Animation animation = llComboNum.getAnimation();
        if (null != animation) {
            animation.setAnimationListener(null);
            animation.cancel();
        }
        llComboNum.removeAllViews();

        animation = vComboX.getAnimation();
        if (null != animation) {
            animation.setAnimationListener(null);
            animation.cancel();
        }


        //移除入场、出场动画相关
        animation = getAnimation();
        if (null != animation) {
            animation.setAnimationListener(null);
            animation.cancel();
        }
    }

    public void setOnOutAnimEndListener(OnOutAnimEndListener listener) {
        this.onOutAnimEndListener = listener;
    }

    /**
     * 重设连送数字图标资源
     *
     * @param comboRangStart
     * @param comboRangEnd
     * @return
     */
    private boolean resetComboNumImg(int comboRangStart, int comboRangEnd) {
        boolean hasNextNumAnim = false;

        if (0 == giftNumImgWidth && GlideContextCheckUtil.checkContextUsable(getContext())) {
            giftNumImgWidth = DisplayUtils.dip2px(getContext(), 19);
            giftNumImgHeight = DisplayUtils.dip2px(getContext(), 29);
        }

        printLog("resetComboNumImg  comboId:" + comboGiftVoInfo.getItemComboId() + " comboRangStart:" + comboRangStart + " comboRangEnd:" + comboRangEnd);
        if (comboRangEnd >= this.currComboCount && comboRangEnd > comboRangStart) {
            hasNextNumAnim = true;
            currComboCount = comboRangEnd;
            //位数之长
            String number = String.valueOf(currComboCount);
            int numberLength = number.length();
            //已有控件数量
            int childCount = llComboNum.getChildCount();
            //取两者最大值
            int forMax = Math.max(numberLength, childCount);
            ImageView childImageView = null;
            for (int i = 0; i < forMax; i++) {
                childImageView = (ImageView) llComboNum.getChildAt(i);
                //如果位数之长小于已有控件数量
                if (numberLength < childCount) {
                    if (i >= numberLength) {
                        //那么索引值超过位数之长的控件都应该被隐藏
                        childImageView.setVisibility(View.GONE);
                        childImageView.setImageDrawable(null);
                        continue;
                    } else {
                        //索引值在位数之长范围内的，都设置相应的数字图片
                        childImageView.setImageResource(getGiftNumImageResId(number.charAt(i)));
                        childImageView.setVisibility(View.VISIBLE);
                    }
                } else {
                    //如果位数之长超过以后控件数量
                    if (i < childCount) {
                        //已有控件，显示，设置数字图标
                        childImageView.setImageResource(getGiftNumImageResId(number.charAt(i)));
                        childImageView.setVisibility(View.VISIBLE);
                    } else {
                        //超过的部分，创建并添加，显示并设置图标
                        childImageView = new ImageView(mContext);
                        llComboNum.addView(childImageView);
                        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) childImageView.getLayoutParams();
                        params.weight = giftNumImgWidth;
                        params.height = giftNumImgHeight;
                        childImageView.setLayoutParams(params);
                        childImageView.setImageResource(getGiftNumImageResId(number.charAt(i)));
                        childImageView.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
        return hasNextNumAnim;
    }

    /**
     * 数字转图片资源ID
     *
     * @param number
     * @return
     */
    private int getGiftNumImageResId(char number) {
        int imageResId = -1;
        switch (number) {
            case '0':
                imageResId = R.mipmap.icon_combo_gift_anim_num_0;
                break;
            case '1':
                imageResId = R.mipmap.icon_combo_gift_anim_num_1;
                break;
            case '2':
                imageResId = R.mipmap.icon_combo_gift_anim_num_2;
                break;
            case '3':
                imageResId = R.mipmap.icon_combo_gift_anim_num_3;
                break;
            case '4':
                imageResId = R.mipmap.icon_combo_gift_anim_num_4;
                break;
            case '5':
                imageResId = R.mipmap.icon_combo_gift_anim_num_5;
                break;
            case '6':
                imageResId = R.mipmap.icon_combo_gift_anim_num_6;
                break;
            case '7':
                imageResId = R.mipmap.icon_combo_gift_anim_num_7;
                break;
            case '8':
                imageResId = R.mipmap.icon_combo_gift_anim_num_8;
                break;
            case '9':
                imageResId = R.mipmap.icon_combo_gift_anim_num_9;
                break;
            default:
                break;
        }
        return imageResId;
    }

    /**
     * 播放连击x数字动画
     */
    public void playComboNumAnim() {
        printLog("playComboNumAnim  comboId:" + comboGiftVoInfo.getItemComboId());
        comboGiftVoInfo.setGiftAnimStatus(ComboGiftVoInfo.GiftAnimStatus.PrepareAndPlaying);
        ScaleAnimation animation = new ScaleAnimation(
                1.2f, 0.8f, 1.2f, 0.8f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f
        );
        animation.setDuration(200);
        animation.setInterpolator(new AnticipateOvershootInterpolator());
        //动画执行完毕后是否停在结束时的状态
        animation.setFillAfter(true);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                llComboNum.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                printLog("playComboNumAnim-->onAnimationEnd-->playNextComboNumAnim  comboId:" + comboGiftVoInfo.getItemComboId());
                playNextComboNumAnim();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        llComboNum.startAnimation(animation);
        vComboX.startAnimation(animation);
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }

    public ComboGiftVoInfo getComboGiftVoInfo() {
        return comboGiftVoInfo;
    }

    public void setComboGiftVoInfo(ComboGiftVoInfo voInfo) {
        this.comboGiftVoInfo = voInfo;
        currComboCount = 0;
    }

    public void updateComboItemNumRange(ComboRang comboRang) {
        if (null != comboGiftVoInfo) {
            comboGiftVoInfo.addComboRangToLast(comboRang);
        }
    }

    public interface OnOutAnimEndListener {
        void onOutAnimEnd(ComboGiftItemView comboGiftItemView);
    }

}

