package com.yuhuankj.tmxq.ui.launch.empty;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.ui.MainActivity;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.AVRoomActivity;

/**
 * 空的activity，做跳转
 */
public class EmptyActivity extends BaseMvpActivity<IMvpBaseView, EmptyPresenter> implements IMvpBaseView {

    public static void start(Context context, long roomUid, int type) {
        Intent intent = new Intent(context, EmptyActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Constants.ROOM_TYPE, type);
        intent.putExtra(Constants.ROOM_UID, roomUid);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_transparent);
        View contentView = findViewById(R.id.content_layout);
        contentView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                finish();
                return false;
            }
        });
        Intent intent = getIntent();
        if (intent == null) {
            finish();
            return;
        }
        int type = intent.getIntExtra(Constants.ROOM_TYPE, 0);
        long uid = getIntent().getLongExtra(Constants.ROOM_UID, 0);
        if (type == RoomInfo.ROOMTYPE_HOME_PARTY) {//进入直播间
            AVRoomActivity.start(this, uid);
        } else if (type == RoomInfo.ROOMTYPE_MULTI_AUDIO || type == RoomInfo.ROOMTYPE_SINGLE_AUDIO || type == RoomInfo.ROOMTYPE_NEW_AUCTION) {//打开首页
            RoomServiceScheduler.getInstance().enterRoom(this, uid, type);
        } else {
            MainActivity.start(this);
        }
    }

//    @Override
//    protected boolean needSteepStateBar() {
//        return false;
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        finish();
    }
}
