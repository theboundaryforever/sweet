package com.yuhuankj.tmxq.ui.liveroom.nimroom.widget;

import android.content.Context;
import android.util.AttributeSet;

public class BossMicWaveView extends WaveView {

    public BossMicWaveView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public BossMicWaveView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BossMicWaveView(Context context) {
        super(context);
    }

    @Override
    public void init() {
        customRadus = 30f;
        addRadus = 26;
        delRadus = 22;
        stopRadus = 30;
        middleRadus = 26;
        alphaMax = 6;
        alphaDel = 1;
        super.init();
    }
}
