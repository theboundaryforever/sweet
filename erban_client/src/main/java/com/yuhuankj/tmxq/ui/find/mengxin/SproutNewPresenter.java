package com.yuhuankj.tmxq.ui.find.mengxin;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.room.model.RoomSettingModel;

import java.util.List;
import java.util.Map;

public class SproutNewPresenter extends AbstractMvpPresenter<SproutNewView> {

    private final String TAG = SproutNewPresenter.class.getSimpleName();

    private RoomSettingModel model;

    public SproutNewPresenter() {
        model = new RoomSettingModel();
    }

    public void getSproutUserList(SproutNewFragment.SproutListType type, int pageNum, int pageSize) {
        LogUtils.d(TAG, "getSproutUserList-type:" + type + " pageNum:" + pageNum + " pageSize:" + pageSize);
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("pageNum", pageNum + "");
        params.put("pageSize", pageSize + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");

        OkHttpManager.getInstance().postRequest(type == SproutNewFragment.SproutListType.sprout ? UriProvider.getSproutNewUserList() : UriProvider.getSproutNearByUserList(),
                params, new OkHttpManager.MyCallBack<ServiceResult<List<SproutUserInfo>>>() {
                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                        if (null != getMvpView()) {
                            getMvpView().onGetSproutNewList(false, e.getMessage(), null, pageNum);
                        }
                    }

                    @Override
                    public void onResponse(ServiceResult<List<SproutUserInfo>> response) {
                        if (null != getMvpView()) {
                            if (null != response && response.isSuccess()) {
                                getMvpView().onGetSproutNewList(true, response.getMessage(), response.getData(), pageNum);
                            } else {
                                getMvpView().onGetSproutNewList(false, response.getMessage(), null, pageNum);
                            }
                        }
                    }
                });
    }


}
