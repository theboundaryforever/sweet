package com.yuhuankj.tmxq.ui.liveroom.imroom.publicscreen.base;

import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tongdaxing.erban.libcommon.widget.messageview.MsgHolderLayoutId;
import com.tongdaxing.erban.libcommon.widget.messageview.RoomMsgHolerType;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.imroom.publicscreen.holder.RoomMsgChatTxtHolder;
import com.yuhuankj.tmxq.ui.liveroom.imroom.publicscreen.holder.RoomMsgGiftHolder;
import com.yuhuankj.tmxq.ui.liveroom.imroom.publicscreen.holder.RoomMsgTxtHolder;
import com.yuhuankj.tmxq.ui.liveroom.imroom.publicscreen.holder.RoomMsgTxtNoBgHolder;

import java.lang.reflect.Constructor;

/**
 * 文件描述：房间消息类型管理类
 * 1.注册新的消息类型和hodler   2.根据消息类型获取不同的消息类型的hodler使用
 * @auther：zwk
 * @data：2019/1/29
 */
public class RoomMsgHolderFactory {
    public static final String DEFUAL_MSG_CONTENT = "不支持消息类型";

    private static SparseArray<Class<? extends RoomMsgBaseHolder>> holderList = new SparseArray<>();

    static {
        register(RoomMsgHolerType.MSG_DEFAULT_TXT, RoomMsgDefaultTextHolder.class);
        register(RoomMsgHolerType.MSG_TXT, RoomMsgTxtHolder.class);
        register(RoomMsgHolerType.MSG_CHAT_TXT, RoomMsgChatTxtHolder.class);
        register(RoomMsgHolerType.MSG_GIFT, RoomMsgGiftHolder.class);
        register(RoomMsgHolerType.MSG_TXT_NO_BG, RoomMsgTxtNoBgHolder.class);
    }

    public static void register(int msgType, Class<? extends RoomMsgBaseHolder> viewHolder) {
        holderList.put(msgType, viewHolder);
    }

    /**
     * 消息holder工厂
     *
     * @param context
     * @param parent
     * @param viewType
     * @return
     */
    public static RoomMsgBaseHolder createRoomMsgHolder(Context context, ViewGroup parent, int viewType) {
        RoomMsgBaseHolder holder = null;
        Class<?> holderClass = holderList.get(viewType);
        View itemView = null;
        if (holderClass != null) {
            MsgHolderLayoutId annotation = holderClass.getAnnotation(MsgHolderLayoutId.class);
            if (annotation == null &&  holderClass.getSuperclass() != null){
                annotation = holderClass.getSuperclass().getAnnotation(MsgHolderLayoutId.class);
            }
            if (annotation != null) {
                try {
                    itemView = LayoutInflater.from(context).inflate(annotation.value(), parent, false);
                    //根据参数类型获取相应的构造函数
                    Constructor constructor = holderClass.getConstructor(View.class);
                    holder = (RoomMsgBaseHolder) constructor.newInstance(itemView);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (itemView == null) {
            itemView = LayoutInflater.from(context).inflate(R.layout.item_room_msg_default_txt, parent, false);
        }
        if (holder == null) {
            holder = new RoomMsgDefaultTextHolder(itemView);
        }
        return holder;
    }
}
