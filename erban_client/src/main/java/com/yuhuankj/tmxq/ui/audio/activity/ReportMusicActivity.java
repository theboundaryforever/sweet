package com.yuhuankj.tmxq.ui.audio.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import com.netease.nimlib.sdk.StatusCode;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomKickOutEvent;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.im.login.IIMLoginClient;
import com.tongdaxing.xchat_core.room.IRoomCoreClient;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.databinding.ActivityMusicReportBinding;
import com.yuhuankj.tmxq.ui.audio.adapter.MusicReportReasonListAdapter;
import com.yuhuankj.tmxq.ui.audio.presenter.MusicReportPresenter;
import com.yuhuankj.tmxq.ui.widget.DividerItemDecoration;

import java.util.Arrays;

/**
 * Created by chenran on 2017/10/28.
 */
@CreatePresenter(MusicReportPresenter.class)
public class ReportMusicActivity extends BaseMvpActivity<MusicReportView, MusicReportPresenter>
        implements MusicReportView,View.OnClickListener, MusicReportReasonListAdapter.OnMusicReportReasonClickListener {

    private final String TAG = ReportMusicActivity.class.getSimpleName();

    private ActivityMusicReportBinding musicReportBinding;

    private MusicReportReasonListAdapter adapter;

    private long singId = -1L;
    private String lastChoosedReportReason = null;

    public static void start(Context context, long singId) {
        Intent intent = new Intent(context, ReportMusicActivity.class);
        intent.putExtra("singId",singId);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View content = findViewById(android.R.id.content);
        ViewGroup.LayoutParams params = content.getLayoutParams();
        params.height = getResources().getDisplayMetrics().heightPixels;

        musicReportBinding = DataBindingUtil.setContentView(this, R.layout.activity_music_report);
        musicReportBinding.setClick(this);
        adapter = new MusicReportReasonListAdapter(this);
        adapter.setOnMusicReportReasonClickListener(this);
        adapter.setReportReasonList(Arrays.asList(getResources().getStringArray(R.array.music_report_reasons)));
        LinearLayoutManager linearLayoutManager = new RecyclerViewNoBugLinearLayoutManager(this);
        musicReportBinding.rvReportMusic.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, linearLayoutManager.getOrientation(),
                1, R.color.color_1Affffff);
        dividerItemDecoration.setIngoreLastDividerItem(true);
        musicReportBinding.rvReportMusic.addItemDecoration(dividerItemDecoration);
        musicReportBinding.rvReportMusic.setAdapter(adapter);

        singId = getIntent().getLongExtra("singId",singId);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_reportMusic:
                if(singId>0 && !TextUtils.isEmpty(lastChoosedReportReason)){
                    getMvpPresenter().reportMusic(lastChoosedReportReason,singId);
                }
                break;
            case R.id.back_btn:
                finish();
                break;
            default:
        }
    }


    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onBeKickOut(ChatRoomKickOutEvent.ChatRoomKickOutReason reason) {
        finish();
    }

    @CoreEvent(coreClientClass = IIMLoginClient.class)
    public void onKickedOut(StatusCode code) {
        finish();
    }

    @Override
    public void onMusicReport(boolean isSuccess, String msg) {
        if(!TextUtils.isEmpty(msg)){
            toast(msg);
        }
        if(isSuccess){
            finish();
        }
    }

    @Override
    public void onReportReasonChoosed(String reason) {
        lastChoosedReportReason = reason;
    }
}
