package com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.juxiao.library_utils.DisplayUtils;
import com.tongdaxing.erban.libcommon.base.AbstractMvpRelativeLayout;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomEvent;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.SingleAudioRoomEnitity;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter.ReturnRoomAdapter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter.RecommRoomListPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IRecommRoomListView;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * 创建者      魏海涛
 * 创建时间    2019年4月25日 18:12:44
 * 描述        新多人房，用户触摸物理返回键按钮，弹出推荐房间列表，封装view
 * NOTE 仅一次请求检测判断
 *
 * @author dell
 */
@CreatePresenter(RecommRoomListPresenter.class)
public class ReturnRoomListView extends AbstractMvpRelativeLayout<IRecommRoomListView, RecommRoomListPresenter<IRecommRoomListView>>
        implements IRecommRoomListView {

    private final String TAG = ReturnRoomListView.class.getSimpleName();

    private ReturnRoomAdapter adapter;

    private RecyclerView rvFriendList;

    private Call<ResponseBody> lastRequestCall;

    private View blllRoomList;

    public ReturnRoomListView(Context context) {
        this(context, null);
    }

    public ReturnRoomListView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ReturnRoomListView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getViewLayoutId() {
        return R.layout.view_return_room_list;
    }

    @Override
    public void initialView(Context context) {
        setBackgroundColor(Color.parseColor("#00000000"));
        rvFriendList = findViewById(R.id.rvFriendList);
        adapter = new ReturnRoomAdapter(new ArrayList<>());
        rvFriendList.setAdapter(adapter);
        blllRoomList = findViewById(R.id.blllRoomList);
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initViewState() {
        LogUtils.d(TAG, "initViewState");
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        LogUtils.d(TAG, "onAttachedToWindow");
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        LogUtils.d(TAG, "onDetachedFromWindow");
        release();
    }

    public void refreshRecommRoomList() {
        LogUtils.d(TAG, "refreshRecommRoomList hasRequestRecommRoomList:" + RoomDataManager.get().hasRequestRecommRoomList);
        if (!ListUtils.isListEmpty(RoomDataManager.get().getSingleAudioRoomEnitities())) {
            refreshRecommRoomList(RoomDataManager.get().getSingleAudioRoomEnitities());
        } else {
            adapter.setNewData(new ArrayList<>());
            lastRequestCall = getMvpPresenter().getReturnRoomList();
        }
    }

    public void playInAnim() {
        LogUtils.d(TAG, "playInAnim");
        setVisibility(View.VISIBLE);
        //位移动画，如果包含了诸如界面数据的动态更新设置，那么绝对位置的位移动画最为平滑
        TranslateAnimation translateAni = new TranslateAnimation(-DisplayUtils.dip2px(getContext(), 116), 0, 0, 0);
        translateAni.setDuration(300L);
        translateAni.setRepeatCount(0);
        translateAni.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                LogUtils.d(TAG, "playInAnim-->onAnimationStart");
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                LogUtils.d(TAG, "playInAnim-->onAnimationEnd setOnClickListener");
                if (null != blllRoomList) {
                    blllRoomList.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            LogUtils.d(TAG, "playInAnim-->onAnimationEnd--> onClick1");
                        }
                    });
                }
                setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LogUtils.d(TAG, "playInAnim-->onAnimationEnd--> onClick0");
                        playOutAnim();
                    }
                });
                if (null != adapter) {
                    adapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {

                        @Override
                        public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                            List<SingleAudioRoomEnitity> enitityList = adapter.getData();
                            if (enitityList.size() > position) {
                                SingleAudioRoomEnitity enitity = enitityList.get(position);
                                if (null != enitity) {
                                    IMRoomEvent imRoomEvent = new IMRoomEvent();
                                    imRoomEvent.setEvent(RoomEvent.ROOM_ENTER_OTHER_ROOM);
                                    imRoomEvent.setRoomUid(enitity.getUid());
                                    imRoomEvent.setRoomType(enitity.getType());
                                    IMRoomMessageManager.get().getIMRoomEventObservable().onNext(imRoomEvent);
                                }
                                playOutAnim();
                            }
                        }
                    });
                }

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                LogUtils.d(TAG, "playInAnim-->onAnimationRepeat");
            }
        });
        startAnimation(translateAni);
    }

    public void playOutAnim() {
        LogUtils.d(TAG, "playOutAnim");
        TranslateAnimation translateAni = new TranslateAnimation(0, -DisplayUtils.dip2px(getContext(), 116), 0, 0);
        translateAni.setDuration(300L);
        translateAni.setRepeatCount(0);
        translateAni.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                LogUtils.d(TAG, "playOutAnim-->onAnimationStart setOnClickListener null");
                setOnClickListener(null);
                if (null != blllRoomList) {
                    blllRoomList.setOnClickListener(null);
                }
                if (null != adapter) {
                    adapter.setOnItemChildClickListener(null);
                }
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                LogUtils.d(TAG, "playOutAnim-->onAnimationEnd");
                setVisibility(View.GONE);
                release();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                LogUtils.d(TAG, "playOutAnim-->onAnimationRepeat");
            }
        });
        startAnimation(translateAni);
    }

    public void release() {
        LogUtils.d(TAG, "release");
        if (null != lastRequestCall && lastRequestCall.isExecuted()) {
            lastRequestCall.cancel();
            lastRequestCall = null;
        }
        if (getAnimation() != null && !getAnimation().hasEnded()) {
            getAnimation().cancel();
        }
    }

    @Override
    public void refreshRecommRoomList(List<SingleAudioRoomEnitity> enitityList) {
        LogUtils.d(TAG, "refreshRecommRoomList");
        adapter.setNewData(enitityList);
        playInAnim();
        lastRequestCall = null;
    }
}
