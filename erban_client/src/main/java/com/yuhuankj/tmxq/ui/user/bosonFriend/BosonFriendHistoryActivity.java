package com.yuhuankj.tmxq.ui.user.bosonFriend;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.netease.nim.uikit.common.util.sys.NetworkUtil;
import com.netease.nim.uikit.session.activity.P2PMessageActivity;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.user.bean.BosonFriendEnitity;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liaoxy
 * @Description:挚友历史页
 * @date 2019/5/5 16:20
 */
public class BosonFriendHistoryActivity extends BaseActivity {
    private List<BosonFriendEnitity> datas;
    private LinearLayout llEmply;
    private RecyclerView rcvDatas;
    private BaseQuickAdapter adapter;
    private SwipeRefreshLayout sprContent;
    private int page = Constants.PAGE_START;

    private SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            if (NetworkUtil.isNetAvailable(BosonFriendHistoryActivity.this)) {
                page = Constants.PAGE_START;
                getBosonFriendRecords();
            } else {
                sprContent.setRefreshing(false);
            }
        }
    };

    private BaseQuickAdapter.RequestLoadMoreListener loadMoreListener = new BaseQuickAdapter.RequestLoadMoreListener() {
        @Override
        public void onLoadMoreRequested() {
            // 解决数据重复的问题
            if (page == Constants.PAGE_START) {
                return;
            }
            if (NetworkUtil.isNetAvailable(BosonFriendHistoryActivity.this)) {
                if (datas.size() >= Constants.PAGE_SIZE) {
                   getBosonFriendRecords();
                } else {
                    adapter.setEnableLoadMore(false);
                }
            } else {
                adapter.loadMoreEnd(true);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitor);
        initTitleBar("挚友历史");
        mTitleBar.setCommonBackgroundColor(Color.WHITE);
        initView();
        initData();
    }

    private void initView() {
        sprContent = (SwipeRefreshLayout) findViewById(R.id.sprContent);
        rcvDatas = (RecyclerView) findViewById(R.id.rcvFriends);
        llEmply = (LinearLayout) findViewById(R.id.llEmply);

        sprContent.setOnRefreshListener(onRefreshListener);
    }

    private void initData() {
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(P2PMessageActivity.ACTION_FINISH));
        ImageView imvEmply = findView(R.id.imvEmply);
        TextView tvEmplyTip = findView(R.id.tvEmplyTip);
        imvEmply.setImageResource(R.drawable.ic_bosonfriend_history_emply);
        tvEmplyTip.setText("暂无历史");
        LinearLayoutManager layoutmanager = new RecyclerViewNoBugLinearLayoutManager(this);
        layoutmanager.setOrientation(LinearLayoutManager.VERTICAL);
        rcvDatas.setLayoutManager(layoutmanager);
        datas = new ArrayList<>();

        llEmply.setVisibility(View.GONE);
        adapter = new BosonFriendHistoryAdapter(datas);
        rcvDatas.setAdapter(adapter);
        adapter.setOnLoadMoreListener(loadMoreListener, rcvDatas);
        adapter.setEnableLoadMore(false);
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                try {
                    BosonFriendEnitity msgEnitity = datas.get(position);
                    UserInfoActivity.start(BosonFriendHistoryActivity.this,msgEnitity.getfUid());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        getBosonFriendRecords();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void getBosonFriendRecords() {
        new BosonFriendModel().getBosonFriendRecords(new OkHttpManager.MyCallBack<ServiceResult<List<BosonFriendEnitity>>>() {

            @Override
            public void onError(Exception e) {
                sprContent.setRefreshing(false);
                adapter.loadMoreComplete();
                SingleToastUtil.showToast("获取挚友列表失败");
            }

            @Override
            public void onResponse(ServiceResult<List<BosonFriendEnitity>> response) {
                sprContent.setRefreshing(false);
                if (response == null || response.getData() == null) {
                    SingleToastUtil.showToast("获取挚友列表失败");
                    return;
                }
                if (!response.isSuccess()) {
                    SingleToastUtil.showToast(response.getErrorMessage());
                    return;
                }
                List<BosonFriendEnitity> tmps = response.getData();
                if (tmps.size() >= Constants.PAGE_SIZE) {
                    adapter.setEnableLoadMore(true);
                    page++;
                } else {
                    adapter.setEnableLoadMore(false);
                }
                if (page == Constants.PAGE_START) {
                    datas.clear();
                }
                datas.addAll(tmps);
                adapter.notifyDataSetChanged();
                if (datas.size() == 0) {
                    llEmply.setVisibility(View.VISIBLE);
                } else {
                    llEmply.setVisibility(View.GONE);
                }
            }
        });
    }
}
