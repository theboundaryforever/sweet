package com.yuhuankj.tmxq.ui.widget.itemdecotion;

import android.view.View;


public interface PowerGroupListener {

    String getGroupName(int position);

    View getGroupView(int position);
}
