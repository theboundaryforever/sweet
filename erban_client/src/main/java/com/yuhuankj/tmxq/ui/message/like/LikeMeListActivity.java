package com.yuhuankj.tmxq.ui.message.like;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tencent.bugly.crashreport.BuglyLog;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.thirdsdk.nim.MsgCenterRedPointStatusManager;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * 我的点赞列表
 *
 * @author weihaitao
 * @date 2019/4/17
 */
@CreatePresenter(LikeMeListPresenter.class)
public class LikeMeListActivity extends BaseMvpActivity<LikeMeListView, LikeMeListPresenter>
        implements LikeMeListView, SwipeRefreshLayout.OnRefreshListener, BaseQuickAdapter.RequestLoadMoreListener, MsgCenterRedPointStatusManager.OnRedPointStatusChangedListener {

    private final String TAG = LikeMeListActivity.class.getSimpleName();
    private final int dataLoadStartPage = 1;
    private final int dataLoadPageSize = 10;
    private SwipeRefreshLayout srlDownLoadAnim;
    private RecyclerView rvFriendList;
    private LikeAdapter likeAdapter;
    private int nextDataLoadPage = 1;
    private boolean isLoadingData = false;

    private List<LikeEnitity> datas;

    public static void start(Context context) {
        context.startActivity(new Intent(context, LikeMeListActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_list);
        initTitleBar(getResources().getString(R.string.my_like));
        if(null != mTitleBar){
            mTitleBar.setCommonBackgroundColor(Color.WHITE);
        }
        initView();
        initListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ListUtils.isListEmpty(likeAdapter.getData())) {
            loadData();
        }
    }

    private void initView() {
        srlDownLoadAnim = (SwipeRefreshLayout) findViewById(R.id.srlDownLoadAnim);
        srlDownLoadAnim.setEnabled(true);
        rvFriendList = (RecyclerView) findViewById(R.id.rvFriendList);
        LinearLayoutManager linearLayoutManager = new RecyclerViewNoBugLinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvFriendList.setLayoutManager(linearLayoutManager);
        datas = new ArrayList<>();
        likeAdapter = new LikeAdapter(datas);
        likeAdapter.bindToRecyclerView(rvFriendList);
        rvFriendList.setAdapter(likeAdapter);
    }

    private void initListener() {
        srlDownLoadAnim.setOnRefreshListener(this);
        likeAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (adapter.getData().size() > position) {
                    try {
                        LikeEnitity like = likeAdapter.getData().get(position);
                        UserInfoActivity.start(LikeMeListActivity.this, like.getUid());
                        if (like.getIsRead() == 0 && !likeAdapter.isRead(like)) {
                            //未读消息且是第一次点击
                        }
                        likeAdapter.addNoshowRedPoint(like);
                        likeAdapter.notifyItemChanged(position);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        likeAdapter.setOnLoadMoreListener(this, rvFriendList);
        MsgCenterRedPointStatusManager.getInstance().addListener(this);
    }

    private void loadData() {
        BuglyLog.d(TAG, "loadData-isLoadingData:" + isLoadingData);
        if (!isLoadingData) {
            nextDataLoadPage = dataLoadStartPage;
            isLoadingData = true;
            getDialogManager().showProgressDialog(this, getResources().getString(R.string.network_loading));
            getMvpPresenter().getMyLikeList(nextDataLoadPage, dataLoadPageSize);
        }
    }

    private void loadMoreData() {
        BuglyLog.d(TAG, "loadMoreData-isLoadingData:" + isLoadingData);
        if (!isLoadingData) {
            isLoadingData = true;
            getDialogManager().showProgressDialog(this, getResources().getString(R.string.network_loading));
            getMvpPresenter().getMyLikeList(nextDataLoadPage, dataLoadPageSize);
        }
    }

    @Override
    public void onRefresh() {
        if (!isLoadingData) {
            nextDataLoadPage = dataLoadStartPage;
            isLoadingData = true;
            BuglyLog.d(TAG, "onRefresh-nextDataLoadPage:" + nextDataLoadPage);
            getMvpPresenter().getMyLikeList(nextDataLoadPage, dataLoadPageSize);
        }
    }


    @Override
    public void showLikeList(List list) {
        LogUtils.d(TAG, "showLikeList");
        if (dataLoadStartPage == nextDataLoadPage) {
            likeAdapter.setNewData(list);
            //下拉刷新后返回的数据，不够10个的话，禁用上拉加载更多的功能
            likeAdapter.setEnableLoadMore(list.size() >= dataLoadPageSize);
        } else {
            likeAdapter.addData(list);
            likeAdapter.loadMoreComplete();
            likeAdapter.loadMoreEnd(list.size() == 0);
        }
        nextDataLoadPage += 1;
        isLoadingData = false;
        getDialogManager().dismissDialog();
        srlDownLoadAnim.setRefreshing(false);
    }

    @Override
    public void showLikeListEmptyView() {
        LogUtils.d(TAG, "showLikeListEmptyView");
        likeAdapter.setEmptyView(getEmptyView(rvFriendList, getString(R.string.no_like_text)));
        isLoadingData = false;
        getDialogManager().dismissDialog();
        srlDownLoadAnim.setRefreshing(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MsgCenterRedPointStatusManager.getInstance().removeListener(this);
    }

    @Override
    public void onLoadMoreRequested() {
        // 解决数据重复的问题
        if (nextDataLoadPage == dataLoadStartPage) {
            return;
        }
        if (likeAdapter.getData().size() >= dataLoadPageSize) {
            BuglyLog.d(TAG, "onLoadMoreRequested-加载更多数据");
            loadMoreData();
        } else {
            likeAdapter.setEnableLoadMore(false);
        }

    }

    /**
     * 显示新粉丝红点
     */
    @Override
    public void showRedPointOnNewFan() {

    }

    /**
     * 显示新好友红点
     */
    @Override
    public void showRedPointOnNewFriend() {

    }

    /**
     * 显示新点赞红点
     */
    @Override
    public void showRedPointOnNewLike() {
        loadData();
    }

    /**
     * 签到红点
     */
    @Override
    public void showRedPointOnSignIn() {

    }
}
