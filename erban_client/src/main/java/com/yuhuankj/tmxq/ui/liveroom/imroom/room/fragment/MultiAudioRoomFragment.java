package com.yuhuankj.tmxq.ui.liveroom.imroom.room.fragment;

import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewStub;
import android.widget.TextView;

import com.netease.nimlib.sdk.msg.model.RecentContact;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.gift.ComboGiftPublicScreenMsgSender;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftType;
import com.tongdaxing.xchat_core.im.message.IIMMessageCore;
import com.tongdaxing.xchat_core.im.message.IIMMessageCoreClient;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomQueueInfo;
import com.tongdaxing.xchat_core.room.bean.RoomAdditional;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.tongdaxing.xchat_core.room.queue.bean.RoomConsumeInfo;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.audio.widget.MusicPlayerView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.gift.RoomGiftDialog;
import com.yuhuankj.tmxq.ui.liveroom.imroom.pk.RoomPKView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.populargift.OnPopularGiftResultClick;
import com.yuhuankj.tmxq.ui.liveroom.imroom.populargift.PopularGiftView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.publicscreen.MessageView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.redpackage.RoomRedPackageView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.activity.RoomCallUpUserListActivity;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.base.BaseRoomDetailFragment;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.listener.OnMicroItemClickListener;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter.MultiAudioRoomPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IMultiAudioView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.ComingMsgView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.MultiAudioMicroView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.MultiInputMsgView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.RoomAdmireView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.RoomCallUpView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.RoomLotteryBoxView;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.activity.RoomTopicActivity;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.dialog.CallUpDialog;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.dialog.RoomPrivateMsgDialog;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.face.DynamicFaceDialog;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.widget.GiftV2View;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.widget.MultiBottomView;
import com.yuhuankj.tmxq.ui.widget.BigListDataDialog;
import com.yuhuankj.tmxq.ui.widget.RoomMoreOpearDialog;
import com.yuhuankj.tmxq.ui.widget.RoomTopicDIalog;
import com.yuhuankj.tmxq.widget.Banner;

import java.util.List;

import butterknife.BindView;

/**
 * 文件描述：新版多人语聊房
 * 目前流程：调用user/get接口和socket进房后
 * 显示主要UI，和声望初始化同步进行，声网初始化成功后显示底部麦位
 *
 * @auther：zwk
 * @data：2019/6/3
 */
@CreatePresenter(MultiAudioRoomPresenter.class)
public class MultiAudioRoomFragment extends BaseRoomDetailFragment<IMultiAudioView, MultiAudioRoomPresenter, MultiAudioMicroView> implements IMultiAudioView, OnPopularGiftResultClick {

    @NonNull
    @Override
    protected MultiAudioMicroView getMicroView() {
        return multiAudioMicroView;
    }

    @NonNull
    @Override
    protected MessageView getMessageView() {
        return messageView;
    }

    @NonNull
    @Override
    protected GiftV2View getGiftView() {
        return giftV2View;
    }

    @NonNull
    @Override
    protected MultiBottomView getBottomView() {
        return bottomView;
    }

//    @BindView(R.id.cgvGiftAnim)
//    public ComboGiftView cgvGiftAnim;
//    @BindView(R.id.rgcsComboView)
//    RoomGiftComboSender rgcsComboView;

    @Override
    protected TextView getFollowRoomView() {
        return multiAudioMicroView.getBltAttention();
    }

    @NonNull
    @Override
    protected ComingMsgView getComingMsgView() {
        return comingMsgView;
    }

    @BindView(R.id.ravAdmire)
    RoomAdmireView ravAdmire;

//    @Override
//    protected RoomGiftComboSender getRoomGiftComboSenderView() {
//        return rgcsComboView;
//    }

//    @NonNull
//    @Override
//    protected ImageView getRoomBgView() {
//        return ivBack;
//    }

    @NonNull
    @Override
    protected MultiInputMsgView getInputMsgView() {
        return inputMsgView;
    }

    @NonNull
    @Override
    protected View getMoreOperateBtn() {
        return getMicroView().getIvMoreFunction();
    }

    @BindView(R.id.mamv_multi_room_micro_view)
    public MultiAudioMicroView multiAudioMicroView;
    @BindView(R.id.mv_multi_room_msg_list)
    public MessageView messageView;
    @BindView(R.id.bv_mulit_room_bottom)
    public MultiBottomView bottomView;
    @BindView(R.id.imv_multi_input_msg)
    public MultiInputMsgView inputMsgView;
    @BindView(R.id.gv_multi_gift_effect)
    public GiftV2View giftV2View;

//    @Override
//    protected ComboGiftView getComboGiftView() {
//        return cgvGiftAnim;
//    }
    @BindView(R.id.vs_music_player)
    public ViewStub mVsMusicPlayer;
    @BindView(R.id.rlbv_lottery_box)
    public RoomLotteryBoxView roomLotteryBoxView;
    @BindView(R.id.cmv_multi_msg)
    public ComingMsgView comingMsgView;
    @BindView(R.id.room_pk_view)
    RoomPKView homePartyPKView;
    @BindView(R.id.pgv_gift)
    PopularGiftView popularGiftView;
    @BindView(R.id.rcvCallUp)
    RoomCallUpView rcvCallUp;
    @BindView(R.id.banner_activity)
    Banner bannerActivity;

    @BindView(R.id.rrpv_room_red_package)
    RoomRedPackageView roomRedPackageView;

    @Override
    protected RoomAdmireView getRoomAdmireView() {
        return ravAdmire;
    }

    @Override
    protected Banner getBanner() {
        return bannerActivity;
    }

    private DynamicFaceDialog dynamicFaceDialog;
    private MusicPlayerView mMusicPlayerView;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_multi_audio_room;
    }

    @Override
    public void onFindViews() {
        giftV2View.setRoomType(RoomInfo.ROOMTYPE_MULTI_AUDIO);
        messageView.initMsgData();
        bottomView.initState();
        ravAdmire.initLayoutParams(getContext(),RoomInfo.ROOMTYPE_MULTI_AUDIO);
        popularGiftView.initLayoutParams(getContext(),RoomInfo.ROOMTYPE_MULTI_AUDIO);
        popularGiftView.setOnPopularGiftResultClick(this);
        popularGiftView.setViewRightMargin(DisplayUtility.dp2px(getActivity(), 190));
        popularGiftView.addTouchEvent();
//        RelativeLayout rl = mView.findViewById(R.id.ll_multi_room_container);
//        //构建LayoutTransition
//        LayoutTransition mTransitioner = new LayoutTransition();
//        //设置给ViewGroup容器
//        mTransitioner.setStagger(LayoutTransition.CHANGE_APPEARING, 100);
//        PropertyValuesHolder pvhLeft =
//                PropertyValuesHolder.ofFloat("alpha", 0.5f, 1.0f);
//        ObjectAnimator changeIn = ObjectAnimator.ofPropertyValuesHolder(
//                getActivity(), pvhLeft).setDuration(mTransitioner.getDuration(LayoutTransition.CHANGE_APPEARING));
//        //设置过渡动画
//        mTransitioner.setAnimator(LayoutTransition.CHANGE_APPEARING, changeIn);
//        rl.setLayoutTransition(mTransitioner);

//        if (CoreManager.getCore(VersionsCore.class).isGiftComboSwitchOpen()) {
//            cgvGiftAnim.setVisibility(View.VISIBLE);
//        } else {
//            cgvGiftAnim.setVisibility(View.GONE);
//        }

        //保证有未读私聊消息就展示红点
        changeState();
        super.onFindViews();
    }


    @Override
    public void initiate() {
        super.initiate();
        getMicroView().setOnOwnerMicroItemClickListener(v -> getMvpPresenter().dealWithMicroClickEvent(getContext(), RoomDataManager.get().getRoomQueueMemberInfoByMicPosition(RoomDataManager.MIC_POSITION_BY_OWNER), RoomDataManager.MIC_POSITION_BY_OWNER));
        getMicroView().setOnRankMoreClickListener(v -> onShowRoomRankListDialog());
        getMicroView().setOnlinePeopleClickListener(v -> showOnlineMemberDialog(chatRoomMember -> getMvpPresenter().dealWithOnlinePeoleClickEvent(getContext(), getMvpPresenter().getCurrRoomInfo(), chatRoomMember)));
        getMicroView().setOnRoomTopicClickListener(v -> showRoomTopicView());
        getMicroView().setOnRoomShareClickListener(v -> showShareDialog());

        getMvpPresenter().getRoomActionList();

        if (null != roomLotteryBoxView) {
            roomLotteryBoxView.setOnQuickClickListener(this::showLotteryBoxDialog);
        }
        if (null != rcvCallUp) {
            rcvCallUp.setOnRoomCallUpViewClickListener(new RoomCallUpView.OnRoomCallUpViewClickListener() {
                @Override
                public void onRoomCallUpViewClick() {
                    //显示召集令弹框
                    if (null == RoomDataManager.get().getCurrentRoomInfo()) {
                        LogUtils.d(TAG, "房间信息为空");
                        return;
                    }
                    if (null == getActivity()) {
                        return;
                    }
                    if (RoomDataManager.get().conveneState == 0) {
                        if (!RoomDataManager.get().isRoomOwner()) {
                            LogUtils.d(TAG, "onRoomCallUpViewClick-非房主不可发起召集令");
                            return;
                        }
                        if (RoomDataManager.get().conveneCount <= 0) {
                            toast(getActivity().getResources().getString(R.string.room_call_up_tips_3));
                            return;
                        }
                        CallUpDialog callUprDialog = new CallUpDialog(getActivity(), RoomDataManager.get().getCurrentRoomInfo().getRoomId());
                        callUprDialog.show();

                        //新交友房间，左上角召集令，控件点击统计,事件同轰趴房，一致
                        StatisticManager.get().onEvent(getActivity(),
                                StatisticModel.EVENT_ID_ROOM_CALL_UP_CLICK,
                                StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
                    } else if (RoomDataManager.get().conveneState == 1) {
                        //跳转召集令召集成员列表
                        RoomCallUpUserListActivity.start(getActivity());
                    }
                }
            });
        }
        homePartyPKView.setOnPKVoteSendGiftClickListener(new RoomPKView.OnPKVoteSendGiftClickListener() {
            @Override
            public void onPkVoteSendGiftClick(long uid) {
                showGiftDialog(uid, true);
            }
        });
    }

    @CoreEvent(coreClientClass = IIMMessageCoreClient.class)
    public void onReceiveRecentContactChanged(List<RecentContact> imMessages) {
        //有新的联系人消息
        changeState();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo userInfo) {
        //切换用户
        changeState();
    }

    /**
     * 更新新消息提示红点显示状态
     */
    private void changeState() {
        int unreadCount = CoreManager.getCore(IIMMessageCore.class).queryUnreadMsg();
        LogUtils.d(TAG, "changeState-unreadCount:" + unreadCount);
        bottomView.showMsgMark(unreadCount > 0);
    }

    /**
     * 麦位和消息列表初始化显示
     */
    @Override
    protected void onRoomInSucShowMicroAndMessageView() {
        if (getMicroView().getVisibility() == View.INVISIBLE) {
            getMicroView().setVisibility(View.VISIBLE);
        }
        if (getMessageView().getVisibility() == View.INVISIBLE) {
            getMessageView().setVisibility(View.VISIBLE);
        }
        if (null != ravAdmire) {
            ravAdmire.updateRoomAdmireStatus();
        }
        if (RoomDataManager.get().getAdditional() != null) {
            if (null != rcvCallUp) {
                rcvCallUp.setVisibility(RoomDataManager.get().getAdditional().getIsConvene() == 1 ? View.VISIBLE : View.GONE);
            }
        }
    }

    @Override
    protected void updateRoomInfoAboutView(RoomInfo roomInfo) {
        getMicroView().updateRoomOwnerMicroInfo();
        getMicroView().updateViewFromRoomInfo(roomInfo);
        if (getMvpPresenter().getCurrRoomInfo() != null) {
            getMvpPresenter().getFortuneRankTopData(getMvpPresenter().getCurrRoomInfo().getUid(), getMvpPresenter().getCurrRoomInfo().getType());
        }
        if (null != ravAdmire && ravAdmire.getVisibility() == View.VISIBLE) {
            ravAdmire.initRoomAdmireView();
        }
        if (null != rcvCallUp && rcvCallUp.getVisibility() == View.VISIBLE) {
            rcvCallUp.initCallUpStatus();
        }
        ComboGiftPublicScreenMsgSender.getInstance().setCurrRoomId(roomInfo.getRoomId());
    }

    /**
     * 底部功能按钮初始化：目前的调用的逻辑顺序是优先处理显示声网以外的相关控件的显示
     * 声网初始化完成后，在显示，（提升部分速度）后续后端优化进房接口后在调整回来
     */
    @Override
    protected void onRoomAudioInitSucShowBottomView() {

    }

    /**
     * 动态改变的view初始状态，
     * 如砸蛋是否显示以及关注等其他状态在统一位置更新避免太过混乱
     */
    @Override
    protected void onRoomDynamicInitView() {
        super.onRoomDynamicInitView();
        homePartyPKView.initData(false);
        roomLotteryBoxView.initialView();
        if (RoomDataManager.get().getAdditional() != null) {
            popularGiftView.updatePopularGiftInfo(true, RoomDataManager.get().getAdditional().getDetonatingState());
        }
        roomRedPackageView.getRedPackageInfo();
    }

    @Override
    protected void refreshOnlineCount(int onlineNum) {
        getMicroView().updateRoomOnlinePeople(onlineNum);
    }


    @Override
    public void onGetFortuneRankTop(boolean isSuccess, String message, List<RoomConsumeInfo> roomConsumeInfos) {
        if (isSuccess && null != roomConsumeInfos) {
            getMicroView().setRankListInfo(roomConsumeInfos.size() >= 1 ? roomConsumeInfos.get(0).getAvatar() : "", roomConsumeInfos.size() >= 2 ? roomConsumeInfos.get(1).getAvatar() : "");
        }
    }


    /**
     * 麦位点击事件：
     *
     * @return
     */
    @Override
    public OnMicroItemClickListener getOnMicroItemClickListener() {
        return new OnMicroItemClickListener() {
            @Override
            public void onUpMicBtnClick(int position) {

            }

            @Override
            public void onMicroBtnClickListener(IMRoomQueueInfo roomQueueInfo, int micPosition) {
                if (getContext() != null) {
                    getMvpPresenter().dealWithMicroClickEvent(getContext(), roomQueueInfo, micPosition);
                }
            }
        };
    }

    /**
     * 右上角的多功能按钮点击事件：
     * 最小和退出房间
     */
    @Override
    public void showRoomMoreOperateDialog() {
        getMvpPresenter().dealWithRoomMoreOperateEvent();
    }

    @Override
    public void onSendFaceBtnClick() {
        if (getMvpPresenter().isOnMicByMyself() || getMvpPresenter().isRoomOwnerByMyself()) {
            if (dynamicFaceDialog == null) {
                dynamicFaceDialog = new DynamicFaceDialog(getContext());
                dynamicFaceDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        dynamicFaceDialog = null;
                    }
                });
            }
            if (!dynamicFaceDialog.isShowing()) {
                dynamicFaceDialog.show();
            }
        } else {
            toast("上麦才能发表情哦!");
        }
    }

    @Override
    public void onPublicRoomMsgBtnClick() {
        RoomPrivateMsgDialog msgDialog = new RoomPrivateMsgDialog();
        msgDialog.show(getChildFragmentManager(), null);
    }

    @Override
    public void onRoomMoreOperaClick() {
        if (getActivity() == null) {
            return;
        }
        RoomAdditional roomAdditional = RoomDataManager.get().getAdditional();
        RoomMoreOpearDialog roomMoreOpearDialog = new RoomMoreOpearDialog(getActivity());
        if (roomAdditional != null) {
            roomMoreOpearDialog.setLuckyWheelSwitch(roomAdditional.isBigWheelSwitch());
//            roomMoreOpearDialog.setRedPacketSwitch(roomAdditional.isRedPacketSwitch());
            roomMoreOpearDialog.setRedPacketSwitch(roomAdditional.isRedPacketSwitch());
        } else {
            roomMoreOpearDialog.setLuckyWheelSwitch(false);
            roomMoreOpearDialog.setRedPacketSwitch(false);
        }
        roomMoreOpearDialog.setCharmValueSwitch(RoomDataManager.get().getCurrentRoomInfo() != null && RoomDataManager.get().getCurrentRoomInfo().getCharmSwitch() == 1);
        roomMoreOpearDialog.setHasPKOpened(homePartyPKView != null && homePartyPKView.hasPkState());
        roomMoreOpearDialog.setOnLockyWheelGiftBtnClickListener(new RoomMoreOpearDialog.OnLockyWheelGiftBtnClickListener() {
            @Override
            public void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, long targetUid, int number) {
                //先用旧的
            }

            @Override
            public void showRoomGiftDialog(long uid) {
                RoomGiftDialog roomGiftDialog = RoomGiftDialog.getInstance(uid, GiftType.Package, true);
//                roomGiftDialog.setOnClickToSendGiftListener(new RoomGiftDialog.OnClickToSendGiftListener() {
//                    @Override
//                    public void onSendRoomMultiGift(GiftInfo giftInfo, long roomUid, long roomId,
//                                                    List<MicMemberInfo> micAvatar, int giftNum, boolean isAllMic) {
//                        MultiAudioRoomFragment.this.sendRoomMultiGift(giftInfo, roomUid, roomId, micAvatar, giftNum, false, isAllMic);
//                    }
//
//                    @Override
//                    public void sendSingleGift(GiftInfo giftInfo, long targetUid, long roomUid, long roomId, int giftNum) {
//                        MultiAudioRoomFragment.this.sendSingleGift(giftInfo, targetUid, roomUid, roomId, giftNum, false);
//                    }
//                });
                roomGiftDialog.show(getChildFragmentManager(), RoomGiftDialog.class.getSimpleName());
            }
        });
        roomMoreOpearDialog.show();
//        hideComBoSenderViewAfterShowGiftDialog();
    }


    /**
     * 显示房间话题：
     * 如果是房主或者管理员进入房间话题设置、普通用户是弹框提醒
     */
    public void showRoomTopicView() {
        if (RoomDataManager.get().isRoomOwner() || RoomDataManager.get().isRoomAdmin()) {
            RoomTopicActivity.start(getContext());
        } else {
            RoomTopicDIalog roomTopicDIalog = new RoomTopicDIalog();
            roomTopicDIalog.show(getChildFragmentManager());
        }
    }

    @Override
    public void refreshMicroView(int micPosition, int roomEvent) {
        if (micPosition == -2) {
            getMicroView().notifyDataSetChanged(roomEvent);
            getMicroView().updateRoomOwnerMicroInfo();
        } else {
            getMicroView().notifyItemChanged(micPosition, roomEvent);
            if (micPosition == -1) {
                getMicroView().updateRoomOwnerMicroInfo();
            }
        }
    }

    @Override
    public void refreshBottomView() {
        super.refreshBottomView();
        onShowMusicPlayView();
    }

    private void onShowMusicPlayView() {
        //获取自己是否在麦上
        boolean isOnMic = getMvpPresenter().isOnMicByMyself();
        // 更新播放器界面
        RoomInfo currRoomInfo = getMvpPresenter().getCurrRoomInfo();
        if (currRoomInfo == null) {
            return;
        }
        if (mMusicPlayerView == null && isOnMic) {
            mMusicPlayerView = (MusicPlayerView) mVsMusicPlayer.inflate();
        }
        if (mMusicPlayerView != null) {
            mMusicPlayerView.setVisibility(isOnMic ? View.VISIBLE : View.GONE);
            mMusicPlayerView.setImageBg(currRoomInfo.getBackPic());
            mMusicPlayerView.setRoomType(currRoomInfo.getType());
        }
    }

    @Override
    public void showGiftDialogView(long targetUid, boolean isPersonal) {
        showGiftDialog(targetUid, isPersonal);
    }

    /**
     * 个人房-房主-停播操作，跳转开播统计界面
     */
    @Override
    public void openRoomLiveInfoH5View() {

    }

    /**
     * 房间贡献榜弹框
     */
    public void onShowRoomRankListDialog() {
        if (getActivity() == null) {
            return;
        }
        BigListDataDialog bigListDataDialog = BigListDataDialog.newContributionListInstance(getActivity());
        bigListDataDialog.setSelectOptionAction(new BigListDataDialog.SelectOptionAction() {
            @Override
            public void optionClick() {
                getDialogManager().showProgressDialog(mContext, "请稍后");
            }

            @Override
            public void onDataResponse() {
                //请求结束前退出可能会导致奔溃，直接捕获没关系
                try {
                    getDialogManager().dismissDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        bigListDataDialog.show(getChildFragmentManager());
        //房间-贡献榜
        StatisticManager.get().onEvent(getActivity(),
                StatisticModel.EVENT_ID_ROOM_CONTRIBUTIONI_LIST,
                StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
    }

    @Override
    protected void onRoomBannerClick() {
        if (null == getActivity()) {
            return;
        }
        //新多人房，活动banner，点击
        StatisticManager.get().onEvent(getActivity(),
                StatisticModel.EVENT_ID_MULTI_ROOM_BANNER_CLICK,
                StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mMusicPlayerView != null) {
            mMusicPlayerView.updateVoiceValue();
        }
    }

    @Override
    protected void releaseView() {
        super.releaseView();
        //释放音乐播放器的view
        if (mMusicPlayerView != null) {
            mMusicPlayerView.release();
        }
        if (null != homePartyPKView) {
            homePartyPKView.setOnPKVoteSendGiftClickListener(null);
        }
    }


    @Override
    public boolean onBack() {
        return false;
    }

    @Override
    public void showGiftDilaogPackage() {
        if (getFragmentManager() == null) {
            return;
        }
        Fragment fragment = getFragmentManager().findFragmentByTag("giftDialog");
        RoomGiftDialog roomGiftDialog = null;
        if (fragment == null) {
            roomGiftDialog = RoomGiftDialog.getInstance(0, GiftType.Exclusive, false);

        } else {
            if (!fragment.isAdded() && !fragment.getUserVisibleHint()) {
                roomGiftDialog = RoomGiftDialog.getInstance(0, GiftType.Exclusive, false);
            }
        }
        if (null != roomGiftDialog) {
//            roomGiftDialog.setOnClickToSendGiftListener(new RoomGiftDialog.OnClickToSendGiftListener() {
//                @Override
//                public void onSendRoomMultiGift(GiftInfo giftInfo, long roomUid, long roomId,
//                                                List<MicMemberInfo> micAvatar, int giftNum, boolean isAllMic) {
//                    MultiAudioRoomFragment.this.sendRoomMultiGift(giftInfo, roomUid, roomId, micAvatar, giftNum, false, isAllMic);
//                }
//
//                @Override
//                public void sendSingleGift(GiftInfo giftInfo, long targetUid, long roomUid, long roomId, int giftNum) {
//                    MultiAudioRoomFragment.this.sendSingleGift(giftInfo, targetUid, roomUid, roomId, giftNum, false);
//                }
//            });
            roomGiftDialog.show(getFragmentManager(), "giftDialog");
//            hideComBoSenderViewAfterShowGiftDialog();
        }
    }

    /**
     * 非主播，点击底部语音连接按钮
     */
    @Override
    public void onFansReqAudioConnClick() {

    }

    /**
     * 主播，点击底部语音连接按钮
     */
    @Override
    public void onAnchorAudioConnRecvClick() {

    }
}
