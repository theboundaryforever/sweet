package com.yuhuankj.tmxq.ui.liveroom.imroom.gift.adapter;

import android.content.Context;
import android.support.annotation.ColorRes;
import android.view.View;
import android.widget.TextView;

import com.tongdaxing.xchat_core.home.TabInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.ArgbEvaluatorHolder;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.commonnavigator.titles.CommonPagerTitleView;

import java.util.List;

/**
 * 礼物分类样式栏的适配器
 *
 * @author Administrator
 * @date 2017/11/15
 */
public class GiftTabIndicatorAdapter extends CommonNavigatorAdapter {
    private List<TabInfo> mTitleList;

    private int normalColorId = 0xff777878;
    private int selectColorId = 0xff10CFA6;

    public GiftTabIndicatorAdapter(Context context, List<TabInfo> titleList) {
        mTitleList = titleList;
    }

    private int size = 12;

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public int getCount() {
        return mTitleList == null ? 0 : mTitleList.size();
    }

    public void setNormalColorId(@ColorRes int normalColorId) {
        this.normalColorId = normalColorId;
    }

    public void setSelectColorId(@ColorRes int selectColorId) {
        this.selectColorId = selectColorId;
    }

    @Override
    public IPagerTitleView getTitleView(Context context, final int i) {
        CommonPagerTitleView pagerTitleView = new CommonPagerTitleView(context);
        pagerTitleView.setContentView(R.layout.ly_indicator_home);
        final TextView indicatorText = pagerTitleView.findViewById(R.id.indicator1);
        indicatorText.setTextSize(size);
        View line = pagerTitleView.findViewById(R.id.indicator_line);
        if (i == 0) {
            line.setVisibility(View.GONE);
        } else {
            line.setVisibility(View.VISIBLE);
        }
        indicatorText.setText(mTitleList.get(i).getName());
        pagerTitleView.setOnPagerTitleChangeListener(new CommonPagerTitleView.OnPagerTitleChangeListener() {

            @Override
            public void onSelected(int index, int totalCount) {
            }

            @Override
            public void onDeselected(int index, int totalCount) {

            }

            @Override
            public void onLeave(int index, int totalCount, float leavePercent, boolean leftToRight) {
                // 实现颜色渐变
                int color = ArgbEvaluatorHolder.eval(leavePercent, selectColorId, normalColorId);
                indicatorText.setTextColor(color);
                indicatorText.setScaleX(1.0f + (0.9f - 1.0f) * leavePercent);
                indicatorText.setScaleY(1.0f + (0.9f - 1.0f) * leavePercent);

            }

            @Override
            public void onEnter(int index, int totalCount, float enterPercent, boolean leftToRight) {
                // 实现颜色渐变
                int color = ArgbEvaluatorHolder.eval(enterPercent, normalColorId, selectColorId);
                indicatorText.setTextColor(color);
                indicatorText.setScaleX(0.9f + (1.0f - 0.9f) * enterPercent);
                indicatorText.setScaleY(0.9f + (1.0f - 0.9f) * enterPercent);
            }
        });
        pagerTitleView.setOnClickListener(view -> {
            if (mOnItemSelectListener != null) {
                mOnItemSelectListener.onItemSelect(i);
            }
        });
        return pagerTitleView;
    }


    @Override
    public IPagerIndicator getIndicator(Context context) {
        return null;
    }

    private OnItemSelectListener mOnItemSelectListener;

    public void setOnItemSelectListener(OnItemSelectListener onItemSelectListener) {
        mOnItemSelectListener = onItemSelectListener;
    }

    public interface OnItemSelectListener{
        void onItemSelect(int position);
    }
}