package com.yuhuankj.tmxq.ui.liveroom.imroom.publicscreen;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;
import com.tongdaxing.xchat_core.gift.GiftType;
import com.tongdaxing.xchat_core.gift.MultiGiftReceiveInfo;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.GiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.MultiGiftAttachment;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMessage;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.SmoothScrollLayoutManager;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.callback.SoftKeyBoardListener;
import com.yuhuankj.tmxq.ui.widget.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

/**
 * 直播间消息界面
 *
 * @author chenran
 * @date 2017/7/26
 */
public class MessageView extends FrameLayout implements SoftKeyBoardListener.OnSoftKeyBoardChangeListener {
    private final String TAG = MessageView.class.getSimpleName();

    private RecyclerView messageListView;
    private TextView tvBottomTip;
    private RoomMessageAdapter mMessageAdapter;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();


    public MessageView(Context context) {
        this(context, null);
    }

    public MessageView(Context context, AttributeSet attr) {
        this(context, attr, 0);
    }

    public MessageView(Context context, AttributeSet attr, int i) {
        super(context, attr, i);
        init(context);
    }

    boolean isNotScrollingAndBottom = true;
    boolean isFirstLoad = true;
    private boolean isAutoUpWhenSoftKeyShow = false;
    private SoftKeyBoardListener.OnSoftKeyBoardChangeListener mOnSoftKeyBoardChangeListener;
    private boolean hasSoftKeyBoardShow = false;

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (compositeDisposable == null) {
            return;
        }
        compositeDisposable.add(IMRoomMessageManager.get().getIMRoomMessageFlowable()
                .subscribe(messages -> {
                    if (ListUtils.isListEmpty(messages)) {
                        return;
                    }
                    onCurrentRoomReceiveNewMsg(messages);
                }));
        compositeDisposable.add(IMRoomMessageManager.get().getIMRoomEventObservable()
                .subscribe(roomEvent -> {
                    if (roomEvent == null || roomEvent.getEvent() != RoomEvent.RECEIVE_MSG) {
                        return;
                    }
                    IMRoomMessage chatRoomMessage = roomEvent.getIMRoomMessage();
                    if (chatRoomMessage == null) {
                        return;
                    }
                    List<IMRoomMessage> messages = new ArrayList<>();
                    messages.add(chatRoomMessage);
                    onCurrentRoomReceiveNewMsg(messages);
                }));
    }

    public void setAutoUpWhenSoftKeyShow(boolean autoUpWhenSoftKeyShow) {
        isAutoUpWhenSoftKeyShow = autoUpWhenSoftKeyShow;
    }

    public void clear() {
        if (mMessageAdapter != null) {
            mMessageAdapter.getData().clear();
            mMessageAdapter.notifyDataSetChanged();
        }
    }

    public SoftKeyBoardListener.OnSoftKeyBoardChangeListener getOnSoftKeyBoardChangeListener() {
        return mOnSoftKeyBoardChangeListener;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
            compositeDisposable = null;
        }
        if (messageListView != null) {
            messageListView.clearOnScrollListeners();
        }
    }

    private void init(Context context) {
        inflate(context, R.layout.view_room_message_view, this);
        messageListView = findViewById(R.id.rv_room_msg_list);
        tvBottomTip = findViewById(R.id.tv_room_more_msg_tip);
        messageListView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL,
                DisplayUtility.dp2px(context, 16), R.color.transparent));
        mMessageAdapter = new RoomMessageAdapter(context);
        //    private List<IMRoomMessage> chatRoomMessages;
        SmoothScrollLayoutManager layoutManger = new SmoothScrollLayoutManager(context);
        messageListView.setLayoutManager(layoutManger);
        //去除动画item刷新效果
        messageListView.setItemAnimator(null);
//        if (messageListView.getItemAnimator() instanceof SimpleItemAnimator) {
//            ((SimpleItemAnimator)messageListView.getItemAnimator()).setSupportsChangeAnimations(false);
//        }
        messageListView.setAdapter(mMessageAdapter);
        tvBottomTip.setOnClickListener(v -> {
            isNotScrollingAndBottom = true;
            tvBottomTip.setVisibility(GONE);
            messageListView.scrollToPosition(mMessageAdapter.getItemCount() - 1);
        });
//        chatRoomMessages = new ArrayList<>();
        messageListView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {//RecyclerView现在不是滚动状态。
                    if (isSlideToBottom()) {
                        isNotScrollingAndBottom = true;
                        tvBottomTip.setVisibility(GONE);
                    }
                } else if (newState == RecyclerView.SCROLL_STATE_SETTLING) {//自动滚动的状态，此时手指已经离开屏幕，RecyclerView的滚动是自身的惯性在维持。
                    isNotScrollingAndBottom = false;
                } else if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {//RecyclerView处于被外力引导的滚动状态，比如手指正在拖着进行滚动。
                    isNotScrollingAndBottom = false;
                }
            }
        });
        mOnSoftKeyBoardChangeListener = this;
    }

    public void refreshLikeMsgStatus(boolean isRefresh) {
        LogUtils.d(TAG, "refreshRoomAttentionMsgStatus-isRefresh:" + isRefresh);
        if (null != mMessageAdapter) {
            if (isRefresh) {
                mMessageAdapter.notifyDataSetChanged();
            }
        }
    }

    public void initMsgData() {
        List<IMRoomMessage> messages = IMRoomMessageManager.get().messages;
        if (!ListUtils.isListEmpty(messages) && mMessageAdapter != null) {
            messages = msgFilter(messages);
            mMessageAdapter.addMessages(messages);
//            messageListView.smoothScrollToPosition(mMessageAdapter.getItemCount() - 1);
            messageListView.scrollToPosition(mMessageAdapter.getItemCount() - 1);
        }
    }

    public RoomMessageAdapter getAdapter() {
        return mMessageAdapter;
    }

    /**
     * 刷新公屏，关注房间提示消息
     *
     * @param hasAttention
     */
    public void refreshRoomAttentionMsgStatus(boolean hasAttention) {
        LogUtils.d(TAG, "refreshRoomAttentionMsgStatus-hasAttention:" + hasAttention);
        if (null != mMessageAdapter) {
            mMessageAdapter.notifyDataSetChanged();
        }
    }

    /**
     * 接收到新聊天室消息
     *
     * @param messages
     */
    public void onCurrentRoomReceiveNewMsg(List<IMRoomMessage> messages) {
        if (ListUtils.isListEmpty(messages)) {
            return;
        }
//        if (chatRoomMessages.size() == 0) {
//            mMessageAdapter.setNewData(chatRoomMessages);
//        }
        showTipsOrScrollToBottom(messages);
    }

    /**
     * 过滤特定类型的消息
     *
     * @param chatRoomMessages
     * @return
     */
    private List<IMRoomMessage> msgFilter(List<IMRoomMessage> chatRoomMessages) {
        List<IMRoomMessage> messages = new ArrayList<>();
        for (IMRoomMessage message : chatRoomMessages) {
            if (message.getAttachment() != null) {
                //公聊的红包消息--过滤掉
                if (message.getAttachment().getFirst() == CustomAttachment.CUSTOM_MSG_FIRST_PUBLIC_CHAT_ROOM) {
                    continue;
                }
                //pk投票消息--过滤掉
                if (message.getAttachment().getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_VOTE_NEW) {
                    continue;
                }
            }

            if (CoreManager.getCore(VersionsCore.class).isGiftComboSwitchOpen()) {
                if (null != message.getAttachment() && message.getAttachment().getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT) {
                    //单人送礼消息，需要依据comBoId、giftType来判断是会否需要公屏展示
                    if (message.getAttachment().getSecond() == CustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_GIFT) {
                        GiftAttachment giftAttachment = (GiftAttachment) message.getAttachment();
                        if (null != giftAttachment && giftAttachment.getGiftRecieveInfo() != null) {
                            GiftReceiveInfo giftReceiveInfo = giftAttachment.getGiftRecieveInfo();
                            long comboId = giftReceiveInfo.getComboId();
                            int giftType = giftReceiveInfo.getGiftType();
                            //新版本comboId肯定不为空，旧版本为空则依旧add，新版本仅宝箱公屏及时展示
                            if (comboId != 0 && giftType != GiftType.Box.ordinal()) {
                                //非礼盒暴击，不需要公屏展示,非暴击或者礼盒暴击，公屏照常展示消息
                                continue;
                            }
                        }
                    }
                    if (message.getAttachment().getSecond() == CustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_GIFT_COMBO_SCREEN) {
                        GiftAttachment giftAttachment = (GiftAttachment) message.getAttachment();
                        if (null != giftAttachment && giftAttachment.getGiftRecieveInfo() != null) {
                            GiftReceiveInfo giftReceiveInfo = giftAttachment.getGiftRecieveInfo();
                            //新版本新消息，comboId肯定不为空，仅非礼盒消息的公屏add展示
                            if (giftReceiveInfo.getComboId() == 0 || giftReceiveInfo.getGiftType() == GiftType.Box.ordinal()) {
                                //仅针对礼盒暴击公屏展示消息
                                continue;
                            }
                        }
                    }

                }

                if (null != message.getAttachment() && message.getAttachment().getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT) {
                    //全麦送礼消息，需要依据comBoId、giftType来判断是会否需要公屏展示
                    if (message.getAttachment().getSecond() == CustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_MULTI_GIFT) {
                        MultiGiftAttachment giftAttachment = (MultiGiftAttachment) message.getAttachment();
                        if (giftAttachment.getMultiGiftRecieveInfo() != null) {
                            MultiGiftReceiveInfo giftReceiveInfo = giftAttachment.getMultiGiftRecieveInfo();
                            //新版本，comboId必定不为空，旧版本comboId必定为空，旧版本直接add，新版本仅add礼盒消息，公屏做展示处理
                            if (giftReceiveInfo.getComboId() != 0 && giftReceiveInfo.getGiftType() != GiftType.Box.ordinal()) {
                                //非礼盒暴击，不需要公屏展示,非暴击或者礼盒暴击，公屏照常展示消息
                                continue;
                            }
                        }
                    }
                    if (message.getAttachment().getSecond() == CustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_MULTI_GIFT_COMBO_SCREEN) {
                        MultiGiftAttachment giftAttachment = (MultiGiftAttachment) message.getAttachment();
                        if (giftAttachment.getMultiGiftRecieveInfo() != null) {
                            MultiGiftReceiveInfo giftReceiveInfo = giftAttachment.getMultiGiftRecieveInfo();
                            //新版本新消息类型，comboId必定不为空，仅针对非礼盒消息做公屏add展示处理
                            if (giftReceiveInfo.getComboId() == 0 || giftReceiveInfo.getGiftType() == GiftType.Box.ordinal()) {
                                //仅针对非礼盒暴击公屏展示消息
                                continue;
                            }
                        }
                    }
                }
            }

            messages.add(message);
        }
        return messages;
    }

    /**
     * 显示新消息提示并自动滚动到最底部
     *
     * @param messages
     */
    private void showTipsOrScrollToBottom(List<IMRoomMessage> messages) {
        // 最后一个item是否显示出来
        messages = msgFilter(messages);
        if (isNotScrollingAndBottom) {
            if (tvBottomTip.getVisibility() == View.VISIBLE) {
                tvBottomTip.setVisibility(GONE);
            }
            mMessageAdapter.addMessages(messages);
            if (isFirstLoad) {//添加第一次显示的滚动效果
                isFirstLoad = false;
                messageListView.smoothScrollToPosition(mMessageAdapter.getItemCount() - 1);
            } else {//正常情况通过item动画显示效果
                messageListView.scrollToPosition(mMessageAdapter.getItemCount() - 1);
            }
        } else {
            if (tvBottomTip.getVisibility() == View.GONE) {
                tvBottomTip.setVisibility(VISIBLE);
            }
            mMessageAdapter.addMessages(messages);
        }
    }

    public void release() {
        if (messageListView != null) {
            messageListView.removeAllViews();
            messageListView = null;
        }
        if (mMessageAdapter != null && !ListUtils.isListEmpty(mMessageAdapter.getData())) {
            mMessageAdapter.getData().clear();
            mMessageAdapter = null;
        }
        removeAllViews();
    }

    public boolean isSlideToBottom() {
        if (messageListView != null) {
            return messageListView.computeVerticalScrollExtent() + messageListView.computeVerticalScrollOffset()
                    >= messageListView.computeVerticalScrollRange();
        } else {
            return false;
        }
    }

    @Override
    public void keyBoardShow(int height) {
        hasSoftKeyBoardShow = true;
        LogUtils.d(TAG, "keyBoardShow-height:" + height + " hasSoftKeyBoardShow:" + hasSoftKeyBoardShow
                + " isAutoUpWhenSoftKeyShow:" + isAutoUpWhenSoftKeyShow);
        if (isAutoUpWhenSoftKeyShow) {
            messageListView.post(new Runnable() {
                @Override
                public void run() {
                    messageListView.setPadding(0, 0, 0, height);
                    messageListView.requestLayout();
                    messageListView.scrollToPosition(mMessageAdapter.getItemCount() - 1);
                }
            });
        }
    }

    @Override
    public void keyBoardHide(int height) {
        hasSoftKeyBoardShow = false;
        LogUtils.d(TAG, "keyBoardHide-height:" + height + " hasSoftKeyBoardShow:" + hasSoftKeyBoardShow
                + " isAutoUpWhenSoftKeyShow:" + isAutoUpWhenSoftKeyShow);
        if (isAutoUpWhenSoftKeyShow) {
            messageListView.post(new Runnable() {
                @Override
                public void run() {
                    messageListView.setPadding(0, 0, 0, 0);
                    messageListView.requestLayout();
                    messageListView.scrollToPosition(mMessageAdapter.getItemCount() - 1);
                }
            });
        }
    }


    public interface OnMsgContentClickListener {
        /**
         * @param uid 被点击内容的发送者uid
         */
        void clickToOpenUserInfoDialog(long uid);

        default void onUserOperate(long uid, String nick) {
        }

        void clickToAttentionRoomOwner(boolean attentionOrCancel);

        void clickToAttentionRoom(boolean attentionOrCancel);
    }
}
