package com.yuhuankj.tmxq.ui.audio.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.dinuscxj.progressbar.CircleProgressBar;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.file.BasicFileUtils;
import com.tongdaxing.xchat_core.music.IMusicDownloaderCore;
import com.tongdaxing.xchat_core.music.bean.HotMusicInfo;
import com.tongdaxing.xchat_core.player.IPlayerCore;
import com.tongdaxing.xchat_core.player.IPlayerDbCore;
import com.tongdaxing.xchat_core.player.bean.LocalMusicInfo;
import com.tongdaxing.xchat_core.utils.MusicFileUtil;
import com.yuhuankj.tmxq.R;

import java.util.List;

import static com.tongdaxing.xchat_core.player.IPlayerCore.PLAY_STATUS_MUSIC_LIST_EMPTY;

/**
 * Created by chenran on 2017/11/1.
 */

public class MusicServerListAdapter extends RecyclerView.Adapter<MusicServerListAdapter.ViewHolder>{
    private Context context;
    private List<HotMusicInfo> hotMusicInfos;

    public MusicServerListAdapter(Context context) {
        this.context = context;
    }

    public void setHotMusicInfos(List<HotMusicInfo> hotMusicInfos) {
        this.hotMusicInfos = hotMusicInfos;
    }

    public List<HotMusicInfo> getHotMusicInfos(){
        return hotMusicInfos;
    }

    @Override
    public MusicServerListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(context).inflate(R.layout.list_item_server_music,parent,false);
        return new MusicServerListAdapter.ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(MusicServerListAdapter.ViewHolder holder, int position, List<Object> payloads) {
        if(null != payloads && payloads.size() > 0 && payloads.get(0) instanceof Integer){
            int progress = (int)payloads.get(0);
            //-1说明下载过程中出错，恢复为未下载状态
            holder.iv_downStatus.setVisibility((-1 == progress || 101 == progress)  ? View.VISIBLE : View.GONE);
            holder.fl_downProgress.setVisibility((-1 == progress || 101 == progress) ? View.GONE : View.VISIBLE);
            holder.cpb_download.setProgress((-1 == progress || 101 == progress) ? 0 : progress);
            if(101 == progress){
                holder.iv_downStatus.setImageDrawable(context.getResources().getDrawable(R.mipmap.icon_music_item_to_play));
            }
        }else{
            onBindViewHolder(holder, position);
        }
    }

    @Override
    public void onBindViewHolder(MusicServerListAdapter.ViewHolder holder, int position) {
        HotMusicInfo hotMusicInfo = hotMusicInfos.get(position);

        holder.tv_musicName.setText(hotMusicInfo.getSingName());
        String artistName = hotMusicInfo.getSingerName();
        if ("<unknow>".equals(artistName)) {
            artistName=context.getResources().getString(R.string.music_library_item_singer_unknow);
        }
        holder.tv_singerName.setText(artistName);
        if(!TextUtils.isEmpty(hotMusicInfo.getUserNick())){
            holder.tv_uploaderName.setVisibility(View.VISIBLE);
            holder.tv_uploaderName.setText(context.getResources().getString(R.string.music_library_item_upload,hotMusicInfo.getUserNick()));
        }else{
            holder.tv_uploaderName.setVisibility(View.GONE);
        }
        String musicDownloadFilePath = MusicFileUtil.getMusicDownloadFilePath(hotMusicInfo.getSingName(),hotMusicInfo.getId());
        boolean hasMusicFileDowned = BasicFileUtils.isFileExisted(musicDownloadFilePath);
        LocalMusicInfo currMusicInfo = CoreManager.getCore(IPlayerCore.class).getCurrLocalMusicInfo();
        boolean isCurrPlayMusic = null != currMusicInfo && currMusicInfo.getSongId().equals(hotMusicInfo.getId()+"")
                && CoreManager.getCore(IPlayerCore.class).getState() != IPlayerCore.STATE_STOP;
        boolean isDownloading = CoreManager.getCore(IMusicDownloaderCore.class).checkHotMusicIsDownloading(hotMusicInfo.getSingUrl());
        if(hasMusicFileDowned){
            holder.iv_downStatus.setVisibility(View.VISIBLE);
            holder.fl_downProgress.setVisibility(View.GONE);
            if(isCurrPlayMusic){
                int state = CoreManager.getCore(IPlayerCore.class).getState();
                holder.iv_downStatus.setImageDrawable(context.getResources().getDrawable(state == IPlayerCore.STATE_PLAY ? R.mipmap.icon_music_item_play_to_pause : R.mipmap.icon_music_item_pause_to_play));
            }else{
                holder.iv_downStatus.setImageDrawable(context.getResources().getDrawable(R.mipmap.icon_music_item_to_play));
            }
        }else{
            holder.iv_downStatus.setVisibility(isDownloading ? View.GONE : View.VISIBLE);
            holder.fl_downProgress.setVisibility(isDownloading ? View.VISIBLE : View.GONE);
            if(!isDownloading){
                holder.iv_downStatus.setImageDrawable(context.getResources().getDrawable(R.mipmap.icon_music_server_down));
            }
        }
        holder.iv_downStatus.setTag(hotMusicInfo);
        holder.iv_downStatus.setOnClickListener(onClickListener);
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            HotMusicInfo info = (HotMusicInfo) v.getTag();
            if(null == info){
                return;
            }
            LocalMusicInfo currInfo = CoreManager.getCore(IPlayerCore.class).getCurrLocalMusicInfo();
            String path = MusicFileUtil.getMusicDownloadFilePath(info.getSingName(),info.getId());
            boolean isDowned = BasicFileUtils.isFileExisted(path);
            boolean isCurrMusic = null != currInfo && currInfo.getSongId().equals(info.getId()+"")
                    && CoreManager.getCore(IPlayerCore.class).getState() != IPlayerCore.STATE_STOP;
            boolean isDownloadingNow = CoreManager.getCore(IMusicDownloaderCore.class).checkHotMusicIsDownloading(info.getSingUrl());
            if(isDowned){
                if(isCurrMusic) {
                    int state = CoreManager.getCore(IPlayerCore.class).getState();
                    if (state == IPlayerCore.STATE_PLAY) {
                        CoreManager.getCore(IPlayerCore.class).pause();
                    } else if (state == IPlayerCore.STATE_PAUSE) {
                        CoreManager.getCore(IPlayerCore.class).play(currInfo);
                    } else {
                        int result = CoreManager.getCore(IPlayerCore.class).playNext();
                        if (result < 0) {
                            if(null != onPlayErrorListener){
                                onPlayErrorListener.onPlayError(
                                        context.getResources().getString(result == PLAY_STATUS_MUSIC_LIST_EMPTY ?
                                                R.string.music_play_failed_list_empty :
                                                R.string.music_play_failed_file_format_error));
                            }
                        }
                    }
                }else{
                    LocalMusicInfo musicInfo = CoreManager.getCore(IPlayerDbCore.class).gueryLocalMusicInfoByLocalUrl(path);
                    int result = CoreManager.getCore(IPlayerCore.class).play(musicInfo);
                    if (result < 0) {
                        if(null != onPlayErrorListener){
                            onPlayErrorListener.onPlayError(
                                    context.getResources().getString(result == PLAY_STATUS_MUSIC_LIST_EMPTY ?
                                            R.string.music_play_failed_list_empty :
                                            R.string.music_play_failed_file_format_error));
                        }
                    }
                }
            }else if(!isDownloadingNow){
                if(null != onHotMusicItemClickListener){
                    onHotMusicItemClickListener.onHotMusicClickToDown(info);
                }
            }
        }
    };

    @Override
    public int getItemCount() {
        if (hotMusicInfos == null) {
            return 0;
        } else {
            return hotMusicInfos.size();
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        ImageView iv_downStatus;
        TextView tv_musicName;
        TextView tv_singerName;
        TextView tv_uploaderName;
        FrameLayout container;
        FrameLayout fl_downProgress;
        CircleProgressBar cpb_download;

        public ViewHolder(View itemView) {
            super(itemView);
            container = (FrameLayout) itemView.findViewById(R.id.container);
            fl_downProgress = (FrameLayout) itemView.findViewById(R.id.fl_downProgress);
            iv_downStatus = (ImageView) itemView.findViewById(R.id.iv_downStatus);
            tv_musicName = (TextView) itemView.findViewById(R.id.tv_musicName);
            tv_uploaderName = (TextView) itemView.findViewById(R.id.tv_uploaderName);
            tv_singerName = (TextView) itemView.findViewById(R.id.tv_singerName);
            cpb_download = (CircleProgressBar) itemView.findViewById(R.id.cpb_download);
        }
    }

    public void setOnHotMusicItemClickListener(OnHotMusicItemClickListener onHotMusicItemClickListener) {
        this.onHotMusicItemClickListener = onHotMusicItemClickListener;
    }

    private OnHotMusicItemClickListener onHotMusicItemClickListener;

    public interface OnHotMusicItemClickListener{
        void onHotMusicClickToDown(HotMusicInfo hotMusicInfo);
    }

    public void setOnPlayErrorListener(MusicPlayListAdapter.OnPlayErrorListener onPlayErrorListener) {
        this.onPlayErrorListener = onPlayErrorListener;
    }

    private MusicPlayListAdapter.OnPlayErrorListener onPlayErrorListener;

    public interface OnPlayErrorListener{
        void onPlayError(String msg);
    }
}
