package com.yuhuankj.tmxq.ui.user.other;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_core.user.bean.UserPhoto;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;

import java.util.ArrayList;

public class ShowPhotoActivity extends BaseActivity {

    private final String TAG = ShowPhotoActivity.class.getSimpleName();

    private ImageView mImageView;
    private TextView imgCount;
    private ViewPager viewPager;
    private UserPhoto userPhoto;
    private PhotoAdapter photoAdapter;
    private ShowPhotoActivity mActivity;
    private int position;
    private long userId;
    private UserInfo userInfo;
    private ArrayList<UserPhoto> photoUrls = new ArrayList<>();
    //true-表示查看个人相册或者头像，false表示
    private boolean showUserPhotoOrVoiceBg = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_photo);
        mActivity = this;
        initView();
        initData();
        setListener();
    }

    private void setListener() {
//        mImageView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                return false;
//            }
//        });
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                viewPager.setCurrentItem(position);
//                imgCount.setText((position + 1) + "/" + photoAdapter.getCount());
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    imgCount.setText((position + 1) + "/" + photoAdapter.getCount());
                }
                imgCount.setText((position + 1) + "/" + photoAdapter.getCount());

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        if (photoAdapter != null)
            photoAdapter.setmImageOnclickListener(new PhotoAdapter.imageOnclickListener() {
                @Override
                public void onClick() {
                    finish();
                }
            });
    }

    private void initData() {
        userId = getIntent().getLongExtra("userId", 0);
        position = getIntent().getIntExtra("position", 1);
        showUserPhotoOrVoiceBg = getIntent().getBooleanExtra("showUserPhotoOrVoiceBg", true);
        LogUtils.d(TAG, "initData-userId:" + userId + " position:" + position);
        userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(userId);
        if (showUserPhotoOrVoiceBg) {
            if (null != userInfo && null != userInfo.getPrivatePhoto()) {
                photoUrls.clear();
                photoUrls.addAll(userInfo.getPrivatePhoto());
            }
        } else {
            if (!TextUtils.isEmpty(userInfo.getBackground())) {
                photoUrls.clear();
                photoUrls.add(new UserPhoto(userId, userInfo.getBackground()));
            }
        }

        if (photoUrls != null && photoUrls.size() > 0) {
            photoAdapter = new PhotoAdapter(mActivity, photoUrls);
            viewPager.setAdapter(photoAdapter);
            viewPager.setCurrentItem(position);
            imgCount.setText((position + 1) + "/" + photoAdapter.getCount());
        } else {
            finish();
        }
    }

    private ArrayList<UserPhoto> json2PhotoList(Json json) {
        if (json == null || json.key_names().length == 0) {
            finish();
            return null;
        }
        ArrayList<UserPhoto> userPhotos = new ArrayList<>();
        String[] keys = json.key_names();

        for (int i = 0; i < keys.length; i++) {
            Json j = json.json_ok(keys[i]);
            UserPhoto userPhoto = new UserPhoto(j.num_l("pid"), j.str("photoUrl"));
            userPhotos.add(userPhoto);

        }

        return userPhotos;
    }

    private void initView() {
//        mImageView = (ImageView) findViewById(R.id.photoview);
        imgCount = (TextView) findViewById(R.id.tv_count);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
    }
}
