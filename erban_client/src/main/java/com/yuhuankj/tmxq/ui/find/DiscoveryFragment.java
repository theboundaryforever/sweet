package com.yuhuankj.tmxq.ui.find;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.netease.nim.uikit.common.util.sys.NetworkUtil;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.ButtonUtils;
import com.tongdaxing.erban.libcommon.utils.ChannelUtil;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.xchat_core.auth.AccountInfo;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.WebViewStyle;
import com.tongdaxing.xchat_core.minigame.MiniGameModel;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.base.fragment.BaseFragment;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.chancemeeting.ChanceMeetingActivity;
import com.yuhuankj.tmxq.ui.find.bean.FindEnitity;
import com.yuhuankj.tmxq.ui.find.bean.FindHotRoomEnitity;
import com.yuhuankj.tmxq.ui.home.game.PlayTogetherAdapter;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.nim.game.GameEnitity;
import com.yuhuankj.tmxq.ui.nim.game.InvitationActivity;
import com.yuhuankj.tmxq.ui.quickmating.QuickMatingHomeActivity;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;
import com.yuhuankj.tmxq.ui.webview.VoiceAuthCardWebViewActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author liaoxy
 * @Description:发现页面
 * @date 2019/5/24 11:15
 */
public class DiscoveryFragment extends BaseFragment {

    private final String TAG = DiscoveryFragment.class.getSimpleName();

    private RelativeLayout rlQuickMating;
    private SVGAImageView svPhone;
    private ImageView imvChangeMeeting, imvSoundVerify;
    private TextView tvAllGame;
    private TextView tvQMOpenStatus;
    private RecyclerView rcvPlayTogether;
    private RecyclerView rcvHotLiveRoom;
    private PlayTogetherAdapter playTogetherAdapter;
    private HotLiveAdapter hotLiveAdapter;
    private SwipeRefreshLayout srfContent;
    private FindEnitity findEnitity;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_discovery;
    }

    @Override
    public void onFindViews() {
        srfContent = mView.findViewById(R.id.srfContent);
        rlQuickMating = mView.findViewById(R.id.rlQuickMating);
        svPhone = mView.findViewById(R.id.svPhone);
        tvQMOpenStatus = mView.findViewById(R.id.tvQMOpenStatus);
        imvChangeMeeting = mView.findViewById(R.id.imvChangeMeeting);
        imvSoundVerify = mView.findViewById(R.id.imvSoundVerify);
        tvAllGame = mView.findViewById(R.id.tvAllGame);
        rcvPlayTogether = mView.findViewById(R.id.rcvPlayTogether);
        rcvHotLiveRoom = mView.findViewById(R.id.rcvHotLiveRoom);
    }

    @Override
    public void onSetListener() {
        rlQuickMating.setOnClickListener(this);
        imvChangeMeeting.setOnClickListener(this);
        imvSoundVerify.setOnClickListener(this);
        tvAllGame.setOnClickListener(this);
    }

    @Override
    public void initiate() {
        GridLayoutManager manager = new GridLayoutManager(getContext(), 3);
        rcvPlayTogether.setLayoutManager(manager);

        rcvHotLiveRoom.setHasFixedSize(true);
        GridLayoutManager managerHotLiveRoom = new GridLayoutManager(getContext(), 3);
        rcvHotLiveRoom.setLayoutManager(managerHotLiveRoom);
        rcvHotLiveRoom.setNestedScrollingEnabled(false);

        playTogetherAdapter = new PlayTogetherAdapter(null);
        playTogetherAdapter.setOnItemChildClickListener(onItemChildClickListener);
        rcvPlayTogether.setAdapter(playTogetherAdapter);

        hotLiveAdapter = new HotLiveAdapter(new ArrayList<>());
        hotLiveAdapter.setOnItemClickListener(onItemClickListener);
        rcvHotLiveRoom.setAdapter(hotLiveAdapter);

        srfContent.setOnRefreshListener(onRefreshListener);

        startPhoneAnimator();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (!CoreManager.getCore(IAuthCore.class).isLogin()) {
                return;
            }
            if (findEnitity == null) {
                getGames();
                discover();
            }
        }
    }

    private SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            if (NetworkUtil.isNetAvailable(mContext)) {
                getGames();
                discover();
            } else {
                toast("网络错误");
            }
        }
    };

    private BaseQuickAdapter.OnItemClickListener onItemClickListener = new BaseQuickAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
            try {
                if (hotLiveAdapter.getData().size() > position) {
                    FindHotRoomEnitity roomEnitity = hotLiveAdapter.getData().get(position);
                    RoomServiceScheduler.getInstance().enterRoom(getActivity(),
                            roomEnitity.getUid(), roomEnitity.getType());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private BaseQuickAdapter.OnItemChildClickListener onItemChildClickListener = new BaseQuickAdapter.OnItemChildClickListener() {
        @Override
        public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
            if (ButtonUtils.isFastDoubleClick(view.getId())) {
                return;
            }
            if (playTogetherAdapter.getData().size() > position) {
                GameEnitity gameEnitity = playTogetherAdapter.getData().get(position);
                if (CommonWebViewActivity.isGameRunning) {
                    LogUtils.e("", "game is running");
                    return;
                }
                String url = gameEnitity.getUrl();
                if (TextUtils.isEmpty(url)) {
                    SingleToastUtil.showToast("游戏链接无效");
                    return;
                }
                CommonWebViewActivity.isGameRunning = true;
                CommonWebViewActivity.start(getActivity(), url + "#/loading", WebViewStyle.NO_TITLE);
            }
        }
    };

    protected void startPhoneAnimator() {
        if (getActivity() == null) {
            return;
        }
        new SVGAParser(getActivity()).parse("quickmating.svga", new SVGAParser.ParseCompletion() {
            @Override
            public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {
                if (null != svPhone) {
                    svPhone.setVisibility(View.VISIBLE);
                    SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);
                    svPhone.setImageDrawable(drawable);
                    svPhone.startAnimation();
                }
            }

            @Override
            public void onError() {

            }
        });
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view == rlQuickMating) {
            //统计-交友速配-入口
            StatisticManager.get().onEvent(getContext(),
                    StatisticModel.EVENT_ID_CLICK_QM_ENTRANCE,
                    StatisticModel.getInstance().getUMAnalyCommonMap(getContext()));
            if (findEnitity != null && findEnitity.getUserMatchState() == 0) {
                DialogManager dialogManager = new DialogManager(getContext());
                dialogManager.setTitle("暂未开放");
                dialogManager.setHideCancel(true);
                String message = "开放时间：" + findEnitity.getOpenTime();
                dialogManager.showOkCancelDialog(message, false, null);
                return;
            }
            Intent intent = new Intent(getContext(), QuickMatingHomeActivity.class);
            startActivity(intent);
        } else if (view == imvChangeMeeting) {
            startActivity(new Intent(getContext(), ChanceMeetingActivity.class));
        } else if (view == imvSoundVerify) {
            //发现-banner-声鉴卡
            Map<String, String> maps = StatisticModel.getInstance().getUMAnalyCommonMap(getActivity());
            StatisticManager.get().onEvent(getActivity(), StatisticModel.EVENT_ID_FIND_BANNER_VOICEAUTHCARD, maps);
            VoiceAuthCardWebViewActivity.start(getActivity(), false);
        } else if (view == tvAllGame) {
            startActivity(new Intent(getContext(), InvitationActivity.class));
        }
    }

    private void getGames() {
        new MiniGameModel().getGames(new OkHttpManager.MyCallBack<ServiceResult<List<GameEnitity>>>() {
            @Override
            public void onError(Exception e) {
                SingleToastUtil.showToast(e.getMessage());
                if (null != srfContent) {
                    srfContent.setRefreshing(false);
                }
            }

            @Override
            public void onResponse(ServiceResult<List<GameEnitity>> response) {
                if (null == srfContent || null == playTogetherAdapter) {
                    return;
                }
                srfContent.setRefreshing(false);
                if (response.isSuccess()) {
                    if (response.getData() != null && response.getData().size() > 0) {
                        List<GameEnitity> games = response.getData();
                        if (games != null && games.size() > 3) {
                            games = games.subList(0, 3);
                        }
                        playTogetherAdapter.setNewData(games);
                    }
                } else {
                    SingleToastUtil.showToast(response.getMessage());
                }
            }
        });
    }

    //获取发现页数据
    private void discover() {
        new FindModel().discover(new HttpRequestCallBack<FindEnitity>() {
            @Override
            public void onSuccess(String message, FindEnitity response) {
                if (null == srfContent || null == tvQMOpenStatus || null == hotLiveAdapter) {
                    return;
                }
                srfContent.setRefreshing(false);
                if (response == null) {
                    toast("获取发现页数据为空");
                    return;
                }
                findEnitity = response;
                int openStatus = response.getUserMatchState();
                if (openStatus == 0) {
                    String openTime = findEnitity.getOpenTime();
                    if (!TextUtils.isEmpty(openTime)) {
                        tvQMOpenStatus.setText("(" + openTime + "开放)");
                    } else {
                        tvQMOpenStatus.setText("(暂未开放");
                    }
                } else {

                    tvQMOpenStatus.setText("(开放中)");
                }
                if (response.getRadioRoomList() != null && response.getRadioRoomList().size() > 0) {
                    List<FindHotRoomEnitity> rooms = response.getRadioRoomList();
                    if (rooms != null && rooms.size() > 6) {
                        rooms = rooms.subList(0, 6);
                    }
                    hotLiveAdapter.setNewData(rooms);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (null != srfContent) {
                    srfContent.setRefreshing(false);
                }
                toast(msg);
            }
        });
    }

    private void syncLoadData() {
        LogUtils.d(TAG, "syncLoadData");
        if (null != getActivity()) {
            String channelId = ChannelUtil.getChannel(getActivity());
            if (!TextUtils.isEmpty(channelId) && channelId.endsWith("_first")) {
                getGames();
                discover();
            }
        }
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogin(AccountInfo accountInfo) {
        LogUtils.d(TAG, "onLogin");
        if (null != accountInfo && accountInfo.getUid() > 0L) {
            syncLoadData();
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoComplete(UserInfo userInfo) {
        LogUtils.d(TAG, "onCurrentUserInfoComplete");
        syncLoadData();
    }
}
