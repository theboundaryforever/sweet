package com.yuhuankj.tmxq.ui.me.taskcenter.view;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.noober.background.view.BLTextView;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.erban.libcommon.utils.TimeUtils;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.thirdsdk.nim.MsgCenterRedPointStatusManager;
import com.yuhuankj.tmxq.ui.MainActivity;
import com.yuhuankj.tmxq.ui.chancemeeting.MyVoiceActivity;
import com.yuhuankj.tmxq.ui.me.charge.ChargeActivity;
import com.yuhuankj.tmxq.ui.me.setting.SettingActivity;
import com.yuhuankj.tmxq.ui.me.taskcenter.model.TaskCenterEnitity;
import com.yuhuankj.tmxq.ui.me.taskcenter.presenter.TakeCenterPresenter;
import com.yuhuankj.tmxq.ui.me.taskcenter.view.adapter.SignInAwardAdapter;
import com.yuhuankj.tmxq.ui.nim.game.InvitationActivity;
import com.yuhuankj.tmxq.ui.signAward.model.SignAwardResult;
import com.yuhuankj.tmxq.ui.signAward.model.SignInAwardInfo;
import com.yuhuankj.tmxq.ui.signAward.view.SignAwardResultDialog;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;
import com.yuhuankj.tmxq.ui.widget.DividerItemDecoration;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.utils.UIHelper;
import com.yuhuankj.tmxq.widget.TitleBar;

import java.util.List;
import java.util.Map;

import static com.yuhuankj.tmxq.ui.signAward.view.SignAwardDaysDialog.FLAG_SIGN_DAY_AWARD_DIALOG_SHOWED;

/**
 * 任务中心
 *
 * @author liaoxy
 * @date 2019/4/11 15:15
 */
@CreatePresenter(TakeCenterPresenter.class)
public class TaskCenterActivity extends BaseMvpActivity<ITaskCenterView, TakeCenterPresenter>
        implements ITaskCenterView, View.OnClickListener {
    private TextView tvDouziMount, tvDouziRecord;
    private LinearLayout llBeginnerTask;
    private LinearLayout llEverydayTask;
    private TextView tvGoGetAward;
    private TextView tvSingDays;
    private LinearLayout llEveryday, llBeginner;
    //任务弹窗相关
    private RelativeLayout rlTaskCenterTip;
    private LinearLayout llGetBox, llDouziRuler;
    private View vOpenBox;
    private String signinGetDouziAmountTip = "";//此次签到获取的豆子数量
    private String boxGiftNameTip = null;//宝箱礼物名称

    private RecyclerView rlvSignAward;
    private SignInAwardAdapter adapter;
    private BLTextView bltvGet;
    private BLTextView bltvGot;
    private ImageView ivBoxRecv;
    private View vSignInRedPoint;
    private TaskCenterEnitity taskCenterEnitity = null;
    private SignInAwardInfo signInAwardInfo = null;
    private int signDays = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_center);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        vSignInRedPoint = findViewById(R.id.vSignInRedPoint);
        bltvGet = (BLTextView) findViewById(R.id.bltvGet);
        bltvGot = (BLTextView) findViewById(R.id.bltvGot);
        tvSingDays = (TextView) findViewById(R.id.tvSignDays);
        tvDouziMount = (TextView) findViewById(R.id.tvDouziMount);
        tvDouziRecord = (TextView) findViewById(R.id.tvDouziRecord);
        llBeginnerTask = (LinearLayout) findViewById(R.id.llBeginnerTask);
        llEverydayTask = (LinearLayout) findViewById(R.id.llEverydayTask);
        tvGoGetAward = (TextView) findViewById(R.id.tvGoGetAward);

        llEveryday = (LinearLayout) findViewById(R.id.llEveryday);
        llBeginner = (LinearLayout) findViewById(R.id.llBeginner);

        rlTaskCenterTip = (RelativeLayout) findViewById(R.id.rlTaskCenterTip);
        llGetBox = (LinearLayout) findViewById(R.id.llGetBox);
        llDouziRuler = (LinearLayout) findViewById(R.id.llDouziRuler);
        vOpenBox = findViewById(R.id.vOpenBox);
        ivBoxRecv = (ImageView) findViewById(R.id.ivBoxRecv);

        rlvSignAward = (RecyclerView) findViewById(R.id.rlvSignAward);
        LinearLayoutManager manager1 = new RecyclerViewNoBugLinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false) {
            @Override
            public boolean canScrollVertically() {
                //禁止垂直滑动
                return false;
            }

            @Override
            public boolean canScrollHorizontally() {
                return false;
            }
        };
        rlvSignAward.setLayoutManager(manager1);
        DividerItemDecoration dividerItemDecoration1 = new DividerItemDecoration(manager1.getOrientation(),
                Color.TRANSPARENT, this, DisplayUtility.dp2px(this, 5));
        rlvSignAward.addItemDecoration(dividerItemDecoration1);
    }

    private void initListener() {
        tvDouziRecord.setOnClickListener(this);
        tvGoGetAward.setOnClickListener(this);
        rlTaskCenterTip.setOnClickListener(this);
        vOpenBox.setOnClickListener(this);
        bltvGet.setOnClickListener(this);
    }

    private void initData() {
        initTitleBar("任务中心");
        mTitleBar.setActionTextColor(Color.WHITE);
        mTitleBar.addAction(new TitleBar.TextAction("规则") {
            @Override
            public void performAction(View view) {
                rlTaskCenterTip.setVisibility(View.VISIBLE);
                llGetBox.setVisibility(View.GONE);
                llDouziRuler.setVisibility(View.VISIBLE);
            }
        });
        mTitleBar.setTitleColor(Color.WHITE);
        mTitleBar.setLeftImageResource(R.drawable.icon_white_back);
        mTitleBar.setCommonBackgroundColor(Color.TRANSPARENT);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getDialogManager().showProgressDialog(this, getResources().getString(R.string.network_loading));
        getMvpPresenter().getTaskList();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.bltvGet) {
            showSignInResultDilaog();
        } else if (view == tvDouziRecord) {
            startActivity(new Intent(this, DouziHistoryActivity.class));
        } else if (view == tvGoGetAward) {
            //去抽奖
            CommonWebViewActivity.start(this, UriProvider.getDouziLuckyAward(), true);
            //任务统计-去抽奖
            Map<String, String> maps = StatisticModel.getInstance().getUMAnalyCommonMap(this);
            StatisticManager.get().onEvent(this, StatisticModel.EVENT_ID_TASK_CHOUJIANG_TASKCENTER, maps);
        } else if (view == rlTaskCenterTip) {
            rlTaskCenterTip.setVisibility(View.GONE);
        } else if (view == vOpenBox) {
            getDialogManager().showProgressDialog(this, getResources().getString(R.string.network_loading));
            getMvpPresenter().receiveSignInGift();

            //萌新大礼包期间，任务中心弹出的签到弹框，领取按钮打点事件
            Map<String, String> params = StatisticModel.getInstance().getUMAnalyCommonMap(this);
            params.put("signDays", String.valueOf(signDays));
            StatisticManager.get().onEvent(this,
                    StatisticModel.EVENT_ID_TASK_CENTER_SIGN_IN_TAKE, params);

            if (TextUtils.isEmpty(boxGiftNameTip)) {
                rlTaskCenterTip.setVisibility(View.GONE);
                return;
            }
            toast("恭喜您获得" + boxGiftNameTip);
            rlTaskCenterTip.setVisibility(View.GONE);
        } else if (view.getId() == R.id.tvAction) {
            try {
                int taskId = (int) view.getTag();
                toDoTask(taskId);
                //任务统计-去完成任务
                Map<String, String> maps = StatisticModel.getInstance().getUMAnalyCommonMap(this);
                maps.put("task_id", String.valueOf(taskId));
                StatisticManager.get().onEvent(this, StatisticModel.EVENT_ID_TASK_DO_TASK, maps);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //根据任务类形中转不同页面去完成任务
    private void toDoTask(int taskId) {
        if (taskId == 1) {
            //关注一个主播
            Intent intent = new Intent();
            intent.putExtra("curPage", 1);
            MainActivity.start(this, intent);
        } else if (taskId == 2) {
            //在大厅发言一次
            // startActivity(new Intent(this, PublicChatRoomActivity.class));
            LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(MainActivity.ACTION_OPEN_FIND));
        } else if (taskId == 3) {
            //上传相册图片
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            if (userInfo == null) {
                toast("用户信息为空");
                return;
            }
            UserInfoActivity.start(this, userInfo.getUid());
        } else if (taskId == 4) {
            //设置个人签名
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            if (userInfo == null) {
                toast("用户信息为空");
                return;
            }
            UIHelper.showUserInfoModifyAct(this, userInfo.getUid());
        } else if (taskId == 5) {
            //绑定手机
            startActivity(new Intent(this, SettingActivity.class));
        } else if (taskId == 6) {
            //上传声音卡片
            startActivity(new Intent(this, MyVoiceActivity.class));
        } else if (taskId == 7) {
            //实名认证
            CommonWebViewActivity.start(this, UriProvider.getRealNameAuthUrl());
        } else if (taskId == 8) {
            //分享房间
            Intent intent = new Intent();
            intent.putExtra("curPage", 1);
            MainActivity.start(this, intent);
        } else if (taskId == 9) {
            //充值一次
            startActivity(new Intent(this, ChargeActivity.class));
        } else if (taskId == 10) {
            //送普通礼物一次
            Intent intent = new Intent();
            intent.putExtra("curPage", 1);
            MainActivity.start(this, intent);
        } else if (taskId == 11) {
            //房间停留10分钟
            Intent intent = new Intent();
            intent.putExtra("curPage", 1);
            MainActivity.start(this, intent);
        } else if (taskId == 12) {
            //玩一局小游戏
            startActivity(new Intent(this, InvitationActivity.class));
        }
    }


    private void setTaskList(TaskCenterEnitity taskCenter) {
        if (taskCenter == null) {
            SingleToastUtil.showToast("获取任务数据失败");
            return;
        }

        taskCenterEnitity = taskCenter;

        tvDouziMount.setText(taskCenter.getPeanNum() + "");
        List<SignInAwardInfo> onceMissions = taskCenter.getOnceMissions();
        List<SignInAwardInfo> dailyMissions = taskCenter.getDailyMissions();
        List<SignInAwardInfo> signInMissions = taskCenter.getSignInMissions();
        if (signInMissions.size() == 7) {
            signDays = taskCenter.getSignDay();
            tvSingDays.setText(getResources().getString(R.string.task_sign_days_tips, String.valueOf(signDays)));
            //默认状态为已领取
            boolean hasTaked = true;
            //循环遍历截至今天之前的礼包是否有自动签到但是仍未领取的
            for (int i = 0; i < signInMissions.size(); i++) {
                SignInAwardInfo signInAwardInfo = signInMissions.get(i);
                //如果小于当前签到天数的，并且还有未领取的奖励的,并且领取状态未更新为未领取，则状态改为未领取
                if (i < signDays && signInAwardInfo.getMissionStatus() != 3 && hasTaked) {
                    //状态改为未领取
                    hasTaked = false;
                }
                //最后一天待领取的签到奖励
                if (i + 1 == signDays && signInAwardInfo.getMissionStatus() == 2) {
                    signinGetDouziAmountTip = "获得" + signInAwardInfo.getPeaNum() + "甜豆";
                }
            }
            //刷新对应的领取状态
            bltvGet.setVisibility(hasTaked ? View.GONE : View.VISIBLE);
            vSignInRedPoint.setVisibility(hasTaked ? View.GONE : View.VISIBLE);
            bltvGot.setVisibility(hasTaked ? View.VISIBLE : View.GONE);

            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            //三天以内的签到奖励未领取，并且注册时间小于三天，也即萌新用户的情况下，弹框提示领取
            //注意，后台当前逻辑是一经手动领取就全部领取，因此只要萌新三天内的任意一天未领取，都需要弹，都可以认为未领取当天奖励

            //先判断当天是否已经显示过弹框了
            long lastShowTimerLong = (Long) SpUtils.get(this, FLAG_SIGN_DAY_AWARD_DIALOG_SHOWED, 0L);
            boolean hasShowSignInDialog = lastShowTimerLong > 0L && TimeUtils.isSameDay(lastShowTimerLong, System.currentTimeMillis());
            signInAwardInfo = signInMissions.get(signDays - 1);

            if (!hasTaked && null != userInfo &&
                    System.currentTimeMillis() - userInfo.getCreateTime() < 86400 * 3000L
                    && signDays > 0 && !hasShowSignInDialog) {
                showSignInResultDilaog();
            }

            adapter = new SignInAwardAdapter(signDays, signInMissions);
            rlvSignAward.setAdapter(adapter);
        }
        llBeginnerTask.removeAllViews();
        llEverydayTask.removeAllViews();
        if (onceMissions == null || onceMissions.size() == 0) {
            llBeginner.setVisibility(View.GONE);
        } else {
            llBeginner.setVisibility(View.VISIBLE);
            for (SignInAwardInfo task : onceMissions) {
                View vTask = LayoutInflater.from(this).inflate(R.layout.layout_task_center, null);
                ImageView imvTaskIcon = vTask.findViewById(R.id.imvTaskIcon);
                TextView tvTitle = vTask.findViewById(R.id.tvTitle);
                TextView tvDesc = vTask.findViewById(R.id.tvDesc);
                TextView tvAction = vTask.findViewById(R.id.tvAction);
                tvAction.setOnClickListener(this);
                ImageLoadUtils.loadImage(this, task.getMissionIcon(), imvTaskIcon, R.drawable.ic_taskcenter_choujiang);
                tvTitle.setText(task.getMissionName());

                tvAction.setTag(task.getId());
                tvDesc.setText("+" + task.getPeaNum());
                if (task.getMissionStatus() == 1) {
                    //任务未完成
                    tvAction.setText("去完成");
                    tvAction.setEnabled(true);
                } else {
                    //任务已完成
                    tvAction.setText("已完成");
                    tvAction.setEnabled(false);
                }
                llBeginnerTask.addView(vTask);
            }
        }

        for (SignInAwardInfo task : dailyMissions) {
            View vTask = LayoutInflater.from(this).inflate(R.layout.layout_task_center, null);
            ImageView imvTaskIcon = vTask.findViewById(R.id.imvTaskIcon);
            TextView tvTitle = vTask.findViewById(R.id.tvTitle);
            TextView tvDesc = vTask.findViewById(R.id.tvDesc);
            TextView tvAction = vTask.findViewById(R.id.tvAction);
            tvAction.setOnClickListener(this);
            ImageLoadUtils.loadImage(this, task.getMissionIcon(), imvTaskIcon, R.drawable.ic_taskcenter_choujiang);
            tvTitle.setText(task.getMissionName());
            tvAction.setTag(task.getId());
            tvDesc.setText("+" + task.getPeaNum());
            if (task.getMissionStatus() == 1) {
                //任务未完成
                tvAction.setText("去完成");
                tvAction.setEnabled(true);
            } else {
                //任务已完成
                tvAction.setText("已完成");
                tvAction.setEnabled(false);
            }
            llEverydayTask.addView(vTask);
        }
    }

    private void showSignInResultDilaog() {
        if (null == signInAwardInfo) {
            return;
        }
        //也即宝箱，弹领取宝箱提示
        if (signInAwardInfo.getFreebiesType() == 1) {
            if (null != taskCenterEnitity) {
                boxGiftNameTip = taskCenterEnitity.getSignGift();
            }

            llGetBox.setVisibility(View.GONE);
            if (signInAwardInfo.getSeq() == 3) {
                ivBoxRecv.setImageResource(R.drawable.ic_task_center_recv_box_3);
            } else if (signInAwardInfo.getSeq() == 7) {
                ivBoxRecv.setImageResource(R.drawable.ic_task_center_recv_box_7);
            }
            llDouziRuler.setVisibility(View.GONE);
            rlTaskCenterTip.postDelayed(new Runnable() {
                @Override
                public void run() {
                    rlTaskCenterTip.setVisibility(View.VISIBLE);
                    llGetBox.setVisibility(View.VISIBLE);
                    llGetBox.setAlpha(0F);
                    llGetBox.animate().alpha(1).setDuration(500).setStartDelay(200).start();
                    SpUtils.put(TaskCenterActivity.this, FLAG_SIGN_DAY_AWARD_DIALOG_SHOWED, System.currentTimeMillis());
                }
            }, 300);
        } else {
            getDialogManager().showProgressDialog(this, getResources().getString(R.string.network_loading));
            getMvpPresenter().receiveSignInGift();
        }
    }

    /**
     * 提示签到失败
     *
     * @param message
     */
    @Override
    public void showSignErr(String message) {
        getDialogManager().dismissDialog();
        toast(message);
    }

    /**
     * 刷新领取状态为已领取
     */
    @Override
    public void refreshSignInAwardGotStatus(SignAwardResult result) {
        bltvGet.setVisibility(View.GONE);
        vSignInRedPoint.setVisibility(View.GONE);
        MsgCenterRedPointStatusManager.getInstance().setShowSignInRedPoint(false);
        bltvGot.setVisibility(View.VISIBLE);
        //展示领取动画
        toast(R.string.sign_award_tips_get_suc);
    }

    @Override
    public void refreshTaskList(TaskCenterEnitity taskCenterEnitity) {
        getDialogManager().dismissDialog();
        setTaskList(taskCenterEnitity);
    }

    /**
     * 显示萌新大礼包领取结果弹框
     *
     * @param result
     */
    @Override
    public void showResultAfterSignSuc(SignAwardResult result) {
        getDialogManager().dismissDialog();
        SignAwardResultDialog.start(this, result);
        MsgCenterRedPointStatusManager.getInstance().setShowSignInRedPoint(false);
    }
}
