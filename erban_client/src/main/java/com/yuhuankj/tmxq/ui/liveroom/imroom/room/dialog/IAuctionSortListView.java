package com.yuhuankj.tmxq.ui.liveroom.imroom.room.dialog;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.room.auction.ParticipateAuctionBean;

import java.util.List;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/7/30
 */
public interface IAuctionSortListView extends IMvpBaseView {
    default void isShowPriorityCardView(boolean show) {
    }

    default void showSortListCount(int count) {
    }

    default void isShowParticipateBtn(boolean hasJoin) {
    }

    void showAuctionSortListView(List<ParticipateAuctionBean> datas);

    void showErrorAuctionSortListView();

    default void updateSortListInfo() {
    }

    default void showErrorToast(String error) {
    }

    default void inviteAuctionSucc() {
    }

    default void downMicro(){

    }
}
