package com.yuhuankj.tmxq.ui.nim.sysmsg;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

public class SysMsgAdapter extends BaseQuickAdapter<SysMsgEnitity, BaseViewHolder> {

    public SysMsgAdapter(List data) {
        super(R.layout.item_sys_msg, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, SysMsgEnitity item) {
        ImageView imvPortrait = helper.getView(R.id.imvPortrait);
        TextView tvUserName = helper.getView(R.id.tvUserName);
        TextView tvMsg = helper.getView(R.id.tvMsg);
        TextView tvAgree = helper.getView(R.id.tvAgree);
        TextView tvAgreed = helper.getView(R.id.tvAgreed);

        helper.addOnClickListener(R.id.tvAgree);
        try {
            ImageLoadUtils.loadCircleImage(mContext, item.getAvatar(), imvPortrait, R.drawable.default_user_head);
            tvUserName.setText(item.getNick());
            tvMsg.setText(item.getMessage());
            if (item.getStatus() == 0) {
                //未处理
                tvAgree.setVisibility(View.VISIBLE);
                tvAgreed.setVisibility(View.GONE);
            } else if (item.getStatus() == 1) {
                tvAgree.setVisibility(View.GONE);
                tvAgreed.setVisibility(View.VISIBLE);
                tvAgreed.setText("已同意");
            } else if (item.getStatus() == 2) {
                tvAgree.setVisibility(View.GONE);
                tvAgreed.setVisibility(View.VISIBLE);
                tvAgreed.setText("未同意");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}