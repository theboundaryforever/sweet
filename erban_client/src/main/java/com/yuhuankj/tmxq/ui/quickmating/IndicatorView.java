package com.yuhuankj.tmxq.ui.quickmating;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.util.AttributeSet;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.jude.rollviewpager.Util;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;

/**
 * @author liaoxy
 * @Description:指示器
 * @date 2019/5/30 14:26
 */
public class IndicatorView extends RadioGroup {

    public IndicatorView(Context context) {
        super(context);
    }

    public IndicatorView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    public void setData(int size) {
        for (int i = 0; i < size; i++) {
            RadioButton rabPoint = new RadioButton(getContext());
            rabPoint.setButtonDrawable(null);
            int pointSize = Util.dip2px(getContext(), 5);

            GradientDrawable checkedDrawable = new GradientDrawable();
            checkedDrawable.setColor(Color.parseColor("#80FFFFFF"));
            checkedDrawable.setCornerRadius(Util.dip2px(getContext(), 4));
            checkedDrawable.setSize(pointSize, pointSize);

            GradientDrawable unCheckedDrawable = new GradientDrawable();
            unCheckedDrawable.setColor(Color.parseColor("#33FFFFFF"));
            unCheckedDrawable.setCornerRadius(Util.dip2px(getContext(), 4));
            unCheckedDrawable.setSize(pointSize, pointSize);

            StateListDrawable stateBackground = new StateListDrawable();
            stateBackground.addState(new int[]{android.R.attr.state_checked}, checkedDrawable);
            stateBackground.addState(new int[]{-android.R.attr.state_checked}, unCheckedDrawable);
            rabPoint.setBackground(stateBackground);

            RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(ScreenUtil.dip2px(5), ScreenUtil.dip2px(5));
            if (i != 0) {
                params.leftMargin = ScreenUtil.dip2px(15);
            }
            rabPoint.setLayoutParams(params);
            addView(rabPoint);
        }
    }

    public void setCurrent(int position) {
        if (getChildCount() == 1) {
            return;
        }
        if (position >= getChildCount()) {
            position = getChildCount() - 1;
        }
        ((RadioButton) getChildAt(position)).setChecked(true);
    }
}
