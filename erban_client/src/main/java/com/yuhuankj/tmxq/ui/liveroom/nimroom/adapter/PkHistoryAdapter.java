package com.yuhuankj.tmxq.ui.liveroom.nimroom.adapter;

import android.view.View;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.bean.PkHistoryEnitity;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.bean.PkHistoryHeadEnitity;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

public class PkHistoryAdapter extends BaseMultiItemQuickAdapter<MultiItemEntity, BaseViewHolder> {

    public static final int TYPE_LEVEL_HEAD = 0;
    public static final int TYPE_LEVEL_ITEM = 1;

    public PkHistoryAdapter(List<MultiItemEntity> data) {
        super(data);
        addItemType(TYPE_LEVEL_HEAD, R.layout.item_pk_history_head);
        addItemType(TYPE_LEVEL_ITEM, R.layout.item_pk_history);
    }

    @Override
    protected void convert(final BaseViewHolder helper, final MultiItemEntity item) {
        switch (helper.getItemViewType()) {
            case TYPE_LEVEL_HEAD:
                PkHistoryHeadEnitity header = (PkHistoryHeadEnitity) item;
                helper.setText(R.id.tvPkName, header.pkName)
                        .setText(R.id.tvTime, header.pkTime)
                        .setImageResource(R.id.imvDirection, header.isExpanded() ? R.drawable.ic_pk_history_up : R.drawable.ic_pk_history_down);
                helper.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = helper.getAdapterPosition();
                        if (header.isExpanded()) {
                            collapse(pos);
                        } else {
                            expand(pos);
                        }
                    }
                });
                break;
            case TYPE_LEVEL_ITEM:
                PkHistoryEnitity pkItem = (PkHistoryEnitity) item;
                helper.setText(R.id.tvName, pkItem.nickName)
                        .setText(R.id.tvCount, pkItem.count);
                ImageView imvPortrait = helper.getView(R.id.imvPortrait);
                ImageLoadUtils.loadCircleImage(mContext, pkItem.portrait, imvPortrait, R.drawable.default_user_head);
                break;
        }
    }
}
