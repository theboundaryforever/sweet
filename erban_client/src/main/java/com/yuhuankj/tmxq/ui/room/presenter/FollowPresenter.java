package com.yuhuankj.tmxq.ui.room.presenter;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.yuhuankj.tmxq.ui.room.interfaces.FollowView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class FollowPresenter extends AbstractMvpPresenter<FollowView> {

    private final String TAG = FollowPresenter.class.getSimpleName();


    public FollowPresenter() {
    }

    /**
     * 获取关注房间列表
     */
    public void getAttentionRoomList(int pageNum) {
        Map<String, String> param = OkHttpManager.getDefaultParam();
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        param.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        param.put("pageNum", String.valueOf(pageNum));
        param.put("pageSize", String.valueOf(Constants.PAGE_SIZE));
        OkHttpManager.getInstance().getRequest(UriProvider.getAttentionRoomListUrl(), param, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (null != getMvpView()) {
                    getMvpView().onGetAttentionRoomList(false, null);
                }
            }

            @Override
            public void onResponse(Json json) {
                LogUtils.d(TAG, "getAttentionRoomList-onResponse json:" + json);
                if (null != getMvpView()) {
                    if (200 != json.num("code")) {
                        getMvpView().onGetAttentionRoomList(false, null);
                        return;
                    }
                    getMvpView().onGetAttentionRoomList(true, json.jlist("data"));
                }
            }
        });
    }

    /**
     * 获取推荐房间列表
     */
    public void getOppositeRoom() {
        Map<String, String> param = OkHttpManager.getDefaultParam();
        param.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.getOppositeRoom(), param, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (null != getMvpView()) {
                    getMvpView().onGetOppositeRoom(false, null);
                }
            }

            @Override
            public void onResponse(Json json) {
                LogUtils.d(TAG, "getOppositeRoom-onResponse json:" + json);
                if (null != getMvpView()) {
                    if (200 != json.num("code")) {
                        getMvpView().onGetOppositeRoom(false, null);
                        return;
                    }
                    getMvpView().onGetOppositeRoom(true, json.jlist("data"));
                }
            }
        });
    }

    /**
     * 获取关注用户列表
     */
    public void getFollowUserList() {
        Map<String, String> param = OkHttpManager.getDefaultParam();
        param.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.getHomeFollow(), param, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (null != getMvpView()) {
                    getMvpView().onGetFollowUserList(false, null);
                }
            }

            @Override
            public void onResponse(Json json) {
                LogUtils.d(TAG, "getFollowUserList-onResponse json:" + json);
                if (null != getMvpView()) {
                    if (200 != json.num("code")) {
                        getMvpView().onGetFollowUserList(false, null);
                        return;
                    }
                    //这里需要过滤非在线的用户
                    List<Json> data = json.jlist("data");
                    List<Json> delete = new ArrayList<>();
                    if (null != data) {
                        for (Json obj : data) {
                            if (!obj.has("userInRoom")) {
                                delete.add(obj);
                            }
                        }
                    }
                    for (Json obj : delete) {
                        data.remove(obj);
                    }
                    getMvpView().onGetFollowUserList(true, data);
                }

            }
        });
    }

    /**
     * 获取我的足迹
     */
    public void getMyFooprint() {
        Map<String, String> param = OkHttpManager.getDefaultParam();
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        param.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        OkHttpManager.getInstance().postRequest(UriProvider.getMyFooprint(), param, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();

                if (null != getMvpView()) {
                    getMvpView().onMyFooprint(false, null);
                }
            }

            @Override
            public void onResponse(Json json) {
                LogUtils.d(TAG, "getAttentionRoomList-onResponse json:" + json);
                if (null != getMvpView()) {
                    if (200 != json.num("code")) {
                        getMvpView().onMyFooprint(false, null);
                        return;
                    }
                    getMvpView().onMyFooprint(true, json.jlist("data"));
                }
            }
        });
    }
}
