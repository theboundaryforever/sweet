package com.yuhuankj.tmxq.ui.me.charge.presenter;

import android.content.Context;

import com.google.gson.JsonObject;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.NetworkUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.pay.bean.ChargeBeanResult;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.ui.me.charge.interfaces.IChargeView;
import com.yuhuankj.tmxq.ui.me.charge.model.PayModel;

import java.util.Map;

/**
 * Created by MadisonRong on 05/01/2018.
 */
public class ChargePresenter extends PayPresenter<IChargeView> {

    private static final String TAG = "ChargePresenter";

    public ChargePresenter() {
        this.payModel = new PayModel();
    }

    /**
     * 加载用户信息(显示用户账号和钱包余额)
     */
    public void loadUserInfo() {
        UserInfo userInfo = payModel.getUserInfo();
        if (userInfo != null) {
            getMvpView().setupUserInfo(userInfo);
        }
        refreshWalletInfo(false);
    }

    /**
     * 获取充值产品列表
     */
    public void getChargeList() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("channelType", "1");

        OkHttpManager.getInstance().getRequest(UriProvider.getChargeList(), params, new OkHttpManager.MyCallBack<ChargeBeanResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (getMvpView() != null) {
                    getMvpView().getChargeListFail(e.getMessage());
                }
            }

            @Override
            public void onResponse(ChargeBeanResult response) {
                if (response != null && getMvpView() != null) {
                    getMvpView().buildChargeList(response.getData());
                }
            }
        });
    }

    /**
     * 显示充值选项(支付宝或者微信)
     */
    public void showChargeOptionsDialog() {

        getMvpView().displayChargeOptionsDialog();

    }

    /**
     * 发起充值
     *
     * @param context      context
     * @param chargeProdId 充值产品 ID
     * @param payChannel   充值渠道，目前只支持
     *                     {@link com.tongdaxing.xchat_core.Constants#CHARGE_ALIPAY} 和
     *                     {@link com.tongdaxing.xchat_core.Constants#CHARGE_WX}
     */
    public void requestCharge(Context context, String chargeProdId, String payChannel) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("chargeProdId", chargeProdId);
        param.put("payChannel", payChannel);
        param.put("clientIp", NetworkUtils.getIPAddress(context));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());

        OkHttpManager.getInstance().postRequest(UriProvider.requestCharge(), param, new OkHttpManager.MyCallBack<ServiceResult<JsonObject>>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (null != getMvpView()) {
                    getMvpView().getChargeOrOrderInfoFail(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<JsonObject> response) {
                if (null == getMvpView()) {
                    return;
                }
                if (null != response && response.isSuccess()) {
                    getMvpView().getChargeOrOrderInfo(response.getData().toString());
                } else {
                    getMvpView().getChargeOrOrderInfoFail(null != response ? response.getErrorMessage() : "");
                }
            }
        });
    }

   //请求使用汇聚支付
   public void getJoinPayData(Context context, String chargeProdId, String payChannel) {
       Map<String, String> param = CommonParamUtil.getDefaultParam();
       param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
       param.put("chargeProdId", chargeProdId);
       param.put("payChannel", payChannel);
       param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
       OkHttpManager.getInstance().postRequest(UriProvider.getJoinPay(), param, new OkHttpManager.MyCallBack<ServiceResult<JsonObject>>() {
           @Override
           public void onError(Exception e) {
               e.printStackTrace();
               if (null != getMvpView()) {
                   getMvpView().getChargeOrOrderInfoFail(e.getMessage());
               }
           }

           @Override
           public void onResponse(ServiceResult<JsonObject> response) {
               if (null == getMvpView()) {
                   return;
               }
               if (null != response && response.isSuccess()) {
                   getMvpView().getChargeOrOrderInfo(response.getData().toString());
               } else {
                   getMvpView().getChargeOrOrderInfoFail(null != response ? response.getErrorMessage() : "");
               }
           }
       });
   }

    //请求使用汇潮支付
    public void getHuiChaoPayData(String chargeProdId) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("chargeProdId", chargeProdId);
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().postRequest(UriProvider.getHuiChaoPay(), param, new OkHttpManager.MyCallBack<ServiceResult<JsonObject>>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (null != getMvpView()) {
                    getMvpView().getChargeOrOrderInfoFail(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<JsonObject> response) {
                if (null == getMvpView()) {
                    return;
                }
                if (null != response && response.isSuccess()) {
                    getMvpView().getChargeOrOrderInfo(response.getData().toString());
                } else {
                    getMvpView().getChargeOrOrderInfoFail(null != response ? response.getErrorMessage() : "");
                }
            }
        });
    }
}
