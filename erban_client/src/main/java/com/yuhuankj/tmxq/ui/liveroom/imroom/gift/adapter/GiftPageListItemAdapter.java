package com.yuhuankj.tmxq.ui.liveroom.imroom.gift.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.juxiao.library_utils.DisplayUtils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftType;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.imroom.gift.RoomGiftConstant;
import com.yuhuankj.tmxq.ui.liveroom.imroom.gift.listener.OnItemClickListener;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.ArrayList;
import java.util.List;

import static com.yuhuankj.tmxq.ui.liveroom.imroom.gift.RoomGiftConstant.PAGE_SIZE;

/**
 * 文件描述：礼物单个网格列表的适配器
 *
 * @auther：zwk
 * @data：2019/5/10
 */
public class GiftPageListItemAdapter extends RecyclerView.Adapter<GiftPageListItemAdapter.GiftListItemHolder> {
    private Context mContext;
    private List<GiftInfo> mDatas;
    private int index = 0;
    private int itemWidth = 0;
    private GiftType giftType;

    public GiftPageListItemAdapter(Context mContext, GiftType giftType) {
        this.mContext = mContext;
        if (this.mDatas == null) {
            this.mDatas = new ArrayList<>();
        }
        itemWidth = DisplayUtils.getScreenWidth(mContext) / 4;
        this.giftType = giftType;
    }

    @NonNull
    @Override
    public GiftPageListItemAdapter.GiftListItemHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int giftType) {
        GiftPageListItemAdapter.GiftListItemHolder holder = new GiftPageListItemAdapter.GiftListItemHolder(LayoutInflater.from(mContext).inflate(R.layout.item_rv_gift_list, viewGroup, false));
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull GiftPageListItemAdapter.GiftListItemHolder holder, int position) {
        GiftInfo giftInfo = getPositionData(position);
//        if (itemWidth > 0) {
//            RecyclerView.LayoutParams rvl = (RecyclerView.LayoutParams) holder.rl_giftItem.getLayoutParams();
//            rvl.width = itemWidth;
//            holder.rl_giftItem.setLayoutParams(rvl);
//        }
        holder.giftName.setText(giftInfo.getGiftName());
        if (giftInfo.getIsActivity() == 1) {
            holder.giftGold.setText("活动掉落");
        } else if (giftInfo.getIsSale() == 0) {
            holder.giftGold.setText("暂不出售");
        } else if (giftInfo.getIsPea() == 1) {
            holder.giftGold.setText(giftInfo.getGoldPrice() + "甜豆");
        } else {
            holder.giftGold.setText(giftInfo.getGoldPrice() + "金币");
        }
        if (giftInfo.getUserGiftPurseNum() > 0) {
            holder.freeGiftCount.setVisibility(View.VISIBLE);
            holder.freeGiftCount.setText(mContext.getResources().getString(R.string.gift_number, giftInfo.getUserGiftPurseNum()));
        } else {
            holder.freeGiftCount.setVisibility(View.GONE);
        }

//        holder.bind(position);
        String giftUrl = ImageLoadUtils.toThumbnailUrl(130, 130, giftInfo.getGiftUrl());
        ImageLoadUtils.loadImage(BasicConfig.INSTANCE.getAppContext(), giftUrl, holder.giftImage);
        if (giftInfo.isExclusive()) {
            //如果是专属礼物
            holder.imvNobleIcon.setVisibility(View.VISIBLE);
            holder.imvNobleIcon.setImageResource(R.drawable.ic_gift_noble_exclusive);
        } else {
            if (giftInfo.getNobleId() > 0 && !TextUtils.isEmpty(giftInfo.getNobleIcon())) {
                holder.imvNobleIcon.setVisibility(View.VISIBLE);
                ImageLoadUtils.loadImage(BasicConfig.INSTANCE.getAppContext(), giftInfo.getNobleIcon(), holder.imvNobleIcon);
            } else {
                holder.imvNobleIcon.setVisibility(View.GONE);
            }
        }
        //如果四个都显示了,那把其中一个图标放到下面一列
        if (giftInfo.isHasEffect() && giftInfo.isHasTimeLimit() && giftInfo.isHasLatest() && holder.imvNobleIcon.getVisibility() == View.VISIBLE) {
            holder.giftEffects.setVisibility(View.GONE);
            holder.giftLimitTime.setVisibility(View.GONE);
            holder.giftNew.setVisibility(View.GONE);

            holder.giftEffects1.setVisibility(View.VISIBLE);
            holder.giftLimitTime1.setVisibility(View.VISIBLE);
            holder.giftNew1.setVisibility(View.VISIBLE);
        } else {
            holder.giftEffects.setVisibility(giftInfo.isHasEffect() ? View.VISIBLE : View.GONE);
            holder.giftLimitTime.setVisibility(giftInfo.isHasTimeLimit() ? View.VISIBLE : View.GONE);
            holder.giftNew.setVisibility(giftInfo.isHasLatest() ? View.VISIBLE : View.GONE);

            holder.giftEffects1.setVisibility(View.GONE);
            holder.giftLimitTime1.setVisibility(View.GONE);
            holder.giftNew1.setVisibility(View.GONE);
        }
        holder.rl_giftItem.setBackgroundDrawable((isCurrentSelectGiftType() && isCurrentSelectPosition(position)) ? mContext.getResources().getDrawable(R.drawable.bg_gift_dialog_selected) : null);
        holder.rl_giftItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isCurrentSelectGiftType() || !isCurrentSelectPosition(position)) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(giftInfo, getCurrentRealPosition(position));
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mDatas == null || mDatas.size() == 0) {
            return 0;
        } else {
            return mDatas.size() > (index + 1) * PAGE_SIZE ? PAGE_SIZE : (mDatas.size() - index * PAGE_SIZE);
        }
    }

    /**
     * 获取该位置的对象数据
     *
     * @param position
     * @return
     */
    public GiftInfo getPositionData(int position) {
        if (mDatas == null || position + index * PAGE_SIZE >= mDatas.size()) {
            return null;
        } else {
            return mDatas.get(position + index * PAGE_SIZE);
        }
    }

    public void updataDataAndIndex(List<GiftInfo> mDatas, int postion) {
        this.mDatas = mDatas;
        this.index = postion;
        notifyDataSetChanged();
    }

    /**
     * 获取当前列表位置对应整个列表的真实位置
     *
     * @param position
     * @return
     */
    public int getCurrentRealPosition(int position) {
        return (position + index * PAGE_SIZE);
    }


    /**
     * 判断当列表的位置是否为整个类型的礼物列表的选中礼物的位置
     *
     * @param position
     * @return
     */
    public boolean isCurrentSelectPosition(int position) {
        return (position + index * PAGE_SIZE) == RoomGiftConstant.currentGiftTypeSelect;
    }

    /**
     * 判断当列表的位置是否为整个类型的礼物列表的选中礼物的类型
     *
     * @return
     */
    public boolean isCurrentSelectGiftType() {
        return giftType == RoomGiftConstant.currentGiftType;
    }

    class GiftListItemHolder extends RecyclerView.ViewHolder {
        private ImageView giftImage;
        private TextView giftName;
        private TextView giftGold;
        private ImageView giftEffects;
        private ImageView giftNew;
        private ImageView giftLimitTime;
        private TextView freeGiftCount;
        private View rl_giftItem;
        private ImageView imvNobleIcon;

        private ImageView giftEffects1;
        private ImageView giftNew1;
        private ImageView giftLimitTime1;

        public GiftListItemHolder(@NonNull View itemView) {
            super(itemView);
            giftImage = (ImageView) itemView.findViewById(R.id.gift_image);
//            gift_anim_image = (ImageView) itemView.findViewById(R.id.gift_anim_image);
            rl_giftItem = itemView.findViewById(R.id.rl_giftItem);
            giftGold = (TextView) itemView.findViewById(R.id.gift_gold);
            giftName = (TextView) itemView.findViewById(R.id.gift_name);
            giftEffects = (ImageView) itemView.findViewById(R.id.icon_gift_effect);
            giftNew = (ImageView) itemView.findViewById(R.id.icon_gift_new);
            giftLimitTime = (ImageView) itemView.findViewById(R.id.icon_gift_limit_time);
            freeGiftCount = (TextView) itemView.findViewById(R.id.tv_free_gift_count);
            imvNobleIcon = (ImageView) itemView.findViewById(R.id.imvNobleIcon);
            giftEffects1 = (ImageView) itemView.findViewById(R.id.icon_gift_effect1);
            giftNew1 = (ImageView) itemView.findViewById(R.id.icon_gift_new1);
            giftLimitTime1 = (ImageView) itemView.findViewById(R.id.icon_gift_limit_time1);
        }
    }


    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }
}
