package com.yuhuankj.tmxq.ui.me.wallet.bills;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.bills.IBillsCore;
import com.tongdaxing.xchat_core.bills.IBillsCoreClient;
import com.tongdaxing.xchat_core.bills.bean.BillItemEntity;
import com.tongdaxing.xchat_core.bills.bean.IncomeInfo;
import com.tongdaxing.xchat_core.bills.bean.IncomeListInfo;
import com.tongdaxing.xchat_core.pay.IPayCore;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.me.charge.ChargeActivity;
import com.yuhuankj.tmxq.ui.me.wallet.bills.adapter.GiftIncomeAdapter;
import com.yuhuankj.tmxq.widget.TitleBar;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p> 账单礼物收入界面 </p>
 *
 * @author Administrator
 * @date 2017/11/7
 */
public class BillGiftInComeActivity extends BillBaseActivity {
    private GiftIncomeAdapter adapter;
    private TextView mGoldNum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initTitleBar(getString(R.string.bill_gift_income));
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_bills_gift;
    }

    @Override
    protected void initView() {
        super.initView();
        mGoldNum = (TextView) findViewById(R.id.tv_gold_num);

    }

    @Override
    protected void initData() {
        super.initData();
        adapter = new GiftIncomeAdapter(mBillItemEntityList);
        adapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                mCurrentCounter++;
                loadData();
            }
        }, mRecyclerView);

        RecyclerViewNoBugLinearLayoutManager manager = new RecyclerViewNoBugLinearLayoutManager(mActivity);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setAdapter(adapter);


        firstLoadDate();
    }

    private void firstLoadDate() {
        mCurrentCounter = Constants.PAGE_START;
        showLoading();
        onLoadGoldNum();
        loadData();
    }

    @Override
    protected void loadData() {
        CoreManager.getCore(IBillsCore.class).getGiftIncomeBills(mCurrentCounter, PAGE_SIZE, time);
    }

    private void onLoadGoldNum() {
        WalletInfo walletInfo = CoreManager.getCore(IPayCore.class).getCurrentWalletInfo();
        if (null != walletInfo) {
            mGoldNum.setText(getString(R.string.bill_gift_gold_num, walletInfo.getGoldNum()));
        }
    }

    @CoreEvent(coreClientClass = IBillsCoreClient.class)
    public void onGetIncomeBills(IncomeListInfo data) {
        mRefreshLayout.setRefreshing(false);
        if (null != data) {
            if (mCurrentCounter == Constants.PAGE_START) {
                hideStatus();
                mBillItemEntityList.clear();
                adapter.setNewData(mBillItemEntityList);
            } else {
                adapter.loadMoreComplete();
            }
            List<Map<String, List<IncomeInfo>>> billList = data.getBillList();
            if (!billList.isEmpty()) {
                tvTime.setVisibility(View.GONE);
                int size = mBillItemEntityList.size();
                List<BillItemEntity> billItemEntities = new ArrayList<>();
                BillItemEntity billItemEntity;
                for (int i = 0; i < billList.size(); i++) {
                    Map<String, List<IncomeInfo>> map = billList.get(i);
                    for (String key : map.keySet()) {
//                        // key ---日期    value：list集合记录
                        List<IncomeInfo> incomeInfos = map.get(key);
                        if (ListUtils.isListEmpty(incomeInfos)) continue;
                        //正常item
                        for (IncomeInfo temp : incomeInfos) {
                            billItemEntity = new BillItemEntity(BillItemEntity.ITEM_NORMAL);
                            billItemEntity.mGiftInComeInfo = temp;
                            //目的是为了比较
                            billItemEntity.time = key;
                            billItemEntities.add(billItemEntity);
                        }
                    }
                }
                if (billItemEntities.size() < Constants.BILL_PAGE_SIZE && mCurrentCounter == Constants.PAGE_START) {
                    adapter.setEnableLoadMore(false);
                }
                adapter.addData(billItemEntities);
            } else {
                if (mCurrentCounter == Constants.PAGE_START) {
                    showNoData(getResources().getString(R.string.bill_no_data_text));
                } else {
                    adapter.loadMoreEnd(true);
                }
            }
        }
    }

    @CoreEvent(coreClientClass = IBillsCoreClient.class)
    public void onGetIncomeBillsError(String error) {
        if (mCurrentCounter == Constants.PAGE_START) {
            showNetworkErr();
        } else {
            adapter.loadMoreFail();
        }
    }

    @Override
    public void initTitleBar(String title) {
        TitleBar mTitleBar = (TitleBar) findViewById(R.id.title_bar);
        if (mTitleBar != null) {
            mTitleBar.setTitle(title);
            mTitleBar.setImmersive(false);
            mTitleBar.setTitleColor(getResources().getColor(R.color.back_font));
            mTitleBar.setLeftImageResource(R.drawable.arrow_left);
            mTitleBar.setLeftClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
        mTitleBar.setActionTextColor(getResources().getColor(R.color.text_tertiary));
        mTitleBar.addAction(new TitleBar.TextAction("充值") {
            @Override
            public void performAction(View view) {
                startActivity(new Intent(mActivity, ChargeActivity.class));
            }
        });
    }

    @Override
    public void onReloadDate() {
        super.onReloadDate();
        firstLoadDate();
    }
}
