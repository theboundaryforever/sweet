package com.yuhuankj.tmxq.ui.room.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

/**
 * <p> 首页非热门adapter </p>
 *
 * @author Administrator
 * @date 2017/11/15
 */
public class HotRecommendAdapter extends BaseQuickAdapter<Json, BaseViewHolder> {

    private int recommBgWidth = 0;
    private int recommBgHeight = 0;
    private int recommIconWidthAndHeight = 0;
    private final Context context;

    public HotRecommendAdapter(Context context, @Nullable List<Json> data) {
        super(R.layout.hot_list_recommend_item, data);
        this.context = context;
        recommBgWidth = DisplayUtility.dp2px(mContext, 190);
        recommBgHeight = DisplayUtility.dp2px(mContext, 80);
        recommIconWidthAndHeight = DisplayUtility.dp2px(mContext, 60);
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, Json json) {
        if (json == null) {
            return;
        }
        baseViewHolder.setText(R.id.tv_recommend_title, json.str("title"));
        baseViewHolder.setText(R.id.tv_recommend_online_count, json.str("onlineNum") + "人在线");
        ImageView imvRunning = baseViewHolder.getView(R.id.imvRunning);
        ImageView ivIcon = baseViewHolder.getView(R.id.iv_recommend_icon);
        String avatar = json.str("avatar");
        if (GlideContextCheckUtil.checkContextUsable(context)) {
//            GlideApp.with(context).asGif().load(R.drawable.anim_game_home_room_running).diskCacheStrategy(DiskCacheStrategy.ALL).into(imvRunning);
            ImageLoadUtils.loadImage(context, ImageLoadUtils.toThumbnailUrl(recommIconWidthAndHeight, recommIconWidthAndHeight, avatar), ivIcon);
        }
    }
}
