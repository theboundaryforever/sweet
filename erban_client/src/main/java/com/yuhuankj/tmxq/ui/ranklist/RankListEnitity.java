package com.yuhuankj.tmxq.ui.ranklist;

import java.io.Serializable;

/**
 * 排行榜数据
 */
public class RankListEnitity implements Serializable {

    private String avatar;//": "https://img.pinjin88.com/FrdGDhIxIcNwGZLlDWp_djC3Xljn?imageslim",
    private int charmLevel;//": 42,
    private long erbanNo;//": 2222229,
    private int experLevel;//": 50,
    private int gender;//": 1,
    private String nick;//": "2nd🎁t/出追光者",
    private long totalNum;//": 1.03044457E8,
    private long uid;//": 260289
    private String vipIcon;//": "",
    private int vipId;//": 0,
    private String vipMedal;//": "",
    private String vipName;//": ""

    public String getVipIcon() {
        return vipIcon;
    }

    public void setVipIcon(String vipIcon) {
        this.vipIcon = vipIcon;
    }

    public int getVipId() {
        return vipId;
    }

    public void setVipId(int vipId) {
        this.vipId = vipId;
    }

    public String getVipMedal() {
        return vipMedal;
    }

    public void setVipMedal(String vipMedal) {
        this.vipMedal = vipMedal;
    }

    public String getVipName() {
        return vipName;
    }

    public void setVipName(String vipName) {
        this.vipName = vipName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getCharmLevel() {
        return charmLevel;
    }

    public void setCharmLevel(int charmLevel) {
        this.charmLevel = charmLevel;
    }

    public long getErbanNo() {
        return erbanNo;
    }

    public void setErbanNo(long erbanNo) {
        this.erbanNo = erbanNo;
    }

    public int getExperLevel() {
        return experLevel;
    }

    public void setExperLevel(int experLevel) {
        this.experLevel = experLevel;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public long getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(long totalNum) {
        this.totalNum = totalNum;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }
}
