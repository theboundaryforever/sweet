package com.yuhuankj.tmxq.ui.search;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.widget.LinearLayout;

import com.netease.nim.uikit.StatusBarUtil;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.ui.search.ui.adapter.SearchAdapter;
import com.yuhuankj.tmxq.ui.search.ui.adapter.SearchMagicIndicatorAdapter;
import com.yuhuankj.tmxq.widget.TitleBar;
import com.yuhuankj.tmxq.widget.magicindicator.MagicIndicator;
import com.yuhuankj.tmxq.widget.magicindicator.ViewPagerHelper;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.UIUtil;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.commonnavigator.CommonNavigator;

import java.util.ArrayList;
import java.util.List;

/**
 * 搜索界面
 *
 * @author chenran
 * @date 2017/10/3
 */
public class SearchActivity extends BaseMvpActivity implements SearchMagicIndicatorAdapter.OnItemSelectListener, IMvpBaseView {

    private MagicIndicator mi_ind;
    private ViewPager vp_search;

    private List<TabInfo> mTabInfoList;

    public static void start(Context context) {
        Intent intent = new Intent(context, SearchActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        addView(StatusBarUtil.StatusBarLightMode(this));
        initView();
        initViewPager();
    }

    private void initView() {
        initToolBar();
        initTabIndicator();
    }

    public void initToolBar() {
        mTitleBar = (TitleBar) findViewById(R.id.tv_nav);
        if (mTitleBar != null) {
            mTitleBar.setTitle(getString(R.string.search));
            mTitleBar.setImmersive(false);
            mTitleBar.setTitleColor(getResources().getColor(R.color.back_font));
            mTitleBar.setLeftImageResource(R.drawable.arrow_left);
            mTitleBar.setLeftClickListener(v -> finish());
            mTitleBar.setTitleSize(17);
            mTitleBar.setCommonBackgroundColor(Color.WHITE);
        }
        mTitleBar.setActionTextColor(getResources().getColor(R.color.text_tertiary));
    }

    private void initTabIndicator(){
        mi_ind = (MagicIndicator) findViewById(R.id.mi_ind);

        mTabInfoList = new ArrayList<>();
        mTabInfoList.add(0,new TabInfo(-2,getString(R.string.search_tab_friend)));
        mTabInfoList.add(0,new TabInfo(-1,getString(R.string.search_tab_room)));

        CommonNavigator commonNavigator = new CommonNavigator(this);
        SearchMagicIndicatorAdapter magicIndicatorAdapter = new SearchMagicIndicatorAdapter(this,
                mTabInfoList, UIUtil.dip2px(this, 2),UIUtil.dip2px(this, 6));
        magicIndicatorAdapter.setOnItemSelectListener(this);
        commonNavigator.setAdapter(magicIndicatorAdapter);
        mi_ind.setNavigator(commonNavigator);
        LinearLayout titleContainer = commonNavigator.getTitleContainer();
        titleContainer.setGravity(Gravity.CENTER);
        magicIndicatorAdapter.setSize(15);
    }

    private void initViewPager(){
        vp_search = (ViewPager) findViewById(R.id.vp_search);
        vp_search.setAdapter(new SearchAdapter(getSupportFragmentManager(), mTabInfoList));
        ViewPagerHelper.bind(mi_ind, vp_search);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onItemSelect(int position) {
        vp_search.setCurrentItem(position);
    }
}
