package com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget;

import android.content.Context;
import android.graphics.Point;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.erban.libcommon.widget.MicroWaveView;
import com.tongdaxing.xchat_core.im.custom.bean.RoomCharmAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomCharmInfo;
import com.tongdaxing.xchat_core.liveroom.im.model.AudioConnTimeOutTipsQueueScheduler;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomEvent;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomQueueInfo;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.room.face.FaceReceiveInfo;
import com.uuzuche.lib_zxing.DisplayUtil;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter.SingleAudioConnMicroAdapter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.listener.OnMicroItemClickListener;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.face.AnimFaceFactory;

import java.util.List;

import io.reactivex.disposables.Disposable;

/**
 * 文件描述：单人房，中间四坑位布局
 *
 * @auther：weihaitao
 * @data：2019年7月16日 10:06:01
 */
public class SingleAudioConnMicroView extends AbstractMicroView {

    private final String TAG = SingleAudioConnMicroView.class.getSimpleName();

    private Disposable subscribe;
    private RecyclerView rvRoomMicro;
    private SingleAudioConnMicroAdapter singleAudioConnMicroAdapter;
    private int faceWidth;
    private OnVisibilityChangeListener onVisibilityChangeListener;

    public SingleAudioConnMicroView(Context context) {
        super(context);
    }

    public SingleAudioConnMicroView(Context context, AttributeSet attr) {
        super(context, attr);
    }

    @Override
    protected int getMicroLayoutId() {
        return R.layout.view_single_multi_audio_micro;
    }

    @Override
    public void init(Context context) {
        faceWidth = DisplayUtil.dip2px(context, 60);
        super.init(context);
        rvRoomMicro = findViewById(R.id.rv_room_normal_micro);
        if (rvRoomMicro.getItemAnimator() != null) {
            ((DefaultItemAnimator) rvRoomMicro.getItemAnimator()).setSupportsChangeAnimations(false);
        }
        initState(context);
    }

    private void initState(Context context) {
        singleAudioConnMicroAdapter = new SingleAudioConnMicroAdapter(context);
        rvRoomMicro.setAdapter(singleAudioConnMicroAdapter);
        refreshAudioConnMicroView(RoomEvent.NONE);
    }

    @Override
    protected void calculateMicCenterPoint() {
        // 普通麦位的位置
        if (null == RoomDataManager.get().mMicPointMap) {
            RoomDataManager.get().mMicPointMap = new SparseArray<>();
        }
        int count = rvRoomMicro.getChildCount();
        View child;
        for (int i = 0; i < count; i++) {
            child = rvRoomMicro.getChildAt(i);
            int[] location3 = new int[2];
            child.getLocationInWindow(location3);
            Point point3 = new Point(location3[0], location3[1]);
            RoomDataManager.get().mMicPointMap.put(i, point3);
            // 放置表情占位image view
            addFaceInfoView(i, location3);
        }
    }

    /**
     * 添加表情的ImageView的位置信息
     *
     * @param i
     * @param location3
     */
    private void addFaceInfoView(int i, int[] location3) {
        if (getFaceImageViews().get(i) == null && getContext() != null) {
            LayoutParams params = new LayoutParams(faceWidth * 2 / 3, faceWidth * 2 / 3);
            params.leftMargin = location3[0];
            params.topMargin = location3[1];
            ImageView face = new ImageView(getContext());
            face.setLayoutParams(params);
            getFaceImageViews().put(i, face);
            addView(face);
        }
    }

    @Override
    public void notifyDataSetChanged(int roomEvent) {
        if (singleAudioConnMicroAdapter != null) {
            singleAudioConnMicroAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void notifyItemChanged(int position, int roomEvent) {
        if (position <= -1) {
            return;
        }
        if (singleAudioConnMicroAdapter != null) {
            singleAudioConnMicroAdapter.notifyItemChanged(position);
        }
    }

    /**
     * 房间麦位的声浪监听 -- 控制WaveView的开启播放
     *
     * @param micPositionList
     */
    public void onSpeakQueueUpdate(List<Integer> micPositionList) {
        for (int i = 0; i < micPositionList.size(); i++) {
            int pos = micPositionList.get(i);
            if (pos >= 0 && pos < 4) {
                if (rvRoomMicro != null) {
                    MicroWaveView speakState = rvRoomMicro.getChildAt(pos).findViewById(R.id.waveview);
                    if (speakState != null) {
                        speakState.start();
                    }
                }
            }
        }
    }

    @Override
    protected MicroWaveView getRoomOnerSpeakView() {
        return null;
    }

    @Override
    protected MicroWaveView getRoomNormalSpeakView(int pos) {
        return null;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        subscribe = IMRoomMessageManager.get().getIMRoomEventObservable().subscribe(this::onReceiveRoomEvent);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (subscribe != null) {
            subscribe.dispose();
            subscribe = null;
        }
    }

    private void onReceiveRoomEvent(IMRoomEvent roomEvent) {
        if (roomEvent == null || roomEvent.getEvent() == RoomEvent.NONE) {
            return;
        }
        switch (roomEvent.getEvent()) {
            case IMRoomEvent.MIC_QUEUE_STATE_CHANGE:
                LogUtils.d(TAG, "坑位队列状态更新");
                notifyDataSetChanged(roomEvent.getEvent());
                break;
            case IMRoomEvent.ROOM_RECONNECT:
            case IMRoomEvent.SOCKET_ROOM_ENTER_SUC://socket进房(包括socket重连进房)
                LogUtils.d(TAG, "断线重连,重新进房成功");
                refreshAudioConnMicroView(roomEvent.getEvent());
                break;
            case IMRoomEvent.KICK_DOWN_MIC:
                LogUtils.d(TAG, "被踢下麦");
                refreshAudioConnMicroView(roomEvent.getEvent());
                break;
            case IMRoomEvent.DOWN_MIC:
                //下麦
                LogUtils.d(TAG, "下麦");
                refreshAudioConnMicroView(roomEvent.getEvent());
                break;
            case IMRoomEvent.UP_MIC:
                //上麦
                LogUtils.d(TAG, "上麦");
                if (!TextUtils.isEmpty(roomEvent.getAccount())) {
                    AudioConnTimeOutTipsQueueScheduler.getInstance().stopDelayTipsRunnable(roomEvent.getAccount());
                }
                refreshAudioConnMicroView(roomEvent.getEvent());
                break;
            case IMRoomEvent.ROOM_AUDIO_CONN_REQ_RECV_NEW:
                LogUtils.d(TAG, "接收到新的连麦申请通知");
//                refreshAudioConnMicroView(roomEvent.getEvent());
                break;
            case IMRoomEvent.ROOM_MIC_CHARM_UPDATE:
                LogUtils.d(TAG, "坑位魅力值更新通知");
                updateCharmData(roomEvent.getRoomCharmInfo());
                break;
            case RoomEvent.SPEAK_STATE_CHANGE:
                onSpeakQueueUpdate(roomEvent.getMicPositionList());
                break;
            default:
        }
    }

    private void refreshAudioConnMicroView(int event) {
        //判断0-3坑位是否有member信息，有则表示有人，无则表示没人，有人则显示，没人则隐藏
        if (RoomDataManager.get().checkAudioConnMicQueueExist()) {
            setVisibility(View.VISIBLE);
            notifyDataSetChanged(event);

            rvRoomMicro.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    rvRoomMicro.postDelayed(() -> calculateMicCenterPoint(), 500);
                    rvRoomMicro.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });
        } else {
            setVisibility(GONE);
        }
    }

    /**
     * 更新麦位魅力值
     *
     * @param roomCharmAttachment
     */
    @Override
    public void updateCharmData(RoomCharmAttachment roomCharmAttachment) {
        if (roomCharmAttachment == null || RoomDataManager.get().mMicQueueMemberMap == null) {
            return;
        }
        if (roomCharmAttachment.getLatestCharm() != null
                && roomCharmAttachment.getLatestCharm().size() > 0
                && roomCharmAttachment.getTimestamps() > RoomDataManager.get().getCharmTimestamps()) {
            RoomDataManager.get().setCharmTimestamps(roomCharmAttachment.getTimestamps());
            for (int i = 1; i < RoomDataManager.get().mMicQueueMemberMap.size(); i++) {
                IMRoomQueueInfo roomQueueInfo = RoomDataManager.get().mMicQueueMemberMap.valueAt(i);
                if (roomQueueInfo != null) {
                    if (roomQueueInfo.mChatRoomMember != null) {
                        if (StringUtils.isNotEmpty(roomQueueInfo.mChatRoomMember.getAccount())) {
                            RoomCharmInfo roomCharmInfo = roomCharmAttachment.getLatestCharm().get(roomQueueInfo.mChatRoomMember.getAccount());
                            if (roomCharmInfo != null && roomQueueInfo.getValue() != roomCharmInfo.getValue()) {
                                roomQueueInfo.setValue(roomCharmInfo.getValue());
                                if (singleAudioConnMicroAdapter != null) {
                                    singleAudioConnMicroAdapter.notifyItemChanged(roomQueueInfo.mRoomMicInfo.getPosition() + 1);
                                }
                            }
                        }
                    } else {
                        roomQueueInfo.setValue(0);
                    }
                }
            }
        }
    }

    @Override
    public void showFaceView(List<FaceReceiveInfo> faceReceiveInfos) {
        if (faceReceiveInfos == null || faceReceiveInfos.size() <= 0) {
            return;
        }
        for (FaceReceiveInfo faceReceiveInfo : faceReceiveInfos) {
            int position = RoomDataManager.get().getMicPosition(faceReceiveInfo.getUid());
            if (position < -1) {
                continue;
            }
            ImageView imageView;
            if (position == -1) {
                imageView = null;
            } else {
                imageView = rvRoomMicro.getChildAt(position).findViewById(R.id.iv_room_micro_face);
            }
            if (imageView == null || getContext() == null) {
                continue;
            }
            AnimFaceFactory.asynLoadAnim(faceReceiveInfo, imageView, getContext(), faceWidth, faceWidth);
        }
    }

    @Override
    public void setOnMicroItemClickListener(OnMicroItemClickListener onMicroItemClickListener) {
        if (singleAudioConnMicroAdapter != null) {
            singleAudioConnMicroAdapter.setOnMicroItemClickListener(onMicroItemClickListener);
        }
    }

    public void setOnVisibilityChangeListener(OnVisibilityChangeListener listener) {
        this.onVisibilityChangeListener = listener;
    }

    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        if (null != onVisibilityChangeListener) {
            onVisibilityChangeListener.onVisibilityChanged(visibility);
        }
    }

    @Override
    public void release() {
        super.release();
        onVisibilityChangeListener = null;
    }

    public interface OnVisibilityChangeListener {
        void onVisibilityChanged(int visibility);
    }
}
