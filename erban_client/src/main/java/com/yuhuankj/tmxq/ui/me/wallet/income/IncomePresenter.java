package com.yuhuankj.tmxq.ui.me.wallet.income;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.result.WalletInfoResult;
import com.yuhuankj.tmxq.ui.me.UserMvpModel;
import com.yuhuankj.tmxq.ui.me.charge.presenter.PayPresenter;

import java.util.Map;

/**
 * Created by MadisonRong on 08/01/2018.
 */

public class IncomePresenter extends PayPresenter<IIncomeView> {

    private UserMvpModel userMvpModel;

    public IncomePresenter() {
        super();
        this.userMvpModel = new UserMvpModel();
    }

    public void loadWalletInfo() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("Cache-Control", "no-cache");

        OkHttpManager.getInstance().getRequest(UriProvider.getWalletInfo(), params, new OkHttpManager.MyCallBack<WalletInfoResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (getMvpView() != null) {
                    getMvpView().getUserWalletInfoFail(e.getMessage());
                }
            }

            @Override
            public void onResponse(WalletInfoResult response) {
                if (null != response && response.isSuccess()) {
                    if (getMvpView() != null && response.getData() != null) {
                        getMvpView().refreshUserWalletBalance(response.getData());
                    }
                } else {
                    if (getMvpView() != null && response != null) {
                        getMvpView().getUserWalletInfoFail(response.getErrorMessage());
                    }
                }
            }
        });
    }

    public void handleClick(int id) {
        getMvpView().handleClick(id);
    }

    public void hasBindPhone() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        OkHttpManager.getInstance().getRequest(UriProvider.isBindPhone(), params, new OkHttpManager.MyCallBack<ServiceResult<String>>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (getMvpView() != null) {
                    getMvpView().hasBindPhoneFail(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<String> response) {
                if (response != null && response.isSuccess()) {
                    if (getMvpView() != null) {
                        getMvpView().hasBindPhone();
                    }
                } else {
                    if (getMvpView() != null && response != null) {
                        getMvpView().hasBindPhoneFail(response.getErrorMessage());
                    }
                }
            }
        });
    }
}
