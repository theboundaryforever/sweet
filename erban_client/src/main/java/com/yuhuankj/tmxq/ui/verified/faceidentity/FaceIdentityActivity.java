package com.yuhuankj.tmxq.ui.verified.faceidentity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;

import com.megvii.meglive_sdk.listener.DetectCallback;
import com.megvii.meglive_sdk.listener.PreCallback;
import com.megvii.meglive_sdk.manager.MegLiveManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.xchat_core.UriProvider;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;

public class FaceIdentityActivity extends BaseActivity implements DetectCallback {
    private String name = "甜甜用户";
    private String IDCard = "";
    private String phoneNo = "";
    private String token = "";
    private MegLiveManager megLiveManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_face_identity);
        initView();
        initData();
    }

    private void initView() {

    }

    private void initData() {
        Intent intent = getIntent();
        if (intent == null || intent.getExtras() == null) {
            SingleToastUtil.showToast("没有获取到用户信息");
            finish();
            return;
        }
        Bundle bundle = intent.getExtras();
        IDCard = bundle.getString("IDCard");
        name = bundle.getString("name");
        token = bundle.getString("token");
        phoneNo = bundle.getString("phoneNo");
        if (TextUtils.isEmpty(IDCard) || TextUtils.isEmpty(name) || TextUtils.isEmpty(phoneNo) || TextUtils.isEmpty(token)) {
            SingleToastUtil.showToast("没有获取到用户信息");
            finish();
            return;
        }
        notityWebChange();
//        IDCard = "411021192006507178";
//        name = "我超甜";
//        phoneNo = "18565286852";
//        token = "18565286852";
        megLiveManager = MegLiveManager.getInstance();
        megLiveManager.preDetect(this, token, null, "https://api.megvii.com", new PreCallback() {
            @Override
            public void onPreStart() {

            }

            @Override
            public void onPreFinish(String token, int errorCode, String errorMessage) {
                if (errorCode == 1000) {
                    megLiveManager.setVerticalDetectionType(MegLiveManager.DETECT_VERITICAL_FRONT);
                    megLiveManager.startDetect(FaceIdentityActivity.this);
                } else {
                    LogUtils.e("onPreFinish : errorCode = " + errorCode + " errorMessage = " + errorMessage);
                    toast(errorMessage);
                    finish();
                }
            }
        });
    }

    @Override
    public void onDetectFinish(String token, int errorCode, String errorMessage, String data) {
        if (errorCode == 1000) {
            Intent intent = new Intent(this, FaceIdentityResultActivity.class);
            intent.putExtra("images", data);
            intent.putExtra("IDCard", IDCard);
            intent.putExtra("name", name);
            intent.putExtra("token", token);
            intent.putExtra("phoneNo", phoneNo);
            startActivity(intent);
        } else {
            toast("活体检测失败,请重试");
            LogUtils.e("onDetectFinish : errorCode = " + errorCode + " errorMessage = " + errorMessage);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }

    private void notityWebChange() {
        Intent intent = new Intent(CommonWebViewActivity.ACTION_BACK);
        intent.putExtra("url", UriProvider.getRealNameAuthUrl());
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

}
