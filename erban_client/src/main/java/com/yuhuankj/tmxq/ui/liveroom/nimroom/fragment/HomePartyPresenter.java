package com.yuhuankj.tmxq.ui.liveroom.nimroom.fragment;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.room.model.RoomBaseModel;

public class HomePartyPresenter extends AbstractMvpPresenter<HomePartView> {

    private final String TAG = HomePartyPresenter.class.getSimpleName();
    private RoomBaseModel roomBaseModel;

    public HomePartyPresenter() {
        roomBaseModel = new RoomBaseModel();
    }

    public void getCheckRoomAttention(long roomId){
        OkHttpManager.MyCallBack myCallBack = new OkHttpManager.MyCallBack<ServiceResult<Boolean>>() {
            @Override
            public void onError(Exception e) {
                LogUtils.d(TAG, "getCheckRoomAttention-onError");
                e.printStackTrace();
            }

            @Override
            public void onResponse(ServiceResult<Boolean> response) {
                LogUtils.d(TAG, "getMeetingData-onResponse");
                if(null != getMvpView()){
                    getMvpView().onGetRoomAttentionStatus(response.isSuccess(),response.getMessage(),response.getData());
                }
            }
        };
        roomBaseModel.checkRoomAttention(roomId, myCallBack);
    }

    public void doRoomAttention(long roomId){
        OkHttpManager.MyCallBack myCallBack = new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                LogUtils.d(TAG, "doRoomAttention-onError");
                e.printStackTrace();
                if(null != getMvpView()){
                    getMvpView().onRoomAttention(false,null);
                }
            }

            @Override
            public void onResponse(Json json) {
                LogUtils.d(TAG, "doRoomAttention-onResponse json:" + json);
                if(null != getMvpView()){
                    getMvpView().onRoomAttention(json.num("code") == 200,json.str("message"));
                }
            }
        };
        roomBaseModel.doRoomAttention(roomId, myCallBack);
    }

    public void doRoomCancelAttention(long roomId){
        OkHttpManager.MyCallBack myCallBack = new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                LogUtils.d(TAG, "doRoomCancelAttention-onError");
                e.printStackTrace();
                if(null != getMvpView()){
                    getMvpView().onRoomCancelAttention(false,null);
                }
            }

            @Override
            public void onResponse(Json json) {
                LogUtils.d(TAG, "doRoomCancelAttention-onResponse json:" + json);
                if(null != getMvpView()){
                    getMvpView().onRoomCancelAttention(json.num("code") == 200,json.str("message"));
                }
            }
        };
        roomBaseModel.doRoomCancelAttention(roomId, myCallBack);
    }
}
