package com.yuhuankj.tmxq.ui.me.shop;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.opensource.svgaplayer.SVGACallback;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tencent.bugly.crashreport.BuglyLog;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.codec.DESUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.base.fragment.BaseFragment;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.widget.ButtonItemFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.tongdaxing.erban.libcommon.utils.codec.DESUtils.giftCarSecret;
import static com.tongdaxing.xchat_core.UriProvider.JAVA_WEB_URL;

/**
 * Created by Administrator on 2018/5/7.
 */
public class ShopFragment extends BaseFragment {

    private final String TAG = ShopFragment.class.getSimpleName();

    private CarAdapter shopAdapter;
    private RecyclerView recyclerView;
    private SVGAImageView svgaImageview;
    private boolean showVgg = false;

    private String shortUrl;
    private String prizeName;
    private boolean isShowVgg;
    private String prizeParamsName = "";

    //个人页面跳进去，数量为空的时候才会有
    private long tagUid = 0;
    private String tagNick = "";

    public void setTagUid(long tagUid) {
        this.tagUid = tagUid;
    }

    public void setTagNick(String tagNick) {
        this.tagNick = tagNick;
    }

    public String getPrizeParamsName() {
        return prizeParamsName;
    }

    public void setPrizeParamsName(String prizeParamsName) {
        this.prizeParamsName = prizeParamsName;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public String getPrizeName() {
        return prizeName;
    }

    public void setPrizeName(String prizeName) {
        this.prizeName = prizeName;
    }

    public boolean isShowVgg() {
        return isShowVgg;
    }

    public void setShowVgg(boolean showVgg) {
        isShowVgg = showVgg;
    }

    @Override
    public void onFindViews() {
        recyclerView = mView.findViewById(R.id.rv_shop);
        svgaImageview = mView.findViewById(R.id.svga_imageview);
    }

    @Override
    public void onSetListener() {

    }

    @Override
    public void initiate() {

        svgaImageview.setVisibility(View.GONE);
        svgaImageview.setClearsAfterStop(true);
        svgaImageview.setLoops(1);
        svgaImageview.setCallback(new SVGACallback() {
            @Override
            public void onPause() {

            }

            @Override
            public void onFinished() {
                showVgg = false;
                svgaImageview.setVisibility(View.GONE);
                svgaImageview.stopAnimation();
            }

            @Override
            public void onRepeat() {

            }

            @Override
            public void onStep(int i, double v) {

            }
        });
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        shopAdapter = new CarAdapter(getContext());

        shopAdapter.setVggAction(new CarAdapter.VggAction() {
            @Override
            public void showVgg(String url) {
                try {
                    url = DESUtils.DESAndBase64Decrypt(url, giftCarSecret);
                    drawSvgaEffect(url);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        shopAdapter.setPayAction(new CarAdapter.PayAction() {
            @Override
            public void buyCar(int type, String carId, String carName, int nobleId) {
                if (isGiveTagUser()) {
                    showConfirmGiveDialog(tagUid + "", tagNick, carName, type, carId);
                } else {
                    showBottonMenu(type, carId, carName, nobleId);
                }
            }
        });
        shopAdapter.prizeParamasName = getPrizeParamsName();
        shopAdapter.isShowSvga = isShowVgg();
        recyclerView.setAdapter(shopAdapter);

        if (isShowVgg() && getDialogManager() != null) {
            getDialogManager().showProgressDialog(getContext(), getResources().getString(R.string.do_loading));
        }

        getData();
    }

    private void drawSvgaEffect(String url) throws MalformedURLException {
        if (showVgg || !isShowVgg()) {
            return;
        }
        BuglyLog.d(TAG, "drawSvgaEffect-url:" + url);
        showVgg = true;
        SVGAParser parser = new SVGAParser(getContext());
        parser.parse(new URL(url), new SVGAParser.ParseCompletion() {
            @Override
            public void onComplete(@NonNull SVGAVideoEntity videoItem) {
                SVGADrawable drawable = new SVGADrawable(videoItem);
                svgaImageview.setImageDrawable(drawable);
                svgaImageview.startAnimation();
                svgaImageview.setVisibility(View.VISIBLE);
                ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(svgaImageview, "alpha", 0.0F, 2.0F).setDuration(800);
                objectAnimator1.setInterpolator(new AccelerateDecelerateInterpolator());
                objectAnimator1.start();
            }

            @Override
            public void onError() {
                showVgg = false;
            }
        });
    }

    private void getData() {
        if (!isAdded() || getShortUrl() == null) {
            return;
        }
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        requestParam.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        requestParam.put("pageNum", "1");
        requestParam.put("pageSize", "500");

        OkHttpManager.getInstance().postRequest(JAVA_WEB_URL.concat(getShortUrl()).concat("/list"), requestParam, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (null != getActivity() && getDialogManager() != null) {
                    getDialogManager().dismissDialog();
                }
                toast(getResources().getString(R.string.network_error));
            }

            @Override
            public void onResponse(Json response) {
                if (null != getActivity() && getDialogManager() != null) {
                    getDialogManager().dismissDialog();
                }

                if (response.num("code") != 200) {
                    toast(response.str("message", getResources().getString(R.string.network_error)));
                    return;
                }

                List<Json> data = response.jlist("data");
                if (null != shopAdapter) {
                    shopAdapter.setNewData(data);
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (shopAdapter != null) {
            String selectId = shopAdapter.selectId;
            saveUserCar(selectId);
        }
    }

    private void showBottonMenu(int type, String carId, String carName, int nobleId) {
        List<ButtonItem> buttonItems = new ArrayList<>();
        ButtonItem buy = ButtonItemFactory.createMsgBlackListItem(getActivity().getString(R.string.buy_to_self), new ButtonItemFactory.OnItemClick() {
            @Override
            public void itemClick() {
                confirmBuy(carName, type, carId);

            }
        });
        buttonItems.add(buy);
        ButtonItem giveFriend = ButtonItemFactory.createMsgBlackListItem(getActivity().getString(R.string.buy_to_ta), new ButtonItemFactory.OnItemClick() {
            @Override
            public void itemClick() {
                if (nobleId > 0) {
                    if (isCar()) {
                        toast("贵族座驾不可以用于赠送");
                    } else {
                        toast("贵族头饰不可以用于赠送");
                    }
                    return;
                }
                Intent intent = new Intent(mContext, GiveGoodsActivity.class);
                intent.putExtra("carName", carName);
                intent.putExtra("goodsId", carId);

                intent.putExtra("type", isCar() ? GiveGoodsActivity.CAR : GiveGoodsActivity.HEADWEAR);
                mContext.startActivity(intent);
            }
        });
        buttonItems.add(giveFriend);
        getDialogManager().showCommonPopupDialog(buttonItems, getString(R.string.cancel));
    }


    private void showConfirmGiveDialog(String uid, String userName, String carName, int type, String carId) {
        String message = getResources().getString(R.string.give_sure_tips, getPrizeName(), carName, tagNick);
        getDialogManager().showOkCancelDialog(
                message, true,
                new DialogManager.AbsOkDialogListener() {
                    @Override
                    public void onOk() {
                        requestGiveGoods(uid, type, carId);
                    }
                });
    }


    private void confirmBuy(String carName, int type, String carId) {
        String message = getResources().getString(R.string.buy_shop_sure_tips, getPrizeName(), carName);
        getDialogManager().showOkCancelDialog(
                message, true,
                new DialogManager.AbsOkDialogListener() {
                    @Override
                    public void onOk() {
                        getDialogManager().showProgressDialog(getContext(), getResources().getString(R.string.network_loading));
                        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
                        String uid = CoreManager.getCore(IAuthCore.class).getCurrentUid() + "";
                        if (isGiveTagUser()) {
                            uid = tagUid + "";
                        }
                        requestParam.put("uid", uid);
                        requestParam.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
                        requestParam.put("type", type + "");
                        requestParam.put(getPrizeParamsName() + "Id", carId);

                        OkHttpManager.getInstance().postRequest(JAVA_WEB_URL.concat(getShortUrl()).concat("/purse"), requestParam, new OkHttpManager.MyCallBack<Json>() {
                            @Override
                            public void onError(Exception e) {
                                e.printStackTrace();
                                if (null != getActivity() && getDialogManager() != null) {
                                    getDialogManager().dismissDialog();
                                }
                                toast(getResources().getString(R.string.network_error));

                            }

                            @Override
                            public void onResponse(Json response) {
                                if (null != getActivity() && getDialogManager() != null) {
                                    getDialogManager().dismissDialog();
                                }
                                if (response.num("code") != 200) {
                                    toast(response.str("message", getResources().getString(R.string.network_error)));
                                    return;
                                }
                                toast(response.str("message", getResources().getString(R.string.buy_success)));
                                CoreManager.getCore(IUserCore.class).requestUserInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
                                getData();
                            }
                        });
                    }
                });
    }

    private void requestGiveGoods(String uid, int type, String goodsId) {
        if (TextUtils.isEmpty(uid) || TextUtils.isEmpty(goodsId)) {
            toast(getResources().getString(R.string.params_error));
            return;
        }

        Map<String, String> param = OkHttpManager.getDefaultParam();
        param.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        param.put("targetUid", uid);
        param.put("type", type + "");
        param.put(getPrizeParamsName() + "Id", goodsId);

        OkHttpManager.getInstance().getRequest(isCar() ? UriProvider.getCarGiveUrl()
                        : UriProvider.getHeadWearGiveUrl(),
                param, new OkHttpManager.MyCallBack<Json>() {
                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                        toast(R.string.network_error);
                    }

                    @Override
                    public void onResponse(Json json) {
                        if (json.num("code") == 200) {
                            toast(R.string.give_success);
                        } else {
                            toast(json.str("message", getResources().getString(R.string.network_error)));
                        }
                    }
                });
    }

    private boolean isCar() {
        return "car".equals(prizeParamsName);
    }

    public void saveUserCar(String selectId) {
        if (TextUtils.isEmpty(selectId)) {
            return;
        }

        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        requestParam.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        //如果为空的话传-1表明不选座驾
        requestParam.put(getPrizeParamsName() + "Id", TextUtils.isEmpty(selectId) ? "-1" : selectId);
        OkHttpManager.getInstance().postRequest(JAVA_WEB_URL.concat(getShortUrl()).concat("/use"),
                requestParam, new OkHttpManager.MyCallBack<Json>() {
                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                        toast(R.string.network_error);
                    }

                    @Override
                    public void onResponse(Json response) {
                        if (response.num("code") == 200) {
                            CoreManager.getCore(IUserCore.class).requestUserInfo(
                                    CoreManager.getCore(IAuthCore.class).getCurrentUid());
                        }
                    }
                });
    }

    private boolean isGiveTagUser() {
        return tagUid > 0;
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.shop_fragment;
    }
}
