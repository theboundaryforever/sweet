package com.yuhuankj.tmxq.ui.me.wallet.bills.adapter;

import android.graphics.Color;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.libcommon.utils.TimeUtils;
import com.tongdaxing.xchat_core.bills.bean.BillItemEntity;
import com.tongdaxing.xchat_core.bills.bean.IncomeInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

/**
 * Created by Seven on 2017/9/11.
 */

public class GiftIncomeAdapter extends BillBaseAdapter {

    public GiftIncomeAdapter(List<BillItemEntity> billItemEntityList) {
        super(billItemEntityList);
        addItemType(BillItemEntity.ITEM_NORMAL, R.layout.list_expend_gift_item);
    }


  /*  @Override
    public int getItemViewType(int position) {
        if (mDataList == null || mDataList.get(position).getItemType() == IncomeInfo.INCOME_TITLE) {
            return 0;
        } else {
            return 1;
        }
    }*/

    @Override
    public void convertNormal(BaseViewHolder baseViewHolder, BillItemEntity billItemEntity) {
        IncomeInfo incomeInfo = billItemEntity.mGiftInComeInfo;
        if (incomeInfo == null) return;
        baseViewHolder.setText(R.id.tv_gift_income, "+" + incomeInfo.getDiamondNum())
                .setText(R.id.tv_send_name, "送礼人 " + incomeInfo.getTargetNick())
                .setText(R.id.tv_user_name, incomeInfo.getGiftName())
                .setText(R.id.gift_date, TimeUtils.getDateTimeString(incomeInfo.getRecordTime(), "HH:mm:ss"))
                .setTextColor(R.id.tv_gift_income, Color.parseColor("#ff1c32"))
                .setImageResource(R.id.image, R.drawable.ic_jewek_small);
        ImageView avatar = baseViewHolder.getView(R.id.img_avatar);
        ImageLoadUtils.loadImage(mContext, incomeInfo.getGiftPic(), avatar);
    }
}
