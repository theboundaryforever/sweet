package com.yuhuankj.tmxq.ui.me.charge.presenter;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_core.result.WalletInfoResult;
import com.yuhuankj.tmxq.ui.me.charge.interfaces.IPayView;
import com.yuhuankj.tmxq.ui.me.charge.model.PayModel;

/**
 * Created by MadisonRong on 08/01/2018.
 */

public class PayPresenter<T extends IPayView> extends AbstractMvpPresenter<T> {

    protected PayModel payModel;
    protected WalletInfo walletInfo;

    public PayPresenter() {
        this.payModel = new PayModel();
    }

    public WalletInfo getWalletInfo() {
        return walletInfo;
    }

    /**
     * 刷新钱包信息
     */
    public void refreshWalletInfo(boolean force) {
        payModel.refreshWalletInfo(force, new OkHttpManager.MyCallBack<WalletInfoResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (getMvpView() != null) {
                    getMvpView().getUserWalletInfoFail(e.getMessage());
                }
            }

            @Override
            public void onResponse(WalletInfoResult response) {
                if (null != response && response.isSuccess() && response.getData() != null) {
                    walletInfo = response.getData();
                    if (getMvpView() != null) {
                        getMvpView().refreshUserWalletBalance(response.getData());
                    }
                } else {
                    if (getMvpView() != null && response != null) {
                        getMvpView().getUserWalletInfoFail(response.getErrorMessage());
                    }
                }
            }
        });
    }
}
