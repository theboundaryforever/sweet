package com.yuhuankj.tmxq.ui.home.game;

import android.text.TextUtils;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.thirdsdk.nim.MsgCenterRedPointStatusManager;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

/**
 * @author liaoxy
 * @Description:一起玩横向滚动数据adapter
 * @date 2019/2/15 10:11
 */
public class GameHomeIconAdapter extends BaseQuickAdapter<GameIconEnitity, BaseViewHolder> {

    private int imgWidthAndHeight = 0;
    private static boolean isShowCornor = true;

    public GameHomeIconAdapter(List<GameIconEnitity> data) {
        super(R.layout.item_game_home_icon, data);
        imgWidthAndHeight = DisplayUtility.dp2px(mContext, 58);
    }

    @Override
    protected void convert(BaseViewHolder helper, GameIconEnitity item) {
        ImageView imvIcon = helper.getView(R.id.imvIcon);
        ImageLoadUtils.loadBannerRoundBackground(mContext, ImageLoadUtils.toThumbnailUrl(imgWidthAndHeight, imgWidthAndHeight, item.getPic()), imvIcon);
        ImageView imvIconTip = helper.getView(R.id.imvIconTip);

        //签到红点
        helper.getView(R.id.vRedPoint).setVisibility(
                item.getTitle().equals(mContext.getResources().getString(R.string.main_icon_sign_on))
                        && MsgCenterRedPointStatusManager.getInstance().isShowSignInRedPoint()
                        ? View.VISIBLE : View.GONE);

        if (!TextUtils.isEmpty(item.getParams())) {
            try {
                Json json = Json.parse(item.getParams());
                int duration = json.getInt("duration");
                String url = json.getString("corner");
                if (!TextUtils.isEmpty(url) && isShowCornor) {
                    imvIconTip.setVisibility(View.VISIBLE);
                    ImageLoadUtils.loadImage(mContext, url, imvIconTip);
                    imvIconTip.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0f);
                            alphaAnimation.setDuration(3000);    //深浅动画持续时间
                            alphaAnimation.setFillAfter(true);   //动画结束时保持结束的画面
                            imvIconTip.startAnimation(alphaAnimation);
                            isShowCornor = false;
                            imvIconTip.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    imvIconTip.setVisibility(View.INVISIBLE);
                                }
                            }, 3000);
                        }
                    }, duration * 1000);
                } else {
                    imvIconTip.setVisibility(View.INVISIBLE);
                }
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }

        TextView tvName = helper.getView(R.id.tvName);
        tvName.setText(item.getTitle());
        helper.addOnClickListener(R.id.imvIcon);
        helper.addOnClickListener(R.id.tvName);
    }
}