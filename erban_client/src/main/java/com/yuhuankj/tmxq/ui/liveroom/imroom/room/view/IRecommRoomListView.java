package com.yuhuankj.tmxq.ui.liveroom.imroom.room.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.SingleAudioRoomEnitity;

import java.util.List;

public interface IRecommRoomListView extends IMvpBaseView {
    void refreshRecommRoomList(List<SingleAudioRoomEnitity> enitityList);

}
