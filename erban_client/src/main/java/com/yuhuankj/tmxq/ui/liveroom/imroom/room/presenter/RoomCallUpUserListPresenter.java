package com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomModel;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.ConveneUserInfo;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomEvent;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IConeveUserListView;

import java.util.ArrayList;
import java.util.List;

/**
 * <p> </p>
 *
 * @author jiahui
 * @date 2017/12/19
 */
public class RoomCallUpUserListPresenter extends AbstractMvpPresenter<IConeveUserListView> {

    private final String TAG = RoomCallUpUserListPresenter.class.getSimpleName();

    private IMRoomModel imRoomModel;

    private List<ConveneUserInfo> conveneUserInfoList = new ArrayList<>();

    public RoomCallUpUserListPresenter() {
        imRoomModel = new IMRoomModel();
    }

    public void getConveneUserList(int pageNum, int pageSize) {
        LogUtils.d(TAG, "getConveneUserList-pageNum:" + pageNum + " pageSize:" + pageSize);
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (null == roomInfo) {
            return;
        }
        imRoomModel.getConveneUserList(pageNum, pageSize, roomInfo.getRoomId(), new HttpRequestCallBack<List<ConveneUserInfo>>() {
            @Override
            public void onSuccess(String message, List<ConveneUserInfo> response) {
                LogUtils.d(TAG, "getConveneUserList-->onSuccess message:" + message);
                if (null != getMvpView()) {
                    getMvpView().refreshConeveUserList(response);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                LogUtils.d(TAG, "getConveneUserList-->onFailure code:" + code + " msg:" + msg);
                if (null != getMvpView()) {
                    getMvpView().showErrMsg(msg);
                }
            }
        });
    }

    public void cancelCallUp() {
        LogUtils.d(TAG, "cancelCallUp");
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (null == roomInfo) {
            return;
        }
        imRoomModel.cancelConvene(roomInfo.getRoomId(), new HttpRequestCallBack<String>() {
            @Override
            public void onSuccess(String message, String response) {
                LogUtils.d(TAG, "cancelCallUp-->onSuccess message:" + message);
                RoomDataManager.get().conveneState = 0;
                RoomDataManager.get().conveneUserCount = 0;
                IMRoomMessageManager.get().getIMRoomEventObservable().onNext(new IMRoomEvent()
                        .setEvent(IMRoomEvent.ROOM_CALL_UP_NUM_NOTIFY));
                if (null != getMvpView()) {
                    getMvpView().finishAfterCancelCallUp();
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                LogUtils.d(TAG, "cancelCallUp-->onFailure code:" + code + " msg:" + msg);
                if (null != getMvpView()) {
                    getMvpView().showErrMsg(msg);
                }
            }
        });
    }

}
