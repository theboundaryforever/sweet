package com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import com.jude.rollviewpager.hintview.ShapeHintView;
import com.yuhuankj.tmxq.R;

/**
 * @author weihaitao
 * @date 2019/5/21
 */
//TODO 参考com.yuhuankj.tmxq.widget.UserInfoColorPointHintView，结合GradientDrawable写一个共用类，将圆角、高宽、色值全部抽成api
public class RoomBannerShapeHintView extends ShapeHintView {

    public RoomBannerShapeHintView(Context context) {
        super(context);
    }

    public RoomBannerShapeHintView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public Drawable makeFocusDrawable() {
        if (null != getContext()) {
            return getContext().getResources().getDrawable(R.drawable.shape_room_banner_focus);
        }
        return null;
    }

    @Override
    public Drawable makeNormalDrawable() {
        if (null != getContext()) {
            return getContext().getResources().getDrawable(R.drawable.shape_room_banner_normal);
        }
        return null;
    }
}
