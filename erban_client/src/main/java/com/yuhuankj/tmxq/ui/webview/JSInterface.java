package com.yuhuankj.tmxq.ui.webview;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.megvii.meglive_sdk.listener.DetectCallback;
import com.megvii.meglive_sdk.listener.PreCallback;
import com.megvii.meglive_sdk.manager.MegLiveManager;
import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.session.activity.P2PMessageActivity;
import com.opensource.svgaplayer.SVGAImageView;
import com.tmxq.ndklib.JniUtils;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.http_image.util.DeviceUuidFactory;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ButtonUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.VersionUtil;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.utils.json.JsonParser;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.JsResponseInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.tongdaxing.xchat_core.manager.agora.AgoraEngineManager;
import com.tongdaxing.xchat_core.minigame.MiniGameModel;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.ui.MainActivity;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.me.charge.ChargeActivity;
import com.yuhuankj.tmxq.ui.me.noble.OrderConfirmActivity;
import com.yuhuankj.tmxq.ui.share.ShareDialog;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;
import com.yuhuankj.tmxq.ui.verified.faceidentity.FaceIdentityResultActivity;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * <p> html js 与webview 交互接口</p>
 * Created by ${user} on 2017/11/6.
 */
public class JSInterface {
    public static String TAG = JSInterface.class.getSimpleName();
    protected WebView mWebView;
    protected FragmentActivity mActivity;
    private int mPosition;
    private boolean isWebLoaded = false;
    private boolean isInterceptBack = false;

    public JSInterface() {

    }

    public JSInterface(WebView webView, FragmentActivity activity) {
        mWebView = webView;
        mActivity = activity;
    }

    /**
     * 动态跳转页面
     */
    @JavascriptInterface
    public void autoJump(String activity, String url, String params) {
        if (null != mActivity && mActivity instanceof BaseMvpActivity) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((BaseMvpActivity) mActivity).autoJump(activity, url, Json.parse(params));
                }
            });
        }
    }

    public void setPosition(int position) {
        mPosition = position;
    }

    /**
     * 调转个人主页
     *
     * @param uid 用户id
     */
    @JavascriptInterface
    public void openPersonPage(String uid) {
        LogUtils.d(TAG, "openPersonPage：" + uid);
        if (!TextUtils.isEmpty(uid)) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        long uidLong = Long.parseLong(uid);
                        UserInfoActivity.start(mActivity, uidLong);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    /**
     * 刷新背包礼物
     */
    @JavascriptInterface
    public void refreshGift() {
        CoreManager.getCore(IGiftCore.class).requestGiftInfos();
    }

    /**
     * 跳转充值页面
     */
    @JavascriptInterface
    public void openChargePage() {
        if (mActivity != null) {
            ChargeActivity.start(mActivity);
        }
    }


    @JavascriptInterface
    public void openSharePage() {
        if (mActivity != null) {
            ShareDialog shareDialog = new ShareDialog(mActivity);
            if (mActivity instanceof ShareDialog.OnShareDialogItemClick) {
                shareDialog.setOnShareDialogItemClick((ShareDialog.OnShareDialogItemClick) mActivity);
            }
            shareDialog.show();
        }
    }

    /**
     * 调转钱包页
     */
    @JavascriptInterface
    public void openPurse() {
        LogUtils.d(TAG, "openPurse：");
    }

    /**
     * 调转房间
     *
     * @param uid 房主uid
     */
    @Deprecated
    @JavascriptInterface
    public void openRoom(String json) {
        LogUtils.d(TAG, "openRoom json：" + json);
        if (!TextUtils.isEmpty(json)) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        //1.0.5.0版本开始，更改uid类型，由String-Long，改为String-json
                        Json jsonObj = new Json(json);
                        RoomServiceScheduler.getInstance().enterRoom(mActivity, jsonObj.num_l("roomUid"), jsonObj.num("roomType"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }


    /**
     * 获取用户ticket
     *
     * @return
     */
    @JavascriptInterface
    public String getTicket() {
        return CoreManager.getCore(IAuthCore.class).getTicket();
    }

    /**
     * 获取设备ID
     *
     * @return 设备id
     */
    @JavascriptInterface
    public String getDeviceId() {
        return DeviceUuidFactory.getDeviceId(BasicConfig.INSTANCE.getAppContext());
    }

    /**
     * 获取uid
     *
     * @return uid
     */
    @JavascriptInterface
    public String getUid() {
        return String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }

    @JavascriptInterface
    public String getPosition() {
        return String.valueOf(mPosition);
    }

    @JavascriptInterface
    public void loadSuc() {
        isWebLoaded = true;
    }

    /**
     * 控制用户返回行为
     *
     * @param isInterceptBack 是否拦截
     */
    @JavascriptInterface
    public void controlReturn(int isInterceptBack) {
        setInterceptBack(isInterceptBack == 1);
    }

    public boolean isInterceptBack() {
        return isInterceptBack;
    }

    public void setInterceptBack(boolean interceptBack) {
        isInterceptBack = interceptBack;
    }

    /**
     * 用户按返回
     */
    public void userReturn(WebView webView) {
        webView.evaluateJavascript("userReturn()", s -> {

        });
    }

    /*--------------------图片选择 start--------------------------*/

    /**
     * 请求调起拍照+相册选择器
     */
    @JavascriptInterface
    public void requestImageChooser() {
        if (mActivity != null && mActivity instanceof CommonWebViewActivity) {
            ((CommonWebViewActivity) mActivity).showImageChooser();
        }
    }

    /**
     * 通知H5图片选择结果
     */
    public void onImageChooserResult(String imageUrl) {
        mWebView.evaluateJavascript("onImageChooserResult('" + imageUrl + "')", value -> {

        });
    }


    @JavascriptInterface
    public void closeWin(String jsonStr) {
        LogUtils.d(TAG, "closeWin-jsonStr:" + jsonStr);
        if (mActivity != null && mActivity instanceof CommonWebViewActivity) {
            try {
                Json json = new Json(jsonStr);
                long roomId = json.getLong("roomid");
                getGameResult(roomId);
            } catch (Exception e) {
                e.printStackTrace();
                if (MainActivity.linkStartTime > 0) {
                    Bundle gotoP2pArg = new Bundle();
                    gotoP2pArg.putInt("linkMacro_status", P2PMessageActivity.AGREE_LINK_MACRO);
                    boolean isOpenMicro = AgoraEngineManager.get().isOpenMicro();
                    gotoP2pArg.putBoolean("isOpenMicro", isOpenMicro);
                    gotoP2pArg.putLong("linkStartTime", MainActivity.linkStartTime);
                    NimUIKit.startP2PSession(mActivity, MainActivity.linkMicroAccountId + "", gotoP2pArg);
                }
                ((CommonWebViewActivity) mActivity).close();
            }
        }
    }

    //获取游戏结果
    private void getGameResult(long roomId) {
        if (null == mActivity) {
            return;
        }

        DialogManager dialogManager = new DialogManager(mActivity);
        dialogManager.showProgressDialog(mActivity, "查询结果中...");
        new MiniGameModel().queryResult(roomId, new OkHttpManager.MyCallBack<String>() {
            @Override
            public void onError(Exception e) {
                dialogManager.dismissDialog();
                SingleToastUtil.showToast(e.getMessage());
                if (MainActivity.linkStartTime > 0) {
                    Bundle gotoP2pArg = new Bundle();
                    gotoP2pArg.putInt("linkMacro_status", P2PMessageActivity.AGREE_LINK_MACRO);
                    boolean isOpenMicro = AgoraEngineManager.get().isOpenMicro();
                    gotoP2pArg.putBoolean("isOpenMicro", isOpenMicro);
                    gotoP2pArg.putLong("linkStartTime", MainActivity.linkStartTime);
                    NimUIKit.startP2PSession(mActivity, MainActivity.linkMicroAccountId + "", gotoP2pArg);
                }
                if (null != mActivity && mActivity instanceof CommonWebViewActivity) {
                    ((CommonWebViewActivity) mActivity).close();
                }
            }

            @Override
            public void onResponse(String response) {
                dialogManager.dismissDialog();
                Bundle gotoP2pArg = getBundle(response);
                if (gotoP2pArg == null) {
                    if (MainActivity.linkStartTime > 0) {
                        gotoP2pArg = new Bundle();
                        gotoP2pArg.putInt("linkMacro_status", P2PMessageActivity.AGREE_LINK_MACRO);
                        boolean isOpenMicro = AgoraEngineManager.get().isOpenMicro();
                        gotoP2pArg.putBoolean("isOpenMicro", isOpenMicro);
                        gotoP2pArg.putLong("linkStartTime", MainActivity.linkStartTime);
                        NimUIKit.startP2PSession(mActivity, MainActivity.linkMicroAccountId + "", gotoP2pArg);
                    }
                } else {
                    String account = "";
                    if (MainActivity.linkStartTime > 0) {
                        gotoP2pArg.putInt("linkMacro_status", P2PMessageActivity.AGREE_LINK_MACRO);
                        boolean isOpenMicro = AgoraEngineManager.get().isOpenMicro();
                        gotoP2pArg.putBoolean("isOpenMicro", isOpenMicro);
                        gotoP2pArg.putLong("linkStartTime", MainActivity.linkStartTime);
                        account = MainActivity.linkMicroAccountId + "";
                    } else {
                        account = gotoP2pArg.getString("oppAccount");
                    }
                    if (!TextUtils.isEmpty(account) && null != mActivity) {
                        NimUIKit.startP2PSession(mActivity, account, gotoP2pArg);
                    }
                }
                if (null != mActivity && mActivity instanceof CommonWebViewActivity) {
                    ((CommonWebViewActivity) mActivity).close();
                }

            }
        });
    }

    //从游戏结果中获取跳转到私聊页的bundle
    private Bundle getBundle(String response) {
        Bundle gotoP2pArg = null;
        try {
            Json json = new Json(response);
            int code = json.getInt("code");
            if (code != 200) {
                SingleToastUtil.showToast("获取游戏结果失败");
                return null;
            }
            JSONObject jsonObject = json.getJSONObject("data");
            long winnerId = -1;
            if (jsonObject.has("winner") && !"null".equals(jsonObject.getString("winner"))) {
                winnerId = jsonObject.getLong("winner");
            }
            long inviterId = jsonObject.getLong("inviterId");
            long uid = CoreManager.getCore(IAuthCore.class).getCurrentAccount().getUid();
            UserInfo mUserInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(uid);
            if (mUserInfo == null) {
                SingleToastUtil.showToast("获取游戏结果失败");
                LogUtils.e(TAG, "获取用户资料失败");
                return null;
            }
            int result;
            if (winnerId == -1) {
                //和局
                result = -1;
            } else if (winnerId == uid) {
                //我是赢家
                result = 0;
            } else {
                //我是输家
                result = 1;
            }

            String fromAvatar;
            String fromNick;
            int oppWinCount;
            int ownerWinCount;
            String oppAccount;
            //如果我是邀请者
            if (inviterId == uid) {
                fromAvatar = jsonObject.getString("inviteeAvatar");
                fromNick = jsonObject.getString("inviteeNickname");
                oppWinCount = jsonObject.getInt("inviteeWincount");
                ownerWinCount = jsonObject.getInt("inviterWinCount");
                oppAccount = jsonObject.getString("inviteeId");
            } else {
                fromAvatar = jsonObject.getString("inviterAvatar");
                fromNick = jsonObject.getString("inviterNickname");
                oppWinCount = jsonObject.getInt("inviterWinCount");
                ownerWinCount = jsonObject.getInt("inviteeWincount");
                oppAccount = jsonObject.getString("inviterId");
            }

            gotoP2pArg = new Bundle();
            gotoP2pArg.putString("showAction", "showGamesResult");
            gotoP2pArg.putString("meAvatar", mUserInfo.getAvatar());
            gotoP2pArg.putString("meNick", mUserInfo.getNick());
            gotoP2pArg.putString("fromAvatar", fromAvatar);
            gotoP2pArg.putString("fromNick", fromNick);
            gotoP2pArg.putInt("result", result);
            gotoP2pArg.putString("gameImage", jsonObject.getString("gameBgImage"));
            gotoP2pArg.putString("gameName", jsonObject.getString("gameName"));
            gotoP2pArg.putInt("incScore", jsonObject.getInt("incScore"));
            gotoP2pArg.putInt("oppWinCount", oppWinCount);
            gotoP2pArg.putInt("ownerWinCount", ownerWinCount);
            gotoP2pArg.putString("oppAccount", oppAccount);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return gotoP2pArg;
    }

    /*--------------------图片选择 end--------------------------*/

    /*-----------------网络请求 start----------------------*/

    /**
     * 加密请求参数
     */
    @JavascriptInterface
    public String buildRequest(String request) {
        if (mActivity != null && !TextUtils.isEmpty(request)) {
            try {
                return JniUtils.encryptWebAes(mActivity, request.replaceAll("###uid###", getUid()).replaceAll("###ticket###", getTicket()));//加密需替换uid和ticket
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    /**
     * 解密返回参数
     */
    @JavascriptInterface
    public String extractResponse(String response) {
        if (mActivity != null && !TextUtils.isEmpty(response)) {
            try {
                return JniUtils.decryptWebAes(mActivity, response);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    private View vLoading;
    private ProgressBar pbLoading;
    private TextView tvProgress;
    private boolean isLoadingShowing = false;

    public boolean isLoadingShowing() {
        return isLoadingShowing;
    }

    public void setLoadingShowing(boolean isLoadingShowing) {
        this.isLoadingShowing = isLoadingShowing;
    }

    /**
     * 显示loading加载框
     */
    @JavascriptInterface
    public void showLoading(String desc) {
        if (mActivity == null) {
            LogUtils.e(TAG, "showLoading activity is null");
            return;
        }
        if (vLoading != null && isLoadingShowing) {
            LogUtils.e(TAG, "vLoading is showed");
            return;
        }
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (vLoading == null) {
                    vLoading = LayoutInflater.from(mActivity).inflate(R.layout.view_common_webview_minigame_loading, null);
                }

                //如果是测试环境，则增加点击直接隐藏
                if (BasicConfig.isDebug) {
                    vLoading.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            hideLoading();
                        }
                    });
                }

                SVGAImageView svgImvAnima = vLoading.findViewById(R.id.svgImvAnima);
                ImageView imvBack = vLoading.findViewById(R.id.imvBack);
                if (!TextUtils.isEmpty(desc)) {
                    TextView tvDesc = vLoading.findViewById(R.id.tvDesc);
                    tvDesc.setText(desc);
                }
                imvBack.setColorFilter(Color.WHITE);
                imvBack.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (ButtonUtils.isFastDoubleClick(v.getId())) {
                            return;
                        }
                        if (null == mActivity) {
                            return;
                        }
                        if (CommonWebViewActivity.isGameRunning && isLoadingShowing
                                && mActivity instanceof CommonWebViewActivity) {
                            ((CommonWebViewActivity) mActivity).getDialogManager().showOkCancelDialog(
                                    "主动退出会被处罚扣分喔", "是", "否", true, new DialogManager.AbsOkDialogListener() {
                                @Override
                                public void onOk() {
                                    mActivity.finish();
                                }
                            });
                            return;
                        }
                        mActivity.onBackPressed();
                    }
                });
                pbLoading = vLoading.findViewById(R.id.pbLoading);
                tvProgress = vLoading.findViewById(R.id.tvProgress);
                Glide.with(mActivity).asGif().load(R.drawable.anima_minigame_loading).into(svgImvAnima);
                ViewGroup decorView = (ViewGroup) mActivity.getWindow().getDecorView();
                if (vLoading.getParent() != null) {
                    ((ViewGroup) vLoading.getParent()).removeView(vLoading);
                }
                decorView.addView(vLoading);
                isLoadingShowing = true;
                if (null != mActivity && mActivity instanceof CommonWebViewActivity) {
                    CommonWebViewActivity commonWebViewActivity = (CommonWebViewActivity) mActivity;
                    commonWebViewActivity.changeGameDisplay();
                    commonWebViewActivity.initImmersionBar(false);
                    commonWebViewActivity.startCheckGameLoadTime();
                }

            }
        });
    }

    @JavascriptInterface
    public void setProgress(int progress) {
        if (mActivity == null) {
            LogUtils.e(TAG, "setProgress activity is null");
            return;
        }
        if (pbLoading == null) {
            LogUtils.e(TAG, "showLoading pbLoading is null");
            return;
        }
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pbLoading.setProgress(progress);
                tvProgress.setText(progress + "%");
            }
        });
    }

    /**
     * 隐藏loading
     */
    @JavascriptInterface
    public void hideLoading() {
        isLoadingShowing = false;
        if (mActivity == null) {
            LogUtils.e(TAG, "hideLoading activity is null");
            return;
        }
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (vLoading != null && vLoading.getParent() != null) {
                    ((ViewGroup) vLoading.getParent()).removeView(vLoading);
                }
                if (null != mActivity && mActivity instanceof CommonWebViewActivity) {
                    ((CommonWebViewActivity) mActivity).initImmersionBar(true);
                }
            }
        });
    }

    @JavascriptInterface
    public void closeOrOpenMicro(boolean isOpen) {
        if (mActivity == null) {
            LogUtils.e(TAG, "closeOrOpenMicro activity is null");
            return;
        }
        int result = AgoraEngineManager.get().closeOrOpenMacro(isOpen);
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int value = (result == 0) ? 1 : 0;
                mWebView.evaluateJavascript("closeOrOpenMicroStatus('" + value + "')", s -> {

                });
            }
        });
    }

    /*-------------------用户信息 start---------------------------*/

    @JavascriptInterface
    public String getUserPhoneNumber() {
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo != null && !TextUtils.isEmpty(userInfo.getPhone()) && !userInfo.getPhone().equals(String.valueOf(userInfo.getErbanNo()))) {//手机号为用户id 也是属于未绑定手机号
            return userInfo.getPhone();
        }
        return "";
    }

    /*-------------------用户信息 end---------------------------*/

    /*-----------------网络请求 start----------------------*/

    /**
     * 发起网络请求
     */
    @JavascriptInterface
    public void httpRequest(int requestMethod, String urlController, String headerMapString, String paramMapString) {
        Map<String, String> header;
        Map<String, String> params;
        try {
            header = mapObejctsToMapStrings(JsonParser.toMap(headerMapString));
            params = mapObejctsToMapStrings(JsonParser.toMap(paramMapString));
        } catch (Exception e) {
            e.printStackTrace();
            onHttpResponse(urlController, true, "", e.getMessage());
            return;
        }
        if (params == null) {
            params = new HashMap<>();
        }
        params = CommonParamUtil.getDefaultParam(params);
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        String url = UriProvider.JAVA_WEB_URL.concat(urlController);
        if (requestMethod == 1) {
            OkHttpManager.getInstance().postRequest(url, header, params, new OkHttpManager.MyCallBack<String>() {
                @Override
                public void onError(Exception e) {
                    onHttpResponse(urlController, true, "", e.getMessage());
                }

                @Override
                public void onResponse(String response) {
                    onHttpResponse(urlController, false, response, "");
                }
            });
        } else {
            OkHttpManager.getInstance().getRequest(url, header, params, new OkHttpManager.MyCallBack<String>() {
                @Override
                public void onError(Exception e) {
                    onHttpResponse(urlController, true, "", e.getMessage());
                }

                @Override
                public void onResponse(String response) {
                    onHttpResponse(urlController, false, response, "");
                }
            });
        }
    }

    private Map<String, String> mapObejctsToMapStrings(Map<String, Object> mapObjects) {
        Map<String, String> mapStrings = new HashMap<>();
        if (mapObjects != null) {
            for (Map.Entry<String, Object> mapObject : mapObjects.entrySet()) {
                mapStrings.put(mapObject.getKey(), mapObject.getValue().toString());
            }
        }
        return mapStrings;
    }

    /**
     * 通知H5请求结果
     */
    public void onHttpResponse(String urlController, boolean isRequestError, String bodyString, String errorMsg) {
        String responseStr;
        JsResponseInfo responseInfo = new JsResponseInfo();
        responseInfo.setUrlController(urlController);
        responseInfo.setRequestError(isRequestError);
        responseInfo.setBodyString(bodyString);
        responseInfo.setErrorMsg(errorMsg);
        responseStr = JsonParser.toJson(responseInfo);
        mWebView.evaluateJavascript("onHttpResponse(" + responseStr + ")", value -> {

        });
    }

    /*-----------------网络请求 end----------------------*/

    private long lastClickToOrderConfirmTime = 0L;

    /*-----------------贵族------------------------*/
    @JavascriptInterface
    public void openNobility(String jsonData) {
        LogUtils.d(TAG, "openNobility-jsonData:" + jsonData);
        Json json = null;

        try {
            json = new Json(jsonData);
            //{"nbLevelName":"xxx"，"time":xxx， "coins":xxx，"reward":xxx， "type":xxx}
            String nbLevelName = null;
            int timeDays = 0;
            int coins = 0;
            String reward = null;
            int type = 0;
            int vipId = 0;
            if (json.has("nbLevelName")) {
                nbLevelName = json.str("nbLevelName");
            }
            if (json.has("time")) {
                timeDays = json.num("time");
            }
            if (json.has("coins")) {
                coins = json.num("coins");
            }
            if (json.has("reward")) {
                reward = json.str("reward");
            }
            if (json.has("type")) {
                type = json.num("type");
            }
            if (json.has("vipId")) {
                vipId = json.num("vipId");
            }
            if (null != mActivity) {
                if (System.currentTimeMillis() - lastClickToOrderConfirmTime < 1000L) {
                    LogUtils.d(TAG, "openNobility-过滤多次点击");
                    return;
                }
                lastClickToOrderConfirmTime = System.currentTimeMillis();
                if (mActivity instanceof CommonWebViewActivity) {
                    CommonWebViewActivity commonWebViewActivity = (CommonWebViewActivity) mActivity;
                    commonWebViewActivity.isBackFromOrderConfirmView = true;
                    OrderConfirmActivity.startFromH5(commonWebViewActivity, vipId, nbLevelName, timeDays, coins, reward, type, commonWebViewActivity.roomId);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @JavascriptInterface
    public String getAppName() {
        //用于前端H5页面获取当前app名称
        return BasicConfig.INSTANCE.getAppContext().getResources().getString(R.string.app_name);
    }


    //用于前端H5页面获取当前version code名称
    @JavascriptInterface
    public int getVersionCode() {
        return VersionUtil.getVersionCode(BasicConfig.INSTANCE.getAppContext());
    }

    //用于前端H5页面获取当前version code名称
    @JavascriptInterface
    public long getCurrRoomId() {
        RoomInfo roomInfo = BaseRoomServiceScheduler.getCurrentRoomInfo();
        long roomId = null == roomInfo ? 0L : roomInfo.getRoomId();
        LogUtils.d(TAG, "getCurrRoomId-roomId:" + roomId);
        return roomId;
    }

    //用于前端获取常用的基本参数
    @JavascriptInterface
    public JSONObject getCommmParams() {
        return CommonParamUtil.getCommmParams();
    }

    //用于前端关闭当前界面
    @JavascriptInterface
    public void finishActivity() {
        LogUtils.d(TAG, "finishActivity");
        if (mActivity == null) {
            return;
        }
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (null != mActivity) {
                    mActivity.finish();
                }
            }
        });

    }

    //实名认证
    @JavascriptInterface
    public void startFaceIdentity(String jsonStr) {
        if (mActivity == null) {
            return;
        }
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String IDCard, name, token, phoneNo;
                try {
                    Json json = new Json(jsonStr);
                    IDCard = json.str("IDCard");
                    name = json.str("name");
                    token = json.str("token");
                    phoneNo = json.str("phoneNo");
                } catch (Exception e) {
                    e.printStackTrace();
                    SingleToastUtil.showToast("没有获取到用户信息");
                    return;
                }
                if (TextUtils.isEmpty(IDCard)) {
                    SingleToastUtil.showToast("没有获取到用户信息");
                    return;
                }
                MegLiveManager megLiveManager = MegLiveManager.getInstance();
                megLiveManager.preDetect(mActivity, token, null, "https://api.megvii.com", new PreCallback() {
                    @Override
                    public void onPreStart() {
                        if (null != mActivity && mActivity instanceof BaseMvpActivity) {
                            ((BaseMvpActivity) mActivity).getDialogManager().showProgressDialog(mActivity, "请稍候...");
                        }
                    }

                    @Override
                    public void onPreFinish(String token, int errorCode, String errorMessage) {
                        if (null != mActivity && mActivity instanceof BaseMvpActivity) {
                            ((BaseMvpActivity) mActivity).getDialogManager().dismissDialog();
                        }

                        if (errorCode == 1000) {
                            if (mWebView.canGoBack()) {
                                mWebView.goBack();
                            }
                            megLiveManager.setVerticalDetectionType(MegLiveManager.DETECT_VERITICAL_FRONT);
                            megLiveManager.startDetect(new DetectCallback() {
                                @Override
                                public void onDetectFinish(String token, int code, String errorMessage, String data) {
                                    if (code == 1000) {
                                        Intent intent = new Intent(mActivity, FaceIdentityResultActivity.class);
                                        intent.putExtra("images", data);
                                        intent.putExtra("IDCard", IDCard);
                                        intent.putExtra("name", name);
                                        intent.putExtra("token", token);
                                        intent.putExtra("phoneNo", phoneNo);
                                        mActivity.startActivity(intent);
                                    } else {
                                        SingleToastUtil.showToast("活体检测失败,请重试");
                                        LogUtils.e("onDetectFinish : errorCode = " + errorCode + " errorMessage = " + errorMessage);
                                    }
                                }
                            });
                        } else {
                            LogUtils.e("onPreFinish : errorCode = " + errorCode + " errorMessage = " + errorMessage);
                            SingleToastUtil.showToast(errorMessage);
                        }
                    }
                });
            }
        });

    }
}
