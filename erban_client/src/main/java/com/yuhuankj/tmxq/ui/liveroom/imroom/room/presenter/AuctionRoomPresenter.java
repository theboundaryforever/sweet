package com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter;

import android.content.Context;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.JavaUtil;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomModel;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMember;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomQueueInfo;
import com.tongdaxing.xchat_core.result.RoomConsumeInfoListResult;
import com.tongdaxing.xchat_core.room.auction.AuctionUserBean;
import com.tongdaxing.xchat_core.room.auction.ParticipateAuctionBean;
import com.tongdaxing.xchat_core.room.auction.ParticipateAuctionListBean;
import com.tongdaxing.xchat_core.room.auction.RoomAuctionBean;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.base.presenter.BaseRoomDetailPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.model.AuctionModel;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IAuctionRoomView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.IMButtonItemFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 文件描述：竞拍房 P层
 *
 * @auther：zwk
 * @data：2019/7/26
 */
public class AuctionRoomPresenter extends BaseRoomDetailPresenter<IAuctionRoomView> {

    public RoomAuctionBean getAuction() {
        return RoomDataManager.get().getAuction();
    }


    /**
     * 竞拍房麦位事件处理：主持和拍卖位
     *
     * @param mContext
     * @param roomQueueInfo
     * @param micPosition
     */
    public void onAuctionMicroClickEvent(Context mContext, IMRoomQueueInfo roomQueueInfo, int micPosition) {
        if (mContext == null) {
            return;
        }
        if (micPosition == RoomDataManager.MIC_POSITION_BY_OWNER) {//房主位
            IMRoomMember imRoomMember = null;
            if (roomQueueInfo == null || roomQueueInfo.mChatRoomMember == null) {
                //房主的话，创建一下默认信息
                imRoomMember = getRoomOwnerDefaultMemberInfo();
            } else {
                imRoomMember = roomQueueInfo.mChatRoomMember;
            }
            if (imRoomMember == null)
                return;
            long account = imRoomMember.getLongAccount();
            if (isRoomOwnerByMyself()) {//我是房主
                if (getMvpView() != null) {
                    getMvpView().openUserInfoActivity(JavaUtil.str2long(imRoomMember.getAccount()));
                }
            } else {//我是管理员或者普通用户
                List<ButtonItem> buttonItems = new ArrayList<>();
                buttonItems.add(new ButtonItem("送礼物",
                        () -> {
                            if (getMvpView() != null) {
                                getMvpView().showGiftDialogView(account, true);
                            }
                        }));
                buttonItems.add(new ButtonItem("查看资料",
                        () -> {
                            if (getMvpView() != null) {
                                getMvpView().openUserInfoActivity(account);
                            }
                        }));
                if (getMvpView() != null) {
                    getMvpView().showCommonPopupDialog(buttonItems, R.string.cancel);
                }
            }
        } else {//主持位和拍卖位
            if (roomQueueInfo != null && roomQueueInfo.mChatRoomMember != null) {//麦位不是空
                //是自己
                if (Objects.equals(String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()), roomQueueInfo.mChatRoomMember.getAccount())) {
                    List<ButtonItem> buttonItems = new ArrayList<>();
                    buttonItems.add(new ButtonItem("查看资料",
                            () -> {
                                if (getMvpView() != null && roomQueueInfo.mChatRoomMember != null) {
                                    getMvpView().showUserInfoDialogView(Long.valueOf(roomQueueInfo.mChatRoomMember.getAccount()));
                                }
                            }));
                    buttonItems.add(IMButtonItemFactory.createDownMicItem());
                    //下麦旁听
                    //禁麦/解麦操作
                    if (isRoomAdminByMyself() && roomQueueInfo.mRoomMicInfo != null) {
                        buttonItems.add(roomQueueInfo.mRoomMicInfo.isMicMute() ?
                                IMButtonItemFactory.createFreeMicItem(micPosition,
                                        () -> operateMicroOpenOrClose(micPosition, true)) :
                                IMButtonItemFactory.createLockMicItem(micPosition,
                                        () -> operateMicroOpenOrClose(micPosition, false)));
                    }
                    if (getMvpView() != null) {
                        getMvpView().showCommonPopupDialog(buttonItems, R.string.cancel);
                    }
                } else {
                    boolean isTargetRoomAdmin = isRoomAdmin(Long.valueOf(roomQueueInfo.mChatRoomMember.getAccount()));
                    boolean isTargetRoomOwner = isRoomOwner(Long.valueOf(roomQueueInfo.mChatRoomMember.getAccount()));
                    if (isRoomOwnerByMyself()) {//我自己是房主
                        List<ButtonItem> buttonItems = new ArrayList<>();
                        buttonItems.add(new ButtonItem("送礼物",
                                () -> {
                                    if (getMvpView() != null && roomQueueInfo.mChatRoomMember != null) {
                                        getMvpView().showGiftDialogView(Long.valueOf(roomQueueInfo.mChatRoomMember.getAccount()), true);
                                    }
                                }));
                        buttonItems.add(new ButtonItem("查看资料",
                                () -> {
                                    if (getMvpView() != null && roomQueueInfo.mChatRoomMember != null) {
                                        getMvpView().openUserInfoActivity(Long.valueOf(roomQueueInfo.mChatRoomMember.getAccount()));
                                    }
                                }));
                        buttonItems.add(IMButtonItemFactory.createKickDownMicItem(roomQueueInfo.mChatRoomMember.getAccount()));
                        //禁麦/解麦操作
                        if (roomQueueInfo.mRoomMicInfo != null) {
                            buttonItems.add(roomQueueInfo.mRoomMicInfo.isMicMute() ?
                                    IMButtonItemFactory.createFreeMicItem(micPosition,
                                            () -> operateMicroOpenOrClose(micPosition, true)) :
                                    IMButtonItemFactory.createLockMicItem(micPosition,
                                            () -> operateMicroOpenOrClose(micPosition, false)));
                        }
                        buttonItems.add(IMButtonItemFactory.createKickOutRoomItem(mContext, roomQueueInfo.mChatRoomMember,
                                String.valueOf(getCurrRoomInfo().getRoomId()), roomQueueInfo.mChatRoomMember.getAccount()));
                        buttonItems.add(IMButtonItemFactory
                                .createMarkManagerListItem(String.valueOf(getCurrRoomInfo().getRoomId()),
                                        roomQueueInfo.mChatRoomMember.getAccount(), !isTargetRoomAdmin, roomQueueInfo.mChatRoomMember));
                        buttonItems.add(IMButtonItemFactory.createMarkBlackListItem(mContext, roomQueueInfo.mChatRoomMember,
                                String.valueOf(getCurrRoomInfo().getRoomId())));
                        if (getMvpView() != null) {
                            getMvpView().showCommonPopupDialog(buttonItems, R.string.cancel);
                        }
                    } else if (isRoomAdminByMyself()) {//我自己是管理员
                        List<ButtonItem> buttonItems = new ArrayList<>();
                        buttonItems.add(new ButtonItem("送礼物",
                                () -> {
                                    if (getMvpView() != null && roomQueueInfo.mChatRoomMember != null) {
                                        getMvpView().showGiftDialogView(Long.valueOf(roomQueueInfo.mChatRoomMember.getAccount()), true);
                                    }
                                }));
                        buttonItems.add(new ButtonItem("查看资料",
                                () -> {
                                    if (getMvpView() != null && roomQueueInfo.mChatRoomMember != null) {
                                        getMvpView().openUserInfoActivity(Long.valueOf(roomQueueInfo.mChatRoomMember.getAccount()));
                                    }
                                }));
                        //他非房主或管理员
                        if (!isTargetRoomAdmin && !isTargetRoomOwner) {
                            //抱他下麦
                            buttonItems.add(IMButtonItemFactory.createKickDownMicItem(roomQueueInfo.mChatRoomMember.getAccount()));
                            //禁麦/解麦操作
                            if (roomQueueInfo.mRoomMicInfo != null) {
                                buttonItems.add(roomQueueInfo.mRoomMicInfo.isMicMute() ?
                                        IMButtonItemFactory.createFreeMicItem(micPosition,
                                                () -> operateMicroOpenOrClose(micPosition, true)) :
                                        IMButtonItemFactory.createLockMicItem(micPosition,
                                                () -> operateMicroOpenOrClose(micPosition, false)));
                            }
                            //踢出房间
                            buttonItems.add(IMButtonItemFactory.createKickOutRoomItem(mContext, roomQueueInfo.mChatRoomMember,
                                    String.valueOf(getCurrRoomInfo().getRoomId()), roomQueueInfo.mChatRoomMember.getAccount()));
                            //加入黑名单
                            buttonItems.add(IMButtonItemFactory.createMarkBlackListItem(mContext, roomQueueInfo.mChatRoomMember,
                                    String.valueOf(getCurrRoomInfo().getRoomId())));
                        } else {
                            if (roomQueueInfo.mRoomMicInfo != null && !roomQueueInfo.mRoomMicInfo.isMicMute() && !isTargetRoomOwner) {
                                buttonItems.add(IMButtonItemFactory.createLockMicItem(micPosition,
                                        () -> operateMicroOpenOrClose(micPosition, false)));
                            }
                            if (!isTargetRoomOwner) {
                                buttonItems.add(IMButtonItemFactory.createKickDownMicItem(roomQueueInfo.mChatRoomMember.getAccount()));
                            }
                            if (roomQueueInfo.mRoomMicInfo != null && roomQueueInfo.mRoomMicInfo.isMicMute()) {
                                // 对于管理员和房主,只有解麦功能
                                buttonItems.add(IMButtonItemFactory.createFreeMicItem(micPosition,
                                        () -> operateMicroOpenOrClose(micPosition, true)));
                            }
                        }
                        if (getMvpView() != null && !ListUtils.isListEmpty(buttonItems)) {
                            getMvpView().showCommonPopupDialog(buttonItems, R.string.cancel);
                        }
                    } else {//游客
                        List<ButtonItem> buttonItems = new ArrayList<>();
                        buttonItems.add(new ButtonItem("送礼物",
                                () -> {
                                    if (getMvpView() != null && roomQueueInfo.mChatRoomMember != null) {
                                        getMvpView().showGiftDialogView(Long.valueOf(roomQueueInfo.mChatRoomMember.getAccount()), true);
                                    }
                                }));
                        buttonItems.add(new ButtonItem("查看资料",
                                () -> {
                                    if (getMvpView() != null && roomQueueInfo.mChatRoomMember != null) {
                                        getMvpView().openUserInfoActivity(Long.valueOf(roomQueueInfo.mChatRoomMember.getAccount()));
                                    }
                                }));
                        if (getMvpView() != null && !ListUtils.isListEmpty(buttonItems)) {
                            getMvpView().showCommonPopupDialog(buttonItems, R.string.cancel);
                        }
//                        if (getMvpView() != null && roomQueueInfo.mChatRoomMember != null) {
//                            getMvpView().showGiftDialogView(Long.valueOf(roomQueueInfo.mChatRoomMember.getAccount()), true);
//                        }
                    }
                }
            } else {//空的主持位或者普通位置
                if (isRoomOwnerByMyself()) {
                    List<ButtonItem> buttonItems = new ArrayList<>(4);
                    buttonItems.add(new ButtonItem(mContext.getString(R.string.embrace_up_mic),
                            () -> {
                                if (getMvpView() != null) {
                                    if (micPosition == RoomDataManager.AUCTION_ROOM_HOST_POSITION) {
                                        getMvpView().openRoomInviteActivity(micPosition);
                                    } else {
                                        if (RoomDataManager.get().getCurrentAuctionUid() <= 0) {
                                            getMvpView().openRoomInviteActivity(micPosition);
                                        } else {
//                                            if (RoomDataManager.get().myselfIsCurrentAution()) {
//                                                getMvpView().openRoomInviteActivity(micPosition);
//                                            } else {
                                                getMvpView().toast("当前有用户在拍卖中...");
//                                            }
                                        }
                                    }
                                }
                            }));
                    if (roomQueueInfo != null) {
                        buttonItems.add(new ButtonItem(
                                roomQueueInfo.mRoomMicInfo.isMicMute() ? mContext.getString(R.string.no_forbid_mic)
                                        : mContext.getString(R.string.forbid_mic), () -> {
                            operateMicroOpenOrClose(micPosition,
                                    roomQueueInfo.mRoomMicInfo.isMicMute());
                        }));
                        buttonItems.add(
                                new ButtonItem(roomQueueInfo.mRoomMicInfo.isMicLock() ? mContext.getString(R.string.unlock_mic) :
                                        mContext.getString(R.string.lock_mic), () -> operateMicroLockOrUnLock(micPosition,
                                        !roomQueueInfo.mRoomMicInfo.isMicLock())));
                    }
                    if (getMvpView() != null) {
                        getMvpView().showCommonPopupDialog(buttonItems, R.string.cancel);
                    }
                } else if (isRoomAdminByMyself()) {
                    if (micPosition == RoomDataManager.AUCTION_ROOM_HOST_POSITION) {
                        //多人房间，则保持点击空白麦位自动上麦
                        operateUpMicro(micPosition, String.valueOf(getCurrentUserId()), false);
                    } else {
                        List<ButtonItem> buttonItems = new ArrayList<>(4);
                        buttonItems.add(new ButtonItem(mContext.getString(R.string.embrace_up_mic),
                                () -> {
                                    if (getMvpView() != null) {
                                        if (RoomDataManager.get().getCurrentAuctionUid() <= 0) {
                                            getMvpView().openRoomInviteActivity(micPosition);
                                        } else {
                                            getMvpView().toast("当前有用户在拍卖中...");
                                        }
                                    }
                                }));
                        if (roomQueueInfo != null) {
                            buttonItems.add(new ButtonItem(
                                    roomQueueInfo.mRoomMicInfo.isMicMute() ? mContext.getString(R.string.no_forbid_mic)
                                            : mContext.getString(R.string.forbid_mic), () -> {
                                operateMicroOpenOrClose(micPosition,
                                        roomQueueInfo.mRoomMicInfo.isMicMute());
                            }));
                            buttonItems.add(
                                    new ButtonItem(roomQueueInfo.mRoomMicInfo.isMicLock() ? mContext.getString(R.string.unlock_mic) :
                                            mContext.getString(R.string.lock_mic), () -> operateMicroLockOrUnLock(micPosition,
                                            !roomQueueInfo.mRoomMicInfo.isMicLock())));
                        }
                        buttonItems.add(new ButtonItem("移到此座位",
                                () -> {
//                                    operateUpMicro(micPosition, String.valueOf(getCurrentUserId()), false);
                                    if (getMvpView() != null) {
                                        if (RoomDataManager.get().getCurrentAuctionUid() <= 0) {
                                            getMvpView().showAuctionSettingDialog(getCurrentUserId(), false);
                                        } else {
                                            if (RoomDataManager.get().myselfIsCurrentAution()) {
                                                operateUpMicro(micPosition, String.valueOf(getCurrentUserId()), false);
                                            } else {
                                                getMvpView().toast("当前有用户在拍卖中...");
                                            }
                                        }
                                    }
                                }));
                        if (getMvpView() != null) {
                            getMvpView().showCommonPopupDialog(buttonItems, R.string.cancel);
                        }
                    }
                } else {
                    if (getMvpView() != null) {
                        if (micPosition == RoomDataManager.AUCTION_ROOM_HOST_POSITION) {
                            getMvpView().toast("只有房主和管理员才能操作主持位哦~");
                        } else {
                            if (RoomDataManager.get().myselfIsCurrentAution()) {
                                operateUpMicro(micPosition, String.valueOf(getCurrentUserId()), false);
                            } else {
                                getMvpView().showAuctionSortDialog();
                            }
                        }
                    }
                }
            }
        }
    }


    /**
     * 竞拍位点击事件处理
     *
     * @param auctionUserBean
     */
    public void onAuctionUserClickEvent(AuctionUserBean auctionUserBean) {
        if (auctionUserBean == null) {//空的竞拍位
//            getMvpView().showGiftDialogView(auctionUserBean.getUid(), true);
        } else {
            if (RoomDataManager.get().isUserSelf(auctionUserBean.getUid())) {
                if (getMvpView() != null) {
                    getMvpView().openUserInfoActivity(auctionUserBean.getUid());
                }
            } else {
                List<ButtonItem> buttonItems = new ArrayList<>();
                buttonItems.add(new ButtonItem("送礼物",
                        () -> {
                            if (getMvpView() != null) {
                                getMvpView().showGiftDialogView(auctionUserBean.getUid(), true);
                            }
                        }));
                buttonItems.add(new ButtonItem("查看资料",
                        () -> {
                            if (getMvpView() != null) {
                                getMvpView().openUserInfoActivity(auctionUserBean.getUid());
                            }
                        }));
                if (getMvpView() != null) {
                    getMvpView().showCommonPopupDialog(buttonItems, R.string.cancel);
                }
            }
        }
    }

    /**
     * 房间获取排行榜数据
     *
     * @param roomUid
     * @param roomType
     */
    public void getFortuneRankTopData(long roomUid, int roomType) {
        OkHttpManager.MyCallBack<RoomConsumeInfoListResult> myCallBack =
                new OkHttpManager.MyCallBack<RoomConsumeInfoListResult>() {
                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                        if (null != getMvpView()) {
                            getMvpView().onGetFortuneRankTop(false, null, null);
                        }
                    }

                    @Override
                    public void onResponse(RoomConsumeInfoListResult result) {
                        if (null != getMvpView()) {
                            getMvpView().onGetFortuneRankTop(result.isSuccess(), result.getMessage(), result.getData());
                        }
                    }
                };
        new IMRoomModel().getRoomConsumeList(roomUid, roomType, 1, 1, myCallBack);
    }


    /**
     * 开启竞拍
     *
     * @param roomId
     * @param auctionUid
     */
    public void startRoomAuction(long roomId, long auctionUid) {
        new AuctionModel().startRoomAuction(roomId, auctionUid, new CallBack<ParticipateAuctionBean>() {
            @Override
            public void onSuccess(ParticipateAuctionBean data) {
                if (RoomDataManager.get().getAuction() == null) {
                    RoomAuctionBean roomAuctionBean = new RoomAuctionBean();
                    RoomDataManager.get().setAuction(roomAuctionBean);
                }
                RoomDataManager.get().getAuction().setRoomId(data.getRoomId());
                RoomDataManager.get().getAuction().setUid(data.getUid());
                RoomDataManager.get().getAuction().setProject(data.getProject());
                RoomDataManager.get().getAuction().setDay(data.getDay());
                if (getMvpView() != null) {
                    getMvpView().onUpdateAuctionOperateBtn();
                }
            }

            @Override
            public void onFail(int code, String error) {
                if (getMvpView() != null) {
                    getMvpView().toast(error);
                }
            }
        });
    }

    /**
     * 结束竞拍
     *
     * @param roomId
     * @param auctionUid
     */
    public void endRoomAuction(long roomId, long auctionUid) {
        new AuctionModel().endRoomAuction(roomId, auctionUid, new CallBack<ParticipateAuctionBean>() {
            @Override
            public void onSuccess(ParticipateAuctionBean data) {
                //以后端为准
                RoomDataManager.get().setAuction(null);
                if (getMvpView() != null) {
                    getMvpView().onUpdateAuctionOperateBtn();
                }
                if (data != null) {
                    int micPosition = RoomDataManager.get().getMicPosition(data.getUid());
                    IMRoomMessageManager.get().kickMicroPhoneBySdk(
                            micPosition, data.getUid(), data.getRoomId());
                }
            }

            @Override
            public void onFail(int code, String error) {
                if (getMvpView() != null) {
                    getMvpView().toast(error);
                }
            }
        });
    }

    /**
     * 开始拍卖、结束拍卖以及拍下TA按钮的点击事件处理
     */
    public void onAuctionBtnEvent() {
        if (getCurrRoomInfo() == null) {
            return;
        }
        if (getAuction() != null && getAuction().getUid() > 0) {//当前有竞拍进行中
            if (isRoomOwnerByMyself() || isRoomAdminByMyself()) {//房主和管理员 -- 关闭
                endRoomAuction(getCurrRoomInfo().getRoomId(), getAuction().getUid());
            } else {//普通用户 — 送礼
                if (getMvpView() != null) {
                    getMvpView().showGiftDialogView(getAuction().getUid(), true);
                }
            }
        } else {//没有竞拍
            if (isRoomOwnerByMyself() || isRoomAdminByMyself()) {//房主和管理员 -- 关闭
                IMRoomQueueInfo roomQueueInfo = RoomDataManager.get().getRoomQueueMemberInfoByMicPosition(1);
                if (roomQueueInfo == null || roomQueueInfo.mChatRoomMember == null) {
                    if (getMvpView() != null) {
                        getMvpView().openRoomInviteActivity(1);
                    }
                } else {
                    startRoomAuction(getCurrRoomInfo().getRoomId(), JavaUtil.str2long(roomQueueInfo.mChatRoomMember.getAccount()));
                }
            }
        }
    }


    /**
     * 获取参与竞拍用户列表
     *
     * @param roomId
     */
    public void getRoomAuctionList(long roomId, long uid, boolean isInvite) {
        new AuctionModel().getRoomAuctionList(roomId, uid, new CallBack<ParticipateAuctionListBean>() {
            @Override
            public void onSuccess(ParticipateAuctionListBean data) {
                if (getMvpView() != null) {
                    if (data != null) {
                        getMvpView().isAdminJoinAuction(data.isJoin(), uid, isInvite);
                    }
                }
            }

            @Override
            public void onFail(int code, String error) {

            }
        });
    }

    /**
     * 邀请竞拍上麦
     *
     * @param micPosition   上的麦位
     * @param account       上麦的人
     * @param isInviteUpMic 上麦成功后是否自动开麦
     */
    public void operateUpMicro(int micPosition, long account, boolean isInviteUpMic) {
        IMRoomMessageManager.get().inviteMicroPhoneBySdk(account, micPosition);
    }
}
