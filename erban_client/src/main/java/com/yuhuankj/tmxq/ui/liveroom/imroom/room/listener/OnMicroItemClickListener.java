package com.yuhuankj.tmxq.ui.liveroom.imroom.room.listener;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMember;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomQueueInfo;


public abstract class OnMicroItemClickListener {

    public void onAvatarBtnClick(int position, @Nullable IMRoomMember chatRoomMember) {

    }

    public abstract void onUpMicBtnClick(int position);

    public void onDownMicBtnClick(int position, @NonNull IMRoomMember chatRoomMember) {

    }

    public void onNoteGiftClick(@NonNull IMRoomMember chatRoomMember) {

    }

    public void onLockBtnClick(int position) {

    }

    public void onAvatarSendMsgClick(int position) {

    }

    public void onGiftBtnClick(@NonNull IMRoomMember chatRoomMember) {

    }

    public void onMicroBtnClickListener(IMRoomQueueInfo roomQueueInfo, int micPosition) {

    }
}
