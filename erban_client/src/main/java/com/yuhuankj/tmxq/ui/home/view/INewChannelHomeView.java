package com.yuhuankj.tmxq.ui.home.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.GameRoomEnitity;
import com.yuhuankj.tmxq.ui.home.model.NewChannelHomeResponse;

import java.util.List;

/**
 * @author weihaitao
 * @date 2019/5/21
 */
public interface INewChannelHomeView extends IMvpBaseView {

    /**
     * 刷新_new市场首页数据,对应下拉刷新操作
     * @param response
     */
    void refreshOpscHomeData(NewChannelHomeResponse response);

    /**
     * 显示首页接口错误状态
     *
     * @param message
     */
    void showOpscHomeDataErr(String message);

    /**
     * 底部聊天室列表添加新数据,对应上拉加载更多操作,如果上拉调用接口出错，无需提示用户，因此也直接采用这个回调
     * @param canLoadMore 是否可继续上拉加载更多
     * @param listRoom 数据列表
     */
    void showNewRoomList(boolean canLoadMore, List<GameRoomEnitity> listRoom);
}
