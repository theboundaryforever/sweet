package com.yuhuankj.tmxq.ui.room.adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.juxiao.library_utils.DisplayUtils;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.yuhuankj.tmxq.R;

import java.util.List;

/**
 * @author liaoxy
 * @Description:交友房间列表顶部tag的adapter
 * @date 2019/3/5 11:45
 */
public class RoomTagTypeAdapter extends BaseQuickAdapter<TabInfo, BaseViewHolder> {

    private int curPosition = 0;//当前选择的位置

    public RoomTagTypeAdapter(List<TabInfo> data) {
        super(R.layout.item_room_tag_type, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, TabInfo item) {
        if (helper.itemView != null) {
            RecyclerView rcvContent = getRecyclerView();
            int allWidth = 0;
            if (rcvContent != null && rcvContent.getWidth() > 0) {
                allWidth = rcvContent.getWidth();
            } else {
                allWidth = DisplayUtils.getScreenWidth(mContext);
            }
            ViewGroup.LayoutParams layoutParams = helper.itemView.getLayoutParams();
            layoutParams.width = allWidth / 5;
            helper.itemView.requestLayout();
        }
        ImageView imvTagIcon = helper.getView(R.id.imvTagIcon);
        TextView tvTagName = helper.getView(R.id.tvTagName);

        try {
            if (helper.getAdapterPosition() == curPosition) {
                tvTagName.setTextColor(Color.parseColor("#5A5A5A"));
                GlideApp.with(mContext).load(item.getDescription()).dontAnimate().placeholder(imvTagIcon.getDrawable()).into(imvTagIcon);
            } else {
                tvTagName.setTextColor(Color.parseColor("#BABABA"));
                GlideApp.with(mContext).load(item.getPict()).dontAnimate().placeholder(imvTagIcon.getDrawable()).into(imvTagIcon);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        tvTagName.setText(item.getName());
    }

    public void setCurPosition(int curPosition) {
        this.curPosition = curPosition;
        notifyDataSetChanged();
    }
}