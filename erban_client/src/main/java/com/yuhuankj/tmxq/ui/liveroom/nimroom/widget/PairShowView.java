package com.yuhuankj.tmxq.ui.liveroom.nimroom.widget;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.opensource.svgaplayer.SVGACallback;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tencent.bugly.crashreport.BuglyLog;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.net.MalformedURLException;
import java.net.URL;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PairShowView extends RelativeLayout implements SVGACallback {

    private final String TAG = PairShowView.class.getSimpleName();

    @BindView(R.id.svga_pair)
    SVGAImageView svga;
    @BindView(R.id.iv_pair_user_icon1)
    ImageView ivPairUserIcon1;
    @BindView(R.id.tv_pair_user_nick1)
    TextView tvPairUserNick1;
    @BindView(R.id.iv_pair_user_icon2)
    ImageView ivPairUserIcon2;
    @BindView(R.id.tv_pair_user_nick2)
    TextView tvPairUserNick2;
    @BindView(R.id.ll_pair_user_info)
    LinearLayout userInfoBg;
    private Context context;
    private Json pairInfo;

    String svgaUrl = "https://img.pinjin88.com/room_pair.svga";

    public PairShowView(Context context) {
        this(context, null);
    }


    public PairShowView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        View inflate = View.inflate(context, R.layout.pair_show_view, this);
        ButterKnife.bind(this, inflate);
        userInfoBg = inflate.findViewById(R.id.ll_pair_user_info);
        svga = inflate.findViewById(R.id.svga_pair);


        //动态调节高度，参数随便调的
        int screenWidth = ScreenUtil.getScreenWidth(context);
        int top = (int) (screenWidth * 0.37f);
        LayoutParams bgLayoutParams = (LayoutParams) userInfoBg.getLayoutParams();
        bgLayoutParams.setMargins(0, top * 2, 0, 0);
        userInfoBg.setLayoutParams(bgLayoutParams);
        LayoutParams svgaLayoutParams = (LayoutParams) svga.getLayoutParams();
        svgaLayoutParams.height = screenWidth;
        svgaLayoutParams.setMargins(0, top, 0, 0);


        //初始化svga控件
        svga.setLayoutParams(svgaLayoutParams);
        svga.setLoops(1);
        svga.setCallback(this);
        svga.setClearsAfterStop(true);
    }

    private int showPairIndex = 0;

    public void onPairEnd(Json json) {
        if (json == null || json.key_names().length == 0) {
            return;
        }

        showPairIndex = 0;
        //设置重复播放几次
        this.pairInfo = json;
        svga.setLoops(pairInfo.key_names().length);

        setPairUserInfo();
        try {
            drawSvga(svgaUrl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据PairImpl->checkPair解析数据
     */
    private void setPairUserInfo() {
        if (showPairIndex >= pairInfo.key_names().length) {
            return;
        }

        Json pairUser = pairInfo.json_ok(pairInfo.key_names()[showPairIndex++]);
        if (pairUser.key_names().length != 2) {
            return;
        }
        Json user1 = pairUser.json_ok(pairUser.key_names()[0]);
        Json user2 = pairUser.json_ok(pairUser.key_names()[1]);
        tvPairUserNick1.setText(user1.str("nick"));
        tvPairUserNick2.setText(user2.str("nick"));
        ImageLoadUtils.loadCircleImage(context, user1.str("avatar"), ivPairUserIcon1, R.drawable.default_user_head);
        ImageLoadUtils.loadCircleImage(context, user2.str("avatar"), ivPairUserIcon2, R.drawable.default_user_head);

    }


    public void drawSvga(String url) throws MalformedURLException {
        setVisibility(VISIBLE);
        SVGAParser parser = new SVGAParser(getContext());
        BuglyLog.d(TAG, "drawSvga-url:" + url);
        parser.parse(new URL(url), new SVGAParser.ParseCompletion() {
            @Override
            public void onComplete(@NonNull SVGAVideoEntity videoItem) {
                SVGADrawable drawable = new SVGADrawable(videoItem);
                svga.setImageDrawable(drawable);
                svga.startAnimation();
                ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(userInfoBg, "alpha", 0.0F, 1.0F).setDuration(800);
                objectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
                objectAnimator.start();
                userInfoBg.setVisibility(VISIBLE);

            }

            @Override
            public void onError() {

            }
        });

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onFinished() {
        setVisibility(GONE);
        userInfoBg.setVisibility(GONE);
    }

    @Override
    public void onRepeat() {
        setPairUserInfo();
    }

    @Override
    public void onStep(int i, double v) {

    }
}
