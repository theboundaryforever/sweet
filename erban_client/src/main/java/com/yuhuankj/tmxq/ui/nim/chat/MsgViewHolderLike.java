package com.yuhuankj.tmxq.ui.nim.chat;

import android.graphics.Color;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.netease.nim.uikit.common.ui.recyclerview.adapter.BaseMultiItemFetchLoadAdapter;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.netease.nim.uikit.session.viewholder.MsgViewHolderBase;

/**
 * @author liaoxy
 * @Description:偶遇点赞
 * @date 2019/4/17 18:32
 */
public class MsgViewHolderLike extends MsgViewHolderBase {

    protected TextView bodyTextView;
    private View llGameLinkMicro;

    public MsgViewHolderLike(BaseMultiItemFetchLoadAdapter adapter) {
        super(adapter);
    }

    @Override
    protected int getContentResId() {
        return com.netease.nim.uikit.R.layout.nim_message_item_text_icon;
    }

    @Override
    protected void inflateContentView() {
        bodyTextView = findViewById(com.netease.nim.uikit.R.id.nim_message_item_text_body);
        llGameLinkMicro = findViewById(com.netease.nim.uikit.R.id.llGameLinkMicro);
    }

    @Override
    protected void bindContentView() {
        contentContainer.setBackgroundColor(Color.TRANSPARENT);
        if (isReceivedMessage()) {
            llGameLinkMicro.setBackgroundResource(com.netease.nim.uikit.R.drawable.nim_message_content_left_bg);
            llGameLinkMicro.setPadding(ScreenUtil.dip2px(10), 0, ScreenUtil.dip2px(10), 0);
        } else {
            llGameLinkMicro.setBackgroundResource(com.netease.nim.uikit.R.drawable.nim_message_content_right_bg);
            llGameLinkMicro.setPadding(ScreenUtil.dip2px(10), 0, ScreenUtil.dip2px(10), 0);
        }

        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) bodyTextView.getLayoutParams();
        layoutParams.weight = LinearLayout.LayoutParams.WRAP_CONTENT;
        layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        bodyTextView.setLayoutParams(layoutParams);
        bodyTextView.setMinHeight(0);
    }
}
