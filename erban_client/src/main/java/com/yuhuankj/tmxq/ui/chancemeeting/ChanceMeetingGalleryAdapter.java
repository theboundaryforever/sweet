package com.yuhuankj.tmxq.ui.chancemeeting;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.xchat_core.utils.StarUtils;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.match.bean.MicroMatch;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.widget.RoundImageView;

import java.util.Date;

/**
 * Created by chenran on 2017/10/3.
 */

public class ChanceMeetingGalleryAdapter extends BaseQuickAdapter<MicroMatch, ChanceMeetingGalleryAdapter.ViewHolder> {

    private Context context;


    @Override
    protected void convert(ViewHolder helper, final MicroMatch microMatch) {
        if (null == lastSelectedMicroMatch) {
            lastSelectedMicroMatch = getData().get(lastSelectedIndex);
        }
        String nick = microMatch.getNick();
        if (StringUtils.isNotEmpty(nick)) {
            if (nick.length() > 8) {
                nick = nick.substring(0, 8);
                helper.tv_nick.setText(context.getResources().getString(R.string.nick_length_max_six, nick));
            } else {
                helper.tv_nick.setText(nick);
            }
        }else {
            helper.tv_nick.setText("");
        }
        int age = StarUtils.getAge(new Date(microMatch.getBirth()));

        helper.tv_age.setText(String.valueOf(age));
        helper.tv_report.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (null != onChanceMeetingClickListener && lastSelectedMicroMatch.getUid() == microMatch.getUid()) {
                    onChanceMeetingClickListener.onReportUser(microMatch);
                }

            }
        });
        helper.iv_playStatus.setVisibility(!TextUtils.isEmpty(microMatch.getUserVoice()) && microMatch.getVoiceDura() > 0 ? View.VISIBLE : View.GONE);
        final boolean playStatus = lastPlayAudioMeetingUserId == microMatch.getUid();
        helper.iv_playStatus.setOnClickListener(!TextUtils.isEmpty(microMatch.getUserVoice()) ? new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onChanceMeetingClickListener && lastSelectedMicroMatch.getUid() == microMatch.getUid()) {
                    onChanceMeetingClickListener.onPlayOrPauseUserAudio(microMatch, !playStatus);
                }
            }
        } : null);

        if (lastPlayAudioMeetingUserId == microMatch.getUid()) {
            if (itemUpdateType == ItemUpdateType.updatePlayStatus.ordinal()) {
                helper.iv_playStatus.setImageDrawable(context.getResources().getDrawable(playProgress >= 100 ?
                        R.mipmap.icon_chance_meeting_audio_play : R.mipmap.icon_chance_meeting_audio_pause));

                helper.iv_playStatus.setOnClickListener(!TextUtils.isEmpty(microMatch.getUserVoice()) ? new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (null != onChanceMeetingClickListener && lastSelectedMicroMatch.getUid() == microMatch.getUid()) {
                            onChanceMeetingClickListener.onPlayOrPauseUserAudio(microMatch, playProgress >= 100);
                        }
                    }
                } : null);
            } else if (itemUpdateType == ItemUpdateType.updatePauseStatus.ordinal()) {
                helper.iv_playStatus.setImageDrawable(context.getResources().getDrawable(R.mipmap.icon_chance_meeting_audio_play));
                helper.iv_playStatus.setOnClickListener(!TextUtils.isEmpty(microMatch.getUserVoice()) ? new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (null != onChanceMeetingClickListener && lastSelectedMicroMatch.getUid() == microMatch.getUid()) {
                            onChanceMeetingClickListener.onPlayOrPauseUserAudio(microMatch, true);
                        }

                    }
                } : null);
            }
        } else {
            helper.iv_playStatus.setImageDrawable(context.getResources().getDrawable(R.mipmap.icon_chance_meeting_audio_play));
        }

        helper.iv_sex.setImageDrawable(helper.iv_sex.getContext().getResources().getDrawable(
                microMatch.getGender() == 1 ? R.mipmap.icon_sex_man : R.mipmap.icon_sex_woman));
        helper.ll_sex.setBackground(helper.ll_sex.getContext().getResources().getDrawable(
                microMatch.getGender() == 1 ? R.drawable.bg_chance_meeting_card_info_male : R.drawable.bg_chance_meeting_card_info_woman));

        String star = StarUtils.getConstellation(new Date(microMatch.getBirth()));
        if (null == star) {
            helper.tv_const.setVisibility(View.GONE);
        } else {
            helper.tv_const.setText(star);
            helper.tv_const.setVisibility(View.VISIBLE);
        }

        helper.ll_chanceMeeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onChanceMeetingClickListener && lastSelectedMicroMatch.getUid() == microMatch.getUid()) {
                    onChanceMeetingClickListener.jumpUserInfoActivity(microMatch);
                }
            }
        });
        ViewGroup.LayoutParams lp = helper.ll_chanceMeeting.getLayoutParams();
        lp.width = screenWidth * 3 / 4;
        lp.height = lp.width * 400 / 282;
        helper.ll_chanceMeeting.setLayoutParams(lp);

        ViewGroup.LayoutParams lp1 = helper.rivMeetingHead.getLayoutParams();
        lp1.width = lp.width;
        lp1.height = lp1.width;
        helper.rivMeetingHead.setLayoutParams(lp1);

        if (GlideContextCheckUtil.checkContextUsable(context)) {
            if (microMatch.getBackgroundStatus() == 1 && !TextUtils.isEmpty(microMatch.getBackground())) {
                GlideApp.with(context)
                        .load(ImageLoadUtils.toThumbnailUrl(lp1.width, lp1.height, microMatch.getBackground()))
                        .dontAnimate()
                        .into(helper.rivMeetingHead);
            } else {
                GlideApp.with(context)
                        .load(ImageLoadUtils.toThumbnailUrl(lp1.width, lp1.height, microMatch.getAvatar()))
                        .dontAnimate()
                        .into(helper.rivMeetingHead);
            }
        }

        helper.tvVoice.setText(microMatch.getTimbre());
        helper.tvVoice.setVisibility(TextUtils.isEmpty(microMatch.getTimbre()) ? View.GONE : View.VISIBLE);

        helper.tvFindTA.setVisibility(microMatch.getInRoomUid() > 0L ? View.VISIBLE : View.GONE);

        helper.tvFindTA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onChanceMeetingClickListener && lastSelectedMicroMatch.getUid() == microMatch.getUid()) {
                    onChanceMeetingClickListener.findRoomTaIn(microMatch.getInRoomUid(), microMatch.getInRoomType());
                }
            }
        });
    }

    public void setItemUpdateType(int itemUpdateType) {
        this.itemUpdateType = itemUpdateType;
    }

    private int itemUpdateType = -1;

    public void setLastSelectedIndex(int lastSelectedIndex) {
        this.lastSelectedIndex = lastSelectedIndex;
    }

    private int lastSelectedIndex = 0;

    public void setLastSelectedMicroMatch(MicroMatch microMatch) {
        this.lastSelectedMicroMatch = microMatch;
    }

    private MicroMatch lastSelectedMicroMatch = null;
    private int screenWidth = 0;

    public ChanceMeetingGalleryAdapter(Context context) {
        super(R.layout.item_chance_meeting);
        this.context = context;
        screenWidth = DisplayUtility.getScreenWidth(context);
    }

    public enum ItemUpdateType {
        /**
         * 未知操作
         */
        unknow,
        updatePlayStatus,
        updatePauseStatus,
    }

    private OnChanceMeetingClickListener onChanceMeetingClickListener;

    public void setOnChanceMeetingClickListener(OnChanceMeetingClickListener onItemClick) {
        this.onChanceMeetingClickListener = onItemClick;
    }

    public void setLastPlayAudioMeetingUserId(long lastPlayAudioMeetingUserId) {
        this.lastPlayAudioMeetingUserId = lastPlayAudioMeetingUserId;
    }

    public void updateAudioPlayInfo(String playTime, int playProgress) {
        this.playProgress = playProgress;
        this.playTime = playTime;
    }

    private String playTime = "";
    private int playProgress = 0;

    private long lastPlayAudioMeetingUserId;

    public interface OnChanceMeetingClickListener {
        /**
         * 举报用户
         *
         * @param microMatch
         */
        void onReportUser(MicroMatch microMatch);

        /**
         * 跳转到个人资料页面
         *
         * @param microMatch
         */
        void jumpUserInfoActivity(MicroMatch microMatch);

        /**
         * 播放或者暂停播放匹配用户的录音
         *
         * @param microMatch
         */
        void onPlayOrPauseUserAudio(MicroMatch microMatch, boolean playOrPause);

        void findRoomTaIn(long roomUid, int roomType);
    }

    static class ViewHolder extends BaseViewHolder {

        public TextView tv_report;
        public TextView tv_nick;
        public TextView tv_age;
        public TextView tv_const;
        public TextView tvFindTA;
        public TextView tvVoice;

        public View ll_sex;
        public View ll_chanceMeeting;
        public RoundImageView rivMeetingHead;
        public ImageView iv_sex;
        public ImageView iv_playStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            tvFindTA = itemView.findViewById(R.id.tvFindTA);
            tvVoice = itemView.findViewById(R.id.tvVoice);
            tv_report = itemView.findViewById(R.id.tv_report);
            rivMeetingHead = itemView.findViewById(R.id.rivMeetingHead);
            tv_nick = itemView.findViewById(R.id.tv_nick);
            ll_sex = itemView.findViewById(R.id.ll_sex);
            iv_sex = itemView.findViewById(R.id.iv_sex);
            iv_playStatus = itemView.findViewById(R.id.iv_playStatus);
            tv_age = itemView.findViewById(R.id.tv_age);
            tv_const = itemView.findViewById(R.id.tv_const);
            ll_chanceMeeting = itemView.findViewById(R.id.ll_chanceMeeting);
        }
    }
}
