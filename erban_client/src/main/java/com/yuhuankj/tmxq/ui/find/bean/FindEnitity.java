package com.yuhuankj.tmxq.ui.find.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 发现页model层
 */
public class FindEnitity implements Serializable {

    private String openTime;//	string     开放时间
    private int userMatchState;//	string 交友速配状态（1:开发中 0:暂未开放）
    private ArrayList<FindHotRoomEnitity> radioRoomList = new ArrayList<>();

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public int getUserMatchState() {
        return userMatchState;
    }

    public void setUserMatchState(int userMatchState) {
        this.userMatchState = userMatchState;
    }

    public ArrayList<FindHotRoomEnitity> getRadioRoomList() {
        return radioRoomList;
    }

    public void setRadioRoomList(ArrayList<FindHotRoomEnitity> radioRoomList) {
        this.radioRoomList = radioRoomList;
    }
}
