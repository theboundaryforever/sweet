package com.yuhuankj.tmxq.ui.signAward.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.yuhuankj.tmxq.ui.signAward.model.SignAwardResult;

/**
 * @author weihaitao
 * @date 2019/5/21
 */
public interface ISignAwardDaysView extends IMvpBaseView {

    /**
     * 显示萌新大礼包领取结果弹框
     */
    void showResultAfterSignSuc(SignAwardResult result);

    /**
     * 提示签到失败
     *
     * @param message
     */
    void showSignErr(String message);
}
