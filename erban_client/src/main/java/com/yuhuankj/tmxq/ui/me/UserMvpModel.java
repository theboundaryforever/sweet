package com.yuhuankj.tmxq.ui.me;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.RxNet;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by MadisonRong on 04/01/2018.
 */

public class UserMvpModel extends BaseMvpModel {

    private UserService userService;

    public UserMvpModel() {
        this.userService = RxNet.create(UserService.class);
    }

    public UserService getUserService() {
        return userService;
    }

    public UserInfo getUserDate() {
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentAccount().getUid();
        return CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(uid);
    }

    public interface UserService {

        /**
         * 用户是否已经绑定手机
         * @param uid 用户 id
         */
        @GET("/user/isBindPhone")
        Observable<ServiceResult<String>> isBindPhone(@Query("uid") String uid);
    }
}
