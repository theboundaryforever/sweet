package com.yuhuankj.tmxq.ui.webview;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.netease.nimlib.sdk.media.record.IAudioRecordCallback;
import com.netease.nimlib.sdk.media.record.RecordType;
import com.tongdaxing.erban.libcommon.audio.OnPlayListener;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.file.IFileCore;
import com.tongdaxing.xchat_core.file.IFileCoreClient;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.base.permission.PermissionActivity;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;

import java.io.File;
import java.util.Map;

/**
 * 上传声鉴卡录音webview
 */
@CreatePresenter(VoiceAuthCardPresenter.class)
public class VoiceAuthCardWebViewActivity extends CommonWebViewActivity<IVoiceAuthCardView, VoiceAuthCardPresenter>
        implements View.OnClickListener, IVoiceAuthCardView {

    private UserRecordVoiceManager userRecordVoiceManager;
    DialogManager.OkCancelDialogListener okCancelDialogListener = okCancelDialogListener = new DialogManager.OkCancelDialogListener() {
        @Override
        public void onCancel() {
        }

        @Override
        public void onOk() {
            getMvpPresenter().exitRoom(null);
        }
    };
    private boolean hasUserUpFinger = false;
    private long lastRecordStartTime = 0L;
    private long lastRecordDuration = 0L;

    private UserInfo userInfo;

    private FrameLayout flRoot;
    private FrameLayout flTitle;
    private View mPreView;
    private TextView tvTips1;
    private TextView tvTips2;
    private TextView tvAnalyProgress;
    private ImageView ivUploadAuthVoice;
    private View llAnalyTips;

    private VoiceAuthCardJSInterface voiceAuthCardJSInterface;
    private IAudioRecordCallback onRecordCallback = new IAudioRecordCallback() {
        @Override
        public void onRecordReady() {
            LogUtils.d(TAG, "onRecordReady");

        }

        @Override
        public void onRecordStart(File audioFile, RecordType recordType) {
            LogUtils.d(TAG, "onRecordStart-recordType:" + recordType);

        }

        @Override
        public void onRecordSuccess(File audioFile, long audioLength, RecordType recordType) {
            LogUtils.d(TAG, "onRecordSuccess-audioLength:" + audioLength + " recordType:" + recordType);

        }

        @Override
        public void onRecordFail() {
            LogUtils.d(TAG, "onRecordFail");
            if (null != voiceAuthCardJSInterface) {
                voiceAuthCardJSInterface.recordFailed();
            }
        }

        @Override
        public void onRecordCancel() {
            LogUtils.d(TAG, "onRecordCancel");

        }

        @Override
        public void onRecordReachedMaxTime(int maxTime) {
            LogUtils.d(TAG, "onRecordReachedMaxTime-maxTime:" + maxTime);

        }
    };
    private OnPlayListener onPlayListener = new OnPlayListener() {

        @Override
        public void onPrepared() {
            LogUtils.d(TAG, "onPrepared");
            //准备开始试听,图标状态不管
        }

        @Override
        public void onCompletion() {
            LogUtils.d(TAG, "onCompletion");
            //试听结束,通知H5更新试听按钮状态为试听
            if (null != voiceAuthCardJSInterface) {
                voiceAuthCardJSInterface.listenComplete();
            }
        }

        @Override
        public void onInterrupt() {
            LogUtils.d(TAG, "onInterrupt");
            //试听被中断,通知H5更新试听按钮状态为试听
            if (null != voiceAuthCardJSInterface) {
                voiceAuthCardJSInterface.listenComplete();
            }
        }

        @Override
        public void onError(String s) {
            LogUtils.d(TAG, "onError :" + s);
            if (null != voiceAuthCardJSInterface) {
                voiceAuthCardJSInterface.listenComplete();
            }
            //试听出错
            if (!TextUtils.isEmpty(s)) {
                toast(s);
            }
        }

        @Override
        public void onPlaying(long l) {
            LogUtils.d(TAG, "onPlaying :" + l);

        }
    };
    private Runnable gotoReportRunnable = null;

    public static void start(Context context, boolean showTitle) {
        Intent intent = new Intent(context, VoiceAuthCardWebViewActivity.class);
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (null != userInfo && !TextUtils.isEmpty(userInfo.getTimbre())) {
            intent.putExtra("url", UriProvider.getVoiceAuthCardUrl());
            //声鉴卡-结果页
            Map<String, String> maps = StatisticModel.getInstance().getUMAnalyCommonMap(context);
            StatisticManager.get().onEvent(context, StatisticModel.EVENT_ID_VOICEAUTHCARD_2, maps);
        } else {
            intent.putExtra("url", UriProvider.getRecordAuthCardVoiceUrl());
            //声鉴卡-未上传
            Map<String, String> maps = StatisticModel.getInstance().getUMAnalyCommonMap(context);
            StatisticManager.get().onEvent(context, StatisticModel.EVENT_ID_VOICEAUTHCARD_1, maps);
        }
        intent.putExtra("showTitle", showTitle);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TAG = VoiceAuthCardWebViewActivity.class.getSimpleName();
        userRecordVoiceManager = new UserRecordVoiceManager(this);
        userRecordVoiceManager.setCommOnPlayListener(onPlayListener);
        userRecordVoiceManager.setCommOnRecordCallback(onRecordCallback);
        voiceAuthCardJSInterface = new VoiceAuthCardJSInterface(webView, this);
        webView.removeJavascriptInterface("androidJsObj");
        webView.addJavascriptInterface(voiceAuthCardJSInterface, "androidJsObj");
        loadRecordVoicePreView();
        checkPermission(new PermissionActivity.CheckPermListener() {
                            @Override
                            public void superPermission() {
                            }
                        }, R.string.ask_again,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    //未录制声音，进来先显示上传录音提示界面，同时加载H5，点击按钮隐藏这个mPreView
    private void loadRecordVoicePreView() {
        userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        //这里初始化标题栏，做到界面展示效果匹配
        if (null != mTitleBar) {
            mTitleBar.setLeftImageResource(R.drawable.icon_white_back);
            flTitle = (FrameLayout) findViewById(R.id.fl_title);
            flTitle.setBackgroundColor(Color.TRANSPARENT);
        }


        initPreView();
        //界面需要根据这个字段的值来做不同的展示处理
        if (null != userInfo && !TextUtils.isEmpty(userInfo.getTimbre())) {
            //不为空，二次查看声鉴卡报告
            tvTips1.setVisibility(View.GONE);
            tvTips2.setVisibility(View.GONE);
            ivUploadAuthVoice.setVisibility(View.GONE);
            llAnalyTips.setVisibility(View.VISIBLE);
        } else {
            //为空，首次走声鉴卡流程
            tvTips1.setVisibility(View.VISIBLE);
            tvTips2.setVisibility(View.VISIBLE);
            ivUploadAuthVoice.setVisibility(View.VISIBLE);
            llAnalyTips.setVisibility(View.GONE);
        }
    }

    private void initPreView() {
        if (mPreView == null) {
            mPreView = LayoutInflater.from(this).inflate(R.layout.view_record_auth_card_voice, null);
            mPreView.findViewById(R.id.ivBack).setOnClickListener(this);
            tvTips1 = mPreView.findViewById(R.id.tvTips1);
            tvTips2 = mPreView.findViewById(R.id.tvTips2);
            llAnalyTips = mPreView.findViewById(R.id.llAnalyTips);
            ivUploadAuthVoice = mPreView.findViewById(R.id.ivUploadAuthVoice);
            ivUploadAuthVoice.setOnClickListener(this);
            tvAnalyProgress = mPreView.findViewById(R.id.tvAnalyProgress);
        }

        //失效
//        ViewGroup decorView = (ViewGroup) getWindow().getDecorView();
//        if (mPreView.getParent() != null) {
//            ((ViewGroup) mPreView.getParent()).removeView(mPreView);
//        }
//        decorView.addView(mPreView);

        flRoot = (FrameLayout) findViewById(R.id.flRoot);
        flRoot.removeView(mPreView);
        flRoot.addView(mPreView);
    }

    /**
     * h5通知客户端，用户开始录制 | 重新录制录音
     */
    public void startRecord() {
        LogUtils.d(TAG, "startRecord");
        //按下的时候先判断当前是否处在房间内
        RoomInfo current = RoomServiceScheduler.getCurrentRoomInfo();
        if (null == current) {
            record();
            return;
        }
        if (null != voiceAuthCardJSInterface) {
            voiceAuthCardJSInterface.recordFailed();
        }
        getDialogManager().showOkCancelDialog(
                getResources().getString(R.string.record_need_exit_avroom),
                true, okCancelDialogListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (null != userRecordVoiceManager) {
            userRecordVoiceManager.releaseOnPause();
        }
    }

    @Override
    protected void onDestroy() {
        if (mHandler != null && null != gotoReportRunnable) {
            mHandler.removeCallbacks(gotoReportRunnable);
            gotoReportRunnable = null;
        }
        super.onDestroy();
        if (null != userRecordVoiceManager) {
            userRecordVoiceManager.releaseOnDestory();
        }
        if (webView != null) {
            //https://blog.csdn.net/u013085697/article/details/53259116  单纯移除webview依然会有ContentViewCore引起webview的mContent泄露
            ViewParent parent = webView.getParent();
            if (parent != null) {
                ((ViewGroup) parent).removeView(webView);
            }
            webView.removeAllViews();
            webView.destroy();
            webView = null;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivUploadAuthVoice:
                //点击上传声音，隐藏过渡页界面
                removePreView();
                break;
            case R.id.ivBack:
                //返回要不要增加提示框
                finish();
                break;
            default:
                break;
        }
    }

    private void removePreView() {
        if (mPreView != null && mPreView.getParent() != null) {
            ((ViewGroup) mPreView.getParent()).removeView(mPreView);
        }
    }

    private void record() {
        LogUtils.d(TAG, "record");
        if (null != userRecordVoiceManager) {
            userRecordVoiceManager.startRecord();
        }
    }

    /**
     * h5计时回调客户端通知停止录制，并提示用户
     */
    public void recordStoped(boolean cancel, String message) {
        LogUtils.d(TAG, "recordStoped-message:" + message);
        if (null != userRecordVoiceManager) {
            userRecordVoiceManager.stopRecord(cancel);
        }
        if (!TextUtils.isEmpty(message)) {
            toast(message);
        }
    }

    /**
     * h5通知客户端用户试听录音 type=0 开始试听 type=1 停止试听
     */
    public void tryToListener(int type) {
        LogUtils.d(TAG, "tryToListener-type:" + type);
        if (null != userRecordVoiceManager) {
            userRecordVoiceManager.tryToListener(0 == type);
        }
    }

    /**
     * h5通知客户端开始生成声鉴卡报告流程，客户端上传录音到七牛，并更新url到服务器
     */
    public void startVoiceAuth() {
        LogUtils.d(TAG, "startVoiceAuth");
        //停掉播放，释放资源
        if (null != userRecordVoiceManager && null != userRecordVoiceManager.getAudioFile()
                && userRecordVoiceManager.getAudioDura() > 0L) {
            userRecordVoiceManager.tryToListener(false);
            getDialogManager().showProgressDialog(this, getResources().getString(R.string.network_loading));
            CoreManager.getCore(IFileCore.class).upload(userRecordVoiceManager.getAudioFile());
        } else {
            LogUtils.w(TAG, "startVoiceAuth-状态出错");
        }
    }

    /**
     * h5通知客户端生成报告进度 判断进度，最大99停止
     */
    public void authProgress(int progress) {
        LogUtils.d(TAG, "authProgress-progress:" + progress);
        if (progress < 100) {
//            String progressStr = getResources().getString(R.string.record_auth_voice_tips3,String.valueOf(progress));
//            LogUtils.d(TAG, "authProgress-progressStr:" + progressStr);
            tvAnalyProgress.setText("正在生产声音报告 ".concat(String.valueOf(progress)).concat("%"));
        }
    }

    /**
     * H5通知客户端，退出当前声鉴卡流程
     */
    public void gotoBack() {
        LogUtils.d(TAG, "gotoBack");
        finish();
    }

    /**
     * h5通知客户端，报告已生成 先改进度为100 然后显示生成报告结果H5页
     */
    public void authDown(int progress) {
        LogUtils.d(TAG, "authProgress-progress:" + progress);
//        tvAnalyProgress.setText(getResources().getString(R.string.record_auth_voice_tips3,String.valueOf(progress)));
        tvAnalyProgress.setText("正在生产声音报告 ".concat(String.valueOf(progress)).concat("%"));
        removePreView();
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    @Override
    public void onUpload(String url) {
        LogUtils.d(TAG, "onUpload-url:" + url);
        getMvpPresenter().updateAuthCardVoiceUrl(url);
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    @Override
    public void onUploadFail() {
        LogUtils.d(TAG, "onUploadFail");
        getDialogManager().dismissDialog();
        toast(R.string.voice_auth_card_analy_failed);
    }

    @Override
    public void onUpdateAuthCardVoice(boolean isSuccess, String message) {
        LogUtils.d(TAG, "onUpdateAuthCardVoice-isSuccess:" + isSuccess + " message:" + message);
        getDialogManager().dismissDialog();
        if(!isSuccess){
            if(!TextUtils.isEmpty(message)){
                toast(message);
            }
            return;
        }
        //切换到生成报告过渡页
        initPreView();
        tvTips1.setVisibility(View.GONE);
        tvTips2.setVisibility(View.GONE);
        ivUploadAuthVoice.setVisibility(View.GONE);
        llAnalyTips.setVisibility(View.VISIBLE);
        tvAnalyProgress.setText("正在生产声音报告 0%");

//        if(null == gotoReportRunnable){
//            gotoReportRunnable = new Runnable() {
//                @Override
//                public void run() {
//                    //通知h5上传完成,h5准备生成报告
//                    if (null != voiceAuthCardJSInterface) {
//                        voiceAuthCardJSInterface.uploadDone();
//                    }
//                }
//            };
//        }
//        if (mHandler != null) {
//            mHandler.postDelayed(gotoReportRunnable,200L);
//        }

        voiceAuthCardJSInterface.uploadDone();
    }

    @Override
    public boolean isStatusBarDarkFont() {
        return false;
    }
}
