package com.yuhuankj.tmxq.ui.room;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.home.IHomeCore;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_core.user.VersionsCoreClient;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.fragment.BaseLazyFragment;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.room.adapter.CommonMagicIndicatorAdapter;
import com.yuhuankj.tmxq.ui.room.adapter.MainAdapter;
import com.yuhuankj.tmxq.ui.search.SearchActivity;
import com.yuhuankj.tmxq.widget.magicindicator.MagicIndicator;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.UIUtil;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.commonnavigator.CommonNavigator;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>首页  </p>
 *
 * @author Administrator
 * @date 2017/11/14
 */
public class MainFragment extends BaseLazyFragment implements CommonMagicIndicatorAdapter.OnItemSelectListener, View.OnClickListener {
    public static final String TAG = MainFragment.class.getSimpleName();

    private MagicIndicator mMagicIndicator;
    private ViewPager mViewPager;
    private ImageView mIvSerach;

    private List<TabInfo> mTabInfoList;
    private String bilibili = "fan";


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (!ListUtils.isListEmpty(mTabInfoList)) {
            outState.putParcelableArrayList(Constants.KEY_MAIN_TAB_LIST, (ArrayList<? extends Parcelable>) mTabInfoList);
        }
    }

    @Override
    protected void restoreState(@Nullable Bundle savedInstanceState) {
        super.restoreState(savedInstanceState);
        if (savedInstanceState != null) {
            mTabInfoList = savedInstanceState.getParcelableArrayList(Constants.KEY_MAIN_TAB_LIST);
        }
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_main;
    }

    @Override
    public void onFindViews() {
        mMagicIndicator = (MagicIndicator) mView.findViewById(R.id.main_indicator);
        mViewPager = (ViewPager) mView.findViewById(R.id.main_viewpager);
        mIvSerach = (ImageView) mView.findViewById(R.id.iv_main_search);

        mViewPager.setOffscreenPageLimit(7);
    }

    @Override
    public void onSetListener() {
        mIvSerach.setOnClickListener(this);
    }


    @Override
    public void initiate() {

    }

    private void initTabIndex(boolean isSuccess, int index) {
        LogUtils.d(TAG, "initTabIndex-index:" + index + " isSuccess:" + isSuccess);
        if (isSuccess && mTabInfoList != null && mTabInfoList.size() > index && mViewPager != null) {
            mMagicIndicator.onPageSelected(index);
            mViewPager.setCurrentItem(index);
        }
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                LogUtils.d(TAG, "onPageScrolled-position:" + position + " positionOffset:" + positionOffset + " positionOffsetPixels");
                mMagicIndicator.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {
                mMagicIndicator.onPageSelected(position);
                LogUtils.d(TAG, "onPageSelected-position:" + position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                LogUtils.d(TAG, "onPageScrollStateChanged-state:" + state);
                switch (state) {
                    case ViewPager.SCROLL_STATE_IDLE:
                        //无动作、初始状态
                        LogUtils.d(TAG, "onPageScrollStateChanged->无动作、初始状态");
                        break;
                    case ViewPager.SCROLL_STATE_DRAGGING:
                        //点击、滑屏
                        LogUtils.d(TAG, "onPageScrollStateChanged-点击、滑屏");
                        break;
                    case ViewPager.SCROLL_STATE_SETTLING:
                        //释放
                        LogUtils.d(TAG, "onPageScrollStateChanged-释放");
                        break;
                }
                mMagicIndicator.onPageScrollStateChanged(state);
            }
        });
    }

    @Override
    public void onItemSelect(int position) {
        mViewPager.setCurrentItem(position);
        StatisticManager.get().onEvent(getActivity(),
                StatisticModel.getInstance().getMainTabChangeToEventId(position),
                StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
        LogUtils.d(TAG, "onItemSelect-position:" + position);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_main_search:
                SearchActivity.start(mContext);
                StatisticManager.get().onEvent(mContext, "main_search", StatisticModel.getInstance().getUMAnalyCommonMap(mContext));
                break;
            default:
        }
    }

    @CoreEvent(coreClientClass = VersionsCoreClient.class)
    public void onGetChannelConfig(boolean isSuccess, String msg, int index) {
        LogUtils.d(TAG, "onGetChannelConfig-isSuccess:" + isSuccess + " msg:" + msg + " index:" + index);
        initTabIndex(isSuccess, index);
    }

    @Override
    protected void onLazyLoadData() {
        mTabInfoList = CoreManager.getCore(IHomeCore.class).getMainTabInfos();//.subList(0, 4);
        // 判断 titles不为 null，为null则拿本地数据
        if (ListUtils.isListEmpty(mTabInfoList)) {
            mTabInfoList = TabInfo.getTabDefaultList();
        }

        //这个tab固定
        String tabNameHot = "推荐";

        if (mTabInfoList.size() > 0 && !mTabInfoList.get(0).getName().equals(tabNameHot)) {
            mTabInfoList.add(0, new TabInfo(-1, tabNameHot));
        }
        mTabInfoList.add(0, new TabInfo(-2, "关注"));
        CommonNavigator commonNavigator = new CommonNavigator(mContext);
        commonNavigator.setSmoothScroll(false);
        CommonMagicIndicatorAdapter magicIndicatorAdapter = new CommonMagicIndicatorAdapter(mContext,
                mTabInfoList, UIUtil.dip2px(mContext, 3), UIUtil.dip2px(mContext, 12));
        magicIndicatorAdapter.setOnItemSelectListener(this);
        commonNavigator.setAdapter(magicIndicatorAdapter);

        mMagicIndicator.setNavigator(commonNavigator);
        mViewPager.setAdapter(new MainAdapter(getChildFragmentManager(), mTabInfoList));
        //首次打开app，可能需要走非自动登录流程，这个时候不能判断是否isRequesting
        VersionsCore core = CoreManager.getCore(VersionsCore.class);
        if (null != core && CoreManager.getCore(VersionsCore.class).isRequestSuccess()) {
            LogUtils.d(TAG, "initiate-isRequestSuccess:true");
            initTabIndex(true, core.getLastRequestHomeIndex());
        } else {
            LogUtils.d(TAG, "initiate-isRequestSuccess:false");
        }
    }
}
