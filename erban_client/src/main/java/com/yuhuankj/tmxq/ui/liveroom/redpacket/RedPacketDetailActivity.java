package com.yuhuankj.tmxq.ui.liveroom.redpacket;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.room_red_packet.RedPacketModel;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liaoxy
 * @Description:红包详情页面
 * @date 2019/1/17 11:26
 */
public class RedPacketDetailActivity extends BaseActivity {
    private List<RedPacketDetaiItemlEnitity> datas;
    private RecyclerView rcvRedPacket;
    private BaseQuickAdapter adapter;
    private TextView tvCount;
    private String redPacketId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_red_packet_history_detail);
        initTitleBar("记录");
        initView();
        initData();
    }

    private void initView() {
        rcvRedPacket = (RecyclerView) findViewById(R.id.rcvRedPacket);
        tvCount = (TextView) findViewById(R.id.tvCount);
    }

    private void initData() {
        Intent intent = getIntent();
        if (intent != null) {
            redPacketId = intent.getStringExtra("redPacketId");
        }
        if (TextUtils.isEmpty(redPacketId)) {
            SingleToastUtil.showToast("红包ID不正确");
            finish();
            return;
        }
        LinearLayoutManager layoutmanager = new RecyclerViewNoBugLinearLayoutManager(this);
        layoutmanager.setOrientation(LinearLayoutManager.VERTICAL);
        rcvRedPacket.setLayoutManager(layoutmanager);
        datas = new ArrayList<>();
        adapter = new RedPacketDetailAdapter(datas);
        rcvRedPacket.setAdapter(adapter);

        DialogManager dialogManager = new DialogManager(this);
        dialogManager.showProgressDialog(this, "请稍后...");
        new RedPacketModel().getRedPacketDetail(redPacketId, new OkHttpManager.MyCallBack<ServiceResult<RedPacketDetailEnitity>>() {
            @Override
            public void onError(Exception e) {
                dialogManager.dismissDialog();
                SingleToastUtil.showToast(e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<RedPacketDetailEnitity> response) {
                dialogManager.dismissDialog();
                if (response.isSuccess()) {
                    RedPacketDetailEnitity detail = response.getData();
                    if (detail == null) {
                        return;
                    }
                    String countStr = String.format("总金额：%d金币  目前人数%d/%d", detail.getRedPacketGold(), detail.getRedPacketReceiveNum(), detail.getRedPacketNum());
                    tvCount.setText(countStr);
                    List<RedPacketDetaiItemlEnitity> temps = detail.getList();
                    if (temps != null) {
                        datas.clear();
                        datas.addAll(temps);
                        adapter.notifyDataSetChanged();
                    }
                } else {
                    SingleToastUtil.showToast(getApplicationContext(), response.getMessage());
                }
            }
        });
    }
}
