package com.yuhuankj.tmxq.ui.liveroom.nimroom.callback;

import android.support.v7.util.DiffUtil;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;

import java.util.List;
import java.util.Objects;

/**
 * @author Administrator
 */
public class ChatMemberDiffUtilCallback extends DiffUtil.Callback {
    private List<ChatRoomMember> mOldMemberList, mNewMemberList;

    public ChatMemberDiffUtilCallback(List<ChatRoomMember> oldMemberList, List<ChatRoomMember> newMemberList) {
        mOldMemberList = oldMemberList;
        mNewMemberList = newMemberList;
    }

    @Override
    public int getOldListSize() {
        return mOldMemberList == null ? 0 : mOldMemberList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewMemberList == null ? 0 : mNewMemberList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return Objects.equals(mOldMemberList.get(oldItemPosition).getAccount(),
                mNewMemberList.get(newItemPosition).getAccount());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        ChatRoomMember oldItem = mOldMemberList.get(oldItemPosition);
        ChatRoomMember newItem = mNewMemberList.get(newItemPosition);
        return Objects.equals(oldItem.getAccount(), newItem.getAccount())
                && Objects.equals(oldItem.getNick(), newItem.getNick());
    }
}