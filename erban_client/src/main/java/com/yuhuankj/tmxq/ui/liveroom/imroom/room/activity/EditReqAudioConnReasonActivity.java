package com.yuhuankj.tmxq.ui.liveroom.imroom.room.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter.EditReqAudioConnReasonPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IEditReqAudioConnReasonView;

/**
 * 编辑连线理由
 *
 * @author 魏海涛
 * @date 2019年7月17日 11:25:54
 */
@CreatePresenter(EditReqAudioConnReasonPresenter.class)
public class EditReqAudioConnReasonActivity extends BaseMvpActivity<IEditReqAudioConnReasonView, EditReqAudioConnReasonPresenter<IEditReqAudioConnReasonView>>
        implements IEditReqAudioConnReasonView, View.OnClickListener {

    public static final int EDIT_REQ_AUDIO_CONN_REASON_REQUEST_CODE = 2;
    public static final int EDIT_REQ_AUDIO_CONN_REASON_RESULT_CODE = 2;
    private EditText edReason;
    private TextView tvInputNum;
    private String lastReason = null;

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (null != edReason.getText() && !TextUtils.isEmpty(edReason.getText().toString())) {
                refreshInputLengthTips(edReason.getText().toString());
            }
        }
    };

    public static void startForResult(FragmentActivity fragmentActivity, int requestCode, String reason) {
        Intent intent = new Intent(fragmentActivity, EditReqAudioConnReasonActivity.class);
        intent.putExtra("reason", reason);
        fragmentActivity.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_req_audio_conn_reason);
        initTitleBar(getResources().getString(R.string.audio_connect_reason));
        initView();
    }

    private void initView() {
        edReason = (EditText) findViewById(R.id.edReason);
        tvInputNum = (TextView) findViewById(R.id.tvInputNum);
        findViewById(R.id.bltvSaveReason).setOnClickListener(this);
        edReason.addTextChangedListener(textWatcher);
        lastReason = getIntent().getStringExtra("reason");
        refreshInputLengthTips(lastReason);
    }

    private void refreshInputLengthTips(String reason) {
        edReason.removeTextChangedListener(textWatcher);
        if (!TextUtils.isEmpty(reason)) {
            reason = reason.replace("\n", "").replaceAll(" ", "");
        }
        edReason.setText(reason);
        edReason.setSelection(reason.length());
        edReason.addTextChangedListener(textWatcher);
        int length = reason.length();
        String tips = getResources().getString(R.string.audio_conn_edit_reason_input_num, String.valueOf(length));
        SpannableStringBuilder ssb = new SpannableStringBuilder(tips);
        ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#333333")), 0, String.valueOf(length).length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#CCCCCC")),
                String.valueOf(length).length(), tips.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        tvInputNum.setText(ssb);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bltvSaveReason:
                //调用修改申请信息接口
                editAudioConnReqReason();
                break;
            default:
                break;
        }
    }

    private void editAudioConnReqReason() {
        if (null != edReason.getText() && !TextUtils.isEmpty(edReason.getText().toString())) {
            getDialogManager().showProgressDialog(this, getResources().getString(R.string.network_loading));
            getMvpPresenter().editAudioConnReqReason(edReason.getText().toString());
        } else {
            toast(getResources().getString(R.string.audio_conn_edit_reason_tips1));
        }
    }

    @Override
    public void toast(String toast) {
        if (!TextUtils.isEmpty(toast)) {
            super.toast(toast);
        }
        getDialogManager().dismissDialog();
    }

    @Override
    public void finishForResult() {
        getDialogManager().dismissDialog();
        setResult(EditReqAudioConnReasonActivity.EDIT_REQ_AUDIO_CONN_REASON_REQUEST_CODE);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != edReason) {
            edReason.removeTextChangedListener(textWatcher);
        }
    }
}
