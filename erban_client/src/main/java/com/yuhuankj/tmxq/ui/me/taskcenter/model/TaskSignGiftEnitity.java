package com.yuhuankj.tmxq.ui.me.taskcenter.model;

import java.io.Serializable;

/**
 * @author weihaitao
 * @date 2019/5/24
 */
public class TaskSignGiftEnitity implements Serializable {

    /**
     * 连续签到天数
     */
    private int signDay;

    /**
     * 签到描述语
     */
    private int signDesc;

    /**
     * 签到物品名称
     */
    private String signName;

    /**
     * 签到物品图片
     */
    private String signPic;

    public int getSignDay() {
        return signDay;
    }

    public void setSignDay(int signDay) {
        this.signDay = signDay;
    }

    public int getSignDesc() {
        return signDesc;
    }

    public void setSignDesc(int signDesc) {
        this.signDesc = signDesc;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public String getSignPic() {
        return signPic;
    }

    public void setSignPic(String signPic) {
        this.signPic = signPic;
    }
}
