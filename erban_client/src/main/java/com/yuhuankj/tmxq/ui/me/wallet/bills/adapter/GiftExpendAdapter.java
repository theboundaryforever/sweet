package com.yuhuankj.tmxq.ui.me.wallet.bills.adapter;

import android.widget.ImageView;

import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.libcommon.utils.TimeUtils;
import com.tongdaxing.xchat_core.bills.bean.BillItemEntity;
import com.tongdaxing.xchat_core.bills.bean.ExpendInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

/**
 * ExpendInfo
 * Created by Seven on 2017/9/10.
 */
public class GiftExpendAdapter extends BillBaseAdapter {

    public GiftExpendAdapter(List<BillItemEntity> billItemEntityList) {
        super(billItemEntityList);
        addItemType(BillItemEntity.ITEM_NORMAL, R.layout.list_expend_gift_item);
    }

    @Override
    public void convertNormal(BaseViewHolder baseViewHolder, BillItemEntity billItemEntity) {
        ExpendInfo expendInfo = billItemEntity.mGiftExpendInfo;
        if (expendInfo == null) return;
        baseViewHolder.setText(R.id.tv_gift_income,"-"+ expendInfo.getGoldNum())
                .setText(R.id.tv_send_name, "收礼人 " + expendInfo.getTargetNick())
                .setText(R.id.tv_user_name, expendInfo.getGiftName())
                .setText(R.id.gift_date, TimeUtils.getDateTimeString(expendInfo.getRecordTime(), "HH:mm:ss"))
                .setImageResource(R.id.image, R.drawable.ic_gold_coin_small);

        ImageView img_avatar = baseViewHolder.getView(R.id.img_avatar);
        ImageLoadUtils.loadImage(mContext, expendInfo.getGiftPic(), img_avatar);
    }
}
