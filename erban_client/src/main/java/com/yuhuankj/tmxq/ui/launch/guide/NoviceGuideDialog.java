package com.yuhuankj.tmxq.ui.launch.guide;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.NotchFixUtil;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.home.game.PlayTogetherAdapter;
import com.yuhuankj.tmxq.ui.nim.game.GameEnitity;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.yuhuankj.tmxq.ui.launch.guide.NoviceGuideDialog.GuideType.AUCTION_ROOM_GUIDE;
import static com.yuhuankj.tmxq.ui.launch.guide.NoviceGuideDialog.GuideType.AVROOM_MICRO;
import static com.yuhuankj.tmxq.ui.launch.guide.NoviceGuideDialog.GuideType.GAME_HOME_GUIDE_1;

/**
 * Dilaog的生命周期场景
 * 点击显示按钮，第一次显示Dialog，然后按BACK键返回。
 * show() —> onCreate() —> onStart();
 * cancel() —> onDismiss() —> Stop();
 * 再次点击显示按钮，然后点击Dialog外部。
 * show() —> onStart();
 * cancel() —> onDismiss() —> Stop();
 * 再次点击显示按钮，然后执行Dialog.dismiss() 方法。
 * show() —> onStart();
 * onDismiss() —> Stop();
 */
public class NoviceGuideDialog extends AppCompatDialog {

    private final String TAG = NoviceGuideDialog.class.getSimpleName();

    private FragmentActivity context;
    private ImageView iv_guide;

    private boolean isFullScreen = false;

    private GuideType guideType;
    private int[] locations;

    private int[] gameHomeGuideLocations;
    private String iconUrl;
    private List<GameEnitity> gameEnitities = new ArrayList<>();
    private int guideImgIndex = 0;

    private boolean canDialogDismiss = false;

    public void setGameHomeGuideLocations(int[] gameHomeGuideLocations) {
        this.gameHomeGuideLocations = gameHomeGuideLocations;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public void setGameEnitities(List<GameEnitity> gameEnitities) {
        this.gameEnitities = gameEnitities;
    }

    //445dp 18dp

    public NoviceGuideDialog(FragmentActivity context, GuideType guideType, int[] locations) {
        super(context, R.style.GuideDialog);

        this.context = context;
        this.guideType = guideType;
        this.locations = locations;
        // We hide the title bar for any style configuration. Otherwise, there will be a gap
        // above the bottom sheet when it is expanded.
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (null != iv_guide) {
            iv_guide.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // 正常情况下新手引导界面仅仅会展示一次，所以不用理会dismiss再show的场景
                    canDialogDismiss = true;
                }
            }, 500L);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCanceledOnTouchOutside(false);
        setCancelable(false);
        setContentView(getContentViewLayoutId(guideType));

        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display d = windowManager.getDefaultDisplay();
        DisplayMetrics realDisplayMetrics = new DisplayMetrics();
        d.getRealMetrics(realDisplayMetrics);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        int disHeight = context.getResources().getDisplayMetrics().heightPixels;
        int realDisHeight = realDisplayMetrics.heightPixels;
        LogUtils.d(TAG, "onCreate-disHeight:" + disHeight + " realDisHeight:" + realDisHeight + " isFullScreen:" + isFullScreen);
        params.height = (isFullScreen ? disHeight : realDisHeight) -
                (Utils.hasSoftKeys(context) ? Utils.getNavBarHeight(context) : 0);

//        params.flags = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;//忽略状态栏
        getWindow().setAttributes(params);


        findViewById(R.id.ll_root).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (canDialogDismiss) {
                    if (guideType == GAME_HOME_GUIDE_1 && 0 == guideImgIndex) {
                        guideImgIndex += 1;
                        displayFix();
                    }
                    if (guideType == AUCTION_ROOM_GUIDE && 0 == guideImgIndex) {
                        guideImgIndex += 1;
                        displayFix();
                    } else {
                        int oridnal = guideType.ordinal() + 1;
                        oridnal = oridnal == 7 ? 3 : oridnal;
                        //7为修改后的3，也即服务器从1到6，后面紧跟的应该是7、8、9...
                        if (oridnal > 7) {
                            oridnal -= 1;
                        }
                        CoreManager.getCore(IUserCore.class).updateGuideStatus(oridnal);
                        dismiss();
                    }
                }
            }
        });
        iv_guide = findViewById(R.id.iv_guide);
        notifyNoviceGuideShowStatusChanged(guideType, true);
        displayFix();
    }

    /**
     * 屏幕适配
     */
    private void displayFix() {
        LogUtils.d(TAG, "displayFix-manufacturer:" + Build.MANUFACTURER);
        FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) iv_guide.getLayoutParams();
        if (AVROOM_MICRO == guideType) {
            //房间麦位引导图，需要指定窗口位置
            if (null != locations && locations.length > 0) {
                lp.leftMargin = locations[0] - DisplayUtility.dp2px(context, 46);
            }
        } else if (GAME_HOME_GUIDE_1 == guideType) {
            ImageView ivChanceMeeting = findViewById(R.id.ivChanceMeeting);
            RecyclerView rcvPlayTogether = findViewById(R.id.rcvPlayTogether);
            View vGameList = findViewById(R.id.vGameList);
            if (0 == guideImgIndex) {
                rcvPlayTogether.setVisibility(View.VISIBLE);
                vGameList.setVisibility(View.VISIBLE);
                ivChanceMeeting.setVisibility(View.GONE);
                if (null != locations && locations.length > 1) {
                    FrameLayout.LayoutParams lp1 = (FrameLayout.LayoutParams) rcvPlayTogether.getLayoutParams();
                    lp1.topMargin = locations[1];
                    FrameLayout.LayoutParams lp2 = (FrameLayout.LayoutParams) vGameList.getLayoutParams();
                    lp2.topMargin = locations[1] - DisplayUtility.dp2px(context, 6);
                    if (null != gameEnitities && gameEnitities.size() > 0) {
                        GridLayoutManager manager = new GridLayoutManager(getContext(), 3);
                        rcvPlayTogether.setLayoutManager(manager);
                        PlayTogetherAdapter adapter = new PlayTogetherAdapter(gameEnitities);
                        rcvPlayTogether.setAdapter(adapter);
                    }
                    lp.topMargin = locations[1] + DisplayUtility.dp2px(context, 124);
                }
            } else if (1 == guideImgIndex) {
                rcvPlayTogether.setVisibility(View.GONE);
                vGameList.setVisibility(View.GONE);
                ivChanceMeeting.setVisibility(View.VISIBLE);
                if (null != gameHomeGuideLocations && gameHomeGuideLocations.length > 1) {
                    FrameLayout.LayoutParams lp1 = (FrameLayout.LayoutParams) ivChanceMeeting.getLayoutParams();
                    lp1.topMargin = gameHomeGuideLocations[1];
                    int tempWidth = DisplayUtility.getScreenWidth(context) / 4;
                    lp1.leftMargin = tempWidth * 2 + (tempWidth - DisplayUtility.dp2px(context, 58)) / 2;
                    ImageLoadUtils.loadBannerRoundBackground(context,
                            ImageLoadUtils.toThumbnailUrl(DisplayUtility.dp2px(context, 58),
                                    DisplayUtility.dp2px(context, 58), iconUrl), ivChanceMeeting);
                    lp.topMargin = gameHomeGuideLocations[1] + DisplayUtility.dp2px(context, 64);
                    iv_guide.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_game_home_guide_2));
                }
            }
        } else if (AUCTION_ROOM_GUIDE == guideType) {
            if (null == lp || !GlideContextCheckUtil.checkContextUsable(context)) {
                return;
            }
            if (0 == guideImgIndex) {
                lp.width = DisplayUtility.dp2px(context, 250);
                lp.height = DisplayUtility.dp2px(context, 196);
                lp.gravity = Gravity.BOTTOM | Gravity.RIGHT;
                lp.rightMargin = DisplayUtility.dp2px(context, 20);
                lp.bottomMargin = DisplayUtility.dp2px(context, 65);
                lp.topMargin = DisplayUtility.dp2px(context, 0);
                iv_guide.setLayoutParams(lp);
                iv_guide.setImageResource(R.mipmap.icon_auction_room_guide_1);
            } else if (1 == guideImgIndex) {
                lp.width = DisplayUtility.dp2px(context, 266);
                lp.height = DisplayUtility.dp2px(context, 202);
                lp.gravity = Gravity.TOP | Gravity.RIGHT;
                lp.rightMargin = DisplayUtility.dp2px(context, 18);
                lp.bottomMargin = DisplayUtility.dp2px(context, 0);
                lp.topMargin = DisplayUtility.dp2px(context, 225);
                iv_guide.setLayoutParams(lp);
                iv_guide.setImageResource(R.mipmap.icon_auction_room_guide_2);
            }
        }

        if (android.os.Build.VERSION.SDK_INT >= 28) {
            if (NotchFixUtil.check(NotchFixUtil.ROM_EMUI)) {
                if (NotchFixUtil.hasHwNotchInScreen(context)) {
                    //已适配:HUAWEI CLT-AL01 android9.0 28
                    if (GAME_HOME_GUIDE_1 == guideType) {
                        ImageView ivChanceMeeting = findViewById(R.id.ivChanceMeeting);
                        RecyclerView rcvPlayTogether = findViewById(R.id.rcvPlayTogether);
                        View vGameList = findViewById(R.id.vGameList);
                        if (0 == guideImgIndex) {
                            //rcvPlayTogether、vGameList、iv_guide同时调高
                            FrameLayout.LayoutParams lp1 = (FrameLayout.LayoutParams) rcvPlayTogether.getLayoutParams();
                            lp1.topMargin -= NotchFixUtil.getNotchSize(context)[1];
                            FrameLayout.LayoutParams lp2 = (FrameLayout.LayoutParams) vGameList.getLayoutParams();
                            lp2.topMargin -= NotchFixUtil.getNotchSize(context)[1];
                            lp.topMargin -= NotchFixUtil.getNotchSize(context)[1];
                        } else if (1 == guideImgIndex) {
                            //iv_guide、ivChanceMeeting同时调高
                            FrameLayout.LayoutParams lp1 = (FrameLayout.LayoutParams) ivChanceMeeting.getLayoutParams();
                            lp1.topMargin -= NotchFixUtil.getNotchSize(context)[1];
                            lp.topMargin -= NotchFixUtil.getNotchSize(context)[1];
                        }
                    }
                }
            }
        } else if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (NotchFixUtil.check(NotchFixUtil.ROM_MIUI) && NotchFixUtil.hasMiNotchInScreen(context)) {
                int miNotchHeight = NotchFixUtil.getMiNotchHeight(context);
                //已适配:xuaomi redmi 6 Pro Android 8.1.0
                if (0 == miNotchHeight) {
                    if (AVROOM_MICRO == guideType) {
                        lp.topMargin -= DisplayUtility.dp2px(context, 25);
                    } else if (guideType == GuideType.AVROOM_GIFT_DIALOG) {
                        lp.bottomMargin += DisplayUtility.dp2px(context, 25);
                    } else if (guideType == GuideType.USERINFO_OTHER || guideType == GuideType.USERINFO_SELF) {
                        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
                        Display d = windowManager.getDefaultDisplay();
                        DisplayMetrics realDisplayMetrics = new DisplayMetrics();
                        d.getRealMetrics(realDisplayMetrics);
                        WindowManager.LayoutParams params = getWindow().getAttributes();
                        params.height = realDisplayMetrics.heightPixels;
                        getWindow().setAttributes(params);
                    }
                }
            } else if (NotchFixUtil.check(NotchFixUtil.ROM_VIVO) && NotchFixUtil.hasVivoNotchInScreen(context)) {
                //已适配:Vivo z1 android 7.1.0
                if (AVROOM_MICRO == guideType) {
                    lp.topMargin -= DisplayUtility.dp2px(context, 27);
                } else if (guideType == GuideType.AVROOM_GIFT_DIALOG) {
                    lp.bottomMargin += DisplayUtility.dp2px(context, 32);
                } else if (guideType == GuideType.USERINFO_OTHER || guideType == GuideType.USERINFO_SELF) {
                    lp.bottomMargin += DisplayUtility.dp2px(context, 27);
                } else if (AUCTION_ROOM_GUIDE == guideType) {
                    //已适配 Vivo Y1813BA Android 8.1.0 API 27
                    if (0 == guideImgIndex) {
                        lp.bottomMargin += DisplayUtility.dp2px(context, 27);
                    } else {
                        lp.topMargin -= DisplayUtility.dp2px(context, 32);
                    }
                }
            } else if (NotchFixUtil.check(NotchFixUtil.ROM_EMUI)) {
                if (NotchFixUtil.hasHwNotchInScreen(context)) {
                    //已适配:HUAWEI CLT-AL01 android8.1.0 27
                } else {
                    //已适配:huawei pra-al00x android 8.0.0 26/huwei eml-2 android 8.1.0 27/huwei eml-AL00 android 8.1.0 27
                    if (AVROOM_MICRO == guideType) {
                        lp.topMargin -= DisplayUtility.dp2px(context, 25);
                    } else if (guideType == GuideType.AVROOM_GIFT_DIALOG) {
                        lp.bottomMargin += DisplayUtility.dp2px(context, 25);
                    }
                }
            }
        } else if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M) {
            //已适配:TCL android 6.0 23
            if (AVROOM_MICRO == guideType) {
                lp.topMargin -= NotchFixUtil.getStatusBarHeight(context);
            } else if (guideType == GuideType.AVROOM_GIFT_DIALOG) {
                lp.bottomMargin += NotchFixUtil.getStatusBarHeight(context);
            }
        } else if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N_MR1) {
            //Xiao Mi 6 android 7.1.1 25
            if (NotchFixUtil.check(NotchFixUtil.ROM_OPPO)) {
                LogUtils.d(TAG, "android.os.Build.MODEL:" + android.os.Build.MODEL);
                //Oppo A83 android 7.1.1 25/OPPO R11 Android 7.1.1 25
                if (AVROOM_MICRO == guideType && Build.MODEL.endsWith("OPPO A83")) {
                    lp.topMargin -= NotchFixUtil.getStatusBarHeight(context);
                } else if (guideType == GuideType.AVROOM_GIFT_DIALOG) {
                    lp.bottomMargin += NotchFixUtil.getStatusBarHeight(context);
                }
            } else if (NotchFixUtil.check(NotchFixUtil.ROM_VIVO)) {
                //Vivo X20A android 7.1.1 25
                if (AVROOM_MICRO == guideType) {
                    lp.topMargin -= NotchFixUtil.getStatusBarHeight(context);
                } else if (guideType == GuideType.AVROOM_GIFT_DIALOG) {
                    lp.bottomMargin += NotchFixUtil.getStatusBarHeight(context);
                }
            }
        }
    }

    /**
     * 根据新手引导页面类型选择对应的布局资源
     *
     * @param guideType
     * @return
     */
    private int getContentViewLayoutId(GuideType guideType) {
        int layoutId = 0;
        switch (guideType) {
            case AVROOM_GIFT_BTN:
                layoutId = R.layout.activity_avroom_gift_btn_guide;
                break;
            case AVROOM_MICRO:
                layoutId = R.layout.activity_avroom_micro_guide;
                break;
            case AVROOM_GIFT_DIALOG:
                layoutId = R.layout.dialog_gift_guide;
                break;
            case USERINFO_SELF:
                layoutId = R.layout.activity_user_info_guide_self;
                break;
            case USERINFO_OTHER:
                layoutId = R.layout.activity_user_info_guide_other;
                break;
            case CHANCE_MEETING:
                layoutId = R.layout.activity_chance_meeting_guide;
                break;
            case GAME_HOME_GUIDE_1:
                layoutId = R.layout.activity_game_home_guide_1;
                break;
            case AUCTION_ROOM_GUIDE:
                layoutId = R.layout.activity_auction_room_guide;
                break;
            default:
                break;
        }
        return layoutId;
    }

    private void notifyNoviceGuideShowStatusChanged(GuideType guideType, boolean isShow) {
        switch (guideType) {
            case AVROOM_MICRO:
                break;
            case AVROOM_GIFT_DIALOG:
                break;
            case USERINFO_SELF:
                break;
            case USERINFO_OTHER:
                break;
            case CHANCE_MEETING:
                CoreManager.notifyClients(INoviceGuideCoreClient.class, INoviceGuideCoreClient.METHOD_ON_CHANCE_MEETING_SHOW_STATUS_CHANGED, isShow);
                break;
            case GAME_HOME_GUIDE_1:
                break;
            case AVROOM_GIFT_BTN:
                break;
            case AUCTION_ROOM_GUIDE:
                break;
            default:
                break;
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        notifyNoviceGuideShowStatusChanged(guideType, false);
    }

    public enum GuideType {
        USERINFO_OTHER,          //个人资料页--非自己的
        USERINFO_SELF,          //个人资料页--自己的
        AVROOM_MICRO,           //房间连麦指引
        AVROOM_GIFT_DIALOG,     //房间礼物对话框指引
        CHANCE_MEETING,         //偶遇界面说明
        GAME_HOME_GUIDE_1,      //游戏大厅，第一张引导图
        AVROOM_GIFT_BTN,        //轰趴房修改后的新手引导图
        AUCTION_ROOM_GUIDE,     //拍卖房新手引导图
    }
}
