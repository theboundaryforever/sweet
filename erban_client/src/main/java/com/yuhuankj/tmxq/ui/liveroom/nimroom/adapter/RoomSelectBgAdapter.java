package com.yuhuankj.tmxq.ui.liveroom.nimroom.adapter;


import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.bean.ChatSelectBgBean;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/3/23.
 */

public class RoomSelectBgAdapter extends BaseQuickAdapter<ChatSelectBgBean, RoomSelectBgAdapter.ViewHolder> {

    ItemAction itemAction;
    private UserInfo mCurrUserInfo = null;

    public int selectIndex = 0;
    public String selectUrl = "";
    public String selectBgName = "";

    public RoomSelectBgAdapter() {
        super(R.layout.item_select_chat_room_bg);
        mCurrUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
    }

    @Override
    protected void convert(ViewHolder viewHolder, ChatSelectBgBean item) {
        if (!TextUtils.isEmpty(item.picUrl)) {
            ImageLoadUtils.loadImage(viewHolder.mView.getContext(), item.picUrl, viewHolder.mIvSelectBg);
        } else {
            viewHolder.mIvSelectBg.setImageResource(item.id);
        }

        if (!TextUtils.isEmpty(item.vipIcon)) {
            ImageLoadUtils.loadImage(viewHolder.mView.getContext(), item.vipIcon, viewHolder.ivMinNobelLevel);
            viewHolder.ivMinNobelLevel.setVisibility(View.VISIBLE);
        } else {
            viewHolder.ivMinNobelLevel.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        String url = getData().get(position).picUrl;
        String name = getData().get(position).name;
        int vipId = getData().get(position).vipId;
        if (TextUtils.isEmpty(selectUrl) && 0 == position) {
            selectUrl = url;
        }
        holder.mIvSelectIcon.setVisibility(url.equals(selectUrl) ? View.VISIBLE : View.GONE);
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vipId > 0 && null != mCurrUserInfo && mCurrUserInfo.getVipId() < vipId) {
                    if (null != itemAction) {
                        itemAction.nobleNotEnoughtBuy();
                    }
                    return;
                }
                selectIndex = position;
                selectUrl = url;
                selectBgName = name;
                if (itemAction != null) {
                    itemAction.itemSelect(position);
                }
                notifyDataSetChanged();
            }
        });
    }

    public void setItemAction(ItemAction itemAction) {
        this.itemAction = itemAction;
    }

    public interface ItemAction {
        void itemSelect(int index);

        void nobleNotEnoughtBuy();
    }

    public class ViewHolder extends BaseViewHolder {
        public View mView;
        @BindView(R.id.iv_select_bg)
        ImageView mIvSelectBg;
        @BindView(R.id.iv_select_icon)
        ImageView mIvSelectIcon;
        @BindView(R.id.ivMinNobelLevel)
        ImageView ivMinNobelLevel;

        public ViewHolder(View view) {

            super(view);
            mView = view;
            ButterKnife.bind(this, view);
        }
    }


}
