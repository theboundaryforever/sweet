package com.yuhuankj.tmxq.ui.liveroom.nimroom.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.JavaUtil;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.pair.IPairCore;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.bean.ServerUserMemberInfo;
import com.tongdaxing.xchat_core.room.model.RoomBaseModel;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.activity.RoomBlackListActivity;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.activity.RoomManagerListActivity;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.gift.GiftDialog;
import com.yuhuankj.tmxq.ui.me.charge.ChargeActivity;
import com.yuhuankj.tmxq.ui.me.noble.NobleBusinessManager;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;
import com.yuhuankj.tmxq.ui.widget.UserInfoDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiConsumer;

import static com.tongdaxing.xchat_core.Constants.NOBLE_INVISIABLE_ENTER_ROOM;
import static com.tongdaxing.xchat_core.Constants.NOBLE_INVISIABLE_UID;
import static com.tongdaxing.xchat_core.Constants.USER_MEDAL_DATE;
import static com.tongdaxing.xchat_core.Constants.USER_MEDAL_ID;


/**
 * @author chenran
 * @date 2017/10/11
 */
public class ButtonItemFactory {

    private static final String TAG = ButtonItemFactory.class.getSimpleName();

    /**
     * 公屏点击的所有items
     *
     * @return -
     */
    public static List<ButtonItem> createAllRoomPublicScreenButtonItems(Context context, String account) {
        if (AvRoomDataManager.get().mCurrentRoomInfo == null
                || TextUtils.isEmpty(account)) {
            return null;
        }
        List<ButtonItem> buttonItems = new ArrayList<>();
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        String roomId = String.valueOf(roomInfo.getRoomId());

        ChatRoomMember chatRoomMember = AvRoomDataManager.get().getChatRoomMember(account);

        return getButtonItems(context, account, buttonItems, roomInfo, roomId, chatRoomMember);
    }


    public static List<ButtonItem> createAllRoomPublicScreenButtonItems(Context context, ChatRoomMember chatRoomMember) {
        if (AvRoomDataManager.get().mCurrentRoomInfo == null || chatRoomMember == null) {
            return null;
        }
        List<ButtonItem> buttonItems = new ArrayList<>();
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        String roomId = String.valueOf(roomInfo.getRoomId());
        String account = chatRoomMember.getAccount();
        return getButtonItems(context, account, buttonItems, roomInfo, roomId, chatRoomMember);
    }

    @Nullable
    private static List<ButtonItem> getButtonItems(Context context, String account, List<ButtonItem> buttonItems,
                                                   RoomInfo roomInfo, String roomId, ChatRoomMember chatRoomMember) {
        boolean isMyself = AvRoomDataManager.get().isOwner(account);
        boolean isTargetGuess = AvRoomDataManager.get().isGuess(account);
        boolean isTargetRoomAdmin = AvRoomDataManager.get().isRoomAdmin(account);
        boolean isTargetRoomOwner = AvRoomDataManager.get().isRoomOwner(account);
        boolean isTargetOnMic = AvRoomDataManager.get().isOnMic(account);

        int vipId = 0;
        int vipDate = 0;
        boolean isInvisible = false;
        if (null != chatRoomMember) {
            Map<String, Object> extenMap = chatRoomMember.getExtension();
            if (null != extenMap) {
                if (extenMap.containsKey(Constants.USER_MEDAL_ID)) {
                    vipId = (int) extenMap.get(Constants.USER_MEDAL_ID);
                }
                if (extenMap.containsKey(Constants.USER_MEDAL_DATE)) {
                    vipDate = (int) extenMap.get(Constants.USER_MEDAL_DATE);
                }
                if (extenMap.containsKey(Constants.NOBLE_INVISIABLE_ENTER_ROOM)) {
                    isInvisible = (int) extenMap.get(Constants.NOBLE_INVISIABLE_ENTER_ROOM) == 1;
                }
            }
        }

        // 房主点击
        if (AvRoomDataManager.get().isRoomOwner()) {
            if (isTargetRoomOwner) {
                // 非竞拍房
                new UserInfoDialog(context, JavaUtil.str2long(account)).show();
                return null;
            } else if (isTargetRoomAdmin || isTargetGuess) {
                //送礼物
                buttonItems.add(createSendGiftItem(context, account, vipId, vipDate, isInvisible));
                // 查看资料
                buttonItems.add(createCheckUserInfoDialogItem(context, account, false));
                // 抱Ta上麦
                if (!isTargetOnMic) {
                    buttonItems.add(createInviteOnMicItem(account));
                }
                // 踢出房间
                buttonItems.add(createKickOutRoomItem(context, chatRoomMember, roomId, account));
                // 增加/移除管理员
                buttonItems.add(createMarkManagerListItem(roomId, account, isTargetGuess, chatRoomMember));
                // 加入黑名单
                buttonItems.add(createMarkBlackListItem(context, chatRoomMember, roomId));
            }
        } else if (AvRoomDataManager.get().isRoomAdmin()) {
            if (isMyself) {
                // 非竞拍房
                new UserInfoDialog(context, JavaUtil.str2long(account)).show();
                return null;
            } else if (isTargetRoomAdmin || isTargetRoomOwner) {
                if (isTargetOnMic) {
                    new UserInfoDialog(context, JavaUtil.str2long(account)).show();
                    return null;
                }
            } else if (isTargetGuess) {
                //送礼物
                buttonItems.add(createSendGiftItem(context, account, vipId, vipDate, isInvisible));
                // 查看资料
                buttonItems.add(createCheckUserInfoDialogItem(context, account, false));
                // 抱Ta上麦
                if (!isTargetOnMic) {
                    buttonItems.add(createInviteOnMicItem(account));
                }
                // 踢出房间
                buttonItems.add(createKickOutRoomItem(context, chatRoomMember, roomId, account));
                // 加入黑名单
                buttonItems.add(createMarkBlackListItem(context, chatRoomMember, roomId));
            }
        } else if (AvRoomDataManager.get().isGuess()) {
            if ((CoreManager.getCore(IAuthCore.class).getCurrentUid() + "").equals(account)) {
                new UserInfoDialog(context, JavaUtil.str2long(account)).show();
            } else {
                showGiftDialog(context, account, vipId, vipDate, isInvisible);
            }
            return null;
        }
        return buttonItems;
    }


    private static void showGiftDialog(Context context, String account, int vipId, int vipDate, boolean isInvisible) {
        GiftDialog giftDialog = new GiftDialog(context, JavaUtil.str2long(account), "", "", vipId, vipDate, isInvisible);
        giftDialog.setRoomId(null == AvRoomDataManager.get().mCurrentRoomInfo ? 0L :
                AvRoomDataManager.get().mCurrentRoomInfo.getRoomId());
        giftDialog.setGiftDialogBtnClickListener(new GiftDialog.OnGiftDialogBtnClickListener() {
            @Override
            public void onRechargeBtnClick() {
                ChargeActivity.start(context);
            }

            @Override
            public void onSendGiftBtnClick(GiftInfo giftInfo, long uid, int number) {
                RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                if (currentRoomInfo == null) {
                    return;
                }
                CoreManager.getCore(IGiftCore.class).sendRoomGift(giftInfo.getGiftId(), uid, currentRoomInfo.getUid(), number, giftInfo.getGoldPrice());
            }

            @Override
            public void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, int number) {
                RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                if (currentRoomInfo == null) {
                    return;
                }
                List<Long> targetUids = new ArrayList<>();
                for (int i = 0; i < micMemberInfos.size(); i++) {
                    targetUids.add(micMemberInfos.get(i).getUid());
                }
                CoreManager.getCore(IGiftCore.class).sendRoomMultiGift(giftInfo.getGiftId(), targetUids, currentRoomInfo.getUid(), number, giftInfo.getGoldPrice());
            }
        });
        giftDialog.show();
    }

    public static ButtonItem createMsgBlackListItem(String s, OnItemClick onItemClick) {
        return new ButtonItem(s, new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                onItemClick.itemClick();
            }
        });
    }

    /**
     * 踢Ta下麦
     */
    public static ButtonItem createKickDownMicItem(final String account, int targetVipId, int targetVipDate,
                                                   boolean targetIsInvisiable, String targetNick) {
        return new ButtonItem(BasicConfig.INSTANCE.getAppContext().getString(R.string.embrace_down_mic),
                new ButtonItem.OnClickListener() {
                    @Override
                    public void onClick() {
                        if (AvRoomDataManager.get().isOnMic(JavaUtil.str2long(account))) {
                            int micPosition = AvRoomDataManager.get().getMicPosition(JavaUtil.str2long(account));
                            IMNetEaseManager.get().downMicroPhoneBySdk(micPosition, null);

                            RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                            if (roomInfo != null) {
                                Disposable disposable = IMNetEaseManager.get().kickMicroPhoneBySdk(
                                        JavaUtil.str2long(account), micPosition, roomInfo.getRoomId(),
                                        targetVipId, targetVipDate, targetIsInvisiable, targetNick)
                                        .subscribe(new BiConsumer<ChatRoomMessage, Throwable>() {

                                            @Override
                                            public void accept(ChatRoomMessage chatRoomMessage, Throwable throwable) throws Exception {
                                                if (throwable == null) {
                                                    IMNetEaseManager.get().addMessagesImmediately(chatRoomMessage);
                                                }

                                            }
                                        });

                            }
                        }


                    }
                });
    }

    /**
     * 拉他上麦
     */
    public static ButtonItem createInviteOnMicItem(final String account) {
        return new ButtonItem(BasicConfig.INSTANCE.getAppContext().getString(R.string.embrace_up_mic),
                new ButtonItem.OnClickListener() {
                    @Override
                    public void onClick() {
                        int freePosition = AvRoomDataManager.get().findFreePosition();
                        LogUtils.d("upMicroPhone", "freePosition:" + freePosition);
                        if (freePosition == Integer.MIN_VALUE) {
                            SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext().getString(R.string.no_empty_mic));
                            return;
                        }
                        Disposable disposable = IMNetEaseManager.get().inviteMicroPhoneBySdk(JavaUtil.str2long(account),
                                freePosition).subscribe(new BiConsumer<ChatRoomMessage, Throwable>() {
                            @Override
                            public void accept(ChatRoomMessage chatRoomMessage, Throwable throwable) throws Exception {
                                if (throwable == null) {
                                    IMNetEaseManager.get().addMessagesImmediately(chatRoomMessage);
                                }
                            }
                        });
                    }
                });
    }

    /**
     * 踢出房间:  先强制下麦，再踢出房间
     */
    public static ButtonItem createKickOutRoomItem(final Context context, final ChatRoomMember chatRoomMember,
                                                   final String roomId, final String account) {
        return new ButtonItem("踢出房间", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                if (chatRoomMember == null) {
                    return;
                }
                if (context instanceof BaseMvpActivity) {
                    ((BaseMvpActivity) context).getDialogManager().showOkCancelDialog(
                            "是否将" + chatRoomMember.getNick() + "踢出房间？", true,
                            new DialogManager.AbsOkDialogListener() {
                                @Override
                                public void onOk() {
                                    //检测是否防踢防拉黑
                                    Map<String, Object> extMap = chatRoomMember.getExtension();
                                    int medalId = 0;
                                    int medalDate = 0;
                                    boolean isInvisible = false;

                                    if (null != extMap) {
                                        if (extMap.containsKey(USER_MEDAL_ID)) {
                                            medalId = (int) extMap.get(USER_MEDAL_ID);
                                        }
                                        if (extMap.containsKey(USER_MEDAL_DATE)) {
                                            medalDate = (int) extMap.get(USER_MEDAL_DATE);
                                        }
                                        if (extMap.containsKey(NOBLE_INVISIABLE_ENTER_ROOM)) {
                                            isInvisible = (int) extMap.get(NOBLE_INVISIABLE_ENTER_ROOM) == 1;
                                        }
                                        if (NobleBusinessManager.isAntiKickOff(medalId, medalDate)) {
                                            ((BaseMvpActivity) context).getDialogManager().showOkCancelDialog(
                                                    context.getString(R.string.room_kick_off_kings_tips),
                                                    context.getString(R.string.sure), null, false, null);
                                            return;
                                        }
                                    }

                                    final Map<String, Object> reason = new HashMap<>(2);
                                    reason.put("reason", "kick");
                                    long currentUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
                                    reason.put("administratorsUid", currentUid + "");
                                    if (AvRoomDataManager.get().isOnMic(JavaUtil.str2long(account))) {
                                        int micPosition = AvRoomDataManager.get().getMicPosition(JavaUtil.str2long(account));
                                        reason.put("micPosition", micPosition);
                                        reason.put("account", account);
                                        IMNetEaseManager.get().downMicroPhoneBySdk(micPosition, null);
                                    }

                                    //判断是否在排麦
                                    Json json = AvRoomDataManager.get().mMicInListMap.get(Integer.parseInt(chatRoomMember.getAccount()));
                                    if (json != null) {
                                        IMNetEaseManager.get().removeMicInList(chatRoomMember.getAccount(), roomId, null);
                                    }

                                    Disposable disposable = IMNetEaseManager.get().kickMemberFromRoomBySdk(JavaUtil.str2long(roomId),
                                            JavaUtil.str2long(account), reason, medalId, medalDate, isInvisible,
                                            chatRoomMember.getNick()).subscribe(new BiConsumer<String, Throwable>() {
                                        @Override
                                        public void accept(String s, Throwable throwable) throws Exception {
                                            if (throwable != null) {
                                                SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), throwable.getMessage());
                                            } else {
                                                LogUtils.e(TAG, "kick out mic and room: " + s);
                                                IMNetEaseManager.get().noticeKickOutChatMember(null, account);
                                            }
                                        }
                                    });
                                }
                            });
                }
            }
        });
    }

    /**
     * 查看资料弹窗
     */
    public static ButtonItem createCheckUserInfoDialogItem(final Context context, final String account,
                                                           boolean gotoUserInfoActivity) {
        return new ButtonItem("查看资料", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                if (gotoUserInfoActivity) {
                    UserInfoActivity.start(context, JavaUtil.str2long(account));
                } else {
                    new UserInfoDialog(context, JavaUtil.str2long(account)).show();
                }

            }
        });
    }

    /**
     * 查看资料弹窗
     */
    public static ButtonItem createChoicePair(final String roomId, final String account, int gender) {
        return new ButtonItem("喜欢Ta", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                UserInfo cacheLoginUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
                if (cacheLoginUserInfo == null) {
                    SingleToastUtil.showShortToast("数据异常");
                    return;
                }
                if (cacheLoginUserInfo.getGender() == gender) {
                    SingleToastUtil.showShortToast("只能选择异性");
                    return;
                }

                CoreManager.getCore(IPairCore.class).choiceLover(account, roomId);
            }
        });
    }

    /**
     * 下麦
     */
    public static ButtonItem createDownMicItem() {
        return new ButtonItem(BasicConfig.INSTANCE.getAppContext().getString(R.string.down_mic_text), new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                long currentUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
                IMNetEaseManager.get().downMicroPhoneBySdk(
                        AvRoomDataManager.get().getMicPosition(currentUid), null);
            }
        });
    }

    //设置管理员
    public static ButtonItem createMarkManagerListItem(final String roomId, final String account, final boolean mark, ChatRoomMember chatRoomMember) {
        String title = BasicConfig.INSTANCE.getAppContext().getString(mark ? R.string.set_manager : R.string.remove_manager);
        return new ButtonItem(title, new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                markManagerListOnServer(roomId, account, mark,
                        new OkHttpManager.MyCallBack<ServiceResult<ServerUserMemberInfo>>() {
                            @Override
                            public void onError(Exception e) {
                                e.printStackTrace();
                                SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), "操作失败，请重试");
                            }

                            @Override
                            public void onResponse(ServiceResult<ServerUserMemberInfo> response) {
                                if (null != response && response.isSuccess()) {
                                    ServerUserMemberInfo serverUserMemberInfo = response.getData();

                                    ChatRoomMember chatRoomMember = new ChatRoomMember();
                                    if (mark) {
                                        IMNetEaseManager.get().addManagerMember(chatRoomMember);
                                    } else {
                                        IMNetEaseManager.get().removeManagerMember(chatRoomMember);
                                    }
                                }
                            }
                        });
            }
        });
    }

    private static void markManagerListOnServer(String roomId, final String account,
                                                final boolean mark,
                                                final OkHttpManager.MyCallBack<ServiceResult<ServerUserMemberInfo>> myCallBack) {
        RoomBaseModel roomBaseModel = new RoomBaseModel();
        if (mark) {
            roomBaseModel.addUserToAdminList(Long.valueOf(account), Long.valueOf(roomId), myCallBack);
        } else {
            roomBaseModel.removeUserFromAdminList(Long.valueOf(account), Long.valueOf(roomId), myCallBack);
        }


    }

    //加入黑名单
    public static ButtonItem createMarkBlackListItem(final Context context, final ChatRoomMember chatRoomMember, final String roomId) {
        LogUtils.d("createMarkBlackListItem", "create");
        return new ButtonItem(context.getResources().getString(R.string.add_user_to_room_black), new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                LogUtils.d("createMarkBlackListItem", "onClick");
                if (null == chatRoomMember) {
                    LogUtils.d("incomingChatObserver", "无用户信息");
                    return;
                }
                DialogManager.AbsOkDialogListener listener = new DialogManager.AbsOkDialogListener() {
                    @Override
                    public void onOk() {
                        addUserToBlackList(context, chatRoomMember, roomId);
                    }
                };
                if (context instanceof BaseMvpActivity) {
                    BaseMvpActivity activity = (BaseMvpActivity) context;
                    String message = activity.getResources().getString(R.string.add_user_to_room_black_tips, chatRoomMember.getNick());
                    activity.getDialogManager().showOkCancelDialog(message, true, listener);
                }
            }
        });
    }

    private static void addUserToBlackList(final Context context, final ChatRoomMember chatRoomMember, final String roomId) {
        //检测是否防踢防拉黑
        Map<String, Object> extMap = chatRoomMember.getExtension();
        int medalId = 0;
        int medalDate = 0;
        boolean isInvisible = false;
        long invisibleUid = 0L;

//        JSONObject jsonObject = new JSONObject();
//        try {
            if (null != extMap) {
                if (extMap.containsKey(USER_MEDAL_ID)) {
                    medalId = (int) extMap.get(USER_MEDAL_ID);
//                    jsonObject.put(USER_MEDAL_ID, medalId);
                }
                if (extMap.containsKey(USER_MEDAL_DATE)) {
                    medalDate = (int) extMap.get(USER_MEDAL_DATE);
//                    jsonObject.put(USER_MEDAL_DATE, medalDate);
                }
                if (extMap.containsKey(NOBLE_INVISIABLE_ENTER_ROOM)) {
                    isInvisible = (int) extMap.get(NOBLE_INVISIABLE_ENTER_ROOM) == 1;
//                    jsonObject.put(NOBLE_INVISIABLE_ENTER_ROOM, (int) extMap.get(NOBLE_INVISIABLE_ENTER_ROOM));
                }
                if (extMap.containsKey(NOBLE_INVISIABLE_UID)) {
                    invisibleUid = (long) extMap.get(NOBLE_INVISIABLE_UID);
//                    jsonObject.put(NOBLE_INVISIABLE_UID, invisibleUid);
                }
            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }


        if (NobleBusinessManager.isAntiKickOff(medalId, medalDate)) {
            DialogManager dialogManager = null;
            if (context instanceof BaseMvpActivity) {
                dialogManager = ((BaseMvpActivity) context).getDialogManager();
                dialogManager.showOkDialog(
                        context.getString(R.string.room_black_kings_tips), null);
            }
            return;
        }

        new RoomBaseModel().addUserToBlackList(Long.valueOf(chatRoomMember.getAccount()),
                Long.valueOf(roomId), new OkHttpManager.MyCallBack<ServiceResult<String>>() {
                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                        SingleToastUtil.showToast(e.getMessage());
                    }

            @Override
            public void onResponse(ServiceResult<String> response) {
                //2.请求服务器修改额外字段
                //3.发送房间自定义通知消息，用于房间公屏展示
                //以上这两个操作放到服务器上来处理
                if (null != response && response.isSuccess()) {
                    //4.判断是否在排麦,在就从麦序列表中移除，并做下麦操作
                    Json json = AvRoomDataManager.get().mMicInListMap.get(Integer.parseInt(chatRoomMember.getAccount()));
                    if (json != null) {
                        IMNetEaseManager.get().removeMicInList(chatRoomMember.getAccount(), roomId, null);
                    }
                    int micPosition = AvRoomDataManager.get().getMicPosition(chatRoomMember.getAccount());
                    Disposable disposable = IMNetEaseManager.get().downMicroPhoneBySdk(micPosition).subscribe();
                    if (null != context && context instanceof BaseMvpActivity) {
                        BaseMvpActivity activity = (BaseMvpActivity) context;
                        activity.mCompositeDisposable.add(disposable);
                    }
                } else if (null != response && !TextUtils.isEmpty(response.getMessage())) {
                    SingleToastUtil.showToast(response.getMessage());
                }
            }
                });
    }

    public static ButtonItem createSendGiftItem(final Context context, String uid, int vipId, int vipDate, boolean isInvisible) {
        ButtonItem buttonItem = new ButtonItem("送礼物", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                showGiftDialog(context, uid, vipId, vipDate, isInvisible);

            }
        });
        return buttonItem;
    }

    public static ButtonItem createSendGiftItem(final Context context, final ChatRoomMember chatRoomMember,
                                                final GiftDialog.OnGiftDialogBtnClickListener giftDialogBtnClickListener,
                                                boolean isRoomToShowLimitedExempteGift) {
        ButtonItem buttonItem = new ButtonItem("送礼物", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                GiftDialog dialog = new GiftDialog(context, AvRoomDataManager.get().mMicQueueMemberMap, chatRoomMember);
                dialog.setRoomId(null == AvRoomDataManager.get().mCurrentRoomInfo ? 0L :
                        AvRoomDataManager.get().mCurrentRoomInfo.getRoomId());
                dialog.setRoomToShowLimitedExempteGift(isRoomToShowLimitedExempteGift);
                if (giftDialogBtnClickListener != null) {
                    dialog.setGiftDialogBtnClickListener(giftDialogBtnClickListener);
                }
                dialog.show();

            }
        });
        return buttonItem;
    }

    /**
     * 禁麦
     */
    public static ButtonItem createLockMicItem(final int position, ButtonItem.OnClickListener onClickListener) {
        return new ButtonItem(BasicConfig.INSTANCE.getAppContext().getString(R.string.forbid_mic), onClickListener);
    }

    /**
     * 取消禁麦
     */
    public static ButtonItem createFreeMicItem(final int position, ButtonItem.OnClickListener onClickListener) {
        return new ButtonItem(BasicConfig.INSTANCE.getAppContext().getString(R.string.no_forbid_mic), onClickListener);
    }

    public static ButtonItem createManagerListItem(final Context context) {
        ButtonItem buttonItem = new ButtonItem("管理员", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                RoomManagerListActivity.start(context);
            }
        });
        return buttonItem;
    }

    public static ButtonItem createBlackListItem(final Context context) {
        ButtonItem buttonItem = new ButtonItem("黑名单", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                RoomBlackListActivity.start(context);
            }
        });
        return buttonItem;
    }

    /**
     * 举报功能按钮
     *
     * @param context
     * @param title   房间内为举报房间  个人为举报
     * @param type    个人 和 房间举报
     * @return
     */
    public static ButtonItem createReportItem(Context context, String title, int type, long uid) {
        ButtonItem buttonItem = new ButtonItem(title, new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                if (context == null) {
                    return;
                }
                if (!TextUtils.isEmpty(title) && title.equals(context.getResources().getString(R.string.user_info_report))) {
                    //资料-举报
                    StatisticManager.get().onEvent(context,
                            StatisticModel.EVENT_ID_USERINFO_REPORT,
                            StatisticModel.getInstance().getUMAnalyCommonMap(context));
                }
                List<ButtonItem> buttons = new ArrayList<>();
                ButtonItem button1 = new ButtonItem("政治敏感", new ButtonItem.OnClickListener() {
                    @Override
                    public void onClick() {
                        reportCommit(context, type, 1, uid);
                    }
                });
                ButtonItem button2 = new ButtonItem("色情低俗", new ButtonItem.OnClickListener() {
                    @Override
                    public void onClick() {
                        reportCommit(context, type, 2, uid);
                    }
                });
                ButtonItem button3 = new ButtonItem("广告骚扰", new ButtonItem.OnClickListener() {
                    @Override
                    public void onClick() {
                        reportCommit(context, type, 3, uid);
                    }
                });
                ButtonItem button4 = new ButtonItem("人身攻击", new ButtonItem.OnClickListener() {
                    @Override
                    public void onClick() {
                        reportCommit(context, type, 4, uid);
                    }
                });
                buttons.add(button1);
                buttons.add(button2);
                buttons.add(button3);
                buttons.add(button4);
                DialogManager dialogManager = null;
                if (context instanceof BaseMvpActivity) {
                    dialogManager = ((BaseMvpActivity) context).getDialogManager();
                } else if (context instanceof BaseActivity) {
                    dialogManager = ((BaseActivity) context).getDialogManager();
                }
                if (dialogManager != null) {
                    dialogManager.showCommonPopupDialog(buttons, "取消");
                }
            }
        });
        return buttonItem;
    }

    /**
     * uid  被举报的用户
     * reportUid  举报的用户
     * reportType 举报类型 1.政治 2.色情 3.广告 4.人身攻击 ',
     * type  		'类型 1. 用户 2. 房间',
     *
     * @param reportType
     */
    public static void reportCommit(Context context, int type, int reportType, long uid) {
        if (context != null) {
            if (context instanceof BaseMvpActivity) {
                ((BaseMvpActivity) context).toast(context.getResources().getString(R.string.report_tips));
            } else if (context instanceof BaseActivity) {
                ((BaseActivity) context).toast(context.getResources().getString(R.string.report_tips));
            }
        }
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("reportType", reportType + "");
        requestParam.put("type", type + "");
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        requestParam.put("phoneNo", userInfo == null ? "" : userInfo.getPhone());
        requestParam.put("reportUid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        requestParam.put("uid", uid + "");
        OkHttpManager.getInstance().postRequest(UriProvider.reportUserUrl(), requestParam, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Json json) {
                LogUtils.d(TAG, "reportCommit-onResponse json:" + json);
            }
        });
    }

    public interface OnItemClick {
        void itemClick();
    }
}
