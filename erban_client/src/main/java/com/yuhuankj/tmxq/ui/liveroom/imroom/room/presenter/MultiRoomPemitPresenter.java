package com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter;

import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomModel;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.MultiRoomInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.bean.GiftWallInfo;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.base.presenter.BaseMvpPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IMultiRoomPemitView;

/**
 * 语音房间页面的presenter
 */
public class MultiRoomPemitPresenter extends BaseMvpPresenter<IMultiRoomPemitView> {

    private final String TAG = MultiRoomPemitPresenter.class.getSimpleName();

    private IMRoomModel imRoomModel;

    private MultiRoomInfo lastMultiRoomInfo = null;

    private boolean isReuqesting = false;

    private long recvGiftNumTotal = 0L;

    public long getRecvGiftNumTotal() {
        return recvGiftNumTotal;
    }

    public MultiRoomPemitPresenter() {
        imRoomModel = new IMRoomModel();
    }

    public void resetLastMultiRoomInfo() {
        lastMultiRoomInfo = null;
    }

    public MultiRoomInfo getLastMultiRoomInfo() {
        return lastMultiRoomInfo;
    }

    public void getMultiRoomInfo() {
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (null == roomInfo) {
            return;
        }

        LogUtils.d(TAG, "getMultiRoomInfo-roomId:" + roomInfo.getRoomId() + " isReuqesting:" + isReuqesting);

        if (isReuqesting) {
            return;
        }
        isReuqesting = true;

        imRoomModel.getMultiRoomInfo(roomInfo.getRoomId(), new HttpRequestCallBack<MultiRoomInfo>() {
            @Override
            public void onSuccess(String message, MultiRoomInfo response) {
                LogUtils.d(TAG, "getMultiRoomInfo-->onSuccess message:" + message);

                lastMultiRoomInfo = response;
                recvGiftNumTotal = 0;
                if (null != response && null != response.getGiftList()) {
                    for (GiftWallInfo giftWallInfo : response.getGiftList()) {
                        recvGiftNumTotal += giftWallInfo.getReciveCount();
                    }
                }

                if (null != getMvpView()) {
                    getMvpView().refreshMultiRoomPemit();
                }
                isReuqesting = false;
            }

            @Override
            public void onFailure(int code, String msg) {
                LogUtils.d(TAG, "getMultiRoomInfo-->onSuccess msg:" + msg + " code:" + code);
                isReuqesting = false;
            }
        });
    }


}
