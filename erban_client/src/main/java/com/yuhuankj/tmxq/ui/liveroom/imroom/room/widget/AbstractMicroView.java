package com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.widget.MicroWaveView;
import com.tongdaxing.xchat_core.im.custom.bean.RoomCharmAttachment;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.manager.RtcEngineManager;
import com.tongdaxing.xchat_core.room.face.FaceReceiveInfo;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.listener.OnMicroItemClickListener;
import com.zego.zegoavkit2.soundlevel.ZegoSoundLevelInfo;

import java.util.List;

/**
 * 房间麦位抽象层
 * 再多一层抽象是为了扩展单人音频方只有房主头像的情况
 *
 * @author weihaitao
 * @date 2019/4/21
 */
public abstract class AbstractMicroView extends RelativeLayout {
    private SparseArray<ImageView> faceImageViews;
    protected OnMicroItemClickListener onMicroItemClickListener;

    public AbstractMicroView(Context context) {
        this(context, null);
    }

    public AbstractMicroView(Context context, AttributeSet attr) {
        this(context, attr, 0);
    }

    public AbstractMicroView(Context context, AttributeSet attr, int i) {
        super(context, attr, i);
        init(context);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        CoreManager.addClient(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        CoreManager.removeClient(this);
    }

    protected SparseArray<ImageView> getFaceImageViews() {
        return faceImageViews;
    }

    public void init(final Context context) {
        inflate(context,getMicroLayoutId(),this);
        faceImageViews = new SparseArray<>();
    }

    protected abstract int getMicroLayoutId();

    protected abstract void calculateMicCenterPoint();

    public abstract void notifyDataSetChanged(int roomEvent);

    public abstract void notifyItemChanged(int position, int roomEvent);

    public void setOnMicroItemClickListener(OnMicroItemClickListener listener) {
        this.onMicroItemClickListener = listener;
    }

    public void release() {
        onMicroItemClickListener = null;
    }

    public void showFaceView(List<FaceReceiveInfo> faceReceiveInfos) {

    }

    public void updateCharmData(RoomCharmAttachment roomCharmAttachment) {

    }

    /**
     * 房间麦位的声浪监听 -- 控制WaveView的开启播放
     * @param micPositionList
     */
    public void onSpeakQueueUpdate(List<Integer> micPositionList){
        for (int i = 0; i < micPositionList.size(); i++) {
            int pos = micPositionList.get(i);
            if (pos == -1) {
                if (getRoomOnerSpeakView()!= null){
                    getRoomOnerSpeakView().start();
                }
            } else if (pos >= 0 && pos < 8) {
                MicroWaveView speakState = getRoomNormalSpeakView(pos);
                if (speakState != null) {
                    speakState.start();
                }
            }
        }
    }


    /**
     * 即构除自己外其他人的声浪监听
     *
     * @param speakers
     */
    public void onZegoSpeakQueueUpdate(List<ZegoSoundLevelInfo> speakers){
        for (int i = 0; i < speakers.size(); i++) {
            int pos = RoomDataManager.get().getMicPositionByStreamID(speakers.get(i).streamID);
            if (pos == -1){
                if (getRoomOnerSpeakView()!= null){
                    getRoomOnerSpeakView().start();
                }
            }else {
                MicroWaveView speakState = getRoomNormalSpeakView(pos);
                if (speakState != null) {
                    if (RtcEngineManager.get().isRemoteMute()) {
                        speakState.stop();
                    } else {
                        if (speakers.get(i).soundLevel > 0) {
                            speakState.start();
                        } else {
                            speakState.stop();
                        }
                    }
                }
            }
        }
    }


    /**
     * 即构除自己的声浪监听
     *
     * @param currentMicPosition    当前麦位逻辑
     * @param currentMicStreamLevel
     */
    public void onCurrentSpeakUpdate(int currentMicPosition, float currentMicStreamLevel){
        if (currentMicPosition == Integer.MIN_VALUE) {
            return;
        }
        if (currentMicPosition == -1){
            if (getRoomOnerSpeakView()!= null){
                getRoomOnerSpeakView().start();
            }
        }else {
            MicroWaveView speakState = getRoomNormalSpeakView(currentMicPosition);
            if (speakState != null) {
                if (RtcEngineManager.get().isMute()) {
                    speakState.stop();
                } else {
                    if (currentMicStreamLevel > 0) {
                        speakState.start();
                    } else {
                        speakState.stop();
                    }
                }
            }
        }
    }

    protected abstract MicroWaveView getRoomOnerSpeakView();

    protected abstract MicroWaveView getRoomNormalSpeakView(int pos);
}
