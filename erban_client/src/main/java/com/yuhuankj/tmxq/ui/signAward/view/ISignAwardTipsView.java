package com.yuhuankj.tmxq.ui.signAward.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.yuhuankj.tmxq.ui.signAward.model.RecvNewsGiftPkgResult;

/**
 * @author weihaitao
 * @date 2019/5/21
 */
public interface ISignAwardTipsView extends IMvpBaseView {

    /**
     * 显示萌新大礼包领取结果弹框
     *
     * @param result
     */
    void showSignAwardResultDialog(RecvNewsGiftPkgResult result);

    void showRecvSignPkgError(String message);
}
