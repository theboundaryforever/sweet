package com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter;

import android.text.TextUtils;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.liveroom.im.model.AudioConnTimeOutTipsQueueScheduler;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomModel;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.AudioConnReqInfo;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.AudioConnReqInfoResp;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomEvent;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IAudioConnectView;

import java.util.ArrayList;
import java.util.List;

/**
 * 房间页面的presenter
 */
public class AudioConnectListPresenter<I extends IMvpBaseView> extends AbstractMvpPresenter<IAudioConnectView> {

    private final String TAG = AudioConnectListPresenter.class.getSimpleName();

    private IMRoomModel imRoomModel;

    private int overCount = 0;
    private int conningCount = 0;
    private String lastReason = "";

    private List<AudioConnReqInfo> audioConnReqInfoAllList = new ArrayList<>();
    private List<AudioConnReqInfo> audioConnReqInfoReqList = new ArrayList<>();

    private AudioConnReqInfo.ApplyStatus mApplyStatus = AudioConnReqInfo.ApplyStatus.Unknow;

    public AudioConnectListPresenter() {
        imRoomModel = new IMRoomModel();
    }

    public AudioConnReqInfo.ApplyStatus getApplyStatus() {
        return mApplyStatus;
    }

    public long getReqingCount() {
        return RoomDataManager.get().singleAudioConnReqNum;
    }

    public int getOverCount() {
        return overCount;
    }

    public int getConningCount() {
        return conningCount;
    }

    public List<AudioConnReqInfo> getAudioConnReqInfoAllList() {
        return audioConnReqInfoAllList;
    }

    public List<AudioConnReqInfo> getAudioConnReqInfoReqList() {
        return audioConnReqInfoReqList;
    }

    public String getLastReason() {
        return lastReason;
    }

    public void getAudioConnReqList() {
        LogUtils.d(TAG, "getAudioConnReqList");
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (null == roomInfo) {
            return;
        }

        imRoomModel.getRoomAudioConnReqList(roomInfo.getRoomId(), new HttpRequestCallBack<AudioConnReqInfoResp>() {

            @Override
            public void onSuccess(String message, AudioConnReqInfoResp response) {
                LogUtils.d(TAG, "getAudioConnReqList-->onSuccess message:" + message);
                if (null == getMvpView()) {
                    return;
                }

                if (null != response) {
                    RoomDataManager.get().singleAudioAnchorConnSwitch = response.openState == 1;
                    if (RoomDataManager.get().isRoomOwner()) {
                        getMvpView().refreshConnSwitchStatus();
                    }

                    mApplyStatus = AudioConnReqInfo.ApplyStatus.Unknow;
                    audioConnReqInfoAllList = new ArrayList<>();
                    audioConnReqInfoReqList = new ArrayList<>();
                    RoomDataManager.get().singleAudioConnReqNum = 0;

                    overCount = 0;
                    conningCount = 0;
                    lastReason = "";
                    if (!ListUtils.isListEmpty(response.micList)) {
                        LogUtils.d(TAG, "getAudioConnReqList-->onSuccess micList.size:" + response.micList.size());
                        for (AudioConnReqInfo audioConnReqInfo : response.micList) {
                            audioConnReqInfo.setApplyStatus(AudioConnReqInfo.ApplyStatus.Conning);

                            //非主播，判断当前是否在队列中
                            if (!RoomDataManager.get().isRoomOwner()
                                    && audioConnReqInfo.getUid() == CoreManager.getCore(IAuthCore.class).getCurrentUid()
                                    && AudioConnReqInfo.ApplyStatus.Unknow == mApplyStatus) {
                                mApplyStatus = AudioConnReqInfo.ApplyStatus.Conning;
                            }
                            audioConnReqInfoAllList.add(audioConnReqInfo);
                            conningCount = response.micList.size();
                        }
                    }

                    if (!ListUtils.isListEmpty(response.applyList)) {
                        LogUtils.d(TAG, "getAudioConnReqList-->onSuccess applyList.size:" + response.applyList.size());
                        for (AudioConnReqInfo audioConnReqInfo : response.applyList) {
                            audioConnReqInfo.setApplyStatus(AudioConnReqInfo.ApplyStatus.Reqing);

                            //非主播，判断当前是否在队列中
                            if (!RoomDataManager.get().isRoomOwner()
                                    && audioConnReqInfo.getUid() == CoreManager.getCore(IAuthCore.class).getCurrentUid()
                                    && AudioConnReqInfo.ApplyStatus.Unknow == mApplyStatus) {
                                mApplyStatus = AudioConnReqInfo.ApplyStatus.Reqing;
                                lastReason = audioConnReqInfo.getApplyContent();
                            }
                            audioConnReqInfoAllList.add(audioConnReqInfo);
                            audioConnReqInfoReqList.add(audioConnReqInfo);

                            RoomDataManager.get().singleAudioConnReqNum = response.applyList.size();
                        }
                    } else {
                        RoomDataManager.get().singleAudioConnReqNum = 0;
                    }

                    if (!ListUtils.isListEmpty(response.overList)) {
                        LogUtils.d(TAG, "getAudioConnReqList-->onSuccess overList.size:" + response.overList.size());
                        for (AudioConnReqInfo audioConnReqInfo : response.overList) {
                            audioConnReqInfo.setApplyStatus(AudioConnReqInfo.ApplyStatus.Over);

                            //非主播，判断当前是否在队列中
                            if (!RoomDataManager.get().isRoomOwner()
                                    && audioConnReqInfo.getUid() == CoreManager.getCore(IAuthCore.class).getCurrentUid()
                                    && AudioConnReqInfo.ApplyStatus.Unknow == mApplyStatus) {
                                mApplyStatus = AudioConnReqInfo.ApplyStatus.Over;
                            }
                            audioConnReqInfoAllList.add(audioConnReqInfo);
                            overCount = response.overList.size();
                        }
                    }

                    //非主播，更新底部操作状态
                    if (!RoomDataManager.get().isRoomOwner()) {
                        getMvpView().refreshOperaStatus(mApplyStatus);
                    }

                    //主播主动更新底部红点数量
                    if (RoomDataManager.get().isRoomOwner()) {
                        IMRoomMessageManager.get().getIMRoomEventObservable().onNext(new IMRoomEvent()
                                .setEvent(IMRoomEvent.ROOM_AUDIO_CONN_REQ_RECV_NEW));
                    }

                    //非空判断，空则继续显示空白页，默认空白页
                    getMvpView().refreshAudioConnReqList();
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                LogUtils.d(TAG, "getAudioConnReqList-->onFailure code:" + code + " msg:" + msg);
                if (null == getMvpView()) {
                    return;
                }

                if (!TextUtils.isEmpty(msg)) {
                    getMvpView().toast(msg);
                }
                //非空判断，空则继续显示空白页，默认空白页
                getMvpView().refreshAudioConnReqList();
            }
        });
    }

    public void callDownAudioConn() {
        LogUtils.d(TAG, "callDownAudioConn");
        callDownAudioConn(CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }

    public void callDownAudioConn(long targetUid) {
        LogUtils.d(TAG, "callDownAudio targetUid:" + targetUid);
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (null == roomInfo) {
            return;
        }
        if (!RoomDataManager.get().isOnMic(targetUid)) {
            LogUtils.d(TAG, "callDownAudio 并未上麦,直接刷新");
            getAudioConnReqList();
            return;
        }
        imRoomModel.callDownAudioConn(targetUid, roomInfo.getRoomId(), new HttpRequestCallBack<String>() {
            @Override
            public void onSuccess(String message, String response) {
                LogUtils.d(TAG, "callDownAudio-->onSuccess message:" + message + " response:" + response);
                if (null != getMvpView()) {
                    //提示
                    if (!TextUtils.isEmpty(response)) {
                        getMvpView().toast(response);
                    }
                    //刷新列表
                    getAudioConnReqList();
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                LogUtils.d(TAG, "callDownAudio-->onFailure code:" + code + " msg:" + msg);
                if (null == getMvpView()) {
                    return;
                }

                if (!TextUtils.isEmpty(msg)) {
                    getMvpView().toast(msg);
                }
                //非空判断，空则继续显示空白页，默认空白页
                getMvpView().refreshAudioConnReqList();
            }
        });
    }

    public void connAudioReq(long targetUid) {
        LogUtils.d(TAG, "connAudioReq targetUid:" + targetUid);
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (null == roomInfo) {
            return;
        }
        if (RoomDataManager.get().isOnMic(targetUid)) {
            //房主主播点击连接时，其他管理员已经将该用户抱上麦位，为了房主主播端会多次刷新，列表并未主动刷新更改状态，因此此时点击连接，应该判断一下,已经在麦上，则直接刷新
            LogUtils.d(TAG, "callDownAudio 已经上麦,直接刷新");
            getAudioConnReqList();
            return;
        }

        //判断麦位是否已满的逻辑扔给后端一并处理

        imRoomModel.connAudioReq(targetUid, roomInfo.getRoomId(), new HttpRequestCallBack<String>() {
            @Override
            public void onSuccess(String message, String response) {
                LogUtils.d(TAG, "connAudioReq-->onSuccess message:" + message + " response:" + response);
                if (null != getMvpView()) {
                    //提示
                    if (!TextUtils.isEmpty(response)) {
                        getMvpView().toast(response);
                    }
                    AudioConnTimeOutTipsQueueScheduler.getInstance().startDelayTipsRunnable(targetUid);
                    //刷新列表
                    getAudioConnReqList();
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                LogUtils.d(TAG, "connAudioReq-->onFailure code:" + code + " msg:" + msg);
                if (null == getMvpView()) {
                    return;
                }

                if (!TextUtils.isEmpty(msg)) {
                    getMvpView().toast(msg);
                }
                if (code == 3013) {
                    //当前用户申请信息不存在，可能结果为用户已经取消了申请，主播端未能刷新
                    getAudioConnReqList();
                    return;
                }
                //非空判断，空则继续显示空白页，默认空白页
                getMvpView().refreshAudioConnReqList();
            }
        });
    }

    public void reqAudioConn(String applyContent, long goldNum) {
        LogUtils.d(TAG, "reqAudioConn-applyContent:" + applyContent + " goldNum:" + goldNum);
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (null == roomInfo) {
            return;
        }
        imRoomModel.reqAudioConn(applyContent, goldNum, roomInfo.getRoomId(), new HttpRequestCallBack<String>() {
            @Override
            public void onSuccess(String message, String response) {
                LogUtils.d(TAG, "reqAudioConn-->onSuccess message:" + message + " response:" + response);
                if (null != getMvpView()) {
                    //提示
                    if (!TextUtils.isEmpty(response)) {
                        getMvpView().toast(response);
                    }
                    //刷新列表
                    getAudioConnReqList();
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                LogUtils.d(TAG, "reqAudioConn-->onFailure code:" + code + " msg:" + msg);
                if (null == getMvpView()) {
                    return;
                }

                if (!TextUtils.isEmpty(msg)) {
                    getMvpView().toast(msg);
                }
                //非空判断，空则继续显示空白页，默认空白页
                getMvpView().refreshAudioConnReqList();
            }
        });
    }

    public void cancelAudioConnReq() {
        LogUtils.d(TAG, "cancelAudioConnReq");
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (null == roomInfo) {
            return;
        }
        imRoomModel.cancelAudioConnReq(roomInfo.getRoomId(), new HttpRequestCallBack<String>() {
            @Override
            public void onSuccess(String message, String response) {
                LogUtils.d(TAG, "cancelAudioConnReq-->onSuccess message:" + message + " response:" + response);
                if (null != getMvpView()) {
                    //提示
                    if (!TextUtils.isEmpty(response)) {
                        getMvpView().toast(response);
                    }
                    //刷新列表
                    getAudioConnReqList();
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                LogUtils.d(TAG, "cancelAudioConnReq-->onFailure code:" + code + " msg:" + msg);
                if (null == getMvpView()) {
                    return;
                }

                if (!TextUtils.isEmpty(msg)) {
                    getMvpView().toast(msg);
                }
                //非空判断，空则继续显示空白页，默认空白页
                getMvpView().refreshAudioConnReqList();
            }
        });
    }

    public void changeAudioConnSwitch() {
        LogUtils.d(TAG, "changeAudioConnSwitch");
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (null == roomInfo) {
            return;
        }
        boolean modifySwitch = !RoomDataManager.get().singleAudioAnchorConnSwitch;
        LogUtils.d(TAG, "changeAudioConnSwitch-modifySwitch:" + modifySwitch);
        imRoomModel.changeAudioConnSwitch(modifySwitch, roomInfo.getRoomId(), new HttpRequestCallBack<String>() {
            @Override
            public void onSuccess(String message, String response) {
                LogUtils.d(TAG, "changeAudioConnSwitch-->onSuccess message:" + message + " response:" + response);
                if (null != getMvpView()) {
                    //提示
                    if (!TextUtils.isEmpty(response)) {
                        getMvpView().toast(response);
                    }
                    //刷新列表
                    getAudioConnReqList();
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                LogUtils.d(TAG, "changeAudioConnSwitch-->onFailure code:" + code + " msg:" + msg);
                if (null == getMvpView()) {
                    return;
                }

                if (!TextUtils.isEmpty(msg)) {
                    getMvpView().toast(msg);
                }
                //非空判断，空则继续显示空白页，默认空白页
                getMvpView().refreshAudioConnReqList();
            }
        });
    }
}
