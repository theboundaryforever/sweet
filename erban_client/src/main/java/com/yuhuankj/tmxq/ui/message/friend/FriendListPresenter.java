package com.yuhuankj.tmxq.ui.message.friend;

import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCore;

import java.util.List;


public class FriendListPresenter extends AbstractMvpPresenter<FriendListView> {

    private final String TAG = FriendListPresenter.class.getSimpleName();


    public FriendListPresenter() {
    }


    public void getFriendList(int pageNum, int pageSize) {
        LogUtils.d(TAG, "getFriendList-pageNum:" + pageNum + " pageSize:" + pageSize);
        List<NimUserInfo> userInfos = CoreManager.getCore(IIMFriendCore.class).getMyFriends();
        if (null != getMvpView()) {
            if (!ListUtils.isListEmpty(userInfos)) {
                getMvpView().showFriendList(userInfos);
            } else {
                getMvpView().showFriendListEmptyView();
            }
        }
    }

}
