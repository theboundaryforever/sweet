package com.yuhuankj.tmxq.ui.user.other;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

/**
 * Created by chenran on 2017/10/17.
 */

public class UserPropAdapter extends BaseUserPropAdapter {

    private List<UserPropInfo> userPropInfoList;
    private Context context;

    public UserPropAdapter(Context context) {
        this.context = context;
    }


    @Override
    public UserPropInfoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.list_item_gift_wall_info, parent, false);
        return new UserPropInfoHolder(item);
    }

    @Override
    public void onBindViewHolder(UserPropInfoHolder holder, int position) {
        UserPropInfo userPropInfo = userPropInfoList.get(position);
        holder.giftName.setText(userPropInfo.getPropName());
        holder.giftNum.setText(null);
        ImageLoadUtils.loadImage(context, userPropInfo.getUrl(), holder.giftPic);
    }

    @Override
    public int getItemCount() {
        if (userPropInfoList == null)
            return 0;
        else {
            return userPropInfoList.size();
        }
    }

    @Override
    public void setUserPropInfoList(List<UserPropInfo> userPropInfoList) {
        this.userPropInfoList = userPropInfoList;
        notifyDataSetChanged();
    }
}
