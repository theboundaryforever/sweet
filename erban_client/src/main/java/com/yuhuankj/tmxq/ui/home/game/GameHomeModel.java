package com.yuhuankj.tmxq.ui.home.game;

import android.text.TextUtils;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.agora.AgoraEngineManager;
import com.tongdaxing.xchat_core.user.VersionsCore;

import java.util.Map;

/**
 * @author liaoxy
 * @Description:游戏大厅model
 * @date 2019/2/14 11:34
 */
public class GameHomeModel {

    //获取游戏大厅banner
    public void gameBanner(OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.gameBanner(), params, callBack);
    }

    //获取游戏大厅图标
    public void gameIcons(OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.gameIcons(), params, callBack);
    }

    //获取游戏大厅房间列表
    public void gameTops(int size, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("size", size + "");
        OkHttpManager.getInstance().getRequest(UriProvider.gameTops(), params, callBack);
    }

    // 接受连麦
    public void agreeLinkMacro(String linkUid, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("linkUid", linkUid);
        OkHttpManager.getInstance().postRequest(UriProvider.agreeLinkMacro(), params, callBack);
    }

    // 断开连麦
    public void finishLinkMacro(String linkUid, String talkTime, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("linkUid", linkUid);
        params.put("talkTime", talkTime);
        OkHttpManager.getInstance().postRequest(UriProvider.finishLinkMacro(), params, callBack);
    }

    // 取消连麦
    public void cancelLinkMacro(String linkUid, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("linkUid", linkUid);
        OkHttpManager.getInstance().postRequest(UriProvider.cancelLinkMacro(), params, callBack);
    }

    // 邀请连麦
    public void invitationLinkMacro(String linkUid, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("linkUid", linkUid);
        OkHttpManager.getInstance().postRequest(UriProvider.invitationLinkMacro(), params, callBack);
    }

    // 拒绝连麦
    public void refuseLinkMacro(String linkUid, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("linkUid", linkUid);
        OkHttpManager.getInstance().postRequest(UriProvider.refuseLinkMacro(), params, callBack);
    }

    // 连麦异常
    //1:超时 2:占线
    public void linkMacroException(String linkUid, int type, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("linkUid", linkUid);
        params.put("type", type + "");
        OkHttpManager.getInstance().postRequest(UriProvider.linkMacroException(), params, callBack);
    }

    public void enterLinkMacro(String roomId) {
        boolean dynamicKeyOption = CoreManager.getCore(VersionsCore.class).getDynamicKeyOption();
        if (dynamicKeyOption) {
            getAgoreKeyFromServer(roomId);
        } else {
            long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
            AgoraEngineManager.get().joinLinkMacro(null, roomId + "", (int) uid);
        }
    }

    /**
     * 获取声网动态key
     *
     * @param roomId
     */
    private void getAgoreKeyFromServer(String roomId) {
        Map<String, String> param = OkHttpManager.getDefaultParam();
        param.put("roomId", roomId);
        param.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        OkHttpManager.getInstance().postRequest(UriProvider.getAgoraKeyUri(), param, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                SingleToastUtil.showToast(e.getMessage());
            }

            @Override
            public void onResponse(Json response) {
                boolean isRequestSuccess = response.num("code") == 200;
                String token = response.str("data");
                String message = response.str("message");
                if (isRequestSuccess && !TextUtils.isEmpty(token)) {
                    long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
                    AgoraEngineManager.get().joinLinkMacro(token, roomId + "", (int) uid);
                } else {
                    if (!TextUtils.isEmpty(message)) {
                        SingleToastUtil.showToast(message);
                    }
                }
            }
        });
    }

    /**
     * 获取未读消息
     *
     * @param callBack
     */
    public void getUnreadMsgCount(OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().postRequest(UriProvider.getUnreadMsgCount(), params, callBack);
    }
}
