package com.yuhuankj.tmxq.ui.login;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.xchat_core.auth.AccountInfo;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderMiniGameInvited;

/**
 * @author liaoxy
 * @Description:手机号登录页
 * @date 2019/4/10 19:22
 */
public class LoginPhoneActivity extends BaseActivity implements View.OnClickListener {
    private EditText edtPhone;
    private EditText edtPwd;
    private TextView tvLogin;
    private String phone, pwd;
    private LinearLayout llVerifyCode;
    private RelativeLayout rlActionTip;
    private TextView tvRegister;
    private TextView tvForgetPwd;
    private View vClearText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        edtPhone = (EditText) findViewById(R.id.et_phone);
        GrowingIO.getInstance().trackEditText(edtPhone);
        edtPwd = (EditText) findViewById(R.id.et_password);
        GrowingIO.getInstance().trackEditText(edtPwd);
        tvLogin = (TextView) findViewById(R.id.btn_regist);
        llVerifyCode = (LinearLayout) findViewById(R.id.code_layout);
        rlActionTip = (RelativeLayout) findViewById(R.id.rlActionTip);
        tvRegister = (TextView) findViewById(R.id.tvRegister);
        tvForgetPwd = (TextView) findViewById(R.id.tvForgetPwd);

        vClearText = findViewById(R.id.vClearText);
    }

    private void initListener() {
        tvLogin.setOnClickListener(this);
        tvRegister.setOnClickListener(this);
        tvForgetPwd.setOnClickListener(this);
        vClearText.setOnClickListener(this);
        edtPhone.addTextChangedListener(textWatcher);
        edtPhone.setOnFocusChangeListener(focusChangeListener);
    }

    private void initData() {
        initTitleBar("登录");
        mTitleBar.setCommonBackgroundColor(Color.WHITE);
        llVerifyCode.setVisibility(View.GONE);
        tvLogin.setText("立即登录");
        edtPhone.setHint("请输入账号ID/手机号");
    }

    //参数检查
    private boolean check() {
        phone = edtPhone.getText().toString();
        pwd = edtPwd.getText().toString();
        if (TextUtils.isEmpty(phone)) {
            toast("请输入手机号");
            return false;
        }
        if (TextUtils.isEmpty(pwd)) {
            toast("密码不能为空");
            return false;
        }
        if (pwd.length() < 6) {
            toast("请输入6~16位的密码");
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view == tvLogin) {
            if (check()) {
                CoreManager.getCore(IAuthCore.class).login(phone, pwd);
                getDialogManager().showProgressDialog(this, getResources().getString(R.string.logining),
                        true, true, null);
            }
            //点击立即登录
            StatisticManager.get().onEvent(this,
                    StatisticModel.EVENT_ID_CLICK_LOGIN_NOW,
                    StatisticModel.getInstance().getUMAnalyCommonMap(this));
        } else if (view == tvRegister) {
            startActivity(new Intent(this, RegisterActivity.class));
            //点击立即注册
            StatisticManager.get().onEvent(this,
                    StatisticModel.EVENT_ID_CLICK_REGISTE_NOW,
                    StatisticModel.getInstance().getUMAnalyCommonMap(this));
        } else if (view == tvForgetPwd) {
            startActivity(new Intent(this, ForgetPswActivity.class));
        } else if (view == vClearText) {
            edtPhone.setText("");
        }
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLoginFail(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogin(AccountInfo accountInfo) {
        getDialogManager().dismissDialog();
        //登录成功，清空小游戏邀请消息
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(RegisterActivity.ACTION_FINISH_REGISTER));
        MsgViewHolderMiniGameInvited.clear();
        finish();
    }

    private View.OnFocusChangeListener focusChangeListener=new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if(hasFocus){
                String nickName = edtPhone.getText().toString();
                if (TextUtils.isEmpty(nickName) || nickName.length() == 0) {
                    vClearText.setVisibility(View.GONE);
                } else {
                    vClearText.setVisibility(View.VISIBLE);
                }
            }else {
                vClearText.setVisibility(View.GONE);
            }
        }
    };

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String nickName = edtPhone.getText().toString();
            if (TextUtils.isEmpty(nickName) || nickName.length() == 0) {
                vClearText.setVisibility(View.GONE);
            } else {
                vClearText.setVisibility(View.VISIBLE);
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (edtPhone != null){
            edtPhone.removeTextChangedListener(textWatcher);
            edtPhone.setOnFocusChangeListener(null);
        }
    }

}
