package com.yuhuankj.tmxq.ui.user.bosonFriend;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.netease.nim.uikit.common.util.sys.NetworkUtil;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.user.bean.BosonFriendEnitity;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.ui.nim.sysmsg.SysMsgEnitity;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;
import com.yuhuankj.tmxq.widget.TitleBar;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liaoxy
 * @Description:挚友列表
 * @date 2019/5/5 14:53
 */
public class BosonFriendActivity extends BaseActivity {
    private List<BosonFriendEnitity> datas;
    private LinearLayout llEmply;
    private TextView tvEmplyTip;
    private RecyclerView rcvDatas;
    private BosonFriendAdapter adapter;
    private SwipeRefreshLayout sprContent;
    private int page = Constants.PAGE_START;
    private long uid;
    private boolean isSelf = false;

    private SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            if (NetworkUtil.isNetAvailable(BosonFriendActivity.this)) {
                page = Constants.PAGE_START;
                getBosonFriends(uid);
            } else {
                sprContent.setRefreshing(false);
            }
        }
    };

    private BaseQuickAdapter.RequestLoadMoreListener loadMoreListener = new BaseQuickAdapter.RequestLoadMoreListener() {
        @Override
        public void onLoadMoreRequested() {
            // 解决数据重复的问题
            if (page == Constants.PAGE_START) {
                return;
            }
            if (NetworkUtil.isNetAvailable(BosonFriendActivity.this)) {
                if (datas.size() >= Constants.PAGE_SIZE) {
                    getBosonFriends(uid);
                } else {
                    adapter.setEnableLoadMore(false);
                }
            } else {
                adapter.loadMoreEnd(true);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boson_friend);
        initTitleBar("挚友");
        Intent intent = getIntent();
        if (intent == null) {
            toast("挚友ID为空");
            finish();
            return;
        }
        uid = intent.getLongExtra("uid", -1);
        if (uid == -1) {
            toast("挚友ID为空");
            finish();
            return;
        }
        long myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();

        mTitleBar.setCommonBackgroundColor(Color.WHITE);
        if (myUid == uid) {
            isSelf = true;
            mTitleBar.addAction(new TitleBar.TextAction("历史") {
                @Override
                public void performAction(View view) {
                    startActivity(new Intent(BosonFriendActivity.this, BosonFriendHistoryActivity.class));
                }
            });
        } else {
            isSelf = false;
        }
        initView();
        tvEmplyTip.setText(getResources().getString(myUid == uid ? R.string.boson_friend_empty_tips_self
                : R.string.boson_friend_empty_tips_other));
        initData();
    }

    private void initView() {
        sprContent = (SwipeRefreshLayout) findViewById(R.id.sprContent);
        rcvDatas = (RecyclerView) findViewById(R.id.rcvFriends);
        llEmply = (LinearLayout) findViewById(R.id.llEmply);
        tvEmplyTip = (TextView) findViewById(R.id.tvEmplyTip);

        sprContent.setOnRefreshListener(onRefreshListener);
        findViewById(R.id.bltvAbout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonWebViewActivity.start(BosonFriendActivity.this, UriProvider.getBosonFriendAbout());
            }
        });
    }

    private void initData() {
        GridLayoutManager layoutmanager = new GridLayoutManager(this, 2);
        layoutmanager.setOrientation(LinearLayoutManager.VERTICAL);
        rcvDatas.setLayoutManager(layoutmanager);
        datas = new ArrayList<>();

        llEmply.setVisibility(View.GONE);
        adapter = new BosonFriendAdapter(datas);
        adapter.setSelf(isSelf);
        rcvDatas.setAdapter(adapter);
        adapter.setOnLoadMoreListener(loadMoreListener, rcvDatas);
        adapter.setEnableLoadMore(false);
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                try {
                    BosonFriendEnitity msgEnitity = datas.get(position);
                    UserInfoActivity.start(BosonFriendActivity.this, msgEnitity.getfUid());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        adapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                if (view != null && view.getId() == R.id.tvRemove) {
                    getDialogManager().showOkCancelDialog("是否解除挚友关系?", true, new DialogManager.OkCancelDialogListener() {
                        @Override
                        public void onCancel() {

                        }

                        @Override
                        public void onOk() {
                            try {
                                BosonFriendEnitity enitity = datas.get(position);
                                unbindBosonFriend(enitity);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });
        getBosonFriends(uid);
    }

    private void getBosonFriends(long uid) {
        new BosonFriendModel().getBosonFriends(uid, page, Constants.PAGE_SIZE, new OkHttpManager.MyCallBack<ServiceResult<List<BosonFriendEnitity>>>() {

            @Override
            public void onError(Exception e) {
                sprContent.setRefreshing(false);
                adapter.loadMoreComplete();
                SingleToastUtil.showToast("获取挚友列表失败");
            }

            @Override
            public void onResponse(ServiceResult<List<BosonFriendEnitity>> response) {
                sprContent.setRefreshing(false);
                adapter.loadMoreComplete();
                if (response == null || response.getData() == null) {
                    SingleToastUtil.showToast("获取挚友列表失败");
                    return;
                }
                if (!response.isSuccess()) {
                    SingleToastUtil.showToast(response.getErrorMessage());
                    return;
                }
                List<BosonFriendEnitity> tmps = response.getData();
                if (tmps.size() >= Constants.PAGE_SIZE) {
                    adapter.setEnableLoadMore(true);
                    page++;
                } else {
                    adapter.setEnableLoadMore(false);
                }
                if (page == Constants.PAGE_START) {
                    datas.clear();
                }
                datas.addAll(tmps);
                adapter.notifyDataSetChanged();
                if (datas.size() == 0) {
                    llEmply.setVisibility(View.VISIBLE);
                } else {
                    llEmply.setVisibility(View.GONE);
                }
            }
        });
    }

    private void unbindBosonFriend(BosonFriendEnitity enitity) {
        getDialogManager().showProgressDialog(this, "请稍候...");
        new BosonFriendModel().unbindBosonFriend(enitity.getfUid(), new OkHttpManager.MyCallBack<ServiceResult<List<SysMsgEnitity>>>() {

            @Override
            public void onError(Exception e) {
                getDialogManager().dismissDialog();
                SingleToastUtil.showToast(e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<List<SysMsgEnitity>> response) {
                getDialogManager().dismissDialog();
                if (response == null) {
                    SingleToastUtil.showToast("操作失败");
                    return;
                }
                if (!response.isSuccess()) {
                    SingleToastUtil.showToast(response.getMessage());
                } else {
                    try {
                        datas.remove(enitity);
                        adapter.notifyDataSetChanged();
                        if (datas.size() == 0) {
                            llEmply.setVisibility(View.VISIBLE);
                        } else {
                            llEmply.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}
