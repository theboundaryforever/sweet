package com.yuhuankj.tmxq.ui.quickmating;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.netease.nim.uikit.session.OfficSecrRedPointCountManager;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.Observer;
import com.netease.nimlib.sdk.msg.MsgServiceObserve;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.QuickMatingActionAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.QuickMatingAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.QuickMatingAttentionAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.QuickMatingBean;
import com.tongdaxing.xchat_core.im.custom.bean.QuickMatingFinishAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.QuickMatingFinishBean;
import com.tongdaxing.xchat_core.im.custom.bean.QuickMatingRenewAttachment;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.thirdsdk.nim.MsgCenterRedPointStatusManager;
import com.yuhuankj.tmxq.ui.find.bean.FindHotRoomEnitity;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.widget.ButtonItemFactory;
import com.yuhuankj.tmxq.ui.me.taskcenter.view.TaskCenterActivity;
import com.yuhuankj.tmxq.ui.quickmating.bean.QMInfoBean;
import com.yuhuankj.tmxq.ui.quickmating.bean.RoomBean;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.ArrayList;
import java.util.List;

public class QuickMatingHomeActivity extends BaseActivity {
    //完成页面相关UI
    private ImageView imvClose;
    private RelativeLayout rlQuickMatingFinish;
    private TextView tvFinishDouziCount;
    private TextView tvFinishReport;
    private ImageView imvFinishOpposite;
    private TextView tvOppositeNick;
    private TextView tvGetFlowerCount;
    private ImageView imvDislike;
    private ImageView imvLike;
    private RelativeLayout rlFinishContinue;
    private TextView tvFinishTimes;
    public int gender = 0;//结束页选择的性别
    private long oppositeUid;
    private ImageView imvFilter;
    private GenderDialog genderDialog;
    private QuickMatingFragment quickMatingFragment;
    private QuickMatingLinkingFragment quickMatingLinkingFragment;
    private boolean isFinishAnimaing = false;//正在进行结束动画
    private RoomBean roomBean;//匹配超时后随机进入的房间信息

    public static void start(Context context, ArrayList<FindHotRoomEnitity> rooms) {
        Intent intent = new Intent(context, QuickMatingHomeActivity.class);
        intent.putExtra("rooms", rooms);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quick_mating_home);
        initData();
        initView();
    }

    private void initData() {
        setSwipeBackEnable(false);
        quickMatingFragment = new QuickMatingFragment();
        quickMatingLinkingFragment = new QuickMatingLinkingFragment();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fryMain, quickMatingFragment)
                .add(R.id.fryMain, quickMatingLinkingFragment)
                .commitNowAllowingStateLoss();
        showQuickMating();
        //接收连麦消息
        NIMClient.getService(MsgServiceObserve.class).observeReceiveMessage(observer, true);
    }

    //IM消息监听
    private Observer observer = new Observer<List<IMMessage>>() {
        @Override
        public void onEvent(List<IMMessage> imMessages) {
            for (IMMessage msg : imMessages) {
                if (msg.getMsgType() == MsgTypeEnum.custom) {
                    CustomAttachment customAttachment = ((CustomAttachment) msg.getAttachment());
                    if (customAttachment == null) {
                        continue;
                    }
                    if (customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_QUICK_MATING_FIRST) {
                        try {
                            OfficSecrRedPointCountManager.addRedCountDelNum();
                            MsgCenterRedPointStatusManager.getInstance().onRecvNewLikeNotify();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (customAttachment instanceof QuickMatingAttachment) {
                        //速配成功进入频道
                        QuickMatingBean quickMatingBean = ((QuickMatingAttachment) customAttachment).getDataInfo();
                        if (quickMatingBean == null) {
                            continue;
                        }
                        quickMatingFragment.enterLinkMicro(quickMatingBean);
                    } else if (customAttachment instanceof QuickMatingActionAttachment) {
                        QuickMatingActionAttachment quickMatingAction = ((QuickMatingActionAttachment) customAttachment);
                        quickMatingLinkingFragment.receiveActionIM(quickMatingAction.getType(), quickMatingAction.getContent());
                    } else if (customAttachment instanceof QuickMatingFinishAttachment) {
                        QuickMatingFinishAttachment qmFinishAttachment = (QuickMatingFinishAttachment) customAttachment;
                        QuickMatingFinishBean qmBean = qmFinishAttachment.getDataInfo();
                        quickMatingLinkingFragment.updateQMData(qmBean);
                        quickMatingLinkingFragment.exitLinkMacro(false);
                    } else if (customAttachment instanceof QuickMatingRenewAttachment) {
                        quickMatingLinkingFragment.renewTime();
                    } else if (customAttachment instanceof QuickMatingAttentionAttachment) {
                        quickMatingLinkingFragment.attentionMe();
                    }
                }
            }
        }
    };

    private void initView() {
        //完成页面相关UI
        rlQuickMatingFinish = (RelativeLayout) findViewById(R.id.rlQuickMatingFinish);
        imvClose = (ImageView) findViewById(R.id.imvClose);
        tvFinishDouziCount = (TextView) findViewById(R.id.tvFinishDouziCount);
        tvFinishReport = (TextView) findViewById(R.id.tvFinishReport);
        imvFinishOpposite = (ImageView) findViewById(R.id.imvFinishOpposite);
        tvOppositeNick = (TextView) findViewById(R.id.tvOppositeNick);
        tvGetFlowerCount = (TextView) findViewById(R.id.tvGetFlowerCount);
        imvDislike = (ImageView) findViewById(R.id.imvDislike);
        imvLike = (ImageView) findViewById(R.id.imvLike);
        rlFinishContinue = (RelativeLayout) findViewById(R.id.rlFinishContinue);
        tvFinishTimes = (TextView) findViewById(R.id.tvFinishTimes);
        imvFilter = (ImageView) findViewById(R.id.imvFilter);

        //完成页面相关
        tvFinishDouziCount.setOnClickListener(this);
        tvFinishReport.setOnClickListener(this);
        imvDislike.setOnClickListener(this);
        imvLike.setOnClickListener(this);
        rlFinishContinue.setOnClickListener(this);
        imvFilter.setOnClickListener(this);
        rlQuickMatingFinish.setOnClickListener(this);
        imvClose.setOnClickListener(this);
        imvFinishOpposite.setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        NIMClient.getService(MsgServiceObserve.class).observeReceiveMessage(observer, false);
    }

    private void showReport(long uid, int type, Context context) {
        List<ButtonItem> buttons = new ArrayList<>();
        ButtonItem button1 = new ButtonItem("政治敏感", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                ButtonItemFactory.reportCommit(context, type, 1, uid);
            }
        });
        ButtonItem button2 = new ButtonItem("色情低俗", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                ButtonItemFactory.reportCommit(context, type, 2, uid);
            }
        });
        ButtonItem button3 = new ButtonItem("广告骚扰", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                ButtonItemFactory.reportCommit(context, type, 3, uid);
            }
        });
        ButtonItem button4 = new ButtonItem("人身攻击", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                ButtonItemFactory.reportCommit(context, type, 4, uid);
            }
        });
        buttons.add(button1);
        buttons.add(button2);
        buttons.add(button3);
        buttons.add(button4);
        DialogManager dialogManager = getDialogManager();
        if (dialogManager != null) {
            dialogManager.showCommonPopupDialog(buttons, "取消");
        }
    }

    //显示评价动画
    private void commentAnimation(boolean isLike) {
        float toX = -1f;
        if (!isLike) {
            toX = 1f;
        }
        TranslateAnimation translateAni = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0f, Animation.RELATIVE_TO_SELF,
                toX, Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_SELF, 0);
        translateAni.setDuration(500);
        translateAni.setRepeatCount(0);
        translateAni.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (isLike) {
                    imvDislike.setVisibility(View.GONE);
                    imvLike.clearAnimation();
                } else {
                    imvLike.setVisibility(View.GONE);
                    imvDislike.clearAnimation();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        if (isLike) {
            imvLike.setSelected(true);
            imvLike.setClickable(false);
            imvDislike.setVisibility(View.INVISIBLE);
            imvLike.startAnimation(translateAni);
        } else {
            imvDislike.setSelected(true);
            imvDislike.setClickable(false);
            imvLike.setVisibility(View.INVISIBLE);
            imvDislike.startAnimation(translateAni);
        }
    }

    //评价,只要点赞就可以了
    private void like() {
        new QuickMatingModel().like(oppositeUid, 1, new HttpRequestCallBack<String>() {

            @Override
            public void onSuccess(String message, String response) {
            }

            @Override
            public void onFailure(int code, String msg) {
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (quickMatingLinkingFragment != null && quickMatingLinkingFragment.isLinkMating()) {
            getDialogManager().showOkCancelDialog(getResources().getString(R.string.link_mic_exit_tips),
                    true, new DialogManager.OkCancelDialogListener() {
                        @Override
                        public void onCancel() {
                        }

                        @Override
                        public void onOk() {
                            getDialogManager().showProgressDialog(QuickMatingHomeActivity.this, "请稍候...");
                            quickMatingLinkingFragment.forceEndQuickMating(new HttpRequestCallBack<String>() {
                                @Override
                                public void onSuccess(String message, String response) {
                                    getDialogManager().dismissDialog();
                                    finish();
                                }

                                @Override
                                public void onFailure(int code, String msg) {
                                    getDialogManager().dismissDialog();
                                    finish();
                                }
                            });
                        }
                    });
            return;
        } else if (isFinishAnimaing) {
            return;
        } else if (rlQuickMatingFinish.isShown()) {
            hideFinish(false);
            return;
        }
        super.onBackPressed();
    }

    public void showQuickMating() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.hide(quickMatingLinkingFragment)
                .show(quickMatingFragment)
                .commitNowAllowingStateLoss();
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view == imvClose) {
            hideFinish(false);
        } else if (view == tvFinishDouziCount) {
            startActivity(new Intent(this, TaskCenterActivity.class));
        } else if (view == tvFinishReport) {
            showReport(oppositeUid, 1, this);
        } else if (view == imvDislike) {
            //统计-交友速配-苦脸
            StatisticManager.get().onEvent(this,
                    StatisticModel.EVENT_ID_CLICK_QM_DISLIKE,
                    StatisticModel.getInstance().getUMAnalyCommonMap(this));
            commentAnimation(false);
        } else if (view == imvLike) {
            //统计-交友速配-点赞
            StatisticManager.get().onEvent(this,
                    StatisticModel.EVENT_ID_CLICK_QM_LIKE,
                    StatisticModel.getInstance().getUMAnalyCommonMap(this));
            like();
            commentAnimation(true);
        } else if (view == rlFinishContinue) {
            //统计-交友速配-继续匹配
            StatisticManager.get().onEvent(this,
                    StatisticModel.EVENT_ID_CLICK_QM_CONTINUE_MATCHING,
                    StatisticModel.getInstance().getUMAnalyCommonMap(this));
            hideFinish(true);
        } else if (view == imvFilter) {
            if (genderDialog != null && genderDialog.isShowing()) {
                return;
            }
            genderDialog = new GenderDialog(this, gender);
            genderDialog.setOnGenderListener(new GenderDialog.OnGenderListener() {
                @Override
                public void onGender(int gender) {
                    QuickMatingHomeActivity.this.gender = gender;
                }
            });
            genderDialog.show();
        } else if (view == rlQuickMatingFinish) {

        } else if (view == imvFinishOpposite) {
            //统计-交友速配-进入对方主页
            StatisticManager.get().onEvent(this,
                    StatisticModel.EVENT_ID_CLICK_QM_ENTER_USERPAGE,
                    StatisticModel.getInstance().getUMAnalyCommonMap(this));
            UserInfoActivity.start(this, oppositeUid, 2);
        }
    }

    public void startQuickMating(int gender) {
        showQuickMating();
        quickMatingFragment.startQuickMatingIf(gender);
    }

    public void updateDouziCount(QMInfoBean qmInfoBean) {
        if (tvFinishDouziCount != null && qmInfoBean != null) {
            tvFinishDouziCount.setText(qmInfoBean.getPeaNum() + "");
        }
        quickMatingFragment.updateDouziCount();
        quickMatingLinkingFragment.updateDouziCount();
    }

    public void updateTimes(QMInfoBean qmInfoBean, int times) {
        if (qmInfoBean == null) {
            return;
        }
        int matchCount = 0;
        if (qmInfoBean.getUserFreeMatchCount() > 0) {
            matchCount = qmInfoBean.getUserFreeMatchCount() + times;
            if (matchCount < 0) {
                matchCount = 0;
            }
            qmInfoBean.setUserFreeMatchCount(matchCount);
        } else {
            matchCount = qmInfoBean.getUserBuyMatchCount() + times;
            if (matchCount < 0) {
                matchCount = 0;
            }
            qmInfoBean.setUserBuyMatchCount(matchCount);
        }
        updateTimes(qmInfoBean);
    }

    private void updateTimes(QMInfoBean qmInfoBean) {
        //更新匹配页次数
        quickMatingFragment.updateTimes();
        //更新完成页次数
        int hasMatchCount = qmInfoBean.getUserFreeMatchCount();
//        if (hasMatchCount <= 0) {
//            hasMatchCount = qmInfoBean.getUserBuyMatchCount();
//        }
        String matchCountStr = hasMatchCount + "";
        SpannableString spannableString = new SpannableString("今日剩余" + matchCountStr + "次");
        spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#FF4F8D")), 4, 4 + matchCountStr.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvFinishTimes.setText(spannableString);
    }

    public void showQuickMatingLinking(QMInfoBean qmInfoBean, QuickMatingBean quickMatingBean) {
        if (qmInfoBean == null || quickMatingBean == null) {
            toast("匹配数据为空");
            return;
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        quickMatingLinkingFragment.setQuickMatingBean(qmInfoBean, quickMatingBean);
        transaction.hide(quickMatingFragment)
                .show(quickMatingLinkingFragment)
                .commitNowAllowingStateLoss();
    }

    public void hideFinish(boolean isRemating) {
        isFinishAnimaing = true;
        TranslateAnimation translateAni = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0f, Animation.RELATIVE_TO_SELF,
                0f, Animation.RELATIVE_TO_SELF, 0f,
                Animation.RELATIVE_TO_SELF, 1f);
        translateAni.setDuration(500);
        translateAni.setRepeatCount(0);
        translateAni.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                rlQuickMatingFinish.setVisibility(View.GONE);
                isFinishAnimaing = false;
                if (isRemating) {
                    startQuickMating(gender);
                } else {
                    showQuickMating();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        rlQuickMatingFinish.startAnimation(translateAni);
    }

    public void setRoomBean(RoomBean roomBean) {
        this.roomBean = roomBean;
    }

    public void randomEnterRoom() {
        if (roomBean == null) {
            return;
        }
        RoomServiceScheduler.getInstance().enterRoom(this, roomBean.getRoomUid(), roomBean.getRoomType());
    }

    public void showFinish(QMInfoBean qmInfoBean, int receiveFlowerCount, long oppositeUid,
                           String oppositeNick, String oppoiteAvatar) {
        if (rlQuickMatingFinish.isShown()) {
            return;
        }
        this.oppositeUid = oppositeUid;
        //设置完成页面数据
        tvFinishDouziCount.setText(qmInfoBean.getPeaNum() + "");
        ImageLoadUtils.loadCircleImage(this, oppoiteAvatar, imvFinishOpposite, R.drawable.default_user_head);
        tvOppositeNick.setText(oppositeNick);
        int hasMatchCount = qmInfoBean.getUserFreeMatchCount();
//        if (hasMatchCount <= 0) {
//            hasMatchCount = qmInfoBean.getUserBuyMatchCount();
//        }
        String matchCountStr = hasMatchCount + "";
        SpannableString spannableString = new SpannableString("今日剩余" + matchCountStr + "次");
        spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#FF4F8D")), 4, 4 + matchCountStr.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvFinishTimes.setText(spannableString);

        imvDislike.setSelected(false);
        imvLike.setSelected(false);
        imvLike.setClickable(true);
        imvDislike.setClickable(true);
        imvLike.setVisibility(View.VISIBLE);
        imvDislike.setVisibility(View.VISIBLE);
        String receiveFlowerCountStr = receiveFlowerCount + "";
        SpannableString spanFlower = new SpannableString("本次收到" + receiveFlowerCountStr + "朵花，请给对方评价");
        spanFlower.setSpan(new ForegroundColorSpan(Color.parseColor("#FF4F8D")), 4, 4 + receiveFlowerCountStr.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvGetFlowerCount.setText(spanFlower);
        rlQuickMatingFinish.setVisibility(View.VISIBLE);
        TranslateAnimation translateAni = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0f, Animation.RELATIVE_TO_SELF,
                0f, Animation.RELATIVE_TO_SELF, 1f,
                Animation.RELATIVE_TO_SELF, 0);
        translateAni.setDuration(500);
        translateAni.setRepeatCount(0);
        translateAni.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                showQuickMating();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        rlQuickMatingFinish.startAnimation(translateAni);
    }

    @Override
    public boolean isStatusBarDarkFont() {
        return false;
    }
}
