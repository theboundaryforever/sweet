package com.yuhuankj.tmxq.ui.audio.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;

import com.netease.nimlib.sdk.StatusCode;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomKickOutEvent;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.erban.libcommon.utils.config.SpEvent;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.im.login.IIMLoginClient;
import com.tongdaxing.xchat_core.music.IMusicDownloaderCore;
import com.tongdaxing.xchat_core.music.IMusicDownloaderCoreClient;
import com.tongdaxing.xchat_core.music.bean.HotMusicInfo;
import com.tongdaxing.xchat_core.player.IPlayerCoreClient;
import com.tongdaxing.xchat_core.player.bean.LocalMusicInfo;
import com.tongdaxing.xchat_core.room.IRoomCoreClient;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.databinding.ActivitySearchMusicListBinding;
import com.yuhuankj.tmxq.ui.audio.adapter.MusicPlayListAdapter;
import com.yuhuankj.tmxq.ui.audio.adapter.MusicSearchHistoryAdapter;
import com.yuhuankj.tmxq.ui.audio.adapter.MusicServerListAdapter;
import com.yuhuankj.tmxq.ui.audio.presenter.SearchServerMusicPresenter;
import com.yuhuankj.tmxq.ui.widget.DividerItemDecoration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by chenran on 2017/10/28.
 */
@CreatePresenter(SearchServerMusicPresenter.class)
public class SearchServerMusicActivity extends BaseMvpActivity<SearchServerMusicView, SearchServerMusicPresenter>
        implements SearchServerMusicView,View.OnClickListener, MusicPlayListAdapter.OnPlayErrorListener, MusicServerListAdapter.OnHotMusicItemClickListener, SwipeRefreshLayout.OnRefreshListener {

    private final String TAG = SearchServerMusicActivity.class.getSimpleName();

    private String imgBgUrl;

    private ActivitySearchMusicListBinding musicListBinding;

    private Json searchHistoryCacheJson;
    private MusicSearchHistoryAdapter searchHistoryAdapter;
    private List<String> searchHistoryList = new ArrayList<>();

    private MusicServerListAdapter searchListAdapter;
    private List<HotMusicInfo> seachMusicInfoList = new ArrayList<>();

    public static void start(Context context, String imgBgUrl) {
        Intent intent = new Intent(context, SearchServerMusicActivity.class);
        intent.putExtra("imgBgUrl", imgBgUrl);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View content = findViewById(android.R.id.content);
        ViewGroup.LayoutParams params = content.getLayoutParams();
        params.height = getResources().getDisplayMetrics().heightPixels;

        musicListBinding = DataBindingUtil.setContentView(this,R.layout.activity_search_music_list);
        musicListBinding.setClick(this);
        musicListBinding.etSearch.setFilters(new InputFilter[]{enterBlankInputFilter});
        musicListBinding.etSearch.addTextChangedListener(searchWatcher);
        DisplayUtility.showSoftInputFromWindow(this,musicListBinding.etSearch);
        LinearLayoutManager linearLayoutManager = new RecyclerViewNoBugLinearLayoutManager(this);
        musicListBinding.rvSearchMusic.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, linearLayoutManager.getOrientation(),
                1, R.color.color_1Affffff);
        dividerItemDecoration.setIngoreLastDividerItem(true);
        musicListBinding.rvSearchMusic.addItemDecoration(dividerItemDecoration);
        searchListAdapter = new MusicServerListAdapter(this);
        searchListAdapter.setOnPlayErrorListener(this);
        searchListAdapter.setOnHotMusicItemClickListener(this);
        imgBgUrl = getIntent().getStringExtra("imgBgUrl");
        initSearchHistoryData();
        musicListBinding.srlRefresh.setEnabled(true);
        musicListBinding.srlRefresh.setOnRefreshListener(this);
    }

    InputFilter enterBlankInputFilter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            if (source.equals(" ") || source.toString().contentEquals("\n")) {
                return "";
            } else {
                return null;
            }
        }
    };

    private void initSearchHistoryData() {
        String searchCache = (String) SpUtils.get(this, SpEvent.search_hot_music_history, "");
        searchHistoryCacheJson = Json.parse(searchCache);
        for (String key : searchHistoryCacheJson.key_names()) {
            if(!searchHistoryList.contains(key)){
                searchHistoryList.add(key);
            }
        }
        refreshSearchHistory();
    }

    private void refreshSearchHistory() {
        if (searchHistoryList.size() == 0) {
            changeSearchHistoryVisiable(false);
            return;
        }
        //仅仅是将list反转，https://blog.csdn.net/Shiny0815/article/details/79581317
        Collections.reverse(searchHistoryList);
        if(null == searchHistoryAdapter){
            searchHistoryAdapter = new MusicSearchHistoryAdapter(this);
            searchHistoryAdapter.setOnMusicSearchHistoryDeleteClickListener(new MusicSearchHistoryAdapter.OnMusicSearchHistoryDeleteClickListener() {
                @Override
                public void onMusicSearchHistoryDeleted(String deleteHistory) {
                    deleteSeachHistory(deleteHistory);
                }

                @Override
                public void onMusicSearchHistorySelected(String history) {
                    musicListBinding.etSearch.setText(history);
                    musicListBinding.etSearch.setSelection(history.length());
                    searchServerMusic(history);
                }
            });
        }
        searchHistoryAdapter.setSearchHistoryList(searchHistoryList);
        musicListBinding.rvSearchMusic.setAdapter(searchHistoryAdapter);
        changeSearchHistoryVisiable(true);
    }

    private void searchServerMusic(String key){
        LogUtils.d(TAG,"search-key:"+key);
        if(!TextUtils.isEmpty(key)){
            markSearchContent(key);
            getMvpPresenter().searchServerMusic(key);
        }else{
            musicListBinding.srlRefresh.setRefreshing(false);
        }
    }

    private void deleteSeachHistory(String key){
        if(searchHistoryList.contains(key)){
            searchHistoryList.remove(key);
//            Collections.reverse(searchHistoryList);
        }
        if(searchHistoryCacheJson.has(key)){
            searchHistoryCacheJson.remove(key);
            SpUtils.put(this, SpEvent.search_hot_music_history, searchHistoryCacheJson.toString());
        }
        refreshSearchHistory();
    }

    private void markSearchContent(String s) {
        if (TextUtils.isEmpty(s)) {
            return;
        }
        if(!searchHistoryList.contains(s)){
            searchHistoryList.add(s);
//            Collections.reverse(searchHistoryList);
            searchHistoryCacheJson.set(s,"");
            SpUtils.put(this, SpEvent.search_hot_music_history, searchHistoryCacheJson.toString());
            //搜索历史生成后并不一定要立即展示
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_clearHistory:
                searchHistoryList.clear();
                searchHistoryCacheJson = new Json();
                SpUtils.put(this, SpEvent.search_hot_music_history, "");
                refreshSearchHistory();
                break;
            case R.id.tv_search:
                searchServerMusic(musicListBinding.etSearch.getText().toString());
                break;
            case R.id.back_btn:
                finish();
                break;
            default:
        }
    }


    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onBeKickOut(ChatRoomKickOutEvent.ChatRoomKickOutReason reason) {
        finish();
    }

    @CoreEvent(coreClientClass = IIMLoginClient.class)
    public void onKickedOut(StatusCode code) {
        finish();
    }

    @Override
    public void onGetSearchMusicList(boolean isSuccess, String msg, List list) {
        seachMusicInfoList.clear();
        musicListBinding.srlRefresh.setRefreshing(false);
        changeSearchHistoryVisiable(false);
        if(isSuccess){
            if(null != list && list.size()>0){
                seachMusicInfoList = list;
            }
        }else{
            toast(msg);
        }
        searchListAdapter.setHotMusicInfos(seachMusicInfoList);
        musicListBinding.rvSearchMusic.setAdapter(searchListAdapter);
        changeSearchListVisiable(seachMusicInfoList.size()>0);
    }

    private void changeSearchHistoryVisiable(boolean visiable){
        musicListBinding.tvSearchHistory.setVisibility(visiable ? View.VISIBLE : View.GONE);
        musicListBinding.tvClearHistory.setVisibility(visiable ? View.VISIBLE : View.GONE);
        musicListBinding.srlRefresh.setVisibility(visiable ? View.VISIBLE : View.GONE);
        musicListBinding.srlRefresh.setEnabled(false);
    }

    private void changeSearchListVisiable(boolean visiable){
        musicListBinding.llLocalMusicEmpty.setVisibility(visiable ? View.GONE : View.VISIBLE);
        musicListBinding.srlRefresh.setVisibility(visiable ? View.VISIBLE : View.GONE);
        musicListBinding.srlRefresh.setEnabled(visiable);
    }

    private TextWatcher searchWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if(TextUtils.isEmpty(s.toString())){
                refreshSearchHistory();
                musicListBinding.llLocalMusicEmpty.setVisibility(View.GONE);
            }
        }
    };

    private HotMusicInfo currMusicInfo;

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicPlaying(LocalMusicInfo localMusicInfo) {
//        notifyLastMusicStopPlay();
//        changeMusicPlayStatus(localMusicInfo);
        searchListAdapter.notifyDataSetChanged();
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicPause(LocalMusicInfo localMusicInfo) {
//        changeMusicPlayStatus(localMusicInfo);
        searchListAdapter.notifyDataSetChanged();
    }


    private void notifyLastMusicStopPlay() {
        if(null != currMusicInfo){
            int index = seachMusicInfoList.indexOf(currMusicInfo);
            if(-1 != index){
                searchListAdapter.notifyItemChanged(index);
            }
        }
    }

    private void changeMusicPlayStatus(LocalMusicInfo localMusicInfo){
        try {
            int songId = Integer.valueOf(localMusicInfo.getSongId());
            HotMusicInfo tempInfo = new HotMusicInfo();
            tempInfo.setId(songId);
            if(songId>0){
                int index = seachMusicInfoList.indexOf(tempInfo);
                if(-1 != index){
                    searchListAdapter.notifyItemChanged(index);
                    currMusicInfo = seachMusicInfoList.get(index);
                }
            }
        } catch (NumberFormatException nfex){
            nfex.printStackTrace();
        } catch (IllegalStateException ise){
            ise.printStackTrace();
        }
    }

    @Override
    public void onPlayError(String msg) {
        if(!TextUtils.isEmpty(msg)){
            toast(msg);
        }
    }

    @Override
    public void onHotMusicClickToDown(HotMusicInfo hotMusicInfo) {
        CoreManager.getCore(IMusicDownloaderCore.class).addHotMusicToDownQueue(hotMusicInfo);
        //房间-搜索音乐下载
        StatisticManager.get().onEvent(this,
                StatisticModel.EVENT_ID_ROOM_HOT_MUSIC_SEARCH_DOWNLOAD,
                StatisticModel.getInstance().getUMAnalyCommonMap(this));
    }

    @Override
    public void onRefresh() {
        searchServerMusic(musicListBinding.etSearch.getText().toString());
    }

    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient.class)
    public void onHotMusicDownloadProgressUpdate(LocalMusicInfo localMusicInfo, HotMusicInfo hotMusicInfo, int progress) {
        int index = seachMusicInfoList.indexOf(hotMusicInfo);
        if(-1 != index){
            searchListAdapter.notifyItemChanged(index,progress);
        }
    }

    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient.class)
    public void onHotMusicDownloadError(String msg, LocalMusicInfo localMusicInfo, HotMusicInfo hotMusicInfo) {
        int index = seachMusicInfoList.indexOf(hotMusicInfo);
        if(-1 != index){
            searchListAdapter.notifyItemChanged(index,-1);
        }
    }

    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient.class)
    public void onHotMusicDownloadCompleted(HotMusicInfo hotMusicInfo) {
        int index = seachMusicInfoList.indexOf(hotMusicInfo);
        if(-1 != index){
            searchListAdapter.notifyItemChanged(index,101);
        }
    }

    @Override
    public boolean isStatusBarDarkFont() {
        return false;
    }
}
