package com.yuhuankj.tmxq.ui.chancemeeting;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseViewHolder;
import com.kyleduo.switchbutton.SwitchButton;
import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.cache.NimUserInfoCache;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.msg.MessageBuilder;
import com.netease.nimlib.sdk.msg.MsgService;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.CustomMessageConfig;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tencent.bugly.crashreport.BuglyLog;
import com.tongdaxing.erban.libcommon.audio.AudioPlayer;
import com.tongdaxing.erban.libcommon.audio.OnPlayListener;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.ButtonUtils;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.erban.libcommon.widget.coverflow.CoverFlowLayoutManger;
import com.tongdaxing.erban.libcommon.widget.coverflow.RecyclerCoverFlow;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.audio.AudioPlayAndRecordManager;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.im.custom.bean.ChanceMeetingMsgAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.room.IRoomCoreClient;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.publicchatroom.PublicChatRoomController;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.MainActivity;
import com.yuhuankj.tmxq.ui.launch.guide.INoviceGuideCoreClient;
import com.yuhuankj.tmxq.ui.launch.guide.NoviceGuideDialog;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.widget.ButtonItemFactory;
import com.yuhuankj.tmxq.ui.match.bean.MicroMatch;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * 偶遇界面
 *
 * @author weihaitao
 * @date 2019/1/4
 */
@CreatePresenter(ChanceMeetingPresenter.class)
public class ChanceMeetingActivity extends BaseMvpActivity<ChanceMeetingView, ChanceMeetingPresenter>
        implements ChanceMeetingView, View.OnClickListener {

    public static final String TAG = ChanceMeetingActivity.class.getSimpleName();

    private List<MicroMatch> microMatches = new ArrayList<>();

    private RecyclerCoverFlow rcf_meeting;
    private ChanceMeetingGalleryAdapter adapter;

    private SwitchButton sb_autoPlayAudio;
    private boolean isAutoPlayAudio = false;
    private boolean isRequestMeetingData = false;
    private MicroMatch lastSelectedMicroMatch = null;

    private ImageView iv_likeStatus;
    private ImageView iv_msg;

    private AudioPlayAndRecordManager audioManager;
    private AudioPlayer audioPlayer;

    private String[] man2WomanMsgList = null;
    private String[] woman2ManMsgList = null;

    private int lastSelectedIndex = 0;
    private final int autoScrollToIndex = 5;
    private final int loadMoreDataBeforeIndex = 9;
    private final int eachLoadDataSize = 20;

    private SVGAImageView svgaiv_msgAnim;
    private SVGAImageView svgaiv_bgAnim;
    private boolean msgAnimLoaded = false;
    private boolean bgAnimLoaded = false;
    private SVGAParser msgSvgaAnimParser;
    private SVGAParser bgSvgaAnimParser;
    private SVGAVideoEntity msgSvgaVideoEntity;

    private ImageView iv_back;
    private TextView tvMyVoice;

    private int colorMatrixRedColorScale = 20;
    private int colorMatrixGreenColorScale = 20;
    private int colorMatrixBlueColorScale = 60;
    private int colorMatrixAlphaScale = 147;

    public static void start(Context context) {
        context.startActivity(new Intent(context, ChanceMeetingActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtils.d(TAG, "onCreate");
        setContentView(R.layout.activity_chance_meeting);
        //去除activity的窗口默认的背景
        getWindow().setBackgroundDrawable(null);
        man2WomanMsgList = getResources().getStringArray(R.array.chance_meeting_random_man_to_woman);
        woman2ManMsgList = getResources().getStringArray(R.array.chance_meeting_random_woman_to_man);
        onFindViews();
        onSetListener();
        loadLazyIfYouWant();
    }

    private Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_WHAT_AUTO_SMOOTH_SCROOLL_TO_POSITION:
                    int index1 = msg.arg1;
                    LogUtils.d(TAG, "handleMessage-MSG_WHAT_AUTO_SMOOTH_SCROOLL_TO_POSITION index1:" + index1);
                    if (null != adapter && index1 < adapter.getData().size()) {
                        selectedByAutoScroll = true;
                        rcf_meeting.smoothScrollToPosition(index1);
                    }
                    return true;
                case MSG_WHAT_AUTO_PLAY_AUDIO:
                    int index2 = msg.arg1;
                    if (null != adapter && index2 < adapter.getData().size()) {
                        BaseViewHolder viewHolder = (BaseViewHolder) rcf_meeting.findViewHolderForLayoutPosition(index2);
                        if (viewHolder != null) {
                            View rl_meetingHe = viewHolder.getView(R.id.iv_playStatus);
                            if (null != rl_meetingHe) {
                                rl_meetingHe.callOnClick();
                            }
                        }
//                        View rl_meetingHe = adapter.getViewByPosition(index2,R.id.rl_meetingHe);
//                        rl_meetingHe.callOnClick();
                    }
                    return true;
                case MSG_WHAT_SEND_CHANCE_MEETING_MSG:
                    Bundle bundle = msg.getData();
                    if (null == bundle) {
                        return false;
                    }

                    IMMessage imMessage = MessageBuilder.createTextMessage(bundle.getString("uid"),
                            SessionTypeEnum.P2P, bundle.getString("msg"));
                    NIMClient.getService(MsgService.class).sendMessage(imMessage, false);
                    return true;
                default:
                    return false;
            }

        }
    });

    private boolean hasSelectedItemChanged = false;
    OnPlayListener onPlayListener = new OnPlayListener() {

        @Override
        public void onPrepared() {
            LogUtils.d(TAG, "onPrepared");
            if (null != lastPlayAudioMicroMatch) {
                adapter.updateAudioPlayInfo(getResources().getString(lastPlayAudioMicroMatch.getVoiceDura() > 9 ?
                        R.string.record_dura_2 : R.string.record_dura_1, lastPlayAudioMicroMatch.getVoiceDura()), 0);
                if (adapter.getData().size() > lastSelectedIndex) {
                    notifyForDataUpdated(lastPlayAudioMicroMatch.getUid(), ChanceMeetingGalleryAdapter.ItemUpdateType.updatePlayStatus.ordinal());
                }
            }
        }

        @Override
        public void onCompletion() {
            LogUtils.d(TAG, "onCompletion");
            onPlayCompletion(true);
        }


        @Override
        public void onInterrupt() {
            LogUtils.d(TAG, "onInterrupt");
            if (null != lastPlayAudioMicroMatch) {
                //试听被中断
                adapter.updateAudioPlayInfo(getResources().getString(lastPlayAudioMicroMatch.getVoiceDura() > 9 ?
                        R.string.record_dura_2 : R.string.record_dura_1, lastPlayAudioMicroMatch.getVoiceDura()), 0);
                if (adapter.getData().size() > lastSelectedIndex && null != lastPlayAudioMicroMatch) {
                    notifyForDataUpdated(lastPlayAudioMicroMatch.getUid(), ChanceMeetingGalleryAdapter.ItemUpdateType.updatePauseStatus.ordinal());
                }
                lastPlayAudioMicroMatch = null;
            }
        }

        @Override
        public void onError(String s) {
            LogUtils.d(TAG, "onError :" + s);
            if (null != lastPlayAudioMicroMatch) {
                //试听出错
                adapter.updateAudioPlayInfo(getResources().getString(lastPlayAudioMicroMatch.getVoiceDura() > 9 ?
                        R.string.record_dura_2 : R.string.record_dura_1, lastPlayAudioMicroMatch.getVoiceDura()), 0);
                if (adapter.getData().size() > lastSelectedIndex && null != lastPlayAudioMicroMatch) {
                    notifyForDataUpdated(lastPlayAudioMicroMatch.getUid(), ChanceMeetingGalleryAdapter.ItemUpdateType.updatePauseStatus.ordinal());
                }
                lastPlayAudioMicroMatch = null;
            }

        }

        @Override
        public void onPlaying(long l) {
            LogUtils.d(TAG, "onPlaying-l:" + l);
//            if (null != lastPlayAudioMicroMatch) {
//                String dur = "";
//                int currDur = Long.valueOf(l / 1000).intValue();
//                currDur = lastPlayAudioMicroMatch.getVoiceDura() - currDur;
//                if (currDur >= 10) {
//                    dur = "00:".concat(String.valueOf(currDur));
//                } else if (currDur > 0) {
//                    dur = "00:0".concat(String.valueOf(currDur));
//                }
//                LogUtils.d(TAG, "onPlaying-currDur:" + currDur);
//                int progress = 0;
//                LogUtils.d(TAG, "onPlaying-voiceDura:" + lastPlayAudioMicroMatch.getVoiceDura());
//                if (lastPlayAudioMicroMatch.getVoiceDura() > 0) {
//                    progress = Long.valueOf((l * 100) / (lastPlayAudioMicroMatch.getVoiceDura() * 1000L)).intValue();
//                    LogUtils.d(TAG, "onPlaying-progress:" + progress);
//                }
//                adapter.updateAudioPlayInfo(dur, progress);
//                if (adapter.getData().size() > lastSelectedIndex) {
//                    notifyForDataUpdated(lastPlayAudioMicroMatch.getUid(), ChanceMeetingGalleryAdapter.ItemUpdateType.updatePlayStatus.ordinal());
//                }
//            }
        }
    };

    private void onFindViews() {
        LogUtils.d(TAG, "onFindViews");
        tvMyVoice = (TextView) findViewById(R.id.tvMyVoice);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_likeStatus = (ImageView) findViewById(R.id.iv_likeStatus);
        iv_msg = (ImageView) findViewById(R.id.iv_msg);
        rcf_meeting = (RecyclerCoverFlow) findViewById(R.id.rcf_meeting);
        CoverFlowLayoutManger coverFlowLayoutManger = (CoverFlowLayoutManger) rcf_meeting.getLayoutManager();
        if (null != coverFlowLayoutManger) {
            coverFlowLayoutManger.setItemGradualAlpha(false);
            coverFlowLayoutManger.setItemGradualGrey(true);
            coverFlowLayoutManger.setColorMatrixAlphaScale(colorMatrixAlphaScale);
            coverFlowLayoutManger.setColorMatrixBlueColorScale(colorMatrixBlueColorScale);
            coverFlowLayoutManger.setColorMatrixGreenColorScale(colorMatrixGreenColorScale);
            coverFlowLayoutManger.setColorMatrixRedColorScale(colorMatrixRedColorScale);
        }
        rcf_meeting.setFlingScale(0.5f);
        sb_autoPlayAudio = (SwitchButton) findViewById(R.id.sb_autoPlayAudio);
        svgaiv_msgAnim = (SVGAImageView) findViewById(R.id.svgaiv_msgAnim);
        svgaiv_bgAnim = (SVGAImageView) findViewById(R.id.svgaiv_bgAnim);
        adapter = new ChanceMeetingGalleryAdapter(this);
        adapter.replaceData(microMatches);
        rcf_meeting.setAdapter(adapter);

        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) rcf_meeting.getLayoutParams();
        lp.width = DisplayUtility.getScreenWidth(this);
        lp.height = lp.width * 3 / 4 * 400 / 282;
        rcf_meeting.setLayoutParams(lp);
        runnable1 = new NotifyMongoliaStatusChangeRunnable();
        checkIsNeedShowGuideDialog();
        msgSvgaAnimParser = new SVGAParser(this);
        bgSvgaAnimParser = new SVGAParser(this);
        BuglyLog.d(TAG, "play-svga anim, url1:" + UriProvider.getChanceMeetingMsgAnimUrl());
        try {
            msgSvgaAnimParser.parse(new URL(UriProvider.getChanceMeetingMsgAnimUrl()), new SVGAParser.ParseCompletion() {
                @Override
                public void onComplete(SVGAVideoEntity svgaVideoEntity) {
                    LogUtils.d(TAG, "onFindViews-parse-onComplete0");
                    msgAnimLoaded = true;

                    //新手引导显示过程中,主要为了避免因为svgaiv_msgAnim尺寸同iv_msg有所差异，导致的新手引导显示效果有所偏差的问题
                    if (null != chanceMeetingGuideDialog && chanceMeetingGuideDialog.isShowing()) {
                        msgSvgaVideoEntity = svgaVideoEntity;
                    } else {
                        svgaiv_msgAnim.setVisibility(View.VISIBLE);
                        svgaiv_msgAnim.setImageDrawable(new SVGADrawable(svgaVideoEntity));
                        svgaiv_msgAnim.startAnimation();
                        iv_msg.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onError() {

                }
            });
            BuglyLog.d(TAG, "play-svga anim, url2:" + UriProvider.getChanceMeetingBgAnimUrl());
            bgSvgaAnimParser.parse(new URL(UriProvider.getChanceMeetingBgAnimUrl()), new SVGAParser.ParseCompletion() {
                @Override
                public void onComplete(SVGAVideoEntity svgaVideoEntity) {
                    LogUtils.d(TAG, "onFindViews-parse-onComplete1");
                    bgAnimLoaded = true;
                    svgaiv_bgAnim.setVisibility(View.VISIBLE);
                    svgaiv_bgAnim.setImageDrawable(new SVGADrawable(svgaVideoEntity));
                    svgaiv_bgAnim.startAnimation();
                }

                @Override
                public void onError() {

                }
            });
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private boolean queueRoomByUserClick = false;

    private void notifyForDataUpdated(long uid, int type) {
        //不能remove，不然歌曲播放的时候，进度条就因为播放信息更新间隔过快而无法更新了
//        if(null != runnable){
//            rcf_meeting.removeCallbacks(runnable);
//        }
        runnable = new NotifyDataUpdatedRunnable(uid, type);
        rcf_meeting.post(runnable);
    }

    private NotifyDataUpdatedRunnable runnable = null;

    private void onSetListener() {
        LogUtils.d(TAG, "onSetListener");
        tvMyVoice.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        iv_likeStatus.setOnClickListener(this);
        iv_msg.setOnClickListener(this);
        svgaiv_msgAnim.setOnClickListener(this);
        rcf_meeting.setOnItemSelectedListener(new CoverFlowLayoutManger.OnSelected() {
            @Override
            public void onItemSelected(int position) {
                LogUtils.d(TAG, "onItemSelected-position:" + position);
//                if(position != lastSelectedIndex){
                lastSelectedIndex = position;
                lastSelectedMicroMatch = microMatches.get(position);
                adapter.setLastSelectedIndex(lastSelectedIndex);
                if (null != lastSelectedMicroMatch) {
                    LogUtils.d(TAG, "onItemSelected-uid:" + lastSelectedMicroMatch.getUid());
                    iv_likeStatus.setImageDrawable(getResources().getDrawable(lastSelectedMicroMatch.isAdmired() ?
                            R.mipmap.icon_chance_meeting_liked : R.mipmap.icon_chance_meeting_like));
                    adapter.setLastSelectedMicroMatch(lastSelectedMicroMatch);
                    if (hasSelectedItemChanged || selectedByAutoScroll) {
                        rcf_meeting.post(runnable1);
                    }
                }

                LogUtils.d(TAG, "onItemSelected-hasSelectedItemChanged:" + hasSelectedItemChanged);
                LogUtils.d(TAG, "onItemSelected-selectedByAutoScroll:" + selectedByAutoScroll);
                if (hasSelectedItemChanged || selectedByAutoScroll) {
                    onMeetingUserChanged();
                    checkIsNeedPreLoadMeetingData(position);
                    hasSelectedItemChanged = false;
                    selectedByAutoScroll = false;
                }

//                }
            }

            /**
             * 监听item即将选中，在item横向滚动结束后，回滚到中间位置前触发
             *
             * @param position
             */
            @Override
            public void onItemPreSelected(int position) {
                LogUtils.d(TAG, "onItemPreSelected-position:" + position);
                LogUtils.d(TAG, "onItemPreSelected-hasSelectedItemChanged:" + hasSelectedItemChanged);
                LogUtils.d(TAG, "onItemPreSelected-selectedByAutoScroll:" + selectedByAutoScroll);
                if (position != lastSelectedIndex) {
                    lastSelectedIndex = position;
                    lastSelectedMicroMatch = microMatches.get(position);
                    adapter.setLastSelectedIndex(lastSelectedIndex);
                    checkForUploadAudioTips();
                    hasSelectedItemChanged = true;
                }
                LogUtils.d(TAG, "onItemPreSelected-hasSelectedItemChanged1:" + hasSelectedItemChanged);
                LogUtils.d(TAG, "onItemPreSelected-selectedByAutoScroll1:" + selectedByAutoScroll);
            }
        });

        sb_autoPlayAudio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                LogUtils.d(TAG, "OnChanged-isChecked:" + isChecked);
                isAutoPlayAudio = isChecked;
                sb_autoPlayAudio.setBackColorRes(isChecked ? R.color.color_4036B4 : R.color.color_white);
                if (isChecked) {
                    //偶遇-自动播放-关闭
                    StatisticManager.get().onEvent(ChanceMeetingActivity.this,
                            StatisticModel.EVENT_ID_CHANCE_MEETING_AUTO_PLAY_OPEN,
                            StatisticModel.getInstance().getUMAnalyCommonMap(ChanceMeetingActivity.this));
                } else {
                    //偶遇-自动播放-开启
                    StatisticManager.get().onEvent(ChanceMeetingActivity.this,
                            StatisticModel.EVENT_ID_CHANCE_MEETING_AUTO_PLAY_CLOSE,
                            StatisticModel.getInstance().getUMAnalyCommonMap(ChanceMeetingActivity.this));
                }
            }
        });
        sb_autoPlayAudio.setChecked(true);
        adapter.setOnChanceMeetingClickListener(new ChanceMeetingGalleryAdapter.OnChanceMeetingClickListener() {
            @Override
            public void onReportUser(MicroMatch microMatch) {
                LogUtils.d(TAG, "onReportUser-microMatch:" + microMatch);
                List<ButtonItem> buttonItems = new ArrayList<>();
                buttonItems.add(ButtonItemFactory.createReportItem(ChanceMeetingActivity.this,
                        getResources().getString(R.string.user_info_report), 1, null != lastSelectedMicroMatch ? lastSelectedMicroMatch.getUid() : 0L));
                getDialogManager().showCommonPopupDialog(buttonItems, getString(R.string.cancel));
                String[] reasons = getResources().getStringArray(R.array.chance_meeting_report_reason);
                List<ButtonItem> buttons = new ArrayList<>();
                for (int i = 0; i < reasons.length; i++) {
                    final int index = i + 1;
                    buttons.add(new ButtonItem(reasons[i], new ButtonItem.OnClickListener() {
                        @Override
                        public void onClick() {
                            getDialogManager().showProgressDialog(ChanceMeetingActivity.this, getString(R.string.network_loading));
                            getMvpPresenter().reportUser(microMatch.getUid(), index);
                        }
                    }));
                }
                getDialogManager().showCommonPopupDialog(buttons, getResources().getString(R.string.cancel));
            }

            @Override
            public void jumpUserInfoActivity(MicroMatch microMatch) {
                LogUtils.d(TAG, "jumpUserInfoActivity-microMatch:" + microMatch);
                if (!ButtonUtils.isFastDoubleClick()) {
                    UserInfoActivity.start(ChanceMeetingActivity.this, microMatch.getUid());
                }
            }

            @Override
            public void onPlayOrPauseUserAudio(MicroMatch microMatch, boolean playOrPause) {
                LogUtils.d(TAG, "onPlayOrPauseUserAudio-microMatch:" + microMatch);
                if (null == audioManager) {
                    audioManager = AudioPlayAndRecordManager.getInstance();
                }
                //如果本地播放录音对应用户与上次的不一致
                if (playOrPause) {
                    lastPlayAudioMicroMatch = microMatch;
                    notifyForDataUpdated(microMatch.getUid(), ChanceMeetingGalleryAdapter.ItemUpdateType.updatePlayStatus.ordinal());
                    if (null != audioPlayer) {
                        audioPlayer.stop();
                        audioPlayer.setOnPlayListener(null);
                        audioPlayer = null;
                    }

                    if (null == audioPlayer) {
                        audioPlayer = audioManager.getAudioPlayer(ChanceMeetingActivity.this, null, onPlayListener);
                    }
                    if (!audioPlayer.isPlaying() && !TextUtils.isEmpty(microMatch.getUserVoice())) {
                        LogUtils.d(TAG, "onPlayOrPauseUserAudio-play audioFileUrl: " + microMatch.getUserVoice());
                        audioPlayer.setDataSource(microMatch.getUserVoice());
                        audioManager.play();

                        //偶遇-播放录音
                        StatisticManager.get().onEvent(ChanceMeetingActivity.this,
                                StatisticModel.EVENT_ID_CHANCE_MEETING_AUDIO_PLAY,
                                StatisticModel.getInstance().getUMAnalyCommonMap(ChanceMeetingActivity.this));
                    }
                } else {
                    //如果本地播放录音对应用户与上次的一致,那么视为暂停播放
                    if (null == audioPlayer) {
                        audioPlayer = audioManager.getAudioPlayer(ChanceMeetingActivity.this, null, onPlayListener);
                    }

                    if (audioPlayer.isPlaying()) {
                        audioManager.stopPlay();
                        lastPlayAudioMicroMatch = null;
                    }
                    notifyForDataUpdated(microMatch.getUid(), ChanceMeetingGalleryAdapter.ItemUpdateType.updatePauseStatus.ordinal());
                }
            }

            @Override
            public void findRoomTaIn(long roomUid, int roomType) {
                RoomServiceScheduler.getInstance().enterRoom(ChanceMeetingActivity.this, roomUid, roomType);
            }
        });
    }

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onGetUserRoom(RoomInfo roomInfo) {
        if (!queueRoomByUserClick) {
            return;
        }
        queueRoomByUserClick = false;
        getDialogManager().dismissDialog();
        if (null == roomInfo || roomInfo.getUid() == 0) {
            toast(R.string.user_not_in_room);
            return;
        }
        if (roomInfo.getRoomId() == (BasicConfig.isDebug ? PublicChatRoomController.devRoomId : PublicChatRoomController.formalRoomId)) {
            //不再房间，不展示去找TA
            toast(R.string.user_not_in_room);
            return;
        }

        if (roomInfo.getUid() > 0) {
            RoomInfo current = RoomServiceScheduler.getCurrentRoomInfo();
            if (current != null && current.getUid() == roomInfo.getUid()) {
                toast(R.string.already_in_same_room);
                return;
            }
            if (MainActivity.isLinkMicroing) {
                SingleToastUtil.showToast("连麦中不能进入房间");
                return;
            }

            RoomServiceScheduler.getInstance().enterRoom(ChanceMeetingActivity.this,
                    roomInfo.getUid(), roomInfo.getType());
        }
    }

    private NotifyMongoliaStatusChangeRunnable runnable1;

    class NotifyMongoliaStatusChangeRunnable implements Runnable {

        @Override
        public void run() {
            if (null != adapter) {
                adapter.notifyDataSetChanged();
            }
        }
    }

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onGetUserRoomFail(String msg) {
        if (!queueRoomByUserClick) {
            return;
        }
        queueRoomByUserClick = false;
        getDialogManager().dismissDialog();
        if (!TextUtils.isEmpty(msg)) {
            toast(msg);
        }
    }

    private void onPlayCompletion(boolean continueAutoScrollToNex) {
        LogUtils.d(TAG, "onPlayCompletion-continueAutoScrollToNex:" + continueAutoScrollToNex);
        if (null != lastPlayAudioMicroMatch) {
            LogUtils.d(TAG, "onPlayCompletion-lastPlayAudioMicroMatch.uid:" + lastPlayAudioMicroMatch.getUid());
            //试听结束,更新试听按钮为试听
            adapter.updateAudioPlayInfo(getResources().getString(lastPlayAudioMicroMatch.getVoiceDura() > 9 ?
                    R.string.record_dura_2 : R.string.record_dura_1, lastPlayAudioMicroMatch.getVoiceDura()), 100);
            if (adapter.getData().size() > lastSelectedIndex) {
                notifyForDataUpdated(lastPlayAudioMicroMatch.getUid(), ChanceMeetingGalleryAdapter.ItemUpdateType.updatePlayStatus.ordinal());
                if (isAutoPlayAudio && continueAutoScrollToNex) {
                    Message msg = handler.obtainMessage(MSG_WHAT_AUTO_SMOOTH_SCROOLL_TO_POSITION);
                    msg.arg1 = lastSelectedIndex + 1;
                    handler.sendMessageDelayed(msg, 100L);
                }
            }
            lastPlayAudioMicroMatch = null;
        }
    }

    /**
     * 数据懒加载
     */
    private void loadLazyIfYouWant() {
        if ((null == microMatches || microMatches.size() == 0) && !isRequestMeetingData) {
            LogUtils.d(TAG, "loadLazyIfYouWant-懒加载，数据请求");
            getDialogManager().showProgressDialog(ChanceMeetingActivity.this, getString(R.string.network_loading));
            isRequestMeetingData = true;
            getMvpPresenter().getMeetingData(eachLoadDataSize);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (null != svgaiv_msgAnim && msgAnimLoaded) {
            svgaiv_msgAnim.stopAnimation(true);
            msgAnimLoaded = false;
        }
        if (null != svgaiv_bgAnim && bgAnimLoaded) {
            svgaiv_bgAnim.stopAnimation(true);
            bgAnimLoaded = false;
        }
        LogUtils.d(TAG, "onDestroy");
        audioRelease();
        if (null != handler) {
            handler.removeMessages(MSG_WHAT_AUTO_PLAY_AUDIO);
            handler.removeMessages(MSG_WHAT_AUTO_SMOOTH_SCROOLL_TO_POSITION);
            handler.removeMessages(MSG_WHAT_SEND_CHANCE_MEETING_MSG);
        }

        if (null != dialogManager && dialogManager.isDialogShowing()) {
            dialogManager.dismissDialog();
        }

        if (null != runnable) {
            rcf_meeting.removeCallbacks(runnable);
        }
        if (null != runnable1) {
            rcf_meeting.removeCallbacks(runnable1);
        }
    }

    @Override
    public void onGetMeetingData(boolean isSuccess, String msg, List list) {
        LogUtils.d(TAG, "onGetMeetingData-isSuccess:" + isSuccess + " msg:" + msg);
        getDialogManager().dismissDialog();
        isRequestMeetingData = false;
        if (isSuccess && null != list) {
            if (list.size() == 0) {
                return;
            }
            if (microMatches.size() == 0) {
                lastSelectedMicroMatch = (MicroMatch) list.get(0);
            }
            microMatches.addAll(list);
            //修复 java.lang.NullPointerException: Attempt to invoke virtual method
            // 'boolean android.support.v7.widget.RecyclerView$State.isPreLayout()'
            // on a null object reference
            rcf_meeting.post(() -> adapter.replaceData(microMatches));
        } else if (!TextUtils.isEmpty(msg)) {
            toast(msg);
        }
    }

    private void checkIsNeedPreLoadMeetingData(int position) {
        LogUtils.d(TAG, "checkIsNeedPreLoadMeetingData-position:" + position + " microMatches.size:" + microMatches.size());
        if (microMatches.size() - position <= loadMoreDataBeforeIndex && !isRequestMeetingData) {
            isRequestMeetingData = true;
            getMvpPresenter().getMeetingData(eachLoadDataSize);
        }
    }

    private List<Long> hasSentMsgUidList = new ArrayList<>();

    private long lastAdmireOperaUid = 0L;
    private long lastAdmireOperaTime = 0L;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvMyVoice:
                MyVoiceActivity.start(this);
                //偶遇-左上角按钮-跳转我的声音界面
                Map<String, String> maps = StatisticModel.getInstance().getUMAnalyCommonMap(ChanceMeetingActivity.this);
                StatisticManager.get().onEvent(ChanceMeetingActivity.this, StatisticModel.EVENT_ID_CHANCE_MEETING_BTN_MYVOICE, maps);
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_likeStatus:

                if (null != lastSelectedMicroMatch) {
                    //限制短时间内对单个用户的连续过快点赞/取消点赞操作
                    long timeDiv = System.currentTimeMillis() - lastAdmireOperaTime;
                    if (timeDiv < 1000L && lastSelectedMicroMatch.getUid() == lastAdmireOperaUid) {
                        return;
                    }

                    lastAdmireOperaUid = lastSelectedMicroMatch.getUid();
                    lastAdmireOperaTime = System.currentTimeMillis();

                    getDialogManager().showProgressDialog(ChanceMeetingActivity.this, getString(R.string.network_loading));
                    getMvpPresenter().admireUser(lastSelectedMicroMatch.getUid(), lastSelectedMicroMatch.isAdmired() ? 2 : 1);
                    //偶遇-点赞、取消点赞
                    StatisticManager.get().onEvent(ChanceMeetingActivity.this,
                            lastSelectedMicroMatch.isAdmired() ?
                                    StatisticModel.EVENT_ID_ENTER_CHANCE_MEETING_ADMIRE_CANCEL
                                    : StatisticModel.EVENT_ID_ENTER_CHANCE_MEETING_ADMIRE,
                            StatisticModel.getInstance().getUMAnalyCommonMap(ChanceMeetingActivity.this));
                }
                break;
            case R.id.svgaiv_msgAnim:
            case R.id.iv_msg:
                if (null != lastSelectedMicroMatch) {
                    //每个目标仅发送一次随机私聊消息
                    if (null != hasSentMsgUidList && !hasSentMsgUidList.contains(lastSelectedMicroMatch.getUid())) {
                        //发送随机私聊消息
                        Random random = new Random();
                        String msg = null;
                        int index = 0;
                        if (lastSelectedMicroMatch.getGender() == 2) {
                            index = random.nextInt(man2WomanMsgList.length);
                            msg = man2WomanMsgList[index];
                        } else {
                            index = random.nextInt(woman2ManMsgList.length);
                            msg = woman2ManMsgList[index];
                        }

                        ChanceMeetingMsgAttachment attachment = new ChanceMeetingMsgAttachment(CustomAttachment.CUSTOM_MSG_CHANCE_MEETING, CustomAttachment.CUSTOM_MSG_CHANCE_MEETING);
                        attachment.setSendTime(System.currentTimeMillis());
                        attachment.setTargetAvatar(lastSelectedMicroMatch.getAvatar());
                        attachment.setTargetUid(lastSelectedMicroMatch.getUid());
                        CustomMessageConfig customMessageConfig = new CustomMessageConfig();
                        customMessageConfig.enablePush = false;
                        IMMessage imMessage = MessageBuilder.createCustomMessage(lastSelectedMicroMatch.getUid() + "",
                                SessionTypeEnum.P2P, "", attachment, customMessageConfig);
                        NIMClient.getService(MsgService.class).sendMessage(imMessage, false);

                        sendTextMessage(lastSelectedMicroMatch.getUid(), msg);

                        hasSentMsgUidList.add(lastSelectedMicroMatch.getUid());
                    }

                    //跳转私聊界面
                    NimUserInfo nimUserInfo = NimUserInfoCache.getInstance().getUserInfo(lastSelectedMicroMatch.getUid() + "");
                    if (nimUserInfo != null) {
                        NimUIKit.startP2PSession(ChanceMeetingActivity.this, lastSelectedMicroMatch.getUid() + "");
                    } else {
                        NimUserInfoCache.getInstance().getUserInfoFromRemote(lastSelectedMicroMatch.getUid() + "", new RequestCallbackWrapper<NimUserInfo>() {
                            @Override
                            public void onResult(int i, NimUserInfo nimUserInfo, Throwable throwable) {
                                if (i == 200) {
                                    NimUIKit.startP2PSession(ChanceMeetingActivity.this, lastSelectedMicroMatch.getUid() + "");
                                } else {
                                    toast(getResources().getString(R.string.network_error_retry));
                                }
                            }
                        });
                    }

                    //偶遇-私聊
                    StatisticManager.get().onEvent(ChanceMeetingActivity.this,
                            StatisticModel.EVENT_ID_CHANCE_MEETING_MSG,
                            StatisticModel.getInstance().getUMAnalyCommonMap(ChanceMeetingActivity.this));
                }
                break;
            default:
                break;
        }
    }

    private void sendTextMessage(long uid, String msg) {
        Message message = handler.obtainMessage(MSG_WHAT_SEND_CHANCE_MEETING_MSG);
        Bundle bundle = new Bundle();
        bundle.putString("msg", msg);
        bundle.putString("uid", String.valueOf(uid));
        message.setData(bundle);
        handler.sendMessageDelayed(message, 100L);
    }

    private final int MSG_WHAT_SEND_CHANCE_MEETING_MSG = 3;

//--------------------------------------录音播放--------------------------------------

    private MicroMatch lastPlayAudioMicroMatch = null;

    private void onMeetingUserChanged() {
        LogUtils.d(TAG, "");
        if (null != audioManager && audioManager.isPlaying()) {
            audioManager.stopPlay();
            lastPlayAudioMicroMatch = null;
        }

        if (null == audioManager) {
            audioManager = AudioPlayAndRecordManager.getInstance();
        }
        if (null != audioPlayer) {
            audioPlayer.stop();
            audioPlayer.setOnPlayListener(null);
            audioPlayer = null;
        }

        adapter.setLastPlayAudioMeetingUserId(0);
        adapter.setItemUpdateType(ChanceMeetingGalleryAdapter.ItemUpdateType.unknow.ordinal());
        handler.removeMessages(MSG_WHAT_AUTO_PLAY_AUDIO);

        if (!checkForUploadAudioTips() && isAutoPlayAudio) {
            Message msg = handler.obtainMessage(MSG_WHAT_AUTO_PLAY_AUDIO);
            msg.arg1 = lastSelectedIndex;
            handler.sendMessageDelayed(msg, 150L);
        }
    }

    private DialogManager dialogManager;
    private boolean hasShowUploadRecordDialog = false;
    private boolean selectedByAutoScroll = false;

    private boolean checkForUploadAudioTips() {
        boolean result = false;
        LogUtils.d(TAG, "checkForUploadAudioTips-hasShowUploadRecordDialog:" + hasShowUploadRecordDialog);
        LogUtils.d(TAG, "checkForUploadAudioTips-lastSelectedIndex:" + lastSelectedIndex + " autoScrollToIndex:" + autoScrollToIndex);
        if (lastSelectedIndex >= autoScrollToIndex) {
            final UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            result = null != userInfo && (TextUtils.isEmpty(userInfo.getUserVoice()) || userInfo.getVoiceDura() <= 0);
            if (result) {
                if (null == dialogManager) {
                    dialogManager = new DialogManager(ChanceMeetingActivity.this);
                }
                if (!hasShowUploadRecordDialog && !dialogManager.isDialogShowing()) {
                    dialogManager.showOkCancelDialog(getResources().getString(R.string.chance_meeting_need_upload_audio_tips),
                            getResources().getString(R.string.upload), getResources().getString(R.string.cancel), true, new DialogManager.OkCancelDialogListener() {
                                @Override
                                public void onCancel() {
                                    if (null != adapter && (autoScrollToIndex + 1) <= adapter.getData().size()) {
                                        selectedByAutoScroll = true;
                                        rcf_meeting.smoothScrollToPosition(autoScrollToIndex - 1);
                                        hasShowUploadRecordDialog = lastSelectedIndex >= autoScrollToIndex;
                                        LogUtils.d(TAG, "checkForUploadAudioTips-onCancel0 hasShowUploadRecordDialog:" + hasShowUploadRecordDialog);
                                    }
                                }

                                @Override
                                public void onOk() {
                                    MyVoiceActivity.start(ChanceMeetingActivity.this);
                                    //偶遇-上传录音提示-跳转我的声音界面
                                    Map<String, String> maps = StatisticModel.getInstance().getUMAnalyCommonMap(ChanceMeetingActivity.this);
                                    StatisticManager.get().onEvent(ChanceMeetingActivity.this, StatisticModel.EVENT_ID_CHANCE_MEETING_TIPS_MYVOICE, maps);
                                }
                            }, new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialog) {
                                    if (null != adapter && (autoScrollToIndex + 1) <= adapter.getData().size()) {
                                        selectedByAutoScroll = true;
                                        rcf_meeting.smoothScrollToPosition(autoScrollToIndex - 1);
                                        hasShowUploadRecordDialog = lastSelectedIndex >= autoScrollToIndex;
                                        LogUtils.d(TAG, "checkForUploadAudioTips-onCancel1 hasShowUploadRecordDialog:" + hasShowUploadRecordDialog);

                                        //偶遇-上传录音提示-取消
                                        StatisticManager.get().onEvent(ChanceMeetingActivity.this,
                                                StatisticModel.EVENT_ID_CHANCE_MEETING_UPLOAD_AUDIO_CANCEL,
                                                StatisticModel.getInstance().getUMAnalyCommonMap(ChanceMeetingActivity.this));
                                    }
                                }
                            });
                }
            }
        }
        hasShowUploadRecordDialog = false;
        LogUtils.d(TAG, "checkForUploadAudioTips-hasShowUploadRecordDialog1:" + hasShowUploadRecordDialog);
//        rcf_meeting.getCoverFlowLayout().setUseCustomMaxOffset(result);
        return result;
    }

    private final int MSG_WHAT_AUTO_PLAY_AUDIO = 1;
    private final int MSG_WHAT_AUTO_SMOOTH_SCROOLL_TO_POSITION = 2;

    @Override
    public void onReportUserResponse(boolean isSuccess, String msg) {
        getDialogManager().dismissDialog();
        toast(msg);
    }

    @Override
    public void onAdmireOperaResponse(boolean isSuccess, String msg, long uid, boolean isAdmire) {
        getDialogManager().dismissDialog();
        if (!isSuccess && !TextUtils.isEmpty(msg)) {
            toast(msg);
        }
        if (isSuccess) {
            if (null != lastSelectedMicroMatch && lastSelectedMicroMatch.getUid() == uid) {
                lastSelectedMicroMatch.setAdmired(isAdmire);
                iv_likeStatus.setImageDrawable(getResources().getDrawable(isAdmire ?
                        R.mipmap.icon_chance_meeting_liked : R.mipmap.icon_chance_meeting_like));
            } else {
                MicroMatch microMatch = new MicroMatch();
                microMatch.setUid(uid);
                int index = microMatches.indexOf(microMatch);
                if (-1 != index) {
                    microMatch = microMatches.get(index);
                    microMatch.setAdmired(isAdmire);
                }
            }
            toast(getResources().getString(R.string.admire_success));
            //发送点赞文本消息，后端通过返回的String是否为空，控制发送的次数
            if (!TextUtils.isEmpty(msg)) {
                sendTextMessage(uid, msg);
            }
        }
    }

    class NotifyDataUpdatedRunnable implements Runnable {

        private long uid;
        private int type;

        public NotifyDataUpdatedRunnable(long uid, int type) {
            this.uid = uid;
            this.type = type;
        }

        @Override
        public void run() {
            if (null != adapter) {
                adapter.setLastPlayAudioMeetingUserId(uid);
                adapter.setItemUpdateType(type);
                //会闪烁
//                MicroMatch tempMMatch = new MicroMatch();
//                tempMMatch.setUid(uid);
//                int index = adapter.getData().indexOf(tempMMatch);
//                if(-1 != index){
//                    adapter.notifyItemChanged(index);
//                }else{
                adapter.notifyDataSetChanged();
//                }

            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        final UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        boolean result = null != userInfo && (TextUtils.isEmpty(userInfo.getUserVoice()) || userInfo.getVoiceDura() <= 0);
        LogUtils.d(TAG, "onResume result:" + result);
        if (result && null != adapter && (autoScrollToIndex + 1) <= adapter.getData().size() && lastSelectedIndex >= autoScrollToIndex) {
            rcf_meeting.smoothScrollToPosition(autoScrollToIndex - 1);
        }
        loadLazyIfYouWant();

        if (null != svgaiv_msgAnim && msgAnimLoaded) {
            svgaiv_msgAnim.startAnimation();
        }
        if (null != svgaiv_bgAnim && bgAnimLoaded) {
            svgaiv_bgAnim.startAnimation();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        LogUtils.d(TAG, "onPause");
        //audioRelease会触发onInterrupt
//        onPlayCompletion(false);
        audioRelease();
        if (null != svgaiv_msgAnim && msgAnimLoaded) {
            svgaiv_msgAnim.stopAnimation(false);
        }
        if (null != svgaiv_bgAnim && bgAnimLoaded) {
            svgaiv_bgAnim.stopAnimation(false);
        }
    }

    private void audioRelease() {
        if (audioManager != null) {
            //audioManager.release();会停止player播放器并设置listener为null
            audioManager.release();
            if (audioPlayer != null) {
                audioPlayer = null;
            }
            audioManager = null;
        }
    }

    //----------------------------新手引导------------------------------
    private NoviceGuideDialog chanceMeetingGuideDialog = null;

    private void checkIsNeedShowGuideDialog() {
        //判断是否需要显示新手引导
        UserInfo uinfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        if (null != uinfo && null != uinfo.getGuideState()) {
            if (null == uinfo.getGuideState().getType5() || "0".equals(uinfo.getGuideState().getType5())) {
                if (chanceMeetingGuideDialog == null) {
                    chanceMeetingGuideDialog = new NoviceGuideDialog(ChanceMeetingActivity.this, NoviceGuideDialog.GuideType.CHANCE_MEETING, null);
                }
                //新手引导暂时屏蔽
//                chanceMeetingGuideDialog.show();
            }
        }
    }

    @CoreEvent(coreClientClass = INoviceGuideCoreClient.class)
    public void onChanceMeetingGuideShowStatusChanged(boolean isShow) {
        LogUtils.d(TAG, "onChanceMeetingGuideShowStatusChanged-isShow:" + isShow);
        if (!isShow && msgAnimLoaded && null != msgSvgaVideoEntity) {
            msgAnimLoaded = true;
            svgaiv_msgAnim.setVisibility(View.VISIBLE);
            svgaiv_msgAnim.setImageDrawable(new SVGADrawable(msgSvgaVideoEntity));
            svgaiv_msgAnim.startAnimation();
            iv_msg.setVisibility(View.GONE);
        } else if (isShow) {
            //因为未知比较难计算，这了暂时以一种折中的方案来做，展示新手引导的时候就隐藏iv_msg，避免界面显示出来的效果是交错重叠的
            iv_msg.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public boolean isStatusBarDarkFont() {
        return false;
    }
}
