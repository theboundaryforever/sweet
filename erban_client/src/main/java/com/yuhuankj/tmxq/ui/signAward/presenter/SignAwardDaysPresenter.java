package com.yuhuankj.tmxq.ui.signAward.presenter;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.signAward.model.SignAwardResult;
import com.yuhuankj.tmxq.ui.signAward.model.SignInModel;
import com.yuhuankj.tmxq.ui.signAward.view.ISignAwardDaysView;

/**
 * @author weihaitao
 * @date 2019/5/21
 */
public class SignAwardDaysPresenter extends AbstractMvpPresenter<ISignAwardDaysView> {

    private SignInModel signInModel;

    public SignAwardDaysPresenter() {
        signInModel = new SignInModel();
    }

    @Deprecated
    public void signInMission() {
        signInModel.signInMission(new HttpRequestCallBack<SignAwardResult>() {
            @Override
            public void onSuccess(String message, SignAwardResult result) {
                if (null == getMvpView()) {
                    return;
                }
                if (null == result) {
                    getMvpView().showSignErr(BasicConfig.INSTANCE.getAppContext().getResources().getString(R.string.network_error));
                    return;
                }
                getMvpView().showResultAfterSignSuc(result);
            }

            @Override
            public void onFailure(int code, String msg) {
                if (null == getMvpView()) {
                    return;
                }
                getMvpView().showSignErr(msg);
            }
        });
    }


    public void receiveSignInGift() {
        signInModel.receiveSignInGift(new HttpRequestCallBack<SignAwardResult>() {

            @Override
            public void onSuccess(String message, SignAwardResult result) {
                if (null == getMvpView()) {
                    return;
                }
                if (null == result) {
                    getMvpView().showSignErr(BasicConfig.INSTANCE.getAppContext().getResources().getString(R.string.network_error));
                    return;
                }
                //这里需要刷新礼物列表
                CoreManager.getCore(IGiftCore.class).requestGiftInfos();
                //视为成功签到，直接修改状态为已经领取
                getMvpView().showResultAfterSignSuc(result);
            }

            @Override
            public void onFailure(int code, String msg) {
                if (null == getMvpView()) {
                    return;
                }
                getMvpView().showSignErr(msg);
            }
        });
    }
}
