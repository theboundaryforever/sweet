package com.yuhuankj.tmxq.ui.launch.middle;

import android.os.Bundle;

import com.netease.nim.uikit.NimUIKit;
import com.netease.nimlib.sdk.NimIntent;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.OpenRoomNotiAttachment;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;

import java.util.ArrayList;

/**
 * Created by chenran on 2017/8/5.
 */

public class NimMiddleActivity extends BaseActivity{

    private final String TAG = NimMiddleActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtils.d(TAG, "onCreate-云信消息通知跳转");
        if (getIntent() != null) {
            ArrayList<IMMessage> messages = (ArrayList<IMMessage>)
                    getIntent().getSerializableExtra(NimIntent.EXTRA_NOTIFY_CONTENT);
            if (messages != null && messages.size() > 0) {
                IMMessage imMessage = messages.get(messages.size()-1);
                if (imMessage.getMsgType() == MsgTypeEnum.custom) {
                    CustomAttachment attachment = (CustomAttachment) imMessage.getAttachment();
                    if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_OPEN_ROOM_NOTI) {
                        OpenRoomNotiAttachment noticeAttachment = (OpenRoomNotiAttachment) attachment;
                        if (noticeAttachment.getUid() > 0) {
                            RoomServiceScheduler.getInstance().enterRoom(this,
                                    noticeAttachment.getUid(), noticeAttachment.getRoomType());
                        }
                    } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT
                            && attachment.getSecond() == CustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_GIFT
                            && imMessage.getSessionType() == SessionTypeEnum.P2P) {
                        LogUtils.d(TAG, "onCreate-私聊-礼物消息");
                        NimUIKit.startP2PSession(this, imMessage.getSessionId());
                    }
                } else if (imMessage.getMsgType() == MsgTypeEnum.text || imMessage.getMsgType() == MsgTypeEnum.audio
                        || imMessage.getMsgType() == MsgTypeEnum.image) {
                    LogUtils.d(TAG, "onCreate-私聊-文本|图片|音频消息");
                    NimUIKit.startP2PSession(this, imMessage.getSessionId());
                }
            }
        }
        finish();
    }
}
