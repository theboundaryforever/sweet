package com.yuhuankj.tmxq.ui.search.presenter;

import com.netease.nim.uikit.common.util.string.StringUtil;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.room.bean.SearchRoomPersonInfo;
import com.yuhuankj.tmxq.ui.search.interfaces.SearchFriendView;

import java.util.List;
import java.util.Map;


public class SearchFriendPresenter extends AbstractMvpPresenter<SearchFriendView> {

    private final String TAG = SearchFriendPresenter.class.getSimpleName();


    public SearchFriendPresenter() {
    }

    public void searchFriend(String content){
        LogUtils.d(TAG,"searchFriend-content:"+content);
        if (!StringUtil.isEmpty(content)) {
            Map params = CommonParamUtil.getDefaultParam();
            params.put("key", content);
            params.put("type", "2");
            params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
            params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid()+"");

            OkHttpManager.getInstance().getRequest(UriProvider.searchByType(), params, new OkHttpManager.MyCallBack<ServiceResult<List<SearchRoomPersonInfo>>>() {
                @Override
                public void onError(Exception e) {
                    e.printStackTrace();
                    if(null != getMvpView()){
                        getMvpView().onFriendSearched(false,e.getMessage(),null);
                    }
                }

                @Override
                public void onResponse(ServiceResult<List<SearchRoomPersonInfo>> response) {
                    if(null != getMvpView()){
                        if (null != response && response.isSuccess()) {
                            getMvpView().onFriendSearched(true,response.getMessage(),response.getData());
                        }else{
                            getMvpView().onFriendSearched(false,"",null);
                        }
                    }
                }
            });
        }
    }

}
