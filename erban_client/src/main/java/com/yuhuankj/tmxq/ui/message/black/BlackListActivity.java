package com.yuhuankj.tmxq.ui.message.black;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.netease.nim.uikit.cache.FriendDataCache;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.tencent.bugly.crashreport.BuglyLog;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.JavaUtil;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.ui.message.friend.FriendListAdapter;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;

import java.util.List;

/**
 * 黑名单列表
 *
 * @author weihaitao
 * @date 2019/4/17
 */
@CreatePresenter(BlackListPresenter.class)
public class BlackListActivity extends BaseMvpActivity<BlackListView, BlackListPresenter>
        implements BlackListView, SwipeRefreshLayout.OnRefreshListener {

    private final String TAG = BlackListActivity.class.getSimpleName();
    private final int dataLoadStartPage = 1;
    private final int dataLoadPageSize = 10;
    private SwipeRefreshLayout srlDownLoadAnim;
    private RecyclerView rvFriendList;
    private FriendListAdapter friendListAdapter;
    private int nextDataLoadPage = 1;
    private boolean isLoadingData = false;

    public static void start(Context context) {
        context.startActivity(new Intent(context, BlackListActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_list);
        initTitleBar(getResources().getString(R.string.setting_black_list));
        if(null != mTitleBar){
            mTitleBar.setCommonBackgroundColor(Color.WHITE);
        }
        initView();
        initListener();
    }

    FriendDataCache.FriendDataChangedObserver friendDataChangedObserver = new FriendDataCache.FriendDataChangedObserver() {

        @Override
        public void onAddedOrUpdatedFriends(List<String> accounts) {

        }

        @Override
        public void onDeletedFriends(List<String> accounts) {

        }

        @Override
        public void onAddUserToBlackList(List<String> accounts) {
            LogUtils.d(TAG,"onAddUserToBlackList");
            //直接重新加载，不做数据的增删处理
            loadData();
        }

        @Override
        public void onRemoveUserFromBlackList(List<String> accounts) {
            LogUtils.d(TAG,"onRemoveUserFromBlackList");
            //直接重新加载，不做数据的增删处理
            loadData();
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        if (ListUtils.isListEmpty(friendListAdapter.getData())) {
            loadData();
        }
    }

    private void initView() {
        srlDownLoadAnim = (SwipeRefreshLayout) findViewById(R.id.srlDownLoadAnim);
        srlDownLoadAnim.setEnabled(true);
        rvFriendList = (RecyclerView) findViewById(R.id.rvFriendList);
        LinearLayoutManager linearLayoutManager = new RecyclerViewNoBugLinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvFriendList.setLayoutManager(linearLayoutManager);
        friendListAdapter = new FriendListAdapter(R.layout.list_item_friend, com.yuhuankj.tmxq.BR.userInfo);
        rvFriendList.setAdapter(friendListAdapter);
    }

    private void initListener() {
        srlDownLoadAnim.setOnRefreshListener(this);
        friendListAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (adapter.getData().size() > position) {
                    NimUserInfo nimUserInfo = (NimUserInfo) adapter.getData().get(position);
                    UserInfoActivity.start(BlackListActivity.this, JavaUtil.str2long(nimUserInfo.getAccount()));
                }
            }
        });
        FriendDataCache.getInstance().registerFriendDataChangedObserver(friendDataChangedObserver, true);
    }

    private void loadData() {
        BuglyLog.d(TAG, "loadData-isLoadingData:" + isLoadingData);
        if (!isLoadingData) {
            nextDataLoadPage = dataLoadStartPage;
            isLoadingData = true;
            getDialogManager().showProgressDialog(this, getResources().getString(R.string.network_loading));
            getMvpPresenter().getBlackList(nextDataLoadPage, dataLoadPageSize);
        }
    }

    @Override
    public void onRefresh() {
        if (!isLoadingData) {
            nextDataLoadPage = dataLoadStartPage;
            isLoadingData = true;
            BuglyLog.d(TAG, "onRefresh-nextDataLoadPage:" + nextDataLoadPage);
            getMvpPresenter().getBlackList(nextDataLoadPage, dataLoadPageSize);
        }
    }


    @Override
    public void showBlackList(List list) {
        LogUtils.d(TAG, "showBlackList");
        nextDataLoadPage += 1;
        friendListAdapter.setNewData(list);
        isLoadingData = false;
        getDialogManager().dismissDialog();
        srlDownLoadAnim.setRefreshing(false);
    }

    @Override
    public void showBlackListEmptyView() {
        LogUtils.d(TAG, "showBlackListEmptyView");
        friendListAdapter.setEmptyView(getEmptyView(rvFriendList, getString(R.string.no_black_text)));
        isLoadingData = false;
        getDialogManager().dismissDialog();
        srlDownLoadAnim.setRefreshing(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        FriendDataCache.getInstance().registerFriendDataChangedObserver(friendDataChangedObserver, false);
    }
}
