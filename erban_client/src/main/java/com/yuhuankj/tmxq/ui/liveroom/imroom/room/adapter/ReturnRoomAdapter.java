package com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter;

import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.SingleAudioRoomEnitity;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

/**
 * oppo市场，首页，单人音频直播间列表，adapter
 *
 * @author weihaitao
 * @date 2019年5月21日 17:18:56
 */
public class ReturnRoomAdapter extends BaseQuickAdapter<SingleAudioRoomEnitity, BaseViewHolder> {

    private int imvIconWidthAndHeight = 0;

    public ReturnRoomAdapter(List<SingleAudioRoomEnitity> data) {
        super(R.layout.item_room_return_room_list, data);
        imvIconWidthAndHeight = DisplayUtility.dp2px(mContext, 97);
    }

    @Override
    protected void convert(BaseViewHolder helper, SingleAudioRoomEnitity item) {
        helper.addOnClickListener(R.id.rlRoomInfo);
        ImageView ivRoomOwnerAvatar = helper.getView(R.id.ivRoomOwnerAvatar);
        TextView tvOnlineNum = helper.getView(R.id.tvOnlineNum);
        ImageView ivLiving = helper.getView(R.id.ivLiving);
        ImageView ivGender = helper.getView(R.id.ivGender);
        TextView roomTag = helper.getView(R.id.roomTag);
        TextView tvRoomTitle = helper.getView(R.id.tvRoomTitle);

        if (GlideContextCheckUtil.checkContextUsable(mContext)) {
            GlideApp.with(mContext).load(ImageLoadUtils.toThumbnailUrl(imvIconWidthAndHeight,
                    imvIconWidthAndHeight, item.getAvatar()))
                    .dontAnimate()
                    .placeholder(R.mipmap.icon_room_return_room_list_default_avatar)
                    .error(R.mipmap.icon_room_return_room_list_default_avatar)
                    .transforms(new CenterCrop(), new RoundedCorners(DisplayUtility.dp2px(mContext, 6)))
                    .into(ivRoomOwnerAvatar);

            GlideApp.with(mContext).asGif().load(R.drawable.anim_oppo_single_room_living)
                    .diskCacheStrategy(DiskCacheStrategy.ALL).into(ivLiving);
        }

        ivGender.setImageResource(item.getGender() == 1 ? R.drawable.icon_man : R.drawable.icon_women);

        tvOnlineNum.setText(String.valueOf(item.getOnlineNum()));
        String title = item.getTitle();
        if (!TextUtils.isEmpty(title) && title.length() > 7) {
            title = mContext.getResources().getString(R.string.nick_length_max_six, title.substring(0, 7));
        }
        tvRoomTitle.setText(title);

        helper.addOnClickListener(R.id.rlRoomInfo);

        roomTag.setText(item.getRoomTag());
    }
}