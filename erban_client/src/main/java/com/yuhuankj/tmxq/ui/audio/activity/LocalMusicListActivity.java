package com.yuhuankj.tmxq.ui.audio.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.netease.nimlib.sdk.StatusCode;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomKickOutEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.TimeUtils;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.im.login.IIMLoginClient;
import com.tongdaxing.xchat_core.music.IMusicDownloaderCoreClient;
import com.tongdaxing.xchat_core.player.IPlayerCore;
import com.tongdaxing.xchat_core.player.IPlayerCoreClient;
import com.tongdaxing.xchat_core.player.bean.LocalMusicInfo;
import com.tongdaxing.xchat_core.room.IRoomCoreClient;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.databinding.ActivityLocalMusicListBinding;
import com.yuhuankj.tmxq.ui.audio.adapter.LocalMusicListAdapter;
import com.yuhuankj.tmxq.ui.widget.DividerItemDecoration;
import com.yuhuankj.tmxq.ui.widget.VoiceSeekDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;

import static com.tongdaxing.xchat_core.player.IPlayerCore.PLAY_STATUS_MUSIC_LIST_EMPTY;
import static java.util.concurrent.Executors.newCachedThreadPool;

/**
 * Created by chenran on 2017/10/28.
 */

public class LocalMusicListActivity extends BaseActivity implements View.OnClickListener, LocalMusicListAdapter.OnLocalMusicAddClickListener, SeekBar.OnSeekBarChangeListener {

    private final String TAG = LocalMusicListActivity.class.getSimpleName();

    private String imgBgUrl;
    private LocalMusicListAdapter adapter;
    private ActivityLocalMusicListBinding musicListBinding;

    private LocalMusicInfo currMusicInfo;
    private List<LocalMusicInfo> mLocalMusicInfoList = new ArrayList<>();
    private List<LocalMusicInfo> mSearchMusicInfoList = new ArrayList<>();

    public static void start(Context context, String imgBgUrl) {
        Intent intent = new Intent(context, LocalMusicListActivity.class);
        intent.putExtra("imgBgUrl", imgBgUrl);
        context.startActivity(intent);
    }

    private ExecutorService executorService = newCachedThreadPool();

    private Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (null != musicListBinding.ivScan && musicListBinding.ivScan.getVisibility() == View.GONE) {
                LogUtils.d(TAG, "handleMessage-->refreshLocalMusicList");
                refreshLocalMusicList(mSearchMusicInfoList, true);
            }
            return true;
        }
    });
    private TextWatcher searchWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (null != mLocalMusicInfoList && mLocalMusicInfoList.size() > 0) {
                String key = s.toString();
                LogUtils.d(TAG, "afterTextChanged-key:" + key);
                if (!TextUtils.isEmpty(key) && !TextUtils.isEmpty(key.trim())) {
                    searchMusicFromLocalList(key);
                } else {
                    refreshLocalMusicList(mLocalMusicInfoList,false);
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View content = findViewById(android.R.id.content);
        ViewGroup.LayoutParams params = content.getLayoutParams();
        params.height = getResources().getDisplayMetrics().heightPixels;

        musicListBinding = DataBindingUtil.setContentView(this, R.layout.activity_local_music_list);
        musicListBinding.setClick(this);
        musicListBinding.etSearch.addTextChangedListener(searchWatcher);

        initMusicPlayControlView();

        imgBgUrl = getIntent().getStringExtra("imgBgUrl");
        initData();
    }

    private void initData() {
        mLocalMusicInfoList = CoreManager.getCore(IPlayerCore.class).requestLocalMusicInfos();
        adapter = new LocalMusicListAdapter(this);
        adapter.setLocalMusicInfos(mLocalMusicInfoList);
        adapter.setOnLocalMusicAddClickListener(this);
        LinearLayoutManager linearLayoutManager = new RecyclerViewNoBugLinearLayoutManager(this);
        musicListBinding.rvLocalMusic.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, linearLayoutManager.getOrientation(),
                1, R.color.color_1Affffff);
        dividerItemDecoration.setIngoreLastDividerItem(true);
        musicListBinding.rvLocalMusic.addItemDecoration(dividerItemDecoration);
        musicListBinding.rvLocalMusic.setAdapter(adapter);

        if (mLocalMusicInfoList.size() != 0) {
            musicListBinding.rvLocalMusic.setVisibility(View.VISIBLE);
            musicListBinding.llLocalMusicEmpty.setVisibility(View.GONE);
        }

        currMusicInfo = CoreManager.getCore(IPlayerCore.class).getCurrLocalMusicInfo();
        updateMusicPlayView();
        //本地增加扫描机制
        if (!CoreManager.getCore(IPlayerCore.class).isScaning()) {
            getDialogManager().showProgressDialog(this, getResources().getString(R.string.loading));
            CoreManager.getCore(IPlayerCore.class).refreshLocalMusic();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_report:
            case R.id.fl_report:
                try {
                    long songId = Long.valueOf(currMusicInfo.getSongId());
                    if (null != currMusicInfo && songId > 0L) {
                        ReportMusicActivity.start(this, songId);
                    }
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                } catch (IllegalStateException ise) {
                    ise.printStackTrace();
                }

                break;
            case R.id.fl_musicVoice:
                VoiceSeekDialog voiceSeekDialog = new VoiceSeekDialog(this);
                voiceSeekDialog.show();
                break;
            case R.id.iv_musicPlayStatus:
                int state = CoreManager.getCore(IPlayerCore.class).getState();
                if (state == IPlayerCore.STATE_PLAY) {
                    CoreManager.getCore(IPlayerCore.class).pause();
                } else if (state == IPlayerCore.STATE_PAUSE) {
                    int result = CoreManager.getCore(IPlayerCore.class).play(currMusicInfo);
                    if (result < 0) {
                        toast(R.string.music_play_failed_file_format_error);
                    }
                } else {
                    int result = CoreManager.getCore(IPlayerCore.class).play(null);
                    if (result < 0) {
                        if (result == PLAY_STATUS_MUSIC_LIST_EMPTY) {
                            toast(R.string.music_play_failed_list_empty);
                        } else {
                            toast(R.string.music_play_failed_file_format_error);
                        }
                    }
                }
                break;
            case R.id.tv_searchLocal:
                musicListBinding.ivScan.setVisibility(View.VISIBLE);
                musicListBinding.tvLocalTitle.setVisibility(View.VISIBLE);
                musicListBinding.tvSearchLocal.setVisibility(View.GONE);
                musicListBinding.llSearchLocalMusic.setVisibility(View.GONE);
                musicListBinding.etSearch.removeTextChangedListener(searchWatcher);
                handler.removeMessages(0);
                refreshLocalMusicList(mLocalMusicInfoList,false);
                break;
            case R.id.iv_scan:
                musicListBinding.ivScan.setVisibility(View.GONE);
                musicListBinding.tvLocalTitle.setVisibility(View.GONE);
                musicListBinding.tvSearchLocal.setVisibility(View.VISIBLE);
                musicListBinding.llSearchLocalMusic.setVisibility(View.VISIBLE);
                musicListBinding.etSearch.requestFocus();
                musicListBinding.etSearch.addTextChangedListener(searchWatcher);
                break;
            case R.id.back_btn:
                finish();
                break;
            default:
        }
    }

    private void searchMusicFromLocalList(final String key) {
        LogUtils.d(TAG, "searchMusicFromLocalList-key:" + key);
        try {
            LogUtils.d(TAG, "searchMusicFromLocalList-isShutdown:" + executorService.isShutdown());
            //https://blog.csdn.net/wzy_1988/article/details/38922449
            if (!executorService.isShutdown()) {
                executorService.execute(new Runnable() {
                    @Override
                    public void run() {
                        synchronized (mSearchMusicInfoList) {
                            if (null != musicListBinding.ivScan && musicListBinding.ivScan.getVisibility() == View.GONE) {
                                LogUtils.d(TAG, "searchMusicFromLocalList-->execute task");
                                mSearchMusicInfoList.clear();
                                for (LocalMusicInfo musicInfo : mLocalMusicInfoList) {
                                    List<String> artistNames = musicInfo.getArtistNames();
                                    if (null != artistNames && artistNames.size() > 0) {
                                        if (musicInfo.getSongName().toLowerCase().contains(key.toLowerCase()) || artistNames.get(0).toLowerCase().contains(key.toLowerCase())) {
                                            if (!mSearchMusicInfoList.contains(musicInfo)) {
                                                mSearchMusicInfoList.add(musicInfo);
                                            }
                                        }
                                    }
                                }
                                LogUtils.d(TAG, "searchMusicFromLocalList-->execute task --> handler message.what=0");
                                handler.sendEmptyMessage(0);
                            }
                        }
                    }
                });
            }
        } catch (RejectedExecutionException reex) {
            reex.printStackTrace();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getDialogManager().dismissDialog();
        handler.removeMessages(0);
        executorService.shutdownNow();
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onRefreshLocalMusic(List<LocalMusicInfo> localMusicInfoList) {
        mLocalMusicInfoList = localMusicInfoList;
        boolean isListEmpty = ListUtils.isListEmpty(localMusicInfoList);
        getDialogManager().dismissDialog();
        toast(isListEmpty ? R.string.music_local_scan_empty : R.string.music_local_scan_comp);
        refreshLocalMusicList(mLocalMusicInfoList,false);
    }

    /**
     *
     * @param localMusicInfoList
     * @param handleRefresh Cannot call this method while RecyclerView is computing a layout or scrolling android.support.v7.widg
     */
    private void refreshLocalMusicList(List<LocalMusicInfo> localMusicInfoList,boolean handleRefresh) {
        boolean isListEmpty = ListUtils.isListEmpty(localMusicInfoList);
        LogUtils.d(TAG, "refreshLocalMusicList-handleRefresh:" + handleRefresh + " isListEmpty:" + isListEmpty);
        //list指针地址拷贝，需要重新调用adater的setxxx的方法
        adapter.setLocalMusicInfos(localMusicInfoList);
        if (handleRefresh){
            musicListBinding.rvLocalMusic.post(new Runnable() {
                @Override
                public void run() {
                    adapter.notifyDataSetChanged();
                }
            });
        }else {
            adapter.notifyDataSetChanged();
        }
        if (isListEmpty) {
            musicListBinding.rvLocalMusic.setVisibility(View.GONE);
            musicListBinding.llLocalMusicEmpty.setVisibility(View.VISIBLE);
        } else {
            musicListBinding.rvLocalMusic.setVisibility(View.VISIBLE);
            musicListBinding.llLocalMusicEmpty.setVisibility(View.GONE);
        }
    }

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onBeKickOut(ChatRoomKickOutEvent.ChatRoomKickOutReason reason) {
        finish();
    }

    @CoreEvent(coreClientClass = IIMLoginClient.class)
    public void onKickedOut(StatusCode code) {
        finish();
    }


    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient.class)
    public void onHotMusicDownloadCompleInfoUpdated(LocalMusicInfo localMusicInfo) {
        synchronized (mLocalMusicInfoList) {
            mLocalMusicInfoList.add(0, localMusicInfo);
            adapter.notifyItemInserted(0);
        }
    }

//==============================音乐播放控制面板===================================

    private View fl_report;
    private View tv_report;
    private View fl_musicVoice;
    private View rl_musicPlayControl;
    private TextView tv_musicName;
    private ImageView iv_musicPlayStatus;
    private SeekBar sb_musicPlayProgress;
    private TextView tv_totalPlayTime;
    private TextView tv_currPlayTime;

    private boolean isSBProgressChangedByUserTouch = false;

    private void initMusicPlayControlView() {
        tv_musicName = (TextView) findViewById(R.id.tv_musicName);
        tv_totalPlayTime = (TextView) findViewById(R.id.tv_totalPlayTime);
        tv_currPlayTime = (TextView) findViewById(R.id.tv_currPlayTime);
        fl_report = findViewById(R.id.fl_report);
        tv_report = findViewById(R.id.tv_report);
        rl_musicPlayControl = findViewById(R.id.rl_musicPlayControl);
        fl_musicVoice = findViewById(R.id.fl_musicVoice);
        iv_musicPlayStatus = (ImageView) findViewById(R.id.iv_musicPlayStatus);
        sb_musicPlayProgress = (SeekBar) findViewById(R.id.sb_musicPlayProgress);
        sb_musicPlayProgress.setMax(100);
        sb_musicPlayProgress.setOnSeekBarChangeListener(this);
        iv_musicPlayStatus.setOnClickListener(this);
        fl_report.setOnClickListener(this);
        tv_report.setOnClickListener(this);
        fl_musicVoice.setOnClickListener(this);
    }

    private void updateMusicPlayView() {
        String dur = null;
        if (currMusicInfo != null && CoreManager.getCore(IPlayerCore.class).getState() != IPlayerCore.STATE_STOP) {
            rl_musicPlayControl.setVisibility(View.VISIBLE);
            long songId = 0;
            try {
                songId = Long.valueOf(currMusicInfo.getSongId());
            } catch (NumberFormatException nfe) {
                nfe.printStackTrace();
            } catch (IllegalStateException ise) {
                ise.printStackTrace();
            }
            fl_report.setVisibility(songId > 0 ? View.VISIBLE : View.INVISIBLE);
            tv_musicName.setText(currMusicInfo.getSongName());
            dur = TimeUtils.getFormatTimeString(currMusicInfo.getDuration(), "min:sec");
            int state = CoreManager.getCore(IPlayerCore.class).getState();
            if (state == IPlayerCore.STATE_PLAY) {
                iv_musicPlayStatus.setImageResource(R.mipmap.icon_music_pause);
            } else {
                iv_musicPlayStatus.setImageResource(R.mipmap.icon_music_play);
            }
        } else {
            tv_musicName.setText(getResources().getString(R.string.music_play_empty));
            dur = TimeUtils.getFormatTimeString(0, "min:sec");
            iv_musicPlayStatus.setImageResource(R.mipmap.icon_music_play);
            fl_report.setVisibility(View.INVISIBLE);
            rl_musicPlayControl.setVisibility(View.GONE);
        }
        tv_totalPlayTime.setText(getResources().getString(R.string.music_play_control_dur, dur));
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicProgressUpdate(long total, long current) {
        int progress = (int) (current * 100 / total);
        LogUtils.d(TAG, "onMusicProgressUpdate-total:" + total + " current:" + current + " progress:" + progress + " isSBProgressChangedByUserTouch:" + isSBProgressChangedByUserTouch);
        if (isSBProgressChangedByUserTouch) {
            return;
        }
        tv_currPlayTime.setText(TimeUtils.getFormatTimeString(current, "min:sec"));
        tv_totalPlayTime.setText(getResources().getString(R.string.music_play_control_dur, TimeUtils.getFormatTimeString(total, "min:sec")));
        sb_musicPlayProgress.setProgress(progress);
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicPlaying(LocalMusicInfo localMusicInfo) {
        this.currMusicInfo = localMusicInfo;
        updateMusicPlayView();
        //changeMusicPlayStatus(localMusicInfo);
        adapter.notifyDataSetChanged();
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicPause(LocalMusicInfo localMusicInfo) {
        this.currMusicInfo = localMusicInfo;
        updateMusicPlayView();
//        changeMusicPlayStatus(localMusicInfo);
        adapter.notifyDataSetChanged();
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicStop() {
        updateMusicPlayView();
    }

    @Override
    public void onLocalMusicAddClicked(LocalMusicInfo localMusicInfo) {
        toast(R.string.music_list_local_add_success);
//        if(!localMusicInfo.isInPlayerList()){
        CoreManager.getCore(IPlayerCore.class).addMusicToPlayerListAndUpdateDb(localMusicInfo);
        localMusicInfo.setInPlayerList(true);
//        String key = musicListBinding.etSearch.getText().toString();
//        int index = 0;
//        if(!TextUtils.isEmpty(key) && !TextUtils.isEmpty(key.trim())){
//            index = mSearchMusicInfoList.indexOf(localMusicInfo);
//        }else{
//            index = mLocalMusicInfoList.indexOf(localMusicInfo);
//        }
//        adapter.notifyItemChanged(index);
        //解决使用adapter.notifyItemChanged(index);时可能出现用户操作过快导致的java.lang.IndexOutOfBoundsException
        adapter.notifyDataSetChanged();
//        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        isSBProgressChangedByUserTouch = true;
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        CoreManager.getCore(IPlayerCore.class).setAudioMixCurrPosition(seekBar.getProgress());
        isSBProgressChangedByUserTouch = false;
    }

    private void changeMusicPlayStatus(LocalMusicInfo localMusicInfo) {
        int index = mLocalMusicInfoList.indexOf(localMusicInfo);
        if (-1 != index) {
            adapter.notifyItemChanged(index);
        }
    }

    @Override
    public boolean isStatusBarDarkFont() {
        return false;
    }
}
