package com.yuhuankj.tmxq.ui.me.wallet.income;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.ui.me.wallet.exchange.ExchangeGoldActivity;
import com.yuhuankj.tmxq.ui.me.wallet.income.withdraw.diamond.WithdrawActivity;

import java.util.Locale;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MadisonRong on 08/01/2018.
 */
@CreatePresenter(IncomePresenter.class)
public class MyIncomeActivity extends BaseMvpActivity<IIncomeView, IncomePresenter>
        implements IIncomeView, View.OnClickListener {

    private static final String TAG = "MyIncomeActivity";

    @BindView(R.id.tv_my_income_diamond_balance)
    TextView diamondBalanceTextView;
    @BindView(R.id.btn_my_income_exchange_gold)
    Button exchangeGoldButton;
    @BindView(R.id.btn_my_income_withdraw)
    Button withdrawButton;
    @BindString(R.string.my_income)
    String titleContent;
    @BindView(R.id.iv_exchange_awards)
    ImageView ivExchangeAwards;
    private boolean isRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_income);
        ButterKnife.bind(this);
        initTitleBar(titleContent);
        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getMvpPresenter().loadWalletInfo();
    }

    private void initViews() {
        diamondBalanceTextView.setOnClickListener(this);
        exchangeGoldButton.setOnClickListener(this);
        withdrawButton.setOnClickListener(this);
        if (CoreManager.getCore(VersionsCore.class).getConfigData().num("isExchangeAwards") == 1) {
            ivExchangeAwards.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        getMvpPresenter().handleClick(view.getId());
    }

    @Override
    public void refreshUserWalletBalance(WalletInfo walletInfo) {
        Log.i(TAG, "refreshUserWalletBalance: " + walletInfo);
        diamondBalanceTextView.setText(String.format(Locale.getDefault(), "%.2f", walletInfo.diamondNum));
    }

    @Override
    public void getUserWalletInfoFail(String error) {
        toast(error);
        diamondBalanceTextView.setText("0");
    }

    @Override
    public void handleClick(int id) {
        switch (id) {
            case R.id.btn_my_income_exchange_gold:
                startActivity(new Intent(this, ExchangeGoldActivity.class));
                break;

            case R.id.btn_my_income_withdraw:
                isRequest = true;
                getMvpPresenter().hasBindPhone();
                break;
            default:
                break;
        }
    }

    @Override
    public void hasBindPhone() {
        if (!isRequest) {
            return;
        }
        isRequest = false;

        //验证手机号码被绑定后，再验证是否已经实名认证
        if (null != CoreManager.getCore(IUserCore.class) && CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo() != null) {
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            if (!userInfo.isRealNameAudit()) {
                getDialogManager().showAuthBeforeOperaDialog(this);
                return;
            }
        }

        startActivity(new Intent(this, WithdrawActivity.class));
    }

    @Override
    public void hasBindPhoneFail(String error) {
        if (!isRequest) {
            return;
        }
        isRequest = false;
        //未绑定手机号，则跳转绑定手机号界面，绑定后，重新点击提现按钮进入后续流程
//        startActivity(new Intent(this, BinderPhoneActivity.class));
        //流程修改为，未绑定手机号，也依旧提示
        getDialogManager().showAuthBeforeOperaDialog(this);
    }
}
