package com.yuhuankj.tmxq.ui.login;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.growingio.android.sdk.collection.GrowingIO;
import com.jph.takephoto.app.TakePhotoActivity;
import com.jph.takephoto.compress.CompressConfig;
import com.jph.takephoto.model.CropOptions;
import com.jph.takephoto.model.TResult;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.netease.nim.uikit.common.util.sys.TimeUtil;
import com.sleepbot.datetimepicker.time.RadialPickerLayout;
import com.sleepbot.datetimepicker.time.TimePickerDialog;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.erban.libcommon.utils.TimeUtils;
import com.tongdaxing.erban.libcommon.utils.file.JXFileUtils;
import com.tongdaxing.erban.libcommon.utils.toast.ToastCompat;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.auth.ThirdUserInfo;
import com.tongdaxing.xchat_core.file.IFileCore;
import com.tongdaxing.xchat_core.file.IFileCoreClient;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.linked.ILinkedCore;
import com.tongdaxing.xchat_core.linked.LinkedInfo;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.permission.PermissionActivity;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.widget.address.CityWheelDialog;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zhouxiangfeng
 * @date 2017/5/12
 */

public class AddUserInfoActivity extends TakePhotoActivity implements IMvpBaseView, View.OnClickListener, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private static final String CAMERA_PREFIX = "picture_";
    private static final String BMP_TEMP_NAME = "bmp_temp_name";
    private static final String TAG = "AddUserInfoActivity";

    private DatePickerDialog datePickerDialog;
    private String avatarUrl;
    private String avatarUrlWX;

    private String mCameraCapturingName;
    private File cameraOutFile;
    private File photoFile;
    private String birth;
    private String data = "757353600000";
    private int sexCode;

    private EditText tvNick;
    private TextView tvBirth;
    private ImageView imvMan;
    private ImageView imvWoman;
    private ImageView civAvatar;
    private TextView tvCity;
    private TextView tvSave;

    public static void start(Context context) {
        Intent intent = new Intent(context, AddUserInfoActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addinfo);
        setSwipeBackEnable(false);
        init();
        addWXUserInfo();
        initTitleBar("完善资料");
    }

    InputFilter enterBlankInputFilter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            if (source.equals(" ") || source.toString().contentEquals("\n")) {
                return "";
            } else {
                return null;
            }
        }
    };

    private void addWXUserInfo() {
        ThirdUserInfo thirdUserInfo = CoreManager.getCore(IAuthCore.class).getThirdUserInfo();
        if (thirdUserInfo != null) {
            avatarUrlWX = thirdUserInfo.getUserIcon();
            birth = TimeUtil.getDateTimeString(Long.valueOf(data), "yyyy-MM-dd");
            tvBirth.setText(birth);
            if (!StringUtil.isEmpty(thirdUserInfo.getUserGender())) {
                if (thirdUserInfo.getUserGender().equals("m")) {
                    sexCode = 1;
                    imvWoman.setSelected(false);
                    imvMan.setSelected(true);
                } else {
                    sexCode = 2;
                    imvWoman.setSelected(true);
                    imvMan.setSelected(false);
                }
            } else {
                sexCode = 1;
                imvWoman.setSelected(false);
                imvMan.setSelected(true);
            }
            String nick = thirdUserInfo.getUserName();
            if (!StringUtil.isEmpty(nick)) {
                nick = nick.replaceAll("\n", "").replaceAll(" ", "");
            }
            if (!StringUtil.isEmpty(nick) && nick.length() > 15) {
                tvNick.setText(nick.substring(0, 15));
            } else {
                tvNick.setText(nick);
            }
            ImageLoadUtils.loadSmallRoundBackground(this, avatarUrlWX, civAvatar);
        }
    }

    private void init() {
        tvNick = (EditText) findViewById(R.id.tv_nick);
        GrowingIO.getInstance().trackEditText(tvNick);
        tvBirth = (TextView) findViewById(R.id.tv_birth);
        imvMan = (ImageView) findViewById(R.id.imvMan);
        imvWoman = (ImageView) findViewById(R.id.imvWoman);
        civAvatar = (ImageView) findViewById(R.id.civ_avatar);
        tvCity = (TextView) findViewById(R.id.tvCity);
        tvSave = (TextView) findViewById(R.id.tvSave);
        findViewById(R.id.llMan).setOnClickListener(this);
        findViewById(R.id.llWoman).setOnClickListener(this);
        findViewById(R.id.tvSave).setOnClickListener(this);
        findViewById(R.id.civ_avatar).setOnClickListener(this);
        findViewById(R.id.tv_birth).setOnClickListener(this);
        findViewById(R.id.tvCity).setOnClickListener(this);
        tvNick.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setEnable();
            }
        });
        //setDefaultArea();

        int year = TimeUtils.getYear(Long.parseLong(data));
        int month = TimeUtils.getMonth(Long.parseLong(data));
        int day = TimeUtils.getDayOfMonth(Long.parseLong(data));
        datePickerDialog = DatePickerDialog.newInstance(this, year, (month - 1), day, true);
        tvNick.setFilters(new InputFilter[]{enterBlankInputFilter});
    }

    public static boolean isExitSDCard() {
        return Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
    }

    @Override
    protected void onLeftClickListener() {
        CoreManager.getCore(IAuthCore.class).logout();
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llWoman:
                sexCode = 2;
                imvWoman.setSelected(true);
                imvMan.setSelected(false);
                setEnable();
                break;
            case R.id.llMan:
                sexCode = 1;
                imvWoman.setSelected(false);
                imvMan.setSelected(true);
                setEnable();
                break;
            case R.id.tv_birth:
                if (datePickerDialog.isAdded()) {
                    datePickerDialog.dismiss();
                } else {
                    datePickerDialog.setVibrate(true);
                    datePickerDialog.setYearRange(1945, 2017);
                    datePickerDialog.show(getSupportFragmentManager(), "DATEPICKER_TAG");
                }
                break;
            case R.id.tvCity:
                String curCity = tvCity.getText().toString();
                CityWheelDialog cityWheelDialog = new CityWheelDialog(this, curCity, new CityWheelDialog.CallBack() {
                    @Override
                    public void callback(String area) {
                        tvCity.setText(area);
                        setEnable();
                    }
                });
                cityWheelDialog.show();
                break;
            case R.id.tvSave:
                String nick = tvNick.getText().toString();
                String birth = tvBirth.getText().toString();
                String city = tvCity.getText().toString();
                if (nick.trim().isEmpty()) {
                    ToastCompat.makeText(this, "昵称不能为空！", ToastCompat.LENGTH_SHORT).show();
                    return;
                } else if (city.trim().isEmpty()) {
                    ToastCompat.makeText(this, "城市不能为空！", ToastCompat.LENGTH_SHORT).show();
                    return;
                } else if (birth.trim().isEmpty()) {
                    ToastCompat.makeText(this, "生日不能为空！", ToastCompat.LENGTH_SHORT).show();
                    return;
                } else if (sexCode == 0) {
                    ToastCompat.makeText(this, "请选择性别", ToastCompat.LENGTH_SHORT).show();
                    return;
                }
                if (photoFile != null) {
                    getDialogManager().showProgressDialog(AddUserInfoActivity.this, "正在上传请稍后...");
                    CoreManager.getCore(IFileCore.class).upload(photoFile);
                    return;
                }
                //用户如果自己拍照作为头像就上传，如果为空就代表没拍照，直接拿微信头像上传
                if (avatarUrlWX != null) {
                    avatarUrl = avatarUrlWX;
                } else if (StringUtils.isEmpty(avatarUrl)) {
                    ToastCompat.makeText(this, "请上传头像！", ToastCompat.LENGTH_SHORT).show();
                    return;
                }
                commit();
                break;
            case R.id.civ_avatar:
                ButtonItem upItem = new ButtonItem("拍照上传", new ButtonItem.OnClickListener() {
                    @Override
                    public void onClick() {
                        checkPermissionAndStartCamera();
                    }
                });

                ButtonItem localItem = new ButtonItem("本地相册", new ButtonItem.OnClickListener() {
                    @Override
                    public void onClick() {
                        String mCameraCapturingName = CAMERA_PREFIX + System.currentTimeMillis() + ".jpg";
                        File cameraOutFile = JXFileUtils.getTempFile(AddUserInfoActivity.this, mCameraCapturingName);
                        if (!cameraOutFile.getParentFile().exists()) {
                            cameraOutFile.getParentFile().mkdirs();
                        }
                        Uri uri = Uri.fromFile(cameraOutFile);
                        CompressConfig compressConfig = new CompressConfig.Builder().create();
                        getTakePhoto().onEnableCompress(compressConfig, true);
                        CropOptions options = new CropOptions.Builder().setWithOwnCrop(true).create();
                        getTakePhoto().onPickFromGalleryWithCrop(uri, options);
                    }
                });
                List<ButtonItem> buttonItemList = new ArrayList<>();
                buttonItemList.add(upItem);
                buttonItemList.add(localItem);
                getDialogManager().showCommonPopupDialog(buttonItemList, "取消", false);
                break;
            default:
        }
    }

    private void setEnable() {
        String nick = tvNick.getText().toString();
        String birth = tvBirth.getText().toString();
        String city = tvCity.getText().toString();
        if (nick.trim().isEmpty()) {
            tvSave.setEnabled(false);
            return;
        } else if (city.trim().isEmpty()) {
            tvSave.setEnabled(false);
            return;
        } else if (birth.trim().isEmpty()) {
            tvSave.setEnabled(false);
            return;
        } else if (sexCode == 0) {
            tvSave.setEnabled(false);
            return;
        }
//        //用户如果自己拍照作为头像就上传，如果为空就代表没拍照，直接拿微信头像上传
//        if (avatarUrlWX != null) {
//            avatarUrl = avatarUrlWX;
//        } else if (StringUtils.isEmpty(avatarUrl)) {
//            tvSave.setEnabled(true);
//            return;
//        }
        tvSave.setEnabled(true);
    }

    private void commit() {
        String nick = tvNick.getText().toString();
        String birth = tvBirth.getText().toString();
        String city = tvCity.getText().toString();
        UserInfo userInfo = new UserInfo();
        userInfo.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        userInfo.setBirthStr(birth);
        userInfo.setNick(nick);
        userInfo.setCity(city);
        userInfo.setAvatar(avatarUrl);
        userInfo.setGender(sexCode);
        getDialogManager().showProgressDialog(AddUserInfoActivity.this, "请稍后...");
        LinkedInfo linkedInfo = CoreManager.getCore(ILinkedCore.class).getLinkedInfo();
        String channel = "";
        String roomUid = "";
        String uid = "";
        if (linkedInfo != null) {
            channel = linkedInfo.getChannel();
            roomUid = linkedInfo.getRoomUid();
            uid = linkedInfo.getUid();
        }
        CoreManager.getCore(IUserCore.class).requestCompleteUserInfo(userInfo, channel, uid, roomUid);
        //增加补全信息打点事件
        StatisticManager.get().onEvent(this, StatisticModel.EVENT_ID_LOGIN_REG_ADD_INFO, StatisticModel.getInstance().getUMAnalyCommonMap(this));
    }

    PermissionActivity.CheckPermListener checkPermissionListener = new PermissionActivity.CheckPermListener() {
        @Override
        public void superPermission() {
            mCameraCapturingName = CAMERA_PREFIX + System.currentTimeMillis() + ".jpg";
            cameraOutFile = JXFileUtils.getTempFile(AddUserInfoActivity.this, mCameraCapturingName);
            if (!cameraOutFile.getParentFile().exists()) {
                cameraOutFile.getParentFile().mkdirs();
            }
            Uri uri = Uri.fromFile(cameraOutFile);
            getTakePhoto().onEnableCompress(CompressConfig.ofDefaultConfig(), true);
            getTakePhoto().onPickFromCapture(uri);
        }
    };

    private void checkPermissionAndStartCamera() {
        //低版本授权检查
        checkPermission(checkPermissionListener, R.string.ask_camera, android.Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String path = null;
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        datePickerDialog = null;
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
        String monthstr;
        if ((month + 1) < 10) {
            monthstr = "0" + (month + 1);
        } else {
            monthstr = String.valueOf(month + 1);
        }
        String daystr;
        if (day < 10) {
            daystr = "0" + day;
        } else {
            daystr = String.valueOf(day);
        }
        tvBirth.setText(year + "-" + monthstr + "-" + daystr);

        setEnable();
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {

    }


    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUpload(String url) {
        LogUtils.d(TAG, "onUpload: 这是添加用户更改上传");
        avatarUrl = url;
        getDialogManager().dismissDialog();
        ImageLoadUtils.loadSmallRoundBackground(this, url, civAvatar);
        commit();
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUploadFail() {
        toast("上传失败");
        getDialogManager().dismissDialog();
    }


    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoComplete(UserInfo userInfo) {
        getDialogManager().dismissDialog();
        //完善资料以后重新加载礼物列表,因为完善资料会送礼物给用户
        CoreManager.getCore(IGiftCore.class).requestGiftInfos();
        CoreManager.getCore(IAuthCore.class).setThirdUserInfo(null);
        CoreManager.getCore(IUserCore.class).requestUserInfo(userInfo.getUid());
        finish();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoCompleteFaith(String error) {
        getDialogManager().dismissDialog();
        toast(error);
        LogUtils.i("liao", "onCurrentUserInfoCompleteFaith......................................");
    }

    @Override
    public void takeSuccess(TResult result) {
        photoFile = new File(result.getImage().getCompressPath());
        ImageLoadUtils.loadImage(this, photoFile, civAvatar, R.drawable.ic_userinfo_default);
    }

    @Override
    public void takeFail(TResult result, String msg) {
        toast(msg);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
