package com.yuhuankj.tmxq.ui.nim.game;

import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.uinfo.UserService;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

public class InvitationAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

    public InvitationAdapter(int layoutResId, List data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {
        ImageView imvPortrait = helper.getView(R.id.imvPortrait);
        TextView tvName = helper.getView(R.id.tvName);
        TextView tvInvitation = helper.getView(R.id.tvInvitation);

        // 获取所有好友用户资料
        NimUserInfo user = NIMClient.getService(UserService.class).getUserInfo(item);
        if (null != user && !TextUtils.isEmpty(user.getAvatar())) {
            ImageLoadUtils.loadImage(mContext, user.getAvatar(), imvPortrait);
        }
        tvName.setText(user.getName());
        helper.addOnClickListener(R.id.tvInvitation);
    }
}