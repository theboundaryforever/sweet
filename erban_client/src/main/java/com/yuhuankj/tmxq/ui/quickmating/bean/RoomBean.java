package com.yuhuankj.tmxq.ui.quickmating.bean;

import java.io.Serializable;

public class RoomBean implements Serializable {

    private long roomId;
    private int roomUid;
    private int roomType;

    public long getRoomId() {
        return roomId;
    }

    public int getRoomUid() {
        return roomUid;
    }

    public void setRoomUid(int roomUid) {
        this.roomUid = roomUid;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public int getRoomType() {
        return roomType;
    }

    public void setRoomType(int roomType) {
        this.roomType = roomType;
    }
}
