package com.yuhuankj.tmxq.ui.me.wallet.income.withdraw.diamond;

import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.withdraw.bean.WithdrwaListInfo;
import com.yuhuankj.tmxq.R;

/**
 * <p>  钻石提现界面</p>
 * Created by Administrator on 2017/11/21.
 */
public class WithdrawJewelAdapter extends BaseQuickAdapter<WithdrwaListInfo, BaseViewHolder> {
    public WithdrawJewelAdapter() {
        super(R.layout.withdraw_item);
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, WithdrwaListInfo withdrwaListInfo) {
        if (withdrwaListInfo == null) return;

        try {
            String[] split = withdrwaListInfo.cashProdName.split("钻=");
            baseViewHolder.setText(R.id.list_name, split[1]);
            baseViewHolder.setText(R.id.tv_jewel,split[0]);
        }catch (Exception e){
            baseViewHolder.setText(R.id.list_name, withdrwaListInfo.cashProdName);
        }

        LinearLayout layout = baseViewHolder.getView(R.id.select_withdraw);
        TextView money=baseViewHolder.getView(R.id.list_name);
        TextView text1=baseViewHolder.getView(R.id.text1);
        TextView text2=baseViewHolder.getView(R.id.text2);
        TextView jewel=baseViewHolder.getView(R.id.tv_jewel);
        if (withdrwaListInfo.isSelected){
            layout.setBackgroundResource(R.drawable.img_bg_rise_select);
            money.setTextColor(BasicConfig.INSTANCE.getAppContext().getResources().getColor(R.color.color_FF4D4D));
            text1.setTextColor(BasicConfig.INSTANCE.getAppContext().getResources().getColor(R.color.white));
            text2.setTextColor(BasicConfig.INSTANCE.getAppContext().getResources().getColor(R.color.white));
            jewel.setTextColor(BasicConfig.INSTANCE.getAppContext().getResources().getColor(R.color.white));
        }else {
            layout.setBackgroundResource(R.drawable.img_bg_rise_noselect);
            money.setTextColor(BasicConfig.INSTANCE.getAppContext().getResources().getColor(R.color.black));
            text1.setTextColor(BasicConfig.INSTANCE.getAppContext().getResources().getColor(R.color.color_666666));
            text2.setTextColor(BasicConfig.INSTANCE.getAppContext().getResources().getColor(R.color.color_666666));
            jewel.setTextColor(BasicConfig.INSTANCE.getAppContext().getResources().getColor(R.color.color_FF4D4D));
        }
    }
}
