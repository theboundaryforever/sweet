package com.yuhuankj.tmxq.ui.liveroom.nimroom.gift;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

/**
 * @author xiaoyu
 * @date 2017/12/11
 */

public class GiftAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<GiftInfo> giftInfoList;

    private final String TAG = GiftAdapter.class.getSimpleName();

    public void setGiftInfoList(List<GiftInfo> giftInfoList) {
        this.giftInfoList = giftInfoList;
    }

    private Context context;
    private int index;

    private int gift_image_width = 0;
    private int gift_image_height = 0;
    private int gift_image_anim_height = 0;
    private int gift_image_anim_width = 0;

    public int getIndex() {
        return index;
    }

    private int lastSelectedIndex = -1;
    private int lastScaleBigAnimIndex = -1;
    private int lastScaleSmallAnimIndex = -1;

    public void setIndex(int index) {
        if (this.index != index) {
            lastSelectedIndex = this.index;
            this.index = index;
            notifyDataSetChanged();
        }

    }


    GiftAdapter(Context context, List<GiftInfo> giftInfoList) {
        this.giftInfoList = giftInfoList;
        this.context = context;
        gift_image_width = (int) context.getResources().getDimension(R.dimen.gift_image_width);
        gift_image_height = (int) context.getResources().getDimension(R.dimen.gift_image_height);
        gift_image_anim_height = (int) context.getResources().getDimension(R.dimen.gift_image_anim_height);
        gift_image_anim_width = (int) context.getResources().getDimension(R.dimen.gift_image_anim_width);
    }

    private int totalRefreshCount = 0;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(context).inflate(R.layout.list_item_gift, parent, false);
        return new GiftViewHolder(root);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        GiftViewHolder holder = (GiftViewHolder) viewHolder;
        GiftInfo giftInfo = giftInfoList.get(position);
        holder.giftName.setText(giftInfo.getGiftName());
        if (giftInfo.getIsActivity() == 1) {
            holder.giftGold.setText("活动掉落");
        } else if (giftInfo.getIsSale() == 0) {
            holder.giftGold.setText("暂不出售");
        } else if (giftInfo.getIsPea() == 1) {
            holder.giftGold.setText(giftInfo.getGoldPrice() + "甜豆");
        } else {
            holder.giftGold.setText(giftInfo.getGoldPrice() + "金币");
        }
        if (giftInfo.getUserGiftPurseNum() > 0) {
            holder.freeGiftCount.setVisibility(View.VISIBLE);
            holder.freeGiftCount.setText(context.getResources().getString(R.string.gift_number, giftInfo.getUserGiftPurseNum()));
        } else {
            holder.freeGiftCount.setVisibility(View.GONE);
        }

        holder.bind(position);
        String giftUrl = ImageLoadUtils.toThumbnailUrl(130, 130, giftInfo.getGiftUrl());
        ImageLoadUtils.loadImage(BasicConfig.INSTANCE.getAppContext(), giftUrl, holder.giftImage);
        if (giftInfo.isExclusive()) {
            //如果是专属礼物
            holder.imvNobleIcon.setVisibility(View.VISIBLE);
            holder.imvNobleIcon.setImageResource(R.drawable.ic_gift_noble_exclusive);
        } else {
            if (giftInfo.getNobleId() > 0 && !TextUtils.isEmpty(giftInfo.getNobleIcon())) {
                holder.imvNobleIcon.setVisibility(View.VISIBLE);
                ImageLoadUtils.loadImage(BasicConfig.INSTANCE.getAppContext(), giftInfo.getNobleIcon(), holder.imvNobleIcon);
            } else {
                holder.imvNobleIcon.setVisibility(View.GONE);
            }
        }

        totalRefreshCount += 1;
        LogUtils.d(TAG, "onBindViewHolder-totalRefreshCount:" + totalRefreshCount + " giftUrl:" + giftUrl);
        LogUtils.d(TAG, "onBindViewHolder-position:" + position + " index:" + index + " lastSelectedIndex:" + lastSelectedIndex);
        LogUtils.d(TAG, "onBindViewHolder-lastScaleBigAnimIndex:" + lastScaleBigAnimIndex + " lastScaleSmallAnimIndex:" + lastScaleSmallAnimIndex);
        if (position == index) {
            holder.giftImage.setScaleX(1.2f);
            holder.giftImage.setScaleY(1.2f);
        } else if (lastSelectedIndex == position) {
            holder.giftImage.setScaleX(1.0f);
            holder.giftImage.setScaleY(1.0f);
        } else {
            holder.giftImage.setScaleX(1.0f);
            holder.giftImage.setScaleY(1.0f);
        }
        LogUtils.d(TAG, "onBindViewHolder-position:" + position + "---------------------------------");
        holder.rl_giftItem.setBackgroundDrawable(position == index ? context.getResources().getDrawable(R.drawable.bg_gift_dialog_selected) : null);
        holder.itemView.setOnClickListener(position == index ? null : new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mOnItemClickListener) {
                    mOnItemClickListener.onItemClick(holder.giftImage, position);
                }
            }
        });
        //如果四个都显示了,那把其中一个图标放到下面一列
        if (giftInfo.isHasEffect() && giftInfo.isHasTimeLimit() && giftInfo.isHasLatest() && holder.imvNobleIcon.getVisibility() == View.VISIBLE) {
            holder.giftEffects.setVisibility(View.GONE);
            holder.giftLimitTime.setVisibility(View.GONE);
            holder.giftNew.setVisibility(View.GONE);

            holder.giftEffects1.setVisibility(View.VISIBLE);
            holder.giftLimitTime1.setVisibility(View.VISIBLE);
            holder.giftNew1.setVisibility(View.VISIBLE);
        } else {
            holder.giftEffects.setVisibility(giftInfo.isHasEffect() ? View.VISIBLE : View.GONE);
            holder.giftLimitTime.setVisibility(giftInfo.isHasTimeLimit() ? View.VISIBLE : View.GONE);
            holder.giftNew.setVisibility(giftInfo.isHasLatest() ? View.VISIBLE : View.GONE);

            holder.giftEffects1.setVisibility(View.GONE);
            holder.giftLimitTime1.setVisibility(View.GONE);
            holder.giftNew1.setVisibility(View.GONE);
        }
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return giftInfoList == null ? 0 : giftInfoList.size();
    }

    class GiftViewHolder extends RecyclerView.ViewHolder {
        private ImageView giftImage;
        //        private ImageView gift_anim_image;
        private TextView giftName;
        private TextView giftGold;
        private ImageView giftEffects;
        private ImageView giftNew;
        private ImageView giftLimitTime;
        private TextView freeGiftCount;
        private View rl_giftItem;
        private ImageView imvNobleIcon;

        private ImageView giftEffects1;
        private ImageView giftNew1;
        private ImageView giftLimitTime1;

        GiftViewHolder(View itemView) {
            super(itemView);
            giftImage = (ImageView) itemView.findViewById(R.id.gift_image);
//            gift_anim_image = (ImageView) itemView.findViewById(R.id.gift_anim_image);
            rl_giftItem = itemView.findViewById(R.id.rl_giftItem);
            giftGold = (TextView) itemView.findViewById(R.id.gift_gold);
            giftName = (TextView) itemView.findViewById(R.id.gift_name);
            giftEffects = (ImageView) itemView.findViewById(R.id.icon_gift_effect);
            giftNew = (ImageView) itemView.findViewById(R.id.icon_gift_new);
            giftLimitTime = (ImageView) itemView.findViewById(R.id.icon_gift_limit_time);
            freeGiftCount = (TextView) itemView.findViewById(R.id.tv_free_gift_count);
            imvNobleIcon = (ImageView) itemView.findViewById(R.id.imvNobleIcon);
            giftEffects1 = (ImageView) itemView.findViewById(R.id.icon_gift_effect1);
            giftNew1 = (ImageView) itemView.findViewById(R.id.icon_gift_new1);
            giftLimitTime1 = (ImageView) itemView.findViewById(R.id.icon_gift_limit_time1);
        }

        int position;

        public void bind(int position) {
            this.position = position;
        }
    }

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}
