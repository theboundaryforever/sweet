package com.yuhuankj.tmxq.ui.webview;

import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import com.tongdaxing.erban.libcommon.utils.LogUtils;

/**
 * <p> html js 与webview 交互接口</p>
 * Created by ${user} on 2017/11/6.
 */
public class SingleAudioRoomLiveInfoJSInterface extends JSInterface {

    public SingleAudioRoomLiveInfoJSInterface(WebView webView, CommonWebViewActivity activity) {
        super(webView, activity);
        TAG = SingleAudioRoomLiveInfoJSInterface.class.getSimpleName();
    }

    /*----------------------------开播统计------------------------------*/

    /**
     * 停播操作，客户端需要调用停播接口，接口调用成功后关闭当前界面
     */
    @JavascriptInterface
    public void stopPlayRoom() {
        LogUtils.d(TAG, "stopPlayRoom");
        if (null != mActivity && mActivity instanceof SingleAudioRoomLiveInfoWebViewActivity) {
            SingleAudioRoomLiveInfoWebViewActivity activity = (SingleAudioRoomLiveInfoWebViewActivity) mActivity;
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    activity.stopPlayRoom();
                }
            });
        }
    }
}
