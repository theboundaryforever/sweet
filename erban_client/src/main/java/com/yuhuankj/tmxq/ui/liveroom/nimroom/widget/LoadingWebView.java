package com.yuhuankj.tmxq.ui.liveroom.nimroom.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.util.AttributeSet;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * @author liaoxy
 * @Description:自定义webview
 * @date 2016/8/13 13:58
 */
public class LoadingWebView extends WebView {
    private Context context;
    public ScrollInterface mScrollInterface;// 监听滚动的接口
    private LoadingWebView webView;
    private String cachePath = "";
    private boolean isLoadFaild = false;

    /**
     * @param context
     * @param attrs
     * @param defStyle
     */
    public LoadingWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initContext(context);
    }

    /**
     * @param context
     * @param attrs
     */
    public LoadingWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initContext(context);
    }

    /**
     * @param context
     */
    public LoadingWebView(Context context) {
        super(context);
        initContext(context);
    }

    /**
     * @param context
     * @return void
     * @Title: initContext
     * @Description: 初始化context
     */
    @SuppressLint("NewApi")
    private void initContext(Context context) {
        this.context = context;
        webView = this;
        requestFocus();
        setInitialScale(39);
        WebSettings settings = getSettings();
//      //关于android端，webview自适应html中图片的问题。网上比较流行的有三中方法。
//      //一、通过设置LayoutAlgorithm 来自适应屏幕
//
//        settings.setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
//        //二、通过设置webview显示窗口和加载模式来自适应屏幕
//        settings.setUseWideViewPort(true);
//        settings.setLoadWithOverviewMode(true);
//        getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        settings.setJavaScriptEnabled(true);
        settings.setAllowFileAccess(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setDomStorageEnabled(true);
        settings.setDatabaseEnabled(true);
        // 只有17及以上才可以
        if (android.os.Build.VERSION.SDK_INT >= 17) {
            settings.setMediaPlaybackRequiresUserGesture(false);
        }
        settings.setAppCachePath(cachePath);
        settings.setAppCacheEnabled(true);
        //settings.setDefaultFontSize(SizeUtils.sp2px(15));
        this.setWebViewClient(webClient);
    }

    public void setCachePath(String cachePath) {
        this.cachePath = cachePath;
    }

    private boolean isStarted = false;

    public void setLoadFinishedListener(LoadFinishedListener loadFinishedListener) {
        this.loadFinishedListener = loadFinishedListener;
    }

    private LoadFinishedListener loadFinishedListener;

    public interface LoadFinishedListener {
        void onStart();

        void onFinished(boolean isLoadFaid);
    }

    @Override
    public void computeScroll() {
        super.computeScroll();
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        try {
            mScrollInterface.onSChanged(l, t, oldl, oldt);
        } catch (Exception e) {
        }

    }

    public void setOnScroolChangeListener(ScrollInterface scrollInterface) {
        this.mScrollInterface = scrollInterface;
    }

    public interface ScrollInterface {
        void onSChanged(int l, int t, int oldl, int oldt);
    }

    /**
     * @return void
     * @Title: destroyWebView
     * @Description: 回收webview
     */
    public void destroyWebView() {
        clearCache(true);
        clearHistory();
        removeAllViews();
        destroy();
    }

    /**
     * @return void
     * @Title: addProgressBar
     * @Description: 添加进度条
     */
    public void addProgressBar() {

        setWebChromeClient(new WebChromeClient() {

            @Override
            public boolean onJsConfirm(WebView view, String url, String message, final android.webkit.JsResult result) {
                return true;
            }

            @Override
            public boolean onJsAlert(WebView view, String url, String message, final android.webkit.JsResult result) {
                return false;
            }

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (newProgress == 100) {
                    if (loadFinishedListener != null && isStarted) {
                        loadFinishedListener.onFinished(isLoadFaild);
                        isStarted = false;
                    }
                } else {
                    if (loadFinishedListener != null && !isStarted) {
                        loadFinishedListener.onStart();
                        isStarted = true;
                        isLoadFaild = false;
                    }
                }
                super.onProgressChanged(view, newProgress);
            }
        });
    }

    // webview客户端对象
    private WebViewClient webClient = new WebViewClient() {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (loadFinishedListener != null && isStarted) {
                loadFinishedListener.onFinished(isLoadFaild);
                isStarted = false;
            }
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            if (loadFinishedListener != null && !isStarted) {
                loadFinishedListener.onStart();
                isStarted = true;
            }
            isLoadFaild = false;
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            isLoadFaild = true;
        }

        @Override
        public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
            super.onReceivedHttpError(view, request, errorResponse);
            isLoadFaild = true;
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            super.onReceivedSslError(view, handler, error);
            isLoadFaild = true;
        }

        //6.0以下执行
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            isLoadFaild = true;
        }

//        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//            // 返回值是true的时候控制去WebView打开，为false调用系统浏览器或第三方浏览器
//            view.loadUrl(url);
//            return true;
//        }

        public boolean shouldOverrideUrlLoading(WebView webView, String url) {
            return super.shouldOverrideUrlLoading(webView, url);
        }

    };
}
