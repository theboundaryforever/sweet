package com.yuhuankj.tmxq.ui.home.presenter;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.Constants;
import com.yuhuankj.tmxq.ui.home.game.GameHomeModel;
import com.yuhuankj.tmxq.ui.home.model.NewChannelHomeModel;
import com.yuhuankj.tmxq.ui.home.model.NewChannelHomeResponse;
import com.yuhuankj.tmxq.ui.home.view.INewChannelHomeView;
import com.yuhuankj.tmxq.ui.message.UnreadMsgResult;

/**
 * @author weihaitao
 * @date 2019/5/21
 */
public class NewChannelHomePresenter extends AbstractMvpPresenter<INewChannelHomeView> {

    private final String TAG = NewChannelHomePresenter.class.getSimpleName();

    private NewChannelHomeModel newChannelHomeModel;

    public NewChannelHomePresenter() {
        newChannelHomeModel = new NewChannelHomeModel();
    }

    public void getUnreadMsgCount(OkHttpManager.MyCallBack<ServiceResult<UnreadMsgResult>> myCallBack) {
        new GameHomeModel().getUnreadMsgCount(myCallBack);
    }

    public void getOpscHomeData(int pageNum, int pageSize) {
        LogUtils.d(TAG, "getOpscHomeData-pageNum:" + pageNum + " pageSize:" + pageSize);
        newChannelHomeModel.getOpscHomeData(pageNum, pageSize, new HttpRequestCallBack<NewChannelHomeResponse>() {
            @Override
            public void onSuccess(String message, NewChannelHomeResponse response) {
                LogUtils.d(TAG,"onSuccess- message:"+message);
                if (null == getMvpView()) {
                    LogUtils.d(TAG,"onSuccess- mvpView is null");
                    return;
                }
                if (null == response) {
                    LogUtils.d(TAG,"onSuccess- response is null");
                    getMvpView().showNewRoomList(false, null);
                    return;
                }
                LogUtils.d(TAG,"onSuccess- pageNum:"+pageNum);
                //这里需要根据pageNum是否为1来区分是否下拉刷新的状态
                if (pageNum == Constants.PAGE_START) {
                    //下拉刷新，则直接展示数据
                    getMvpView().refreshOpscHomeData(response);
                } else {
                    boolean canLoadMore = null != response.getListRoom() && response.getListRoom().size() == pageSize;
                    getMvpView().showNewRoomList(canLoadMore, response.getListRoom());
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                LogUtils.d(TAG,"onFailure-code:"+code+" msg:"+msg);
                if (null == getMvpView()) {
                    LogUtils.d(TAG,"onFailure- mvpView is null");
                    return;
                }

                //这里需要根据pageNum是否为1来区分是否下拉刷新的状态
                if (pageNum == Constants.PAGE_SIZE) {
                    getMvpView().showOpscHomeDataErr(msg);
                } else {
                    getMvpView().showNewRoomList(false, null);
                }
            }
        });
    }

}
