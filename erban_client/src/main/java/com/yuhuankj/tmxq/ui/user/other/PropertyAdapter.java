package com.yuhuankj.tmxq.ui.user.other;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.user.bean.GiftWallInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

/**
 * Created by chenran on 2017/10/17.
 */

public class PropertyAdapter extends BasePropertyAdapter {
    private List<Json> giftWallInfoList;
    private Context context;

    private String urlKeyName;
    private String propertyKeyName;
    private String countKeyName;

    public PropertyAdapter(Context context, String urlKeyName, String propertyKeyName, String countKeyName) {
        this.context = context;
        this.urlKeyName = urlKeyName;
        this.propertyKeyName = propertyKeyName;
        this.countKeyName = countKeyName;
    }


    @Override
    public BasePropertyAdapter.GiftWallHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.list_item_gift_wall_info, parent, false);
        return new BasePropertyAdapter.GiftWallHolder(item);
    }

    @Override
    public void onBindViewHolder(BasePropertyAdapter.GiftWallHolder holder, int position) {
        Json json = giftWallInfoList.get(position);
        holder.giftName.setText(json.str(propertyKeyName));
        holder.giftNum.setText("");
        ImageLoadUtils.loadImage(context, json.str(urlKeyName), holder.giftPic);
    }

    @Override
    public int getItemCount() {
        if (giftWallInfoList == null)
            return 0;
        else {
            return giftWallInfoList.size();
        }
    }


    @Override
    public void setGiftWallInfoList(List<GiftWallInfo> giftWallInfoList) {

    }

    @Override
    public void setJsonData(List<Json> data) {
        giftWallInfoList = data;
        notifyDataSetChanged();
    }
}
