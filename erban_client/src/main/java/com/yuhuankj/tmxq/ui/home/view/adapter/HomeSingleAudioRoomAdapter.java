package com.yuhuankj.tmxq.ui.home.view.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.juxiao.library_utils.DisplayUtils;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.SingleAudioRoomEnitity;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

/**
 * oppo市场，首页，单人音频直播间列表，adapter
 *
 * @author weihaitao
 * @date 2019年5月21日 17:18:56
 */
public class HomeSingleAudioRoomAdapter extends BaseQuickAdapter<SingleAudioRoomEnitity, BaseViewHolder> {

    private int imvIconWidthAndHeight = 0;
    private int labelHeight = 36;

    public HomeSingleAudioRoomAdapter(Context context, List<SingleAudioRoomEnitity> data) {
        super(R.layout.item_single_room_oppo_home, data);
        this.labelHeight = DisplayUtils.dip2px(context, 18);
        imvIconWidthAndHeight = (DisplayUtils.getScreenWidth(context) - DisplayUtils.dip2px(context, 48)) / 2;
    }

    @Override
    protected void convert(BaseViewHolder helper, SingleAudioRoomEnitity item) {
        ImageView ivRoomOwnerAvatar = helper.getView(R.id.ivRoomOwnerAvatar);
        TextView tvRoomHot = helper.getView(R.id.tvRoomHot);
        ImageView ivLiving = helper.getView(R.id.ivLiving);
        TextView tvRoomTitle = helper.getView(R.id.tvRoomTitle);
        TextView tvOwnerNick = helper.getView(R.id.tvOwnerNick);

        //房主头像
        RelativeLayout.LayoutParams lp1 = (RelativeLayout.LayoutParams) ivRoomOwnerAvatar.getLayoutParams();
        lp1.width = imvIconWidthAndHeight;
        lp1.height = imvIconWidthAndHeight;
        ivRoomOwnerAvatar.setLayoutParams(lp1);

        if (GlideContextCheckUtil.checkContextUsable(mContext)) {
            GlideApp.with(mContext).load(ImageLoadUtils.toThumbnailUrl(imvIconWidthAndHeight,
                    imvIconWidthAndHeight, item.getAvatar()))
                    .dontAnimate()
                    .error(R.drawable.bg_default_cover_round_placehold_12)
                    .transforms(new CenterCrop(), new RoundedCorners(DisplayUtility.dp2px(mContext, 12)))
                    .into(ivRoomOwnerAvatar);
        }

        tvRoomHot.setText(String.valueOf(item.getHotScore()));
        tvRoomTitle.setText(item.getTitle());
        tvOwnerNick.setText(item.getNick());
        GlideApp.with(mContext).asGif().load(R.drawable.anim_oppo_single_room_living)
                .diskCacheStrategy(DiskCacheStrategy.ALL).into(ivLiving);

        helper.addOnClickListener(R.id.rlRoomInfo);

        ImageView imvLabel = helper.getView(R.id.imvLabel);
        if (!TextUtils.isEmpty(item.getRoomLabel())) {
            imvLabel.setVisibility(View.VISIBLE);
            ImageLoadUtils.loadImage(mContext, item.getRoomLabel(), imvLabel);
        } else {
            imvLabel.setVisibility(View.GONE);
        }
        ImageView imvTagLabel = helper.getView(R.id.imvTagLabel);
        if (!TextUtils.isEmpty(item.getTagPict())) {
            imvTagLabel.setVisibility(View.VISIBLE);
            ImageLoadUtils.loadImageWidthLimitView(mContext, item.getTagPict(), imvTagLabel, labelHeight);
        } else {
            imvTagLabel.setVisibility(View.GONE);
        }
    }
}