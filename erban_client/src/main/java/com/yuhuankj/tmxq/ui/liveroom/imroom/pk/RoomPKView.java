package com.yuhuankj.tmxq.ui.liveroom.imroom.pk;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.juxiao.library_utils.DisplayUtils;
import com.tongdaxing.erban.libcommon.base.AbstractMvpRelativeLayout;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.JavaUtil;
import com.tongdaxing.erban.libcommon.utils.NetworkUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.SortList;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.PkCustomAttachment;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMessage;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.pk.bean.PkVoteInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.adapter.PkUserFloatAdapter;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.ArrayList;
import java.util.List;

import static com.tongdaxing.xchat_core.pk.bean.PkVoteInfo.PkUser;

/**
 * 文件描述：全新IM的PK 互动view存在全屏和最小化两种状态兼容房间消息和房间信息
 *
 * @auther：zwk
 * @data：2019/7/8
 */
@CreatePresenter(RoomPKPresenter.class)
public class RoomPKView extends AbstractMvpRelativeLayout<IRoomPKView, RoomPKPresenter> implements IRoomPKView, View.OnClickListener, View.OnTouchListener {

    private final String TAG = RoomPKView.class.getSimpleName();

    private Context mContext;
    private RelativeLayout rlMinimize;//最小化布局
    private RelativeLayout rlFull;//大屏布局
    private ImageView ivMinimize;//最小化按钮
    private TextView tvMinCount;//最小化时的倒计时
    private TextView tvPkTimer;//最大化时的倒计时
    private PkVoteInfo pkVoteInfo;
    public boolean isFull = false;
    private boolean isShowing = false;

    private TextView tvPkName;
    private RecyclerView rcvUser;
    private TextView tvPkVote;
    private RelativeLayout rlEmply;
    private TextView tvPkType;
    //最小化时控件
    private ImageView imvPkUser1, imvPkUser2;
    private TextView tvPkCountTip;
    private PkUserFloatAdapter pkUserAdapter;
    private List<PkUser> datas;


    public RoomPKView(Context context) {
        this(context, null);
    }

    public RoomPKView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoomPKView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }

    @Override
    protected int getViewLayoutId() {
        return R.layout.layout_home_party_pk;
    }

    @Override
    public void initialView(Context context) {
        this.mContext = context;
        initView(context);
        initListener();
    }

    @Override
    protected void initListener() {
        rlMinimize.setOnClickListener(this);
        ivMinimize.setOnClickListener(this);
        rlMinimize.setOnTouchListener(this);
        tvPkVote.setOnClickListener(this);
        rlFull.setOnClickListener(this);
        rlEmply.setOnClickListener(this);
    }

    private void initView(Context context) {
        mWidthPixels = DisplayUtils.getScreenWidth(context);
        mHeightPixels = DisplayUtils.getScreenHeight(context);
        rlFull = findViewById(R.id.rl_pk_full_screen);
        tvPkTimer = findViewById(R.id.tvPkTimer);
        LayoutParams rl = (LayoutParams) rlFull.getLayoutParams();
        rl.width = 323 * mWidthPixels / 375;
//        rl.height = 204 * ScreenUtil.getScreenHeight(context)/667;
        rlFull.setLayoutParams(rl);
        rlMinimize = findViewById(R.id.rl_pk_minimize);
        ivMinimize = findViewById(R.id.iv_minimize);
        tvMinCount = findViewById(R.id.tv_pk_min_countdown);
        tvPkType = findViewById(R.id.tvPkType);
        tvPkName = findViewById(R.id.tvPkName);
        rcvUser = findViewById(R.id.rcvUser);
        tvPkVote = findViewById(R.id.tvPkVote);
        rlEmply = findViewById(R.id.rlEmply);
        //最小化时控件
        imvPkUser1 = findViewById(R.id.imvPkUser1);
        imvPkUser2 = findViewById(R.id.imvPkUser2);
        tvPkCountTip = findViewById(R.id.tvPkCountTip);

        GridLayoutManager layoutmanager = new GridLayoutManager(getContext(), 4);
        rcvUser.setLayoutManager(layoutmanager);
        datas = new ArrayList<>();
        pkUserAdapter = new PkUserFloatAdapter();
        pkUserAdapter.bindToRecyclerView(rcvUser);
        pkUserAdapter.setOnItemClickListener((adapter, view, position) -> {
            try {
                if (datas.get(position) == null || !tvPkVote.isEnabled()) {
                    return;
                }
                pkUserAdapter.setCurSelPos(position);
                pkUserAdapter.setCurSelUid(JavaUtil.str2long(datas.get(position).getUid()));
                pkUserAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        if (isFull) {
            if (rlMinimize.getVisibility() == View.VISIBLE) {
                rlMinimize.setVisibility(View.GONE);
            }
            if (rlFull.getVisibility() == View.GONE) {
                rlFull.setVisibility(View.VISIBLE);
            }
            disableClickOther(false);
        } else {
            disableClickOther(true);
        }
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            try {
                //倒计时剩余3和7秒分别请求
                if (countDuration == 3 || countDuration == 7) {
                    initData(true);
                }
                if (duration <= 0) {
                    handler.removeCallbacks(runnable);
                    resetState();
                    showPKWin();
                    return;
                }
                duration--;
                countDuration--;
                tvPkTimer.setText(duration + "S");
                tvMinCount.setText(duration + "S");
                handler.postDelayed(this, 1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void restorePkVoteStatus() {
        if (RoomDataManager.get().hasPkVoted()) {
            tvPkVote.setText("已投票");
            tvPkVote.setEnabled(false);
            pkUserAdapter.setCurSelPos(-1);
            pkUserAdapter.setCurSelUid(0L);
            pkUserAdapter.notifyDataSetChanged();
        }
    }


    @Override
    protected void initViewState() {

    }

    private void disableClickOther(boolean isEnable) {
        if (isEnable) {
            rlEmply.setVisibility(GONE);
        } else {
            rlEmply.setVisibility(VISIBLE);
        }
    }

    private OnPKVoteSendGiftClickListener onPKVoteSendGiftClickListener;

    //倒计时
    Handler handler = new Handler();
    private int duration = 0;//真实的时间 -- 用于显示
    private int countDuration = 0;//用于倒计时 -- 如果延迟10秒依然出现问题则隐藏显示

    /**
     * 通过接口获取最新信息
     *
     * @param isUpdateVote true 投票操作  false初始化获取pk数据
     */
    public void initData(boolean isUpdateVote) {
        if (!NetworkUtils.isNetworkAvailable(mContext)) {
            if (duration == 0) {
                resetState();
                restorePkVoteStatus();
            }
            return;
        }
        getMvpPresenter().getRoomPkInfo((RoomDataManager.get().getCurrentRoomInfo() == null ? 0 : RoomDataManager.get().getCurrentRoomInfo().getRoomId()) + "", isUpdateVote);
    }


    @Override
    public void initPKViewFromVoteInfo(PkVoteInfo pkVoteInfo) {
        if (pkVoteInfo != null) {
            if (pkVoteInfo.getDuration() > 0) {
                setPkInfo(pkVoteInfo);
                countDown(pkVoteInfo.getDuration());
                restorePkVoteStatus();
            } else {
                resetState();
                restorePkVoteStatus();
            }
        } else {
            resetState();
            restorePkVoteStatus();
        }
    }

    @Override
    public void updatePKViewFromVoteInfo(PkVoteInfo pkVoteInfo) {
        if (pkVoteInfo == null) {
            return;
        }
        if (pkVoteInfo.getDuration() > 0) {
            countDown(pkVoteInfo.getDuration());
            setPkInfo(pkVoteInfo);
            restorePkVoteStatus();
        } else {
            //屏蔽因为延迟导致的重复执行显示隐藏问题
            if (!isShowing && pkVoteInfo != null && (rlFull.getVisibility() == View.VISIBLE
                    || rlMinimize.getVisibility() == View.VISIBLE)) {
                dealWithPKEnd(pkVoteInfo);
                RoomDataManager.get().setHasPkVoted(false);
            }
        }
    }

    @Override
    public void getPKVoteInfoFail(String error) {
        if (duration == 0) {
            resetState();
            restorePkVoteStatus();
        }
    }

    /**
     * 倒计时 + 延时10秒
     *
     * @param count
     */
    private void countDown(int count) {
        if (count <= 0) {
            handlerRelease();
            return;
        }
        countDuration = count + 10;
        duration = count;
        tvPkTimer.setText(duration + "S");
        tvMinCount.setText(duration + "S");
        if (handler != null) {
            try {
                handler.removeCallbacks(runnable);
                handler.postDelayed(runnable, 1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 释放handler
     */
    private void handlerRelease() {
        if (handler != null && runnable != null) {
            try {
                handler.removeCallbacks(runnable);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void pkVoteSuc(PkVoteInfo pkVoteInfo) {
        tvPkVote.setText("已投票");
        tvPkVote.setEnabled(false);
        pkUserAdapter.setCurSelPos(-1);
        pkUserAdapter.setCurSelUid(0L);
        pkUserAdapter.notifyDataSetChanged();
        RoomDataManager.get().setHasPkVoted(true);
    }

    @Override
    public void pkVoteFail(String error) {
        SingleToastUtil.showToast(error);
    }

    /**
     * 控制不同消息显示
     *
     * @param chatRoomMessage
     */
    private void dealWithEvent(IMRoomMessage chatRoomMessage) {
        if (chatRoomMessage.getAttachment() instanceof PkCustomAttachment) {
            PkCustomAttachment pk = (PkCustomAttachment) chatRoomMessage.getAttachment();
            if (pk == null) {
                return;
            }
            if (pk.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_START_NEW) {
                //操作者是自己
                if (pk.getPkVoteInfo() != null){
                    if(RoomDataManager.get().isUserSelf(pk.getPkVoteInfo().getUid())) {
                        isFull = true;
                    }
                    countDown(pk.getPkVoteInfo().getDuration());
                }
                setPkInfo(pk.getPkVoteInfo());
            } else if (pk.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_VOTE_NEW) {
                setPkInfo(pk.getPkVoteInfo());
            } else if (pk.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_END_NEW) {
                //屏蔽因为延迟导致的重复执行显示隐藏问题
                if (!isShowing && pkVoteInfo != null) {
                    dealWithPKEnd(pk.getPkVoteInfo());
                    RoomDataManager.get().setHasPkVoted(false);
                }
            } else if (pk.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_CANCEL_NEW) {
                resetState();
                RoomDataManager.get().setHasPkVoted(false);
            }
        }
    }

    /**
     * 处理PK结果
     *
     * @param info
     */
    private void dealWithPKEnd(PkVoteInfo info) {
        if (info != null) {
            handlerRelease();
            isFull = true;
            setPkInfo(info);
            tvPkVote.setVisibility(GONE);
            showPKWin();
        } else {
            if (duration == 0) {//数据异常倒计时结束隐藏
                resetState();
            }
        }
    }

    /**
     * 显示大窗口并且执行消失动画
     */
    private void showPKWin() {
        isShowing = true;
        rlFull.setVisibility(VISIBLE);
        disableClickOther(false);
        rlMinimize.setVisibility(GONE);
        tvPkTimer.setVisibility(GONE);
        tvPkVote.setVisibility(GONE);
        tvPkName.setText("投票结果");
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ScaleAnimation disappear = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, rlFull.getWidth() / 2, rlFull.getHeight() / 2);
                disappear.setDuration(500);
                disappear.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        resetState();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
                rlFull.startAnimation(disappear);
//                resetState();
                handler.removeCallbacks(this);
            }
        }, 5000);
    }

    /***
     * 重置view的初始状态
     */
    private void resetState() {
        //释放定时器
        handlerRelease();
        //隐藏布局
        rlFull.setVisibility(View.GONE);
        rlMinimize.setVisibility(View.GONE);
        disableClickOther(true);
        //重置默认状态
        pkVoteInfo = null;
        isFull = false;
        duration = 0;
        countDuration = 0;
        isShowing = false;
        tvPkVote.setVisibility(VISIBLE);
        tvPkTimer.setVisibility(VISIBLE);
        tvPkVote.setEnabled(true);
        tvPkVote.setText("投票");
        pkUserAdapter.setCurSelPos(-1);
        pkUserAdapter.setCurSelUid(0L);
    }

    /**
     * seekbar进度显示
     *
     * @param voteCount
     * @param pkVote
     * @return
     */
    private int getProgress(int voteCount, int pkVote) {
        if (voteCount == pkVote) {
            return 50;
        }
        return voteCount * 100 / (voteCount + pkVote == 0 ? 1 : voteCount + pkVote);
    }

    public void setPkInfo(PkVoteInfo info) {
        if (info == null) {
            return;
        }
        pkVoteInfo = info;
        datas.clear();
        //针对旧版本，2人PK的显示逻辑，getVoteCount左边票数，getPkVoteCount右边票数
        if ((info.getPkList() == null || info.getPkList().size() == 0) && !TextUtils.isEmpty(info.getPkNick())) {
            PkUser pkUser = new PkUser();
            pkUser.setUid(info.getUid() + "");
            pkUser.setAvatar(info.getAvatar());
            pkUser.setNick(info.getNick());
            pkUser.setVoteCount(info.getVoteCount());

            PkUser pkUser1 = new PkUser();
            pkUser1.setUid(info.getPkUid() + "");
            pkUser1.setAvatar(info.getPkAvatar());
            pkUser1.setNick(info.getPkNick());
            pkUser1.setVoteCount(info.getPkVoteCount());

            //获取麦位位置
            pkUser.setMicPosition(RoomDataManager.get().getMicPosition(pkUser.getUid()));
            pkUser1.setMicPosition(RoomDataManager.get().getMicPosition(pkUser1.getUid()));
            datas.add(pkUser);
            datas.add(pkUser1);
        } else {
            for (PkUser pkUser : info.getPkList()) {
                //获取麦位位置
                pkUser.setMicPosition(RoomDataManager.get().getMicPosition(pkUser.getUid()));
                datas.add(pkUser);
            }
        }

        //根据麦位麦序-->票数进行排序
        SortList<PkUser> sortList = new SortList<PkUser>();
        //正序排序--先按麦位顺序排序
        sortList.Sort(datas, "getMicPosition", "asc");
        //倒序排序--再按[2礼物价值/1投票人数]票数排序
        sortList.Sort(datas, "getVoteCount", "desc");

        tvPkName.setText(info.getTitle());
        //最小化时控件
        ImageLoadUtils.loadCircleImage(mContext, datas.get(0).getAvatar(), imvPkUser1, R.drawable.default_user_head);
        ImageLoadUtils.loadCircleImage(mContext, datas.get(1).getAvatar(), imvPkUser2, R.drawable.default_user_head);

        int index = 0;
        String pkCountStr = datas.size() + "";
        String pkCount = pkCountStr + "人正在PK";
        if (datas.size() > 2) {
            index = 1;
            pkCount = "等" + pkCountStr + "人正在PK";
        }
        SpannableStringBuilder ssb = new SpannableStringBuilder(pkCount);
        ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#10CFA6")), index, index + pkCountStr.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        tvPkCountTip.setText(ssb);

        pkUserAdapter.setPkType(pkVoteInfo.getPkType());
        pkUserAdapter.setNewData(datas);
        //1--投票人数，2--礼物价值
        tvPkType.setText(mContext.getResources().getString(pkVoteInfo.getPkType() == 2
                ? R.string.room_pk_type_tips_gift_price : R.string.room_pk_type_tips_person_num));
        if (isFull) {
            rlFull.setVisibility(View.VISIBLE);
            rlMinimize.setVisibility(View.GONE);
            disableClickOther(false);
        } else {
            rlFull.setVisibility(View.GONE);
            rlMinimize.setVisibility(View.VISIBLE);
            disableClickOther(true);
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        CoreManager.addClient(this);
        compositeDisposable.add(IMRoomMessageManager.get().getIMRoomMessageFlowable()
                .subscribe(messages -> {
                    if (messages.size() > 0) {
                        for (IMRoomMessage msg : messages) {
                            dealWithEvent(msg);
                        }
                    }
                }));
        compositeDisposable.add(IMRoomMessageManager.get().getIMRoomEventObservable()
                .subscribe(roomEvent -> {
                    if (roomEvent == null ||
                            roomEvent.getEvent() != RoomEvent.RECEIVE_MSG) {
                        return;
                    }
                    dealWithEvent(roomEvent.getIMRoomMessage());
                }));
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        CoreManager.removeClient(this);
        handlerRelease();
    }

    public PkVoteInfo getPkVoteInfo() {
        return pkVoteInfo;
    }

    /**
     * 控制view的位置
     */
    private int mWidthPixels;
    private int mHeightPixels;
    long mDownTimeMillis = 0;
    int xDelta = 0;
    int yDelta;

    @Override
    public boolean performClick() {
        return super.performClick();
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        final int x = (int) event.getRawX();
        final int y = (int) event.getRawY();
//        LogUtils.d(TAG, "onTouch: x= " + x + "y=" + y);
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                mDownTimeMillis = System.currentTimeMillis();
                LayoutParams params = (LayoutParams) view
                        .getLayoutParams();
                xDelta = x - params.leftMargin;
                yDelta = y - params.topMargin;
                performClick();
//                LogUtils.d(TAG, "ACTION_DOWN: xDelta= " + xDelta + "yDelta=" + yDelta);
                break;
            case MotionEvent.ACTION_MOVE:
                LayoutParams layoutParams = (LayoutParams) view
                        .getLayoutParams();
                int width = layoutParams.width;
                int height = layoutParams.height;
                int xDistance = x - xDelta;
                int yDistance = y - yDelta;

                int outX = (mWidthPixels - width) - 10;
                if (xDistance > outX) {
                    xDistance = outX;
                }

                int outY = mHeightPixels - height;
                if (yDistance > outY) {
                    yDistance = outY;
                }

                if (yDistance < 100) {
                    yDistance = 100;
                }
                if (xDistance < 10) {
                    xDistance = 10;
                }

                layoutParams.leftMargin = xDistance;
                layoutParams.topMargin = yDistance;
                view.setLayoutParams(layoutParams);
                break;
            case MotionEvent.ACTION_UP:
                if (System.currentTimeMillis() - mDownTimeMillis < 150) {
                    if (pkVoteInfo != null) {
                        isFull = true;
                        if (rlMinimize.getVisibility() == View.VISIBLE) {
                            rlMinimize.setVisibility(View.GONE);
                        }
                        if (rlFull.getVisibility() == View.GONE) {
                            rlFull.setVisibility(View.VISIBLE);
                        }
                        disableClickOther(false);
                    } else {
                        resetState();
                    }
                }
                break;
            default:
                break;
        }
        return true;
    }

    public boolean hasPkState() {
        return pkVoteInfo != null && pkVoteInfo.getVoteId() > 0;
    }

    private long lastClickToShowGiftDialogTime = 0L;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlEmply:
                break;
            case R.id.rl_pk_full_screen:
                //空实现，避免点击事件传递到坑位上，触发相应的业务逻辑
                break;
            case R.id.iv_minimize:
                //由全屏到最小化
                if (pkVoteInfo == null) {
                    resetState();
                    return;
                }
                isFull = false;
                if (rlFull.getVisibility() == View.VISIBLE) {
                    rlFull.setVisibility(View.GONE);
                }
                if (rlMinimize.getVisibility() == View.GONE) {
                    rlMinimize.setVisibility(View.VISIBLE);
                }
                disableClickOther(true);
                break;
            case R.id.tvPkVote:
                try {
                    int curSelPos = pkUserAdapter.getCurSelPos();
                    if (curSelPos != -1) {
                        if (pkVoteInfo.getPkType() == 2) {
                            //礼物pk
                            if (null != onPKVoteSendGiftClickListener) {
                                if (System.currentTimeMillis() - lastClickToShowGiftDialogTime < 1000L) {
                                    return;
                                }
                                lastClickToShowGiftDialogTime = System.currentTimeMillis();
                                onPKVoteSendGiftClickListener.onPkVoteSendGiftClick(JavaUtil.str2long(datas.get(curSelPos).getUid()));
                            }
                        } else {
                            //投票pk
                            getMvpPresenter().pkVote((RoomDataManager.get().getCurrentRoomInfo() == null ?
                                    0 : RoomDataManager.get().getCurrentRoomInfo().getRoomId()) + "", datas.get(curSelPos).getUid() + "");
                        }
                    } else {
                        SingleToastUtil.showToast("请先选择要投票的用户");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
    }

    public void setOnPKVoteSendGiftClickListener(OnPKVoteSendGiftClickListener listener) {
        this.onPKVoteSendGiftClickListener = listener;
    }

    public interface OnPKVoteSendGiftClickListener {
        void onPkVoteSendGiftClick(long uid);
    }

}
