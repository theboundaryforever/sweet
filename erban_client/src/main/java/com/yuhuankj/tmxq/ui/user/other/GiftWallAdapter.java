package com.yuhuankj.tmxq.ui.user.other;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.user.bean.GiftWallInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

/**
 * Created by chenran on 2017/10/17.
 */

public class GiftWallAdapter extends BasePropertyAdapter {
    private List<GiftWallInfo> giftWallInfoList;
    private Context context;

    public GiftWallAdapter(Context context) {
        this.context = context;
    }

    public void setGiftWallInfoList(List<GiftWallInfo> giftWallInfoList) {
        this.giftWallInfoList = giftWallInfoList;
    }

    @Override
    public void setJsonData(List<Json> data) {

    }

    @Override
    public GiftWallHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.list_item_gift_wall_info, parent, false);
        return new GiftWallHolder(item);
    }

    @Override
    public void onBindViewHolder(GiftWallHolder holder, int position) {
        GiftWallInfo giftWallInfo = giftWallInfoList.get(position);
        holder.giftName.setText(giftWallInfo.getGiftName());
        holder.giftNum.setText("X" + giftWallInfo.getReciveCount());
        ImageLoadUtils.loadImage(context, giftWallInfo.getPicUrl(), holder.giftPic);
    }

    @Override
    public int getItemCount() {
        if (giftWallInfoList == null)
            return 0;
        else {
            return giftWallInfoList.size();
        }
    }



}
