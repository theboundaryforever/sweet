package com.yuhuankj.tmxq.ui.find;


import android.view.View;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.libcommon.utils.ButtonUtils;
import com.tongdaxing.xchat_core.find.DiscoverIconListBean;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

public class FindTopAdapter extends BaseQuickAdapter<DiscoverIconListBean, FindTopAdapter.ViewHolder> {
    public FindTopAdapter() {
        super(R.layout.item_find_top);
    }


    @Override
    protected void convert(ViewHolder helper, DiscoverIconListBean item) {
        ImageLoadUtils.loadImage(helper.imageView.getContext(), item.getPic(), helper.imageView, R.drawable.bg_default_cover_round_placehold_5);
        helper.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick(v.getId())) {
                    return;
                }
                if (iItemAction != null) {
                    iItemAction.onItemClick(item);
                }
            }
        });
    }


    public IItemAction iItemAction;

    public interface IItemAction {
        void onItemClick(DiscoverIconListBean bean);
    }


    public class ViewHolder extends BaseViewHolder {
        private View bg;
        private ImageView imageView;

        public ViewHolder(View view) {
            super(view);

            imageView = view.findViewById(R.id.iv_find_top_item);
            bg = view;
        }
    }
}
