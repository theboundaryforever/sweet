package com.yuhuankj.tmxq.ui.liveroom.imroom.room.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

public interface IRoomView extends IMvpBaseView {

    void showEnterRoomLoading();

    void dismisEnterRoomLoading();

    void showRoomFinishedView();

    void showRoomPwdDialog(boolean showFailText);

    void showRoomGuideDialog();

    void dismissDialog();

    void finishActivity();
}
