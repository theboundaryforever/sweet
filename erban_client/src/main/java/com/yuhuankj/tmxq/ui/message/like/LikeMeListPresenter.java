package com.yuhuankj.tmxq.ui.message.like;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;

import java.util.List;


public class LikeMeListPresenter extends AbstractMvpPresenter<LikeMeListView> {

    private final String TAG = LikeMeListPresenter.class.getSimpleName();
    private LikeModel likeModel;

    public LikeMeListPresenter() {
        likeModel = new LikeModel();
    }


    public void getMyLikeList(int pageNum, int pageSize) {
        LogUtils.d(TAG, "getMyLikeList-pageNum:" + pageNum + " pageSize:" + pageSize);

        likeModel.getLikeMsgList(pageNum, pageSize, new OkHttpManager.MyCallBack<ServiceResult<List<LikeEnitity>>>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if(null != getMvpView()){
                    getMvpView().showLikeListEmptyView();
                }
            }

            @Override
            public void onResponse(ServiceResult<List<LikeEnitity>> response) {
                if (null == getMvpView()) {
                    return;
                }
                if (null != response && response.isSuccess() && null != response.getData() && response.getData().size() > 0) {
                    likeModel.setLikeMsgListRead(new OkHttpManager.MyCallBack<ServiceResult<String>>() {
                        @Override
                        public void onError(Exception e) {
                            e.printStackTrace();
                            getMvpView().showLikeList(response.getData());
                        }

                        @Override
                        public void onResponse(ServiceResult<String> serviceResult) {
                            getMvpView().showLikeList(response.getData());
                        }
                    });
                } else {
                    getMvpView().showLikeListEmptyView();
                }
            }
        });
    }
}
