package com.yuhuankj.tmxq.ui.launch.guide;

import com.tongdaxing.erban.libcommon.coremanager.ICoreClient;

/**
 * 新手引导，弹框可见状态发生更改时的界面回调，以通知界面做相应处理，解决冲突、兼容性问题
 *
 * @author 魏海涛
 * @date 2019年2月22日 10:20:24
 */

public interface INoviceGuideCoreClient extends ICoreClient {

    //偶遇-新手引导
    String METHOD_ON_CHANCE_MEETING_SHOW_STATUS_CHANGED = "onChanceMeetingGuideShowStatusChanged";

    void onChanceMeetingGuideShowStatusChanged(boolean isShow);
}
