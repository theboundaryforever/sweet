package com.yuhuankj.tmxq.ui.liveroom.nimroom.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.adapter.RoomSelectBgAdapter;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.bean.ChatSelectBgBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/3/23.
 */

public class RoomSelectBgActivity extends BaseActivity {

    private RecyclerView mRecyclerView;
    private RoomSelectBgAdapter mRoomSelectBgAdapter;
    private List<ChatSelectBgBean> chatSelectBgBeans;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String backPic = getIntent().getStringExtra("backPic");

        setContentView(R.layout.activity_chat_room_select_bg);
        initTitleBar("主题背景");

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_chat_room_select_bg);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        mRoomSelectBgAdapter = new RoomSelectBgAdapter();

        mRoomSelectBgAdapter.setItemAction(new RoomSelectBgAdapter.ItemAction() {

            @Override
            public void itemSelect(int index) {
                Intent intent = new Intent();
                String selectUrl = mRoomSelectBgAdapter.selectUrl;
                if (!TextUtils.isEmpty(selectUrl)) {
                    intent.putExtra("selectUrl", selectUrl);
                }
                String name = mRoomSelectBgAdapter.selectBgName;
                if (!TextUtils.isEmpty(mRoomSelectBgAdapter.selectBgName)) {
                    intent.putExtra("selectBgName", name);
                }
                setResult(2, intent);
            }

            @Override
            public void nobleNotEnoughtBuy() {
                getDialogManager().showOkDialog(getResources().getString(R.string.noble_level_not_enought_buy_roombg), null);
            }
        });
        mRoomSelectBgAdapter.selectUrl = backPic;
        mRecyclerView.setAdapter(mRoomSelectBgAdapter);
        chatSelectBgBeans = new ArrayList<>();
        mRoomSelectBgAdapter.setNewData(chatSelectBgBeans);
        getNetBg();
    }

    private void getNetBg() {
        RoomInfo cRoomInfo = RoomServiceScheduler.getServerRoomInfo();
        if (null == cRoomInfo) {
            return;
        }
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket() + "");
        param.put("roomId", cRoomInfo.getRoomId() + "");
        OkHttpManager.getInstance().postRequest(UriProvider.getRoomBgV2List(), param, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Json json) {
                if (json.num("code") != 200) {
                    return;
                }
                List<Json> data = json.jlist("data");
                for (Json j : data) {
                    chatSelectBgBeans.add(new ChatSelectBgBean(j.num("id"), j.str("picUrl"),
                            j.str("name"), j.num("vipId"), j.str("vipIcon")));
                }
                mRoomSelectBgAdapter.setNewData(chatSelectBgBeans);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
