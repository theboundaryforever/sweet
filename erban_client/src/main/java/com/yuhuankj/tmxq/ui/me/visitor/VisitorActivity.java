package com.yuhuankj.tmxq.ui.me.visitor;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.netease.nim.uikit.common.util.sys.NetworkUtil;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.Constants;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liaoxy
 * @Description:我的访客
 * @date 2019/5/6 10:33
 */
public class VisitorActivity extends BaseActivity {
    private List<VisitorEnitity> datas;
    private LinearLayout llEmply;
    private RecyclerView rcvDatas;
    private BaseQuickAdapter adapter;
    private SwipeRefreshLayout sprContent;
    private int page = Constants.PAGE_START;

    private SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            if (NetworkUtil.isNetAvailable(VisitorActivity.this)) {
                page = Constants.PAGE_START;
                getVisitors();
            } else {
                sprContent.setRefreshing(false);
            }
        }
    };

    private BaseQuickAdapter.RequestLoadMoreListener loadMoreListener = new BaseQuickAdapter.RequestLoadMoreListener() {
        @Override
        public void onLoadMoreRequested() {
            // 解决数据重复的问题
            if (page == Constants.PAGE_START) {
                return;
            }
            if (NetworkUtil.isNetAvailable(VisitorActivity.this)) {
                if (datas.size() >= Constants.PAGE_SIZE) {
                    getVisitors();
                } else {
                    adapter.setEnableLoadMore(false);
                }
            } else {
                adapter.loadMoreEnd(true);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitor);
        initTitleBar("我的访客");
        mTitleBar.setCommonBackgroundColor(Color.WHITE);
        initView();
        initData();
    }

    private void initView() {
        sprContent = (SwipeRefreshLayout) findViewById(R.id.sprContent);
        rcvDatas = (RecyclerView) findViewById(R.id.rcvFriends);
        llEmply = (LinearLayout) findViewById(R.id.llEmply);

        sprContent.setOnRefreshListener(onRefreshListener);
    }

    private void initData() {
        LinearLayoutManager layoutmanager = new RecyclerViewNoBugLinearLayoutManager(this);
        layoutmanager.setOrientation(LinearLayoutManager.VERTICAL);
        rcvDatas.setLayoutManager(layoutmanager);
        datas = new ArrayList<>();

        llEmply.setVisibility(View.GONE);
        adapter = new VisitorAdapter(datas);
        rcvDatas.setAdapter(adapter);
        adapter.setOnLoadMoreListener(loadMoreListener, rcvDatas);
        adapter.setEnableLoadMore(false);
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                try {
                    VisitorEnitity visitor = datas.get(position);
                    UserInfoActivity.start(VisitorActivity.this, visitor.getUid());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        getVisitors();
    }

    private void getVisitors() {
        new VisitorModel().getVisitors(page, Constants.PAGE_SIZE, new OkHttpManager.MyCallBack<ServiceResult<List<VisitorEnitity>>>() {

            @Override
            public void onError(Exception e) {
                sprContent.setRefreshing(false);
                adapter.loadMoreComplete();
                SingleToastUtil.showToast("获取访客列表失败");
            }

            @Override
            public void onResponse(ServiceResult<List<VisitorEnitity>> response) {
                sprContent.setRefreshing(false);
                adapter.loadMoreComplete();
                if (response == null || response.getData() == null) {
                    SingleToastUtil.showToast("获取访客列表失败");
                    return;
                }
                if (!response.isSuccess()) {
                    SingleToastUtil.showToast(response.getErrorMessage());
                    return;
                }
                List<VisitorEnitity> tmps = response.getData();
                if (tmps.size() >= Constants.PAGE_SIZE) {
                    adapter.setEnableLoadMore(true);
                    page++;
                } else {
                    adapter.setEnableLoadMore(false);
                }
                if (page == Constants.PAGE_START) {
                    datas.clear();
                }
                datas.addAll(tmps);
                adapter.notifyDataSetChanged();
                if (datas.size() == 0) {
                    llEmply.setVisibility(View.VISIBLE);
                } else {
                    llEmply.setVisibility(View.GONE);
                }
            }
        });
    }
}
