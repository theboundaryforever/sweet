package com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget;

import android.content.Context;
import android.support.annotation.Nullable;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.im.IMReportBean;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.JavaUtil;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMember;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.activity.RoomManagerListActivity;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.gift.GiftDialog;
import com.yuhuankj.tmxq.ui.me.charge.ChargeActivity;
import com.yuhuankj.tmxq.ui.widget.UserInfoDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author weihaitao
 * @date 2019年4月24日
 */
public class IMButtonItemFactory {

    private static final String TAG = IMButtonItemFactory.class.getSimpleName();


    public static List<ButtonItem> createRoomPublicScreenButtonItems(Context context,
                                                                     IMRoomMember chatRoomMember,
                                                                     boolean showSendGiftBItem) {
        if (RoomDataManager.get().getCurrentRoomInfo() == null || chatRoomMember == null) {
            return null;
        }
        List<ButtonItem> buttonItems = new ArrayList<>();
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        String roomId = String.valueOf(roomInfo.getRoomId());
        String account = chatRoomMember.getAccount();
        return getButtonItems(context, account, buttonItems, roomInfo, roomId, chatRoomMember, showSendGiftBItem);
    }

    @Nullable
    private static List<ButtonItem> getButtonItems(Context context, String account, List<ButtonItem> buttonItems,
                                                   RoomInfo roomInfo, String roomId, IMRoomMember chatRoomMember,
                                                   boolean showSendGiftBItem) {
        boolean isMyself = RoomDataManager.get().isUserSelf(account);
        boolean isTargetGuess = RoomDataManager.get().isGuess(account);
        boolean isTargetRoomAdmin = RoomDataManager.get().isRoomAdmin(account);
        boolean isTargetRoomOwner = RoomDataManager.get().isRoomOwner(account);
        boolean isTargetOnMic = RoomDataManager.get().isOnMic(account);
        boolean showInviteOnMicItem = null != roomInfo && roomInfo.getType() != RoomInfo.ROOMTYPE_SINGLE_AUDIO;
        // 房主点击
        if (RoomDataManager.get().isRoomOwner()) {
            //目标用户为房间创建者，其实也就是用户自己
            if (isTargetRoomOwner) {
                new UserInfoDialog(context, JavaUtil.str2long(account)).show();
                return null;
            } else if (isTargetRoomAdmin || isTargetGuess) {
                if (showSendGiftBItem) {
                    //送礼物
                    buttonItems.add(createSendGiftItem(context, account));
                }
                // 查看资料
                buttonItems.add(createCheckUserInfoDialogItem(context, account));
                // 抱Ta上麦
                if (!isTargetOnMic && showInviteOnMicItem) {
                    buttonItems.add(createInviteOnMicItem(account));
                }
                // 踢出房间
                buttonItems.add(createKickOutRoomItem(context, chatRoomMember, roomId, account));
                // 增加/移除管理员
                buttonItems.add(createMarkManagerListItem(roomId, account, isTargetGuess, chatRoomMember));
                // 加入黑名单
                buttonItems.add(createMarkBlackListItem(context, chatRoomMember, roomId));
            }
        } else if (RoomDataManager.get().isRoomAdmin()) {
            if (isMyself) {
                new UserInfoDialog(context, JavaUtil.str2long(account)).show();
                return null;
            } else if (isTargetRoomAdmin || isTargetRoomOwner) {
                if (isTargetOnMic) {
                    new UserInfoDialog(context, JavaUtil.str2long(account)).show();
                    return null;
                }

                if (isTargetRoomOwner || showSendGiftBItem) {
                    //送礼物
                    buttonItems.add(createSendGiftItem(context, account));
                }

                // 查看资料
                buttonItems.add(createCheckUserInfoDialogItem(context, account));
                // 抱Ta上麦
                if (showInviteOnMicItem) {
                    buttonItems.add(createInviteOnMicItem(account));
                }
            } else if (isTargetGuess) {
                if (showSendGiftBItem) {
                    //送礼物
                    buttonItems.add(createSendGiftItem(context, account));
                }
                // 查看资料
                buttonItems.add(createCheckUserInfoDialogItem(context, account));
                // 抱Ta上麦
                if (!isTargetOnMic && showInviteOnMicItem) {
                    buttonItems.add(createInviteOnMicItem(account));
                }
                // 踢出房间
                buttonItems.add(createKickOutRoomItem(context, chatRoomMember, roomId, account));
                // 加入黑名单
                buttonItems.add(createMarkBlackListItem(context, chatRoomMember, roomId));
            }
        } else if (RoomDataManager.get().isGuess()) {
            if ((CoreManager.getCore(IAuthCore.class).getCurrentUid() + "").equals(account)) {
                new UserInfoDialog(context, JavaUtil.str2long(account)).show();
            } else {
                if (showSendGiftBItem) {
                    showGiftDialog(context, account);
                } else {
                    new UserInfoDialog(context, JavaUtil.str2long(account)).show();
                }
            }
            return null;
        }
        return buttonItems;
    }


    private static void showGiftDialog(Context context, String account) {
        GiftDialog giftDialog = new GiftDialog(context, JavaUtil.str2long(account), "", "", 0, 0, false);
        giftDialog.setRoomId(null == RoomDataManager.get().getCurrentRoomInfo() ? 0L :
                RoomDataManager.get().getCurrentRoomInfo().getRoomId());
        giftDialog.setGiftDialogBtnClickListener(new GiftDialog.OnGiftDialogBtnClickListener() {
            @Override
            public void onRechargeBtnClick() {
                ChargeActivity.start(context);
            }

            @Override
            public void onSendGiftBtnClick(GiftInfo giftInfo, long uid, int number) {
                RoomInfo currentRoomInfo = RoomDataManager.get().getCurrentRoomInfo();
                if (currentRoomInfo == null) {
                    return;
                }
                CoreManager.getCore(IGiftCore.class).sendRoomGift(giftInfo.getGiftId(), uid,
                        currentRoomInfo.getUid(), number, giftInfo.getGoldPrice());
            }

            @Override
            public void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, int number) {
                RoomInfo currentRoomInfo = RoomDataManager.get().getCurrentRoomInfo();
                if (currentRoomInfo == null) {
                    return;
                }
                List<Long> targetUids = new ArrayList<>();
                for (int i = 0; i < micMemberInfos.size(); i++) {
                    targetUids.add(micMemberInfos.get(i).getUid());
                }
                CoreManager.getCore(IGiftCore.class).sendRoomMultiGift(giftInfo.getGiftId(),
                        targetUids, currentRoomInfo.getUid(), number, giftInfo.getGoldPrice());
            }
        });
        giftDialog.show();
    }

    /**
     * 踢Ta下麦
     */
    public static ButtonItem createKickDownMicItem(final String account) {
        ButtonItem.OnClickListener onClickListener = new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                if (RoomDataManager.get().isOnMic(JavaUtil.str2long(account))) {
                    int micPosition = RoomDataManager.get().getMicPosition(JavaUtil.str2long(account));
                    IMRoomMessageManager.get().downMicroPhoneBySdk(micPosition, null);

                    RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
                    if (roomInfo != null) {
                        IMRoomMessageManager.get().kickMicroPhoneBySdk(
                                micPosition, JavaUtil.str2long(account), roomInfo.getRoomId());

                    }
                }
            }
        };

        return new ButtonItem(BasicConfig.INSTANCE.getAppContext().getString(R.string.embrace_down_mic),
                onClickListener);
    }

    /**
     * 拉他上麦
     */
    public static ButtonItem createInviteOnMicItem(final String account) {
        ButtonItem.OnClickListener onClickListener = new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                int freePosition = RoomDataManager.get().findFreePosition();
                LogUtils.d("upMicroPhone", "freePosition:" + freePosition);
                if (freePosition == Integer.MIN_VALUE) {
                    SingleToastUtil.showToast(
                            BasicConfig.INSTANCE.getAppContext().getString(R.string.no_empty_mic));
                    return;
                }
                IMRoomMessageManager.get().inviteMicroPhoneBySdk(JavaUtil.str2long(account), freePosition);
            }
        };
        return new ButtonItem(BasicConfig.INSTANCE.getAppContext().getString(R.string.embrace_up_mic),
                onClickListener);
    }

    /**
     * 踢出房间:  先强制下麦，再踢出房间
     */
    public static ButtonItem createKickOutRoomItem(final Context context, final IMRoomMember chatRoomMember,
                                                   final String roomId, final String account) {
        ButtonItem.OnClickListener onClickListener = new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                if (chatRoomMember == null) {
                    return;
                }
                if (context instanceof BaseMvpActivity) {
                    OkHttpManager.MyCallBack myCallBack = new OkHttpManager.MyCallBack<Json>() {
                        @Override
                        public void onError(Exception e) {
                            if (e != null) {
                                e.printStackTrace();
                                SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), e.getMessage());
                            }
                        }

                        @Override
                        public void onResponse(Json response) {
                            LogUtils.e(TAG, "kickMember-onResponse response:" + response);
                            IMReportBean imReportBean = new IMReportBean(response);
                            if (imReportBean.getReportData().errno != 0) {
                                SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), imReportBean.getReportData().errmsg + "");
                            }
                        }
                    };

                    DialogManager dialogManager = ((BaseMvpActivity) context).getDialogManager();
                    DialogManager.AbsOkDialogListener absOkDialogListener = new DialogManager.AbsOkDialogListener() {
                        @Override
                        public void onOk() {
                            final Map<String, Object> reason = new HashMap<>(2);
                            reason.put("reason", "kick");
                            long currentUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
                            reason.put("administratorsUid", currentUid + "");
                            if (RoomDataManager.get().isOnMic(JavaUtil.str2long(account))) {
                                int micPosition = RoomDataManager.get().getMicPosition(JavaUtil.str2long(account));
                                reason.put("micPosition", micPosition);
                                reason.put("account", account);
                                IMRoomMessageManager.get().downMicroPhoneBySdk(micPosition, null);
                            }

                            //判断是否在排麦
                            Json json = RoomDataManager.get().mMicInListMap.get(
                                    Integer.parseInt(chatRoomMember.getAccount()));
                            if (json != null) {
                                IMRoomMessageManager.get().removeMicInList(
                                        chatRoomMember.getAccount(), roomId, null);
                            }
                            IMRoomMessageManager.get().kickMember(account, myCallBack);
                        }
                    };
                    dialogManager.showOkCancelDialog("是否将".concat(chatRoomMember.getNick()).concat("踢出房间？"),
                            true, absOkDialogListener);
                }


            }
        };
        return new ButtonItem("踢出房间", onClickListener);
    }

    /**
     * 查看资料弹窗
     */
    public static ButtonItem createCheckUserInfoDialogItem(final Context context, final String account) {
        return new ButtonItem("查看资料", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                new UserInfoDialog(context, JavaUtil.str2long(account)).show();
            }
        });
    }

    /**
     * 下麦
     */
    public static ButtonItem createDownMicItem() {
        return new ButtonItem(BasicConfig.INSTANCE.getAppContext().getString(R.string.down_mic_text), new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                long currentUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
                IMRoomMessageManager.get().downMicroPhoneBySdk(
                        RoomDataManager.get().getMicPosition(currentUid), null);
            }
        });
    }

    //设置管理员
    public static ButtonItem createMarkManagerListItem(final String roomId, final String account, final boolean mark, IMRoomMember chatRoomMember) {
        String title = BasicConfig.INSTANCE.getAppContext().getString(mark ? R.string.set_manager : R.string.remove_manager);
        ButtonItem.OnClickListener onClickListener = new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                IMRoomMessageManager.get().markManager(account, mark, null);
            }
        };
        return new ButtonItem(title, onClickListener);
    }

    //加入黑名单
    public static ButtonItem createMarkBlackListItem(final Context context, final IMRoomMember chatRoomMember, final String roomId) {
        LogUtils.d("createMarkBlackListItem", "create");
        return new ButtonItem(context.getResources().getString(R.string.add_user_to_room_black), new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                LogUtils.d("createMarkBlackListItem", "onClick");
                if (null == chatRoomMember) {
                    LogUtils.d("incomingChatObserver", "无用户信息");
                    return;
                }
                DialogManager.AbsOkDialogListener listener = new DialogManager.AbsOkDialogListener() {
                    @Override
                    public void onOk() {
                        IMRoomMessageManager.get().markBlackList(chatRoomMember.getAccount(), true, null);
                    }
                };
                if (context instanceof BaseMvpActivity) {
                    BaseMvpActivity activity = (BaseMvpActivity) context;
                    String message = activity.getResources().getString(R.string.add_user_to_room_black_tips, chatRoomMember.getNick());
                    activity.getDialogManager().showOkCancelDialog(message, true, listener);
                }
            }
        });
    }

    public static ButtonItem createSendGiftItem(final Context context, String uid) {
        ButtonItem buttonItem = new ButtonItem("送礼物", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                showGiftDialog(context, uid);
            }
        });
        return buttonItem;
    }

    /**
     * 禁麦
     */
    public static ButtonItem createLockMicItem(final int position, ButtonItem.OnClickListener onClickListener) {
        return new ButtonItem(BasicConfig.INSTANCE.getAppContext().getString(R.string.forbid_mic), onClickListener);
    }

    /**
     * 取消禁麦
     */
    public static ButtonItem createFreeMicItem(final int position, ButtonItem.OnClickListener onClickListener) {
        return new ButtonItem(BasicConfig.INSTANCE.getAppContext().getString(R.string.no_forbid_mic), onClickListener);
    }

    /**
     * 管理员
     *
     * @param context
     * @return
     */
    public static ButtonItem createManagerListItem(final Context context) {
        ButtonItem buttonItem = new ButtonItem("管理员", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                RoomManagerListActivity.start(context);
            }
        });
        return buttonItem;
    }
}
