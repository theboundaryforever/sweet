package com.yuhuankj.tmxq.ui.user.password;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;

/**
 * @author liaoxy
 * @Description:修改密码
 * @date 2019/3/26 18:39
 */
public class ModifyPwdActivity extends BaseActivity {

    private EditText edtPwd1;
    private View vPwdTip1;
    private EditText edtPwd2;
    private View vPwdTip2;
    private EditText edtPwd3;
    private View vPwdTip3;
    private Button btnEnter;
    private int type = 1;
    private LinearLayout llPwd1;
    private LinearLayout llPwd2;
    private LinearLayout llPwd3;
    private TextView tvFogetPwd;
    private TextView tvPwdTitle1;
    private TextView tvPwdTitle2;
    private TextView tvPwdTitle3;
    private String pwd1, pwd2, pwd3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_psw);
        initView();
        initData();
    }

    private void initView() {
        edtPwd1 = (EditText) findViewById(R.id.edtPwd1);
        GrowingIO.getInstance().trackEditText(edtPwd1);
        vPwdTip1 = findViewById(R.id.vPwdTip1);
        edtPwd2 = (EditText) findViewById(R.id.edtPwd2);
        GrowingIO.getInstance().trackEditText(edtPwd2);
        vPwdTip2 = findViewById(R.id.vPwdTip2);
        edtPwd3 = (EditText) findViewById(R.id.edtPwd3);
        GrowingIO.getInstance().trackEditText(edtPwd3);
        vPwdTip3 = findViewById(R.id.vPwdTip3);
        btnEnter = (Button) findViewById(R.id.btnEnter);
        tvFogetPwd = (TextView) findViewById(R.id.tvFogetPwd);
        llPwd1 = (LinearLayout) findViewById(R.id.llPwd1);
        llPwd2 = (LinearLayout) findViewById(R.id.llPwd2);
        llPwd3 = (LinearLayout) findViewById(R.id.llPwd3);
        llPwd3.setVisibility(View.VISIBLE);
        tvFogetPwd.setVisibility(View.VISIBLE);
        tvPwdTitle1 = (TextView) findViewById(R.id.tvPwdTitle1);
        tvPwdTitle2 = (TextView) findViewById(R.id.tvPwdTitle2);
        tvPwdTitle3 = (TextView) findViewById(R.id.tvPwdTitle3);
    }

    private void initData() {
        Intent intent = getIntent();
        if (intent == null) {
            finish();
            return;
        }
        type = intent.getIntExtra("type", 1);
        if (type == 1) {
            initTitleBar("修改登录密码");
        } else {
            initTitleBar("修改提现密码");
            edtPwd1.setHint("请输入当前提现密码");
            edtPwd3.setHint("再次确认新的提现密码");
        }
        btnEnter.setOnClickListener(this);
        tvFogetPwd.setOnClickListener(this);
        vPwdTip1.setOnClickListener(this);
        vPwdTip2.setOnClickListener(this);
        vPwdTip3.setOnClickListener(this);

        edtPwd1.setFocusable(true);
        edtPwd1.requestFocus();
    }

    private boolean check() {
        pwd1 = edtPwd1.getText().toString();
        pwd2 = edtPwd2.getText().toString();
        pwd3 = edtPwd3.getText().toString();
        if (TextUtils.isEmpty(pwd1) || TextUtils.isEmpty(pwd2) || TextUtils.isEmpty(pwd3)) {
            SingleToastUtil.showToast("密码不能为空");
            return false;
        }
        if (pwd1.length() < 6 || pwd2.length() < 6 || pwd3.length() < 6) {
            SingleToastUtil.showToast("密码不能小于6位数");
            return false;
        }
        if (!pwd2.equals(pwd3)) {
            SingleToastUtil.showToast("两次密码输入不一致");
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view == btnEnter) {
            if (check()) {
                DialogManager dialogManager = new DialogManager(this);
                dialogManager.showProgressDialog(this, "加载中...");
                new PwdModel().modifyPwd(pwd1, pwd2, pwd3, type, new OkHttpManager.MyCallBack<ServiceResult<String>>() {
                    @Override
                    public void onError(Exception e) {
                        dialogManager.dismissDialog();
                        e.printStackTrace();
                        SingleToastUtil.showToast(e.getMessage());
                    }

                    @Override
                    public void onResponse(ServiceResult<String> response) {
                        dialogManager.dismissDialog();
                        if (response != null && response.isSuccess()) {
                            SingleToastUtil.showToast("修改密码成功");
                            finish();
                        } else {
                            if (response != null && !TextUtils.isEmpty(response.getMessage())) {
                                SingleToastUtil.showToast(response.getMessage());
                            } else {
                                SingleToastUtil.showToast("修改密码失败");
                            }
                        }
                    }
                });
            }
        } else if (view == tvFogetPwd) {
            tvFogetPwd.setEnabled(false);
            Intent intent = new Intent(this, ResetPwdActivity.class);
            intent.putExtra("type", type);
            startActivity(intent);
            tvFogetPwd.postDelayed(new Runnable() {
                @Override
                public void run() {
                    tvFogetPwd.setEnabled(true);
                }
            },1000);
        } else if (view == vPwdTip1) {
            vPwdTip1.setSelected(!vPwdTip1.isSelected());
            if (!vPwdTip1.isSelected()) {
                edtPwd1.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            } else {
                edtPwd1.setInputType(InputType.TYPE_CLASS_TEXT);
            }
            edtPwd1.setSelection(edtPwd1.getText().length());
        } else if (view == vPwdTip2) {
            vPwdTip2.setSelected(!vPwdTip2.isSelected());
            if (!vPwdTip2.isSelected()) {
                edtPwd2.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            } else {
                edtPwd2.setInputType(InputType.TYPE_CLASS_TEXT);
            }
            edtPwd2.setSelection(edtPwd2.getText().length());
        } else if (view == vPwdTip3) {
            vPwdTip3.setSelected(!vPwdTip3.isSelected());
            if (!vPwdTip3.isSelected()) {
                edtPwd3.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            } else {
                edtPwd3.setInputType(InputType.TYPE_CLASS_TEXT);
            }
            edtPwd3.setSelection(edtPwd3.getText().length());
        }
    }
}
