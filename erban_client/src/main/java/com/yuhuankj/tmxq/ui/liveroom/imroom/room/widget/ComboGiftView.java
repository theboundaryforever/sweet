package com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.gift.ComboGiftPublicScreenMsgSender;
import com.tongdaxing.xchat_core.gift.ComboGiftVoInfo;
import com.tongdaxing.xchat_core.gift.ComboRang;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.gift.MultiGiftReceiveInfo;
import com.yuhuankj.tmxq.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2019/4/7.
 */

public class ComboGiftView extends LinearLayout {

    public final static String TAG = ComboGiftView.class.getSimpleName();

    private Context mContext;

    private int mMaxItem = 2;

    /**
     * 等待中的
     */
    private Map<String, ComboGiftVoInfo> waitingComboGiftVoInfoMap = new HashMap<>();

    public ComboGiftView(Context context) {
        this(context, null);
    }

    public ComboGiftView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ComboGiftView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        setOrientation(VERTICAL);
        waitingComboGiftVoInfoMap.clear();
    }


    /**
     * 添加giftInfo给本地缓存队列
     *
     * @param multiGiftReceiveInfo
     * @return
     */
    public void addMultiGiftInfo(MultiGiftReceiveInfo multiGiftReceiveInfo) {
        String comboGiftAnimId = ComboGiftPublicScreenMsgSender.getInstance().genComboGiftAnimId(multiGiftReceiveInfo.getUid(), multiGiftReceiveInfo.getGiftId(), multiGiftReceiveInfo.getComboId());
        ComboGiftItemView itemView = findViewByItemComboId(comboGiftAnimId);
        int currentCount = getChildCount();
        LogUtils.d(TAG, "addMultiGiftInfo comboGiftAnimId:" + comboGiftAnimId + " currentCount:" + currentCount + " mMaxItem:" + mMaxItem);
        ComboGiftItemView.OnOutAnimEndListener onOutAnimEndListener = new ComboGiftItemView.OnOutAnimEndListener() {
            @Override
            public void onOutAnimEnd(ComboGiftItemView view) {
                if (null != waitingComboGiftVoInfoMap && waitingComboGiftVoInfoMap.keySet().size() > 0) {
                    ComboGiftVoInfo voInfo = waitingComboGiftVoInfoMap.get(waitingComboGiftVoInfoMap.keySet().iterator().next());
                    view.setComboGiftVoInfo(voInfo);
                    LogUtils.d(TAG, "addMultiGiftInfo-->onOutAnimEnd-->playInAnim comboId:" + voInfo.getItemComboId());
                    view.setTag(voInfo.getItemComboId());
                    view.playInAnim();
                } else {
                    LogUtils.d(TAG, "addMultiGiftInfo-->onOutAnimEnd-->clear");
                    view.clear();
                    view.setTag(null);
                    //参考：https://blog.csdn.net/yshxjoy/article/details/50330819
                    ComboGiftView.this.post(new Runnable() {
                        @Override
                        public void run() {
                            removeView(view);
                        }
                    });

                }
            }
        };
        if (itemView == null) {
            // 当前没有找到活动的itemView
            ComboGiftVoInfo voInfo = assembleComboGiftVo(multiGiftReceiveInfo);
            if (currentCount < mMaxItem) {
                // 当前活动的itemView少于两个,说明可以继续插入
                ComboGiftItemView comboGiftItemView = new ComboGiftItemView(getContext());
                comboGiftItemView.setComboGiftVoInfo(voInfo);
                comboGiftItemView.setOnOutAnimEndListener(onOutAnimEndListener);
                comboGiftItemView.setTag(comboGiftAnimId);
                addView(comboGiftItemView);
                LogUtils.d(TAG, "addMultiGiftInfo add, then playInAnim");
                comboGiftItemView.playInAnim();
            } else {
                //当前活动的itemview数量大于等于2，则说明现在的必须进入等待队列
                //如果等待队列中已经存在comboId对应voInfo，则更新rang
                if (null != waitingComboGiftVoInfoMap && waitingComboGiftVoInfoMap.containsKey(comboGiftAnimId)) {
                    LogUtils.d(TAG, "addMultiGiftInfo 更新缓存");
                    waitingComboGiftVoInfoMap.get(comboGiftAnimId).addComboRangToLast(
                            new ComboRang(multiGiftReceiveInfo.getComboRangStart(), multiGiftReceiveInfo.getComboRangEnd()));
                } else {
                    //如果不存在，则直接添加
                    if (null != waitingComboGiftVoInfoMap) {
                        LogUtils.d(TAG, "addMultiGiftInfo 插入缓存");
                        waitingComboGiftVoInfoMap.put(comboGiftAnimId, voInfo);
                    }
                }
            }
        } else {
            if (null != itemView.getComboGiftVoInfo()) {
                LogUtils.d(TAG, "addMultiGiftInfo 找到了，animStatus:" + itemView.getComboGiftVoInfo().getGiftAnimStatus());
            }
            //当前能够找到itemView,且还未播放退出动画
            if (null != itemView.getComboGiftVoInfo() &&
                    (itemView.getComboGiftVoInfo().getGiftAnimStatus() == ComboGiftVoInfo.GiftAnimStatus.PrepareAndPlaying
                            || itemView.getComboGiftVoInfo().getGiftAnimStatus() == ComboGiftVoInfo.GiftAnimStatus.Holding)) {
                itemView.updateComboItemNumRange(new ComboRang(multiGiftReceiveInfo.getComboRangStart(), multiGiftReceiveInfo.getComboRangEnd()));
                if (itemView.getComboGiftVoInfo().getGiftAnimStatus() == ComboGiftVoInfo.GiftAnimStatus.Holding) {
                    LogUtils.d(TAG, "addMultiGiftInfo-->playNextComboNumAnim");
                    //停留动画的过程中，才再次进入下个数字动画循环,否则就等数字动画自己播放结束了自己处理就好了
                    itemView.playNextComboNumAnim();
                }
            } else {
                //在播放退出动画或者已退出，那么直接交给onOutAnimEnd回调处理，同样插入等待队列中
                //极端情况，会出现0-1，1-2，3-4，3-4，但是3-4是2-3播完移除后的后续插入播放的，也即直接播放x4动画
                ComboGiftVoInfo voInfo = assembleComboGiftVo(multiGiftReceiveInfo);
                if (null != waitingComboGiftVoInfoMap) {
                    LogUtils.d(TAG, "addMultiGiftInfo 插入缓存");
                    waitingComboGiftVoInfoMap.put(comboGiftAnimId, voInfo);
                }
            }
        }
    }

    /**
     * 添加giftInfo给本地缓存队列
     *
     * @param giftReceiveInfo
     * @return
     */
    public void addSingleGiftInfo(GiftReceiveInfo giftReceiveInfo) {
        String comboGiftAnimId = ComboGiftPublicScreenMsgSender.getInstance().genComboGiftAnimId(giftReceiveInfo.getUid(), giftReceiveInfo.getGiftId(), giftReceiveInfo.getComboId());
        ComboGiftItemView itemView = findViewByItemComboId(comboGiftAnimId);
        int currentCount = getChildCount();
        LogUtils.d(TAG, "addSingleGiftInfo comboGiftAnimId:" + comboGiftAnimId + " currentCount:" + currentCount + " mMaxItem:" + mMaxItem);
        ComboGiftItemView.OnOutAnimEndListener onOutAnimEndListener = new ComboGiftItemView.OnOutAnimEndListener() {
            @Override
            public void onOutAnimEnd(ComboGiftItemView view) {
                if (null != waitingComboGiftVoInfoMap && waitingComboGiftVoInfoMap.keySet().size() > 0) {
                    ComboGiftVoInfo voInfo = waitingComboGiftVoInfoMap.get(waitingComboGiftVoInfoMap.keySet().iterator().next());
                    view.setComboGiftVoInfo(voInfo);
                    view.setTag(voInfo.getItemComboId());
                    LogUtils.d(TAG, "addSingleGiftInfo-->onOutAnimEnd-->playInAnim comboId:" + voInfo.getItemComboId());
                    view.playInAnim();
                } else {
                    LogUtils.d(TAG, "addSingleGiftInfo-->onOutAnimEnd-->clear");
                    view.setTag(null);
                    view.clear();
                    //参考：https://blog.csdn.net/yshxjoy/article/details/50330819
                    ComboGiftView.this.post(new Runnable() {
                        @Override
                        public void run() {
                            removeView(view);
                        }
                    });
                }
            }
        };
        if (itemView == null) {
            // 当前没有找到活动的itemView
            ComboGiftVoInfo voInfo = assembleComboGiftVo(giftReceiveInfo);
            if (currentCount < mMaxItem) {
                // 当前活动的itemView少于两个,说明可以继续插入
                ComboGiftItemView comboGiftItemView = new ComboGiftItemView(getContext());
                comboGiftItemView.setComboGiftVoInfo(voInfo);
                comboGiftItemView.setOnOutAnimEndListener(onOutAnimEndListener);
                comboGiftItemView.setTag(comboGiftAnimId);
                addView(comboGiftItemView);
                LogUtils.d(TAG, "addSingleGiftInfo add, then playInAnim");
                comboGiftItemView.playInAnim();
            } else {
                //当前活动的itemview数量大于等于2，则说明现在的必须进入等待队列
                //如果等待队列中已经存在comboId对应voInfo，则更新rang
                if (null != waitingComboGiftVoInfoMap && waitingComboGiftVoInfoMap.containsKey(comboGiftAnimId)) {
                    LogUtils.d(TAG, "addSingleGiftInfo 更新缓存");
                    waitingComboGiftVoInfoMap.get(comboGiftAnimId).addComboRangToLast(
                            new ComboRang(giftReceiveInfo.getComboRangStart(), giftReceiveInfo.getComboRangEnd()));
                } else {
                    //如果不存在，则直接添加
                    if (null != waitingComboGiftVoInfoMap) {
                        LogUtils.d(TAG, "addSingleGiftInfo 插入缓存");
                        waitingComboGiftVoInfoMap.put(comboGiftAnimId, voInfo);
                    }
                }
            }
        } else {
            //当前能够找到itemView,且还未播放退出动画
            if (null != itemView.getComboGiftVoInfo()) {
                LogUtils.d(TAG, "addSingleGiftInfo 找到了，animStatus:" + itemView.getComboGiftVoInfo().getGiftAnimStatus());
            }
            if (null != itemView.getComboGiftVoInfo() &&
                    (itemView.getComboGiftVoInfo().getGiftAnimStatus() == ComboGiftVoInfo.GiftAnimStatus.PrepareAndPlaying
                            || itemView.getComboGiftVoInfo().getGiftAnimStatus() == ComboGiftVoInfo.GiftAnimStatus.Holding)) {
                LogUtils.d(TAG, "addSingleGiftInfo-->updateComboItemNumRange");
                itemView.updateComboItemNumRange(new ComboRang(giftReceiveInfo.getComboRangStart(), giftReceiveInfo.getComboRangEnd()));
                if (itemView.getComboGiftVoInfo().getGiftAnimStatus() == ComboGiftVoInfo.GiftAnimStatus.Holding) {
                    //停留动画的过程中，才再次进入下个数字动画循环,否则就等数字动画自己播放结束了自己处理就好了
                    LogUtils.d(TAG, "addSingleGiftInfo-->playNextComboNumAnim");
                    itemView.playNextComboNumAnim();
                }
            } else {
                //在播放退出动画或者已退出，那么直接交给onOutAnimEnd回调处理，同样插入等待队列中
                //极端情况，会出现0-1，1-2，3-4，3-4，但是3-4是2-3播完移除后的后续插入播放的，也即直接播放x4动画
                ComboGiftVoInfo voInfo = assembleComboGiftVo(giftReceiveInfo);
                if (null != waitingComboGiftVoInfoMap) {
                    LogUtils.d(TAG, "addSingleGiftInfo 插入缓存");
                    waitingComboGiftVoInfoMap.put(comboGiftAnimId, voInfo);
                }
            }
        }
    }

    /**
     * // 组装Vo
     *
     * @param multiGiftReceiveInfo
     * @return
     */
    private ComboGiftVoInfo assembleComboGiftVo(MultiGiftReceiveInfo multiGiftReceiveInfo) {
        ComboGiftVoInfo voInfo = new ComboGiftVoInfo();
        GiftInfo giftInfo = CoreManager.getCore(IGiftCore.class).findGiftInfoById(multiGiftReceiveInfo.getGiftId());

        voInfo.addComboRangToLast(new ComboRang(multiGiftReceiveInfo.getComboRangStart(), multiGiftReceiveInfo.getComboRangEnd()));
        voInfo.setAvatar(multiGiftReceiveInfo.getAvatar());
        String nick = multiGiftReceiveInfo.getNick();
        if (!TextUtils.isEmpty(nick) && nick.length() > 5) {
            nick = mContext.getResources().getString(R.string.nick_length_max_six, nick.substring(0, 5));
        }
        voInfo.setNick(nick);

        voInfo.setContent(mContext.getResources().getString(R.string.combo_gift_anim_content_allmic));
        voInfo.setGid(giftInfo.getGiftId());
        voInfo.setGiftUrl(giftInfo.getGiftUrl());
        voInfo.setAllMicCombo(true);
        voInfo.setComboId(multiGiftReceiveInfo.getComboId());
        voInfo.setItemComboId(ComboGiftPublicScreenMsgSender.getInstance().genComboGiftAnimId(multiGiftReceiveInfo.getUid(), multiGiftReceiveInfo.getGiftId(), multiGiftReceiveInfo.getComboId()));
        Log.d(TAG, "GiftDialog assembleComboGiftVo: ComboId = " + voInfo.getItemComboId());
        return voInfo;
    }

    /**
     * // 组装Vo
     *
     * @param giftReceiveInfo
     * @return
     */
    private ComboGiftVoInfo assembleComboGiftVo(GiftReceiveInfo giftReceiveInfo) {
        ComboGiftVoInfo voInfo = new ComboGiftVoInfo();
        GiftInfo giftInfo = CoreManager.getCore(IGiftCore.class).findGiftInfoById(giftReceiveInfo.getGiftId());

        voInfo.addComboRangToLast(new ComboRang(giftReceiveInfo.getComboRangStart(), giftReceiveInfo.getComboRangEnd()));
        voInfo.setAvatar(giftReceiveInfo.getAvatar());

        String nick = giftReceiveInfo.getNick();
        if (!TextUtils.isEmpty(nick) && nick.length() > 5) {
            nick = mContext.getResources().getString(R.string.nick_length_max_six, nick.substring(0, 5));
        }
        voInfo.setNick(nick);

        String targetNick = giftReceiveInfo.getTargetNick();
        if (!TextUtils.isEmpty(targetNick) && targetNick.length() > 4) {
            targetNick = mContext.getResources().getString(R.string.nick_length_max_six, targetNick.substring(0, 4));
        }
        String content = mContext.getResources().getString(R.string.combo_gift_anim_content_single, targetNick);
        voInfo.setContent(content);
        voInfo.setGid(giftInfo.getGiftId());
        voInfo.setGiftUrl(giftInfo.getGiftUrl());
        voInfo.setAllMicCombo(false);
        voInfo.setComboId(giftReceiveInfo.getComboId());
        voInfo.setItemComboId(ComboGiftPublicScreenMsgSender.getInstance().genComboGiftAnimId(giftReceiveInfo.getUid(), giftReceiveInfo.getGiftId(), giftReceiveInfo.getComboId()));
        Log.d(TAG, "GiftDialog assembleComboGiftVo: ComboId = " + voInfo.getItemComboId());
        return voInfo;
    }

    /**
     * 找到对应ComboId的ComboGiftItemView
     *
     * @param comboId
     * @return
     */
    public ComboGiftItemView findViewByItemComboId(String comboId) {
        ComboGiftItemView giftView = null;
        for (int i = 0; i < getChildCount(); i++) {
            View childView = getChildAt(i);
            if (childView instanceof ComboGiftItemView) {
                giftView = (ComboGiftItemView) childView;
                if (null != childView.getTag() && !TextUtils.isEmpty(childView.getTag().toString())
                        && childView.getTag().toString().equals(comboId)) {
                    return giftView;
                }
            }
        }
        return null;
    }

    public void clear() {

        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            ComboGiftItemView comboGiftItemView = (ComboGiftItemView) getChildAt(i);
            if (null != comboGiftItemView) {
                comboGiftItemView.setOnOutAnimEndListener(null);
                comboGiftItemView.clear();
                removeView(comboGiftItemView);
            }
        }
        removeAllViews();
        if (null != waitingComboGiftVoInfoMap) {
            waitingComboGiftVoInfoMap.clear();
        }

    }
}
