package com.yuhuankj.tmxq.ui.nim.chat;

import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.netease.nim.uikit.common.ui.recyclerview.adapter.BaseMultiItemFetchLoadAdapter;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.netease.nim.uikit.session.viewholder.MsgViewHolderBase;
import com.netease.nimlib.sdk.msg.constant.MsgDirectionEnum;
import com.yuhuankj.tmxq.R;

/**
 * @author liaoxy
 * @Description:游戏连麦邀请超时
 * @date 2019/2/15 11:21
 */
public class MsgViewHolderGameLinkMacroTimeOut extends MsgViewHolderBase {

    protected TextView bodyTextView;
    private View llGameLinkMicro;

    public MsgViewHolderGameLinkMacroTimeOut(BaseMultiItemFetchLoadAdapter adapter) {
        super(adapter);
    }

    @Override
    protected int getContentResId() {
        return com.netease.nim.uikit.R.layout.nim_message_item_text_icon;
    }

    @Override
    protected void inflateContentView() {
        bodyTextView = findViewById(com.netease.nim.uikit.R.id.nim_message_item_text_body);
        llGameLinkMicro = findViewById(com.netease.nim.uikit.R.id.llGameLinkMicro);
    }

    @Override
    protected void bindContentView() {
        //GameLinkMacroInvitaion info = ((GameLinkMacroTimeOutAttachment) message.getAttachment()).getDataInfo();
        if (message.getDirect() == MsgDirectionEnum.Out) {
            bodyTextView.setTextColor(Color.WHITE);
            bodyTextView.setText("对方未接听");
        } else {
            bodyTextView.setTextColor(Color.BLACK);
            bodyTextView.setText("未接听");
        }

        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) bodyTextView.getLayoutParams();
        layoutParams.weight = LinearLayout.LayoutParams.WRAP_CONTENT;
        layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        bodyTextView.setLayoutParams(layoutParams);
        bodyTextView.setMinHeight(0);

        contentContainer.setBackgroundColor(Color.TRANSPARENT);
        if (isReceivedMessage()) {
            llGameLinkMicro.setBackgroundResource(com.netease.nim.uikit.R.drawable.nim_message_content_left_bg);
            llGameLinkMicro.setPadding(ScreenUtil.dip2px(10), 0, ScreenUtil.dip2px(10), 0);
        } else {
            llGameLinkMicro.setBackgroundResource(com.netease.nim.uikit.R.drawable.nim_message_content_right_bg);
            llGameLinkMicro.setPadding(ScreenUtil.dip2px(10), 0, ScreenUtil.dip2px(10), 0);
        }

        ImageView imvIcon = findViewById(com.netease.nim.uikit.R.id.imvIcon);
        imvIcon.setImageResource(R.drawable.ic_p2p_linkmacro_dianhua_d);
    }
}
