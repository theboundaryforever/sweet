package com.yuhuankj.tmxq.ui.liveroom.imroom.publicscreen;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.tongdaxing.erban.libcommon.im.IMReportRoute;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.JavaUtil;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomTipAttachment;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMessage;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.liveroom.imroom.publicscreen.base.RoomMessageBaseAdapter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.publicscreen.base.RoomMsgBaseHolder;

/**
 * 文件描述：新的重构房间的适配器
 *
 * @auther：zwk
 * @data：2019/5/7
 */
public class RoomMessageAdapter extends RoomMessageBaseAdapter<IMRoomMessage> {

    private MessageView.OnMsgContentClickListener onMsgContentClickListener;

    public RoomMessageAdapter(Context context) {
        super(context);
    }

    @Override
    protected void initItemListener(RoomMsgBaseHolder holder, IMRoomMessage imRoomMessage, int position) {
        holder.setOnItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dealWithClickEvent(imRoomMessage, false);
            }
        });
        holder.setOnItemUseClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dealWithClickEvent(imRoomMessage, true);
            }
        });
    }

    public void setOnMsgContentClickListener(MessageView.OnMsgContentClickListener onMsgContentClickListener) {
        this.onMsgContentClickListener = onMsgContentClickListener;
    }


    public void dealWithClickEvent(IMRoomMessage imRoomMessage, boolean isUserClick) {
        if (!IMReportRoute.ChatRoomTip.equalsIgnoreCase(imRoomMessage.getRoute())) {
            String account = "";
            String nick = "";
            if (IMReportRoute.sendMessageReport.equalsIgnoreCase(imRoomMessage.getRoute())) {
                CustomAttachment attachment = imRoomMessage.getAttachment();
                if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_ROOM_TIP) {
                    account = ((RoomTipAttachment) attachment).getUid() + "";
                } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_ROOM_ATTENTION) {
                    if (!RoomDataManager.get().hasLikeRoomOwner()) {
                        //房间-公屏关注
                        StatisticManager.get().onEvent(mContext,
                                StatisticModel.EVENT_ID_ROOM_PUBLIC_SCREEN_ATTENTION,
                                StatisticModel.getInstance().getUMAnalyCommonMap(mContext));
                    }
                    if (onMsgContentClickListener != null) {
                        onMsgContentClickListener.clickToAttentionRoomOwner(!RoomDataManager.get().hasLikeRoomOwner());
                    }
                    return;
                } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_FIRST_ROOM_ATTENTION_TIPS
                        && attachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_ROOM_ATTENTION_GUIDE) {
                    if (!RoomDataManager.get().hasAttentionRoom()) {
                        StatisticManager.get().onEvent(mContext,
                                StatisticModel.EVENT_ID_ROOM_PUBLIC_SCREEN_ATTENTION,
                                StatisticModel.getInstance().getUMAnalyCommonMap(mContext));
                    }
                    if (onMsgContentClickListener != null) {
                        onMsgContentClickListener.clickToAttentionRoom(!RoomDataManager.get().hasAttentionRoom());
                    }
                } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_ROOM_RICH_TXT
                        || attachment.getFirst() == CustomAttachment.CUSTOM_MSG_ROOM_SYSTEM_RICH_TXT) {
                    if (isUserClick) {
                        if (imRoomMessage.getImRoomMember() != null) {
                            account = imRoomMessage.getImRoomMember().getAccount();
                            nick = imRoomMessage.getImRoomMember().getNick();
                        }
                    } else {
                        return;
                    }
                } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK
                        || attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE
                        || attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT
                        || attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT
                        || attachment.getFirst() == CustomAttachment.CUSTOM_MSG_LOTTERY_BOX) {
                    if (imRoomMessage.getImRoomMember() != null) {
                        account = imRoomMessage.getImRoomMember().getAccount();
                    }
                    if (onMsgContentClickListener != null) {
                        onMsgContentClickListener.clickToOpenUserInfoDialog(JavaUtil.str2long(account));
                    }
                    return;
                } else {
                    if (imRoomMessage.getImRoomMember() != null) {
                        account = imRoomMessage.getImRoomMember().getAccount();
                        nick = imRoomMessage.getImRoomMember().getNick();
                    }
                }
            } else {
                if (imRoomMessage.getImRoomMember() != null) {
                    account = imRoomMessage.getImRoomMember().getAccount();
                    nick = imRoomMessage.getImRoomMember().getNick();
                }
            }
            if (!TextUtils.isEmpty(account) && onMsgContentClickListener != null) {
                onMsgContentClickListener.onUserOperate(Long.valueOf(account), nick);
            }
        }
    }
}
