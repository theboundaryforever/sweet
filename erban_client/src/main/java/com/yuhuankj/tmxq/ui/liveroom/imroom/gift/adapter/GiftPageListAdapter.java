package com.yuhuankj.tmxq.ui.liveroom.imroom.gift.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.juxiao.library_utils.DisplayUtils;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftType;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.imroom.gift.RoomGiftConstant;
import com.yuhuankj.tmxq.ui.liveroom.imroom.gift.listener.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * 文件描述：礼物列表分页布局页面
 *
 * @auther：zwk
 * @data：2019/5/10
 */
public class GiftPageListAdapter extends RecyclerView.Adapter<GiftPageListAdapter.GiftHolder> {
    private Context mContext;
    private List<GiftInfo> mDatas;
    private int screenWith = 0;
    private GiftType giftType;

    public GiftPageListAdapter(Context mContext, GiftType giftType) {
        this.mContext = mContext;
        if (this.mDatas == null) {
            this.mDatas = new ArrayList<>();
        }
        screenWith = DisplayUtils.getScreenWidth(mContext);
        this.giftType = giftType;
    }

    @NonNull
    @Override
    public GiftHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        GiftHolder holder = new GiftHolder(LayoutInflater.from(mContext).inflate(R.layout.item_gift_page_list, viewGroup, false));
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull GiftHolder giftHolder, int i) {
        giftHolder.rvGiftPageItem.setHasFixedSize(true);
        giftHolder.rvGiftPageItem.setNestedScrollingEnabled(false);
        giftHolder.updateData(mDatas, i);
    }

    @Override
    public int getItemCount() {
        if (mDatas == null || mDatas.size() <= 0) {
            return 0;
        }
        return (int) Math.ceil(mDatas.size() * 1.0 / RoomGiftConstant.PAGE_SIZE);
    }

    public void setGiftInfoList(List<GiftInfo> giftInfoList) {
        this.mDatas = giftInfoList;
        notifyDataSetChanged();
    }

    public void setmDatas(List<GiftInfo> mDatas) {
        this.mDatas = mDatas;
        notifyDataSetChanged();
    }

    public List<GiftInfo> getmDatas() {
        return mDatas;
    }

    public void setGiftType(GiftType giftType) {
        this.giftType = giftType;
    }

    class GiftHolder extends RecyclerView.ViewHolder {
        private RecyclerView rvGiftPageItem;
        private GiftPageListItemAdapter adapter;

        public GiftHolder(@NonNull View itemView) {
            super(itemView);
            rvGiftPageItem = itemView.findViewById(R.id.rv_gift_page_list);
            adapter = new GiftPageListItemAdapter(mContext, giftType);
            rvGiftPageItem.setAdapter(adapter);
            adapter.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(GiftInfo giftInfo, int position) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(giftInfo, position);
                        notifyDataSetChanged();
                    }
                }
            });
        }


        public void updateData(List<GiftInfo> mDatas, int postion) {
            if (adapter != null) {
                adapter.updataDataAndIndex(mDatas, postion);
            }
        }
    }

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

}
