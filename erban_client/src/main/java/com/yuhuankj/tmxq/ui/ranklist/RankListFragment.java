package com.yuhuankj.tmxq.ui.ranklist;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.fragment.BaseFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liaoxy
 * @Description:排行榜
 * @date 2019/3/5 17:13
 */
public class RankListFragment extends BaseFragment {
    private static final String TAG = RankListFragment.class.getSimpleName();
    private ViewPager vpContent;
    private TabPageAdapter tabPageAdapter;
    private int type = 2;//默认财富榜
    private TextView tvDayType, tvWeekType, tvAllType;
    private LinearLayout llTypeTab;
    private List<Fragment> fragments;

    public static RankListFragment newInstance(int type) {
        RankListFragment rankListFragment = new RankListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        rankListFragment.setArguments(bundle);
        return rankListFragment;
    }

    @Override
    protected void onInitArguments(Bundle bundle) {
        super.onInitArguments(bundle);
        if (bundle != null) {
            type = bundle.getInt("type", 2);
        }
    }

    @Override
    public int getRootLayoutId() {
        if (type == 1) {
            return R.layout.fragment_ranklist;
        } else {
            return R.layout.fragment_ranklist_fortune;
        }
    }

    @Override
    public void onFindViews() {
        llTypeTab = mView.findViewById(R.id.llTypeTab);
        vpContent = mView.findViewById(R.id.vpContent);

        tvDayType = mView.findViewById(R.id.tvDayType);
        tvWeekType = mView.findViewById(R.id.tvWeekType);
        tvAllType = mView.findViewById(R.id.tvAllType);

        tvDayType.setOnClickListener(this);
        tvWeekType.setOnClickListener(this);
        tvAllType.setOnClickListener(this);

        tvDayType.setSelected(true);
        tvAllType.setSelected(false);
        tvWeekType.setSelected(false);
    }

    @Override
    public void onSetListener() {

    }


    @Override
    public void initiate() {
        fragments = new ArrayList<>();
        fragments.add(RankListItemFragment.newInstance(type, 1));
        fragments.add(RankListItemFragment.newInstance(type, 2));
        fragments.add(RankListItemFragment.newInstance(type, 4));
        tabPageAdapter = new TabPageAdapter(getChildFragmentManager(), fragments);
        vpContent.setAdapter(tabPageAdapter);

        vpContent.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    tvDayType.setSelected(true);
                    tvAllType.setSelected(false);
                    tvWeekType.setSelected(false);
                } else if (position == 1) {
                    tvDayType.setSelected(false);
                    tvAllType.setSelected(false);
                    tvWeekType.setSelected(true);
                } else {
                    tvDayType.setSelected(false);
                    tvAllType.setSelected(true);
                    tvWeekType.setSelected(false);
                }

                try {
                    RankListItemFragment rankListItemFragment = (RankListItemFragment) fragments.get(position);
                    rankListItemFragment.setOffsetY();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        vpContent.setCurrentItem(0);
    }

    public void setOffsetY() {
        try {
            int curItem = vpContent.getCurrentItem();
            RankListItemFragment rankListItemFragment = (RankListItemFragment) fragments.get(curItem);
            rankListItemFragment.setOffsetY();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view == tvDayType) {
            tvDayType.setSelected(true);
            tvAllType.setSelected(false);
            tvWeekType.setSelected(false);
            vpContent.setCurrentItem(0);
        } else if (view == tvWeekType) {
            tvDayType.setSelected(false);
            tvAllType.setSelected(false);
            tvWeekType.setSelected(true);
            vpContent.setCurrentItem(1);
        } else if (view == tvAllType) {
            tvDayType.setSelected(false);
            tvAllType.setSelected(true);
            tvWeekType.setSelected(false);
            vpContent.setCurrentItem(2);
        }
    }

    private RelativeLayout.LayoutParams layoutParams;

    public int setTabOffsetY(int y, boolean isScroing) {
        if (layoutParams == null) {
            layoutParams = (RelativeLayout.LayoutParams) llTypeTab.getLayoutParams();
            llTypeTab.setTag(R.id.tv_position + 1, layoutParams.topMargin);
        }
        int oriTopMargin = (int) llTypeTab.getTag(R.id.tv_position + 1);
        if (isScroing) {
            layoutParams.topMargin -= y;
        } else {
            if (y == 0) {
                layoutParams.topMargin = oriTopMargin;
            } else {
                layoutParams.topMargin = y;
            }
        }
        llTypeTab.requestLayout();

        return layoutParams.topMargin;
    }
}