package com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget;

import android.animation.ValueAnimator;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.dinuscxj.progressbar.CircleProgressBar;
import com.tongdaxing.erban.libcommon.base.AbstractMvpRelativeLayout;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.gift.ComboGiftPublicScreenMsgSender;
import com.tongdaxing.xchat_core.gift.ComboInfo;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.yuhuankj.tmxq.R;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author weihaitao
 * @date 2019/8/26
 */
public class RoomGiftComboSender extends AbstractMvpRelativeLayout implements IMvpBaseView {

    /**
     * 礼物连击发送按钮，默认倒计时总时长
     */
    public final static long COMBO_SEND_TIME_MAX_INNER = 5000L;
    /**
     * 默认最大进度
     */
    public final static int MAX_PROGRESS = 100;
    private final String TAG = RoomGiftComboSender.class.getSimpleName();
    /**
     * 当前连击ID
     */
    private long comboId = 0;
    private ConcurrentHashMap<Long, ComboInfo> uidComboInfos = new ConcurrentHashMap<>();
    /**
     * 最近一次选中的礼物赠送数量值
     */
    private int lastSendCount = 0;
    /**
     * 连击范围，左界限
     */
    private int comboRangStart = 0;
    /**
     * 连击范围，右界限
     */
    private int comboRangEnd = 1;
    /**
     * 当前连击赠送礼物的ID
     */
    private long comboGiftId = 0;
    /**
     * 当前选中的礼物ID
     */
    private GiftInfo giftInfo = null;
    private List<MicMemberInfo> micAvatar;
    private long targetUid;
    private ValueAnimator animator;
    private CircleProgressBar cpbComboTimer;
    private int multiAndNoAllMicOperaMicMemberCount = 0;
    private boolean isMultiAndNoAllMic = false;
    private OnComboClickListener onComboClickListener;

    public RoomGiftComboSender(Context context) {
        this(context, null);
    }

    public RoomGiftComboSender(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoomGiftComboSender(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public long getComboId() {
        return comboId;
    }

    public int getComboRangStart() {
        return comboRangStart;
    }

    public int getComboRangEnd() {
        return comboRangEnd;
    }

    public int getLastSendCount() {
        return lastSendCount;
    }

    public GiftInfo getGiftInfo() {
        return giftInfo;
    }

    public List<MicMemberInfo> getMicAvatar() {
        if (null == micAvatar) {
            LogUtils.d(TAG, "getMicAvatar-重新初始化micAvatar");
            micAvatar = new ArrayList<>();
        }
        return micAvatar;
    }

    public void setMicAvatar(List<MicMemberInfo> infos) {
        micAvatar = infos;
    }

    public long getTargetUid() {
        return targetUid;
    }

    @Override
    protected int getViewLayoutId() {
        return R.layout.layout_room_gift_combo_sender;
    }

    @Override
    public void initialView(Context context) {
        cpbComboTimer = findViewById(R.id.cpbComboTimer);
    }

    @Override
    protected void initListener() {
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtils.d(TAG, "initListener-->handleMessage isMultiAndNoAllMic:" + isMultiAndNoAllMic);
                if (isMultiAndNoAllMic) {
                    LogUtils.d(TAG, "initListener-->handleMessage 非全麦多人连送，comboRang外部累加");
                } else {
                    LogUtils.d(TAG, "initListener-->handleMessage-->refreshComBoInfo");
                    refreshComBoInfo();
                }

                restartTimeCountProgressAnim();
                if (null != onComboClickListener) {
                    LogUtils.d(TAG, "initListener-->handleMessage-->onComboClicked");
                    onComboClickListener.onComboClicked();
                }
            }
        });
    }

    public ConcurrentHashMap<Long, ComboInfo> getUidComboInfos() {
        if (null == uidComboInfos) {
            uidComboInfos = new ConcurrentHashMap<>();
        }
        return uidComboInfos;
    }

    //全麦、单人,初始化操作
    public void refreshComboSenderStatus(GiftInfo giftInfo, List<MicMemberInfo> micAvatarList, long targetUid, int giftNum) {
        this.lastSendCount = giftNum;
        if (null == this.micAvatar) {
            LogUtils.d(TAG, "refreshComboSenderStatus-重新初始化micAvatar");
            this.micAvatar = new ArrayList<>();
        }
        this.micAvatar = micAvatarList;
        this.targetUid = targetUid;
        this.giftInfo = giftInfo;
        this.comboId = 0L;
        isMultiAndNoAllMic = false;
        multiAndNoAllMicOperaMicMemberCount = 0;
        LogUtils.d(TAG, "refreshComboSenderStatus-重新初始化uidComboInfos");
        uidComboInfos.clear();
        LogUtils.d(TAG, "refreshComboSenderStatus-->refreshComBoInfo lastSendCount:" + lastSendCount);
        refreshComBoInfo();
        setVisibility(VISIBLE);
    }

    public void initComBoInfoForMultiNoAllMic(GiftInfo giftInfo, int giftNum) {
        this.lastSendCount = giftNum;
        this.giftInfo = giftInfo;
        isMultiAndNoAllMic = true;
        this.targetUid = 0;
        comboGiftId = giftInfo.getGiftId();
        LogUtils.d(TAG, "initComBoInfoForMultiNoAllMic-重新初始化uidComboInfos");
    }

    public ComboInfo genMultiNoAllMicComboId(long uid, int giftNum) {
        if (null == uidComboInfos) {
            LogUtils.d(TAG, "genMultiNoAllMicComboId-重新初始化uidComboInfos");
            uidComboInfos = new ConcurrentHashMap<>();
        }
        LogUtils.d(TAG, "genMultiNoAllMicComboId uid:" + uid + " giftNum:" + giftNum + " uidComboInfos.size:" + uidComboInfos.size());
        ComboInfo info = null;
        if (uidComboInfos.containsKey(uid)) {
            LogUtils.d(TAG, "genMultiNoAllMicComboId 更新缓存");
            info = uidComboInfos.get(uid);
            info.setComboRangStart(info.getComboRangEnd());
            info.setComboRangEnd(info.getComboRangEnd() + giftNum);
        } else {
            LogUtils.d(TAG, "genMultiNoAllMicComboId  插入缓存");
            info = new ComboInfo(0, giftNum, System.currentTimeMillis());
            uidComboInfos.put(uid, info);
        }
        return info;
    }

    //非全麦多人
    public int addMicMemeberInfo(MicMemberInfo micMemberInfo) {
        if (null == this.micAvatar) {
            this.micAvatar = new ArrayList<>();
        }
        //若已经包含，则不更新micAvatar，可以避免连击过程中的多个同步操作
        // 首次发送会先clear再put，可以避免同步操作的问题
        if (!micAvatar.contains(micMemberInfo)) {
            this.micAvatar.add(micMemberInfo);
        }
        LogUtils.d(TAG, "addMicMemeberInfo lastSendCount:" + lastSendCount + " micAvatar.size:" + micAvatar.size());
        return micAvatar.size();
    }

    //单人、全麦
    public void refreshComBoInfo() {
        //连送按钮显示的时候，
        // 1是刚赠送礼物的时候，那么需要赠送之前就更新comboId和comboGiftId，
        // 2是连续点击的时候，产品需求是连击按钮不可同礼物列表同时出现，因此可保证comboGiftId的唯一
        if (null == giftInfo) {
            setVisibility(GONE);
            return;
        }

        if (comboId == 0 || giftInfo.getGiftId() != comboGiftId) {
            comboGiftId = giftInfo.getGiftId();
            comboId = System.currentTimeMillis();
            this.comboRangStart = 0;
            this.comboRangEnd = lastSendCount;
        } else {
            comboRangStart = comboRangEnd;
            comboRangEnd += lastSendCount;
        }

        LogUtils.d(TAG, "refreshComBoInfo comboId:" + comboId + " comboGiftId:" + comboGiftId + " comboRangStart:" + comboRangStart + " comboRangEnd:" + comboRangEnd);
    }

    /**
     * 重新开始礼物连送点击动画
     */
    public void restartTimeCountProgressAnim() {
        setVisibility(View.VISIBLE);
        if (null != animator && animator.isRunning()) {
            animator.removeAllUpdateListeners();
            animator.cancel();
        }
        LogUtils.d(TAG, "restartTimeCountProgressAnim curComboProgress:" + 0);
        cpbComboTimer.setProgress(0);
        animator = ValueAnimator.ofInt(0, 100);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override

            public void onAnimationUpdate(ValueAnimator animation) {
                int progress = (int) animation.getAnimatedValue();
//                LogUtils.d(TAG, "initListener-->handleMessage progress:" + progress);
                cpbComboTimer.setProgress(progress);
                //0--100
                if (progress >= MAX_PROGRESS) {
                    clear();
                    setVisibility(GONE);
                    LogUtils.d(TAG, "initListener-->handleMessage-->sendComboGiftScreenMsg");
                    ComboGiftPublicScreenMsgSender.getInstance().sendComboGiftScreenMsg();
                }
            }
        });
        animator.setRepeatCount(0);
        animator.setDuration(COMBO_SEND_TIME_MAX_INNER);
        animator.start();
    }

    @Override
    public void initViewState() {
        cpbComboTimer.setProgress(0);
        cpbComboTimer.setMax(MAX_PROGRESS);
        LogUtils.d(TAG, "initViewState curComboProgress:" + 0);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        LogUtils.d(TAG, "onDetachedFromWindow-->clear");
        clear();
    }

    public boolean isMultiAndNoAllMic() {
        return isMultiAndNoAllMic;
    }

    public void clear() {
        LogUtils.d(TAG, "clear");
        if (null != animator) {
            animator.removeAllUpdateListeners();
            animator.cancel();
        }
        if (null != micAvatar) {
            micAvatar.clear();
        }
        if (null != uidComboInfos) {
            uidComboInfos.clear();
        }
        comboId = 0;
        comboGiftId = 0;
        comboRangStart = 0;
        comboRangEnd = 0;
        giftInfo = null;
        micAvatar = null;
        targetUid = 0;
        lastSendCount = 0;
        multiAndNoAllMicOperaMicMemberCount = 0;
    }

    public void setOnComboClickListener(OnComboClickListener listener) {
        this.onComboClickListener = listener;
    }

    public interface OnComboClickListener {

        /**
         * 通知外部，发送连击礼物消息
         */
        void onComboClicked();

    }
}
