package com.yuhuankj.tmxq.ui.liveroom.imroom.room.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMember;

import java.util.List;

/**
 * <p> </p>
 *
 * @author jiahui
 * @date 2017/12/19
 */
public interface IRoomBlackListView extends IMvpBaseView {

    void showRoomBlackList(List<IMRoomMember> imRoomMembers);

    void removeUserFromBlackList(String account);

    void showErrMsg(String message);

    void showListEmptyView();
}
