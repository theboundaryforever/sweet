package com.yuhuankj.tmxq.ui.liveroom.redpacket;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.room_red_packet.RedPacketModel;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liaoxy
 * @Description:红包记录页面
 * @date 2019/1/17 11:26
 */
public class RedPacketHistoryActivity extends BaseActivity {
    private List<RedPacketHistoryEnitity> datas;
    private RecyclerView rcvRedPacket;
    private BaseQuickAdapter adapter;
    private RelativeLayout rlNoData;
    private int page = Constants.PAGE_START;//当前获取第几页数据
    private boolean isReqing = false;//是否正在请求

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_red_packet_history);
        initTitleBar("记录");
        initView();
        initData();
    }

    private void initView() {
        rcvRedPacket = (RecyclerView) findViewById(R.id.rcvRedPacket);
        rlNoData = (RelativeLayout) findViewById(R.id.rlNoData);
    }

    private void initData() {
        LinearLayoutManager layoutmanager = new RecyclerViewNoBugLinearLayoutManager(this);
        layoutmanager.setOrientation(LinearLayoutManager.VERTICAL);
        rcvRedPacket.setLayoutManager(layoutmanager);
        datas = new ArrayList<>();

        adapter = new RedPacketHistoryAdapter(R.layout.item_red_packet_history, datas);
        rcvRedPacket.setAdapter(adapter);
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                RedPacketHistoryEnitity enitity = datas.get(position);
                Intent intent = new Intent(RedPacketHistoryActivity.this, RedPacketDetailActivity.class);
                intent.putExtra("redPacketId", enitity.getRedPacketId());
                startActivity(intent);
            }
        });

        rcvRedPacket.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                //静止没有滚动
                //public static final int SCROLL_STATE_IDLE = 0;
                //正在被外部拖拽,一般为用户正在用手指滚动
                //public static final int SCROLL_STATE_DRAGGING = 1;
                //自动滚动
                //public static final int SCROLL_STATE_SETTLING = 2;

                //只有滚动条静止的时候才刷新
                if (newState == 0) {
                    if (!rcvRedPacket.canScrollVertically(1) && datas.size() > 5) {
                        //滑动到底部
                        getData();
                    }
                }
            }
        });
        getData();
    }

    private void getData() {
        if (isReqing) {
            return;
        }
        isReqing = true;
        DialogManager dialogManager = new DialogManager(this);
        dialogManager.showProgressDialog(this, "请稍后...");
        new RedPacketModel().getRedPacketHistory(page, 100, new OkHttpManager.MyCallBack<ServiceResult<List<RedPacketHistoryEnitity>>>() {
            @Override
            public void onError(Exception e) {
                dialogManager.dismissDialog();
                SingleToastUtil.showToast(e.getMessage());
                isReqing = false;
            }

            @Override
            public void onResponse(ServiceResult<List<RedPacketHistoryEnitity>> response) {
                dialogManager.dismissDialog();
                if (response.isSuccess()) {
                    List<RedPacketHistoryEnitity> temps = response.getData();
                    if (temps != null && temps.size() > 0) {
                        if (page == Constants.PAGE_START) {
                            datas.clear();
                        }
                        datas.addAll(temps);
                        adapter.notifyDataSetChanged();
                        page++;
                    } else {
                        if (datas.size() == 0) {
                            rlNoData.setVisibility(View.VISIBLE);
                        } else {
                            SingleToastUtil.showToast("数据已全部加载完成");
                        }
                    }
                } else {
                    SingleToastUtil.showToast(RedPacketHistoryActivity.this, response.getMessage());
                }
                isReqing = false;
            }
        });
    }
}
