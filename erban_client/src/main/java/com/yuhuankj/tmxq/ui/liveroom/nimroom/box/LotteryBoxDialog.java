package com.yuhuankj.tmxq.ui.liveroom.nimroom.box;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.JavaUtil;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.pay.IPayCore;
import com.tongdaxing.xchat_core.pay.IPayCoreClient;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.lotterybox.ILotteryBoxCore;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.me.charge.ChargeActivity;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Administrator on 2018/4/11.
 */

public class LotteryBoxDialog extends AppCompatDialogFragment implements View.OnClickListener {

    @BindView(R.id.iv_lottery_box_box)
    ImageView ivLotteryBoxBox;
    @BindView(R.id.iv_lottery_box_gift)
    ImageView ivLotteryBoxGift;
    @BindView(R.id.tv_lottery_box_title)
    TextView tvLotteryBoxTitle;
    @BindView(R.id.bu_lottery_box_one)
    Button buLotteryBoxOne;
    @BindView(R.id.bu_lottery_box_ten)
    Button buLotteryBoxTen;
    @BindView(R.id.ll_lottery_button)
    LinearLayout llLotteryButton;
    @BindView(R.id.tv_gold_count)
    TextView tvGoldCount;
    @BindView(R.id.tv_lottery_box_recharge)
    TextView tvLotteryBoxRecharge;
    Unbinder unbinder;
    @BindView(R.id.tv_lottery_dialog_title)
    TextView tvLotteryDialogTitle;
    @BindView(R.id.rv_lottery_dialog)
    RecyclerView rvLotteryDialog;
    @BindView(R.id.bu_lottery_dialog_ok)
    Button buLotteryDialogOk;
    @BindView(R.id.rl_lottery_ten_time_dialog)
    RelativeLayout rlLotteryTenTimeDialog;
    @BindView(R.id.iv_lottery_rule)
    ImageView ivLotteryRule;
    @BindView(R.id.tv_lottery_rule)
    TextView tvLotteryRule;
    private ILotteryBoxCore iLotteryBoxCore;
    private LotteryGiftAdapter lotteryGiftAdapter;

    private double EVERY_TIME_COUNT = 20;
    private double goldNum;
    private final String TAG = LotteryBoxDialog.class.getSimpleName();

    public LotteryBoxDialog() {

    }

    /**
     * 根据：
     * http://blog.sina.com.cn/s/blog_5da93c8f0102x5zl.html
     * https://blog.csdn.net/freelander_j/article/details/52925745
     * 修复IllegalStateException: Can not perform this action after onSaveInstanceState with DialogFragment
     *
     * @param fragmentManager
     * @param tag
     * @return
     */
    @Override
    public void show(FragmentManager fragmentManager, String tag) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(this, tag).addToBackStack(null);
        transaction.commitAllowingStateLoss();
    }

    public static LotteryBoxDialog newInstance() {
        LotteryBoxDialog lotteryBoxDialog = new LotteryBoxDialog();
        return lotteryBoxDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CoreManager.addClient(this);
        isDestroy = false;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        iLotteryBoxCore = CoreManager.getCore(ILotteryBoxCore.class);
        Window window = getDialog().getWindow();
        View view = inflater.inflate(R.layout.dialog_lotter_box, window.findViewById(android.R.id.content), false);
        unbinder = ButterKnife.bind(this, view);
        setCancelable(true);

        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        rvLotteryDialog.setLayoutManager(new GridLayoutManager(getContext(), 3));
        lotteryGiftAdapter = new LotteryGiftAdapter();
        rvLotteryDialog.setAdapter(lotteryGiftAdapter);

        ininEvent();
        rlLotteryTenTimeDialog.setVisibility(View.GONE);

        WalletInfo walletInfo = CoreManager.getCore(IPayCore.class).getCurrentWalletInfo();
        if (walletInfo != null) {
            goldNum = walletInfo.getGoldNum();
            tvGoldCount.setText(getContext().getString(R.string.gold_num_text, goldNum));
        }

        //初始化头奖名称
        Json configData = CoreManager.getCore(VersionsCore.class).getConfigData();
        String lotteryBoxBigGift = configData.str("lotteryBoxBigGift");
        lotteryBoxBigGift = null == lotteryBoxBigGift ? "" : lotteryBoxBigGift;
        if (!TextUtils.isEmpty(lotteryBoxBigGift)) {
            tvLotteryBoxTitle.setText(getResources().getString(R.string.room_lottery_box_tips_1, lotteryBoxBigGift));
            tvLotteryRule.setText(getResources().getString(R.string.room_lottery_box_tips_2, lotteryBoxBigGift));
        }
        CoreManager.getCore(IPayCore.class).updateMyselfWalletInfo();
        return view;
    }

    private void showDialogStyle(boolean isRule) {
        if (null == tvLotteryDialogTitle || rvLotteryDialog == null
                || null == tvLotteryRule || null == buLotteryDialogOk) {
            return;
        }
        if (isRule) {
            tvLotteryDialogTitle.setText(getResources().getString(R.string.room_lottery_box_rule_title));
            rvLotteryDialog.setVisibility(View.GONE);
            tvLotteryRule.setVisibility(View.VISIBLE);
            buLotteryDialogOk.setText(getResources().getString(R.string.room_lottery_box_rule_close));
        } else {
            tvLotteryDialogTitle.setText(getResources().getString(R.string.room_lottery_box_tips_3));
            rvLotteryDialog.setVisibility(View.VISIBLE);
            tvLotteryRule.setVisibility(View.GONE);
            buLotteryDialogOk.setText(getResources().getString(R.string.room_lottery_box_rule_sure));
        }
    }


    OkHttpManager.MyCallBack<Json> jsonMyCallBack = new OkHttpManager.MyCallBack<Json>() {
        @Override
        public void onError(Exception e) {
            if (isDestroy){
                return;
            }
            e.printStackTrace();
            try {
                onLOtteryError(getResources().getString(R.string.network_error));
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

        @Override
        public void onResponse(Json response) {
            if (isDestroy){
                return;
            }
            if (response.num("code") == 200) {
                onLotterySuccess(response.str("data"));
                return;
            } else if (response.num("code") == 2103) {
                lotteryLoading = true;
                SingleToastUtil.showToast(response.str("message"));
                return;
            }
            onLOtteryError(response.str("message"));
        }
    };

    private void ininEvent() {
        buLotteryBoxOne.setOnClickListener(this);
        buLotteryBoxTen.setOnClickListener(this);
        buLotteryDialogOk.setOnClickListener(this);
        tvLotteryBoxRecharge.setOnClickListener(this);
        ivLotteryRule.setOnClickListener(this);
    }

    private boolean isDestroy = false;
    @Override
    public void onDestroyView() {
        isDestroy = true;
        super.onDestroyView();
        if (jsonMyCallBack != null){
            jsonMyCallBack = null;
        }
        CoreManager.removeClient(this);
        if (null != rlLotteryTenTimeDialog) {
//            rlLotteryTenTimeDialog.getAnimation().cancel();
            rlLotteryTenTimeDialog.clearAnimation();
        }
        if (null != ivLotteryBoxGift) {
            ivLotteryBoxGift.clearAnimation();
        }
        handler.removeCallbacksAndMessages(null);
        handler = null;
        //修复recyclerview的mObserver的泄露
        if (rvLotteryDialog != null) {
            try {
                Field field = rvLotteryDialog.getClass().getDeclaredField("mObserver");
                field.setAccessible(true); // 设置些属性是可以访问的
                Object val = field.get(rvLotteryDialog); // 得到此属性的值
                if (lotteryGiftAdapter != null) {
                    lotteryGiftAdapter.unregisterAdapterDataObserver((RecyclerView.AdapterDataObserver) val);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        unbinder.unbind();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bu_lottery_box_one:
                //房间-宝箱1次
                StatisticManager.get().onEvent(getActivity(),
                        StatisticModel.EVENT_ID_ROOM_BOX_LUCK_1,
                        StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));

                if (lotteryLoading) {
                    SingleToastUtil.showToast(getResources().getString(R.string.room_lottery_box_tips_4));
                    return;
                }
                lotteryLoading = true;
                if (iLotteryBoxCore !=null) {
                    iLotteryBoxCore.lotteryRequest("1", jsonMyCallBack);
                }
                break;
            case R.id.bu_lottery_box_ten:
                //房间-宝箱10次
                StatisticManager.get().onEvent(getActivity(),
                        StatisticModel.EVENT_ID_ROOM_BOX_LUCK_10,
                        StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
                if (lotteryLoading) {
                    SingleToastUtil.showToast(getResources().getString(R.string.room_lottery_box_tips_4));
                    return;
                }
                lotteryLoading = true;
                if (iLotteryBoxCore !=null) {
                    iLotteryBoxCore.lotteryRequest("2", jsonMyCallBack);
                }
                break;

            case R.id.bu_lottery_dialog_ok:
                rlLotteryTenTimeDialog.setVisibility(View.GONE);
                ivLotteryBoxBox.setImageResource(R.mipmap.lottery_baoxiang_moren);
                lotteryLoading = false;
                break;

            case R.id.tv_lottery_box_recharge:
                ChargeActivity.start(getActivity());
                lotteryLoading = false;
                //房间-点击宝箱充值入口
                StatisticManager.get().onEvent(getActivity(),
                        StatisticModel.EVENT_ID_ROOM_BOX_RECHARGE,
                        StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
                break;
            case R.id.iv_lottery_rule:
                showDialogStyle(true);
                if (null != rlLotteryTenTimeDialog) {
                    rlLotteryTenTimeDialog.setVisibility(View.VISIBLE);
                }
                //房间-宝箱规则
                StatisticManager.get().onEvent(getActivity(),
                        StatisticModel.EVENT_ID_ROOM_BOX_RULE,
                        StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
                break;
            default:

        }
    }


    public void onLotterySuccess(String json) {
        //dialog销毁之后不执行方法
        if (getContext() == null) {
            return;
        }
        Json parse = Json.parse(json);
        String[] keyNames = parse.key_names();
        if (keyNames.length < 1) {
            lotteryLoading = false;
            return;
        }
        if (keyNames.length == 1 && parse.num(keyNames[0]) == 1) {
            goldNum -= EVERY_TIME_COUNT;
            CoreManager.getCore(IPayCore.class).minusGold(20);
            //解决https://bugly.qq.com/v2/crash-reporting/crashes/23dbf7aab1/40682?pid=1
            if (null != tvGoldCount) {
                tvGoldCount.setText(getContext().getString(R.string.gold_num_text, goldNum));
                showFreeGiftAnim(keyNames[0]);
            }
        } else if (keyNames.length > 1 || parse.num(keyNames[0]) != 1) {
            goldNum -= EVERY_TIME_COUNT * 10;
            CoreManager.getCore(IPayCore.class).minusGold(200);
            if (null != tvGoldCount) {
                tvGoldCount.setText(getContext().getString(R.string.gold_num_text, goldNum));
                showFreeGiftDialog(parse);
            }
        }
        setLotteryGiftInfo(parse);
    }

    private void setLotteryGiftInfo(Json json) {
        LogUtils.d(TAG, "setLotteryGiftInfo json:" + json);
        if (json == null || json.key_names().length < 1) {
            return;
        }

        GiftInfo markGift = new GiftInfo();
        int markCount = 0;

        List<GiftInfo> giftInfos = new ArrayList<>();
        //把gift的索引标记起来,为了提升连续抽奖时索引查找的性能
        Map<String, Integer> map = new HashMap<>();
        giftInfos = CoreManager.getCore(IGiftCore.class).getLastRequestGiftInfos();
        for (int i = 0; i < giftInfos.size(); i++) {
            GiftInfo giftInfo = giftInfos.get(i);
            map.put(giftInfo.getGiftId() + "", i);
        }

        //把获得的礼物加进集合
        String[] keyNames = json.key_names();
        for (int i = 0; i < keyNames.length; i++) {
            String lotteryGiftID = keyNames[i];
            int index = map.get(lotteryGiftID);
            GiftInfo giftInfo = giftInfos.get(index);
            if (giftInfo.getGoldPrice() >= 520 && giftInfo.getGoldPrice() > markGift.getGoldPrice()) {
                markGift = giftInfo;
                markCount = json.num(lotteryGiftID, 0);
            }

            int userGiftPurseNum = giftInfo.getUserGiftPurseNum() + json.num(lotteryGiftID, 0);
            LogUtils.d(TAG, "setLotteryGiftInfo userGiftPurseNum:" + userGiftPurseNum + " giftName:" + giftInfo.getGiftName());
            giftInfo.setUserGiftPurseNum(userGiftPurseNum);
        }

        LogUtils.d(TAG, "setLotteryGiftInfo goldPrice:" + markGift.getGoldPrice() + " markCount:" + markCount);
        if (markGift.getGoldPrice() >= 520 && markCount > 0) {
            RoomInfo cRoomInfo = RoomServiceScheduler.getCurrentRoomInfo();
            if (null != cRoomInfo) {
                if (cRoomInfo.getType() == RoomInfo.ROOMTYPE_HOME_PARTY) {
                    CoreManager.getCore(IGiftCore.class).sendLotteryMeg(markGift, markCount);
                } else {
                    IMRoomMessageManager.get().sendLotteryBoxMsg(markGift, markCount, null);
                }
            }

        }
    }


    private boolean lotteryLoading = false;

    private void showFreeGiftDialog(Json json) {
        showDialogStyle(false);
        IGiftCore core = CoreManager.getCore(IGiftCore.class);
        List<Json> jsons = new ArrayList<>();
        String[] keyNames = json.key_names();
        for (int i = 0; i < keyNames.length; i++) {
            if (i > 5) {
                break;
            }
            int giftId = JavaUtil.str2int(keyNames[i]);
            if (giftId == 0) {
                continue;
            }
            Json j = new Json();
            GiftInfo giftInfo = core.findGiftInfoById(giftId);
            if (null == giftInfo) {
                continue;
            }
            j.set("icon", giftInfo.getGiftUrl());
            j.set("info", "x" + json.str(keyNames[i]));
            jsons.add(j);
        }
        if (jsons.size() > 0 && null != lotteryGiftAdapter) {
            lotteryGiftAdapter.setNewData(jsons);
        }
        if (null != handler) {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (null != ivLotteryBoxBox) {
                        ivLotteryBoxBox.setImageResource(R.mipmap.lottery_baoxiang_dakai);
                    }
                }
            }, 100);

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (null != rlLotteryTenTimeDialog) {
                        ObjectAnimator alpha = ObjectAnimator.ofFloat(rlLotteryTenTimeDialog, "alpha", 0f, 1f);
                        ObjectAnimator scaleX = ObjectAnimator.ofFloat(rlLotteryTenTimeDialog, "scaleX", 0f, 1f);
                        ObjectAnimator scaleY = ObjectAnimator.ofFloat(rlLotteryTenTimeDialog, "scaleY", 0f, 1f);
                        AnimatorSet animatorSet = new AnimatorSet();  //组合动画
                        animatorSet.playTogether(alpha, scaleX, scaleY); //设置动画
                        animatorSet.setDuration(500);  //设置动画时间
                        animatorSet.start(); //启动
                        rlLotteryTenTimeDialog.setVisibility(View.VISIBLE);
                    }
                }
            }, 100);
        }
    }

    Handler handler = new Handler();

    private void showFreeGiftAnim(String giftId) {
        if (null == ivLotteryBoxGift) {
            return;
        }
        ivLotteryBoxGift.setVisibility(View.GONE);
        Integer str2int = JavaUtil.str2int(giftId);
        if (str2int == 0) {
            lotteryLoading = false;
            return;
        }

        GiftInfo giftInfo = CoreManager.getCore(IGiftCore.class).findGiftInfoById(str2int);
        if (getContext() != null) {
            ImageLoadUtils.loadImage(getContext(), giftInfo.getGiftUrl(), ivLotteryBoxGift);
        }
        if (null != handler) {
            //打开宝箱
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (null != ivLotteryBoxBox) {
                        ivLotteryBoxBox.setImageResource(R.mipmap.lottery_baoxiang_dakai);
                    }
                }
            }, 100);
            //礼物特效
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (null != ivLotteryBoxGift) {
                        ivLotteryBoxGift.setVisibility(View.VISIBLE);
                        ObjectAnimator alpha = ObjectAnimator.ofFloat(ivLotteryBoxGift, "alpha", 0f, 1f);
                        ObjectAnimator scaleX = ObjectAnimator.ofFloat(ivLotteryBoxGift, "scaleX", 0, 1f);
                        ObjectAnimator scaleY = ObjectAnimator.ofFloat(ivLotteryBoxGift, "scaleY", 0, 1f);
                        AnimatorSet animatorSet = new AnimatorSet();  //组合动画
                        animatorSet.playTogether(alpha, scaleX, scaleY); //设置动画
                        animatorSet.setDuration(800);  //设置动画时间
                        animatorSet.start(); //启动
                    }

                }
            }, 100);
            //礼物消失
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (null != ivLotteryBoxGift) {
                        ObjectAnimator alpha = ObjectAnimator.ofFloat(ivLotteryBoxGift, "alpha", 1f, 0f);
                        alpha.setDuration(1000);
                        alpha.start();
                    }
                }
            }, 3000);
            //恢复
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    lotteryLoading = false;
                    if (null != ivLotteryBoxGift) {
                        ivLotteryBoxBox.setImageResource(R.mipmap.lottery_baoxiang_moren);
                        ivLotteryBoxGift.setVisibility(View.GONE);
                    }
                }
            }, 3500);
        }
    }


    public void onLOtteryError(String msg) {
        lotteryLoading = false;
        SingleToastUtil.showToast(msg);
    }

    @CoreEvent(coreClientClass = IPayCoreClient.class)
    public void onWalletInfoUpdate(WalletInfo walletInfo) {
        if (walletInfo != null && null != tvGoldCount) {
            goldNum = walletInfo.getGoldNum();
            tvGoldCount.setText(getContext().getString(R.string.gold_num_text, goldNum));
        }
    }

}
