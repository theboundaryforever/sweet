package com.yuhuankj.tmxq.ui.home.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.initial.InitInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.ui.signAward.model.RecommMicUserInfo;
import com.yuhuankj.tmxq.ui.signAward.model.SignInAwardInfo;
import com.yuhuankj.tmxq.ui.signAward.model.SignRecvGiftPkgInfo;

import java.util.List;
import java.util.Map;

/**
 * <p>  </p>
 *
 * @author jiahui
 * @date 2017/12/12
 */
public interface IMainView extends IMvpBaseView {

    /**
     * 退出房间
     */
    void onRoomExited();

    /**
     * -
     *
     * @param data -
     */
    void onInitSuccess(InitInfo data);

    void onGetRecommAd(String data);

    /**
     * 创建homepart房间失败
     *
     * @param message 提示信息
     */
    void notifyHomePartRoomCreateFailed(String message);

    /**
     * 进入轰趴房间需要实名认证
     */
    void notifyHomePartRoomNeedRealNameAuth(long queryUid, int roomType, RoomInfo roomInfo);

    void enterHomePartRoom(long uid);

    /**
     * 弹萌新大礼包界面，走领取萌新大礼包流程
     *
     * @param getDay
     * @param micRoomUserList
     * @param newsBigGiftMap
     */
    void showNewPkgDialog(int getDay, List<RecommMicUserInfo> micRoomUserList,
                          Map<Integer, List<SignRecvGiftPkgInfo>> newsBigGiftMap);


    /**
     * 弹连续签到奖励弹框，走连续签到奖励流程
     *
     * @param signDay
     * @param signInAwardInfos
     */
    void showSignRecvDialog(int signDay, List<SignInAwardInfo> signInAwardInfos);

}
