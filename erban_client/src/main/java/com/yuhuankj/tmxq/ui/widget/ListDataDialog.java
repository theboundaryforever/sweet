package com.yuhuankj.tmxq.ui.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.fragment.OnlineUserFragment;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.fragment.RoomContributeFragment;

/**
 * Created by MadisonRong on 13/01/2018.
 */

public class ListDataDialog extends AppCompatDialogFragment implements View.OnClickListener {

    public static final String TYPE_ONLINE_USER = "ONLINE_USER";
    public static final String TYPE_CONTRIBUTION = "ROOM_CONTRIBUTION";
    public static final String TYPE_MIC_USER = "ROOM_MIC_USER";
    public static final String KEY_TITLE = "KEY_TITLE";
    public static final String KEY_TYPE = "KEY_TYPE";
    public static final String KEY_ISMIC = "isMic";

    private String title;
    private String type;
    private OnlineUserFragment.OnlineItemClick onlineItemClick;

    private FragmentTransaction fragmentTransaction;
    private OnlineUserFragment micUserFragment;
    private OnlineUserFragment onlineUserFragment;
    private RoomContributeFragment roomContributeFragment;

    public void setOnlineItemClick(OnlineUserFragment.OnlineItemClick onlineItemClick) {
        this.onlineItemClick = onlineItemClick;
    }


    public static ListDataDialog newOnlineUserListInstance(Context context) {
        return newInstance(context.getString(R.string.online_user_text), TYPE_ONLINE_USER);
    }

    public static ListDataDialog newContributionListInstance(Context context) {
        return newInstance(context.getString(R.string.contribution_list_text), TYPE_CONTRIBUTION);
    }

    public static ListDataDialog newMicUserInstance(Context context) {
        return newInstance(context.getString(R.string.mic_user_list_text), TYPE_MIC_USER);
    }


    public static ListDataDialog newInstance(String title, String type) {
        ListDataDialog listDataDialog = new ListDataDialog();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_TITLE, title);
        bundle.putString(KEY_TYPE, type);
        listDataDialog.setArguments(bundle);
        return listDataDialog;
    }

    public ListDataDialog() {
    }

    @Override
    public void show(FragmentManager fragmentManager, String tag) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(this, tag).addToBackStack(null);
        transaction.commitAllowingStateLoss();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            String titleArg = arguments.getString(KEY_TITLE);
            this.title = titleArg != null ? titleArg : "";
            String typeArg = arguments.getString(KEY_TYPE);
            this.type = typeArg != null ? typeArg : "";
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        // setup window and width
        View view = inflater.inflate(R.layout.dialog_list_data, window.findViewById(android.R.id.content), false);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        setCancelable(true);
        TextView titleTextView = view.findViewById(R.id.tv_list_data_title);
        titleTextView.setText(this.title);
        view.findViewById(R.id.iv_close_dialog).setOnClickListener(this);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!getActivity().isFinishing()) {

            switch (this.type) {
                case TYPE_ONLINE_USER:
                    if (null != fragmentTransaction && null != onlineUserFragment && onlineUserFragment.isVisible()) {
                        fragmentTransaction.hide(onlineUserFragment);
                    }

                    //参考：https://stackoverflow.com/questions/24561874/android-fragmenttransaction-commit-already-called
                    //解决因使用同一个fragmentTransaction多次commit抛java.lang.IllegalStateException: commit already called
                    fragmentTransaction = getChildFragmentManager().beginTransaction();

                    if (null == onlineUserFragment) {
                        onlineUserFragment = new OnlineUserFragment();
                        fragmentTransaction.add(R.id.fl_container, onlineUserFragment, OnlineUserFragment.class.getSimpleName());
                    }

                    if (onlineItemClick != null) {
                        onlineUserFragment.setOnlineItemClick(onlineItemClick);
                    }
                    fragmentTransaction.show(onlineUserFragment).commitAllowingStateLoss();
                    break;

                case TYPE_MIC_USER: {
                    if (null != fragmentTransaction && null != micUserFragment && micUserFragment.isVisible()) {
                        fragmentTransaction.hide(micUserFragment);
                    }

                    fragmentTransaction = getChildFragmentManager().beginTransaction();

                    if (null == micUserFragment) {
                        micUserFragment = new OnlineUserFragment();
                        Bundle bundle = new Bundle();
                        bundle.putBoolean(KEY_ISMIC, true);
                        micUserFragment.setArguments(bundle);
                        fragmentTransaction.add(R.id.fl_container, micUserFragment, OnlineUserFragment.class.getSimpleName());
                    }

                    if (onlineItemClick != null) {
                        micUserFragment.setOnlineItemClick(onlineItemClick);
                    }
                    fragmentTransaction.show(micUserFragment).commitAllowingStateLoss();
                }
                break;
                case TYPE_CONTRIBUTION:
                    if (null != fragmentTransaction && null != roomContributeFragment && roomContributeFragment.isVisible()) {
                        fragmentTransaction.hide(roomContributeFragment);
                    }

                    fragmentTransaction = getChildFragmentManager().beginTransaction();

                    if (null == roomContributeFragment) {
                        roomContributeFragment = new RoomContributeFragment();
                        fragmentTransaction.add(R.id.fl_container, roomContributeFragment, RoomContributeFragment.class.getSimpleName());
                    }

                    fragmentTransaction.show(roomContributeFragment).commitAllowingStateLoss();
                    break;
                default:
                    break;
            }
        }
    }

    public void show(FragmentManager fragmentManager) {
        show(fragmentManager, this.type);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (null != micUserFragment) {
            micUserFragment.setOnlineItemClick(null);
            if (micUserFragment.isVisible() && null != fragmentTransaction) {
                fragmentTransaction.hide(micUserFragment);
            }
            micUserFragment = null;
        }
        if (null != onlineUserFragment) {
            onlineUserFragment.setOnlineItemClick(null);
            if (onlineUserFragment.isVisible() && null != fragmentTransaction) {
                fragmentTransaction.hide(onlineUserFragment);
            }
            onlineUserFragment = null;
        }
        if (null != onlineItemClick) {
            onlineItemClick = null;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_close_dialog:
                try {
                    dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
    }
}
