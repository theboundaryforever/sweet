package com.yuhuankj.tmxq.ui.liveroom.imroom.gift.listener;

import com.tongdaxing.xchat_core.gift.GiftInfo;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/5/16
 */
public interface OnItemClickListener {
    void onItemClick(GiftInfo giftInfo, int position);
}
