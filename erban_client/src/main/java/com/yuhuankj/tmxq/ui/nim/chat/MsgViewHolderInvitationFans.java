package com.yuhuankj.tmxq.ui.nim.chat;

import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.netease.nim.uikit.R;
import com.netease.nim.uikit.common.ui.recyclerview.adapter.BaseMultiItemFetchLoadAdapter;
import com.netease.nim.uikit.glide.GlideApp;
import com.netease.nim.uikit.session.module.IShareFansCoreClient;
import com.netease.nim.uikit.session.viewholder.MsgViewHolderBase;
import com.netease.nim.uikit.utils.BlurTransformation;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.im.custom.bean.ShareFansAttachment;

/**
 * Created by zhoujianghua on 2015/8/4.
 */
public class MsgViewHolderInvitationFans extends MsgViewHolderBase {

    private final String TAG = MsgViewHolderInvitationFans.class.getSimpleName();

    protected TextView bodyTextView;
    private ImageView imageViewIcon;
    private ImageView imageViewBg;
    private View bg;
    private View buJoin;

    public MsgViewHolderInvitationFans(BaseMultiItemFetchLoadAdapter adapter) {
        super(adapter);
    }

    @Override
    protected int getContentResId() {
        return R.layout.nim_message_item_share_fans;
    }

    @Override
    protected void inflateContentView() {
        bodyTextView = findViewById(R.id.tv_title);
        imageViewIcon = findViewById(R.id.iv_user_icon);
        imageViewBg = findViewById(R.id.iv_user_bg);
        bg = findViewById(R.id.rl_bg);
        buJoin = findViewById(R.id.bu_join);
    }

    @Override
    protected void bindContentView() {
        //正确的逻辑逻辑是，以attachment附件data作为数据解析的依据，
        //兼容旧版本
        Json json = null;
        if (StringUtils.isNotEmpty(message.getContent())){
            json = Json.parse(message.getContent());
        }else {
            MsgAttachment msgAttachment = message.getAttachment();
            if (msgAttachment instanceof ShareFansAttachment) {
                ShareFansAttachment shareFansAttachment = (ShareFansAttachment) msgAttachment;
                if (StringUtils.isNotEmpty(shareFansAttachment.getParams())) {
                    json = Json.parse(shareFansAttachment.getParams());
                }
            }
        }
        if (json == null)
            return;
        bodyTextView.setText(json.str("title",""));
//        bodyTextView.setTextColor(isReceivedMessage() ? Color.BLACK : Color.WHITE);
        bodyTextView.setMovementMethod(LinkMovementMethod.getInstance());
        bodyTextView.setOnLongClickListener(longClickListener);
        GlideApp.with(context).load(json.str("avatar","")).into(imageViewIcon);
        GlideApp.with(context)
                .load(json.str("bg",""))
                .transform(new BlurTransformation(context, 23, 4))
                .into(imageViewBg);
        final long uid = json.num_l("uid");
        //3-轰趴房 ,4单人音频房, 具体参考com.tongdaxing.xchat_core.room.bean.RoomInfo
        int roomType = 3;
        if (json.has("type")) {
            roomType = json.num("type");
        }
        //召集令下发的私聊消息， 新增roomId参数，用于上报
        long roomId = 0L;
        if (json.has("roomId")) {
            roomId = json.num_l("roomId");
        }
        final long room_id = roomId;
        final int type = roomType;
        View.OnClickListener l = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CoreManager.notifyClients(IShareFansCoreClient.class, IShareFansCoreClient.onShareFansJoin, room_id, uid, type);
            }
        };
        bg.setOnClickListener(l);
        bodyTextView.setOnClickListener(l);
        buJoin.setOnClickListener(l);
    }


    @Override
    protected int leftBackground() {
        return 0;
    }

    @Override
    protected int rightBackground() {
        return 0;
    }


}
