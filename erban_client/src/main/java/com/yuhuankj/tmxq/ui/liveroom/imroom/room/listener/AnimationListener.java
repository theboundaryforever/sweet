package com.yuhuankj.tmxq.ui.liveroom.imroom.room.listener;

import android.view.animation.Animation;

/**
 * 创建者      Created by dell
 * 创建时间    2019/3/18
 * 描述        AnimationListener默认实现
 *
 * @author dell
 */
public class AnimationListener implements Animation.AnimationListener {

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
