package com.yuhuankj.tmxq.ui.nim.actions;

import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.View;

import com.netease.nim.uikit.session.actions.BaseAction;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.xchat_core.gift.ChargeListener;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.gift.GiftDialog;
import com.yuhuankj.tmxq.ui.me.charge.ChargeActivity;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by chenran on 2017/10/2.
 */

public class GiftAction extends BaseAction implements GiftDialog.OnGiftDialogBtnClickListener, ChargeListener {


    public GiftAction() {
        super(R.drawable.icon_gift_action, R.string.gift_action);
    }

    transient private GiftDialog giftDialog;

    @Override
    public void onClick() {
        isShowingChargeDialog = false;
        if (giftDialog == null && null != getActivity() && !TextUtils.isEmpty(getAccount())) {
            giftDialog = new GiftDialog(getActivity(), Long.valueOf(getAccount()), false);
            giftDialog.setGiftDialogBtnClickListener(this);
            giftDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    giftDialog = null;
                }
            });
        }
        if (null != giftDialog && !giftDialog.isShowing()) {
            giftDialog.show();
        }
    }

    @Override
    public void onRechargeBtnClick() {
        if (null != getActivity()) {
            ChargeActivity.start(getActivity());
        }
    }

    @Override
    public void onSendGiftBtnClick(GiftInfo giftInfo, long uid, int number) {
        CoreManager.getCore(IGiftCore.class).sendPersonalGiftToNIM(giftInfo.getGiftId(), uid, number, giftInfo.getGoldPrice(), new WeakReference<>(getContainer()), this);
    }

    @Override
    public void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, int number) {
    }

    private boolean isShowingChargeDialog;

    @Override
    public void onNeedCharge() {
        if (isShowingChargeDialog) {
            return;
        }
        if (null == getActivity()) {
            return;
        }
        isShowingChargeDialog = true;
        ChargeDialogFragment.instance("余额不足，是否充值", new ChargeDialogFragment.ChargeDialogListener() {
            @Override
            public void onClick(View view, ChargeDialogFragment fragment) {
                if (view.getId() == R.id.btn_cancel) {
                    fragment.dismiss();
                } else if (view.getId() == R.id.btn_ok) {
                    if (null != getActivity()) {
                        ChargeActivity.start(getActivity());
                    }
                    fragment.dismiss();
                }
                isShowingChargeDialog = false;
            }
        }).show(getActivity().getFragmentManager(), "charge");

    }
}