package com.yuhuankj.tmxq.ui.liveroom.imroom.pk.setting;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomQueueInfo;
import com.tongdaxing.xchat_core.pk.bean.PkVoteInfo;

import java.util.List;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/7/8
 */
public interface IRoomPkSettingView extends IMvpBaseView {

    void showPKMemberListView(List<IMRoomQueueInfo> datas);

    void savePKSucView(PkVoteInfo pkVoteInfo);

    void savePkError(String error);
}
