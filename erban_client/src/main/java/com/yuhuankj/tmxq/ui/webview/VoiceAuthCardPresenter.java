package com.yuhuankj.tmxq.ui.webview;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;

import java.util.Map;

/**
 * <p> </p>
 *
 * @author jiahui
 * @date 2017/12/19
 */
public class VoiceAuthCardPresenter extends AbstractMvpPresenter<IVoiceAuthCardView> {

    private final String TAG = VoiceAuthCardPresenter.class.getSimpleName();

    public VoiceAuthCardPresenter() {

    }

    public void exitRoom(CallBack<String> callBack) {
        BaseRoomServiceScheduler.exitRoom(callBack);
    }


    public void updateAuthCardVoiceUrl(String soundUrl) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        //第一页
//        params.put("sound", sound);
        params.put("soundUrl", soundUrl);
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().postRequest(UriProvider.getVoiceAuthCardReportUrl(), params,
                new OkHttpManager.MyCallBack<ServiceResult<VoiceUploadResponse>>() {
                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                        if (null != getMvpView()) {
                            getMvpView().onUpdateAuthCardVoice(false, e.getMessage());
                        }
                    }

                    @Override
                    public void onResponse(ServiceResult<VoiceUploadResponse> serviceResult) {
                        LogUtils.d(TAG, "getRoomOnLineUserList-onResponse-result:" + serviceResult);
                        if (null == getMvpView()) {
                            return;
                        }
                        if (null != serviceResult) {
                            getMvpView().onUpdateAuthCardVoice(serviceResult.isSuccess(), serviceResult.getMessage());
                        } else {
                            getMvpView().onUpdateAuthCardVoice(false, null);
                        }

                    }
                });
    }

}
