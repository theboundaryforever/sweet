package com.yuhuankj.tmxq.ui.liveroom.imroom.pk;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.xchat_core.pk.bean.PkVoteInfo;

/**
 * 文件描述：pk 的 P 层
 *
 * @auther：zwk
 * @data：2019/7/9
 */
public class RoomPKPresenter extends AbstractMvpPresenter<IRoomPKView> {
    private RoomPKModel roomPKModel;

    public RoomPKPresenter() {
        roomPKModel = new RoomPKModel();
    }


    /**
     * 根据房间id获取其房间最新的PK信息
     *
     * @param roomId
     * @param isUpdateVote
     */
    public void getRoomPkInfo(String roomId, boolean isUpdateVote) {
        roomPKModel.getPkInfo(roomId, isUpdateVote, new CallBack<PkVoteInfo>() {
            @Override
            public void onSuccess(PkVoteInfo data) {
                if (getMvpView() != null) {
                    if (isUpdateVote) {
                        getMvpView().updatePKViewFromVoteInfo(data);
                    } else {
                        getMvpView().initPKViewFromVoteInfo(data);
                    }
                }
            }

            @Override
            public void onFail(int code, String error) {
                if (getMvpView() != null) {
                    getMvpView().getPKVoteInfoFail(error);
                }
            }
        });
    }


    /**
     * 给对应用户投票
     *
     * @param voteUid
     */
    public void pkVote(String roomId, String voteUid) {
        roomPKModel.pkVote(roomId, voteUid, new CallBack<PkVoteInfo>() {
            @Override
            public void onSuccess(PkVoteInfo data) {
                if (getMvpView() != null) {
                    getMvpView().pkVoteSuc(data);
                }
            }

            @Override
            public void onFail(int code, String error) {
                if (getMvpView() != null) {
                    getMvpView().pkVoteFail(error);
                }
            }
        });
    }
}
