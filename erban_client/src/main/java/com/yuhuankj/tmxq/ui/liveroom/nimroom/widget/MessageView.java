package com.yuhuankj.tmxq.ui.liveroom.nimroom.widget;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.netease.nim.uikit.glide.GlideApp;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomNotificationAttachment;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomRoomMemberInAttachment;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.msg.constant.NotificationType;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.JavaUtil;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.RichTextUtil;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RoomRedPacketFinish;
import com.tongdaxing.xchat_core.bean.attachmsg.RoomQueueMsgAttachment;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.gift.MultiGiftReceiveInfo;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.FaceAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.GiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.LotteryBoxAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.LoverUpMicAnimAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.MultiGiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.PkCustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomRedPacketFinishAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomRuleAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomTipAttachment;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.pk.bean.PkVoteInfo;
import com.tongdaxing.xchat_core.praise.IPraiseCore;
import com.tongdaxing.xchat_core.room.face.FaceInfo;
import com.tongdaxing.xchat_core.room.face.FaceReceiveInfo;
import com.tongdaxing.xchat_core.room.face.IFaceCore;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.AVRoomActivity;
import com.yuhuankj.tmxq.ui.me.noble.NobleBusinessManager;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;
import com.yuhuankj.tmxq.ui.widget.DividerItemDecoration;
import com.yuhuankj.tmxq.ui.widget.UserInfoDialog;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.utils.StringUtil;
import com.yuhuankj.tmxq.utils.Utils;
import com.yuhuankj.tmxq.widget.LevelView;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.UIUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.disposables.CompositeDisposable;

import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_OPEN_MIC;

/**
 * 直播间消息界面
 *
 * @author chenran
 * @date 2017/7/26
 */
public class MessageView extends FrameLayout implements BaseQuickAdapter.OnItemClickListener {
    private final String TAG = MessageView.class.getSimpleName();
    private RecyclerView messageListView;
    private TextView tvBottomTip;
    private MessageAdapter mMessageAdapter;
    private List<ChatRoomMessage> chatRoomMessages;
    private List<ChatRoomMessage> tempMessages;
    private ScrollSpeedLinearLayoutManger layoutManger;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private boolean needUpdateDataSet = false;
    private BaseQuickAdapter.OnItemClickListener onItemClickListener;

    public MessageView(Context context) {
        this(context, null);
    }

    public MessageView(Context context, AttributeSet attr) {
        this(context, attr, 0);
    }

    public MessageView(Context context, AttributeSet attr, int i) {
        super(context, attr, i);
        init(context);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        LogUtils.d(TAG, "onAttachedToWindow");
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        compositeDisposable.add(IMNetEaseManager.get().getChatRoomMsgFlowable().subscribe(messages -> {
            if (messages.size() == 0) {
                return;
            }

            ChatRoomMessage chatRoomMessage = messages.get(0);
            if (checkNoNeedMsg(chatRoomMessage)) {
                return;
            }
            onCurrentRoomReceiveNewMsg(messages);
        }));

        compositeDisposable.add(IMNetEaseManager.get().getChatRoomEventObservable().subscribe(roomEvent -> {
            if (roomEvent == null || roomEvent.getEvent() != RoomEvent.RECEIVE_MSG) {
                return;
            }

            ChatRoomMessage chatRoomMessage = roomEvent.getChatRoomMessage();
            if (checkNoNeedMsg(chatRoomMessage)) {
                return;
            }

            tempMessages.clear();
            tempMessages.add(chatRoomMessage);
            onCurrentRoomReceiveNewMsg(tempMessages);
        }));
    }

    /**
     * 检查是否是公屏需要的消息，送礼物和谁来了的消息不加入公屏
     *
     * @param chatRoomMessage
     * @return
     */
    private boolean checkNoNeedMsg(ChatRoomMessage chatRoomMessage) {
        if (chatRoomMessage == null) {
            return true;
        }
        if (chatRoomMessage.getMsgType() == MsgTypeEnum.custom) {
            CustomAttachment attachment = (CustomAttachment) chatRoomMessage.getAttachment();
            return attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_ROOM_TIP
                    || attachment.getFirst() == CustomAttachment.CUSTOM_MSG_SUB_PUBLIC_CHAT_ROOM
                    || attachment.getFirst() == CustomAttachment.CUSTOM_MSG_BOSON_FIRST;
        }
        return false;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
            compositeDisposable = null;
        }
    }

    public void clear() {
        if (mMessageAdapter != null) {
            mMessageAdapter.getData().clear();
            mMessageAdapter.notifyDataSetChanged();
        }
    }

    boolean isFirstLoad = true;

    private void initData() {
        List<ChatRoomMessage> messages = IMNetEaseManager.get().messages;
        if (!ListUtils.isListEmpty(messages)) {
            messages = msgFilter(messages);
            chatRoomMessages.addAll(messages);
            mMessageAdapter.setNewData(chatRoomMessages);
            messageListView.scrollToPosition(messages.size() - 1);
        }
    }

    public void onCurrentRoomReceiveNewMsg(List<ChatRoomMessage> messages) {
        if (ListUtils.isListEmpty(messages)) {
            return;
        }
        //过滤掉进房无座驾的消息
        //避免多人同时进房时刷新列表卡顿
        List<ChatRoomMessage> messages1 = new ArrayList<>();
        for (ChatRoomMessage message : messages) {
            if (message.getAttachment() instanceof ChatRoomRoomMemberInAttachment) {
                String carUrl = null;
                try {
                    carUrl = (String) message.getChatRoomMessageExtension().getSenderExtension().get("user_car");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (TextUtils.isEmpty(carUrl)) {
                    continue;
                }
            }
            messages1.add(message);
        }
        if (ListUtils.isListEmpty(messages1)) {
            return;
        }
        if (null == chatRoomMessages) {
            chatRoomMessages = new ArrayList<>();
        }
        if (chatRoomMessages.size() == 0) {
            mMessageAdapter.setNewData(chatRoomMessages);
        }
        showTipsOrScrollToBottom(messages);
    }

    private List<ChatRoomMessage> msgFilter(List<ChatRoomMessage> chatRoomMessages) {
        List<ChatRoomMessage> messages = new ArrayList<>();
        for (ChatRoomMessage message : chatRoomMessages) {
            if (message.getAttachment() instanceof CustomAttachment) {
                CustomAttachment customAttachment = (CustomAttachment) message.getAttachment();
                if (customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_SUB_PUBLIC_CHAT_ROOM
                        || customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_VOTE
                        || customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_VOTE_NEW
                        || customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_INVITE
                        || customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_BOSON_FIRST) {
                    continue;
                }
            }

            //拦截其他房间的信息
            if (IMNetEaseManager.get().filterOtherRoomMsg(message)) {
                LogUtils.d(TAG, "msgFilter - 其他房间信息_被拦截，roomId = " + message.getSessionId());
                continue;
            }
            messages.add(message);
        }
        return messages;
    }

    //    private void showTipsOrScrollToBottom(List<ChatRoomMessage> messages) {
//        // 最后一个item是否显示出来
//        int lastCompletelyVisibleItemPosition = layoutManger.findLastCompletelyVisibleItemPosition();
//        if (lastCompletelyVisibleItemPosition == RecyclerView.NO_POSITION) {
//            tvBottomTip.setVisibility(GONE);
//            messages = msgFilter(messages);
//            chatRoomMessages.addAll(messages);
//            mMessageAdapter.notifyDataSetChanged();
//            messageListView.scrollToPosition(mMessageAdapter.getItemCount() - 1);
//            LogUtils.d(TAG,"showTipsOrScrollToBottom nitify all");
//            return;
//        }
//        boolean needScroll = (lastCompletelyVisibleItemPosition == mMessageAdapter.getItemCount() - 1);
//        messages = msgFilter(messages);
//        chatRoomMessages.addAll(messages);
//        if (needScroll) {
//            needUpdateDataSet = false;
//            tvBottomTip.setVisibility(GONE);
//            mMessageAdapter.notifyDataSetChanged();
//            messageListView.smoothScrollToPosition(mMessageAdapter.getItemCount() - 1);
//            LogUtils.d(TAG,"showTipsOrScrollToBottom nitify all,needUpdateDataSet:"+needUpdateDataSet+" needScroll:"+needScroll);
//        } else {
//            needUpdateDataSet = true;
//            tvBottomTip.setVisibility(VISIBLE);
//            LogUtils.d(TAG,"showTipsOrScrollToBottom nitify all,needUpdateDataSet:"+needUpdateDataSet+" needScroll:"+needScroll);
//        }
//    }
    boolean isNotScrollingAndBottom = true;

    private void init(Context context) {
        // 内容区域
        layoutManger = new ScrollSpeedLinearLayoutManger(context);
        FrameLayout.LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.rightMargin = Utils.dip2px(context, 10);
        messageListView = new RecyclerView(context);
        messageListView.setLayoutParams(params);
        messageListView.setOverScrollMode(OVER_SCROLL_NEVER);
        messageListView.setHorizontalScrollBarEnabled(false);
        addView(messageListView);
        messageListView.setLayoutManager(layoutManger);
        //去除动画item刷新效果
        messageListView.setItemAnimator(null);
        messageListView.addItemDecoration(new DividerItemDecoration(context, layoutManger.getOrientation(), 3, R.color.transparent));
        mMessageAdapter = new MessageAdapter();
        messageListView.setAdapter(mMessageAdapter);
        mMessageAdapter.setOnItemClickListener(this);
        // 底部有新消息
        tvBottomTip = new TextView(context);
        FrameLayout.LayoutParams params1 = new LayoutParams(
                Utils.dip2px(context, 100F), Utils.dip2px(context, 30));
        params1.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
        params1.bottomMargin = Utils.dip2px(context, 0);
        tvBottomTip.setBackgroundResource(R.drawable.bg_messge_view_bottom_tip);
        tvBottomTip.setGravity(Gravity.CENTER);
        tvBottomTip.setText(context.getString(R.string.message_view_bottom_tip));
        tvBottomTip.setTextColor(Color.WHITE);
        tvBottomTip.setLayoutParams(params1);
        tvBottomTip.setVisibility(GONE);
        tvBottomTip.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                isNotScrollingAndBottom = true;
                tvBottomTip.setVisibility(GONE);
                messageListView.scrollToPosition(mMessageAdapter.getItemCount() - 1);
            }
        });
        addView(tvBottomTip);

        chatRoomMessages = new ArrayList<>();
        tempMessages = new ArrayList<>();
        initData();

        messageListView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (needUpdateDataSet && newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    needUpdateDataSet = false;
                    // 如果用户在滑动,并且显示的是tip,则需要跟新
                    mMessageAdapter.notifyDataSetChanged();
                }
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (layoutManger.findLastCompletelyVisibleItemPosition() == mMessageAdapter.getItemCount() - 1) {
                        tvBottomTip.setVisibility(GONE);
                    }
                }
//
//                if (newState == RecyclerView.SCROLL_STATE_IDLE) {//RecyclerView现在不是滚动状态。
//                    if (isSlideToBottom()) {
//                        isNotScrollingAndBottom = true;
//                        tvBottomTip.setVisibility(GONE);
//                    }
//                } else if (newState == RecyclerView.SCROLL_STATE_SETTLING) {//自动滚动的状态，此时手指已经离开屏幕，RecyclerView的滚动是自身的惯性在维持。
//                    isNotScrollingAndBottom = false;
//                } else if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {//RecyclerView处于被外力引导的滚动状态，比如手指正在拖着进行滚动。
//                    isNotScrollingAndBottom = false;
//                }
            }
        });
    }

    /**
     * 显示新消息提示并自动滚动到最底部
     *
     * @param messages
     */
    private void showTipsOrScrollToBottom(List<ChatRoomMessage> messages) {
//        // 最后一个item是否显示出来
//        messages = msgFilter(messages);
//        if (isNotScrollingAndBottom) {
//            if (tvBottomTip.getVisibility() == View.VISIBLE) {
//                tvBottomTip.setVisibility(GONE);
//            }
//            mMessageAdapter.addMessages(messages);
//            if (isFirstLoad) {//添加第一次显示的滚动效果
//                isFirstLoad = false;
//                messageListView.smoothScrollToPosition(mMessageAdapter.getItemCount() - 1);
//            } else {//正常情况通过item动画显示效果
//                messageListView.scrollToPosition(mMessageAdapter.getItemCount() - 1);
//            }
//        } else {
//            if (tvBottomTip.getVisibility() == View.GONE) {
//                tvBottomTip.setVisibility(VISIBLE);
//            }
//            mMessageAdapter.addMessages(messages);
//        }
        // 最后一个item是否显示出来
        int lastCompletelyVisibleItemPosition = layoutManger.findLastCompletelyVisibleItemPosition();
        if (lastCompletelyVisibleItemPosition == RecyclerView.NO_POSITION) {
            tvBottomTip.setVisibility(GONE);
            messages = msgFilter(messages);
            chatRoomMessages.addAll(messages);
            mMessageAdapter.notifyDataSetChanged();
            messageListView.scrollToPosition(mMessageAdapter.getItemCount() - 1);
            return;
        }
        boolean needScroll = (lastCompletelyVisibleItemPosition == mMessageAdapter.getItemCount() - 1);
        messages = msgFilter(messages);
        chatRoomMessages.addAll(messages);
        if (needScroll) {
            needUpdateDataSet = false;
            tvBottomTip.setVisibility(GONE);
            mMessageAdapter.notifyDataSetChanged();
            messageListView.smoothScrollToPosition(mMessageAdapter.getItemCount() - 1);
        } else {
            needUpdateDataSet = true;
            tvBottomTip.setVisibility(VISIBLE);
        }
    }

    public boolean isSlideToBottom() {
        if (messageListView != null) {
            return messageListView.computeVerticalScrollExtent() + messageListView.computeVerticalScrollOffset()
                    >= messageListView.computeVerticalScrollRange();
        } else {
            return false;
        }
    }


    public void release() {
        if (chatRoomMessages != null) {
            if (chatRoomMessages.size() > 0) {
                chatRoomMessages.clear();
            }
            chatRoomMessages = null;
        }
        if (tempMessages != null) {
            if (tempMessages.size() > 0) {
                tempMessages.clear();
            }
            tempMessages = null;
        }
        if (onItemClickListener != null){
            onItemClickListener = null;
        }
    }

    public void refreshLikeMsgStatus(boolean isLiked, boolean isRefresh) {
        LogUtils.d(TAG, "refreshLikeMsgStatus-isLiked:" + isLiked + " isRefresh:" + isRefresh);
        if (null != mMessageAdapter) {
            mMessageAdapter.isLiked = isLiked;
            if (isRefresh) {
                mMessageAdapter.notifyDataSetChanged();
            }
        }
    }

    public void setOnItemClickListener(BaseQuickAdapter.OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }

    /**
     * Callback method to be invoked when an item in this RecyclerView has
     * been clicked.
     *
     * @param adapter  the adpater
     * @param view     The itemView within the RecyclerView that was clicked (this
     *                 will be a view provided by the adapter)
     * @param position The position of the view in the adapter.
     */
    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        LogUtils.d(TAG, "onItemClick-position:" + position);
        if (null != onItemClickListener) {
            onItemClickListener.onItemClick(adapter, view, position);
        }
    }

    private static class MessageAdapter extends BaseQuickAdapter<ChatRoomMessage, BaseViewHolder> implements OnClickListener {

        private final String TAG = MessageAdapter.class.getSimpleName();
        public boolean isLiked = false;
        private String account;

        MessageAdapter() {
            super(R.layout.list_item_chatrrom_msg);
        }

        public void addMessages(List<ChatRoomMessage> messages) {
            if (messages == null || messages.size() <= 0) {
                return;
            }
            int size = this.getData().size();
            this.getData().addAll(messages);
            if (size > 0) {
                notifyItemChanged(size - 1, messages.size());
            } else {
                notifyDataSetChanged();
            }
        }

        @Override
        protected void convert(BaseViewHolder baseViewHolder, ChatRoomMessage chatRoomMessage) {
            if (chatRoomMessage == null) {
                return;
            }

            TextView tv_nick_name = baseViewHolder.getView(R.id.tv_nick_name);
            String fromAccount = chatRoomMessage.getFromAccount();
            OnClickListener commClickListener = v -> {
                long userId = JavaUtil.str2long(fromAccount);
                if (userId > 0) {
                    new UserInfoDialog(mContext, userId).show();

                }

            };
            tv_nick_name.setOnClickListener(commClickListener);
            View ll_senderInfo = baseViewHolder.getView(R.id.ll_senderInfo);

            LevelView levelView = baseViewHolder.getView(R.id.level_view_msg_view);
            levelView.setOnClickListener(commClickListener);
            levelView.setLevelImageViewHeight(DisplayUtility.dp2px(mContext, 15));
            LevelView levelViewOther = baseViewHolder.getView(R.id.level_view_msg_view_other);
            levelViewOther.setOnClickListener(commClickListener);
            levelViewOther.setLevelImageViewHeight(DisplayUtility.dp2px(mContext, 15));
            ImageView newUserIcon = baseViewHolder.getView(R.id.iv_new_user_msg_icon);
            ImageView newUserIcon1 = baseViewHolder.getView(R.id.iv_new_user_msg_icon1);
            ImageView newUserIconGift = baseViewHolder.getView(R.id.iv_new_user_msg_icon_gift);

            ImageView ivMsgUserMedal = baseViewHolder.getView(R.id.ivMsgUserMedal);
            ImageView ivGiftUserMedal = baseViewHolder.getView(R.id.ivGiftUserMedal);

            LinearLayout faceContainer = baseViewHolder.getView(R.id.face_container);
            TextView tvGiftNumber = baseViewHolder.getView(R.id.tv_gift_number);
            LinearLayout msgContainer = baseViewHolder.getView(R.id.msg_container);
            levelView.setVisibility(GONE);
            ivMsgUserMedal.setVisibility(GONE);
            LinearLayout.LayoutParams levelViewLp = (LinearLayout.LayoutParams) levelView.getLayoutParams();
            levelViewLp.gravity = Gravity.CENTER_VERTICAL;
            faceContainer.setVisibility(View.GONE);
            msgContainer.setVisibility(View.VISIBLE);
            msgContainer.setBackgroundDrawable(msgContainer.getContext().getResources().getDrawable(R.drawable.shape_room_message_bg));
            faceContainer.removeAllViews();

            TextView tvContent = baseViewHolder.getView(R.id.tv_content);
            tvContent.setText(null);
            tvContent.setTextColor(mContext.getResources().getColor(R.color.white));
            tvContent.setOnClickListener(this);
            tvContent.setTag(chatRoomMessage);
            tvContent.setBackgroundDrawable(null);
            tvContent.setGravity(Gravity.CENTER_VERTICAL);
            tvContent.setPadding(0, 0, 0, 0);
            tvContent.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);

            ImageView ivGift = baseViewHolder.getView(R.id.iv_gift);
            ivGift.setVisibility(View.GONE);
            ivGift.setImageDrawable(null);
            ivGift.setTag(chatRoomMessage);
            ivGift.setOnClickListener(this);
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) ivGift.getLayoutParams();
            if (null != lp) {
                lp.width = DisplayUtility.dp2px(ivGift.getContext(), 45);
                lp.height = DisplayUtility.dp2px(ivGift.getContext(), 45);
                lp.leftMargin = 0;
            }
            tvGiftNumber.setVisibility(View.GONE);

            ll_senderInfo.setVisibility(View.GONE);
            newUserIconGift.setVisibility(View.GONE);
            levelViewOther.setVisibility(View.GONE);
            ivGiftUserMedal.setVisibility(View.GONE);

            LogUtils.d(TAG, "convert-chatRoomMessage.msgType:" + chatRoomMessage.getMsgType());
            if (chatRoomMessage.getMsgType() == MsgTypeEnum.tip) {
                tvContent.setTextColor(mContext.getResources().getColor(R.color.room_tip_color));
                tvContent.setText(chatRoomMessage.getContent());
                LogUtils.d(TAG, "convert-tip:" + chatRoomMessage.getContent());
            } else if (chatRoomMessage.getMsgType() == MsgTypeEnum.text) {
                ll_senderInfo.setVisibility(View.VISIBLE);
                //这里仅处理文本消息类型的贵族勋章
                setMsgText(chatRoomMessage, tv_nick_name, tvContent, levelView, newUserIcon, newUserIcon1, ivMsgUserMedal, msgContainer);
            } else if (chatRoomMessage.getMsgType() == MsgTypeEnum.notification) {
                checkIsNewUser(chatRoomMessage, newUserIconGift);
                setMsgNotification(chatRoomMessage, tvContent, msgContainer, levelViewOther, ivGiftUserMedal);
            } else if (chatRoomMessage.getMsgType() == MsgTypeEnum.custom) {
                CustomAttachment attachment = (CustomAttachment) chatRoomMessage.getAttachment();
                int experLevel = -1;
                if (chatRoomMessage.getAttachment() instanceof CustomAttachment) {
                    experLevel = attachment.getExperLevel();
                    if (experLevel > 0 && !(chatRoomMessage.getContent() + "").contains("甜甜工作人员")) {
                        levelViewOther.setVisibility(VISIBLE);
                        levelViewOther.setExperLevel(experLevel);
                    } else {
                        levelViewOther.setVisibility(GONE);
                    }
                }

                LogUtils.d(TAG, "attachment.getFirst():" + attachment.getFirst() + " attachment.getSecond():" + attachment.getSecond());
                if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_AUCTION) {
                    setMsgAuction(chatRoomMessage, tvContent, attachment);
                } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_ROOM_TIP) {
                    setMsgRoomTip(tvContent, (RoomTipAttachment) attachment);
                } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT) {
                    //这里仅处理送礼消息类型的贵族勋章
                    checkIsNewUser(chatRoomMessage, newUserIconGift);
                    setMsgHeaderGift(tvContent, ivGift, tvGiftNumber, (GiftAttachment) attachment, ivGiftUserMedal);
                } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_TYPE_RULE_FIRST) {
                    if (attachment instanceof RoomRuleAttachment) {
                        tvContent.setTextColor(mContext.getResources().getColor(R.color.color_FFA800));
                        tvContent.setText(((RoomRuleAttachment) attachment).getRule());
                    }
                } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE) {
                    int second = attachment.getSecond();
                    RoomQueueMsgAttachment roomQueueMsgAttachment = (RoomQueueMsgAttachment) attachment;
                    setSystemMsg(second, roomQueueMsgAttachment, tvContent);
                } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_FACE) {
                    setMsgFace(chatRoomMessage, faceContainer, msgContainer, (FaceAttachment) attachment, experLevel);
                } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT) {
                    //这里仅处理送礼消息类型的贵族勋章
                    checkIsNewUser(chatRoomMessage, newUserIconGift);
                    setMsgMultiGift(tvContent, ivGift, tvGiftNumber, (MultiGiftAttachment) attachment, ivGiftUserMedal);
                } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_SUB_PUBLIC_CHAT_ROOM) {
                    msgContainer.setVisibility(View.GONE);
                } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_LOTTERY_BOX) {
                    setLotteryInfo(tvContent, (LotteryBoxAttachment) attachment, chatRoomMessage, msgContainer);
                } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK) {//Pk消息
                    setPkMsg((PkCustomAttachment) attachment, tvContent);
                } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_ROOM_SHARE) {
                    setShareMsg(tvContent, ivGift);
                } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_ROOM_ATTENTION) {
                    setAttentionMsg(ivGift);
                } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_ROOM_RICH_TXT) {
                    showRichTxtMsg(tvContent, attachment.getData());
                } else if (CustomAttachment.CUSTOM_MSG_ROOM_LOVER_UP_MIC == attachment.getFirst()) {
                    showLoverUpMicMsg(tvContent, attachment);
                } else if (CustomAttachment.CUSTOM_MSG_ROOM_PAIR_SUC_ROOM_NOTICE == attachment.getFirst()) {
                    showPairResultMsg(tvContent, attachment);
                } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_ROOM_RED_PACKET_RECEIVE_FINISH) {
                    if (attachment instanceof RoomRedPacketFinishAttachment) {
                        setMsgRoomRedPacketFinishTip(tvContent, (RoomRedPacketFinishAttachment) attachment, levelView);
                    }
                }
            }
        }

        private void showRichTxtMsg(TextView tvContent, com.alibaba.fastjson.JSONObject data) {
            LogUtils.d(TAG, "showRichTxtMsg-data:" + data);
            if (null != data && null != tvContent.getContext()) {
                tvContent.setText(RichTextUtil.getSpannableStringFromJson(tvContent, data.toJSONString(), new RichTextUtil.OnClickableRichTxtItemClickedListener() {
                    @Override
                    public void onClickToEnterRoomActivity(long roomUid) {
                        LogUtils.d(TAG, "onClickToEnterRoomActivity-roomUid:" + roomUid);
                        if (null != AvRoomDataManager.get().mCurrentRoomInfo && AvRoomDataManager.get().mCurrentRoomInfo.getUid() != roomUid) {
                            //TODO 如果后续MessageView统一成一个类，需要在这里判断跳转房间类型
                            AVRoomActivity.start(tvContent.getContext(), roomUid);
                        }
                    }

                    @Override
                    public void onClickToShowUserInfoDialog(long uid) {
                        LogUtils.d(TAG, "onClickToShowUserInfoDialog-uid:" + uid);
                        new UserInfoDialog(tvContent.getContext(), uid).show();
                    }

                    @Override
                    public void onClickToShowWebViewLoadUrl(String url) {
                        LogUtils.d(TAG, "onClickToShowWebViewLoadUrl-url:" + url);
                        CommonWebViewActivity.start(tvContent.getContext(), url, false);
                    }
                }));
                tvContent.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
                tvContent.setOnClickListener(null);
            }
        }

        private void showPairResultMsg(TextView tvContent, CustomAttachment attachment) {
            LogUtils.d(TAG, "showPairResultMsg-attachment:" + attachment);
            LoverUpMicAnimAttachment loverUpMicAnimAttachment = (LoverUpMicAnimAttachment) attachment;
            if (null != tvContent.getContext() && null != loverUpMicAnimAttachment) {
                String firstNick = loverUpMicAnimAttachment.getFirstNick();
                if (!TextUtils.isEmpty(firstNick) && firstNick.length() > 6) {
                    firstNick = tvContent.getContext().getResources().getString(R.string.nick_length_max_six, firstNick.substring(0, 6));
                }
                String secondNick = loverUpMicAnimAttachment.getSecondNick();
                if (!TextUtils.isEmpty(secondNick) && secondNick.length() > 6) {
                    secondNick = tvContent.getContext().getResources().getString(R.string.nick_length_max_six, secondNick.substring(0, 6));
                }
                //修改提示文本显示样式
                String tips = tvContent.getContext().getString(R.string.pair_result_room_notice, firstNick, secondNick);
                LogUtils.d(TAG, "showPairResultMsg-tips:" + tips);
                SpannableStringBuilder ssb = new SpannableStringBuilder(tips);
                int firstIndex = tips.indexOf(firstNick);
                int firstLength = firstNick.length();
                //避免firstNick包含secondNick情况下抛IndexOutOfBoundsException异常
                int secondIndex = tips.lastIndexOf(secondNick);
                int secondLength = secondNick.length();
                if (firstIndex < tips.length() && (firstIndex + firstLength) < tips.length()) {
                    ssb.setSpan(new ForegroundColorSpan(NobleBusinessManager.getNobleRoomNickColor(
                            loverUpMicAnimAttachment.getFirstVipId(),
                            loverUpMicAnimAttachment.getFirstVipDate(),
                            loverUpMicAnimAttachment.isFirstIsInvisible(),
                            0xFF10CFA6,
                            firstNick)), firstIndex, firstIndex + firstLength, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                }
                if ((firstIndex + firstLength) < tips.length() && secondIndex < tips.length()) {
                    ssb.setSpan(new ForegroundColorSpan(Color.WHITE), firstIndex + firstLength, secondIndex, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                }
                if (secondIndex < tips.length() && (secondIndex + secondLength) < tips.length()) {
                    ssb.setSpan(new ForegroundColorSpan(NobleBusinessManager.getNobleRoomNickColor(
                            loverUpMicAnimAttachment.getSecondVipId(),
                            loverUpMicAnimAttachment.getSecondVipDate(),
                            loverUpMicAnimAttachment.isSecondIsInvisible(),
                            0xFF10CFA6,
                            secondNick)), secondIndex, secondIndex + secondLength, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                }
                if ((secondIndex + secondLength) < tips.length()) {
                    ssb.setSpan(new ForegroundColorSpan(Color.WHITE), secondIndex + secondLength, tips.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                }
                tvContent.setText(ssb);
            }
        }

        private void showLoverUpMicMsg(TextView tvContent, CustomAttachment attachment) {
            LogUtils.d(TAG, "showRichTxtMsg-attachment:" + attachment);
            LoverUpMicAnimAttachment loverUpMicAnimAttachment = (LoverUpMicAnimAttachment) attachment;
            if (null != tvContent.getContext() && null != loverUpMicAnimAttachment) {
                //修改提示文本显示样式
                String firstNick = loverUpMicAnimAttachment.getFirstNick();
                firstNick = NobleBusinessManager.getNobleRoomNick(loverUpMicAnimAttachment.getFirstVipId(),
                        loverUpMicAnimAttachment.isFirstIsInvisible(),
                        loverUpMicAnimAttachment.getFirstVipDate(),
                        firstNick);
                if (firstNick.length() > 6) {
                    firstNick = tvContent.getContext().getResources().getString(R.string.nick_length_max_six, firstNick.substring(0, 6));
                }
                String secondNick = loverUpMicAnimAttachment.getSecondNick();
                secondNick = NobleBusinessManager.getNobleRoomNick(loverUpMicAnimAttachment.getSecondVipId(),
                        loverUpMicAnimAttachment.isSecondIsInvisible(),
                        loverUpMicAnimAttachment.getSecondVipDate(),
                        secondNick);
                if (secondNick.length() > 6) {
                    secondNick = tvContent.getContext().getResources().getString(R.string.nick_length_max_six, secondNick.substring(0, 6));
                }

                String tips = tvContent.getContext().getString(R.string.room_lover_msg_tips, firstNick, secondNick);
                LogUtils.d(TAG, "showRichTxtMsg-tips:" + tips);
                SpannableStringBuilder ssb = new SpannableStringBuilder(tips);

                int firstIndex = tips.indexOf(firstNick);
                int firstLength = firstNick.length();
                //避免firstNick包含secondNick情况下抛IndexOutOfBoundsException异常
                int secondIndex = tips.lastIndexOf(secondNick);
                int secondLength = secondNick.length();
//                int defaultNickColor = 0xFF10CFA6;
                if (firstIndex < tips.length() && (firstIndex + firstLength) < tips.length()) {
                    ssb.setSpan(new ForegroundColorSpan(NobleBusinessManager.getNobleRoomNickColor(
                            loverUpMicAnimAttachment.getFirstVipId(), loverUpMicAnimAttachment.getFirstVipDate(),
                            loverUpMicAnimAttachment.isFirstIsInvisible(), 0xFF10CFA6, firstNick)),
                            firstIndex, firstIndex + firstLength, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                }
                if ((firstIndex + firstLength) < tips.length() && secondIndex < tips.length()) {
                    ssb.setSpan(new ForegroundColorSpan(Color.WHITE), firstIndex + firstLength, secondIndex, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                }
                if (secondIndex < tips.length() && (secondIndex + secondLength) < tips.length()) {
                    ssb.setSpan(new ForegroundColorSpan(NobleBusinessManager.getNobleRoomNickColor(
                            loverUpMicAnimAttachment.getSecondVipId(), loverUpMicAnimAttachment.getSecondVipDate(),
                            loverUpMicAnimAttachment.isSecondIsInvisible(), 0xFF10CFA6, secondNick)),
                            secondIndex, secondIndex + secondLength, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                }
                if ((secondIndex + secondLength) < tips.length()) {
                    ssb.setSpan(new ForegroundColorSpan(Color.WHITE), secondIndex + secondLength, tips.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                }
                tvContent.setText(ssb);
            }
        }

        /**
         * 显示自定义的分享房间消息
         */
        private void setShareMsg(TextView tvContent, ImageView ivGift) {
            tvContent.setText(tvContent.getContext().getResources().getString(R.string.room_msg_share));
            ivGift.setVisibility(View.VISIBLE);
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) ivGift.getLayoutParams();
            if (null != lp) {
                lp.width = DisplayUtility.dp2px(tvContent.getContext(), 36);
                lp.height = DisplayUtility.dp2px(tvContent.getContext(), 20);
                lp.leftMargin = 0;
            }
            if (GlideContextCheckUtil.checkContextUsable(ivGift.getContext())) {
                GlideApp.with(ivGift.getContext()).load(R.mipmap.icon_room_msg_item_share)
                        .dontAnimate()
                        .into(ivGift);
            }
        }

        /**
         * 显示自定义的关注房主消息
         */
        private void setAttentionMsg(ImageView ivGift) {
            ivGift.setVisibility(View.VISIBLE);
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) ivGift.getLayoutParams();
            if (null != lp) {
                lp.width = DisplayUtility.dp2px(ivGift.getContext(), 167);
                lp.height = DisplayUtility.dp2px(ivGift.getContext(), 76);
                lp.leftMargin = 0;
            }
            if (GlideContextCheckUtil.checkContextUsable(ivGift.getContext())) {
                GlideApp.with(ivGift.getContext()).load(isLiked ? R.mipmap.icon_room_msg_item_attention_already : R.mipmap.icon_room_msg_item_attention)
                        .dontAnimate()
                        .into(ivGift);
            }
        }


        /**
         * 设置PK消息结果
         *
         * @param chatRoomMessage
         * @param tvContent
         */
        private void setPkMsg(PkCustomAttachment chatRoomMessage, TextView tvContent) {
            PkVoteInfo info = chatRoomMessage.getPkVoteInfo();
            if (info == null) {
                return;
            }
            if (chatRoomMessage.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_START) {
                String tips = (AvRoomDataManager.get().isRoomOwner(info.getOpUid()) ? "房主" : "管理员") + "发起了";
                String nick = NobleBusinessManager.getNobleRoomNick(info.getVipId(),
                        info.isInvisible(), info.getVipDate(), info.getNick());
                String pkNick = NobleBusinessManager.getNobleRoomNick(info.getPkVipId(),
                        info.isPkIsInvisible(), info.getPkVipDate(), info.getPkNick());
                if (pkNick == null || nick == null) {
                    tvContent.setText(tips + "PK");
                    return;
                }
                if (!TextUtils.isEmpty(nick) && nick.length() > 6) {
                    nick = nick.substring(0, 6).concat("...");
                }
                if (pkNick.length() > 6) {
                    pkNick = pkNick.substring(0, 6).concat("...的PK");
                } else {
                    pkNick = pkNick.concat("的PK");
                }
                String content = tips + nick + "和" + pkNick;
                SpannableStringBuilder builder = new SpannableStringBuilder(content);
                int nickColor = NobleBusinessManager.getNobleRoomNickColor(
                        info.getVipId(), info.getVipDate(), info.isInvisible(),
                        0xff09d2a8, nick);
                int pkNickColor = NobleBusinessManager.getNobleRoomNickColor(
                        info.getVipId(), info.getVipDate(), info.isInvisible(), 0xff09d2a8, nick);
                ForegroundColorSpan rsNick = new ForegroundColorSpan(nickColor);
                ForegroundColorSpan rsPkNick = new ForegroundColorSpan(pkNickColor);
                if (!TextUtils.isEmpty(nick)) {
                    builder.setSpan(rsNick, tips.length(), tips.length() + nick.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
                if (!TextUtils.isEmpty(pkNick)) {
                    builder.setSpan(rsPkNick, content.length() - pkNick.length(), content.length() - 3, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
                tvContent.setText(builder);
            } else if (chatRoomMessage.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_START_NEW) {
                String lightText = "PK窗口";
                String tips = (AvRoomDataManager.get().isRoomOwner(info.getOpUid()) ? "房主" : "管理员") + "发起了PK，点击" + lightText + "可参与投票～";
                int firstIndex = tips.indexOf(lightText);
                SpannableStringBuilder builder = new SpannableStringBuilder(tips);
                ForegroundColorSpan pkSpan = new ForegroundColorSpan(0xff10CFA6);
                builder.setSpan(pkSpan, firstIndex, firstIndex + lightText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tvContent.setText(builder);
            } else if (chatRoomMessage.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_END) {
                String content = "本场PK结果：";
                String result = null;
                if (info.getVoteCount() == info.getPkVoteCount()) {
                    result = "平局!";
                } else if (info.getVoteCount() > info.getPkVoteCount()) {
                    result = info.getNick() + "胜利!";
                } else {
                    result = info.getPkNick() + "胜利!";
                }
                content = content + result;
                SpannableStringBuilder builder = new SpannableStringBuilder(content);
                ForegroundColorSpan redSpan1 = new ForegroundColorSpan(0xff09d2a8);
                if (!TextUtils.isEmpty(result)) {
                    builder.setSpan(redSpan1, content.length() - result.length(), content.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
                tvContent.setText(builder);
            } else if (chatRoomMessage.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_END_NEW) {
                try {
                    PkVoteInfo.PkUser pkUserFirst = info.getPkList().get(0);
                    SpannableStringBuilder builder = new SpannableStringBuilder("投票结果公布：");
                    if (info.getPkList() != null && info.getPkList().size() > 0 && info.getPkList().get(0).getVoteCount() == 0) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            builder.append("平局!", new ForegroundColorSpan(0xff10CFA6), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        } else {
                            builder.append("平局!");
                        }
                        tvContent.setText(builder);
                        return;
                    }
                    builder.append("\n");
                    for (int i = 0; i < info.getPkList().size(); i++) {
                        PkVoteInfo.PkUser pkUser = info.getPkList().get(i);
                        String nick = pkUser.getNick();
                        if (nick.length() > 4) {
                            nick = nick.substring(0, 4) + "...";
                        }
                        int voteCount = pkUser.getVoteCount();
                        String rankTip = "";
                        if (pkUserFirst.getVoteCount() > 0) {
                            if (i == 0) {
                                rankTip = "，获得本轮PK第一名";
                            } else if (voteCount == pkUserFirst.getVoteCount()) {
                                rankTip = "，并列本轮PK第一名";
                            }
                        }
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            builder.append(nick, new ForegroundColorSpan(Color.WHITE), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                            builder.append("  " + voteCount + "票" + rankTip + "\n", new ForegroundColorSpan(0xff10CFA6), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        } else {
                            builder.append(nick).append("  ").append(String.valueOf(voteCount)).append("票").append(rankTip).append("\n");
                        }
                    }
                    tvContent.setText(builder);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (chatRoomMessage.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_CANCEL) {
                String nick = (AvRoomDataManager.get().isRoomOwner(info.getOpUid()) ? "房主" : "管理员") + "取消了";
                String targetNick = info.getNick();
                String targetNick2 = info.getPkNick();
                if (!TextUtils.isEmpty(targetNick) && targetNick.length() > 6) {
                    targetNick = targetNick.substring(0, 6) + "...";
                }
                if (!TextUtils.isEmpty(targetNick2) && targetNick2.length() > 6) {
                    targetNick2 = targetNick2.substring(0, 6) + "..." + "的PK";
                } else {
                    targetNick2 = targetNick2 + "的PK";
                }
                String content = nick + targetNick + "和" + targetNick2;
                SpannableStringBuilder builder = new SpannableStringBuilder(content);
                ForegroundColorSpan redSpan = new ForegroundColorSpan(mContext.getResources().getColor(R.color.common_color_gift_content));
                ForegroundColorSpan redSpan1 = new ForegroundColorSpan(mContext.getResources().getColor(R.color.common_color_gift_content));

                if (!TextUtils.isEmpty(targetNick)) {
                    builder.setSpan(redSpan, nick.length(), nick.length() + targetNick.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }

                if (!TextUtils.isEmpty(targetNick2)) {
                    builder.setSpan(redSpan1, content.length() - targetNick2.length(), content.length() - 3, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
                tvContent.setText(builder);
            } else if (chatRoomMessage.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_CANCEL_NEW) {
                String nick = (AvRoomDataManager.get().isRoomOwner(info.getOpUid()) ? "房主" : "管理员") + "取消了本场PK";
                tvContent.setText(nick);
            }
        }


        private void setSystemMsg(int second, RoomQueueMsgAttachment roomQueueMsgAttachment, TextView tvContent) {
            int micPosition = roomQueueMsgAttachment.micPosition + 1;
            String handleNick = roomQueueMsgAttachment.getHandleNick() + "";
            String targetNick = roomQueueMsgAttachment.getTargetNick() + "";
            if (StringUtils.isEmpty(targetNick) || "null".equals(targetNick)) {
                targetNick = "";
            }
            int targetNickColor =
                    NobleBusinessManager.getNobleRoomNickColor(roomQueueMsgAttachment.targetVipId,
                            roomQueueMsgAttachment.targetVipDate, roomQueueMsgAttachment.targetIsInvisiable,
                            Color.WHITE, targetNick);
            int handleNickColor =
                    NobleBusinessManager.getNobleRoomNickColor(roomQueueMsgAttachment.handleVipId,
                            roomQueueMsgAttachment.handleVipDate, roomQueueMsgAttachment.handleIsInvisible,
                            Color.WHITE, handleNick);
            List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
            list.add(RichTextUtil.getRichTextMap("系统消息: ", 0x8affffff));
            if (second == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_INVITE) {
                list.add(RichTextUtil.getRichTextMap("玩家 ", 0x8affffff));
                list.add(RichTextUtil.getRichTextMap(targetNick, targetNickColor));
                list.add(RichTextUtil.getRichTextMap(" 被[管理] ", 0x8affffff));
                list.add(RichTextUtil.getRichTextMap(handleNick, handleNickColor));
                list.add(RichTextUtil.getRichTextMap(" 抱上了" + micPosition + "号麦位", 0x8affffff));

            } else if (second == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_KICK) {
                list.add(RichTextUtil.getRichTextMap("玩家 ", 0x8affffff));
                list.add(RichTextUtil.getRichTextMap(targetNick, targetNickColor));
                list.add(RichTextUtil.getRichTextMap(" 被[管理] ", 0x8affffff));
                list.add(RichTextUtil.getRichTextMap(handleNick, handleNickColor));
                list.add(RichTextUtil.getRichTextMap(" 抱下了" + micPosition + "号麦位", 0x8affffff));
            } else if (second == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_CLOSE_MIC) {
                //系统消息：X号麦位被[管理员/房主]XXX禁麦了
                list.add(RichTextUtil.getRichTextMap(micPosition + "号麦位被[管理] ", 0x8affffff));
                list.add(RichTextUtil.getRichTextMap(handleNick, handleNickColor));
                list.add(RichTextUtil.getRichTextMap(" 禁麦了", 0x8affffff));
            } else if (second == CUSTOM_MSG_HEADER_TYPE_QUEUE_OPEN_MIC) {
                list.add(RichTextUtil.getRichTextMap(micPosition + "号麦位被[管理] ", 0x8affffff));
                list.add(RichTextUtil.getRichTextMap(handleNick, handleNickColor));
                list.add(RichTextUtil.getRichTextMap(" 解禁了", 0x8affffff));
            } else if (second == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_LOCK_MIC) {
                list.add(RichTextUtil.getRichTextMap(micPosition + "号麦位被[管理] ", 0x8affffff));
                list.add(RichTextUtil.getRichTextMap(handleNick, handleNickColor));
                list.add(RichTextUtil.getRichTextMap(" 封锁了", 0x8affffff));
            } else if (second == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_UNLOCK_MIC) {
                list.add(RichTextUtil.getRichTextMap(micPosition + "号麦位被[管理] ", 0x8affffff));
                list.add(RichTextUtil.getRichTextMap(handleNick, handleNickColor));
                list.add(RichTextUtil.getRichTextMap(" 解锁了", 0x8affffff));
            } else if (second == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_LOCK_MIC_ALL) {
                list.add(RichTextUtil.getRichTextMap("所有麦位被[管理] ", 0x8affffff));
                list.add(RichTextUtil.getRichTextMap(handleNick, handleNickColor));
                list.add(RichTextUtil.getRichTextMap(" 封锁了", 0x8affffff));
            } else if (second == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_UNLOCK_MIC_ALL) {
                list.add(RichTextUtil.getRichTextMap("所有麦位被[管理] ", 0x8affffff));
                list.add(RichTextUtil.getRichTextMap(handleNick, handleNickColor));
                list.add(RichTextUtil.getRichTextMap(" 解锁了", 0x8affffff));
            } else if (second == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_KICK_ROOM) {
                list.add(RichTextUtil.getRichTextMap("玩家 ", 0x8affffff));
                list.add(RichTextUtil.getRichTextMap(targetNick, targetNickColor));
                list.add(RichTextUtil.getRichTextMap(" 被[管理] ", 0x8affffff));
                list.add(RichTextUtil.getRichTextMap(handleNick, handleNickColor));
                list.add(RichTextUtil.getRichTextMap(" 踢出了房间", 0x8affffff));
            } else if (second == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_ADD_BLACK_LIST) {
                list.add(RichTextUtil.getRichTextMap("玩家 ", 0x8affffff));
                list.add(RichTextUtil.getRichTextMap(targetNick, targetNickColor));
                list.add(RichTextUtil.getRichTextMap(" 被[管理] ", 0x8affffff));
                list.add(RichTextUtil.getRichTextMap(handleNick, handleNickColor));
                list.add(RichTextUtil.getRichTextMap(" 加入了黑名单", 0x8affffff));
            } else if (second == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_BLACK_LIST) {
                list.add(RichTextUtil.getRichTextMap("玩家 ", 0x8affffff));
                list.add(RichTextUtil.getRichTextMap(targetNick, targetNickColor));
                list.add(RichTextUtil.getRichTextMap(" 被[管理] ", 0x8affffff));
                list.add(RichTextUtil.getRichTextMap(handleNick, handleNickColor));
                list.add(RichTextUtil.getRichTextMap(" 移除了黑名单", 0x8affffff));
            } else if (second == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_CLOSE) {
                list.add(RichTextUtil.getRichTextMap("[管理] ", 0x8affffff));
                list.add(RichTextUtil.getRichTextMap(handleNick, handleNickColor));
                list.add(RichTextUtil.getRichTextMap(" 关闭了公屏", 0x8affffff));
            } else if (second == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_OPEN) {
                list.add(RichTextUtil.getRichTextMap("[管理] ", 0x8affffff));
                list.add(RichTextUtil.getRichTextMap(handleNick, handleNickColor));
                list.add(RichTextUtil.getRichTextMap(" 打开了公屏", 0x8affffff));
            } else if (second == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_OPEN) {
                list.add(RichTextUtil.getRichTextMap("[管理] ", 0x8affffff));
                list.add(RichTextUtil.getRichTextMap(handleNick, handleNickColor));
                list.add(RichTextUtil.getRichTextMap(" 开启过滤小额礼物特效", 0x8affffff));
            } else if (second == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_CLOSE) {
                list.add(RichTextUtil.getRichTextMap("[管理] ", 0x8affffff));
                list.add(RichTextUtil.getRichTextMap(handleNick, handleNickColor));
                list.add(RichTextUtil.getRichTextMap(" 关闭过滤小额礼物特效", 0x8affffff));
            }
            tvContent.setText(RichTextUtil.getSpannableStringFromList(list));
        }


        private void setLotteryInfo(TextView tvContent, LotteryBoxAttachment attachment,
                                    ChatRoomMessage chatRoomMessage, LinearLayout msgContainer) {

            String params = attachment.getParams();
            Json json = Json.parse(params);

            String senderNick = json.str("nick");
            String giftName = json.str("giftName");

            int defaultColor = 0xFFFFFFFF;
            int medalId = 0;
            int medalDate = 0;
            boolean isInVisiable = false;
            try {
                if (json.has(Constants.USER_MEDAL_ID)) {
                    medalId = json.num(Constants.USER_MEDAL_ID);
                }
                if (json.has(Constants.USER_MEDAL_DATE)) {
                    medalDate = json.num(Constants.USER_MEDAL_DATE);
                }
                if (json.has(Constants.NOBLE_INVISIABLE_ENTER_ROOM)) {
                    isInVisiable = json.num(Constants.NOBLE_INVISIABLE_ENTER_ROOM) == 1;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
            HashMap<String, Object> map;
            map = new HashMap<String, Object>();
            map.put(RichTextUtil.RICHTEXT_STRING, "恭喜 ");
            map.put(RichTextUtil.RICHTEXT_COLOR, 0xFFFFFFFF);
            list.add(map);

            map = new HashMap<String, Object>();
            map.put(RichTextUtil.RICHTEXT_STRING, senderNick);
            map.put(RichTextUtil.RICHTEXT_COLOR, NobleBusinessManager.getNobleRoomNickColor(medalId, medalDate, isInVisiable, 0xFF10CFA6, senderNick));
            list.add(map);

            map = new HashMap<String, Object>();
            map.put(RichTextUtil.RICHTEXT_STRING, " 打开宝箱获得");
            map.put(RichTextUtil.RICHTEXT_COLOR, 0xFFFFFFFF);
            list.add(map);

            map = new HashMap<String, Object>();
            map.put(RichTextUtil.RICHTEXT_STRING, " “" + giftName + "” ");
            map.put(RichTextUtil.RICHTEXT_COLOR, 0xFF10CFA6);
            list.add(map);

            map = new HashMap<String, Object>();
            map.put(RichTextUtil.RICHTEXT_STRING, " x" + json.str("count"));
            map.put(RichTextUtil.RICHTEXT_COLOR, 0xFFFFFFFF);
            list.add(map);

            tvContent.setText(RichTextUtil.getSpannableStringFromList(list));
            msgContainer.setVisibility(VISIBLE);
            if (json.has("goldPrice") && json.num("goldPrice") >= 10000) {
                msgContainer.setBackgroundDrawable(null);
                tvContent.setGravity(Gravity.CENTER_VERTICAL);
                if (json.num("goldPrice") == 33440) {
                    tvContent.setBackgroundResource(R.drawable.bg_room_custom_msg_lottery_box_33440);
                } else if (json.num("goldPrice") == 66666) {
                    tvContent.setBackgroundResource(R.drawable.bg_room_custom_msg_lottery_box_66666);
                } else {
                    tvContent.setBackgroundResource(R.drawable.bg_room_custom_msg_lottery_box);
                }
            }
            LogUtils.d("setLotteryInfo", json + "");
            String fromAccount = chatRoomMessage.getFromAccount();
            tvContent.setOnClickListener(v -> {
                long userId = JavaUtil.str2long(fromAccount);
                if (userId > 0) {
                    new UserInfoDialog(mContext, userId).show();
                }

            });

        }

        /**
         * 房间竞拍消息 -- 产品明确已废弃
         *
         * @param chatRoomMessage
         * @param tvContent
         * @param attachment
         */
        private void setMsgAuction(ChatRoomMessage chatRoomMessage, TextView tvContent, CustomAttachment attachment) {

        }

        /**
         * 根据指定等级规则修改昵称色值
         *
         * @param exLevel
         * @param nobleId
         * @param nobleDate
         * @param isInvisible
         * @param defaultColor
         * @param nick
         * @return
         */
        private static int getNickColor(int exLevel, int nobleId, int nobleDate,
                                        boolean isInvisible, int defaultColor, String nick) {
            //轰趴房昵称规则：
            //对于需要用户主动操作的消息类型，比如宝箱抽奖、普通文本、表情、送礼、进房等消息，昵称增加颜色修改规则，
            // 财富等级>=50，并且非贵族隐身模式，则昵称颜色改为金色
            //先判断是否贵族隐身，规则隐身时[神秘人金色高亮]
            int nickColor = NobleBusinessManager.getNobleRoomNickColor(nobleId, nobleDate, isInvisible,
                    defaultColor, nick);

            //再判断是否财富等级满足50级及以上，金色高亮
            if (!NobleBusinessManager.isNobleMysteryMan(nobleId, isInvisible, nobleDate)
                    && exLevel >= 50) {
                nickColor = 0xffF8DD4E;
            }

            return nickColor;
        }

        private void setMsgMultiGift(TextView tvContent, ImageView ivGift, TextView tvGiftNumber,
                                     MultiGiftAttachment attachment, ImageView ivGiftUserMedal) {
            ivGift.setVisibility(View.VISIBLE);
            tvGiftNumber.setVisibility(View.VISIBLE);
            MultiGiftReceiveInfo multiGiftRecieveInfo = attachment.getMultiGiftRecieveInfo();
            if (multiGiftRecieveInfo == null) {
                return;
            }

            if (!GlideContextCheckUtil.checkContextUsable(mContext)) {
                return;
            }

            GiftInfo giftInfo = CoreManager.getCore(IGiftCore.class).findGiftInfoById(multiGiftRecieveInfo.getGiftId());
            if (giftInfo != null) {
                String nick = multiGiftRecieveInfo.getNick();
                int medalId = multiGiftRecieveInfo.getVipId();
                int medalDate = multiGiftRecieveInfo.getVipDate();
                boolean isInvisible = multiGiftRecieveInfo.isInvisible();

                if (!TextUtils.isEmpty(nick) && nick.length() > 6) {
                    nick = nick.substring(0, 6) + "...";
                }
                SpannableStringBuilder builder = new SpannableStringBuilder(
                        tvContent.getContext().getResources().getString(R.string.room_msg_gift_to_all_mic, nick));
//                int defaultColor = mContext.getResources().getColor(R.color.roomTextNick);
                ForegroundColorSpan redSpan = new ForegroundColorSpan(getNickColor(attachment.getExperLevel(),
                        medalId, medalDate, isInvisible, 0xFFFF3A30, nick));

                builder.setSpan(redSpan, 0, nick.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tvContent.setText(builder);
                tvContent.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
                tvGiftNumber.setText(tvGiftNumber.getContext().getResources().getString(
                        R.string.room_msg_gift_num, multiGiftRecieveInfo.getGiftNum()));
                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) ivGift.getLayoutParams();
                if (null != lp) {
                    lp.width = DisplayUtility.dp2px(mContext, 45);
                    lp.height = DisplayUtility.dp2px(mContext, 45);
                    lp.leftMargin = DisplayUtility.dp2px(mContext, 5);
                }
                ImageLoadUtils.loadImage(mContext, giftInfo.getGiftUrl(), ivGift);
            }

            if (!TextUtils.isEmpty(multiGiftRecieveInfo.getVipMedal())) {
                ivGiftUserMedal.setVisibility(VISIBLE);
                GlideApp.with(mContext).load(multiGiftRecieveInfo.getVipMedal()).dontAnimate().into(ivGiftUserMedal);
            }
        }

        private void setMsgHeaderGift(TextView tvContent, ImageView ivGift, TextView tvGiftNumber,
                                      GiftAttachment attachment, ImageView ivGiftUserMedal) {
            ivGift.setVisibility(View.VISIBLE);
            tvGiftNumber.setVisibility(View.VISIBLE);
            GiftReceiveInfo giftRecieveInfo = attachment.getGiftRecieveInfo();
            if (giftRecieveInfo == null) {
                return;
            }
            GiftInfo giftInfo = CoreManager.getCore(IGiftCore.class).findGiftInfoById(giftRecieveInfo.getGiftId());
            if (giftInfo != null) {
                String nick = giftRecieveInfo.getNick();
                String targetNick = giftRecieveInfo.getTargetNick();
                int medalId = giftRecieveInfo.getVipId();
                int targetMedalId = giftRecieveInfo.getTargetVipId();
                int medalDate = giftRecieveInfo.getVipDate();
                int targetMedalDate = giftRecieveInfo.getTargetVipDate();
                boolean isInvisible = giftRecieveInfo.isInvisible();
                boolean targetIsInvisible = giftRecieveInfo.isTargetIsInvisible();

                if (!TextUtils.isEmpty(nick) && nick.length() > 4) {
                    nick = nick.substring(0, 4) + "...";
                }
                if (!TextUtils.isEmpty(targetNick) && targetNick.length() > 4) {
                    targetNick = targetNick.substring(0, 4) + "...";
                }

                if (!GlideContextCheckUtil.checkContextUsable(mContext)) {
                    return;
                }

                String content = mContext.getResources().getString(R.string.room_msg_gift_to, nick, targetNick);
                SpannableStringBuilder builder = new SpannableStringBuilder(content);

                //发送者昵称颜色
//                int defaultColor = mContext.getResources().getColor(R.color.common_color_gift_content);
                ForegroundColorSpan redSpan = new ForegroundColorSpan(getNickColor(attachment.getExperLevel(),
                        medalId, medalDate, isInvisible, 0xff09d2a8, nick));

                //接受者昵称颜色
                ForegroundColorSpan redSpan1 = new ForegroundColorSpan(
                        NobleBusinessManager.getNobleRoomNickColor(targetMedalId, targetMedalDate, targetIsInvisible,
                                0xff09d2a8, targetNick));

                if (!TextUtils.isEmpty(nick)) {
                    builder.setSpan(redSpan, 0, nick.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }

                if (!TextUtils.isEmpty(targetNick)) {
                    builder.setSpan(redSpan1, content.length() - targetNick.length(), content.length(),
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
                tvContent.setText(builder);
                tvContent.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
                tvGiftNumber.setText(mContext.getResources().getString(
                        R.string.room_msg_gift_num, giftRecieveInfo.getGiftNum()));
                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) ivGift.getLayoutParams();
                if (null != lp) {
                    lp.width = DisplayUtility.dp2px(mContext, 45);
                    lp.height = DisplayUtility.dp2px(mContext, 45);
                    lp.leftMargin = DisplayUtility.dp2px(mContext, 5);
                }
                ImageLoadUtils.loadImage(mContext, giftInfo.getGiftUrl(), ivGift);
            }

            if (!TextUtils.isEmpty(giftRecieveInfo.getVipMedal())) {
                ivGiftUserMedal.setVisibility(VISIBLE);
                GlideApp.with(mContext).load(giftRecieveInfo.getVipMedal()).dontAnimate().into(ivGiftUserMedal);
            }else {
                ivGiftUserMedal.setVisibility(GONE);
            }
        }

        private void setMsgRoomRedPacketFinishTip(TextView tvContent, RoomRedPacketFinishAttachment finishAttachment, LevelView levelView) {
            if (finishAttachment != null && finishAttachment.getDataInfo() != null) {
                RoomRedPacketFinish info = finishAttachment.getDataInfo();
                int level = info.getLevel();
                levelView.setExperLevel(level);
                levelView.setVisibility(VISIBLE);

                String contentStr = info.getNick() + "在本轮红包雨中，抢到了" + info.getGoldNum() + "金币，属于人气王";
                SpannableString span = new SpannableString(contentStr);
                //昵称绿色
                Object green = new ForegroundColorSpan(0xff10cfa6);
                span.setSpan(green, 0, info.getNick().length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                //金额绿色
                int goldIndex = (info.getNick() + "在本轮红包雨中，抢到了").length();
                Object green1 = new ForegroundColorSpan(0xff10cfa6);
                span.setSpan(green1, goldIndex, goldIndex + (info.getGoldNum() + "").length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                //人气王三个字绿色
                Object green2 = new ForegroundColorSpan(0xff10cfa6);
                span.setSpan(green2, contentStr.length() - 3, contentStr.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                tvContent.setText(span);
            }
        }

        private void setMsgNotification(ChatRoomMessage chatRoomMessage, TextView tvContent,
                                        LinearLayout msgContainer, LevelView levelView, ImageView ivGiftUserMedal) {
            ChatRoomNotificationAttachment attachment = (ChatRoomNotificationAttachment) chatRoomMessage.getAttachment();
            String carName = "";
            int level = 0;
            long uid = 0;
            boolean isFromMic = false;
            String userMedalUrl = null;
            boolean isInvisible = false;
            int medalId = 0;
            int medalDate = 0;
            Map<String, Object> extenMap = chatRoomMessage.getChatRoomMessageExtension().getSenderExtension();
            if (null != extenMap) {
                if (extenMap.containsKey(Constants.USER_MEDAL_ID)) {
                    medalId = (Integer) extenMap.get(Constants.USER_MEDAL_ID);
                }
                if (extenMap.containsKey(Constants.USER_MEDAL_DATE)) {
                    medalDate = (Integer) extenMap.get(Constants.USER_MEDAL_DATE);
                }
                if (extenMap.containsKey(Constants.NOBLE_INVISIABLE_ENTER_ROOM)) {
                    isInvisible = (int) extenMap.get(Constants.NOBLE_INVISIABLE_ENTER_ROOM) == 1;
                }
                if (extenMap.containsKey(Constants.USER_EXPER_LEVEL)) {
                    level = (Integer) extenMap.get(Constants.USER_EXPER_LEVEL);
                }
                if (extenMap.containsKey(Constants.USER_CAR_NAME)) {
                    carName = (String) extenMap.get(Constants.USER_CAR_NAME);
                }
                if (extenMap.containsKey(Constants.USER_NOBLE_MEDAL)) {
                    userMedalUrl = (String) extenMap.get(Constants.USER_NOBLE_MEDAL);
                }
                if (extenMap.containsKey(Constants.ROOM_MEMBER_ID)) {
                    uid = (Integer) extenMap.get(Constants.ROOM_MEMBER_ID);
                }
                if (extenMap.containsKey(Constants.ROOM_FROM_MIC)) {
                    isFromMic = (Boolean) extenMap.get(Constants.ROOM_FROM_MIC);
                }
            }
            LogUtils.d(TAG, "setMsgNotification-level:" + level + " carName:" + carName
                    + " isFromMic:" + isFromMic + " uid:" + uid
                    + " selfUid:" + CoreManager.getCore(IAuthCore.class).getCurrentUid()
                    + " handleIsInvisible:" + isInvisible);

            if (!TextUtils.isEmpty(userMedalUrl)) {
                ivGiftUserMedal.setVisibility(VISIBLE);
                if (GlideContextCheckUtil.checkContextUsable(ivGiftUserMedal.getContext())) {
                    GlideApp.with(ivGiftUserMedal.getContext()).load(userMedalUrl).dontAnimate().into(ivGiftUserMedal);
                }
            }

            String senderNick = "";
            List<String> nicks = attachment.getTargetNicks();
            if (nicks != null && nicks.size() > 0) {
                senderNick = attachment.getTargetNicks().get(0);
            }
            senderNick = NobleBusinessManager.getNobleRoomNick(
                    medalId, isInvisible, medalDate, senderNick);
            if (attachment.getType() == NotificationType.ChatRoomMemberIn) {
                levelView.setExperLevel(level);
                levelView.setVisibility(VISIBLE);
                if (!TextUtils.isEmpty(carName)) {
                    List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
                    HashMap<String, Object> map = new HashMap<String, Object>();
                    map.put(RichTextUtil.RICHTEXT_STRING, StringUtil.limitStr(mContext, senderNick, 6));
                    map.put(RichTextUtil.RICHTEXT_COLOR, getNickColor(level, medalId, medalDate, isInvisible,
                            Color.WHITE, senderNick));
                    list.add(map);

                    map = new HashMap<String, Object>();
                    map.put(RichTextUtil.RICHTEXT_STRING, " 驾着");
                    map.put(RichTextUtil.RICHTEXT_COLOR, Color.WHITE);
                    list.add(map);

                    map = new HashMap<String, Object>();
                    map.put(RichTextUtil.RICHTEXT_STRING, "“".concat(carName).concat("”"));
                    map.put(RichTextUtil.RICHTEXT_COLOR, 0xFF10CFA6);
                    list.add(map);

                    map = new HashMap<String, Object>();
                    /*&& CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getUid()!= uid*/
                    map.put(RichTextUtil.RICHTEXT_STRING, isFromMic ? "来了（来自一键连麦）" : "来了");
                    map.put(RichTextUtil.RICHTEXT_COLOR, Color.WHITE);
                    list.add(map);

                    CharSequence cs = RichTextUtil.getSpannableStringFromList(list);
                    LogUtils.d(TAG, "setMsgNotification-cs:" + (null != cs ? cs.toString() : ""));
                    //保持同IOS样式一致
                    tvContent.setText(cs);
                    msgContainer.setVisibility(VISIBLE);
                } else {
                    /*&& CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getUid()!= uid*/
                    msgContainer.setVisibility(isFromMic ? View.VISIBLE : GONE);
                    String content = senderNick.concat(isFromMic ? "来了（来自一键连麦）" : "来了");
                    LogUtils.d(TAG, "setMsgNotification-content:" + content);
                    SpannableString s = new SpannableString(content);
                    Object green = new ForegroundColorSpan(NobleBusinessManager.getNobleRoomNickColor(
                            medalId, medalDate, isInvisible, 0xff10cfa6, senderNick));
                    s.setSpan(green, 0, senderNick.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                    Object white = new ForegroundColorSpan(Color.WHITE);
                    s.setSpan(white, senderNick.length(), content.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                    tvContent.setText(s);
                }
                tvContent.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
            }
        }

        private void setMsgRoomTip(TextView tvContent, RoomTipAttachment roomTipAttachment) {
            String content = "";
            if (StringUtils.isEmpty(roomTipAttachment.getNick())) {
                roomTipAttachment.setNick(" ");
            }
            if (roomTipAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SUB_TYPE_ROOM_TIP_SHARE_ROOM) {
                content = roomTipAttachment.getNick() + " 分享了房间";
            } else if (roomTipAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SUB_TYPE_ROOM_TIP_ATTENTION_ROOM_OWNER) {
                content = roomTipAttachment.getNick() + " 关注了房主";
            }
            SpannableStringBuilder builder = new SpannableStringBuilder(content);
            if (GlideContextCheckUtil.checkContextUsable(mContext)) {
                ForegroundColorSpan redSpan = new ForegroundColorSpan(0x8affffff);
                if (!TextUtils.isEmpty(roomTipAttachment.getNick())) {
                    builder.setSpan(redSpan, 0, roomTipAttachment.getNick().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            }
            tvContent.setText(builder);
        }

        private void checkIsNewUser(ChatRoomMessage chatRoomMessage, ImageView newUserIconGift) {
//            String newUserContent = "";
            String aliasIconUrl = "";
            try {
                if (chatRoomMessage.getChatRoomMessageExtension() == null) {
                    aliasIconUrl = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getAlias();
                } else {
//                newUserContent = (String) chatRoomMessage.getChatRoomMessageExtension().getSenderExtension().get("is_new_user");
                    aliasIconUrl = (String) chatRoomMessage.getChatRoomMessageExtension().getSenderExtension().get(Constants
                            .ALIAS_ICON_URL);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
//            newUserIconGift.setVisibility(TextUtils.isEmpty(newUserContent) ? GONE : VISIBLE);
            LogUtils.d(TAG, "checkIsNewUser-aliasIconUrl:" + aliasIconUrl);
            if (!TextUtils.isEmpty(aliasIconUrl)) {
                newUserIconGift.setVisibility(VISIBLE);
                if (GlideContextCheckUtil.checkContextUsable(newUserIconGift.getContext())) {
                    GlideApp.with(newUserIconGift.getContext()).load(aliasIconUrl).dontAnimate().into(newUserIconGift);
                }
            } else {
                newUserIconGift.setVisibility(GONE);
            }
        }

        private void setMsgFace(ChatRoomMessage chatRoomMessage, LinearLayout faceContainer,
                                LinearLayout msgContainer, FaceAttachment attachment, int experLevel) {
            faceContainer.removeAllViews();
            faceContainer.setVisibility(View.VISIBLE);
            msgContainer.setVisibility(View.GONE);
            FaceAttachment faceAttachment = attachment;

            if (!GlideContextCheckUtil.checkContextUsable(mContext)) {
                return;
            }

//            String newUserContent = "";
            String aliasIconUrl = "";
            if (chatRoomMessage.getChatRoomMessageExtension() == null) {
                UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
                if (null != userInfo) {
                    aliasIconUrl = userInfo.getAlias();
                }
            } else {
                try {
//                newUserContent = (String) chatRoomMessage.getChatRoomMessageExtension().getSenderExtension().get("is_new_user");
                    aliasIconUrl = (String) chatRoomMessage.getChatRoomMessageExtension().getSenderExtension().get(Constants.ALIAS_ICON_URL);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            List<FaceReceiveInfo> faceReceiveInfos = faceAttachment.getFaceReceiveInfos();
            for (int i = 0; i < faceReceiveInfos.size(); i++) {
                FaceReceiveInfo faceReceiveInfo = faceReceiveInfos.get(i);
                FaceInfo faceInfo = CoreManager.getCore(IFaceCore.class).findFaceInfoById(faceReceiveInfo.getFaceId());
                if (faceReceiveInfo.getResultIndexes().size() <= 0 || faceInfo == null) {
                    continue;
                }

                LinearLayout linearLayout = new LinearLayout(mContext);
                linearLayout.setOrientation(faceReceiveInfo.getResultIndexes().size() == 1 ? LinearLayout.HORIZONTAL : LinearLayout.VERTICAL);

                //普通文本
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.gravity = Gravity.CENTER_VERTICAL;

                //动态称号
                LinearLayout.LayoutParams aliasTViewLayoutParams = new LinearLayout.LayoutParams(
                        DisplayUtility.dp2px(mContext, 20),
                        DisplayUtility.dp2px(mContext, 18));
                aliasTViewLayoutParams.rightMargin = DisplayUtility.dp2px(mContext, 3);
                aliasTViewLayoutParams.gravity = Gravity.CENTER_VERTICAL;

                //财富值等级
                LinearLayout.LayoutParams levelViewLayoutParams = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT, DisplayUtility.dp2px(mContext, 15));
                levelViewLayoutParams.gravity = Gravity.CENTER_VERTICAL;
                levelViewLayoutParams.rightMargin = DisplayUtility.dp2px(mContext, 3);

                //贵族勋章
                LinearLayout.LayoutParams medalIViewLayoutParams = new LinearLayout.LayoutParams(
                        DisplayUtility.dp2px(mContext, 22),
                        DisplayUtility.dp2px(mContext, 20));
                medalIViewLayoutParams.gravity = Gravity.CENTER_VERTICAL;
//                medalIViewLayoutParams.rightMargin = DisplayUtility.dp2px(mContext, 3);

                if (TextUtils.isEmpty(aliasIconUrl)) {
                    aliasIconUrl = faceReceiveInfo.getAliasIconUrl();
                }

                boolean notEmpty = !TextUtils.isEmpty(aliasIconUrl);
                if (experLevel > 0 || notEmpty) {
//                    //然后是贵族勋章
//                    if (!TextUtils.isEmpty(faceReceiveInfo.getVipMedal())) {
//                        ImageView imageView = new ImageView(mContext);
//                        imageView.setLayoutParams(medalIViewLayoutParams);
//                        linearLayout.addView(imageView);
//                        if (GlideContextCheckUtil.checkContextUsable(mContext)) {
//                            GlideApp.with(mContext).load(faceReceiveInfo.getVipMedal()).dontAnimate().into(imageView);
//                        }
//                    }
//                    //动态称号最左边
//                    if (notEmpty) {
//                        ImageView imageView = new ImageView(mContext);
//                        imageView.setLayoutParams(aliasTViewLayoutParams);
//                        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
//                        linearLayout.addView(imageView);
//                        if (GlideContextCheckUtil.checkContextUsable(mContext)) {
//                            GlideApp.with(mContext).load(aliasIconUrl).dontAnimate().into(imageView);
//                        }
//                    }

                    //最后是财富值等级
                    LevelView levelView = new LevelView(mContext);
                    levelView.setExperLevel(experLevel);
                    levelView.setVisibility(VISIBLE);
                    levelView.setLayoutParams(levelViewLayoutParams);
                    levelView.setLevelImageViewHeight(DisplayUtility.dp2px(mContext, 15));
                    linearLayout.addView(levelView);
                }

                faceContainer.addView(linearLayout);
                linearLayout.setLayoutParams(layoutParams);
                TextView textView = new TextView(mContext);
                textView.setTextColor(0xffffffff);
                String nick = faceReceiveInfo.getNick();
                if (StringUtils.isEmpty(nick)) {
                    nick = "";
                }
                nick = NobleBusinessManager.getNobleRoomNick(faceReceiveInfo.getVipId(),
                        faceReceiveInfo.getIsInvisible() == 1, faceReceiveInfo.getVipDate(), nick);
//                int defaultNickColor = mContext.getResources().getColor(R.color.common_color_11_transparent_54);
                int nickColor = getNickColor(experLevel, faceReceiveInfo.getVipId(), faceReceiveInfo.getVipDate(),
                        faceReceiveInfo.getIsInvisible() == 1, 0x8affffff, nick);

                int margin = Utils.dip2px(mContext, 5);
                nick = StringUtil.limitStr(mContext, nick, 6);
                String content = nick.concat(" 出 ");
                SpannableStringBuilder builder = new SpannableStringBuilder(content);
                ForegroundColorSpan redSpan = new ForegroundColorSpan(nickColor);
                builder.setSpan(redSpan, 0, nick.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                textView.setTextSize(12);
                textView.setText(builder);
                textView.setLayoutParams(layoutParams);
                linearLayout.addView(textView);

                List<Integer> resultIndexes = faceReceiveInfo.getResultIndexes();
                LinearLayout llFaceContainer = new LinearLayout(mContext);
                llFaceContainer.setOrientation(LinearLayout.HORIZONTAL);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.gravity = Gravity.CENTER_VERTICAL;
                llFaceContainer.setLayoutParams(layoutParams);
                for (Integer index : resultIndexes) {
                    ImageView imageView = new ImageView(mContext);
                    imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                    LinearLayout.LayoutParams imgLayoutParams = new LinearLayout.LayoutParams(UIUtil.dip2px(mContext, 30), UIUtil.dip2px(mContext, 30));
                    imgLayoutParams.gravity = Gravity.CENTER_VERTICAL;
                    if (faceInfo.getId() == 17 && resultIndexes.size() > 1) {
                        // 骰子
                        imgLayoutParams = new LinearLayout.LayoutParams(UIUtil.dip2px(mContext, 22), UIUtil.dip2px(mContext, 22));
                        imgLayoutParams.setMargins(0, margin, margin, margin);
                        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    } else if (faceInfo.getId() == 23) {
                        // 纸牌
                        imageView.setScaleType(ImageView.ScaleType.FIT_START);
                        imgLayoutParams.setMargins(0, margin, 0, margin);
                    }
                    imageView.setLayoutParams(imgLayoutParams);
                    ImageLoadUtils.loadImage(mContext, faceInfo.getFacePath(index), imageView);
                    llFaceContainer.addView(imageView);
                }
                linearLayout.addView(llFaceContainer);
            }
        }

        private void setMsgText(ChatRoomMessage chatRoomMessage, TextView tvNickName, TextView tvContent,
                                LevelView levelView, ImageView newUserIcon, ImageView newUserIcon1, ImageView ivUserMedal,
                                LinearLayout msgContainer) {
            int experLevel = -1;
            int medalId = 0;
            int medalDate = 0;
            boolean isInvisible = false;
//            String newUserContent = "";
//            String aliasIconUrl = "";
            String nick = null;
            String nobleMedalIconUrl = "";
//            String nobleBubbleUrl = "";

            List<String> medalUrls = new ArrayList<>();

            Map<String, Object> data = new HashMap<>();
            if (null != chatRoomMessage.getRemoteExtension()) {
                data = chatRoomMessage.getRemoteExtension();
            }
            if (null != chatRoomMessage.getLocalExtension()) {
                data = chatRoomMessage.getLocalExtension();
            }
            if (null != data && data.size() > 0) {
                if (data.containsKey(Constants.USER_EXPER_LEVEL)) {
                    experLevel = (int) data.get(Constants.USER_EXPER_LEVEL);
                }
                if (data.containsKey(Constants.USER_NICK)) {
                    nick = (String) data.get(Constants.USER_NICK);
                }
//                if (data.containsKey(Constants.ALIAS_ICON_URL)) {
//                    aliasIconUrl = (String) data.get(Constants.ALIAS_ICON_URL);
//                }
                if (data.containsKey(Constants.MEDAL_URLS)) {
                    medalUrls = (List<String>) data.get(Constants.MEDAL_URLS);
                }
                if (data.containsKey(Constants.USER_NOBLE_MEDAL)) {
                    nobleMedalIconUrl = (String) data.get(Constants.USER_NOBLE_MEDAL);
                }
//                if (data.containsKey(Constants.USER_NOBLE_BUBBLE)) {
//                    nobleBubbleUrl = (String) data.get(Constants.USER_NOBLE_BUBBLE);
//                }
                if (data.containsKey(Constants.USER_MEDAL_ID)) {
                    medalId = (int) data.get(Constants.USER_MEDAL_ID);
                }

                if (data.containsKey(Constants.USER_MEDAL_DATE)) {
                    medalDate = (Integer) data.get(Constants.USER_MEDAL_DATE);
                }
                if (data.containsKey(Constants.NOBLE_INVISIABLE_ENTER_ROOM)) {
                    isInvisible = (int) data.get(Constants.NOBLE_INVISIABLE_ENTER_ROOM) == 1;
                }
            }

            if (!TextUtils.isEmpty(nobleMedalIconUrl)) {
                ivUserMedal.setVisibility(VISIBLE);
                if (GlideContextCheckUtil.checkContextUsable(ivUserMedal.getContext())) {
                    GlideApp.with(ivUserMedal.getContext()).load(nobleMedalIconUrl).dontAnimate().into(ivUserMedal);
                }
            }else {
                ivUserMedal.setVisibility(GONE);
            }
            if (!TextUtils.isEmpty(nick)) {
                nick = NobleBusinessManager.getNobleRoomNick(medalId, isInvisible, medalDate, nick);
            }
            //兼容旧版
            if (experLevel == -1 && TextUtils.isEmpty(nick)) {
                if (chatRoomMessage.getChatRoomMessageExtension() == null) {
                    UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
                    if (null != userInfo) {
                        experLevel = userInfo.getExperLevel();
                        nick = userInfo.getNick();
//                        aliasIconUrl = userInfo.getAlias();
                    }
                } else {
                    nick = chatRoomMessage.getChatRoomMessageExtension().getSenderNick();
                    try {
                        experLevel = (Integer) chatRoomMessage.getChatRoomMessageExtension().getSenderExtension().get("experLevel");
                        //解析和发送逻辑保留，兼容旧版本
//                    newUserContent = (String) chatRoomMessage.getChatRoomMessageExtension().getSenderExtension().get("is_new_user");
//                        aliasIconUrl = (String) chatRoomMessage.getChatRoomMessageExtension().getSenderExtension().get(Constants.ALIAS_ICON_URL);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            int bubbleResId = NobleBusinessManager.getNobleRoomMsgBubbleResId(medalId);
            if (bubbleResId > 0 && GlideContextCheckUtil.checkContextUsable(tvContent.getContext())) {
                tvContent.setBackgroundDrawable(tvContent.getContext().getResources().getDrawable(bubbleResId));
            }

            tvContent.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
//            msgContainer.setBackgroundDrawable(null);
            tvNickName.setText(StringUtil.limitStr(mContext, nick, 6));

            int defaultColor = Color.WHITE;
            if (GlideContextCheckUtil.checkContextUsable(tvContent.getContext())) {
                defaultColor = 0x99ffffff;
            }
            tvNickName.setTextColor(getNickColor(experLevel, medalId, medalDate, isInvisible,
                    defaultColor, nick));
            if (!ListUtils.isListEmpty(medalUrls)) {
                if (medalUrls.size() == 1) {
                    if (!TextUtils.isEmpty(medalUrls.get(0))) {
                        newUserIcon.setVisibility(VISIBLE);
                        newUserIcon1.setVisibility(GONE);
                        GlideApp.with(newUserIcon.getContext()).load(medalUrls.get(0)).dontAnimate().into(newUserIcon);
                    }else {
                        newUserIcon.setVisibility(GONE);
                        newUserIcon1.setVisibility(GONE);
                    }
                } else if (medalUrls.size() >= 2) {
                    if (!TextUtils.isEmpty(medalUrls.get(0))) {
                        newUserIcon.setVisibility(VISIBLE);
                        GlideApp.with(newUserIcon.getContext()).load(medalUrls.get(0)).dontAnimate().into(newUserIcon);
                    }else {
                        newUserIcon.setVisibility(GONE);
                    }
                    if (!TextUtils.isEmpty(medalUrls.get(1))) {
                        newUserIcon1.setVisibility(VISIBLE);
                        GlideApp.with(newUserIcon.getContext()).load(medalUrls.get(1)).dontAnimate().into(newUserIcon1);
                    }else {
                        newUserIcon1.setVisibility(GONE);
                    }
                }
            }else {
                newUserIcon.setVisibility(GONE);
                newUserIcon1.setVisibility(GONE);
            }

            if (experLevel > 0) {
                levelView.setVisibility(VISIBLE);
                levelView.setExperLevel(experLevel);
            } else {
                levelView.setVisibility(GONE);
            }

            String chatRoomMessageContent = chatRoomMessage.getContent();
            tvContent.setText(chatRoomMessageContent);
            tvNickName.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    String fromAccount = chatRoomMessage.getFromAccount();
                    long userId = JavaUtil.str2long(fromAccount);
                    if (userId > 0) {
                        new UserInfoDialog(mContext, userId).show();
                    }
                }
            });
        }

        @Override
        public void onClick(View v) {
            ChatRoomMessage chatRoomMessage = (ChatRoomMessage) v.getTag();
            if (chatRoomMessage.getMsgType() != MsgTypeEnum.tip) {
                if (chatRoomMessage.getMsgType() == MsgTypeEnum.text) {
                    account = chatRoomMessage.getFromAccount();
                } else if (chatRoomMessage.getMsgType() == MsgTypeEnum.notification) {
                    account = chatRoomMessage.getFromAccount();
                } else if (chatRoomMessage.getMsgType() == MsgTypeEnum.custom) {
                    CustomAttachment attachment = (CustomAttachment) chatRoomMessage.getAttachment();
                    if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT) {
                        GiftAttachment giftAttachment = (GiftAttachment) attachment;
                        if (giftAttachment != null) {
                            GiftReceiveInfo giftRecieveInfo = giftAttachment.getGiftRecieveInfo();
                            if (giftRecieveInfo != null) {
                                account = giftRecieveInfo.getUid() + "";
                            }
                        }
                    } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT) {
                        MultiGiftAttachment giftAttachment = (MultiGiftAttachment) attachment;
                        account = giftAttachment.getMultiGiftRecieveInfo().getUid() + "";
                    } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_ROOM_TIP) {
                        account = ((RoomTipAttachment) attachment).getUid() + "";
                    } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_ROOM_SHARE) {
                        //分享的执行逻辑放在HomePartyRoomFragment中处理
                        setOnItemClick(v, 0);
                    } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_ROOM_ATTENTION) {
                        if (!isLiked) {
                            if (null != AvRoomDataManager.get().mCurrentRoomInfo) {
                                CoreManager.getCore(IPraiseCore.class).praise(AvRoomDataManager.get().mCurrentRoomInfo.getUid());
                            }
                            //房间-公屏关注
                            StatisticManager.get().onEvent(v.getContext(),
                                    StatisticModel.EVENT_ID_ROOM_PUBLIC_SCREEN_ATTENTION,
                                    StatisticModel.getInstance().getUMAnalyCommonMap(v.getContext()));
                        } else {
                            if (null != AvRoomDataManager.get().mCurrentRoomInfo) {
                                CoreManager.getCore(IPraiseCore.class).cancelPraise(AvRoomDataManager.get().mCurrentRoomInfo.getUid());
                            }
                        }
                    }
                }
                if (TextUtils.isEmpty(account)) {
                    return;
                }
                //公屏点击弹框
                final List<ButtonItem> buttonItems = new ArrayList<>();
                List<ButtonItem> items = ButtonItemFactory.createAllRoomPublicScreenButtonItems(mContext, account);
                if (items == null) {
                    return;
                }
                buttonItems.addAll(items);
                ((BaseMvpActivity) mContext).getDialogManager().showCommonPopupDialog(buttonItems, "取消");
            }
        }
    }
}
