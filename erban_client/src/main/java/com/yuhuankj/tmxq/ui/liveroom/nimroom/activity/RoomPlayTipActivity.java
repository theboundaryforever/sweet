package com.yuhuankj.tmxq.ui.liveroom.nimroom.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.presenter.RoomSettingPresenter;
import com.tongdaxing.xchat_core.room.view.IRoomSettingView;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;

import java.util.List;

/**
 * 设置房间进入时的消息提示
 */
@CreatePresenter(RoomSettingPresenter.class)
public class RoomPlayTipActivity extends BaseMvpActivity<IRoomSettingView, RoomSettingPresenter>
        implements IRoomSettingView {
    private EditText etPlay;

    public static void start(Context context) {
        Intent intent = new Intent(context, RoomPlayTipActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_play_tip);
        initTitleBar(getResources().getString(R.string.room_enter_modify_title));
        initView();
    }

    private void initView() {
        etPlay = (EditText) findViewById(R.id.et_room_play);
        GrowingIO.getInstance().trackEditText(etPlay);
        RoomInfo cRoomInfo = RoomServiceScheduler.getServerRoomInfo();
        if (null != cRoomInfo && !TextUtils.isEmpty(cRoomInfo.getPlayInfo())) {
            etPlay.setText(cRoomInfo.getPlayInfo());
        }
        TextView rightAction = new TextView(this);
        rightAction.setTextColor(getResources().getColor(R.color.color_1A1A1A));
        rightAction.setText(getResources().getString(R.string.save));
        rightAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });
        if (mTitleBar != null) {
            mTitleBar.mRightLayout.addView(rightAction);
        }
    }

    private void save() {
        String playInfo = etPlay.getText().toString();
        if (null == playInfo) {
            finish();
            return;
        }
        playInfo = playInfo.replaceAll("%", " ");
        RoomInfo roomInfo = RoomServiceScheduler.getServerRoomInfo();
        if (null == roomInfo) {
            finish();
            return;
        }
        getDialogManager().showProgressDialog(this, getString(R.string.network_loading));
        getMvpPresenter().updateRoomInfo(roomInfo.getTitle(), roomInfo.getRoomDesc(),
                roomInfo.getRoomPwd(), roomInfo.getRoomTag(), roomInfo.getTagId(),
                roomInfo.getBackPic(), roomInfo.getBackName(), roomInfo.getGiftEffectSwitch(),
                roomInfo.getCharmSwitch(), roomInfo.getRoomNotice(), playInfo);
    }

    @Override
    public void updateRoomInfoSuccess(RoomInfo roomInfo) {
        getDialogManager().dismissDialog();
        toast(R.string.update_success);
        finish();
    }

    @Override
    public void updateRoomInfoFail(String error) {
        getDialogManager().dismissDialog();
        toast(TextUtils.isEmpty(error) ? getResources().getString(R.string.network_error) : error);
    }

    @Override
    public void onResultRequestTagAllSuccess(List<TabInfo> tabInfoList) {
        //忽略
    }

    @Override
    public void onResultRequestTagAllFail(String error) {
        //忽略
    }
}
