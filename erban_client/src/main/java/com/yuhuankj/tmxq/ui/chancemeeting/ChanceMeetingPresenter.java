package com.yuhuankj.tmxq.ui.chancemeeting;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.match.bean.MicroMatch;

import java.util.List;
import java.util.Map;

public class ChanceMeetingPresenter extends AbstractMvpPresenter<ChanceMeetingView> {

    private final String TAG = ChanceMeetingPresenter.class.getSimpleName();

    public void getMeetingData(int size){
        Map<String, String> params = OkHttpManager.getDefaultParam();
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        params.put("size", size + "");
        params.put("uid", uid + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket()+"");
        OkHttpManager.getInstance().getRequest(UriProvider.getChanceMeetingUrl(), params, new OkHttpManager.MyCallBack<ServiceResult<List<MicroMatch>>>() {
            @Override
            public void onError(Exception e) {
                LogUtils.d(TAG, "getMeetingData-onError");
                e.printStackTrace();
                if(null != getMvpView()){
                    getMvpView().onGetMeetingData(false, BasicConfig.INSTANCE.getAppContext().getResources().getString(R.string.network_error_retry), null);
                }
            }

            @Override
            public void onResponse(ServiceResult<List<MicroMatch>> response) {
                LogUtils.d(TAG, "getMeetingData-onResponse");
                if(null != getMvpView()){
                    getMvpView().onGetMeetingData(200 == response.getCode(),response.getMessage(),response.getData());
                }
            }
        });
    }

    public void reportUser(long targetUid,int reportType){
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("reportType", reportType + "");
        params.put("type", "1");
        params.put("phoneNo", CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo() == null ? ""
                : CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getPhone());
        params.put("reportUid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("uid", targetUid + "");
        OkHttpManager.getInstance().postRequest(UriProvider.reportUserUrl(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                LogUtils.d(TAG, "reportUser-onError");
                e.printStackTrace();
                if(null != getMvpView()){
                    getMvpView().onReportUserResponse(false,e.getMessage());
                }
            }

            @Override
            public void onResponse(Json response) {
                LogUtils.d(TAG, "onResponse-response:" + response);
                if(null != getMvpView()){
                    getMvpView().onReportUserResponse(response.num("code") == 200,response.str("message"));
                }

            }
        });
    }

    public void admireUser(long admireUid, int type) {
        LogUtils.d(TAG, "admireUser-admireUid:" + admireUid + " type:" + type);
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("admireUid", String.valueOf(admireUid));
        //1点赞某人，2取消点赞某人
        params.put("type", String.valueOf(type));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket() + "");
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        OkHttpManager.getInstance().postRequest(UriProvider.getAdmireUserUrl(), params, new OkHttpManager.MyCallBack<ServiceResult<String>>() {
            @Override
            public void onError(Exception e) {
                LogUtils.d(TAG, "reportUser-onError");
                e.printStackTrace();
                if (null != getMvpView()) {
                    getMvpView().onAdmireOperaResponse(false, e.getMessage(), admireUid, 1 != type);
                }
            }

            @Override
            public void onResponse(ServiceResult<String> response) {
                LogUtils.d(TAG, "onResponse-response:" + response);
                if (null != getMvpView()) {
                    if (null != response) {
                        getMvpView().onAdmireOperaResponse(response.isSuccess(), response.getMessage(), admireUid, 1 == type);
                    } else {
                        getMvpView().onAdmireOperaResponse(false, null, admireUid, 1 != type);
                    }
                }

            }
        });
    }

}
