package com.yuhuankj.tmxq.ui.signAward.view.adapter;

import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.noober.background.view.BLView;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.signAward.model.SignInAwardInfo;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

/**
 * @author weihaitao
 * @date 2019年5月27日 15:47:18
 */
public class SignAwardRecvAdapter extends BaseQuickAdapter<SignInAwardInfo, BaseViewHolder> {

    private int imvIconWidthAndHeight = 0;
    private int imvIconBoxWidth = 0;

    private int signDay = 0;

    private BLView mBlvNoGet;
    private BLView mBlvGet;
    private ImageView mIvRecv;
    private TextView mTvRecv;
    private BLView mBlvGot;
    private ImageView mIvGot;


    public SignAwardRecvAdapter(int signDay, List<SignInAwardInfo> data) {
        super(R.layout.item_sign_award_recv, data);
        imvIconWidthAndHeight = DisplayUtility.dp2px(mContext, 23);
        imvIconBoxWidth = DisplayUtility.dp2px(mContext, 48);
        this.signDay = signDay;
    }

    @Override
    protected void convert(BaseViewHolder helper, SignInAwardInfo item) {
        mBlvNoGet = helper.getView(R.id.blvNoGet);
        mBlvGet = helper.getView(R.id.blvGet);
        mIvRecv = helper.getView(R.id.ivRecv);
        mTvRecv = helper.getView(R.id.tvRecv);
        mBlvGot = helper.getView(R.id.blvGot);
        mIvGot = helper.getView(R.id.ivGot);
        //是否已经领取
        //1.再判断是否已经完成
        //2.先判断是否顺序小于连签天数
        if (item.getMissionStatus() == 3) {
            //已经领过了
            mIvGot.setVisibility(View.VISIBLE);
            mBlvGot.setVisibility(View.VISIBLE);
            mBlvGet.setVisibility(View.VISIBLE);
            mBlvNoGet.setVisibility(View.GONE);
            mTvRecv.setTextColor(Color.parseColor("#D1D1D1"));
        } else {
            if (item.getSeq() <= signDay) {
                //当前待领取状态
                mIvGot.setVisibility(View.GONE);
                mBlvGot.setVisibility(View.GONE);
                mBlvGet.setVisibility(View.VISIBLE);
                mBlvNoGet.setVisibility(View.GONE);
                mTvRecv.setTextColor(Color.WHITE);
            } else {
                //未领取状态
                mIvGot.setVisibility(View.GONE);
                mBlvGot.setVisibility(View.GONE);
                mBlvGet.setVisibility(View.GONE);
                mBlvNoGet.setVisibility(View.VISIBLE);
                mTvRecv.setTextColor(Color.parseColor("#D1D1D1"));
            }
        }

        //采用赠品名称显示
        mTvRecv.setText(item.getFreebiesName());
        //神秘宝箱，不展示名称，甜豆和勋章才展示赠品名称
        mTvRecv.setVisibility(item.getFreebiesType() == 1 ? View.GONE : View.VISIBLE);

        //直接展示赠品图片，圆角由图片自身决定
        if (GlideContextCheckUtil.checkContextUsable(mIvRecv.getContext())) {
            ImageLoadUtils.loadImage(mIvRecv.getContext(), ImageLoadUtils.toThumbnailUrl(imvIconWidthAndHeight,
                    imvIconWidthAndHeight, item.getMissionIcon()), mIvRecv);
        }
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) mIvRecv.getLayoutParams();
        if (item.getFreebiesType() == 1) {
            lp.width = imvIconBoxWidth;
            lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
            mIvRecv.setLayoutParams(lp);
            mIvRecv.setAdjustViewBounds(true);

        } else {
            lp.width = imvIconWidthAndHeight;
            lp.height = imvIconWidthAndHeight;
            mIvRecv.setLayoutParams(lp);
            mIvRecv.setAdjustViewBounds(false);
        }
    }
}