package com.yuhuankj.tmxq.ui.liveroom.imroom.pk;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_core.pk.bean.PkVoteInfo;

import java.util.Map;

/**
 * 文件描述：新的PK模块的model
 *
 * @auther：zwk
 * @data：2019/7/8
 */
public class RoomPKModel extends BaseMvpModel {


    /**
     * 获取PK信息
     *
     * @param roomId
     * @param isUpdateVote 是否是更新投票信息
     * @param callBack
     */
    public void getPkInfo(String roomId, boolean isUpdateVote, CallBack<PkVoteInfo> callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomId", roomId);
        OkHttpManager.getInstance().postRequest(UriProvider.getPkResult(), params, new OkHttpManager.MyCallBack<ServiceResult<PkVoteInfo>>() {
            @Override
            public void onError(Exception e) {
                if (callBack != null) {
                    callBack.onFail(OkHttpManager.DEFAULT_CODE_ERROR, e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<PkVoteInfo> response) {
                if (callBack != null) {
                    if (response.isSuccess()) {
                        callBack.onSuccess(response.getData());
                    } else {
                        callBack.onFail(response.getCode(), response.getMessage());
                    }
                }
            }
        });

    }

    /**
     * 发起PK接口
     *
     * @param roomId
     * @param info
     * @param callBack
     */
    public void savePk(long roomId, PkVoteInfo info, CallBack<PkVoteInfo> callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomId", roomId + "");
        params.put("pkType", info.getPkType() + "");
        params.put("targetUidList", info.getTargetUidList());
        params.put("title", info.getTitle());
        params.put("expireSeconds", info.getExpireSeconds() + "");
        OkHttpManager.getInstance().postRequest(UriProvider.savePk(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
//                notifyClients(IPKCoreClient.class, IPKCoreClient.METHOD_ON_SAVE_PK_FAIL, e.getMessage());
                if (callBack != null) {
                    callBack.onFail(OkHttpManager.DEFAULT_CODE_ERROR, e.getMessage());
                }
            }

            @Override
            public void onResponse(Json response) {
                if (callBack != null) {
                    int code = response.num("code");
                    if (code != 200) {
//                    notifyClients(IPKCoreClient.class, IPKCoreClient.METHOD_ON_SAVE_PK_FAIL, response.str("message", "网络异常"));
                        callBack.onFail(code, response.str("message"));
                    } else {
                        Json data = response.json("data");
                        if (data != null) {
                            long pkId = data.num_l("id", 0);
                            if (pkId > 0) {
                                info.setVoteId(pkId);
                                callBack.onSuccess(info);
//                                notifyClients(IPKCoreClient.class, IPKCoreClient.METHOD_ON_SAVE_PK, info);
                            } else {
//                                notifyClients(IPKCoreClient.class, IPKCoreClient.METHOD_ON_SAVE_PK_FAIL, "PK id 异常！");
                                callBack.onFail(OkHttpManager.DEFAULT_CODE_ERROR, "PK id 异常！");
                            }
                        }
                    }
                }
            }
        });
    }


    /**
     * 取消房间PK
     *
     * @param roomId
     */
    public void cancelPK(long roomId) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomId", roomId + "");
        OkHttpManager.getInstance().postRequest(UriProvider.cancelPk(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
            }

            @Override
            public void onResponse(Json response) {
            }
        });
    }

    /**
     * 给对应用户投票
     *
     * @param voteUid
     * @param callBack
     */
    public void pkVote(String roomId, String voteUid, CallBack<PkVoteInfo> callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomId", roomId);
        params.put("voteUid", voteUid + "");
        OkHttpManager.getInstance().postRequest(UriProvider.sendPkVote(), params, new OkHttpManager.MyCallBack<ServiceResult<PkVoteInfo>>() {
            @Override
            public void onError(Exception e) {
                if (callBack != null) {
                    callBack.onFail(OkHttpManager.DEFAULT_CODE_ERROR, "投票失败！");
                }
            }

            @Override
            public void onResponse(ServiceResult<PkVoteInfo> response) {
                if (callBack != null) {
                    if (response.isSuccess()) {
                        callBack.onSuccess(response.getData());
                    } else {
                        callBack.onFail(response.getCode(), response.getMessage());
                    }
                }
            }
        });
    }
}
