package com.yuhuankj.tmxq.ui.liveroom.nimroom.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.TimeUtils;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.pk.IPKCoreClient;
import com.tongdaxing.xchat_core.pk.IPkCore;
import com.tongdaxing.xchat_core.pk.bean.PkVoteInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.adapter.PkHistoryAdapter;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.bean.PkHistoryEnitity;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.bean.PkHistoryHeadEnitity;

import java.util.ArrayList;
import java.util.List;

/**
 * PK历史记录页面
 *
 * @author zwk
 */
public class PkHistoryActivity extends BaseActivity implements OnRefreshLoadmoreListener {
    protected RecyclerView mRecyclerView;
    protected SmartRefreshLayout mRefreshLayout;
    protected PkHistoryAdapter mAdapter;
    private LinearLayout llEmply;
    private int mPage = Constants.PAGE_START;


    public static void start(Context context) {
        Intent intent = new Intent(context, PkHistoryActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pk_history);
        initTitleBar("记录");
        initView();
        initListener();
        initData();
    }

    private void initView() {
        mRefreshLayout = findView(R.id.refresh_layout);
        mRecyclerView = findView(R.id.recyclerView);
        llEmply = findView(R.id.llEmply);
        mRecyclerView.setLayoutManager(new RecyclerViewNoBugLinearLayoutManager(this));
        mAdapter = new PkHistoryAdapter(null);
        mRecyclerView.setAdapter(mAdapter);
    }


    private void initListener() {
        mRefreshLayout.setOnRefreshLoadmoreListener(this);
    }

    //获取数据
    private ArrayList<MultiItemEntity> getValidData(List<PkVoteInfo> pkDatas) {
        ArrayList<MultiItemEntity> datas = new ArrayList<>();
        try {
            for (PkVoteInfo voteInfo : pkDatas) {//设置外层数据
                String time = TimeUtils.getTimeStringFromMillis(voteInfo.getCreateTime());
                PkHistoryHeadEnitity pkHeader = new PkHistoryHeadEnitity(voteInfo.getTitle(), time);
                for (PkVoteInfo.PkUser pkUser : voteInfo.getPkList()) {
                    PkHistoryEnitity pkItem = new PkHistoryEnitity();
                    pkItem.setCount(pkUser.getVoteCount() + "");
                    pkItem.setNickName(pkUser.getNick());
                    pkItem.setPortrait(pkUser.getAvatar());
                    pkHeader.addSubItem(pkItem);
                }
                datas.add(pkHeader);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
        return datas;
    }

    private void initData() {
        CoreManager.getCore(IPkCore.class).getPkHistoryList(RoomServiceScheduler.getCurrentRoomInfo() == null ? 0 : RoomServiceScheduler.getCurrentRoomInfo().getRoomId(), mPage);
    }

    @CoreEvent(coreClientClass = IPKCoreClient.class)
    public void onPkHistoryList(List<PkVoteInfo> pkVoteInfos) {
        if (mPage == Constants.PAGE_START) {
            mRefreshLayout.finishRefresh();
            if (ListUtils.isListEmpty(pkVoteInfos)) {
                llEmply.setVisibility(View.VISIBLE);
            } else {
                llEmply.setVisibility(View.GONE);
                mAdapter.setNewData(getValidData(pkVoteInfos));
            }
        } else {
            mRefreshLayout.finishLoadmore();
            if (!ListUtils.isListEmpty(pkVoteInfos)) {
                mAdapter.addData(getValidData(pkVoteInfos));
            }
        }
    }

    @CoreEvent(coreClientClass = IPKCoreClient.class)
    public void onPkHistoryListFail(String error) {
        if (mPage == Constants.PAGE_START) {
            mRefreshLayout.finishRefresh(0);
            showNetworkErr();
        } else {
            mRefreshLayout.finishLoadmore(0);
            hideStatus();
        }
    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        mPage++;
        initData();
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        mPage = Constants.PAGE_START;
        initData();
    }
}
