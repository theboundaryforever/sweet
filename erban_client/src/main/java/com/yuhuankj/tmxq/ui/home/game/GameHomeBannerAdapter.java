package com.yuhuankj.tmxq.ui.home.game;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jude.rollviewpager.adapter.StaticPagerAdapter;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.ButtonUtils;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

/**
 * @author liaoxy
 * @Description:游戏大厅顶部banner的Adapter
 * @date 2019/2/14 14:06
 */
public class GameHomeBannerAdapter extends StaticPagerAdapter {
    private Context context;

    public List<GameBannerEnitity> getBannerInfoList() {
        return bannerInfoList;
    }

    private List<GameBannerEnitity> bannerInfoList;
    private LayoutInflater mInflater;
    private OnGameBannerClickListener onGameBannerClickListener;

    private int screenWidth = 0;
    private int bannerHeight = 0;

    public GameHomeBannerAdapter(List<GameBannerEnitity> bannerInfoList, Context context) {
        this.context = context;
        this.bannerInfoList = bannerInfoList;
        mInflater = LayoutInflater.from(context);
        screenWidth = DisplayUtility.getScreenWidth(context);
        bannerHeight = screenWidth * 356 / 1000;
    }

    public void setBannerInfoList(List<GameBannerEnitity> bannerInfoList) {
        this.bannerInfoList = bannerInfoList;
    }

    @Override
    public View getView(ViewGroup container, int position) {
        final GameBannerEnitity bannerEnitity = bannerInfoList.get(position);
        ImageView imgBanner = (ImageView) mInflater.inflate(R.layout.banner_page_item, container, false);
        ViewGroup.LayoutParams lp = imgBanner.getLayoutParams();
        lp.height = bannerHeight;
        lp.width = screenWidth;
        if(GlideContextCheckUtil.checkContextUsable(context)){
            ImageLoadUtils.loadBannerRoundBg(context,
                    ImageLoadUtils.toThumbnailUrl(screenWidth,bannerHeight,bannerEnitity.getBannerPic()), imgBanner);
        }
        imgBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick(v.getId())) {
                    return;
                }
                if (null != onGameBannerClickListener) {
                    onGameBannerClickListener.onGameBannerClicked(bannerEnitity);
                }
            }
        });
        return imgBanner;
    }

    @Override
    public int getCount() {
        if (bannerInfoList == null) {
            return 0;
        } else {
            return bannerInfoList.size();
        }
    }

    public void setOnGameBannerClickListener(OnGameBannerClickListener onGameBannerClickListener) {
        this.onGameBannerClickListener = onGameBannerClickListener;
    }


    public interface OnGameBannerClickListener {
        void onGameBannerClicked(GameBannerEnitity bannerEnitity);
    }
}
