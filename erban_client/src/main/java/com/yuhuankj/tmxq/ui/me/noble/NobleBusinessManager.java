package com.yuhuankj.tmxq.ui.me.noble;


import android.graphics.Color;
import android.text.TextUtils;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tongdaxing.erban.libcommon.utils.JavaUtil;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.yuhuankj.tmxq.R;

import java.util.Map;

/**
 * 贵族特权业务管理器
 * 业务贵族参考:https://jw9syz.axshare.com/#g=1&p=贵族详情页
 * <p>
 * nobleLevel,1-7分别表示骑士，男爵，子爵，伯爵，侯爵，公爵，国王
 * <p>
 * 以下特权判断方法，用于客户端的业务逻辑，部分后端处理的逻辑不包含在内
 */
public class NobleBusinessManager {

    private static final String TAG = NobleBusinessManager.class.getSimpleName();

    /**
     * 贵族表情，根据最低贵族等级要求，获取对应的角标RES ID
     *
     * @param minNobleLevel
     * @return
     * @deprecated 根据后端接口的vipIcon来
     */
    public static int getNobleEmojiCornerMarkerResId(int minNobleLevel) {
        //贵族表情，角标从男爵开始算起，也即minNobleLevel>=2
        return 0;
    }

    /**
     * 是否可发送贵族表情，目前仅男爵及以上贵族才可以使用贵族表情
     *
     * @param minNobleLevel
     * @return
     */
    public static boolean canSendNobleEmoji(int minNobleLevel) {
        return minNobleLevel >= 2;
    }

    /**
     * 贵族头饰，根据最低贵族等级要求，获取对应的角标RES ID
     *
     * @param minNobleLevel
     * @return
     * @deprecated 根据后端接口的vipIcon来
     */
    public static int getNobleHeadWearCornerMarkerResId(int minNobleLevel) {
        //贵族头饰，角标从男爵开始算起，也即minNobleLevel>=2
        return 0;
    }

    /**
     * 房间背景，根据最低贵族等级要求，获取对应的角标RES ID
     *
     * @param nobleLevel
     * @return
     * @deprecated 根据后端接口的vipIcon来
     */
    public static int getNobleRoomBgCornerMarkerResId(int nobleLevel) {
        //房间背景，角标从伯爵开始算起，也即nobleLevel>=4
        return 0;
    }

    /**
     * 聊天气泡
     *
     * @param nobleLevel
     * @return
     */
    public static int getNobleRoomMsgBubbleResId(int nobleLevel) {
        //聊天气泡，角标从伯爵开始算起，也即nobleLevel>=4
        if (4 == nobleLevel) {
            return R.drawable.bg_room_noble_bubble_earl;
        } else if (5 == nobleLevel) {
            return R.drawable.bg_room_noble_bubble_marquis;
        } else if (6 == nobleLevel) {
            return R.drawable.bg_room_noble_bubble_duke;
        } else if (7 == nobleLevel) {
            return R.drawable.bg_room_noble_bubble_king1;
        } else {
            return R.drawable.shape_room_message_txt_bg;
        }
    }

    /**
     * 进房隐身，
     *
     * @param nobleLevel
     * @return
     */
    public static boolean enterRoomInvisible(int nobleLevel) {
        //进房隐身，从公爵开始算起，也即nobleLevel>=6
        return nobleLevel >= 6;
    }

    /**
     * 全站广播，
     *
     * @param nobleLevel
     * @return
     */
    public static boolean hasAllAppBroadcast(int nobleLevel) {
        //全站广播，仅国王享有特权，也即nobleLevel == 7;
        return nobleLevel == 7;
    }

    /**
     * 根据等级获取勋章图标，用于展示在订单确认页
     *
     * @param nobleLevel
     * @return
     */
    public static int getNobleOrderMedalResId(int nobleLevel) {
        //1-7分别表示骑士，男爵，子爵，伯爵，侯爵，公爵，国王
        if (1 == nobleLevel) {
            return R.mipmap.ic_order_medal_1;
        } else if (2 == nobleLevel) {
            return R.mipmap.ic_order_medal_2;
        } else if (3 == nobleLevel) {
            return R.mipmap.ic_order_medal_3;
        } else if (4 == nobleLevel) {
            return R.mipmap.ic_order_medal_4;
        } else if (5 == nobleLevel) {
            return R.mipmap.ic_order_medal_5;
        } else if (6 == nobleLevel) {
            return R.mipmap.ic_order_medal_6;
        } else if (7 == nobleLevel) {
            return R.mipmap.ic_order_medal_7;
        } else {
            return 0;
        }
    }

    /**
     * 是否可显示进房隐身开关，目前仅公爵和国王可见
     *
     * @return
     */
    public static boolean showEnterRoomInvisiableSwitch(int nobleLevel, int nobleDate) {
        //1-7分别表示骑士，男爵，子爵，伯爵，侯爵，公爵，国王
        return nobleLevel >= 6 && nobleDate > 0;
    }

    /**
     * 是否防踢、防拉黑
     *
     * @param nobleLevel
     * @param nobleDate
     * @return
     */
    public static boolean isAntiKickOff(int nobleLevel, int nobleDate) {
        //1-7分别表示骑士，男爵，子爵，伯爵，侯爵，公爵，国王
        return nobleLevel == 7 && nobleDate > 0;
    }

    /**
     * 根据等级获取入场特效背景图标
     *
     * @param nobleLevel
     * @return
     */
    public static int getNobleEnterRoomAnimBgResId(int nobleLevel, int nobleDate) {
        //1-7分别表示骑士，男爵，子爵，伯爵，侯爵，公爵，国王
        if (nobleDate <= 0) {
            return 0;
        }
        if (3 == nobleLevel) {
            return R.drawable.bg_enter_noble_anim_3;
        } else if (4 == nobleLevel) {
            return R.drawable.bg_enter_noble_anim_4;
        } else if (5 == nobleLevel) {
            return R.drawable.bg_enter_noble_anim_5;
        } else if (6 == nobleLevel) {
            return R.drawable.bg_enter_noble_anim_6;
        } else if (7 == nobleLevel) {
            return R.drawable.bg_enter_noble_anim_7;
        } else {
            return 0;
        }
    }

    /**
     * 贵族进房隐身时的昵称
     *
     * @param nobleLevel
     * @param isInvisible
     * @param nobleDate
     * @param nick
     * @return
     */
    public static String getNobleRoomNick(int nobleLevel, boolean isInvisible, int nobleDate, String nick) {
        //1-7分别表示骑士，男爵，子爵，伯爵，侯爵，公爵，国王
        //1.进房，右上角入场动画，及公屏消息
        boolean isMysteryMan = nobleLevel >= 6 && isInvisible && nobleDate > 0;
        return isMysteryMan ?
                BasicConfig.INSTANCE.getAppContext().getResources().getString(R.string.king_room_invisiable_nick) : nick;
    }

    /**
     * 是否神秘人
     *
     * @param nobleLevel
     * @param isInvisible
     * @param nobleDate
     * @return
     */
    public static boolean isNobleMysteryMan(int nobleLevel, boolean isInvisible, int nobleDate) {
        boolean isMysteryMan = nobleLevel >= 6 && isInvisible && nobleDate > 0;
        return isMysteryMan;
    }

    /**
     * 房间贵族神秘人昵称高亮色值
     *
     * @param isInvisible
     * @param nickColor
     * @param nick
     * @return
     */
    public static int getNobleRoomNickColor(int nobleLevel, int nobleDate, boolean isInvisible, int nickColor, String nick) {
        boolean isMysteryMan = nobleLevel >= 6 && nobleDate > 0 && isInvisible;
        return isMysteryMan ?
                0xffFAD23E : nickColor;
    }

    /**
     * 是否展示全站续费/开通贵族通知
     *
     * @param nobleLevel
     * @return
     */
    public static boolean showOpenNobleNotifyAnim(int nobleLevel) {
        //公爵及国王开通/续费贵族后会在全站进行通知
        return nobleLevel >= 6;
    }

    /**
     * 开通续费贵族全站通知背景图资源ID
     *
     * @param nobleLevel
     * @return
     */
    public static int getOpenNobleNotifyBgResId(int nobleLevel) {
        //公爵及国王开通/续费贵族后会在全站进行通知
        if (6 == nobleLevel) {
            return R.mipmap.bg_room_open_noble_gongjue_notify;
        } else if (7 == nobleLevel) {
            return R.mipmap.bg_room_open_noble_guowang_notify;
        } else {
            return 0;
        }
    }

    /**
     * 全站开通/续费通知，头像光圈
     *
     * @param nobleLevel
     * @return
     */
    public static int getOpenNobleNotifyHeadResId(int nobleLevel) {
        //公爵及国王开通/续费贵族后会在全站进行通知
        if (6 == nobleLevel) {
            return R.drawable.bg_room_open_duke_notify_header;
        } else if (7 == nobleLevel) {
            return R.drawable.bg_room_open_king_notify_header;
        } else {
            return 0;
        }
    }

    /**
     * 全站开通/续费贵族通知，背景色值
     *
     * @param nobleLevel
     * @return
     */
    public static int getOpenNobleNotifyColor(int nobleLevel) {
        //公爵及国王开通/续费贵族后会在全站进行通知
        if (6 == nobleLevel) {
            return 0xffFA9927;
        } else if (7 == nobleLevel) {
            return 0xffFFF100;
        } else {
            return Color.WHITE;
        }
    }

    /**
     * 麦上光晕
     *
     * @param nobleLevel
     * @return
     */
    public static int getNobleMicWaveColor(int nobleLevel) {
        ////1-7分别表示骑士，男爵，子爵，伯爵，侯爵，公爵，国王
        if (5 == nobleLevel) {
            return 0xffDA80FF;
        } else if (6 == nobleLevel) {
            return 0xffFFBB6B;
        } else if (7 == nobleLevel) {
            return 0xffFFF60B;
        } else {
            return 0x77ffffff;
        }
    }


    public static long getNobleUserUID(int nobleLevel, int nobleDate, boolean isInsiviable,
                                       long inVisiableUid, long uid) {

        if (nobleDate > 0 && nobleLevel >= 6 && isInsiviable && inVisiableUid > 0L) {
            return inVisiableUid;
        }else{
            return uid;
        }
    }

    public static long getNobleRoomUID(ChatRoomMember chatRoomMember, String account) {
        if (null == chatRoomMember) {
            return TextUtils.isEmpty(account) ? 0L : JavaUtil.str2int(account);
        }
        Map<String, Object> extenMap = chatRoomMember.getExtension();
        boolean isInvisiable = false;
        int medalId = 0;
        int medalDate = 0;
        long inVisiableUid = 0L;
        if (null != extenMap) {
            if (extenMap.containsKey(Constants.USER_MEDAL_ID)) {
                medalId = (int) extenMap.get(Constants.USER_MEDAL_ID);
            }
            if (extenMap.containsKey(Constants.USER_MEDAL_DATE)) {
                medalDate = (int) extenMap.get(Constants.USER_MEDAL_DATE);
            }
            if (extenMap.containsKey(Constants.NOBLE_INVISIABLE_ENTER_ROOM)) {
                isInvisiable = (int) extenMap.get(Constants.NOBLE_INVISIABLE_ENTER_ROOM) == 1;
            }
            if (extenMap.containsKey(Constants.NOBLE_INVISIABLE_UID)) {
                inVisiableUid = (long) extenMap.get(Constants.NOBLE_INVISIABLE_UID);
            }
        }
        return com.tongdaxing.xchat_core.noble.NobleBusinessManager.getNobleRoomUid(medalId, isInvisiable, medalDate,
                TextUtils.isEmpty(chatRoomMember.getAccount()) ? 0L : Long.valueOf(chatRoomMember.getAccount()), inVisiableUid);
    }

    public static String getNobleRoomAvatarUrl(int nobleLevel, boolean isInvisible, int nobleDate, String avatarUrl) {
        //1-7分别表示骑士，男爵，子爵，伯爵，侯爵，公爵，国王
        //0.进房，右上角入场动画，及公屏消息 --  右上角可以，但是公屏，用的是云信的，需要后端同步修改云信的信息
        boolean isMysteryMan = nobleLevel >= 6 && isInvisible && nobleDate > 0;
        return isMysteryMan ? UriProvider.getNobleMysteriousAvatarUrl() : avatarUrl;
    }

    public static long getNobleUserInfoDialogErbanNo(int nobleLevel, boolean isInvisible, int nobleDate, long invisibleUid, long erbanNo) {
        boolean isMysteryMan = nobleLevel >= 6 && isInvisible && nobleDate > 0;
        return isMysteryMan ? invisibleUid : erbanNo;
    }

    public static boolean isShowLoverUpMicAnim(int firstNobleLevel, boolean firstIsInvisible,
                                               int firstNobleDate, int secondNobleLevel,
                                               boolean secondIsInvisible, int secondNobleDate) {
        boolean isFirstMysteryMan = firstNobleLevel >= 6 && firstIsInvisible && firstNobleDate > 0;
        boolean isSecondMysteryMan = secondNobleLevel >= 6 && secondIsInvisible && secondNobleDate > 0;
        LogUtils.d(TAG, "isShowLoverUpMicAnim-firstNobleLevel:" + firstNobleLevel + " firstIsInvisible:"
                + firstIsInvisible + " firstNobleDate:" + firstNobleDate);
        LogUtils.d(TAG, "isShowLoverUpMicAnim-secondNobleLevel:" + secondNobleLevel + " secondIsInvisible:"
                + secondIsInvisible + " secondNobleDate:" + secondNobleDate);
        LogUtils.d(TAG, "isShowLoverUpMicAnim-isFirstMysteryMan:" + isFirstMysteryMan + " isSecondMysteryMan:"
                + isSecondMysteryMan);
        return !isFirstMysteryMan && !isSecondMysteryMan;
    }
}
