package com.yuhuankj.tmxq.ui.find;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.fragment.BaseFragment;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.fragment.PublicChatFragment;
import com.yuhuankj.tmxq.ui.ranklist.TabPageAdapter;
import com.yuhuankj.tmxq.ui.room.adapter.CommonMagicIndicatorAdapter;
import com.yuhuankj.tmxq.widget.magicindicator.MagicIndicator;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.UIUtil;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.commonnavigator.CommonNavigator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liaoxy
 * @Description:发现模块主页
 * @date 2019/5/24 10:29
 */
public class FindHomeFragment extends BaseFragment {

    private MagicIndicator mgindtorTitle;
    private List<TabInfo> tabList;
    private List<Fragment> fragments;
    private ViewPager vpContent;
    private TabPageAdapter tabPageAdapter;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_find_home;
    }

    @Override
    public void onFindViews() {
        mgindtorTitle = mView.findViewById(R.id.mgindtorTitle);
        vpContent = mView.findViewById(R.id.vpContent);
    }

    @Override
    public void onSetListener() {

    }

    @Override
    public void initiate() {
        initMagicIndicator();
        initVpPages();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            int curItem = 0;
            if (null != vpContent) {
                curItem = vpContent.getCurrentItem();
            }

            if (fragments != null && fragments.size() > curItem) {
                fragments.get(curItem).setUserVisibleHint(true);
            }
        }
    }

    //初始化标题
    private void initMagicIndicator() {
        TabInfo tabInfo = new TabInfo(0, "发现");
        TabInfo tabInfo1 = new TabInfo(1, "广场");
        TabInfo tabInfo2 = new TabInfo(2, "附近");
        tabList = new ArrayList<>();
        tabList.add(tabInfo);
        tabList.add(tabInfo1);
        tabList.add(tabInfo2);
        CommonNavigator commonNavigator = new CommonNavigator(getContext());
        commonNavigator.setSmoothScroll(false);
        CommonMagicIndicatorAdapter magicIndicatorAdapter = new CommonMagicIndicatorAdapter(mContext,
                tabList, UIUtil.dip2px(mContext, 3), UIUtil.dip2px(mContext, 12));
        magicIndicatorAdapter.setOnItemSelectListener(new CommonMagicIndicatorAdapter.OnItemSelectListener() {
            @Override
            public void onItemSelect(int position) {
                vpContent.setCurrentItem(position);
            }
        });
        commonNavigator.setAdapter(magicIndicatorAdapter);
        mgindtorTitle.setNavigator(commonNavigator);
    }

    //初始化Fragment页
    private void initVpPages() {
        fragments = new ArrayList<>();
        fragments.add(new DiscoveryFragment());
        PublicChatFragment publicChatFragment = new PublicChatFragment();
        Bundle args = new Bundle();
        args.putBoolean(PublicChatFragment.ACTION_SHOW_IN_ROOM, false);
        publicChatFragment.setArguments(args);
        fragments.add(publicChatFragment);
        fragments.add(new CityWideFragment());
        tabPageAdapter = new TabPageAdapter(getChildFragmentManager(), fragments);
        vpContent.setAdapter(tabPageAdapter);

        vpContent.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                mgindtorTitle.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {
                mgindtorTitle.onPageSelected(position);
                if (position == 2) {
                    //统计-附近-进入附近
                    StatisticManager.get().onEvent(getContext(),
                            StatisticModel.EVENT_ID_CLICK_NEARBY_ENTER,
                            StatisticModel.getInstance().getUMAnalyCommonMap(getContext()));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                mgindtorTitle.onPageScrollStateChanged(state);
            }
        });
    }

    public void selectPage(int position) {
        try {
            vpContent.setCurrentItem(position, false);
            mgindtorTitle.onPageSelected(position);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
