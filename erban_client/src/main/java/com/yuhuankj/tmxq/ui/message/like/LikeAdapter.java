package com.yuhuankj.tmxq.ui.message.like;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.libcommon.utils.TimeUtils;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.ArrayList;
import java.util.List;

public class LikeAdapter extends BaseQuickAdapter<LikeEnitity, BaseViewHolder> {

    private List<LikeEnitity> datasNoshowRedPoint;

    public LikeAdapter(List<LikeEnitity> data) {
        super(R.layout.item_like, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, LikeEnitity item) {
        ImageView imvIcon = helper.getView(R.id.imvIcon);
        TextView tvTitle = helper.getView(R.id.tvTitle);
        TextView tvDesc = helper.getView(R.id.tvDesc);
        TextView tvTime = helper.getView(R.id.tvTime);
        View vRedPoint = helper.getView(R.id.vRedPoint);

        if (item.getIsRead() == 0) {
            //未读消息
            if (datasNoshowRedPoint != null && datasNoshowRedPoint.contains(item)) {
                vRedPoint.setVisibility(View.INVISIBLE);
            } else {
                vRedPoint.setVisibility(View.VISIBLE);
            }
        } else {
            //已读消息
            vRedPoint.setVisibility(View.INVISIBLE);
        }
        ImageLoadUtils.loadImage(mContext, item.getAvatar(), imvIcon, R.drawable.default_user_head);
        tvTitle.setText(item.getNick());
        tvDesc.setText(item.getAdmireText());
        tvTime.setText(TimeUtils.getPostTimeString(mContext, item.getCreateDate(), true, true));
    }

    public void addNoshowRedPoint(LikeEnitity data) {
        if (datasNoshowRedPoint == null) {
            datasNoshowRedPoint = new ArrayList<>();
        }
        if (!datasNoshowRedPoint.contains(data)) {
            datasNoshowRedPoint.add(data);
        }
    }

    public boolean isRead(LikeEnitity data) {
        if (datasNoshowRedPoint != null && datasNoshowRedPoint.contains(data)) {
            return true;
        }
        return false;
    }
}