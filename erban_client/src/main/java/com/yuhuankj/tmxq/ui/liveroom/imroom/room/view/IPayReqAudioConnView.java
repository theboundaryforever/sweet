package com.yuhuankj.tmxq.ui.liveroom.imroom.room.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

/**
 * <p> </p>
 *
 * @author jiahui
 * @date 2017/12/19
 */
public interface IPayReqAudioConnView extends IMvpBaseView {

    /**
     * 刷新最高、最低出价
     *
     * @param hightest
     * @param lowest
     */
    void refreshAudioConnPayHistoryInfo(long hightest, long lowest);

    void refreshGoldNum(double goldNum);

    void toast(String toast);

    void finishForResult();
}
