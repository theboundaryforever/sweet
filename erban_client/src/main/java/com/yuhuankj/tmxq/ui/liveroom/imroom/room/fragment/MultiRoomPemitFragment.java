package com.yuhuankj.tmxq.ui.liveroom.imroom.room.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.TimeUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomEvent;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.MultiRoomInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.fragment.BaseMvpFragment;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter.MultiRoomPemitListAdapter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter.MultiRoomRecvGiftListAdapter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter.MultiRoomPemitPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IMultiRoomPemitView;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.ArrayList;


/**
 * 个人语音房的房间fragment
 *
 * @author zeda
 */
@CreatePresenter(MultiRoomPemitPresenter.class)
public class MultiRoomPemitFragment extends BaseMvpFragment<IMultiRoomPemitView,
        MultiRoomPemitPresenter> implements IMultiRoomPemitView, View.OnClickListener {

    private final String TAG = MultiRoomPemitFragment.class.getSimpleName();
    private TextView tvRoomTitle;
    private TextView tvRoomOwnerNick;
    private TextView tvRoomId;
    private ImageView ivRoomOwnerAvatar;
    private TextView tvActivityTips;
    private TextView tvRoomNotice;
    private TextView tvActivityNum;
    private TextView tvAdmireNum;
    private TextView tvFansNum;
    private TextView tvMaxOnLineNum;
    private TextView tvMaxOnLineTime;
    private TextView tvRecvGiftValue;
    private TextView tvRecvGiftEmptyTips;
    private ImageView ivRoomLevel;
    private ImageView ivRoomAuthMore;
    private View rlRoomAuth;
    private RecyclerView rvRoomAuthList;
    private RecyclerView rvRecvGiftList;
    private MultiRoomPemitListAdapter multiRoomPemitListAdapter;
    private MultiRoomRecvGiftListAdapter multiRoomRecvGiftListAdapter;
    private int roomOwnerAvatarSize = 0;
    private int roomLevelIconWidth = 0;
    private int roomLevelIconHeight = 0;

    public static MultiRoomPemitFragment newInstance(long roomUid, int roomType) {
        MultiRoomPemitFragment roomDetailFragment = new MultiRoomPemitFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(Constants.ROOM_UID, roomUid);
        bundle.putLong(Constants.ROOM_TYPE, roomType);
        roomDetailFragment.setArguments(bundle);
        return roomDetailFragment;
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_room_level_info;
    }

    @Override
    public void onFindViews() {
        tvRoomTitle = mView.findViewById(R.id.tvRoomTitle);
        ivRoomOwnerAvatar = mView.findViewById(R.id.ivRoomOwnerAvatar);
        tvRoomOwnerNick = mView.findViewById(R.id.tvRoomOwnerNick);
        tvRoomId = mView.findViewById(R.id.tvRoomId);
        ivRoomLevel = mView.findViewById(R.id.ivRoomLevel);
        tvRoomNotice = mView.findViewById(R.id.tvRoomNotice);
        tvActivityTips = mView.findViewById(R.id.tvActivityTips);
        tvActivityNum = mView.findViewById(R.id.tvActivityNum);
        tvAdmireNum = mView.findViewById(R.id.tvAdmireNum);
        tvFansNum = mView.findViewById(R.id.tvFansNum);
        tvMaxOnLineNum = mView.findViewById(R.id.tvMaxOnLineNum);
        tvMaxOnLineTime = mView.findViewById(R.id.tvMaxOnLineTime);
        rlRoomAuth = mView.findViewById(R.id.rlRoomAuth);
        ivRoomAuthMore = mView.findViewById(R.id.ivRoomAuthMore);
        rvRoomAuthList = mView.findViewById(R.id.rvRoomAuthList);
        rvRoomAuthList.setFocusable(false);
        tvRecvGiftValue = mView.findViewById(R.id.tvRecvGiftValue);
        rvRecvGiftList = mView.findViewById(R.id.rvRecvGiftList);
        rvRecvGiftList.setFocusable(false);
        tvRecvGiftEmptyTips = mView.findViewById(R.id.tvRecvGiftEmptyTips);

        multiRoomPemitListAdapter = new MultiRoomPemitListAdapter(new ArrayList<>());
        rvRoomAuthList.setAdapter(multiRoomPemitListAdapter);

        multiRoomRecvGiftListAdapter = new MultiRoomRecvGiftListAdapter(new ArrayList<>());
        rvRecvGiftList.setAdapter(multiRoomRecvGiftListAdapter);
    }

    @Override
    public void onSetListener() {
        ivRoomLevel.setOnClickListener(this);
        tvActivityTips.setOnClickListener(this);
        ivRoomAuthMore.setOnClickListener(this);
        mView.findViewById(R.id.blllActivity).setOnClickListener(this);
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        LogUtils.d(TAG, "onNewIntent");
        getMvpPresenter().resetLastMultiRoomInfo();
        refreshMultiRoomPemit();
    }

    @Override
    public void loadLazyIfYouWant() {
        super.loadLazyIfYouWant();
        LogUtils.d(TAG, "loadLazyIfYouWant");
        refreshMultiRoomPemit();
    }

    @Override
    public void onResume() {
        super.onResume();
        LogUtils.d(TAG, "onResume");
    }


    @Override
    public void initiate() {
        LogUtils.d(TAG, "initiate");
        IMRoomMessageManager.get().subscribeIMRoomEventObservable(this::receiveRoomEvent, this);
    }

    /**
     * 接收房间事件
     */
    public void receiveRoomEvent(IMRoomEvent roomEvent) {
        if (roomEvent == null) {
            return;
        }
        int event = roomEvent.getEvent();
        switch (event) {
            case IMRoomEvent.SOCKET_ROOM_ENTER_SUC:
                //socket进房(包括socket重连进房)
                LogUtils.d(TAG, "receiveRoomEvent-socket进房(包括socket重连进房)");
                refreshMultiRoomPemit();
                break;
            case IMRoomEvent.ROOM_RECONNECT:
                //IM重連
                LogUtils.d(TAG, "receiveRoomEvent-IM重連");
                refreshMultiRoomPemit();
                break;
            case IMRoomEvent.ROOM_INFO_UPDATE:
                //房間信息更新
                LogUtils.d(TAG, "receiveRoomEvent-房間信息更新");
                refreshMultiRoomPemit();
                break;
            default:
                break;
        }
    }

    @Override
    public void refreshMultiRoomPemit() {
        LogUtils.d(TAG, "refreshMultiRoomPemit");

        if (null == getActivity()) {
            return;
        }

        if (0 == roomOwnerAvatarSize) {
            roomOwnerAvatarSize = DisplayUtility.dp2px(getActivity(), 63);
        }

        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (null == roomInfo) {
            return;
        }

        UserInfo ownerUserInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(roomInfo.getUid());

        //刷新头部基本信息
        String roomTitle = roomInfo.getTitle();
        if (!TextUtils.isEmpty(roomTitle) && roomTitle.length() > 10) {
            roomTitle = getActivity().getString(R.string.nick_length_max_six, roomTitle.substring(0, 10));
        }
        tvRoomTitle.setText(roomTitle);

        rlRoomAuth.setVisibility(RoomDataManager.get().isRoomOwner() ? View.VISIBLE : View.GONE);
        if (!TextUtils.isEmpty(roomInfo.getRoomNotice())) {
            tvRoomNotice.setText(roomInfo.getRoomNotice());
        }

        if (null != ownerUserInfo) {
            if (!TextUtils.isEmpty(ownerUserInfo.getAvatar())) {
                ImageLoadUtils.loadCircleImage(getActivity(), ImageLoadUtils.toThumbnailUrl(roomOwnerAvatarSize, roomOwnerAvatarSize,
                        ownerUserInfo.getAvatar()), ivRoomOwnerAvatar, R.drawable.ic_bosonfriend_level_default);
            }
            if (!TextUtils.isEmpty(ownerUserInfo.getNick())) {
                String roomOwnerNick = ownerUserInfo.getNick();
                if (roomOwnerNick.length() > 6) {
                    roomOwnerNick = getActivity().getString(R.string.nick_length_max_six, roomOwnerNick.substring(0, 6));
                }
                tvRoomOwnerNick.setText(getActivity().getString(R.string.room_level_owner_nick, roomOwnerNick));
            }
            tvRoomId.setText(getActivity().getString(R.string.room_level_room_id, String.valueOf(ownerUserInfo.getErbanNo())));
        } else {
            tvRoomOwnerNick.setText(getActivity().getString(R.string.room_level_owner_nick, "---"));
        }

        MultiRoomInfo multiRoomInfo = getMvpPresenter().getLastMultiRoomInfo();
        if (null != multiRoomInfo) {

            if (0 == roomLevelIconWidth) {
                roomLevelIconWidth = DisplayUtility.dp2px(getActivity(), 47);
            }
            if (0 == roomLevelIconHeight) {
                roomLevelIconHeight = DisplayUtility.dp2px(getActivity(), 57);
            }

            //房间等级图标
            ImageLoadUtils.loadImage(getActivity(), ImageLoadUtils.toThumbnailUrl(
                    roomLevelIconWidth, roomLevelIconHeight, multiRoomInfo.getRoomLevelIcon()), ivRoomLevel);

            tvActivityNum.setText(String.valueOf(multiRoomInfo.getActivity()));
            tvAdmireNum.setText(String.valueOf(multiRoomInfo.getAdmire()));
            tvFansNum.setText(String.valueOf(multiRoomInfo.getFans()));
            tvMaxOnLineNum.setText(String.valueOf(multiRoomInfo.getOnline()));
            int day = TimeUtils.getDayOfMonth(multiRoomInfo.getOnlineTime());
            String dayStr = null;
            if (day < 10) {
                dayStr = "0".concat(String.valueOf(day));
            } else {
                dayStr = String.valueOf(day);
            }
            tvMaxOnLineTime.setText(getActivity().getString(R.string.room_level_online_time,
                    String.valueOf(TimeUtils.getMonth(multiRoomInfo.getOnlineTime())),
                    dayStr));

            if (null != multiRoomInfo.getPermitList()) {
                multiRoomPemitListAdapter.setNewData(multiRoomInfo.getPermitList());
            }

            if (!ListUtils.isListEmpty(multiRoomInfo.getGiftList())) {
                multiRoomRecvGiftListAdapter.setNewData(multiRoomInfo.getGiftList());
                tvRecvGiftEmptyTips.setVisibility(View.GONE);
                rvRecvGiftList.setVisibility(View.VISIBLE);
                tvRecvGiftValue.setText(getActivity().getResources().getString(R.string.room_level_gift_value,
                        String.valueOf(getMvpPresenter().getRecvGiftNumTotal())));
            } else {
                tvRecvGiftEmptyTips.setVisibility(View.VISIBLE);
                rvRecvGiftList.setVisibility(View.GONE);
                tvRecvGiftValue.setText(getActivity().getResources().getString(R.string.room_level_gift_value, String.valueOf(0)));
            }
        } else {
            tvRecvGiftEmptyTips.setVisibility(View.VISIBLE);
            rvRecvGiftList.setVisibility(View.GONE);
            tvRecvGiftValue.setText(getActivity().getResources().getString(R.string.room_level_gift_value, String.valueOf(0)));
        }
        mIsLoaded = false;
        getMvpPresenter().getMultiRoomInfo();
    }

    @Override
    public void onDestroyView() {
        releaseView();
        super.onDestroyView();
    }

    private void releaseView() {
        LogUtils.d(TAG, "releaseView");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivRoomAuthMore:
            case R.id.ivRoomLevel:
                CommonWebViewActivity.start(getActivity(), UriProvider.getRoomLevelAboutUrl(), false);
                break;
            case R.id.blllActivity:
            case R.id.tvActivityTips:
                getDialogManager().showRoomActivityTipsDialog();
                break;
            default:
                break;
        }
    }
}
