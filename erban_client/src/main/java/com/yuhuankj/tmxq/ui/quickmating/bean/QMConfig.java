package com.yuhuankj.tmxq.ui.quickmating.bean;

import java.io.Serializable;

public class QMConfig implements Serializable {

    private String logo;
    private int freeTime;
    private int freeMatchCount;
    private int buyCostPea;
    private int maxBuyCount;
    private int maxRenewCount;
    private int renewCostPea;
    private int renewTime;
    private String openTime;
    private String closeTime;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getFreeTime() {
        return freeTime;
    }

    public void setFreeTime(int freeTime) {
        this.freeTime = freeTime;
    }

    public int getFreeMatchCount() {
        return freeMatchCount;
    }

    public void setFreeMatchCount(int freeMatchCount) {
        this.freeMatchCount = freeMatchCount;
    }

    public int getBuyCostPea() {
        return buyCostPea;
    }

    public void setBuyCostPea(int buyCostPea) {
        this.buyCostPea = buyCostPea;
    }

    public int getMaxBuyCount() {
        return maxBuyCount;
    }

    public void setMaxBuyCount(int maxBuyCount) {
        this.maxBuyCount = maxBuyCount;
    }

    public int getMaxRenewCount() {
        return maxRenewCount;
    }

    public void setMaxRenewCount(int maxRenewCount) {
        this.maxRenewCount = maxRenewCount;
    }

    public int getRenewCostPea() {
        return renewCostPea;
    }

    public void setRenewCostPea(int renewCostPea) {
        this.renewCostPea = renewCostPea;
    }

    public int getRenewTime() {
        return renewTime;
    }

    public void setRenewTime(int renewTime) {
        this.renewTime = renewTime;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }
}
