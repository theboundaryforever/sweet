package com.yuhuankj.tmxq.ui.nim.game;

import android.text.TextUtils;

import com.netease.nim.uikit.session.actions.BaseAction;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.xchat_core.minigame.MiniGameModel;
import com.yuhuankj.tmxq.base.dialog.DialogManager;

/**
 * Created by hzxuwen on 2015/6/12.
 */
public class MiniGameAction extends BaseAction {
    private String gameType;

    public MiniGameAction(String iconResUrl, String title, String gameType, String name, String players) {
        super(iconResUrl, title);
        this.gameType = gameType;
        this.name = name;
        this.players = players;
    }

    @Override
    public void onClick() {
        if (null == getActivity()) {
            return;
        }
        String account = getAccount();
        if (TextUtils.isEmpty(account)) {
            SingleToastUtil.showToast("邀请账号为空");
            return;
        }
        long toAccount = 0;
        try {
            toAccount = Long.valueOf(account);
        } catch (NumberFormatException e) {
            SingleToastUtil.showToast("邀请账号不正确");
            return;
        }

        DialogManager dialogManager = new DialogManager(getActivity());
        dialogManager.showProgressDialog(getActivity(), "请稍后...");
        new MiniGameModel().invitedUser(gameType, toAccount, new OkHttpManager.MyCallBack<ServiceResult<Object>>() {
            @Override
            public void onError(Exception e) {
                dialogManager.dismissDialog();
                if (null != getActivity()) {
                    SingleToastUtil.showToast(getActivity(), "操作失败");
                }
            }

            @Override
            public void onResponse(ServiceResult<Object> response) {
                dialogManager.dismissDialog();
                if (response.getCode() != 200 && null != getActivity()) {
                    SingleToastUtil.showToast(getActivity(), response.getMessage());
                }
            }
        });
    }
}

