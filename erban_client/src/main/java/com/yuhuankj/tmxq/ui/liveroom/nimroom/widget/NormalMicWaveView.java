package com.yuhuankj.tmxq.ui.liveroom.nimroom.widget;

import android.content.Context;
import android.util.AttributeSet;

public class NormalMicWaveView extends WaveView {

    public NormalMicWaveView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public NormalMicWaveView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NormalMicWaveView(Context context) {
        super(context);
    }

    @Override
    public void init() {
        customRadus = 45f;
        addRadus = 38;
        delRadus = 34;
        stopRadus = 45;
        middleRadus = 40;
        alphaMax = 8;
        alphaDel = 1;
        super.init();
    }
}
