package com.yuhuankj.tmxq.ui.signAward.presenter;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.signAward.model.RecvNewsGiftPkgResult;
import com.yuhuankj.tmxq.ui.signAward.model.SignInModel;
import com.yuhuankj.tmxq.ui.signAward.view.ISignAwardTipsView;

/**
 * @author weihaitao
 * @date 2019/5/21
 */
public class SignAwardTipsPresenter extends AbstractMvpPresenter<ISignAwardTipsView> {

    private SignInModel signInModel;

    public SignAwardTipsPresenter() {

        signInModel = new SignInModel();

    }

    public void receiveNewsBigGift(int getDay, long micUid, long roomId, int roomType,
                                   long roomUid) {
        signInModel.receiveNewsBigGift(getDay, micUid, roomId, roomType, roomUid,
                new HttpRequestCallBack<RecvNewsGiftPkgResult>() {

                    @Override
                    public void onSuccess(String message, RecvNewsGiftPkgResult result) {
                        if (null == getMvpView()) {
                            return;
                        }
                        if (null == result) {
                            getMvpView().showRecvSignPkgError(BasicConfig.INSTANCE.getAppContext().getResources().getString(R.string.network_error));
                            return;
                        }
                        getMvpView().showSignAwardResultDialog(result);
                        //这里需要刷新礼物列表
                        CoreManager.getCore(IGiftCore.class).requestGiftInfos();
                    }

                    @Override
                    public void onFailure(int code, String msg) {
                        if (null == getMvpView()) {
                            return;
                        }
                        getMvpView().showRecvSignPkgError(msg);
                    }
                });
    }
}
