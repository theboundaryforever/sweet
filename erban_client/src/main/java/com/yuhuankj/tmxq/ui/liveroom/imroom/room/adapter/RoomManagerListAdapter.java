package com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMember;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.widget.SquareImageView;

import java.util.List;


/**
 * Created by chenran on 2017/10/11.
 */

public class RoomManagerListAdapter extends RecyclerView.Adapter<RoomManagerListAdapter.RoomManagerListHolder>
        implements View.OnClickListener {
    private Context context;
    private List<IMRoomMember> normalList;
    private OnRoomManagerListOperationClickListener listOperationClickListener;

    public RoomManagerListAdapter(Context context) {
        this.context = context;
    }

    public List<IMRoomMember> getRoomMemberList() {
        return normalList;
    }

    public void setRoomMemberList(List<IMRoomMember> normalList) {
        this.normalList = normalList;
    }

    public void setListOperationClickListener(OnRoomManagerListOperationClickListener listOperationClickListener) {
        this.listOperationClickListener = listOperationClickListener;
    }

    @Override
    public RoomManagerListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_room_normal, parent, false);
        return new RoomManagerListHolder(view);
    }

    @Override
    public void onBindViewHolder(RoomManagerListHolder holder, int position) {
        IMRoomMember imRoomMember = normalList.get(position);
        String nick = imRoomMember.getNick();
        if (!TextUtils.isEmpty(nick) && nick.length() > 6) {
            nick = holder.nick.getContext().getResources().getString(R.string.black_list_admin_name, nick);
        }
        holder.nick.setText(nick);
        holder.operationImg.setTag(imRoomMember);
        holder.operationImg.setOnClickListener(this);
        ImageLoadUtils.loadSmallRoundBackground(context, imRoomMember.getAvatar(), holder.avatar);
    }

    @Override
    public int getItemCount() {
        if (normalList == null) {
            return 0;
        } else {
            return normalList.size();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getTag() != null && v.getTag() instanceof IMRoomMember) {
            IMRoomMember imRoomMember = (IMRoomMember) v.getTag();
            if (listOperationClickListener != null) {
                listOperationClickListener.onRemoveOperationClick(imRoomMember);
            }
        }
    }

    public interface OnRoomManagerListOperationClickListener {
        void onRemoveOperationClick(IMRoomMember imRoomMember);
    }

    public class RoomManagerListHolder extends RecyclerView.ViewHolder {
        private SquareImageView avatar;
        private TextView nick;
        private TextView erbanNo;
        private ImageView operationImg;

        public RoomManagerListHolder(View itemView) {
            super(itemView);
            avatar = itemView.findViewById(R.id.avatar);
            nick = itemView.findViewById(R.id.nick);
            erbanNo = itemView.findViewById(R.id.erban_no);
            operationImg = itemView.findViewById(R.id.remove_opration);
        }
    }
}
