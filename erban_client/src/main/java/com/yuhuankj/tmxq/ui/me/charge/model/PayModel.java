package com.yuhuankj.tmxq.ui.me.charge.model;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_core.pay.IPayCore;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_core.result.WalletInfoResult;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.Map;

/**
 * Created by MadisonRong on 05/01/2018.
 */

public class PayModel extends BaseMvpModel {

    public PayModel() {
    }

    /**
     * 获取用户信息
     */
    public UserInfo getUserInfo() {
        long currentUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        return CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(currentUid);
    }


    /**
     * 获取用户的钱包信息
     * @param uid
     * @param callback
     */
    public void getWalletInfo(long uid, CallBack<WalletInfo> callback){
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(uid));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.getWalletInfos(), param, new OkHttpManager.MyCallBack<WalletInfoResult>() {
            @Override
            public void onError(Exception e) {
                if (callback != null){
                    callback.onFail(OkHttpManager.DEFAULT_CODE_ERROR,e.getMessage());
                }
            }

            @Override
            public void onResponse(WalletInfoResult response) {
                if (callback == null) {
                    return;
                }
                if (response != null) {
                    if (response.isSuccess()) {
                        if (response.getData()!= null) {
                            CoreManager.getCore(IPayCore.class).setCurrentWalletInfo(response.getData());
                        }
                       callback.onSuccess(response.getData());
                    } else {
                        callback.onFail(response.getCode(),response.getMessage());
                    }
                }
            }
        });
    }

    /**
     * 刷新钱包信息
     *
     * @param force
     * @param myCallBack
     */
    public void refreshWalletInfo(boolean force, OkHttpManager.MyCallBack<WalletInfoResult> myCallBack) {
        String cacheStrategy = force ? "no-cache" : "max-stale";
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("Cache-Control", cacheStrategy);

        OkHttpManager.getInstance().getRequest(UriProvider.getWalletInfo(), params, myCallBack);
    }
}
