package com.yuhuankj.tmxq.ui.me.wallet.bills.fragmemt;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.JavaUtil;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.TimeUtils;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.bills.IBillsCore;
import com.tongdaxing.xchat_core.bills.IBillsCoreClient;
import com.tongdaxing.xchat_core.bills.bean.BillItemEntity;
import com.tongdaxing.xchat_core.bills.bean.IncomeInfo;
import com.tongdaxing.xchat_core.bills.bean.IncomeListInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.fragment.BaseFragment;
import com.yuhuankj.tmxq.ui.me.wallet.bills.adapter.WithdrawBillsAdapter;
import com.yuhuankj.tmxq.ui.me.wallet.bills.event.DateInfoEvent;
import com.yuhuankj.tmxq.ui.widget.itemdecotion.PowerGroupListener;
import com.yuhuankj.tmxq.ui.widget.itemdecotion.PowerfulStickyDecoration;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.UIUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p> 提现账单（不包含红包） </p>
 * Created by Administrator on 2017/11/7.
 */
public class WithdrawBillsFragment extends BaseFragment {
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private WithdrawBillsAdapter adapter;
    private List<BillItemEntity> mBillItemEntityList = new ArrayList<>();
    private Context mContext;

    protected int mCurrentCounter = Constants.PAGE_START;//当前页
    protected static final int PAGE_SIZE = Constants.BILL_PAGE_SIZE;
    protected long time = System.currentTimeMillis();
    private TextView tvTime;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
    }

    @Override
    public void onFindViews() {
        mRecyclerView = mView.findViewById(R.id.recycler_view);
        mSwipeRefreshLayout = mView.findViewById(R.id.swipe_refresh);
        tvTime = mView.findViewById(R.id.tv_time);
    }

    @Override
    public void onSetListener() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mCurrentCounter = 1;
                loadData();
            }
        });
    }

    @Override
    public void initiate() {
        tvTime.setText(new SimpleDateFormat("yyyy-MM-dd").format(new Date(time)));

        adapter = new WithdrawBillsAdapter(mBillItemEntityList);
        adapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                mCurrentCounter++;
                loadData();
            }
        }, mRecyclerView);
        RecyclerViewNoBugLinearLayoutManager manager = new RecyclerViewNoBugLinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setAdapter(adapter);

        PowerfulStickyDecoration decoration = PowerfulStickyDecoration.Builder
                .init(new PowerGroupListener() {
                    @Override
                    public String getGroupName(int position) {
                        //获取组名，用于判断是否是同一组
                        if (mBillItemEntityList.size() > position && position>=0) {
                            return mBillItemEntityList.get(position).time;
                        }
                        return null;
                    }

                    @Override
                    public View getGroupView(int position) {
                        //获取自定定义的组View
                        if (mBillItemEntityList.size() > position && position>=0) {
                            View view = getLayoutInflater().inflate(R.layout.item_group, null, false);
                            ((TextView) view.findViewById(R.id.tv)).setText(TimeUtils.getDateTimeString(JavaUtil.str2long(mBillItemEntityList.get(position).time),"yyyy-MM-dd"));
                            return view;
                        } else {
                            return null;
                        }
                    }
                })
                .setGroupHeight(UIUtil.dip2px(getActivity(), 50))       //设置高度
                .isAlignLeft(true)                                 //靠右边显示   默认左边
                .setGroupBackground(getResources().getColor(R.color.colorPrimaryDark))    //设置背景   默认透明
                .build();
        mRecyclerView.addItemDecoration(decoration);

        showLoading();
        loadData();
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_xrexylerview;
    }

    private void loadData() {
        CoreManager.getCore(IBillsCore.class).getWithdrawBills(mCurrentCounter, PAGE_SIZE, time);
    }

    private String time1 = "";

    @CoreEvent(coreClientClass = IBillsCoreClient.class)
    public void onGetWithdrawBills(IncomeListInfo data) {
        mSwipeRefreshLayout.setRefreshing(false);
        if (null != data) {
            if (mCurrentCounter == Constants.PAGE_START) {
                hideStatus();
                mBillItemEntityList.clear();
                adapter.setNewData(mBillItemEntityList);
            } else {
                adapter.loadMoreComplete();
            }
            BillItemEntity billItemEntity;
            List<Map<String, List<IncomeInfo>>> billList = data.getBillList();
            if (!billList.isEmpty()) {
                tvTime.setVisibility(View.GONE);
                int size = mBillItemEntityList.size();
                List<BillItemEntity> billItemEntities = new ArrayList<>();
                for (int i = 0; i < billList.size(); i++) {
                    Map<String, List<IncomeInfo>> map = billList.get(i);
                    for (String key : map.keySet()) {
                        // key ---日期    value：list集合记录
                        List<IncomeInfo> incomeInfos = map.get(key);
                        if (ListUtils.isListEmpty(incomeInfos)) continue;
                        //正常item
                        for (IncomeInfo temp : incomeInfos) {
                            billItemEntity = new BillItemEntity(BillItemEntity.ITEM_NORMAL);
                            billItemEntity.mWithdrawInfo = temp;
                            billItemEntity.time = key;  //目的是为了比较
                            billItemEntities.add(billItemEntity);
                        }
                    }
                }
                if (billItemEntities.size() < Constants.BILL_PAGE_SIZE && mCurrentCounter == Constants.PAGE_START) {
                    adapter.setEnableLoadMore(false);
                }
                adapter.addData(billItemEntities);
            } else {
                if (mCurrentCounter == 1) {
                    showNoData(getResources().getString(R.string.bill_no_data_text));
                } else {
                    adapter.loadMoreEnd(true);
                }
            }
        }
    }

    @CoreEvent(coreClientClass = IBillsCoreClient.class)
    public void onGetWithdrawBillsError(String error) {
        mSwipeRefreshLayout.setRefreshing(false);
        if (mCurrentCounter == Constants.PAGE_START) {
            mSwipeRefreshLayout.setRefreshing(false);
            showNetworkErr();
        } else {
            adapter.loadMoreFail();
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDateInfoEvent(DateInfoEvent event) {
        if (event != null && event.position == 0) {
            time = event.millSeconds;
            mCurrentCounter = 1;
            showLoading();
            loadData();
        }
    }

    @Override
    public View.OnClickListener getLoadListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentCounter = 1;
                showLoading();
                loadData();
            }
        };
    }
}
