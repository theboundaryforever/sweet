package com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter;

import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yuhuankj.tmxq.R;

/**
 * Created by huangmeng1 on 2018/1/18.
 */

public class RoomMsgTipsListAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

    private final String TAG = RoomMsgTipsListAdapter.class.getSimpleName();


    public RoomMsgTipsListAdapter() {
        super(R.layout.item_rv_room_msg_tips);
    }

    /**
     * Implement this method and use the helper to adapt the view to the given item.
     *
     * @param helper A fully initialized helper.
     * @param item   The item that needs to be displayed.
     */
    @Override
    protected void convert(BaseViewHolder helper, String item) {
        TextView bltvRoomTip = helper.getView(R.id.bltvRoomTip);
        bltvRoomTip.setText(item);

    }
}
