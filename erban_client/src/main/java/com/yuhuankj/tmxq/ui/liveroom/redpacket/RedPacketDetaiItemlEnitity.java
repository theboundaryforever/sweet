package com.yuhuankj.tmxq.ui.liveroom.redpacket;

import java.io.Serializable;

/**
 * @author liaoxy
 * @Description:红包明细实体类
 * @date 2019/1/17 15:55
 */
public class RedPacketDetaiItemlEnitity implements Serializable {
    private String avatar;//": "string",
    private int goldNum;//": 0,
    private String nick;//": "string",
    private String receiveDate;//": "2019-01-22T01:58:01.308Z",
    private int redPacketGold;//": 0,
    private String redPacketId;//": "string",
    private int redPacketNum;//": 0,
    private int redPacketReceiveNum;//": 0,
    private long uid;//": 0

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getGoldNum() {
        return goldNum;
    }

    public void setGoldNum(int goldNum) {
        this.goldNum = goldNum;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(String receiveDate) {
        this.receiveDate = receiveDate;
    }

    public int getRedPacketGold() {
        return redPacketGold;
    }

    public void setRedPacketGold(int redPacketGold) {
        this.redPacketGold = redPacketGold;
    }

    public String getRedPacketId() {
        return redPacketId;
    }

    public void setRedPacketId(String redPacketId) {
        this.redPacketId = redPacketId;
    }

    public int getRedPacketNum() {
        return redPacketNum;
    }

    public void setRedPacketNum(int redPacketNum) {
        this.redPacketNum = redPacketNum;
    }

    public int getRedPacketReceiveNum() {
        return redPacketReceiveNum;
    }

    public void setRedPacketReceiveNum(int redPacketReceiveNum) {
        this.redPacketReceiveNum = redPacketReceiveNum;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }
}
