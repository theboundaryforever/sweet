package com.yuhuankj.tmxq.ui.me.taskcenter.view.adapter;

import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.libcommon.utils.TimeUtils;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.me.taskcenter.model.DouziHistoryEnitity;

import java.util.List;

/**
 * @author liaoxy
 * @Description:甜豆记录
 * @date 2019/4/12 11:12
 */
public class DouziHistoryAdapter extends BaseQuickAdapter<DouziHistoryEnitity, BaseViewHolder> {

    public DouziHistoryAdapter(List<DouziHistoryEnitity> data) {
        super(R.layout.item_taskcenter_douzi_history, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, DouziHistoryEnitity item) {
        TextView tvType = helper.getView(R.id.tvType);
        TextView tvTime = helper.getView(R.id.tvTime);
        TextView tvCount = helper.getView(R.id.tvCount);

        tvType.setText(item.getTypeName());
        tvTime.setText(TimeUtils.getTimeStringFromMillis(item.getRecordTime()));
        if (item.getPeaNum() > 0) {
            tvCount.setText("+" + item.getPeaNum());
        } else {
            tvCount.setText("" + item.getPeaNum());
        }
    }
}