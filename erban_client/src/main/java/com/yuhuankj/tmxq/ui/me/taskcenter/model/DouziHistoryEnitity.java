package com.yuhuankj.tmxq.ui.me.taskcenter.model;

import java.io.Serializable;

public class DouziHistoryEnitity implements Serializable {

    private int id;//": 195,
    private int peaNum;//": 10,
    private long recordTime;//": 1555311063000,
    private int type;//": 1,
    private String typeName;//": "任务奖励+10",
    private long uid;//": 92000633


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPeaNum() {
        return peaNum;
    }

    public void setPeaNum(int peaNum) {
        this.peaNum = peaNum;
    }

    public long getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(long recordTime) {
        this.recordTime = recordTime;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }
}
