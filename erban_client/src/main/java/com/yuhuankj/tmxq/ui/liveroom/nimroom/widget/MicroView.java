package com.yuhuankj.tmxq.ui.liveroom.nimroom.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Point;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.widget.MicroWaveView;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.room.face.FaceReceiveInfo;
import com.tongdaxing.xchat_core.room.face.IFaceCoreClient;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.adapter.MicroViewAdapter;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.face.AnimFaceFactory;
import com.yuhuankj.tmxq.widget.CircleImageView;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.UIUtil;

import java.util.List;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * 上麦布局界面
 *
 * @author xiaoyu
 * @date 2017/12/20
 */
public class MicroView extends RelativeLayout {
    private static final String TAG = "MicroView";
    private RecyclerView recyclerView;
    private MicroViewAdapter adapter;
    private SparseArray<ImageView> faceImageViews;

    private Context mContext;
    private int giftWidth;
    private int giftHeight;
    private int normalFaceWidth;
    private int normalFaceHeight;
    private int ownerFaceWidth;
    private int ownerFaceHeight;
    private Disposable subscribe;
    private boolean isKtv;
    private PairState pairStateView;

    public PairState getPairStateView() {
        return pairStateView;
    }

    public MicroView(Context context) {
        this(context, null);
    }

    public MicroView(Context context, AttributeSet attr) {
        this(context, attr, 0);
    }

    public MicroView(Context context, AttributeSet attr, int i) {
        super(context, attr, i);
        TypedArray array = context.obtainStyledAttributes(attr, R.styleable.MicroView);
        float itemCount = -1;
        if (array != null) {
            itemCount = array.getFloat(R.styleable.MicroView_itemCount, -1);
        }
        init(context, (int) itemCount);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        CoreManager.addClient(this);
        subscribe = IMNetEaseManager.get()
                .getChatRoomEventObservable().subscribe(new Consumer<RoomEvent>() {
                    @Override
                    public void accept(RoomEvent roomEvent) throws Exception {
                        onReceiveRoomEvent(roomEvent);
                    }
                });
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        CoreManager.removeClient(this);
        if (subscribe != null) {
            subscribe.dispose();
            subscribe = null;
        }
    }

    private void onReceiveRoomEvent(RoomEvent roomEvent) {
        if (roomEvent == null || roomEvent.getEvent() == RoomEvent.NONE) return;
        switch (roomEvent.getEvent()) {
            case RoomEvent.SPEAK_STATE_CHANGE:
                onSpeakQueueUpdate(roomEvent.getMicPositionList());
                break;
            default:
                break;
        }
    }

    public void setMicCenterPoint() {
        SparseArray<Point> centerPoints = new SparseArray<>();
        // 算出每一个麦位的位置
        int childCount = recyclerView.getChildCount();
        LogUtils.d(TAG, "setMicCenterPoint: count of children: " + childCount);
        LogUtils.d(TAG, "setMicCenterPoint: item count of children: " + adapter.getItemCount());
        View child;
        for (int i = 0; i < childCount; i++) {
            child = recyclerView.getChildAt(i);
            if (i == 0) {
                int[] location2 = new int[2];
                View view2 = child.findViewById(R.id.online_count_point);
                if (view2 != null) {
                    view2.getLocationInWindow(location2);
                    int x2 = (location2[0] + view2.getWidth() / 2) - giftWidth / 2;
                    int y2 = (location2[1] + view2.getHeight() / 2) - giftHeight / 2;

                    Point point2 = new Point(x2, y2);
                    centerPoints.put(-2, point2);
                }
            }

            if (null != onMicroGuideViewLocationListener) {
                CircleImageView avatar = child.findViewById(R.id.avatar);
                int[] locations = new int[2];
                avatar.getLocationInWindow(locations);
                onMicroGuideViewLocationListener.onMicroGuideViewLocationChanged(i, locations);
            }

            // 找到头像
            View view = child.findViewById(R.id.micro_layout);
            View nameView = child.findViewById(R.id.nick);
            int[] location = new int[2];
            int[] nameLocation = new int[2];
            if (view != null) {
                child = view;
            }
            child.getLocationInWindow(location);
            nameView.getLocationInWindow(nameLocation);
            int x = (location[0] + child.getWidth() / 2) - giftWidth / 2;
            int y = location[1] >= nameLocation[1] ?
                    ((location[1] + child.getHeight() * 7 / 8) - giftHeight / 2) :
                    ((nameLocation[1] + nameView.getHeight() / 2) - giftHeight / 2);


            // 放置表情占位image view
            if (faceImageViews.get(i - 1) == null) {
                int width = 0 == i ? ownerFaceWidth : normalFaceWidth;
                int height = 0 == i ? ownerFaceHeight : normalFaceHeight;
                LayoutParams params = new LayoutParams(width, height);
                child.getLocationInWindow(location);
                int[] containerLocation = new int[2];
                this.getLocationInWindow(containerLocation);
                params.leftMargin = ((location[0] - containerLocation[0] + child.getWidth() / 2) - width / 2);
                params.topMargin = ((location[1] - containerLocation[1] + child.getHeight() / 2) - height / 2);
                ImageView face = new ImageView(mContext);
                face.setLayoutParams(params);
                faceImageViews.put(i - 1, face);
                addView(face);
            }
            Point point = new Point(x, y);
            centerPoints.put(i - 1, point);
            //数上游客飞去在线人数
        }
        AvRoomDataManager.get().mMicPointMap = centerPoints;
    }

    public MicroViewAdapter getAdapter() {
        return adapter;
    }

    private void init(final Context context, int itemCount) {
        this.mContext = context;
        isKtv = itemCount > 0;
        inflate(mContext, R.layout.layout_micro_view, this);
        recyclerView = findViewById(R.id.recycler_view);
        pairStateView = findViewById(R.id.pair_state);
        GridLayoutManager layoutManager = new GridLayoutManager(mContext, 4);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                recyclerView.postDelayed(() -> setMicCenterPoint(), 1000);
                recyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        adapter = new MicroViewAdapter(mContext) {
            @Override
            public int getItemCount() {
                if (isKtv) {
                    return itemCount;
                }
                return super.getItemCount();
            }
        };
        adapter.setHasStableIds(true);
        recyclerView.setItemViewCacheSize(9);
        recyclerView.setAdapter(adapter);

        giftWidth = UIUtil.dip2px(mContext, 80);
        giftHeight = UIUtil.dip2px(mContext, 80);
        normalFaceWidth = UIUtil.dip2px(mContext, 80);
        normalFaceHeight = UIUtil.dip2px(mContext, 80);
        ownerFaceWidth = UIUtil.dip2px(mContext, 66);
        ownerFaceHeight = UIUtil.dip2px(mContext, 66);

        faceImageViews = new SparseArray<>(9);
    }

    public void onSpeakQueueUpdate(List<Integer> positions) {
        int count = recyclerView.getChildCount();
        for (int i = 0; i < positions.size(); i++) {
            int pos = positions.get(i) + 1;
            if (pos >= count) {
                continue;
            }
            MicroWaveView speakState = recyclerView.getChildAt(pos).findViewById(R.id.waveview);
            if (speakState != null) {
                speakState.start();
            }
        }
//        int count = recyclerView.getChildCount();
//        int myMicPosition = AvRoomDataManager.get().getMicPosition(CoreManager.getCore(IAuthCore.class).getCurrentUid());
//        for (int i = 0; i < positions.size(); i++) {
//            int micPosition = positions.get(i);
//            int viewPosition = AvRoomDataManager.getMicroViewPositionByMicPosition(micPosition);
//            if (viewPosition >= count) continue;
//            WaveView speakState = recyclerView.getChildAt(viewPosition).findViewById(R.id.waveview);
//            if (myMicPosition == micPosition && RtcEngineManager.get().isMute()) {
//                //如果是我自己，并且已经静音本地麦的话，那就停止声浪
//                //一般来说，声网下麦/静音本地麦后，声网不会有声音回调，会自动关闭声浪；但是有一个情况意外：断网的时候,声网下麦/静音本地麦后，声网还会有声音回调回来，所以这里加多一层判断
//                speakState.stop();
//            } else if (speakState != null) {
//                speakState.start();
//            }
//        }
    }

    @CoreEvent(coreClientClass = IFaceCoreClient.class)
    public void onReceiveFace(List<FaceReceiveInfo> faceReceiveInfos) {
        if (faceReceiveInfos == null || faceReceiveInfos.size() <= 0) {
            return;
        }
        for (FaceReceiveInfo faceReceiveInfo : faceReceiveInfos) {
            int position = AvRoomDataManager.get().getMicPosition(faceReceiveInfo.getUid());
            if (position < -1) {
                continue;
            }
            ImageView imageView = faceImageViews.get(position);
            if (imageView == null) {
                continue;
            }
            int width = -1 == position ? ownerFaceWidth : normalFaceWidth;
            int height = -1 == position ? ownerFaceHeight : normalFaceHeight;
            AnimFaceFactory.asynLoadAnim(faceReceiveInfo, imageView, mContext, width, height);
        }
    }


    public void release() {
        // 移除麦上的所有成员还有说话的光晕
        for (int i = 0; i < recyclerView.getChildCount(); i++) {
            View child = recyclerView.getChildAt(i);
            RecyclerView.ViewHolder holder = recyclerView.getChildViewHolder(child);
            if (holder instanceof MicroViewAdapter.NormalMicroViewHolder) {
                ((MicroViewAdapter.NormalMicroViewHolder) holder).clear();
            }
        }
        // 移除所有的表情动画
        for (int i = -1; i < faceImageViews.size() - 1; i++) {
            ImageView imageView = faceImageViews.get(i);
            if (imageView == null) {
                continue;
            }
            imageView.setImageDrawable(null);
            imageView.clearAnimation();
        }
    }

    private OnMicroGuideViewLocationListener onMicroGuideViewLocationListener;

    public void setMicroGuideViewLocationListener(OnMicroGuideViewLocationListener listener) {
        this.onMicroGuideViewLocationListener = listener;
    }

    public interface OnMicroGuideViewLocationListener {
        void onMicroGuideViewLocationChanged(int position, int[] locations);
    }
}
