package com.yuhuankj.tmxq.ui.liveroom.imroom.room.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomOnlineMember;

import java.util.List;

/**
 * <p>  </p>
 *
 * @author jiahui
 * @date 2017/12/8
 */
public interface IMRoomOnLineMemberView extends IMvpBaseView {
    void onRequestChatMemberByPageSuccess(List<IMRoomOnlineMember> memberList, int page);

    void onRequestChatMemberByPageFail(String errorStr, int page);

    void onGetOnLineUserList(boolean isSuccess, String message, int page, List<IMRoomOnlineMember> onlineChatMembers);
}
