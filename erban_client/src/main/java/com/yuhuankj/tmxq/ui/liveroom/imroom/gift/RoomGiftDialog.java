package com.yuhuankj.tmxq.ui.liveroom.imroom.gift;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.noober.background.view.BLTextView;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.RealNameAuthStatusChecker;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftType;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMember;
import com.tongdaxing.xchat_core.pay.IPayCore;
import com.tongdaxing.xchat_core.pay.IPayCoreClient;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.AppDialogConstant;
import com.yuhuankj.tmxq.base.dialog.BaseAppMvpDialogFragment;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.ui.liveroom.imroom.gift.adapter.GiftIndicatorStateAdapter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.gift.adapter.GiftTabIndicatorAdapter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.gift.listener.OnGiftListItemClickListener;
import com.yuhuankj.tmxq.ui.liveroom.imroom.gift.listener.OnSendGiftCountListener;
import com.yuhuankj.tmxq.ui.liveroom.imroom.gift.widget.GiftUserAvatarView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.gift.widget.SendGiftCountWindow;
import com.yuhuankj.tmxq.ui.me.charge.ChargeActivity;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;
import com.yuhuankj.tmxq.widget.magicindicator.MagicIndicator;
import com.yuhuankj.tmxq.widget.magicindicator.ViewPagerHelper;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.commonnavigator.CommonNavigator;

import java.util.List;

/**
 * 文件描述：重构的房间礼物弹框
 * 采用mvp模式抛弃旧版本的CoreManager，礼物的赠送回调接口从fragment中抽离，单独独立，不再依赖之前的每个房间fragment
 * 弹框上面部分是送礼对象列表，中间为礼物分类列表(ViewPager + fragment + RecyclerView)
 *
 * @auther：zwk
 * @data：2019/5/13
 */
@CreatePresenter(RoomGiftPresenter.class)
public class RoomGiftDialog extends BaseAppMvpDialogFragment<IRoomGiftView, RoomGiftPresenter>
        implements IRoomGiftView, GiftTabIndicatorAdapter.OnItemSelectListener,
        OnGiftListItemClickListener, View.OnClickListener, OnSendGiftCountListener {
    private LinearLayout llContainer;
    private boolean isPersonal = false;
    private long targetUid = 0;
    private int giftCount = 1;//礼物数量
    private GiftInfo currentSelectGift;//礼物信息
    private GiftType currentGiftType = GiftType.Normal;
    //顶部人物头像列表
    private GiftUserAvatarView topAvatarView;
    //底部礼物列表
    private MagicIndicator indicator;
    private GiftIndicatorStateAdapter giftIndicatorStateAdapter;
    private GiftTabIndicatorAdapter mMsgIndicatorAdapter;
    private ViewPager vpGiftList;
    //底部金币和赠送礼物
    private TextView tvBeans;
    private TextView tvGold;
    private BLTextView bltGiftCount;
    private SendGiftCountWindow giftCountPop;
    private List<RoomGiftListFragment> fragments;
    private WalletInfo walletInfo;

    public static RoomGiftDialog getInstance(long uid, boolean isPersonal) {
        RoomGiftDialog roomGiftDialog = new RoomGiftDialog();
        Bundle bundle = new Bundle();
        bundle.putLong("uid", uid);
        bundle.putBoolean("isPersonal", isPersonal);
        roomGiftDialog.setArguments(bundle);
        return roomGiftDialog;
    }

    public static RoomGiftDialog getInstance(long uid, GiftType giftType, boolean isPersonal) {
        RoomGiftDialog roomGiftDialog = new RoomGiftDialog();
        Bundle bundle = new Bundle();
        bundle.putLong("uid", uid);
        bundle.putSerializable("giftType", giftType);
        bundle.putBoolean("isPersonal", isPersonal);
        roomGiftDialog.setArguments(bundle);
        return roomGiftDialog;
    }

    public static RoomGiftDialog getInstance(ChatRoomMember member, boolean isPersonal) {
        RoomGiftDialog roomGiftDialog = new RoomGiftDialog();
        Bundle bundle = new Bundle();
        bundle.putParcelable("member", member);
        bundle.putBoolean("isPersonal", isPersonal);
        roomGiftDialog.setArguments(bundle);
        return roomGiftDialog;
    }

    public static RoomGiftDialog getInstance(IMRoomMember member, boolean isPersonal) {
        RoomGiftDialog roomGiftDialog = new RoomGiftDialog();
        Bundle bundle = new Bundle();
        bundle.putParcelable("im_member", member);
        bundle.putBoolean("isPersonal", isPersonal);
        roomGiftDialog.setArguments(bundle);
        return roomGiftDialog;
    }

    @Override
    public void initArguments(Bundle savedInstanceState) {
        super.initArguments(savedInstanceState);
        if (savedInstanceState != null) {
            targetUid = savedInstanceState.getLong("uid", 0);
            currentGiftType = (GiftType) savedInstanceState.getSerializable("giftType");
            isPersonal = savedInstanceState.getBoolean("isPersonal", false);
        } else {
            if (getArguments() != null) {
                targetUid = getArguments().getLong("uid", 0);
                currentGiftType = (GiftType) getArguments().getSerializable("giftType");
                isPersonal = getArguments().getBoolean("isPersonal", false);
            }
        }
    }

    @Override
    protected int getDialogStyle() {
        return AppDialogConstant.STYLE_BOTTOM;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_room_gift;
    }

    @Override
    protected void initView(View view) {
        llContainer = view.findViewById(R.id.ll_gift_container);
        topAvatarView = view.findViewById(R.id.guav_gift_user_avatar);
        indicator = view.findViewById(R.id.mi_gift_category);
        vpGiftList = view.findViewById(R.id.vp_gift_list_container);
        tvBeans = view.findViewById(R.id.tv_gift_user_beans);
        tvGold = view.findViewById(R.id.tv_gift_user_gold);
        bltGiftCount = view.findViewById(R.id.bltv_send_gift_count);
    }

    @Override
    protected void initViewState() {
        //送礼数量选择
        giftCountPop = new SendGiftCountWindow(getContext());
        giftCountPop.setOnSendGiftCountListener(this);
    }

    @Override
    protected void onLazyLoadData() {
        super.onLazyLoadData();
        //分类礼物列表
        mMsgIndicatorAdapter = new GiftTabIndicatorAdapter(getContext(), getMvpPresenter().getTabInfoList());
        mMsgIndicatorAdapter.setSize(12);
        mMsgIndicatorAdapter.setOnItemSelectListener(this);
        CommonNavigator commonNavigator = new CommonNavigator(getContext());
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(mMsgIndicatorAdapter);
        indicator.setNavigator(commonNavigator);
        fragments = getMvpPresenter().getGiftTabFragments();
        giftIndicatorStateAdapter = new GiftIndicatorStateAdapter(getChildFragmentManager(), fragments);
        vpGiftList.setAdapter(giftIndicatorStateAdapter);
        //这里最适合保存少量页面，但是因为会出现fragment的销毁后接口监听被移除导致无法点击问题
        vpGiftList.setOffscreenPageLimit(5);
        ViewPagerHelper.bind(indicator, vpGiftList);
        int defaultPosition = getMvpPresenter().getGiftTypePosition(currentGiftType);
        commonNavigator.onPageSelected(defaultPosition);
        vpGiftList.setCurrentItem(defaultPosition);
        //赠送对象的头像
        List<MicMemberInfo> micMemberInfos = getMvpPresenter().getTopUserAvatarList(isPersonal, targetUid, 0, 0, false);
        topAvatarView.setPersonalAvatar(isPersonal);
        if (micMemberInfos != null && micMemberInfos.size() == 1) {
            topAvatarView.setAvatarAllMic(micMemberInfos.get(0).isSelect());//只有一个人而且是选中是需要设置选中状态
            topAvatarView.setPersonalAvatar(true);//只有一个暂时不显示全麦
        }
        topAvatarView.updateAllMicAvatar(micMemberInfos);
        //金币数量
        walletInfo = CoreManager.getCore(IPayCore.class).getCurrentWalletInfo();
        if (walletInfo != null) {
            tvGold.setText(getString(R.string.gift_gold_format, walletInfo.getGoldNum()));
            tvBeans.setText(getString(R.string.gift_gold_and_beans_format, walletInfo.getPeaNum()));
        }
        getMvpPresenter().updateGiftWalletInfo();
    }

    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);
        if (childFragment instanceof RoomGiftListFragment) {
            ((RoomGiftListFragment) childFragment).setOnItemClickListener(this);
        }
    }

    @Override
    protected void initListener(View view) {
        bltGiftCount.setOnClickListener(this);
        view.findViewById(R.id.bltv_send_gift).setOnClickListener(this);
        view.findViewById(R.id.tv_room_gift_user_info).setOnClickListener(this);
        view.findViewById(R.id.tv_gift_charge).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_room_gift_user_info:
                if (targetUid <= 0) {
                    UserInfo userInfo = RoomDataManager.get().getRoomOwnerUserInfo();
                    if (userInfo != null) {
                        UserInfoActivity.start(getContext(), userInfo.getUid());
                    }
                } else {
                    UserInfoActivity.start(getContext(), targetUid);
                }
                break;
            case R.id.tv_gift_charge:
                if (getContext() != null) {
                    ChargeActivity.start(getContext());
                }
                break;
            case R.id.bltv_send_gift_count:
                giftCountPop.showGiftCountAtAnchor(bltGiftCount);
                break;
            case R.id.bltv_send_gift:
//                if (!ButtonUtils.isFastDoubleClick()) {
                sendToPeopleGift();
//                }
                break;
            default:
                break;
        }
    }

    /**
     * 赠送礼物逻辑
     */
    private void sendToPeopleGift() {
        if (topAvatarView == null || getContext() == null) {
            return;
        }
        if (currentSelectGift == null) {
            SingleToastUtil.showToast(getContext().getResources().getString(R.string.room_gift_send_no_gift_selected));
            return;
        }
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (getContext() != null && userInfo != null && currentSelectGift.getNobleId() > userInfo.getVipId() && !TextUtils.isEmpty(currentSelectGift.getNobleName())) {
            SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), currentSelectGift.getNobleName() + "以上贵族才可以赠送该礼物", Toast.LENGTH_SHORT);
            return;
        }
        List<MicMemberInfo> micAvatar = topAvatarView.getSelectMicAvatar();
        if (!ListUtils.isListEmpty(micAvatar)) {
            RoomInfo currentRoom = RoomDataManager.get().getCurrentRoomInfo();
            if (currentRoom != null) {
                if (topAvatarView.isAllMic()) {
                    getMvpPresenter().sendRoomMultiGift(currentSelectGift, currentRoom.getUid(), micAvatar, giftCount);
                } else {
                    for (MicMemberInfo micMemberInfo : micAvatar) {
                        getMvpPresenter().sendSingleGift(currentSelectGift, micMemberInfo.getUid(), currentRoom.getUid(), giftCount);
                    }
                }
            }
        } else {
            SingleToastUtil.showToast(getContext().getResources().getString(R.string.room_gift_send_no_member_selected));
        }
    }

    @Override
    public void onSelectCount(int giftCount) {
        this.giftCount = giftCount;
        bltGiftCount.setText(giftCount + "");
        giftCountPop.dismiss();
    }

    @CoreEvent(coreClientClass = IPayCoreClient.class)
    public void onWalletInfoUpdate(WalletInfo walletInfo) {
        if (walletInfo != null) {
            tvGold.setText(String.format("%d", (int) walletInfo.getGoldNum()));
            tvBeans.setText(String.format("%d", walletInfo.getPeaNum()));
        }
    }

    @Override
    public void onItemSelect(int position) {
        vpGiftList.setCurrentItem(position);
    }


    @Override
    public void onItemClick(GiftInfo giftInfo, GiftType giftType, int position) {
        //不同的分类的礼物选中更新  相同类型的更新放入自己的页面处理
        if (RoomGiftConstant.currentGiftType != giftType) {
            onFragmentUpdateGift(true, giftType, position);
        } else {
            RoomGiftConstant.currentGiftTypeSelect = position;
        }
        currentSelectGift = giftInfo;
    }


    /**
     * 通知fragment更新礼物信息
     *
     * @param changeSelect：选中状态改变/
     * @param giftType
     * @param position
     */
    private void onFragmentUpdateGift(boolean changeSelect, GiftType giftType, int position) {
        if (getChildFragmentManager().getFragments().size() > 0) {
            for (int i = 0; i < getChildFragmentManager().getFragments().size(); i++) {
                RoomGiftListFragment giftListFragment = (RoomGiftListFragment) getChildFragmentManager().getFragments().get(i);
                if (changeSelect) {//点击选中状态更新
                    if (giftListFragment.getGiftType() == RoomGiftConstant.currentGiftType) {
                        RoomGiftConstant.currentGiftType = giftType;
                        RoomGiftConstant.currentGiftTypeSelect = position;
                        giftListFragment.updateData();
                        break;
                    }
                } else {
                    //送礼更新 -- 这里的刷新数据不知道是否有问题：机制是没有数量依然显示的礼物分类直接更新数据列表（数据源数量已更新） - 包裹查询更新如果没有数量需要消失
                    if (giftListFragment.getGiftType() == GiftType.Package || giftListFragment.getGiftType() == GiftType.Exclusive) {
                        giftListFragment.onDataToUpdateView();
                    } else {
                        giftListFragment.updateData();
                    }
                }
            }
        }
    }

    @Override
    public void sendGiftUpdatePacketGiftCount(GiftInfo giftInfo) {
        //目前只有礼物背包中需要显示数量所以只有它需要更新
        onFragmentUpdateGift(false, GiftType.Package, 0);
    }

    @Override
    public void sendGiftErrorToast(int code, String error) {
        if (code == RealNameAuthStatusChecker.RESULT_FAILED_NEED_REAL_NAME_AUTH) {
            if (getContext() == null) {
                return;
            }
            DialogManager dialogManager = new DialogManager(getContext());
            dialogManager.setCanceledOnClickOutside(false);
            dialogManager.showOkCancelDialog(error,
                    getResources().getString(R.string.real_name_auth_accept),
                    getResources().getString(R.string.cancel),
                    new DialogManager.OkCancelDialogListener() {
                        @Override
                        public void onCancel() {

                        }

                        @Override
                        public void onOk() {
                            CommonWebViewActivity.start(getContext(), UriProvider.getRealNameAuthUrl());
                        }
                    });
        } else {
            SingleToastUtil.showToast(error);
        }
    }

    @Override
    public void sendGiftUpdateMinusBeanCount(int minusCount) {
        WalletInfo walletInfo = CoreManager.getCore(IPayCore.class).getCurrentWalletInfo();
        if (walletInfo != null) {
            this.walletInfo = walletInfo;
            tvBeans.setText(getString(R.string.gift_gold_and_beans_format, walletInfo.getPeaNum()));
        }
    }

    @Override
    public void sendGiftUpdateMinusGoldCount(int minusCount) {
        WalletInfo walletInfo = CoreManager.getCore(IPayCore.class).getCurrentWalletInfo();
        if (walletInfo != null) {
            this.walletInfo = walletInfo;
            tvGold.setText(getString(R.string.gift_gold_format, walletInfo.getGoldNum()));
        }
    }

    @Override
    public void updateGiftBeanAndGoldCount(WalletInfo data) {
        if (data != null) {
            this.walletInfo = data;
            tvGold.setText(getString(R.string.gift_gold_format, data.getGoldNum()));
            tvBeans.setText(getString(R.string.gift_gold_and_beans_format, data.getPeaNum()));
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (llContainer != null) {
            llContainer.removeAllViews();
            llContainer = null;
        }
        if (mMsgIndicatorAdapter != null) {
            mMsgIndicatorAdapter.setOnItemSelectListener(null);
        }
        //解决三星手机Handler Thread导致的泄露https://segmentfault.com/q/1010000017286787
        if (getDialog() != null) {
            getDialog().setOnShowListener(null);
            getDialog().setOnCancelListener(null);
            getDialog().setOnDismissListener(null);
        }
        if (giftCountPop != null) {
            giftCountPop.setOnSendGiftCountListener(null);
            giftCountPop.setOnDismissListener(null);
            giftCountPop = null;
        }
        RoomGiftConstant.resetState();
    }
}
