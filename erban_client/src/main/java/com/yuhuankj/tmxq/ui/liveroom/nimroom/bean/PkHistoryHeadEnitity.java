package com.yuhuankj.tmxq.ui.liveroom.nimroom.bean;

import com.chad.library.adapter.base.entity.AbstractExpandableItem;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.adapter.PkHistoryAdapter;

public class PkHistoryHeadEnitity extends AbstractExpandableItem<PkHistoryEnitity> implements MultiItemEntity {

    public String pkName;
    public String pkTime;

    public PkHistoryHeadEnitity(String pkName, String pkTime) {
        this.pkName = pkName;
        this.pkTime = pkTime;
    }

    @Override
    public int getItemType() {
        return PkHistoryAdapter.TYPE_LEVEL_HEAD;
    }

    @Override
    public int getLevel() {
        return 1;
    }
}
