package com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.base.AbstractMvpRelativeLayout;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomOwnerLiveTimeCounter;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomQueueInfo;
import com.tongdaxing.xchat_core.room.bean.RoomAdditional;
import com.yuhuankj.tmxq.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * 创建者      魏海涛
 * 创建时间    2019年4月26日 11:58:08
 * 描述        房间内左上角房主直播时长的封装view
 */
public class RoomOwnerLiveTimeView extends AbstractMvpRelativeLayout
        implements RoomOwnerLiveTimeCounter.OnOwnerLiveTimeUpdateListener, IMvpBaseView {

    private final String TAG = RoomOwnerLiveTimeView.class.getSimpleName();

    private TextView tvOwnerLiveTime;
    private SimpleDateFormat sdf;

    public RoomOwnerLiveTimeView(Context context) {
        this(context, null);
    }

    public RoomOwnerLiveTimeView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoomOwnerLiveTimeView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getViewLayoutId() {
        return 0;
    }

    @Override
    public void initialView(Context context) {
        inflate(getContext(), R.layout.layout_room_owner_live_time, this);
        tvOwnerLiveTime = findViewById(R.id.tvOwnerLiveTime);
        boolean isRoomAdminOnLine = false;
        try {
            //这里需要在对应map更新的情况下调用
            IMRoomQueueInfo roomQueueInfo = RoomDataManager.get().getRoomQueueMemberInfoByMicPosition(-1);
            isRoomAdminOnLine = null != roomQueueInfo && null != roomQueueInfo.mChatRoomMember;
        } catch (Exception e) {
            e.printStackTrace();
            //房主离线
        }

        //房主在线开且开播状态且开播时长不为0
        RoomAdditional roomAdditional = RoomDataManager.get().getAdditional();
        LogUtils.d(TAG, "initialView-isRoomAdminOnLine:" + isRoomAdminOnLine);
        if (null != roomAdditional) {
            LogUtils.d(TAG, "initialView-mPlayState:" + roomAdditional.getPlayState());
        }

        setVisibility(isRoomAdminOnLine && null != roomAdditional && 1 == roomAdditional.getPlayState()
                && roomAdditional.getPlayTime() > 0L ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initViewState() {

    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        RoomOwnerLiveTimeCounter.getInstance().addListener(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        RoomOwnerLiveTimeCounter.getInstance().removeListener(this);
    }

    /**
     * 直播时长更新回调
     *
     * @param millCounts 秒为单位
     */
    @Override
    public void onOwnerLiveTimeUpdated(long millCounts) {
        //如果计时器启动了，说明房主开播了
        if (getVisibility() == View.GONE) {
            setVisibility(View.VISIBLE);
        }
        //1:00:00
        tvOwnerLiveTime.setText(parseTimeValue(millCounts));
    }

    /**
     * 将秒数转换成对应格式的时间字符串
     *
     * @param mseconds
     * @return
     */
    private String parseTimeValue(long mseconds) {
        if (null == sdf) {
            sdf = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
            //TimeZone时区，加上这句话，解决解析出的时间比真实时长多出8小时的问题
            sdf.setTimeZone(TimeZone.getTimeZone("GMT+0"));
        }
        Date date = new Date(mseconds * 1000);
        String str = sdf.format(date);
        LogUtils.d(TAG, "parseTimeValue mseconds:" + mseconds + "=" + str);
        return str;
    }
}
