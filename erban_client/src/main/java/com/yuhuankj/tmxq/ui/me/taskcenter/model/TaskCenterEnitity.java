package com.yuhuankj.tmxq.ui.me.taskcenter.model;

import com.yuhuankj.tmxq.ui.signAward.model.SignInAwardInfo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author liaoxy
 * @Description:任务中心内容
 * @date 2019/4/12 11:23
 */
public class TaskCenterEnitity implements Serializable {

    /**
     * 当天签到获得的任务奖励
     */
    private int peanNum;

    public int getSignDay() {
        return signDay;
    }

    public void setSignDay(int signDay) {
        this.signDay = signDay;
    }

    /**
     * 已连续签到天数
     */
    private int signDay;

    /**
     * @deprecated 甜蜜星球1.0.0.2版本起废弃
     */
    private String signGift;
    private List<SignInAwardInfo> dailyMissions = new ArrayList<>();
    private List<SignInAwardInfo> onceMissions = new ArrayList<>();
    private List<SignInAwardInfo> signInMissions = new ArrayList<>();

    public int getPeanNum() {
        return peanNum;
    }

    public void setPeanNum(int peanNum) {
        this.peanNum = peanNum;
    }

    public List<SignInAwardInfo> getDailyMissions() {
        return dailyMissions;
    }

    public void setDailyMissions(List<SignInAwardInfo> dailyMissions) {
        this.dailyMissions = dailyMissions;
    }

    public List<SignInAwardInfo> getOnceMissions() {
        return onceMissions;
    }

    public void setOnceMissions(List<SignInAwardInfo> onceMissions) {
        this.onceMissions = onceMissions;
    }

    public List<SignInAwardInfo> getSignInMissions() {
        return signInMissions;
    }

    public void setSignInMissions(List<SignInAwardInfo> signInMissions) {
        this.signInMissions = signInMissions;
    }

    public String getSignGift() {
        return signGift;
    }

    public void setSignGift(String signGift) {
        this.signGift = signGift;
    }
}
