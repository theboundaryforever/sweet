package com.yuhuankj.tmxq.ui.me.taskcenter.model;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;

import java.util.Map;

/**
 * 任务中心model层
 */
public class TaskCenterModel extends BaseMvpModel {
    /**
     * 获取任务中心任务列表
     */
    public void getTaskList(HttpRequestCallBack<TaskCenterEnitity> callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.getTaskList(), params, callBack);
    }

    //完成任务
    public void finishTask(int missionId,OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("missionId", String.valueOf(missionId));
        OkHttpManager.getInstance().postRequest(UriProvider.finishTask(), params, callBack);
    }

    //甜豆记录
    public void getDouziHistory(int page,int size,OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("pageNum", String.valueOf(page));
        params.put("pageSize", String.valueOf(size));
        OkHttpManager.getInstance().getRequest(UriProvider.getDouziHistory(), params, callBack);
    }
}
