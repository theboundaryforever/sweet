package com.yuhuankj.tmxq.ui.liveroom.imroom.gift;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/5/15
 */
public interface IRoomGiftView extends IMvpBaseView {
    void sendGiftErrorToast(int code,String error);
    void sendGiftUpdatePacketGiftCount(GiftInfo giftInfo);
    void sendGiftUpdateMinusBeanCount(int minusCount);
    void sendGiftUpdateMinusGoldCount(int minusCount);

    void updateGiftBeanAndGoldCount(WalletInfo data);
}
