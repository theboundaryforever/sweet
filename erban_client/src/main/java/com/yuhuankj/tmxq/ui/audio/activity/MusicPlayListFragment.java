package com.yuhuankj.tmxq.ui.audio.activity;

import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.music.IMusicDownloaderCore;
import com.tongdaxing.xchat_core.music.IMusicDownloaderCoreClient;
import com.tongdaxing.xchat_core.music.bean.HotMusicInfo;
import com.tongdaxing.xchat_core.player.IPlayerCore;
import com.tongdaxing.xchat_core.player.IPlayerCoreClient;
import com.tongdaxing.xchat_core.player.bean.LocalMusicInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.base.fragment.BaseLazyFragment;
import com.yuhuankj.tmxq.ui.audio.adapter.MusicPlayListAdapter;
import com.yuhuankj.tmxq.ui.widget.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MusicPlayListFragment extends BaseLazyFragment {

    private final String TAG = MusicPlayListFragment.class.getSimpleName();

    private RecyclerView rv_musicList;
    private View ll_musicList;
    private TextView tv_total;
    private View ll_searchEmpty;
    private EditText et_search;

    private ExecutorService executorService = Executors.newSingleThreadExecutor();
    private MusicPlayListAdapter adapter;
    private List<LocalMusicInfo> localPlayListMusicInfos = new ArrayList<>();
    private List<LocalMusicInfo> searchMusicInfoList = new ArrayList<>();

    private boolean isSearchListShowCurr = false;

    /**
     * 数据懒加载
     */
    @Override
    protected void onLazyLoadData() {
        if(localPlayListMusicInfos.size() == 0){
            localPlayListMusicInfos = CoreManager.getCore(IPlayerCore.class).getPlayerListMusicInfos();
        }
        refreshMusicListView(localPlayListMusicInfos);
    }

    private void refreshMusicListView(List<LocalMusicInfo> musicInfos) {
        adapter.setLocalMusicInfos(musicInfos);
        adapter.notifyDataSetChanged();
        boolean musicListEmpty = null == musicInfos || musicInfos.size() == 0;
        ll_musicList.setVisibility(musicListEmpty ? View.GONE : View.VISIBLE);
        ll_searchEmpty.setVisibility(musicListEmpty ? View.VISIBLE : View.GONE);
        String totalStr = musicListEmpty ? "0" : String.valueOf(musicInfos.size());
        if (getActivity() != null) {
            String totalTips = getActivity().getResources().getString(R.string.music_library_my_total, totalStr);
            tv_total.setText(totalTips);
        }
    }

    private void insertMusicInfoToView() {
        adapter.notifyItemInserted(0);
        ll_musicList.setVisibility(View.VISIBLE);
        ll_searchEmpty.setVisibility(View.GONE);
        String totalStr =String.valueOf(isSearchListShowCurr ? searchMusicInfoList.size() : localPlayListMusicInfos.size());
        if (getActivity() != null) {
            String totalTips = getActivity().getResources().getString(R.string.music_library_my_total, totalStr);
            tv_total.setText(totalTips);
        }
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_my_music_library;
    }

    @Override
    public void onFindViews() {
        tv_total = mView.findViewById(R.id.tv_total);
        ll_musicList = mView.findViewById(R.id.ll_musicList);
        rv_musicList = mView.findViewById(R.id.rv_musicList);
        ll_searchEmpty = mView.findViewById(R.id.ll_searchEmpty);
        et_search = mView.findViewById(R.id.et_search);
        GrowingIO.getInstance().trackEditText(et_search);
        et_search.setVisibility(View.VISIBLE);
        et_search.clearFocus();
        et_search.addTextChangedListener(searchWatcher);
        adapter = new MusicPlayListAdapter(getActivity());

        adapter.setLocalMusicInfos(localPlayListMusicInfos);
        LinearLayoutManager linearLayoutManager = new RecyclerViewNoBugLinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_musicList.setLayoutManager(linearLayoutManager);
        rv_musicList.setAdapter(adapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(), linearLayoutManager.getOrientation(),
                1, R.color.color_1Affffff);
        dividerItemDecoration.setIngoreLastDividerItem(true);
        rv_musicList.addItemDecoration(dividerItemDecoration);
    }

    private TextWatcher searchWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if(null != localPlayListMusicInfos && localPlayListMusicInfos.size()>0){
                String key = s.toString();
                if(!TextUtils.isEmpty(key) && !TextUtils.isEmpty(key.trim())){
                    searchMusicFromPlayList(key);
                }else{
                    isSearchListShowCurr = false;
                    refreshMusicListView(localPlayListMusicInfos);
                }
            }
        }
    };

    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            isSearchListShowCurr = true;
            refreshMusicListView(searchMusicInfoList);
        }
    };

    private void searchMusicFromPlayList(final String key){
        searchMusicInfoList.clear();
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                if (ListUtils.isListEmpty(localPlayListMusicInfos)) {
                    return;
                }
                for(LocalMusicInfo musicInfo : localPlayListMusicInfos){
                    List<String> artistNames = musicInfo.getArtistNames();
                    if(null != artistNames && artistNames.size()>0){
                        if(musicInfo.getSongName().toLowerCase().contains(key.toLowerCase()) || artistNames.get(0).toLowerCase().contains(key.toLowerCase())){
                            if (searchMusicInfoList != null) {
                                searchMusicInfoList.add(musicInfo);
                            }
                        }
                    }
                }
                handler.sendEmptyMessage(0);
            }
        });
    }



    @Override
    public void onSetListener() {
        adapter.setOnPlayErrorListener(new MusicPlayListAdapter.OnPlayErrorListener() {
            @Override
            public void onPlayError(String msg) {
                toast(msg);
            }
        });
        adapter.setOnLongClickListener(new MusicPlayListAdapter.OnMusicListItemLongClickListener() {
            @Override
            public void onMusicListItemLongClicked(LocalMusicInfo localMusicInfo) {
                deleteMusicItemFromPlayList(localMusicInfo);
            }
        });
    }

    private void deleteMusicItemFromPlayList(LocalMusicInfo localMusicInfo){
        getDialogManager().showOkCancelDialog(getActivity().getResources().getString(R.string.music_list_play_delete_tips),
                true, new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {
                        getDialogManager().dismissDialog();
                    }

                    @Override
                    public void onOk() {
                        if(localMusicInfo.getLocalId()>0L){
                            CoreManager.getCore(IPlayerCore.class).deleteMusicFromPlayerListAndUpdateDb(localMusicInfo);
                        }else{
                            CoreManager.getCore(IMusicDownloaderCore.class).deleteHotMusicDownloadingFromPlayList(localMusicInfo.getRemoteUri());
                        }
                        localPlayListMusicInfos.remove(localMusicInfo);
                        searchMusicInfoList.remove(localMusicInfo);
                        refreshMusicListView(isSearchListShowCurr ? searchMusicInfoList : localPlayListMusicInfos);
                    }
                });
    }

    @Override
    public void initiate() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        executorService.shutdownNow();
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
            handler = null;
        }
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onPlayerListInsert(LocalMusicInfo musicInfo) {
        synchronized (localPlayListMusicInfos){
            int index = localPlayListMusicInfos.indexOf(musicInfo);
            if(-1 == index){
                localPlayListMusicInfos.add(0,musicInfo);
                if(!isSearchListShowCurr){
                    insertMusicInfoToView();
                }
            }

            List<String> artistNames = musicInfo.getArtistNames();
            String key = et_search.getText().toString();
            int searchIndex = searchMusicInfoList.indexOf(musicInfo);
            if(-1 == searchIndex && !TextUtils.isEmpty(key) && null != artistNames && artistNames.size()>0){
                if(musicInfo.getSongName().toLowerCase().contains(key.toLowerCase()) || artistNames.get(0).toLowerCase().contains(key.toLowerCase())){
                    searchMusicInfoList.add(0,musicInfo);
                    if(isSearchListShowCurr){
                        insertMusicInfoToView();
                    }
                }
            }
        }
    }

    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient.class)
    public void onHotMusicDownloadProgressUpdate(LocalMusicInfo localMusicInfo, HotMusicInfo hotMusicInfo, int progress) {
        LogUtils.d(TAG,"onHotMusicDownloadProgressUpdate-localMusicInfo.songId:"+localMusicInfo.getSongId());
        String key = et_search.getText().toString();
        if(TextUtils.isEmpty(key) || !TextUtils.isEmpty(key.trim())){
            int index = isSearchListShowCurr ? searchMusicInfoList.indexOf(localMusicInfo) : localPlayListMusicInfos.indexOf(localMusicInfo);
            if(-1 == index){
                return;
            }
            try {
                adapter.notifyItemChanged(index,progress);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient.class)
    public void onHotMusicDownloadError(String msg, LocalMusicInfo localMusicInfo, HotMusicInfo hotMusicInfo) {
        synchronized (localPlayListMusicInfos){
            int index = localPlayListMusicInfos.indexOf(localMusicInfo);
            if(-1 == index){
                return;
            }
            localPlayListMusicInfos.remove(localMusicInfo);
            if (!isSearchListShowCurr) {//移除数据需要通知列表更新
                adapter.notifyDataSetChanged();
            }
            searchMusicInfoList.remove(localMusicInfo);
            if (isSearchListShowCurr) {
                adapter.notifyDataSetChanged();
            }
        }
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onPlayerListMusicUpdate(LocalMusicInfo oldMusicInfo, LocalMusicInfo newMusicInfo) {
        synchronized (localPlayListMusicInfos){
            try {
                int index = localPlayListMusicInfos.indexOf(oldMusicInfo);
                if(-1 == index){
                    return;
                }
                localPlayListMusicInfos.set(index,newMusicInfo);
                if(!isSearchListShowCurr){
                    adapter.notifyItemChanged(index);
                }
                index = searchMusicInfoList.indexOf(oldMusicInfo);
                if(-1 != index){
                    searchMusicInfoList.set(index,newMusicInfo);
                    if(isSearchListShowCurr){
                        adapter.notifyItemChanged(index);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private LocalMusicInfo currMusicInfo;

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicPlaying(LocalMusicInfo localMusicInfo) {
        adapter.notifyDataSetChanged();
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicPause(LocalMusicInfo localMusicInfo) {
        adapter.notifyDataSetChanged();
    }
}
