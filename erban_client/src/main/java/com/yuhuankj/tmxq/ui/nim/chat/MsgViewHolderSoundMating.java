package com.yuhuankj.tmxq.ui.nim.chat;

import android.view.ViewGroup;
import android.widget.TextView;

import com.netease.nim.uikit.common.ui.recyclerview.adapter.BaseMultiItemFetchLoadAdapter;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.netease.nim.uikit.session.viewholder.MsgViewHolderBase;
import com.tongdaxing.xchat_core.im.custom.bean.SoundMatingBean;
import com.tongdaxing.xchat_core.im.custom.bean.SoundMatingMsgAttachment;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.widget.CircleImageView;

/**
 * @author liaoxy
 * @Description:声鉴卡从健伴侣页跳到私聊页IM消息
 * @date 2019/6/6 16:25
 */
public class MsgViewHolderSoundMating extends MsgViewHolderBase {

    private CircleImageView imvUser1;
    private CircleImageView imvUser2;
    private TextView imvMatingValue;
    private TextView tvContent1;
    private TextView tvContent2;
    private TextView tvContent3;

    public MsgViewHolderSoundMating(BaseMultiItemFetchLoadAdapter adapter) {
        super(adapter);
        setMiddleItem(true);
        setShowHeadImage(false);
    }

    @Override
    protected int getContentResId() {
        return R.layout.layout_msg_view_holder_sound_mating;
    }

    @Override
    protected void inflateContentView() {
        imvUser1 = findViewById(R.id.imvUser1);
        imvUser2 = findViewById(R.id.imvUser2);
        imvMatingValue = findViewById(R.id.imvMatingValue);
        tvContent1 = findViewById(R.id.tvContent1);
        tvContent2 = findViewById(R.id.tvContent2);
        tvContent3 = findViewById(R.id.tvContent3);

        int width = ScreenUtil.getScreenWidth(context) - ScreenUtil.dip2px(34);
        setLayoutParams(width, ViewGroup.LayoutParams.WRAP_CONTENT, contentContainer);
    }

    @Override
    protected void bindContentView() {
        contentContainer.setPadding(0, 0, 0, 0);
        SoundMatingMsgAttachment attachment = (SoundMatingMsgAttachment) message.getAttachment();
        if (null != attachment && attachment.getDataInfo() != null) {
            SoundMatingBean dataInfo = attachment.getDataInfo();
            ImageLoadUtils.loadImage(context, dataInfo.getTargetAvatar(), imvUser1, R.drawable.default_user_head);
            ImageLoadUtils.loadImage(context, dataInfo.getAvatar(), imvUser2, R.drawable.default_user_head);
            imvMatingValue.setText("匹配度：" + dataInfo.getMatchRatio());
            tvContent1.setText(dataInfo.getIntroduce());
            tvContent2.setText(dataInfo.getQuestion1());
            tvContent3.setText(dataInfo.getQuestion2());
        }
    }
}
