package com.yuhuankj.tmxq.ui.me.setting;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;

import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.ButtonUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.PreferencesUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_core.user.VersionsCoreClient;
import com.tongdaxing.xchat_core.user.bean.CheckUpdataBean;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.databinding.ActivitySettingBinding;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.login.BinderPhoneActivity;
import com.yuhuankj.tmxq.ui.login.ThirdPlatformBindingActivity;
import com.yuhuankj.tmxq.ui.me.noble.NobleBusinessManager;
import com.yuhuankj.tmxq.ui.me.setting.other.AboutActivity;
import com.yuhuankj.tmxq.ui.me.setting.other.FeedbackActivity;
import com.yuhuankj.tmxq.ui.me.setting.other.LabActivity;
import com.yuhuankj.tmxq.ui.me.setting.scan.QrCodeScanerActivity;
import com.yuhuankj.tmxq.ui.user.password.ModifyPwdActivity;
import com.yuhuankj.tmxq.ui.user.password.SetingPwdActivity;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;
import com.yuhuankj.tmxq.utils.UIHelper;

//import com.squareup.leakcanary.LeakCanaryWithoutDisplay;

//import com.squareup.leakcanary.LeakCanaryWithoutDisplay;

/**
 * Created by zhouxiangfeng on 2017/4/16.
 */
@CreatePresenter(SettingPresenter.class)
public class SettingActivity extends BaseMvpActivity<SettingView, SettingPresenter>
        implements View.OnClickListener, SettingView {

    private final String TAG = SettingActivity.class.getSimpleName();
    private ActivitySettingBinding settingBinding;

    private UserInfo userInfo;
    private boolean isFirst = true;
    private CompoundButton.OnCheckedChangeListener onCheckedChangeListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        settingBinding = DataBindingUtil.setContentView(this, R.layout.activity_setting);
        settingBinding.setClick(this);
        initTitleBar("设置");
        mTitleBar.setCommonBackgroundColor(Color.WHITE);
        initView();
        initData();
//        findViewById(R.id.tv_send_msg).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (AvRoomDataManager.get().mCurrentRoomInfo != null) {
//                    for (int i = 0;i < 30;i++) {
//                        IMNetEaseManager.get().sendTextMsg(AvRoomDataManager.get().mCurrentRoomInfo.getRoomId(), "第二个 = "+i, "").subscribe(new BiConsumer<ChatRoomMessage, Throwable>() {
//                            @Override
//                            public void accept(ChatRoomMessage chatRoomMessage,
//                                               Throwable throwable) throws Exception {
//                                if (throwable != null) {
//                                    LogUtils.d(TAG, "sendTextMsg 发送房间信息失败" + throwable.getMessage());
//                                } else {
//                                    IMNetEaseManager.get().addMessagesImmediately(chatRoomMessage);
//                                }
//                            }
//                        });
//                    }
//                }
//            }
//        });
    }

    private void initData() {
        settingBinding.versions.setText("V" + BasicConfig.getLocalVersionName(getApplicationContext()));

        userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (null != userInfo) {
            settingBinding.rlEnterRoomInvisiable.setVisibility(NobleBusinessManager.showEnterRoomInvisiableSwitch(userInfo.getVipId(), userInfo.getVipDate()) ? View.VISIBLE : View.GONE);
            settingBinding.sbEnterRoomInvisiable.setChecked(userInfo.getIsInvisible());
            settingBinding.sbCloseNearbyFunc.setChecked(userInfo.getInNearby() == 1);
            settingBinding.sbRingOnMsgComing.setChecked(userInfo.getMessageRingRestriction() == 1);
            settingBinding.sbOnlyRecvFriendMsg.setChecked(userInfo.getMessageRestriction() == 1);
            settingBinding.tvAuthStatus.setText(getResources().getString(userInfo.isRealNameAudit() ?
                    R.string.real_name_auth_status_certified : R.string.real_name_auth_status_uncertified));
            settingBinding.tvAuthStatus.setCompoundDrawablesWithIntrinsicBounds(null, null,
                    userInfo.isRealNameAudit() ? null : getResources().getDrawable(R.drawable.setting_right), null);

            settingBinding.tvLoginPwdTip.setText(userInfo.isPassword() ? "修改" : "设置");
            settingBinding.tvPayPwdTip.setText(userInfo.isPayPassword() ? "修改" : "设置");
        }
        onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                RoomInfo current = RoomServiceScheduler.getCurrentRoomInfo();
                if (current != null) {
                    getDialogManager().showOkCancelDialog(getResources().getString(R.string.room_invisiable_switch_tips),
                            true, new DialogManager.OkCancelDialogListener() {
                                @Override
                                public void onCancel() {
                                    settingBinding.sbEnterRoomInvisiable.setOnCheckedChangeListener(null);
                                    settingBinding.sbEnterRoomInvisiable.setChecked(null != userInfo && userInfo.getIsInvisible());
                                    settingBinding.sbEnterRoomInvisiable.setOnCheckedChangeListener(onCheckedChangeListener);
                                }

                                @Override
                                public void onOk() {
                                    getMvpPresenter().exitRoom();
                                    getDialogManager().showProgressDialog(SettingActivity.this, getResources().getString(R.string.network_loading));
                                    getMvpPresenter().changeNobleEnterRoomVisiableStatus(isChecked);
                                }
                            });

                    return;
                }
                getDialogManager().showProgressDialog(SettingActivity.this, getResources().getString(R.string.network_loading));
                getMvpPresenter().changeNobleEnterRoomVisiableStatus(isChecked);

            }
        };
        settingBinding.sbEnterRoomInvisiable.setOnCheckedChangeListener(onCheckedChangeListener);

        String messageRestrictionOption = CoreManager.getCore(VersionsCore.class).getConfigData().str("messageRestrictionOption");
        settingBinding.rlOnlyRecvFriendMsg.setVisibility(!TextUtils.isEmpty(messageRestrictionOption) && messageRestrictionOption.equals("1") ? View.VISIBLE : View.GONE);
        settingBinding.sbOnlyRecvFriendMsg.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                getDialogManager().showProgressDialog(SettingActivity.this, getResources().getString(R.string.network_loading));
                UserInfo userInfo = new UserInfo();
                userInfo.setMessageRestriction(isChecked ? 1 : 0);
                userInfo.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
                CoreManager.getCore(IUserCore.class).requestUpdateUserInfo(userInfo);
            }
        });

        settingBinding.sbRingOnMsgComing.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                getDialogManager().showProgressDialog(SettingActivity.this, getResources().getString(R.string.network_loading));
                UserInfo userInfo = new UserInfo();
                userInfo.setMessageRingRestriction(isChecked ? 1 : 0);
                userInfo.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
                CoreManager.getCore(IUserCore.class).requestUpdateUserInfo(userInfo);
            }
        });

        settingBinding.sbCloseNearbyFunc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                getDialogManager().showProgressDialog(SettingActivity.this, getResources().getString(R.string.network_loading));
                UserInfo userInfo = new UserInfo();
                userInfo.setInNearby(isChecked ? 1 : 0);
                userInfo.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
                CoreManager.getCore(IUserCore.class).requestUpdateUserInfo(userInfo);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isFirst) {
            userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            if (null != userInfo) {
                settingBinding.rlEnterRoomInvisiable.setVisibility(NobleBusinessManager.showEnterRoomInvisiableSwitch(userInfo.getVipId(), userInfo.getVipDate()) ? View.VISIBLE : View.GONE);
                settingBinding.sbEnterRoomInvisiable.setChecked(userInfo.getIsInvisible());
                settingBinding.sbCloseNearbyFunc.setChecked(userInfo.getInNearby() == 1);
                settingBinding.sbRingOnMsgComing.setChecked(userInfo.getMessageRingRestriction() == 1);
                settingBinding.sbOnlyRecvFriendMsg.setChecked(userInfo.getMessageRestriction() == 1);
                settingBinding.tvAuthStatus.setText(getResources().getString(userInfo.isRealNameAudit() ?
                        R.string.real_name_auth_status_certified : R.string.real_name_auth_status_uncertified));
                settingBinding.tvAuthStatus.setCompoundDrawablesWithIntrinsicBounds(null, null,
                        userInfo.isRealNameAudit() ? null : getResources().getDrawable(R.drawable.setting_right), null);

                settingBinding.tvLoginPwdTip.setText(userInfo.isPassword() ? "修改" : "设置");
                settingBinding.tvPayPwdTip.setText(userInfo.isPayPassword() ? "修改" : "设置");
            }
        }
        isFirst = false;
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfoUpdate(UserInfo userInfo) {
        getDialogManager().dismissDialog();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfoUpdateError(String error) {
        toast(error);
        getDialogManager().dismissDialog();
    }

    @Override
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick(view.getId())) {
            return;
        }
        switch (view.getId()) {
            case R.id.rly_auth:
                CommonWebViewActivity.start(this, UriProvider.getRealNameAuthUrl());
                break;
            case R.id.rly_qrCodeScan:
                //扫一扫
                QrCodeScanerActivity.start(this);
                break;
            case R.id.rly_content:
                //反馈
                startActivity(new Intent(getApplicationContext(), FeedbackActivity.class));
                break;
            case R.id.rly_binder:
                //绑定手机
                ThirdPlatformBindingActivity.start(this, null);
                break;
            case R.id.rly_contact_us:
                UIHelper.openContactUs(this);
                break;
            case R.id.rly_help:
                CommonWebViewActivity.start(this, UriProvider.getHelpUrl());
                break;
            case R.id.rlyAboutUs:
                //关于我们
                startActivity(new Intent(getApplicationContext(), AboutActivity.class));
                break;
            case R.id.rly_check:
                //检查更新
                CoreManager.getCore(VersionsCore.class).checkVersion();
                break;
            case R.id.rly_lab:
                startActivity(new Intent(getApplicationContext(), LabActivity.class));
                break;
            case R.id.btn_login_out:
                //退出登录
//                if (LeakCanaryWithoutDisplay.isRunningMonkey) {
//                    toast("monkey你别想退出登录！");
//                } else {
                getDialogManager().showProgressDialog(this, getResources().getString(R.string.network_loading));
                CoreManager.getCore(IAuthCore.class).logout();
//                }

                break;
            case R.id.rly_blacklist:
                //黑名单
//                startActivity(new Intent(getApplicationContext(), BlackListActivity.class));
                break;
            case R.id.rlyUserAgreement:
                CommonWebViewActivity.start(this, UriProvider.getUserAgreementUrl());
                break;
            case R.id.rlPayPwd:
                if (userInfo != null) {
                    Intent intent = null;
                    if (!userInfo.isBindPhone()) {
                        intent = new Intent(this, BinderPhoneActivity.class);
                    } else if (userInfo.isPayPassword()) {
                        intent = new Intent(this, ModifyPwdActivity.class);
                    } else {
                        intent = new Intent(this, SetingPwdActivity.class);
                    }
                    intent.putExtra("type", 2);
                    startActivity(intent);
                }
                break;
            case R.id.rlLoginPwd:
                if (userInfo != null) {
                    Intent intent = null;
                    if (!userInfo.isBindPhone()) {
                        intent = new Intent(this, BinderPhoneActivity.class);
                    } else if (userInfo.isPassword()) {
                        intent = new Intent(this, ModifyPwdActivity.class);
                    } else {
                        intent = new Intent(this, SetingPwdActivity.class);
                    }
                    intent.putExtra("type", 1);
                    startActivity(intent);
                }
                break;
            default:
                break;
        }
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogout() {
        getDialogManager().dismissDialog();
        PreferencesUtils.setFristQQ(true);
        finish();
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogoutFaith(String error) {
        getDialogManager().dismissDialog();
        if (!TextUtils.isEmpty(error)) {
            toast(error);
        }
    }

    private void initView() {
        if (BasicConfig.INSTANCE.isDebuggable()) {
            settingBinding.rlyLab.setVisibility(View.VISIBLE);
        } else {
            settingBinding.rlyLab.setVisibility(View.GONE);
        }
    }

    @CoreEvent(coreClientClass = VersionsCoreClient.class)
    public void onVersionUpdataDialog(CheckUpdataBean checkUpdataBean) {

        if (checkUpdataBean == null) {
            return;
        }

        if (checkUpdataBean.getStatus() == 1) {
            toast("已经是最新版本");
            settingBinding.rlyCheck.setEnabled(false);
            return;
        }


        AlertDialog alertDialog = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)

                .setTitle("检测到有最新的版本是否更新")
                .setMessage(checkUpdataBean.getUpdateVersionDesc())
                .setPositiveButton("更新", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String download_url = checkUpdataBean.getDownloadUrl();
                        if (TextUtils.isEmpty(download_url)) {
                            download_url = "https://www.baidu.com/";
                        }
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(download_url));
                        startActivity(intent);

                    }
                })
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                    }
                }).create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(true);
        alertDialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getDialogManager().dismissDialog();
    }

    @Override
    public void onChangeNobleEnterRoomVisiableStatus(boolean isSuccess, String message) {
        LogUtils.d(TAG, "onChangeNobleEnterRoomVisiableStatus-isSuccess:" + isSuccess + " message:" + message);
        if (!isSuccess) {
            getDialogManager().dismissDialog();
            if (!TextUtils.isEmpty(message)) {
                toast(message);
            }
        } else {
            CoreManager.getCore(IUserCore.class).requestUserInfo(
                    CoreManager.getCore(IAuthCore.class).getCurrentUid());
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfo(UserInfo info) {
        getDialogManager().dismissDialog();
        LogUtils.d(TAG, "onRequestUserInfo-info:" + info);
        if (info.getUid() == CoreManager.getCore(IAuthCore.class).getCurrentUid()) {
            userInfo = info;
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfoError(String error) {
        getDialogManager().dismissDialog();
        if (!TextUtils.isEmpty(error)) {
            toast(error);
        }
    }
}
