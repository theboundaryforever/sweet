package com.yuhuankj.tmxq.ui.liveroom.imroom.pk;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.pk.bean.PkVoteInfo;

/**
 * 文件描述：PK View的P层接口层 通知view更新
 *
 * @auther：zwk
 * @data：2019/7/9
 */
public interface IRoomPKView extends IMvpBaseView {
    void initPKViewFromVoteInfo(PkVoteInfo pkVoteInfo);

    void updatePKViewFromVoteInfo(PkVoteInfo pkVoteInfo);

    void getPKVoteInfoFail(String error);

    void pkVoteSuc(PkVoteInfo pkVoteInfo);

    void pkVoteFail(String error);
}
