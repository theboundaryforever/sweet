package com.yuhuankj.tmxq.ui.liveroom.imroom.room.view;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

public interface IRoomHeatView<T extends AbstractMvpPresenter> extends IMvpBaseView {

    void refreshRoomHeatData(long rank, long value);
}
