package com.yuhuankj.tmxq.ui.liveroom.nimroom.box;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

/**
 * Created by Administrator on 2018/4/11.
 */

public class LotteryGiftAdapter extends BaseQuickAdapter<Json, LotteryGiftAdapter.ViewHolder> {


    public LotteryGiftAdapter() {
        super(R.layout.item_lottery_gift);
    }

    @Override
    protected void convert(ViewHolder helper, Json item) {
        ImageLoadUtils.loadImage(mContext, item.str("icon"), helper.ivGiftIcon);
        helper.tvGiftInfo.setText(item.str("info"));

    }


    public class ViewHolder extends BaseViewHolder {
        ImageView ivGiftIcon;
        TextView tvGiftInfo;

        public ViewHolder(View view) {
            super(view);
            ivGiftIcon = view.findViewById(R.id.iv_lottery_dialog_gift_icon);
            tvGiftInfo = view.findViewById(R.id.tv_lottery_dialog_gift_info);
        }
    }


}
