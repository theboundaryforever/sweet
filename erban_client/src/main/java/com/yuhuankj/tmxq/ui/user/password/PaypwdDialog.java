package com.yuhuankj.tmxq.ui.user.password;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatDialog;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.DialogManager;

/**
 * @author liaoxy
 * @Description:提现密码弹出窗口
 * @date 2019/3/27 17:10
 */
public class PaypwdDialog extends AppCompatDialog implements View.OnClickListener {
    private final String TAG = PaypwdDialog.class.getSimpleName();
    private EditText edtPwd;
    private View vPwdTip;
    private View tvForgetPwd;
    private TextView tvCancel;
    private TextView tvEnter;
    private View.OnClickListener clickListener;
    private Context context;
    private String enterStr;
    private Handler handler = new Handler(Looper.getMainLooper());

    public PaypwdDialog(Context context, String enterStr, View.OnClickListener clickListener) {
        super(context, R.style.dialog);
        this.clickListener = clickListener;
        this.context = context;
        this.enterStr = enterStr;
        init();
    }

    private void init() {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_pay_pwd, null, false);
        setContentView(view);
        if (getWindow() == null) {
            return;
        }
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = (int) (ScreenUtil.getScreenWidth(context) * 0.8);
        params.gravity = Gravity.CENTER;
        getWindow().setAttributes(params);
        setCanceledOnTouchOutside(true);

        edtPwd = view.findViewById(R.id.edtPwd);
        GrowingIO.getInstance().trackEditText(edtPwd);
        vPwdTip = view.findViewById(R.id.vPwdTip);
        tvForgetPwd = view.findViewById(R.id.tvForgetPwd);
        tvCancel = view.findViewById(R.id.tvCancel);
        tvEnter = view.findViewById(R.id.tvEnter);
        if (!TextUtils.isEmpty(enterStr)) {
            tvEnter.setText(enterStr);
        }
        tvCancel.setOnClickListener(this);
        tvEnter.setOnClickListener(this);
        tvForgetPwd.setOnClickListener(this);
        vPwdTip.setOnClickListener(this);
    }

    @Override
    public void dismiss() {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(edtPwd.getWindowToken(), 0); //强制隐藏键盘
        }
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
            handler = null;
        }
        super.dismiss();
    }

    @Override
    public void show() {
        super.show();
        try {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null) {
                        imm.showSoftInput(edtPwd, InputMethodManager.SHOW_FORCED);
                    }
                }
            }, 100);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == tvCancel) {
            dismiss();
        } else if (view == tvEnter) {
            checkPayPwd();
        } else if (view == tvForgetPwd) {
            try {
                Intent intent = new Intent(context, ResetPwdActivity.class);
                intent.putExtra("type", 2);
                context.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (view == vPwdTip) {
            vPwdTip.setSelected(!vPwdTip.isSelected());
            if (!vPwdTip.isSelected()) {
                edtPwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            } else {
                edtPwd.setInputType(InputType.TYPE_CLASS_TEXT);
            }
            edtPwd.setSelection(edtPwd.getText().length());
        }
    }

    private void checkPayPwd() {
        String password = edtPwd.getText().toString();
        if (TextUtils.isEmpty(password)) {
            SingleToastUtil.showToast("提现密码不能为空");
            return;
        }
        if (password.length() < 6 || password.length() > 16) {
            SingleToastUtil.showToast("提现密码长度为6-16位");
            return;
        }
        DialogManager dialogManager = new DialogManager(context);
        dialogManager.showProgressDialog(context, "加载中...");
        new PwdModel().checkPayPwd(password, new OkHttpManager.MyCallBack<ServiceResult<Boolean>>() {
            @Override
            public void onError(Exception e) {
                dialogManager.dismissDialog();
                e.printStackTrace();
                SingleToastUtil.showToast(e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<Boolean> response) {
                dialogManager.dismissDialog();
                if (response != null && response.isSuccess()) {
                    boolean isPass = response.getData();
                    if (!isPass) {
                        SingleToastUtil.showToast("提现密码不正确");
                        return;
                    }
                    if (clickListener != null) {
                        clickListener.onClick(tvEnter);
                    }
                    dismiss();
                } else {
                    if (response != null && !TextUtils.isEmpty(response.getMessage())) {
                        SingleToastUtil.showToast(response.getMessage());
                    } else {
                        SingleToastUtil.showToast("检测提现密码失败");
                    }
                }
            }
        });
    }
}
