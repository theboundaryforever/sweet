package com.yuhuankj.tmxq.ui.me.wallet.income.withdraw;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.growingio.android.sdk.collection.GrowingIO;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.withdraw.IWithdrawCore;
import com.tongdaxing.xchat_core.withdraw.IWithdrawCoreClient;
import com.tongdaxing.xchat_core.withdraw.bean.WithdrawInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.ui.login.CodeDownTimer;

public class BinderAlipayActivity extends BaseActivity {
    private EditText etAlipayAccount;
    private EditText etAlipayName;
    private EditText etSmsCode;
    private Button btnGetCode;
    private Button btnBinder;
    private Button btnBinderRquest;


    private TextWatcher textWatcher;

    public static void start(Context context) {
        Intent intent = new Intent(context, BinderAlipayActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_binder_alipay);
        initTitleBar("绑定支付宝");
        initView();
        initData();
        onSetListener();
    }

    private void onSetListener() {
        //获取绑定支付宝验证码
        btnGetCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CodeDownTimer timer = new CodeDownTimer(btnGetCode, 60000, 1000);
                timer.start();
                CoreManager.getCore(IWithdrawCore.class).getSmsCode(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            }
        });

        //请求绑定支付宝
        btnBinderRquest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                System.out.println("aliPayAccount"+aliPayAccount+"aliPayAccountName"+aliPayAccountName+"code"+code);
                CoreManager.getCore(IWithdrawCore.class).binderAlipay(etAlipayAccount.getText().toString(), etAlipayName.getText().toString(), etSmsCode.getText().toString());
            }
        });
        //输入框监听改变
        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (etAlipayAccount.getText() != null && etAlipayAccount.getText().length() > 0
                        && etAlipayName.getText() != null && etAlipayName.getText().length() > 0
                        && etSmsCode.getText() != null && etSmsCode.getText().length() > 0) {
                    btnBinder.setVisibility(View.GONE);
                    btnBinderRquest.setVisibility(View.VISIBLE);
                } else {
                    btnBinder.setVisibility(View.VISIBLE);
                    btnBinderRquest.setVisibility(View.GONE);
                }
            }
        };

        etAlipayAccount.addTextChangedListener(textWatcher);
        etAlipayName.addTextChangedListener(textWatcher);
        etSmsCode.addTextChangedListener(textWatcher);

    }

    private void initData() {
        WithdrawInfo info = (WithdrawInfo) getIntent().getSerializableExtra("withdrawInfo");
        if (info != null) {
            setupData(info);
        } else {
            getDialogManager().showProgressDialog(this, "请稍后...");
            CoreManager.getCore(IWithdrawCore.class).getWithdrawUserInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        }
    }

    private void setupData(WithdrawInfo info) {
        etAlipayAccount.setText("");
        etAlipayName.setText("");
        if (info != null && !info.isNotBoundPhone) {
            if (StringUtils.isNotEmpty(info.alipayAccount))
                etAlipayAccount.setText(info.alipayAccount + "");
            if (StringUtils.isNotEmpty(info.alipayAccountName))
                etAlipayName.setText(info.alipayAccountName + "");
        }
    }

    private void initView() {
        etAlipayAccount = (EditText) findViewById(R.id.et_phone);
        GrowingIO.getInstance().trackEditText(etAlipayAccount);
     /*   //不需要判断支付宝账号格式，布局里面也不要写type=phone
        etAlipayAccount.addValidator(new AccountValidator("Only Integer Valid!", "\\d+"));
        etAlipayAccount.setFilters(new InputFilter[]{new InputFilter.LengthFilter(11)});*/
        etAlipayName = (EditText) findViewById(R.id.et_name);
        GrowingIO.getInstance().trackEditText(etAlipayName);
        etSmsCode = (EditText) findViewById(R.id.et_smscode);
        GrowingIO.getInstance().trackEditText(etSmsCode);
        etSmsCode.setFilters(new InputFilter[]{new InputFilter.LengthFilter(5)});
        btnGetCode = (Button) findViewById(R.id.btn_get_code);
        btnBinder = (Button) findViewById(R.id.btn_binder);
        btnBinderRquest = (Button) findViewById(R.id.btn_binder_request);

    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onGetSmsCodeFail(String error) {
        toast(error);
    }


    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onBinderAlipay() {
        finish();
    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onBinderAlipayFail(String error) {
        toast(error);
    }


    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onGetWithdrawUserInfo(WithdrawInfo withdrawInfo) {
        getDialogManager().dismissDialog();
        setupData(withdrawInfo);
    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onGetWithdrawUserInfoFail(String error) {
        getDialogManager().dismissDialog();
        setupData(null);
    }

}
