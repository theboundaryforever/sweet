package com.yuhuankj.tmxq.ui.signAward.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author weihaitao
 * @date 2019/5/22
 */
public class SignAwardResponse implements Serializable {

    /**
     * 麦上用户列表
     */
    private List<RecommMicUserInfo> micRoomUserList;
    /**
     * 萌新大礼包列表 -- 以该字段为依据展示哪个流程界面，如果newsBigGiftMap为空，则走连续签到逻辑
     */
    private Map<Integer, List<SignRecvGiftPkgInfo>> newsBigGiftMap;
    /**
     * 当前领取天数 -- 对应萌新大礼包 取值1、2、3
     */
    private int getDay = 0;
    /**
     * 当前签到天数
     */
    private int signDay;
    /**
     * 签到任务列表
     */
    private List<SignInAwardInfo> signInList;
    /**
     * 0表示大礼包，1表示签到,2表示当天已签到
     */
    private int isSign = -1;

    public Map<Integer, List<SignRecvGiftPkgInfo>> getNewsBigGiftMap() {
        return newsBigGiftMap;
    }

    public void setNewsBigGiftMap(Map<Integer, List<SignRecvGiftPkgInfo>> newsBigGiftMap) {
        this.newsBigGiftMap = newsBigGiftMap;
    }

    public List<RecommMicUserInfo> getMicRoomUserList() {
        return micRoomUserList;
    }

    public void setMicRoomUserList(List<RecommMicUserInfo> micRoomUserList) {
        this.micRoomUserList = micRoomUserList;
    }

    public int getGetDay() {
        return getDay;
    }

    public void setGetDay(int getDay) {
        this.getDay = getDay;
    }

    public int getSignDay() {
        return signDay;
    }

    public void setSignDay(int signDay) {
        this.signDay = signDay;
    }

    public List<SignInAwardInfo> getSignInList() {
        return signInList;
    }

    public void setSignInList(List<SignInAwardInfo> signInList) {
        this.signInList = signInList;
    }

    public int getIsSign() {
        return isSign;
    }

    public void setIsSign(int isSign) {
        this.isSign = isSign;
    }
}
