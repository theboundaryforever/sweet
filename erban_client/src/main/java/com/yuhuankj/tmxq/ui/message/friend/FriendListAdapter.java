package com.yuhuankj.tmxq.ui.message.friend;

import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.bindadapter.BaseAdapter;
import com.yuhuankj.tmxq.base.bindadapter.BindingViewHolder;
import com.yuhuankj.tmxq.databinding.ListItemFriendBinding;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

/**
 * Created by chenran on 2017/10/3.
 */

public class FriendListAdapter extends BaseAdapter<NimUserInfo> {

    int imgWidthHeight = 0;

    public FriendListAdapter(int layoutResId, int brid) {
        super(layoutResId, brid);
    }

    @Override
    protected void convert(BindingViewHolder helper, NimUserInfo item) {
        super.convert(helper, item);
        ListItemFriendBinding binding = (ListItemFriendBinding) helper.getBinding();
        if (GlideContextCheckUtil.checkContextUsable(binding.civAvatar.getContext())) {
            if (0 == imgWidthHeight) {
                imgWidthHeight = DisplayUtility.dp2px(binding.civAvatar.getContext(), 60);
            }
            ImageLoadUtils.loadBannerRoundBgWithOutPlaceHolder(binding.civAvatar.getContext(),
                    ImageLoadUtils.toThumbnailUrl(imgWidthHeight, imgWidthHeight, item.getAvatar()),
                    binding.civAvatar,
                    imgWidthHeight,
                    R.drawable.bg_default_cover_round_placehold_size60);
        }
    }
}
