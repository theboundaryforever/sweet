package com.yuhuankj.tmxq.ui.liveroom.imroom.redpackage;

import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.listener.HttpCallBack;
import com.tongdaxing.xchat_core.bean.RoomRedPacket;
import com.tongdaxing.xchat_core.bean.RoomRedPacketReceiveResult;
import com.tongdaxing.xchat_core.room_red_packet.RedPacketModel;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.base.presenter.BaseMvpPresenter;

import java.util.List;

/**
 * 文件描述：房间红包View的P层
 *
 * @auther：zwk
 * @data：2019/8/23
 */
public class RoomRedPackagePresenter extends BaseMvpPresenter<IRoomRedPackageView> {
    private RedPacketModel redPacketModel;

    public RoomRedPackagePresenter() {
        this.redPacketModel = new RedPacketModel();
    }

    /**
     * 获取房间红包信息
     *
     * @param roomId
     */
    public void getRoomRedPacketInfo(long roomId) {
        redPacketModel.getRoomRedPackageInfo(roomId, new CallBack<List<RoomRedPacket>>() {
            @Override
            public void onSuccess(List<RoomRedPacket> data) {
                if (getMvpView() != null) {
                    getMvpView().initStateAndShowRedPackageView(data);
                }
            }

            @Override
            public void onFail(int code, String error) {

            }
        });
    }

    /**
     * 领取房间红包
     *
     * @param roomId
     * @param redPackageId
     */
    public void receiveRedPacket(long roomId, String redPackageId) {
        redPacketModel.receiveRoomRedPackage(roomId, redPackageId, new HttpCallBack<RoomRedPacketReceiveResult>() {
            @Override
            public void onFail(int code, String error, RoomRedPacketReceiveResult roomRedPacketReceiveResult) {
                if (getMvpView() != null) {
                    if (code == ServiceResult.RECEIVE_NO_RED_PACKAGE) {
                        getMvpView().showNoRedPackageDialog(roomRedPacketReceiveResult);
                    } else {
                        getMvpView().showFailToast(error);
                    }
                }
            }

            @Override
            public void onSuccess(RoomRedPacketReceiveResult data) {
                if (getMvpView() != null) {
                    getMvpView().showReceiveSucAnimAndDialog(data);
                }
            }

            @Override
            public void onFail(int code, String error) {
                if (getMvpView() != null) {
                    getMvpView().showFailToast(error);
                }
            }
        });
    }

}
