package com.yuhuankj.tmxq.ui.search.ui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.xchat_core.room.bean.SearchRoomPersonInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

/**
 * Created by chenran on 2017/10/3.
 */

public class SearchRecommendRoomAdapter extends BaseQuickAdapter<SearchRoomPersonInfo,SearchRecommendRoomAdapter.ViewHolder> {

    private Context context;

    public SearchRecommendRoomAdapter(Context context) {
        super(R.layout.list_item_search_recommend);
        this.context = context;
    }


    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    protected void convert(ViewHolder helper, SearchRoomPersonInfo item) {
        if(GlideContextCheckUtil.checkContextUsable(context)){
            ImageLoadUtils.loadRoundImage(context, item.getAvatar(), helper.iv_ownerAvator, R.dimen.common_cover_round_size);
            helper.tv_onlineNum.setText(context.getResources().getString(R.string.search_recommend_online_num,item.getOnlineNum()));
            helper.tv_roomName.setText(item.getTitle());
            helper.rl_roomInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(null != listener){
                        listener.onRoomInfoItemClick(item.getUid(), item.getType());
                    }
                }
            });
            View.OnClickListener onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(null != listener){
                        listener.onClickToAttention(item);
                    }
                }
            };
            Drawable drawable = null;
            if(!item.isFollow()){
                drawable = context.getResources().getDrawable(R.mipmap.icon_search_recom_room_like);
                drawable.setBounds(0,0,DisplayUtility.dp2px(context,12),DisplayUtility.dp2px(context,10));
            }
            helper.tv_attentionStatus.setCompoundDrawablePadding(item.isFollow() ? 0 : DisplayUtility.dp2px(context,3));
            helper.tv_attentionStatus.setCompoundDrawables(drawable,null,null,null);
            helper.tv_attentionStatus.setText(context.getResources().getString(item.isFollow() ? R.string.user_info_already_attention : R.string.user_info_attention));
            helper.tv_attentionStatus.setOnClickListener(onClickListener);
            helper.fl_attentionStatus.setBackground(context.getResources().getDrawable(item.isFollow() ? R.drawable.bg_search_recom_atten : R.drawable.bg_search_recom_unatten));
            helper.fl_attentionStatus.setOnClickListener(onClickListener);
            ViewGroup.LayoutParams lp = helper.rl_roomInfo.getLayoutParams();
            lp.height = lp.width = (ScreenUtil.getScreenWidth(mContext)-ScreenUtil.dip2px(50f))/3;

        }
    }

    public void setListener(OnSearchRecommendRoomItemClickListener listener) {
        this.listener = listener;
    }

    public OnSearchRecommendRoomItemClickListener listener;

    static class ViewHolder extends BaseViewHolder {

        public ImageView iv_ownerAvator;
        public TextView tv_onlineNum;
        public TextView tv_roomName;
        public TextView tv_attentionStatus;
        public FrameLayout fl_attentionStatus;
        public View rl_roomInfo;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_ownerAvator = itemView.findViewById(R.id.iv_ownerAvator);
            tv_onlineNum = itemView.findViewById(R.id.tv_onlineNum);
            tv_roomName = itemView.findViewById(R.id.tv_roomName);
            tv_attentionStatus = itemView.findViewById(R.id.tv_attentionStatus);
            fl_attentionStatus = itemView.findViewById(R.id.fl_attentionStatus);
            rl_roomInfo = itemView.findViewById(R.id.rl_roomInfo);
        }
    }

    public interface OnSearchRecommendRoomItemClickListener{
        void onClickToAttention(SearchRoomPersonInfo searchRoomPersonInfo);

        void onRoomInfoItemClick(long userid, int type);
    }
}
