package com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.erban.libcommon.widget.MicroWaveView;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomQueueInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.listener.OnMicroItemClickListener;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

/**
 * 文件描述：新版本的多人语聊房的普通麦位的adapter
 *
 * @auther：zwk
 * @data：2019/6/6
 */
public class AuctionRoomMicroAdapter extends RecyclerView.Adapter<AuctionRoomMicroAdapter.AuctionHostMicroHolder> {
    private Context mContext;
    private OnMicroItemClickListener onMicroItemClickListener;
    private static final int TYPE_AUCTION_HOST = 0;
    private static final int TYPE_AUCTION = 1;

    public AuctionRoomMicroAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public AuctionHostMicroHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        AuctionHostMicroHolder holder;
        if (getItemViewType(i) == TYPE_AUCTION_HOST) {
            holder = new AuctionHostMicroHolder(LayoutInflater.from(mContext).inflate(R.layout.item_rv_auction_host_micro, viewGroup, false));
        } else {
            holder = new AuctionMicroHolder(LayoutInflater.from(mContext).inflate(R.layout.item_rv_auction_micro, viewGroup, false));
        }
        return holder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(@NonNull AuctionHostMicroHolder holder, int position) {
        //普通麦位是从0开始的
        IMRoomQueueInfo roomQueueInfo = RoomDataManager.get().getRoomQueueMemberInfoByMicPosition(position);
        holder.waveview.stop();
        if (roomQueueInfo != null) {
            //麦位状态  禁麦和锁坑
            holder.ivMicroMute.setVisibility((roomQueueInfo.mRoomMicInfo != null && roomQueueInfo.mRoomMicInfo.isMicMute()) ? View.VISIBLE : View.GONE);
            holder.ivMicroState.setSelected((roomQueueInfo.mRoomMicInfo != null && roomQueueInfo.mRoomMicInfo.isMicLock()));
            if (roomQueueInfo.mChatRoomMember != null) {//
                if (StringUtils.isNotEmpty(roomQueueInfo.mChatRoomMember.getHalo())){
                    holder.waveview.setColor(Color.parseColor(roomQueueInfo.mChatRoomMember.getHalo()));
                }else {
                    holder.waveview.setColor(0x55ffffff);
                }
                holder.ivMicroState.setVisibility(View.GONE);
                holder.ivMicroAvatar.setVisibility(View.VISIBLE);
                holder.ivMicroHeadwear.setVisibility(View.VISIBLE);
                if (position == 0){
                    holder.tvAuctionInfo.setVisibility(View.GONE);
                    holder.ivGender.setVisibility(View.GONE);
                }else {
                    holder.ivGender.setVisibility(View.VISIBLE);
                    holder.ivGender.setImageResource(roomQueueInfo.mChatRoomMember.getGender() == 1?R.drawable.icon_man:R.drawable.icon_female);
                    holder.tvAuctionInfo.setVisibility(View.VISIBLE);
                    if (StringUtils.isNotEmpty(roomQueueInfo.mChatRoomMember.getProject())){
                        holder.tvAuctionInfo.setText(String.format("拍卖: %s | %d天", roomQueueInfo.mChatRoomMember.getProject(), roomQueueInfo.getmChatRoomMember().getDay()));
                    }else {
                        holder.tvAuctionInfo.setText("快来参与被拍卖的行列吧~");
                    }
                }
                holder.tvMicroTitle.setText(StringUtils.limitStr(mContext,roomQueueInfo.mChatRoomMember.getNick(),6));
                ImageLoadUtils.loadImage(mContext, roomQueueInfo.mChatRoomMember.getHeadwearUrl(), holder.ivMicroHeadwear);
                ImageLoadUtils.loadCircleImage(mContext, roomQueueInfo.mChatRoomMember.getAvatar(), holder.ivMicroAvatar, R.drawable.ic_default_avatar);
            } else {
                holder.ivMicroState.setVisibility(View.VISIBLE);
                holder.ivMicroAvatar.setVisibility(View.GONE);
                holder.ivMicroHeadwear.setVisibility(View.GONE);
                holder.ivGender.setVisibility(View.GONE);
                holder.tvMicroTitle.setText("暂无");
                if (position == 0){
                    holder.tvAuctionInfo.setVisibility(View.GONE);
                }else {
                    holder.tvAuctionInfo.setVisibility(View.VISIBLE);
                    holder.tvAuctionInfo.setText("快来参与被拍卖的行列吧~");
                }
            }
        } else {
            holder.ivMicroState.setVisibility(View.VISIBLE);
            holder.ivMicroState.setSelected(true);
            holder.ivMicroMute.setVisibility(View.GONE);
            holder.ivMicroAvatar.setVisibility(View.GONE);
            holder.ivMicroHeadwear.setVisibility(View.GONE);
            holder.ivGender.setVisibility(View.GONE);
            holder.tvMicroTitle.setText("暂无");
            holder.tvAuctionInfo.setVisibility(position == 0?View.GONE:View.VISIBLE);
            if (position == 0){
                holder.tvAuctionInfo.setVisibility(View.GONE);
            }else {
                holder.tvAuctionInfo.setVisibility(View.VISIBLE);
                holder.tvAuctionInfo.setText("快来参与被拍卖的行列吧~");
            }
        }
        holder.flMicro.setOnClickListener(v -> onMicroItemClick(roomQueueInfo, position));
    }


    /**
     * 处理不同状态的 点击事件
     *
     * @param roomQueueInfo
     * @param position
     */
    private void onMicroItemClick(IMRoomQueueInfo roomQueueInfo, int position) {
        if (null == onMicroItemClickListener) {
            return;
        }
        onMicroItemClickListener.onMicroBtnClickListener(roomQueueInfo, position);
    }

    @Override
    public int getItemCount() {
        return 2;
    }

    public void setOnMicroItemClickListener(OnMicroItemClickListener onMicroItemClickListener) {
        this.onMicroItemClickListener = onMicroItemClickListener;
    }

    class AuctionHostMicroHolder extends RecyclerView.ViewHolder {
        private FrameLayout flMicro;
        private ImageView ivMicroAvatar;
        //显示麦位的：空的麦位和锁坑
        private ImageView ivMicroState;
        private ImageView ivMicroHeadwear;
        private MicroWaveView waveview;
        private ImageView ivMicroMute;
        private TextView tvMicroTitle;
        private ImageView ivGender;
        private TextView tvAuctionInfo;


        public AuctionHostMicroHolder(@NonNull View itemView) {
            super(itemView);
            flMicro = itemView.findViewById(R.id.fl_room_micro_container);
            waveview = itemView.findViewById(R.id.waveview);
            ivMicroHeadwear = itemView.findViewById(R.id.iv_room_micro_headwear);
            ivMicroAvatar = itemView.findViewById(R.id.iv_room_micro_avatar);
            ivMicroState = itemView.findViewById(R.id.iv_room_micro_state);
            ivMicroMute = itemView.findViewById(R.id.iv_room_micro_mute);
            tvMicroTitle = itemView.findViewById(R.id.tv_room_micro_title);
            ivGender = itemView.findViewById(R.id.iv_micro_gender);
            tvAuctionInfo = itemView.findViewById(R.id.tv_current_auction_info);
        }
    }

    class AuctionMicroHolder extends AuctionHostMicroHolder {
        public AuctionMicroHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
