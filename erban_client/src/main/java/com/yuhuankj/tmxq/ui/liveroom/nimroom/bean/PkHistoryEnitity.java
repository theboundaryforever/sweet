package com.yuhuankj.tmxq.ui.liveroom.nimroom.bean;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.adapter.PkHistoryAdapter;

public class PkHistoryEnitity implements MultiItemEntity {

    public String portrait;
    public String nickName;
    public String count;

    public String getPortrait() {
        return portrait;
    }

    public void setPortrait(String portrait) {
        this.portrait = portrait;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    @Override
    public int getItemType() {
        return PkHistoryAdapter.TYPE_LEVEL_ITEM;
    }
}
