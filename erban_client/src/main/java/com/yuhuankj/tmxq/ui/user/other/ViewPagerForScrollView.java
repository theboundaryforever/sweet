package com.yuhuankj.tmxq.ui.user.other;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class ViewPagerForScrollView extends ViewPager {
    public ViewPagerForScrollView(Context context) {
        super(context);
    }

    public ViewPagerForScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
          //如果想要以当前Viewpager中最高的View为Viewpager的高度可以这样写
//        int index = getCurrentItem();
//        int height = 0;
//        View v = getChildAt(index);
//        if (v != null) {
//            v.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
//            height = v.getMeasuredHeight();
//        }
//        heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        getParent().requestDisallowInterceptTouchEvent(isGetEvent);
        return super.onInterceptTouchEvent(ev);
    }

    private boolean isGetEvent = false;

    //设置是否获得响应事件
    public void setGetEvent(boolean isGetEvent) {
        this.isGetEvent = isGetEvent;
    }
}