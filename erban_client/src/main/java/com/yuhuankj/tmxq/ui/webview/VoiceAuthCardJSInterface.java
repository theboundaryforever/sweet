package com.yuhuankj.tmxq.ui.webview;

import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.cache.NimUserInfoCache;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.yuhuankj.tmxq.R;

import java.io.File;
import java.util.HashMap;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;

/**
 * <p> html js 与webview 交互接口</p>
 * Created by ${user} on 2017/11/6.
 */
public class VoiceAuthCardJSInterface extends JSInterface {

    public VoiceAuthCardJSInterface(WebView webView, CommonWebViewActivity activity) {
        super(webView, activity);
        TAG = VoiceAuthCardJSInterface.class.getSimpleName();
    }

    /*----------------------------声鉴卡------------------------------*/

    /**
     * h5通知客户端，用户开始录制 | 重新录制录音
     */
    @JavascriptInterface
    public void startRecord() {
        LogUtils.d(TAG, "startRecord");
        if (null != mActivity && mActivity instanceof VoiceAuthCardWebViewActivity) {
            VoiceAuthCardWebViewActivity activity = (VoiceAuthCardWebViewActivity) mActivity;
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    activity.startRecord();
                }
            });
        }
    }

    /**
     * h5计时回调客户端通知停止录制，并提示用户
     */
    @JavascriptInterface
    public void recordStoped(String json) {
        LogUtils.d(TAG, "recordStoped-json:" + json);
        if (!TextUtils.isEmpty(json)) {
            try {
                Json json1 = new Json(json);
                final boolean cancel = json1.getBoolean("cancel");
                final String message = json1.getString("message");
                LogUtils.d(TAG, "recordStoped-message:" + message + " cancel:" + cancel);
                if (null != mActivity && mActivity instanceof VoiceAuthCardWebViewActivity) {
                    VoiceAuthCardWebViewActivity activity = (VoiceAuthCardWebViewActivity) mActivity;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            activity.recordStoped(cancel, message);
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * h5通知客户端用户试听录音 type=0 开始试听 type=1 停止试听
     */
    @JavascriptInterface
    public void tryToListener(String json) {
        LogUtils.d(TAG, "tryToListener-json:" + json);
        if (!TextUtils.isEmpty(json)) {
            try {
                Json json1 = new Json(json);
                final int type = json1.getInt("type");
                LogUtils.d(TAG, "tryToListener-type:" + type);
                if (null != mActivity && mActivity instanceof VoiceAuthCardWebViewActivity) {
                    VoiceAuthCardWebViewActivity activity = (VoiceAuthCardWebViewActivity) mActivity;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            activity.tryToListener(type);
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * h5通知客户端开始生成声鉴卡报告流程，客户端上传录音
     */
    @JavascriptInterface
    public void startVoiceAuth() {
        LogUtils.d(TAG, "startVoiceAuth");
        if (null != mActivity && mActivity instanceof VoiceAuthCardWebViewActivity) {
            VoiceAuthCardWebViewActivity activity = (VoiceAuthCardWebViewActivity) mActivity;
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    activity.startVoiceAuth();
                }
            });
        }
    }

    /**
     * 客户端通知h5录音更新成功，h5准备升级卡报告
     */
    @JavascriptInterface
    public void listenComplete() {
        LogUtils.d(TAG, "listenComplete");
        mWebView.evaluateJavascript("listenComplete()", s -> {
        });
    }

    /**
     * 退出页面，也即退出当前声鉴卡流程
     */
    @JavascriptInterface
    public void gotoBack() {
        LogUtils.d(TAG, "gotoBack");
        if (null != mActivity && mActivity instanceof VoiceAuthCardWebViewActivity) {
            VoiceAuthCardWebViewActivity activity = (VoiceAuthCardWebViewActivity) mActivity;
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    activity.gotoBack();
                }
            });
        }
    }

    /**
     * 客户端通知h5录制失败
     */
    @JavascriptInterface
    public void recordFailed() {
        LogUtils.d(TAG, "recordFailed");
        mWebView.evaluateJavascript("recordFailed()", s -> {
        });
    }

    /**
     * 客户端通知h5录音更新成功，h5准备升级卡报告
     */
    @JavascriptInterface
    public void uploadDone() {
        LogUtils.d(TAG, "uploadDone");
        mWebView.evaluateJavascript("uploadDone()", s -> {
        });
    }

    /**
     * h5通知客户端生成报告进度 判断进度，最大99停止
     */
    @JavascriptInterface
    public void authProgress(String json) {
        LogUtils.d(TAG, "authProgress-json:" + json);
        if (!TextUtils.isEmpty(json)) {
            try {
                Json json1 = new Json(json);
                final int progress = json1.getInt("progress");
                LogUtils.d(TAG, "authProgress-progress:" + progress);
                if (null != mActivity && mActivity instanceof VoiceAuthCardWebViewActivity) {
                    VoiceAuthCardWebViewActivity activity = (VoiceAuthCardWebViewActivity) mActivity;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            activity.authProgress(progress);
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * h5通知客户端，报告已生成 先改进度为100 然后显示生成报告结果H5页
     */
    @JavascriptInterface
    public void authDown(String json) {
        LogUtils.d(TAG, "authDown-json:" + json);
        if (!TextUtils.isEmpty(json)) {
            try {
                Json json1 = new Json(json);
                final int progress = json1.getInt("progress");
                LogUtils.d(TAG, "authProgress-progress:" + progress);
                if (null != mActivity && mActivity instanceof VoiceAuthCardWebViewActivity) {
                    VoiceAuthCardWebViewActivity activity = (VoiceAuthCardWebViewActivity) mActivity;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            activity.authDown(progress);
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 保存声鉴卡图片
     *
     * @param imgBase64
     */
    @JavascriptInterface
    public void saveAudioCardImg(String imgBase64) {
        LogUtils.d(TAG, "saveAudioCardImg-imgBase64:" + imgBase64);
        Bitmap bitmap = WebViewDownImgUtil.stringToBitmap(imgBase64);
        if (bitmap != null) {
            String fileName = System.currentTimeMillis() + ".jpg";
            String insertFilePath = MediaStore.Images.Media.insertImage(mActivity.getContentResolver(), bitmap, fileName, fileName);//插入图片到相册中
            File file1 = new File(WebViewDownImgUtil.getRealPathFromURI(Uri.parse(insertFilePath), mActivity));
            WebViewDownImgUtil.updatePhotoMedia(file1, mActivity);
            SingleToastUtil.showToast(mActivity.getResources().getString(R.string.save_img_succeed));
        }
    }

    /**
     * h5通知客户端，用户开始录制 | 重新录制录音
     */
    @JavascriptInterface
    public void share(String json) {
        LogUtils.d(TAG, "share json == " + json);
        if (mActivity instanceof VoiceAuthCardWebViewActivity) {
            VoiceAuthCardWebViewActivity activity = (VoiceAuthCardWebViewActivity) mActivity;
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Json jsonObj = new Json(json);
                        int type = jsonObj.num("type");
                        String imgData = jsonObj.str("imgData");
                        shareImg(type, imgData);
                    } catch (Exception e) {
                        e.printStackTrace();
                        SingleToastUtil.showToast("分享图片失败");
                    }
                }
            });
        }
    }

    /**
     * 跳转私聊页
     */
    @JavascriptInterface
    public void openChatPage(String json) {
        LogUtils.d(TAG, "openChatPage json == " + json);
        if (mActivity instanceof VoiceAuthCardWebViewActivity) {
            VoiceAuthCardWebViewActivity activity = (VoiceAuthCardWebViewActivity) mActivity;
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (null == activity) {
                            SingleToastUtil.showToast("跳转私聊页失败");
                            return;
                        }
                        Json jsonObj = new Json(json);
                        String account = jsonObj.str("uid");
                        //跳转私聊界面 -- 按照偶遇的逻辑来
                        NimUserInfo nimUserInfo = NimUserInfoCache.getInstance().getUserInfo(account);
                        if (nimUserInfo != null) {
                            LogUtils.d(TAG, "openChatPage nimUserInfo existed");
                            NimUIKit.startP2PSession(activity, account);
                        } else {
                            LogUtils.d(TAG, "openChatPage nimUserInfo no exist, request for update");
                            NimUserInfoCache.getInstance().getUserInfoFromRemote(account, new RequestCallbackWrapper<NimUserInfo>() {
                                @Override
                                public void onResult(int i, NimUserInfo nimUserInfo, Throwable throwable) {
                                    LogUtils.d(TAG, "openChatPage-onResult i:" + i);
                                    if (null != throwable) {
                                        throwable.printStackTrace();
                                    }
                                    if (null == activity) {
                                        SingleToastUtil.showToast("跳转私聊页失败");
                                        return;
                                    }
                                    if (i == 200) {
                                        NimUIKit.startP2PSession(activity, account);
                                    } else {
                                        activity.toast(activity.getResources().getString(R.string.network_error_retry));
                                    }
                                }
                            });
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        SingleToastUtil.showToast("跳转私聊页失败");
                    }
                }
            });
        }
    }

    //分离图片
    private void shareImg(int type, String imgData) {
        Bitmap bitmap = WebViewDownImgUtil.stringToBitmap(imgData);
        String saveFilePath = null;
        if (bitmap != null) {
            String fileName = System.currentTimeMillis() + ".jpg";
            String insertFilePath = MediaStore.Images.Media.insertImage(mActivity.getContentResolver(), bitmap, fileName, fileName);//插入图片到相册中
            File file1 = new File(WebViewDownImgUtil.getRealPathFromURI(Uri.parse(insertFilePath), mActivity));
            saveFilePath = file1.getAbsolutePath();
            WebViewDownImgUtil.updatePhotoMedia(file1, mActivity);
        }
        if (saveFilePath == null) {
            SingleToastUtil.showToast("分享图片失败");
            return;
        }
        String platformName = "";
        if (type == 1) {
            platformName = Wechat.NAME;
        } else if (type == 2) {
            platformName = WechatMoments.NAME;
        } else if (type == 3) {
            platformName = QQ.NAME;
        }
        Platform.ShareParams sp = new Platform.ShareParams();
        sp.setTitle(null);
        sp.setText(null);
        sp.setImagePath(saveFilePath);
        sp.setShareType(Platform.SHARE_IMAGE);//设置分享为图片类型
        Platform platform = ShareSDK.getPlatform(platformName);// 要分享的平台//QQ.NAME
        platform.setPlatformActionListener(new PlatformActionListener() {
            @Override
            public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {
                SingleToastUtil.showToast("分享图片成功");
            }

            @Override
            public void onError(Platform platform, int i, Throwable throwable) {
                SingleToastUtil.showToast("分享图片失败");
            }

            @Override
            public void onCancel(Platform platform, int i) {
                SingleToastUtil.showToast("分享图片取消");
            }
        }); // 设置分享事件回调
        // 执行图文分享
        platform.share(sp);
    }
}
