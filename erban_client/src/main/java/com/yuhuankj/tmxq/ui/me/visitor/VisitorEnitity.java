package com.yuhuankj.tmxq.ui.me.visitor;

import java.io.Serializable;

public class VisitorEnitity implements Serializable {
    private String avatar;//": "https://img.pinjin88.com/FvYQoqnsuwQrB8yr91yv4fRg8L2K?imageslim",
    private long createDate;//": 1557113745000,
    private int gender;//": 1,
    private int id;//": 68,
    private int isRead;//": 0,
    private String nick;//": "小黎的厨神",
    private long targetUid;//": 93000746,
    private long uid;//": 92000700


    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIsRead() {
        return isRead;
    }

    public void setIsRead(int isRead) {
        this.isRead = isRead;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public long getTargetUid() {
        return targetUid;
    }

    public void setTargetUid(long targetUid) {
        this.targetUid = targetUid;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }
}
