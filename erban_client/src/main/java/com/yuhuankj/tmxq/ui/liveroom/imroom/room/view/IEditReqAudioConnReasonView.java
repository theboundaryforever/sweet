package com.yuhuankj.tmxq.ui.liveroom.imroom.room.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

/**
 * <p> </p>
 *
 * @author jiahui
 * @date 2017/12/19
 */
public interface IEditReqAudioConnReasonView extends IMvpBaseView {

    void toast(String toast);

    void finishForResult();
}
