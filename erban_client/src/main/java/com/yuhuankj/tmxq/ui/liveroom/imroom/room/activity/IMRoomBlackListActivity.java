package com.yuhuankj.tmxq.ui.liveroom.imroom.room.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMember;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.BR;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter.RoomBlackListAdapter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter.IMRoomBlackPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IRoomBlackListView;

import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

/**
 * 黑名单
 *
 * @author chenran
 * @date 2017/10/11
 */
@CreatePresenter(IMRoomBlackPresenter.class)
public class IMRoomBlackListActivity extends BaseMvpActivity<IRoomBlackListView, IMRoomBlackPresenter>
        implements IRoomBlackListView, RoomBlackListAdapter.RoomBlackDelete {

    private final String TAG = IMRoomBlackListActivity.class.getSimpleName();

    private TextView count;
    private RecyclerView recyclerView;
    private RoomBlackListAdapter normalListAdapter;
    private int loadSize = 100;
    private int start = 0;

    public static void start(Context context) {
        Intent intent = new Intent(context, IMRoomBlackListActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_black_list);
        initTitleBar("黑名单");
        initView();
        showLoading();
        LogUtils.d(TAG, "onCreate-loadData");
        start = 0;
        loadData();
    }

    private void loadData() {
        LogUtils.d(TAG, "loadData-start:" + start + " loadSize:" + loadSize);
        //一次最多只能加载200条
        getMvpPresenter().queryRoomBlackList(loadSize, start);
    }

    private void initView() {
        count = (TextView) findViewById(R.id.count);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        normalListAdapter = new RoomBlackListAdapter(R.layout.list_item_liveroom_black, BR.imRoomMember);
        normalListAdapter.setRoomBlackDelete(this);
        //目前该接口后端的处理逻辑是一次性返回，没有分页处理操作
//        normalListAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
//            @Override
//            public void onLoadMoreRequested() {
//                LogUtils.d(TAG, "onLoadMoreRequested-loadData");
//                start = normalListAdapter.getData().size() + 1;
//                loadData();
//            }
//        }, recyclerView);
        recyclerView.setLayoutManager(new RecyclerViewNoBugLinearLayoutManager(this));
        recyclerView.setAdapter(normalListAdapter);
    }

    @Override
    public View.OnClickListener getLoadListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtils.d(TAG, "onClick-loadData");
                showLoading();
                start = 0;
                loadData();
            }
        };
    }

    private void dealRoomBlackListResponse(List<IMRoomMember> imRoomMembers) {
        hideStatus();
        if (imRoomMembers != null) {
            normalListAdapter.loadMoreComplete();
            if (imRoomMembers.size() < loadSize) {
                normalListAdapter.loadMoreEnd(true);
            }
        }
        if (imRoomMembers != null && imRoomMembers.size() > 0) {
            if (normalListAdapter.getData().size() > 0) {
                normalListAdapter.addData(imRoomMembers);
            } else {
                normalListAdapter.setNewData(imRoomMembers);
            }
            count.setText("黑名单".concat(String.valueOf(normalListAdapter.getData().size())).concat("人"));
        } else if (normalListAdapter.getData().size() < 1) {
            showNoData("暂没有设置黑名单");
            count.setText("黑名单0人");
        }
        LogUtils.d(TAG, "dealRoomBlackListResponse-start:" + start);
    }


    private void dealRemoveBlackUserResponse(String account) {
        if (TextUtils.isEmpty(account)) {
            return;
        }
        List<IMRoomMember> normalList = normalListAdapter.getData();
        if (!ListUtils.isListEmpty(normalList)) {
            hideStatus();
            ListIterator<IMRoomMember> iterator = normalList.listIterator();
            for (; iterator.hasNext(); ) {
                if (Objects.equals(iterator.next().getAccount(), account)) {
                    iterator.remove();
                }
            }
            normalListAdapter.notifyDataSetChanged();
            count.setText("黑名单".concat(String.valueOf(normalList.size())).concat("人"));
            if (normalList.size() == 0) {
                showNoData("暂没有设置黑名单");
            }
        } else {
            showNoData("暂没有设置黑名单");
            count.setText("黑名单0人");
        }
        toast("操作成功");
    }

    @Override
    public void showRoomBlackList(List<IMRoomMember> imRoomMembers) {
        getDialogManager().dismissDialog();
        dealRoomBlackListResponse(imRoomMembers);
    }

    @Override
    public void removeUserFromBlackList(String account) {
        dealRemoveBlackUserResponse(account);
    }

    @Override
    public void showErrMsg(String message) {
        if (!TextUtils.isEmpty(message)) {
            toast(message);
        }
    }

    @Override
    public void showListEmptyView() {

    }

    @Override
    public void onDeleteClick(IMRoomMember imRoomMember) {
        if (null == imRoomMember) {
            return;
        }
        getDialogManager().showOkCancelDialog(
                "是否将" + imRoomMember.getNick() + "移除黑名单列表？",
                true,
                new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onOk() {
                        RoomInfo roomInfo = RoomServiceScheduler.getCurrentRoomInfo();
                        if (roomInfo != null) {
                            getMvpPresenter().removeUserFromBlackList(roomInfo.getRoomId(), imRoomMember.getAccount(), false);
                        }
                    }
                });
    }
}
