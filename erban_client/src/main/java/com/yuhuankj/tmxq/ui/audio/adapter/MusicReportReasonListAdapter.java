package com.yuhuankj.tmxq.ui.audio.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yuhuankj.tmxq.R;

import java.util.List;

/**
 * Created by chenran on 2017/11/1.
 */

public class MusicReportReasonListAdapter extends RecyclerView.Adapter<MusicReportReasonListAdapter.ViewHolder>{

    private Context context;
    private List<String> reportReasonList;

    private int choosedPosition = -1;

    public MusicReportReasonListAdapter(Context context) {
        this.context = context;
    }

    public void setReportReasonList(List<String> reportReasonList) {
        this.reportReasonList = reportReasonList;
    }

    @Override
    public MusicReportReasonListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(context).inflate(R.layout.list_item_report_music,parent,false);
        return new MusicReportReasonListAdapter.ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(MusicReportReasonListAdapter.ViewHolder holder, int position) {
        final String reason = reportReasonList.get(position);
        holder.tv_reportReason.setText(reason);
        holder.iv_chooseStatus.setTag(reason);
        if(choosedPosition == position){
            holder.iv_chooseStatus.setImageDrawable(context.getResources().getDrawable(R.mipmap.icon_music_report_choosed));
        }else{
            holder.iv_chooseStatus.setImageDrawable(context.getResources().getDrawable(R.mipmap.icon_music_report_unchoose));
        }

        holder.iv_chooseStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choosedPosition = position;
                notifyDataSetChanged();
                if(null != onMusicReportReasonClickListener){
                    onMusicReportReasonClickListener.onReportReasonChoosed(reason);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (reportReasonList == null) {
            return 0;
        } else {
            return reportReasonList.size();
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        ImageView iv_chooseStatus;
        TextView tv_reportReason;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_chooseStatus = (ImageView) itemView.findViewById(R.id.iv_chooseStatus);
            tv_reportReason = (TextView) itemView.findViewById(R.id.tv_reportReason);
        }
    }

    public void setOnMusicReportReasonClickListener(OnMusicReportReasonClickListener onMusicReportReasonClickListener) {
        this.onMusicReportReasonClickListener = onMusicReportReasonClickListener;
    }

    private OnMusicReportReasonClickListener onMusicReportReasonClickListener;

    public interface OnMusicReportReasonClickListener{
        void onReportReasonChoosed(String reason);
    }

}
