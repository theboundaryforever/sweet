package com.yuhuankj.tmxq.ui.user.bosonFriend;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;

import java.util.Map;

/**
 * 挚友model层
 */
public class BosonFriendModel extends BaseMvpModel {
    /**
     * 获取挚友列表
     */
    public void getBosonFriends(long queryUid,int pageNum ,int pageSize,OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("queryUid", String.valueOf(queryUid));
        params.put("pageNum", String.valueOf(pageNum));
        params.put("pageSize", String.valueOf(pageSize));
        OkHttpManager.getInstance().postRequest(UriProvider.getBosonFriends(), params, callBack);
    }

    /**
     * 获取挚友申请列表
     */
    public void getBosonFriendApplys(int pageNum ,int pageSize,OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("pageNum", String.valueOf(pageNum));
        params.put("pageSize", String.valueOf(pageSize));
        OkHttpManager.getInstance().postRequest(UriProvider.getBosonFriendApplys(), params, callBack);
    }

    /**
     * 获取挚友历史记录列表
     */
    public void getBosonFriendRecords(OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().postRequest(UriProvider.getBosonFriendRecords(), params, callBack);
    }

    /**
     * 同意挚友申请
     */
    public void agreeBosonFriendApply(long fUid,int applyId, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("fUid", String.valueOf(fUid));
        params.put("type", "1");
        params.put("applyId", String.valueOf(applyId));
        OkHttpManager.getInstance().postRequest(UriProvider.agreeBosonFriendApply(), params, callBack);
    }

    /**
     * 拒绝（删除）挚友申请
     */
    public void disagreeBosonFriendApply(long fUid,int applyId, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("fUid", String.valueOf(fUid));
        params.put("type", "2");
        params.put("applyId", String.valueOf(applyId));
        OkHttpManager.getInstance().postRequest(UriProvider.agreeBosonFriendApply(), params, callBack);
    }

    /**
     * 解除挚友关系
     */
    public void unbindBosonFriend(long fUid, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("fUid", String.valueOf(fUid));
        OkHttpManager.getInstance().postRequest(UriProvider.unbindBosonFriend(), params, callBack);
    }
}
