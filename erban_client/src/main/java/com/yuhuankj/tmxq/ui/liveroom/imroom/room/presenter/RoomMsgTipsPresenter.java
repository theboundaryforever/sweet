package com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomModel;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.base.presenter.BaseMvpPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IRoomMsgTipsView;

import java.util.List;

/**
 * 房间页面的presenter
 */
public class RoomMsgTipsPresenter<I extends IMvpBaseView> extends BaseMvpPresenter<IRoomMsgTipsView> {

    private final String TAG = RoomMsgTipsPresenter.class.getSimpleName();

    private IMRoomModel imRoomModel;

    public RoomMsgTipsPresenter() {
        imRoomModel = new IMRoomModel();
    }

    public void getRoomMsgTipsList() {
        LogUtils.d(TAG, "getRoomMsgTipsList");
        //非房主显示
        if (RoomDataManager.get().getCurrentRoomInfo() != null && !RoomDataManager.get().isRoomOwner()) {
            imRoomModel.getRoomTipsList(new HttpRequestCallBack<List<String>>() {

                @Override
                public void onSuccess(String message, List<String> tipsList) {
                    LogUtils.d(TAG, "getRoomMsgTipsList-->onSuccess message:" + message);
                    if (!ListUtils.isListEmpty(tipsList) && null != getMvpView()) {
                        getMvpView().refreshMsgTipsView(tipsList);
                    }
                }

                @Override
                public void onFailure(int code, String msg) {
                    LogUtils.d(TAG, "getRoomMsgTipsList-->onFailure msg:" + msg + " code:" + code);
                }
            });
        } else {
            LogUtils.d(TAG, "getRoomMsgTipsList,房主，请求取消");
        }

    }


    public void reportRoomMsgTipsSend() {
        LogUtils.d(TAG, "reportRoomMsgTipsSend");
        imRoomModel.sendRoomTipsMsg(new HttpRequestCallBack<Boolean>() {
            @Override
            public void onSuccess(String message, Boolean response) {
                LogUtils.d(TAG, "getRoomMsgTipsList-->onSuccess message:" + message + " response:" + response);
                if (null != getMvpView()) {
                    getMvpView().refreshMsgTipsShowStatus(response);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                LogUtils.d(TAG, "getRoomMsgTipsList-->onFailure msg:" + msg + " code:" + code);
            }
        });
    }

}
