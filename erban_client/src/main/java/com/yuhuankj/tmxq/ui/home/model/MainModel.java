package com.yuhuankj.tmxq.ui.home.model;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.ui.message.emoji.ServerEmojiListRespon;

import java.util.Map;

/**
 * @author weihaitao
 * @date 2019/5/22
 */
public class MainModel {

    /**
     * 获取推荐广告
     *
     * @param callBack
     */
    public void getRecommAdInfo(OkHttpManager.MyCallBack<ServiceResult<String>> callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        params.put("uid", uid + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket() + "");
        OkHttpManager.getInstance().postRequest(UriProvider.getRecommAdUrl(), params, callBack);
    }

    /**
     * 同步当前在线状态至服务器
     */
    public void syncOnLineStatus(OkHttpManager.MyCallBack<Json> callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if (0L == uid) {
            return;
        }
        params.put("uid", uid + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket() + "");
        RoomInfo cRoomInfo = BaseRoomServiceScheduler.getServerRoomInfo();
        if (null != cRoomInfo) {
            params.put("roomId", cRoomInfo.getRoomId() + "");
        }
        OkHttpManager.getInstance().postRequest(UriProvider.getOnLineStatusSyncUrl(), params, callBack);
    }

    public void getEmojiListFromServer(long targetUid, HttpRequestCallBack<ServerEmojiListRespon> callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if (0L == uid) {
            return;
        }
        params.put("uid", uid + "");
        params.put("targetUid", targetUid + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket() + "");
        OkHttpManager.getInstance().getRequest(UriProvider.getEmojiListUrl(), params, callBack);
    }

    public void reportEmojiSent(long targetUid, HttpRequestCallBack<Boolean> callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if (0L == uid) {
            return;
        }
        params.put("uid", uid + "");
        params.put("targetUid", targetUid + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket() + "");
        OkHttpManager.getInstance().postRequest(UriProvider.getReportEmojiUrl(), params, callBack);
    }

    public void getRandomTopic(long targetUid, HttpRequestCallBack<String> callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if (0L == uid) {
            return;
        }
        params.put("uid", uid + "");
        params.put("targetUid", targetUid + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket() + "");
        OkHttpManager.getInstance().postRequest(UriProvider.getRandomTopicUrl(), params, callBack);
    }
}
