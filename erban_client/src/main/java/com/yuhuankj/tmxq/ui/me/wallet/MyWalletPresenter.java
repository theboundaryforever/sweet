package com.yuhuankj.tmxq.ui.me.wallet;

import com.yuhuankj.tmxq.ui.me.charge.presenter.PayPresenter;

/**
 * Created by MadisonRong on 08/01/2018.
 */

public class MyWalletPresenter extends PayPresenter<IMyWalletView> {

    public void loadWalletInfo() {
        refreshWalletInfo(false);
    }

    public void handleClick(int id) {
        getMvpView().handleClickByViewId(id);
    }
}
