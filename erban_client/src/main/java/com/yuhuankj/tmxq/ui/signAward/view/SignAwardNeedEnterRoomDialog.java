package com.yuhuankj.tmxq.ui.signAward.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.signAward.model.SignRecvGiftPkgInfo;

import java.io.Serializable;
import java.util.List;

/**
 * 签到奖励领取页面 -- 强制用户跳转房间
 *
 * @author weihaitao
 * @date 2019/5/22
 */
public class SignAwardNeedEnterRoomDialog extends BaseSignAwardTakeDialog {

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            RoomServiceScheduler.getInstance().enterRoom(SignAwardNeedEnterRoomDialog.this, roomUid, roomType);
            finish();
        }
    };

    /**
     * @param context
     * @param roomUid  即将跳转的房主UID
     * @param roomType 即将跳转的房间类型
     * @param nick     麦上用户昵称
     */
    public static void start(Context context, long roomUid, int roomType, String nick,
                             List<SignRecvGiftPkgInfo> signRecvGiftPkgInfos) {
        Intent intent = new Intent(context, SignAwardNeedEnterRoomDialog.class);
        intent.putExtra("roomUid", roomUid);
        intent.putExtra("roomType", roomType);
        intent.putExtra("nick", nick);
        intent.putExtra("signRecvGiftPkgInfos", (Serializable) signRecvGiftPkgInfos);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        mBltvFindTa.postDelayed(runnable, 3000L);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != runnable && null != mBltvFindTa) {
            mBltvFindTa.removeCallbacks(runnable);
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}
