package com.yuhuankj.tmxq.ui.quickmating;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.GridView;

import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.xchat_core.room.face.FaceInfo;
import com.tongdaxing.xchat_core.room.face.IFaceCore;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.BaseAppDialog;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.face.DynamicFaceAdapter;
import com.yuhuankj.tmxq.ui.login.ViewAdapter;

import java.util.ArrayList;
import java.util.List;

import static com.yuhuankj.tmxq.base.dialog.AppDialogConstant.STYLE_BOTTOM_BACK_DARK;

public class EmojiDialog extends BaseAppDialog {

    private ViewPager vpContent;
    private IndicatorView invContent;

    public EmojiDialog(Context context) {
        super(context);
    }

    @Override
    public int onDialogStyle() {
        return STYLE_BOTTOM_BACK_DARK;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.layout_quick_mating_emoji;
    }

    @Override
    protected void initView() {
        vpContent = findViewById(R.id.vpContent);
        invContent = findViewById(R.id.invContent);
    }

    @Override
    public void initListener() {

    }

    @Override
    public void initViewState() {
        List<List<FaceInfo>> faceInfos = getFaceInfos();
        ArrayList<View> pagerView = new ArrayList<>();
        int size = faceInfos.size();
        for (int i = 0; i < size; i++) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_quick_mating_face, null, false);
            GridView gridView = view.findViewById(R.id.gridView);
            DynamicFaceAdapter adapter = new DynamicFaceAdapter(getContext(), faceInfos.get(i));
            adapter.setOnFaceItemClickListener(onFaceItemClickListener);
            gridView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            pagerView.add(view);
        }
        ViewAdapter pagerAdapter = new ViewAdapter(pagerView);
        vpContent.setAdapter(pagerAdapter);
        vpContent.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                invContent.setCurrent(position);
            }
        });

        invContent.setData(faceInfos.size());
        invContent.setCurrent(0);
        try {
            getWindow().getAttributes().width = ScreenUtil.getScreenWidth(getContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<List<FaceInfo>> getFaceInfos() {
        int pageSize = 15;//每一页显示数量
        List<FaceInfo> faceInfos = CoreManager.getCore(IFaceCore.class).getFaceInfos(1);
        List<List<FaceInfo>> faces = new ArrayList<>();
        if (faceInfos == null || faceInfos.size() < 2) {
            return faces;
        }
        int size = faceInfos.size() - 2;
        int pageCount = size / pageSize;
        for (int i = 0; i < pageCount; i++) {
            faces.add(faceInfos.subList(i * pageSize, pageSize));
        }
        if (pageCount * pageSize < size) {
            faces.add(faceInfos.subList(pageCount * pageSize, size));
        }
        return faces;
    }

    private DynamicFaceAdapter.OnFaceItemClickListener onFaceItemClickListener;

    public void setOnFaceItemClickListener(DynamicFaceAdapter.OnFaceItemClickListener onFaceItemClickListener) {
        this.onFaceItemClickListener = onFaceItemClickListener;
    }

}
