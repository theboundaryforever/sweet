package com.yuhuankj.tmxq.ui.me.charge;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.pingplusplus.android.Pingpp;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.count.IChargeClient;
import com.tongdaxing.xchat_core.pay.IPayCoreClient;
import com.tongdaxing.xchat_core.pay.bean.ChargeBean;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.me.charge.interfaces.IChargeView;
import com.yuhuankj.tmxq.ui.me.charge.presenter.ChargePresenter;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import org.json.JSONException;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.wechat.friends.Wechat;

/**
 * Created by MadisonRong on 05/01/2018.
 */
@CreatePresenter(ChargePresenter.class)
public class ChargeActivity extends BaseMvpActivity<IChargeView, ChargePresenter>
        implements IChargeView {
    private static final String TAG = "ChargeActivity";

    @BindView(R.id.civ_head)
    RoundedImageView mCiv_head;
    @BindView(R.id.tv_name)
    TextView mTv_name;
    @BindView(R.id.tv_gold)
    TextView mTv_gold;
    @BindView(R.id.tvNobleTip)
    TextView tvNobleTip;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindString(R.string.charge)
    String activityTitle;
    private ChargeAdapter mChargeAdapter;
    private ChargeBean mSelectChargeBean;

    private int wechatPayChannel = 1;//微信支付渠道 1：ping++， 2:汇聚
    private int aliPayChannel = 1;//支付宝支付渠道 1：ping++，2:汇潮
    private int payStyle = 1;//支付方式，1：支付宝 2：微信

    public static void start(Context context) {
        Intent intent = new Intent(context, ChargeActivity.class);
        context.startActivity(intent);
    }

    /**
     * 检测是否安装支付宝
     *
     * @param context
     * @return
     */
    public static boolean isAliPayInstalled(Context context) {
        Uri uri = Uri.parse("alipays://platformapi/startApp");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        ComponentName componentName = intent.resolveActivity(context.getPackageManager());
        return componentName != null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        getMvpPresenter().loadUserInfo();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_charge);
        ButterKnife.bind(this);
        initTitleBar(activityTitle);
        initViews();
        getMvpPresenter().getChargeList();
        getMvpPresenter().refreshWalletInfo(true);

        Json config = CoreManager.getCore(VersionsCore.class).getConfigData();
        try {
            wechatPayChannel = config.getInt("payChannel");
            aliPayChannel = config.getInt("aliPayChannel");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setupUserInfo(UserInfo userInfo) {
        mTv_name.setText(getString(R.string.charge_user_name, userInfo.getNick()));
        ImageLoadUtils.loadAvatar(this, userInfo.getAvatar(), mCiv_head, false);
    }

    private void initViews() {
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 3, OrientationHelper.VERTICAL, false));
        mChargeAdapter = new ChargeAdapter();
        mRecyclerView.setAdapter(mChargeAdapter);
        mChargeAdapter.setOnItemClickListener((baseQuickAdapter, view, position) -> {
            List<ChargeBean> list = mChargeAdapter.getData();
            if (ListUtils.isListEmpty(list)) {
                return;
            }
            mSelectChargeBean = list.get(position);
            int size = list.size();
            for (int i = 0; i < size; i++) {
                list.get(i).isSelected = position == i;
            }
            mChargeAdapter.notifyDataSetChanged();
            getMvpPresenter().showChargeOptionsDialog();
            StatisticManager.get().onEvent(this,
                    StatisticModel.getInstance().getRechargeValueClickEventId(mSelectChargeBean.money * 10),
                    StatisticModel.getInstance().getUMAnalyCommonMap(this));
        });
    }

    @Override
    public void getUserWalletInfoFail(String error) {
        toast(error);
    }

    @Override
    public void buildChargeList(List<ChargeBean> chargeBeanList) {
        if (chargeBeanList != null && chargeBeanList.size() > 0) {
            for (int i = 0; i < chargeBeanList.size(); i++) {
                ChargeBean chargeBean = chargeBeanList.get(i);
                chargeBean.isSelected = chargeBean.getMoney() == 48;
                if (48 == chargeBean.getMoney()) {
                    mSelectChargeBean = chargeBean;
                    StatisticManager.get().onEvent(this,
                            StatisticModel.getInstance().getRechargeValueClickEventId(mSelectChargeBean.money * 10),
                            StatisticModel.getInstance().getUMAnalyCommonMap(this));
                }
            }
            mChargeAdapter.setNewData(chargeBeanList);
        }
    }

    @Override
    public void getChargeListFail(String error) {
        toast(error);
    }

    @Override
    public void refreshUserWalletBalance(WalletInfo walletInfo) {
        Log.i(TAG, "refreshUserWalletBalance: " + walletInfo.goldNum);
        mTv_gold.setText(getString(R.string.charge_gold, walletInfo.goldNum));
        tvNobleTip.setText(getString(R.string.charge_gold_noble_tip, walletInfo.nobleGoldNum));
    }

    @Override
    public void displayChargeOptionsDialog() {
//        getMvpPresenter().requestCharge(this, String.valueOf(mSelectChargeBean.chargeProdId), Constants.CHARGE_ALIPAY);
        if (mSelectChargeBean == null) return;

        ButtonItem buttonItem = new ButtonItem(getString(R.string.charge_alipay), new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                payStyle = 1;
                if (aliPayChannel == 2) {
                    if (!isAliPayInstalled(ChargeActivity.this)) {
                        SingleToastUtil.showToast("未安装支付宝APP");
                        return;
                    }
                    //汇潮支付
                    getMvpPresenter().getHuiChaoPayData(String.valueOf(mSelectChargeBean.chargeProdId));
                } else {
                    //ping++支付
                    getMvpPresenter().requestCharge(ChargeActivity.this, String.valueOf(mSelectChargeBean.chargeProdId), Constants.CHARGE_ALIPAY);
                }
                StatisticManager.get().onEvent(ChargeActivity.this,
                        StatisticModel.EVENT_ID_RECHARGE_ALIPAY,
                        StatisticModel.getInstance().getUMAnalyCommonMap(ChargeActivity.this));
            }
        });
        ButtonItem buttonItem1 = new ButtonItem(getString(R.string.charge_webchat), new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                payStyle = 2;
                if (wechatPayChannel == 2) {
                    if (!ShareSDK.getPlatform(Wechat.NAME).isClientValid()) {
                        SingleToastUtil.showToast("未安装微信APP");
                        return;
                    }
                    //汇聚支付
                    getMvpPresenter().getJoinPayData(ChargeActivity.this, String.valueOf(mSelectChargeBean.chargeProdId), Constants.CHARGE_WX_JOINPAY);
                } else {
                    //Ping+支付
                    getMvpPresenter().requestCharge(ChargeActivity.this, String.valueOf(mSelectChargeBean.chargeProdId), Constants.CHARGE_WX);
                }
                StatisticManager.get().onEvent(ChargeActivity.this,
                        StatisticModel.EVENT_ID_RECHARGE_WX,
                        StatisticModel.getInstance().getUMAnalyCommonMap(ChargeActivity.this));
            }
        });

        List<ButtonItem> buttonItems = new ArrayList<>();
        buttonItems.add(buttonItem);
        buttonItems.add(buttonItem1);
        getDialogManager().showCommonPopupDialog(buttonItems, getString(R.string.cancel), false);
    }

    @Override
    public void getChargeOrOrderInfo(String data) {
        if (data != null) {
            if (aliPayChannel == 2 && payStyle == 1) {
                //汇潮支付
                try {
                    String payUrl = new Json(data).str("payUrl");
                    LogUtils.e("payUrl == " + payUrl);
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("alipays://platformapi/startApp?appId=10000011&url=" + URLEncoder.encode(payUrl, "UTF-8")));
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                    toast("发起充值失败，请联系客服人员");
                }
            } else if (wechatPayChannel == 2 && payStyle == 2) {
                //汇聚支付
                joinPay(data);
            } else {
                //Ping+支付
                Pingpp.createPayment(this, data);
            }
        }
    }

    @Override
    public void getChargeOrOrderInfoFail(String error) {
        toast("发起充值失败" + error);
    }

    //启动汇聚支付
    private void joinPay(String data) {
        try {
            Json json = new Json(data);
            String appId = json.getString("appId");
            String prepayId = json.getString("prepayId");
            String partnerId = json.getString("partnerId");
            String nonceStr = json.getString("nonceStr");
            String timeStamp = json.getString("timeStamp");
            String sign = json.getString("hmac");
            PayReq request = new PayReq();
            request.appId = appId;
            request.partnerId = partnerId;
            request.prepayId = prepayId;
            request.packageValue = "Sign=WXPay";
            request.nonceStr = nonceStr;
            request.timeStamp = timeStamp;
            request.sign = sign;

            IWXAPI api = WXAPIFactory.createWXAPI(this, appId, false);
            api.registerApp(appId);
            api.sendReq(request);
        } catch (Exception e) {
            e.printStackTrace();
            toast("拉起微信支付失败");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onActivityResult-requestCode:" + requestCode + " resultCode:" + resultCode);
        //支付页面返回处理
        if (requestCode == Pingpp.REQUEST_CODE_PAYMENT) {
            if (data != null && data.getExtras() != null) {
                String result = data.getExtras().getString("pay_result");
                Log.i(TAG, "onActivityResult-result:" + result);
                if (result != null) {

                    /* 处理返回值
                     * "success" - 支付成功
                     * "fail"    - 支付失败
                     * "cancel"  - 取消支付
                     * "invalid" - 支付插件未安装（一般是微信客户端未安装的情况）
                     * "unknown" - app进程异常被杀死(一般是低内存状态下,app进程被杀死)
                     */
                    // 错误信息
                    String errorMsg = data.getExtras().getString("error_msg");
                    String extraMsg = data.getExtras().getString("extra_msg");
                    CoreManager.notifyClients(IChargeClient.class, IChargeClient.chargeAction, result);
                    if ("success".equals(result)) {
                        getMvpPresenter().refreshWalletInfo(true);
                        toast(R.string.pay_success);
                        StatisticManager.get().onEvent(ChargeActivity.this,
                                StatisticModel.EVENT_ID_RECHARGE_SUCCESS,
                                StatisticModel.getInstance().getUMAnalyCommonMap(ChargeActivity.this));
                    } else if ("cancel".equals(result)) {
                        toast(R.string.pay_canceled);
                    } else {
                        toast(R.string.pay_failed);
                    }
                }
            }
        }
    }

    @CoreEvent(coreClientClass = IPayCoreClient.class)
    public void onWalletInfoUpdate(WalletInfo walletInfo) {
        if (walletInfo != null) {
            mTv_gold.setText(getString(R.string.charge_gold, walletInfo.getGoldNum()));
            tvNobleTip.setText(getString(R.string.charge_gold_noble_tip, walletInfo.nobleGoldNum));
        }
    }
}
