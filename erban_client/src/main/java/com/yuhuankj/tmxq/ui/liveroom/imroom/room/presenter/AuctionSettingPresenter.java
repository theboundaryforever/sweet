package com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter;

import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomModel;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.room.auction.ParticipateAuctionBean;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.base.presenter.BaseMvpPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.model.AuctionModel;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IAuctionSettingView;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/7/30
 */
public class AuctionSettingPresenter extends BaseMvpPresenter<IAuctionSettingView> {

    private AuctionModel auctionModel;

    public AuctionSettingPresenter() {
        this.auctionModel = new AuctionModel();
    }

    /**
     * 获取参与竞拍用户列表
     * @param roomId
     */
    public void joinRoomAuction(long roomId,boolean isAdmin,long adminUid, int day,String project){
        auctionModel.joinOrCancelRoomAuction(1,roomId,day,project, new CallBack<ParticipateAuctionBean>() {
            @Override
            public void onSuccess(ParticipateAuctionBean data) {
                if (getMvpView() != null) {
                    if (isAdmin && adminUid > 0) {
                        operateUpMicro(RoomDataManager.AUCTION_ROOM_POSITION, String.valueOf(adminUid), false);
                    }
                    getMvpView().showResultToast("参加成功");
                }
            }

            @Override
            public void onFail(int code, String error) {
                if (getMvpView() != null) {
                    getMvpView().showResultToast(error);
                }
            }
        });
    }

    /**
     * 邀请竞拍上麦
     *
     * @param micPosition   上的麦位
     * @param account       上麦的人
     * @param isInviteUpMic 上麦成功后是否自动开麦
     */
    public void operateUpMicro(int micPosition, String account, boolean isInviteUpMic) {
        new IMRoomModel().operateUpMicro(micPosition, account, isInviteUpMic, new CallBack<String>() {
            @Override
            public void onSuccess(String data) {

            }

            @Override
            public void onFail(int code, String error) {

            }
        });
    }
}
