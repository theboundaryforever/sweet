package com.yuhuankj.tmxq.ui.liveroom.imroom.populargift;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.xchat_core.room.bean.RoomAdditional;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.BaseAppDialog;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.widget.CircleImageView;

/**
 * 文件描述：人气礼物引爆的结果的弹框
 *
 * @auther：zwk
 * @data：2019/7/10
 */
public class PopularGiftResultDialog extends BaseAppDialog implements View.OnClickListener {
    private TextView tv_detonateNick;
    private TextView tv_detonateGift;
    private ImageView iv_detonateGift;
    private CircleImageView civ_detonateHead;
    private RoomAdditional roomAdditional;
    private long showTime = 0L;
    private OnPopularGiftResultClick onPopularGiftResultClick;

    public PopularGiftResultDialog(Context context, RoomAdditional roomAdditional) {
        super(context);
        this.roomAdditional = roomAdditional;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_popular_gift_result;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initView() {
        showTime = System.currentTimeMillis();
        tv_detonateNick = findViewById(R.id.tv_detonateNick);
        civ_detonateHead = findViewById(R.id.civ_detonateHead);
        tv_detonateGift = findViewById(R.id.tv_detonateGift);
        iv_detonateGift = findViewById(R.id.iv_detonateGift);
        findViewById(R.id.ll_blankToClose).setOnClickListener(this);

    }

    @Override
    public void initListener() {
        tv_detonateGift.setOnClickListener(this);
    }

    @Override
    public void initViewState() {
        if (null != roomAdditional) {
            if (StringUtils.isNotEmpty(roomAdditional.getDetonatingNick())) {
                tv_detonateNick.setText(roomAdditional.getDetonatingNick());
            }
            if (StringUtils.isNotEmpty(roomAdditional.getDetonatingAvatar())) {
                ImageLoadUtils.loadImage(getContext(), roomAdditional.getDetonatingAvatar(), civ_detonateHead);
            }
            if (StringUtils.isNotEmpty(roomAdditional.getDetonatingGiftName())) {
                tv_detonateGift.setText(roomAdditional.getDetonatingGiftName());
            }
            if (StringUtils.isNotEmpty(roomAdditional.getDetonatingGiftUrl())) {
                ImageLoadUtils.loadImage(getContext(), roomAdditional.getDetonatingGiftUrl(), iv_detonateGift);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_blankToClose:
                if (System.currentTimeMillis() - showTime >= 500L) {
                    dismiss();
                }
                break;
            case R.id.tv_detonateGift:
                if (onPopularGiftResultClick != null) {
                    onPopularGiftResultClick.showGiftDilaogPackage();
                }
                dismiss();
                break;
            default:
                break;
        }
    }

    public void setOnPopularGiftResultClick(OnPopularGiftResultClick onPopularGiftResultClick) {
        this.onPopularGiftResultClick = onPopularGiftResultClick;
    }
}
