package com.yuhuankj.tmxq.ui.me.wallet.bills;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jzxiang.pickerview.TimePickerDialog;
import com.jzxiang.pickerview.data.Type;
import com.jzxiang.pickerview.listener.OnDateSetListener;
import com.tongdaxing.erban.libcommon.utils.JavaUtil;
import com.tongdaxing.erban.libcommon.utils.TimeUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.bills.bean.BillItemEntity;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.ui.widget.itemdecotion.PowerGroupListener;
import com.yuhuankj.tmxq.ui.widget.itemdecotion.PowerfulStickyDecoration;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.UIUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p> 账单base activity </p>
 * Created by Administrator on 2017/11/6.
 */
public abstract class BillBaseActivity extends BaseActivity implements OnDateSetListener, View.OnClickListener {
    protected BillBaseActivity mActivity;
    protected RecyclerView mRecyclerView;
    protected SwipeRefreshLayout mRefreshLayout;
    private ImageView ivGoTop;
    private boolean isShow;
    public TextView tvTime;
    protected int mCurrentCounter = Constants.PAGE_START;//当前页
    protected static final int PAGE_SIZE = Constants.BILL_PAGE_SIZE;
    protected TimePickerDialog.Builder mDialogYearMonthDayBuild;
    protected long time = System.currentTimeMillis();
    protected List<BillItemEntity> mBillItemEntityList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        mActivity = this;
        initView();
        initData();
        setListener();
    }

    protected int getLayout() {
        return R.layout.activity_bills;
    }

    protected void initView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        ivGoTop = findView(R.id.iv_go_today);
        tvTime = (TextView) findViewById(R.id.tv_time);
    }

    protected void initData() {
        setDate(System.currentTimeMillis());
        mDialogYearMonthDayBuild = new TimePickerDialog.Builder()
                .setType(Type.YEAR_MONTH_DAY)
                .setTitleStringId("日期选择")
                .setThemeColor(getResources().getColor(R.color.line_background))
                .setWheelItemTextNormalColor(getResources().getColor(R.color
                        .timetimepicker_default_text_color))
                .setWheelItemTextSelectorColor(getResources().getColor(R.color.black))
                .setCallBack(mActivity);

        PowerfulStickyDecoration decoration = PowerfulStickyDecoration.Builder
                .init(new PowerGroupListener() {
                    @Override
                    public String getGroupName(int position) {
                        //获取组名，用于判断是否是同一组
                        if (mBillItemEntityList.size() > position && position>=0) {
                            return mBillItemEntityList.get(position).time;
                        }
                        return null;
                    }

                    @Override
                    public View getGroupView(int position) {
                        //获取自定定义的组View
                        if (mBillItemEntityList.size() > position && position>=0) {
                            View view = getLayoutInflater().inflate(R.layout.item_group, null, false);
                            ((TextView) view.findViewById(R.id.tv)).setText(TimeUtils.getDateTimeString(JavaUtil.str2long(mBillItemEntityList.get(position).time),"yyyy-MM-dd"));
                            return view;
                        } else {
                            return null;
                        }
                    }
                })
                .setGroupHeight(UIUtil.dip2px(this, 50))       //设置高度
                .isAlignLeft(true)                                 //靠右边显示   默认左边
                .setGroupBackground(getResources().getColor(R.color.colorPrimaryDark))    //设置背景   默认透明
                .build();
        mRecyclerView.addItemDecoration(decoration);
    }

    protected void setDate(long time) {
        tvTime.setText(new SimpleDateFormat("yyyy-MM-dd").format(new Date(time)));
    }

    protected abstract void loadData();

    protected void setListener() {
        findView(R.id.iv_date).setOnClickListener(this);
        findView(R.id.iv_go_today).setOnClickListener(this);
        ivGoTop.setOnClickListener(this);
        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mCurrentCounter = Constants.PAGE_START;
//                time = System.currentTimeMillis();
                loadData();
            }
        });

        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy>0 && !isShow){
                    isShow = true;
                    ObjectAnimator.ofFloat(ivGoTop,"translationY",0,300).setDuration(200).start();
                }else if(dy<0 && isShow){
                    isShow=false;
                    ObjectAnimator.ofFloat(ivGoTop,"translationY",300,0).setDuration(200).start();
                }
            }
        });
    }

    @Override
    public void showNetworkErr() {
        mRefreshLayout.setRefreshing(false);
        super.showNetworkErr();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_go_today:
                mCurrentCounter = Constants.PAGE_START;
                time = System.currentTimeMillis();
                setDate(time);
                showLoading();
                loadData();
                break;
            case R.id.iv_date:
                mDialogYearMonthDayBuild.build().show(getSupportFragmentManager(), "year_month_day");
                break;
        }
    }

    @Override
    public void onDateSet(TimePickerDialog timePickerView, long millseconds) {
        this.time = millseconds;
        setDate(millseconds);
        mCurrentCounter = Constants.PAGE_START;
        showLoading();
        loadData();
    }

    @Override
    public View.OnClickListener getLoadListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentCounter = Constants.PAGE_START;
                showLoading();
                loadData();
            }
        };
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mDialogYearMonthDayBuild != null) {
            mDialogYearMonthDayBuild.setCallBack(null);
            mDialogYearMonthDayBuild = null;
        }
    }
}
