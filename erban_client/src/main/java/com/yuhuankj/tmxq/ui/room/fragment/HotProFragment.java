package com.yuhuankj.tmxq.ui.room.fragment;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.ethanhua.skeleton.Skeleton;
import com.ethanhua.skeleton.SkeletonScreen;
import com.netease.nim.uikit.common.util.sys.NetworkUtil;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.tencent.bugly.crashreport.BuglyLog;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.fragment.BaseLazyFragment;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.databinding.HeadFragmentHotBinding;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.ranklist.RankListActivity;
import com.yuhuankj.tmxq.ui.room.adapter.BannerAdapter;
import com.yuhuankj.tmxq.ui.room.adapter.HotHeader;
import com.yuhuankj.tmxq.ui.room.adapter.HotProAdapter;
import com.yuhuankj.tmxq.ui.room.adapter.HotRecommendAdapter;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.widget.CircleImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.tongdaxing.xchat_core.Constants.PAGE_START;


/**
 * <p>首页热门界面  </p>
 *
 * @author Administrator
 * @date 2017/11/15
 */
public class HotProFragment extends BaseLazyFragment {
    private static final String TAG = HotProFragment.class.getSimpleName();
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;

    private int mPage = PAGE_START;

    private HotProAdapter hotAdapter;
    private HotProAdapter headAdapter;
    private HeadFragmentHotBinding headFragmentHotBinding;
    private Json homeInfoBean;
    private int mScreenWidth;
    private HotHeader hotHeaderItem;
    private Handler handler = new Handler(Looper.getMainLooper());

    private int greenRoomIndex;
    private View hotTitle;
    private HotRecommendAdapter hotRecommendAdapter;
    private View headViewRecommend;
    private RelativeLayout rlRankList;

    private RelativeLayout rlUser1;
    private RelativeLayout rlUser2;
    private RelativeLayout rlUser3;
    private CircleImageView imvUser1;
    private CircleImageView imvUser2;
    private CircleImageView imvUser3;
    private View vCrown1;
    private View vCrown2;
    private View vCrown3;
    private TextView tvHotRoomTitle;
    private SkeletonScreen skeletonScreen;//加载状态view
    private SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            if (NetworkUtil.isNetAvailable(mContext)) {
                onRefreshing();
            } else {
                swipeRefreshLayout.setRefreshing(false);
            }
        }
    };
    private boolean userInfoUpdateeEnabled = false;
    private boolean isHideLoad = false;//是否已经隐藏了加载布局

    public static HotProFragment newInstance() {
        return new HotProFragment();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        LogUtils.e(TAG, "onSaveInstanceState-- mPage: " + mPage);
        super.onSaveInstanceState(outState);
        if (homeInfoBean != null) {
            outState.putString(Constants.KEY_HOME_DATA, homeInfoBean + "");
        }
    }

    @Override
    protected void restoreState(@Nullable Bundle savedInstanceState) {
        super.restoreState(savedInstanceState);
        if (savedInstanceState != null) {
            LogUtils.e(TAG, "restoreState-- mPage: " + mPage);
            homeInfoBean = Json.parse(savedInstanceState.getString(Constants.KEY_HOME_DATA));
        }
    }

    @Override
    public void onFindViews() {
        swipeRefreshLayout = mView.findViewById(R.id.swipe_refresh);
        recyclerView = mView.findViewById(R.id.recycler_view);
    }

    @Override
    public void onSetListener() {
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
    }

    /**
     * 取消子recycler的焦点，防止乱划
     */
    private void setRvFocus(RecyclerView recyclerView) {
        recyclerView.setFocusableInTouchMode(false);
        recyclerView.requestFocus();
    }

    @Override
    public void initiate() {
        Json configData = CoreManager.getCore(VersionsCore.class).getConfigData();
        if (configData != null) {
            greenRoomIndex = configData.num("greenRoomIndex", 6);
        }
        mScreenWidth = ScreenUtil.getDisplayWidth() - ScreenUtil.dip2px(30);
        View headView = LayoutInflater.from(getActivity()).inflate(R.layout.head_fragment_hot, null);
        rlRankList = headView.findViewById(R.id.rlRankList);
        rlRankList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), RankListActivity.class));
            }
        });
        rlUser1 = headView.findViewById(R.id.rlUser1);
        rlUser2 = headView.findViewById(R.id.rlUser2);
        rlUser3 = headView.findViewById(R.id.rlUser3);
        imvUser1 = headView.findViewById(R.id.imvUser1);
        imvUser2 = headView.findViewById(R.id.imvUser2);
        imvUser3 = headView.findViewById(R.id.imvUser3);
        vCrown1 = headView.findViewById(R.id.vCrown1);
        vCrown2 = headView.findViewById(R.id.vCrown2);
        vCrown3 = headView.findViewById(R.id.vCrown3);
        tvHotRoomTitle = headView.findViewById(R.id.tvHotRoomTitle);
        hotTitle = headView.findViewById(R.id.tv_hot_title);

        headViewRecommend = LayoutInflater.from(getActivity()).inflate(R.layout.head_recommend, null);

        headFragmentHotBinding = DataBindingUtil.bind(headView);
        headFragmentHotBinding.recyclerView.setLayoutManager(new RecyclerViewNoBugLinearLayoutManager(getActivity()));

        hotRecommendAdapter = new HotRecommendAdapter(mContext, new ArrayList<>());
        hotRecommendAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Json json = hotRecommendAdapter.getData().get(position);
                long uid = json.num_l("uid");
                int roomType = RoomInfo.ROOMTYPE_HOME_PARTY;
                if (json.has("type")) {
                    roomType = json.num("type");
                }
                RoomServiceScheduler.getInstance().enterRoom(getActivity(), uid, roomType);
                //火热房间-绿色房间
                LogUtils.d(TAG, "onItemClick-火热房间-绿色房间 position:" + position);
                StatisticManager.get().onEvent(mContext,
                        StatisticModel.EVENT_ID_HOT_GREEN_ROOM,
                        StatisticModel.getInstance().getUMAnalyCommonMap(mContext));
            }
        });
        hotAdapter = new HotProAdapter(getContext(),hotRecommendAdapter, null);
        hotAdapter.setHotRoomType(HotProAdapter.HotRoomType.hot_room);
        hotAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                mPage++;
                BuglyLog.d(TAG, "onLoadMoreRequested-requestHotData mPage:" + mPage);
                requestHotData();
            }
        }, recyclerView);
        hotAdapter.addHeaderView(headView);

        hotAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Json json = hotAdapter.getData().get(position);
                long uid = json.num_l("uid");

                int roomType = RoomInfo.ROOMTYPE_HOME_PARTY;
                if (json.has("type")) {
                    roomType = json.num("type");
                }
                RoomServiceScheduler.getInstance().enterRoom(getActivity(), uid, roomType);
            }
        });
        headAdapter = new HotProAdapter(getContext(),null, null);
        headAdapter.setHotRoomType(HotProAdapter.HotRoomType.recommend_room);
//        topSixHotAdapter = new BaseAdapter<>(R.layout.list_item_home_hot_normal_new, BR.homeHotBean);
        headAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Json json = headAdapter.getData().get(position);
                long uid = json.num_l("uid");

                int roomType = RoomInfo.ROOMTYPE_HOME_PARTY;
                if (json.has("type")) {
                    roomType = json.num("type");
                }

                RoomServiceScheduler.getInstance().enterRoom(getActivity(), uid, roomType);
            }
        });
        hotHeaderItem = new HotHeader(mContext);
        hotHeaderItem.setVisibility(View.GONE);
        LinearLayoutManager manager = new RecyclerViewNoBugLinearLayoutManager(mContext);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(hotAdapter);
        headFragmentHotBinding.recyclerView.setAdapter(headAdapter);
        headAdapter.addHeaderView(hotHeaderItem);

        initBannerSetting();
        setRvFocus(headFragmentHotBinding.recyclerView);

        swipeRefreshLayout.setEnabled(false);
        skeletonScreen = Skeleton.bind(recyclerView)
                .adapter(hotAdapter)
                .load(R.layout.head_fragment_hot_loading)
                .show();

//        skeletonScreen = Skeleton.bind(swipeRefreshLayout)
//                .load(R.layout.head_fragment_hot_loading)
//                .show();
    }

    private void initBannerSetting() {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) headFragmentHotBinding.rollView.getLayoutParams();
        layoutParams.width = mScreenWidth;
        layoutParams.height = mScreenWidth / 3;
        layoutParams.gravity = Gravity.CENTER_HORIZONTAL;
        headFragmentHotBinding.rollView.setLayoutParams(layoutParams);
        headFragmentHotBinding.rollView.setPlayDelay(3000);
        //设置透明度
        headFragmentHotBinding.rollView.setAnimationDurtion(500);
    }

    @Override
    protected void onLazyLoadData() {
        BuglyLog.e(TAG, "onLazyLoadData-- mPage: " + mPage);
        swipeRefreshLayout.setRefreshing(false);
        if (homeInfoBean != null) {
            LogUtils.i(TAG, "懒加载数据....热门----" + homeInfoBean.toString());
            setHeadData();
            setHotListData(homeInfoBean, mPage);
        } else {
            LogUtils.i(TAG, "加载网络");
            swipeRefreshLayout.setRefreshing(true);
            onRefreshing();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && handler != null && !isHideLoad) {
            //20秒后隐藏，防止有意外情况导致没有隐藏加载布局，页面无法重新加载数据情况
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    hideSkeleton();
                }
            }, 20000);
        }
    }

    //设置排行榜数据
    private void setRankListData(Json data) {
        BuglyLog.d(TAG, "setRankListData");
        if (data == null) {
            return;
        }
        List<String> jsonList = data.sList("rankAvatar");
        if (jsonList == null || jsonList.size() == 0) {
            return;
        }
        rlUser1.setVisibility(View.INVISIBLE);
        rlUser2.setVisibility(View.INVISIBLE);
        rlUser3.setVisibility(View.INVISIBLE);
        vCrown1.setVisibility(View.INVISIBLE);
        vCrown2.setVisibility(View.INVISIBLE);
        vCrown3.setVisibility(View.INVISIBLE);
        if (jsonList.size() == 1) {
            rlUser3.setVisibility(View.VISIBLE);
            vCrown3.setVisibility(View.VISIBLE);
            imvUser3.setBorderColor(Color.parseColor("#FFEA6A"));
            ImageLoadUtils.loadImage(getActivity(), jsonList.get(0), imvUser3, R.drawable.default_user_head);
        } else if (jsonList.size() == 2) {
            rlUser2.setVisibility(View.VISIBLE);
            vCrown2.setVisibility(View.VISIBLE);
            imvUser2.setBorderColor(Color.parseColor("#FFEA6A"));
            ImageLoadUtils.loadImage(getActivity(), jsonList.get(0), imvUser2, R.drawable.default_user_head);

            rlUser3.setVisibility(View.VISIBLE);
            imvUser3.setBorderColor(Color.parseColor("#FFFFFF"));
            ImageLoadUtils.loadImage(getActivity(), jsonList.get(1), imvUser3, R.drawable.default_user_head);
        } else {
            rlUser1.setVisibility(View.VISIBLE);
            vCrown1.setVisibility(View.VISIBLE);
            imvUser1.setBorderColor(Color.parseColor("#FFEA6A"));
            ImageLoadUtils.loadImage(getActivity(), jsonList.get(0), imvUser1, R.drawable.default_user_head);

            rlUser2.setVisibility(View.VISIBLE);
            imvUser2.setBorderColor(Color.parseColor("#FFFFFF"));
            ImageLoadUtils.loadImage(getActivity(), jsonList.get(1), imvUser2, R.drawable.default_user_head);

            rlUser3.setVisibility(View.VISIBLE);
            imvUser3.setBorderColor(Color.parseColor("#FFFFFF"));
            ImageLoadUtils.loadImage(getActivity(), jsonList.get(2), imvUser3, R.drawable.default_user_head);
        }
    }

    private void setHotListData(Json homeInfoBean, int page) {
        BuglyLog.d(TAG, "setNewData-page:" + page);
        if (homeInfoBean == null) {
            return;
        }

        List<Json> listRoom = new ArrayList<>();
        List<Json> jsonList = homeInfoBean.jlist("listRoom");
        if (jsonList.size() < Constants.PAGE_HOME_HOT_SIZE) {
            hotAdapter.setEnableLoadMore(false);
        }
        listRoom.addAll(jsonList);

        if (page == PAGE_START) {
            int recommendIndex = (greenRoomIndex < listRoom.size()) && (greenRoomIndex > -1) ? greenRoomIndex : listRoom.size();
            int showLineIndex = recommendIndex - 1;
            if (showLineIndex > -1) {
                listRoom.get(showLineIndex).set("showLine", -2);
            }
            if (listRoom.size() == 0) {
                tvHotRoomTitle.setVisibility(View.INVISIBLE);
            } else {
                tvHotRoomTitle.setVisibility(View.VISIBLE);
            }
            listRoom.add(recommendIndex, Json.parse("viewType:" + HotProAdapter.Recommend));
            hotAdapter.setNewData(listRoom);
        } else {
            hotAdapter.addData(listRoom);
        }
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_hot;
    }

    private void onRefreshing() {
        mPage = PAGE_START;
        hotAdapter.setEnableLoadMore(true);
        BuglyLog.d(TAG, "onRefreshing-requestHotData mPage:" + mPage);
        requestHotData();
    }

    //隐藏Skeleton
    private void hideSkeleton() {
        try {
            if (!isHideLoad) {
                skeletonScreen.hide();
                swipeRefreshLayout.setEnabled(true);
            }
            isHideLoad = true;
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    private void requestHotData() {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        int page = this.mPage;
        param.put("pageNum", page + "");
        param.put("pageSize", Constants.PAGE_HOME_HOT_SIZE + "");
        param.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");

        OkHttpManager.getInstance().getRequest(UriProvider.getMainHotData(), param, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                hideSkeleton();
                if (PAGE_START == HotProFragment.this.mPage) {
                    if (hotAdapter != null) {
                        hotAdapter.loadMoreComplete();
                    }
                    swipeRefreshLayout.setRefreshing(false);
                    showNetworkErr();
                } else {
                    if (hotAdapter != null) {
                        hotAdapter.loadMoreFail();
                    }
                }
            }

            @Override
            public void onResponse(Json json) {
                hideSkeleton();
                if (hotAdapter != null) {
                    hotAdapter.loadMoreComplete();
                }
                if (json.num("code") == 200) {
                    Json data = json.json_ok("data");
                    homeInfoBean = data;
                    setRankListData(data);
                    setHotListData(data, page);
                    //分页加载不刷新这部分数据
                    if (page == PAGE_START) {
                        setHeadData();
                        List<Json> listGreenRoom = data.jlist("listGreenRoom");
                        if (listGreenRoom.size() > 0) {
                            hotRecommendAdapter.setNewData(listGreenRoom);
                            headViewRecommend.setVisibility(View.VISIBLE);
                            hotAdapter.showRecommendBg = true;
                        } else {
                            hotAdapter.showRecommendBg = false;
                            headViewRecommend.setVisibility(View.GONE);
                        }
                    }
                    swipeRefreshLayout.setRefreshing(false);
                    hideStatus();
                    hotAdapter.disableLoadMoreIfNotFullPage(recyclerView);
                } else {
                    if (PAGE_START == HotProFragment.this.mPage) {
                        swipeRefreshLayout.setRefreshing(false);
                        showNetworkErr();
                    }
                }
            }
        });
    }

    private void setHeadData() {
        BuglyLog.d(TAG, "setHeadData");
        List<Json> banners = homeInfoBean.jlist("banners");
        List<Json> hotRooms = homeInfoBean.jlist("hotRooms");
        //List<Json> homeIcons = homeInfoBean.jlist("homeIcons");

        if (banners.size() > 0) {
            setBannerData(banners);
        }

        headFragmentHotBinding.llIconList.setVisibility(View.GONE);
        headFragmentHotBinding.iconListLine.setVisibility(View.GONE);

        List<Json> hotRoomTop2 = new ArrayList<>();
        if (hotRooms.size() > 0) {
            hotRoomTop2.addAll(hotRooms);
        }
        Json firstData = null;
        if (hotRoomTop2.size() > 0) {
            firstData = hotRoomTop2.get(0);
            hotRoomTop2.remove(0);
        }
        Json secondData = null;
        if (hotRoomTop2.size() > 0) {
            secondData = hotRoomTop2.get(0);
            hotRoomTop2.remove(0);
        }
        if (firstData == null) {
            //隐藏热门推荐标题
            hotTitle.setVisibility(View.GONE);
            hotHeaderItem.setVisibility(View.GONE);
        } else {
            hotTitle.setVisibility(View.VISIBLE);
            hotHeaderItem.setVisibility(View.VISIBLE);
        }
        hotHeaderItem.setData(firstData, secondData);
        headAdapter.setNewData(hotRoomTop2);
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo userInfo) {
        LogUtils.e(TAG, "onCurrentUserInfoUpdate-- mPage: " + mPage);
        if (!userInfoUpdateeEnabled) {
            userInfoUpdateeEnabled = true;
            return;
        }
        onRefreshing();

    }

    @Override
    public void onReloadData() {
        LogUtils.e(TAG, "onCurrentUserInfoUpdate-- mPage: " + mPage);
        super.onReloadData();
        showLoading();
        onRefreshing();
    }

    /**
     * 设置广告位数据
     *
     * @param homeItem
     */
    private void setBannerData(List<Json> homeItem) {
        BuglyLog.d(TAG, "setBannerData");
        if (!ListUtils.isListEmpty(homeItem)) {
            headFragmentHotBinding.rollView.setVisibility(View.VISIBLE);
            BannerAdapter bannerAdapter = new BannerAdapter(homeItem, getActivity());
            headFragmentHotBinding.rollView.setAdapter(bannerAdapter);
            //不知什么原因，每次切换账号重新设置banner的时候，总是会显示空白
            //目前只能通过smoothScrollBy滚动一下才能显示出来,可能是Glide加载图片的问题
            headFragmentHotBinding.rollView.setPageSelectedLisenter(new ViewPager.SimpleOnPageChangeListener() {
                @Override
                public void onPageSelected(int position) {
                    recyclerView.smoothScrollBy(0, 1);
                    recyclerView.smoothScrollBy(0, -1);
                }
            });
            try {
                headFragmentHotBinding.rollView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        recyclerView.smoothScrollBy(0, 1);
                        recyclerView.smoothScrollBy(0, -1);
                    }
                }, 500);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            headFragmentHotBinding.rollView.setVisibility(View.GONE);
        }
    }
}