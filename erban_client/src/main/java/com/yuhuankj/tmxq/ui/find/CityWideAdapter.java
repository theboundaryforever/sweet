package com.yuhuankj.tmxq.ui.find;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.netease.nim.uikit.glide.GlideApp;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.find.mengxin.SproutUserInRoomInfo;
import com.yuhuankj.tmxq.ui.find.mengxin.SproutUserInfo;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

public class CityWideAdapter extends BaseQuickAdapter<SproutUserInfo, BaseViewHolder> {

    public CityWideAdapter(List<SproutUserInfo> data) {
        super(R.layout.item_nearby, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, SproutUserInfo item) {
        ImageView imvAvatar = helper.getView(R.id.imvAvatar);
        ImageView imvGender = helper.getView(R.id.imvGender);
        TextView tvUserNick = helper.getView(R.id.tvUserNick);
        TextView tvDesc = helper.getView(R.id.tvDesc);
        LinearLayout llChating = helper.getView(R.id.llChating);
        ImageView imvChating = helper.getView(R.id.imvChating);
        TextView tvJustComing = helper.getView(R.id.tvJustComing);

        SproutUserInRoomInfo userInRoom = item.getUserInRoom();
        if (null != userInRoom && userInRoom.getUid() > 0L) {
            helper.addOnClickListener(R.id.llChating);
            llChating.setVisibility(View.VISIBLE);
            tvJustComing.setVisibility(View.GONE);
        } else {
            llChating.setVisibility(View.GONE);
            tvJustComing.setVisibility(View.GONE);
        }
        tvUserNick.setText(item.getNick());
        String nick=item.getUserDesc();
        if(!TextUtils.isEmpty(nick)&&nick.length()>15){
            nick=nick.substring(0,15)+"...";
        }
        tvDesc.setText(nick);
        imvGender.setImageResource(item.getGender() == 1 ? R.drawable.icon_gender_man : R.drawable.icon_gender_woman);
        ImageLoadUtils.loadCircleImage(mContext, item.getAvatar(), imvAvatar, R.drawable.ic_userinfo_default);
        GlideApp.with(mContext.getApplicationContext()).asGif()
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .load(R.drawable.anim_sprout_user_in_room)
                .into(imvChating);

    }
}