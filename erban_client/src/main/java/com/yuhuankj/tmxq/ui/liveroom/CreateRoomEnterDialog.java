package com.yuhuankj.tmxq.ui.liveroom;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.xchat_core.room.bean.CreateRoomBean;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.BaseAppDialog;

/**
 * 文件描述：创建房间入口dialog
 * 针对不同用户分配不同房间类型开关控制入口
 *
 * @auther：zwk
 * @data：2019/8/13
 */
public class CreateRoomEnterDialog extends BaseAppDialog {
    private RecyclerView rvRoomList;
    private CreateRoomEnterAdapter mAdapter;
    private long uid;

    public CreateRoomEnterDialog(Context context, long uid) {
        super(context);
        this.uid = uid;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_create_room_enter;
    }

    @Override
    protected void initView() {
        rvRoomList = findViewById(R.id.rv_room_type_list);
    }

    @Override
    public void initListener() {
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo != null && !ListUtils.isListEmpty(userInfo.getPowerRoom())) {
            GridLayoutManager gridLayoutManager = null;
            if (userInfo.getPowerRoom().size() < 3) {
                gridLayoutManager = new GridLayoutManager(getContext(), userInfo.getPowerRoom().size());
            } else {
                gridLayoutManager = new GridLayoutManager(getContext(), 3);
            }
            rvRoomList.setLayoutManager(gridLayoutManager);
            mAdapter = new CreateRoomEnterAdapter();
            rvRoomList.setAdapter(mAdapter);
            mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                    if (mAdapter != null && !ListUtils.isListEmpty(mAdapter.getData()) && mAdapter.getData().size() > position) {
                        CreateRoomBean createRoomBean = mAdapter.getData().get(position);
                        if (createRoomBean == null) {
                            return;
                        }
                        if (createRoomBean.getType() > RoomInfo.ROOMTYPE_NEW_AUCTION) {
                            SingleToastUtil.showToast("当前版本暂时不支持该类型房间，请先升级最新版~");
                            return;
                        }
                        if (onCreateRoomClickListener != null) {
                            if (createRoomBean.getType() != RoomInfo.ROOMTYPE_HOME_PARTY){
                                if (!userInfo.isRealNameAudit()) {
                                    onCreateRoomClickListener.onRealNameAudit(createRoomBean.getType(), uid);
                                } else {
                                    onCreateRoomClickListener.onClickCallback(createRoomBean.getType(), uid);
                                }
                            }else {//轰趴房交由内部处理 -- 实名
                                onCreateRoomClickListener.onClickCallback(createRoomBean.getType(), uid);
                            }
                        }
                    }
                    dismiss();
                }
            });
            mAdapter.setNewData(userInfo.getPowerRoom());
        } else {
            dismiss();
        }
    }

    @Override
    public void initViewState() {

    }

    private OnCreateRoomClickListener onCreateRoomClickListener;

    public void setOnCreateRoomClickListener(OnCreateRoomClickListener onCreateRoomClickListener) {
        this.onCreateRoomClickListener = onCreateRoomClickListener;
    }

    public interface OnCreateRoomClickListener {
        void onClickCallback(int type, long uid);

        void onRealNameAudit(int type, long uid);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        onCreateRoomClickListener = null;
    }
}
