package com.yuhuankj.tmxq.ui.liveroom.imroom.gift.listener;

import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftType;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/5/16
 */
public interface OnGiftListItemClickListener {
    void onItemClick(GiftInfo giftInfo, GiftType giftType,int position);
}
