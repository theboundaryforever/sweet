package com.yuhuankj.tmxq.ui.liveroom.nimroom.activity;


import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.growingio.android.sdk.collection.GrowingIO;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.coremanager.IUserInfoCore;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ButtonUtils;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.ResolutionUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.utils.json.JsonParser;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.RealNameAuthStatusChecker;
import com.tongdaxing.xchat_core.bean.NoblePublicMsg;
import com.tongdaxing.xchat_core.bean.NoblePublicMsgBd;
import com.tongdaxing.xchat_core.bean.RoomRedPacket;
import com.tongdaxing.xchat_core.im.custom.bean.NoblePublicMsgAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomRedPacketAttachment;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.room.publicchatroom.PublicChatRoomController;
import com.tongdaxing.xchat_core.room_red_packet.RedPacketModel;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.adapter.PublicRoomRedPacketAdapter;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.widget.PublicMessageView;
import com.yuhuankj.tmxq.ui.me.noble.NobleBusinessManager;
import com.yuhuankj.tmxq.ui.me.noble.NobleModel;
import com.yuhuankj.tmxq.ui.me.taskcenter.model.TaskCenterModel;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderMiniGameInvited;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.widget.LevelView;
import com.yuhuankj.tmxq.widget.TitleBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by polo on 2018/3/19.
 */

public class PublicChatRoomActivity extends BaseActivity {

    @BindView(R.id.title_bar)
    TitleBar mTitleBar;
    @BindView(R.id.message_view)
    PublicMessageView mMessageView;
    @BindView(R.id.et_msg)
    EditText mInputEdit;
    @BindView(R.id.btn_send)
    TextView mInputSend;
    @BindView(R.id.tv_count_down)
    TextView mTvCountDown;
    @BindView(R.id.imvStick)
    ImageView imvStick;
    @BindView(R.id.rlNobleStick)
    RelativeLayout rlNobleStick;

    private PublicChatRoomController mPublicChatRoomController;
    private CountDownTimer mCountDownTimer;

    //红包相关UI
    private RecyclerView rcvRedPacket;
    private ScrollSpeedLinearLayoutManger layoutManager;
    private List<RoomRedPacket> listRedPacket = new ArrayList<>();
    private List<RoomRedPacket> listRedPacketTime = new ArrayList<>();
    private PublicRoomRedPacketAdapter adapter;
    private boolean isCanUpdateTimer = true;//是否能更新计时器,滚动时不更新
    private static int MSG_RED_PACKET = 1;//红包消息
    private static int MSG_NOBLE_MSG_STICK = 2;//贵族广播置顶消息
    private Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == MSG_RED_PACKET) {
                if (!listRedPacketTime.isEmpty()) {
                    List<RoomRedPacket> rmRedPacket = new ArrayList<>();
                    for (RoomRedPacket timeRedPacket : listRedPacketTime) {
                        timeRedPacket.setUseabelTime(timeRedPacket.getUseabelTime() - 1);
                        if (timeRedPacket.getUseabelTime() <= 0) {
                            rmRedPacket.add(timeRedPacket);
                        }
                    }
                    if (rmRedPacket.size() > 0) {
                        listRedPacketTime.removeAll(rmRedPacket);
                    }
                    if (isCanUpdateTimer) {
                        adapter.notifyDataSetChanged();
                        scrollToPosition();
                    }

                    handler.removeMessages(MSG_RED_PACKET);
                    handler.sendEmptyMessageDelayed(MSG_RED_PACKET, 1000);
                }
            } else if (msg.what == MSG_NOBLE_MSG_STICK) {
                try {
                    rlNobleStick.setVisibility(View.GONE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private int statusHeight = 0;
    private int screenHeight = 0;
    private int keyboardHeight = 0;
    private boolean isHasKey = false;
    private boolean lastStatus = false;
    private final int KEY_ORI_BOTTOM = R.id.emoticon_picker_view;
    private boolean isCanSend = true;//是否能发送置顶广播

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_public_chat_room);
        ButterKnife.bind(this);
        initTitleBar(getString(R.string.public_chat_room));

        //红包相关UI初始化
        rcvRedPacket = (RecyclerView) findViewById(R.id.rcvRedPacket);
        layoutManager = new ScrollSpeedLinearLayoutManger(this);
        layoutManager.setSpeed(0.6f);
        layoutManager.setStackFromEnd(true);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rcvRedPacket.setLayoutManager(layoutManager);
        listRedPacket = new ArrayList<>(10);
        adapter = new PublicRoomRedPacketAdapter(listRedPacket);
        rcvRedPacket.setAdapter(adapter);
        //设置动画
        DefaultItemAnimator defaultItemAnimator = new DefaultItemAnimator();
        defaultItemAnimator.setAddDuration(3000);
        defaultItemAnimator.setRemoveDuration(3000);
        rcvRedPacket.setItemAnimator(defaultItemAnimator);

        rcvRedPacket.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                //静止没有滚动
                //public static final int SCROLL_STATE_IDLE = 0;
                //正在被外部拖拽,一般为用户正在用手指滚动
                //public static final int SCROLL_STATE_DRAGGING = 1;
                //自动滚动
                //public static final int SCROLL_STATE_SETTLING = 2;

                //只有滚动条静止的时候才刷新
                if (newState == 0) {
                    getPositionAndOffset();
                    isCanUpdateTimer = true;
                    if (listRedPacketTime.size() == 0) {
                        adapter.notifyDataSetChanged();
                        scrollToPosition();
                    }
                } else {
                    isCanUpdateTimer = false;
                }
            }
        });

        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                RoomRedPacket roomRedPacket = listRedPacket.get(position);
                if (roomRedPacket != null && roomRedPacket.getRoomUid() != 0) {
                    //单人音频房等新类型房间暂时没有添加派红包业务功能，因此这里保持默认的轰趴房间类型
                    RoomServiceScheduler.getInstance().enterOtherTypeRoomFromService(
                            PublicChatRoomActivity.this, roomRedPacket.getRoomUid(),
                            roomRedPacket.getRoomType());
                } else {
                    SingleToastUtil.showToast("进房失败");
                    listRedPacket.remove(position);
                    adapter.notifyDataSetChanged();
                }
            }
        });

        mMessageView.clear();
        mPublicChatRoomController = new PublicChatRoomController();
        mPublicChatRoomController.enterRoom(new PublicChatRoomController.ActionCallBack() {

            @Override
            public void success() {
            }

            @Override
            public void error() {
                toast(R.string.enter_public_chat_failed);
                mTitleBar.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, 2000);

            }
        });
        mPublicChatRoomController.setSendMsgCallBack(new PublicChatRoomController.SendMsgCallBack() {
            @Override
            public void success() {
                //完成大厅发言任务
                new TaskCenterModel().finishTask(2, new OkHttpManager.MyCallBack<ServiceResult<Object>>() {
                    @Override
                    public void onError(Exception e) {
                    }

                    @Override
                    public void onResponse(ServiceResult<Object> response) {
                    }
                });
            }

            @Override
            public void error() {
            }
        });
        mPublicChatRoomController.inintCacheTime();
        checkCountDown();
        initBar();
        initEvent();
        CoreManager.getCore(VersionsCore.class).requestSensitiveWord();

        //监听键盘状态
        statusHeight = ResolutionUtils.getStatusBarHeight2(this);
        screenHeight = ResolutionUtils.getScreenHeight(this);

        final LinearLayout llInput = (LinearLayout) findViewById(R.id.llInput);
        getWindow().getDecorView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                getWindow().getDecorView().getWindowVisibleDisplayFrame(r);
                int visibleHeight = r.bottom - r.top + statusHeight;

                keyboardHeight = screenHeight - (r.bottom - r.top);
                boolean visible = Math.abs(keyboardHeight + statusHeight) > screenHeight / 3;

                if (lastStatus != visible) {
                    lastStatus = visible;
                    if (!visible) {
                        isHasKey = false;
                        LogUtils.e("键盘隐藏");
                        closeInputLayout(llInput);
                    } else {
                        isHasKey = true;
                        openInputLayout(llInput, visibleHeight);
                        LogUtils.e("键盘显示");
                    }
                }
            }
        });

        GrowingIO.getInstance().trackEditText(mInputEdit);
    }

    private void openInputLayout(final View view, int visibleHeight) {
        int[] vPoint = new int[2];
        view.getLocationOnScreen(vPoint);
        int viewY = vPoint[1] + view.getMeasuredHeight();
        final int bottomMargin = viewY - visibleHeight;

        if (view.getParent() instanceof FrameLayout) {
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) view.getLayoutParams();
            view.setTag(KEY_ORI_BOTTOM, lp.bottomMargin);
            lp.bottomMargin += bottomMargin;
            view.setLayoutParams(lp);
        } else if (view.getParent() instanceof RelativeLayout) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) view.getLayoutParams();
            view.setTag(KEY_ORI_BOTTOM, lp.bottomMargin);
            lp.bottomMargin += bottomMargin;
            view.setLayoutParams(lp);
        } else if (view.getParent() instanceof LinearLayout) {
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) view.getLayoutParams();
            view.setTag(KEY_ORI_BOTTOM, lp.bottomMargin);
            lp.bottomMargin += bottomMargin;
            view.setLayoutParams(lp);
        }
        view.requestLayout();

        view.post(new Runnable() {
            @Override
            public void run() {
                if (mMessageView != null) {
                    mMessageView.scrollToBottom();
                }
            }
        });
    }

    private void closeInputLayout(final View view) {

        if (view.getParent() instanceof FrameLayout) {
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) view.getLayoutParams();
            if (view.getTag(KEY_ORI_BOTTOM) != null) {
                lp.bottomMargin = (int) view.getTag(KEY_ORI_BOTTOM);
            } else {
                lp.bottomMargin = 0;
            }
        } else if (view.getParent() instanceof RelativeLayout) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) view.getLayoutParams();
            if (view.getTag(KEY_ORI_BOTTOM) != null) {
                lp.bottomMargin = (int) view.getTag(KEY_ORI_BOTTOM);
            } else {
                lp.bottomMargin = 0;
            }
        } else if (view.getParent() instanceof LinearLayout) {
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) view.getLayoutParams();
            if (view.getTag(KEY_ORI_BOTTOM) != null) {
                lp.bottomMargin = (int) view.getTag(KEY_ORI_BOTTOM);
            } else {
                lp.bottomMargin = 0;
            }
        }
        view.requestLayout();
    }

    //上一次滚动的位置
    private int lastOffset = 0;
    private int lastPosition = 0;

    /**
     * 记录RecyclerView当前位置
     */
    private void getPositionAndOffset() {
        if (layoutManager == null) {
            return;
        }
        //获取可视的第一个view
        View topView = layoutManager.getChildAt(0);
        if (topView != null) {
            //获取与该view的顶部的偏移量
            lastOffset = topView.getTop();
            //得到该View的数组位置
            lastPosition = layoutManager.getPosition(topView);
        }
    }

    /**
     * 让RecyclerView滚动到指定位置
     */
    private void scrollToPosition() {
        if (layoutManager != null && lastPosition >= 0) {
            layoutManager.scrollToPositionWithOffset(lastPosition, lastOffset - 29);
        }
    }

    private void initBar() {
        ImageView imageView = new ImageView(this);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick(v.getId())) {
                    return;
                }
                CommonWebViewActivity.start(PublicChatRoomActivity.this, UriProvider.JAVA_WEB_URL + "/ttyy/friends/index.html");
            }
        });
        imageView.setPadding(5, 5, 5, 5);
        imageView.setImageResource(R.drawable.faxian_shuomnig);
        mTitleBar.mRightLayout.addView(imageView);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
        mPublicChatRoomController.leaveRoom();

        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
            handler = null;
        }
    }


    private void initEvent() {
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        //是贵族国王显示置顶按纽
        if (userInfo != null && NobleBusinessManager.hasAllAppBroadcast(userInfo.getVipId())) {
            imvStick.setVisibility(View.VISIBLE);
            imvStick.setOnClickListener(this);
        } else {
            imvStick.setVisibility(View.GONE);
        }

        rlNobleStick.setBackgroundColor(Color.parseColor("#FFFBF7"));
        rlNobleStick.setVisibility(View.GONE);
        rlNobleStick.setOnClickListener(this);

        mInputSend.setOnClickListener(this);
        mTvCountDown.setOnClickListener(this);

        Disposable disposable = IMNetEaseManager.get().getChatRoomEventObservable()
                .compose(bindToLifecycle())
                .subscribe(new Consumer<RoomEvent>() {
                    @Override
                    public void accept(RoomEvent roomEvent) throws Exception {
                        if (roomEvent == null) {
                            return;
                        }
                        LogUtils.d("dealRoomEvent", "roomEven:" + roomEvent.getEvent());
                        int event = roomEvent.getEvent();
                        switch (event) {
                            case RoomEvent.ROOM_RECEIVE_RED_PACKET:
                                RoomRedPacketAttachment redPacketAttachment = roomEvent.getRedPacketAttachment();
                                if (redPacketAttachment != null) {
                                    RoomRedPacket roomRedPacket = redPacketAttachment.getDataInfo();

                                    if (roomRedPacket == null) {
                                        return;
                                    }
                                    //如果说不在广播中显示,直接返回
                                    if (roomRedPacket.getShowBroadcast() == 0) {
                                        return;
                                    }
                                    listRedPacket.add(roomRedPacket);
                                    adapter.notifyDataSetChanged();

                                    rcvRedPacket.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            rcvRedPacket.smoothScrollToPosition(listRedPacket.size() - 1);
                                        }
                                    }, 100);

                                    if (roomRedPacket.getType() == 2) {
                                        long startTime = roomRedPacket.getRedPacketTime();
                                        long curTime = MsgViewHolderMiniGameInvited.getCurTimeMillis();
                                        long residue = curTime - startTime;
                                        int timing = roomRedPacket.getTiming() * 1000;
                                        if (residue < timing) {
                                            residue = timing - residue;
                                            //取整
                                            residue = residue + (1000 - residue % 1000);
                                            //转换成秒
                                            residue = residue / 1000;
                                            if (residue > 1200) {
                                                residue = 1200;
                                            }
                                            roomRedPacket.setUseabelTime(residue);
                                            listRedPacketTime.add(roomRedPacket);
                                        }
                                        if (listRedPacketTime != null && listRedPacketTime.size() > 0) {
                                            handler.removeMessages(MSG_RED_PACKET);
                                            handler.sendEmptyMessage(MSG_RED_PACKET);
                                        }
                                    }
                                }
                                break;
                            case RoomEvent.ROOM_RECEIVE_RED_PACKET_FINISH:
                                break;
                            case RoomEvent.ROOM_RECEIVE_NOBLE_PUBLIC_MSG:
                                NoblePublicMsgAttachment nobleAttachment = roomEvent.getNoblePublicMsgAttachment();
                                if (nobleAttachment == null) {
                                    break;
                                }
                                NoblePublicMsg noblePublicMsg = nobleAttachment.getDataInfo();
                                if (noblePublicMsg == null) {
                                    break;
                                }
                                setMsgNobleMsgStick(noblePublicMsg);
                                break;
                            default:
                                break;
                        }
                    }
                });
        mCompositeDisposable.add(disposable);

        getNoblePublicMsg();
        getRoomHistoryRedPacket();
    }

    //设置置顶广播
    private void setMsgNobleMsgStick(NoblePublicMsg noblePublicMsg) {
        if (noblePublicMsg == null) {
            return;
        }
        //全站广播消息
        try {
            //3分钟
            long allTime = 3 * 60 * 1000;
            long offset = 0;
//            if (noblePublicMsg.getCurrent() > 0) {
//                offset = MsgViewHolderMiniGameInvited.getCurTimeMillis() - noblePublicMsg.getCurrent();
//                //超时了
//                if (offset > allTime) {
//                    return;
//                }
//                if (offset < 0) {
//                    offset = 0;
//                }
//            }

            ImageView imvNobleStick = rlNobleStick.findViewById(R.id.imvNobleStick);
            ImageView userIcon = rlNobleStick.findViewById(R.id.iv_user_icon_chat_room);
            TextView userNick = rlNobleStick.findViewById(R.id.tv_user_nick_chat_room);
            TextView content = rlNobleStick.findViewById(R.id.tv_content_chat_room);
            LevelView levelView = rlNobleStick.findViewById(R.id.level_chat_room);
            ImageView ivUserNobleMedal = rlNobleStick.findViewById(R.id.ivUserNobleMedal);
            levelView.setLevelImageViewHeight(DisplayUtility.dp2px(PublicChatRoomActivity.this, 15));

            ivUserNobleMedal.setVisibility(View.VISIBLE);
            imvNobleStick.setVisibility(View.VISIBLE);

            ImageLoadUtils.loadImage(PublicChatRoomActivity.this, noblePublicMsg.getTopUrl(), imvNobleStick);
            ImageLoadUtils.loadImage(PublicChatRoomActivity.this, noblePublicMsg.getAvatar(), userIcon);
            String nick = "";
            if (!TextUtils.isEmpty(noblePublicMsg.getNick())) {
                nick = noblePublicMsg.getNick();
                if (noblePublicMsg.getNick().length() > 6) {
                    nick = noblePublicMsg.getNick().substring(0, 6) + "...";
                }
            }
            userNick.setText(nick);
            content.setText(noblePublicMsg.getMessage());
            levelView.setCharmLevel(noblePublicMsg.getCharmLevel());
            levelView.setExperLevel(noblePublicMsg.getExperLevel());
            ImageLoadUtils.loadImage(PublicChatRoomActivity.this, noblePublicMsg.getVipMedal(), ivUserNobleMedal);

            rlNobleStick.setVisibility(View.VISIBLE);
            rlNobleStick.setTag(noblePublicMsg.getUid());

            //3分钟后移除置顶广播
            handler.removeMessages(MSG_NOBLE_MSG_STICK);
            handler.sendEmptyMessageDelayed(MSG_NOBLE_MSG_STICK, allTime - offset);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //获取房间历史红包
    private void getRoomHistoryRedPacket() {
        listRedPacket.clear();
        listRedPacketTime.clear();
        new RedPacketModel().getRoomHistoryRedPacket(PublicChatRoomController.getPublicChatRoomId(), new OkHttpManager.MyCallBack<ServiceResult<List<RoomRedPacket>>>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(ServiceResult<List<RoomRedPacket>> response) {
                if (response.isSuccess()) {
                    List<RoomRedPacket> redPackets = response.getData();
                    if (redPackets != null && redPackets.size() > 0) {
                        for (RoomRedPacket roomRedPacket : redPackets) {
                            if (roomRedPacket.getType() == 2) {
                                long startTime = roomRedPacket.getRedPacketTime();
                                long curTime = MsgViewHolderMiniGameInvited.getCurTimeMillis();
                                long residue = curTime - startTime;
                                int timing = roomRedPacket.getTiming() * 1000;
                                if (residue < timing) {
                                    residue = timing - residue;
                                    //转换成秒
                                    residue = residue / 1000;
                                    if (residue > 1200) {
                                        residue = 1200;
                                    }
                                    roomRedPacket.setUseabelTime(residue);
                                    listRedPacketTime.add(roomRedPacket);
                                }
                            }
                        }
                        listRedPacket.addAll(redPackets);
                        adapter.notifyDataSetChanged();

                        rcvRedPacket.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                rcvRedPacket.smoothScrollToPosition(listRedPacket.size() - 1);
                            }
                        }, 100);
                        if (listRedPacketTime != null && listRedPacketTime.size() > 0) {
                            handler.removeMessages(MSG_RED_PACKET);
                            handler.sendEmptyMessage(MSG_RED_PACKET);
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.btn_send:
                String trim = mInputEdit.getText().toString().trim();
                if (TextUtils.isEmpty(trim)) {
                    SingleToastUtil.showToast("消息内容不能为空");
                    return;
                }
                if (!RealNameAuthStatusChecker.getInstance().authStatus(this, getResources().getString(R.string.real_name_auth_tips,
                        getResources().getString(R.string.real_name_auth_tips_send_msg)))) {
                    return;
                }

                if (!checkSendPower() || checkBanned()) {
                    return;
                }
                String sensitiveWordData = CoreManager.getCore(VersionsCore.class).getSensitiveWordData();
                if (!TextUtils.isEmpty(sensitiveWordData) && !TextUtils.isEmpty(trim) && trim.replaceAll("\n", "").replace(" ", "").matches(sensitiveWordData)) {
                    SingleToastUtil.showToast(this.getResources().getString(R.string.sensitive_word_data));
                    return;
                }

                if (trim.length() > 40) {
                    trim = trim.substring(0, 40);
                }
                if (imvStick.isShown() && imvStick.isSelected()) {
                    //发送贵族广播
                    sendNoblePublicMsg(trim);
                }
                Disposable disposable = mPublicChatRoomController.sendMsg(trim);
                if (null == disposable || null == mCompositeDisposable) {
                    return;
                }
                mCompositeDisposable.add(disposable);
                mPublicChatRoomController.refreshTime();
                checkCountDown();
                mInputEdit.setText("");
                break;
            case R.id.tv_count_down:
                toast(R.string.wait_please);
                break;
            case R.id.imvStick:
                if (!imvStick.isSelected() && !isCanSend) {
                    toast("每天只可置顶一次，你今天已经置顶过了哦~");
                    return;
                }
                //贵族置顶按纽
                imvStick.setSelected(!imvStick.isSelected());
                break;
            case R.id.rlNobleStick:
                try {
                    long uid = (long) rlNobleStick.getTag();
                    UserInfoActivity.start(this, uid);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:

        }
    }

    //获取贵族置顶广播
    private void getNoblePublicMsg() {
        new NobleModel().getNoblePublicMsg(new OkHttpManager.MyCallBack<ServiceResult<NoblePublicMsgBd>>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(ServiceResult<NoblePublicMsgBd> response) {
                if (response != null && response.isSuccess()) {
                    try {
                        NoblePublicMsgBd noblePublicMsgBd = response.getData();
                        if (noblePublicMsgBd == null) {
                            return;
                        }
                        isCanSend = noblePublicMsgBd.isCanSend();
                        NoblePublicMsg noblePublicMsg = JsonParser.parseJsonToNormalObject(noblePublicMsgBd.getBroadcast(), NoblePublicMsg.class);
                        setMsgNobleMsgStick(noblePublicMsg);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    //发送贵族置顶广播
    private void sendNoblePublicMsg(String message) {
        DialogManager dialogManager = new DialogManager(this);
        dialogManager.showProgressDialog(this, "请稍候...");
        new NobleModel().sendNoblePublicMsg(message, new OkHttpManager.MyCallBack<ServiceResult<Json>>() {
            @Override
            public void onError(Exception e) {
                dialogManager.dismissDialog();
                e.printStackTrace();
                SingleToastUtil.showToast("发送广播失败");
                if (mCountDownTimer != null) {
                    mCountDownTimer.cancel();
                }
                mInputSend.setVisibility(View.VISIBLE);
                mTvCountDown.setVisibility(View.GONE);
            }

            @Override
            public void onResponse(ServiceResult<Json> response) {
                dialogManager.dismissDialog();
                if (response == null) {
                    SingleToastUtil.showToast("发送广播失败");
                    return;
                }
                if (!response.isSuccess()) {
                    SingleToastUtil.showToast(response.getMessage());
                    if (mCountDownTimer != null) {
                        mCountDownTimer.cancel();
                    }
                    mInputSend.setVisibility(View.VISIBLE);
                    mTvCountDown.setVisibility(View.GONE);
                } else {
                    SingleToastUtil.showToast("发送成功");
                    imvStick.setSelected(false);
                    isCanSend = false;
                    getNoblePublicMsg();
                }
            }
        });
    }

    private boolean checkBanned() {
        Json json = CoreManager.getCore(IUserInfoCore.class).getBannedMap();
        boolean all = json.boo(IUserInfoCore.BANNED_ALL + "");
        boolean room = json.boo(IUserInfoCore.BANNED_PUBLIC_ROOM + "");
        if (all || room) {
            toast(getResources().getString(R.string.banned));
            return true;
        }
        return false;
    }

    public boolean checkSendPower() {
        if (!AvRoomDataManager.get().isOwnerOnMic()) {
            toast(R.string.send_broadcast_msg_mic_tips);
            return false;

        }

        if (System.currentTimeMillis() - mPublicChatRoomController.cacheTime <= 60000) {
            toast(R.string.send_broadcast_msg_time_interval_tips);
            return false;
        }
        return true;
    }

    public void checkCountDown() {
        long time = System.currentTimeMillis();
        long l = time - mPublicChatRoomController.cacheTime;
        l = PublicChatRoomController.maxWaitTime - l;

        if (l <= PublicChatRoomController.maxWaitTime && l >= 0) {
            mInputSend.setVisibility(View.GONE);
            mTvCountDown.setVisibility(View.VISIBLE);
            mCountDownTimer = new CountDownTimer(l, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    int countDownTime = (int) (millisUntilFinished / 1000);
                    mTvCountDown.setText(getString(R.string.send_msg_count, countDownTime));
                }

                @Override
                public void onFinish() {
                    mInputSend.setVisibility(View.VISIBLE);
                    mTvCountDown.setVisibility(View.GONE);
                }
            };
            mCountDownTimer.start();
        } else {
            mInputSend.setVisibility(View.VISIBLE);
            mTvCountDown.setVisibility(View.GONE);
        }

    }

    //可设置滚动速度的LinearLayoutManager
    private class ScrollSpeedLinearLayoutManger extends LinearLayoutManager {

        private float MILLISECONDS_PER_INCH = 0.03f;

        public ScrollSpeedLinearLayoutManger(Context context) {
            super(context);
        }

        @Override
        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
            LinearSmoothScroller linearSmoothScroller = new LinearSmoothScroller(recyclerView.getContext()) {
                @Override
                protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                    //返回滑动一个pixel需要多少毫秒
                    return MILLISECONDS_PER_INCH / displayMetrics.density;
                }
            };
            linearSmoothScroller.setTargetPosition(position);
            startSmoothScroll(linearSmoothScroller);
        }

        public void setSpeed(float speed) {
            MILLISECONDS_PER_INCH = speed;
        }

        public void resetSpeed() {
            MILLISECONDS_PER_INCH = 0.03f;
        }
    }
}
