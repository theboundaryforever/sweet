package com.yuhuankj.tmxq.ui.liveroom.imroom;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.chatroom.ChatRoomService;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.erban.libcommon.utils.ThreadUtil;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.gift.ComboGiftPublicScreenMsgSender;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomEvent;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.base.BaseRoomDetailFragment;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.dialog.InputPwdDialog;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.fragment.AuctionRoomFragment;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.fragment.MultiAudioRoomFragment;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.fragment.MultiRoomPemitFragment;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.fragment.SingleAudioRoomFragment;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter.RoomFramePresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IRoomView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.ReturnRoomListView;
import com.yuhuankj.tmxq.ui.ranklist.TabPageAdapter;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.ArrayList;
import java.util.List;

import static com.yuhuankj.tmxq.ui.liveroom.nimroom.AVRoomActivity.ACTION_EXIT_ROOM;

/**
 * 房间框架activity（进房）
 * 进房流程：房间背景在activity中（避免进房出现黑屏-- 这里和红豆不同没有使用透明色背景（房间加载看起来在点击进入之前操作实际还是在activity中））
 * 1.现根据房间类型：给定fragment
 * 2.在根据socket调用进入成功后在更新fragment的UI
 * 3.最后声网初始化成功后在更新底部UI（和声网声音相关）
 * 甜蜜星球后续新的房间类型统一按照这个框架来做
 *
 * @author zeda
 */
@CreatePresenter(RoomFramePresenter.class)
public class RoomFrameActivity extends BaseMvpActivity<IRoomView, RoomFramePresenter> implements IRoomView {
    private final String TAG = RoomFrameActivity.class.getSimpleName();
    protected long roomUid;
    protected int roomType;
    private InputPwdDialog mPwdDialogFragment;
    private BaseRoomDetailFragment roomFragment;
    private MultiRoomPemitFragment multiRoomPemitFragment;
    private ImageView ivBack;

    private ViewPager vpContent;
    private TabPageAdapter tabPageAdapter;
    private List<Fragment> fragments;
    private ViewPager.OnPageChangeListener onPageChangeListener;
    private View llRoomIndic;
    private ImageView ivMainRoom;
    private ImageView ivRoomLevel;

    private ReturnRoomListView rrlvReturnRoomList;

    private Runnable delayRequestReturnRoomListRunnable = null;

    /**
     * 上次点击返回键的时间
     * 逻辑：
     * 1.如果上次点击的时间间隔大于2秒，
     * 1.1toast提示用户
     * 1.2判断rrlvReturnRoomList是否可见，不可见则直接调用rrlvReturnRoomList请求接口的方法，rrlvReturnRoomList内部需要对接口做唯一请求处理
     * 2.如果上次点击的时间间隔小于2秒，则直接走退出房间的逻辑
     */
    private long lastClickBackKeyTime = 0L;

    public static void start(Context context, long roomUid, int roomType) {
        Intent intent = new Intent(context, RoomFrameActivity.class);
        intent.putExtra(Constants.ROOM_UID, roomUid);
        intent.putExtra(Constants.ROOM_TYPE, roomType);
        context.startActivity(intent);
    }

    public static void startFromService(Context context, long roomUid, int roomType) {
        Intent intent = new Intent(context, RoomFrameActivity.class);
        intent.putExtra(Constants.ROOM_UID, roomUid);
        intent.putExtra(Constants.ROOM_TYPE, roomType);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    private void initFragments() {
        ivMainRoom = (ImageView) findViewById(R.id.ivMainRoom);
        ivRoomLevel = (ImageView) findViewById(R.id.ivRoomLevel);
        llRoomIndic = findViewById(R.id.llRoomIndic);
        rrlvReturnRoomList = (ReturnRoomListView) findViewById(R.id.rrlvReturnRoomList);
        rrlvReturnRoomList.setVisibility(View.GONE);
        vpContent = (ViewPager) findViewById(R.id.vpContent);
        onPageChangeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                refreshBottomIndicSelecteStatus(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };
        vpContent.addOnPageChangeListener(onPageChangeListener);

        refreshBottomIndicStatus(false);
        refreshBottomIndicSelecteStatus(0);
    }

    private void refreshBottomIndicStatus(boolean showIndic) {
        llRoomIndic.setVisibility(showIndic ? View.VISIBLE : View.GONE);
        FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) vpContent.getLayoutParams();
        lp.bottomMargin = showIndic ? DisplayUtility.dp2px(this, 14) : 0;
        vpContent.setLayoutParams(lp);
    }

    private void refreshBottomIndicSelecteStatus(int pointIndex) {
        ivMainRoom.setImageResource(0 == pointIndex ? R.drawable.bg_room_point_selected : R.drawable.bg_room_point_normal);
        ivRoomLevel.setImageResource(1 == pointIndex ? R.drawable.bg_room_point_selected : R.drawable.bg_room_point_normal);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_EXIT_ROOM.equals(intent.getAction())) {
                finish();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_room);
        overridePendingTransition(R.anim.in_from_right, R.anim.out_from_left);
        initFragments();
        //保持房间页面不息屏
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setSwipeBackEnable(false);
        roomUid = getIntent().getLongExtra(Constants.ROOM_UID, 0);
        roomType = getIntent().getIntExtra(Constants.ROOM_TYPE, 0);
        //先检查是不是已经被此房间踢出去了
        boolean isKick = getMvpPresenter().checkIsKick(roomUid, roomType);
        if (isKick) {
            toast("你已被此房间踢出，请稍后再进");
            finish();
        }
        ivBack = (ImageView) findViewById(R.id.iv_room_bg);
        //注册房间事件监听
        IMRoomMessageManager.get().subscribeIMRoomEventObservable(this::onRoomEventReceive, this);
        initRoomBg(roomType);
        RoomInfo roomInfo = BaseRoomServiceScheduler.getCurrentRoomInfo();
        if (roomInfo != null) {//最小化进入
            if (roomInfo.getUid() != roomUid || roomInfo.getType() != roomType) {//最小化切换新的房间
                clearRoomHistory();//历史消息问题需要先清除 -- 优先显示界面的缺陷
                initRoomDetailFragment(roomType);
                getMvpPresenter().enterRoom(roomUid, roomType, null);
            } else {
                initRoomDetailFragment(roomType);
            }
        } else {//第一次打开房间
            initRoomDetailFragment(roomType);
            getMvpPresenter().enterRoom(roomUid, roomType, null);
        }

        //注册广播接收
        IntentFilter filter = new IntentFilter(ACTION_EXIT_ROOM);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, filter);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        long newRoomUid = intent.getLongExtra(Constants.ROOM_UID, 0);
        int newRoomType = intent.getIntExtra(Constants.ROOM_TYPE, 0);
        if (newRoomUid == roomUid && newRoomType == roomType) {//自己进入自己的房间
            if (null != roomFragment && !roomFragment.isDetached() && null != multiRoomPemitFragment && !multiRoomPemitFragment.isDetached()) {
                roomFragment.onNewIntent(intent);
                multiRoomPemitFragment.onNewIntent(intent);
            }else {
                initRoomDetailFragment(newRoomType);
            }
        } else {//复用上一个房间界面情况
            clearRoomHistory();
            if (roomFragment != null) {
                getSupportFragmentManager().beginTransaction().remove(roomFragment).commitAllowingStateLoss();
                roomFragment = null;
            }
            if (multiRoomPemitFragment != null) {
                getSupportFragmentManager().beginTransaction().remove(multiRoomPemitFragment).commitAllowingStateLoss();
                multiRoomPemitFragment = null;
            }
            lastClickBackKeyTime = 0L;
            rrlvReturnRoomList.setVisibility(View.GONE);
            initRoomDetailFragment(newRoomType);
            roomUid = newRoomUid;
            roomType = newRoomType;
            getMvpPresenter().enterRoom(roomUid, roomType, null);
        }
    }


    /**
     * 清除就房间历史数据 -- 防止后续界面初始化有调用房间数据情况导致新的房间残留旧数据
     */
    public void clearRoomHistory() {
        if (BaseRoomServiceScheduler.getCurrentRoomInfo() != null) {
            if (BaseRoomServiceScheduler.getCurrentRoomInfo().getType() == RoomInfo.ROOMTYPE_HOME_PARTY) {
                NIMClient.getService(ChatRoomService.class).exitChatRoom(String.valueOf(BaseRoomServiceScheduler.getCurrentRoomInfo().getRoomId()));
                AvRoomDataManager.get().release();
                IMNetEaseManager.get().getChatRoomEventObservable()
                        .onNext(new RoomEvent().setEvent(RoomEvent.ROOM_EXIT));
            } else {
                RoomDataManager.get().release();
                IMRoomMessageManager.get().getIMRoomEventObservable()
                        .onNext(new IMRoomEvent().setEvent(IMRoomEvent.ROOM_EXIT));
            }
        }
    }

    /**
     * 显示房间里的详细内容
     */
    public void initRoomDetailFragment(int newRoomType) {
        fragments = new ArrayList<>();
        if (newRoomType == RoomInfo.ROOMTYPE_SINGLE_AUDIO) {
            roomFragment = new SingleAudioRoomFragment();
            fragments.add(roomFragment);
            multiRoomPemitFragment = null;
            tabPageAdapter = new TabPageAdapter(getSupportFragmentManager(), fragments);
            vpContent.setAdapter(tabPageAdapter);
            vpContent.setCurrentItem(0);
            vpContent.setOffscreenPageLimit(1);
        } else if (newRoomType == RoomInfo.ROOMTYPE_MULTI_AUDIO) {
            multiRoomPemitFragment = new MultiRoomPemitFragment();
            fragments.add(multiRoomPemitFragment);
            roomFragment = new MultiAudioRoomFragment();
            fragments.add(roomFragment);
            tabPageAdapter = new TabPageAdapter(getSupportFragmentManager(), fragments);
            vpContent.setAdapter(tabPageAdapter);
            vpContent.setCurrentItem(1);
            vpContent.setOffscreenPageLimit(2);
        } else if (newRoomType == RoomInfo.ROOMTYPE_NEW_AUCTION) {
            roomFragment = new AuctionRoomFragment();
            fragments.add(roomFragment);
            multiRoomPemitFragment = null;
            tabPageAdapter = new TabPageAdapter(getSupportFragmentManager(), fragments);
            vpContent.setAdapter(tabPageAdapter);
            vpContent.setCurrentItem(0);
            vpContent.setOffscreenPageLimit(1);
        }

        vpContent.setVisibility(View.VISIBLE);
        refreshBottomIndicStatus(newRoomType == RoomInfo.ROOMTYPE_MULTI_AUDIO);
    }

    protected void onRoomEventReceive(IMRoomEvent roomEvent) {
        if (roomEvent == null || roomEvent.getEvent() == RoomEvent.NONE) {
            return;
        }
        int event = roomEvent.getEvent();
        if (event != RoomEvent.SPEAK_STATE_CHANGE) {
            LogUtils.d(TAG, "onRoomEventReceive-event:" + event);
        }
        switch (event) {
            case IMRoomEvent.SOCKET_ROOM_ENTER_SUC://socket进房(包括socket重连进房)
                if (null != delayRequestReturnRoomListRunnable) {
                    ThreadUtil.getThreadPool().cancel(delayRequestReturnRoomListRunnable);
                }
                if (roomType == RoomInfo.ROOMTYPE_MULTI_AUDIO && null != rrlvReturnRoomList) {
                    delayRequestReturnRoomListRunnable = new Runnable() {
                        @Override
                        public void run() {
                            getMvpPresenter().getReturnRoomList();
                        }
                    };
                    ThreadUtil.getThreadPool().execute(delayRequestReturnRoomListRunnable);
                }
                break;
            case IMRoomEvent.ENTER_ROOM:
                //房间进入成功会走这里
                dismissDialog();
                break;
            case IMRoomEvent.KICK_OUT_ROOM:
                if (StringUtils.isNotEmpty(roomEvent.getReasonMsg())) {
                    toast(roomEvent.getReasonMsg());
                }
                finish();
                break;
            case RoomEvent.RTC_ENGINE_NETWORK_BAD:
                toast("当前网络不稳定，请检查网络");
                break;
            case IMRoomEvent.RTC_ENGINE_NETWORK_CLOSE:
                toast("当前网络异常，与服务器断开连接，请检查网络");
                break;
            case IMRoomEvent.PLAY_OR_PUBLISH_NETWORK_ERROR:
                toast("当前网络异常，与服务器断开连接，请检查网络");
                break;
            case IMRoomEvent.ZEGO_AUDIO_DEVICE_ERROR:
                toast("当前麦克风可能被占用，请检查设备是否可用");
                break;
            case IMRoomEvent.ROOM_ENTER_OTHER_ROOM:
                BaseRoomServiceScheduler.exitRoom(null);
                finish();
                RoomServiceScheduler.getInstance().enterRoomFromService(this,
                        roomEvent.getRoomUid(), roomEvent.getRoomType());
                ComboGiftPublicScreenMsgSender.getInstance().sendComboGiftScreenMsg();
                LogUtils.d(TAG, "onRoomEventReceive-ROOM_ENTER_OTHER_ROOM:切换房间并退出原来房间");
                break;
            default:
                break;
        }
    }

    @Override
    public void showEnterRoomLoading() {
//        getDialogManager().showRoomLoadingDialog(this);
    }

    @Override
    public void dismisEnterRoomLoading() {
        getDialogManager().dismissDialog();
    }

    /**
     * 显示房间直播结束
     */
    @Override
    public void showRoomFinishedView() {

    }

    /**
     * 显示房间密码弹框
     */
    @Override
    public void showRoomPwdDialog(boolean showFailText) {
        if (mPwdDialogFragment != null && mPwdDialogFragment.getDialog() != null && mPwdDialogFragment.getDialog().isShowing()) {
            return;
        }
        mPwdDialogFragment = InputPwdDialog.newInstance(getString(R.string.input_pwd), getString(R.string.ok), getString(R.string.cancel), showFailText);
        //google的bug
        try {
            mPwdDialogFragment.show(getSupportFragmentManager(), "pwdDialog");
        } catch (Exception e) {
            finish();
        }
        mPwdDialogFragment.setOnDialogBtnClickListener(new InputPwdDialog.OnDialogBtnClickListener() {
            @Override
            public void onBtnConfirm(String pwd) {
                getMvpPresenter().enterRoom(roomUid, roomType, pwd);
            }

            @Override
            public void onBtnCancel() {
                mPwdDialogFragment.dismiss();
                finish();
            }
        });
    }

    /**
     * 显示开房间认证引导dialog
     */
    @Override
    public void showRoomGuideDialog() {
        toast("显示开房间认证引导dialog");
    }

    @Override
    public void dismissDialog() {
        if (mPwdDialogFragment != null) {
            mPwdDialogFragment.dismiss();
        }
    }


    private String roomUrl = "0";

    /**
     * 根据房间类型设置不同默认背景
     *
     * @param roomType
     */
    protected void initRoomBg(int roomType) {
        if (roomType == RoomInfo.ROOMTYPE_SINGLE_AUDIO) {
            ivBack.setImageResource(R.drawable.icon_home_part_room_bg_new);
        } else if (roomType == RoomInfo.ROOMTYPE_MULTI_AUDIO || roomType == RoomInfo.ROOMTYPE_NEW_AUCTION) {
            ivBack.setImageResource(R.drawable.icon_home_part_room_bg_new);
        }
    }

    /**
     * 设置房间背景
     */
    public void setupRoomBg(RoomInfo roomInfo) {
        setupRoomBg(roomInfo, true);
    }

    /**
     * 设置房间背景
     * 这里因为后台的默认背景图设置的值是：0
     */
    public void setupRoomBg(RoomInfo roomInfo, boolean hasAnim) {
        if (null == roomInfo) {
            return;
        }
        if (StringUtils.isNotEmpty(roomInfo.getBackPic())) {
            if (!roomInfo.getBackPic().equals(roomUrl)) {
                if ("0".equals(roomInfo.getBackPic())) {//默认背景图
                    initRoomBg(roomInfo.getType());
                } else {
                    ImageLoadUtils.loadRoomBgImage(this, roomInfo.getBackPic(), ivBack, hasAnim);
                }
                roomUrl = roomInfo.getBackPic();
            }

        } else {
            initRoomBg(roomInfo.getType());
            roomUrl = "0";
        }
    }

    @Override
    public void finishActivity() {
        finish();
    }

    /**
     * 退出登录的时候释放房间资源和退出activity
     */
    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogout() {
        getMvpPresenter().releaseRoomData();
        finish();
    }

    @Override
    public void onBackPressed() {
        if (roomFragment != null) {
            boolean isIntercept = roomFragment.onBack();
            LogUtils.d(TAG, "onBackPressed-isIntercept:" + isIntercept + " roomType:" + roomType);
            if (isIntercept) {
                return;
            }
            if (roomType == RoomInfo.ROOMTYPE_MULTI_AUDIO) {
                long timeInner = System.currentTimeMillis() - lastClickBackKeyTime;
                LogUtils.d(TAG, "onBackPressed-timeInner:" + timeInner);
                if (timeInner < 2000L) {
                    //两次点击间隔小于2秒，直接退出房间
                    LogUtils.d(TAG, "onBackPressed-两次点击间隔小于2秒，直接最小化房间");
                    RoomDataManager.get().setMinimize(true);
                    finish();
                } else {
                    lastClickBackKeyTime = System.currentTimeMillis();
                    LogUtils.d(TAG, "onBackPressed-两次点击间隔大于等于2秒，显示引导房间列表");
                    toast(getResources().getString(R.string.room_back_key_press_tips));
                    if (null != rrlvReturnRoomList && rrlvReturnRoomList.getVisibility() != View.VISIBLE) {
                        rrlvReturnRoomList.refreshRecommRoomList();
                    }
                }
                return;
            } else {
                RoomDataManager.get().setMinimize(true);
            }
        }
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        roomUrl = "0";
        if (null != vpContent && null != onPageChangeListener) {
            vpContent.removeOnPageChangeListener(onPageChangeListener);
            vpContent.removeAllViews();
        }
        if (null != delayRequestReturnRoomListRunnable) {
            ThreadUtil.getThreadPool().cancel(delayRequestReturnRoomListRunnable);
            delayRequestReturnRoomListRunnable = null;
        }
        if (broadcastReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        }

    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.in_from_left, R.anim.out_from_right);
    }

    @Override
    public boolean isStatusBarDarkFont() {
        return false;
    }
}
