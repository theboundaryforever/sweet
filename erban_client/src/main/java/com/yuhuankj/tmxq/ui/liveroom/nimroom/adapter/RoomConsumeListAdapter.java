package com.yuhuankj.tmxq.ui.liveroom.nimroom.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.xchat_core.room.queue.bean.RoomConsumeInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.widget.LevelView;

import java.util.Locale;

/**
 * <p>房间消费adapter  </p>
 *
 * @author Administrator
 * @date 2017/11/20
 */
public class RoomConsumeListAdapter extends BaseQuickAdapter<RoomConsumeInfo, BaseViewHolder> {
    private Drawable mManDrawable, mFemaleDrawable;
    private int whiteColor, blackColor, firstColor, secondColor, thirdColor;
    private String contributionValueFormatText;
    public int rankType = 0;

    public RoomConsumeListAdapter(Context context) {
        super(R.layout.list_item_room_consume);
        mManDrawable = context.getResources().getDrawable(R.drawable.icon_man);
        mFemaleDrawable = context.getResources().getDrawable(R.drawable.icon_female);
        whiteColor = ContextCompat.getColor(context, R.color.white);
        blackColor = ContextCompat.getColor(context, R.color.color_1A1A1A);
        firstColor = ContextCompat.getColor(context, R.color.color_ff6164);
        secondColor = ContextCompat.getColor(context, R.color.color_ffc107);
        thirdColor = ContextCompat.getColor(context, R.color.color_339cfe);
        contributionValueFormatText = context.getString(R.string.contribution_value);
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, RoomConsumeInfo roomConsumeInfo) {
        if (roomConsumeInfo == null) {
            return;
        }
        Log.i(TAG, "convert: " + roomConsumeInfo);
        baseViewHolder.setText(R.id.nick, roomConsumeInfo.getNick())
                .setImageDrawable(R.id.gender, roomConsumeInfo.getGender() == 1 ? mManDrawable : mFemaleDrawable)
                .setText(R.id.coin_text, String.format(Locale.getDefault(),
                        rankType == 0 ? contributionValueFormatText : "%s魅力", String.valueOf(roomConsumeInfo.getSumGold())));
        TextView nick = baseViewHolder.getView(R.id.nick);
        ImageView avatar = baseViewHolder.getView(R.id.avatar);
        ImageView imvNobleIcon = baseViewHolder.getView(R.id.imvNobleIcon);
        ImageLoadUtils.loadAvatar(mContext, roomConsumeInfo.getAvatar(), avatar, true);

        TextView numberText = baseViewHolder.getView(R.id.auction_number_text);
        LevelView levelView = baseViewHolder.getView(R.id.level_info_room_user_list);
        levelView.setLevelImageViewHeight(DisplayUtility.dp2px(mContext, 15));
        if (rankType == 0) {
            levelView.setExperLevel(roomConsumeInfo.getExperLevel());
            levelView.setCharmLevel(0);
        } else {
            levelView.setCharmLevel(roomConsumeInfo.getCharmLevel());
            levelView.setExperLevel(0);
        }
//
        if (!TextUtils.isEmpty(roomConsumeInfo.getVipMedal())) {
            imvNobleIcon.setVisibility(View.VISIBLE);
            ImageLoadUtils.loadImage(mContext, roomConsumeInfo.getVipMedal(), imvNobleIcon);
        } else {
            imvNobleIcon.setVisibility(View.GONE);
        }

        int position = baseViewHolder.getLayoutPosition();
        if (position <= 2) {
            numberText.setTextColor(whiteColor);
            numberText.setText(String.valueOf((position + 1)));
            if (position == 0) {
                nick.setTextColor(firstColor);
                numberText.setBackgroundResource(R.drawable.list_number_background_first);
            } else if (position == 1) {
                nick.setTextColor(secondColor);
                numberText.setBackgroundResource(R.drawable.list_number_background_second);
            } else {
                nick.setTextColor(thirdColor);
                numberText.setBackgroundResource(R.drawable.list_number_background_third);
            }
        } else {
            numberText.setTextColor(blackColor);
            numberText.setBackgroundColor(whiteColor);
            numberText.setText(String.valueOf((position + 1)));
        }
    }
}
