package com.yuhuankj.tmxq.ui.find;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.RadioGroup;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.netease.nim.uikit.common.util.sys.NetworkUtil;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.fragment.BaseFragment;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.find.mengxin.SproutUserInfo;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * @author liaoxy
 * @Description:同城页面
 * @date 2019/5/24 11:15
 */
public class CityWideFragment extends BaseFragment {

    private SwipeRefreshLayout srfContent;
    private RecyclerView rcvContent;
    private RadioGroup ragGender;
    private CityWideAdapter cityWideAdapter;
    private int page = Constants.PAGE_START;
    private List<SproutUserInfo> datas;
    private int gender = 0;

    private final String TAG = CityWideFragment.class.getSimpleName();

    private Call<ResponseBody> lastRequestCall;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_city_wide;
    }

    @Override
    public void onFindViews() {
        rcvContent = mView.findViewById(R.id.rcvContent);
        ragGender = mView.findViewById(R.id.ragGender);
        srfContent = mView.findViewById(R.id.srfContent);
    }

    private BaseQuickAdapter.OnItemClickListener onItemClickListener = new BaseQuickAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
            try {
                if (cityWideAdapter.getData().size() > position) {
                    //统计-附近-进入他人主页
                    Map<String, String> params = StatisticModel.getInstance().getUMAnalyCommonMap(getContext());
                    StatisticManager.get().onEvent(getContext(),
                            StatisticModel.EVENT_ID_CLICK_NEARBY_ENTER_USERPAGE, params);

                    SproutUserInfo user = cityWideAdapter.getData().get(position);
                    UserInfoActivity.start(getContext(), user.getUid(), 1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void initiate() {
        LinearLayoutManager manager = new RecyclerViewNoBugLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rcvContent.setLayoutManager(manager);

        cityWideAdapter = new CityWideAdapter(new ArrayList<>());
        cityWideAdapter.setOnItemClickListener(onItemClickListener);
        cityWideAdapter.setOnItemChildClickListener(onItemChildClickListener);
        cityWideAdapter.setEnableLoadMore(true);
        rcvContent.setAdapter(cityWideAdapter);

        srfContent.setOnRefreshListener(onRefreshListener);
        cityWideAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                nearby();
            }
        }, rcvContent);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (datas == null || datas.size() == 0) {
                if (!CoreManager.getCore(IAuthCore.class).isLogin()) {
                    return;
                }
                nearby();
            }
        }
    }

    private SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            if (NetworkUtil.isNetAvailable(mContext)) {
                if (!ListUtils.isListEmpty(datas)) {
                    datas.clear();
                }
                cityWideAdapter.notifyDataSetChanged();
                page = Constants.PAGE_START;
                nearby();
            } else {
                toast("网络错误");
            }
        }
    };

    @Override
    public void onSetListener() {
        ragGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                gender = 0;
                if (!ListUtils.isListEmpty(datas)) {
                    datas.clear();
                }
                if (cityWideAdapter != null) {
                    cityWideAdapter.notifyDataSetChanged();
                }
                page = Constants.PAGE_START;
                if (checkedId == R.id.rabMan) {
                    //统计-附近-筛选性别-男
                    Map<String, String> params = StatisticModel.getInstance().getUMAnalyCommonMap(getContext());
                    params.put("gender", "1");
                    StatisticManager.get().onEvent(getContext(),
                            StatisticModel.EVENT_ID_CLICK_NEARBY_SELECT_GENDER, params);
                    gender = 1;
                    nearby();
                } else if (checkedId == R.id.rabWoman) {
                    //统计-附近-筛选性别-女
                    Map<String, String> params = StatisticModel.getInstance().getUMAnalyCommonMap(getContext());
                    params.put("gender", "2");
                    StatisticManager.get().onEvent(getContext(),
                            StatisticModel.EVENT_ID_CLICK_NEARBY_SELECT_GENDER, params);
                    gender = 2;
                    nearby();
                } else {
                    //统计-附近-筛选性别-全部
                    Map<String, String> params = StatisticModel.getInstance().getUMAnalyCommonMap(getContext());
                    params.put("gender", "0");
                    StatisticManager.get().onEvent(getContext(),
                            StatisticModel.EVENT_ID_CLICK_NEARBY_SELECT_GENDER, params);
                    nearby();
                }
            }
        });
    }

    private BaseQuickAdapter.OnItemChildClickListener onItemChildClickListener = new BaseQuickAdapter.OnItemChildClickListener() {
        @Override
        public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
            if (view.getId() == R.id.llChating) {
                try {
                    if (datas != null && datas.size() > position) {
                        SproutUserInfo user = cityWideAdapter.getData().get(position);
                        RoomServiceScheduler.getInstance().enterRoom(getActivity(),
                                user.getUserInRoom().getUid(), user.getUserInRoom().getType());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private void nearby() {
        if (null != lastRequestCall && lastRequestCall.isExecuted()) {
            lastRequestCall.cancel();
        }
        LogUtils.d(TAG, "nearby gender:" + gender + " page:" + page);
        lastRequestCall = new FindModel().nearby(gender, page, Constants.PAGE_SIZE, new HttpRequestCallBack<List<SproutUserInfo>>() {
            @Override
            public void onSuccess(String message, List<SproutUserInfo> response) {
                LogUtils.d(TAG, "nearby-->onSuccess message:" + message);
                srfContent.setRefreshing(false);
                if (response != null) {
                    if (page == Constants.PAGE_START) {
                        datas = response;
                    } else {
                        if (datas == null) {
                            datas = new ArrayList<>();
                        }
                        datas.addAll(response);
                    }
                    cityWideAdapter.setNewData(datas);
                    if (response.size() < Constants.PAGE_SIZE) {
                        cityWideAdapter.loadMoreEnd();
                    } else {
                        cityWideAdapter.loadMoreComplete();
                    }
                    page++;
                } else {
                    cityWideAdapter.loadMoreComplete();
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                LogUtils.d(TAG, "nearby-->onFailure code:" + code + " msg:" + msg);
                cityWideAdapter.loadMoreComplete();
                srfContent.setRefreshing(false);
                if (!TextUtils.isEmpty(msg) && !(msg.equals("Canceled")
                        || msg.equals("Connection closed by peer") || msg.equals("Socket closed"))) {
                    toast(msg);
                }
            }
        });
    }
}
