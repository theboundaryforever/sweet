package com.yuhuankj.tmxq.ui.liveroom.nimroom.fragment;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.netease.nim.uikit.common.util.string.StringUtil;
import com.tencent.bugly.crashreport.BuglyLog;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.codec.DESUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.pair.IPairClient;
import com.tongdaxing.xchat_core.pk.IPkCore;
import com.tongdaxing.xchat_core.redpacket.bean.ActionDialogInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.share.IShareCore;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.databinding.FragmentChatroomGameMainBinding;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.AVRoomActivity;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.activity.PkSettingActivity;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.activity.RoomSettingActivity;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.widget.ButtonItemFactory;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.widget.HomePartyPKView;
import com.yuhuankj.tmxq.ui.share.ShareDialog;
import com.yuhuankj.tmxq.ui.widget.ListDataDialog;

import java.util.ArrayList;
import java.util.List;

import cn.sharesdk.framework.Platform;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

import static com.tongdaxing.erban.libcommon.utils.codec.DESUtils.giftCarSecret;

/**
 * 轰趴房
 * Created by 2016/9/22.
 *
 * @author Administrator
 */
@CreatePresenter(HomePartyPresenter.class)
public class HomePartyFragment extends AbsRoomFragment
        implements View.OnClickListener, ShareDialog.OnShareDialogItemClick, HomePartView, HomePartyRoomFragment.OnPkVoteInfoChangeListener {

    public boolean isFristEnterRoom = true;
    private HomePartyRoomFragment roomFragment;
    private UserInfo mUserInfo;

    private FragmentChatroomGameMainBinding gameMainBinding;
    private HomePartyPKView pkView;
    private boolean hasAttentionRoomAlready = false;

    private final String TAG = HomePartyFragment.class.getSimpleName();

    public static HomePartyFragment newInstance(long roomUid) {
        HomePartyFragment homePartyFragment = new HomePartyFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(Constants.ROOM_UID, roomUid);
        homePartyFragment.setArguments(bundle);

        return homePartyFragment;
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        // clear views
        gameMainBinding.roomTitle.setText("");
        gameMainBinding.roomId.setText(getString(R.string.room_id_online_number, 0, 0));
        if (roomFragment != null) {
            roomFragment.isFristEnterRoom = isFristEnterRoom;
            roomFragment.onNewIntent(intent);
        }
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_chatroom_game_main;
    }

    @Override
    public void onFindViews() {
        gameMainBinding = DataBindingUtil.bind(mView);
        gameMainBinding.setClick(this);
        gameMainBinding.roomTitle.setSelected(true);
        pkView = mView.findViewById(R.id.view_pk);
    }

    @Override
    public void onSetListener() {
    }

    @Override
    public void initiate() {
        roomFragment = new HomePartyRoomFragment();
        roomFragment.isFristEnterRoom = isFristEnterRoom;
        roomFragment.setSvgaAciton(new HomePartyRoomFragment.SvgaAciton() {
            @Override
            public void showCar(String url) {
                try {
                    url = DESUtils.DESAndBase64Decrypt(url, giftCarSecret);
                    BuglyLog.d(TAG, "play car anim, url:" + url);
                    if (!gameMainBinding.giftView.giftEffectView.isAnim()) {
                        gameMainBinding.giftView.giftEffectView.drawSvgaEffect(0, url, null, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void showPair(Json json) {
                gameMainBinding.pairShowView.onPairEnd(json);
            }

        });
        roomFragment.setOnPkVoteInfoChangeListener(this);
        getChildFragmentManager().beginTransaction().replace(R.id.fm_content, roomFragment).commitAllowingStateLoss();
        LogUtils.d(TAG, "initiate-add roomFragment");
        updateView();

        RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoomInfo != null && currentRoomInfo.getUid() != CoreManager.getCore(IAuthCore.class).getCurrentUid()) {
            getMvpPresenter().getCheckRoomAttention(currentRoomInfo.getRoomId());
        }

        //1.2.7.5版本隐藏大转盘，因为没有对应链接放出来，房间更多按钮在非房主且非管理员，隐藏
        //2.2.7.6版本有大转盘，接口开关控制，所以房间更多按钮，在非房主非管理员关闭大转盘的条件下再隐藏
        Disposable subscribe = IMNetEaseManager.get().getChatRoomEventObservable()
                .compose(bindToLifecycle())
                .subscribe(new Consumer<RoomEvent>() {
                    @Override
                    public void accept(RoomEvent roomEvent) throws Exception {
                        if (roomEvent == null) {
                            return;
                        }
                        int event = roomEvent.getEvent();
                        switch (event) {
                            case RoomEvent.ENTER_ROOM:
                            case RoomEvent.ROOM_INFO_UPDATE:
                                LogUtils.d(TAG, "initiate-accept roomEvent:" + event);
                                updateView();
                                break;
                            default:
                        }
                    }
                });
        mCompositeDisposable.add(subscribe);
    }

    @CoreEvent(coreClientClass = IPairClient.class)
    public void onResponseToast(String toastContent) {
        if (toastContent != null) {
            toast(toastContent);
        }
    }

    @Override
    public void release() {
        super.release();
        if (roomFragment != null) {
            roomFragment.setOnPkVoteInfoChangeListener(null);
            roomFragment = null;
        }
    }

    private void setIdOnlineData() {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo == null) {
            return;
        }
        if (null == getActivity()) {
            return;
        }
        if (mUserInfo != null) {
            gameMainBinding.roomId.setText(getActivity().getResources().getString(R.string.room_id, String.valueOf(mUserInfo.getErbanNo())));
        } else {
            gameMainBinding.roomId.setText(getActivity().getResources().getString(R.string.room_id_zero));
        }
        gameMainBinding.setRoomInfo(roomInfo);
        gameMainBinding.tvOnlineNum.setText(getActivity().getResources().getString(R.string.room_online_num, roomInfo.onlineNum + roomInfo.getFactor()));
    }

    public void updateView() {
        RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoomInfo != null) {
            mUserInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(currentRoomInfo.getUid());
            gameMainBinding.roomTitle.setText(currentRoomInfo.getTitle());
            if (!StringUtil.isEmpty(currentRoomInfo.getRoomPwd())) {
                gameMainBinding.roomId.setCompoundDrawablesWithIntrinsicBounds(null, null,
                        getResources().getDrawable(R.drawable.icon_room_lock), null);
            } else {
                gameMainBinding.roomId.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
            }
            setIdOnlineData();
        }

        RoomInfo serverRoomInfo = AvRoomDataManager.get().mServiceRoominfo;
        LogUtils.d(TAG, "updateView isRoomOwner:" + AvRoomDataManager.get().isRoomOwner());
        LogUtils.d(TAG, "updateView isRoomAdmin:" + AvRoomDataManager.get().isRoomAdmin());

        if (null != roomFragment) {
            boolean noShowMoreOperaBtn = !AvRoomDataManager.get().isRoomOwner() && !AvRoomDataManager.get().isRoomAdmin() && (serverRoomInfo == null || (!serverRoomInfo.isBigWheelSwitch() && !serverRoomInfo.isRedPacketSwitch()));
            roomFragment.setShowMoreOperaBtn(!noShowMoreOperaBtn);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_followRoom:
                if (null == getActivity()) {
                    return;
                }
                getDialogManager().showProgressDialog(getActivity(), getResources().getString(R.string.network_loading));
                RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                if (currentRoomInfo != null && currentRoomInfo.getUid() != CoreManager.getCore(IAuthCore.class).getCurrentUid()) {
                    if (hasAttentionRoomAlready) {
                        getMvpPresenter().doRoomCancelAttention(currentRoomInfo.getRoomId());
                    } else {
                        getMvpPresenter().doRoomAttention(currentRoomInfo.getRoomId());
                    }
                }
                break;
            case R.id.room_more:
                showMoreItems();
                break;
            case R.id.iv_share:
                if (null == getActivity()) {
                    return;
                }
                ShareDialog shareDialog = new ShareDialog(getActivity());
                shareDialog.showShareFans = true;
                shareDialog.setOnShareDialogItemClick(HomePartyFragment.this);
                shareDialog.show();

                //房间-分享
                StatisticManager.get().onEvent(getActivity(),
                        StatisticModel.EVENT_ID_ROOM_OPEN_SHARE,
                        StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
                break;
            case R.id.room_id:
            case R.id.tv_online_num:
            case R.id.room_title:
                if (null == getActivity()) {
                    return;
                }
                ListDataDialog.newOnlineUserListInstance(getActivity())
                        .show(getChildFragmentManager());
                break;
            default:
        }
    }

    private void showMoreItems() {
        List<ButtonItem> buttonItems = new ArrayList<>();
        final RoomInfo mCurrentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (mCurrentRoomInfo != null) {
            int publicChatSwitch = mCurrentRoomInfo.getPublicChatSwitch();
            boolean roomOwner = AvRoomDataManager.get().isRoomOwner();
            boolean roomAdmin = AvRoomDataManager.get().isRoomAdmin();
            if (roomOwner || roomAdmin) {
                ButtonItem buttonItem = new ButtonItem(getResources().getString(publicChatSwitch == 0 ?
                        R.string.room_settings_public_screen_close :
                        R.string.room_settings_public_screen_open), new ButtonItem.OnClickListener() {
                    @Override
                    public void onClick() {
                        IAuthCore iAuthCore = CoreManager.getCore(IAuthCore.class);
                        if (iAuthCore == null) {
                            return;
                        }
                        String ticket = iAuthCore.getTicket();
                        long currentUid = iAuthCore.getCurrentUid();
                        CoreManager.getCore(IAVRoomCore.class).changeRoomMsgFilter(roomOwner,
                                publicChatSwitch == 0 ? 1 : 0, ticket, currentUid + "",
                                0, 0, false, null);
                        if (null == getActivity()) {
                            return;
                        }
                        //房间-打开公屏、房间关闭公屏
                        StatisticManager.get().onEvent(getActivity(),
                                publicChatSwitch == 0 ? StatisticModel.EVENT_ID_ROOM_CLOSE_PUBLIC_SCREEN :
                                        StatisticModel.EVENT_ID_ROOM_OPEN_PUBLIC_SCREEN,
                                StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
                    }
                });
                buttonItems.add(buttonItem);
            }
        }

        ButtonItem buttonItem1 = new ButtonItem("最小化", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                AvRoomDataManager.get().setMinimize(true);
                if (null != getActivity()) {
                    getActivity().finish();
                }
            }
        });
        ButtonItem buttonItem2 = new ButtonItem("退出房间", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                if (null != getActivity()) {
                    ((AVRoomActivity) getActivity()).toBack();
                }
            }
        });
        ButtonItem buttonItem3 = new ButtonItem("房间设置", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                if (null != getContext()) {
                    RoomSettingActivity.start(getContext());
                }
            }
        });
        String currentUid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        if (!AvRoomDataManager.get().isRoomOwner(currentUid) && null != mContext) {
            buttonItems.add(ButtonItemFactory.createReportItem(mContext, "举报房间", 2, null != mCurrentRoomInfo ? mCurrentRoomInfo.getUid() : 0L));
        }
        boolean isRoomManager = AvRoomDataManager.get().isRoomOwner(currentUid) || AvRoomDataManager.get().isRoomAdmin();
        if (isRoomManager) {
            buttonItems.add(buttonItem3);
        }
        //PK按钮
        if (isRoomManager) {
            ButtonItem buttonItem4 = null;
            if (pkView != null && pkView.getPkVoteInfo() != null && pkView.getPkVoteInfo().getVoteId() > 0) {
                buttonItem4 = new ButtonItem(getResources().getString(R.string.room_settings_pk_close), new ButtonItem.OnClickListener() {
                    @Override
                    public void onClick() {
                        CoreManager.getCore(IPkCore.class).cancelPK(AvRoomDataManager.get().mCurrentRoomInfo == null ?
                                        0 : AvRoomDataManager.get().mCurrentRoomInfo.getRoomId());
                        //房间-关闭PK
                        if (null != getActivity()) {
                            StatisticManager.get().onEvent(getActivity(),
                                    StatisticModel.EVENT_ID_ROOM_CLOSE_PK,
                                    StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
                        }
                    }
                });
            } else {
                buttonItem4 = new ButtonItem(getResources().getString(R.string.room_settings_pk_open), new ButtonItem.OnClickListener() {
                    @Override
                    public void onClick() {
                        if (null != mContext) {
                            mContext.startActivity(new Intent(mContext, PkSettingActivity.class));
                        }
                        if (null != getActivity()) {
                            //房间-进入PK设置
                            StatisticManager.get().onEvent(getActivity(),
                                    StatisticModel.EVENT_ID_ROOM_ENTER_PK_SETTING,
                                    StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
                        }
                    }
                });
            }
            if (buttonItem4 != null) {
                buttonItems.add(buttonItem4);
            }
        }
        if (isRoomManager && mCurrentRoomInfo != null && mCurrentRoomInfo.getCharmSwitch() == 1) {
            long roomId = mCurrentRoomInfo.getRoomId();
            ButtonItem buttonItem = new ButtonItem(getResources().getString(R.string.room_settings_charm_value_clear), new ButtonItem.OnClickListener() {
                @Override
                public void onClick() {
                    getDialogManager().showOkCancelDialog(
                            getResources().getString(R.string.room_settings_charm_value_clear_tips), true,
                            new DialogManager.AbsOkDialogListener() {
                                @Override
                                public void onOk() {
                                    CoreManager.getCore(IAVRoomCore.class).clearCharm(currentUid, roomId + "");
                                }
                            });

                }
            });
            buttonItems.add(buttonItem);
        }

        buttonItems.add(buttonItem1);
        buttonItems.add(buttonItem2);
        if (null == getActivity()) {
            return;
        }
        DialogManager dialogManager = ((BaseMvpActivity) getActivity()).getDialogManager();
        if (dialogManager != null) {
            dialogManager.showCommonPopupDialog(buttonItems, "取消");
        }
    }

    @Override
    public void onSharePlatformClick(Platform platform) {
        RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoomInfo != null) {
            CoreManager.getCore(IShareCore.class).shareRoom(platform, currentRoomInfo.getUid(),
                    currentRoomInfo.getTitle(), currentRoomInfo.getType());
        }
    }

    @Override
    public void onShowActivity(List<ActionDialogInfo> dialogInfos) {
        if (roomFragment != null && roomFragment.isAdded()) {
            roomFragment.showActivity(dialogInfos);
        }
    }

    @Override
    public void onDestroy() {
        try {
            if (gameMainBinding.giftView != null) {
                gameMainBinding.giftView.release();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (roomFragment != null) {
            roomFragment = null;
        }
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (roomFragment != null) {
            roomFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRoomOnlineNumberSuccess(int onlineNumber) {
        super.onRoomOnlineNumberSuccess(onlineNumber);
        setIdOnlineData();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfo(UserInfo info) {
        RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (null != info && null != currentRoomInfo && currentRoomInfo.getUid() == info.getUid()) {
            updateView();
        }
    }

    @Override
    public void onGetRoomAttentionStatus(boolean isSuccess, String message, boolean status) {
        LogUtils.d(TAG, "onGetRoomAttentionStatus-isSuccess:" + isSuccess + " message:" + message + " status:" + status);
        if (isSuccess) {
            hasAttentionRoomAlready = status;
            gameMainBinding.tvFollowRoom.setVisibility(View.VISIBLE);
            if (status) {
                gameMainBinding.tvFollowRoom.setBackgroundColor(Color.TRANSPARENT);
            } else {
                gameMainBinding.tvFollowRoom.setBackgroundResource(R.drawable.bg_room_follow_room);
            }
            gameMainBinding.tvFollowRoom.setText(getResources().getString(status ? R.string.user_info_already_attention : R.string.user_info_attention));
        }
    }

    @Override
    public void onRoomAttention(boolean isSuccess, String message) {
        LogUtils.d(TAG, "onRoomAttention-isSuccess:" + isSuccess + " message:" + message);
        getDialogManager().dismissDialog();
        if (isSuccess) {
            hasAttentionRoomAlready = true;
            gameMainBinding.tvFollowRoom.setBackgroundColor(Color.TRANSPARENT);
            gameMainBinding.tvFollowRoom.setText(getResources().getString(R.string.user_info_already_attention));
        } else if (!TextUtils.isEmpty(message)) {
            toast(message);
        }
    }

    @Override
    public void onRoomCancelAttention(boolean isSuccess, String message) {
        LogUtils.d(TAG, "onGetRoomAttentionStatus-isSuccess:" + isSuccess + " message:" + message);
        getDialogManager().dismissDialog();
        if (isSuccess) {
            hasAttentionRoomAlready = false;
            gameMainBinding.tvFollowRoom.setBackgroundResource(R.drawable.bg_room_follow_room);
            gameMainBinding.tvFollowRoom.setText(getResources().getString(R.string.user_info_attention));
        } else if (!TextUtils.isEmpty(message)) {

            toast(message);
        }
    }

    @Override
    public boolean hasPKOpened() {
        boolean hasPKOpened = pkView != null && pkView.getPkVoteInfo() != null && pkView.getPkVoteInfo().getVoteId() > 0;
        LogUtils.d(TAG, "hasPKOpened-hasPKOpened:" + hasPKOpened);
        return hasPKOpened;
    }
}