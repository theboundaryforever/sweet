package com.yuhuankj.tmxq.ui.user.password;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;

import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.databinding.ActivityForgetPswBinding;
import com.yuhuankj.tmxq.ui.login.CodeDownTimer;

/**
 * @author liaoxy
 * @Description:重置密码
 * @date 2019/3/27 18:22
 */
public class ResetPwdActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = "ResetPwdActivity";
    private String errorStr;
    private ActivityForgetPswBinding forgetPswBinding;
    private CodeDownTimer timer;
    private int type = 1;//1：登录密码 2：提现密码

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        forgetPswBinding = DataBindingUtil.setContentView(this, R.layout.activity_forget_psw);
        initTitleBar("重置密码");
        onFindViews();
        onSetListener();
    }


    public void onFindViews() {
        Intent intent = getIntent();
        if (intent != null) {
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            if (userInfo != null && userInfo.isBindPhone() && !TextUtils.isEmpty(userInfo.getPhone())) {
                forgetPswBinding.etPhone.setText(userInfo.getPhone());
                forgetPswBinding.etPhone.setEnabled(false);
            }
            type = intent.getIntExtra("type", 1);
        }
        forgetPswBinding.etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() < 6 || charSequence.length() > 16) {
                    forgetPswBinding.tlPassword.setErrorEnabled(true);
                    forgetPswBinding.tlPassword.setError("密码长度6-16个字符");
                } else {
                    forgetPswBinding.tlPassword.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    public void onSetListener() {
        forgetPswBinding.setClick(this);
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onSmsFail(String error) {
        toast(error);
        LogUtils.d(TAG, "onSmsFail error:" + error);
    }

    @Override
    public void onClick(View v) {
        String phone = forgetPswBinding.etPhone.getText().toString();
        switch (v.getId()) {
            case R.id.btn_modify:
                String psw = forgetPswBinding.etPassword.getText().toString();
                String sms_code = forgetPswBinding.etCode.getText().toString();
                if (!StringUtils.isEmpty(sms_code)) {
                    if (isOK(phone, psw)) {
                        DialogManager dialogManager = new DialogManager(this);
                        dialogManager.showProgressDialog(this, "加载中...");
                        new PwdModel().resetPwd(psw, phone, sms_code, type, new OkHttpManager.MyCallBack<ServiceResult<String>>() {
                            @Override
                            public void onError(Exception e) {
                                dialogManager.dismissDialog();
                                e.printStackTrace();
                                SingleToastUtil.showToast(e.getMessage());
                            }

                            @Override
                            public void onResponse(ServiceResult<String> response) {
                                dialogManager.dismissDialog();
                                if (response != null && response.isSuccess()) {
                                    SingleToastUtil.showToast("重置密码成功");
                                    finish();
                                } else {
                                    if (response != null && !TextUtils.isEmpty(response.getMessage())) {
                                        SingleToastUtil.showToast(response.getMessage());
                                    } else {
                                        SingleToastUtil.showToast("重置密码失败");
                                    }
                                }
                            }
                        });
                    } else {
                        toast(errorStr);
                    }
                } else {
                    toast("验证码不能为空！");
                }
                break;
            case R.id.btn_get_code:
                if (phone.length() == 11) {
                    timer = new CodeDownTimer(forgetPswBinding.btnGetCode, 60000, 1000);
                    timer.start();
                    CoreManager.getCore(IAuthCore.class).requestSMSCode(phone, 3);
                } else {
                    toast("手机号码不正确");
                }
                break;
        }
    }

    private boolean isOK(String phone, String psw) {
        if (StringUtils.isEmpty(psw) && psw.length() < 6) {
            errorStr = "请核对密码！";
            return false;
        }
        if (StringUtils.isEmpty(phone)) {
            errorStr = "请填写手机号码！";
            return false;
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
        }
    }
}
