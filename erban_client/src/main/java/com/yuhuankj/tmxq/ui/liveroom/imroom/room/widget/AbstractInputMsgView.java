package com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.growingio.android.sdk.collection.GrowingIO;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.callback.SoftKeyBoardListener;

/**
 * 房间消息输入view
 *
 * @author zeda
 */
public abstract class AbstractInputMsgView extends FrameLayout implements SoftKeyBoardListener.OnSoftKeyBoardChangeListener {

    private final String TAG = AbstractInputMsgView.class.getSimpleName();

    private EditText etInput;
    private View btnInput;
    private OnSendBtnClickListener onSendBtnClickListener;
    private SoftKeyBoardListener.OnSoftKeyBoardChangeListener mOnSoftKeyBoardChangeListener;

    public AbstractInputMsgView(Context context) {
        this(context, null);
    }

    public AbstractInputMsgView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AbstractInputMsgView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private Runnable showKeyBoardRunable = new Runnable() {
        @Override
        public void run() {
            InputMethodManager imm = null;
            if (getContext() != null) {
                imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            }
            if (imm == null) {
                return;
            }
            imm.showSoftInput(etInput, InputMethodManager.SHOW_FORCED);
        }
    };

    public SoftKeyBoardListener.OnSoftKeyBoardChangeListener getOnSoftKeyBoardChangeListener() {
        return mOnSoftKeyBoardChangeListener;
    }

    @SuppressLint("ClickableViewAccessibility")
    private void init() {
        inflate(getContext(), R.layout.layout_live_room_input_msg, this);
        etInput = findViewById(R.id.etInput);
        GrowingIO.getInstance().trackEditText(etInput);
        btnInput = findViewById(R.id.btnInput);

        setOnTouchListener((v, event) -> {
            etInput.clearFocus();
            setVisibility(View.GONE);
            hideKeyBoard();
            return false;
        });

        btnInput.setOnClickListener(v -> {
            if (onSendBtnClickListener != null) {
                onSendBtnClickListener.onClick(getInputText());
            }
        });

        mOnSoftKeyBoardChangeListener = this;
    }

    /**
     * 打开软键盘并显示头布局
     */
    public void showKeyBoard() {
        postDelayed(showKeyBoardRunable, 80L);
    }

    /**
     * 隐藏软键盘并隐藏头布局
     */
    public void hideKeyBoard() {
        LogUtils.d(TAG, "hideKeyBoard");
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm == null) {
            return;
        }
        imm.hideSoftInputFromWindow(etInput.getWindowToken(), 0);
    }

    public String getInputText() {
        return etInput.getText().toString();
    }

    public void setInputText(String inputText) {
        if (inputText == null) {
            inputText = "";
        }
        etInput.setText(inputText);
        etInput.setSelection(inputText.length());
        etInput.setFocusable(true);
        etInput.setFocusableInTouchMode(true);
        etInput.requestFocus();
    }

    public void setOnSendBtnClickListener(OnSendBtnClickListener listener) {
        this.onSendBtnClickListener = listener;
    }

    public void clearFocusAndInput() {
        etInput.setText("");
        etInput.setSelection(0);
        etInput.setFocusable(false);
        etInput.setFocusableInTouchMode(false);
        etInput.clearFocus();
        setVisibility(View.GONE);
        hideKeyBoard();
    }

    public interface OnSendBtnClickListener {
        void onClick(String inputContent);
    }

    public void release() {
        if (null != mOnSoftKeyBoardChangeListener) {
            mOnSoftKeyBoardChangeListener = null;
        }
        removeCallbacks(showKeyBoardRunable);
    }
}
