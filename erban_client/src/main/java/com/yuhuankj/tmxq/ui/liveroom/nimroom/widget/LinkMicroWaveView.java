package com.yuhuankj.tmxq.ui.liveroom.nimroom.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class LinkMicroWaveView extends View {
    private Paint paint;
    private List<Float> radus = new ArrayList<>();
    private List<Integer> alphaList = new ArrayList<>();
    private boolean isStarting = false;

    public LinkMicroWaveView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public LinkMicroWaveView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LinkMicroWaveView(Context context) {
        super(context);
        init();
    }

    protected void init() {
        paint = new Paint();
        paint.setColor(Color.WHITE);
//        paint.setAntiAlias(true); //消除锯齿
//        paint.setStrokeWidth(10);
//绘制空心圆或 空心矩形,只显示边缘的线，不显示内部
//        paint.setStyle(Paint.Style.STROKE);
        radus.add(customRadus);
        alphaList.add(180);
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!isStarting) return;

        //循环每个圈，逐层增加半径，减小透明度
        for (int i = 0; i < radus.size(); i++) {
            paint.setAlpha(alphaList.get(i));
            canvas.drawCircle(getWidth() / 2, getHeight() / 2, radus.get(i), paint);
            if (radus.get(i) < middleRadus) {
                radus.set(i, radus.get(i) + 5f);
            }
            if (alphaList.get(i) > alphaMax) {
                alphaList.set(i, alphaList.get(i) - alphaDel);
            }
        }

        //最外面一圈半径为35dp的时候，增加一圈
        if (radus.get(0) >= addRadus) {
            radus.add(0, customRadus);
            alphaList.add(0, 180);
        }

        //最外面一圈半径为50的时候，减少一圈
        if (radus.get(radus.size() - 1) >= delRadus && radus.size() > 1) {
            radus.clear();
            radus.add(customRadus);
        }
        invalidate();
    }

    public void start() {
        isStarting = true;
        invalidate();
    }

    public void stop() {
        radus.clear();
        alphaList.clear();
        alphaList.add(180);
        radus.add(customRadus);
        isStarting = false;
    }


    public void setCustomRadus(float customRadus) {
        this.customRadus = customRadus;
    }

    public void setAddRadus(int addRadus) {
        this.addRadus = addRadus;
    }

    public void setDelRadus(int delRadus) {
        this.delRadus = delRadus;
    }

    public void setStopRadus(int stopRadus) {
        this.stopRadus = stopRadus;
    }

    public void setMiddleRadus(int middleRadus) {
        this.middleRadus = middleRadus;
    }

    public void setAlphaMax(int alphaMax) {
        this.alphaMax = alphaMax;
    }

    public void setAlphaDel(int alphaDel) {
        this.alphaDel = alphaDel;
    }

    protected float customRadus = 50f;
    protected int addRadus = 35;
    protected int delRadus = 50;
    protected int stopRadus = 50;
    protected int middleRadus = 40;
    protected int alphaMax = 10;
    protected int alphaDel = 1;
}
