package com.yuhuankj.tmxq.ui.liveroom.nimroom.face;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.widget.ImageView;

import com.netease.nim.uikit.common.util.log.LogUtil;
import com.tencent.bugly.crashreport.BuglyLog;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.xchat_core.room.face.FaceInfo;
import com.tongdaxing.xchat_core.room.face.FaceReceiveInfo;
import com.tongdaxing.xchat_core.room.face.IFaceCore;
import com.tongdaxing.xchat_core.utils.ThreadUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author xiaoyu
 * @date 2017/11/30
 */

public class AnimFaceFactory {

    private static final String TAG = "AnimFaceFactory";

    private AnimFaceFactory() {
    }

    public static void asynLoadAnim(FaceReceiveInfo faceReceiveInfo, ImageView imageView, Context context, int width, int height) {
        ThreadUtil.getThreadPool().execute(new Runnable() {
            @Override
            public void run() {
                if (context == null) {
                    return;
                }
                AnimationDrawable drawable = AnimFaceFactory.get(faceReceiveInfo, context, width, height);
                ThreadUtil.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (drawable == null || imageView == null) {
                            return;
                        }
                        imageView.setImageDrawable(drawable);
                        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                        drawable.setOneShot(true);
                        drawable.start();
                    }
                });
            }
        });
    }

    public static AnimationDrawable get(FaceReceiveInfo faceReceiveInfo, Context context, int width, int height) {
        long time = System.currentTimeMillis();
        AnimationDrawable drawable = new AnimationDrawable();
        FaceInfo faceInfo = CoreManager.getCore(IFaceCore.class).findFaceInfoById(faceReceiveInfo.getFaceId());
        // 如果找不到对应的表情,直接返回null
        if (faceInfo == null) {
            return null;
        }
        // 定位引起ANR的表情资源
        BuglyLog.d(TAG, "get-->faceId:" + faceInfo.getId());
        int startIndex = faceInfo.getAnimationIndexStart();
        int endIndex = faceInfo.getAnimationIndexEnd();
        int duration = (int) ((faceInfo.getAnimationDuration() + 0.F) / (endIndex - startIndex));
        int repeatCount = faceInfo.getRepeatCount();
        while (repeatCount > 0) {
            for (int i = startIndex; i <= endIndex; i++) {
                // 增加每一帧到drawable中
                if (StringUtils.isNotEmpty(faceInfo.getFacePath(i))) {
                    File file = new File(faceInfo.getFacePath(i));
                    if (file.exists()) {
                        // 确定一张图片的位置
                        Bitmap originalBitmap = BitmapFactory.decodeFile(faceInfo.getFacePath(i));
                        if (originalBitmap != null && originalBitmap.getWidth() > 0 && originalBitmap.getHeight() > 0) {
                            OneFaceDrawable face = new OneFaceDrawable(context,originalBitmap , width, height);
                            drawable.addFrame(face, duration);
                        }
                    }
                }
            }
            repeatCount--;
        }
        // 如果是普通表情,则直接返回
        if (faceInfo.getResultCount() <= 0) {
            drawable.addFrame(new ColorDrawable(Color.TRANSPARENT), 10);
            return drawable;
        }
        // 如果有结果,增加结果帧
        List<Integer> resultIndexes = faceReceiveInfo.getResultIndexes();
        duration = faceInfo.getResultDuration();
        // 找出所有结果图片的路径
        List<String> images = new ArrayList<>();
        for (int i = 0; i < resultIndexes.size(); i++) {
            LogUtil.e(TAG, "get i:" + i + " facePath:" + faceInfo.getFacePath(resultIndexes.get(i)));
            images.add(faceInfo.getFacePath(resultIndexes.get(i)));
        }
        // 根据显示类型,产生对应的类型的结果图片
        int displayType = faceInfo.getDisplayType();
        if (displayType == FaceInfo.DISPLAY_TYPE_ONE_PIC || images.size() == 1) {
            if (StringUtils.isNotEmpty(faceInfo.getFacePath(0))) {
                File file = new File(faceInfo.getFacePath(0));
                if (file.exists()) {
                    // 确定一张图片的位置
                    Bitmap originalBitmap = BitmapFactory.decodeFile(images.get(0));
                    if (originalBitmap != null && originalBitmap.getWidth() > 0 && originalBitmap.getHeight() > 0){
                        OneFaceDrawable face = new OneFaceDrawable(context, originalBitmap, width, height);
                        drawable.addFrame(face, duration);
                    }
                }
            }
        } else if (displayType == FaceInfo.DISPLAY_TYPE_FLOW) {
            FlowFaceDrawable flowFaceDrawable = new FlowFaceDrawable(context, images, width, height);
            flowFaceDrawable.setBounds(0, 0, width, height);
            drawable.addFrame(flowFaceDrawable, duration);
        } else if (displayType == FaceInfo.DISPLAY_TYPE_OVERLAY) {
            OverlayFaceDrawable overlayFaceDrawable = new OverlayFaceDrawable(context, images, width, height);
            overlayFaceDrawable.setBounds(0, 0, width, height);
            drawable.addFrame(overlayFaceDrawable, duration);
        } else {
            // 未知类型,不显示动画
            return null;
        }

        //隐藏drawable
        ColorDrawable colorDrawable = new ColorDrawable(Color.TRANSPARENT);
        colorDrawable.setBounds(0, 0, width, height);
        drawable.addFrame(new ColorDrawable(Color.TRANSPARENT), 10);
        Log.e(TAG, "time consumed: " + (System.currentTimeMillis() - time));
        return drawable;
    }
}
