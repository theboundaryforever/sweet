package com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

/**
 * 多人语聊房房间消息输入view
 *
 * @author zeda
 */
public class MultiInputMsgView extends AbstractInputMsgView {

    public MultiInputMsgView(Context context) {
        super(context);
    }

    public MultiInputMsgView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MultiInputMsgView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void keyBoardShow(int height) {
        /*软键盘显示：执行隐藏title动画，并修改listview高度和装载礼物容器的高度*/
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) getLayoutParams();
        if (null == lp) {
            return;
        }
        lp.bottomMargin = height;
//        if (NotchFixUtil.check(NotchFixUtil.ROM_EMUI) && android.os.Build.VERSION.SDK_INT >= 28) {
//            Context context = getContext();
//            if (context instanceof FragmentActivity) {
//                FragmentActivity fragmentActivity = (FragmentActivity) context;
//                if (NotchFixUtil.hasHwNotchInScreen(fragmentActivity)) {
//                    //HUAWEI CLT-AL01 Android9 API28、
////                    lp.bottomMargin -= NotchFixUtil.getNotchSize(context)[1];
//                }
//            }
//        }
        setLayoutParams(lp);
    }

    @Override
    public void keyBoardHide(int height) {
        /*软键盘隐藏：隐藏聊天输入框并显示聊天按钮，执行显示title动画，并修改listview高度和装载礼物容器的高度*/
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) getLayoutParams();
        if (null == lp) {
            return;
        }
        lp.bottomMargin = 0;
        setLayoutParams(lp);
        setVisibility(View.GONE);
    }
}
