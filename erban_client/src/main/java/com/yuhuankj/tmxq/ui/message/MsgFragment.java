package com.yuhuankj.tmxq.ui.message;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.recent.RecentContactsCallback;
import com.netease.nim.uikit.recent.RecentContactsFragment;
import com.netease.nim.uikit.session.NIMSessionStatusManager;
import com.netease.nim.uikit.session.OfficSecrRedPointCountManager;
import com.netease.nimlib.sdk.msg.MsgService;
import com.netease.nimlib.sdk.msg.attachment.AudioAttachment;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.RecentContact;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.MiniGameInvitedAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.NoticeAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RedPacketAttachment;
import com.tongdaxing.xchat_core.im.message.IIMMessageCoreClient;
import com.tongdaxing.xchat_core.redpacket.bean.RedPacketInfoV2;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.fragment.BaseLazyFragment;
import com.yuhuankj.tmxq.thirdsdk.nim.MsgCenterRedPointStatusManager;
import com.yuhuankj.tmxq.ui.message.attention.AttentionListActivity;
import com.yuhuankj.tmxq.ui.message.fans.FansListActivity;
import com.yuhuankj.tmxq.ui.message.friend.FriendListActivity;
import com.yuhuankj.tmxq.ui.message.like.LikeMeListActivity;
import com.yuhuankj.tmxq.ui.message.like.LikeModel;
import com.yuhuankj.tmxq.ui.nim.sysmsg.SysMsgActivity;

import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_LOTTERY;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_NOTICE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_OPEN_ROOM_NOTI;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PACKET;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_MINI_GAME;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_MINI_GAME_ON_INVITED_NOTIFY;

/**
 * <p> 主页消息界面  </p>
 *
 * @author Administrator
 * @date 2017/11/14
 */
public class MsgFragment extends BaseLazyFragment implements MsgCenterRedPointStatusManager.OnRedPointStatusChangedListener{
    public static final String TAG = MsgFragment.class.getSimpleName();

    private View vNewFriendRedPoint;
    private View vNewLikeRedPoint;
    private View vNewFansRedPoint;

    private RecentContactsFragment recentContactsFragment;
    private boolean mIsInitView;
    private RecentContactsCallback recentContactsCallback;

    private View llMenu;
    private View llUnraedClear;
    private View llRingSwitch;
    private ImageView ivRingSwitch;
    private TextView tvRingSwitch;
    private View llPrivateSwitch;
    private ImageView ivPrivateSwitch;
    private TextView tvPrivateSwitch;


    @Override
    public void onFindViews() {
        LogUtils.d(TAG, "onFindViews");
        vNewFriendRedPoint = mView.findViewById(R.id.vNewFriendRedPoint);
        vNewLikeRedPoint = mView.findViewById(R.id.vNewLikeRedPoint);
        vNewFansRedPoint = mView.findViewById(R.id.vNewFansRedPoint);

        int hDivWidth = (DisplayUtility.getScreenWidth(getActivity()) - DisplayUtility.dp2px(getActivity(), 278)) / 3;
        LinearLayout.LayoutParams lp1 = (LinearLayout.LayoutParams) mView.findViewById(R.id.rlLikes).getLayoutParams();
        lp1.leftMargin = hDivWidth;
        lp1.rightMargin = hDivWidth / 2;
        LinearLayout.LayoutParams lp2 = (LinearLayout.LayoutParams) mView.findViewById(R.id.llAttention).getLayoutParams();
        lp2.leftMargin = hDivWidth / 2;
        lp2.rightMargin = hDivWidth;


        recentContactsFragment = new RecentContactsFragment();
        recentContactsFragment.viewScene = RecentContactsFragment.ViewScene.MainMsgTab;
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.flRecentContact, recentContactsFragment).commitAllowingStateLoss();

        llMenu = mView.findViewById(R.id.llMenu);
        llUnraedClear = mView.findViewById(R.id.llUnraedClear);
        llRingSwitch = mView.findViewById(R.id.llRingSwitch);
        ivRingSwitch = mView.findViewById(R.id.ivRingSwitch);
        tvRingSwitch = mView.findViewById(R.id.tvRingSwitch);
        llPrivateSwitch = mView.findViewById(R.id.llPrivateSwitch);
        ivPrivateSwitch = mView.findViewById(R.id.ivPrivateSwitch);
        tvPrivateSwitch = mView.findViewById(R.id.tvPrivateSwitch);
    }

    @Override
    public void onSetListener() {
        LogUtils.d(TAG, "onSetListener");
        mView.findViewById(R.id.ivMsgMenu).setOnClickListener(this);
        mView.findViewById(R.id.rlFriends).setOnClickListener(this);
        mView.findViewById(R.id.rlLikes).setOnClickListener(this);
        mView.findViewById(R.id.llAttention).setOnClickListener(this);
        mView.findViewById(R.id.rlFans).setOnClickListener(this);
        llUnraedClear.setOnClickListener(this);
        llRingSwitch.setOnClickListener(this);
        llPrivateSwitch.setOnClickListener(this);

        //失效
        llMenu.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    LogUtils.d(TAG, "onTouch x:" + event.getX() + " left:" + llMenu.getLeft());
                    LogUtils.d(TAG, "onTouch x:" + event.getX() + " left:" + llMenu.getRight());
                    LogUtils.d(TAG, "onTouch y:" + event.getY() + " top:" + llMenu.getTop());
                    LogUtils.d(TAG, "onTouch y:" + event.getY() + " bottom:" + llMenu.getBottom());
                    if (!(event.getX() > llMenu.getLeft()
                            && event.getX() < llMenu.getRight()
                            && event.getY() > llMenu.getTop()
                            && event.getY() < llMenu.getBottom())) {
                        llMenu.setVisibility(View.GONE);
                    }
                }
                return false;
            }
        });

        recentContactsCallback = new RecentContactsCallback() {
            @Override
            public void onRecentContactsLoaded() {

            }

            @Override
            public void onUnreadCountChange(int unreadCount) {

            }

            @Override
            public void onItemClick(RecentContact recent) {
                if (recent.getSessionType() == SessionTypeEnum.Team) {
                    NimUIKit.startTeamSession(getActivity(), recent.getContactId());
                } else if (recent.getSessionType() == SessionTypeEnum.P2P) {
                    if (OfficSecrRedPointCountManager.isOfficMsgAccount(recent.getContactId())) {
                        NimUIKit.startP2PSession(getActivity(), recent.getContactId());
                        startActivity(new Intent(getActivity(), SysMsgActivity.class));
                        OfficSecrRedPointCountManager.sysMsgNum = 0;
                    } else {
                        NimUIKit.startP2PSession(getActivity(), recent.getContactId());
                    }
                }
                CoreManager.getCore(VersionsCore.class).requestSensitiveWord();
            }

            @Override
            public String getDigestOfAttachment(RecentContact recent, MsgAttachment attachment) {
                if (attachment instanceof CustomAttachment) {
                    CustomAttachment customAttachment = (CustomAttachment) attachment;
                    if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_OPEN_ROOM_NOTI) {
                        return "您关注的TA上线啦，快去围观吧~~~";
                    } else if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_GIFT) {
                        return "[礼物]";
                    } else if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_NOTICE) {
                        NoticeAttachment noticeAttachment = (NoticeAttachment) attachment;
                        return noticeAttachment.getTitle();
                    } else if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_PACKET) {
                        RedPacketAttachment redPacketAttachment = (RedPacketAttachment) attachment;
                        RedPacketInfoV2 redPacketInfo = redPacketAttachment.getRedPacketInfo();
                        if (redPacketInfo == null) {
                            return "您收到一个红包哦!";
                        }
                        return "您收到一个" + redPacketInfo.getPacketName() + "红包哦!";
                    } else if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_LOTTERY) {
                        return "恭喜您，获得抽奖机会";
                    } else if (customAttachment.getFirst() == CUSTOM_MSG_MINI_GAME) {
                        if (customAttachment.getSecond() == CUSTOM_MSG_MINI_GAME_ON_INVITED_NOTIFY) {
                            MiniGameInvitedAttachment miniGameInvitedAttachment = (MiniGameInvitedAttachment) attachment;
                            return miniGameInvitedAttachment.getDataInfo().getFromNick().concat("邀请你进入游戏");
                        }
                    }
                } else if (attachment instanceof AudioAttachment) {
                    return "[语音]";
                }
                return null;
            }

            @Override
            public String getDigestOfTipMsg(RecentContact recent) {
                return null;
            }
        };

        recentContactsFragment.setCallback(recentContactsCallback);
        MsgCenterRedPointStatusManager.getInstance().addListener(this);
    }

    @Override
    public void initiate() {
        LogUtils.d(TAG, "initiate");
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.llUnraedClear:
                ignoreUnreadMsg();
                break;
            case R.id.llRingSwitch: {
                //消息铃声开关
                switchIsRingOnNewMsg();
            }
            break;
            case R.id.llPrivateSwitch:
                //非好友私聊开关
                switchOnlyRecvFriendMsg();
                break;
            case R.id.llAttention:
                //跳转关注列表
                AttentionListActivity.start(getActivity());
                llMenu.setVisibility(View.GONE);
                break;
            case R.id.rlFans:
                //跳转粉丝列表
                FansListActivity.start(getActivity());
                llMenu.setVisibility(View.GONE);
                vNewFansRedPoint.setVisibility(View.GONE);
                break;
            case R.id.rlLikes:
                //跳转点赞列表
                LikeMeListActivity.start(getActivity());
                llMenu.setVisibility(View.GONE);
                vNewLikeRedPoint.setVisibility(View.GONE);
                break;
            case R.id.rlFriends:
                //跳转好友列表
                FriendListActivity.start(getActivity());
                llMenu.setVisibility(View.GONE);
                vNewFriendRedPoint.setVisibility(View.GONE);
                break;
            case R.id.ivMsgMenu:
                //显示菜单
                showOrHiddenMsgMenu();
                break;
            default:
                hideMsgCenterMenu();
                break;
        }

    }

    private void hideMsgCenterMenu() {
        if (llMenu.getVisibility() == View.VISIBLE) {
            llMenu.setVisibility(View.GONE);
        }
    }

    /**
     * 刷新界面菜单按钮状态
     */
    private void showOrHiddenMsgMenu() {
        if (llMenu.getVisibility() == View.GONE) {
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            if (null != userInfo) {
                //更新铃声设置
                boolean ringOpended = userInfo.getMessageRingRestriction() == 1;
                ivRingSwitch.setImageDrawable(getResources().getDrawable(ringOpended ? R.mipmap.ic_msg_center_menu_ring_close : R.mipmap.ic_msg_center_menu_ring_open));
                tvRingSwitch.setText(getResources().getString(ringOpended ? R.string.msg_center_menu_ring_close : R.string.msg_center_menu_ring_open));

                //更新私聊设置
                boolean privateOpended = userInfo.getMessageRestriction() == 1;
                ivPrivateSwitch.setImageDrawable(getResources().getDrawable(privateOpended ? R.mipmap.ic_msg_center_menu_private_close : R.mipmap.ic_msg_center_menu_private_open));
                tvPrivateSwitch.setText(getResources().getString(privateOpended ? R.string.msg_center_menu_private_close : R.string.msg_center_menu_private_open));
            }
            llMenu.setVisibility(View.VISIBLE);
        } else {
            llMenu.setVisibility(View.GONE);
        }

    }

    /**
     * 切换仅接受好友私聊消息开关状态
     */
    private void switchOnlyRecvFriendMsg() {
        UserInfo selfUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (null != selfUserInfo) {
            getDialogManager().showProgressDialog(getActivity(), getResources().getString(R.string.network_loading));
            UserInfo userInfo = new UserInfo();
            userInfo.setMessageRestriction(selfUserInfo.getMessageRestriction() == 1 ? 0 : 1);
            userInfo.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            CoreManager.getCore(IUserCore.class).requestUpdateUserInfo(userInfo);
        }
        llMenu.setVisibility(View.GONE);
    }

    /**
     * 切换新消息铃声提示开关状态
     */
    private void switchIsRingOnNewMsg() {
        UserInfo selfUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (null != selfUserInfo) {
            getDialogManager().showProgressDialog(getActivity(), getResources().getString(R.string.network_loading));
            UserInfo userInfo = new UserInfo();
            userInfo.setMessageRingRestriction(selfUserInfo.getMessageRingRestriction() == 1 ? 0 : 1);
            userInfo.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            CoreManager.getCore(IUserCore.class).requestUpdateUserInfo(userInfo);
        }
        llMenu.setVisibility(View.GONE);
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfoUpdate(UserInfo userInfo) {
        getDialogManager().dismissDialog();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfoUpdateError(String error) {
        toast(error);
        getDialogManager().dismissDialog();
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_msg;
    }

    /**
     * 数据懒加载
     */
    @Override
    protected void onLazyLoadData() {
        LogUtils.d(TAG, "onLazyLoadData");
        getUnreadLikeMsgCount();
    }

    //获取未读点赞消息的数量
    public void getUnreadLikeMsgCount() {

        new LikeModel().getUnreadLikeMsgCount(new OkHttpManager.MyCallBack<ServiceResult<Integer>>() {

            @Override
            public void onError(Exception e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(ServiceResult<Integer> response) {
                if (response != null && response.isSuccess()) {
                    vNewLikeRedPoint.setVisibility(response.getData() > 0 ? View.VISIBLE : View.GONE);
                }
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        LogUtils.d(TAG, "onActivityCreated");
        super.onActivityCreated(savedInstanceState);
        mIsInitView = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        //当前选中的不是联系人列表，说明消息和好友两个tab之间经过切换操作了，不管
        //hidden-true 表示不可见 false表示可见
        LogUtils.d(TAG, "onResume-进入最近联系人列表界面");
        NIMSessionStatusManager.getInstance().updateChattingAccount(MsgService.MSG_CHATTING_ACCOUNT_ALL, SessionTypeEnum.None);
    }

    @Override
    public void onPause() {
        super.onPause();
        LogUtils.d(TAG, "onPause-退出聊天界面或离开最近联系人列表界面");
        NIMSessionStatusManager.getInstance().updateChattingAccount(MsgService.MSG_CHATTING_ACCOUNT_NONE, SessionTypeEnum.None);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        LogUtils.d(TAG, "setUserVisibleHint-isVisibleToUser:" + isVisibleToUser + " mIsInitView:" + mIsInitView);
        if (!mIsInitView) {
            return;
        }
        if (isVisibleToUser) {
            LogUtils.d(TAG, "setUserVisibleHint-进入最近联系人列表界面");
            NIMSessionStatusManager.getInstance().updateChattingAccount(MsgService.MSG_CHATTING_ACCOUNT_ALL, SessionTypeEnum.None);
        } else {
            LogUtils.d(TAG, "setUserVisibleHint-退出聊天界面或离开最近联系人列表界面");
            hideMsgCenterMenu();
            NIMSessionStatusManager.getInstance().updateChattingAccount(MsgService.MSG_CHATTING_ACCOUNT_NONE, SessionTypeEnum.None);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        LogUtils.d(TAG, "onDestroyView");
        mIsInitView = false;
        MsgCenterRedPointStatusManager.getInstance().removeListener(this);
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo userInfo) {
        recentContactsFragment.requestMessages(true);
    }

    /**
     * 忽略未读消息（非全部已读）
     */
    public void ignoreUnreadMsg() {
        getDialogManager().showProgressDialog(getActivity(), getResources().getString(R.string.network_loading));
        if (recentContactsFragment != null) {
            recentContactsFragment.ignoreUnreadMsg();
        }

        //更新房间私聊消息未读红点状态及主界面底部消息tab的红点未读数
        CoreManager.notifyClients(IIMMessageCoreClient.class, IIMMessageCoreClient.METHOD_ON_RECEIVE_CONTACT_CHANGED);
        llMenu.setVisibility(View.GONE);

        //设置点赞列表，所有未读状态改为已读
        new LikeModel().setLikeMsgListRead(null);
        vNewLikeRedPoint.setVisibility(View.GONE);
        getDialogManager().dismissDialog();
    }

    /**
     * 显示新粉丝红点
     */
    @Override
    public void showRedPointOnNewFan() {
        vNewFansRedPoint.setVisibility(View.VISIBLE);
    }

    /**
     * 显示新好友红点
     */
    @Override
    public void showRedPointOnNewFriend() {
        vNewFriendRedPoint.setVisibility(View.VISIBLE);
    }

    /**
     * 显示新点赞红点
     */
    @Override
    public void showRedPointOnNewLike() {
        vNewLikeRedPoint.setVisibility(View.VISIBLE);
    }

    /**
     * 签到红点
     */
    @Override
    public void showRedPointOnSignIn() {

    }

}