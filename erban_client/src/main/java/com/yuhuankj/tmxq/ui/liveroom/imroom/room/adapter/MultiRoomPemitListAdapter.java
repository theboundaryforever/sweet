package com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter;

import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.MultiRoomPermitInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

/**
 * oppo市场，首页，单人音频直播间列表，adapter
 *
 * @author weihaitao
 * @date 2019年5月21日 17:18:56
 */
public class MultiRoomPemitListAdapter extends BaseQuickAdapter<MultiRoomPermitInfo, BaseViewHolder> {

    private final String TAG = MultiRoomPemitListAdapter.class.getSimpleName();

    private int imvIconWidthAndHeight = 0;

    public MultiRoomPemitListAdapter(List<MultiRoomPermitInfo> data) {
        super(R.layout.item_room_auth_list, data);
        imvIconWidthAndHeight = DisplayUtility.dp2px(mContext, 56);
    }

    @Override
    protected void convert(BaseViewHolder helper, MultiRoomPermitInfo item) {
        helper.addOnClickListener(R.id.rlRoomAuth);
        ImageView ivRoomAuth = helper.getView(R.id.ivRoomAuth);
        if (GlideContextCheckUtil.checkContextUsable(mContext) && !TextUtils.isEmpty(item.getPermitUrl())) {
            GlideApp.with(mContext).load(ImageLoadUtils.toThumbnailUrl(imvIconWidthAndHeight,
                    imvIconWidthAndHeight, item.getPermitUrl()))
                    .dontAnimate()
                    .error(R.drawable.bg_default_cover_round_placehold_size60)
                    .into(ivRoomAuth);
        }

        TextView tvRoomAuth = helper.getView(R.id.tvRoomAuth);
        tvRoomAuth.setText(item.getPermitName());
    }
}