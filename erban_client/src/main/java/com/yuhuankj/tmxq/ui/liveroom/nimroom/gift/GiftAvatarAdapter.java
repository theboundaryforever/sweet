package com.yuhuankj.tmxq.ui.liveroom.nimroom.gift;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.me.noble.NobleBusinessManager;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.widget.CircleImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chenran on 2017/10/25.
 */

public class GiftAvatarAdapter extends RecyclerView.Adapter<GiftAvatarAdapter.ViewHolder> implements View.OnClickListener {

    private final String TAG = GiftAvatarAdapter.class.getSimpleName();

    private List<MicMemberInfo> micMemberInfos = new ArrayList<>();
    private List<MicMemberInfo> selectedMemberInfoMap = new ArrayList<>();
    private Context context;
    private int selectIndex = 1;
    private OnItemSelectedListener onItemSelectedListener;
    public boolean justRoomOwner = false;

    private boolean isSendGiftToPersonal = false;

    public void setSendGiftToPersonal(boolean sendGiftToPersonal) {
        isSendGiftToPersonal = sendGiftToPersonal;
    }

    public List<MicMemberInfo> getSelectedMemberInfoMap(){
        return selectedMemberInfoMap;
    }

    public void setOnItemSelectedListener(OnItemSelectedListener onItemSelectedListener) {
        this.onItemSelectedListener = onItemSelectedListener;
    }

    public GiftAvatarAdapter(Context context) {
        this.context = context;
    }

    public void setMicMemberInfos(List<MicMemberInfo> micMemberInfos) {
        this.micMemberInfos = micMemberInfos;
    }

    public List<MicMemberInfo> getMicMemberInfos() {
        return micMemberInfos;
    }

    public void setSelectIndex(int selectIndex) {
        this.selectIndex = selectIndex;
        LogUtils.d(TAG,"setSelectIndex-selectIndex:"+ selectIndex);
        selectedMemberInfoMap.add(micMemberInfos.get(selectIndex));
    }

    public int getSelectIndex() {
        return selectIndex;
    }

    @Override
    public GiftAvatarAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.list_item_gift_avatar, parent, false);
        return new GiftAvatarAdapter.ViewHolder(item);
    }

    @Override
    public void onBindViewHolder(GiftAvatarAdapter.ViewHolder holder, int position) {
        holder.avatarContainer.setTag(position);
        setupItem(holder, position);
    }

    private void setupItem(ViewHolder holder, int position) {
        final MicMemberInfo micMemberInfo = micMemberInfos.get(position);
        String avatarUrl = null;
        RoomInfo current = RoomServiceScheduler.getCurrentRoomInfo();
        if (null != current && current.getUid() == micMemberInfo.getUid()) {
            avatarUrl = ImageLoadUtils.toThumbnailUrl(60, 60, micMemberInfo.getAvatar());
        } else {
            avatarUrl = ImageLoadUtils.toThumbnailUrl(60, 60, NobleBusinessManager.getNobleRoomAvatarUrl(
                    micMemberInfo.getVipId(), micMemberInfo.isInvisible(), micMemberInfo.getVipDate(), micMemberInfo.getAvatar()));
        }
        ImageLoadUtils.loadAvatar(holder.avatar.getContext(), avatarUrl, holder.avatar);
        if(micMemberInfo.getMicPosition() == -1){
            holder.nickName.setText(context.getResources().getString(R.string.gift_recv_owner));
            holder.nickName.setVisibility(View.VISIBLE);
        }else if(micMemberInfo.getMicPosition() == -2){
            holder.nickName.setText(null);
            holder.nickName.setVisibility(View.GONE);
        }else{
            holder.nickName.setVisibility(View.VISIBLE);
            holder.nickName.setText(context.getResources().getString(R.string.gift_recv_mic,micMemberInfo.getMicPosition()+1));
        }

        boolean isChecked = selectedMemberInfoMap.contains(micMemberInfo);
        holder.iv_checkStatus.setVisibility(isChecked ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public int getItemCount() {
        return micMemberInfos == null ? 0 : micMemberInfos.size();
    }

    @Override
    public void onClick(View v) {
        Integer position = (Integer) v.getTag();
        LogUtils.d(TAG,"onClick-position:"+position);
        MicMemberInfo micMemberInfo = micMemberInfos.get(position);
        if(selectedMemberInfoMap.contains(micMemberInfo)){
            selectedMemberInfoMap.remove(micMemberInfo);
        }else{
            selectedMemberInfoMap.add(micMemberInfo);
        }
        selectIndex = position;
        notifyDataSetChanged();
        if (onItemSelectedListener != null) {
            onItemSelectedListener.onItemSelected(position);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private View itemView;
        private CircleImageView avatar;
        private TextView nickName;
        private FrameLayout avatarContainer;
        private ImageView iv_checkStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            avatar = itemView.findViewById(R.id.avatar);
            nickName = itemView.findViewById(R.id.user_name);
            iv_checkStatus = itemView.findViewById(R.id.iv_checkStatus);
            avatarContainer = itemView.findViewById(R.id.avatar_container);
            avatarContainer.setOnClickListener(GiftAvatarAdapter.this);
            this.itemView = itemView;
        }
    }

    public interface OnItemSelectedListener {
        void onItemSelected(int position);
    }

    public void updateAllMicSelectedStatus(boolean isSendAllMic){
        LogUtils.d(TAG,"updateAllMicSelectedStatus-isSendAllMic:"+isSendAllMic);
        selectedMemberInfoMap.clear();
        if(isSendAllMic && null != micMemberInfos){
            selectedMemberInfoMap.addAll(micMemberInfos);
        }
        notifyDataSetChanged();
    }
}
