package com.yuhuankj.tmxq.ui.room.adapter;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.xchat_core.home.HomeRoom;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

/**
 * @author liaoxy
 * @Description:首页其它房间列表的adapter
 * @date 2019/3/5 14:23
 */
public class NotHotProAdapter extends BaseQuickAdapter<HomeRoom, BaseViewHolder> {

    private int roomLevelIconWidth = 0;
    private int roomLevelIconHeight = 0;

    public NotHotProAdapter(List<HomeRoom> datas) {
        super(R.layout.item_room_new, datas);
        roomLevelIconWidth = DisplayUtility.dp2px(mContext, 24);
        roomLevelIconHeight = DisplayUtility.dp2px(mContext, 12);
    }

    @Override
    protected void convert(BaseViewHolder helper, HomeRoom item) {
        ImageView imvIcon = helper.getView(R.id.imvIcon);
        TextView tvRoomName = helper.getView(R.id.tvRoomName);
        TextView tvOnLineCount = helper.getView(R.id.tvOnLineCount);
        TextView tvRoomInfo = helper.getView(R.id.tvRoomInfo);
        ImageView imvTag = helper.getView(R.id.imvTag);
        ImageView imvRunning = helper.getView(R.id.imvRunning);
        ImageView imvRoomBorder = helper.getView(R.id.imvRoomBorder);

        if (GlideContextCheckUtil.checkContextUsable(mContext)) {
            ImageLoadUtils.loadBannerRoundBg(mContext, item.getAvatar(), imvIcon, DisplayUtility.dp2px(mContext, 8));
        }
        if (!TextUtils.isEmpty(item.getRoomFrame())) {
            ImageLoadUtils.loadImage(mContext, item.getRoomFrame(), imvRoomBorder);
            imvRoomBorder.setVisibility(View.VISIBLE);
        } else {
            imvRoomBorder.setVisibility(View.GONE);
        }
        tvRoomName.setText(item.getTitle());
        tvOnLineCount.setText(item.getOnlineNum() + "");
        tvRoomInfo.setText(item.getRoomTag());
        ImageView imvRoomLevel = helper.getView(R.id.imvRoomLevel);
        imvRoomLevel.setVisibility(TextUtils.isEmpty(item.getRoomLevelIcon()) ? View.GONE : View.VISIBLE);

        if (GlideContextCheckUtil.checkContextUsable(mContext)) {
            ImageLoadUtils.loadImage(mContext, item.badge, imvTag);
            GlideApp.with(mContext).asGif().load(R.drawable.anim_game_home_room_running).diskCacheStrategy(DiskCacheStrategy.ALL).into(imvRunning);

            if (!TextUtils.isEmpty(item.getRoomLevelIcon())) {
                ImageLoadUtils.loadImage(mContext, ImageLoadUtils.toThumbnailUrl(roomLevelIconWidth, roomLevelIconHeight, item.getRoomLevelIcon()), imvRoomLevel);
            }
        }


    }
}
