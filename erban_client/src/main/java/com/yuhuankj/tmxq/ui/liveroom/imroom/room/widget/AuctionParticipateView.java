package com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.noober.background.view.BLTextView;
import com.yuhuankj.tmxq.R;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/7/29
 */
public class AuctionParticipateView extends RelativeLayout{
    private BLTextView bltvCount;

    public AuctionParticipateView(Context context) {
        this(context,null);
    }

    public AuctionParticipateView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    private void initView(Context context) {
        inflate(context,R.layout.view_auction_participate,this);
        bltvCount = findViewById(R.id.bltv_auction_participate_msg);
    }

    /**
     * 更新参加竞拍人数
     * @param count
     */
    public void updateCount(int count){
        if (count > 0){
            if (bltvCount.getVisibility() == View.GONE){
                bltvCount.setVisibility(View.VISIBLE);
            }
            bltvCount.setText(String.valueOf(count));
        }else {
            if (bltvCount.getVisibility() == View.VISIBLE){
                bltvCount.setVisibility(View.GONE);
            }
            bltvCount.setText(String.valueOf(0));
        }
    }

}
