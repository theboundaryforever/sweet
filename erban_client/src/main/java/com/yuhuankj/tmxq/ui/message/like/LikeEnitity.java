package com.yuhuankj.tmxq.ui.message.like;

import java.io.Serializable;

public class LikeEnitity implements Serializable {

    private String admireText;//": "Ta在偶遇给你点的赞哦～",
    private long admireUid;//": 92000633,
    private String avatar;//": "https://img.pinjin88.com/FgGrMzb9W_sB21ny_Pk73KeI3Qy-?imageslim",
    private long createDate;//": 1555496599000,
    private int id;//": 284,
    private int isRead;//": 0,
    private String nick;//": "hsjjdjdjdjjdjdj",
    private int type;//": 1,
    private long uid;//": 92000646

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LikeEnitity that = (LikeEnitity) o;

        if (id != that.id) return false;
        return uid == that.uid;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (int) (uid ^ (uid >>> 32));
        return result;
    }

    public String getAdmireText() {
        return admireText;
    }

    public void setAdmireText(String admireText) {
        this.admireText = admireText;
    }

    public long getAdmireUid() {
        return admireUid;
    }

    public void setAdmireUid(long admireUid) {
        this.admireUid = admireUid;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIsRead() {
        return isRead;
    }

    public void setIsRead(int isRead) {
        this.isRead = isRead;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }
}
