package com.yuhuankj.tmxq.ui.liveroom.imroom.gift.adapter;

import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.me.noble.NobleBusinessManager;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

/**
 * 文件描述：礼物的用户头像列表
 *
 * @auther：zwk
 * @data：2019/5/17
 */
public class GiftTopAvatarAdapter extends BaseQuickAdapter<MicMemberInfo, BaseViewHolder> {
    private OnCancelAllMicSelectListener onCancelAllMicSelectListener;
    private int selectCount = 0;

    public void setAllSelect(boolean isAllMic) {
        this.selectCount = isAllMic ? getItemCount() : 0;
    }

    public GiftTopAvatarAdapter() {
        super(R.layout.item_rv_gift_top_avatar);
    }

    @Override
    protected void convert(BaseViewHolder helper, MicMemberInfo item) {
        RoomInfo current = RoomServiceScheduler.getCurrentRoomInfo();
        String avatarUrl = "";
        if (null != current && current.getUid() == item.getUid()) {
            avatarUrl = ImageLoadUtils.toThumbnailUrl(60, 60, item.getAvatar());
        } else {
            avatarUrl = ImageLoadUtils.toThumbnailUrl(60, 60, NobleBusinessManager.getNobleRoomAvatarUrl(
                    item.getVipId(), item.isInvisible(), item.getVipDate(), item.getAvatar()));
        }
        ImageLoadUtils.loadAvatar(mContext, avatarUrl, helper.getView(R.id.civ_user_avatar));
        TextView tvMic = helper.getView(R.id.tv_mic_position);
        if (item.getMicPosition() == -2){
            tvMic.setVisibility(View.GONE);
        }else {
            tvMic.setVisibility(View.VISIBLE);
            tvMic.setText(item.getMicPosition() == -1 ? "房主" : (String.valueOf(item.getMicPosition() + 1).concat("麦")));
        }
        helper.setGone(R.id.iv_check_status, item.isSelect());
        helper.getView(R.id.rl_gift_avatar_container).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.isSelect()) {
                    item.setSelect(false);
                    selectCount--;
                } else {
                    item.setSelect(true);
                    selectCount++;
                }
                if (selectCount == getItemCount()) {//选中之后如果满人数则全选
                    if (onCancelAllMicSelectListener != null)
                        onCancelAllMicSelectListener.onAllMicSelectChange(true);
                } else {
                    if (onCancelAllMicSelectListener != null)//选中之前如果满人数则取消全选
                        onCancelAllMicSelectListener.onAllMicSelectChange(false);
                }
                notifyDataSetChanged();
            }
        });
    }

    public interface OnCancelAllMicSelectListener {
        void onAllMicSelectChange(boolean isAllMic);
    }

    public void setOnCancelAllMicSelectListener(OnCancelAllMicSelectListener onCancelAllMicSelectListener) {
        this.onCancelAllMicSelectListener = onCancelAllMicSelectListener;
    }

    public void setSelectCount(int selectCount) {
        this.selectCount = selectCount;
    }
}
