package com.yuhuankj.tmxq.ui.liveroom.imroom.room.dialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.liveroom.im.model.AudioConnTimeCounter;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.AudioConnReqInfo;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomEvent;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.AppDialogConstant;
import com.yuhuankj.tmxq.base.dialog.BaseAppMvpDialogFragment;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.activity.EditReqAudioConnReasonActivity;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.activity.PayReqAudioConnActivity;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter.AudioConnListAdapter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter.AudioConnectListPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IAudioConnectView;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.widget.ButtonItemFactory;
import com.yuhuankj.tmxq.ui.widget.DividerItemDecoration;
import com.yuhuankj.tmxq.ui.widget.UserInfoDialog;

import java.util.ArrayList;
import java.util.List;

/**
 * 语音连线列表
 * 内部区分是主播还是粉丝操作
 *
 * @author weihaitao
 * @date 22019年7月15日 16:00:24
 */
@CreatePresenter(AudioConnectListPresenter.class)
public class AudioConnectListDialog extends BaseAppMvpDialogFragment<IAudioConnectView, AudioConnectListPresenter<IAudioConnectView>>
        implements View.OnClickListener, IAudioConnectView {

    private static final String TAG = AudioConnectListDialog.class.getSimpleName();
    private RelativeLayout rlContent;
    private SwipeRefreshLayout srlRefresh;
    private boolean isRequesting = false;
    private TextView tvRecvConnCount;
    private RecyclerView rvConnList;
    private View llEmpty;
    private TextView bltvReqConn;
    private TextView bltvNoReqConn;
    private TextView bltvEditConnReq;
    private TextView bltvCannelConnReq;
    private TextView bltvCallDownConn;
    private TextView tvConnListTab;
    private TextView tvTabReq;
    private TextView tvTabAll;
    private TextView tvConnTabTips;
    private TextView tvConnSwitch;
    private View llBottomOpera;
    private View rlFansTop;
    private View rlAnchorTop;
    private View blllTabSwitch;
    private TextView tvEmptyTips;
    private AudioConnListAdapter audioConnListAdapter;

    private boolean isAnchor;

    /**
     * 提供dialog的样式：
     * 目前提供四种：0.中间显示背景不变暗  1.底部显示背景不变暗 2.中间显示背景变暗  3.底部显示背景变暗
     *
     * @return
     */
    @Override
    protected int getDialogStyle() {
        return AppDialogConstant.STYLE_BOTTOM;
    }

    /**
     * 提供dialog对应的布局
     *
     * @return
     */
    @Override
    protected int getLayoutId() {
        return R.layout.layout_audio_conn_list_dialog;
    }

    /**
     * 初始化view
     *
     * @param view
     */
    @Override
    protected void initView(View view) {
        rlContent = view.findViewById(R.id.rl_content);
        tvRecvConnCount = view.findViewById(R.id.tvRecvConnCount);
        rvConnList = view.findViewById(R.id.rvConnList);
        srlRefresh = view.findViewById(R.id.srlRefresh);
        llEmpty = view.findViewById(R.id.llEmpty);
        bltvReqConn = view.findViewById(R.id.bltvReqConn);
        bltvNoReqConn = view.findViewById(R.id.bltvNoReqConn);
        bltvEditConnReq = view.findViewById(R.id.bltvEditConnReq);
        bltvCannelConnReq = view.findViewById(R.id.bltvCannelConnReq);
        bltvCallDownConn = view.findViewById(R.id.bltvCallDownConn);
        llBottomOpera = view.findViewById(R.id.llBottomOpera);
        rlFansTop = view.findViewById(R.id.rlFansTop);
        rlAnchorTop = view.findViewById(R.id.rlAnchorTop);
        tvConnListTab = view.findViewById(R.id.tvConnListTab);
        blllTabSwitch = view.findViewById(R.id.blllTabSwitch);
        tvTabReq = view.findViewById(R.id.tvTabReq);
        tvTabAll = view.findViewById(R.id.tvTabAll);
        tvConnTabTips = view.findViewById(R.id.tvConnTabTips);
        tvConnSwitch = view.findViewById(R.id.tvConnSwitch);
        tvEmptyTips = view.findViewById(R.id.tvEmptyTips);
        refreshAudioReqConnCount(0L);
        audioConnListAdapter = new AudioConnListAdapter();
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(OrientationHelper.VERTICAL,
                Color.parseColor("#F5F5F5"), getActivity(), DisplayUtility.dp2px(getActivity(), 1));
        rvConnList.addItemDecoration(dividerItemDecoration);
        rvConnList.setAdapter(audioConnListAdapter);
    }

    public void refreshAudioReqConnCount(long count) {
        if (null == getActivity()) {
            return;
        }

        String tips = "";
        if (tvConnListTab.getText().toString().equals(getActivity().getResources().getString(R.string.audio_connect_tab_req))) {
            tips = getActivity().getResources().getString(R.string.audio_connect_tab_req_tips, String.valueOf(count));
        } else {
            tips = getActivity().getResources().getString(R.string.audio_connect_tab_all_tips, String.valueOf(count));
        }
        SpannableStringBuilder ssb = new SpannableStringBuilder(tips);
        int firstIndex = tips.indexOf(String.valueOf(count));
        ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#e6333333")), 0, firstIndex, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#1CC9A4")),
                firstIndex, firstIndex + String.valueOf(count).length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#e6333333")),
                firstIndex + String.valueOf(count).length(), tips.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        tvConnTabTips.setText(ssb);
    }

    /**
     * 添加事件监听
     *
     * @param view
     */
    @Override
    protected void initListener(View view) {
        bltvReqConn.setOnClickListener(this);
        bltvEditConnReq.setOnClickListener(this);
        bltvCannelConnReq.setOnClickListener(this);
        bltvCallDownConn.setOnClickListener(this);
        tvConnSwitch.setOnClickListener(this);
        tvConnListTab.setOnClickListener(this);
        tvTabReq.setOnClickListener(this);
        tvTabAll.setOnClickListener(this);
        audioConnListAdapter.setOnAudioConnItemClickListener(new AudioConnListAdapter.OnAudioConnItemClickListener() {
            @Override
            public void onRecvConnReqClick(long targetUid) {
                if (null == getActivity()) {
                    return;
                }
                getDialogManager().showProgressDialog(getActivity(), getActivity().getResources().getString(R.string.network_loading));
                //连接申请
                getMvpPresenter().connAudioReq(targetUid);
            }

            @Override
            public void onCallDownConnClick(long targetUid) {
                if (null == getActivity()) {
                    return;
                }
                getDialogManager().showProgressDialog(getActivity(), getActivity().getResources().getString(R.string.network_loading));
                //挂断连接
                getMvpPresenter().callDownAudioConn(targetUid);
            }

            @Override
            public void onShowUserInfoDialogClick(long targetUid) {
                //显示个人资料弹框
                if (null != getActivity()) {
                    new UserInfoDialog(getActivity(), targetUid).show();
                }
            }
        });
        IMRoomMessageManager.get().subscribeIMRoomEventObservable(this::receiveRoomEvent, this);
        srlRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                if (isRequesting) {
                    srlRefresh.setRefreshing(false);
                } else {
                    getAudioConnReqList(false);
                }
            }
        });
    }

    public void getAudioConnReqList(boolean showLoading) {
        if (isRequesting) {
            return;
        }
        isRequesting = true;
        if (showLoading) {
            getDialogManager().showProgressDialog(getActivity(), getString(R.string.network_loading));
        }
        getMvpPresenter().getAudioConnReqList();
    }

    /**
     * 根据初始数据改变view的样式
     */
    @Override
    protected void initViewState() {
        //保持默认值
        isAnchor = RoomDataManager.get().isRoomOwner();

        llEmpty.setVisibility(View.VISIBLE);
        tvEmptyTips.setText(getActivity().getResources().getString(
                isAnchor ? R.string.audio_connect_list_empty_tips_anchor : R.string.audio_connect_list_empty_tips_fans));

        audioConnListAdapter.setAnchor(isAnchor);
        audioConnListAdapter.setSelfUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());

        rlAnchorTop.setVisibility(isAnchor ? View.VISIBLE : View.GONE);
        rlFansTop.setVisibility(isAnchor ? View.GONE : View.VISIBLE);

        RelativeLayout.LayoutParams srlRefreshLp = (RelativeLayout.LayoutParams) srlRefresh.getLayoutParams();
        if (null != srlRefreshLp) {
            srlRefreshLp.addRule(RelativeLayout.BELOW, isAnchor ? R.id.rlAnchorTop : R.id.rlFansTop);
            srlRefresh.setLayoutParams(srlRefreshLp);
        }

        llBottomOpera.setVisibility(isAnchor ? View.GONE : View.VISIBLE);
        refreshConnSwitchStatus();

        bltvReqConn.setVisibility(View.GONE);
        bltvNoReqConn.setVisibility(View.VISIBLE);
        bltvEditConnReq.setVisibility(View.GONE);
        bltvCannelConnReq.setVisibility(View.GONE);
        bltvCallDownConn.setVisibility(View.GONE);
        blllTabSwitch.setVisibility(View.GONE);
        getAudioConnReqList(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvConnListTab:
                boolean isShowing = blllTabSwitch.getVisibility() == View.VISIBLE;
                blllTabSwitch.setVisibility(isShowing ? View.GONE : View.VISIBLE);
                tvConnListTab.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                        isShowing ? R.drawable.ic_audio_conn_indic_down : R.drawable.ic_audio_conn_indic_up, 0);
                break;
            case R.id.tvTabReq:
                displayReqAndConnListOlny();
                break;
            case R.id.tvTabAll:
                displayAllAudioConnReqList();
                break;
            case R.id.tvConnSwitch:
                getDialogManager().showProgressDialog(getActivity(), getString(R.string.network_loading));
                getMvpPresenter().changeAudioConnSwitch();
                break;
            case R.id.bltvReqConn:
                reqAudioConn();
                break;
            case R.id.bltvEditConnReq:
                //用户编辑连接申请理由
                editAudioConnReq();
                break;
            case R.id.bltvCannelConnReq:
                //用户取消自己的连接
                cancelAudioConnReq();
                break;
            case R.id.bltvCallDownConn:
                //用户挂断自己的连接
                callDownAudioConn();
                break;
            default:
                break;
        }
    }

    /**
     * 仅显示连线中和等待中的记录
     */
    private void displayReqAndConnListOlny() {
        tvTabReq.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                R.drawable.ic_audio_conn_list_tab_selected,
                0);
        tvTabAll.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        if (null != getActivity()) {
            tvConnListTab.setText(getActivity().getString(R.string.audio_connect_tab_req));
        }
        blllTabSwitch.setVisibility(View.GONE);
        tvConnListTab.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                R.drawable.ic_audio_conn_indic_down, 0);
        refreshAudioConnReqList();
        refreshAudioReqConnCount(getMvpPresenter().getReqingCount());
    }

    /**
     * 显示所有连线记录
     */
    private void displayAllAudioConnReqList() {
        tvTabAll.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                R.drawable.ic_audio_conn_list_tab_selected,
                0);
        tvTabReq.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        if (null != getActivity()) {
            tvConnListTab.setText(getActivity().getString(R.string.audio_connect_tab_all));
        }
        blllTabSwitch.setVisibility(View.GONE);
        tvConnListTab.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                R.drawable.ic_audio_conn_indic_down, 0);
        refreshAudioConnReqList();
        refreshAudioReqConnCount(getMvpPresenter().getReqingCount() + getMvpPresenter().getConningCount() + getMvpPresenter().getOverCount());
    }

    private void editAudioConnReq() {
        LogUtils.d(TAG, "editAudioConnReq");
        if (null == getActivity()) {
            return;
        }
        EditReqAudioConnReasonActivity.startForResult(getActivity(),
                EditReqAudioConnReasonActivity.EDIT_REQ_AUDIO_CONN_REASON_REQUEST_CODE,
                getMvpPresenter().getLastReason());
    }

    private void callDownAudioConn() {
        LogUtils.d(TAG, "callDownAudioConn");
        if (null == getActivity()) {
            return;
        }
        getDialogManager().showOkCancelDialog(
                getActivity().getResources().getString(R.string.audio_connect_req_calldown_tips),
                getActivity().getResources().getString(R.string.sure),
                getActivity().getResources().getString(R.string.cancel), new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onOk() {
                        if (null == getActivity()) {
                            return;
                        }
                        getDialogManager().showProgressDialog(getActivity(), getString(R.string.network_loading));
                        getMvpPresenter().callDownAudioConn();
                    }
                });
    }

    private void cancelAudioConnReq() {
        LogUtils.d(TAG, "cancelAudioConnReq");
        if (null == getActivity()) {
            return;
        }
        getDialogManager().showOkCancelDialog(
                getActivity().getResources().getString(R.string.audio_connect_req_cancel_tips),
                getActivity().getResources().getString(R.string.audio_connect_req_cancel_ok),
                getActivity().getResources().getString(R.string.audio_connect_req_cancel_cancel), new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onOk() {
                        if (null == getActivity()) {
                            return;
                        }
                        getDialogManager().showProgressDialog(getActivity(), getString(R.string.network_loading));
                        getMvpPresenter().cancelAudioConnReq();
                    }
                });
    }

    private void reqAudioConn() {
        LogUtils.d(TAG, "reqAudioConn");
        if (null == getActivity()) {
            return;
        }
        if (!RoomDataManager.get().singleAudioAnchorConnSwitch) {
            toast(getActivity().getResources().getString(R.string.audio_connect_req_tips2));
            return;
        }
        //如果在麦上，则直接提示
        if (RoomDataManager.get().isOnMic(CoreManager.getCore(IAuthCore.class).getCurrentUid())) {
            toast(getActivity().getResources().getString(R.string.audio_connect_req_tips3));
            return;
        }
        //用户申请连接
        List<ButtonItem> buttonItems = new ArrayList<>();
        ButtonItem freeReqItem = ButtonItemFactory.createMsgBlackListItem(getActivity().getString(R.string.audio_connect_req_free),
                new ButtonItemFactory.OnItemClick() {
                    @Override
                    public void itemClick() {
                        if (null == getActivity()) {
                            return;
                        }
                        //免费申请
                        getDialogManager().showProgressDialog(getActivity(), getString(R.string.network_loading));
                        getMvpPresenter().reqAudioConn(getActivity().getResources().getString(R.string.audio_connect_req_reason_default), 0);
                    }
                });
        buttonItems.add(freeReqItem);
        ButtonItem payReqItem = ButtonItemFactory.createMsgBlackListItem(getActivity().getString(R.string.audio_connect_req_pay),
                new ButtonItemFactory.OnItemClick() {
                    @Override
                    public void itemClick() {
                        //付费申请
                        if (null == getActivity()) {
                            return;
                        }
                        PayReqAudioConnActivity.startForResult(getActivity(), PayReqAudioConnActivity.PAY_REQ_AUDIO_CONN_REQUEST_CODE);
                    }
                });
        buttonItems.add(payReqItem);
        getDialogManager().showCommonPopupDialog(buttonItems, getActivity().getString(R.string.cancel));
    }

    @Override
    public void refreshAudioConnReqList() {
        LogUtils.d(TAG, "refreshAudioConnReqList-isAnchor:" + isAnchor);
        srlRefresh.setRefreshing(false);
        isRequesting = false;
        if (null == getActivity()) {
            return;
        }
        getDialogManager().dismissDialog();
        if (isAnchor) {
            //主播视角
            if (tvConnListTab.getText().toString().equals(getActivity().getResources().getString(R.string.audio_connect_tab_req))) {
                //等待连线
                if (!ListUtils.isListEmpty(getMvpPresenter().getAudioConnReqInfoReqList())) {
                    audioConnListAdapter.replaceData(getMvpPresenter().getAudioConnReqInfoReqList());
                    rvConnList.setVisibility(View.VISIBLE);
                    llEmpty.setVisibility(View.GONE);
                } else {
                    audioConnListAdapter.replaceData(new ArrayList<>());
                    rvConnList.setVisibility(View.GONE);
                    llEmpty.setVisibility(View.VISIBLE);
                }
                refreshAudioReqConnCount(getMvpPresenter().getReqingCount());
            } else {
                //全部连线
                if (!ListUtils.isListEmpty(getMvpPresenter().getAudioConnReqInfoAllList())) {
                    audioConnListAdapter.replaceData(getMvpPresenter().getAudioConnReqInfoAllList());
                    rvConnList.setVisibility(View.VISIBLE);
                    llEmpty.setVisibility(View.GONE);
                } else {
                    audioConnListAdapter.replaceData(new ArrayList<>());
                    rvConnList.setVisibility(View.GONE);
                    llEmpty.setVisibility(View.VISIBLE);
                }
                refreshAudioReqConnCount(getMvpPresenter().getReqingCount() + getMvpPresenter().getConningCount() + getMvpPresenter().getOverCount());
            }

            tvConnSwitch.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                    RoomDataManager.get().singleAudioAnchorConnSwitch ?
                            R.drawable.ic_audio_conn_switch_open : R.drawable.ic_audio_conn_switch_close,
                    0);

        } else {
            //用户视图,可查看所有列表数据
            if (!ListUtils.isListEmpty(getMvpPresenter().getAudioConnReqInfoAllList())) {
                LogUtils.d(TAG, "refreshAudioConnReqList-reqList is not null");
                audioConnListAdapter.replaceData(getMvpPresenter().getAudioConnReqInfoAllList());
                rvConnList.setVisibility(View.VISIBLE);
                llEmpty.setVisibility(View.GONE);
            } else {
                LogUtils.d(TAG, "refreshAudioConnReqList-reqList is null");
                audioConnListAdapter.replaceData(new ArrayList<>());
                rvConnList.setVisibility(View.GONE);
                llEmpty.setVisibility(View.VISIBLE);
            }
            tvRecvConnCount.setText(String.valueOf(getMvpPresenter().getReqingCount() + getMvpPresenter().getConningCount() + getMvpPresenter().getOverCount()));
        }

    }

    @Override
    public void refreshOperaStatus(AudioConnReqInfo.ApplyStatus applyStatus) {
        llBottomOpera.setVisibility(View.VISIBLE);
        //非主播
        if (applyStatus == AudioConnReqInfo.ApplyStatus.Unknow || applyStatus == AudioConnReqInfo.ApplyStatus.Over) {
            if (RoomDataManager.get().isOnMic(CoreManager.getCore(IAuthCore.class).getCurrentUid())) {
                //申请前已经在麦上,不可申请
                bltvReqConn.setVisibility(View.GONE);
                bltvNoReqConn.setVisibility(View.VISIBLE);
            } else {
                //未申请、申请已经结束，重新走申请连接逻辑
                bltvReqConn.setVisibility(RoomDataManager.get().singleAudioAnchorConnSwitch ? View.VISIBLE : View.GONE);
                bltvNoReqConn.setVisibility(RoomDataManager.get().singleAudioAnchorConnSwitch ? View.GONE : View.VISIBLE);
            }

            bltvEditConnReq.setVisibility(View.GONE);
            bltvCannelConnReq.setVisibility(View.GONE);
            bltvCallDownConn.setVisibility(View.GONE);
        } else if (applyStatus == AudioConnReqInfo.ApplyStatus.Conning) {
            //连接中，显示挂断
            bltvReqConn.setVisibility(View.GONE);
            bltvNoReqConn.setVisibility(View.GONE);
            bltvEditConnReq.setVisibility(View.GONE);
            bltvCannelConnReq.setVisibility(View.GONE);
            bltvCallDownConn.setVisibility(View.VISIBLE);
        } else {
            //等待中，显示取消申请，编辑申请理由
            bltvReqConn.setVisibility(View.GONE);
            bltvNoReqConn.setVisibility(View.GONE);
            bltvEditConnReq.setVisibility(View.VISIBLE);
            bltvCannelConnReq.setVisibility(View.VISIBLE);
            bltvCallDownConn.setVisibility(View.GONE);
        }
    }

    @Override
    public void refreshConnSwitchStatus() {
        tvConnSwitch.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                RoomDataManager.get().singleAudioAnchorConnSwitch ?
                        R.drawable.ic_audio_conn_switch_open : R.drawable.ic_audio_conn_switch_close,
                0);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == PayReqAudioConnActivity.PAY_REQ_AUDIO_CONN_REQUEST_CODE
                && resultCode == PayReqAudioConnActivity.PAY_REQ_AUDIO_CONN_RESULT_CODE)
                || requestCode == EditReqAudioConnReasonActivity.EDIT_REQ_AUDIO_CONN_REASON_REQUEST_CODE
                && resultCode == EditReqAudioConnReasonActivity.EDIT_REQ_AUDIO_CONN_REASON_RESULT_CODE) {
            if (isVisible() && null != getActivity()) {
                getAudioConnReqList(true);
            }
        }
    }

    /**
     * 接收房间事件
     */
    public void receiveRoomEvent(IMRoomEvent roomEvent) {
        if (roomEvent == null) {
            return;
        }
        int event = roomEvent.getEvent();
        switch (event) {
            case IMRoomEvent.ROOM_AUDIO_CONN_USER_CALL_DOWN_NOTIFY:
                if (isAnchor && isVisible()) {
                    getAudioConnReqList(true);
                }
                break;
            case IMRoomEvent.ROOM_AUDIO_CONN_ANCHOR_CALL_DOWN_NOTIFY:
                if (!isAnchor && isVisible()) {
                    getAudioConnReqList(true);
                }
                break;
            case IMRoomEvent.DOWN_MIC:
                //下麦
                LogUtils.d(TAG, "下麦");
                //主播、该用户均不作刷新
                break;
            case IMRoomEvent.UP_MIC:
                //上麦
                LogUtils.d(TAG, "上麦");
                if (isAnchor && isVisible()) {
                    //无任何提示
                } else if (!TextUtils.isEmpty(roomEvent.getAccount())
                        && null != CoreManager.getCore(IAuthCore.class)
                        && String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()).equals(roomEvent.getAccount())
                        && isVisible()) {
                    dismiss();
                }
                break;
            case IMRoomEvent.INVITE_UP_MIC:
                if (!isAnchor && isVisible() && getMvpPresenter().getApplyStatus() == AudioConnReqInfo.ApplyStatus.Reqing) {
                    dismiss();
                }
                break;
            case IMRoomEvent.ROOM_AUDIO_CONN_REQ_CONN_BY_ANCHOR:
                if (!isAnchor && isVisible()) {
                    dismiss();
                }
                break;
            case IMRoomEvent.ROOM_RECEIVE_LIVE_STOP:
                LogUtils.d(TAG, "主播停播通知");
                //主播停播通知
                if (isVisible()) {
                    getAudioConnReqList(true);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        AudioConnTimeCounter.getInstance().release();
        audioConnListAdapter.setOnAudioConnItemClickListener(null);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (rlContent != null){
            rlContent.removeAllViews();
            rlContent = null;
        }
        AudioConnTimeCounter.getInstance().release();
        if (null != srlRefresh) {
            srlRefresh.setOnRefreshListener(null);
        }
        if (null != audioConnListAdapter) {
            audioConnListAdapter.setOnAudioConnItemClickListener(null);
        }

        //解决三星手机Handler Thread导致的泄露https://segmentfault.com/q/1010000017286787
        if (getDialog() != null) {
            getDialog().setOnShowListener(null);
            getDialog().setOnCancelListener(null);
            getDialog().setOnDismissListener(null);
        }
    }
}
