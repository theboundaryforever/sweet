package com.yuhuankj.tmxq.ui.liveroom.imroom.room.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewStub;
import android.widget.TextView;

import com.juxiao.library_utils.DisplayUtils;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.ButtonUtils;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftType;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomEvent;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomQueueInfo;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.redpacket.bean.ActionDialogInfo;
import com.tongdaxing.xchat_core.room.bean.RoomAdditional;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.tongdaxing.xchat_core.room.queue.bean.RoomConsumeInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.audio.widget.MusicPlayerView;
import com.yuhuankj.tmxq.ui.launch.guide.NoviceGuideDialog;
import com.yuhuankj.tmxq.ui.liveroom.imroom.gift.RoomGiftDialog;
import com.yuhuankj.tmxq.ui.liveroom.imroom.populargift.OnPopularGiftResultClick;
import com.yuhuankj.tmxq.ui.liveroom.imroom.populargift.PopularGiftView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.publicscreen.MessageView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.activity.AuctionInviteListActivity;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.activity.IMRoomInviteActivity;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.base.BaseRoomDetailFragment;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.dialog.AuctionEndDilaog;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.dialog.AuctionSettingDialog;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.dialog.AuctionSortListDialog;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.dialog.AuctionStartAndFailDilaog;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.listener.OnMicroItemClickListener;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter.AuctionRoomPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IAuctionRoomView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.AuctionParticipateView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.AuctionRoomMicroView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.ComingMsgView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.MultiInputMsgView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.RoomAdmireView;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.RoomLotteryBoxView;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.activity.RoomTopicActivity;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.dialog.RoomPrivateMsgDialog;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.face.DynamicFaceDialog;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.widget.GiftV2View;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.widget.MultiBottomView;
import com.yuhuankj.tmxq.ui.widget.BigListDataDialog;
import com.yuhuankj.tmxq.ui.widget.RoomMoreOpearDialog;
import com.yuhuankj.tmxq.ui.widget.RoomTopicDIalog;
import com.yuhuankj.tmxq.widget.Banner;

import java.util.List;

import butterknife.BindView;

/**
 * 文件描述：竞拍房
 *
 * @auther：zwk
 * @data：2019/7/26
 */
@CreatePresenter(AuctionRoomPresenter.class)
public class AuctionRoomFragment extends BaseRoomDetailFragment<IAuctionRoomView, AuctionRoomPresenter, AuctionRoomMicroView>
        implements IAuctionRoomView, OnPopularGiftResultClick {
    @NonNull
    @Override
    protected AuctionRoomMicroView getMicroView() {
        return auctionRoomMicroView;
    }

    @NonNull
    @Override
    protected MessageView getMessageView() {
        return messageView;
    }

    @NonNull
    @Override
    protected GiftV2View getGiftView() {
        return giftV2View;
    }

    @NonNull
    @Override
    protected MultiBottomView getBottomView() {
        return bottomView;
    }

//    @Override
//    protected RoomGiftComboSender getRoomGiftComboSenderView() {
//        return null;
//    }
//
//    @Override
//    protected ComboGiftView getComboGiftView() {
//        return null;
//    }

    @Override
    protected TextView getFollowRoomView() {
        return auctionRoomMicroView.getBltAttention();
    }

    @NonNull
    @Override
    protected ComingMsgView getComingMsgView() {
        return comingMsgView;
    }

    @Override
    protected RoomAdmireView getRoomAdmireView() {
        return null;
    }

    @Override
    protected Banner getBanner() {
        return null;
    }

    @NonNull
    @Override
    protected MultiInputMsgView getInputMsgView() {
        return inputMsgView;
    }

    @NonNull
    @Override
    protected View getMoreOperateBtn() {
        return getMicroView().getIvMoreFunction();
    }

    @BindView(R.id.aurmv_auction_micro_view)
    public AuctionRoomMicroView auctionRoomMicroView;
    @BindView(R.id.mv_auction_room_msg_list)
    public MessageView messageView;
    @BindView(R.id.bv_auction_room_bottom)
    public MultiBottomView bottomView;
    @BindView(R.id.imv_auction_input_msg)
    public MultiInputMsgView inputMsgView;
    @BindView(R.id.gv_gift_effect)
    public GiftV2View giftV2View;
    @BindView(R.id.vs_music_player)
    public ViewStub mVsMusicPlayer;
    @BindView(R.id.rlbv_lottery_box)
    public RoomLotteryBoxView roomLotteryBoxView;
    @BindView(R.id.cmv_auction_msg)
    public ComingMsgView comingMsgView;
    //    @BindView(R.id.room_pk_view)
//    RoomPKView homePartyPKView;
    @BindView(R.id.pgv_gift)
    PopularGiftView popularGiftView;
    @BindView(R.id.apv_auction_participate)
    AuctionParticipateView participateView;

    private DynamicFaceDialog dynamicFaceDialog;
    private MusicPlayerView mMusicPlayerView;


    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_auction_room;
    }


    @Override
    public void onFindViews() {
        giftV2View.setRoomType(RoomInfo.ROOMTYPE_NEW_AUCTION);
        messageView.initMsgData();
        bottomView.initState();
        popularGiftView.setOnPopularGiftResultClick(this);
        popularGiftView.setViewRightMargin(DisplayUtils.dip2px(getActivity(), 123));
        popularGiftView.addTouchEvent();
        super.onFindViews();
    }

    @Override
    public void initiate() {
        super.initiate();
        getMicroView().setOnOwnerMicroItemClickListener(v -> getMvpPresenter().onAuctionMicroClickEvent(getContext(), RoomDataManager.get().getRoomQueueMemberInfoByMicPosition(RoomDataManager.MIC_POSITION_BY_OWNER), RoomDataManager.MIC_POSITION_BY_OWNER));
        getMicroView().setOnRankMoreClickListener(v -> onShowRoomRankListDialog());
        getMicroView().setOnlinePeopleClickListener(v -> showOnlineMemberDialog(chatRoomMember -> getMvpPresenter().dealWithOnlinePeoleClickEvent(getContext(), getMvpPresenter().getCurrRoomInfo(), chatRoomMember)));
        getMicroView().setOnRoomTopicClickListener(v -> showRoomTopicView());
        getMicroView().setOnRoomShareClickListener(v -> showShareDialog());
        getMicroView().setOnRoomAuctionBtnClickListener(v -> getMvpPresenter().onAuctionBtnEvent());
        getMicroView().getRoomAuctionAdapter().setOnRoomAuctionClickListener(auctionUserBean -> getMvpPresenter().onAuctionUserClickEvent(auctionUserBean));
        participateView.setOnClickListener(v -> {
            if (!ButtonUtils.isFastDoubleClick(v.getId(), 500)) {
                AuctionSortListDialog auctionSortListDialog = new AuctionSortListDialog();
                auctionSortListDialog.show(getChildFragmentManager(), null);
            }
        });
        roomLotteryBoxView.setOnQuickClickListener(this::showLotteryBoxDialog);
    }

    //------------------------------------新手引导----------------------------------------------
    private NoviceGuideDialog auctionRoomGuideDialog;

    @Override
    public void receiveRoomEvent(IMRoomEvent roomEvent) {
        super.receiveRoomEvent(roomEvent);
        switch (roomEvent.getEvent()) {
            case IMRoomEvent.ROOM_AUCTION_STATE_NOTIFY:
                getMicroView().updateOperateAuctionState();
                if (roomEvent.getAuctionState() == CustomAttachment.CUSTOM_MSG_SECOND_AUCTION_START) {
                    if ( RoomDataManager.get().getAuction() != null) {
                        participateView.updateCount(RoomDataManager.get().getAuction().getCount());
                    }
                    AuctionStartAndFailDilaog startDilaog = new AuctionStartAndFailDilaog(getContext());
                    startDilaog.show();
                } else if (roomEvent.getAuctionState() == CustomAttachment.CUSTOM_MSG_SECOND_AUCTION_UPDATE) {
                    getMicroView().updateAuctionList();
                } else if (roomEvent.getAuctionState() == CustomAttachment.CUSTOM_MSG_SECOND_AUCTION_END) {
                    getMicroView().updateAuctionList();
                    if (roomEvent.getRoomAuctionBean() != null) {
                        if (!ListUtils.isListEmpty(roomEvent.getRoomAuctionBean().getList())) {
                            AuctionEndDilaog endDilaog = new AuctionEndDilaog(getContext(), roomEvent.getRoomAuctionBean());
                            endDilaog.show();
                        } else {
                            AuctionStartAndFailDilaog startDilaog = new AuctionStartAndFailDilaog(getContext(), roomEvent.getRoomAuctionBean(), true);
                            startDilaog.show();
                        }
                    }
                } else if (roomEvent.getAuctionState() == CustomAttachment.CUSTOM_MSG_SECOND_AUCTION_PARTICIPATE_COUNT) {
                    if (RoomDataManager.get().getAuction() != null){
                        RoomDataManager.get().getAuction().setCount(roomEvent.getAuctionJoinCount());
                    }
                    participateView.updateCount(roomEvent.getAuctionJoinCount());
                }
                break;
            case RoomEvent.ROOM_MANAGER_ADD:
                if (RoomDataManager.get().isUserSelf(roomEvent.getAccount())) {//如果是自己
                    if (RoomDataManager.get().mSelfRoomMember != null) {
                        RoomDataManager.get().mSelfRoomMember.setIsManager(true);
                    }
                    getMicroView().updateOperateAuctionState();
                }
                break;
            case RoomEvent.ROOM_MANAGER_REMOVE:
                if (RoomDataManager.get().isUserSelf(roomEvent.getAccount())) {//如果是自己
                    if (RoomDataManager.get().mSelfRoomMember != null) {
                        RoomDataManager.get().mSelfRoomMember.setIsManager(false);
                    }
                    //主持人移除管理员需要下麦
                    int position = RoomDataManager.get().getMicPosition(CoreManager.getCore(IAuthCore.class).getCurrentUid());
                    if (position == RoomDataManager.AUCTION_ROOM_HOST_POSITION){//主持位
                        getMvpPresenter().downMicro(0);
                    }
                    getMicroView().updateOperateAuctionState();
                }
                break;
        }
    }

    @Override
    protected void updateRoomInfoAboutView(RoomInfo roomInfo) {
        getMicroView().updateRoomOwnerMicroInfo();
        getMicroView().updateAuctionList();
        getMicroView().updateViewFromRoomInfo(roomInfo);
        if (getMvpPresenter().getCurrRoomInfo() != null) {
            getMvpPresenter().getFortuneRankTopData(getMvpPresenter().getCurrRoomInfo().getUid(), getMvpPresenter().getCurrRoomInfo().getType());
        }
    }

    /**
     * 底部功能按钮初始化：目前的调用的逻辑顺序是优先处理显示声网以外的相关控件的显示
     * 声网初始化完成后，在显示，（提升部分速度）后续后端优化进房接口后在调整回来
     */
    @Override
    protected void onRoomAudioInitSucShowBottomView() {

    }

    /**
     * 动态改变的view初始状态，
     * 如砸蛋是否显示以及关注等其他状态在统一位置更新避免太过混乱
     */
    @Override
    protected void onRoomDynamicInitView() {
        super.onRoomDynamicInitView();
//        homePartyPKView.initData(false);
        if ( RoomDataManager.get().getAuction() != null) {
            participateView.updateCount(RoomDataManager.get().getAuction().getCount());
        }
        roomLotteryBoxView.initialView();
        if (RoomDataManager.get().getAdditional() != null) {
            popularGiftView.updatePopularGiftInfo(true, RoomDataManager.get().getAdditional().getDetonatingState());
        }
    }

    @Override
    protected void refreshOnlineCount(int onlineNum) {
        getMicroView().updateRoomOnlinePeople(onlineNum);
    }


    @Override
    public void onGetFortuneRankTop(boolean isSuccess, String message, List<RoomConsumeInfo> roomConsumeInfos) {
        if (isSuccess && null != roomConsumeInfos) {
            getMicroView().setRankListInfo(roomConsumeInfos.size() >= 1 ? roomConsumeInfos.get(0).getAvatar() : "", roomConsumeInfos.size() >= 2 ? roomConsumeInfos.get(1).getAvatar() : "");
        }
    }

    @Override
    public void onUpdateAuctionOperateBtn() {
        getMicroView().updateOperateAuctionState();
    }

    @Override
    public void showAuctionSortDialog() {
        AuctionSortListDialog auctionSortListDialog = new AuctionSortListDialog();
        auctionSortListDialog.show(getChildFragmentManager(), null);
    }

    @Override
    public void isAdminJoinAuction(boolean join,long uid,boolean isInvite) {
        if (join){
            getMvpPresenter().operateUpMicro(1, uid, isInvite);
        }else {
            AuctionSettingDialog settingDialog = new AuctionSettingDialog();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isAdmin",true);
            bundle.putLong("adminUid",uid);
            settingDialog.setArguments(bundle);
            //在这里使用getChildFragmentManager会不起作用
            settingDialog.show(getChildFragmentManager(), null);
        }
    }

    @Override
    public void showAuctionSettingDialog(long adminUid,boolean isInvite) {
        if (RoomDataManager.get().getCurrentRoomInfo() != null) {
            getMvpPresenter().getRoomAuctionList(RoomDataManager.get().getCurrentRoomInfo().getRoomId(),adminUid,isInvite);
        }
    }

    /**
     * 麦位点击事件：
     *
     * @return
     */
    @Override
    public OnMicroItemClickListener getOnMicroItemClickListener() {
        return new OnMicroItemClickListener() {
            @Override
            public void onUpMicBtnClick(int position) {

            }

            @Override
            public void onMicroBtnClickListener(IMRoomQueueInfo roomQueueInfo, int micPosition) {
                if (getContext() != null) {
                    getMvpPresenter().onAuctionMicroClickEvent(getContext(), roomQueueInfo, micPosition);
                }
            }
        };
    }

    /**
     * 右上角的多功能按钮点击事件：
     * 最小和退出房间
     */
    @Override
    public void showRoomMoreOperateDialog() {
        getMvpPresenter().dealWithRoomMoreOperateEvent();
    }

    @Override
    public void onSendFaceBtnClick() {
        if (getMvpPresenter().isOnMicByMyself() || getMvpPresenter().isRoomOwnerByMyself()) {
            if (dynamicFaceDialog == null) {
                dynamicFaceDialog = new DynamicFaceDialog(getContext());
                dynamicFaceDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        dynamicFaceDialog = null;
                    }
                });
            }
            if (!dynamicFaceDialog.isShowing()) {
                dynamicFaceDialog.show();
            }
        } else {
            toast("上麦才能发表情哦!");
        }
    }

    @Override
    public void onPublicRoomMsgBtnClick() {
        RoomPrivateMsgDialog msgDialog = new RoomPrivateMsgDialog();
        msgDialog.show(getChildFragmentManager(), null);
    }

    @Override
    public void onRoomMoreOperaClick() {
        if (getActivity() == null) {
            return;
        }
        RoomAdditional roomAdditional = RoomDataManager.get().getAdditional();
        RoomMoreOpearDialog roomMoreOpearDialog = new RoomMoreOpearDialog(getActivity());
        if (roomAdditional != null) {
            roomMoreOpearDialog.setLuckyWheelSwitch(roomAdditional.isBigWheelSwitch());
//            roomMoreOpearDialog.setRedPacketSwitch(roomAdditional.isRedPacketSwitch());
            roomMoreOpearDialog.setRedPacketSwitch(false);
        } else {
            roomMoreOpearDialog.setLuckyWheelSwitch(false);
            roomMoreOpearDialog.setRedPacketSwitch(false);
        }
        roomMoreOpearDialog.setCharmValueSwitch(RoomDataManager.get().getCurrentRoomInfo() != null && RoomDataManager.get().getCurrentRoomInfo().getCharmSwitch() == 1);
//        roomMoreOpearDialog.setHasPKOpened(homePartyPKView != null && homePartyPKView.hasPkState());
        roomMoreOpearDialog.setOnLockyWheelGiftBtnClickListener(new RoomMoreOpearDialog.OnLockyWheelGiftBtnClickListener() {
            @Override
            public void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, long targetUid, int number) {
                //先用旧的
            }

            @Override
            public void showRoomGiftDialog(long uid) {
                RoomGiftDialog.getInstance(uid, GiftType.Package, true).show(getChildFragmentManager(), RoomGiftDialog.class.getSimpleName());
            }
        });
        roomMoreOpearDialog.show();
    }


    /**
     * 显示房间话题：
     * 如果是房主或者管理员进入房间话题设置、普通用户是弹框提醒
     */
    public void showRoomTopicView() {
        if (RoomDataManager.get().isRoomOwner() || RoomDataManager.get().isRoomAdmin()) {
            RoomTopicActivity.start(getContext());
        } else {
            RoomTopicDIalog roomTopicDIalog = new RoomTopicDIalog();
            roomTopicDIalog.show(getChildFragmentManager());
        }
    }

    @Override
    public void refreshMicroView(int micPosition, int roomEvent) {
        if (micPosition == -2) {
            getMicroView().notifyDataSetChanged(roomEvent);
            getMicroView().updateRoomOwnerMicroInfo();
        } else {
            getMicroView().notifyItemChanged(micPosition, roomEvent);
            if (micPosition == -1) {
                getMicroView().updateRoomOwnerMicroInfo();
            }
        }
    }

    @Override
    public void refreshBottomView() {
        super.refreshBottomView();
        onShowMusicPlayView();
    }

    private void onShowMusicPlayView() {
        //获取自己是否在麦上
        boolean isOnMic = getMvpPresenter().isOnMicByMyself();
        // 更新播放器界面
        RoomInfo currRoomInfo = getMvpPresenter().getCurrRoomInfo();
        if (currRoomInfo == null) {
            return;
        }
        if (mMusicPlayerView == null && isOnMic) {
            mMusicPlayerView = (MusicPlayerView) mVsMusicPlayer.inflate();
        }
        if (mMusicPlayerView != null) {
            mMusicPlayerView.setVisibility(isOnMic ? View.VISIBLE : View.GONE);
            mMusicPlayerView.setImageBg(currRoomInfo.getBackPic());
            mMusicPlayerView.setRoomType(currRoomInfo.getType());
        }
    }

    @Override
    public void showGiftDialogView(long targetUid, boolean isPersonal) {
        showGiftDialog(targetUid, isPersonal);
    }

    @Override
    public void showRoomActionBanner(List<ActionDialogInfo> actionDialogInfos) {

    }

    /**
     * 房间贡献榜弹框
     */
    public void onShowRoomRankListDialog() {
        if (getActivity() == null) {
            return;
        }
        BigListDataDialog bigListDataDialog = BigListDataDialog.newContributionListInstance(getActivity());
        bigListDataDialog.setSelectOptionAction(new BigListDataDialog.SelectOptionAction() {
            @Override
            public void optionClick() {
                getDialogManager().showProgressDialog(mContext, "请稍后");
            }

            @Override
            public void onDataResponse() {
                //请求结束前退出可能会导致奔溃，直接捕获没关系
                try {
                    getDialogManager().dismissDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        bigListDataDialog.show(getChildFragmentManager());
        //房间-贡献榜
        StatisticManager.get().onEvent(getActivity(),
                StatisticModel.EVENT_ID_ROOM_CONTRIBUTIONI_LIST,
                StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mMusicPlayerView != null) {
            mMusicPlayerView.updateVoiceValue();
        }
    }

    @Override
    protected void releaseView() {
        super.releaseView();
        //释放音乐播放器的view
        if (mMusicPlayerView != null) {
            mMusicPlayerView.release();
        }
    }


    @Override
    public boolean onBack() {
        return false;
    }

    @Override
    public void showGiftDilaogPackage() {
        if (getFragmentManager() == null) {
            return;
        }
        Fragment fragment = getFragmentManager().findFragmentByTag("giftDialog");
        if (fragment == null) {
            RoomGiftDialog.getInstance(0, GiftType.Exclusive, false).show(getFragmentManager(), "giftDialog");
        } else {
            if (!fragment.isAdded() && !fragment.getUserVisibleHint()) {
                RoomGiftDialog.getInstance(0, GiftType.Exclusive, false).show(getFragmentManager(), "giftDialog");
            }
        }
    }

    @Override
    public void openRoomInviteActivity(int micPosition) {
//        super.openRoomInviteActivity(micPosition);
        if (getActivity() != null) {
            if (micPosition == 0) {
                IMRoomInviteActivity.openActivity(getActivity(), micPosition, true);
            } else if (micPosition == 1) {
                AuctionInviteListActivity.start(getActivity());
            }
        }

    }

    /**
     * 个人房-房主-停播操作，跳转开播统计界面
     */
    @Override
    public void openRoomLiveInfoH5View() {

    }

    /**
     * 非主播，点击底部语音连接按钮
     */
    @Override
    public void onFansReqAudioConnClick() {

    }

    /**
     * 麦位和消息列表初始化显示
     */
    @Override
    protected void onRoomInSucShowMicroAndMessageView() {
        if (getMicroView().getVisibility() == View.INVISIBLE) {
            getMicroView().setVisibility(View.VISIBLE);
        }
        if (getMessageView().getVisibility() == View.INVISIBLE) {
            getMessageView().setVisibility(View.VISIBLE);
        }
        participateView.setVisibility(View.VISIBLE);
        checkShouldShowGiftGuideView();
    }

    /**
     * 主播，点击底部语音连接按钮
     */
    @Override
    public void onAnchorAudioConnRecvClick() {

    }

    /**
     * 判断是否显示新手引导界面
     */
    private void checkShouldShowGiftGuideView() {
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        if (null != userInfo && userInfo.getGuideState() != null && "0".equals(userInfo.getGuideState().getType7())) {
            if (null == auctionRoomGuideDialog) {
                if (null == getActivity()) {
                    return;
                }
                auctionRoomGuideDialog = new NoviceGuideDialog(getActivity(), NoviceGuideDialog.GuideType.AUCTION_ROOM_GUIDE, null);
            }
            LogUtils.d(TAG, "checkShouldShowGiftGuideView-->AUCTION_ROOM_GUIDE");
            auctionRoomGuideDialog.show();
        }
    }

    @Override
    public void toast(String tip) {
        super.toast(tip);
    }
}
