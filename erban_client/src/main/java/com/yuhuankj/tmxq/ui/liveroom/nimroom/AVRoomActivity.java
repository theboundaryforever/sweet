package com.yuhuankj.tmxq.ui.liveroom.nimroom;

import android.animation.Animator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewStub;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.microquation.linkedme.android.LinkedME;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomKickOutEvent;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.erban.libcommon.utils.TimeUtils;
import com.tongdaxing.erban.libcommon.utils.config.SpEvent;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.auth.RealNameAuthStatusChecker;
import com.tongdaxing.xchat_core.common.ICommonClient;
import com.tongdaxing.xchat_core.gift.ComboGiftPublicScreenMsgSender;
import com.tongdaxing.xchat_core.gift.IGiftCoreClient;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCore;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.redpacket.bean.ActionDialogInfo;
import com.tongdaxing.xchat_core.room.IBgClient;
import com.tongdaxing.xchat_core.room.IRoomCoreClient;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.presenter.AvRoomPresenter;
import com.tongdaxing.xchat_core.room.view.IAvRoomView;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.fragment.AbsRoomFragment;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.fragment.HomePartyFragment;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.fragment.InputPwdDialogFragment;
import com.yuhuankj.tmxq.ui.me.charge.ChargeActivity;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.UIUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * 房间入口
 *
 * @author Administrator
 */
@CreatePresenter(AvRoomPresenter.class)
public class AVRoomActivity extends BaseMvpActivity<IAvRoomView, AvRoomPresenter>
        implements View.OnClickListener, IAvRoomView {

    private final String TAG = AVRoomActivity.class.getSimpleName();

    private RelativeLayout finishLayout;
    private ViewStub mVsRoomOffline;
    private ImageView avatarBg;
    private TextView nick;
    private ImageView avatar;

    private long roomUid;
    private AbsRoomFragment mCurrentFragment;

    private InputPwdDialogFragment mPwdDialogFragment;

    private RoomInfo mRoomInfo;
    private boolean isFromMic = false;

    private View mainContainer;
    private ImageView ivBg;
    private ImageView iv_enterBg;
    private TextView tv_enterTips;
    private boolean isFirstComeRoom = true;

    public static void start(Context context, long roomUid) {
        Intent intent = new Intent(context, AVRoomActivity.class);
        intent.putExtra(Constants.ROOM_UID, roomUid);
        context.startActivity(intent);
    }

    public static void start(Context context, long roomUid, int roomType) {
        Intent intent = new Intent(context, AVRoomActivity.class);
        intent.putExtra(Constants.ROOM_UID, roomUid);
        intent.putExtra(Constants.ROOM_TYPE, roomType);
        context.startActivity(intent);
    }

    public static void startFromService(Context context, long roomUid) {
        Intent intent = new Intent(context, AVRoomActivity.class);
        intent.putExtra(Constants.ROOM_UID, roomUid);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }


    public static void start(Context context, long roomUid, boolean isFromMic) {
        Intent intent = new Intent(context, AVRoomActivity.class);
        intent.putExtra(Constants.ROOM_UID, roomUid);
        intent.putExtra(Constants.ROOM_FROM_MIC, isFromMic);
        context.startActivity(intent);
    }

    //退出房间广播动作
    public final static String ACTION_EXIT_ROOM = "exitRoom";

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //由于私聊窗口，发送图片收不到onActivityResult,在这里接收，然后通过广播转发给MessageFragment
        if (data != null) {
            data.setAction("getImageFile");
            data.putExtra("requestCode", requestCode);
            data.putExtra("resultCode", resultCode);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(data);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        IMNetEaseManager.get().inAvRoom = true;
        LinkedME.getInstance().setImmediate(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        IMNetEaseManager.get().inAvRoom = false;
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        // 如果是同一个房间，则只更新房间的信息
        isFromMic = intent.getBooleanExtra(Constants.ROOM_FROM_MIC, false);
        long newRoomUid = intent.getLongExtra(Constants.ROOM_UID, 0);
        if (newRoomUid != 0 && newRoomUid == roomUid) {
            updateRoomInfo();
            return;
        }
        roomUid = newRoomUid;
        // 相同类型的房间，但是是不同人的房间
        isFirstComeRoom = getMvpPresenter().isFirstEnterRoomOrChangeOtherRoom(roomUid, RoomInfo.ROOMTYPE_HOME_PARTY);
        if (mCurrentFragment instanceof HomePartyFragment) {
            ((HomePartyFragment) mCurrentFragment).isFristEnterRoom = isFirstComeRoom;
        }
        getSupportFragmentManager().beginTransaction().remove(mCurrentFragment).commitAllowingStateLoss();
        mCurrentFragment = null;
        if (isFirstComeRoom) {
            mainContainer.setVisibility(View.GONE);
            iv_enterBg.setVisibility(View.VISIBLE);
            tv_enterTips.setVisibility(View.VISIBLE);
            updateRoomInfo();

            //进入房间
            StatisticManager.get().onEvent(this,
                    StatisticModel.EVENT_ID_ROOM_ENTER,
                    StatisticModel.getInstance().getUMAnalyCommonMap(this));
        } else {
            mainContainer.setVisibility(View.VISIBLE);
            iv_enterBg.setVisibility(View.GONE);
            tv_enterTips.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_room);
        mainContainer = findViewById(R.id.rl_content_bg);
        ivBg = (ImageView) findViewById(R.id.iv_bg);
        iv_enterBg = (ImageView) findViewById(R.id.iv_enterBg);
        tv_enterTips = (TextView) findViewById(R.id.tv_enterTips);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        roomUid = getIntent().getLongExtra(Constants.ROOM_UID, 0);
        isFromMic = getIntent().getBooleanExtra(Constants.ROOM_FROM_MIC, false);

        if (getMvpPresenter().checkIsKick(roomUid, RoomInfo.ROOMTYPE_HOME_PARTY)) {
            toast("您被此房间踢出，请稍后再进");
            finish();
        }
        setSwipeBackEnable(false);
        mVsRoomOffline = (ViewStub) findViewById(R.id.vs_room_offline);
        Disposable subscribe = IMNetEaseManager.get().getChatRoomEventObservable()
                .compose(bindToLifecycle())
                .subscribe(new Consumer<RoomEvent>() {
                    @Override
                    public void accept(RoomEvent roomEvent) throws Exception {
                        onRoomEventReceive(roomEvent);
                    }
                });
        mCompositeDisposable.add(subscribe);

        //注册广播接收
        IntentFilter filter = new IntentFilter(ACTION_EXIT_ROOM);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, filter);

        //第一次进来
        isFirstComeRoom = getMvpPresenter().isFirstEnterRoomOrChangeOtherRoom(roomUid, RoomInfo.ROOMTYPE_HOME_PARTY);
        if (isFirstComeRoom) {
            LogUtils.d(TAG, "onCreate-第一次进来");
            mainContainer.setVisibility(View.GONE);
            iv_enterBg.setVisibility(View.VISIBLE);
            tv_enterTips.setVisibility(View.VISIBLE);
            // 同一个接口多次请求，会导致外部进入自己的房间，过程较慢
            updateRoomInfo();

            //进入房间
            StatisticManager.get().onEvent(this,
                    StatisticModel.EVENT_ID_ROOM_ENTER,
                    StatisticModel.getInstance().getUMAnalyCommonMap(this));
            return;
        }
        mainContainer.setVisibility(View.VISIBLE);
        iv_enterBg.setVisibility(View.GONE);
        tv_enterTips.setVisibility(View.GONE);
        //为了避免标记未改变
        addRoomFragment();
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_EXIT_ROOM.equals(intent.getAction())) {
                finish();
            }
        }
    };


    private void onRoomEventReceive(RoomEvent roomEvent) {

        if (roomEvent == null || roomEvent.getEvent() == RoomEvent.NONE) {
            return;
        }

        int event = roomEvent.getEvent();
        LogUtils.d(TAG, "onRoomEventReceive-event:" + event);
        switch (event) {
            case RoomEvent.ENTER_ROOM:
                LogUtils.d(TAG, "onRoomEventReceive-ENTER_ROOM");
                onEnterRoom();
                break;
            case RoomEvent.KICK_OUT_ROOM:
                onKickMember(roomEvent.getReason());
                break;
            case RoomEvent.ROOM_MANAGER_ADD:
            case RoomEvent.ROOM_MANAGER_REMOVE:
                if (AvRoomDataManager.get().isOwner(roomEvent.getAccount())) {
                    if (roomEvent.getEvent() == RoomEvent.ROOM_MANAGER_ADD) {
                        toast(R.string.set_room_manager);
                    } else if (roomEvent.getEvent() == RoomEvent.ROOM_MANAGER_REMOVE) {
                        toast(R.string.remove_room_manager);
                    }
                }
                break;
            case RoomEvent.RTC_ENGINE_NETWORK_BAD:
                toast("当前网络不稳定，请检查网络");
                LogUtils.d(TAG, "onRoomEventReceive-RTC_ENGINE_NETWORK_BAD:当前网络不稳定，请检查网络");
                break;
            case RoomEvent.RTC_ENGINE_NETWORK_CLOSE:
                toast("当前网络异常，与服务器断开连接，请检查网络");
                LogUtils.d(TAG, "onRoomEventReceive-RTC_ENGINE_NETWORK_CLOSE:当前网络异常，与服务器断开连接，请检查网络");
                break;
            case RoomEvent.ROOM_ENTER_OTHER_ROOM:
                BaseRoomServiceScheduler.exitRoom(null);
                finish();
                ComboGiftPublicScreenMsgSender.getInstance().sendComboGiftScreenMsg();
                RoomServiceScheduler.getInstance().enterRoomFromService(this,
                        roomEvent.getRoomUid(), roomEvent.getRoomType());
                LogUtils.d(TAG, "onRoomEventReceive-ROOM_ENTER_OTHER_ROOM:切换房间并退出原来房间");
                break;
            default:
        }
    }

    private void onKickMember(ChatRoomKickOutEvent reason) {
        if (reason == null) {
            return;
        }
        statisticsReason(reason);
        ChatRoomKickOutEvent.ChatRoomKickOutReason reasonReason = reason.getReason();
        getDialogManager().dismissDialog();
        if (reasonReason == ChatRoomKickOutEvent.ChatRoomKickOutReason.BE_BLACKLISTED) {
            toast(getString(R.string.add_black_list));
            finish();
        } else if (reasonReason == ChatRoomKickOutEvent.ChatRoomKickOutReason.CHAT_ROOM_INVALID) {
            showLiveFinishView();
        } else if (reasonReason == ChatRoomKickOutEvent.ChatRoomKickOutReason.KICK_OUT_BY_MANAGER) {
            Json json = new Json();
            json.set(SpEvent.roomUid, roomUid + "");
            json.set(SpEvent.time, System.currentTimeMillis());
            SpUtils.put(this, SpEvent.onKickRoomInfo, json + "");
            toast(getString(R.string.kick_member_by_manager));
            finish();
        } else {
            finish();
        }
    }

    private void statisticsReason(ChatRoomKickOutEvent reason) {
        Map<String, Object> extension = reason.getExtension();
        String roomId = reason.getRoomId();

        Map<String, String> hashMap = new HashMap<>();
        StringBuffer stringBuffer = new StringBuffer();
        System.currentTimeMillis();
        hashMap.put("uid", "a" + CoreManager.getCore(IAuthCore.class).getCurrentUid());
        hashMap.put("time", TimeUtils.stampToDate(System.currentTimeMillis()) + "  ");
        hashMap.put("roomId", roomId + "  ");
        if (extension != null) {
            for (Map.Entry<String, Object> entry : extension.entrySet()) {
                hashMap.put(entry.getKey() + "", entry.getValue() + "");
            }
        }

        for (Map.Entry<String, String> entry : hashMap.entrySet()) {
            stringBuffer.append(entry.getValue() + ",");
        }
    }

    private void onEnterRoom() {
        mRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        LogUtils.d(TAG, "onEnterRoom 进入房间成功,roomId:" + mRoomInfo.getRoomId());
        mainContainer.setVisibility(View.VISIBLE);
        iv_enterBg.setVisibility(View.GONE);
        tv_enterTips.setVisibility(View.GONE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int cicular_R = getResources().getDisplayMetrics().heightPixels / 2;
            Animator animator = ViewAnimationUtils.createCircularReveal(mainContainer, UIUtil.getScreenWidth(this) / 2, cicular_R, 0, cicular_R);
            // Animator animator = ViewAnimationUtils.createCircularReveal(mainContainer, UIUtil.dip2px(this,40),  UIUtil.dip2px(this,70) , 0, cicular_R);
            animator.setDuration(800);
            animator.setInterpolator(new AccelerateInterpolator());
            animator.start();
            animator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    iv_enterBg.setVisibility(View.GONE);
                    tv_enterTips.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        }
        dimissDialog();
    }

    private void addRoomFragment() {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        LogUtils.d(TAG, "addRoomFragment-roomInfo:" + roomInfo);
        if (roomInfo != null) {
            //获取房主信息
            IAVRoomCore core = CoreManager.getCore(IAVRoomCore.class);
            core.removeRoomOwnerInfo();
            core.requestRoomOwnerInfo(roomInfo.getUid() + "");
            //这里拿到的其实是云信的数据，有可能会和自身服务器的数据不一致
            if (null != AvRoomDataManager.get().mServiceRoominfo) {
                setBackBg(AvRoomDataManager.get().mServiceRoominfo);
            }
            int roomType = roomInfo.getType();
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            LogUtils.d("request_AVRoomType:" + roomType);
            if (roomType == RoomInfo.ROOMTYPE_HOME_PARTY &&
                    !(mCurrentFragment instanceof HomePartyFragment)) {
                mCurrentFragment = HomePartyFragment.newInstance(roomUid);
                ((HomePartyFragment) mCurrentFragment).isFristEnterRoom = isFirstComeRoom;
            } else if (roomType == RoomInfo.ROOMTYPE_SINGLE_AUDIO) {
                return;
            }
            if (mCurrentFragment != null) {
                fragmentTransaction.replace(R.id.main_container, mCurrentFragment).commit();
            }
            getMvpPresenter().getActionDialog(2);
        }
    }

    private void setBackBg(RoomInfo roomInfo) {
        if (roomInfo != null) {
            String backPic = roomInfo.getBackPic();
            //长度大于5是url不是id直接设置其为背景
            if (!TextUtils.isEmpty(backPic)) {
                ImageLoadUtils.loadRoomBackgroundImage(this, backPic, ivBg, R.drawable.icon_home_part_room_bg_new);
            }
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfo(UserInfo userInfo) {
        RoomInfo roomInfo = AvRoomDataManager.get().mServiceRoominfo;
        if (roomInfo != null) {
            setBackBg(roomInfo);
        }
        setUserInfo(userInfo);
    }

    @CoreEvent(coreClientClass = IBgClient.class)
    public void bgModify(String type) {
        if (!TextUtils.isEmpty(type)) {
            ImageLoadUtils.loadImage(this, type, ivBg);
            return;
        }
    }

    private void showLiveFinishView() {
        if (mRoomInfo != null) {
            if (finishLayout == null) {
                finishLayout = (RelativeLayout) mVsRoomOffline.inflate();
                avatar = finishLayout.findViewById(R.id.avatar);
                avatarBg = finishLayout.findViewById(R.id.avatar_bg);
                nick = finishLayout.findViewById(R.id.nick);
            }
            finishLayout.setVisibility(View.VISIBLE);
            finishLayout.findViewById(R.id.home_page_btn).setOnClickListener(this);
            finishLayout.findViewById(R.id.back_btn).setOnClickListener(this);

            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(mRoomInfo.getUid());
            setUserInfo(userInfo);
        }
        AvRoomDataManager.get().release();
    }

    private void setUserInfo(UserInfo userInfo) {
        if (avatarBg == null || avatar == null || nick == null) {
            return;
        }
        if (userInfo != null) {
            //   ImageLoadUtils.loadImageWithBlurTransformation(this, userInfo.getAvatar(), avatarBg);
            ImageLoadUtils.loadAvatar(this, userInfo.getAvatar(), avatar);
            nick.setText(userInfo.getNick());
        } else {
            avatar.setImageResource(R.drawable.default_user_head);
        }
    }

    private void updateRoomInfo() {
        LogUtils.d(TAG, "updateRoomInfo");
        getMvpPresenter().requestRoomInfoFromService(String.valueOf(roomUid), RoomInfo.ROOMTYPE_HOME_PARTY);
    }

    private void showRoomPwdDialog(final RoomInfo roomInfo) {
        //如果前面已经打开当前房间了，再一次打开说明是通过newIntent进来，不需要再显示密码框了
        final RoomInfo currentRoom = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoom != null && roomInfo != null && currentRoom.getRoomId() == roomInfo.getRoomId()) {
            LogUtils.e("第二次打开房间，不需要显示输入密码框了");
            return;
        }
        /*同步解决一下问题：
        bug重现：进入一个有锁房的账号的个人主页，连续快速点击“去找TA”，此时房间对弹出输入密码对话框，无论你输入什么密码都无法进入房间。
        bug原因：AVRoomActivity是一个singletask模式，当连续快速点击这个Activity会依次调用onCreate和onNewIntent方法，
        导致showRoomPwdDialog方法的mPwdDialogFragment变量被多次实例化。
         */
        if (null == mPwdDialogFragment) {
            mPwdDialogFragment = InputPwdDialogFragment.newInstance(getString(R.string.input_pwd),
                    getString(R.string.ok),
                    getString(R.string.cancel),
                    roomInfo.getRoomPwd());

            //google的bug
            try {
                mPwdDialogFragment.show(getSupportFragmentManager(), "pwdDialog");
            } catch (Exception e) {
                finish();
            }

            mPwdDialogFragment.setOnDialogBtnClickListener(new InputPwdDialogFragment.OnDialogBtnClickListener() {
                @Override
                public void onBtnConfirm(String pwd) {
                    mPwdDialogFragment.dismiss();
                    mPwdDialogFragment = null;
                    getMvpPresenter().enterRoom(roomInfo, isFromMic);
                }

                @Override
                public void onBtnCancel() {
                    mPwdDialogFragment.dismiss();
                    mPwdDialogFragment = null;
                    finish();
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AvRoomDataManager.get().setMinimize(true);
    }

    @Override
    public void onGetActionDialog(List<ActionDialogInfo> dialogInfo) {
        if (mCurrentFragment != null && mCurrentFragment.isVisible()) {
            mCurrentFragment.onShowActivity(dialogInfo);
        }
    }

    @Override
    public void onGetActionDialogError(String error) {
    }

    @Override
    public void exitRoom(RoomInfo currentRoomInfo) {
        if (currentRoomInfo != null && currentRoomInfo.getUid() == roomUid) {
            finish();
        }
    }


    @Override
    public void onRoomOnlineNumberSuccess(int onlineNumber) {
        if (mCurrentFragment != null) {
            mCurrentFragment.onRoomOnlineNumberSuccess(onlineNumber);
        }
    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onGiftPastDue() {
        toast("该礼物已过期");
    }

    @CoreEvent(coreClientClass = ICommonClient.class)
    public void onRecieveNeedRecharge() {
        getDialogManager().showOkCancelDialog("余额不足，是否充值", true,
                new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onOk() {
                        ChargeActivity.start(AVRoomActivity.this);
                    }
                });
    }

    public void toBack() {
        long myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomUid == myUid && roomInfo != null && roomInfo.getType() != RoomInfo.ROOMTYPE_HOME_PARTY) {
            getDialogManager().showOkCancelDialog("当前正在开播，是否要关闭直播？", true, new DialogManager.OkCancelDialogListener() {
                @Override
                public void onCancel() {

                }

                @Override
                public void onOk() {
                    getMvpPresenter().exitRoom();
                    finish();
                }
            });
        } else {
            getMvpPresenter().exitRoom();
            finish();
        }
    }


    @Override
    protected int setBgColor() {
        return R.color.black;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPwdDialogFragment != null) {
            //这里可能会导致空指针，直接捕获，不蹦就行
            try {
                mPwdDialogFragment.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            mPwdDialogFragment = null;
        }
        if (broadcastReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        }

        if (mCurrentFragment != null) {
            mCurrentFragment = null;
        }
        fixInputMethodManagerLeak(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.home_page_btn:
                if (mRoomInfo != null) {
                    UserInfoActivity.start(this, mRoomInfo.getUid());
                    finish();
                }
                break;
            case R.id.back_btn:
                finish();
                break;
            default:
        }
    }

    @Override
    public void requestRoomInfoSuccessView(RoomInfo roomInfo) {
        LogUtil.d(TAG, "enterRoom: requestRoomInfoSuccessView > roomId = " + roomInfo.getRoomId());
        mRoomInfo = roomInfo;
        AvRoomDataManager.get().setHideFaceList(roomInfo.getHideFace());
        CoreManager.getCore(IUserCore.class).requestUserInfoNotNotify(CoreManager.getCore(IAuthCore.class).getCurrentUid(), new OkHttpManager.MyCallBack<UserInfo>() {
            @Override
            public void onError(Exception e) {
                doEnterRoomAction();
            }

            @Override
            public void onResponse(UserInfo response) {
                doEnterRoomAction();
            }
        });
    }

    private void doEnterRoomAction() {
        if (null != mRoomInfo && (TextUtils.isEmpty(mRoomInfo.getRoomPwd())
                || mRoomInfo.getUid() == CoreManager.getCore(IAuthCore.class).getCurrentUid())) {
            getMvpPresenter().enterRoom(mRoomInfo, isFromMic);
        } else {
            if (isFinishing()) {
                return;
            }
            showRoomPwdDialog(mRoomInfo);
        }
    }

    @Override
    public void requestRoomInfoFailView(int code, String errorStr) {
        getDialogManager().dismissDialog();
        //业务逻辑保持，不删除，以方便旧版本出现问题时调试定位
        if (code == RealNameAuthStatusChecker.RESULT_FAILED_NEED_REAL_NAME_AUTH) {
            //MainActivity回调弹出弹框
//            CoreManager.notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_NEED_REAL_NAME_AUTH, this,
//                    getResources().getString(R.string.real_name_auth_tips,
//                            getResources().getString(R.string.real_name_auth_tips_create_room)));
            DialogManager mDialogManager = new DialogManager(this);
            mDialogManager.setCanceledOnClickOutside(false);
            mDialogManager.setCanceledOnClickBackKey(false);
            mDialogManager.showOkCancelDialog(getResources().getString(R.string.real_name_auth_tips,
                    getResources().getString(R.string.real_name_auth_tips_create_room)),
                    getResources().getString(R.string.real_name_auth_accept),
                    getResources().getString(R.string.cancel), new DialogManager.OkCancelDialogListener() {
                        @Override
                        public void onCancel() {
                            finish();
                        }

                        @Override
                        public void onOk() {
                            CommonWebViewActivity.start(AVRoomActivity.this, UriProvider.getRealNameAuthUrl());
                            finish();
                        }
                    });
        } else {
            toast(errorStr);
            finish();
        }
    }

    @Override
    public void enterRoomSuccess() {
        LogUtils.d(TAG, "enterRoomSuccess");
        addRoomFragment();
        //获取管理员
        getMvpPresenter().getNormalChatMember();
    }

    @Override
    public void enterRoomFail(int code, String error) {
        dimissDialog();
        AvRoomDataManager.get().release();
        toast(error);
        finish();
    }

    @Override
    public void showFinishRoomView() {
        dimissDialog();
        showLiveFinishView();
    }

    @Override
    public void showBlackEnterRoomView() {
        dimissDialog();
        AvRoomDataManager.get().release();
        toast(getString(R.string.add_black_list));
        finish();
    }

    private void dimissDialog() {
        getDialogManager().dismissDialog();
        if (mPwdDialogFragment != null) {
            mPwdDialogFragment.dismiss();
        }
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogout() {
        AvRoomDataManager.get().release();
        finish();
    }

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void enterError() {
        enterRoomFail(-1, "网络异常");
    }

    @Override
    public boolean isStatusBarDarkFont() {
        return false;
    }
}