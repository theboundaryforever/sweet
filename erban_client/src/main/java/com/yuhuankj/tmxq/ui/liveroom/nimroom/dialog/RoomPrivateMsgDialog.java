package com.yuhuankj.tmxq.ui.liveroom.nimroom.dialog;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.recent.RecentContactsCallback;
import com.netease.nim.uikit.recent.RecentContactsFragment;
import com.netease.nim.uikit.session.OfficSecrRedPointCountManager;
import com.netease.nim.uikit.session.activity.P2PMessageActivity;
import com.netease.nimlib.sdk.msg.attachment.AudioAttachment;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.RecentContact;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.MiniGameInvitedAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.NoticeAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RedPacketAttachment;
import com.tongdaxing.xchat_core.redpacket.bean.RedPacketInfoV2;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.BaseDialogFragment;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.fragment.PublicChatFragment;
import com.yuhuankj.tmxq.ui.nim.sysmsg.SysMsgActivity;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_LOTTERY;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_NOTICE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_OPEN_ROOM_NOTI;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PACKET;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_MINI_GAME;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_MINI_GAME_ON_INVITED_NOTIFY;

//import com.yuhuankj.tmxq.ui.liveroom.nimroom.fragment.PublicChatFragment;

/**
 * 房间消息弹框，包含两个tab页面，消息和广场
 */
public class RoomPrivateMsgDialog extends BaseDialogFragment implements View.OnClickListener {

    private final String TAG = RoomPrivateMsgDialog.class.getSimpleName();

    private RecentContactsFragment recentContactsFragment;
    private PublicChatFragment publicChatFragment;

    private ImageView ivCloss;

    private RelativeLayout rl_msgTab;
    private TextView tv_msgTab;
    private View v_msgInds;

    private RelativeLayout rl_squareTab;
    private TextView tv_squareTab;
    private View v_squareInds;

    private String[] tags = {"recent", "public"};// fragment标记
    private Map<String, Fragment> fragments = new HashMap<>();// fragment表
    private LinkedList<String> mFragments = new LinkedList<String>();// 入栈的fragment使用标记区分
    FragmentTransaction fragmentTransaction;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CoreManager.addClient(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //参考https://segmentfault.com/q/1010000017286787
        Dialog dialog = getDialog();
        if (null != dialog) {
            dialog.setOnShowListener(null);
            dialog.setOnCancelListener(null);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        // setup window and width
        View view = inflater.inflate(R.layout.dialog_room_private_msg, window.findViewById(android.R.id.content), false);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.BOTTOM);
        setCancelable(true);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ivCloss = view.findViewById(R.id.iv_close_dialog);
        ivCloss.setOnClickListener(this);

        rl_msgTab = view.findViewById(R.id.rl_msgTab);
        rl_msgTab.setOnClickListener(this);
        tv_msgTab = view.findViewById(R.id.tv_msgTab);
        v_msgInds = view.findViewById(R.id.v_msgInds);


        v_squareInds = view.findViewById(R.id.v_squareInds);
        tv_squareTab = view.findViewById(R.id.tv_squareTab);
        rl_squareTab = view.findViewById(R.id.rl_squareTab);
        rl_squareTab.setOnClickListener(this);

        recentContactsFragment = new RecentContactsFragment();
        recentContactsFragment.viewScene = RecentContactsFragment.ViewScene.RoomMsgDialog;
        fragments.put(tags[0], recentContactsFragment);
        recentContactsFragment.setCallback(recentContactsCallback);
        switchTabPage(TabType.Msg);

        publicChatFragment = new PublicChatFragment();
        Bundle args = new Bundle();
        args.putBoolean(PublicChatFragment.ACTION_SHOW_IN_ROOM, true);
        publicChatFragment.setArguments(args);
        fragments.put(tags[1], publicChatFragment);

        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if (0L == uid) {
            return;
        }
        if (null != NimUIKit.onServerEmojiOperaListener) {
            NimUIKit.onServerEmojiOperaListener.onNeedInitEmojiFromServer(uid);
        }
    }


    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo userInfo) {
        recentContactsFragment.requestMessages(true);
    }

    @Override
    public void show(FragmentManager fragmentManager, String tag) {
        super.show(fragmentManager, tag);
        LogUtils.d(TAG, "show");
    }

    private RecentContactsCallback recentContactsCallback = new RecentContactsCallback() {
        @Override
        public void onRecentContactsLoaded() {

        }

        @Override
        public void onUnreadCountChange(int unreadCount) {

        }

        @Override
        public void onItemClick(RecentContact recent) {
            LogUtils.d(TAG, "onItemClick-recentID:" + recent.getContactId());
//                if (recent.getSessionType() == SessionTypeEnum.Team) {
////                    NimUIKit.startTeamSession(getActivity(), recent.getContactId());
////                } else
            if (recent.getSessionType() == SessionTypeEnum.P2P) {
                if (OfficSecrRedPointCountManager.isOfficMsgAccount(recent.getContactId())) {
                    //如果是系统通知账号，跳转到系统通知页面
                    try {
                        NimUIKit.startP2PSession(getActivity(), recent.getContactId());
                        startActivity(new Intent(getActivity(), SysMsgActivity.class));
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (getActivity() != null) {
                            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent(P2PMessageActivity.ACTION_FINISH));
                        }
                    }
                    OfficSecrRedPointCountManager.sysMsgNum = 0;
                } else {
                    RoomPrivateChatDialog chat = RoomPrivateChatDialog.newInstance(recent.getContactId());
                    chat.show(getFragmentManager(), null);
                    //为了体验上更好些，这里列表弹框不隐藏
//                dismiss();
                }
            }
        }

        @Override
        public String getDigestOfAttachment(RecentContact recent, MsgAttachment attachment) {
            if (attachment instanceof CustomAttachment) {
                CustomAttachment customAttachment = (CustomAttachment) attachment;
                if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_OPEN_ROOM_NOTI) {
                    return "您关注的TA上线啦，快去围观吧~~~";
                } else if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_GIFT) {
                    return "[礼物]";
                } else if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_NOTICE) {
                    NoticeAttachment noticeAttachment = (NoticeAttachment) attachment;
                    return noticeAttachment.getTitle();
                } else if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_PACKET) {
                    RedPacketAttachment redPacketAttachment = (RedPacketAttachment) attachment;
                    RedPacketInfoV2 redPacketInfo = redPacketAttachment.getRedPacketInfo();
                    if (redPacketInfo == null) {
                        return "您收到一个红包哦!";
                    }
                    return "您收到一个" + redPacketInfo.getPacketName() + "红包哦!";
                } else if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_LOTTERY) {
                    return "恭喜您，获得抽奖机会";
                } else if (customAttachment.getFirst() == CUSTOM_MSG_MINI_GAME) {
                    if (customAttachment.getSecond() == CUSTOM_MSG_MINI_GAME_ON_INVITED_NOTIFY) {
                        MiniGameInvitedAttachment miniGameInvitedAttachment = (MiniGameInvitedAttachment) attachment;
                        return miniGameInvitedAttachment.getDataInfo().getFromNick().concat("邀请你进入游戏");
                    }
                }
            } else if (attachment instanceof AudioAttachment) {
                return "[语音]";
            }
            return null;
        }

        @Override
        public String getDigestOfTipMsg(RecentContact recent) {
            return null;
        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        CoreManager.removeClient(this);

        //修复recentContactsCallback中Message obj引用被持有而导致的内存泄漏
        if (recentContactsFragment != null) {
            recentContactsFragment.setCallback(null);
        }
        if (null != recentContactsCallback) {
            recentContactsCallback = null;
        }
        if (ivCloss != null) {
            ivCloss.setOnClickListener(null);
            ivCloss = null;
        }
        if (rl_msgTab != null) {
            rl_msgTab.setOnClickListener(null);
            rl_msgTab = null;
        }
        if (rl_squareTab != null) {
            rl_squareTab.setOnClickListener(null);
            rl_squareTab = null;
        }
        tv_msgTab = null;
        v_msgInds = null;
        tv_squareTab = null;
        v_squareInds = null;

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.iv_close_dialog:
                dismiss();
                break;
            case R.id.rl_msgTab:
                switchTabPage(TabType.Msg);
                break;
            case R.id.rl_squareTab:
                switchTabPage(TabType.Square);
                break;
            default:
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /**
     * 自定义tab类型
     */
    enum TabType {
        Msg,
        Square
    }

    @Override
    public void onResume() {
        super.onResume();
        LogUtils.d(TAG, "switchTabPage");
    }

    /**
     * 根据tab类型切换fragment
     *
     * @param tabType
     */
    private void switchTabPage(TabType tabType) {
        LogUtils.d(TAG, "switchTabPage-tabType:" + tabType);
        FragmentManager fragmentManager = getChildFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(null);
        Fragment fragment = null;
        if (tabType == TabType.Msg) {
            fragment = recentContactsFragment;
            if (tv_msgTab != null && getActivity() != null) {
                tv_msgTab.setTextColor(getActivity().getResources().getColor(R.color.text_1A1A1A));
            }
            if (v_msgInds != null) {
                v_msgInds.setVisibility(View.VISIBLE);
            }
            if (tv_squareTab != null && getActivity() != null) {
                tv_squareTab.setTextColor(getActivity().getResources().getColor(R.color.common_color_9));
            }
            if (v_squareInds != null) {
                v_squareInds.setVisibility(View.INVISIBLE);
            }
        } else if (tabType == TabType.Square) {
            fragment = publicChatFragment;
            if (tv_msgTab != null && getActivity() != null) {
                tv_msgTab.setTextColor(getActivity().getResources().getColor(R.color.common_color_9));
            }
            if (v_msgInds != null) {
                v_msgInds.setVisibility(View.INVISIBLE);
            }
            if (tv_squareTab != null && getActivity() != null) {
                tv_squareTab.setTextColor(getActivity().getResources().getColor(R.color.text_1A1A1A));
            }
            if (v_squareInds != null) {
                v_squareInds.setVisibility(View.VISIBLE);
            }
        }
        selectFragment(fragment, tags[tabType.ordinal()]);
    }

    /**
     * fragment切换
     */
    public void selectFragment(Fragment f, String tag) {
        if (mFragments != null) {
            if (mFragments.contains(tag)) {
                // 当含有标记的fragment时
                for (String str : mFragments) {
                    if (tag.equals(str)) {
                        // 显示标记的
                        fragmentTransaction.show(fragments.get(tag));
                    } else {
                        // 隐藏不是标记的
                        fragmentTransaction.hide(fragments.get(str));
                    }
                }
            } else {
                // 当不含该标记的fragment的时隐藏其中已有的
                for (String str : mFragments) {
                    fragmentTransaction.hide(fragments.get(str));
                }
                // 添加该标记的fragment
                addNewFragment(f, tag);
            }
        } else {
            addNewFragment(f, tag);
        }
        fragmentTransaction.commitAllowingStateLoss();
//        ft.commit();
    }

    /**
     * 添加新的fragment
     *
     * @param fragment
     * @param tag
     */
    public void addNewFragment(Fragment fragment, String tag) {
        fragmentTransaction.add(R.id.fl_room_msg_container, fragment, tag);
        mFragments.add(tag);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        LogUtils.d(TAG, "dismiss");
    }
}
