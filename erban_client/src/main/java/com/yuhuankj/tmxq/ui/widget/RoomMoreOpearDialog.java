package com.yuhuankj.tmxq.ui.widget;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCore;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.pk.IPkCore;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.liveroom.imroom.pk.RoomPKModel;
import com.yuhuankj.tmxq.ui.liveroom.imroom.pk.setting.IMPkSettingActivity;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.activity.IMRoomBlackListActivity;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.activity.IMRoomManagerListActivity;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.base.presenter.BaseRoomDetailPresenter;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.activity.PkSettingActivity;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.activity.RoomBlackListActivity;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.activity.RoomManagerListActivity;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.dialog.LuckyWheelDialog;
import com.yuhuankj.tmxq.ui.liveroom.redpacket.RedPacketActivity;
import com.yuhuankj.tmxq.ui.share.ShareFansActivity;
import com.yuhuankj.tmxq.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * 房间底部新增选项弹框
 * <p>
 * 其中，pk、魅力值、管理员、黑名单、公屏需要判断用户角色，仅仅对管理员或者房主开放
 * 幸运大转盘需要接口luckyWheelSwitch开关控制其是否可显示，
 * 如果对普通用户来说，luckyWheelSwitch=false，意味着底部的+按钮需要隐藏
 * <p>
 * AvRoomDataManager.get().isRoomOwner()
 * AvRoomDataManager.get().isRoomAdmin()
 *
 * @author weihaitao
 * @date 2019/01/10
 */

public class RoomMoreOpearDialog extends BottomSheetDialog implements View.OnClickListener {

    private FragmentActivity context;
    private static final String TAG = "RoomMoreOpearDialog";

    private ImageView iv_pkOpera;
    private TextView tv_pkOpera;
    private ImageView iv_charmValueOpera;
    private TextView tv_charmValueOpera;
    private View ll_charmValueOpera;
    private ImageView iv_publicScreenOpera;
    private TextView tv_publicScreenOpera;

    private boolean luckyWheelSwitch;

    private boolean redPacketSwitch;

    private int roomType = RoomInfo.ROOMTYPE_HOME_PARTY;

    public void setRedPacketSwitch(boolean redPacketSwitch) {
        this.redPacketSwitch = redPacketSwitch;
    }

    public void setLuckyWheelSwitch(boolean luckyWheelSwitch) {
        this.luckyWheelSwitch = luckyWheelSwitch;
    }

    public void setHasPKOpened(boolean hasPKOpened) {
        this.hasPKOpened = hasPKOpened;
    }

    private boolean hasPKOpened = false;

    public void setCharmValueSwitch(boolean charmValueSwitch) {
        this.charmValueSwitch = charmValueSwitch;
    }

    private boolean charmValueSwitch = false;

    public RoomMoreOpearDialog(FragmentActivity context) {
        super(context, R.style.ErbanBottomSheetDialog);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_room_more_opear);
        setCanceledOnTouchOutside(true);

        initView();

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = context.getResources().getDisplayMetrics().heightPixels - (Utils.hasSoftKeys(context) ? Utils.getNavigationBarHeight(context) : 0);
        getWindow().setAttributes(params);
    }

    private OnLockyWheelGiftBtnClickListener onLockyWheelGiftBtnClickListener = null;

    private void initView() {
        iv_pkOpera = findViewById(R.id.iv_pkOpera);
        tv_pkOpera = findViewById(R.id.tv_pkOpera);
        findViewById(R.id.ll_pkOpera).setOnClickListener(this);

        iv_charmValueOpera = findViewById(R.id.iv_charmValueOpera);
        tv_charmValueOpera = findViewById(R.id.tv_charmValueOpera);
        ll_charmValueOpera = findViewById(R.id.ll_charmValueOpera);
        ll_charmValueOpera.setOnClickListener(this);

        findViewById(R.id.ll_adminOpera).setOnClickListener(this);
        findViewById(R.id.ll_blackListOpera).setOnClickListener(this);

        iv_publicScreenOpera = findViewById(R.id.iv_publicScreenOpera);
        tv_publicScreenOpera = findViewById(R.id.tv_publicScreenOpera);
        findViewById(R.id.ll_publicScreenOpera).setOnClickListener(this);
        findViewById(R.id.ll_luckyWheelOpera).setOnClickListener(this);
        findViewById(R.id.llRedPacket).setOnClickListener(this);
        findViewById(R.id.llInvatationFriend).setOnClickListener(this);
    }

    @Override
    public void show() {
        super.show();
        //公屏打开状态
        RoomInfo mServerRoomInfo = RoomServiceScheduler.getServerRoomInfo();
        if (null == mServerRoomInfo) {
            dismiss();
            return;
        }
        roomType = mServerRoomInfo.getType();
        initRoomOperaStatus(mServerRoomInfo);
    }

    private void initSingleAudioRoomOperaStatus(RoomInfo mServerRoomInfo) {
        //目前单人音频房间，仅根据后台控制大转盘，其他一律暂时隐藏
        if (mServerRoomInfo.getType() == RoomInfo.ROOMTYPE_SINGLE_AUDIO) {
            findViewById(R.id.ll_operaLine).setVisibility(View.GONE);

            luckyWheelSwitch = (RoomDataManager.get().getAdditional() != null && RoomDataManager.get().getAdditional().isBigWheelSwitch());
            findViewById(R.id.ll_luckyWheelOpera).setVisibility(luckyWheelSwitch ? View.VISIBLE : View.GONE);

            findViewById(R.id.llRedPacket).setVisibility(View.GONE);
            findViewById(R.id.llInvatationFriend).setVisibility(View.GONE);
            ll_charmValueOpera.setVisibility(View.GONE);
        }
    }

    private void initRoomOperaStatus(RoomInfo mServerRoomInfo) {
        if (mServerRoomInfo.getType() == RoomInfo.ROOMTYPE_HOME_PARTY) {
            //轰趴房，保持之前的外部设置参数逻辑不变，避免引入新的bug
            if (!AvRoomDataManager.get().isRoomOwner() && !AvRoomDataManager.get().isRoomAdmin()) {
                findViewById(R.id.ll_operaLine).setVisibility(View.GONE);
                ll_charmValueOpera.setVisibility(View.GONE);
            }
            if (!luckyWheelSwitch) {
                findViewById(R.id.ll_luckyWheelOpera).setVisibility(View.GONE);
            }
            if (!redPacketSwitch) {
                findViewById(R.id.llRedPacket).setVisibility(View.GONE);
            }

            if (AvRoomDataManager.get().isRoomOwner() || AvRoomDataManager.get().isRoomAdmin()) {
                final RoomInfo mCurrentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                if (mCurrentRoomInfo != null) {
                    int publicChatSwitch = mCurrentRoomInfo.getPublicChatSwitch();
                    tv_publicScreenOpera.setText(context.getResources().getString(publicChatSwitch == 0 ?
                            R.string.room_settings_public_screen_close : R.string.room_settings_public_screen_open));
                    iv_publicScreenOpera.setImageDrawable(context.getResources().getDrawable(publicChatSwitch == 0 ?
                            R.drawable.icon_room_opera_public_screen_close : R.drawable.icon_room_opera_public_screen_open));
                }

                iv_pkOpera.setImageDrawable(context.getResources().getDrawable(hasPKOpened ? R.drawable.icon_room_opera_pk_close : R.drawable.icon_room_opera_pk_open));
                tv_pkOpera.setText(context.getResources().getString(hasPKOpened ?
                        R.string.room_settings_pk_close : R.string.room_settings_pk_open));

                ll_charmValueOpera.setVisibility(charmValueSwitch ? View.VISIBLE : View.GONE);
            }
        }else if (mServerRoomInfo.getType() == RoomInfo.ROOMTYPE_SINGLE_AUDIO || mServerRoomInfo.getType() == RoomInfo.ROOMTYPE_NEW_AUCTION) {//目前单人音频房间，仅根据后台控制大转盘，其他一律暂时隐藏
            findViewById(R.id.ll_operaLine).setVisibility(View.GONE);

            luckyWheelSwitch = (RoomDataManager.get().getAdditional() != null && RoomDataManager.get().getAdditional().isBigWheelSwitch());
            findViewById(R.id.ll_luckyWheelOpera).setVisibility(luckyWheelSwitch ? View.VISIBLE : View.GONE);

            findViewById(R.id.llRedPacket).setVisibility(View.GONE);
            findViewById(R.id.llInvatationFriend).setVisibility(View.GONE);
            ll_charmValueOpera.setVisibility(charmValueSwitch ? View.VISIBLE : View.GONE);
        }else if (mServerRoomInfo.getType() == RoomInfo.ROOMTYPE_MULTI_AUDIO){
            if (!RoomDataManager.get().isRoomOwner() && !RoomDataManager.get().isRoomAdmin()) {
                findViewById(R.id.ll_operaLine).setVisibility(View.GONE);
                ll_charmValueOpera.setVisibility(View.GONE);
            }
            if (!luckyWheelSwitch) {
                findViewById(R.id.ll_luckyWheelOpera).setVisibility(View.GONE);
            }
            if (!redPacketSwitch) {
                findViewById(R.id.llRedPacket).setVisibility(View.GONE);
            }

            if (RoomDataManager.get().isRoomOwner() || RoomDataManager.get().isRoomAdmin()) {
                final RoomInfo mCurrentRoomInfo = RoomDataManager.get().getCurrentRoomInfo();
                if (mCurrentRoomInfo != null) {
                    int publicChatSwitch = mCurrentRoomInfo.getPublicChatSwitch();
                    tv_publicScreenOpera.setText(context.getResources().getString(publicChatSwitch == 0 ?
                            R.string.room_settings_public_screen_close : R.string.room_settings_public_screen_open));
                    iv_publicScreenOpera.setImageDrawable(context.getResources().getDrawable(publicChatSwitch == 0 ?
                            R.drawable.icon_room_opera_public_screen_close : R.drawable.icon_room_opera_public_screen_open));
                }

                iv_pkOpera.setImageDrawable(context.getResources().getDrawable(hasPKOpened ? R.drawable.icon_room_opera_pk_close : R.drawable.icon_room_opera_pk_open));
                tv_pkOpera.setText(context.getResources().getString(hasPKOpened ?
                        R.string.room_settings_pk_close : R.string.room_settings_pk_open));

                ll_charmValueOpera.setVisibility(charmValueSwitch ? View.VISIBLE : View.GONE);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_pkOpera:
                if (roomType == RoomInfo.ROOMTYPE_HOME_PARTY) {
                    if (hasPKOpened) {
                        CoreManager.getCore(IPkCore.class).cancelPK(AvRoomDataManager.get().mCurrentRoomInfo == null ?
                                0 : AvRoomDataManager.get().mCurrentRoomInfo.getRoomId());
                    } else {
                        context.startActivity(new Intent(context, PkSettingActivity.class));
                    }
                }else if (roomType == RoomInfo.ROOMTYPE_SINGLE_AUDIO || roomType == RoomInfo.ROOMTYPE_MULTI_AUDIO) {
                    if (hasPKOpened) {
                        new RoomPKModel().cancelPK(RoomDataManager.get().getCurrentRoomInfo() == null ?
                                0 : RoomDataManager.get().getCurrentRoomInfo().getRoomId());
                    } else {
                        context.startActivity(new Intent(context, IMPkSettingActivity.class));
                    }
                }
                break;
            case R.id.ll_charmValueOpera: {
                DialogManager mDialogManager = new DialogManager(context);
                mDialogManager.setCanceledOnClickOutside(false);
                mDialogManager.showOkCancelDialog(
                        context.getResources().getString(R.string.room_settings_charm_value_clear_tips), true,
                        new DialogManager.AbsOkDialogListener() {
                            @Override
                            public void onOk() {
                                if (RoomServiceScheduler.getCurrentRoomInfo() != null) {
                                        CoreManager.getCore(IAVRoomCore.class).clearCharm(
                                                CoreManager.getCore(IAuthCore.class).getCurrentUid() + "",
                                                RoomServiceScheduler.getCurrentRoomInfo().getRoomId() + "");
                                }
                            }
                        });
            }
            break;
            case R.id.ll_adminOpera:
                if (roomType == RoomInfo.ROOMTYPE_HOME_PARTY) {
                    if (!AvRoomDataManager.get().isRoomOwner(CoreManager.getCore(IAuthCore.class).getCurrentUid())) {
                        SingleToastUtil.showToast(context, R.string.only_admin_can_opera);
                        return;
                    }
                    RoomManagerListActivity.start(context);
                }else if (roomType == RoomInfo.ROOMTYPE_SINGLE_AUDIO || roomType == RoomInfo.ROOMTYPE_MULTI_AUDIO) {
                    if (!RoomDataManager.get().isRoomOwner(CoreManager.getCore(IAuthCore.class).getCurrentUid())) {
                        SingleToastUtil.showToast(context, R.string.only_admin_can_opera);
                        return;
                    }
                    IMRoomManagerListActivity.start(context);
                }
                break;
            case R.id.ll_blackListOpera:
                if (roomType == RoomInfo.ROOMTYPE_HOME_PARTY) {
                    RoomBlackListActivity.start(context);
                }else if (roomType == RoomInfo.ROOMTYPE_SINGLE_AUDIO || roomType == RoomInfo.ROOMTYPE_MULTI_AUDIO) {
                    IMRoomBlackListActivity.start(context);
                }
                break;
            case R.id.ll_publicScreenOpera: {
                if (roomType == RoomInfo.ROOMTYPE_HOME_PARTY) {
                    final RoomInfo mCurrentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                    IAuthCore authCore = CoreManager.getCore(IAuthCore.class);
                    if (mCurrentRoomInfo != null && null != authCore) {
                        int publicChatSwitch = mCurrentRoomInfo.getPublicChatSwitch();
                        CoreManager.getCore(IAVRoomCore.class).changeRoomMsgFilter(AvRoomDataManager.get().isRoomOwner(),
                                publicChatSwitch == 0 ? 1 : 0, authCore.getTicket(), authCore.getCurrentUid() + "",
                                0, 0, false, null);
                    }
                }else if (roomType == RoomInfo.ROOMTYPE_SINGLE_AUDIO || roomType == RoomInfo.ROOMTYPE_MULTI_AUDIO) {
                    if (RoomDataManager.get().getCurrentRoomInfo() != null) {
                        new BaseRoomDetailPresenter<>().updateRoomPublicScreenState(RoomDataManager.get().getCurrentRoomInfo().getPublicChatSwitch() == 0 ? 1 : 0);
                    }
                }
            }
            break;
            case R.id.ll_luckyWheelOpera:
                DialogManager mDialogManager = new DialogManager(context);
                mDialogManager.setCanceledOnClickOutside(false);
                mDialogManager.showProgressDialog(context, "加载中...");
                new LuckyWheelDialog(context, new LuckyWheelDialog.Callback() {
                    @Override
                    public void succed(LuckyWheelDialog dialog) {
                        //修复android.view.WindowManager$BadTokenException
                        //Unable to add window -- token android.os.BinderProxy@cdc34f0 is not valid;
                        // is your activity running?
                        if (null != context && context instanceof FragmentActivity) {
                            FragmentActivity fActivity = (FragmentActivity) context;
                            if (!fActivity.isFinishing()) {
                                dialog.show();
                                mDialogManager.dismissDialog();
                            }
                        }
                    }

                    @Override
                    public void failed(LuckyWheelDialog dialog) {
                        if (null != context && context instanceof FragmentActivity) {
                            FragmentActivity fActivity = (FragmentActivity) context;
                            //isFinishing()方法用来判断当前Activity是否处于finishing阶段，
                            // 常用在onPause()方法中用来判断Activity是简单的pausing还是completely finishing。
                            // 返回true表明Activity正在finishing，否则返回false。
                            if (!fActivity.isFinishing()) {
                                mDialogManager.dismissDialog();

                            }
                        }
                        SingleToastUtil.showToast("打开大转盘失败");
                    }

                    @Override
                    public void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, long targetUid, int number) {
                        RoomInfo currentRoomInfo = RoomServiceScheduler.getCurrentRoomInfo();
                        if (currentRoomInfo == null) {
                            return;
                        }
                        if (currentRoomInfo.getType() == RoomInfo.ROOMTYPE_HOME_PARTY) {
                            //非空，多人送礼
                            if (!ListUtils.isListEmpty(micMemberInfos)) {
                                List<Long> targetUids = new ArrayList<>();
                                for (MicMemberInfo micMemberInfo : micMemberInfos) {
                                    LogUtils.d(TAG, "onSendGiftBtnClick-uid:" + micMemberInfo.getUid());
                                    targetUids.add(micMemberInfo.getUid());
                                }
                                CoreManager.getCore(IGiftCore.class).sendRoomMultiGift(giftInfo.getGiftId(), targetUids,
                                        currentRoomInfo.getUid(), number, giftInfo.getGoldPrice());
                            } else if (targetUid > 0L) {
                                //为空，单人送礼
                                CoreManager.getCore(IGiftCore.class).sendRoomGift(giftInfo.getGiftId(),
                                        targetUid, currentRoomInfo.getUid(), number, giftInfo.getGoldPrice());
                            }
                        }
                    }

                    @Override
                    public void showRoomGiftDialog(long uid) {
                        if (null != onLockyWheelGiftBtnClickListener) {
                            onLockyWheelGiftBtnClickListener.showRoomGiftDialog(uid);
                        }
                    }
                });
                break;
            case R.id.llRedPacket:
                if (RoomServiceScheduler.getCurrentRoomInfo() != null) {
                    Intent intent = new Intent(context, RedPacketActivity.class);
                    intent.putExtra("roomId", RoomServiceScheduler.getCurrentRoomInfo().getRoomId());
                    context.startActivity(intent);
                }
                break;
            case R.id.llInvatationFriend:

                context.startActivity(new Intent(context, ShareFansActivity.class));

                RoomInfo mServerRoomInfo = RoomServiceScheduler.getServerRoomInfo();
                if (null != mServerRoomInfo && mServerRoomInfo.getType() == RoomInfo.ROOMTYPE_HOME_PARTY) {
                    //交友方，加号，邀请好友，点击统计
                    StatisticManager.get().onEvent(context,
                            StatisticModel.EVENT_ID_ROOM_MORE_INVITE_CLICK,
                            StatisticModel.getInstance().getUMAnalyCommonMap(context));
                }
                break;
            default:
                break;
        }
        dismiss();
    }

    public void setOnLockyWheelGiftBtnClickListener(OnLockyWheelGiftBtnClickListener listener) {
        this.onLockyWheelGiftBtnClickListener = listener;
    }

    public interface OnLockyWheelGiftBtnClickListener {
        void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos,
                                long targetUid, int number);

        void showRoomGiftDialog(long uid);
    }

}
