package com.yuhuankj.tmxq.ui.me.charge.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/5/6.
 */
public class PackageInfo implements Serializable {

    private String original_id;
    private String app_id;
    private String prepay_id;

    public String getOriginal_id() {
        return original_id;
    }

    public void setOriginal_id(String original_id) {
        this.original_id = original_id;
    }

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getPrepay_id() {
        return prepay_id;
    }

    public void setPrepay_id(String prepay_id) {
        this.prepay_id = prepay_id;
    }

    @Override
    public String toString() {
        return "app_id:"+this.getApp_id()+",prepay_id:"+this.getPrepay_id()+",original_id:"+this.getOriginal_id();
    }
}
