package com.yuhuankj.tmxq.ui.home.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jude.rollviewpager.adapter.StaticPagerAdapter;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.ButtonUtils;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.home.game.GameBannerEnitity;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

/**
 * Oppo渠道市场包对应的首页，banner的adapter
 *
 * @author weihaitao
 * @date 2019年5月21日 16:13:23
 */
public class OppoHomeBannerAdapter extends StaticPagerAdapter {
    private Context context;
    private List<GameBannerEnitity> bannerInfoList;
    private LayoutInflater mInflater;
    private OnGameBannerClickListener onGameBannerClickListener;
    private int bannerWidth = 0;
    private int bannerHeight = 0;

    public OppoHomeBannerAdapter(List<GameBannerEnitity> bannerInfoList, Context context) {
        this.context = context;
        this.bannerInfoList = bannerInfoList;
        mInflater = LayoutInflater.from(context);
        bannerWidth = DisplayUtility.getScreenWidth(context) - DisplayUtility.dp2px(context, 30);
        bannerHeight = bannerWidth * 90 / 346;
    }

    public List<GameBannerEnitity> getBannerInfoList() {
        return bannerInfoList;
    }

    public void setBannerInfoList(List<GameBannerEnitity> bannerInfoList) {
        this.bannerInfoList = bannerInfoList;
    }

    @Override
    public View getView(ViewGroup container, int position) {
        final GameBannerEnitity bannerEnitity = bannerInfoList.get(position);
        ImageView imgBanner = (ImageView) mInflater.inflate(R.layout.item_banner_oppo_home, container, false);
        if (GlideContextCheckUtil.checkContextUsable(context)) {
            ImageLoadUtils.loadBannerRoundBgWithOutPlaceHolder(context,
                    ImageLoadUtils.toThumbnailUrl(bannerWidth, bannerHeight, bannerEnitity.getBannerPic()),
                    imgBanner,
                    DisplayUtility.dp2px(context, 10),
                    R.drawable.bg_default_cover_round_placehold_size60);
        }
        imgBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick(v.getId())) {
                    return;
                }
                if (null != onGameBannerClickListener) {
                    onGameBannerClickListener.onGameBannerClicked(bannerEnitity);
                }
            }
        });
        return imgBanner;
    }

    @Override
    public int getCount() {
        if (bannerInfoList == null) {
            return 0;
        } else {
            return bannerInfoList.size();
        }
    }

    public void setOnGameBannerClickListener(OnGameBannerClickListener onGameBannerClickListener) {
        this.onGameBannerClickListener = onGameBannerClickListener;
    }


    public interface OnGameBannerClickListener {
        void onGameBannerClicked(GameBannerEnitity bannerEnitity);
    }
}
