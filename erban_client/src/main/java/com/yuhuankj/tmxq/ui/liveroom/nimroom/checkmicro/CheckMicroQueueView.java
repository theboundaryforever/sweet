package com.yuhuankj.tmxq.ui.liveroom.nimroom.checkmicro;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

public interface CheckMicroQueueView<T extends AbstractMvpPresenter> extends IMvpBaseView {

    void onRequestCheckMicroQueue(boolean isSuccess, String msg);

    void onRequestCleanMicroQueue(boolean isSuccess, String msg);
}
