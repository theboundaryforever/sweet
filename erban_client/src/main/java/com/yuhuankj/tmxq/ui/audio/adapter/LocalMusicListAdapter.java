package com.yuhuankj.tmxq.ui.audio.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.TimeUtils;
import com.tongdaxing.xchat_core.player.bean.LocalMusicInfo;
import com.yuhuankj.tmxq.R;

import java.util.List;

/**
 * Created by chenran on 2017/11/1.
 */

public class LocalMusicListAdapter extends RecyclerView.Adapter<LocalMusicListAdapter.ViewHolder> implements View.OnClickListener{
    private Context context;
    private List<LocalMusicInfo> localMusicInfos;

    public LocalMusicListAdapter(Context context) {
        this.context = context;
    }

    public void setLocalMusicInfos(List<LocalMusicInfo> localMusicInfos) {
        this.localMusicInfos = localMusicInfos;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(context).inflate(R.layout.list_item_local_music,parent,false);
        return new ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LocalMusicInfo localMusicInfo = localMusicInfos.get(position);
        if(localMusicInfo.getLocalId()>0 && GlideContextCheckUtil.checkContextUsable(context)){
            GlideApp.with(context)
                    .load(Uri.parse("content://media/external/audio/media/"+localMusicInfo.getLocalId()+"/albumart"))
                    .error(context.getResources().getDrawable(R.mipmap.icon_music_default_album))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .transforms(new RoundedCorners(context.getResources().getDimensionPixelOffset(R.dimen.common_cover_round_size))
                    )
                    .into(holder.iv_musicAlbumCover);
        }
        holder.tv_musicName.setText(localMusicInfo.getSongName());
        holder.tv_musicTotalTime.setText(TimeUtils.getFormatTimeString(localMusicInfo.getDuration(), "min:sec"));
        List<String> artistNames = localMusicInfo.getArtistNames();
        if(null != artistNames && artistNames.size()>0){
            holder.tv_singerName.setText(artistNames.get(0));
        }else{
            holder.tv_singerName.setText(context.getResources().getString(R.string.music_library_item_singer_unknow));
        }
        holder.iv_musicAdd.setTag(localMusicInfo);
        holder.iv_musicAdd.setVisibility(localMusicInfo.isInPlayerList() ? View.GONE : View.VISIBLE);
        holder.iv_musicAdded.setVisibility(localMusicInfo.isInPlayerList() ? View.VISIBLE : View.GONE);
        holder.iv_musicAdd.setOnClickListener(this);
    }

    @Override
    public int getItemCount() {
        if (localMusicInfos == null) {
            return 0;
        } else {
            return localMusicInfos.size();
        }
    }

    @Override
    public void onClick(View v) {
        LocalMusicInfo localMusicInfo = (LocalMusicInfo) v.getTag();
        if(null != localMusicInfo && null != onLocalMusicAddClickListener){
            onLocalMusicAddClickListener.onLocalMusicAddClicked(localMusicInfo);
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        ImageView iv_musicAdd;
        ImageView iv_musicAdded;
        ImageView iv_musicAlbumCover;
        TextView tv_musicName;
        TextView tv_singerName;
        TextView tv_musicTotalTime;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_musicAdd = (ImageView) itemView.findViewById(R.id.iv_musicAdd);
            iv_musicAdded = (ImageView) itemView.findViewById(R.id.iv_musicAdded);
            iv_musicAlbumCover = (ImageView) itemView.findViewById(R.id.iv_musicAlbumCover);
            tv_musicName = (TextView) itemView.findViewById(R.id.tv_musicName);
            tv_singerName = (TextView) itemView.findViewById(R.id.tv_singerName);
            tv_musicTotalTime = (TextView) itemView.findViewById(R.id.tv_musicTotalTime);
        }
    }

    public void setOnLocalMusicAddClickListener(OnLocalMusicAddClickListener onLocalMusicAddClickListener) {
        this.onLocalMusicAddClickListener = onLocalMusicAddClickListener;
    }

    private OnLocalMusicAddClickListener onLocalMusicAddClickListener;

    public interface OnLocalMusicAddClickListener{
        void onLocalMusicAddClicked(LocalMusicInfo localMusicInfo);
    }
}
