package com.yuhuankj.tmxq.ui.quickmating;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.AnimationDrawable;
import android.os.CountDownTimer;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.noober.background.view.BLTextView;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.im.custom.bean.QuickMatingBean;
import com.tongdaxing.xchat_core.im.custom.bean.QuickMatingFinishBean;
import com.tongdaxing.xchat_core.manager.agora.AgoraEngineManager;
import com.tongdaxing.xchat_core.room.face.FaceInfo;
import com.tongdaxing.xchat_core.room.face.FaceReceiveInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.base.fragment.BaseFragment;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.home.game.GameHomeModel;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.face.AnimFaceFactory;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.face.DynamicFaceAdapter;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.widget.ButtonItemFactory;
import com.yuhuankj.tmxq.ui.quickmating.bean.QMInfoBean;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.UIUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.annotation.Nonnull;

public class QuickMatingLinkingFragment extends BaseFragment {

    private TextView tvDouziCount;
    private TextView tvReport;
    private ImageView imvUser1, imvUser2;
    private TextView tvFlowerCount1, tvFlowerCount2;
    private TextView tvNick1, tvNick2;
    private BLTextView tvAttention1, tvAttention2;
    private TextView tvLinkMicroTip;
    private TextView tvTimer;
    private ImageView imvSilence;
    private ImageView imvLoudspeaker;
    private ImageView imvDisconnection;
    private FrameLayout fraGift;
    private LinearLayout llInteraction;
    private ImageView imvCaiquan;
    private ImageView imvLiao;
    private ImageView imvEmoji;
    private ImageView imvSendFlower;
    private ImageView imvFace;
    private LinearLayout rlOpenInfo;
    private TextView tvOpen;
    private TextView tvOpenTip;
    private RelativeLayout rlOpen;

    private QMInfoBean qmInfoBean;
    private QuickMatingBean quickMatingBean;
    private CountDownTimer timer;

    private QuickMatingModel qmModel;
    private long oppositeUid;//对方Uid
    private String oppositeNick;//对方昵称
    private String oppositeAvatar;//对方头像

    private long curTimerMills = 0;//当前计时时间

    private int receiveFlowerCount = 0;//本次连麦收到的花数量
    private boolean isLinkMating = false;//是否正在连麦中
    private boolean isShowingCaiquan = false;//是否正在显示猜拳
    private boolean isShowenFlowerTip = false;//是否显示了送花花费提示，只显示一次
    private EmojiDialog emojiDialog;

    private List<String> chatTips;//连麦提示语，每次进入连麦随机取一条
    private List<String> chatTopics;//连麦话题，每次点击话题夹随机取一条

    @Override
    public int getRootLayoutId() {
        return R.layout.activity_quick_mating_link_micro;
    }

    @Override
    public void onFindViews() {
        tvDouziCount = mView.findViewById(R.id.tvDouziCount);
        imvDisconnection = mView.findViewById(R.id.imvDisconnection);

        tvReport = mView.findViewById(R.id.tvReport);
        rlOpen = mView.findViewById(R.id.rlOpen);
        imvUser1 = mView.findViewById(R.id.imvUser1);
        imvUser2 = mView.findViewById(R.id.imvUser2);
        tvFlowerCount1 = mView.findViewById(R.id.tvFlowerCount1);
        tvFlowerCount2 = mView.findViewById(R.id.tvFlowerCount2);
        tvNick1 = mView.findViewById(R.id.tvNick1);
        tvNick2 = mView.findViewById(R.id.tvNick2);
        tvAttention1 = mView.findViewById(R.id.tvAttention1);
        tvAttention2 = mView.findViewById(R.id.tvAttention2);

        tvLinkMicroTip = mView.findViewById(R.id.tvLinkMicroTip);
        tvTimer = mView.findViewById(R.id.tvTimer);
        fraGift = mView.findViewById(R.id.fraGift);
        imvSilence = mView.findViewById(R.id.imvSilence);
        imvLoudspeaker = mView.findViewById(R.id.imvLoudspeaker);
        imvDisconnection = mView.findViewById(R.id.imvDisconnection);

        llInteraction = mView.findViewById(R.id.llInteraction);
        imvCaiquan = mView.findViewById(R.id.imvCaiquan);
        imvLiao = mView.findViewById(R.id.imvLiao);
        imvEmoji = mView.findViewById(R.id.imvEmoji);
        imvSendFlower = mView.findViewById(R.id.imvSendFlower);

        imvFace = mView.findViewById(R.id.imvFace);

        rlOpenInfo = mView.findViewById(R.id.rlOpenInfo);
        tvOpen = mView.findViewById(R.id.tvOpen);
        tvOpenTip = mView.findViewById(R.id.tvOpenTip);
    }

    @Override
    public void onSetListener() {
        imvDisconnection.setOnClickListener(this);
        imvSilence.setOnClickListener(this);
        imvLoudspeaker.setOnClickListener(this);

        imvCaiquan.setOnClickListener(this);
        imvLiao.setOnClickListener(this);
        imvEmoji.setOnClickListener(this);
        imvSendFlower.setOnClickListener(this);
        rlOpen.setOnClickListener(this);
        tvReport.setOnClickListener(this);
        tvAttention1.setOnClickListener(this);
        tvAttention2.setOnClickListener(this);
    }

    @Override
    public void initiate() {
        qmModel = new QuickMatingModel();

        chatTips = new ArrayList<>(8);
        chatTips.add("好听的声音千篇一律，有趣的灵魂万里挑一");
        chatTips.add("多交流情感，思想和想法，避免讲道理！");
        chatTips.add("不说话是会被挂断电话的哦！");
        chatTips.add("不要随意挂断别人电话哦，这样是很没有礼貌的！");
        chatTips.add("能够匹配到就是有缘分，请好好珍惜~");
        chatTips.add("把这次对话当作今天地第一次匹配来聊天吧！");
        chatTips.add("友好的聊天会收到对方的点赞！增加匹配成功的机率！");
        chatTips.add("和对方愉快地聊天，让Ta为你送朵花吧！");

        int romIndex = new Random().nextInt(chatTips.size());
        String chatTip = chatTips.get(romIndex);
        tvLinkMicroTip.setText(chatTip);

        if (getContext() != null) {
            isShowenFlowerTip = (boolean) SpUtils.get(getContext(), "QuickMatingLinkingFragment_ShowFlowerTip", false);
        }
    }

    //设置连麦数据
    public void setQuickMatingBean(QMInfoBean qmInfoBean, QuickMatingBean quickMatingBean) {
        this.quickMatingBean = quickMatingBean;
        this.qmInfoBean = qmInfoBean;
        if (quickMatingBean == null || isLinkMating || qmInfoBean == null) {
            return;
        }
        receiveFlowerCount = 0;
        isLinkMating = true;
        //进入频道
        new GameHomeModel().enterLinkMacro(quickMatingBean.getChannelId() + "");
        setSilence(false);
        setLoudSpeaker(false);
        chatTopics = new ArrayList<>(qmInfoBean.getTopicList());
        tvDouziCount.setText(qmInfoBean.getPeaNum() + "");
        long myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if (quickMatingBean.getUid() == myUid) {
            ImageLoadUtils.loadCircleImage(getContext(), quickMatingBean.getAvatar(), imvUser2, R.drawable.default_user_head);
            tvNick2.setText("我");
            tvAttention2.setEnabled(!quickMatingBean.isFans());
            tvAttention2.setText(quickMatingBean.isFans() ? "已被关注" : "等待关注");
            tvFlowerCount2.setText(quickMatingBean.getFlowerCount() + "");

            ImageLoadUtils.loadCircleImage(getContext(), quickMatingBean.getLinkAvatar(), imvUser1, R.drawable.default_user_head);
            tvNick1.setText(quickMatingBean.getLinkNick());
            tvAttention2.setEnabled(!quickMatingBean.isLinkIsFans());
            tvAttention1.setText(quickMatingBean.isLinkIsFans() ? "已关注" : "关注Ta");
            tvFlowerCount1.setText(quickMatingBean.getLinkFlowerCount() + "");

            oppositeNick = quickMatingBean.getLinkNick();
            oppositeAvatar = quickMatingBean.getLinkAvatar();
            oppositeUid = quickMatingBean.getLinkUid();
        } else {
            ImageLoadUtils.loadCircleImage(getContext(), quickMatingBean.getLinkAvatar(), imvUser2, R.drawable.default_user_head);
            tvNick2.setText("我");
            tvAttention2.setEnabled(!quickMatingBean.isLinkIsFans());
            tvAttention2.setText(quickMatingBean.isLinkIsFans() ? "已被关注" : "等待关注");
            tvFlowerCount2.setText(quickMatingBean.getLinkFlowerCount() + "");

            ImageLoadUtils.loadCircleImage(getContext(), quickMatingBean.getAvatar(), imvUser1, R.drawable.default_user_head);
            tvNick1.setText(quickMatingBean.getNick());
            tvAttention1.setEnabled(!quickMatingBean.isFans());
            tvAttention1.setText(quickMatingBean.isFans() ? "已关注" : "关注Ta");
            tvFlowerCount1.setText(quickMatingBean.getFlowerCount() + "");

            oppositeNick = quickMatingBean.getNick();
            oppositeAvatar = quickMatingBean.getAvatar();
            oppositeUid = quickMatingBean.getUid();
        }
        //如果互相关注了则无限畅聊
        if (quickMatingBean.isFans() && quickMatingBean.isLinkIsFans()) {
            tvTimer.setVisibility(View.INVISIBLE);
        } else {
            //启动计时器
            startTimer(240000);
        }
        int renewDouziCount = qmInfoBean.getUserMatchConfig().getRenewCostPea();
        int renewTime = qmInfoBean.getUserMatchConfig().getRenewTime() / 60;
        tvOpen.setText("通话续时" + renewTime + "分钟");
        tvOpenTip.setText("每次" + renewDouziCount + "甜豆");
    }

    //启动计时器
    private void startTimer(long mills) {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        tvTimer.setVisibility(View.VISIBLE);
        timer = new CountDownTimer(mills, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                curTimerMills = millisUntilFinished;
                tvTimer.setText(formatTime(millisUntilFinished));
                if (millisUntilFinished <= 60000) {
                    tvTimer.setTextColor(Color.parseColor("#FF6764"));
                    if (!rlOpen.isShown()) {
                        rlOpen.setVisibility(View.VISIBLE);
                        llInteraction.setVisibility(View.GONE);
                        if (tvTimer.isShown()) {
                            tvLinkMicroTip.setText("如果还想和对方聊天，需要支付甜豆续时哦，或者彼此关注进入不限时聊天");
                        }
                    }
                } else {
                    tvTimer.setTextColor(Color.WHITE);
                    if (!llInteraction.isShown()) {
                        rlOpen.setVisibility(View.GONE);
                        llInteraction.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFinish() {
                //退出连麦
                exitLinkMacro(true);
                if (null != quickMatingBean) {
                    finishQuickMating(quickMatingBean.getRecordId(), 2);
                }
            }
        };
        timer.start();
    }

    //设置是否静音
    private void setSilence(boolean isSilence) {
        imvSilence.setSelected(isSilence);
        AgoraEngineManager.get().closeOrOpenSpeaker(isSilence);
    }

    //设置听筒或扬声器
    private void setLoudSpeaker(boolean isLoudSpeaker) {
        imvLoudspeaker.setSelected(isLoudSpeaker);
        AgoraEngineManager.get().changeSpeaker(!isLoudSpeaker);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view == imvSilence) {
            setSilence(!imvSilence.isSelected());
        } else if (view == imvLoudspeaker) {
            setLoudSpeaker(!imvLoudspeaker.isSelected());
        } else if (view == imvDisconnection) {
            getDialogManager().showOkCancelDialog(getResources().getString(R.string.link_mic_exit_tips),
                    true, new DialogManager.OkCancelDialogListener() {
                        @Override
                        public void onCancel() {
                        }

                        @Override
                        public void onOk() {
                            exitLinkMacro(true);
                            if (null != quickMatingBean) {
                                finishQuickMating(quickMatingBean.getRecordId(), 1);
                            }
                        }
                    });
        } else if (view == tvAttention1) {
            attention(oppositeUid, 1);
        } else if (view == tvAttention2) {
            toast("相互关注就可以不限时畅聊~");
        } else if (view == tvReport) {
            showReport(oppositeUid, 1, getContext());
        } else if (view == imvCaiquan) {
            //统计-交友速配-猜拳
            StatisticManager.get().onEvent(getContext(),
                    StatisticModel.EVENT_ID_CLICK_QM_CAIQUAN,
                    StatisticModel.getInstance().getUMAnalyCommonMap(getContext()));
            int action = new Random().nextInt(3);
            quickMatingAction(3, action + "");
        } else if (view == imvLiao) {
            //统计-交友速配-话题夹
            StatisticManager.get().onEvent(getContext(),
                    StatisticModel.EVENT_ID_CLICK_QM_TALK_TOPIC,
                    StatisticModel.getInstance().getUMAnalyCommonMap(getContext()));
            String topic = getRomdomTopic();
            tvLinkMicroTip.setText(topic);
            quickMatingAction(4, topic);
        } else if (view == imvEmoji) {
            //统计-交友速配-表情
            StatisticManager.get().onEvent(getContext(),
                    StatisticModel.EVENT_ID_CLICK_QM_EMOJI,
                    StatisticModel.getInstance().getUMAnalyCommonMap(getContext()));
            if (emojiDialog != null && emojiDialog.isShowing()) {
                return;
            }
            emojiDialog = new EmojiDialog(getContext());
            emojiDialog.setOnFaceItemClickListener(new DynamicFaceAdapter.OnFaceItemClickListener() {
                @Override
                public void onFaceItemClick(FaceInfo faceInfo) {
                    emojiDialog.dismiss();
                    quickMatingAction(2, faceInfo.getId() + "");
                }
            });
            emojiDialog.show();
        } else if (view == imvSendFlower) {
            //统计-交友速配-送花
            StatisticManager.get().onEvent(getContext(),
                    StatisticModel.EVENT_ID_CLICK_QM_SEND_FLOWER,
                    StatisticModel.getInstance().getUMAnalyCommonMap(getContext()));
            quickMatingAction(1, "收到了一朵鲜花");
        } else if (view == rlOpen) {
            try {
                //统计-交友速配-续费
                StatisticManager.get().onEvent(getContext(),
                        StatisticModel.EVENT_ID_CLICK_QM_RENEW,
                        StatisticModel.getInstance().getUMAnalyCommonMap(getContext()));
                int renewDouziCount = qmInfoBean.getUserMatchConfig().getRenewCostPea();
                int renewTime = qmInfoBean.getUserMatchConfig().getRenewTime() / 60;
                String buyTimeTip = String.format("确定花费%d甜豆,续时%s分钟？", renewDouziCount, renewTime);
                DialogManager dialogManager = new DialogManager(getContext());
                dialogManager.showOkCancelDialog(buyTimeTip, true, new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onOk() {
                        if (null != quickMatingBean) {
                            quickMatingRenew(quickMatingBean.getRecordId());
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    //续费重新计时
    public void renewTime() {
        try {
            int renewTime = qmInfoBean.getUserMatchConfig().getRenewTime();
            startTimer(curTimerMills + (renewTime * 1000));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //连麦续费
    private void quickMatingRenew(int recordId) {
        getDialogManager().showProgressDialog(getContext(), "请稍候...");
        qmModel.quickMatingRenew(recordId, new HttpRequestCallBack<String>() {

            @Override
            public void onSuccess(String message, String response) {
                getDialogManager().dismissDialog();
                int renewDouziCount = qmInfoBean.getUserMatchConfig().getRenewCostPea();
                updateDouziCount(-renewDouziCount);
                renewTime();
            }

            @Override
            public void onFailure(int code, String msg) {
                getDialogManager().dismissDialog();
                toast(msg);
            }
        });
    }

    //连麦中互动的接口
    //交友互动接口 操作类型（1送花、2表情、3剪刀石头、4话题夹）
    private void quickMatingAction(int type, String content) {
        if (null == quickMatingBean) {
            return;
        }
        qmModel.quickMatingAction(type, quickMatingBean.getRecordId(), content, new HttpRequestCallBack<QuickMatingBean>() {

            @Override
            public void onSuccess(String message, QuickMatingBean response) {
                if (type == 1) {
                    showFlower(false);
                } else if (type == 2) {
                    showFace(Integer.parseInt(content));
                } else if (type == 3) {
                    showCaiquan(Integer.parseInt(content));
                } else if (type == 4) {
                    showTopic(content);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                toast(msg);
            }
        });
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            AgoraEngineManager.get().exitLinkMacro();
        }
    }

    //更新甜豆数量
    private void updateDouziCount(int peaNum) {
        if (qmInfoBean == null || getActivity() == null) {
            return;
        }
        int douziCount = qmInfoBean.getPeaNum() + peaNum;
        if (douziCount < 0) {
            douziCount = 0;
        }
        qmInfoBean.setPeaNum(douziCount);
        ((QuickMatingHomeActivity) getActivity()).updateDouziCount(qmInfoBean);
    }

    public void updateDouziCount() {
        if (qmInfoBean == null) {
            return;
        }
        tvDouziCount.setText(qmInfoBean.getPeaNum() + "");
    }

    //1送花、2表情、3剪刀石头、4话题夹
    public void receiveActionIM(int type, String content) {
        if (!isAdded()) {
            return;
        }
        if (type == 1) {
            showFlower(true);
        } else if (type == 2) {
            showFace(Integer.parseInt(content));
        } else if (type == 3) {
            showCaiquan(Integer.parseInt(content));
        } else if (type == 4) {
            showTopic(content);
        }
    }

    //显示送花动画
    public void showFlower(boolean isReceive) {
        Activity context = getActivity();
        if (context == null) {
            return;
        }
        if (!isReceive && !isShowenFlowerTip) {
            tvLinkMicroTip.setText("系统提示:赠送一次鲜花需要消耗10甜豆");
            SpUtils.put(context, "QuickMatingLinkingFragment_ShowFlowerTip", true);
            isShowenFlowerTip = true;
        }
        int giftWidth = imvFace.getMeasuredWidth();
        int giftHeight = imvFace.getMeasuredHeight();
        int screenWidth = ScreenUtil.getScreenWidth(context);
        int screenHeight = ScreenUtil.getScreenHeight(context);
        Point senderPoint = new Point(screenWidth / 2 - giftWidth / 2, UIUtil.dip2px(context, 25));
        final Point center = new Point();
        center.x = screenWidth / 2;
        center.y = screenHeight / 2;
        final ImageView imageView = new ImageView(context);
        int[] receiveLocation = new int[2];
        if (isReceive) {
            receiveFlowerCount++;
            String receiveFlowerCount2Str = tvFlowerCount2.getText().toString();
            int receiveFlowerCount2 = 0;
            try {
                receiveFlowerCount2 = Integer.parseInt(receiveFlowerCount2Str);
            } catch (Exception e) {
                receiveFlowerCount2 = receiveFlowerCount;
                e.printStackTrace();
            }
            receiveFlowerCount2++;
            tvFlowerCount2.setText(receiveFlowerCount2 + "");
            imvUser2.getLocationOnScreen(receiveLocation);
        } else {
            updateDouziCount(-10);

            String receiveFlowerCount1 = tvFlowerCount1.getText().toString();
            int receiveFlowerCount1Str = 0;
            try {
                receiveFlowerCount1Str = Integer.parseInt(receiveFlowerCount1);
            } catch (Exception e) {
                e.printStackTrace();
            }
            receiveFlowerCount1Str++;
            tvFlowerCount1.setText(receiveFlowerCount1Str + "");
            imvUser1.getLocationOnScreen(receiveLocation);
        }
        Point receivePoint = new Point(receiveLocation[0], receiveLocation[1]);

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(giftWidth, giftHeight);
        layoutParams.leftMargin = screenWidth / 2 - giftWidth / 2;
        layoutParams.topMargin = UIUtil.dip2px(context, 25);
        imageView.setLayoutParams(layoutParams);
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        fraGift.addView(imageView);
        imageView.setImageResource(R.drawable.ic_quick_mating_linking_flower);
        fraGift.setVisibility(View.VISIBLE);

        Keyframe kx0 = Keyframe.ofFloat(0f, 0);
        Keyframe kx1 = Keyframe.ofFloat(0.2f, center.x - senderPoint.x - giftWidth / 2);
        Keyframe kx2 = Keyframe.ofFloat(0.4f, center.x - senderPoint.x - giftWidth / 2);
        kx2.setInterpolator(new AccelerateDecelerateInterpolator());
        Keyframe kx3 = Keyframe.ofFloat(0.8f, center.x - senderPoint.x - giftWidth / 2);
        kx3.setInterpolator(new AccelerateDecelerateInterpolator());
        Keyframe kx4 = Keyframe.ofFloat(1f, receivePoint.x - senderPoint.x);
        kx4.setInterpolator(new AccelerateDecelerateInterpolator());

        Keyframe ky0 = Keyframe.ofFloat(0f, 0);
        Keyframe ky1 = Keyframe.ofFloat(0.2f, center.y - senderPoint.y - giftHeight / 2);
        Keyframe ky2 = Keyframe.ofFloat(0.4f, center.y - senderPoint.y - giftHeight / 2);
        ky2.setInterpolator(new AccelerateDecelerateInterpolator());
        Keyframe ky3 = Keyframe.ofFloat(0.8f, center.y - senderPoint.y - giftHeight / 2);
        ky3.setInterpolator(new AccelerateDecelerateInterpolator());
        Keyframe ky4 = Keyframe.ofFloat(1f, receivePoint.y - senderPoint.y);
        ky4.setInterpolator(new AccelerateDecelerateInterpolator());

        Keyframe ks0 = Keyframe.ofFloat(0f, 0.2f);
        Keyframe ks1 = Keyframe.ofFloat(0.2f, 1f);
        Keyframe ks2 = Keyframe.ofFloat(0.4f, 1.5f);
        Keyframe ks3 = Keyframe.ofFloat(0.6f, 2f);
        Keyframe ks4 = Keyframe.ofFloat(0.8f, 2f);
        Keyframe ks5 = Keyframe.ofFloat(1f, 0.2f);

        PropertyValuesHolder p0 = PropertyValuesHolder.ofKeyframe("translationX", kx0, kx1, kx2, kx3, kx4);
        PropertyValuesHolder p1 = PropertyValuesHolder.ofKeyframe("translationY", ky0, ky1, ky2, ky3, ky4);
        PropertyValuesHolder p2 = PropertyValuesHolder.ofKeyframe("scaleX", ks0, ks1, ks2, ks3, ks4, ks5);
        PropertyValuesHolder p3 = PropertyValuesHolder.ofKeyframe("scaleY", ks0, ks1, ks2, ks3, ks4, ks5);

        ObjectAnimator objectAnimator = ObjectAnimator.ofPropertyValuesHolder(imageView, p2, p3, p1, p0);
        objectAnimator.setDuration(4000);
        objectAnimator.start();

        objectAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                ViewGroup viewGroup = (ViewGroup) imageView.getParent();
                viewGroup.removeView(imageView);
            }
        });
    }

    //显示收到聊天主题
    public void showTopic(String content) {
        tvLinkMicroTip.setText(content);
    }

    //显示表情
    public void showFace(int faceId) {
        FaceReceiveInfo receiveInfo = new FaceReceiveInfo();
        receiveInfo.setFaceId(faceId);
        receiveInfo.setResultIndexes(new ArrayList<>());
        imvFace.setVisibility(View.VISIBLE);
        AnimFaceFactory.asynLoadAnim(receiveInfo, imvFace, getContext(), ScreenUtil.dip2px(80), ScreenUtil.dip2px(80));
    }

    //展示猜拳
    private void showCaiquan(int action) {
        if (!isAdded() || isShowingCaiquan) {
            return;
        }
        isShowingCaiquan = true;
        int actionResId = -1;
        if (action == 0) {
            actionResId = R.drawable.ic_quick_mating_linking_shitou;
        } else if (action == 1) {
            actionResId = R.drawable.ic_quick_mating_linking_bu;
        } else {
            actionResId = R.drawable.ic_quick_mating_linking_jiandao;
        }
        AnimationDrawable frameAnim = new AnimationDrawable();
        frameAnim.addFrame(getResources().getDrawable(R.drawable.ic_quick_mating_linking_shitou), 80);
        frameAnim.addFrame(getResources().getDrawable(R.drawable.ic_quick_mating_linking_bu), 80);
        frameAnim.addFrame(getResources().getDrawable(R.drawable.ic_quick_mating_linking_jiandao), 80);
        imvFace.setVisibility(View.VISIBLE);
        imvFace.setImageDrawable(frameAnim);
        frameAnim.start();
        int finalActionResId = actionResId;
        imvFace.postDelayed(new Runnable() {
            @Override
            public void run() {
                isShowingCaiquan = false;
                frameAnim.stop();
                imvFace.setImageResource(finalActionResId);
                imvFace.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        imvFace.setVisibility(View.GONE);
                    }
                }, 2000);
            }
        }, 1000);
    }

    public void showFinish() {
        if (qmInfoBean == null || getActivity() == null) {
            return;
        }
        isLinkMating = false;
        ((QuickMatingHomeActivity) getActivity()).showFinish(qmInfoBean, receiveFlowerCount
                , oppositeUid, oppositeNick, oppositeAvatar);
    }

    //对方关注了我
    public void attentionMe() {
        if (quickMatingBean == null) {
            return;
        }
        tvAttention2.setEnabled(false);
        tvAttention2.setText("已被关注");

        long myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if (quickMatingBean.getUid() == myUid) {
            quickMatingBean.setFans(true);
        } else {
            quickMatingBean.setLinkIsFans(true);
        }
        //如果互相关注了则无限畅聊
        if (quickMatingBean.isFans() && quickMatingBean.isLinkIsFans()) {
            tvTimer.setVisibility(View.INVISIBLE);
            if (timer != null) {
                timer.cancel();
            }
            rlOpen.setVisibility(View.GONE);
            llInteraction.setVisibility(View.VISIBLE);
        }
    }

    //关注用户
    private void attention(long uid, int type) {
        qmModel.attention(uid, type, new HttpRequestCallBack<String>() {

            @Override
            public void onSuccess(String message, String response) {
                toast("关注成功");
                tvAttention1.setEnabled(false);
                tvAttention1.setText("已关注");

                long myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
                if (null == quickMatingBean) {
                    return;
                }
                if (quickMatingBean.getUid() == myUid) {
                    quickMatingBean.setLinkIsFans(true);
                } else {
                    quickMatingBean.setFans(true);
                }
                //如果互相关注了则无限畅聊
                if (quickMatingBean.isFans() && quickMatingBean.isLinkIsFans()) {
                    tvTimer.setVisibility(View.INVISIBLE);
                    if (timer != null) {
                        timer.cancel();
                    }
                    rlOpen.setVisibility(View.GONE);
                    llInteraction.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                toast(msg);
            }
        });
    }

    //更新匹配数据
    public void updateQMData(QuickMatingFinishBean bean) {
        try {
            int lastBuyCount = bean.getOppLastBuyCount();
            int peaNum = bean.getOppPeaNum();
            int lastMatchCount = bean.getOppLastMatchCount();
            if (lastBuyCount < 0) {
                lastBuyCount = 0;
            }
            if (peaNum < 0) {
                peaNum = 0;
            }
            if (lastMatchCount < 0) {
                lastMatchCount = 0;
            }
            qmInfoBean.setPeaNum(peaNum);
            qmInfoBean.setUserBuyMatchCount(lastBuyCount);
            qmInfoBean.setUserFreeMatchCount(lastMatchCount);

            updateDouziCount();
            getHomeActivity().updateTimes(qmInfoBean, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //退出连麦
    public void exitLinkMacro(boolean isSelfDisconnect) {
        if (isLinkMating) {
            AgoraEngineManager.get().exitLinkMacro();
            //如果是无限连麦并且是被动挂断的，则匹配次数不减少
            if (!(tvTimer.isShown() && !isSelfDisconnect)) {
                getHomeActivity().updateTimes(qmInfoBean, -1);
            }
            showFinish();
        }
    }

    public QuickMatingHomeActivity getHomeActivity() {
        return ((QuickMatingHomeActivity) getActivity());
    }

    //强制结束连麦，一般供外部调用
    public void forceEndQuickMating(@Nonnull HttpRequestCallBack callBack) {
        try {
            AgoraEngineManager.get().exitLinkMacro();
            qmModel.finishQuickMating(quickMatingBean.getRecordId(), 1, callBack);
        } catch (Exception e) {
            e.printStackTrace();
            callBack.onFailure(200, "");
        }
    }

    private void finishQuickMating(int recordId, int operate) {
        qmModel.finishQuickMating(recordId, operate, new HttpRequestCallBack<String>() {

            @Override
            public void onSuccess(String message, String response) {
                try {
                    Json json = new Json(response);
                    int lastBuyCount = json.num("lastBuyCount");
                    int peaNum = json.num("peaNum");
                    int lastMatchCount = json.num("lastMatchCount");
                    if (lastBuyCount < 0) {
                        lastBuyCount = 0;
                    }
                    if (peaNum < 0) {
                        peaNum = 0;
                    }
                    if (lastMatchCount < 0) {
                        lastMatchCount = 0;
                    }
                    qmInfoBean.setPeaNum(peaNum);
                    qmInfoBean.setUserBuyMatchCount(lastBuyCount);
                    qmInfoBean.setUserFreeMatchCount(lastMatchCount);

                    getHomeActivity().updateDouziCount(qmInfoBean);
                    getHomeActivity().updateTimes(qmInfoBean, 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                toast(msg);
            }
        });
    }

    private void showReport(long uid, int type, Context context) {
        List<ButtonItem> buttons = new ArrayList<>();
        ButtonItem button1 = new ButtonItem("政治敏感", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                ButtonItemFactory.reportCommit(context, type, 1, uid);
            }
        });
        ButtonItem button2 = new ButtonItem("色情低俗", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                ButtonItemFactory.reportCommit(context, type, 2, uid);
            }
        });
        ButtonItem button3 = new ButtonItem("广告骚扰", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                ButtonItemFactory.reportCommit(context, type, 3, uid);
            }
        });
        ButtonItem button4 = new ButtonItem("人身攻击", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                ButtonItemFactory.reportCommit(context, type, 4, uid);
            }
        });
        buttons.add(button1);
        buttons.add(button2);
        buttons.add(button3);
        buttons.add(button4);
        DialogManager dialogManager = getDialogManager();
        if (dialogManager != null) {
            dialogManager.showCommonPopupDialog(buttons, "取消");
        }
    }

    //传入的数据为毫秒数
    public static String formatTime(long time) {
        String min = (time / (1000 * 60)) + "";
        String second = (time % (1000 * 60) / 1000) + "";
        if (min.length() < 2) {
            min = 0 + min;
        }
        if (second.length() < 2) {
            second = 0 + second;
        }
        return min + ":" + second;
    }

    //从提示语中随便获取一条
    private String getRomdomTopic() {
        if (qmInfoBean == null) {
            return "";
        }
        if (chatTopics == null || chatTopics.size() == 0) {
            chatTopics = new ArrayList<>(qmInfoBean.getTopicList());
        }
        if (chatTopics.size() == 0) {
            chatTopics.add(chatTips.get(3));
        }
        int romIndex = new Random().nextInt(chatTopics.size());
        String chatTip = chatTopics.get(romIndex);
        chatTopics.remove(romIndex);
        return chatTip;
    }

    public boolean isLinkMating() {
        return isLinkMating;
    }
}
