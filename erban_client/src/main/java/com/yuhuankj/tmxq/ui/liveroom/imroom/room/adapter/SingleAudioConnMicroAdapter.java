package com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.erban.libcommon.widget.MicroWaveView;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomQueueInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.listener.OnMicroItemClickListener;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

/**
 * 文件描述：新版本的多人语聊房的普通麦位的adapter
 *
 * @auther：zwk
 * @data：2019/6/6
 */
public class SingleAudioConnMicroAdapter extends RecyclerView.Adapter<SingleAudioConnMicroAdapter.MultiRoomMicroHolder> {

    private Context mContext;
    private OnMicroItemClickListener onMicroItemClickListener;


    public SingleAudioConnMicroAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MultiRoomMicroHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        MultiRoomMicroHolder holder = new MultiRoomMicroHolder(
                LayoutInflater.from(mContext).inflate(R.layout.item_rv_single_audio_conn_micro, viewGroup, false));
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MultiRoomMicroHolder holder, int position) {
        //普通麦位是从0开始的
        IMRoomQueueInfo roomQueueInfo = RoomDataManager.get().getRoomQueueMemberInfoByMicPosition(position);
        holder.waveview.stop();
        if (roomQueueInfo != null) {
            //麦位状态  禁麦和锁坑
            holder.ivMicroMute.setVisibility((roomQueueInfo.mRoomMicInfo != null && roomQueueInfo.mRoomMicInfo.isMicMute()) ? View.VISIBLE : View.GONE);
            holder.ivMicroState.setSelected((roomQueueInfo.mRoomMicInfo != null && roomQueueInfo.mRoomMicInfo.isMicLock()));
            if (roomQueueInfo.mChatRoomMember != null) {//
                holder.ivMicroState.setVisibility(View.GONE);
                holder.ivMicroAvatar.setVisibility(View.VISIBLE);
                holder.ivMicroHeadwear.setVisibility(View.VISIBLE);
                if (StringUtils.isNotEmpty(roomQueueInfo.mChatRoomMember.getHalo())){
                    holder.waveview.setColor(Color.parseColor(roomQueueInfo.mChatRoomMember.getHalo()));
                }else {
                    holder.waveview.setColor(0x55ffffff);
                }
                String nick = roomQueueInfo.mChatRoomMember.getNick();
                if (!TextUtils.isEmpty(nick) && nick.length() > 5) {
                    nick = mContext.getResources().getString(R.string.nick_length_max_six, nick.substring(0, 6));
                }
                holder.tvMicroTitle.setText(nick);
                holder.tvMicroPosition.setBackgroundResource(roomQueueInfo.mChatRoomMember.getGender() == 1 ? R.drawable.shape_circle_blue : R.drawable.shape_circle_red_f72732);
                ImageLoadUtils.loadImage(mContext, roomQueueInfo.mChatRoomMember.getHeadwearUrl(), holder.ivMicroHeadwear);
                ImageLoadUtils.loadCircleImage(mContext, roomQueueInfo.mChatRoomMember.getAvatar(), holder.ivMicroAvatar, R.drawable.ic_default_avatar);
            } else {
                holder.ivMicroState.setVisibility(View.VISIBLE);
                holder.ivMicroAvatar.setVisibility(View.GONE);
                holder.ivMicroHeadwear.setVisibility(View.GONE);
                holder.tvMicroPosition.setBackgroundResource(R.drawable.shape_circle_whire14);
                holder.tvMicroTitle.setText("号麦位");
            }
            holder.tvMicroPosition.setText(String.valueOf(position + 1));
        } else {
            holder.ivMicroState.setVisibility(View.VISIBLE);
            holder.ivMicroState.setSelected(true);
            holder.ivMicroMute.setVisibility(View.GONE);
            holder.ivMicroAvatar.setVisibility(View.GONE);
            holder.ivMicroHeadwear.setVisibility(View.GONE);
            holder.tvMicroPosition.setText(String.valueOf(position + 1));
            holder.tvMicroPosition.setBackgroundResource(R.drawable.shape_circle_whire14);
            holder.tvMicroTitle.setText("号麦位");
        }
        if (RoomDataManager.get().getCurrentRoomInfo() != null
                && RoomDataManager.get().getCurrentRoomInfo().getCharmOpen() == 1
                && RoomDataManager.get().getCurrentRoomInfo().getCharmSwitch() == 1) {
            if (roomQueueInfo != null && roomQueueInfo.mChatRoomMember != null) {
                changeCharmState(holder, false);
                holder.tvMicroCharm.setText(String.valueOf(roomQueueInfo.getValue()));
            } else {
                changeCharmState(holder, false);
            }
        } else {
            changeCharmState(holder, false);
        }
        holder.flMicro.setOnClickListener(v -> onMicroItemClick(roomQueueInfo, position));
    }

    /**
     * 魅力值状态
     *
     * @param holder
     * @param hasCharm
     */
    public void changeCharmState(MultiRoomMicroHolder holder, boolean hasCharm) {
        if (hasCharm) {
            if (holder.tvMicroCharm.getVisibility() == View.GONE) {
                holder.tvMicroCharm.setVisibility(View.VISIBLE);
            }
            if (holder.ivMicCharmHat.getVisibility() == View.GONE) {
                holder.ivMicCharmHat.setVisibility(View.VISIBLE);
            }
        } else {
            if (holder.tvMicroCharm.getVisibility() == View.VISIBLE) {
                holder.tvMicroCharm.setVisibility(View.GONE);
            }
            if (holder.ivMicCharmHat.getVisibility() == View.VISIBLE) {
                holder.ivMicCharmHat.setVisibility(View.GONE);
            }
        }
    }

    /**
     * 处理不同状态的 点击事件
     *
     * @param roomQueueInfo
     * @param position
     */
    private void onMicroItemClick(IMRoomQueueInfo roomQueueInfo, int position) {
        if (null == onMicroItemClickListener) {
            return;
        }
        onMicroItemClickListener.onMicroBtnClickListener(roomQueueInfo, position);
    }

    @Override
    public int getItemCount() {
        return 4;
    }

    public void setOnMicroItemClickListener(OnMicroItemClickListener onMicroItemClickListener) {
        this.onMicroItemClickListener = onMicroItemClickListener;
    }

    class MultiRoomMicroHolder extends RecyclerView.ViewHolder {
        private FrameLayout flMicro;
        private ImageView ivMicroAvatar;
        //显示麦位的：空的麦位和锁坑
        private ImageView ivMicroState;
        private ImageView ivMicroHeadwear;
        private MicroWaveView waveview;
        private ImageView ivMicroMute;
        private TextView tvMicroPosition;
        private TextView tvMicroTitle;
        private TextView tvMicroCharm;
        private ImageView ivMicCharmHat;


        public MultiRoomMicroHolder(@NonNull View itemView) {
            super(itemView);
            flMicro = itemView.findViewById(R.id.fl_room_micro_container);
            waveview = itemView.findViewById(R.id.waveview);
            ivMicroHeadwear = itemView.findViewById(R.id.iv_room_micro_headwear);
            ivMicroAvatar = itemView.findViewById(R.id.iv_room_micro_avatar);
            ivMicroState = itemView.findViewById(R.id.iv_room_micro_state);
            ivMicroMute = itemView.findViewById(R.id.iv_room_micro_mute);
            tvMicroPosition = itemView.findViewById(R.id.tv_room_micro_position);
            tvMicroTitle = itemView.findViewById(R.id.tv_room_micro_title);
            tvMicroCharm = itemView.findViewById(R.id.tv_room_mic_charm);
            ivMicCharmHat = itemView.findViewById(R.id.iv_room_micro_charm_hat);
        }
    }
}
