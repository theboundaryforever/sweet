package com.yuhuankj.tmxq.ui.liveroom.nimroom.dialog;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.http.SslError;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatDialog;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SignUtils;
import com.tongdaxing.erban.libcommon.utils.VersionUtil;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftType;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.liveroom.imroom.gift.RoomGiftDialog;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.gift.GiftDialog;
import com.yuhuankj.tmxq.ui.me.charge.ChargeActivity;
import com.yuhuankj.tmxq.ui.webview.JSInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 大转盘窗口
 */
public class LuckyWheelDialog extends AppCompatDialog implements GiftDialog.OnGiftDialogBtnClickListener {

    private final String TAG = LuckyWheelDialog.class.getSimpleName();
    private FragmentActivity context;
    private WebView webContent;
    //礼物列表弹窗
    private GiftDialog giftDialog;
    private RoomGiftDialog roomGiftDialog;
    //下一次打开礼物列表时是否刷新
    public static boolean isRefrshGift = false;

    public LuckyWheelDialog(FragmentActivity context, Callback callback) {
        super(context, R.style.dialog);
        this.context = context;
        this.callback = callback;
        init();
    }

    private void init() {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_lucky_wheel, null, false);
        setContentView(view);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = ScreenUtil.getScreenWidth(context);
        params.height = (int) (ScreenUtil.getScreenHeight(context) * 0.75);
        params.gravity = Gravity.BOTTOM;
        getWindow().setAttributes(params);
        setCanceledOnTouchOutside(true);

        webContent = view.findViewById(R.id.webContent);
        initWebView();

        //webContent.loadUrl("http://10.0.1.73:7000/ttyy/newluckdraw/index.html");
        webContent.loadUrl(UriProvider.getLuckyWheel());
    }

    private void initWebView() {
        LuckyWheelJSInterface luckyWheelJsInterface = new LuckyWheelJSInterface(webContent, context);
        webContent.addJavascriptInterface(luckyWheelJsInterface, "androidJsObj");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { // http 与 https 混合的页面
            webContent.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        webContent.setBackgroundColor(Color.WHITE);
        webContent.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                if (newProgress > 99) {
                    webContent.setBackgroundColor(Color.TRANSPARENT);
                    if (callback != null && !isShowing()) {
                        callback.succed(LuckyWheelDialog.this);
                    }
                }
            }
        });
        webContent.setWebViewClient(new WebViewClient(){
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                if (callback != null && !isShowing()) {
                    callback.failed(LuckyWheelDialog.this);
                }
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
                if (callback != null && !isShowing()) {
                    callback.failed(LuckyWheelDialog.this);
                }
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                super.onReceivedSslError(view, handler, error);
                if (callback != null && !isShowing()) {
                    callback.failed(LuckyWheelDialog.this);
                }
            }
        });
    }

    private Callback callback;

    /**
     * 需要留意点击引爆礼物墙的礼物跳转礼物列表的时候，礼物列表是否已经展示给用户了
     */
    private void clickBtnToShowGiftDialog() {
        RoomInfo currRoomInfo = RoomServiceScheduler.getCurrentRoomInfo();
        if (null == currRoomInfo) {
            return;
        }
        if (currRoomInfo.getType() == RoomInfo.ROOMTYPE_HOME_PARTY) {
            if (giftDialog == null) {
                giftDialog = new GiftDialog(getContext(), AvRoomDataManager.get().mMicQueueMemberMap, null);
                giftDialog.setRoomToShowLimitedExempteGift(true);
                giftDialog.setGiftDialogBtnClickListener(this);
                giftDialog.setRoomId(currRoomInfo.getRoomId());
                giftDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        giftDialog = null;
                    }
                });
            }
            if (!giftDialog.isShowing()) {
                giftDialog.show();
                giftDialog.showGiftListBy(GiftType.Package);
            }
        }
        if ((currRoomInfo.getType() == RoomInfo.ROOMTYPE_SINGLE_AUDIO||currRoomInfo.getType() == RoomInfo.ROOMTYPE_MULTI_AUDIO) && null != callback) {
            callback.showRoomGiftDialog(currRoomInfo.getUid());
        }
    }

    @Override
    public void onSendGiftBtnClick(GiftInfo giftInfo, long uid, int number) {
        LogUtils.d(TAG, "onSendGiftBtnClick-giftInfo:" + giftInfo + " uid:" + uid + " number:" + number);
        if (null != callback) {
            callback.onSendGiftBtnClick(giftInfo, null, uid, number);
        }
    }

    @Override
    public void onRechargeBtnClick() {
        //尝试修复https://bugly.qq.com/v2/crash-reporting/crashes/23dbf7aab1/23204?pid=1
        if (null != context) {
            ChargeActivity.start(context);
        }
    }

    @Override
    public void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, int number) {
        LogUtils.d(TAG, "onSendGiftBtnClick-giftInfo:" + giftInfo + " micMemberInfos.length:" + micMemberInfos.size() + " number:" + number);
        if (null != callback) {
            callback.onSendGiftBtnClick(giftInfo, micMemberInfos, 0L, number);
        }
    }

    public interface Callback {
        void succed(LuckyWheelDialog dialog);

        void failed(LuckyWheelDialog dialog);

        void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos,
                                long targetUid, int number);

        void showRoomGiftDialog(long uid);
    }

    public class LuckyWheelJSInterface extends JSInterface {

        public LuckyWheelJSInterface(WebView webView, FragmentActivity activity) {
            super(webView, activity);
        }

        /**
         * 减少金币
         */
        @JavascriptInterface
        public void reduceGold(int number) {
            //CoreManager.getCore(IPayCore.class).minusGold(number);
        }

        /**
         * 跳转到充值页
         */
        @JavascriptInterface
        public void jumpToCharge() {
            ChargeActivity.start(context);
        }

        /**
         * 刷新背包礼物
         */
        @JavascriptInterface
        public void refreshGift(String jsonStr) {
            //刷新背包礼物
            CoreManager.getCore(IGiftCore.class).requestGiftInfos();
//            try {
//                JSONObject json = new JSONObject(jsonStr);
//                JSONArray array = json.getJSONArray("gift");
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
            isRefrshGift = true;
            // gift:[{id:礼物id,num:礼物数量},{id:礼物id,num:礼物数量}]
        }

        /**
         * 跳转到礼物页
         */
        @JavascriptInterface
        public void jumpToGift() {
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    clickBtnToShowGiftDialog();
                    //房间-打开送礼界面
                    StatisticManager.get().onEvent(context,
                            StatisticModel.EVENT_ID_ROOM_OPEN_GIFT,
                            StatisticModel.getInstance().getUMAnalyCommonMap(context));
                }
            });
        }

        @JavascriptInterface
        public String getSn(String jsonStr) {
            String sn = "";
            if (TextUtils.isEmpty(jsonStr)) {
                return sn;
            }
            try {
                JSONObject jsonObject = new JSONObject(jsonStr);
                String url = jsonObject.getString("url");
                JSONObject jsonParams = jsonObject.getJSONObject("data");
                Map<String, String> params = new HashMap<>();
                for (Iterator<String> it = jsonParams.keys(); it.hasNext(); ) {
                    String key = it.next();
                    params.put(key, jsonParams.getString(key));
                }
                sn = SignUtils.getSign(url, params, System.currentTimeMillis() + "");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return sn;
        }

        /**
         * 获取uid，ticket，roomId
         */
        @JavascriptInterface
        public String getDrawPageInfo() {
            String uid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
            RoomInfo currentRoomInfo = RoomServiceScheduler.getCurrentRoomInfo();
            String version = VersionUtil.getLocalName(BasicConfig.INSTANCE.getAppContext());
            long roomId = 0;
            if (currentRoomInfo != null) {
                roomId = currentRoomInfo.getRoomId();
            }
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("uid", uid);
                jsonObject.put("ticket", ticket);
                jsonObject.put("roomId", roomId);
                jsonObject.put("appVersion", version);
                return jsonObject.toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return "";
        }
    }
}
