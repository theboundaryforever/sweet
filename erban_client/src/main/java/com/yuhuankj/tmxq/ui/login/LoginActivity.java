package com.yuhuankj.tmxq.ui.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.auth.AuthService;
import com.netease.nimlib.sdk.auth.ClientType;
import com.netease.nis.captcha.Captcha;
import com.netease.nis.captcha.CaptchaListener;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.ButtonUtils;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.erban.libcommon.utils.config.SpEvent;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.AccountInfo;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderMiniGameInvited;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;
import com.yuhuankj.tmxq.ui.widget.LoginTypeDialog;

import java.util.ArrayList;

/**
 * @author liaoxy
 * @Description:登录页
 * @date 2019/4/10 18:46
 */
public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "LoginActivity";

    private static final String KICK_OUT = "KICK_OUT";
    private static final String ydKet = "8088bebc6ce54e41892ec0d8bd88c543";
    public static final int Enter_Type_Login = 1;
    public static final int Enter_Type_Register = 2;

    private ViewPager vpContent;
    private View vPoint1, vPoint2, vPoint3;
    private View vPhoneLogin, vWechatLogin, vQQLogin;
    private TextView tvUserProtocol;
    private TextView tvPrivacyProtocol;

    private int clickId = 0;
    private boolean captchaShowing = false;
    private Captcha mCaptcha;

    public static void start(Context context, int type) {
        start(context, false, type);
    }

    public static void start(Context context, boolean kickOut, int type) {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.putExtra(KICK_OUT, kickOut);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        vpContent = (ViewPager) findViewById(R.id.vpContent);
        vPoint1 = findViewById(R.id.vPoint1);
        vPoint2 = findViewById(R.id.vPoint2);
        vPoint3 = findViewById(R.id.vPoint3);
        vPhoneLogin = findViewById(R.id.vPhoneLogin);
        vWechatLogin = findViewById(R.id.vWechatLogin);
        vQQLogin = findViewById(R.id.vQQLogin);
        tvUserProtocol = (TextView) findViewById(R.id.tvUserProtocol);
        tvPrivacyProtocol = (TextView) findViewById(R.id.tvPrivacyProtocol);
    }

    private void initListener() {
        vPhoneLogin.setOnClickListener(this);
        vWechatLogin.setOnClickListener(this);
        vQQLogin.setOnClickListener(this);
        tvUserProtocol.setOnClickListener(this);
        tvPrivacyProtocol.setOnClickListener(this);
        vpContent.addOnPageChangeListener(pageChangeListener);
    }

    private void initData() {
        setSwipeBackEnable(false);
        // 初始化引导图片列表
        ArrayList<View> views = new ArrayList<>();
        View vGuide1 = LayoutInflater.from(this).inflate(R.layout.layout_login_guide1, null);
        View vGuide2 = LayoutInflater.from(this).inflate(R.layout.layout_login_guide2, null);
        View vGuide3 = LayoutInflater.from(this).inflate(R.layout.layout_login_guide3, null);

        views.add(vGuide1);
        views.add(vGuide2);
        views.add(vGuide3);
        ViewAdapter vpAdapter = new ViewAdapter(views);
        vpContent.setAdapter(vpAdapter);

        vPoint1.setSelected(true);
        vPoint2.setSelected(false);
        vPoint3.setSelected(false);

        initCaptcha();
        onParseIntent();
    }

    private void initCaptcha() {
        mCaptcha = new Captcha(this);
        mCaptcha.setCaptchaId(ydKet);
//        mCaptcha.setDebug(BasicConfig.isDebug);
        mCaptcha.setCaListener(new CaptchaListener() {
            @Override
            public void onReady(boolean b) {
            }

            @Override
            public void closeWindow() {
                captchaShowing = false;
            }

            @Override
            public void onError(String s) {
                captchaShowing = false;
            }

            @Override
            public void onValidate(String result, String validate, String message) {
                captchaShowing = false;
                if (validate.length() > 0) {
                    loginAction();
                }
            }

            @Override
            public void onCancel() {
                captchaShowing = false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            //显示登录页，则清空第三方登录账号
            CoreManager.getCore(IAuthCore.class).setThirdUserInfo(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        onParseIntent();
    }

    private ViewPager.SimpleOnPageChangeListener pageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
        @Override
        public void onPageSelected(int position) {
            if (position == 0) {
                vPoint1.setSelected(true);
                vPoint2.setSelected(false);
                vPoint3.setSelected(false);
            } else if (position == 1) {
                vPoint1.setSelected(false);
                vPoint2.setSelected(true);
                vPoint3.setSelected(false);
            } else if (position == 2) {
                vPoint1.setSelected(false);
                vPoint2.setSelected(false);
                vPoint3.setSelected(true);
            }
        }
    };

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (ButtonUtils.isFastDoubleClick()) {
            // 进行点击事件后的逻辑操作
            return;
        }
        if (view == tvPrivacyProtocol) {
            CommonWebViewActivity.start(LoginActivity.this, UriProvider.getPrivacyProtocolUrl());
        } else if (view == tvUserProtocol) {
            CommonWebViewActivity.start(LoginActivity.this, UriProvider.getUserAgreementUrl());
            //点击协议
            StatisticManager.get().onEvent(this,
                    StatisticModel.EVENT_ID_CLICK_PROTOCAL_LOGIN,
                    StatisticModel.getInstance().getUMAnalyCommonMap(this));
        } else if (view == vPhoneLogin) {
            LoginTypeDialog loginTypeDialog = new LoginTypeDialog(this);
            loginTypeDialog.show();
            //点击登录
            StatisticManager.get().onEvent(this,
                    StatisticModel.EVENT_ID_CLICK_PHONE_LOGIN,
                    StatisticModel.getInstance().getUMAnalyCommonMap(this));
        } else if (view == vWechatLogin) {
            clickId = R.id.vWechatLogin;
            checkCaptcha();
            //微信登录
            StatisticManager.get().onEvent(this,
                    StatisticModel.EVENT_ID_CLICK_WEIXIN_LOGIN,
                    StatisticModel.getInstance().getUMAnalyCommonMap(this));
        } else if (view == vQQLogin) {
            clickId = R.id.vQQLogin;
            checkCaptcha();
            //QQ登录
            StatisticManager.get().onEvent(this,
                    StatisticModel.EVENT_ID_CLICK_QQ_LOGIN,
                    StatisticModel.getInstance().getUMAnalyCommonMap(this));
//            startActivity(new Intent(this, SupplyInfo1Activity.class));
        }
    }

    private void loginAction() {
        switch (clickId) {
            case R.id.vWechatLogin:
                getDialogManager().showProgressDialog(LoginActivity.this, "请稍后");
                CoreManager.getCore(IAuthCore.class).wxLogin();
                break;
            case R.id.vQQLogin:
                getDialogManager().showProgressDialog(LoginActivity.this, "请稍后");
                CoreManager.getCore(IAuthCore.class).qqLogin();
                break;
            default:
                break;
        }
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLoginFail(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogin(AccountInfo accountInfo) {
        getDialogManager().dismissDialog();
        //登录成功，清空小游戏邀请消息
        MsgViewHolderMiniGameInvited.clear();
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void onParseIntent() {
        if (getIntent().getBooleanExtra(KICK_OUT, false)) {
            int type = NIMClient.getService(AuthService.class).getKickedClientType();
            String client;
            switch (type) {
                case ClientType.Web:
                    client = "网页端";
                    break;
                case ClientType.Windows:
                    client = "电脑端";
                    break;
                case ClientType.REST:
                    client = "服务端";
                    break;
                default:
                    client = "移动端";
                    break;
            }
            getDialogManager().showOkAndLabelDialog("你的帐号在" + client + "登录，被踢出下线，请确定帐号信息安全", "下线通知", true, true, false, false, new DialogManager.OkDialogListener() {
                @Override
                public void onOk() {
                }
            });
        }
    }

    private void checkCaptcha() {
        if (captchaShowing) {
            return;
        }
        if (cleckLoginTimeNeedCaptcha()) {
            captchaShowing = true;
            mCaptcha.start();
            if (mCaptcha.checkParams()) {
                mCaptcha.Validate();
            } else {
                loginAction();
            }

        } else {
            loginAction();
        }
    }

    //5分钟内再次登录，要验证
    private boolean cleckLoginTimeNeedCaptcha() {
        long l = (Long) SpUtils.get(this, SpEvent.cleckLoginTime, 0L);
        long currentTimeMillis = System.currentTimeMillis();
        int i = 300000;
        if (BasicConfig.isDebug) {
            i = 30000;
        }
        if (currentTimeMillis - l > i) {
            SpUtils.put(this, SpEvent.cleckLoginTime, currentTimeMillis);
            return false;
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCaptcha != null) {
            mCaptcha.setCaListener(null);
            mCaptcha = null;
        }
    }

    @Override
    public boolean isStatusBarDarkFont() {
        return false;
    }
}
