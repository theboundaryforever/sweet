package com.yuhuankj.tmxq.ui.liveroom.nimroom.fragment;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

public interface HomePartView<T extends AbstractMvpPresenter> extends IMvpBaseView {

    void onGetRoomAttentionStatus(boolean isSuccess,String message, boolean status);
    void onRoomAttention(boolean isSuccess,String message);
    void onRoomCancelAttention(boolean isSuccess,String message);
}
