package com.yuhuankj.tmxq.ui.liveroom.imroom.room.dialog;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import com.donkingliang.labels.LabelsView;
import com.noober.background.view.BLTextView;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.AppDialogConstant;
import com.yuhuankj.tmxq.base.dialog.BaseAppMvpDialogFragment;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter.AuctionSettingPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IAuctionSettingView;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * 文件描述：拍卖设置弹框
 *
 * @auther：zwk
 * @data：2019/7/30
 */
@CreatePresenter(AuctionSettingPresenter.class)
public class AuctionSettingDialog extends BaseAppMvpDialogFragment<IAuctionSettingView, AuctionSettingPresenter> implements IAuctionSettingView, LabelsView.OnLabelSelectChangeListener, RadioGroup.OnCheckedChangeListener {
    private LinearLayout llContainer;
    private LabelsView lvAuctionProject;
    private RadioGroup rgAuctionTime;
    private BLTextView tvSave;
    private int days = 1;
    private String project = "CP";
    private boolean isAdmin = false;
    private long adminUid = 0;

    @Override
    public void initArguments(Bundle savedInstanceState) {
        super.initArguments(savedInstanceState);
        if (getArguments() != null){
            isAdmin = getArguments().getBoolean("isAdmin",false);
            adminUid = getArguments().getLong("adminUid",adminUid);
        }
    }

    @Override
    protected int getDialogStyle() {
        return AppDialogConstant.STYLE_CENTER_BACK_DARK;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_auction_setting;
    }

    @Override
    protected void initView(View view) {
        llContainer = view.findViewById(R.id.ll_auction_setting_container);
        lvAuctionProject = view.findViewById(R.id.lv_auction_setting_project);
        rgAuctionTime = view.findViewById(R.id.rg_auction_setting_time);
        tvSave = view.findViewById(R.id.bltv_auction_setting_save);
    }

    @Override
    protected void initListener(View view) {
        tvSave.setOnClickListener(v -> {
            if (getContext() != null) {
                getDialogManager().showProgressDialog(getContext(), "提交中...");
            }
            getMvpPresenter().joinRoomAuction(RoomDataManager.get().getCurrentRoomInfo() != null ? RoomDataManager.get().getCurrentRoomInfo().getRoomId() : 0, isAdmin,adminUid,days, project);
        });
    }

    @Override
    protected void initViewState() {
        String[] arrays = getResources().getStringArray(R.array.auction_project_list);
        ArrayList<String> projectLabels = new ArrayList<>(Arrays.asList(arrays));
        lvAuctionProject.setLabels(projectLabels);
        lvAuctionProject.setSelects(0);
        lvAuctionProject.setOnLabelSelectChangeListener(this);
        rgAuctionTime.setOnCheckedChangeListener(this);
    }

    @Override
    public void onLabelSelectChange(View label, String labelText, boolean isSelect, int position) {
        project = labelText;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (checkedId == R.id.rg_auction_setting_day) {
            days = 1;
        } else if (checkedId == R.id.rg_auction_setting_three_day) {
            days = 3;
        } else if (checkedId == R.id.rg_auction_setting_seven_day) {
            days = 7;
        } else if (checkedId == R.id.rg_auction_setting_thirty_day) {
            days = 30;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (llContainer != null) {
            llContainer.removeAllViews();
            llContainer = null;
        }
    }

    @Override
    public void showResultToast(String result) {
        toast(result);
        getDialogManager().dismissDialog();
        dismiss();
    }
}
