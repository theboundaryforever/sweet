package com.yuhuankj.tmxq.ui.liveroom;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.xchat_core.room.bean.CreateRoomBean;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

/**
 * 文件描述：创建房间入口adapter
 *
 * @auther：zwk
 * @data：2019/8/13
 */
public class CreateRoomEnterAdapter extends BaseQuickAdapter<CreateRoomBean, BaseViewHolder> {


    public CreateRoomEnterAdapter() {
        super(R.layout.item_rv_create_room_enter);
    }

    @Override
    protected void convert(BaseViewHolder helper, CreateRoomBean item) {
        helper.setText(R.id.tv_room_type_name,item.getName());
        ImageLoadUtils.loadImage(mContext,item.getUrl(),helper.getView(R.id.iv_room_type_logo),R.drawable.ic_default_avatar);
    }
}
