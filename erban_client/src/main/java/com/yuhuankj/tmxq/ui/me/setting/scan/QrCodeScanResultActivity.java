package com.yuhuankj.tmxq.ui.me.setting.scan;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.databinding.ActivityQrcodeScanLoginBinding;

@CreatePresenter(QrCodeScanResultPresenter.class)
public class QrCodeScanResultActivity extends BaseMvpActivity<QrCodeScanResultView, QrCodeScanResultPresenter>
        implements QrCodeScanResultView, View.OnClickListener {

    private final String TAG = QrCodeScanResultActivity.class.getSimpleName();

    private String qrcodeResult = null;
    private ActivityQrcodeScanLoginBinding qrcodeScanLoginBinding;

    public static void start(Context context, String qrcodeResult){
        Intent intent = new Intent(context, QrCodeScanResultActivity.class);
        intent.putExtra("qrcodeResult",qrcodeResult);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        qrcodeScanLoginBinding = DataBindingUtil.setContentView(this,R.layout.activity_qrcode_scan_login);
        qrcodeScanLoginBinding.setClick(this);
        qrcodeResult = getIntent().getStringExtra("qrcodeResult");
        initTitleBar(R.string.qrcode_scan_result_login_title, getResources().getColor(R.color.color_B3B3B3));
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_login:
                getMvpPresenter().loginForPcByQrCode(qrcodeResult);
                break;
            case R.id.tv_cancel:
                QrCodeScanerActivity.start(this);
                finish();
                break;
        }
    }

    @Override
    protected void onLeftClickListener() {
        QrCodeScanerActivity.start(this);
        super.onLeftClickListener();
    }

    @Override
    public void onQrCodeLogin(boolean isSuccess, String msg) {
        LogUtils.d(TAG,"onQrCodeLogin-isSuccess:"+isSuccess+" msg:"+msg);
        if(!TextUtils.isEmpty(msg)){
            toast(msg);
        }
        if(isSuccess){
            finish();
        }
    }
}
