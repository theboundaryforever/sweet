package com.yuhuankj.tmxq.ui.audio.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yuhuankj.tmxq.R;

import java.util.List;

/**
 * Created by chenran on 2017/11/1.
 */

public class MusicSearchHistoryAdapter extends RecyclerView.Adapter<MusicSearchHistoryAdapter.ViewHolder> implements View.OnClickListener{
    private Context context;
    private List<String> searchHistoryList;

    public MusicSearchHistoryAdapter(Context context) {
        this.context = context;
    }

    public void setSearchHistoryList(List<String> searchHistoryList) {
        this.searchHistoryList = searchHistoryList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(context).inflate(R.layout.list_item_search_history_music,parent,false);
        return new ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String searchContent = searchHistoryList.get(position);
        holder.tv_searchContent.setText(searchContent);
        holder.iv_delete.setTag(searchContent);
        holder.rl_contain.setTag(searchContent);
        holder.iv_delete.setOnClickListener(this);
        holder.rl_contain.setOnClickListener(this);
    }

    @Override
    public int getItemCount() {
        if (searchHistoryList == null) {
            return 0;
        } else {
            return searchHistoryList.size();
        }
    }

    @Override
    public void onClick(View v) {
        String deleteHistory = (String) v.getTag();
        switch (v.getId()){
            case R.id.iv_delete:
                if(!TextUtils.isEmpty(deleteHistory) && null != onMusicSearchHistoryDeleteClickListener){
                    onMusicSearchHistoryDeleteClickListener.onMusicSearchHistoryDeleted(deleteHistory);
                }
                break;
            case R.id.rl_contain:
                if(!TextUtils.isEmpty(deleteHistory) && null != onMusicSearchHistoryDeleteClickListener){
                    onMusicSearchHistoryDeleteClickListener.onMusicSearchHistorySelected(deleteHistory);
                }
                break;
        }

    }

    static class ViewHolder extends RecyclerView.ViewHolder{

        View rl_contain;
        ImageView iv_delete;
        TextView tv_searchContent;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_delete = itemView.findViewById(R.id.iv_delete);
            rl_contain = itemView.findViewById(R.id.rl_contain);
            tv_searchContent = itemView.findViewById(R.id.tv_searchContent);
        }
    }

    public void setOnMusicSearchHistoryDeleteClickListener(OnMusicSearchHistoryDeleteClickListener onMusicSearchHistoryDeleteClickListener) {
        this.onMusicSearchHistoryDeleteClickListener = onMusicSearchHistoryDeleteClickListener;
    }

    private OnMusicSearchHistoryDeleteClickListener onMusicSearchHistoryDeleteClickListener;

    public interface OnMusicSearchHistoryDeleteClickListener{
        void onMusicSearchHistoryDeleted(String deleteHistory);
        void onMusicSearchHistorySelected(String history);
    }
}
