package com.yuhuankj.tmxq.ui.room.adapter;


import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

/**
 * Created by Administrator on 2018/5/18.
 */

public class HotHeader extends LinearLayout {

    private TextView tvRoomName, tvRoomName1;
    private ImageView imvRunning, imvRunning1;
    private TextView tvRoomInfo, tvRoomInfo1;
    private ImageView imvIcon, imvIcon1;
    private RelativeLayout rlFirst, rlSecond;
    private LinearLayout llHotHeader;
    private ImageView imvRightArrow;
    private Context context;

    public HotHeader(Context context) {
        this(context, null);
    }

    public HotHeader(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        View inflate = LayoutInflater.from(context).inflate(R.layout.item_header_hot, this);
        this.context = context;
        tvRoomName = inflate.findViewById(R.id.tvRoomName);
        imvRunning = inflate.findViewById(R.id.imvRunning);
        imvIcon = inflate.findViewById(R.id.imvIcon);
        tvRoomInfo = inflate.findViewById(R.id.tvRoomInfo);

        tvRoomName1 = inflate.findViewById(R.id.tvRoomName1);
        imvRunning1 = inflate.findViewById(R.id.imvRunning1);
        tvRoomInfo1 = inflate.findViewById(R.id.tvRoomInfo1);
        imvIcon1 = inflate.findViewById(R.id.imvIcon1);

        rlFirst = inflate.findViewById(R.id.rlFirst);
        rlSecond = inflate.findViewById(R.id.rlSecond);
        imvRightArrow = inflate.findViewById(R.id.imvRightArrow);
        llHotHeader = inflate.findViewById(R.id.llHotHeader);
    }

    public void setData(Json dataFirst, Json dataSecond) {
        if (dataFirst == null) {
            llHotHeader.setVisibility(GONE);
            return;
        }
        llHotHeader.setVisibility(VISIBLE);
        rlFirst.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                int roomType = RoomInfo.ROOMTYPE_HOME_PARTY;
                if (dataFirst.has("type")) {
                    roomType = dataFirst.num("type");
                }
                RoomServiceScheduler.getInstance().enterRoom(context, dataFirst.num_l("uid"),
                        roomType);
                //热门推荐-前面第一个大推荐位
                StatisticManager.get().onEvent(context,
                        StatisticModel.EVENT_ID_HOT_RECOMMEND_ROOM,
                        StatisticModel.getInstance().getUMAnalyCommonMap(context));
                LogUtils.d("HotHeader", "onClick-热门推荐-前面第一个大推荐位");
            }
        });
        ImageLoadUtils.loadImage(context, dataFirst.str("avatar"), imvIcon);
        tvRoomName.setText(dataFirst.str("title"));
        tvRoomInfo.setText(dataFirst.str("onlineNum") + "人在线");
        GlideApp.with(context).asGif().load(R.drawable.anim_room_running_white).diskCacheStrategy(DiskCacheStrategy.ALL).into(imvRunning);

        if (dataSecond == null) {
            rlSecond.setVisibility(VISIBLE);
            imvRunning1.setVisibility(GONE);
            tvRoomName1.setText("推荐位");
            tvRoomInfo1.setText("立即上热门");
            imvRightArrow.setVisibility(VISIBLE);
            imvIcon1.setImageResource(R.drawable.ic_hot_room_emply_recommand);
            rlSecond.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    CommonWebViewActivity.start(context, UriProvider.JAVA_WEB_URL + "/ttyy/hotroom/index.html");
                }
            });
        } else {
            rlSecond.setVisibility(VISIBLE);
            rlSecond.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int roomType = RoomInfo.ROOMTYPE_HOME_PARTY;
                    if (dataSecond.has("type")) {
                        roomType = dataSecond.num("type");
                    }
                    RoomServiceScheduler.getInstance().enterRoom(context, dataSecond.num_l("uid"),
                            roomType);
                    //热门推荐-前面第二个大推荐位
                    StatisticManager.get().onEvent(context,
                            StatisticModel.EVENT_ID_HOT_RECOMMEND_ROOM,
                            StatisticModel.getInstance().getUMAnalyCommonMap(context));
                    LogUtils.d("HotHeader", "onClick-热门推荐-前面第二个大推荐位");
                }
            });
            ImageLoadUtils.loadImage(context, dataSecond.str("avatar"), imvIcon1);
            tvRoomName1.setText(dataSecond.str("title"));
            tvRoomInfo1.setText(dataSecond.str("onlineNum").concat("人在线"));
            imvRunning1.setVisibility(VISIBLE);
            imvRightArrow.setVisibility(GONE);
//            GlideApp.with(context).asGif().load(R.drawable.anim_room_running_white).diskCacheStrategy(DiskCacheStrategy.ALL).into(imvRunning1);
        }


    }
}
