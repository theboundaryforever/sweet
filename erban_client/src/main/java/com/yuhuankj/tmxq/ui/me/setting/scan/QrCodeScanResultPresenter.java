package com.yuhuankj.tmxq.ui.me.setting.scan;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.match.MicroMatchPool;

import java.util.List;
import java.util.Map;

public class QrCodeScanResultPresenter extends AbstractMvpPresenter<QrCodeScanResultView> {

    private final String TAG = QrCodeScanResultPresenter.class.getSimpleName();

    public QrCodeScanResultPresenter() {

    }


    public void loginForPcByQrCode(String uuid){
        LogUtils.d(TAG,"loginForPcByQrCode-uuid:"+uuid);
        Map<String, String> params = OkHttpManager.getDefaultParam();
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        params.put("uid", uid + "");
        params.put("uuid", uuid);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket()+"");
        OkHttpManager.getInstance().postRequest(UriProvider.getQrCodeLoginPcUrl(), params, new OkHttpManager.MyCallBack<ServiceResult<List<MicroMatchPool>>>() {
            @Override
            public void onError(Exception e) {
                LogUtils.d(TAG, "getMicroMatchPool-onError");
                e.printStackTrace();
                getMvpView().onQrCodeLogin(false,e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<List<MicroMatchPool>> response) {
                LogUtils.d(TAG, "getMicroMatchPool-onResponse");
                getMvpView().onQrCodeLogin(200 == response.getCode(),response.getMessage());
            }
        });

    }

}
