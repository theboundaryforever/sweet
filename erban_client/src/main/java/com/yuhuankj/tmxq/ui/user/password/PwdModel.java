package com.yuhuankj.tmxq.ui.user.password;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.codec.DESUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;

import java.util.Map;

/**
 * 密码model层
 */
public class PwdModel extends BaseMvpModel {

    private String DESAndBase64(String psw) {
        String pwd = "";
        try {
            pwd = DESUtils.DESAndBase64Encrypt(psw);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pwd;
    }

    //设置密码
    public void setPwd(String password, String confirmPwd, int type, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("confirmPwd", DESAndBase64(confirmPwd));
        params.put("password", DESAndBase64(password));
        params.put("type", String.valueOf(type));
        OkHttpManager.getInstance().postRequest(UriProvider.setPwd(), params, callBack);
    }

    //修改密码
    public void modifyPwd(String oldPwd, String password, String confirmPwd, int type, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("oldPwd", DESAndBase64(oldPwd));
        params.put("confirmPwd", DESAndBase64(confirmPwd));
        params.put("password", DESAndBase64(password));
        params.put("type", String.valueOf(type));
        OkHttpManager.getInstance().postRequest(UriProvider.modifyPwd(), params, callBack);
    }

    //重置密码
    public void resetPwd(String password, String phone, String smsCode, int type, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("password", DESAndBase64(password));
        params.put("phone", phone);
        params.put("smsCode", smsCode);
        params.put("type", String.valueOf(type));
        OkHttpManager.getInstance().postRequest(UriProvider.resetPwd(), params, callBack);
    }

    //检测提现密码是否正确
    public void checkPayPwd(String payPassword, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("payPassword", DESAndBase64(payPassword));
        OkHttpManager.getInstance().postRequest(UriProvider.checkPwd(), params, callBack);
    }
}
