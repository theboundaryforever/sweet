package com.yuhuankj.tmxq.ui.liveroom.nimroom.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.uuzuche.lib_zxing.DisplayUtil;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.UIUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by huangmeng1 on 2018/1/13.
 */

public class WaveView extends View {
    private Paint paint;
    private List<Float> radus=new ArrayList<>();
    private List<Integer> alphaList=new ArrayList<>();
    private boolean isStarting = false;
    private int second;
    public WaveView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public WaveView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public WaveView(Context context) {
        super(context);
        init();
    }

    private int color = 0x77ffffff;

    public void setColor(int color) {
        this.color = color;
        if (null != paint) {
            paint.setColor(color);
        }
    }

    int width = 0;
    int height = 0;

    protected void init() {
        paint = new Paint();
        paint.setColor(color);
        radus.add(customRadus);
        alphaList.add(180);
        middleRadus = DisplayUtil.dip2px(getContext(), 40);
        addRadus = UIUtil.dip2px(getContext(), addRadus);
        delRadus = UIUtil.dip2px(getContext(), delRadus);

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        width = getWidth() / 2;
        height = getHeight() / 2;
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!isStarting) {
            return;
        }
        second++;

        //循环每个圈，逐层增加半径，减小透明度
        for (int i = 0; i < radus.size(); i++) {
            paint.setAlpha(alphaList.get(i));
            canvas.drawCircle(width, height, radus.get(i), paint);
            if (radus.get(i) < middleRadus) {
                radus.set(i,radus.get(i)+0.5f);
            }
            if (alphaList.get(i)>alphaMax){
                alphaList.set(i,alphaList.get(i)-alphaDel);
            }
        }

        //最外面一圈半径为35dp的时候，增加一圈
        if (radus.get(0) == addRadus) {
            radus.add(0,customRadus);
            alphaList.add(0,180);
        }

        //最外面一圈半径为50的时候，减少一圈
        if (radus.get(0) == delRadus && radus.size() > 2) {
            radus.remove(1);
            alphaList.remove(1);
        }
        if (second==stopRadus){
            stop();
        }
        invalidate();
    }

    public void start() {
        second=0;
        isStarting = true;
        invalidate();
    }

    public void stop() {
        radus.clear();
        alphaList.clear();
        alphaList.add(180);
        second=0;
        radus.add(customRadus);
        isStarting = false;
    }


    public void setCustomRadus(float customRadus) {
        this.customRadus = customRadus;
    }

    public void setAddRadus(int addRadus) {
        this.addRadus = addRadus;
    }

    public void setDelRadus(int delRadus) {
        this.delRadus = delRadus;
    }

    public void setStopRadus(int stopRadus) {
        this.stopRadus = stopRadus;
    }

    public void setMiddleRadus(int middleRadus) {
        this.middleRadus = middleRadus;
    }

    public void setAlphaMax(int alphaMax) {
        this.alphaMax = alphaMax;
    }

    public void setAlphaDel(int alphaDel) {
        this.alphaDel = alphaDel;
    }

    protected float customRadus = 50f;
    protected int addRadus = 35;
    protected int delRadus = 50;
    protected int stopRadus = 50;
    protected int middleRadus = 40;
    protected int alphaMax = 10;
    protected int alphaDel = 1;
}
