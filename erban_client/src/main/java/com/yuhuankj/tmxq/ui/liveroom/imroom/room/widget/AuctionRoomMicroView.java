package com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.juxiao.library_utils.DisplayUtils;
import com.noober.background.view.BLTextView;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.erban.libcommon.widget.MicroWaveView;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.im.custom.bean.RoomCharmAttachment;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMember;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomQueueInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.face.FaceReceiveInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter.AuctionRoomMicroAdapter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter.RoomAuctionAdapter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.listener.OnMicroItemClickListener;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.face.AnimFaceFactory;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

/**
 * 文件描述：多人语聊房的麦位布局
 *
 * @auther：zwk
 * @data：2019/6/3
 */
public class AuctionRoomMicroView extends AbstractMicroView {
    private ImageView ivMoreFunction;
    //房间信息
    private TextView tvRoomTitle;
    private TextView tvErbanId;
    private TextView tvOnlinePeople;
    private ImageView ivRoomLock;
    private ImageView ivFirst, ivSecond;
    private RelativeLayout rlRandList;
    private BLTextView bltAnnouncement;
    private TextView bltAttention;
    private ImageView ivShare;
    //房主麦位信息
    private ImageView ivOwnerState;
    private MicroWaveView bMicWave;
    private FrameLayout flOwnerMic;
    private ImageView ivOwnerAvatar;
    //    private ImageView ivOwnerFace;
    private ImageView ivOwnerHeadwear;
    //普通麦位
    private RecyclerView rvRoomMicro;
    private AuctionRoomMicroAdapter auctionRoomMicroAdapter;
    private int faceWidth;
    private int auctionFaceWidth;
    private int screenWidth;
    private TextView tvAuctionOperate;
//    private TextView tvAuctionInfo;
    //竞拍列表
    private RecyclerView rvRoomAuction;
    private RoomAuctionAdapter roomAuctionAdapter;


    public AuctionRoomMicroView(Context context) {
        super(context);
    }

    public AuctionRoomMicroView(Context context, AttributeSet attr) {
        super(context, attr);
    }

    @Override
    protected int getMicroLayoutId() {
        return R.layout.view_auction_micro;
    }

    @Override
    public void init(Context context) {
        faceWidth = DisplayUtils.dip2px(context, 70);
        auctionFaceWidth = DisplayUtils.dip2px(context, 100);
        screenWidth = DisplayUtils.getScreenWidth(getContext());
        super.init(context);
        ivMoreFunction = findViewById(R.id.iv_room_more_function);
        tvRoomTitle = findViewById(R.id.iv_room_title);
        tvErbanId = findViewById(R.id.tv_room_id);
        tvOnlinePeople = findViewById(R.id.tv_room_online_people);
        ivRoomLock = findViewById(R.id.iv_room_lock);
        ivFirst = findViewById(R.id.iv_wealth_rank_avatar);
        ivSecond = findViewById(R.id.iv_charm_rank_avatar);
        rlRandList = findViewById(R.id.rl_room_rank_list);
        bltAnnouncement = findViewById(R.id.bltv_room_announcement);
        bltAttention = findViewById(R.id.bltv_room_attention);
        ivShare = findViewById(R.id.iv_room_share);


        ivOwnerState = findViewById(R.id.iv_room_owner_offline);
        flOwnerMic = findViewById(R.id.fl_room_owner_avatar);
        bMicWave = findViewById(R.id.bmwv_multi_room_boss_wave);
        ivOwnerAvatar = findViewById(R.id.iv_room_owner_avatar);
//        ivOwnerFace = findViewById(R.id.iv_room_owner_face);
        ivOwnerHeadwear = findViewById(R.id.iv_room_owner_headwear);

        rvRoomMicro = findViewById(R.id.rv_room_normal_micro);
        if (rvRoomMicro.getItemAnimator() != null) {
            ((DefaultItemAnimator) rvRoomMicro.getItemAnimator()).setSupportsChangeAnimations(false);
        }
        tvAuctionOperate = findViewById(R.id.tv_auction_operate);
//        tvAuctionInfo = findViewById(R.id.tv_current_auction_info);
        rvRoomAuction = findViewById(R.id.rv_room_auction_position);
        rvRoomAuction.setLayoutManager(new RecyclerViewNoBugLinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        roomAuctionAdapter = new RoomAuctionAdapter(context);
        rvRoomAuction.setAdapter(roomAuctionAdapter);
        initState(context);
    }

    private void initState(Context context) {
        bMicWave.stop();
        auctionRoomMicroAdapter = new AuctionRoomMicroAdapter(context);
        rvRoomMicro.setAdapter(auctionRoomMicroAdapter);
        rvRoomMicro.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                rvRoomMicro.postDelayed(() -> calculateMicCenterPoint(), 500);
                rvRoomMicro.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

    }

    @Override
    protected void calculateMicCenterPoint() {
        SparseArray<Point> centerPoints = new SparseArray<>();
        //在线用户列表送礼位置
        if (tvOnlinePeople != null) {
            int[] location = new int[2];
            tvOnlinePeople.getLocationInWindow(location);
            //textview 没有显示文本宽高会异常
            Point point1 = new Point(location[0], location[1]);
            centerPoints.put(-2, point1);
        }
        //房主送礼物位置
        if (flOwnerMic != null) {
            int[] location2 = new int[2];
            flOwnerMic.getLocationInWindow(location2);
            Point point2 = new Point(location2[0], location2[1]);
            centerPoints.put(-1, point2);
            addFaceInfoView(-1, location2);
        }
        // 普通麦位的位置
        View child;
        for (int i = 0; i <  rvRoomMicro.getChildCount(); i++) {
            child = rvRoomMicro.getChildAt(i);
            int[] location3 = new int[2];
            child.getLocationInWindow(location3);
            Point point3 = new Point(i == 0 ?location3[0]:screenWidth/2 - faceWidth/2,i == 0 ?location3[1]:location3[1]+faceWidth/4);
            centerPoints.put(i, point3);
            // 放置表情占位image view
            addFaceInfoView(i, location3);
        }
        RoomDataManager.get().mMicPointMap = centerPoints;
    }

    /**
     * 添加表情的ImageView的位置信息
     *
     * @param i
     * @param location3
     */
    private void addFaceInfoView(int i, int[] location3) {
        if (getFaceImageViews().get(i) == null && getContext() != null) {
            LayoutParams params;
            if (i == -1){//房主位
                params =  new LayoutParams(faceWidth * 5 / 6, faceWidth * 5 / 6);
                params.leftMargin =location3[0] + faceWidth / 14;
                params.topMargin = location3[1];
            }else if (i == 1){//拍卖位
                params = new LayoutParams(auctionFaceWidth, auctionFaceWidth);
                params.leftMargin =(screenWidth-auctionFaceWidth)/2;
                params.topMargin = location3[1];
            }else {//主持位
                params = new LayoutParams(faceWidth * 6 / 7, faceWidth * 6 / 7);
                params.leftMargin =location3[0] + faceWidth / 14;
                params.topMargin = location3[1];
            }
            ImageView face = new ImageView(getContext());
            face.setLayoutParams(params);
            getFaceImageViews().put(i, face);
            addView(face);
        }
    }

    /**
     * 设置房间的排行榜前几名数据
     *
     * @param fortuneFirstAvatar
     * @param fortuneSecondAvatar
     */
    public void setRankListInfo(String fortuneFirstAvatar, String fortuneSecondAvatar) {
        if (getContext() != null) {
            if (StringUtils.isNotEmpty(fortuneFirstAvatar) && ivFirst != null) {
                ImageLoadUtils.loadCircleImage(getContext(), fortuneFirstAvatar, ivFirst, R.drawable.ic_default_avatar);
            }
            if (StringUtils.isNotEmpty(fortuneSecondAvatar) && ivSecond != null) {
                ImageLoadUtils.loadCircleImage(getContext(), fortuneSecondAvatar, ivSecond, R.drawable.ic_default_avatar);
            }
        }
    }

    @Override
    public void notifyDataSetChanged(int roomEvent) {
        if (auctionRoomMicroAdapter != null) {
            auctionRoomMicroAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void notifyItemChanged(int position, int roomEvent) {
        if (auctionRoomMicroAdapter != null) {
            auctionRoomMicroAdapter.notifyItemChanged(position);
        }
    }

    public ImageView getIvMoreFunction() {
        return ivMoreFunction;
    }

    public RoomAuctionAdapter getRoomAuctionAdapter() {
        return roomAuctionAdapter;
    }

    /**
     * 更新在线用户数量
     *
     * @param onlinePeople
     */
    public void updateRoomOnlinePeople(int onlinePeople) {
        if (getContext() != null) {
            tvOnlinePeople.setText(getContext().getString(R.string.online_number_text, onlinePeople));
        }
    }

    /**
     * 根据房间信息更新房间相关UI
     *
     * @param roomInfo
     */
    public void updateViewFromRoomInfo(RoomInfo roomInfo) {
        if (roomInfo != null && getContext() != null) {
            tvRoomTitle.setText(roomInfo.getTitle());
            ivRoomLock.setVisibility(StringUtils.isNoneEmpty(roomInfo.getRoomPwd()) ? View.VISIBLE : View.GONE);
            //在线人数更新替换到成员退出和进入消息以及socket进入房间位置更新
//            tvOnlinePeople.setText(getContext().getString(R.string.online_number_text, roomInfo.getOnlineNum()));
//            ImageLoadUtils.loadImage(getContext(), roomInfo.getTagPict(), ivRoomTag, R.mipmap.ic_tag_default);
            if (!RoomDataManager.get().isUserSelf(roomInfo.getUid())) {
                if (bltAttention.getVisibility() == View.GONE) {
                    bltAttention.setVisibility(View.VISIBLE);
                }
            }
        } else {
            if (bltAttention.getVisibility() == View.VISIBLE) {
                bltAttention.setVisibility(View.GONE);
            }
        }
        updateOperateAuctionState();
    }


    /**
     * 房主麦位信息更新
     */
    public void updateRoomOwnerMicroInfo() {
        //这里将替换成新的获取方式由后台从room/get接口返回房主信息：头像头饰等
        IMRoomQueueInfo roomQueueInfo = RoomDataManager.get().getRoomQueueMemberInfoByMicPosition(RoomDataManager.MIC_POSITION_BY_OWNER);
        if (getContext() != null && roomQueueInfo != null) {
            if (roomQueueInfo.mChatRoomMember != null) {
                roomQueueInfo.mChatRoomMember.setIs_online(true);
                setRoomOwnerInfo(roomQueueInfo.mChatRoomMember);
            } else {
                IMRoomMember imRoomMember = RoomDataManager.get().getRoomOwnerDefaultMemberInfo();
                if (imRoomMember != null) {
                    imRoomMember.setIs_online(false);
                    setRoomOwnerInfo(imRoomMember);
                } else {
                    setRoomOwnerInfo(null);
                }
            }
        } else {
            setRoomOwnerInfo(null);
        }
    }


    /**
     * 根据房主信息更新房主麦位UI
     *
     * @param imRoomMember
     */
    public void setRoomOwnerInfo(IMRoomMember imRoomMember) {
        if (imRoomMember != null) {
            ivOwnerState.setVisibility(imRoomMember.isIs_online() ? View.GONE : View.VISIBLE);
            if (StringUtils.isNotEmpty(imRoomMember.getHalo())){
                bMicWave.setColor(Color.parseColor(imRoomMember.getHalo()));
            }else {
                bMicWave.setColor(0x55ffffff);
            }
            tvErbanId.setText(getContext().getString(R.string.room_id, String.valueOf(imRoomMember.getErbanNo())));
            ImageLoadUtils.loadCircleImage(getContext(), imRoomMember.getAvatar(), ivOwnerAvatar, R.drawable.ic_default_avatar);
            ivOwnerHeadwear.setVisibility(View.VISIBLE);
            ImageLoadUtils.loadImage(getContext(), imRoomMember.getHeadwearUrl(), ivOwnerHeadwear);
        } else {
            ivOwnerState.setVisibility(View.VISIBLE);
            ivOwnerAvatar.setImageResource(R.drawable.ic_default_avatar);
            ivOwnerHeadwear.setVisibility(View.GONE);
        }
    }

    /**
     * 房间麦位的声浪监听 -- 控制WaveView的开启播放
     *
     * @param micPositionList
     */
    public void onSpeakQueueUpdate(List<Integer> micPositionList) {
        for (int i = 0; i < micPositionList.size(); i++) {
            int pos = micPositionList.get(i);
            if (pos == -1) {
                if (bMicWave != null) {
                    bMicWave.start();
                }
            } else if (pos >= 0 && pos < 8) {
                if (rvRoomMicro != null) {
                    MicroWaveView speakState = rvRoomMicro.getChildAt(pos).findViewById(R.id.waveview);
                    if (speakState != null) {
                        speakState.start();
                    }
                }
            }
        }
    }

    @Override
    protected MicroWaveView getRoomOnerSpeakView() {
        return bMicWave;
    }

    @Override
    protected MicroWaveView getRoomNormalSpeakView(int pos) {
        if (rvRoomMicro != null && rvRoomMicro.getChildCount() > pos) {
            return rvRoomMicro.getChildAt(pos).findViewById(R.id.waveview);
        }else {
            return null;
        }
    }


    /**
     * 更新麦位魅力值
     *
     * @param roomCharmAttachment
     */
    @Override
    public void updateCharmData(RoomCharmAttachment roomCharmAttachment) {
    }

    /**
     * 更新竞拍位列表
     */
    public void updateAuctionList() {
        if (roomAuctionAdapter != null) {
            roomAuctionAdapter.notifyDataSetChanged();
        }
    }

    /**
     * 更新竞拍 按钮状态
     */
    public void updateOperateAuctionState() {
        if (RoomDataManager.get().isRoomOwner() || (RoomDataManager.get().mSelfRoomMember != null && RoomDataManager.get().mSelfRoomMember.isManager())) {
            if (tvAuctionOperate.getVisibility() == View.GONE) {
                tvAuctionOperate.setVisibility(View.VISIBLE);
            }
            if (RoomDataManager.get().getAuction() != null && RoomDataManager.get().getAuction().getUid() > 0) {
                tvAuctionOperate.setText("结束拍卖");
            } else {
                tvAuctionOperate.setText("开始拍卖");
            }
        } else {
            if (RoomDataManager.get().getAuction() != null
                    && RoomDataManager.get().getAuction().getUid() > 0) {
                if (!RoomDataManager.get().isUserSelf(RoomDataManager.get().getAuction().getUid())) {
                    if (tvAuctionOperate.getVisibility() == View.GONE) {
                        tvAuctionOperate.setVisibility(View.VISIBLE);
                    }
                    tvAuctionOperate.setText("拍下TA");
                }
//                if (getContext() != null) {
//                    tvAuctionInfo.setText(getContext().getString(R.string.auction_start_dialog_txt, RoomDataManager.get().getAuction().getProject(), RoomDataManager.get().getAuction().getDay()));
//                }
                //使用string 方式 分割符"|" 无法居中  ？？？
            } else {
                tvAuctionOperate.setVisibility(View.GONE);
            }
        }
    }


    @Override
    public void showFaceView(List<FaceReceiveInfo> faceReceiveInfos) {
        if (faceReceiveInfos == null || faceReceiveInfos.size() <= 0) {
            return;
        }
        for (FaceReceiveInfo faceReceiveInfo : faceReceiveInfos) {
            int position = RoomDataManager.get().getMicPosition(faceReceiveInfo.getUid());
            if (position < -1 || getFaceImageViews() == null || getFaceImageViews().size() <= position) {
                continue;
            }
            ImageView imageView = getFaceImageViews().get(position);
            if (imageView == null || getContext() == null) {
                continue;
            }
            AnimFaceFactory.asynLoadAnim(faceReceiveInfo, imageView, getContext(), faceWidth, faceWidth);
        }
    }

    public void setOnOwnerMicroItemClickListener(OnClickListener onOwnerMicroItemClickListener) {
        if (flOwnerMic != null) {
            flOwnerMic.setOnClickListener(onOwnerMicroItemClickListener);
        }
    }

    @Override
    public void setOnMicroItemClickListener(OnMicroItemClickListener onMicroItemClickListener) {
        if (auctionRoomMicroAdapter != null) {
            auctionRoomMicroAdapter.setOnMicroItemClickListener(onMicroItemClickListener);
        }
    }

    public void setOnRankMoreClickListener(OnClickListener onRankMoreClickListener) {
        if (rlRandList != null) {
            rlRandList.setOnClickListener(onRankMoreClickListener);
        }
    }

    public void setOnlinePeopleClickListener(OnClickListener onRankMoreClickListener) {
        if (tvOnlinePeople != null) {
            tvOnlinePeople.setOnClickListener(onRankMoreClickListener);
        }
    }

    public void setOnRoomTopicClickListener(OnClickListener onRoomTopicClickListener) {
        if (bltAnnouncement != null) {
            bltAnnouncement.setOnClickListener(onRoomTopicClickListener);
        }
    }

    public void setOnRoomShareClickListener(OnClickListener onRoomTopicClickListener) {
        if (ivShare != null) {
            ivShare.setOnClickListener(onRoomTopicClickListener);
        }
    }

    public void setOnRoomAuctionBtnClickListener(OnClickListener onRoomAuctionBtnClickListener) {
        if (tvAuctionOperate != null) {
            tvAuctionOperate.setOnClickListener(onRoomAuctionBtnClickListener);
        }
    }

    public TextView getBltAttention() {
        return bltAttention;
    }
}
