package com.yuhuankj.tmxq.ui.find.mengxin;

import android.graphics.Color;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tencent.bugly.crashreport.BuglyLog;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.fragment.BaseMvpFragment;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;
import com.yuhuankj.tmxq.ui.widget.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * 萌新-萌新界面
 *
 * @author weihaitao
 * @date 2017/10/3
 */
@CreatePresenter(SproutNewPresenter.class)
public class SproutNewFragment extends BaseMvpFragment<SproutNewView, SproutNewPresenter> implements SproutNewView, SwipeRefreshLayout.OnRefreshListener, BaseQuickAdapter.RequestLoadMoreListener {

    private final String TAG = SproutNewFragment.class.getSimpleName();

    private RecyclerView rv_sprout;
    private SproutNewAdapter sproutNewAdapter;

    private List<SproutUserInfo> sproutNewList = new ArrayList<>();

    private SwipeRefreshLayout srl_refresh;
    //加载更多现在有两种种方式，
    // 一种是利用com.tongdaxing.erban.libcommon.widget.SwipeRefreshView,
    // 一种是com.chad.library.adapter.base.BaseQuickAdapter.setLoadMoreView、
    // BaseQuickAdapter.setOnLoadMoreListener(BaseQuickAdapter.RequestLoadMoreListener)
    //由于SwipeRefreshScrollView.onOverScrolled会短时间触发多次，故并不适应做上拉加载
    private int nextDataLoadPage = 1;
    private final int dataLoadStartPage = 1;
    private final int dataLoadPageSize = 10;
    private boolean isLoadingData = false;

    public void setSproutListType(SproutListType sproutListType) {
        this.sproutListType = sproutListType;
    }

    private SproutListType sproutListType = SproutListType.sprout;

    public void setFilterType(SproutNewActivity.FilterType filterType) {
        this.filterType = filterType;
        if (null != sproutNewList && sproutNewList.size() > 0) {
            updateSproutListByFilter();
        }
    }

    private void updateSproutListByFilter() {
        int genderRequest = 0;
        List<SproutUserInfo> requestUserInfos = new ArrayList<>();
        if (filterType == SproutNewActivity.FilterType.Man) {
            genderRequest = 1;
        } else if (filterType == SproutNewActivity.FilterType.Woman) {
            genderRequest = 2;
        } else {
            sproutNewAdapter.setNewData(sproutNewList);
            return;
        }
        for (SproutUserInfo userInfo : sproutNewList) {
            if (userInfo.getGender() == genderRequest) {
                requestUserInfos.add(userInfo);
            }
        }
        sproutNewAdapter.setNewData(requestUserInfos);
    }

    private SproutNewActivity.FilterType filterType = SproutNewActivity.FilterType.ALL;

    public enum SproutListType {
        sprout,
        nearby
    }

    @Override
    public void initiate() {

    }

    @Override
    public void onFindViews() {
        rv_sprout = mView.findViewById(R.id.rv_sprout);
        sproutNewAdapter = new SproutNewAdapter(getActivity());
        LinearLayoutManager linearLayoutManager = new RecyclerViewNoBugLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rv_sprout.setLayoutManager(linearLayoutManager);
        rv_sprout.addItemDecoration(new DividerItemDecoration(linearLayoutManager.getOrientation(),
                Color.parseColor("#F5F5F5"), mContext, 1));
        rv_sprout.setAdapter(sproutNewAdapter);
        srl_refresh = mView.findViewById(R.id.srl_refresh);
        srl_refresh.setEnabled(true);
    }

    @Override
    public void onSetListener() {
        srl_refresh.setOnRefreshListener(this);
        sproutNewAdapter.setOnItemClick(new SproutNewAdapter.OnSproutAdapterItemClickListener() {
            @Override
            public void onClickToEnterRoom(SproutUserInfo sproutUserInfo) {
                //跳转个人资料页面和房间
                if (null != sproutUserInfo.getUserInRoom()) {
                    RoomServiceScheduler.getInstance().enterRoom(getActivity(),
                            sproutUserInfo.getUserInRoom().getUid(),
                            sproutUserInfo.getUserInRoom().getType());
                    //萌新广场-萌新、附近-进入房间
                    StatisticManager.get().onEvent(getActivity(),
                            sproutListType == SproutListType.sprout ? StatisticModel.EVENT_ID_SPROUT_NEW_ENTER_ROOM : StatisticModel.EVENT_ID_SPROUT_NEWARBY_ENTER_ROOM,
                            StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
                }
            }

            @Override
            public void onClickToViewUserInfo(SproutUserInfo sproutUserInfo) {
                //跳转个人资料页面和房间
                UserInfoActivity.start(getActivity(), sproutUserInfo.getUid());

                //萌新广场-萌新、附近-查看个人资料
                StatisticManager.get().onEvent(getActivity(),
                        sproutListType == SproutListType.sprout ? StatisticModel.EVENT_ID_SPROUT_NEW_VIEW_USERINFO : StatisticModel.EVENT_ID_SPROUT_NEWARBY_VIEW_USERINFO,
                        StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
            }
        });
        sproutNewAdapter.setOnLoadMoreListener(this, rv_sprout);
        sproutNewAdapter.setEnableLoadMore(true);
    }

    @Override
    public void onLoadMoreRequested() {
        BuglyLog.d(TAG, "onLoadMoreRequested");
        if (!isLoadingData) {
            getMvpPresenter().getSproutUserList(sproutListType, nextDataLoadPage, dataLoadPageSize);
        } else {
            sproutNewAdapter.loadMoreComplete();
        }
    }

    /**
     * 数据懒加载
     */
    @Override
    public void loadLazyIfYouWant() {
        if ((null == sproutNewList || sproutNewList.size() == 0) && !isLoadingData) {
            nextDataLoadPage = dataLoadStartPage;
            isLoadingData = true;
            BuglyLog.d(TAG, "loadLazyIfYouWant-nextDataLoadPage:" + nextDataLoadPage);
            getMvpPresenter().getSproutUserList(sproutListType, nextDataLoadPage, dataLoadPageSize);
        }
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_sprout_list;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LogUtils.d(TAG, "onDestroy");
    }

    @Override
    public void onRefresh() {
        if (!isLoadingData) {
            nextDataLoadPage = dataLoadStartPage;
            isLoadingData = true;
            BuglyLog.d(TAG, "onRefresh-nextDataLoadPage:" + nextDataLoadPage);
            getMvpPresenter().getSproutUserList(sproutListType, nextDataLoadPage, dataLoadPageSize);
        } else {
            srl_refresh.setRefreshing(false);
        }
    }

    @Override
    public void onGetSproutNewList(boolean success, String message, List userInfos, int currPageNum) {
        BuglyLog.d(TAG, "onGetSproutNewList-success:" + success + " message:" + message
                + " userInfos.size:" + (null == userInfos ? 0 : userInfos.size()) + " currPageNum:" + currPageNum);
        srl_refresh.setRefreshing(false);
        isLoadingData = false;
        if (success) {
            if (currPageNum == dataLoadStartPage) {
                sproutNewList.clear();
            }

            if (null != userInfos && userInfos.size() > 0) {
                sproutNewList.addAll(userInfos);
                nextDataLoadPage += 1;
                updateSproutListByFilter();
                if (currPageNum > dataLoadStartPage) {
                    sproutNewAdapter.loadMoreComplete();
                }
                sproutNewAdapter.setEnableLoadMore(userInfos.size() >= dataLoadPageSize);
            } else {
                sproutNewAdapter.loadMoreEnd(true);
            }

        } else if (!TextUtils.isEmpty(message)) {
            toast(message);
            if (currPageNum > dataLoadStartPage) {
                sproutNewAdapter.loadMoreFail();
            }
        }

    }
}
