package com.yuhuankj.tmxq.ui.home.view.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import com.jude.rollviewpager.hintview.ShapeHintView;
import com.yuhuankj.tmxq.R;

/**
 * @author weihaitao
 * @date 2019/5/21
 */
public class NewChannelHomeBannerShapeHintView extends ShapeHintView {

    public NewChannelHomeBannerShapeHintView(Context context) {
        super(context);
    }

    public NewChannelHomeBannerShapeHintView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public Drawable makeFocusDrawable() {
        if (null != getContext()) {
            return getContext().getResources().getDrawable(R.drawable.shape_oppo_home_banner_focus);
        }
        return null;
    }

    @Override
    public Drawable makeNormalDrawable() {
        if (null != getContext()) {
            return getContext().getResources().getDrawable(R.drawable.shape_oppo_home_banner_normal);
        }
        return null;
    }
}
