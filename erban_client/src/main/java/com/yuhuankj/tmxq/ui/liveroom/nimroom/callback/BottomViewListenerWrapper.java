package com.yuhuankj.tmxq.ui.liveroom.nimroom.callback;

/**
 * Created by chenran on 2017/11/21.
 */

public interface BottomViewListenerWrapper {

    void onOpenMicBtnClick();

    void onSendMsgBtnClick();

    void onSendFaceBtnClick();

    void onSendGiftBtnClick();

    void onShareBtnClick();

    void onRemoteMuteBtnClick();

    void onBuShowMicInList();

    void onPublicRoomMsgBtnClick();

    void onRoomMoreOperaClick();

    /**
     * 非主播，点击底部语音连接按钮
     */
    void onFansReqAudioConnClick();

    /**
     * 主播，点击底部语音连接按钮
     */
    void onAnchorAudioConnRecvClick();
}
