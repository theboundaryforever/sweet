package com.yuhuankj.tmxq.ui.find.mengxin;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.netease.nim.uikit.StatusBarUtil;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.widget.TitleBar;
import com.yuhuankj.tmxq.widget.magicindicator.MagicIndicator;
import com.yuhuankj.tmxq.widget.magicindicator.ViewPagerHelper;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.UIUtil;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.commonnavigator.CommonNavigator;

import java.util.ArrayList;
import java.util.List;

/**
 * 萌新界面
 *
 * @author chenran
 * @date 2017/10/3
 */
public class SproutNewActivity extends BaseMvpActivity implements
        SproutMagicIndicatorAdapter.OnItemSelectListener, IMvpBaseView, View.OnClickListener {

    private MagicIndicator mi_ind;
    private ViewPager vp_sprout;

    private List<TabInfo> mTabInfoList;
    private View ll_menu;
    private View ll_filter;
    private SproutAdapter adapter;

    private ImageView iv_checkAll, iv_checkMan, iv_checkWoman;
    private TextView tv_checkAll, tv_checkMan, tv_checkWoman;

    private FilterType filterType = FilterType.ALL;

    public enum FilterType {
        ALL,
        Man,
        Woman
    }


    public static void start(Context context) {
        Intent intent = new Intent(context, SproutNewActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sprout);
        addView(StatusBarUtil.StatusBarLightMode(this));
        initView();
        initViewPager();
    }

    private void initView() {
        initToolBar();
        initTabIndicator();
        ll_filter = findViewById(R.id.ll_filter);
        ll_menu = findViewById(R.id.ll_menu);
        ll_menu.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (!(event.getX() > ll_menu.getLeft()
                            && event.getX() < ll_menu.getRight()
                            && event.getY() > ll_menu.getTop()
                            && event.getY() < ll_menu.getBottom())) {
                        ll_menu.setVisibility(View.GONE);
                    }
                }
                return false;
            }
        });
        ll_filter.setOnClickListener(this);
        iv_checkAll = (ImageView) findViewById(R.id.iv_checkAll);
        iv_checkMan = (ImageView) findViewById(R.id.iv_checkMan);
        iv_checkWoman = (ImageView) findViewById(R.id.iv_checkWoman);
        tv_checkAll = (TextView) findViewById(R.id.tv_checkAll);
        tv_checkMan = (TextView) findViewById(R.id.tv_checkMan);
        tv_checkWoman = (TextView) findViewById(R.id.tv_checkWoman);
        findViewById(R.id.ll_viewAllSprout).setOnClickListener(this);
        findViewById(R.id.ll_viewManSprout).setOnClickListener(this);
        findViewById(R.id.ll_viewWomanSprout).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_viewAllSprout:
                changeFilterSelectedStatus(0);
                break;
            case R.id.ll_viewManSprout:
                changeFilterSelectedStatus(1);
                break;
            case R.id.ll_viewWomanSprout:
                changeFilterSelectedStatus(2);
                break;
            case R.id.ll_filter:
                ll_menu.setVisibility(ll_menu.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                break;
        }
    }

    private void changeFilterSelectedStatus(int index) {
        iv_checkAll.setVisibility(0 == index ? View.VISIBLE : View.INVISIBLE);
        tv_checkAll.setTextColor(Color.parseColor(0 == index ? "#0BCDA8" : "#BBBEC0"));
        iv_checkMan.setVisibility(1 == index ? View.VISIBLE : View.INVISIBLE);
        tv_checkMan.setTextColor(Color.parseColor(1 == index ? "#0BCDA8" : "#BBBEC0"));
        iv_checkWoman.setVisibility(2 == index ? View.VISIBLE : View.INVISIBLE);
        tv_checkWoman.setTextColor(Color.parseColor(2 == index ? "#0BCDA8" : "#BBBEC0"));
        filterType = FilterType.values()[index];
        ll_menu.setVisibility(View.GONE);
        for (int i = 0; i < adapter.getmFragmentList().size(); i++) {
            SproutNewFragment fragment = adapter.getmFragmentList().get(i);
            fragment.setFilterType(filterType);
        }
    }

    public void initToolBar() {
        mTitleBar = (TitleBar) findViewById(R.id.tv_nav);
        if (mTitleBar != null) {
            mTitleBar.setTitle(getString(R.string.sprout_activity_title));
            mTitleBar.setImmersive(false);
            mTitleBar.setTitleColor(getResources().getColor(R.color.back_font));
            mTitleBar.setLeftImageResource(R.drawable.arrow_left);
            mTitleBar.setLeftClickListener(v -> finish());
            mTitleBar.setTitleSize(17);
            mTitleBar.setCommonBackgroundColor(Color.WHITE);
        }
        mTitleBar.setActionTextColor(getResources().getColor(R.color.text_tertiary));
    }

    private void initTabIndicator() {
        mi_ind = (MagicIndicator) findViewById(R.id.mi_ind);

        mTabInfoList = new ArrayList<>();
        mTabInfoList.add(0, new TabInfo(-1, getString(R.string.sprout_fragment_nearby)));
        mTabInfoList.add(0, new TabInfo(-2, getString(R.string.sprout_fragment_new)));

        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdjustMode(true);
        SproutMagicIndicatorAdapter magicIndicatorAdapter = new SproutMagicIndicatorAdapter(this,
                mTabInfoList, UIUtil.dip2px(this, 0), UIUtil.dip2px(this, 0));
        magicIndicatorAdapter.setOnItemSelectListener(this);
        commonNavigator.setAdapter(magicIndicatorAdapter);
        mi_ind.setNavigator(commonNavigator);
        LinearLayout titleContainer = commonNavigator.getTitleContainer();
        titleContainer.setGravity(Gravity.CENTER);
        magicIndicatorAdapter.setSize(15);
    }

    private void initViewPager() {
        vp_sprout = (ViewPager) findViewById(R.id.vp_sprout);
        adapter = new SproutAdapter(getSupportFragmentManager(), mTabInfoList);
        vp_sprout.setAdapter(adapter);
        vp_sprout.setOffscreenPageLimit(2);
        ViewPagerHelper.bind(mi_ind, vp_sprout);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onItemSelect(int position) {
        vp_sprout.setCurrentItem(position);
    }
}
