package com.yuhuankj.tmxq.ui.match.bean;

import com.tongdaxing.xchat_core.room.bean.RoomInfo;

import java.io.Serializable;

public class MicroMatch implements Serializable {


    /**
     * uid : 90000883
     * erbanNo : 7457085
     * hasPrettyErbanNo : false
     * phone : 15202058640
     * birth : 757353600000
     * star : null
     * nick : avenger
     * email : null
     * signture : null
     * userVoice : https://img.pinjin88.com/Frc2c7tTtQnA6pTopmPyn05BHN_-?imageslim
     * voiceDura : 4
     * followNum : 0
     * fansNum : 0
     * defUser : 1
     * fortune : null
     * channelType : 1
     * lastLoginTime : null
     * lastLoginIp : null
     * gender : 1
     * avatar : https://img.pinjin88.com/Fq0fHi5YmgouWbGAcR7CLNnWpJSu?imageslim
     * region : null
     * userDesc : null
     * alipayAccount : null
     * alipayAccountName : null
     * createTime : 1536218933000
     * updateTime : 1537498950000
     * wxPubFansOpenid : null
     * wxPubFansGender : null
     * roomUid : null
     * shareUid : null
     * shareChannel : null
     * wxOpenid : null
     * os : android
     * osversion : 6.0.1
     * app : xchat
     * imei : null
     * channel : tt
     * linkedmeChannel : null
     * ispType : 4
     * netType : 2
     * model : SM-C7010
     * deviceId : 00a701b1-277d-3eff-bda3-f75e815c739c
     * appVersion : 2.5.9
     * nobleId : 0
     * nobleName : null
     * withdrawStatus : 0
     * backgroundStatus : 0
     */

    private long uid;
    private long erbanNo;
    private boolean hasPrettyErbanNo;
    private String phone;
    private long birth;
    private Object star;
    private String nick;
    private String email;
    private String signture;
    private String userVoice;
    private int voiceDura;
    private int followNum;
    private int fansNum;
    private int defUser;
    private Object fortune;
    private int channelType;
    private long lastLoginTime;
    private Object lastLoginIp;
    private int gender;
    private String avatar;
    private Object region;
    private String userDesc;
    private String alipayAccount;
    private String alipayAccountName;
    private long createTime;
    private long updateTime;
    private Object wxPubFansOpenid;
    private Object wxPubFansGender;
    private long roomUid;
    //所在房间房主UID
    private long inRoomUid;
    //所在房间的房间类型
    private int inRoomType = RoomInfo.ROOMTYPE_HOME_PARTY;

    public int getInRoomType() {
        return inRoomType;
    }

    public void setInRoomType(int inRoomType) {
        this.inRoomType = inRoomType;
    }
    private long shareUid;
    private String shareChannel;
    private String linkedmeChannel;
    private String nobleName;
    private Object wxOpenid;
    private String os;
    private String osversion;
    private String app;
    private Object imei;
    private String channel;

    public long getInRoomUid() {
        return inRoomUid;
    }

    private String ispType;
    private String netType;
    private String model;
    private String deviceId;
    private String appVersion;
    private int nobleId;

    public void setInRoomUid(long inRoomUid) {
        this.inRoomUid = inRoomUid;
    }

    private int withdrawStatus;
    private int backgroundStatus;
    private String background;
    private String timbre;
    private boolean isAdmired;

    @Override
    public String toString() {
        return "MicroMatch{" +
                "uid=" + uid +
                ", erbanNo=" + erbanNo +
                ", hasPrettyErbanNo=" + hasPrettyErbanNo +
                ", phone='" + phone + '\'' +
                ", birth=" + birth +
                ", star=" + star +
                ", nick='" + nick + '\'' +
                ", email=" + email +
                ", signture=" + signture +
                ", userVoice='" + userVoice + '\'' +
                ", voiceDura=" + voiceDura +
                ", followNum=" + followNum +
                ", fansNum=" + fansNum +
                ", defUser=" + defUser +
                ", fortune=" + fortune +
                ", channelType=" + channelType +
                ", lastLoginTime=" + lastLoginTime +
                ", lastLoginIp=" + lastLoginIp +
                ", gender=" + gender +
                ", avatar='" + avatar + '\'' +
                ", region=" + region +
                ", userDesc='" + userDesc + '\'' +
                ", alipayAccount=" + alipayAccount +
                ", alipayAccountName=" + alipayAccountName +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", wxPubFansOpenid=" + wxPubFansOpenid +
                ", wxPubFansGender=" + wxPubFansGender +
                ", roomUid=" + roomUid +
                ", shareUid=" + shareUid +
                ", shareChannel=" + shareChannel +
                ", wxOpenid=" + wxOpenid +
                ", os='" + os + '\'' +
                ", osversion='" + osversion + '\'' +
                ", app='" + app + '\'' +
                ", imei=" + imei +
                ", channel='" + channel + '\'' +
                ", linkedmeChannel=" + linkedmeChannel +
                ", ispType='" + ispType + '\'' +
                ", netType='" + netType + '\'' +
                ", model='" + model + '\'' +
                ", deviceId='" + deviceId + '\'' +
                ", appVersion='" + appVersion + '\'' +
                ", nobleId=" + nobleId +
                ", nobleName=" + nobleName +
                ", withdrawStatus=" + withdrawStatus +
                ", backgroundStatus=" + backgroundStatus +
                ", background='" + background + '\'' +
                ", timbre='" + timbre + '\'' +
                ", isAdmired=" + isAdmired +
                ", roomId=" + roomId +
                ", isLike=" + isLike +
                ", inRoomUid=" + inRoomUid +
                ", inRoomType=" + inRoomType +
                '}';
    }

    public int getBackgroundStatus() {
        return backgroundStatus;
    }

    public void setBackgroundStatus(int backgroundStatus) {
        this.backgroundStatus = backgroundStatus;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getTimbre() {
        return timbre;
    }


    /**
     * uid : 90000883
     * erbanNo : 7457085
     * star : null
     * email : null
     * signture : null
     * fortune : null
     * userDesc : null
     * roomId : 55995413
     * isLike : false
     */

    private int roomId;
    private boolean isLike;

    public void setTimbre(String timbre) {
        this.timbre = timbre;
    }

    public void setLike(boolean like) {
        isLike = like;
    }

    public boolean isAdmired() {
        return isAdmired;
    }

    public void setAdmired(boolean admired) {
        isAdmired = admired;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public long getErbanNo() {
        return erbanNo;
    }

    public void setErbanNo(long erbanNo) {
        this.erbanNo = erbanNo;
    }

    public boolean isHasPrettyErbanNo() {
        return hasPrettyErbanNo;
    }

    public void setHasPrettyErbanNo(boolean hasPrettyErbanNo) {
        this.hasPrettyErbanNo = hasPrettyErbanNo;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public long getBirth() {
        return birth;
    }

    public void setBirth(long birth) {
        this.birth = birth;
    }

    public Object getStar() {
        return star;
    }

    public void setStar(Object star) {
        this.star = star;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public Object getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getSignture() {
        return signture;
    }

    public void setSignture(String signture) {
        this.signture = signture;
    }

    public String getUserVoice() {
        return userVoice;
    }

    public void setUserVoice(String userVoice) {
        this.userVoice = userVoice;
    }

    public int getVoiceDura() {
        return voiceDura;
    }

    public void setVoiceDura(int voiceDura) {
        this.voiceDura = voiceDura;
    }

    public int getFollowNum() {
        return followNum;
    }

    public void setFollowNum(int followNum) {
        this.followNum = followNum;
    }

    public int getFansNum() {
        return fansNum;
    }

    public void setFansNum(int fansNum) {
        this.fansNum = fansNum;
    }

    public int getDefUser() {
        return defUser;
    }

    public void setDefUser(int defUser) {
        this.defUser = defUser;
    }

    public Object getFortune() {
        return fortune;
    }

    public void setFortune(Object fortune) {
        this.fortune = fortune;
    }

    public int getChannelType() {
        return channelType;
    }

    public void setChannelType(int channelType) {
        this.channelType = channelType;
    }

    public long getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(long lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public Object getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(Object lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Object getRegion() {
        return region;
    }

    public void setRegion(Object region) {
        this.region = region;
    }

    public String getUserDesc() {
        return userDesc;
    }

    public void setUserDesc(String userDesc) {
        this.userDesc = userDesc;
    }

    public String getAlipayAccount() {
        return alipayAccount;
    }

    public void setAlipayAccount(String alipayAccount) {
        this.alipayAccount = alipayAccount;
    }

    public String getAlipayAccountName() {
        return alipayAccountName;
    }

    public void setAlipayAccountName(String alipayAccountName) {
        this.alipayAccountName = alipayAccountName;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public Object getWxPubFansOpenid() {
        return wxPubFansOpenid;
    }

    public void setWxPubFansOpenid(Object wxPubFansOpenid) {
        this.wxPubFansOpenid = wxPubFansOpenid;
    }

    public Object getWxPubFansGender() {
        return wxPubFansGender;
    }

    public void setWxPubFansGender(Object wxPubFansGender) {
        this.wxPubFansGender = wxPubFansGender;
    }

    public long getRoomUid() {
        return roomUid;
    }

    public void setRoomUid(long roomUid) {
        this.roomUid = roomUid;
    }

    public long getShareUid() {
        return shareUid;
    }

    public void setShareUid(long shareUid) {
        this.shareUid = shareUid;
    }

    public String getShareChannel() {
        return shareChannel;
    }

    public void setShareChannel(String shareChannel) {
        this.shareChannel = shareChannel;
    }

    public Object getWxOpenid() {
        return wxOpenid;
    }

    public void setWxOpenid(Object wxOpenid) {
        this.wxOpenid = wxOpenid;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getOsversion() {
        return osversion;
    }

    public void setOsversion(String osversion) {
        this.osversion = osversion;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public Object getImei() {
        return imei;
    }

    public void setImei(Object imei) {
        this.imei = imei;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getLinkedmeChannel() {
        return linkedmeChannel;
    }

    public void setLinkedmeChannel(String linkedmeChannel) {
        this.linkedmeChannel = linkedmeChannel;
    }

    public String getIspType() {
        return ispType;
    }

    public void setIspType(String ispType) {
        this.ispType = ispType;
    }

    public String getNetType() {
        return netType;
    }

    public void setNetType(String netType) {
        this.netType = netType;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public int getNobleId() {
        return nobleId;
    }

    public void setNobleId(int nobleId) {
        this.nobleId = nobleId;
    }

    public String getNobleName() {
        return nobleName;
    }

    public void setNobleName(String nobleName) {
        this.nobleName = nobleName;
    }

    public int getWithdrawStatus() {
        return withdrawStatus;
    }

    public void setWithdrawStatus(int withdrawStatus) {
        this.withdrawStatus = withdrawStatus;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public boolean isLike() {
        return isLike;
    }

    public void setIsLike(boolean isLike) {
        this.isLike = isLike;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof MicroMatch)) {
            return false;
        }
        boolean isSameGift = false;
        MicroMatch other = (MicroMatch) obj;
        if (other.getUid() == this.getUid()) {
            isSameGift = true;
        }
        return isSameGift;
    }
}
