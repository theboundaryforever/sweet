package com.yuhuankj.tmxq.ui.liveroom.nimroom.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

public class PkUserAdapter extends BaseQuickAdapter<RoomQueueInfo, BaseViewHolder> {

    public PkUserAdapter() {
        super(R.layout.item_pk_user);
    }

    @Override
    protected void convert(BaseViewHolder helper, RoomQueueInfo item) {
        if (item == null || item.mChatRoomMember == null) {
            return;
        }
        ImageView imvUser = helper.getView(R.id.imvUser);
        View vSelect = helper.getView(R.id.vSelect);
        TextView tvUserNick = helper.getView(R.id.tvUserNick);

        ImageLoadUtils.loadCircleImage(mContext, item.mChatRoomMember.getAvatar(), imvUser, R.drawable.ic_default_avator_circle);
        tvUserNick.setText(item.mChatRoomMember.getNick());

        if (item.isSelect()) {
            vSelect.setVisibility(View.VISIBLE);
        } else {
            vSelect.setVisibility(View.INVISIBLE);
        }
    }
}
