package com.yuhuankj.tmxq.ui.nim.sysmsg;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.netease.nim.uikit.common.util.sys.NetworkUtil;
import com.netease.nim.uikit.session.activity.P2PMessageActivity;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.Constants;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.ui.user.bosonFriend.BosonFriendModel;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liaoxy
 * @Description:系统消息页面
 * @date 2019/5/5 16:48
 */
public class SysMsgActivity extends BaseActivity {
    private List<SysMsgEnitity> datas;
    private LinearLayout llEmply;
    private RecyclerView rcvDatas;
    private BaseQuickAdapter adapter;
    private SwipeRefreshLayout sprContent;
    private int page = Constants.PAGE_START;

    private SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            if (NetworkUtil.isNetAvailable(SysMsgActivity.this)) {
                page = Constants.PAGE_START;
                getBosonFriendApplys();
            } else {
                sprContent.setRefreshing(false);
            }
        }
    };

    private BaseQuickAdapter.RequestLoadMoreListener loadMoreListener = new BaseQuickAdapter.RequestLoadMoreListener() {
        @Override
        public void onLoadMoreRequested() {
            // 解决数据重复的问题
            if (page == Constants.PAGE_START) {
                return;
            }
            if (NetworkUtil.isNetAvailable(SysMsgActivity.this)) {
                if (datas.size() >= Constants.PAGE_SIZE) {
                    getBosonFriendApplys();
                } else {
                    adapter.setEnableLoadMore(false);
                }
            } else {
                adapter.loadMoreEnd(true);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitor);
        initTitleBar("系统通知");
        mTitleBar.setCommonBackgroundColor(Color.WHITE);
        initView();
        initData();
    }

    private void initView() {
        sprContent = (SwipeRefreshLayout) findViewById(R.id.sprContent);
        rcvDatas = (RecyclerView) findViewById(R.id.rcvFriends);
        llEmply = (LinearLayout) findViewById(R.id.llEmply);

        sprContent.setOnRefreshListener(onRefreshListener);
    }

    private void initData() {
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(P2PMessageActivity.ACTION_FINISH));

        TextView tvEmplyTip = findView(R.id.tvEmplyTip);
        tvEmplyTip.setText("暂时没有系统消息~");
        LinearLayoutManager layoutmanager = new RecyclerViewNoBugLinearLayoutManager(this);
        layoutmanager.setOrientation(LinearLayoutManager.VERTICAL);
        rcvDatas.setLayoutManager(layoutmanager);
        datas = new ArrayList<>();

        llEmply.setVisibility(View.GONE);
        adapter = new SysMsgAdapter(datas);
        rcvDatas.setAdapter(adapter);
        adapter.setOnLoadMoreListener(loadMoreListener, rcvDatas);
        adapter.setEnableLoadMore(false);
        adapter.setOnItemLongClickListener(new BaseQuickAdapter.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(BaseQuickAdapter adapter, View view, int position) {
                getDialogManager().showOkCancelDialog("是否要删除这条消息?", true, new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onOk() {
                        try {
                            SysMsgEnitity enitity = datas.get(position);
                            disageeBosonFriend(enitity);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                return true;
            }
        });
        adapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                if (view.getId() == R.id.tvAgree) {
                    try {
                        SysMsgEnitity msgEnitity = datas.get(position);
                        agreeBosonFriendApply(msgEnitity);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        getBosonFriendApplys();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void getBosonFriendApplys() {
        new BosonFriendModel().getBosonFriendApplys(page, Constants.PAGE_SIZE, new OkHttpManager.MyCallBack<ServiceResult<List<SysMsgEnitity>>>() {

            @Override
            public void onError(Exception e) {
                sprContent.setRefreshing(false);
                adapter.loadMoreComplete();
                SingleToastUtil.showToast("获取系统消息列表失败");
            }

            @Override
            public void onResponse(ServiceResult<List<SysMsgEnitity>> response) {
                sprContent.setRefreshing(false);
                adapter.loadMoreComplete();
                if (response == null || response.getData() == null) {
                    SingleToastUtil.showToast("获取系统消息列表失败");
                    return;
                }
                if (!response.isSuccess()) {
                    SingleToastUtil.showToast(response.getErrorMessage());
                    return;
                }
                List<SysMsgEnitity> tmps = response.getData();
//                if (tmps.size() >= Constants.PAGE_SIZE) {
//                    adapter.setEnableLoadMore(true);
//                    page++;
//                } else {
//                    adapter.setEnableLoadMore(false);
//                }
                if (page == Constants.PAGE_START) {
                    datas.clear();
                }
                datas.addAll(tmps);
                adapter.notifyDataSetChanged();
                if (datas.size() == 0) {
                    llEmply.setVisibility(View.VISIBLE);
                } else {
                    llEmply.setVisibility(View.GONE);
                }
            }
        });
    }

    private void agreeBosonFriendApply(SysMsgEnitity msgEnitity) {
        getDialogManager().showProgressDialog(this, "请稍候...");
        new BosonFriendModel().agreeBosonFriendApply(msgEnitity.getUid(), msgEnitity.getId(), new OkHttpManager.MyCallBack<ServiceResult<List<SysMsgEnitity>>>() {

            @Override
            public void onError(Exception e) {
                getDialogManager().dismissDialog();
                SingleToastUtil.showToast(e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<List<SysMsgEnitity>> response) {
                getDialogManager().dismissDialog();
                if (response == null) {
                    SingleToastUtil.showToast("操作失败");
                    return;
                }
                if (!response.isSuccess()) {
                    SingleToastUtil.showToast(response.getMessage());
                } else {
                    try {
                        msgEnitity.setStatus(1);
                        adapter.notifyItemChanged(datas.indexOf(msgEnitity));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void disageeBosonFriend(SysMsgEnitity enitity) {
        getDialogManager().showProgressDialog(this, "请稍候...");
        new BosonFriendModel().disagreeBosonFriendApply(enitity.getUid(), enitity.getId(), new OkHttpManager.MyCallBack<ServiceResult<List<SysMsgEnitity>>>() {

            @Override
            public void onError(Exception e) {
                getDialogManager().dismissDialog();
                SingleToastUtil.showToast(e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<List<SysMsgEnitity>> response) {
                getDialogManager().dismissDialog();
                if (response == null) {
                    SingleToastUtil.showToast("操作失败");
                    return;
                }
                if (!response.isSuccess()) {
                    SingleToastUtil.showToast(response.getMessage());
                } else {
                    adapter.remove(datas.indexOf(enitity));
                }
            }
        });
    }
}
