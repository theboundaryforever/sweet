package com.yuhuankj.tmxq.ui.liveroom.imroom.gift;

import com.tongdaxing.xchat_core.gift.GiftType;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/5/21
 */
public class RoomGiftConstant {
    public static GiftType currentGiftType = GiftType.Normal;
    public static int currentGiftTypeSelect = 0;
    public static final int PAGE_SIZE = 8;

    public static void resetState() {
        RoomGiftConstant.currentGiftType = GiftType.Normal;
        RoomGiftConstant.currentGiftTypeSelect = 0;
    }
}
