package com.yuhuankj.tmxq.ui.liveroom.imroom.publicscreen.holder;

import android.graphics.Color;
import android.os.Build;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;

import com.netease.nim.uikit.common.util.string.StringUtil;
import com.tongdaxing.erban.libcommon.im.IMReportRoute;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.RichTextUtil;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.erban.libcommon.widget.messageview.MsgHolderLayoutId;
import com.tongdaxing.xchat_core.bean.RoomRedPacketFinish;
import com.tongdaxing.xchat_core.im.custom.bean.AuctionAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.PkCustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomRedPacketFinishAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomRuleAttachment;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMessage;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.pk.bean.PkVoteInfo;
import com.tongdaxing.xchat_core.room.auction.RoomAuctionBean;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.liveroom.imroom.publicscreen.base.RoomMsgBaseHolder;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;
import com.yuhuankj.tmxq.ui.widget.UserInfoDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 文件描述：默认无用户信息栏的文本消息 - 采用默认气泡样式（多用于系统消息）
 *
 * @auther：zwk
 * @data：2019/5/7
 */
@MsgHolderLayoutId(value = R.layout.item_room_msg_txt)
public class RoomMsgTxtHolder extends RoomMsgBaseHolder<IMRoomMessage> {

    public RoomMsgTxtHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void onDataTransformView(RoomMsgBaseHolder holder, IMRoomMessage message, int position) {
        TextView tvContent = (TextView) holder.getView(R.id.etv_content);
        if (IMReportRoute.ChatRoomTip.equalsIgnoreCase(message.getRoute())) {
            tvContent.setText(message.getContent());
        } else if (IMReportRoute.sendMessageReport.equalsIgnoreCase(message.getRoute())) {
            if (message.getAttachment() != null) {
                if (message.getAttachment().getFirst() == CustomAttachment.CUSTOM_MSG_TYPE_RULE_FIRST) {
                    //进房提示消息类型
                    setRoomRuleMsg(tvContent, message.getAttachment());
                } else if (message.getAttachment().getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK) {
                    setPkMsg(message.getAttachment(), tvContent);
                } else if (message.getAttachment().getFirst() == CustomAttachment.CUSTOM_MSG_ROOM_RICH_TXT) {
                    setRichTextMsg(message.getAttachment(), tvContent);
                } else if (message.getAttachment().getFirst() == CustomAttachment.CUSTOM_MSG_FIRST_AUCTION) {
                    if (message.getAttachment().getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_AUCTION_START) {
                        setAuctionStartMsg(tvContent, message.getAttachment());
                    } else if (message.getAttachment().getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_AUCTION_END) {
                        setAuctionEndMsg(tvContent, message.getAttachment());
                    } else {
                        tvContent.setText("不支持消息类型");
                    }
                } else if (message.getAttachment().getFirst() == CustomAttachment.CUSTOM_MSG_ROOM_RED_PACKET_FINISH) {
                    if (message.getAttachment().getSecond() == CustomAttachment.CUSTOM_MSG_ROOM_RED_PACKET_RECEIVE_FINISH) {
                        setMsgRoomRedPacketFinishTip(tvContent, message.getAttachment());
                    } else {
                        tvContent.setText("不支持消息类型");
                    }
                } else {
                    tvContent.setText("不支持消息类型");
                }
            } else {
                tvContent.setText("不支持消息类型");
            }
        } else {
            tvContent.setText("不支持消息类型");
        }
    }

    /**
     * 显示竞拍开始消息
     *
     * @param tvContent
     * @param attachment
     */
    private void setAuctionStartMsg(TextView tvContent, CustomAttachment attachment) {
        if (attachment instanceof AuctionAttachment) {
            RoomAuctionBean roomAuctionBean = ((AuctionAttachment) attachment).getRoomAuctionBean();
            if (roomAuctionBean != null) {
                List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
                list.add(RichTextUtil.getRichTextMap("本轮拍卖开始啦，快对用户", 0xFFFFFFFF));
                list.add(RichTextUtil.getRichTextMap(StringUtils.limitStr(tvContent.getContext(),roomAuctionBean.getNick(),6), 0xFF10CFA6));
                list.add(RichTextUtil.getRichTextMap("出价吧～", 0xFFFFFFFF));
                tvContent.setText(RichTextUtil.getSpannableStringFromList(list));
            } else {
                tvContent.setText("不支持消息类型");
            }
        } else {
            tvContent.setText("不支持消息类型");
        }
    }

    /**
     * 设置竞拍结束消息
     *
     * @param tvContent
     */
    private void setAuctionEndMsg(TextView tvContent,CustomAttachment attachment) {
        if (attachment instanceof AuctionAttachment) {
            RoomAuctionBean roomAuctionBean = ((AuctionAttachment) attachment).getRoomAuctionBean();
            if (roomAuctionBean != null) {
                List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
                list.add(RichTextUtil.getRichTextMap(StringUtils.limitStr(tvContent.getContext(),roomAuctionBean.getNick(),6), 0xFF10CFA6));
                if (!ListUtils.isListEmpty(roomAuctionBean.getList())) {
                    list.add(RichTextUtil.getRichTextMap(" 被 ", 0xFFFFFFFF));
                    list.add(RichTextUtil.getRichTextMap(StringUtils.limitStr(tvContent.getContext(),roomAuctionBean.getList().get(0).getNick(),6), 0xFF10CFA6));
                    list.add(RichTextUtil.getRichTextMap("成功拍下，", 0xFFFFFFFF));
                    list.add(RichTextUtil.getRichTextMap(roomAuctionBean.getProject() + " " + roomAuctionBean.getDay() + "天", 0xFF10CFA6));
                }else {
                    list.add(RichTextUtil.getRichTextMap("拍卖失败", 0xFFFFFFFF));
                }
                tvContent.setText(RichTextUtil.getSpannableStringFromList(list));
            } else {
                tvContent.setText("不支持消息类型");
            }
        } else {
            tvContent.setText("不支持消息类型");
        }
    }

    @Override
    protected int getItemClickLayoutId() {
        return R.id.etv_content;
    }

    @Override
    protected int getItemUserClickLayoutId() {
        return 0;
    }

    /**
     * 显示自定义的进房提示消息
     */
    private void setRoomRuleMsg(TextView tvContent, CustomAttachment customAttachment) {
        if (customAttachment instanceof RoomRuleAttachment) {
            RoomRuleAttachment roomRuleAttachment = (RoomRuleAttachment) customAttachment;
            tvContent.setText(roomRuleAttachment.getRule());
        } else {
            tvContent.setText("不支持消息类型");
        }
    }

    /**
     * 设置富文本消息
     *
     * @param customAttachment
     * @param tvContent
     */
    private void setRichTextMsg(CustomAttachment customAttachment, TextView tvContent) {
        if (null != customAttachment.getData()) {
            String msgContent = customAttachment.getData().toJSONString();
            //新版本富文本消息一律没有单独的点击事件
            if (!StringUtil.isEmpty(msgContent)) {
                tvContent.setText(RichTextUtil.getSpannableStringFromJson(tvContent, msgContent, new RichTextUtil.OnClickableRichTxtItemClickedListener() {
                    @Override
                    public void onClickToEnterRoomActivity(long roomUid) {
                        RoomServiceScheduler.getInstance().enterRoom(tvContent.getContext(), roomUid);
                    }

                    @Override
                    public void onClickToShowUserInfoDialog(long uid) {
                        new UserInfoDialog(tvContent.getContext(), uid).show();
                    }

                    @Override
                    public void onClickToShowWebViewLoadUrl(String url) {
                        CommonWebViewActivity.start(tvContent.getContext(), url, false);
                    }
                }));
            } else {
                tvContent.setText("");
            }
        } else {
            tvContent.setText("");
        }
    }

    /**
     * 设置PK消息结果
     *
     * @param chatRoomMessage
     * @param tvContent
     */
    private void setPkMsg(CustomAttachment chatRoomMessage, TextView tvContent) {
        if (chatRoomMessage instanceof PkCustomAttachment) {
            PkVoteInfo info = ((PkCustomAttachment) chatRoomMessage).getPkVoteInfo();
            if (info != null) {
                if (chatRoomMessage.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_START_NEW) {
                    String lightText = "PK窗口";
                    String tips = (AvRoomDataManager.get().isRoomOwner(info.getOpUid()) ? "房主" : "管理员") + "发起了PK，点击" + lightText + "可参与投票～";
                    int firstIndex = tips.indexOf(lightText);
                    SpannableStringBuilder builder = new SpannableStringBuilder(tips);
                    ForegroundColorSpan pkSpan = new ForegroundColorSpan(0xff10CFA6);
                    builder.setSpan(pkSpan, firstIndex, firstIndex + lightText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tvContent.setText(builder);
                } else if (chatRoomMessage.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_END_NEW) {
                    try {
                        PkVoteInfo.PkUser pkUserFirst = info.getPkList().get(0);
                        SpannableStringBuilder builder = new SpannableStringBuilder("投票结果公布：");
                        if (info.getPkList() != null && info.getPkList().size() > 0 && info.getPkList().get(0).getVoteCount() == 0) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                builder.append("平局!", new ForegroundColorSpan(0xFF10CFA6), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                            } else {
                                builder.append("平局!");
                            }
                            tvContent.setText(builder);
                            return;
                        }
                        builder.append("\n");
                        for (int i = 0; i < info.getPkList().size(); i++) {
                            PkVoteInfo.PkUser pkUser = info.getPkList().get(i);
                            String nick = pkUser.getNick();
                            if (nick.length() > 4) {
                                nick = nick.substring(0, 4) + "...";
                            }
                            int voteCount = pkUser.getVoteCount();
                            String rankTip = "";
                            if (pkUserFirst.getVoteCount() > 0) {
                                if (i == 0) {
                                    rankTip = "，获得本轮PK第一名";
                                } else if (voteCount == pkUserFirst.getVoteCount()) {
                                    rankTip = "，并列本轮PK第一名";
                                }
                            }
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                builder.append(nick, new ForegroundColorSpan(Color.WHITE), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                builder.append("  " + voteCount + "票" + rankTip + "\n", new ForegroundColorSpan(0xFF10CFA6), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                            } else {
                                builder.append(nick).append("  ").append(String.valueOf(voteCount)).append("票").append(rankTip).append("\n");
                            }
                        }
                        tvContent.setText(builder);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (chatRoomMessage.getSecond() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_CANCEL_NEW) {
                    String nick = (AvRoomDataManager.get().isRoomOwner(info.getOpUid()) ? "房主" : "管理员") + "取消了本场PK";
                    tvContent.setText(nick);
                } else {
                    tvContent.setText("不支持消息类型");
                }
            } else {
                tvContent.setText("不支持消息类型");
            }
        } else {
            tvContent.setText("不支持消息类型");
        }

    }

    /**
     * 红包领完的人气王通知
     * @param tvContent
     * @param customAttachment
     */
    private void setMsgRoomRedPacketFinishTip(TextView tvContent, CustomAttachment customAttachment) {
        if (customAttachment instanceof RoomRedPacketFinishAttachment) {
            RoomRedPacketFinish info = ((RoomRedPacketFinishAttachment) customAttachment).getDataInfo();
            if (info != null) {
                String contentStr = info.getNick() + "在本轮红包雨中，抢到了" + info.getGoldNum() + "金币，属于人气王";
                SpannableString span = new SpannableString(contentStr);
                //昵称绿色
                ForegroundColorSpan green = new ForegroundColorSpan(0xff10cfa6);
                span.setSpan(green, 0, info.getNick().length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                //金额绿色
                int goldIndex = (info.getNick() + "在本轮红包雨中，抢到了").length();
                ForegroundColorSpan green1 = new ForegroundColorSpan(0xff10cfa6);
                span.setSpan(green1, goldIndex, goldIndex + (info.getGoldNum() + "").length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                //人气王三个字绿色
                ForegroundColorSpan green2 = new ForegroundColorSpan(0xff10cfa6);
                span.setSpan(green2, contentStr.length() - 3, contentStr.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                tvContent.setText(span);
            } else {
                tvContent.setText("不支持消息类型");
            }
        } else {
            tvContent.setText("不支持消息类型");
        }
    }
}
