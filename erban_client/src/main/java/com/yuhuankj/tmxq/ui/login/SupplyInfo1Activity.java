package com.yuhuankj.tmxq.ui.login;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.constant.StatisticModel;

/**
 * @author liaoxy
 * @Description:资料完善页1
 * @date 2019/4/10 19:22
 */
public class SupplyInfo1Activity extends BaseActivity implements View.OnClickListener {
    private View vMan, vWoman;
    private TextView tvMan, tvWoman;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_supply1);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        vMan = findViewById(R.id.vMan);
        vWoman = findViewById(R.id.vWoman);
        tvMan = (TextView) findViewById(R.id.tvMan);
        tvWoman = (TextView) findViewById(R.id.tvWoman);
    }

    private void initListener() {
        vMan.setOnClickListener(this);
        vWoman.setOnClickListener(this);
    }

    private void initData() {
        initTitleBar("");
        mTitleBar.setCommonBackgroundColor(Color.WHITE);
        //这里从广播接收
        IntentFilter filter = new IntentFilter(ACTION_FINISH_SUPPLY1);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, filter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (broadcastReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        }
    }

    public static final String ACTION_FINISH_SUPPLY1 = "ACTION_FINISH_SUPPLY1";
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_FINISH_SUPPLY1.equals(intent.getAction())) {
                finish();
            }
        }
    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            getDialogManager().showOkCancelDialog("确认放弃注册？", "继续", "放弃", new DialogManager.OkCancelDialogListener() {
                @Override
                public void onCancel() {
                    LocalBroadcastManager.getInstance(SupplyInfo1Activity.this).sendBroadcast(new Intent(RegisterActivity.ACTION_FINISH_REGISTER));
                    startActivity(new Intent(SupplyInfo1Activity.this, LoginActivity.class));
                    finish();
                }

                @Override
                public void onOk() {
                    getDialogManager().dismissDialog();
                }
            });
            //点击放弃注册
            StatisticManager.get().onEvent(this,
                    StatisticModel.EVENT_ID_CLICK_GIVEUP_REGISTE,
                    StatisticModel.getInstance().getUMAnalyCommonMap(this));
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onLeftClickListener() {
        getDialogManager().showOkCancelDialog("确认放弃注册？", "继续", "放弃", new DialogManager.OkCancelDialogListener() {
            @Override
            public void onCancel() {
                LocalBroadcastManager.getInstance(SupplyInfo1Activity.this).sendBroadcast(new Intent(RegisterActivity.ACTION_FINISH_REGISTER));
                startActivity(new Intent(SupplyInfo1Activity.this, LoginActivity.class));
                finish();
            }

            @Override
            public void onOk() {
                getDialogManager().dismissDialog();
            }
        });
        //点击放弃注册
        StatisticManager.get().onEvent(this,
                StatisticModel.EVENT_ID_CLICK_GIVEUP_REGISTE,
                StatisticModel.getInstance().getUMAnalyCommonMap(this));
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view == vMan) {
            Intent intent = new Intent(this, SupplyInfo2Activity.class);
            intent.putExtra("gender", 1);
            startActivity(intent);
        } else if (view == vWoman) {
            Intent intent = new Intent(this, SupplyInfo2Activity.class);
            intent.putExtra("gender", 2);
            startActivity(intent);
        }
    }
}
