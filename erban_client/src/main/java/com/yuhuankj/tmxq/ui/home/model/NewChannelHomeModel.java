package com.yuhuankj.tmxq.ui.home.model;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;

import java.util.Map;

/**
 * @author weihaitao
 * @date 2019/5/24
 */
public class NewChannelHomeModel {

    /**
     * 获取_new渠道包首页列表数据，目前包含顶部banner、中部的单人音频房间列表、底部的聊天室列表
     *
     * @param pageNum  下拉刷新传1，上拉刷新依次传2、3、4、。。。
     * @param pageSize 每页数量
     * @param callBack
     */
    public void getOpscHomeData(int pageNum, int pageSize, HttpRequestCallBack<NewChannelHomeResponse> callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        params.put("pageNum", pageNum + "");
        params.put("pageSize", pageSize + "");
        params.put("uid", uid + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket() + "");
        OkHttpManager.getInstance().getRequest(UriProvider.getOpscHomeData(), params, callBack);
    }
}
