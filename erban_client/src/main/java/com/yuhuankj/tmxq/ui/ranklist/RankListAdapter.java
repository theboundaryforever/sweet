package com.yuhuankj.tmxq.ui.ranklist;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.juxiao.library_utils.DisplayUtils;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.xchat_core.UriProvider;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

public class RankListAdapter extends BaseQuickAdapter<RankListEnitity, BaseViewHolder> {

    private int type = 2;
    private int rankHeight = 32;
    private RankListItemFragment rankListFragment;

    public RankListAdapter(Context context,List<RankListEnitity> data, RankListItemFragment rankListFragment) {
        super(R.layout.item_rank_list, data);
        this.rankListFragment = rankListFragment;
        rankHeight = DisplayUtils.dip2px(context,16);
    }

    @Override
    protected void convert(BaseViewHolder helper, RankListEnitity item) {
        ImageView imvUser = helper.getView(R.id.imvUser);
        TextView tvSrialNumber = helper.getView(R.id.tvSrialNumber);
        TextView tvUserName = helper.getView(R.id.tvUserName);
        ImageView imvRank = helper.getView(R.id.imvRank);
        ImageView imvNobleIcon = helper.getView(R.id.imvNobleIcon);
        TextView tvValue = helper.getView(R.id.tvValue);
//        TextView tvLastOffset = helper.getView(R.id.tvLastOffset);

        if (!TextUtils.isEmpty(item.getVipMedal())) {
            imvNobleIcon.setVisibility(View.VISIBLE);
            if (GlideContextCheckUtil.checkContextUsable(mContext)) {
                ImageLoadUtils.loadImage(mContext, item.getVipMedal(), imvNobleIcon);
            }
        } else {
            imvNobleIcon.setVisibility(View.GONE);
        }
        if (GlideContextCheckUtil.checkContextUsable(mContext)) {
            ImageLoadUtils.loadImage(mContext, item.getAvatar(), imvUser);
        }
        tvSrialNumber.setText((helper.getAdapterPosition() + 3) + "");
        tvUserName.setText(item.getNick());

        int drawableId = -1;
        String levelImgUrl = "";
        if (type == 1) {
            //魅力榜
            tvValue.setTextColor(Color.parseColor("#FE7F6C"));
            int level = item.getCharmLevel();
            //drawableId = mContext.getResources().getIdentifier("ml" + level, "drawable", mContext.getPackageName());
            levelImgUrl = UriProvider.getMLImgUrl(level);
        } else {
            //财富榜
            tvValue.setTextColor(Color.parseColor("#FF9239"));
            int level = item.getExperLevel();
            // drawableId = mContext.getResources().getIdentifier("lv" + level, "drawable", mContext.getPackageName());
            levelImgUrl = UriProvider.getCFImgUrl(level);
        }
        if (rankListFragment != null) {
            String offset = rankListFragment.getLastOffset(helper.getAdapterPosition() - 1 + 3);
            if (TextUtils.isEmpty(offset)) {
                tvValue.setText("");
            } else {
                tvValue.setText(offset);
            }
        } else {
            tvValue.setText("");
        }
//        if (drawableId > 0) {
//            imvRank.setVisibility(View.VISIBLE);
//            imvRank.setImageResource(drawableId);
//        } else {
//            imvRank.setVisibility(View.INVISIBLE);
//        }
        if (!TextUtils.isEmpty(levelImgUrl)) {
            imvRank.setVisibility(View.VISIBLE);
            ImageLoadUtils.loadImageWidthLimitView(mContext, levelImgUrl, imvRank,rankHeight);
        } else {
            imvRank.setVisibility(View.INVISIBLE);
        }
    }

    //设置类型
    public void setType(int type) {
        this.type = type;
    }
}