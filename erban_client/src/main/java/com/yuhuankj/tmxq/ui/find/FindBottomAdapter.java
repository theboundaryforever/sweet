package com.yuhuankj.tmxq.ui.find;

import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.find.FindInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FindBottomAdapter extends BaseQuickAdapter<FindInfo, FindBottomAdapter.ViewHolder> {

    private final String TAG = FindBottomAdapter.class.getSimpleName();

    private int width = 0;
    private int height = 0;
    private boolean needDefaultPlaceHolderImg = true;

    public FindBottomAdapter(FragmentActivity context, int xSize, int ySize) {
        super(R.layout.item_find);

        DisplayMetrics dm = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(dm);
        width = dm.widthPixels - DisplayUtility.dp2px(context, 38);
        height = width * ySize / xSize;
        LogUtils.d(TAG, "FindBottomAdapter-width:" + width + " height:" + height);
    }

    public void setNeedDefaultPlaceHolderImg(boolean needDefaultPlaceHolderImg) {
        this.needDefaultPlaceHolderImg = needDefaultPlaceHolderImg;
    }

    @Override
    protected void convert(ViewHolder helper, FindInfo item) {
        //宽度固定，高度按比例缩放
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) helper.image.getLayoutParams();
        lp.width = width;
        lp.height = height;
        helper.image.setLayoutParams(lp);
        //不设置占位符会导致图片变形
        if(GlideContextCheckUtil.checkContextUsable(helper.image.getContext())){
            if (needDefaultPlaceHolderImg) {
                //首次，加载的是缓存数据，需要默认占位符
                ImageLoadUtils.loadBannerRoundBg(helper.image.getContext(),
                        ImageLoadUtils.toThumbnailUrl(width,height,item.getBigIcon()), helper.image);

            } else {
                //非首次，在有加载本地缓存数据的基础上，就不需要占位符了，流畅加载体验
                ImageLoadUtils.loadBannerRoundBgWithOutPlaceHolder(helper.image.getContext(),
                        ImageLoadUtils.toThumbnailUrl(width,height,item.getBigIcon()), helper.image);
            }
        }
        helper.tvAdvName.setText(item.getAdvName());
    }


    public class ViewHolder extends BaseViewHolder {
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.tv_adv_name)
        TextView tvAdvName;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
