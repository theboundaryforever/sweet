package com.yuhuankj.tmxq.ui.find.mengxin;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.widget.CircleImageView;
import com.yuhuankj.tmxq.widget.LevelView;

/**
 * Created by chenran on 2017/10/3.
 */

public class SproutNewAdapter extends BaseQuickAdapter<SproutUserInfo, SproutNewAdapter.ViewHolder> {

    private Context context;

    public SproutNewAdapter(Context context) {
        super(R.layout.list_item_sprout);
        this.context = context;
    }

    @Override
    protected void convert(ViewHolder helper, final SproutUserInfo item) {
        String nick = item.getNick();
        if (!TextUtils.isEmpty(nick) && nick.length() > 6) {
            nick = nick.substring(0, 6);
            helper.tv_userNick.setText(mContext.getResources().getString(R.string.nick_length_max_six, nick));
        } else {
            helper.tv_userNick.setText(nick);
        }
        if (GlideContextCheckUtil.checkContextUsable(helper.civ_avatar.getContext())) {
            ImageLoadUtils.loadImage(helper.civ_avatar.getContext(), item.getAvatar(), helper.civ_avatar);
        }
        helper.iv_gender.setImageDrawable(context.getResources().getDrawable(
                item.getGender() == 1 ? R.drawable.icon_gender_man : R.drawable.icon_gender_woman));
        helper.tv_position.setVisibility(!TextUtils.isEmpty(item.getCity()) ? View.VISIBLE : View.GONE);
        helper.tv_position.setText(context.getResources().getString(R.string.sprout_fragment_position, item.getCity()));
        helper.lv_userLevel.setLevelImageViewHeight(DisplayUtility.dp2px(context, 15));
        helper.lv_userLevel.setCharmLevel(item.getCharmLevel());
        helper.lv_userLevel.setExperLevel(item.getExperLevel());
        SproutUserInRoomInfo userInRoom = item.getUserInRoom();
        boolean showChatingStatus = false;
        if (null != userInRoom) {
            if (userInRoom.getUid() > 0L) {
                showChatingStatus = true;
                helper.ll_chating.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onItemClick != null) {
                            onItemClick.onClickToEnterRoom(item);
                        }
                    }
                });
            }
        }
        helper.ll_chating.setVisibility(showChatingStatus ? View.VISIBLE : View.GONE);
        helper.tv_justComing.setVisibility(showChatingStatus ? View.GONE : View.VISIBLE);
        if (showChatingStatus) {
            GlideApp.with(mContext.getApplicationContext()).asGif()
                    .diskCacheStrategy(DiskCacheStrategy.DATA)
                    .load(R.drawable.anim_sprout_user_in_room)
                    .into(helper.iv_chatAnim);
        }
        helper.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClick != null) {
                    onItemClick.onClickToViewUserInfo(item);
                }
            }
        });
        helper.civ_avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClick != null) {
                    onItemClick.onClickToViewUserInfo(item);
                }
            }
        });
    }

    private OnSproutAdapterItemClickListener onItemClick;

    public void setOnItemClick(OnSproutAdapterItemClickListener onItemClick) {
        this.onItemClick = onItemClick;
    }

    static class ViewHolder extends BaseViewHolder {
        CircleImageView civ_avatar;
        ImageView iv_gender;
        ImageView iv_chatAnim;
        TextView tv_userNick;
        TextView tv_justComing;
        TextView tv_position;
        LevelView lv_userLevel;
        View container;
        View ll_chating;

        public ViewHolder(View itemView) {
            super(itemView);
            civ_avatar = itemView.findViewById(R.id.civ_avatar);
            iv_gender = itemView.findViewById(R.id.iv_gender);
            iv_chatAnim = itemView.findViewById(R.id.iv_chatAnim);
            tv_userNick = itemView.findViewById(R.id.tv_userNick);
            tv_position = itemView.findViewById(R.id.tv_position);
            tv_justComing = itemView.findViewById(R.id.tv_justComing);
            lv_userLevel = itemView.findViewById(R.id.lv_userLevel);
            container = itemView.findViewById(R.id.container);
            ll_chating = itemView.findViewById(R.id.ll_chating);
        }
    }

    public interface OnSproutAdapterItemClickListener {
        void onClickToEnterRoom(SproutUserInfo homeRoom);

        void onClickToViewUserInfo(SproutUserInfo homeRoom);
    }
}
