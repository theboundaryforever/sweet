package com.yuhuankj.tmxq.ui.liveroom.imroom.publicscreen.base;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tongdaxing.erban.libcommon.widget.messageview.IMessage;
import com.tongdaxing.erban.libcommon.widget.messageview.RoomMsgHolerType;

import java.util.ArrayList;
import java.util.List;

/**
 * 文件描述：房间消息适配器基类
 *
 * @auther：zwk
 * @data：2019/1/28
 */
public abstract class RoomMessageBaseAdapter<T extends IMessage> extends RecyclerView.Adapter<RoomMsgBaseHolder> {
    private List<T> messages;
    public Context mContext;

    public RoomMessageBaseAdapter(Context context) {
        this.mContext = context;
        if (messages == null) {
            messages = new ArrayList<>();
        }
    }

    @NonNull
    @Override
    public RoomMsgBaseHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return RoomMsgHolderFactory.createRoomMsgHolder(mContext, parent, viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull RoomMsgBaseHolder holder, int position) {
        holder.onDataTransformView(holder, messages.get(position), position);
        initItemListener(holder,messages.get(position),position);
    }

    protected abstract void initItemListener(RoomMsgBaseHolder holder, T t, int position);

    @Override
    public int getItemViewType(int position) {
        IMessage msg = messages.get(position);
        if (msg != null) {
            return msg.getMsgShowType();
        } else {
            return RoomMsgHolerType.MSG_TXT;
        }
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public List<? extends IMessage> getData() {
        return messages;
    }


    public void setNewData(List<T> messages) {
        if (messages == null || messages.size() <= 0) {
            return;
        }
        this.messages = messages;
        notifyDataSetChanged();
    }

    public void addMessages(List<T> messages) {
        if (messages == null || messages.size() <= 0) {
            return;
        }
        int size = this.messages.size();
        this.messages.addAll(messages);
        if (size > 0) {
            notifyItemChanged(size - 1, messages.size());
        } else {
            notifyDataSetChanged();
        }
    }

    public void addMessage(T messages) {
        if (messages == null) {
            return;
        }
        int size = this.messages.size();
        this.messages.add(messages);
        if (size > 0) {
            notifyItemChanged(size - 1, 1);
        } else {
            notifyDataSetChanged();
        }
    }
}
