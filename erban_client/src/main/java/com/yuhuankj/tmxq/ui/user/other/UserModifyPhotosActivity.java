package com.yuhuankj.tmxq.ui.user.other;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;

import com.jph.takephoto.app.TakePhotoActivity;
import com.jph.takephoto.compress.CompressConfig;
import com.jph.takephoto.model.TResult;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.file.JXFileUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.file.IFileCore;
import com.tongdaxing.xchat_core.file.IFileCoreClient;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_core.user.bean.UserPhoto;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.permission.PermissionActivity;
import com.yuhuankj.tmxq.widget.TitleBar;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cn.bingoogolapple.photopicker.widget.BGASortableNinePhotoLayout;
import io.realm.RealmList;

/**
 * 用户相册
 * Created by chenran on 2017/7/24.
 */

public class UserModifyPhotosActivity extends TakePhotoActivity implements IMvpBaseView, BGASortableNinePhotoLayout.Delegate {
    private static final String TAG = UserModifyPhotosActivity.class.getSimpleName();
    private static final int RC_CHOOSE_PHOTO = 1;
    private static final int RC_PHOTO_PREVIEW = 2;
    private final int MAX_PICTURE = 50;//最大照片数量
    PermissionActivity.CheckPermListener checkPermissionListener = new PermissionActivity.CheckPermListener() {
        @Override
        public void superPermission() {
            takePhoto();
        }
    };
    private long userId;
    private UserInfo userInfo;
    private GridView photoGridView;
    private TitleBar titleBar;
    private boolean isEditMode;
    private UserModifyPhotosAdapter adapter;
    private UserModifyPhotosActivity mActivity;
    private RealmList<UserPhoto> realmList;
    private BGASortableNinePhotoLayout bga;
    private TextView tvEdit;
    private boolean isSelf = true;
    private TextView tvShowHint;
    private int delPosition = -1;
    private ArrayList<Long> userPhotoIdList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_photos_modify);
        initView();
        mActivity = this;
        userId = getIntent().getLongExtra("userId", 0);
        isSelf = getIntent().getBooleanExtra("isSelf", true);
        userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(userId);
        if (userInfo != null) {
            updateView();
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfo(UserInfo info) {
        if (info.getUid() == userId) {
            userInfo = info;
            updateView();
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo info) {
        if (info.getUid() == userId) {
            userInfo = info;
            updateView();
            getDialogManager().dismissDialog();
        }
    }

    private void initView() {
        initTitleBar(getResources().getString(R.string.album));
        tvEdit = (TextView) findViewById(R.id.tv_edit);
        tvShowHint = (TextView) findViewById(R.id.tv_show_hint);
        bga = (BGASortableNinePhotoLayout) findViewById(R.id.snpl_moment_add_photos);
        //设置接口回调
        bga.setDelegate(this);
        if (isSelf) {
            tvEdit.setOnClickListener(v -> {
                ((TextView) v).setText(getResources().getString(bga.isEditable() ? R.string.edit : R.string.complete));
                tvShowHint.setVisibility(bga.isEditable() ? View.GONE : View.VISIBLE);
                if (bga.isEditable()) {
                    getDialogManager().showProgressDialog(this, getResources().getString(R.string.network_loading));
                    CoreManager.getCore(IUserCore.class).sortPhoto(userPhotoIdList);
                }
                bga.setEditable(!bga.isEditable());
            });
        }
    }

    private void updateView() {
        if (userInfo == null) {
            return;
        }
        realmList = userInfo.getPrivatePhoto();
        if (realmList == null) {
            return;
        }
        ArrayList<UserPhoto> userPhotos = new ArrayList<>();
        userPhotoIdList.clear();
        for (int i = 0; i < realmList.size(); i++) {
            UserPhoto userPhoto = realmList.get(i);
            if (userPhoto != null) {
                userPhotos.add(userPhoto);
                userPhotoIdList.add(userPhoto.getPid());
            }
        }
        bga.setData(userPhotos, true);
    }

    private void takePhoto() {
        File cameraOutFile = JXFileUtils.getTempFile(this, "picture_" + System.currentTimeMillis() + ".jpg");
        if (!cameraOutFile.getParentFile().exists()) {
            cameraOutFile.getParentFile().mkdirs();
        }
        Uri uri = Uri.fromFile(cameraOutFile);
        CompressConfig compressConfig = new CompressConfig.Builder().create();
        compressConfig.setMaxSize(500 * 1024);
        getTakePhoto().onEnableCompress(compressConfig, false);
        getTakePhoto().onPickFromCapture(uri);
    }

    private void checkPermissionAndStartCamera() {
        //低版本授权检查
        checkPermission(checkPermissionListener, R.string.ask_camera, android.Manifest.permission.CAMERA);
    }

    public void onPhotoDeleteClick(int position) {
        getDialogManager().showProgressDialog(this, getResources().getString(R.string.network_loading));
        if (null != userInfo.getPrivatePhoto() && userInfo.getPrivatePhoto().size() >= (position + 1)) {
            UserPhoto userPhoto = userInfo.getPrivatePhoto().get(position);
            CoreManager.getCore(IUserCore.class).requestDeletePhoto(userPhoto.getPid());
        }

    }

    private void uploadPicture(int position) {
        if (isSelf) {
            if (userInfo.getPrivatePhoto() != null && userInfo.getPrivatePhoto().size() >= 50) {
                toast(R.string.album_has_out_size);
                return;
            }
            ButtonItem upItem = new ButtonItem(getResources().getString(R.string.photo_upload), this::checkPermissionAndStartCamera);
            ButtonItem loaclItem = new ButtonItem(getResources().getString(R.string.local_album), () -> {
                CompressConfig compressConfig = new CompressConfig.Builder().create();
                compressConfig.setMaxSize(500 * 1024);
                getTakePhoto().onEnableCompress(compressConfig, true);
                getTakePhoto().onPickFromGallery();
            });
            List<ButtonItem> buttonItemList = new ArrayList<>();
            buttonItemList.add(upItem);
            buttonItemList.add(loaclItem);
            getDialogManager().showCommonPopupDialog(buttonItemList, getResources().getString(R.string.cancel), false);
        } else {
            ArrayList<UserPhoto> userPhotos1 = new ArrayList<>();
            userPhotos1.addAll(userInfo.getPrivatePhoto());
            Intent intent = new Intent(mActivity, ShowPhotoActivity.class);
            int position1 = isSelf ? position - 1 : position;
            intent.putExtra("position", position1);
            intent.putExtra("photoList", userPhotos1);
            intent.putExtra("userId", userId);
            startActivity(intent);
        }
    }


    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUploadPhoto(String url) {
        CoreManager.getCore(IUserCore.class).requestAddPhoto(url);
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUploadPhotoFail() {
        toast("操作失败，请检查网络");
        getDialogManager().dismissDialog();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestAddPhoto() {
        getDialogManager().dismissDialog();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestAddPhotoFaith(String msg) {
        if (!TextUtils.isEmpty(msg)) {
            toast(msg);
        }
        getDialogManager().dismissDialog();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestDeletePhoto(long pid) {
        if (delPosition != -1) {
            bga.removeItem(delPosition);
        }
        userPhotoIdList.remove(pid);
        getDialogManager().dismissDialog();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestDeletePhotoFaith(String msg) {
        toast("上传失败");
        getDialogManager().dismissDialog();
    }

    @Override
    public void takeSuccess(TResult result) {
        getDialogManager().showProgressDialog(this, getResources().getString(R.string.network_loading));
        CoreManager.getCore(IFileCore.class).uploadPhoto(new File(result.getImage().getCompressPath()));
    }

    @Override
    public void takeFail(TResult result, String msg) {
        toast(msg);
    }

    @Override
    public void onClickAddNinePhotoItem(BGASortableNinePhotoLayout sortableNinePhotoLayout, View view, int position, ArrayList<UserPhoto> models) {
        uploadPicture(position);
    }

    @Override
    public void onClickDeleteNinePhotoItem(BGASortableNinePhotoLayout sortableNinePhotoLayout, View view, int position, UserPhoto model, ArrayList<UserPhoto> models) {
        onPhotoDeleteClick(position);
        delPosition = position;
    }

    /**
     * 图片点击事件
     *
     * @param sortableNinePhotoLayout
     * @param view
     * @param position
     * @param model
     * @param models
     */
    @Override
    public void onClickNinePhotoItem(BGASortableNinePhotoLayout sortableNinePhotoLayout, View view, int position, UserPhoto model, ArrayList<UserPhoto> models) {
        Intent intent = new Intent(mActivity, ShowPhotoActivity.class);
        intent.putExtra("position", position);
        intent.putExtra("userId", model.getPid());
        startActivity(intent);
    }

    private Json getPhotoDataJson(ArrayList<String> models) {
        if (models == null) {
            return null;
        }
        Json json = new Json();
        for (int i = 0; i < models.size(); i++) {
            Json j = new Json();
            long temp = getPid(models.get(i));
            if (temp == 0) {
                continue;
            }
            j.set("pid", temp);
            j.set("photoUrl", models.get(i));
            json.set(i + "", j.toString());
        }
        return json;
    }

    private long getPid(String url) {
        if (!ListUtils.isListEmpty(realmList)) {
            for (int i = 0; i < realmList.size(); i++) {
                if (url.equals(realmList.get(i).getPhotoUrl())) {
                    return realmList.get(i).getPid();
                }
            }
        }
        return 0;
    }

    @Override
    public void onNinePhotoItemExchanged(BGASortableNinePhotoLayout sortableNinePhotoLayout, int fromPosition, int toPosition, ArrayList<UserPhoto> models) {
        //记录拖拽更改顺序后的相册列表
        userPhotoIdList.clear();
        //models就是拖拽移动之后的顺序
        if (null != models) {
            for (UserPhoto userPhoto : models) {
                userPhotoIdList.add(userPhoto.getPid());
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == RESULT_OK && requestCode == RC_CHOOSE_PHOTO) {
//            bga.addMoreData(BGAPhotoPickerActivity.getSelectedPhotos(data));
//        } else if (requestCode == RC_PHOTO_PREVIEW) {
//            bga.setData(BGAPhotoPickerActivity.getSelectedPhotos(data));
//        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onPhotoSortComplete(boolean isSuccess, String message) {
        LogUtils.d(TAG, "onPhotoSortComplete-isSuccess:" + isSuccess + " message:" + message);
        getDialogManager().dismissDialog();
        if (!isSuccess && !TextUtils.isEmpty(message)) {
            toast(message);
        }
    }

}
