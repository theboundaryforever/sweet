package com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.pay.IPayCore;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.base.presenter.BaseMvpPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.model.AuctionModel;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IAuctionPriorityChannelView;
import com.yuhuankj.tmxq.ui.me.charge.model.PayModel;

/**
 * 文件描述：排麦房的优先卡送礼P层
 *
 * @auther：zwk
 * @data：2019/7/30
 */
public class AuctionPriorityChannelPresenter extends BaseMvpPresenter<IAuctionPriorityChannelView> {
    private AuctionModel auctionModel;

    public AuctionPriorityChannelPresenter() {
        this.auctionModel = new AuctionModel();
    }


    /**
     * 赠送优先卡
     * @param roomId
     * @param hostUid
     * @param num
     */
    public void sendProiorityCard(long roomId,long hostUid,int num){
        auctionModel.sendAuctionPriorityCard(roomId, hostUid, num,new CallBack<String>() {
            @Override
            public void onSuccess(String data) {
                if (getMvpView() != null) {
                    CoreManager.getCore(IPayCore.class).updateMyselfWalletInfo();
                    getMvpView().showToast("赠送成功");
                }
            }

            @Override
            public void onFail(int code, String error) {
                if (getMvpView() != null) {
                    getMvpView().showToast(error);
                }
            }
        });
    }

    /**
     * 更新当前礼物的弹框的用户钱包信息
     */
    public void updateWalletInfo(){
        new PayModel().getWalletInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid(), new CallBack<WalletInfo>() {
            @Override
            public void onSuccess(WalletInfo data) {
                if (getMvpView() != null) {
                    getMvpView().updateWalletGoldCount(data);
                }
            }

            @Override
            public void onFail(int code, String error) {

            }
        });
    }
}
