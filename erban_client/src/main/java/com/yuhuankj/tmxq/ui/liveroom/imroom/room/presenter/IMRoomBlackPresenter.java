package com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.im.IMReportResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomModel;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMember;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IRoomBlackListView;

import java.util.List;

/**
 * <p> </p>
 *
 * @author jiahui
 * @date 2017/12/19
 */
public class IMRoomBlackPresenter extends AbstractMvpPresenter<IRoomBlackListView> {

    private final String TAG = IMRoomBlackPresenter.class.getSimpleName();


    public IMRoomBlackPresenter() {
    }

    public void queryRoomBlackList(int limit, int start) {
        RoomInfo cRoomInfo = RoomServiceScheduler.getCurrentRoomInfo();
        if (null == cRoomInfo) {
            return;
        }
        OkHttpManager.MyCallBack myCallBack = new OkHttpManager.MyCallBack<IMReportResult<List<IMRoomMember>>>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (null != getMvpView()) {
                    getMvpView().showErrMsg(e.getMessage());
                }
            }

            @Override
            public void onResponse(IMReportResult<List<IMRoomMember>> serviceResult) {
                LogUtils.d(TAG, "queryRoomBlackList-onResponse-result:" + serviceResult);
                if (null != getMvpView()) {
                    if (null == serviceResult) {
                        getMvpView().showErrMsg(BasicConfig.INSTANCE.getAppContext().getResources().getString(R.string.network_error));
                        return;
                    }
                    if (serviceResult.isSuccess()) {
                        getMvpView().showRoomBlackList(serviceResult.getData());
                    } else {
                        getMvpView().showErrMsg(serviceResult.getErrmsg());
                    }
                }
            }
        };
        new IMRoomModel().getRoomBlackList(cRoomInfo.getRoomId(), limit, start, myCallBack);

    }

    /**
     * 拉黑操作
     *
     * @param roomId
     * @param account
     * @param mark    true，拉黑，false：移除拉黑
     */
    public void removeUserFromBlackList(long roomId, final String account, final boolean mark) {
        IMRoomMessageManager.get().markBlackList(account, mark, new OkHttpManager.MyCallBack<IMReportResult>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().showErrMsg(e.getMessage());
                }
            }

            @Override
            public void onResponse(IMReportResult reportResult) {
                if (null == getMvpView()) {
                    return;
                }
                if (null == reportResult) {
                    getMvpView().showErrMsg(BasicConfig.INSTANCE.getAppContext().getResources().getString(R.string.network_error));
                    return;
                }
                if (reportResult.isSuccess()) {
                    getMvpView().removeUserFromBlackList(account);
                } else {
                    getMvpView().showErrMsg(reportResult.getErrmsg());
                }
            }
        });
    }
}
