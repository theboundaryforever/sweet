package com.yuhuankj.tmxq.ui.search.ui.fragment;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.erban.libcommon.utils.config.SpEvent;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.room.bean.SearchRoomPersonInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.fragment.BaseMvpFragment;
import com.yuhuankj.tmxq.ui.search.interfaces.SearchFriendView;
import com.yuhuankj.tmxq.ui.search.presenter.SearchFriendPresenter;
import com.yuhuankj.tmxq.ui.search.ui.adapter.SearchRoomPersonAdapter;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 搜索朋友界面
 *
 * @author weihaitao
 * @date 2017/10/3
 */
@CreatePresenter(SearchFriendPresenter.class)
public class SearchFriendFragment extends BaseMvpFragment<SearchFriendView, SearchFriendPresenter> implements SearchFriendView, SwipeRefreshLayout.OnRefreshListener {

    private final String TAG = SearchFriendFragment.class.getSimpleName();

    private RecyclerView rv_searchFriend;
    private SearchRoomPersonAdapter searchRoomPersonAdapter;
    private TextView tv_search;
    private EditText searchEdit;
    private ImageView iv_inputDelete;
    private TagFlowLayout tfl_searchHistory;
    private LayoutInflater mInflater;
    private TagAdapter<String> adapter;
    private Json searchCacheJson;
    private View ll_searchHistory;
    private View clearSearchHistory;
    private SwipeRefreshLayout srl_refresh;
    private String lastSearchContent = null;
    private View ll_searchEmpty;

    @Override
    public void initiate() { }

    @Override
    public void onFindViews() {
        srl_refresh = (SwipeRefreshLayout) mView.findViewById(R.id.srl_refresh);
        rv_searchFriend = (RecyclerView) mView.findViewById(R.id.rv_searchFriend);
        ll_searchEmpty = mView.findViewById(R.id.ll_searchEmpty);
        tfl_searchHistory = (TagFlowLayout) mView.findViewById(R.id.tfl_searchHistory);
        tv_search = (TextView) mView.findViewById(R.id.tv_search);
        iv_inputDelete = (ImageView) mView.findViewById(R.id.iv_inputDelete);
        ll_searchHistory = mView.findViewById(R.id.ll_search_history);
        clearSearchHistory = mView.findViewById(R.id.tv_searchClearHistory);
        searchEdit = (EditText) mView.findViewById(R.id.search_edit);
        GrowingIO.getInstance().trackEditText(searchEdit);
        searchEdit.setFilters(new InputFilter[]{enterBlankInputFilter});
        searchEdit.addTextChangedListener(textWatcher);
        searchEdit.setHint(R.string.search_friend_hint);
        rv_searchFriend.setLayoutManager(new RecyclerViewNoBugLinearLayoutManager(getActivity()));
        searchRoomPersonAdapter = new SearchRoomPersonAdapter(getActivity());
        rv_searchFriend.setAdapter(searchRoomPersonAdapter);
        srl_refresh.setEnabled(true);
    }

    InputFilter enterBlankInputFilter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            if (source.equals(" ") || source.toString().contentEquals("\n")) {
                return "";
            } else {
                return null;
            }
        }
    };

    @Override
    public void onSetListener() {
        iv_inputDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchEdit.setText(null);
            }
        });
        tv_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search(searchEdit.getText().toString());
            }
        });
        clearSearchHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearSearchHistory();
            }
        });
        searchRoomPersonAdapter.setOnItemClick(new SearchRoomPersonAdapter.OnSearchAdapterItemClickListener() {
            @Override
            public void onItemClicked(SearchRoomPersonInfo homeRoom) {
                UserInfoActivity.start(getActivity(),homeRoom.getUid());
            }
        });
        srl_refresh.setOnRefreshListener(this);
    }

    /**
     * 数据懒加载
     */
    @Override
    public void loadLazyIfYouWant() {
        if(null == searchCacheJson){
            initSearchHistory();
        }
    }

    private void markSearchContent(String s) {
        if (TextUtils.isEmpty(s)) {
            return;
        }
        if (searchCacheJson == null) {
            String searchCache = (String) SpUtils.get(getActivity(), SpEvent.search_friend_history, "");
            searchCacheJson = Json.parse(searchCache);
        }
        String[] strings = searchCacheJson.key_names();
        if (strings.length > 9) {
            searchCacheJson.remove(strings[0]);
        }
        searchCacheJson.set(s, "");
        SpUtils.put(getActivity(), SpEvent.search_friend_history, searchCacheJson.toString());
//        refreshSearchHistory();
    }

    private void clearSearchHistory() {
        SpUtils.put(getActivity(), SpEvent.search_friend_history, "");
        searchCacheJson = new Json();
        refreshSearchHistory();
    }

    private void initSearchHistory() {
        String searchCache = (String) SpUtils.get(getActivity(), SpEvent.search_friend_history, "");
        searchCacheJson = Json.parse(searchCache);
        refreshSearchHistory();
    }

    private void refreshSearchHistory() {
        List<String> jsonList = new ArrayList<>();
        if(null == searchCacheJson){
            searchCacheJson = new Json();
        }
        for (String key : searchCacheJson.key_names()) {
            jsonList.add(key);
        }

        if (jsonList.size() > 0) {
            Collections.reverse(jsonList);
            ll_searchHistory.setVisibility(View.VISIBLE);
        } else {
            ll_searchHistory.setVisibility(View.GONE);
            return;
        }
        mInflater = LayoutInflater.from(getActivity());
        adapter = new TagAdapter<String>(jsonList) {
            @Override
            public View getView(FlowLayout parent, int position, String s) {
                TextView tv = (TextView) mInflater.inflate(R.layout.tv_flowlayout_menu_search, tfl_searchHistory, false);
                tv.setText(s);
                return tv;
            }

        };
        tfl_searchHistory.setAdapter(adapter);
        tfl_searchHistory.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
            @Override
            public boolean onTagClick(View view, int position, FlowLayout parent) {
                searchEdit.setText(jsonList.get(position));
                searchEdit.setSelection(searchEdit.getText().length());
                search(jsonList.get(position));
                return false;
            }
        });
    }

    private void search(String content){
        LogUtils.d(TAG,"search-content:"+content);
        lastSearchContent = content;
        markSearchContent(content);
        srl_refresh.setRefreshing(true);
        if (StringUtil.isEmpty(content)) {
            srl_refresh.setRefreshing(false);
            searchRoomPersonAdapter.replaceData(new ArrayList<>());
            searchRoomPersonAdapter.notifyDataSetChanged();
            ll_searchEmpty.setVisibility(View.VISIBLE);
        } else {
            getMvpPresenter().searchFriend(content);
        }
    }

    private void refreshSearchFriendList(List<SearchRoomPersonInfo> homeRooms) {
        boolean hasSearched = homeRooms != null && homeRooms.size() > 0;
        searchRoomPersonAdapter.replaceData(hasSearched ? homeRooms : new ArrayList<>());
        searchRoomPersonAdapter.notifyDataSetChanged();
        ll_searchEmpty.setVisibility(hasSearched ? View.GONE : View.VISIBLE);
        ll_searchHistory.setVisibility(View.GONE);

    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_search_friend;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LogUtils.d(TAG,"onDestroy");
    }

    /**
     * 搜索朋友 关键字搜索结果返回
     *
     * @param success
     * @param message
     * @param friends
     */
    @Override
    public void onFriendSearched(boolean success, String message, List friends) {
        srl_refresh.setRefreshing(false);
        if(success){
            refreshSearchFriendList(friends);
        }else{
            if(!TextUtils.isEmpty(message)){
                toast(message);
            }
        }
    }

    /**
     * Called when a swipe gesture triggers a refresh.
     */
    @Override
    public void onRefresh() {
        search(searchEdit.getText().toString());
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String content = searchEdit.getText().toString();
            if(TextUtils.isEmpty(content) || TextUtils.isEmpty(content.trim())){
                ll_searchEmpty.setVisibility(View.GONE);
                iv_inputDelete.setVisibility(View.INVISIBLE);
                //https://bugly.qq.com/v2/crash-reporting/crashes/23dbf7aab1/30767?pid=1
                if(null == searchCacheJson){
                    initSearchHistory();
                }else{
                    refreshSearchHistory();
                }
                searchRoomPersonAdapter.replaceData(new ArrayList<>());
                searchRoomPersonAdapter.notifyDataSetChanged();
            }else{
                iv_inputDelete.setVisibility(View.VISIBLE);
            }
        }
    };

}
