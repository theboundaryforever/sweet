package com.yuhuankj.tmxq.ui.liveroom.nimroom.widget;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;
import com.tongdaxing.xchat_core.im.custom.bean.CallUpAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.CallUpBean;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.GiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.MultiGiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.PublicChatRoomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomTipAttachment;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomModel;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.room.publicchatroom.PublicChatRoomController;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.me.noble.NobleBusinessManager;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;
import com.yuhuankj.tmxq.ui.widget.DividerItemDecoration;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.utils.Utils;
import com.yuhuankj.tmxq.widget.LevelView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

/**
 * 直播间消息界面
 *
 * @author chenran
 * @date 2017/7/26
 */
public class PublicMessageView extends FrameLayout {
    private RecyclerView messageListView;
    private TextView tvBottomTip;
    private MessageAdapter mMessageAdapter;
    private List<ChatRoomMessage> chatRoomMessages;
    private List<ChatRoomMessage> tempMessages;
    private Context context;
    private ScrollSpeedLinearLayoutManger layoutManger;
    private CompositeDisposable compositeDisposable;

    public PublicMessageView(Context context) {
        this(context, null);
    }

    public PublicMessageView(Context context, AttributeSet attr) {
        this(context, attr, 0);
    }

    public PublicMessageView(Context context, AttributeSet attr, int i) {
        super(context, attr, i);
        init(context);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        LogUtils.d(PublicChatRoomController.TAG, "onAttachedToWindow");
        initRoomMsgEventListener();
    }

    public void initRoomMsgEventListener() {
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
//        LogUtils.d(PublicChatRoomController.TAG,"initRoomMsgEventListener");
        compositeDisposable.add(IMNetEaseManager.get().getChatRoomMsgFlowable().subscribe(messages -> {
            if (messages.size() == 0) {
                return;
            }
            ChatRoomMessage chatRoomMessage = messages.get(0);
//                    LogUtils.d(PublicChatRoomController.TAG,"onRoomMsgFolowable receive");
            if (checkNoNeedMsg(chatRoomMessage)) {
//                        LogUtils.d(PublicChatRoomController.TAG,"onRoomMsgFolowable receive, then no show!");
                return;
            }

            onCurrentRoomReceiveNewMsg(messages);
        }));

        compositeDisposable.add(IMNetEaseManager.get().getChatRoomEventObservable().subscribe(roomEvent -> {
            if (roomEvent == null || roomEvent.getEvent() != RoomEvent.RECEIVE_MSG) {
                return;
            }
            ChatRoomMessage chatRoomMessage = roomEvent.getChatRoomMessage();
//                    LogUtils.d(PublicChatRoomController.TAG,"onRoomEventObservable receive");
            if (checkNoNeedMsg(chatRoomMessage)) {
//                        LogUtils.d(PublicChatRoomController.TAG,"onRoomEventObservable receive, then no show!");
                return;
            }

            tempMessages.clear();
            tempMessages.add(chatRoomMessage);
            onCurrentRoomReceiveNewMsg(tempMessages);
        }));
    }

    /**
     * 检查是否是公屏需要的消息，送礼物和谁来了的消息不加入公屏
     *
     * @param chatRoomMessage
     * @return
     */
    private boolean checkNoNeedMsg(ChatRoomMessage chatRoomMessage) {

        if (chatRoomMessage.getMsgType() == MsgTypeEnum.custom) {
            CustomAttachment attachment = (CustomAttachment) chatRoomMessage.getAttachment();
            return attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_ROOM_TIP;
        }

        return false;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        LogUtils.d(PublicChatRoomController.TAG, "onDetachedFromWindow");
        releaseRoomMsgEventDispose();
    }

    private void releaseRoomMsgEventDispose() {
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
            compositeDisposable = null;
        }
    }

    public void clear() {
        if (mMessageAdapter != null) {
            mMessageAdapter.getData().clear();
            mMessageAdapter.notifyDataSetChanged();
        }
    }

    boolean isFirstLoad = true;

    private void initData() {
        List<ChatRoomMessage> messages = IMNetEaseManager.get().messages;
        if (!ListUtils.isListEmpty(messages)) {
            chatRoomMessages.addAll(messages);
            mMessageAdapter.setNewData(chatRoomMessages);
            messageListView.scrollToPosition(messages.size() - 1);
        }
    }

    public boolean isEmply() {
        try {
            return mMessageAdapter.getData().size() <= 0;
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
    }

    public void setHeaderView(View vHeader) {
        removeAllHeaderView();
        mMessageAdapter.addHeaderView(vHeader);
    }

    public void removeAllHeaderView() {
        try {
            mMessageAdapter.removeAllHeaderView();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onCurrentRoomReceiveNewMsg(List<ChatRoomMessage> messages) {
        if (ListUtils.isListEmpty(messages)) {
            return;
        }
//        LogUtils.d(PublicChatRoomController.TAG,"onCurrentRoomReceiveNewMsg msg.size:"+messages.size());
        if (chatRoomMessages.size() == 0) {
            mMessageAdapter.setNewData(chatRoomMessages);
        }
        showTipsOrScrollToBottom(messages);
    }

    boolean isNotScrollingAndBottom = false;

    private void init(Context context) {
        this.context = context;
//        LogUtils.d(PublicChatRoomController.TAG,"init");
        compositeDisposable = new CompositeDisposable();
        // 内容区域
        layoutManger = new ScrollSpeedLinearLayoutManger(context);
        LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        //params.rightMargin = Utils.dip2px(context, 5);
        messageListView = new RecyclerView(context);
        messageListView.setLayoutParams(params);
        messageListView.setOverScrollMode(OVER_SCROLL_NEVER);
        messageListView.setHorizontalScrollBarEnabled(false);
        addView(messageListView);
        messageListView.setLayoutManager(layoutManger);
        messageListView.addItemDecoration(new DividerItemDecoration(context, layoutManger.getOrientation(), 3, R.color.transparent));
        mMessageAdapter = new MessageAdapter();
        messageListView.setAdapter(mMessageAdapter);

        // 底部有新消息
        tvBottomTip = new TextView(context);
        LayoutParams params1 = new LayoutParams(
                Utils.dip2px(context, 100F), Utils.dip2px(context, 30));
        params1.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
        params1.bottomMargin = Utils.dip2px(context, 0);
        tvBottomTip.setBackgroundResource(R.drawable.bg_messge_view_bottom_tip);
        tvBottomTip.setGravity(Gravity.CENTER);
        tvBottomTip.setText(context.getString(R.string.message_view_bottom_tip));
        tvBottomTip.setTextColor(Color.WHITE);
        tvBottomTip.setLayoutParams(params1);
        tvBottomTip.setVisibility(GONE);
        tvBottomTip.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                isNotScrollingAndBottom = true;
                tvBottomTip.setVisibility(GONE);
                messageListView.scrollToPosition(mMessageAdapter.getItemCount() - 1);
            }
        });
        addView(tvBottomTip);

        chatRoomMessages = new ArrayList<>();
        tempMessages = new ArrayList<>();
        initData();

        messageListView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
//                if (needUpdateDataSet && newState == RecyclerView.SCROLL_STATE_DRAGGING) {
//                    needUpdateDataSet = false;
//                    // 如果用户在滑动,并且显示的是tip,则需要跟新
//                    mMessageAdapter.notifyDataSetChanged();
//                }
//                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
//                    if (layoutManger.findLastCompletelyVisibleItemPosition() == mMessageAdapter.getItemCount() - 1) {
//                        tvBottomTip.setVisibility(GONE);
//                    }
//                }

                if (newState == RecyclerView.SCROLL_STATE_IDLE) {//RecyclerView现在不是滚动状态。
                    if (isSlideToBottom()) {
                        isNotScrollingAndBottom = true;
                        tvBottomTip.setVisibility(GONE);
                    }
                } else if (newState == RecyclerView.SCROLL_STATE_SETTLING) {//自动滚动的状态，此时手指已经离开屏幕，RecyclerView的滚动是自身的惯性在维持。
                    isNotScrollingAndBottom = false;
                } else if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {//RecyclerView处于被外力引导的滚动状态，比如手指正在拖着进行滚动。
                    isNotScrollingAndBottom = false;
                }
            }
        });
    }

    /**
     * 显示新消息提示并自动滚动到最底部
     *
     * @param messages
     */
    private void showTipsOrScrollToBottom(List<ChatRoomMessage> messages) {
        // 最后一个item是否显示出来
        messages = msgFilter(messages);
        if (isNotScrollingAndBottom) {
            if (tvBottomTip.getVisibility() == View.VISIBLE) {
                tvBottomTip.setVisibility(GONE);
            }
            mMessageAdapter.addMessages(messages);
            if (isFirstLoad) {//添加第一次显示的滚动效果
                isFirstLoad = false;
                messageListView.smoothScrollToPosition(mMessageAdapter.getItemCount() - 1);
            } else {//正常情况通过item动画显示效果
                messageListView.scrollToPosition(mMessageAdapter.getItemCount() - 1);
            }
        } else {
            if (tvBottomTip.getVisibility() == View.GONE) {
                tvBottomTip.setVisibility(VISIBLE);
            }
            mMessageAdapter.addMessages(messages);
        }
    }

    public boolean isSlideToBottom() {
        if (messageListView != null) {
            return messageListView.computeVerticalScrollExtent() + messageListView.computeVerticalScrollOffset()
                    >= messageListView.computeVerticalScrollRange();
        } else {
            return false;
        }
    }


//    private boolean needUpdateDataSet = false;

//    private void showTipsOrScrollToBottom(List<ChatRoomMessage> messages) {
//        // 最后一个item是否显示出来
//        int lastCompletelyVisibleItemPosition = layoutManger.findLastCompletelyVisibleItemPosition();
////        LogUtils.d(PublicChatRoomController.TAG,"showTipsOrScrollToBottom lastCompletelyVisibleItemPosition:"+lastCompletelyVisibleItemPosition);
//        if (lastCompletelyVisibleItemPosition == RecyclerView.NO_POSITION) {
//            tvBottomTip.setVisibility(GONE);
//            messages = msgFilter(messages);
//            chatRoomMessages.addAll(messages);
//            mMessageAdapter.notifyDataSetChanged();
//            messageListView.scrollToPosition(mMessageAdapter.getItemCount() - 1);
//            return;
//        }
//        //(lastCompletelyVisibleItemPosition == mMessageAdapter.getItemCount() - 1)
//        //上面这种判断方法有点问题，当有headView的时候判断错误
//        //直接调用提供的api判断能否向上滚动,暂时没出现问题
//        boolean needScroll = !messageListView.canScrollVertically(1);//(lastCompletelyVisibleItemPosition == mMessageAdapter.getItemCount() - 1);
////        LogUtils.d(PublicChatRoomController.TAG,"showTipsOrScrollToBottom needScroll:"+needScroll);
//        messages = msgFilter(messages);
//        chatRoomMessages.addAll(messages);
//        if (needScroll) {
//            needUpdateDataSet = false;
//            tvBottomTip.setVisibility(GONE);
//            mMessageAdapter.notifyDataSetChanged();
//            messageListView.smoothScrollToPosition(mMessageAdapter.getItemCount() - 1);
//        } else {
//            needUpdateDataSet = true;
//            tvBottomTip.setVisibility(VISIBLE);
//        }
//    }

    private List<ChatRoomMessage> msgFilter(List<ChatRoomMessage> chatRoomMessages) {
        List<ChatRoomMessage> messages = new ArrayList<>();
        for (ChatRoomMessage message : chatRoomMessages) {
            //拦截其他房间的信息
            if (PublicChatRoomController.filterOtherRoomMsg(message)) {
//                LogUtils.d(PublicChatRoomController.TAG,
//                        "msgFilter - 其他房间信息_被拦截，roomId = " + message.getSessionId());
                continue;
            }
//            try {
//                LogUtils.d(PublicChatRoomController.TAG,"msgFilter msg.content:"+((PublicChatRoomAttachment) message.getAttachment()).getMsg());
//            }catch (Exception ex){
//                ex.printStackTrace();
//            }
            messages.add(message);
        }
        return messages;
    }

    public void release() {
    }

    private static class MessageAdapter extends BaseQuickAdapter<ChatRoomMessage, BaseViewHolder> implements OnClickListener {
        private String account;

        MessageAdapter() {
            super(R.layout.list_item_public_chatrrom_msg);
        }

        public void addMessages(List<ChatRoomMessage> messages) {
            if (messages == null || messages.size() <= 0) {
                return;
            }
            int size = this.getData().size();
            this.getData().addAll(messages);
            if (size > 0) {
                notifyItemChanged(size - 1, messages.size());
            } else {
                notifyDataSetChanged();
            }
        }

        @Override
        protected void convert(BaseViewHolder baseViewHolder, ChatRoomMessage chatRoomMessage) {
            if (chatRoomMessage == null) {
                return;
            }
            try {
                if (baseViewHolder.getAdapterPosition() == getItemCount() - 1) {
                    RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) baseViewHolder.itemView.getLayoutParams();
                    params.bottomMargin = ScreenUtil.dip2px(15);
                    baseViewHolder.itemView.requestLayout();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            View rlPublicMsg = baseViewHolder.getView(R.id.rlPublicMsg);

            ImageView userIcon = baseViewHolder.getView(R.id.iv_user_icon_chat_room);
            TextView userNick = baseViewHolder.getView(R.id.tv_user_nick_chat_room);

            TextView content = baseViewHolder.getView(R.id.tv_content_chat_room);
            content.setVisibility(View.GONE);
            TextView tv_call_up_msg = baseViewHolder.getView(R.id.tv_call_up_msg);
            tv_call_up_msg.setVisibility(View.GONE);
            TextView tv_enter_call_up_room = baseViewHolder.getView(R.id.tv_enter_call_up_room);
            tv_enter_call_up_room.setVisibility(View.GONE);
            ImageView iv_call_up = baseViewHolder.getView(R.id.iv_call_up);
            iv_call_up.setVisibility(View.GONE);
            View blvDiv = baseViewHolder.getView(R.id.blvDiv);
            blvDiv.setVisibility(View.GONE);

            LevelView levelView = baseViewHolder.getView(R.id.level_chat_room);
            RelativeLayout linearLayout = baseViewHolder.getView(R.id.rl_content_chat_room_bg);
            ImageView ivUserNobleMedal = baseViewHolder.getView(R.id.ivUserNobleMedal);
            LinearLayout llGender = baseViewHolder.getView(R.id.llGender);
            View vGender = baseViewHolder.getView(R.id.vGender);
            TextView tvAge = baseViewHolder.getView(R.id.tvAge);
            levelView.setLevelImageViewHeight(DisplayUtility.dp2px(mContext, 17));
            if (chatRoomMessage.getMsgType() == MsgTypeEnum.custom) {
                CustomAttachment attachment = (CustomAttachment) chatRoomMessage.getAttachment();
                int experLevel = -1;
                if (chatRoomMessage.getAttachment() instanceof CustomAttachment) {
                    experLevel = attachment.getExperLevel();
                    if (experLevel > 0 && !(chatRoomMessage.getContent() + "").contains("甜甜工作人员")) {
                        levelView.setVisibility(VISIBLE);
                        levelView.setExperLevel(experLevel);
                    } else {
                        levelView.setVisibility(GONE);
                    }
                }

                if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_SUB_PUBLIC_CHAT_ROOM) {
                    //消息为空过滤
                    MsgAttachment chatRoomMessageAttachment = chatRoomMessage.getAttachment();
                    if (chatRoomMessageAttachment instanceof PublicChatRoomAttachment) {
                        String msg = ((PublicChatRoomAttachment) chatRoomMessageAttachment).getMsg();
                        if (TextUtils.isEmpty(msg)) {
                            baseViewHolder.getView(R.id.rlPublicMsg).setVisibility(GONE);
                            return;
                        }
                    }
                    baseViewHolder.getView(R.id.rlPublicMsg).setVisibility(VISIBLE);

                    content.setVisibility(View.VISIBLE);
                    setPublicChatRoom(chatRoomMessage, content, levelView, userNick,
                            userIcon, linearLayout, ivUserNobleMedal, llGender, vGender, tvAge);
                } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_FIRST_CALL_UP
                        && attachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_PUBLIC_CHAT_ROOM) {
                    tv_call_up_msg.setVisibility(View.VISIBLE);
                    tv_enter_call_up_room.setVisibility(View.VISIBLE);
                    iv_call_up.setVisibility(View.VISIBLE);
                    blvDiv.setVisibility(View.VISIBLE);
                    setCallUpRoomMsg(chatRoomMessage, tv_call_up_msg, tv_enter_call_up_room, levelView, userNick,
                            userIcon, linearLayout, ivUserNobleMedal, llGender, vGender, tvAge, rlPublicMsg);
                } else {
                    baseViewHolder.getView(R.id.rlPublicMsg).setVisibility(GONE);
                }
            } else {
                baseViewHolder.getView(R.id.rlPublicMsg).setVisibility(GONE);
            }
        }

        private void setPublicChatRoom(ChatRoomMessage chatRoomMessage, TextView tvContent,
                                       LevelView levelView, TextView userNick,
                                       ImageView userIcon, RelativeLayout linearLayout,
                                       ImageView ivUserNobleMedal, LinearLayout llGender, View vGender,
                                       TextView tvAge) {
            MsgAttachment attachment = chatRoomMessage.getAttachment();
            Json json = null;
            String nobleMedalIconUrl = null;
            String msg = "";
            String uid = "";
            String avatarUrl = "";
            String senderNick = null;
            int gender = -1;
            int age = -1;
            int charmLevel = 0;
            int experLevel = -1;
            if (attachment instanceof PublicChatRoomAttachment) {
                msg = ((PublicChatRoomAttachment) attachment).getMsg();
                String params = ((PublicChatRoomAttachment) attachment).getParams();
                json = Json.parse(params);
                if (json.has(Constants.USER_CHARM_LEVEL)) {
                    charmLevel = json.num(Constants.USER_CHARM_LEVEL);
                }
                if (json.has(Constants.USER_EXPER_LEVEL)) {
                    experLevel = json.num(Constants.USER_EXPER_LEVEL);
                }
                if (json.has(Constants.USER_AVATAR)) {
                    avatarUrl = json.str(Constants.USER_AVATAR);
                }
                if (json.has(Constants.USER_NOBLE_MEDAL)) {
                    nobleMedalIconUrl = json.str(Constants.USER_NOBLE_MEDAL);
                }
                senderNick = json.str(Constants.USER_NICK, "我");
                LogUtils.d("setPublicChatRoom", params + "");
                if (json.has(Constants.USER_GENDER)) {
                    gender = json.num(Constants.USER_GENDER);
                }
                if (json.has(Constants.USER_AGE)) {
                    age = json.num(Constants.USER_AGE);
                }
                if (json.has(Constants.USER_UID)) {
                    uid = json.str(Constants.USER_UID);
                }
            } else {
                json = new Json();
            }
            Json finalJson = json;
            userIcon.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != mContext) {
                        openNobleUserInfoActivity(mContext, finalJson);
                    }
                }
            });
            linearLayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != mContext) {
                        openNobleUserInfoActivity(mContext, finalJson);
                    }
                }
            });
            if (-1 == experLevel) {
                if (chatRoomMessage.getChatRoomMessageExtension() == null) {
                    //2.7.7及之前的旧版本，刚发出去的消息，自己的
                    if (CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo() != null) {
                        experLevel = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getExperLevel();
                    }
                } else {
                    //旧版本，历史消息，拿的NIMUSERINFO的,并且PublicChatRoomAttachment.params里面有传递nick，所以可以兼容
//                    senderNick = chatRoomMessage.getChatRoomMessageExtension().getSenderNick();
                    try {
                        //2.7.7及之前的旧版本，历史消息，拿的进房
                        experLevel = (Integer) chatRoomMessage.getChatRoomMessageExtension().getSenderExtension().get("experLevel");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }


            if (!TextUtils.isEmpty(nobleMedalIconUrl)) {
                ivUserNobleMedal.setVisibility(View.VISIBLE);
                ImageLoadUtils.loadImage(mContext, nobleMedalIconUrl, ivUserNobleMedal);
            } else {
                ivUserNobleMedal.setVisibility(View.GONE);
            }

            if (experLevel > 0 || charmLevel > 0) {
                levelView.setVisibility(VISIBLE);
                levelView.setExperLevel(experLevel);
                levelView.setCharmLevel(charmLevel);
            } else {
                levelView.setVisibility(GONE);
            }
            ImageLoadUtils.loadCircleImage(mContext, avatarUrl, userIcon, R.drawable.ic_default_avator_circle);
            String nick = "";
            if (!TextUtils.isEmpty(senderNick)) {
                nick = senderNick;
            }
            userNick.setText(nick);
            tvContent.setText(msg);

            if (age <= 0) {
                llGender.setVisibility(GONE);
            } else {
                llGender.setVisibility(VISIBLE);
                tvAge.setText(age + "");
                if (gender == 1) {
                    //男
                    llGender.setSelected(true);
                    vGender.setBackgroundResource(R.drawable.ic_find_public_nanshi);
                } else {
                    llGender.setSelected(false);
                    vGender.setBackgroundResource(R.drawable.ic_find_public_nvshi);
                }
            }
        }

        private void callUpMsgClick(long roomId, long roomUid, int roomType) {
            if (null != mContext) {
                RoomServiceScheduler.getInstance().enterRoom(mContext, roomUid, roomType);
                if (0 == roomId) {
                    return;
                }
                new IMRoomModel().inRoomFromConvene(roomId, new HttpRequestCallBack<String>() {
                    @Override
                    public void onSuccess(String message, String response) {
                        LogUtils.d(TAG, "callUpMsgClick-->inRoomFromConvene-->onSuccess message:" + message);
                    }

                    @Override
                    public void onFailure(int code, String msg) {
                        LogUtils.d(TAG, "callUpMsgClick-->inRoomFromConvene-->onFailure msg:" + msg + " code:" + code);
                    }
                });
                //广播大厅-召集令消息-点击
                StatisticManager.get().onEvent(mContext,
                        StatisticModel.EVENT_ID_BROADCAST_CALL_UP_MSG_CLICK,
                        StatisticModel.getInstance().getUMAnalyCommonMap(mContext));
            }
        }

        private void setCallUpRoomMsg(ChatRoomMessage chatRoomMessage, TextView tvContent,
                                      TextView tvEnterCallUpRoom, LevelView levelView,
                                      TextView userNick, ImageView userIcon, RelativeLayout relativeLayout,
                                      ImageView ivUserNobleMedal, LinearLayout llGender, View vGender,
                                      TextView tvAge, View rlPublicMsg) {
            MsgAttachment attachment = chatRoomMessage.getAttachment();
            String nobleMedalIconUrl = null;
            String msg = "";

            String avatarUrl = "";
            String senderNick = null;
            int gender = -1;
            int age = -1;
            int charmLevel = 0;
            int experLevel = -1;

            CallUpBean callUpBean = null;
            if (attachment instanceof CallUpAttachment) {
                CallUpAttachment callUpAttachment = ((CallUpAttachment) attachment);
                charmLevel = callUpAttachment.getCharmLevel();
                experLevel = callUpAttachment.getExperLevel();
                callUpBean = callUpAttachment.getDataInfo();
            } else {
                rlPublicMsg.setVisibility(GONE);
                return;
            }

            if (null == callUpBean) {
                rlPublicMsg.setVisibility(GONE);
                return;
            }
            avatarUrl = callUpBean.getAvatar();
            nobleMedalIconUrl = callUpBean.getVipMedal();
            senderNick = callUpBean.getNick();
            gender = callUpBean.getGender();
            age = callUpBean.getAge();
            msg = callUpBean.getMessage();
            final int roomType = callUpBean.getType();
            final long roomUid = callUpBean.getRoomUid();
            final long roomId = callUpBean.getRoomId();
            tvEnterCallUpRoom.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    callUpMsgClick(roomId, roomUid, roomType);
                }
            });
            userIcon.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    callUpMsgClick(roomId, roomUid, roomType);
                }
            });
            relativeLayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    callUpMsgClick(roomId, roomUid, roomType);
                }
            });
            if (-1 == experLevel) {
                if (chatRoomMessage.getChatRoomMessageExtension() == null) {
                    //2.7.7及之前的旧版本，刚发出去的消息，自己的
                    if (CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo() != null) {
                        experLevel = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getExperLevel();
                    }
                } else {
                    try {
                        //2.7.7及之前的旧版本，历史消息，拿的进房
                        experLevel = (Integer) chatRoomMessage.getChatRoomMessageExtension().getSenderExtension().get("experLevel");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }


            if (!TextUtils.isEmpty(nobleMedalIconUrl)) {
                ivUserNobleMedal.setVisibility(View.VISIBLE);
                ImageLoadUtils.loadImage(mContext, nobleMedalIconUrl, ivUserNobleMedal);
            } else {
                ivUserNobleMedal.setVisibility(View.GONE);
            }

            if (experLevel > 0 || charmLevel > 0) {
                levelView.setVisibility(VISIBLE);
                levelView.setExperLevel(experLevel);
                levelView.setCharmLevel(charmLevel);
            } else {
                levelView.setVisibility(GONE);
            }
            ImageLoadUtils.loadCircleImage(mContext, avatarUrl, userIcon, R.drawable.ic_default_avator_circle);
            String nick = "";
            if (!TextUtils.isEmpty(senderNick)) {
                nick = senderNick;
            }
            userNick.setText(nick);
            tvContent.setText(msg);

            if (age <= 0) {
                llGender.setVisibility(GONE);
            } else {
                llGender.setVisibility(VISIBLE);
                tvAge.setText(String.valueOf(age));
                if (gender == 1) {
                    //男
                    llGender.setSelected(true);
                    vGender.setBackgroundResource(R.drawable.ic_find_public_nanshi);
                } else {
                    llGender.setSelected(false);
                    vGender.setBackgroundResource(R.drawable.ic_find_public_nvshi);
                }
            }
        }

        private void openNobleUserInfoActivity(Context context, Json json) {
            boolean isInvisible = false;
            long invisibleUid = 0L;
            long uid = 0L;
            int vipId = 0;
            int vipDate = 0;
            if (json.has(Constants.NOBLE_INVISIABLE_ENTER_ROOM)) {
                isInvisible = json.num(Constants.NOBLE_INVISIABLE_ENTER_ROOM) == 1;
            }
            if (json.has(Constants.NOBLE_INVISIABLE_UID)) {
                invisibleUid = json.num_l(Constants.NOBLE_INVISIABLE_UID);
            }
            if (json.has(Constants.USER_UID)) {
                uid = json.num_l(Constants.USER_UID);
            }
            if (json.has(Constants.USER_MEDAL_ID)) {
                vipId = json.num(Constants.USER_MEDAL_ID);
            }
            if (json.has(Constants.USER_MEDAL_DATE)) {
                vipDate = json.num(Constants.USER_MEDAL_DATE);
            }
            UserInfoActivity.start(mContext, NobleBusinessManager.getNobleUserUID(vipId, vipDate, isInvisible, invisibleUid, uid));
        }

        @Override
        public void onClick(View v) {
            ChatRoomMessage chatRoomMessage = (ChatRoomMessage) v.getTag();
            if (chatRoomMessage.getMsgType() != MsgTypeEnum.tip) {
                if (chatRoomMessage.getMsgType() == MsgTypeEnum.text) {
                    account = chatRoomMessage.getFromAccount();
                } else if (chatRoomMessage.getMsgType() == MsgTypeEnum.notification) {
                    account = chatRoomMessage.getFromAccount();
                } else if (chatRoomMessage.getMsgType() == MsgTypeEnum.custom) {
                    CustomAttachment attachment = (CustomAttachment) chatRoomMessage.getAttachment();
                    if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT) {
                        GiftAttachment giftAttachment = (GiftAttachment) attachment;
                        if (giftAttachment != null) {
                            GiftReceiveInfo giftRecieveInfo = giftAttachment.getGiftRecieveInfo();
                            if (giftRecieveInfo != null) {
                                account = giftRecieveInfo.getUid() + "";
                            }

                        }


                    } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT) {
                        MultiGiftAttachment giftAttachment = (MultiGiftAttachment) attachment;
                        account = giftAttachment.getMultiGiftRecieveInfo().getUid() + "";
                    } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_ROOM_TIP) {
                        account = ((RoomTipAttachment) attachment).getUid() + "";
                    }
                }
                if (TextUtils.isEmpty(account)) {
                    return;
                }
                final List<ButtonItem> buttonItems = new ArrayList<>();
                List<ButtonItem> items = ButtonItemFactory.createAllRoomPublicScreenButtonItems(mContext, account);
                if (items == null) {
                    return;
                }
                buttonItems.addAll(items);
                ((BaseMvpActivity) mContext).getDialogManager().showCommonPopupDialog(buttonItems, "取消");
            }
        }
    }

    public void scrollToBottom() {
        if (null != mMessageAdapter && null != mMessageAdapter.getData()) {
            messageListView.scrollToPosition(mMessageAdapter.getData().size() - 1);
        }
    }
}
