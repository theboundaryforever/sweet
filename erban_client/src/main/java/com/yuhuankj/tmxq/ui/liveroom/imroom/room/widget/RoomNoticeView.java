package com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.base.AbstractMvpLinearLayout;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.R;

/**
 * 创建者      魏海涛
 * 创建时间    2019年4月25日 18:12:44
 * 描述        房间内房间公告界面封装view
 *
 * @author dell
 */
public class RoomNoticeView extends AbstractMvpLinearLayout implements IMvpBaseView {

    private final String TAG = RoomNoticeView.class.getSimpleName();

    private TextView tvRoomNotice;

    public RoomNoticeView(Context context) {
        this(context, null);
    }

    public RoomNoticeView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoomNoticeView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getViewLayoutId() {
        return 0;
    }

    @Override
    protected void initViewState() {

    }

    @Override
    public void initialView(Context context) {
        inflate(getContext(), R.layout.layout_single_audio_room_notice, this);
        setOrientation(LinearLayout.VERTICAL);
        tvRoomNotice = findViewById(R.id.tvRoomNotice);
        tvRoomNotice.setMovementMethod(new ScrollingMovementMethod());
    }

    @Override
    protected void initListener() {

    }

    public void playInAnim() {
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (null != roomInfo && null != tvRoomNotice) {
            LogUtils.d(TAG, "setVisibility roomNotice:" + roomInfo.getRoomNotice());
            if (TextUtils.isEmpty(roomInfo.getRoomNotice())) {
                tvRoomNotice.setText(BasicConfig.INSTANCE.getAppContext().getResources().getString(R.string.room_notice_default));
            } else {
                tvRoomNotice.setText(roomInfo.getRoomNotice());
            }

        }
        setVisibility(View.VISIBLE);
        ScaleAnimation scaleAnimation = new ScaleAnimation(0f, 1.0f, 0f, 1.0f,
                DisplayUtility.dp2px(getContext(), 10), 0);
        scaleAnimation.setDuration(500);
        startAnimation(scaleAnimation);
    }

    public void playOutAnim() {
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 0f, 1.0f, 0f,
                DisplayUtility.dp2px(getContext(), 10), 0);
        scaleAnimation.setDuration(500);
        scaleAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        startAnimation(scaleAnimation);
    }
}
