package com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.erban.libcommon.widget.MicroWaveView;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomEvent;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMember;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomQueueInfo;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.widget.CircleImageView;
import com.zego.zegoavkit2.soundlevel.ZegoSoundLevelInfo;

import java.util.List;

import io.reactivex.disposables.Disposable;


/**
 * 上麦布局界面
 *
 * @author zeda
 */
public class SingleAudioOwneMicrorView extends AbstractMicroView {

    private final String TAG = SingleAudioOwneMicrorView.class.getSimpleName();

    private Disposable subscribe;
    private View flMicro;
    private TextView tvErbanId;
    private TextView tvOnlineNum;
    private ImageView ivRoomTips;
    private TextView tvNick;
    private ImageView civHeadwear;
    private CircleImageView civAvatar;
    private ImageView tvState;
    private MicroWaveView wvSpeakState;
//    private UserInfo ownerUserInfo = null;
    private OnSingleOwnerMicroViewClickListener onSingleOwnerMicroViewClickListener;

    public SingleAudioOwneMicrorView(Context context) {
        super(context, null);
    }

    public SingleAudioOwneMicrorView(Context context, AttributeSet attr) {
        super(context, attr, 0);
    }

    public SingleAudioOwneMicrorView(Context context, AttributeSet attr, int i) {
        super(context, attr, i);
    }

    @Override
    protected int getMicroLayoutId() {
        return R.layout.layout_single_audio_room_owner_view;
    }

    @Override
    public void init(Context context) {
        super.init(context);
        flMicro = findViewById(R.id.flMicro);
        tvErbanId = findViewById(R.id.tvErbanId);
        tvOnlineNum = findViewById(R.id.tvOnlineNum);
        ivRoomTips = findViewById(R.id.ivRoomTips);
        tvNick = findViewById(R.id.tvNick);
        civHeadwear = findViewById(R.id.civHeadwear);
        civAvatar = findViewById(R.id.civAvatar);
        tvState = findViewById(R.id.tvState);
        wvSpeakState = findViewById(R.id.wvSpeakState);
        findViewById(R.id.rlAvatar).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                IMRoomQueueInfo roomQueueInfo = RoomDataManager.get().getRoomQueueMemberInfoByMicPosition(RoomDataManager.MIC_POSITION_BY_OWNER);
                if (null != roomQueueInfo && null != onMicroItemClickListener) {
                    onMicroItemClickListener.onMicroBtnClickListener(roomQueueInfo, RoomDataManager.MIC_POSITION_BY_OWNER);
                }
            }
        });
        ivRoomTips.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onSingleOwnerMicroViewClickListener) {
                    onSingleOwnerMicroViewClickListener.showRoomNoticeInOutAnim();
                }
            }
        });
        flMicro.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                flMicro.postDelayed(() -> calculateMicCenterPoint(), 1000);
                flMicro.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
        tvOnlineNum.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onSingleOwnerMicroViewClickListener) {
                    onSingleOwnerMicroViewClickListener.onOnlineMemberListClicked();
                }
            }
        });
    }

    @Override
    public void notifyDataSetChanged(int roomEvent) {
        LogUtils.d(TAG, "notifyDataSetChanged-roomEvent:" + roomEvent);
        //管理员增删操作，不刷新房主信息，避免刷新过于频繁
        if (roomEvent == RoomEvent.ROOM_MANAGER_ADD || roomEvent == RoomEvent.ROOM_MANAGER_REMOVE) {
            return;
        }
        updateRoomOwnerInfo();
        updateRoomOnLineStatus();
    }

    @Override
    public void notifyItemChanged(int position, int roomEvent) {
        LogUtils.d(TAG, "notifyItemChanged-roomEvent:" + roomEvent + " position:" + position);
        if (position == RoomDataManager.MIC_POSITION_BY_OWNER) {
            updateRoomOwnerInfo();
            updateRoomOnLineStatus();
        }
    }

    @Override
    protected void calculateMicCenterPoint() {
        int giftWidth = DisplayUtility.dp2px(getContext(), 80);
        int giftHeight = DisplayUtility.dp2px(getContext(), 80);
        int faceWidth = DisplayUtility.dp2px(getContext(), 80);
        int faceHeight = DisplayUtility.dp2px(getContext(), 80);

        // 算出房主麦位的位置
        int[] location = new int[2];
        flMicro.getLocationInWindow(location);
        int[] nameLocation = new int[2];
        tvNick.getLocationInWindow(nameLocation);
        int x = (location[0] + flMicro.getWidth() / 2) - giftWidth / 2;
        int y = location[1] >= nameLocation[1] ?
                ((location[1] + flMicro.getHeight() * 7 / 8) - giftHeight / 2) :
                ((nameLocation[1] + tvNick.getHeight() / 2) - giftHeight / 2);

        // 放置表情占位image view
        if (getFaceImageViews().get(-1) == null) {
            LayoutParams params = new LayoutParams(faceWidth, faceHeight);
            flMicro.getLocationInWindow(location);
            int[] containerLocation = new int[2];
            this.getLocationInWindow(containerLocation);
            params.leftMargin = ((location[0] - containerLocation[0] + flMicro.getWidth() / 2) - faceWidth / 2);
            params.topMargin = ((location[1] - containerLocation[1] + flMicro.getHeight() / 2) - faceHeight / 2);
            ImageView face = new ImageView(getContext());
            face.setLayoutParams(params);
            getFaceImageViews().put(-1, face);
            addView(face);
            //单人音频房间暂时没有发送表情业务，暂时隐藏
            face.setVisibility(View.GONE);
        }
        Point point = new Point(x, y);
        if (null == RoomDataManager.get().mMicPointMap) {
            RoomDataManager.get().mMicPointMap = new SparseArray<>();
        }
        RoomDataManager.get().mMicPointMap.put(-1, point);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        subscribe = IMRoomMessageManager.get().getIMRoomEventObservable().subscribe(this::onReceiveRoomEvent);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (subscribe != null) {
            subscribe.dispose();
            subscribe = null;
        }
    }

    private void onReceiveRoomEvent(IMRoomEvent roomEvent) {
        if (roomEvent == null || roomEvent.getEvent() == RoomEvent.NONE) {
            return;
        }
        switch (roomEvent.getEvent()) {
            case RoomEvent.SPEAK_STATE_CHANGE:
                onSpeakQueueUpdate(roomEvent.getMicPositionList());
                break;
            case IMRoomEvent.SPEAK_ZEGO_STATE_CHANGE:
                onZegoSpeakQueueUpdate2(roomEvent.getSpeakQueueMembersPosition());
                break;
            case IMRoomEvent.CURRENT_SPEAK_STATE_CHANGE:
                onCurrentSpeakUpdate2(roomEvent.getCurrentMicPosition(), roomEvent.getCurrentMicStreamLevel());
                break;
            default:
        }
    }

    /**
     * 更新房主信息
     */
    private void updateRoomOwnerInfo() {
        //nick
        RoomInfo cRoomInfo = RoomServiceScheduler.getCurrentRoomInfo();
        if (null != cRoomInfo) {
            if (!TextUtils.isEmpty(cRoomInfo.getTitle()) && cRoomInfo.getTitle().length() > 8) {
                tvNick.setText(getContext().getResources().getString(
                        R.string.nick_length_max_six, cRoomInfo.getTitle().substring(0, 8)));
            } else {
                tvNick.setText(cRoomInfo.getTitle());
            }
        }
        IMRoomMember imRoomMember = RoomDataManager.get().getRoomOwnerDefaultMemberInfo();
        if (imRoomMember == null)
            return;
        if (StringUtils.isNotEmpty(imRoomMember.getHalo())){
            wvSpeakState.setColor(Color.parseColor(imRoomMember.getHalo()));
        }else {
            wvSpeakState.setColor(0x55ffffff);
        }
        //erbanid
        tvErbanId.setText(getContext().getResources().getString(R.string.room_id, String.valueOf(imRoomMember.getErbanNo())));
        //avatar
        ImageLoadUtils.loadAvatar(getContext(), imRoomMember.getAvatar(), civAvatar);
        //--------头饰-----------
        if (GlideContextCheckUtil.checkContextUsable(getContext())) {
            String headwearUrl = imRoomMember.getHeadwearUrl();
            if (!TextUtils.isEmpty(headwearUrl)) {
                ImageLoadUtils.loadImage(getContext(),imRoomMember.getHeadwearUrl(),civHeadwear);
                civHeadwear.setVisibility(View.VISIBLE);
            } else {
                civHeadwear.setVisibility(View.GONE);
            }
        }
    }

    /**
     * 更新房间在线人数
     *
     * @param onLineNum
     */
    public void updateOnLineNum(int onLineNum) {
        if (null != tvOnlineNum) {
            tvOnlineNum.setText(tvOnlineNum.getContext().getResources().getString(R.string.room_online_num, onLineNum));
        }
    }

    /**
     * 更新房间在线状态
     */
    public void updateRoomOnLineStatus() {
        try {
            //这里需要在对应map更新的情况下调用
            IMRoomQueueInfo roomQueueInfo = RoomDataManager.get().getRoomQueueMemberInfoByMicPosition(-1);
            if (roomQueueInfo.mChatRoomMember != null) {
                tvState.setVisibility(View.GONE);
                wvSpeakState.setVisibility(View.VISIBLE);
            } else {
                tvState.setVisibility(View.VISIBLE);
                wvSpeakState.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            tvState.setVisibility(View.VISIBLE);
            wvSpeakState.setVisibility(View.GONE);
        }
    }

    /**
     * 声网的声浪监听
     *
     * @param positions
     */
    public void onSpeakQueueUpdate(List<Integer> positions) {
        for (int i = 0; i < positions.size(); i++) {
            if (positions.get(i) != RoomDataManager.MIC_POSITION_BY_OWNER) {
                continue;
            }
            if (wvSpeakState != null) {
                wvSpeakState.start();
            }
        }
    }

    @Override
    protected MicroWaveView getRoomOnerSpeakView() {
        return null;
    }

    @Override
    protected MicroWaveView getRoomNormalSpeakView(int pos) {
        return null;
    }

    /**
     * 即构除自己外其他人的声浪监听
     *
     * @param speakers
     */
    public void onZegoSpeakQueueUpdate2(List<ZegoSoundLevelInfo> speakers){
        for (int i = 0; i < speakers.size(); i++) {
            int pos = RoomDataManager.get().getMicPositionByStreamID(speakers.get(i).streamID);
            if (pos == -1){
                if (wvSpeakState!= null){
                    wvSpeakState.start();
                }
            }
        }
    }

    /**
     * 即构除自己的声浪监听
     *
     * @param currentMicPosition    当前麦位逻辑
     * @param currentMicStreamLevel
     */
    public void onCurrentSpeakUpdate2(int currentMicPosition, float currentMicStreamLevel) {
        if (currentMicPosition == Integer.MIN_VALUE) {
            return;
        }
        if (currentMicPosition == RoomDataManager.MIC_POSITION_BY_OWNER){
            if (wvSpeakState!= null){
                wvSpeakState.start();
            }
        }
    }

    public void setOnSingleOwnerMicroViewClickListener(OnSingleOwnerMicroViewClickListener listener) {
        this.onSingleOwnerMicroViewClickListener = listener;
    }

    @Override
    public void release() {
        super.release();
        // 移除所有的表情动画
        for (int i = -1; i < getFaceImageViews().size() - 1; i++) {
            ImageView imageView = getFaceImageViews().get(i);
            if (imageView == null) {
                continue;
            }
            imageView.setImageDrawable(null);
            imageView.clearAnimation();
        }

        // 移除监听器
        onSingleOwnerMicroViewClickListener = null;

    }

    public interface OnSingleOwnerMicroViewClickListener {
        /**
         * 长按-房间公告渐入动画
         */
        void showRoomNoticeInOutAnim();


        /**
         * 点击跳转房间在线用户列表
         */
        void onOnlineMemberListClicked();
    }
}
