package com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.ConveneUserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

/**
 * Created by huangmeng1 on 2018/1/18.
 */

public class RoomCallUpUserAdapter extends BaseQuickAdapter<ConveneUserInfo, BaseViewHolder> {


    private int imvIconWidthAndHeight;
    private OnConeveUserClickListener onConeveUserClickListener;

    public RoomCallUpUserAdapter() {
        super(R.layout.item_rv_coneve_user_list);
        imvIconWidthAndHeight = DisplayUtility.dp2px(mContext, 53);
    }

    public void setOnConeveUserClickListener(OnConeveUserClickListener listener) {
        this.onConeveUserClickListener = listener;
    }

    @Override
    protected void convert(BaseViewHolder helper, ConveneUserInfo item) {

        if (GlideContextCheckUtil.checkContextUsable(mContext)) {
            if (!TextUtils.isEmpty(item.getAvatar())) {
                ImageLoadUtils.loadCircleImage(mContext,
                        ImageLoadUtils.toThumbnailUrl(imvIconWidthAndHeight, imvIconWidthAndHeight, item.getAvatar()),
                        helper.getView(R.id.ivAvatar), R.drawable.bg_default_cover_round_placehold_size60);
            }

            TextView tvNick = helper.getView(R.id.tvNick);
            String nick = item.getNick();
            if (!TextUtils.isEmpty(nick) && nick.length() > 5) {
                nick = mContext.getResources().getString(R.string.nick_length_max_six, nick.substring(0, 5));
            }
            tvNick.setText(nick);

            ImageView ivGender = helper.getView(R.id.ivGender);
            ivGender.setImageDrawable(mContext.getResources().getDrawable(item.getGender() == 1 ? R.drawable.icon_man : R.drawable.icon_women));

            helper.getView(R.id.bltvAttention).setVisibility(item.isFans() ? View.GONE : View.VISIBLE);
            helper.getView(R.id.bltvAttention).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != onConeveUserClickListener) {
                        onConeveUserClickListener.onClickToOperaAttention(item.getUid());
                    }
                }
            });
            helper.getView(R.id.rlConeveUser).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != onConeveUserClickListener) {
                        onConeveUserClickListener.onClickToViewUserInfo(item.getUid());
                    }
                }
            });
        }
    }


    public interface OnConeveUserClickListener {

        void onClickToOperaAttention(long uid);

        void onClickToViewUserInfo(long uid);
    }

}
