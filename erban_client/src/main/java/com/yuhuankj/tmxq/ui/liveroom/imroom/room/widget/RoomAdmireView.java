package com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.juxiao.library_utils.DisplayUtils;
import com.noober.background.view.BLTextView;
import com.tongdaxing.erban.libcommon.base.AbstractMvpRelativeLayout;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.TouchAndClickOperaUtils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomAdmireTimeCounter;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomEvent;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.room.bean.RoomAdditional;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter.RoomAdmirePresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IRoomAdmireView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import io.reactivex.disposables.Disposable;

/**
 * 创建者      魏海涛
 * 创建时间    2019年4月25日 18:12:44
 * 描述        房间内右下角点赞模块的封装view
 * 64*82 max
 *
 * @author dell
 */
@CreatePresenter(RoomAdmirePresenter.class)
public class RoomAdmireView extends AbstractMvpRelativeLayout<IRoomAdmireView, RoomAdmirePresenter<IRoomAdmireView>>
        implements IRoomAdmireView, RoomAdmireTimeCounter.OnAdmireTimeUpdateListener/*, IDisposableAddListener*/ {

    private final String TAG = RoomAdmireView.class.getSimpleName();

    private TouchAndClickOperaUtils touchAndClickOperaUtils;
    private TouchAndClickOperaUtils.OnQuickClickListener onQuickClickListener;
    private SimpleDateFormat sdf;

    private TextView tvAdmireTips;
    private BLTextView bltvAdmireNum;
    private TextView tvAdmireTime;
    private ImageView ivAdmire;

    private Disposable subscribe;


    public RoomAdmireView(Context context) {
        this(context, null);
    }

    public RoomAdmireView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoomAdmireView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getViewLayoutId() {
        return 0;
    }

    @Override
    public void initialView(Context context) {
        inflate(getContext(), R.layout.layout_single_audio_room_admire, this);
        tvAdmireTips = findViewById(R.id.tvAdmireTips);
        bltvAdmireNum = findViewById(R.id.bltvAdmireNum);
        tvAdmireTime = findViewById(R.id.tvAdmireTime);
        ivAdmire = findViewById(R.id.ivAdmire);
    }

    public void initLayoutParams(Context context, int roomType){
        if (roomType == RoomInfo.ROOMTYPE_MULTI_AUDIO) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) getLayoutParams();
            if (null != lp && null != context) {
                lp.leftMargin = DisplayUtils.getScreenWidth(context) - DisplayUtils.dip2px(context, 10 + 64);
                lp.topMargin = DisplayUtils.getScreenHeight(context) - DisplayUtils.dip2px(context, 128 + 74);
                setLayoutParams(lp);
            }
        }
    }

    private void initTouchClickListener() {
        if (null == getContext()) {
            return;
        }
        LogUtils.d(TAG, "initTouchClickListener");
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (roomInfo  != null){
            if (roomInfo.getType() == RoomInfo.ROOMTYPE_SINGLE_AUDIO) {
                //单人房，保持可点击，不可拖动，xml布局通过alignParentX参数限制固定位置
                setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (null != onQuickClickListener) {
                            onQuickClickListener.onQuickClick();
                        }
                    }
                });
            }else if (roomInfo.getType() == RoomInfo.ROOMTYPE_MULTI_AUDIO) {
                //新的多人交友房，则可拖动，纯代码设置Lp
                setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (null != onQuickClickListener) {
                            onQuickClickListener.onQuickClick();
                        }
                    }
                });
                setOnTouchListener(null);
                if (null != touchAndClickOperaUtils) {
                    touchAndClickOperaUtils.setTargetView(this);
                    touchAndClickOperaUtils.setMarginButtom(DisplayUtility.dp2px(getContext(), 20));
                }
            }
        }
    }

    @Override
    protected void initListener() {
        LogUtils.d(TAG, "initListener");
        if (null == touchAndClickOperaUtils) {
            touchAndClickOperaUtils = new TouchAndClickOperaUtils(getContext());
            touchAndClickOperaUtils.setNeedCheckQuickClick(false);
            onQuickClickListener = new TouchAndClickOperaUtils.OnQuickClickListener() {
                @Override
                public void onQuickClick() {
                    //根据当前剩余点赞次数来判断，或者根据界面展示状态来判断，是点赞还是切换显示状态
                    LogUtils.d(TAG, "onQuickClick");
                    RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
                    RoomAdditional roomAdditional = RoomDataManager.get().getAdditional();
                    if (null == roomAdditional) {
                        return;
                    }
                    if (roomAdditional.getAdmireCount() > 0) {
                        LogUtils.d(TAG, "onQuickClick-->admireRoomOwner");
                        getMvpPresenter().admireRoomOwner();
                    } else {
                        if (tvAdmireTime.getVisibility() == View.VISIBLE) {
                            tvAdmireTime.setVisibility(View.INVISIBLE);
                            RoomDataManager.get().imRoomAdmireTimeCounterShow = false;
                            LogUtils.d(TAG, "onQuickClick-->倒计时中，切换倒计时的显示状态-->不可见");
                        } else if (tvAdmireTime.getVisibility() == View.INVISIBLE) {
                            tvAdmireTime.setVisibility(View.VISIBLE);
                            RoomDataManager.get().imRoomAdmireTimeCounterShow = true;
                            LogUtils.d(TAG, "onQuickClick-->倒计时中，切换倒计时的显示状态-->可见");
                        }
                    }

                    if (null != roomInfo && roomInfo.getType() == RoomInfo.ROOMTYPE_SINGLE_AUDIO) {
                        //个人房，[赞]控件，点击统计
                        StatisticManager.get().onEvent(getContext(),
                                StatisticModel.EVENT_ID_ROOM_ADMIRE_CLICK,
                                StatisticModel.getInstance().getUMAnalyCommonMap(getContext()));
                    }
                    if (null != roomInfo && roomInfo.getType() == RoomInfo.ROOMTYPE_MULTI_AUDIO) {
                        //新多人交友房间，[赞]控件，点击统计
                        StatisticManager.get().onEvent(getContext(),
                                StatisticModel.EVENT_ID_ROOM_MULIT_ADMIRE_CLICK,
                                StatisticModel.getInstance().getUMAnalyCommonMap(getContext()));
                    }
                }
            };
            touchAndClickOperaUtils.setListener(onQuickClickListener);
        }
    }

    @Override
    protected void initViewState() {
        LogUtils.d(TAG, "initViewState");
    }

    public void initRoomAdmireView() {
        LogUtils.d(TAG, "initRoomAdmireView-->initTouchClickListener");
        initTouchClickListener();
        LogUtils.d(TAG, "initRoomAdmireView-->updateRoomAdmireStatus");
        updateRoomAdmireStatus();
    }

    /**
     * 停止播放上下晃动动画
     */
    private void removeRoomAdmireViewAnim() {
        LogUtils.d(TAG, "removeRoomAdmireViewAnim");
        Animation animation = getAnimation();
        if (null != animation && !animation.hasEnded()) {
            animation.cancel();
        }

    }

    /**
     * 播放单人房，点赞控件上下晃动动画
     */
    private void playRoomAdmireViewAnim() {
        LogUtils.d(TAG, "playRoomAdmireViewAnim");

        //4.播放上下浮动动画
        Animation animation = getAnimation();
        if (null != animation && !animation.hasEnded()) {
            LogUtils.d(TAG, "playRoomAdmireViewAnim-动画播放中");
            return;
        }

        TranslateAnimation translateAni = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0f, Animation.RELATIVE_TO_SELF,
                0, Animation.RELATIVE_TO_SELF, -0.05f,
                Animation.RELATIVE_TO_SELF, 0.10f);
        translateAni.setDuration(3000);
        translateAni.setRepeatCount(Animation.INFINITE);
        translateAni.setRepeatMode(Animation.REVERSE);
        translateAni.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        LogUtils.d(TAG, "removeRoomAdmireViewAnim-->startAnimation");
        this.startAnimation(translateAni);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        LogUtils.d(TAG, "onAttachedToWindow");
        //3.初始化的时候调多一次，避免房间最小化等场景更新不及时
        getMvpPresenter().getRoomAdmireInfo();
        RoomAdmireTimeCounter.getInstance().addListener(this);
        subscribe = IMRoomMessageManager.get().getIMRoomEventObservable().subscribe(this::onReceiveRoomEvent);
    }

    private void onReceiveRoomEvent(IMRoomEvent roomEvent) {
        if (roomEvent == null || roomEvent.getEvent() == RoomEvent.NONE) {
            return;
        }
        switch (roomEvent.getEvent()) {
            case IMRoomEvent.ROOM_DETONATE_GIFT_NOTIFY:
                LogUtils.d(TAG, "onReceiveRoomEvent-人气礼物引爆通知，刷新点赞控件的显示状态");
//                refreshAdmireAndPopularGiftViewStatus(roomEvent.getDetonatingState());
                if (currentDetonatingState != roomEvent.getDetonatingState()){
                    updateRoomAdmireStatus();
                }
                break;
            default:
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        LogUtils.d(TAG, "onDetachedFromWindow");
        RoomAdmireTimeCounter.getInstance().removeListener(this);
        release();
    }

    /**
     * 直播时长更新回调
     *
     * @param millCounts 秒为单位
     */
    @Override
    public void onAdmireTimeUpdated(long millCounts) {
//        LogUtils.d(TAG, "onAdmireTimeUpdated millCounts:" + millCounts);
        String admireTimeTips = BasicConfig.INSTANCE.getAppContext()
                .getResources().getString(R.string.room_admire_time_tips,
                        parseTimeValue(millCounts));
//        LogUtils.d(TAG, "onAdmireTimeUpdated admireTimeTips:" + admireTimeTips);
        tvAdmireTime.setText(admireTimeTips);
    }

    /**
     * 一次计时循环结束，通知外部更新点赞信息，及界面展示情况
     */
    @Override
    public void onAdmireTimeOver() {
        getMvpPresenter().getRoomAdmireInfo();
    }

    /**
     * 将秒数转换成对应格式的时间字符串
     *
     * @param mseconds
     * @return
     */
    private String parseTimeValue(long mseconds) {
        if (null == sdf) {
            sdf = new SimpleDateFormat("mm:ss", Locale.getDefault());
            //TimeZone时区，加上这句话，解决解析出的时间比真实时长多出8小时的问题
            sdf.setTimeZone(TimeZone.getTimeZone("GMT+0"));
        }
        Date date = new Date(mseconds * 1000);
        String str = sdf.format(date);
//        LogUtils.d(TAG, "parseTimeValue mseconds:" + mseconds + "=" + str);
        return str;
    }

    public void release() {
        removeRoomAdmireViewAnim();
        if (null != touchAndClickOperaUtils) {
            touchAndClickOperaUtils.setTargetView(null);
            touchAndClickOperaUtils.setListener(null);
        }
        if (null != onQuickClickListener) {
            onQuickClickListener = null;
        }
        setOnClickListener(null);
        setOnTouchListener(null);
        if (subscribe != null) {
            subscribe.dispose();
            subscribe = null;
        }
    }

    @Override
    public void updateRoomAdmireStatus() {
        LogUtils.d(TAG, "updateRoomAdmireStatus additional:" + RoomDataManager.get().getAdditional());
        if (RoomDataManager.get().getAdditional() == null) {
            return;
        }
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (null != roomInfo && roomInfo.getType() == RoomInfo.ROOMTYPE_SINGLE_AUDIO) {
            //单人房，房主不可以对自己点赞
            if (RoomDataManager.get().isRoomOwner()) {
                LogUtils.d(TAG, "updateRoomAdmireStatus--单人房，房主,隐藏");
                setVisibility(View.GONE);
            } else if (RoomDataManager.get().getAdditional().getPlayState() != 1) {
                if (RoomDataManager.get().getAdditional().getAdmireCount() > 0) {
                    //1.剩余次数如果>0，则表示可以点赞,切换到为主播点赞视图
                    displayAdmireNum();
                    LogUtils.d(TAG, "updateRoomAdmireStatus--单人房，房主未开播且点赞次数不为0,显示点赞次数视图");
                } else {
                    LogUtils.d(TAG, "updateRoomAdmireStatus--单人房，房主未开播且点赞次数为0,隐藏倒计时，隐藏数量信息");
                    displayNoAdmireOpera();
                }
                setVisibility(View.VISIBLE);
                playRoomAdmireViewAnim();
            } else {
                if (RoomDataManager.get().getAdditional().getAdmireCount() > 0) {
                    LogUtils.d(TAG, "updateRoomAdmireStatus--单人房，房主已开播，点赞次数大于0，显示点赞次数视图");
                    displayAdmireNum();
                } else {
                    //2.剩余次数<=0,表示处于倒计时状态,切换到倒计时视图
                    LogUtils.d(TAG, "updateRoomAdmireStatus--单人房，房主已开播，点赞次数等于0，显示倒计时视图");
                    displayAdmireTimeCount();
                }
                setVisibility(View.VISIBLE);
                playRoomAdmireViewAnim();
            }
        } else if (null != roomInfo && roomInfo.getType() == RoomInfo.ROOMTYPE_MULTI_AUDIO) {
            //多人房，进房则默认仅有点赞次数视图<-->倒计时视图的来回切换
            //多人房，房主可以对自己点赞
            if (RoomDataManager.get().getAdditional().getIsAdmire() == 1) {
                if (RoomDataManager.get().getAdditional().getAdmireCount() > 0) {
                    //1.剩余次数如果>0，则表示可以点赞,切换到为主播点赞视图
                    displayAdmireNum();
                    LogUtils.d(TAG, "updateRoomAdmireStatus--新多人房，点赞次数不为0,显示点赞次数视图");
                } else {
                    LogUtils.d(TAG, "updateRoomAdmireStatus--新多人房，点赞次数为0,显示倒计时，隐藏数量信息");
                    displayAdmireTimeCount();
                }
                refreshAdmireAndPopularGiftViewStatus(RoomDataManager.get().getAdditional().getDetonatingState());
            }else {
                setVisibility(View.GONE);
            }
        }else {
            LogUtils.d(TAG, "updateRoomAdmireStatus--未知房間，隐藏");
            setVisibility(View.GONE);
        }
        currentDetonatingState = RoomDataManager.get().getAdditional().getDetonatingState();
    }

    //用于多人房 -- 避免频繁引爆礼物礼物调用更新
    private int currentDetonatingState = -2;

    private void refreshAdmireAndPopularGiftViewStatus(int detonatingState) {
        LogUtils.d(TAG, "refreshAdmireAndPopularGiftViewStatus detonatingState:" + detonatingState);
        //如果新多人房，有引爆礼物球动画，则自动隐藏点赞，否则自动打开点赞
        if (detonatingState > -1) {
            LogUtils.d(TAG, "refreshAdmireAndPopularGiftViewStatus--新多人房，显示引爆礼物，隐藏点赞");
            clearAnimation();
            setVisibility(View.GONE);
        } else {
            LogUtils.d(TAG, "refreshAdmireAndPopularGiftViewStatus--新多人房，显示点赞，隐藏引爆礼物");
            setVisibility(View.VISIBLE);
            playRoomAdmireViewAnim();
        }
    }

    /**
     * 显示空白点赞信息界面
     */
    private void displayNoAdmireOpera() {
        tvAdmireTips.setVisibility(View.GONE);
        bltvAdmireNum.setVisibility(View.GONE);
        tvAdmireTime.setVisibility(View.GONE);
    }

    /**
     * 显示点赞倒计时信息界面
     */
    private void displayAdmireTimeCount() {
        RelativeLayout.LayoutParams lp = (LayoutParams) ivAdmire.getLayoutParams();
        tvAdmireTips.setVisibility(View.GONE);
        bltvAdmireNum.setVisibility(View.GONE);
        tvAdmireTime.setVisibility(RoomDataManager.get().imRoomAdmireTimeCounterShow
                ? View.VISIBLE : View.INVISIBLE);
        onAdmireTimeUpdated(RoomAdmireTimeCounter.getInstance().getTimeCount());
        if (null != lp) {
            lp.addRule(RelativeLayout.BELOW, R.id.tvAdmireTime);
            ivAdmire.setLayoutParams(lp);
        }
    }

    /**
     * 显示剩余点赞次数
     */
    private void displayAdmireNum() {
        //1.剩余次数如果>0，则表示可以点赞,切换到为主播点赞视图
        RelativeLayout.LayoutParams lp = (LayoutParams) ivAdmire.getLayoutParams();
        tvAdmireTips.setVisibility(View.VISIBLE);
        bltvAdmireNum.setText(String.valueOf(RoomDataManager.get().getAdditional().getAdmireCount()));
        bltvAdmireNum.setVisibility(View.VISIBLE);
        tvAdmireTime.setVisibility(View.GONE);
        if (null != lp) {
            lp.addRule(RelativeLayout.BELOW, R.id.tvAdmireTips);
            ivAdmire.setLayoutParams(lp);
        }
    }
}
