package com.yuhuankj.tmxq.ui.liveroom.redpacket;

import java.io.Serializable;

/**
 * @author liaoxy
 * @Description:红包记录实体类
 * @date 2019/1/17 15:55
 */
public class RedPacketHistoryEnitity implements Serializable {
    private String goldNum;//
    private String redPacketDate;//
    private int redPacketState;//
    private String roomId;//
    private String redPacketId;
    private String roomNo = "";

    public String getRoomNo() {
        return roomNo;
    }

    public void setRoomNo(String roomNo) {
        this.roomNo = roomNo;
    }

    public String getRedPacketId() {
        return redPacketId;
    }

    public void setRedPacketId(String redPacketId) {
        this.redPacketId = redPacketId;
    }

    public String getGoldNum() {
        return goldNum;
    }

    public void setGoldNum(String goldNum) {
        this.goldNum = goldNum;
    }

    public String getRedPacketDate() {
        return redPacketDate;
    }

    public void setRedPacketDate(String redPacketDate) {
        this.redPacketDate = redPacketDate;
    }

    public int getRedPacketState() {
        return redPacketState;
    }

    public void setRedPacketState(int redPacketState) {
        this.redPacketState = redPacketState;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }
}
