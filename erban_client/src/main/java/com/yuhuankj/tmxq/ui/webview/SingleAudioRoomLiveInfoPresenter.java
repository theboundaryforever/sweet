package com.yuhuankj.tmxq.ui.webview;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomModel;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;

/**
 * <p> </p>
 *
 * @author jiahui
 * @date 2017/12/19
 */
public class SingleAudioRoomLiveInfoPresenter extends AbstractMvpPresenter<ISingleAudioRoomLiveInfoView> {

    private final String TAG = SingleAudioRoomLiveInfoPresenter.class.getSimpleName();

    public SingleAudioRoomLiveInfoPresenter() {

    }

    /**
     * js调用，通知客户端调用服务器的停播接口
     */
    public void stopPlayRoom() {
        LogUtils.d(TAG, "stopPlayRoom");
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        if (null == roomInfo) {
            return;
        }
        new IMRoomModel().stopPlayRoom(roomInfo.getRoomId(), new HttpRequestCallBack<String>() {
            @Override
            public void onSuccess(String message, String response) {
                LogUtils.d(TAG, "stopPlayRoom-->onSuccess message=" + message + " response:" + response);
                if (null != getMvpView()) {
                    getMvpView().finishLiveInfoView();
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                LogUtils.d(TAG, "stopPlayRoom-->onFailure code=" + code + " msg:" + msg);
                if (null != getMvpView()) {
                    getMvpView().showStopLiveErr(msg);
                }
            }
        });
    }

}
