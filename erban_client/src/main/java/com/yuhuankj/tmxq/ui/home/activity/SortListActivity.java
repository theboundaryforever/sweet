package com.yuhuankj.tmxq.ui.home.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.erban.libcommon.utils.config.SpEvent;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.home.IHomeCore;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.ui.room.adapter.CommonMagicIndicatorAdapter;
import com.yuhuankj.tmxq.ui.room.fragment.SortHotFragment;
import com.yuhuankj.tmxq.widget.magicindicator.MagicIndicator;
import com.yuhuankj.tmxq.widget.magicindicator.ViewPagerHelper;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.UIUtil;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.commonnavigator.CommonNavigator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/4/20.
 */

public class SortListActivity extends BaseActivity {


    @BindView(R.id.sort_indicator)
    MagicIndicator sortIndicator;
    @BindView(R.id.vp_sort)
    ViewPager vpSort;

    private List<Fragment> fragments;


    public static void start(Context context, int type) {
        Intent intent = new Intent(context, SortListActivity.class);
        intent.putExtra("index", type);
        context.startActivity(intent);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sort_list_activity);

        int index = getIntent().getIntExtra("index", 0) + 1;
        int id = getIntent().getIntExtra("id", -1);


        String s = (String) SpUtils.get(this, SpEvent.not_hot_menu, "");
        if (TextUtils.isEmpty(s)) {
            CoreManager.getCore(IHomeCore.class).getMainDataByMenu();
            toast("暂无数据，请稍后重试");
            finish();
        }


        LogUtils.d("not_hot_menu", s + "");
        Json parse = Json.parse(s);
        List<Json> jsonList = parse.jlist("data");
        List<TabInfo> tabInfos = new ArrayList<>();
        //热门的id是-1
        tabInfos.add(new TabInfo(-1, "火热"));
        for (Json json : jsonList) {
            TabInfo tabInfo = new TabInfo(json.num("id"), json.str("name"));
            tabInfos.add(tabInfo);
        }

        if (tabInfos != null && tabInfos.size() > index) {
            initTitleBar(tabInfos.get(index).getName());
        }
        ButterKnife.bind(this);

        initTabOrFragment(tabInfos);
        if (tabInfos != null && tabInfos.size() > index) {


            //如果有uid
            if (id > 0) {
                for (int i = 0; i < tabInfos.size(); i++) {
                    TabInfo tabInfo = tabInfos.get(i);
                    if (id == tabInfo.getId()) {
                        index = i;
                        break;
                    }
                }

            }
            vpSort.setCurrentItem(index);

        }
        initTitleBar("分类");
    }

    private void initTabOrFragment(List<TabInfo> tabInfos) {
        fragments = new ArrayList<>();
        for (TabInfo tabInfo : tabInfos) {

            fragments.add(SortHotFragment.newInstance(tabInfo));
        }

        FragmentPagerAdapter fragmentPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public int getCount() {
                return fragments.size();
            }

            @Override
            public Fragment getItem(int position) {
                return fragments.get(position);
            }
        };

        CommonNavigator commonNavigator = new CommonNavigator(this);
        CommonMagicIndicatorAdapter magicIndicatorAdapter = new CommonMagicIndicatorAdapter(this,
                tabInfos, UIUtil.dip2px(this, 4), UIUtil.dip2px(this, 15));

        magicIndicatorAdapter.setOnItemSelectListener(new CommonMagicIndicatorAdapter.OnItemSelectListener() {
            @Override
            public void onItemSelect(int position) {
                vpSort.setCurrentItem(position);
            }
        });
        commonNavigator.setAdapter(magicIndicatorAdapter);
        commonNavigator.setAdjustMode(false);

        sortIndicator.setNavigator(commonNavigator);
        vpSort.setAdapter(fragmentPagerAdapter);
        vpSort.setOffscreenPageLimit(10);
        ViewPagerHelper.bind(sortIndicator, vpSort);

    }

    public static void start(Context context, int i, String title) {
        Intent intent = new Intent(context, SortListActivity.class);
        intent.putExtra("id", i);
        context.startActivity(intent);
    }
}
