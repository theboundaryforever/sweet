package com.yuhuankj.tmxq.ui.liveroom.redpacket;

import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.netease.nim.uikit.common.util.sys.TimeUtil;
import com.yuhuankj.tmxq.R;

import java.util.List;

/**
 * @author liaoxy
 * @Description:红包记录adapter
 * @date 2019/1/17 15:54
 */
public class RedPacketHistoryAdapter extends BaseQuickAdapter<RedPacketHistoryEnitity, BaseViewHolder> {

    public RedPacketHistoryAdapter(int layoutResId, List<RedPacketHistoryEnitity> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, RedPacketHistoryEnitity item) {
        ImageView imvPortrait = helper.getView(R.id.imvPortrait);
        TextView tvMoneyCount = helper.getView(R.id.tvMoneyCount);
        TextView tvRoomNo = helper.getView(R.id.tvRoomNo);
        TextView tvTime = helper.getView(R.id.tvTime);
        TextView tvStatus = helper.getView(R.id.tvStatus);

        tvMoneyCount.setText(item.getGoldNum() + "金币");
        tvRoomNo.setText(String.format("(房间:%s)", item.getRoomNo()));
        try {
            String timeStr = TimeUtil.getDateTimeString(Long.valueOf(item.getRedPacketDate()), "yyyy.MM.dd HH:mm:ss");
            tvTime.setText(timeStr);
        } catch (Exception e) {
            e.printStackTrace();
            tvTime.setText(item.getRedPacketDate());
        }
        if (item.getRedPacketState() == 0) {
            //未领完
            tvStatus.setText("未领完");
        } else {
            //已领完
            tvStatus.setText("已领完");
        }
    }
}