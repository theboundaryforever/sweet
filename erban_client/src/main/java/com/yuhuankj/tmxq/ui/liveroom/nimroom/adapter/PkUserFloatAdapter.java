package com.yuhuankj.tmxq.ui.liveroom.nimroom.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.libcommon.utils.JavaUtil;
import com.tongdaxing.xchat_core.pk.bean.PkVoteInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

public class PkUserFloatAdapter extends BaseQuickAdapter<PkVoteInfo.PkUser, BaseViewHolder> {

    private int pkType = 1;//1：按投票人数PK，2：按礼物价值PK

    public PkUserFloatAdapter() {
        super(R.layout.item_pk_user_float);
    }

    private long curSelUid = 0L;

    public void setPkType(int pkType) {
        this.pkType = pkType;
    }

    private int curSelPos = -1;

    public int getCurSelPos() {
        return curSelPos;
    }

    public void setCurSelPos(int curSelPos) {
        this.curSelPos = curSelPos;
    }

    @Override
    protected void convert(BaseViewHolder helper, PkVoteInfo.PkUser item) {
        if (item == null) {
            return;
        }
        ImageView imvUser = helper.getView(R.id.imvUser);
        View vSelect = helper.getView(R.id.vSelect);
        TextView tvUserNick = helper.getView(R.id.tvUserNick);
        TextView tvPkVoteCount = helper.getView(R.id.tvPkVoteCount);

        ImageLoadUtils.loadCircleImage(mContext, item.getAvatar(), imvUser, R.drawable.default_user_head);
        tvUserNick.setText(item.getNick());
        //[2礼物价值/1投票人数],
        // 票数已经在外部，区分版本号设置过了,
        //参考 com.yuhuankj.tmxq.ui.liveroom.nimroom.widget.HomePartyPKView.setPkInfo
        //com.yuhuankj.tmxq.ui.liveroom.imroom.pk.RoomPKView.setPkInfo
        if (pkType == 1) {
            tvPkVoteCount.setText(item.getVoteCount() + "");
        } else {
            tvPkVoteCount.setText(item.getVoteCount() + "");
        }

        if (curSelUid == JavaUtil.str2long(item.getUid())) {
            curSelPos = helper.getAdapterPosition();
            vSelect.setVisibility(View.VISIBLE);
        } else {
            vSelect.setVisibility(View.INVISIBLE);
        }
    }

    public long getCurSelUid() {
        return curSelUid;
    }

    public void setCurSelUid(long cussSelUid) {
        this.curSelUid = cussSelUid;
    }


}
