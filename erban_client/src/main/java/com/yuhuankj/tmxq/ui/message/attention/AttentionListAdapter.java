package com.yuhuankj.tmxq.ui.message.attention;

import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.xchat_core.user.bean.AttentionInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.widget.CircleImageView;

import java.util.List;

/**
 * <p> 关注列表 </p>
 * Created by Administrator on 2017/11/17.
 */
public class AttentionListAdapter extends BaseQuickAdapter<AttentionInfo, BaseViewHolder> {

    int imgWidthHeight = 0;

    public AttentionListAdapter(List<AttentionInfo> attentionInfoList) {
        super(R.layout.attention_item, attentionInfoList);
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, final AttentionInfo attentionInfo) {
        if (attentionInfo == null) {
            return;
        }
        baseViewHolder.setText(R.id.tv_userName, attentionInfo.getNick())
                .setText(R.id.tv_fans_count, "粉丝:" + attentionInfo.getFansNum())
                .setVisible(R.id.view_line, baseViewHolder.getLayoutPosition() != getItemCount() - 1)
                .setVisible(R.id.online, attentionInfo.getOperatorStatus() != 2)
                .setVisible(R.id.find_him_layout, attentionInfo.getUserInRoom() != null)
                .setOnClickListener(R.id.find_him_layout, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (rylListener != null)
                            rylListener.findHimListeners(attentionInfo);
                    }
                })
                .setOnClickListener(R.id.rly, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (rylListener != null) {
                            rylListener.rylListeners(attentionInfo);
                        }
                    }
                });
        CircleImageView civAvatar = baseViewHolder.getView(R.id.civAvatar);

        if (GlideContextCheckUtil.checkContextUsable(civAvatar.getContext())) {
            if (0 == imgWidthHeight) {
                imgWidthHeight = DisplayUtility.dp2px(civAvatar.getContext(), 60);
            }

            ImageLoadUtils.loadBannerRoundBgWithOutPlaceHolder(civAvatar.getContext(),
                    ImageLoadUtils.toThumbnailUrl(imgWidthHeight, imgWidthHeight, attentionInfo.getAvatar()),
                    civAvatar,
                    imgWidthHeight,
                    R.drawable.bg_default_cover_round_placehold_size60);
        }

    }

    private onClickListener rylListener;

    public interface onClickListener {
        void rylListeners(AttentionInfo attentionInfo);

        void findHimListeners(AttentionInfo attentionInfo);
    }

    public void setRylListener(onClickListener onClickListener) {
        rylListener = onClickListener;
    }
}
