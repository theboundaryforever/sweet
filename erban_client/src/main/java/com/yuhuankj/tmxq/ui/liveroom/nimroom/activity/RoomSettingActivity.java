package com.yuhuankj.tmxq.ui.liveroom.nimroom.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.donkingliang.labels.LabelsView;
import com.growingio.android.sdk.collection.GrowingIO;
import com.kyleduo.switchbutton.SwitchButton;
import com.netease.nimlib.sdk.StatusCode;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_core.im.login.IIMLoginClient;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.room.IBgClient;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.presenter.RoomSettingPresenter;
import com.tongdaxing.xchat_core.room.view.IRoomSettingView;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.activity.IMRoomBlackListActivity;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.activity.IMRoomManagerListActivity;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.checkmicro.CheckMicroQueueActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author chenran
 * @date 2017/9/26
 */
@CreatePresenter(RoomSettingPresenter.class)
public class RoomSettingActivity extends BaseMvpActivity<IRoomSettingView, RoomSettingPresenter>
        implements LabelsView.OnLabelClickListener, View.OnClickListener, IRoomSettingView {

    private final String TAG = RoomSettingActivity.class.getSimpleName();

    private EditText nameEdit;
    private EditText pwdEdit;
    private LabelsView labelsView;
    private RoomInfo mServerRoomInfo;
    private List<String> labels;
    private String selectLabel;
    private RelativeLayout managerLayout;
    private RelativeLayout blackLayout;
    private LinearLayout mLabelLayout;

    private TabInfo mSelectTabInfo;
    private List<TabInfo> mTabInfoList;
    private RelativeLayout bgLayout;
    private String mBackPic;
    private String mBackName;
    private TextView bgName;
    private RelativeLayout topicLayout;
    private SwitchButton giftSwitchButton;
    private boolean isShowGiftEffect = false;
    private boolean originalShowGiftEffect = false;
    private boolean isShowCharmSwitch = false;
    private boolean originalShowCharmSwitch = false;
    private SwitchButton charmSwitchButton;
    private View charmBg;

    public static void start(Context context) {
        Intent intent = new Intent(context, RoomSettingActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_setting);
        setSwipeBackEnable(false);
        initTitleBar(getString(R.string.room_setting));
        mServerRoomInfo = BaseRoomServiceScheduler.getServerRoomInfo();
        LogUtils.d(TAG, "onCreate-mServerRoomInfo:" + mServerRoomInfo);

        int giftEffectSwitch = null == mServerRoomInfo ? 0 : mServerRoomInfo.getGiftEffectSwitch();
        isShowGiftEffect = giftEffectSwitch == 1;
        originalShowGiftEffect = giftEffectSwitch == 1;

        int charmSwitch = null == mServerRoomInfo ? 0 : mServerRoomInfo.getCharmSwitch();
        isShowCharmSwitch = charmSwitch == 1;
        originalShowCharmSwitch = charmSwitch == 1;

        //初始化界面要在获取数据之后
        initView();
        int charmOpen = null == mServerRoomInfo ? 0 : mServerRoomInfo.getCharmOpen();
        charmBg.setVisibility(null != mServerRoomInfo && (mServerRoomInfo.getType() == RoomInfo.ROOMTYPE_HOME_PARTY
                || mServerRoomInfo.getType() == RoomInfo.ROOMTYPE_MULTI_AUDIO) && charmOpen == 1 ? View.VISIBLE : View.GONE);

        mBackPic = null != mServerRoomInfo ? mServerRoomInfo.getBackPic() : "";
        mBackName = null != mServerRoomInfo ? mServerRoomInfo.getBackName() : "";
        bgName.setText(mBackName);
        getMvpPresenter().requestTagAll();
        boolean isRoomOwner = getMvpPresenter().checkIsRoomOwner();
        managerLayout.setVisibility(isRoomOwner ? View.VISIBLE : View.GONE);
        labelsView.setOnLabelClickListener(this);
    }

    /**
     * 隐藏虚拟键盘
     */
    public void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null && imm.isActive()) {
            imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);
        }
    }

    private void initView() {
        nameEdit = (EditText) findViewById(R.id.name_edit);
        GrowingIO.getInstance().trackEditText(nameEdit);
        pwdEdit = (EditText) findViewById(R.id.pwd_edit);
        GrowingIO.getInstance().trackEditText(pwdEdit);
        labelsView = (LabelsView) findViewById(R.id.labels_view);
        managerLayout = (RelativeLayout) findViewById(R.id.manager_layout);
        bgLayout = (RelativeLayout) findViewById(R.id.bg_layout);
        blackLayout = (RelativeLayout) findViewById(R.id.black_layout);
        mLabelLayout = (LinearLayout) findViewById(R.id.label_layout);
        bgName = (TextView) findViewById(R.id.tv_bg_name);
        topicLayout = (RelativeLayout) findViewById(R.id.topic_layout);
        giftSwitchButton = (SwitchButton) findViewById(R.id.switch_button_gift_effect_option);
        giftSwitchButton.setChecked(isShowGiftEffect);
        giftSwitchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isShowGiftEffect = isChecked;
            }
        });
        charmSwitchButton = (SwitchButton) findViewById(R.id.switch_button_glamour_total_option);
        charmSwitchButton.setChecked(isShowCharmSwitch);
        charmSwitchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isShowCharmSwitch = isChecked;
            }
        });
        charmBg = findViewById(R.id.glamour_total_layout);
        managerLayout.setOnClickListener(this);
        blackLayout.setOnClickListener(this);
        bgLayout.setOnClickListener(this);
        topicLayout.setOnClickListener(this);
        findViewById(R.id.tv_save).setOnClickListener(this);

        if (mServerRoomInfo != null) {
            nameEdit.setText(mServerRoomInfo.getTitle());
            pwdEdit.setText(mServerRoomInfo.getRoomPwd());
            selectLabel = mServerRoomInfo.getRoomTag();
        }

        if (selectLabel == null) {
            selectLabel = "";
        }

        findViewById(R.id.rl_room_tip).setOnClickListener(this);
        View rly_checkMicroQueue = findViewById(R.id.rly_checkMicroQueue);
        rly_checkMicroQueue.setOnClickListener(this);
        findViewById(R.id.rlRoomPwd).setVisibility(View.VISIBLE);
        if (null != mServerRoomInfo && mServerRoomInfo.getType() == RoomInfo.ROOMTYPE_HOME_PARTY) {
            rly_checkMicroQueue.setVisibility(View.VISIBLE);
            findViewById(R.id.vDivLast).setVisibility(View.VISIBLE);
        }
    }

    private void save() {
        //因为进房提示等界面会先行提交一遍room/update接口请求，因此这里需要重新获取本地数据
        mServerRoomInfo = BaseRoomServiceScheduler.getServerRoomInfo();
        if (mServerRoomInfo != null) {
            String name = null;
            String pwd = null;
            String label = null;
            String backPic = null;
            String backName = null;
            if (!nameEdit.getText().toString().equals(mServerRoomInfo.getTitle())) {
                name = nameEdit.getText().toString();
            }
            if (!pwdEdit.getText().toString().equals(mServerRoomInfo.getRoomPwd())) {
                pwd = pwdEdit.getText().toString();
            }
            if (!selectLabel.equals(mServerRoomInfo.getRoomTag())) {
                label = selectLabel;
            }
            if (!TextUtils.isEmpty(mBackPic) && !mBackPic.equals(mServerRoomInfo.getBackPic())) {
                backPic = mBackPic;
            }

            if (null != mBackName && !mBackName.equals(mServerRoomInfo.getBackName())) {
                backName = mBackName;
            }
            int giftEffectParams = isShowGiftEffect ? 1 : 0;
            int charmSwitch = isShowCharmSwitch ? 1 : 0;
            //更新房间信息
            if (null == backName && name == null && pwd == null && label == null
                    && backPic == null && originalShowGiftEffect == isShowGiftEffect
                    && originalShowCharmSwitch == isShowCharmSwitch) {
                toast("暂无更改");
            } else {
                hideKeyboard(nameEdit);
                getDialogManager().showProgressDialog(this, getString(R.string.network_loading));
                if (backPic != null) {
                    //仅对轰趴房做通知处理，IM单人音频房，
                    // 应通过com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager.chatRoomInfoUpdate
                    // 做统一更新处理
                    CoreManager.notifyClients(IBgClient.class, IBgClient.bgModify, backPic);
                }

                int id = mServerRoomInfo.getTagId();
                if (mSelectTabInfo != null) {
                    id = mSelectTabInfo.getId();
                }

                getMvpPresenter().updateRoomInfo(name, null, pwd, label,
                        id, backPic, backName, giftEffectParams, charmSwitch,
                        mServerRoomInfo.getRoomNotice(), mServerRoomInfo.getPlayInfo());
            }
        }
    }


    @Override
    public void onLabelClick(View label, String labelText, int position) {
        if (!ListUtils.isListEmpty(mTabInfoList)) {
            mSelectTabInfo = mTabInfoList.get(position);
        }
        selectLabel = labelText;
        labelsView.setSelects(position);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rly_checkMicroQueue:
                if (null != mServerRoomInfo) {
                    CheckMicroQueueActivity.start(this, mServerRoomInfo.getUid(), mServerRoomInfo.getType());
                }
                break;
            case R.id.rl_room_tip:
                //设置进入房间提示
                RoomPlayTipActivity.start(this);
                break;
            case R.id.manager_layout:
                if (null != mServerRoomInfo) {
                    if (mServerRoomInfo.getType() == RoomInfo.ROOMTYPE_HOME_PARTY) {
                        RoomManagerListActivity.start(this);
                    } else {
                        IMRoomManagerListActivity.start(this);
                    }
                }
                break;
            case R.id.tv_save:
                save();
                break;
            case R.id.black_layout:
                if (null != mServerRoomInfo) {
                    if (mServerRoomInfo.getType() == RoomInfo.ROOMTYPE_HOME_PARTY) {
                        RoomBlackListActivity.start(this);
                    } else {
                        IMRoomBlackListActivity.start(this);
                    }
                }
                break;
            case R.id.bg_layout:
                Intent intent = new Intent(this, RoomSelectBgActivity.class);
                intent.putExtra("backPic", mBackPic);
                startActivityForResult(intent, 2);
                break;
            case R.id.topic_layout:
                RoomTopicActivity.start(this);
                break;
            default:
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            if (resultCode == 2) {
                if (data != null) {
                    mBackName = data.getStringExtra("selectBgName");
                    mBackPic = data.getStringExtra("selectUrl");
                    this.bgName.setText(mBackName);
                }
            }
        }

    }

    @Override
    public void onResultRequestTagAllSuccess(List<TabInfo> tabInfoList) {
        mTabInfoList = tabInfoList;
        if (ListUtils.isListEmpty(tabInfoList)) {
            mLabelLayout.setVisibility(View.GONE);
            return;
        }
        mLabelLayout.setVisibility(View.VISIBLE);

        labels = new ArrayList<>();
        for (TabInfo tabInfo : tabInfoList) {
            labels.add(tabInfo.getName());
        }

        labelsView.setLabels((ArrayList<String>) labels);
        if (mServerRoomInfo != null && !TextUtils.isEmpty(mServerRoomInfo.getRoomTag())
                && labels.contains(mServerRoomInfo.getRoomTag())) {
            labelsView.setSelects(labels.indexOf(mServerRoomInfo.getRoomTag()));
        }
    }

    @Override
    public void onResultRequestTagAllFail(String error) {
        toast(error);
    }

    @Override
    public void updateRoomInfoSuccess(RoomInfo roomInfo) {
        //低价值礼物特效，开关变更通知
        getMvpPresenter().notifyGiftEffectStatusChanged(originalShowGiftEffect, isShowGiftEffect);
        originalShowCharmSwitch = isShowCharmSwitch;
        getDialogManager().dismissDialog();
        finish();
    }

    @Override
    public void updateRoomInfoFail(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }


    @CoreEvent(coreClientClass = IIMLoginClient.class)
    public void onKickedOut(StatusCode code) {
        finish();
    }

    @Override
    protected void onReceiveChatRoomEvent(RoomEvent roomEvent) {
        super.onReceiveChatRoomEvent(roomEvent);
        int event = roomEvent.getEvent();
        switch (event) {
            case RoomEvent.KICK_OUT_ROOM:
                //对于轰趴房，被踢出聊天室是以RoomEvent.KICK_OUT_ROOM事件告知，被踢下线是以onKickedOut方式告知
                //对于IM类房间，被踢出聊天室和被踢下线都是以RoomEvent.KICK_OUT_ROOM事件告知
                //参考com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager.noticeKickOutChatMember的调用逻辑
                finish();
                break;
            case RoomEvent.ROOM_MANAGER_REMOVE:
                if (!getMvpPresenter().checkIsRoomOwner()) {
                    toast(R.string.remove_room_manager);
                    finish();
                }
            default:
        }
    }
}
