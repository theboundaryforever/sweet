package com.yuhuankj.tmxq.ui.liveroom.imroom.gift;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftType;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.fragment.BaseLazyNoCoreFragment;
import com.yuhuankj.tmxq.ui.liveroom.imroom.gift.adapter.GiftPageListAdapter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.gift.listener.OnGiftListItemClickListener;
import com.yuhuankj.tmxq.ui.liveroom.imroom.gift.listener.OnItemClickListener;
import com.yuhuankj.tmxq.widget.PageIndicatorView;

import java.util.List;

/**
 * 文件描述：重构的礼物的单独的某个分类的礼物列表的页面
 *
 * @auther：zwk
 * @data：2019/5/15
 */
public class RoomGiftListFragment extends BaseLazyNoCoreFragment {
    private TextView tvGiftEmpty;
    private GiftType giftType;
    private RecyclerView rvGiftList;
    private GiftPageListAdapter adapter;
    private PageIndicatorView pivGiftPageIndicator;

    public static RoomGiftListFragment getInstance(GiftType giftType) {
        RoomGiftListFragment giftListFragment = new RoomGiftListFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("giftType", giftType);
        giftListFragment.setArguments(bundle);
        return giftListFragment;
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_room_gift_list;
    }

    @Override
    protected void onInitArguments(Bundle bundle) {
        super.onInitArguments(bundle);
        if (getArguments() != null) {
            giftType = (GiftType) getArguments().getSerializable("giftType");
        } else {
            giftType = GiftType.Unknow;
        }
    }

    @Override
    protected boolean hasDisposable() {
        return false;
    }

    @Override
    protected void initView(View view) {
        tvGiftEmpty = view.findViewById(R.id.tv_gift_content_empty);
        rvGiftList = view.findViewById(R.id.rv_gift_list);
        pivGiftPageIndicator = view.findViewById(R.id.piv_gift_page_indicator);
    }

    @Override
    public void onFindViews() {
    }

    @Override
    public void onSetListener() {
    }

    @Override
    public void initiate() {
        PagerSnapHelper pagerSnapHelper = new PagerSnapHelper();
        pagerSnapHelper.attachToRecyclerView(rvGiftList);
        rvGiftList.setLayoutManager(new RecyclerViewNoBugLinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        adapter = new GiftPageListAdapter(getContext(), giftType);
        adapter.setOnItemClickListener(onItemClickListener);
        rvGiftList.setAdapter(adapter);
        rvGiftList.addOnScrollListener(onScrollListener);
        tvGiftEmpty.setText("礼物空空如也！");
    }

    @Override
    protected void onLazyLoadData() {
        List<GiftInfo> gifts = CoreManager.getCore(IGiftCore.class).getGiftInfosByTypeIMRoom(giftType, giftType == GiftType.Exclusive);
        if (!ListUtils.isListEmpty(gifts)) {
            if (rvGiftList.getVisibility() == View.GONE) {
                rvGiftList.setVisibility(View.VISIBLE);
            }
            if (pivGiftPageIndicator != null && pivGiftPageIndicator.getVisibility() == View.GONE) {
                pivGiftPageIndicator.setVisibility(View.VISIBLE);
            }
            if (tvGiftEmpty.getVisibility() == View.VISIBLE) {
                tvGiftEmpty.setVisibility(View.GONE);
            }
            adapter.setGiftInfoList(gifts);
            if (pivGiftPageIndicator != null) {
                pivGiftPageIndicator.initIndicator((int) Math.ceil(gifts.size() * 1.0 / RoomGiftConstant.PAGE_SIZE));
            }
            if (onGiftItemClickListener != null && giftType == RoomGiftConstant.currentGiftType) {//默认选中的回调，因为默认位置不固定采用与点击事件类似方式
                if (gifts.size() > RoomGiftConstant.currentGiftTypeSelect) {
                    onGiftItemClickListener.onItemClick(gifts.get(RoomGiftConstant.currentGiftTypeSelect), giftType, RoomGiftConstant.currentGiftTypeSelect);
                } else {
                    onGiftItemClickListener.onItemClick(gifts.get(0), giftType, 0);
                }
            }
        } else {
            //空布局
            if (rvGiftList != null && rvGiftList.getVisibility() == View.VISIBLE) {
                rvGiftList.setVisibility(View.GONE);
            }
            if (pivGiftPageIndicator != null && pivGiftPageIndicator.getVisibility() == View.VISIBLE) {
                pivGiftPageIndicator.setVisibility(View.GONE);
            }
            if (tvGiftEmpty.getVisibility() == View.GONE) {
                tvGiftEmpty.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * 更新单独的分类下面的礼物列表
     */
    public void updateData() {
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    /**
     * 获取最新的礼物更新界面
     */
    public void onDataToUpdateView() {
        onLazyLoadData();
    }

    private OnItemClickListener onItemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(GiftInfo giftInfo, int position) {
            if (onGiftItemClickListener != null) {
                onGiftItemClickListener.onItemClick(giftInfo, giftType, position);
            }
        }
    };

    private RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                LinearLayoutManager l = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (l != null && pivGiftPageIndicator != null) {
                    int adapterNowPos = l.findFirstVisibleItemPosition();
                    pivGiftPageIndicator.setSelectedPage(adapterNowPos);
                }
            }
        }

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (rvGiftList != null) {
            rvGiftList.removeOnScrollListener(onScrollListener);
        }
        if (onItemClickListener != null) {
            onItemClickListener = null;
        }
        if (onGiftItemClickListener != null) {
            onGiftItemClickListener = null;
        }
        if (pivGiftPageIndicator != null) {
            //解决pivGiftPageIndicator的mParent泄露问题同时手动回收内存
            ViewParent parent = pivGiftPageIndicator.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeAllViews();
            }
            pivGiftPageIndicator.removeAllViews();
            pivGiftPageIndicator.clearViewList();
            pivGiftPageIndicator = null;
        }
    }

    private OnGiftListItemClickListener onGiftItemClickListener;

    public void setOnItemClickListener(OnGiftListItemClickListener onGiftItemClickListener) {
        this.onGiftItemClickListener = onGiftItemClickListener;
    }

    public GiftType getGiftType() {
        return giftType;
    }

}
