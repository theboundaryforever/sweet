package com.yuhuankj.tmxq.ui.liveroom.imroom.gift;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;
import com.tongdaxing.xchat_core.gift.GiftType;
import com.tongdaxing.xchat_core.gift.MultiGiftReceiveInfo;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_core.pay.IPayCore;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.yuhuankj.tmxq.ui.me.charge.model.PayModel;

import java.util.List;
/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/5/15
 */
public class RoomGiftPresenter extends AbstractMvpPresenter<IRoomGiftView> {
    private RoomGiftModel roomGiftModel;

    public RoomGiftPresenter() {
        if (roomGiftModel == null) {
            roomGiftModel = new RoomGiftModel();
        }
    }

    /**
     * 获取礼物分类的tab信息列表 -- 用于礼物分类导航栏数据显示
     *
     * @return
     */
    public List<TabInfo> getTabInfoList() {
        return roomGiftModel.getTabInfoList();
    }

    /**
     * 获取礼物列表对应分类的Fragment的list -- 用于礼物的分类切换
     *
     * @return
     */
    public List<RoomGiftListFragment> getGiftTabFragments() {
        return roomGiftModel.getGiftTabFragments();
    }

    /**
     * 获取礼物枚举类型对应的整形位置
     *
     * @param giftType
     * @return
     */
    public int getGiftTypePosition(GiftType giftType) {
        return roomGiftModel.getGiftTypePosition(giftType);
    }
    /**
     * 根据当前用户信息获取需要显示送礼对象的信息列表
     *
     * @return
     */
    public List<MicMemberInfo> getTopUserAvatarList(boolean isPersonal, long uid, int vipId, int vipDate, boolean isInvisible) {
        return roomGiftModel.getTopUserAvatarList(isPersonal, uid, vipId, vipDate, isInvisible);
    }

    /**
     * 根据当前用户信息获取需要显示送礼对象的信息列表
     *
     * @return
     */
    public List<MicMemberInfo> getTopUserAvatarList(boolean isPersonal, long uid) {
        return roomGiftModel.getTopUserAvatarList(isPersonal, uid, 0, 0, false);
    }


    /**
     * 新版本给单人赠送礼物
     * @param giftInfo
     * @param targetUid
     * @param roomUid
     * @param giftNum
     */
    public void sendSingleGift(GiftInfo giftInfo, long targetUid, long roomUid, final int giftNum){
        roomGiftModel.sendRoomSingleRoomGift(giftInfo, targetUid, roomUid, giftNum, new CallBack<GiftReceiveInfo>() {
            @Override
            public void onSuccess(GiftReceiveInfo data) {
                if (getMvpView() != null) {
                    if (data != null) {
                        if (data.getIsPea() == 1) {
                            //甜豆礼物
                            CoreManager.getCore(IPayCore.class).minusDouzi(data.getUsePeaNum());
                            getMvpView().sendGiftUpdateMinusBeanCount(data.getUsePeaNum());
                        } else {
                            //金币礼物
                            CoreManager.getCore(IPayCore.class).minusGold(data.getUseGiftPurseGold());
                            getMvpView().sendGiftUpdateMinusGoldCount(data.getUseGiftPurseGold());
                        }
                        if (giftInfo.getUserGiftPurseNum() != data.getUserGiftPurseNum()) {
                            giftInfo.setUserGiftPurseNum(data.getUserGiftPurseNum());
                            getMvpView().sendGiftUpdatePacketGiftCount(giftInfo);
                        }
                    }
                }
            }

            @Override
            public void onFail(int code, String error) {
                if (getMvpView() != null){
                    getMvpView().sendGiftErrorToast(code,error);
                }
            }
        });
    }


    /**
     * 新版本给全麦赠送礼物
     * @param roomUid
     * @param giftInfo
     * @param micAvatar
     * @param giftNum
     */
    public void sendRoomMultiGift(GiftInfo giftInfo, long roomUid,List<MicMemberInfo> micAvatar, final int giftNum){
        roomGiftModel.sendRoomMultiGift(giftInfo,roomUid, micAvatar,giftNum, new CallBack<MultiGiftReceiveInfo>() {
            @Override
            public void onSuccess(MultiGiftReceiveInfo data) {
                if (getMvpView() != null) {
                    if (data != null) {
                        if (data.getIsPea() == 1) {
                            //甜豆礼物
                            CoreManager.getCore(IPayCore.class).minusDouzi(data.getUsePeaNum());
                            getMvpView().sendGiftUpdateMinusBeanCount(data.getUsePeaNum());
                        } else {
                            //金币礼物
                            CoreManager.getCore(IPayCore.class).minusGold(data.getUseGiftPurseGold());
                            getMvpView().sendGiftUpdateMinusGoldCount(data.getUseGiftPurseGold());
                        }
                        if (giftInfo.getUserGiftPurseNum() != data.getUserGiftPurseNum()) {
                            giftInfo.setUserGiftPurseNum(data.getUserGiftPurseNum());
                            getMvpView().sendGiftUpdatePacketGiftCount(giftInfo);
                        }
                    }
                }
            }

            @Override
            public void onFail(int code, String error) {
                if (getMvpView() != null){
                    getMvpView().sendGiftErrorToast(code,error);
                }
            }
        });
    }


    /**
     * 更新当前礼物的弹框的用户钱包信息
     */
    public void updateGiftWalletInfo() {
        new PayModel().getWalletInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid(), new CallBack<WalletInfo>() {
            @Override
            public void onSuccess(WalletInfo data) {
                if (getMvpView() != null) {
                    getMvpView().updateGiftBeanAndGoldCount(data);
                }
            }

            @Override
            public void onFail(int code, String error) {

            }
        });
    }
}
