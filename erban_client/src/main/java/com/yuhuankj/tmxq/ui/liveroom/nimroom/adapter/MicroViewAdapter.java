package com.yuhuankj.tmxq.ui.liveroom.nimroom.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.netease.nim.uikit.glide.GlideApp;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.JavaUtil;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.widget.MicroWaveView;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.bean.RoomMicInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.uuzuche.lib_zxing.DisplayUtil;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.me.noble.NobleBusinessManager;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.widget.CacheImageView;
import com.yuhuankj.tmxq.widget.CircleImageView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Map;

/**
 * @author xiaoyu
 * @date 2017/12/18
 */

public class MicroViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final String TAG = MicroViewAdapter.class.getSimpleName();

    private OnMicroItemClickListener onMicroItemClickListener;

    public void setShowPairSelectStatus(boolean showPairSelectStatus) {
        AvRoomDataManager.get().setShowPairSelectStatus(showPairSelectStatus);
        LogUtils.d(TAG, "setShowPairSelectStatus-showPairSelectStatus:" + showPairSelectStatus);
        updateMicUi();
    }

    public void setShowCharm(boolean showCharm) {
        AvRoomDataManager.get().setShowCharm(showCharm);
        notifyDataSetChanged();
    }

    private Context context;
    private String avatarPicture = "";

    public synchronized void updateMicUi() {
        if (AvRoomDataManager.get().isShowCharm()) {
            loadMicCharm();
        }
        LogUtils.d(TAG, "updateMicUi-manMicTopCharmPosition:" + AvRoomDataManager.get().getManMicTopCharmPosition() + " ladyMicTopCharmPosition:" + AvRoomDataManager.get().getLadyMicTopCharmPosition());
        notifyDataSetChanged();
    }

    public synchronized void updateMicUiOnCharmChanged(Json charmList) {
        int micPosition = -2;//魅力值变化导致麦位哪个位置的view发生了变更。-1为全部，-2为没变化。（如果发生变化的麦位超过1个，则通知全部刷新）

        LogUtils.d(TAG, "updateMicUiOnCharmChanged MicroViewAdapter ROOM_CHARM");
        AvRoomDataManager.get().setLastRoomCharmList(AvRoomDataManager.get().getRoomCharmList());
        AvRoomDataManager.get().setRoomCharmList(charmList == null ? new Json() : charmList);
        Json roomCharmList = AvRoomDataManager.get().getRoomCharmList();
        AvRoomDataManager.get().setHatList(new JSONArray());
        AvRoomDataManager.get().setFemaleHatList(new JSONArray());
        try {
            if (AvRoomDataManager.get().getRoomCharmList().has("hatList")) {
                AvRoomDataManager.get().setHatList(new JSONArray(roomCharmList.getString("hatList")));
            }
            if (roomCharmList.has("femaleHatList")) {
                AvRoomDataManager.get().setFemaleHatList(new JSONArray(roomCharmList.getString("femaleHatList")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (AvRoomDataManager.get().isShowCharm()) {
            int lastManMicTopCharmPosition = AvRoomDataManager.get().getManMicTopCharmPosition();
            int lastLadyMicTopCharmPosition = AvRoomDataManager.get().getLadyMicTopCharmPosition();
            String lastManCharmImgUrl = AvRoomDataManager.get().getManCharmImgUrl();
            String lastLadyCharmImgUrl = AvRoomDataManager.get().getLadyCharmImgUrl();

            //重新load魅力值相关
            micPosition = loadMicCharm();
            LogUtils.d(TAG, "updateMicUi-micPosition:" + micPosition);

            int manMicTopCharmPosition = AvRoomDataManager.get().getManMicTopCharmPosition();
            int ladyMicTopCharmPosition = AvRoomDataManager.get().getLadyMicTopCharmPosition();
            String manCharmImgUrl = AvRoomDataManager.get().getManCharmImgUrl();
            String ladyCharmImgUrl = AvRoomDataManager.get().getLadyCharmImgUrl();

            if (lastManMicTopCharmPosition != manMicTopCharmPosition && micPosition != manMicTopCharmPosition) {//最高魅力值的人变化了、并且这个人不是发起魅力值变更的人，全麦刷新（最高的 + 发起的 发生变化超过了1人）
                LogUtils.d(TAG, "updateMicUi-最高魅力值的人变化了、并且这个人不是发起魅力值变更的人，全麦刷新（最高的 + 发起的 发生变化超过了1人）");
                micPosition = -1;
            }
            if (lastLadyMicTopCharmPosition != ladyMicTopCharmPosition && micPosition != ladyMicTopCharmPosition) {//最高魅力值的人变化了、并且这个人不是发起魅力值变更的人，全麦刷新（最高的 + 发起的 发生变化超过了1人）
                LogUtils.d(TAG, "updateMicUi-最高魅力值的人变化了、并且这个人不是发起魅力值变更的人，全麦刷新（最高的 + 发起的 发生变化超过了1人）");
                micPosition = -1;
            }

            if (manMicTopCharmPosition != -1 && !manCharmImgUrl.equals(lastManCharmImgUrl) && micPosition != manMicTopCharmPosition) {//头饰变了
                LogUtils.d(TAG, "updateMicUi-男士头饰变了");
                micPosition = -1;
            }
            if (ladyMicTopCharmPosition != -1 && !ladyCharmImgUrl.equals(lastLadyCharmImgUrl) && micPosition != ladyMicTopCharmPosition) {//头饰变了
                LogUtils.d(TAG, "updateMicUi-女士头饰变了");
                micPosition = -1;
            }

        }
        LogUtils.d(TAG, "updateMicUi-manMicTopCharmPosition:" + AvRoomDataManager.get().getManMicTopCharmPosition() + " ladyMicTopCharmPosition:" + AvRoomDataManager.get().getLadyMicTopCharmPosition() + " micPosition:" + micPosition);
        if (micPosition == -1) {
            notifyDataSetChanged();
        } else if (micPosition != -2) {//-2没变化
            notifyItemChanged(micPosition);
        }
    }

    /**
     * 重新load魅力值
     *
     * @return 魅力值更新的麦位 -1为全部更新，-2为没更新
     */
    private int loadMicCharm() {
        Json roomCharmList = AvRoomDataManager.get().getRoomCharmList();
        Json lastRoomCharmList = AvRoomDataManager.get().getLastRoomCharmList();
        int micPosition = -2;//-1为多个位置更新，-2为没位置更新
        boolean isMultiMicPositionChanged = false;

        int manMaxCharm = -1, ladyMaxCharm = -1;//最高魅力值
        boolean isManMaxCharmRepeat = false, isLadyMaxCharmRepeat = false;//最高魅力值是否有重复
        SparseArray<RoomQueueInfo> mMicQueueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;
        if (roomCharmList == null || mMicQueueMemberMap == null) {
            AvRoomDataManager.get().setManMicTopCharmPosition(-1);
            AvRoomDataManager.get().setLadyMicTopCharmPosition(-1);
            return micPosition;
        }

        int size = mMicQueueMemberMap.size();
        //不算房主
        for (int i = 1; i < size; i++) {
            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(i);
            if (roomQueueInfo != null && roomQueueInfo.mChatRoomMember != null) {
                String account = roomQueueInfo.mChatRoomMember.getAccount();
                int micCharm = roomCharmList.num(account);
                int gender = roomQueueInfo.gender;

                if (lastRoomCharmList != null && lastRoomCharmList.num(account) != micCharm && !isMultiMicPositionChanged) {
                    if (micPosition == -2) {
                        micPosition = mMicQueueMemberMap.keyAt(i) + 1;//key从-1开始的......
                    } else {
                        micPosition = -1;
                        isMultiMicPositionChanged = true;
                    }
                }

                if (gender == 1) {
                    if (micCharm > manMaxCharm) {
                        manMaxCharm = micCharm;
                        isManMaxCharmRepeat = false;
                        AvRoomDataManager.get().setManMicTopCharmPosition(mMicQueueMemberMap.keyAt(i) + 1);
                    } else if (micCharm == manMaxCharm) {
                        isManMaxCharmRepeat = true;
                    }
                } else {
                    if (micCharm > ladyMaxCharm) {
                        ladyMaxCharm = micCharm;
                        isLadyMaxCharmRepeat = false;
                        AvRoomDataManager.get().setLadyMicTopCharmPosition(mMicQueueMemberMap.keyAt(i) + 1);
                    } else if (micCharm == ladyMaxCharm) {
                        isLadyMaxCharmRepeat = true;
                    }
                }
            }
        }

        int manCharmImgUrlIndex = getCharmImageIndex(manMaxCharm);
        JSONArray hatList = AvRoomDataManager.get().getHatList();
        try {
            AvRoomDataManager.get().setManCharmImgUrl(hatList != null ? hatList.getString(manCharmImgUrlIndex) : "");
        } catch (JSONException e) {
            e.printStackTrace();
            AvRoomDataManager.get().setManCharmImgUrl("");
        }
        //最高有重复也返-1
        if (isManMaxCharmRepeat || TextUtils.isEmpty(AvRoomDataManager.get().getManCharmImgUrl())) {
            AvRoomDataManager.get().setManMicTopCharmPosition(-1);
        }

        int ladyCharmImgUrlIndex = getCharmImageIndex(ladyMaxCharm);
        JSONArray femaleHatList = AvRoomDataManager.get().getFemaleHatList();
        try {
            AvRoomDataManager.get().setLadyCharmImgUrl(femaleHatList != null ? femaleHatList.getString(ladyCharmImgUrlIndex) : "");
        } catch (JSONException e) {
            e.printStackTrace();
            AvRoomDataManager.get().setLadyCharmImgUrl("");
        }
        //最高有重复也返-1
        if (isLadyMaxCharmRepeat || TextUtils.isEmpty(AvRoomDataManager.get().getLadyCharmImgUrl())) {
            AvRoomDataManager.get().setLadyMicTopCharmPosition(-1);
        }
        return micPosition;
    }


    public MicroViewAdapter(Context context) {
        this.context = context;
    }

    public void setOnMicroItemClickListener(OnMicroItemClickListener onMicroItemClickListener) {
        this.onMicroItemClickListener = onMicroItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item;
        if (viewType == TYPE_BOSS) {
            item = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.item_boss_micro, parent, false);
            return new BossMicroViewHolder(item);
        } else {
            item = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.list_item_micro, parent, false);
            return new NormalMicroViewHolder(item);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        LogUtils.d(TAG, "onBindViewHolder-position:" + position);
        RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoByMicPosition(position - 1);
        if (roomQueueInfo != null) {
            NormalMicroViewHolder holder = (NormalMicroViewHolder) viewHolder;
            holder.bind(roomQueueInfo, position - 1);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        if (layoutManager instanceof GridLayoutManager) {
            ((GridLayoutManager) layoutManager).setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return position == 0 ? 4 : 1;
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return 9;
    }

    private static final int TYPE_BOSS = 1;
    private static final int TYPE_NORMAL = 0;
    private static final int TYPE_INVALID = -2;

    @Override
    public int getItemViewType(int position) {
        return (position == 0) ? TYPE_BOSS : TYPE_NORMAL;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class NormalMicroViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvNick;
        TextView tvPosition;
        ImageView ivUpImage;
        ImageView ivLockImage;
        ImageView ivMuteImage;
        CircleImageView ivAvatar;
        FrameLayout rlMicLayout;
        MicroWaveView waveView;
        //        WaveView waveView;
        View avatarBg;
        CacheImageView ivHeadwear;
        TextView tvGlamour;
        LinearLayout llGlamour;
        ImageView ivCharmHeader;
        TextView tv_pairStatus;


        NormalMicroViewHolder(View itemView) {
            super(itemView);
            waveView = itemView.findViewById(R.id.waveview);
            rlMicLayout = itemView.findViewById(R.id.micro_layout);
            tvPosition = itemView.findViewById(R.id.tv_position);
            ivUpImage = itemView.findViewById(R.id.up_image);
            ivLockImage = itemView.findViewById(R.id.lock_image);
            ivMuteImage = itemView.findViewById(R.id.mute_image);
            ivAvatar = itemView.findViewById(R.id.avatar);
            tvNick = itemView.findViewById(R.id.nick);
            avatarBg = itemView.findViewById(R.id.avatar_bg);
            ivHeadwear = itemView.findViewById(R.id.iv_headwear);
            tvGlamour = itemView.findViewById(R.id.tv_glamour);
            llGlamour = itemView.findViewById(R.id.ll_mic_glamour);
            ivCharmHeader = itemView.findViewById(R.id.iv_charm_header);
            tv_pairStatus = itemView.findViewById(R.id.tv_pairStatus);

            ivUpImage.setOnClickListener(this);
            ivLockImage.setOnClickListener(this);
            ivAvatar.setOnClickListener(this);
        }

        RoomQueueInfo info;
        int position = TYPE_INVALID;

        public void clear() {
            info = null;
            position = TYPE_INVALID;
            rlMicLayout.setBackground(null);
            rlMicLayout.clearAnimation();
            waveView.stop();
            ivUpImage.setVisibility(View.VISIBLE);
            ivLockImage.setVisibility(View.GONE);
            ivMuteImage.setVisibility(View.GONE);
            ivAvatar.setVisibility(View.GONE);
            avatarBg.setVisibility(View.GONE);
            tvNick.setText("号麦位");
            tvPosition.setBackgroundResource(R.drawable.shape_circle_whire14);
//            ivHeadwear.setVisibility(View.GONE);
//            ivCharmHeader.setVisibility(View.GONE);
        }

        void bind(RoomQueueInfo info, int position) {
            this.info = info;
            int micPosition = position + 1;
            this.position = position;
            RoomMicInfo roomMicInfo = info.mRoomMicInfo;
            ChatRoomMember chatRoomMember = info.mChatRoomMember;

            // 清除动画
            waveView.stop();
            rlMicLayout.setBackground(null);
            rlMicLayout.clearAnimation();
            tvPosition.setText(String.valueOf(micPosition));
            if (roomMicInfo == null) {
                if (tv_pairStatus != null) {
                    tv_pairStatus.setText(null);
                    tv_pairStatus.setVisibility(View.INVISIBLE);
                }
                ivUpImage.setVisibility(View.VISIBLE);
                ivLockImage.setVisibility(View.GONE);
                ivMuteImage.setVisibility(View.GONE);
                ivAvatar.setVisibility(View.GONE);
                avatarBg.setVisibility(View.GONE);
                tvNick.setText("号麦位");
                ivHeadwear.setVisibility(View.GONE);
                ivCharmHeader.setVisibility(View.GONE);
                tvPosition.setBackgroundResource(R.drawable.shape_circle_whire14);
                return;
            }
            int medalId = 0;
            int medalDate = 0;
            boolean isInvisible = false;

            //显示，先展示人，无视麦的锁
            if (chatRoomMember != null) {
                //--------头饰-----------
                Map<String, Object> extension = chatRoomMember.getExtension();
                String headwearUrl = "";
                boolean hasVggPic = false;
                if (extension != null) {
                    if (extension.containsKey(Constants.headwearUrl)) {
                        headwearUrl = (String) extension.get(Constants.headwearUrl);
                    }
                    if (extension.containsKey(Constants.hasVggPic)) {
                        hasVggPic = (Boolean) extension.get(Constants.hasVggPic);
                    }
                    if (extension.containsKey(Constants.USER_MEDAL_ID)) {
                        medalId = (int) extension.get(Constants.USER_MEDAL_ID);
                    }
                    if (extension.containsKey(Constants.USER_MEDAL_DATE)) {
                        medalDate = (int) extension.get(Constants.USER_MEDAL_DATE);
                    }
                    if (extension.containsKey(Constants.NOBLE_INVISIABLE_ENTER_ROOM)) {
                        isInvisible = (int) extension.get(Constants.NOBLE_INVISIABLE_ENTER_ROOM) == 1;
                    }
                }
                waveView.setColor(NobleBusinessManager.getNobleMicWaveColor(medalId));
                //--------魅力值-----------
                int charm = 0;
                if (AvRoomDataManager.get().getRoomCharmList() != null) {
                    charm = AvRoomDataManager.get().getRoomCharmList().num(chatRoomMember.getAccount(), 0);
                }
                llGlamour.setVisibility(AvRoomDataManager.get().isShowCharm() && (charm > -1) ? View.VISIBLE : View.INVISIBLE);
                tvGlamour.setText(String.valueOf(charm));
                int micTopCharmPosition = info.gender == 1 ? AvRoomDataManager.get().getManMicTopCharmPosition()
                        : AvRoomDataManager.get().getLadyMicTopCharmPosition();
                LogUtils.d(TAG, "bind-micPosition:" + micPosition + " charm:" + charm
                        + " micTopCharmPosition:" + micTopCharmPosition);
                String charmImgUrl = info.gender == 1 ? AvRoomDataManager.get().getManCharmImgUrl()
                        : AvRoomDataManager.get().getLadyCharmImgUrl();
                if (micPosition == micTopCharmPosition && charm >= 30 &&
                        AvRoomDataManager.get().isShowCharm() && !TextUtils.isEmpty(charmImgUrl)) {
                    ivCharmHeader.setVisibility(View.VISIBLE);
                    LogUtils.d(TAG, "bind-micPosition:" + micPosition + " charmImgUrl:" + charmImgUrl);
//                    ImageLoadUtils.loadImage(context, charmImgUrl, ivCharmHeader);
                    GlideApp.with(context)
                            .load(charmImgUrl)
                            .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                            .listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object o,
                                                            Target<Drawable> target, boolean b) {
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable drawable, Object o,
                                                               Target<Drawable> target,
                                                               DataSource dataSource, boolean b) {
                                    FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) ivCharmHeader.getLayoutParams();
                                    params.width = DisplayUtil.dip2px(context, Math.round(drawable.getIntrinsicWidth() / 160.0f * 80.0f));
                                    params.height = DisplayUtil.dip2px(context, Math.round(drawable.getIntrinsicHeight() / 160.0f * 80.0f));
                                    ivCharmHeader.setLayoutParams(params);
                                    ivCharmHeader.setImageDrawable(drawable);
                                    return true;
                                }
                            })
                            .into(ivCharmHeader);
                } else {
                    ivCharmHeader.setVisibility(View.GONE);
                }

                ivLockImage.setVisibility(View.GONE);
                ivMuteImage.setVisibility(roomMicInfo.isMicMute() ? View.VISIBLE : View.GONE);

                if (!TextUtils.isEmpty(chatRoomMember.getAccount()) && JavaUtil.str2long(chatRoomMember.getAccount()) > 0) {
                    ivUpImage.setVisibility(View.GONE);
                    ivAvatar.setVisibility(View.VISIBLE);
                    avatarBg.setVisibility(View.VISIBLE);
                    tvNick.setVisibility(View.VISIBLE);
                    tvNick.setText(NobleBusinessManager.getNobleRoomNick(medalId, isInvisible, medalDate, chatRoomMember.getNick()));
                    tvNick.setTextColor(NobleBusinessManager.getNobleRoomNickColor(medalId, medalDate, isInvisible, Color.WHITE, chatRoomMember.getNick()));
                    tvPosition.setBackgroundResource(info.gender == 1 ? R.drawable.shape_circle_blue : R.drawable.shape_circle_red_f72732);
                    final boolean isBoss = 0 == micPosition;
                    LogUtils.d(TAG, "bind-headwearUrl:" + headwearUrl + " hasVggPic:" + hasVggPic);
                    if (!TextUtils.isEmpty(headwearUrl)) {
                        if (hasVggPic) {
//                            ImageLoadUtils.loadGifImage(context, headwearUrl, ivHeadwear);
                            GlideApp.with(context)
                                    .asGif()
                                    .load(headwearUrl)
                                    .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                                    .listener(new RequestListener<GifDrawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<GifDrawable> target, boolean isFirstResource) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(GifDrawable drawable, Object model, Target<GifDrawable> target, DataSource dataSource, boolean isFirstResource) {
                                            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) ivHeadwear.getLayoutParams();

                                            if (isBoss) {
                                                params.width = DisplayUtil.dip2px(context, Math.round(drawable.getIntrinsicWidth() / 160.0f * 60.0f));
                                                params.height = DisplayUtil.dip2px(context, Math.round(drawable.getIntrinsicHeight() / 160.0f * 60.0f));
                                            } else {
                                                params.width = DisplayUtil.dip2px(context, Math.round(drawable.getIntrinsicWidth() / 160.0f * 80.0f));
                                                params.height = DisplayUtil.dip2px(context, Math.round(drawable.getIntrinsicHeight() / 160.0f * 80.0f));
                                            }

                                            ivHeadwear.setLayoutParams(params);
                                            ivHeadwear.setImageDrawable(drawable);
                                            return true;
                                        }
                                    })
                                    .into(ivHeadwear);
                        } else {
//                            ImageLoadUtils.loadImage(context, headwearUrl, ivHeadwear);
                            GlideApp.with(context)
                                    .load(headwearUrl)
                                    .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object o,
                                                                    Target<Drawable> target, boolean b) {
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable drawable, Object o,
                                                                       Target<Drawable> target,
                                                                       DataSource dataSource, boolean b) {
                                            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) ivHeadwear.getLayoutParams();
                                            if (isBoss) {
                                                params.width = DisplayUtil.dip2px(context, Math.round(drawable.getIntrinsicWidth() / 160.0f * 60.0f));
                                                params.height = DisplayUtil.dip2px(context, Math.round(drawable.getIntrinsicHeight() / 160.0f * 60.0f));
                                            } else {
                                                params.width = DisplayUtil.dip2px(context, Math.round(drawable.getIntrinsicWidth() / 160.0f * 80.0f));
                                                params.height = DisplayUtil.dip2px(context, Math.round(drawable.getIntrinsicHeight() / 160.0f * 80.0f));
                                            }
                                            ivHeadwear.setLayoutParams(params);
                                            ivHeadwear.setImageDrawable(drawable);
                                            return true;
                                        }
                                    })
                                    .into(ivHeadwear);
                        }
                        ivHeadwear.setVisibility(View.VISIBLE);
                    } else {
                        ivHeadwear.setVisibility(View.GONE);
                    }
                    //--------头像-----------
                    GlideApp.with(context)
                            .load(NobleBusinessManager.getNobleRoomAvatarUrl(medalId, isInvisible, medalDate, chatRoomMember.getAvatar()))
                            .placeholder(R.drawable.ic_default_avatar)
                            .listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object o,
                                                            Target<Drawable> target, boolean b) {
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable drawable, Object o,
                                                               Target<Drawable> target,
                                                               DataSource dataSource, boolean b) {
                                    ivAvatar.setImageDrawable(drawable);
                                    return true;
                                }
                            })
                            .into(ivAvatar);

                    //------------相亲选择------------
                    if (null != tv_pairStatus && null != AvRoomDataManager.get().mCurrentRoomInfo
                            && AvRoomDataManager.get().mCurrentRoomInfo.tagId == 6) {
                        if (AvRoomDataManager.get().pairSelectMap.containsKey(chatRoomMember.getAccount())
                                && AvRoomDataManager.get().isShowPairSelectStatus()) {
                            tv_pairStatus.setVisibility(View.VISIBLE);
                            tv_pairStatus.setText(String.valueOf(AvRoomDataManager.get().pairSelectMap.get(chatRoomMember.getAccount()) + 1));
                        } else {
                            tv_pairStatus.setText(null);
                            tv_pairStatus.setVisibility(View.INVISIBLE);
                        }
                    }
                } else {
                    ivUpImage.setVisibility(View.VISIBLE);
                    ivAvatar.setVisibility(View.GONE);
                    avatarBg.setVisibility(View.GONE);
                    tvNick.setText("号麦位");
                    tvNick.setTextColor(Color.WHITE);
                    tvPosition.setBackgroundResource(R.drawable.shape_circle_whire14);
                    waveView.setColor(NobleBusinessManager.getNobleMicWaveColor(medalId));
                }
            } else {
                if (tv_pairStatus != null) {
                    tv_pairStatus.setText(null);
                    tv_pairStatus.setVisibility(View.INVISIBLE);
                }
                ivHeadwear.setVisibility(View.GONE);
                ivCharmHeader.setVisibility(View.GONE);
                llGlamour.setVisibility(View.INVISIBLE);
                //锁麦
                if (roomMicInfo.isMicLock()) {
                    ivUpImage.setVisibility(View.GONE);
                    ivMuteImage.setVisibility(roomMicInfo.isMicMute() ? View.VISIBLE : View.GONE);
                    ivLockImage.setVisibility(View.VISIBLE);
                    ivAvatar.setVisibility(View.GONE);
                    avatarBg.setVisibility(View.GONE);
                } else {
                    ivMuteImage.setVisibility(roomMicInfo.isMicMute() ? View.VISIBLE : View.GONE);
                    ivUpImage.setVisibility(View.VISIBLE);
                    ivAvatar.setVisibility(View.GONE);
                    avatarBg.setVisibility(View.GONE);
                    ivLockImage.setVisibility(View.GONE);
                }
                tvNick.setText("号麦位");
                tvNick.setTextColor(Color.WHITE);
                tvPosition.setBackgroundResource(R.drawable.shape_circle_whire14);
                waveView.setColor(NobleBusinessManager.getNobleMicWaveColor(medalId));
            }
        }

        @Override
        public void onClick(View v) {
            if (info == null || position == TYPE_INVALID || onMicroItemClickListener == null) {
                return;
            }
            if (v.getId() == R.id.up_image || v.getId() == R.id.lock_image) {
                if (position != -1) {
                    onMicroItemClickListener.onUpMicBtnClick(position, info.mChatRoomMember);
                }
            } else if (v.getId() == R.id.lock_image) {
                if (position != -1) {
                    onMicroItemClickListener.onLockBtnClick(position);
                }
            } else if (v.getId() == R.id.avatar) {
                onMicroItemClickListener.onAvatarBtnClick(position);
            } else if (v.getId() == R.id.ll_room_desc) {
                onMicroItemClickListener.onRoomSettingsClick();
            }
        }
    }

    private int getCharmImageIndex(int micTopCharm) {
        int index = 0;
        if (micTopCharm >= 9999) {
            index = 3;
        } else if (micTopCharm >= 5200) {
            index = 2;
        } else if (micTopCharm >= 1000) {
            index = 1;
        } else if (micTopCharm >= 30) {
            index = 0;
        }
        return index;
    }

    public class BossMicroViewHolder extends NormalMicroViewHolder {
        /**
         * 主席位特有
         */
        ImageView tvState;
        View ll_room_desc;

        BossMicroViewHolder(View itemView) {
            super(itemView);
            tvState = itemView.findViewById(R.id.tv_state);
            ll_room_desc = itemView.findViewById(R.id.ll_room_desc);
            ll_room_desc.setOnClickListener(this);
        }

        @Override
        void bind(RoomQueueInfo info, int position) {
            super.bind(info, position);
            try {
                RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoByMicPosition(-1);
                if (roomQueueInfo.mChatRoomMember != null) {
                    tvState.setVisibility(View.GONE);
                } else {
                    tvState.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                tvState.setVisibility(View.VISIBLE);
            }

            tvNick.setVisibility(View.INVISIBLE);
            ivLockImage.setVisibility(View.GONE);
            ivMuteImage.setVisibility(View.GONE);
            ivUpImage.setVisibility(View.GONE);
            ivAvatar.setVisibility(View.VISIBLE);
            avatarBg.setVisibility(View.VISIBLE);
            if (StringUtil.isEmpty(avatarPicture)) {
                UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(AvRoomDataManager.get().mCurrentRoomInfo.getUid());
                if (userInfo != null && userInfo.getAvatar() != null) {
                    if (!userInfo.getAvatar().equals(avatarPicture)) {
                        avatarPicture = userInfo.getAvatar();
                        ImageLoadUtils.loadAvatar(BasicConfig.INSTANCE.getAppContext(), avatarPicture, ivAvatar);
                    }
                }
            } else {
                ImageLoadUtils.loadAvatar(BasicConfig.INSTANCE.getAppContext(), avatarPicture, ivAvatar);
            }
        }

        @Override
        public void clear() {
            super.clear();
        }

        @Override
        public void onClick(View v) {
            super.onClick(v);
        }
    }

    public void clear(NormalMicroViewHolder holder) {
        holder.clear();
    }

    public interface OnMicroItemClickListener {
        void onAvatarBtnClick(int position);

        void onUpMicBtnClick(int position, ChatRoomMember chatRoomMember);

        void onLockBtnClick(int position);

        void onRoomSettingsClick();
    }
}
