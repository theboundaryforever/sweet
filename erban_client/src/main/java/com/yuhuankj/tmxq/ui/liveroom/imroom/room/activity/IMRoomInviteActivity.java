package com.yuhuankj.tmxq.ui.liveroom.imroom.room.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMember;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.base.IMRoomConstant;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.fragment.IMRoomOnlineMemberFragment;

import static com.yuhuankj.tmxq.ui.liveroom.imroom.room.dialog.IMRoomMemberListDialog.KEY_IS_ADMIN;
import static com.yuhuankj.tmxq.ui.liveroom.imroom.room.dialog.IMRoomMemberListDialog.KEY_IS_INVITE_UP_TO_AUDIO_CONN_MIC;

/**
 *  邀请非指定用户上麦页面
 *
 * @author jiahui
 * @date 2017/12/21
 */
public class IMRoomInviteActivity extends BaseActivity implements IMRoomOnlineMemberFragment.OnlineMemberItemClick {
    private int micPosition;
    private boolean isAdmin = false;

    public static void openActivity(FragmentActivity fragmentActivity, int micPosition) {
        Intent intent = new Intent(fragmentActivity, IMRoomInviteActivity.class);
        intent.putExtra(Constants.KEY_POSITION, micPosition);
        fragmentActivity.startActivityForResult(intent, IMRoomConstant.REQUEST_CODE);
    }

    public static void openActivity(FragmentActivity fragmentActivity, int micPosition,boolean isAdmin) {
        Intent intent = new Intent(fragmentActivity, IMRoomInviteActivity.class);
        intent.putExtra(Constants.KEY_POSITION, micPosition);
        intent.putExtra("isAdmin",isAdmin);
        fragmentActivity.startActivityForResult(intent, IMRoomConstant.REQUEST_CODE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_im_room_invite);
        findViewById(R.id.toolbar).setOnClickListener(v -> finish());
        Intent intent = getIntent();
        if (intent != null) {
            micPosition = intent.getIntExtra(Constants.KEY_POSITION, Integer.MIN_VALUE);
            isAdmin = intent.getBooleanExtra(KEY_IS_ADMIN,false);
        }else {
            micPosition = Integer.MIN_VALUE;
            isAdmin = false;
        }
        IMRoomOnlineMemberFragment fragment = new IMRoomOnlineMemberFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(KEY_IS_INVITE_UP_TO_AUDIO_CONN_MIC, true);
        bundle.putBoolean(KEY_IS_ADMIN,isAdmin);
        fragment.setArguments(bundle);
        fragment.setOnlineMemberItemClick(this);
        getSupportFragmentManager().beginTransaction().add(R.id.fl_im_online_user,fragment).commitAllowingStateLoss();
    }

    @Override
    public void onItemClick(IMRoomMember chatRoomMember) {
        Intent intent = new Intent();
        intent.putExtra("account", chatRoomMember.getLongAccount());
        intent.putExtra(Constants.KEY_POSITION, micPosition);
        setResult(IMRoomConstant.ROOM_INVITE_RESULT_CODE, intent);
        finish();
    }
}
