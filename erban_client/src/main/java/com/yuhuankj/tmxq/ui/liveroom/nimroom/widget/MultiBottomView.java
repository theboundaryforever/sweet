package com.yuhuankj.tmxq.ui.liveroom.nimroom.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;


/**
 * 2019年4月28日 00:49:54
 * 针对轰趴房
 * 1.房主，依次显示，发言、禁麦、声音、表情、私聊以及礼物按钮
 * 2.非房主，
 * 没有上麦，发言、禁麦、声音、以及礼物按钮
 * 上麦则，发言、禁麦、声音、表情、私聊以及礼物按钮
 * 分享房间按钮始终都是隐藏不可见的
 * <p>
 * 针对个人直播房
 * 1.房主，依次显示，发言、禁麦、声音、分享以及礼物按钮
 * 2.非房主，依次显示，说点什么，声音、分享以及礼物按钮
 * 表情、私聊（因没有上麦操作）、禁麦始终都是不可见的
 * <p>
 * xml默认设置为不可见的控件为，表情、私聊、分享
 * <p>
 * UI上，轰趴房父布局左边距为10dp，个人音频房左边距为15dp
 * 轰趴房btn之间的间距为10dp，个人音频房为12dp
 *
 * @author weihaitao
 * @date 2019年4月28日 00:41:09
 */

public class MultiBottomView extends AbstractBottomView implements View.OnClickListener {

    private final String TAG = MultiBottomView.class.getSimpleName();

    public MultiBottomView(Context context) {
        super(context);
    }

    public MultiBottomView(Context context, AttributeSet attr) {
        super(context, attr);
    }

    public MultiBottomView(Context context, AttributeSet attr, int i) {
        super(context, attr, i);
    }

    @Override
    protected void resetLayoutParams(ImageView iv_sendMsg, ImageView openMic, ImageView remoteMute) {
        if (null == iv_sendMsg || null == openMic || null == remoteMute) {
            return;
        }
        LayoutParams lp1 = (LayoutParams) iv_sendMsg.getLayoutParams();
        LayoutParams lp2 = (LayoutParams) openMic.getLayoutParams();
        LayoutParams lp3 = (LayoutParams) remoteMute.getLayoutParams();
        int leftMargin = DisplayUtility.dp2px(getContext(), 10);
        lp1.leftMargin = leftMargin;
        lp2.leftMargin = leftMargin;
        lp3.leftMargin = leftMargin;
        iv_sendMsg.setLayoutParams(lp1);
        openMic.setLayoutParams(lp2);
        remoteMute.setLayoutParams(lp3);
    }

    @Override
    public void updateBottomView(boolean isOnMicMyself) {
        if (getVisibility() == View.INVISIBLE) {
            setVisibility(View.VISIBLE);
        }
        if (isOnMicMyself) {
            showMultiUpMicBottom();
        } else {
            showMultiyDownMicBottom();
        }
        RoomInfo mCurrentRoomInfo = BaseRoomServiceScheduler.getCurrentRoomInfo();
        boolean inputMsgBtnEnable = null != mCurrentRoomInfo && mCurrentRoomInfo.getPublicChatSwitch() == 0;
        setInputMsgBtnEnable(inputMsgBtnEnable);
        //1.0.2.0临时注释
        if (RoomDataManager.get().getAdditional() != null) {
            //1.0.1.1版本，单人音频房，更多操作按钮暂时限于大转盘抽奖，因此如果此开关时关闭的，[+]就没必要显示了
            iconMoreOpera.setVisibility(View.VISIBLE);
        } else {
            iconMoreOpera.setVisibility(View.GONE);
        }
    }


}
