package com.yuhuankj.tmxq.ui.ranklist;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liaoxy
 * @Description:排行榜
 * @date 2019/3/5 15:29
 */
public class RankListActivity extends BaseActivity {
    private ViewPager vpContent;
    private View vBack;

    private LinearLayout llCharm;
    private TextView tvCharm;
    private View vCharm;

    private LinearLayout llFortune;
    private TextView tvFortune;
    private View vFortune;
    private RelativeLayout rlTopTitle;

    private TabPageAdapter tabPageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranklist);
        initView();
        initData();
    }

    private void initView() {
        rlTopTitle = (RelativeLayout) findViewById(R.id.rlTopTitle);
        vpContent = (ViewPager) findViewById(R.id.vpContent);
        vBack = findViewById(R.id.vBack);

        llCharm = (LinearLayout) findViewById(R.id.llCharm);
        tvCharm = (TextView) findViewById(R.id.tvCharm);
        vCharm = findViewById(R.id.vCharm);

        llFortune = (LinearLayout) findViewById(R.id.llFortune);
        tvFortune = (TextView) findViewById(R.id.tvFortune);
        vFortune = findViewById(R.id.vFortune);

        vBack.setOnClickListener(this);
        llCharm.setOnClickListener(this);
        llFortune.setOnClickListener(this);
    }

    private void initData() {
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(RankListFragment.newInstance(2));
        fragments.add(RankListFragment.newInstance(1));
        tabPageAdapter = new TabPageAdapter(getSupportFragmentManager(), fragments);
        vpContent.setAdapter(tabPageAdapter);

        vpContent.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    selectFortune();
                } else {
                    selectCharm();
                }
                try {
                    RankListFragment rankListFragment = (RankListFragment) fragments.get(position);
                    rankListFragment.setOffsetY();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        selectFortune();
        vpContent.setCurrentItem(0);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (view == vBack) {
            finish();
        } else if (view == llCharm) {
            selectCharm();
            vpContent.setCurrentItem(1);
        } else if (view == llFortune) {
            selectFortune();
            vpContent.setCurrentItem(0);
        }
    }

    private void selectCharm() {
        tvCharm.setTextColor(Color.argb(255, 255, 255, 255));
        tvFortune.setTextColor(Color.argb(150, 255, 255, 255));

        vCharm.setVisibility(View.VISIBLE);
        vFortune.setVisibility(View.INVISIBLE);
    }

    private void selectFortune() {
        tvCharm.setTextColor(Color.argb(150, 255, 255, 255));
        tvFortune.setTextColor(Color.argb(255, 255, 255, 255));

        vCharm.setVisibility(View.INVISIBLE);
        vFortune.setVisibility(View.VISIBLE);
    }

    private RelativeLayout.LayoutParams layoutParams;

    public int setTabOffsetY(int y, boolean isScroing) {
        if (layoutParams == null) {
            layoutParams = (RelativeLayout.LayoutParams) rlTopTitle.getLayoutParams();
            rlTopTitle.setTag(R.id.tv_position + 1, layoutParams.topMargin);
        }
        int oriTopMargin = (int) rlTopTitle.getTag(R.id.tv_position + 1);
        if (isScroing) {
            layoutParams.topMargin -= y;
        } else {
            if (y == 0) {
                layoutParams.topMargin = oriTopMargin;
            } else {
                layoutParams.topMargin = y;
            }
        }
        rlTopTitle.requestLayout();
        return layoutParams.topMargin;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (vpContent != null){
            vpContent.clearOnPageChangeListeners();
        }
    }
}
