package com.yuhuankj.tmxq.ui.home.game;

import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSONArray;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.ethanhua.skeleton.Skeleton;
import com.ethanhua.skeleton.SkeletonScreen;
import com.jude.rollviewpager.hintview.ColorPointHintView;
import com.netease.nim.uikit.common.util.sys.NetworkUtil;
import com.tencent.bugly.crashreport.BuglyLog;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.ButtonUtils;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.AccountInfo;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.WebViewStyle;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.GameRoomEnitity;
import com.tongdaxing.xchat_core.minigame.MiniGameModel;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.fragment.BaseLazyFragment;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.thirdsdk.nim.MsgCenterRedPointStatusManager;
import com.yuhuankj.tmxq.ui.launch.guide.NoviceGuideDialog;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.me.taskcenter.view.TaskCenterActivity;
import com.yuhuankj.tmxq.ui.message.UnreadMsgResult;
import com.yuhuankj.tmxq.ui.nim.game.GameEnitity;
import com.yuhuankj.tmxq.ui.nim.game.InvitationActivity;
import com.yuhuankj.tmxq.ui.ranklist.RankListActivity;
import com.yuhuankj.tmxq.ui.recommad.IRecommAdCoreClient;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;
import com.yuhuankj.tmxq.ui.webview.VoiceAuthCardWebViewActivity;
import com.yuhuankj.tmxq.widget.Banner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author liaoxy
 * @Description:游戏大厅页
 * @date 2019/2/14 11:27
 */
public class GameHomeFragment extends BaseLazyFragment implements MsgCenterRedPointStatusManager.OnRedPointStatusChangedListener {
    public static final String TAG = "GameHomeFragment";
    private Banner bnContent;
    private GameHomeModel model;
    private RecyclerView rcvPlayTogether;
    private RecyclerView rcvIcon;
    private RecyclerView rcvLiveRoom;
    private TextView tvAllGame;

    private GameHomeLiveRoomAdapter roomAdapter;
    private GameHomeIconAdapter iconAdapter;
    private GameHomeBannerAdapter bannerAdapter;
    private PlayTogetherAdapter playTogetherAdapter;

    private boolean isReuqestingTopList = false;
    private boolean isReuqestingIconList = false;
    private boolean isReuqestingBannerList = false;
    private boolean isReuqestingPlayTogetherList = false;

    private boolean isLazyLoadRequestint = false;
    private SkeletonScreen skeletonScreen;//加载状态view
    private SwipeRefreshLayout swipeRefreshLayout;
    private SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            if (NetworkUtil.isNetAvailable(mContext)) {
                onReloadData();
            } else {
                swipeRefreshLayout.setRefreshing(false);
            }
        }
    };
    /**
     * 是否已处于登录状态
     */
    private boolean hasLogined = false;

    //每隔一段时间获取最新的房间显示
    private final static int MSG_CODE_UPATE_ROOM = 1;
    //每隔多久更新一次房间信息
    private final static int UPDATE_TIME = 10000;
    private Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == MSG_CODE_UPATE_ROOM) {
                getNewestRoom();
            }
        }
    };

    @Override
    public void onReloadData() {
        initGameHomeData();
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_game_home;
    }

    private NoviceGuideDialog gameHomeGuideDialog;

    @Override
    public void onSetListener() {
        tvAllGame.setOnClickListener(this);
        MsgCenterRedPointStatusManager.getInstance().addListener(this);
    }

    @Override
    public void initiate() {
        GridLayoutManager manager = new GridLayoutManager(getContext(), 3);
        rcvPlayTogether.setLayoutManager(manager);
        GridLayoutManager managerIcon = new GridLayoutManager(getContext(), 4);
        rcvIcon.setLayoutManager(managerIcon);

        LinearLayoutManager managerLiveRoom = new RecyclerViewNoBugLinearLayoutManager(getContext());
        managerLiveRoom.setOrientation(LinearLayoutManager.VERTICAL);
        rcvLiveRoom.setLayoutManager(managerLiveRoom);
    }

    @Override
    public void onFindViews() {
        swipeRefreshLayout = mView.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
        rcvLiveRoom = mView.findViewById(R.id.rcvLiveRoom);

        View vHeader = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_game_home_head, null);
        bnContent = vHeader.findViewById(R.id.bnContent);
        int screenWidth = DisplayUtility.getScreenWidth(getActivity());
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) bnContent.getLayoutParams();
        lp.width = screenWidth;
        lp.height = screenWidth * 356 / 1000;
        lp.gravity = Gravity.CENTER_HORIZONTAL;
        bnContent.setLayoutParams(lp);

        bannerAdapter = new GameHomeBannerAdapter(null, getActivity());
        bannerAdapter.setOnGameBannerClickListener(new GameHomeBannerAdapter.OnGameBannerClickListener() {
            @Override
            public void onGameBannerClicked(GameBannerEnitity bannerEnitity) {
                if (bannerEnitity.getSkipType() == 3) {
                    if (bannerEnitity.getSkipUri().endsWith(UriProvider.getRecordAuthCardVoiceBannerUrl())) {
                        VoiceAuthCardWebViewActivity.start(getActivity(), false);
                    } else {
                        CommonWebViewActivity.start(getActivity(), bannerEnitity.getSkipUri());
                    }
                } else if (bannerEnitity.getSkipType() == 2) {
                    try {
                        RoomServiceScheduler.getInstance().enterRoom(getActivity(),
                                Long.valueOf(bannerEnitity.getSkipUri()), bannerEnitity.getRoomType());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        bnContent.setAdapter(bannerAdapter);

        tvAllGame = vHeader.findViewById(R.id.tvAllGame);
        rcvPlayTogether = vHeader.findViewById(R.id.rcvPlayTogether);
        playTogetherAdapter = new PlayTogetherAdapter(null);
        playTogetherAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                if (ButtonUtils.isFastDoubleClick(view.getId())) {
                    return;
                }
                if (playTogetherAdapter.getData().size() > position) {
                    GameEnitity gameEnitity = playTogetherAdapter.getData().get(position);
                    if (CommonWebViewActivity.isGameRunning) {
                        LogUtils.e(TAG, "game is running");
                        return;
                    }
                    String url = gameEnitity.getUrl();
                    if (TextUtils.isEmpty(url)) {
                        SingleToastUtil.showToast("游戏链接无效");
                        return;
                    }
                    CommonWebViewActivity.isGameRunning = true;
                    CommonWebViewActivity.start(getActivity(), url + "#/loading", WebViewStyle.NO_TITLE);
                }
            }
        });
        rcvPlayTogether.setAdapter(playTogetherAdapter);

        rcvIcon = vHeader.findViewById(R.id.rcvIcon);
        iconAdapter = new GameHomeIconAdapter(null);
        iconAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                if (iconAdapter.getData().size() > position) {
                    GameIconEnitity gameIconEnitity = iconAdapter.getData().get(position);
                    if (gameIconEnitity != null && gameIconEnitity.getTitle().equals(
                            getContext().getResources().getString(R.string.main_icon_rank))) {
                        startActivity(new Intent(getContext(), RankListActivity.class));
                    } else if (gameIconEnitity != null && gameIconEnitity.getTitle().equals(
                            getContext().getResources().getString(R.string.main_icon_sign_on))) {
                        startActivity(new Intent(getContext(), TaskCenterActivity.class));
                        //任务统计-去签到
                        try {
                            Map<String, String> maps = StatisticModel.getInstance().getUMAnalyCommonMap(getContext());
                            StatisticManager.get().onEvent(getContext(), StatisticModel.EVENT_ID_TASK_TO_SIGNIN, maps);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (gameIconEnitity != null) {
                        if (gameIconEnitity.getTitle().equals(getContext().getResources().getString(R.string.main_icon_luck_draw))) {
                            //任务统计-去抽奖
                            try {
                                Map<String, String> maps = StatisticModel.getInstance().getUMAnalyCommonMap(getContext());
                                StatisticManager.get().onEvent(getContext(), StatisticModel.EVENT_ID_TASK_CHOUJIANG_HOME, maps);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        autoJump(gameIconEnitity.getActivity(), gameIconEnitity.getUrl(), gameIconEnitity.getTitle(), Json.parse(gameIconEnitity.getParams()));
                    }
                }
            }
        });
        rcvIcon.setAdapter(iconAdapter);

        //设置动画
        DefaultItemAnimator defaultItemAnimator = new DefaultItemAnimator();
        defaultItemAnimator.setAddDuration(2000);
        defaultItemAnimator.setRemoveDuration(100);
        rcvLiveRoom.setItemAnimator(defaultItemAnimator);

        roomAdapter = new GameHomeLiveRoomAdapter(new ArrayList<>());
        rcvLiveRoom.setAdapter(roomAdapter);
        roomAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                if (roomAdapter.getData().size() > position) {
                    GameRoomEnitity gameRoomEnitity = roomAdapter.getData().get(position);
                    RoomServiceScheduler.getInstance().enterRoom(getActivity(),
                            gameRoomEnitity.getUid(), gameRoomEnitity.getType());
                }
            }
        });
        roomAdapter.addHeaderView(vHeader);
        rcvLiveRoom.setFocusable(false);
        rcvLiveRoom.setFocusableInTouchMode(false);

        swipeRefreshLayout.setEnabled(false);
        skeletonScreen = Skeleton.bind(rcvLiveRoom)
                .adapter(roomAdapter)
                .load(R.layout.fragment_game_home_head_loading)
                .show();
    }

    //隐藏Skeleton
    private void hideSkeleton() {
        try {
            if (!isHideLoad) {
                skeletonScreen.hide();
                swipeRefreshLayout.setEnabled(true);
            }
            isHideLoad = true;
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        if (tvAllGame == view) {
            startActivity(new Intent(getContext(), InvitationActivity.class));
        }
    }

    private void setCacheData() {
        String bannerJsonStr = (String) SpUtils.get(getContext(), TAG + "_bannerJsonStr", "");
        String iconJsonStr = (String) SpUtils.get(getContext(), TAG + "_iconJsonStr", "");
        String gameJsonStr = (String) SpUtils.get(getContext(), TAG + "_gameJsonStr", "");

        if (!TextUtils.isEmpty(bannerJsonStr) && bannerAdapter.getBannerInfoList() != null && bannerAdapter.getBannerInfoList().size() == 0) {
            List<GameBannerEnitity> banners = JSONArray.parseArray(bannerJsonStr, GameBannerEnitity.class);
            setBannerData(banners);
            hideSkeleton();
        }
        if (!TextUtils.isEmpty(iconJsonStr) && iconAdapter.getData().size() == 0) {
            List<GameIconEnitity> icons = JSONArray.parseArray(iconJsonStr, GameIconEnitity.class);
            iconAdapter.setNewData(icons);
            hideSkeleton();
        }
        if (!TextUtils.isEmpty(gameJsonStr) && playTogetherAdapter.getData().size() == 0) {
            List<GameEnitity> games = JSONArray.parseArray(gameJsonStr, GameEnitity.class);
            playTogetherAdapter.setNewData(games);
            hideSkeleton();
        }
    }

    private void initGameHomeData() {
        if (!CoreManager.getCore(IAuthCore.class).isLogin()) {
            return;
        }
        isReuqestingBannerList = true;
        isReuqestingIconList = true;
        isReuqestingPlayTogetherList = true;
        isReuqestingTopList = true;

        model = new GameHomeModel();
        model.gameBanner(new OkHttpManager.MyCallBack<ServiceResult<List<GameBannerEnitity>>>() {
            @Override
            public void onError(Exception e) {
                isReuqestingBannerList = false;
                SingleToastUtil.showToast(e.getMessage());
                stopRefresh();
            }

            @Override
            public void onResponse(ServiceResult<List<GameBannerEnitity>> response) {
                isReuqestingBannerList = false;
                if (response.isSuccess()) {
                    if (response.getData() != null && response.getData().size() > 0) {
                        setBannerData(response.getData());
                        String bannerJsonStr = JSONArray.toJSONString(response.getData());
                        if (!TextUtils.isEmpty(bannerJsonStr)) {
                            SpUtils.put(getContext(), TAG + "_bannerJsonStr", bannerJsonStr);
                        }
                    }
                } else {
                    SingleToastUtil.showToast(response.getMessage());
                }
                stopRefresh();
            }
        });
        model.gameIcons(new OkHttpManager.MyCallBack<ServiceResult<List<GameIconEnitity>>>() {
            @Override
            public void onError(Exception e) {
                isReuqestingIconList = false;
                SingleToastUtil.showToast(e.getMessage());
                stopRefresh();
            }

            @Override
            public void onResponse(ServiceResult<List<GameIconEnitity>> response) {
                isReuqestingIconList = false;
                if (response.isSuccess()) {
                    if (response.getData() != null && response.getData().size() > 0) {
                        BuglyLog.d(TAG, "refreshGameIcons");
                        iconAdapter.setNewData(response.getData());
                        String iconJsonStr = JSONArray.toJSONString(response.getData());
                        if (!TextUtils.isEmpty(iconJsonStr)) {
                            SpUtils.put(getContext(), TAG + "_iconJsonStr", iconJsonStr);
                        }
                    }
                } else {
                    SingleToastUtil.showToast(response.getMessage());
                }
                stopRefresh();
            }
        });

        new MiniGameModel().getGames(new OkHttpManager.MyCallBack<ServiceResult<List<GameEnitity>>>() {
            @Override
            public void onError(Exception e) {
                isReuqestingPlayTogetherList = false;
                SingleToastUtil.showToast(e.getMessage());
                stopRefresh();
            }

            @Override
            public void onResponse(ServiceResult<List<GameEnitity>> response) {
                isReuqestingPlayTogetherList = false;
                if (response.isSuccess()) {
                    if (response.getData() != null && response.getData().size() > 0) {
                        BuglyLog.d(TAG, "refreshPlayTogetherList");
                        List<GameEnitity> games = response.getData();
                        if (games != null && games.size() > 3) {
                            games = games.subList(0, 3);
                        }
                        playTogetherAdapter.setNewData(games);
                        String gameJsonStr = JSONArray.toJSONString(games);
                        if (!TextUtils.isEmpty(gameJsonStr)) {
                            SpUtils.put(getContext(), TAG + "_gameJsonStr", gameJsonStr);
                        }
                    }
                } else {
                    SingleToastUtil.showToast(response.getMessage());
                }
                stopRefresh();
            }
        });

        handler.removeMessages(MSG_CODE_UPATE_ROOM);
        model.gameTops(20, new OkHttpManager.MyCallBack<ServiceResult<List<GameRoomEnitity>>>() {
            @Override
            public void onError(Exception e) {
                hideSkeleton();
                isReuqestingTopList = false;
                //SingleToastUtil.showToast(e.getMessage());
                stopRefresh();

                handler.removeMessages(MSG_CODE_UPATE_ROOM);
                handler.sendEmptyMessageDelayed(MSG_CODE_UPATE_ROOM, 10000);
            }

            @Override
            public void onResponse(ServiceResult<List<GameRoomEnitity>> response) {
                hideSkeleton();
                isReuqestingTopList = false;
                if (response.isSuccess()) {
                    if (response.getData() != null && response.getData().size() > 0) {
                        BuglyLog.d(TAG, "refreshGameTops");
                        roomAdapter.setNewData(response.getData());
                    }
                } else {
                    //SingleToastUtil.showToast(response.getMessage());
                }
                stopRefresh();

                handler.removeMessages(MSG_CODE_UPATE_ROOM);
                handler.sendEmptyMessageDelayed(MSG_CODE_UPATE_ROOM, 10000);
            }
        });

        model.getUnreadMsgCount(new OkHttpManager.MyCallBack<ServiceResult<UnreadMsgResult>>() {

            @Override
            public void onError(Exception e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(ServiceResult<UnreadMsgResult> response) {
                if (response != null && response.isSuccess() && null != response.getData()) {
                    UnreadMsgResult unreadMsgResult = response.getData();
                    MsgCenterRedPointStatusManager.getInstance().setShowSignInRedPoint(unreadMsgResult.getSignCount() > 0);
                }
            }
        });
    }

    private void setBannerData(List<GameBannerEnitity> list) {
        BuglyLog.d(TAG, "setBannerData");
        bannerAdapter.setBannerInfoList(list);
        bannerAdapter.notifyDataSetChanged();
        if (list.size() > 1) {
            bnContent.setHintView(new ColorPointHintView(getContext(), Color.parseColor("#E3AC42"), Color.parseColor("#88ffffff")));
            bnContent.setPlayDelay(3000);
            bnContent.setAnimationDurtion(500);
        } else {
            bnContent.setHintView(new ColorPointHintView(getContext(), Color.TRANSPARENT, Color.TRANSPARENT));
        }
    }

    //获取最新的一条房间信息
    private void getNewestRoom() {
        if (!CoreManager.getCore(IAuthCore.class).isLogin()) {
            return;
        }
        if (model == null) {
            model = new GameHomeModel();
        }
        model.gameTops(1, new OkHttpManager.MyCallBack<ServiceResult<List<GameRoomEnitity>>>() {
            @Override
            public void onError(Exception e) {
                //SingleToastUtil.showToast(e.getMessage());
                handler.removeMessages(MSG_CODE_UPATE_ROOM);
                handler.sendEmptyMessageDelayed(MSG_CODE_UPATE_ROOM, UPDATE_TIME);
            }

            @Override
            public void onResponse(ServiceResult<List<GameRoomEnitity>> response) {
                if (response.isSuccess()) {
                    if (response.getData() != null && response.getData().size() > 0 && !isExistData(response.getData().get(0))) {
                        BuglyLog.d(TAG, "addNewestRoom");
                        roomAdapter.addData(0, response.getData());
                        if (roomAdapter.getData().size() > 20) {
                            roomAdapter.remove(roomAdapter.getData().size() - 1);
                        }
                    }
                }
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        roomAdapter.notifyDataSetChanged();
                    }
                }, 4000);
                handler.removeMessages(MSG_CODE_UPATE_ROOM);
                handler.sendEmptyMessageDelayed(MSG_CODE_UPATE_ROOM, UPDATE_TIME);
            }
        });
    }

    //是否已经存在此条房间数据
    private boolean isExistData(GameRoomEnitity data) {
        if (data == null || roomAdapter == null) {
            return false;
        }
        int index = roomAdapter.getData().indexOf(data);
        if (index == 0) {
            return true;
        } else if (index > 0) {
            roomAdapter.remove(index);
        }
        return false;
    }

    /**
     * 标识推荐广告弹框是否显示
     */
    private boolean isRecommAdShow = true;
    private boolean isHidden = false;
    private boolean isHideLoad = false;//是否已经隐藏了加载布局

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isHidden = !isVisibleToUser;
        if (handler == null) {
            return;
        }
        if (!isVisibleToUser) {
            handler.removeMessages(MSG_CODE_UPATE_ROOM);
        } else {
            if (!isHideLoad) {
                //20秒后隐藏，防止有意外情况导致没有隐藏加载布局，页面无法重新加载数据情况
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        hideSkeleton();
                    }
                }, 20000);
            }
            handler.removeMessages(MSG_CODE_UPATE_ROOM);
            handler.sendEmptyMessageDelayed(MSG_CODE_UPATE_ROOM, UPDATE_TIME);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
        }
    }

    /**
     * 停止刷新,并在首次懒加载刷新结束时，判断是否需要展示新手引导页
     */
    private void stopRefresh() {
        if (!isReuqestingTopList && !isReuqestingPlayTogetherList && !isReuqestingIconList && !isReuqestingBannerList) {
            swipeRefreshLayout.setRefreshing(false);
            if (isLazyLoadRequestint) {
                isLazyLoadRequestint = false;
                //懒加载，数据全部请求返回完毕，等界面刷新完毕
                rcvLiveRoom.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
//                        rcvLiveRoom.postDelayed(() -> showGameHomeGuideDialog(), 500);
                        rcvLiveRoom.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
            }
            getDialogManager().dismissDialog();
        }
    }

    private Runnable showGameGuideRunnable = new Runnable() {
        @Override
        public void run() {
            showGameHomeGuideDialog();
        }
    };

    @Override
    public void onStop() {
        super.onStop();
        if (handler == null) {
            return;
        }
        handler.removeMessages(MSG_CODE_UPATE_ROOM);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (handler == null) {
            return;
        }
        if (!isHidden) {
            if ((bannerAdapter.getBannerInfoList() == null || bannerAdapter.getBannerInfoList().size() == 0) || roomAdapter.getData().size() == 0) {
                isLazyLoadRequestint = true;
                setCacheData();
                initGameHomeData();
            }
            handler.removeMessages(MSG_CODE_UPATE_ROOM);
            handler.sendEmptyMessageDelayed(MSG_CODE_UPATE_ROOM, UPDATE_TIME);
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        LogUtils.d(TAG, "onHiddenChanged-hidden:" + hidden);
        if (handler == null) {
            return;
        }
        isHidden = hidden;
        if (hidden) {
            handler.removeMessages(MSG_CODE_UPATE_ROOM);
        } else {
            handler.removeMessages(MSG_CODE_UPATE_ROOM);
            handler.sendEmptyMessageDelayed(MSG_CODE_UPATE_ROOM, UPDATE_TIME);
        }
//        rcvLiveRoom.post(showGameGuideRunnable);
    }

    /**
     * 数据懒加载
     */
    @Override
    protected void onLazyLoadData() {
        LogUtils.d(TAG, "onLazyLoadData-hasLogined:" + hasLogined);
        //暂时考虑所有数据均为空的情况下才走懒加载逻辑，避免某个接口数据为空的情况下，不停触发懒加载逻辑
        if (hasLogined && ((bannerAdapter.getBannerInfoList() == null || bannerAdapter.getBannerInfoList().size() == 0) || roomAdapter.getData().size() == 0)) {
            isLazyLoadRequestint = true;
            setCacheData();
            initGameHomeData();
        }
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogin(AccountInfo accountInfo) {
        if (accountInfo != null && accountInfo.getUid() > 0L) {
            hasLogined = true;
            onLazyLoadData();
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfo(UserInfo info) {
        if (info != null && info.getUid() > 0L) {
            onLazyLoadData();
        }
    }

    private void showGameHomeGuideDialog() {
        if (!CoreManager.getCore(IAuthCore.class).isLogin()) {
            return;
        }
        if (isRecommAdShow || isHidden) {
            return;
        }
        //展示新手引导
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (null != userInfo && null != userInfo.getGuideState()) {
            String type6 = userInfo.getGuideState().getType6();
            if (null == type6 || "0".equals(type6)) {
                int[] locations = new int[2];
                rcvPlayTogether.getLocationInWindow(locations);
                int[] locations1 = new int[2];
                rcvIcon.getLocationInWindow(locations1);
                if (null == gameHomeGuideDialog) {
                    if (null == getActivity()) {
                        return;
                    }
                    gameHomeGuideDialog = new NoviceGuideDialog(getActivity(), NoviceGuideDialog.GuideType.GAME_HOME_GUIDE_1, locations);
                }
                gameHomeGuideDialog.setGameHomeGuideLocations(locations1);
                gameHomeGuideDialog.setGameEnitities(playTogetherAdapter.getData());
                if (iconAdapter.getData().size() > 2) {
                    gameHomeGuideDialog.setIconUrl(iconAdapter.getData().get(2).getPic());
                }
                if (!gameHomeGuideDialog.isShowing()) {
                    gameHomeGuideDialog.show();
                }

            }
        }
    }

    @CoreEvent(coreClientClass = IRecommAdCoreClient.class)
    public void onRecommAdShowStatusChanged(boolean isShow) {
        isRecommAdShow = isShow;
//        rcvLiveRoom.post(showGameGuideRunnable);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (null != rcvLiveRoom) {
            rcvLiveRoom.removeCallbacks(showGameGuideRunnable);
        }
        MsgCenterRedPointStatusManager.getInstance().removeListener(this);
    }

    /**
     * 显示新粉丝红点
     */
    @Override
    public void showRedPointOnNewFan() {

    }

    /**
     * 显示新好友红点
     */
    @Override
    public void showRedPointOnNewFriend() {

    }

    /**
     * 显示新点赞红点
     */
    @Override
    public void showRedPointOnNewLike() {

    }

    /**
     * 签到红点
     */
    @Override
    public void showRedPointOnSignIn() {
        if (!isReuqestingIconList && null != iconAdapter) {
            iconAdapter.notifyDataSetChanged();
        }
    }
}
