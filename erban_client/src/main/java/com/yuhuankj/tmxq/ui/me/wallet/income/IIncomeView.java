package com.yuhuankj.tmxq.ui.me.wallet.income;

import com.yuhuankj.tmxq.ui.me.charge.interfaces.IPayView;

/**
 * Created by MadisonRong on 08/01/2018.
 */

public interface IIncomeView extends IPayView {

    public void handleClick(int id);

    public void hasBindPhone();

    public void hasBindPhoneFail(String error);
}
