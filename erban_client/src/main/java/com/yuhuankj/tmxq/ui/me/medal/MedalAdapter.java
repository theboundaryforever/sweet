package com.yuhuankj.tmxq.ui.me.medal;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liaoxy
 * @Description:勋章adapter
 * @date 2019/4/19 10:45
 */
public class MedalAdapter extends BaseQuickAdapter<MedalEntity, BaseViewHolder> {

    private List<Integer> curIndexIds;
    private boolean isSelect = false;
    private int useableCount = 1;//当前用户可以佩戴几个
    private boolean isShowAdorn = false;//是否显示佩戴

    public MedalAdapter(List<MedalEntity> data, int useableCount) {
        super(R.layout.item_medal, data);
        curIndexIds = new ArrayList<>();
        this.useableCount = useableCount;
    }

    @Override
    protected void convert(BaseViewHolder helper, MedalEntity item) {
        ImageView imvMyMedal = helper.getView(R.id.imvMyMedal);
        View vMedalMask = helper.getView(R.id.vMedalMask);
        TextView tvAdorn = helper.getView(R.id.tvAdorn);
        TextView tvMedalName = helper.getView(R.id.tvMedalName);

        if (TextUtils.isEmpty(item.getPicture())) {
            imvMyMedal.setImageResource(R.drawable.ic_medal_default);
        } else {
            ImageLoadUtils.loadImage(mContext, item.getPicture(), imvMyMedal, R.drawable.ic_medal_default);
        }
        tvMedalName.setText(item.getName());

        vMedalMask.setVisibility(View.GONE);
        tvAdorn.setVisibility(View.GONE);
        if (isSelect) {
            if (curIndexIds.contains(item.getTitleId())) {
                tvAdorn.setText("取消");
                tvAdorn.setBackgroundResource(R.drawable.shape_f74c44_radius9);
                vMedalMask.setVisibility(View.VISIBLE);
                tvAdorn.setVisibility(View.VISIBLE);
            } else if (isShowAdorn) {
                vMedalMask.setVisibility(View.VISIBLE);
                tvAdorn.setVisibility(View.VISIBLE);
                tvAdorn.setText("佩戴");
                tvAdorn.setBackgroundResource(R.drawable.shape_1bdcb4_radius9);
            }
        }
    }

    public void setCurSelect(boolean isSelect, List<Integer> curIndexIds) {
        this.isSelect = isSelect;
        this.curIndexIds = curIndexIds;
        isShowAdorn = false;
        if (curIndexIds.get(0) < 0) {
            isShowAdorn = true;
        } else if (useableCount == 2 && (curIndexIds.get(0) < 0 || curIndexIds.get(1) < 0)) {
            isShowAdorn = true;
        }
        notifyDataSetChanged();
    }
}