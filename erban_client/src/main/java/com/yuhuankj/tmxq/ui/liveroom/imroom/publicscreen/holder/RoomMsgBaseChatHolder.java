package com.yuhuankj.tmxq.ui.liveroom.imroom.publicscreen.holder;

import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;

import com.juxiao.library_utils.DisplayUtils;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.erban.libcommon.widget.messageview.MsgHolderLayoutId;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMember;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMessage;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_core.utils.BaseConstant;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.imroom.publicscreen.base.RoomMsgBaseHolder;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.LabelImageSpan;

/**
 * 文件描述：有指定发送人的消息holder基类
 *
 * @auther：zwk
 * @data：2019/5/7
 */
@MsgHolderLayoutId(value = R.layout.item_room_msg_chat_txt)
public abstract class RoomMsgBaseChatHolder extends RoomMsgBaseHolder<IMRoomMessage> {
    private int iconPlaceSpace = 36;


    public RoomMsgBaseChatHolder(View itemView) {
        super(itemView);
        if (itemView != null && itemView.getContext() != null) {
            iconPlaceSpace = DisplayUtils.dip2px(itemView.getContext(), 16);
        }
    }

    @Override
    protected int getItemUserClickLayoutId() {
        return R.id.tv_nick;
    }

    @Override
    public void onDataTransformView(RoomMsgBaseHolder holder, IMRoomMessage message, int position) {
        TextView etvContent = (TextView) holder.getView(R.id.etv_content);
        refreshSenderInfo(message, (TextView) holder.getView(R.id.tv_nick));
        etvContent.setText(message.getContent());
        if (GlideContextCheckUtil.checkContextUsable(etvContent.getContext())) {
            etvContent.setBackgroundResource(R.drawable.shape_liveroom_message_bg);
        }
        onDataTransformContent(holder, message, position, etvContent);
    }

    @Override
    protected int getItemClickLayoutId() {
        return R.id.rl_msgContainer;
    }

    protected abstract void onDataTransformContent(RoomMsgBaseHolder holder, IMRoomMessage message, int position, TextView etvContent);


    /**
     * 更新消息发送者信息
     *
     * @param chatRoomMessage
     * @param tvNick
     */
    private void refreshSenderInfo(IMRoomMessage chatRoomMessage, TextView tvNick) {
        IMRoomMember member;
        if (null == chatRoomMessage.getImRoomMember()) {
            member = new IMRoomMember();
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            if (userInfo != null) {
                member.setNick(userInfo.getNick());
            } else {
                member.setNick("");
            }
        } else {
            member = chatRoomMessage.getImRoomMember();
        }
        String iconContent = "";
        SpannableStringBuilder builder = new SpannableStringBuilder(iconContent);
        if (!ListUtils.isListEmpty(member.getIconList())) {
            for (String icon : member.getIconList()) {
                if (StringUtils.isNotEmpty(icon)) {
                    //插入小图标占位符
                    builder.append(BaseConstant.LABEL_PLACEHOLDER);
                    LabelImageSpan imageSpan = new LabelImageSpan(tvNick.getContext(), R.drawable.ic_room_medal_default, icon, tvNick, iconPlaceSpace);
                    builder.setSpan(imageSpan, builder.length() - BaseConstant.LABEL_PLACEHOLDER.length(),
                            builder.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    builder.append(" ");
                }
            }
        }
        builder.append(StringUtils.limitStrNew(tvNick.getContext(), member.getNick(), 8));
        if (member.getExperLevel() < 50) {
            builder.setSpan(new ForegroundColorSpan(0x90FFFFFF),
                    0, builder.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        } else {
            builder.setSpan(new ForegroundColorSpan(0xFFF8DD4E),
                    0, builder.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        }
        tvNick.setText(builder);
    }
}
