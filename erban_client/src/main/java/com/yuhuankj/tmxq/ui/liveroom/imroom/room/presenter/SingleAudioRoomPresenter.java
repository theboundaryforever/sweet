package com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomModel;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.model.RoomSettingModel;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.base.presenter.BaseRoomDetailPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IRoomDetailView;

import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_CLOSE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_OPEN;

/**
 * 语音房间页面的presenter
 */
public class SingleAudioRoomPresenter extends BaseRoomDetailPresenter<IRoomDetailView> {

    private final String TAG = SingleAudioRoomPresenter.class.getSimpleName();

    public SingleAudioRoomPresenter() {
        super();
    }

    /**
     * 抱人上麦
     */
    public void holdUpMicro(long uid) {
        int freePosition = RoomDataManager.get().findFreePosition();
        if (freePosition == Integer.MIN_VALUE) {
            SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext().getString(R.string.no_empty_mic));
            return;
        }
        IMRoomMessageManager.get().inviteMicroPhoneBySdk(uid, freePosition);
    }

    @Deprecated
    private final long innerShowAttentionMsgTime = 30000L;

    @Override
    public void updateRoomGiftEffectSwitch(boolean isShowGiftEffect) {
        RoomInfo cRoomInfo = getCurrRoomInfo();
        if (null != cRoomInfo) {
            OkHttpManager.MyCallBack myCallBack = new OkHttpManager.MyCallBack<ServiceResult<RoomInfo>>() {
                @Override
                public void onError(Exception e) {
                    e.printStackTrace();

                }

                @Override
                public void onResponse(ServiceResult<RoomInfo> data) {
                    //低价值礼物特效，开关变更通知
                    IMRoomMessageManager.get().systemNotificationBySdk(
                            CoreManager.getCore(IAuthCore.class).getCurrentUid(),
                            isShowGiftEffect ? CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_OPEN :
                                    CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_CLOSE, -1);
                }
            };
            //调用接口更新
            updateRoomInfo(cRoomInfo.title, cRoomInfo.getRoomDesc(),
                    cRoomInfo.roomPwd, cRoomInfo.getRoomTag(), cRoomInfo.tagId,
                    cRoomInfo.getBackPic(), cRoomInfo.getBackName(),
                    isShowGiftEffect ? 0 : 1, cRoomInfo.getCharmSwitch(),
                    cRoomInfo.getRoomNotice(), cRoomInfo.getPlayInfo(), myCallBack);
        }
    }
    /**
     * 是否已经生成过关注房主消息到消息列表
     */
    private boolean hasGenAttentionMsg = false;

    /**
     * 进入房间的时间 毫秒
     */
    private long timeInRoom = 0L;

    /**
     * 更新房间设置信息
     *
     * @param title
     * @param desc
     * @param pwd
     * @param label            标签名字
     * @param tagId            标签id
     * @param backPic
     * @param giftEffectParams
     */
    private void updateRoomInfo(String title, String desc, String pwd, String label, int tagId,
                                String backPic, String backName, int giftEffectParams,
                                int charmSwitch, String roomNotice, String playInfo,
                                OkHttpManager.MyCallBack<ServiceResult<RoomInfo>> myCallBack) {
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if ("".equals(title)) {
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(uid);
            if (userInfo != null) {
                title = userInfo.getNick() + "的房间";
            }
        }


        new RoomSettingModel().updateRoomInfo(title, desc, pwd, label, tagId, uid,
                CoreManager.getCore(IAuthCore.class).getTicket(), backPic, backName,
                giftEffectParams, charmSwitch, roomNotice, playInfo, myCallBack);

    }

    public void resetTimeInRoom() {
        RoomInfo cRoomInfo = getCurrRoomInfo();
        if (null != cRoomInfo && !RoomDataManager.get().isRoomOwner()) {
            timeInRoom = System.currentTimeMillis();
            LogUtils.d(TAG, "resetTimeInRoom timeInRoom:" + timeInRoom);
        }
    }

    public void doRoomOwnerLiveOpera() {
        RoomInfo cRoomInfo = getCurrRoomInfo();
        if (null == cRoomInfo) {
            return;
        }
        if (null == RoomDataManager.get().getAdditional()) {
            return;
        }
        LogUtils.d(TAG, "doRoomOwnerLiveOpera mPlayState:" + RoomDataManager.get().getAdditional().getPlayState());
        if (RoomDataManager.get().getAdditional().getPlayState() == 1) {
            if (null != getMvpView()) {
                getMvpView().openRoomLiveInfoH5View();
            }
        } else {
            new IMRoomModel().startPlayRoom(cRoomInfo.getRoomId(), null);
        }
    }

}
