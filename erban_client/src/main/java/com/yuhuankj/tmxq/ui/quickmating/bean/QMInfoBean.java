package com.yuhuankj.tmxq.ui.quickmating.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class QMInfoBean implements Serializable {
    private int peaNum;
    private int userFreeMatchCount;
    private int isOpen;
    private QMConfig userMatchConfig;
    private List<String> topicList = new ArrayList<>();
    private List<String> ruleList = new ArrayList<>();

    private int userBuyMatchCount;//剩余购买次数

    public int getUserBuyMatchCount() {
        return userBuyMatchCount;
    }

    public void setUserBuyMatchCount(int userBuyMatchCount) {
        this.userBuyMatchCount = userBuyMatchCount;
    }

    public int getPeaNum() {
        return peaNum;
    }

    public void setPeaNum(int peaNum) {
        this.peaNum = peaNum;
    }

    public QMConfig getUserMatchConfig() {
        return userMatchConfig;
    }

    public void setUserMatchConfig(QMConfig userMatchConfig) {
        this.userMatchConfig = userMatchConfig;
    }

    public List<String> getTopicList() {
        return topicList;
    }

    public void setTopicList(List<String> topicList) {
        this.topicList = topicList;
    }

    public List<String> getRuleList() {
        return ruleList;
    }

    public void setRuleList(List<String> ruleList) {
        this.ruleList = ruleList;
    }

    public void setUserFreeMatchCount(int userFreeMatchCount) {
        this.userFreeMatchCount = userFreeMatchCount;
    }

    public int getUserFreeMatchCount() {
        return userFreeMatchCount;
    }

    public int getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(int isOpen) {
        this.isOpen = isOpen;
    }
}
