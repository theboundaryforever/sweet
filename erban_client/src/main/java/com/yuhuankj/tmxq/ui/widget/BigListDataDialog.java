package com.yuhuankj.tmxq.ui.widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomModel;
import com.tongdaxing.xchat_core.result.RoomConsumeInfoListResult;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.queue.bean.RoomConsumeInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.adapter.RoomConsumeListAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by MadisonRong on 13/01/2018.
 */

public class BigListDataDialog extends AppCompatDialogFragment implements View.OnClickListener, BaseQuickAdapter.OnItemClickListener {

    public static final String TYPE_CONTRIBUTION = "ROOM_CONTRIBUTION";
    public static final String KEY_TITLE = "KEY_TITLE";
    public static final String KEY_TYPE = "KEY_TYPE";
    @BindView(R.id.bu_pay_tab)
    ButtonPayList buPayTab;
    @BindView(R.id.bu_imcome_tab)
    ButtonPayList buImcomeTab;
    @BindView(R.id.iv_close_dialog)
    ImageView ivCloseDialog;
    @BindView(R.id.bu_tab_day)
    ButtonPayList buTabDay;
    @BindView(R.id.bu_tab_week)
    ButtonPayList buTabWeek;
    @BindView(R.id.bu_tab_all)
    ButtonPayList buTabAll;
    @BindView(R.id.rv_pay_income_list)
    RecyclerView rvPayIncomeList;
    Unbinder unbinder;


    private int type = 1;
    private int dataType = 1;
    private long roomUId;
    private int roomType = RoomInfo.ROOMTYPE_HOME_PARTY;
    private RoomConsumeListAdapter roomConsumeListAdapter;
    private View noDataView;
    private SelectOptionAction selectOptionAction;

    public BigListDataDialog() {
    }

    public static BigListDataDialog newContributionListInstance(Context context) {
        return newInstance(context.getString(R.string.contribution_list_text), TYPE_CONTRIBUTION);
    }

    public static BigListDataDialog newInstance(String title, String type) {
        BigListDataDialog listDataDialog = new BigListDataDialog();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_TITLE, title);
        bundle.putString(KEY_TYPE, type);
        listDataDialog.setArguments(bundle);
        return listDataDialog;
    }

    public void show(FragmentManager fragmentManager) {
        show(fragmentManager, null);
    }

    @Override
    public void show(FragmentManager fragmentManager, String tag) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(this, tag).addToBackStack(null);
        transaction.commitAllowingStateLoss();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        // setup window and width
        View view = inflater.inflate(R.layout.dialog_big_list_data, window.findViewById(android.R.id.content), false);
        unbinder = ButterKnife.bind(this, view);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        setCancelable(true);

        RoomInfo roomInfo = BaseRoomServiceScheduler.getCurrentRoomInfo();
        if (roomInfo != null) {
            roomUId = roomInfo.getUid();
            roomType = roomInfo.getType();
        }
        view.findViewById(R.id.iv_close_dialog).setOnClickListener(this);
        noDataView = view.findViewById(R.id.tv_no_data);
        unbinder = ButterKnife.bind(this, view);
        initView();
        initRv();

        buImcomeTab.setVisibility(roomType == RoomInfo.ROOMTYPE_SINGLE_AUDIO ? View.GONE : View.VISIBLE);
        return view;
    }

    private void initRv() {
        rvPayIncomeList.setLayoutManager(new RecyclerViewNoBugLinearLayoutManager(getContext()));
        roomConsumeListAdapter = new RoomConsumeListAdapter(getContext());
        roomConsumeListAdapter.setOnItemClickListener(this);
        rvPayIncomeList.setAdapter(roomConsumeListAdapter);
        getData();
    }

    private void initView() {
        buPayTab.getTabName().setText(getResources().getString(R.string.room_ctrb_fortune_list));
        buImcomeTab.getTabName().setText(getResources().getString(R.string.room_ctrb_charm_list));
        buTabDay.getTabName().setText(getResources().getString(R.string.room_ctrb_day_list));
        buTabWeek.getTabName().setText(getResources().getString(R.string.room_ctrb_week_list));
        buTabAll.getTabName().setText(getResources().getString(R.string.room_ctrb_total_list));

        buTabDay.getLine().setVisibility(View.VISIBLE);
        buPayTab.getLine().setVisibility(roomType != RoomInfo.ROOMTYPE_HOME_PARTY ? View.GONE : View.VISIBLE);

        buPayTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                typeChange(1);
            }
        });
        buImcomeTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                typeChange(2);
            }
        });
        buTabDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataTypeChange(1);
            }
        });
        buTabWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataTypeChange(2);
            }
        });
        buTabAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataTypeChange(3);
            }
        });
    }

    private void dataTypeChange(int i) {
        if (i == dataType) {
            return;
        }
        dataType = i;
        buTabDay.getLine().setVisibility(i == 1 ? View.VISIBLE : View.INVISIBLE);
        buTabWeek.getLine().setVisibility(i == 2 ? View.VISIBLE : View.INVISIBLE);
        buTabAll.getLine().setVisibility(i == 3 ? View.VISIBLE : View.INVISIBLE);
        if (selectOptionAction != null) {
            selectOptionAction.optionClick();
        }
        getData();
    }

    private void getData() {
        OkHttpManager.MyCallBack<RoomConsumeInfoListResult> myCallBack = new OkHttpManager.MyCallBack<RoomConsumeInfoListResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (selectOptionAction != null) {
                    selectOptionAction.onDataResponse();
                }
            }

            @Override
            public void onResponse(RoomConsumeInfoListResult response) {
                if (selectOptionAction != null) {
                    selectOptionAction.onDataResponse();
                }
                if (response.isSuccess() && response.getData() != null) {
                    if (noDataView != null) {
                        noDataView.setVisibility(response.getData().size() > 0 ? View.GONE : View.VISIBLE);
                    }
                    if (roomConsumeListAdapter != null) {
                        roomConsumeListAdapter.setNewData(response.getData());
                    }
                }
            }
        };
        new IMRoomModel().getRoomConsumeList(roomUId, roomType, dataType, type, myCallBack);
    }

    private void typeChange(int i) {
        if (i == type) {
            return;
        }

        if (roomConsumeListAdapter != null) {
            if (i == 1) {
                roomConsumeListAdapter.rankType = 0;
            } else {
                roomConsumeListAdapter.rankType = 1;
            }
        }
        dataType = 1;
        type = i;

        buTabDay.getLine().setVisibility(dataType == 1 ? View.VISIBLE : View.INVISIBLE);
        buTabWeek.getLine().setVisibility(dataType == 2 ? View.VISIBLE : View.INVISIBLE);
        buTabAll.getLine().setVisibility(dataType == 3 ? View.VISIBLE : View.INVISIBLE);
        buPayTab.getLine().setVisibility(i == 1 && roomType == RoomInfo.ROOMTYPE_HOME_PARTY ? View.VISIBLE : View.INVISIBLE);
        buImcomeTab.getLine().setVisibility(i == 2 ? View.VISIBLE : View.INVISIBLE);
        getData();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_close_dialog:
                dismiss();
                break;
            default:
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        if (noDataView != null){
            noDataView = null;
        }
        if (selectOptionAction != null){
            selectOptionAction = null;
        }
        if (roomConsumeListAdapter != null) {
            roomConsumeListAdapter = null;
        }
    }

    @Override
    public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
        if (roomConsumeListAdapter == null) {
            return;
        }
        List<RoomConsumeInfo> list = roomConsumeListAdapter.getData();
        if (ListUtils.isListEmpty(list)) {
            return;
        }
        RoomConsumeInfo roomConsumeInfo = list.get(i);
        UserInfoDialog userInfoDialog = new UserInfoDialog(getContext(), roomConsumeInfo.getCtrbUid());
        userInfoDialog.show();
    }

    public void setSelectOptionAction(SelectOptionAction selectOptionAction) {
        this.selectOptionAction = selectOptionAction;
    }

    public interface SelectOptionAction {
        void optionClick();

        void onDataResponse();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //参考https://segmentfault.com/q/1010000017286787
        Dialog dialog = getDialog();
        if (null != dialog) {
            dialog.setOnShowListener(null);
            dialog.setOnCancelListener(null);
        }
    }

}
