package com.yuhuankj.tmxq.ui.signAward.view;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.noober.background.view.BLTextView;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.signAward.model.SignRecvGiftPkgInfo;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

/**
 * 签到奖励领取页面
 *
 * @author weihaitao
 * @date 2019/5/22
 */
public class BaseSignAwardTakeDialog extends BaseActivity {


    protected BLTextView mBltvFindTa;
    protected long roomUid;
    protected int roomType;
    protected String nick;
    protected String desc;
    protected List<SignRecvGiftPkgInfo> signRecvGiftPkgInfos;
    protected int giftPkgPicSize = 0;
    private ImageView mIvCenter;
    private TextView mTvCenter;
    private ImageView mIvLeft;
    private TextView mTvLeft;
    private ImageView mIvRight;
    private TextView mTvRight;
    private TextView mTvTakeTps;
    private ImageView ivClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_award_take);
        initData();
        initView();
        setListener();
    }

    private void initData() {
        Intent intent = getIntent();
        if (intent.hasExtra("roomUid")) {
            roomUid = intent.getLongExtra("roomUid", 0L);
        }
        if (intent.hasExtra("roomType")) {
            roomType = intent.getIntExtra("roomType", 0);
        }
        if (intent.hasExtra("nick")) {
            nick = intent.getStringExtra("nick");
        }
        if (intent.hasExtra("desc")) {
            desc = intent.getStringExtra("desc");
        }
        if (intent.hasExtra("signRecvGiftPkgInfos")) {
            signRecvGiftPkgInfos = (List<SignRecvGiftPkgInfo>) intent.getSerializableExtra("signRecvGiftPkgInfos");
        }
        if (null == signRecvGiftPkgInfos || signRecvGiftPkgInfos.size() == 0) {
            finish();
        }
        giftPkgPicSize = DisplayUtility.dp2px(this, 62);
    }

    private void initView() {
        mIvCenter = (ImageView) findViewById(R.id.ivCenter);
        mTvCenter = (TextView) findViewById(R.id.tvCenter);
        mIvLeft = (ImageView) findViewById(R.id.ivLeft);
        mTvLeft = (TextView) findViewById(R.id.tvLeft);
        mIvRight = (ImageView) findViewById(R.id.ivRight);
        mTvRight = (TextView) findViewById(R.id.tvRight);
        mTvTakeTps = (TextView) findViewById(R.id.tvTakeTps);
        mBltvFindTa = (BLTextView) findViewById(R.id.bltvFindTa);
        ivClose = (ImageView) findViewById(R.id.ivClose);

        if (!TextUtils.isEmpty(nick)) {
            //昵称不为空，有选择用户流程
            if (nick.length() > 6) {
                nick = getString(R.string.nick_length_max_six, nick.substring(0, 6));
            }
            String tips = getResources().getString(R.string.sign_award_tips_enter_room, nick);
            SpannableStringBuilder ssb = new SpannableStringBuilder(tips);
            int firstIndex = tips.indexOf(nick);
            ssb.setSpan(new ForegroundColorSpan(Color.WHITE), 0, firstIndex, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#FDC943")),
                    firstIndex, firstIndex+nick.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            ssb.setSpan(new ForegroundColorSpan(Color.WHITE), firstIndex + nick.length(), tips.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            mTvTakeTps.setText(ssb);
            mBltvFindTa.setVisibility(View.GONE);
            ivClose.setVisibility(View.GONE);
        } else {
            //昵称为空，无选择用户流程
            mTvTakeTps.setText(desc);
            mTvTakeTps.setTextColor(Color.parseColor("#FDC943"));
            if (0 == roomUid) {
                mBltvFindTa.setVisibility(View.GONE);
                ivClose.setVisibility(View.VISIBLE);
            } else {
                mBltvFindTa.setVisibility(View.VISIBLE);
                ivClose.setVisibility(View.GONE);
            }

        }
        if (ListUtils.isListEmpty(signRecvGiftPkgInfos)){
            return;
        }
        //显示礼包图标和名字
        SignRecvGiftPkgInfo signRecvGiftPkgInfo  = signRecvGiftPkgInfos.get(0);
        ImageLoadUtils.loadImage(this,
                ImageLoadUtils.toThumbnailUrl(giftPkgPicSize, giftPkgPicSize,
                        signRecvGiftPkgInfo.getGiftPicUrl())
                , mIvLeft);
        mTvLeft.setText(signRecvGiftPkgInfo.getGiftName());

        if (signRecvGiftPkgInfos.size() > 1) {
            signRecvGiftPkgInfo = signRecvGiftPkgInfos.get(1);
            ImageLoadUtils.loadImage(this,
                    ImageLoadUtils.toThumbnailUrl(giftPkgPicSize, giftPkgPicSize,
                            signRecvGiftPkgInfo.getGiftPicUrl())
                    , mIvCenter);
            mTvCenter.setText(signRecvGiftPkgInfo.getGiftName());
        }

        if (signRecvGiftPkgInfos.size() > 2) {
            signRecvGiftPkgInfo = signRecvGiftPkgInfos.get(2);
            ImageLoadUtils.loadImage(this,
                    ImageLoadUtils.toThumbnailUrl(giftPkgPicSize, giftPkgPicSize,
                            signRecvGiftPkgInfo.getGiftPicUrl())
                    , mIvRight);
            mTvRight.setText(signRecvGiftPkgInfo.getGiftName());
        }
    }

    private void setListener() {
        mBltvFindTa.setOnClickListener(this);
        ivClose.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivClose:
                finish();
                break;
            case R.id.bltvFindTa:
                RoomServiceScheduler.getInstance().enterRoom(this, roomUid, roomType);
                //萌新大礼包-领取界面--去找TA按钮打点事件
                StatisticManager.get().onEvent(this,
                        StatisticModel.EVENT_ID_NEWS_GIFT_FINDTA,
                        StatisticModel.getInstance().getUMAnalyCommonMap(this));
                finish();
                break;
            default:
                super.onClick(view);
                break;
        }
    }
}
