package com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget;

import android.content.Context;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.xchat_core.bean.RoomRedPacket;
import com.tongdaxing.xchat_core.im.custom.bean.BosonFriendUpMicAnimAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.DetonateGiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.LoverUpMicAnimAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomRedPacketAttachment;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomEvent;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMessage;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.widget.CircleImageView;

import java.util.concurrent.ConcurrentLinkedQueue;

import io.reactivex.disposables.CompositeDisposable;

/**
 * 文件描述：悬浮气泡提醒的View
 *
 * @auther：zwk
 * @data：2019/7/12
 */
public class RoomFloatingBubbleView extends RelativeLayout {
    private ImageView ivBg;
    private CircleImageView civHead;
    private ImageView ivLeftIcon;
    private TextView tvContent;
    private CompositeDisposable compositeDisposable;
    //房间悬浮气泡消息队列
    private ConcurrentLinkedQueue<IMRoomMessage> roomFloatingMsgQueue;

    public RoomFloatingBubbleView(Context context) {
        this(context, null);
    }

    public RoomFloatingBubbleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }


    private void initView(Context context) {
        inflate(context, R.layout.view_floating_bubble, this);
        ivBg = findViewById(R.id.iv_floating_bubble_bg);
        civHead = findViewById(R.id.civ_floating_bubble_head);
        ivLeftIcon = findViewById(R.id.iv_floating_bubble_icon);
        tvContent = findViewById(R.id.tv_floating_bubble_content);
        roomFloatingMsgQueue = new ConcurrentLinkedQueue<>();
    }

    private void accept(IMRoomEvent imRoomEvent) {
        if (imRoomEvent.getEvent() == IMRoomEvent.ROOM_FLOATING_BUBBL_NOTIFY) {
            roomFloatingMsgQueue.offer(imRoomEvent.getIMRoomMessage());
            showRoomFloatingBubbleMsg();
        }
    }

    private boolean isPlaying = false;

    public void showRoomFloatingBubbleMsg() {
        if (!isPlaying) {
            isPlaying = true;
            if (roomFloatingMsgQueue != null && !roomFloatingMsgQueue.isEmpty()) {
                IMRoomMessage msg = roomFloatingMsgQueue.poll();
                if (msg != null && msg.getAttachment() != null) {
                    switch (msg.getAttachment().getFirst()) {
                        case CustomAttachment.CUSTOM_MSG_ROOM_DETONATING_GIFT:
                            if (msg.getAttachment().getSecond() == CustomAttachment.CUSTOM_MSG_ROOM_DETONATING_GIFT_NOTIFY) {
                                popularGiftFloatingBubble((DetonateGiftAttachment) msg.getAttachment());
                            } else {
                                isPlaying = false;
                            }
                            break;
                        case CustomAttachment.CUSTOM_MSG_ROOM_LOVER_UP_MIC:
                            if (msg.getAttachment().getSecond() == CustomAttachment.CUSTOM_MSG_ROOM_LOVER_UP_MIC) {
                                loverUpMicFloatingBubble((LoverUpMicAnimAttachment) msg.getAttachment());
                            } else {
                                isPlaying = false;
                            }
                            break;
                        case CustomAttachment.CUSTOM_MSG_BOSON_FIRST:
                            if (msg.getAttachment().getSecond() == CustomAttachment.CUSTOM_MSG_BOSON_UP_MACRO) {
                                bosonFriendFloatingBubble((BosonFriendUpMicAnimAttachment) msg.getAttachment());
                            } else {
                                isPlaying = false;
                            }
                            break;
                        case CustomAttachment.CUSTOM_MSG_ROOM_RED_PACKET:
                            if (msg.getAttachment().getSecond() == CustomAttachment.CUSTOM_MSG_ROOM_RED_PACKET_RECEIVE) {
                                if (msg.getAttachment() instanceof RoomRedPacketAttachment) {
                                    redPackageFloatingBubble(((RoomRedPacketAttachment) msg.getAttachment()).getDataInfo());
                                }
                            } else {
                                isPlaying = false;
                            }
                            break;
                        default:
                            isPlaying = false;
                            break;
                    }
                } else {
                    isPlaying = false;
                    clearAnimation();
                }
            } else {
                isPlaying = false;
                clearAnimation();
            }
        }
    }

    /**
     * 显示爆出礼物的悬浮气泡
     *
     * @param detonateGiftAttachment
     */
    private void popularGiftFloatingBubble(DetonateGiftAttachment detonateGiftAttachment) {
        if (getContext() == null) {
            isPlaying = false;
            return;
        }
        ivBg.setImageResource(R.mipmap.icon_gift_detonate_full_server_notify);
        ivLeftIcon.setVisibility(View.GONE);
        civHead.setVisibility(View.VISIBLE);
        civHead.setBorderColor(0xffA98ACE);
        civHead.setBorderWidth(1);
        ImageLoadUtils.loadCircleImage(getContext(), StringUtils.isNotEmpty(detonateGiftAttachment.getDetonatingAvatar()) ? detonateGiftAttachment.getDetonatingAvatar() : "", civHead, R.drawable.ic_default_avatar);
        String nick = StringUtils.limitStr(getContext(), detonateGiftAttachment.getDetonatingNick(), 6);
        String giftName = StringUtils.isEmpty(detonateGiftAttachment.getDetonatingGiftName()) ? "xxx" : detonateGiftAttachment.getDetonatingGiftName();
        String content = getContext().getResources().getString(R.string.room_detonate_gift_notify, nick, giftName);
        SpannableStringBuilder ssb = new SpannableStringBuilder(content);
        int firstIndex = content.indexOf(nick);
        ssb.setSpan(new ForegroundColorSpan(Color.WHITE), 0, firstIndex, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new ForegroundColorSpan(0xFFE7DA27), firstIndex, firstIndex + nick.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        int secondIndex = content.indexOf(giftName);
        ssb.setSpan(new ForegroundColorSpan(Color.WHITE), firstIndex + nick.length(), secondIndex, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new ForegroundColorSpan(0xFFE7DA27), secondIndex, secondIndex + giftName.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new ForegroundColorSpan(Color.WHITE), secondIndex + giftName.length(), content.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        tvContent.setText(ssb);
        ivBg.setOnClickListener(v -> {
            if (RoomDataManager.get().getCurrentRoomInfo() != null) {
                if (RoomDataManager.get().getCurrentRoomInfo().getUid() != detonateGiftAttachment.getRoomUid() || RoomDataManager.get().getCurrentRoomInfo().getType() !=detonateGiftAttachment.getRoomType()) {//不同房间跳转
                    RoomServiceScheduler.getInstance().enterOtherTypeRoomFromService(getContext(), detonateGiftAttachment.getRoomUid(), detonateGiftAttachment.getRoomType());
                    release();
                }
            }
        });
        setVisibility(View.VISIBLE);
        startPlayFloatingAnim();
    }


    /**
     * 情侣上麦的悬浮气泡
     *
     * @param attachment
     */
    private void loverUpMicFloatingBubble(LoverUpMicAnimAttachment attachment) {
        if (getContext() == null) {
            isPlaying = false;
            return;
        }
        ivBg.setImageResource(R.mipmap.bg_room_lover_up_mic);
        ivLeftIcon.setImageResource(R.mipmap.icon_room_lover_up_mic);
        ivLeftIcon.setVisibility(View.VISIBLE);
        civHead.setVisibility(View.GONE);
        String firstNick = StringUtils.limitStr(getContext(), attachment.getFirstNick(), 4);
        String secondNick = StringUtils.limitStr(getContext(), attachment.getSecondNick(), 4);
        String tips = getContext().getString(R.string.room_lover_anim_tips, firstNick, secondNick);
        SpannableStringBuilder ssb = new SpannableStringBuilder(tips);
        int firstIndex = tips.indexOf(firstNick);
        int secondIndex = tips.lastIndexOf(secondNick);
        ssb.setSpan(new ForegroundColorSpan(0xFFFFFF00), firstIndex, firstIndex + firstNick.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new ForegroundColorSpan(Color.WHITE), firstIndex + firstNick.length(), secondIndex, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new ForegroundColorSpan(0xFFFFFF00), secondIndex, secondIndex + secondNick.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new ForegroundColorSpan(Color.WHITE), secondIndex + secondNick.length(), tips.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        tvContent.setText(ssb);
        setVisibility(View.VISIBLE);
        startPlayFloatingAnim();
    }

    /**
     * 挚友悬浮气泡
     *
     * @param bfUpMicAnimAttachment
     */
    private void bosonFriendFloatingBubble(BosonFriendUpMicAnimAttachment bfUpMicAnimAttachment) {
        if (getContext() == null) {
            isPlaying = false;
            return;
        }
        ivLeftIcon.setVisibility(View.VISIBLE);
        civHead.setVisibility(View.GONE);
        setBosonFriendBackId(bfUpMicAnimAttachment.getBosonFriendLevel());
        //修改提示文本显示样式
        String firstNick = StringUtils.limitStr(getContext(), bfUpMicAnimAttachment.getFirstNick(), 4);
        String secondNick = StringUtils.limitStr(getContext(), bfUpMicAnimAttachment.getSecondNick(), 4);
        String tips = getContext().getString(R.string.room_bosonfriend_anim_tips, bfUpMicAnimAttachment.getBosonFriendLevelName(), firstNick, secondNick);
        SpannableStringBuilder ssb = new SpannableStringBuilder(tips);
        int firstIndex = tips.indexOf(firstNick);
        int secondIndex = tips.lastIndexOf(secondNick);
        ssb.setSpan(new ForegroundColorSpan(Color.WHITE), 0, firstIndex, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new ForegroundColorSpan(0xFFFFFF00), firstIndex, firstIndex + firstNick.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new ForegroundColorSpan(Color.WHITE), firstIndex + firstNick.length(), secondIndex, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new ForegroundColorSpan(0xFFFFFF00), secondIndex, secondIndex + secondNick.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new ForegroundColorSpan(Color.WHITE), secondIndex + secondNick.length(), tips.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        tvContent.setText(ssb);
        setVisibility(View.VISIBLE);
        startPlayFloatingAnim();
    }

    /**
     * 红包悬浮气泡
     *
     * @param roomRedPacket
     */
    private void redPackageFloatingBubble(RoomRedPacket roomRedPacket) {
        if (getContext() == null || roomRedPacket == null) {
            isPlaying = false;
            return;
        }
        ivBg.setImageResource(R.drawable.ic_red_packet_room_banner_bg2);
        ivLeftIcon.setVisibility(View.GONE);
        civHead.setVisibility(View.VISIBLE);
        civHead.setBorderColor(0xFFFF5555);
        civHead.setBorderWidth(1);
        //显示红包横幅
        ImageLoadUtils.loadCircleImage(getContext(), roomRedPacket.getAvatar(), civHead, R.drawable.ic_default_avatar);
        String nickName = roomRedPacket.getNick();
        if (TextUtils.isEmpty(nickName)) {
            nickName = "";
        }
        if (nickName.length() > 6) {
            nickName = nickName.substring(0, 6) + "...";
        }
        String bannerStr = String.format("哇，%s在%s（id:%s）里发了一个大红包，快来抢吧！", nickName, StringUtils.limitStr(getContext(), roomRedPacket.getTitle(), 6), roomRedPacket.getRoomNo() + "");
        SpannableStringBuilder ssb = new SpannableStringBuilder(bannerStr);
        ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#FFED21")), 2, 2 + nickName.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        tvContent.setText(ssb);
        ivBg.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getContext() == null) {
                    return;
                }
                if (RoomDataManager.get().getCurrentRoomInfo() != null) {
                    if (RoomDataManager.get().getCurrentRoomInfo().getRoomId() != roomRedPacket.getRoomId()) {//不同房间跳转
                        RoomServiceScheduler.getInstance().enterOtherTypeRoomFromService(getContext(), roomRedPacket.getRoomUid(), roomRedPacket.getRoomType());
                        release();
                    }
                }
            }
        });
        setVisibility(View.VISIBLE);
        startPlayFloatingAnim();
    }

    /**
     * 设置挚友背景和左边logo
     *
     * @param level
     */
    public void setBosonFriendBackId(int level) {
        int micHead = R.drawable.ic_bf_upmicro_head_1;
        int micBg = R.drawable.ic_bf_upmicro_bg_1;
        switch (level) {
            case 1:
                break;
            case 2:
                micHead = R.drawable.ic_bf_upmicro_head_2;
                micBg = R.drawable.ic_bf_upmicro_bg_2;
                break;
            case 3:
                micHead = R.drawable.ic_bf_upmicro_head_3;
                micBg = R.drawable.ic_bf_upmicro_bg_3;
                break;
            case 4:
                micHead = R.drawable.ic_bf_upmicro_head_4;
                micBg = R.drawable.ic_bf_upmicro_bg_4;
                break;
            case 5:
                micHead = R.drawable.ic_bf_upmicro_head_5;
                micBg = R.drawable.ic_bf_upmicro_bg_5;
                break;
        }
        ivLeftIcon.setImageResource(micHead);
        ivBg.setImageResource(micBg);
    }

    /**
     * 播放特效动画
     */
    private void startPlayFloatingAnim() {
        TranslateAnimation translateAni = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, -1f, Animation.RELATIVE_TO_SELF,
                0, Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_SELF, 0);
        //设置动画执行的时间，单位是毫秒
        translateAni.setInterpolator(new DecelerateInterpolator());
        translateAni.setDuration(3000);
//        translateAni.setFillAfter(true);//不回到起始位置
        translateAni.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                isPlaying = false;
                setVisibility(View.GONE);
                showRoomFloatingBubbleMsg();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        // 启动动画
        startAnimation(translateAni);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        compositeDisposable.add(IMRoomMessageManager.get().getIMRoomEventObservable().subscribe(this::accept));
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
            compositeDisposable = null;
        }
        release();
    }

    public void release(){
        if (roomFloatingMsgQueue != null && !roomFloatingMsgQueue.isEmpty()) {
            roomFloatingMsgQueue.clear();
        }
        clearAnimation();
    }

}
