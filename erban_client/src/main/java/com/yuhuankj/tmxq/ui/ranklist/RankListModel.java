package com.yuhuankj.tmxq.ui.ranklist;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;

import java.util.Map;

/**
 * 排行榜model层
 */
public class RankListModel extends BaseMvpModel {

    //type：1巨星榜；2贵族榜；3房间榜
    //datetype：1、日榜；2周榜；3总榜;4月榜
    public void getRankList(int type, long datetype, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = OkHttpManager.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("type", String.valueOf(type));
        params.put("datetype", String.valueOf(datetype));
        OkHttpManager.getInstance().getRequest(UriProvider.getRankList(), params, callBack);
    }
}
