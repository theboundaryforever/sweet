package com.yuhuankj.tmxq.ui.me.noble;

import com.google.gson.JsonObject;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.result.WalletInfoResult;

import java.util.Map;

public class OrderConfirmPresenter extends AbstractMvpPresenter<OrderConfirmView> {

    private final String TAG = OrderConfirmPresenter.class.getSimpleName();

    public void buyNoble(int vipId, String payChannel, int type, long roomId) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("payChannel", payChannel);
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        param.put("type", String.valueOf(type));
        if (roomId > 0) {
            param.put("roomId", String.valueOf(roomId));
        }
        param.put("vipId", String.valueOf(vipId));

        OkHttpManager.getInstance().postRequest(UriProvider.getBuyNobleUrl(), param, new OkHttpManager.MyCallBack<ServiceResult<JsonObject>>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (getMvpView() != null) {
                    getMvpView().onBuyNoble(false, null, null);
                }
            }

            @Override
            public void onResponse(ServiceResult<JsonObject> response) {
                if (null == getMvpView()) {
                    return;
                }
                if (null != response) {
                    getMvpView().onBuyNoble(response.isSuccess(), response.getData(), response.getMessage());
                } else {
                    getMvpView().onBuyNoble(false, null, "");
                }
            }
        });
    }

    /**
     * 刷新钱包信息
     */
    public void refreshWalletInfo(boolean force) {
        String cacheStrategy = force ? "no-cache" : "max-stale";
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("Cache-Control", cacheStrategy);

        OkHttpManager.getInstance().getRequest(UriProvider.getWalletInfo(), params, new OkHttpManager.MyCallBack<WalletInfoResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if (getMvpView() != null) {
                    getMvpView().onWalletInfoUpdateFail(e.getMessage());
                }
            }

            @Override
            public void onResponse(WalletInfoResult response) {
                if (null == getMvpView()) {
                    return;
                }
                if (null != response && response.isSuccess() && response.getData() != null) {
                    getMvpView().onWalletInfoUpdate(response.getData());
                } else {
                    getMvpView().onWalletInfoUpdateFail(response.getErrorMessage());
                }
            }
        });
    }
}
