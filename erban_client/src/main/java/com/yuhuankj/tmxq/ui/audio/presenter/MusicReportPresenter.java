package com.yuhuankj.tmxq.ui.audio.presenter;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.yuhuankj.tmxq.ui.audio.activity.MusicReportView;

import java.util.Map;

/**
 * <p>  </p>
 *
 * @author jiahui
 * @date 2017/12/8
 */
public class MusicReportPresenter extends AbstractMvpPresenter<MusicReportView> {

    private final String TAG = MusicReportPresenter.class.getSimpleName();

    public void reportMusic(String reason, long singId){
        Map<String,String> params = CommonParamUtil.getDefaultParam();
        params.put("reason", reason);//第一页
        params.put("singId", singId+"");//最多展示1000条
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid()+"");
        OkHttpManager.getInstance().postRequest(UriProvider.getMusicReportUrl(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if(null != getMvpView()){
                    getMvpView().onMusicReport(false,e.getMessage());
                }
            }

            @Override
            public void onResponse(Json json) {
                String msg = json.str("message");
                switch (json.num("code")){
                    case 200:
                        if(null != getMvpView()){
                            getMvpView().onMusicReport(true,msg);
                        }
                        break;
                    default:
                        if(null != getMvpView()){
                            getMvpView().onMusicReport(false,msg);
                        }
                        break;
                }
            }
        });
    }
}
