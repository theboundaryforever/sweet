package com.yuhuankj.tmxq.ui.me.setting.other;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.growingio.android.sdk.collection.GrowingIO;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.log.LogHelper;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.file.IFileCore;
import com.tongdaxing.xchat_core.file.IFileCoreClient;
import com.tongdaxing.xchat_core.home.IHomeCore;
import com.tongdaxing.xchat_core.home.IHomeCoreClient;
import com.tongdaxing.xchat_core.utils.ThreadUtil;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.base.permission.PermissionActivity;
import com.yuhuankj.tmxq.widget.TitleBar;

import java.io.File;

public class FeedbackActivity extends BaseActivity {
    /**
     * 基本权限管理
     */
    private final String[] BASIC_PERMISSIONS = new String[]{
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
    };
    private EditText edtContent;
    private EditText edtContact;
    private TitleBar titleBar;
    private Button btnCommit1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        initTitleBar("我要反馈");
        initView();
        initData();
        SetListener();
    }

    private void SetListener() {
        titleBar.setActionTextColor(getResources().getColor(R.color.text_1A1A1A));
        titleBar.addAction(new TitleBar.TextAction("提交") {
            @Override
            public void performAction(View view) {
                checkPermission(new PermissionActivity.CheckPermListener() {
                    @Override
                    public void superPermission() {
                        if (edtContent.getText().toString().length() > 0) {
                            getDialogManager().showProgressDialog(FeedbackActivity.this, "正在提交...");
                            ThreadUtil.runInThread(uploadLogRunnable);
                        }
                    }
                }, R.string.ask_storage, BASIC_PERMISSIONS);
            }
        });
    }

    public Runnable uploadLogRunnable = new Runnable() {
        @Override
        public void run() {
            LogHelper logHelper = new LogHelper();
            File zipFile = logHelper.getLogFile(FeedbackActivity.this);
            if (zipFile != null) {
                CoreManager.getCore(IFileCore.class).uploadPhoto(zipFile);
            } else {
                CoreManager.getCore(IHomeCore.class).commitFeedback(CoreManager.getCore(IAuthCore.class).getCurrentUid(),
                        edtContent.getText().toString(), edtContact.getText().toString(), "");
            }
        }
    };


    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUploadPhoto(String url) {
        CoreManager.getCore(IHomeCore.class).commitFeedback(CoreManager.getCore(IAuthCore.class).getCurrentUid(),
                edtContent.getText().toString(), edtContact.getText().toString(), url);
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUploadPhotoFail() {
        toast("操作失败，请检查网络");
        getDialogManager().dismissDialog();
    }

    private void initData() {

    }

    private void initView() {
        edtContent = (EditText) findViewById(R.id.edt_content);
        GrowingIO.getInstance().trackEditText(edtContent);
        edtContact = (EditText) findViewById(R.id.edt_contact);
        GrowingIO.getInstance().trackEditText(edtContact);
        titleBar = (TitleBar) findViewById(R.id.title_bar);

//        btnCommit1 = (Button)findViewById(R.id.btn_commit1);
    }

    @CoreEvent(coreClientClass = IHomeCoreClient.class)
    public void onCommitFeedback() {
        getDialogManager().dismissDialog();
        toast("提交成功");
        finish();

    }

    @CoreEvent(coreClientClass = IHomeCoreClient.class)
    public void onCommitFeedbackFail(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (uploadLogRunnable != null) {
            ThreadUtil.cancelThread(uploadLogRunnable);
        }
    }
}
