package com.yuhuankj.tmxq.ui.me.wallet.bills.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.yuhuankj.tmxq.base.fragment.BaseFragment;
import com.yuhuankj.tmxq.ui.me.wallet.bills.fragmemt.WithdrawBillsFragment;
import com.yuhuankj.tmxq.ui.me.wallet.bills.fragmemt.WithdrawRedFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>提现记录adapter  </p>
 * Created by Administrator on 2017/11/7.
 */
public class WithdrawAdapter extends FragmentPagerAdapter {
    private List<BaseFragment> mFragmentList;

    public WithdrawAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (mFragmentList == null) mFragmentList = new ArrayList<>();
        int size = mFragmentList.size();
        BaseFragment fragment = null;
        if (size > 0 && size > position) {
            fragment = mFragmentList.get(position);
        }
        if (position == 0) {
            if (fragment == null) {
                fragment = new WithdrawBillsFragment();
            }
        } else {
            if (fragment == null)
                fragment = new WithdrawRedFragment();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
