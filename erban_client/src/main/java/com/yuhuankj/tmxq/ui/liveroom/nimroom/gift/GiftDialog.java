package com.yuhuankj.tmxq.ui.liveroom.nimroom.gift;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.view.ContextThemeWrapper;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.ButtonUtils;
import com.tongdaxing.erban.libcommon.utils.JavaUtil;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftType;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.gift.IGiftCoreClient;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCore;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMember;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomQueueInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.pay.IPayCore;
import com.tongdaxing.xchat_core.pay.IPayCoreClient;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.liveroom.imroom.gift.listener.OnSendGiftCountListener;
import com.yuhuankj.tmxq.ui.liveroom.imroom.gift.widget.SendGiftCountWindow;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;
import com.yuhuankj.tmxq.utils.Utils;
import com.yuhuankj.tmxq.widget.PageIndicatorView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * @author chenran
 * @date 2017/7/27
 */

public class GiftDialog extends BottomSheetDialog implements View.OnClickListener,
        GiftAvatarAdapter.OnItemSelectedListener, GiftGridPageAdapter.OnItemClickListener, OnSendGiftCountListener {

    private static final String TAG = GiftDialog.class.getSimpleName();
    public static final int ROWS = 2;
    public static final int COLUMNS = 4;
    private Context context;
    private RecyclerView gridView;
    private RecyclerView rv_micList;
    private GiftAvatarAdapter avatarListAdapter;
    private GiftGridPageAdapter adapter;
    private GiftInfo current;
    private List<GiftInfo> giftInfoList;
    private OnGiftDialogBtnClickListener giftDialogBtnClickListener;
    private TextView goldText;
    //    private PopupWindow giftNumberEasyPopup;
    private SendGiftCountWindow giftNumberEasyPopup;
    private TextView giftNumberText;
    private int giftNumber = 1;
    private long uid;

    private long roomId = 0L;

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    private String nick;
    private String avatar;
    private int vipId = 0;
    private int vipDate = 0;
    private boolean isInvisible = false;


    private List<MicMemberInfo> micMemberInfos;
    private MicMemberInfo defaultMicMemberInfo;
    private View giftNumLayout;
    private RelativeLayout giftAvatarListLayout;
    private PageIndicatorView giftIndicator;
    private TextView userInfoText;
    private View ctv_info;
    private View ll_giftContainer;
    private boolean isFullScreen = true;
    private boolean justRoomOwner = false;

    private TextView tv_giftTypePkg;
    private TextView tv_giftTypeNormal;
    private TextView tv_giftTypeBox;
    private TextView tv_giftTypeExc;
    private TextView tv_giftListEmpty;

    private TextView tvBosomFriend;

    private TextView btn_send;
    private TextView tv_allMic;
    private TextView tv_sendTo;
    private TextView btn_recharge;
    private ImageView iv_allMicSelected;

    private View flOpenNoble;
    private TextView tvDouziAmount;


    private GiftType currGiftType = GiftType.Unknow;

    private int roomType = RoomInfo.ROOMTYPE_HOME_PARTY;

    public GiftDialog(Context context, SparseArray<RoomQueueInfo> mMicQueueMemberMap, ChatRoomMember chatRoomMember) {
        super(context, R.style.ErbanBottomSheetDialog);
        this.context = context;
        List<MicMemberInfo> micMemberInfos = getMicMemberInfos(mMicQueueMemberMap, chatRoomMember);
        this.micMemberInfos = micMemberInfos;
    }


    public void setDetonatingBoxGiftInfo(GiftInfo detonatingBoxGiftInfo) {
        this.detonatingBoxGiftInfo = detonatingBoxGiftInfo;
        if (isShowing() && null != detonatingBoxGiftInfo && detonatingBoxGiftInfo.getGiftType() == GiftType.Exclusive.ordinal()) {
            showGiftList(GiftType.Exclusive, detonatingBoxGiftInfo);
            changeGiftTagSelectStatus(GiftType.Exclusive);
        }
    }

    public void showGiftListBy(GiftType type) {
        showGiftList(type, detonatingBoxGiftInfo);
        changeGiftTagSelectStatus(type);
    }

    private GiftInfo detonatingBoxGiftInfo;

    public void setGiftDialogBtnClickListener(OnGiftDialogBtnClickListener giftDialogBtnClickListener) {
        this.giftDialogBtnClickListener = giftDialogBtnClickListener;
    }

    /**
     * 私聊送礼
     * 暂时不展示贵族神秘人身份
     *
     * @param context
     * @param uid
     * @param isFullScreen
     */
    public GiftDialog(Context context, long uid, boolean isFullScreen) {
        super(context, R.style.ErbanBottomSheetDialog);
        this.isFullScreen = isFullScreen;
        this.context = context;
        this.uid = uid;
    }

    public GiftDialog(Context context, long uid, String nick, String avatar, int vipId, int vipDate, boolean isInvisible) {
        super(context, R.style.ErbanBottomSheetDialog);
        this.context = context;
        this.uid = uid;
        this.nick = nick;
        this.avatar = avatar;
        this.vipId = vipId;
        this.vipDate = vipDate;
        this.isInvisible = isInvisible;
    }

    public GiftDialog(Context context, SparseArray<IMRoomQueueInfo> mMicQueueMemberMap, long uid) {
        super(context, R.style.ErbanBottomSheetDialog);
        this.context = context;
        this.uid = uid;
        List<MicMemberInfo> micMemberInfos = getMicMemberInfos(mMicQueueMemberMap);
        this.micMemberInfos = micMemberInfos;
    }

    public GiftDialog(Context context, SparseArray<IMRoomQueueInfo> mMicQueueMemberMap, IMRoomMember chatRoomMember) {
        super(context, R.style.ErbanBottomSheetDialog);
        this.context = context;
        List<MicMemberInfo> micMemberInfos = getMicMemberInfos(mMicQueueMemberMap, chatRoomMember);
        this.micMemberInfos = micMemberInfos;
    }

    public void setRoomType(int roomType) {
        this.roomType = roomType;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CoreManager.addClient(this);
        setCanceledOnTouchOutside(true);
        setContentView(R.layout.dialog_bottom_gift);
        View view = findViewById(R.id.ll_dialog_bottom_gift);
        if (null != view) {
            init(view);
            FrameLayout bottomSheet = findViewById(android.support.design.R.id.design_bottom_sheet);
            if (bottomSheet != null) {
                BottomSheetBehavior.from(bottomSheet).setSkipCollapsed(false);
                BottomSheetBehavior.from(bottomSheet).setPeekHeight(
                        (int) context.getResources().getDimension(R.dimen.dialog_new_gift_height_max) +
                                (Utils.hasSoftKeys(context) ? Utils.getNavigationBarHeight(context) : 0));
            }
            WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            if (null != windowManager) {
                Display d = windowManager.getDefaultDisplay();
                DisplayMetrics realDisplayMetrics = new DisplayMetrics();
                d.getRealMetrics(realDisplayMetrics);
                Window window = getWindow();
                if (null != window) {
                    WindowManager.LayoutParams params = window.getAttributes();
                    params.width = WindowManager.LayoutParams.MATCH_PARENT;
                    params.height = WindowManager.LayoutParams.MATCH_PARENT;
                    getWindow().setAttributes(params);
                }

            }
        }
    }

    private void init(View root) {
        btn_recharge = root.findViewById(R.id.btn_recharge);
        btn_recharge.setOnClickListener(this);
        btn_send = root.findViewById(R.id.btn_send);
        btn_send.setOnClickListener(this);
        giftNumLayout = root.findViewById(R.id.gift_number_layout);
        giftNumLayout.setOnClickListener(this);

        tv_giftTypePkg = root.findViewById(R.id.tv_giftTypePkg);
        tv_giftTypeNormal = root.findViewById(R.id.tv_giftTypeNormal);
        tv_giftTypeBox = root.findViewById(R.id.tv_giftTypeBox);
        tv_giftListEmpty = root.findViewById(R.id.tv_giftListEmpty);
        tv_giftTypeExc = root.findViewById(R.id.tv_giftTypeExc);
        tv_giftTypePkg.setOnClickListener(this);
        tv_giftTypeNormal.setOnClickListener(this);
        tv_giftTypeBox.setOnClickListener(this);
        tv_giftTypeExc.setOnClickListener(this);

        tvBosomFriend = root.findViewById(R.id.tvBosomFriend);
        tvBosomFriend.setOnClickListener(this);

        gridView = root.findViewById(R.id.gridView);
        giftIndicator = root.findViewById(R.id.gift_layout_indicator);
        PagerSnapHelper pagerSnapHelper = new PagerSnapHelper();
        pagerSnapHelper.attachToRecyclerView(gridView);
        gridView.setLayoutManager(new RecyclerViewNoBugLinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        adapter = new GiftGridPageAdapter(getContext());
        adapter.setOnItemClickListener(this);
        gridView.addOnScrollListener(onScrollListener);
        goldText = root.findViewById(R.id.text_gold);
        tvDouziAmount = root.findViewById(R.id.tvDouziAmount);
        giftNumberText = root.findViewById(R.id.gift_number_text);
        userInfoText = root.findViewById(R.id.gift_dialog_info_text);
        userInfoText.setOnClickListener(this);
        ctv_info = root.findViewById(R.id.ctv_info);
        ll_giftContainer = root.findViewById(R.id.ll_giftContainer);
        ctv_info.setOnClickListener(this);
        //这里需要判断是否需要展示
        if (null != detonatingBoxGiftInfo && detonatingBoxGiftInfo.getGiftType() == GiftType.Exclusive.ordinal()) {
            showGiftList(GiftType.Exclusive, detonatingBoxGiftInfo);
            changeGiftTagSelectStatus(GiftType.Exclusive);
        } else {
            showGiftList(GiftType.Normal, null);
            changeGiftTagSelectStatus(GiftType.Normal);
        }

        WalletInfo walletInfo = CoreManager.getCore(IPayCore.class).getCurrentWalletInfo();
        if (walletInfo != null) {
            goldText.setText(String.format("%d", (int) walletInfo.getGoldNum()));
            tvDouziAmount.setText(String.format("%d", walletInfo.getPeaNum()));
        }

        CoreManager.getCore(IPayCore.class).getWalletInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        initEasyPop();

        if (this.uid > 0) {
            if (micMemberInfos == null) {
                micMemberInfos = new ArrayList<>();
            }
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(this.uid, false);
            if (userInfo == null) {
                userInfo = new UserInfo();
            }
            defaultMicMemberInfo = new MicMemberInfo();
            defaultMicMemberInfo.setAvatar(userInfo.getAvatar());
            defaultMicMemberInfo.setNick(userInfo.getNick());
            defaultMicMemberInfo.setUid(this.uid);
            defaultMicMemberInfo.setVipId(vipId);
            defaultMicMemberInfo.setVipDate(vipDate);
            defaultMicMemberInfo.setInvisible(isInvisible);
            defaultMicMemberInfo.setMicPosition(-2);
            if (roomType == RoomInfo.ROOMTYPE_HOME_PARTY && null != AvRoomDataManager.get().mMicQueueMemberMap) {
                for (int i = 0; i < AvRoomDataManager.get().mMicQueueMemberMap.size(); i++) {
                    RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().mMicQueueMemberMap.valueAt(i);
                    if (roomQueueInfo.mChatRoomMember != null && !TextUtils.isEmpty(roomQueueInfo.mChatRoomMember.getAccount())
                            && roomQueueInfo.mChatRoomMember.getAccount().equals(uid + "") && null != roomQueueInfo.mRoomMicInfo) {
                        defaultMicMemberInfo.setMicPosition(roomQueueInfo.mRoomMicInfo.getPosition());
                    }
                }
            }
            if (roomType == RoomInfo.ROOMTYPE_SINGLE_AUDIO && null != RoomDataManager.get().mMicQueueMemberMap) {
                for (int i = 0; i < RoomDataManager.get().mMicQueueMemberMap.size(); i++) {
                    IMRoomQueueInfo roomQueueInfo = RoomDataManager.get().mMicQueueMemberMap.valueAt(i);
                    if (roomQueueInfo.mChatRoomMember != null && !TextUtils.isEmpty(roomQueueInfo.mChatRoomMember.getAccount())
                            && roomQueueInfo.mChatRoomMember.getAccount().equals(uid + "") && null != roomQueueInfo.mRoomMicInfo) {
                        defaultMicMemberInfo.setMicPosition(roomQueueInfo.mRoomMicInfo.getPosition());
                    }
                }
            }

            micMemberInfos.add(defaultMicMemberInfo);
        }
        iv_allMicSelected = root.findViewById(R.id.iv_allMicSelected);
        iv_allMicSelected.setVisibility(View.GONE);
        iv_allMicSelected.setOnClickListener(this);
        tv_sendTo = root.findViewById(R.id.tv_sendTo);
        tv_sendTo.setOnClickListener(this);
        tv_allMic = root.findViewById(R.id.tv_allMic);
        tv_allMic.setOnClickListener(this);

        RoomInfo cRoomInfo = BaseRoomServiceScheduler.getCurrentRoomInfo();
        if (null != cRoomInfo && cRoomInfo.getType() != RoomInfo.ROOMTYPE_HOME_PARTY) {
            tv_allMic.setVisibility(View.GONE);
            tv_sendTo.setVisibility(View.GONE);
        }

        rv_micList = root.findViewById(R.id.rv_micList);
        rv_micList.setLayoutManager(new RecyclerViewNoBugLinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        if (micMemberInfos != null && micMemberInfos.size() > 0) {
            avatarListAdapter = new GiftAvatarAdapter(getContext());
            avatarListAdapter.justRoomOwner = this.justRoomOwner;
            avatarListAdapter.setMicMemberInfos(micMemberInfos);
            avatarListAdapter.setOnItemSelectedListener(this);
            rv_micList.setAdapter(avatarListAdapter);
            if (defaultMicMemberInfo != null) {
                int position = micMemberInfos.indexOf(defaultMicMemberInfo);
                if (position >= 0) {
                    avatarListAdapter.setSelectIndex(position);
                    updateAllMicChooseStatus(micMemberInfos.size() == 1 && position == 0);
                } else {
                    Log.e(TAG, "init: default mic member info not in mic member info list");
                }
            } else {
                avatarListAdapter.setSelectIndex(0);
                updateAllMicChooseStatus(micMemberInfos.size() == 1);
            }
        } else {
            userInfoText.setVisibility(View.GONE);
            ctv_info.setVisibility(View.INVISIBLE);
            ll_giftContainer.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_d918121e_top_6));
        }

        if (null != micMemberInfos && micMemberInfos.size() == 1 && micMemberInfos.get(0).getMicPosition() == -2) {
            tv_allMic.setVisibility(View.GONE);
            iv_allMicSelected.setVisibility(View.GONE);
        }

//        UserInfo selfUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        flOpenNoble = root.findViewById(R.id.flOpenNoble);
//        if (null != selfUserInfo && selfUserInfo.getVipId() > 0 && selfUserInfo.getVipDate() > 0) {
//            flOpenNoble.setVisibility(View.GONE);
//        } else {
        //为了方便用户升级或者续费，入口保持常开
//        flOpenNoble.setVisibility(View.VISIBLE);
//        }
//        flOpenNoble.setOnClickListener(this);
    }

    private void updateAllMicChooseStatus(boolean isAllMic) {
        RoomInfo cRoomInfo = BaseRoomServiceScheduler.getCurrentRoomInfo();
        if (null != cRoomInfo && cRoomInfo.getType() != RoomInfo.ROOMTYPE_HOME_PARTY) {
            iv_allMicSelected.setVisibility(View.GONE);
        } else {
            iv_allMicSelected.setVisibility(isAllMic ? View.VISIBLE : View.GONE);
        }

        tv_allMic.setBackground(context.getResources().getDrawable(
                isAllMic ? R.drawable.bg_room_gift_all_mic : R.drawable.bg_room_gift_none_to_send));
    }

    @NonNull
    private List<MicMemberInfo> getMicMemberInfos(SparseArray<RoomQueueInfo> mMicQueueMemberMap,
                                                  ChatRoomMember chatRoomMember) {
        List<MicMemberInfo> micMemberInfos = new ArrayList<>();
        if (roomType != RoomInfo.ROOMTYPE_HOME_PARTY) {
            return micMemberInfos;
        }

        //如果不包含房主信息，则手动添加房主信息到map
        if (!checkHasOwner(mMicQueueMemberMap)) {
            UserInfo roomOwner = CoreManager.getCore(IAVRoomCore.class).getRoomOwner();
            MicMemberInfo micMemberInfo = new MicMemberInfo();
            micMemberInfo.setRoomOwnner(true);
            micMemberInfo.setNick(roomOwner.getNick());
            micMemberInfo.setAvatar(roomOwner.getAvatar());
            micMemberInfo.setMicPosition(-1);
            micMemberInfo.setUid(roomOwner.getUid());
            micMemberInfo.setVipId(roomOwner.getVipId());
            micMemberInfo.setVipDate(roomOwner.getVipDate());
            micMemberInfo.setInvisible(roomOwner.getIsInvisible());
            micMemberInfos.add(micMemberInfo);
        }

        for (int i = 0; i < mMicQueueMemberMap.size(); i++) {
            MicMemberInfo micMemberInfo = new MicMemberInfo();
            micMemberInfo.setMicPosition(-2);

            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.get(mMicQueueMemberMap.keyAt(i));
            if (null == roomQueueInfo) {
                continue;
            }

            ChatRoomMember mChatRoomMember = roomQueueInfo.mChatRoomMember;
            if (mChatRoomMember == null) {
                continue;
            }

            // 合法判断
            String account = mChatRoomMember.getAccount();

            if (TextUtils.isEmpty(account) ||
                    TextUtils.isEmpty(mChatRoomMember.getNick()) ||
                    TextUtils.isEmpty(mChatRoomMember.getAvatar())) {
                continue;
            }

            // 排除自己
            if (AvRoomDataManager.get().isOwner(account)) {
                continue;
            }

            // 设置默认人员
            if (chatRoomMember != null && chatRoomMember.getAccount().equals(account)) {
                if (null != roomQueueInfo.mRoomMicInfo) {
                    micMemberInfo.setMicPosition(roomQueueInfo.mRoomMicInfo.getPosition());
                }

                Map<String, Object> extenMap = chatRoomMember.getExtension();
                if (null != extenMap) {
                    if (extenMap.containsKey(Constants.USER_MEDAL_ID)) {
                        vipId = (int) extenMap.get(Constants.USER_MEDAL_ID);
                    }
                    if (extenMap.containsKey(Constants.USER_MEDAL_DATE)) {
                        vipDate = (int) extenMap.get(Constants.USER_MEDAL_DATE);
                    }
                    if (extenMap.containsKey(Constants.NOBLE_INVISIABLE_ENTER_ROOM)) {
                        isInvisible = (int) extenMap.get(Constants.NOBLE_INVISIABLE_ENTER_ROOM) == 1;
                    }
                }
                micMemberInfo.setVipId(vipId);
                micMemberInfo.setVipDate(vipDate);
                micMemberInfo.setInvisible(isInvisible);

                this.defaultMicMemberInfo = micMemberInfo;
            }
            // 设置房主
            if (AvRoomDataManager.get().isRoomOwner(account)) {
                micMemberInfo.setRoomOwnner(true);
            }
            micMemberInfo.setNick(mChatRoomMember.getNick());
            micMemberInfo.setAvatar(mChatRoomMember.getAvatar());
            micMemberInfo.setMicPosition(mMicQueueMemberMap.keyAt(i));
            micMemberInfo.setUid(JavaUtil.str2long(account));


            micMemberInfos.add(micMemberInfo);
        }

        return micMemberInfos;
    }

    @NonNull
    private List<MicMemberInfo> getMicMemberInfos(SparseArray<IMRoomQueueInfo> mMicQueueMemberMap, IMRoomMember imRoomMember) {
        List<MicMemberInfo> micMemberInfos = new ArrayList<>();
        if (roomType == RoomInfo.ROOMTYPE_HOME_PARTY) {
            return micMemberInfos;
        }
        //如果不包含房主信息，则手动添加房主信息到map
        if (!ownerCheck(mMicQueueMemberMap)) {
            UserInfo roomOwner = CoreManager.getCore(IAVRoomCore.class).getRoomOwner();
            MicMemberInfo micMemberInfo = new MicMemberInfo();
            micMemberInfo.setRoomOwnner(true);
            micMemberInfo.setNick(roomOwner.getNick());
            micMemberInfo.setAvatar(roomOwner.getAvatar());
            micMemberInfo.setMicPosition(-1);
            micMemberInfo.setUid(roomOwner.getUid());
            micMemberInfo.setVipId(roomOwner.getVipId());
            micMemberInfo.setVipDate(roomOwner.getVipDate());
            micMemberInfo.setInvisible(roomOwner.getIsInvisible());
            micMemberInfos.add(micMemberInfo);
        }

        for (int i = 0; i < mMicQueueMemberMap.size(); i++) {
            MicMemberInfo micMemberInfo = new MicMemberInfo();
            micMemberInfo.setMicPosition(-2);

            IMRoomQueueInfo roomQueueInfo = mMicQueueMemberMap.get(mMicQueueMemberMap.keyAt(i));
            if (null == roomQueueInfo) {
                continue;
            }

            IMRoomMember mChatRoomMember = roomQueueInfo.mChatRoomMember;
            if (mChatRoomMember == null) {
                continue;
            }

            // 合法判断
            String account = mChatRoomMember.getAccount();

            if (TextUtils.isEmpty(account) ||
                    TextUtils.isEmpty(mChatRoomMember.getNick()) ||
                    TextUtils.isEmpty(mChatRoomMember.getAvatar())) {
                continue;
            }

            // 排除自己
            if (RoomDataManager.get().isUserSelf(account)) {
                continue;
            }

            // 设置默认人员
            if (imRoomMember != null && imRoomMember.getAccount().equals(account)) {
                if (null != roomQueueInfo.mRoomMicInfo) {
                    micMemberInfo.setMicPosition(roomQueueInfo.mRoomMicInfo.getPosition());
                }
                this.defaultMicMemberInfo = micMemberInfo;
            }
            // 设置房主
            if (RoomDataManager.get().isRoomOwner(account)) {
                micMemberInfo.setRoomOwnner(true);
            }
            micMemberInfo.setNick(mChatRoomMember.getNick());
            micMemberInfo.setAvatar(mChatRoomMember.getAvatar());
            micMemberInfo.setMicPosition(mMicQueueMemberMap.keyAt(i));
            micMemberInfo.setUid(JavaUtil.str2long(account));


            micMemberInfos.add(micMemberInfo);
        }

        return micMemberInfos;
    }

    @NonNull
    private List<MicMemberInfo> getMicMemberInfos(SparseArray<IMRoomQueueInfo> mMicQueueMemberMap) {
        List<MicMemberInfo> micMemberInfos = new ArrayList<>();
        //如果不包含房主信息，则手动添加房主信息到map
        if (!ownerCheck(mMicQueueMemberMap)) {
            UserInfo roomOwner = CoreManager.getCore(IAVRoomCore.class).getRoomOwner();
            MicMemberInfo micMemberInfo = new MicMemberInfo();
            micMemberInfo.setRoomOwnner(true);
            micMemberInfo.setNick(roomOwner.getNick());
            micMemberInfo.setAvatar(roomOwner.getAvatar());
            micMemberInfo.setMicPosition(-1);
            micMemberInfo.setUid(roomOwner.getUid());
            micMemberInfo.setVipId(roomOwner.getVipId());
            micMemberInfo.setVipDate(roomOwner.getVipDate());
            micMemberInfo.setInvisible(roomOwner.getIsInvisible());
            micMemberInfos.add(micMemberInfo);
        }

        for (int i = 0; i < mMicQueueMemberMap.size(); i++) {
            MicMemberInfo micMemberInfo = new MicMemberInfo();
            micMemberInfo.setMicPosition(-2);

            IMRoomQueueInfo roomQueueInfo = mMicQueueMemberMap.get(mMicQueueMemberMap.keyAt(i));
            if (null == roomQueueInfo) {
                continue;
            }

            IMRoomMember imRoomMember = roomQueueInfo.mChatRoomMember;
            if (imRoomMember == null) {
                continue;
            }

            // 合法判断
            String account = imRoomMember.getAccount();

            if (TextUtils.isEmpty(account) ||
                    TextUtils.isEmpty(imRoomMember.getNick()) ||
                    TextUtils.isEmpty(imRoomMember.getAvatar())) {
                continue;
            }

            // 排除自己
            if (RoomDataManager.get().isUserSelf(account)) {
                continue;
            }

            // 设置默认人员
            if (imRoomMember.getAccount().equals(account)) {
                if (null != roomQueueInfo.mRoomMicInfo) {
                    micMemberInfo.setMicPosition(roomQueueInfo.mRoomMicInfo.getPosition());
                }
                this.defaultMicMemberInfo = micMemberInfo;
            }
            // 设置房主
            if (RoomDataManager.get().isRoomOwner(account)) {
                micMemberInfo.setRoomOwnner(true);
            }
            micMemberInfo.setNick(imRoomMember.getNick());
            micMemberInfo.setAvatar(imRoomMember.getAvatar());
            micMemberInfo.setMicPosition(mMicQueueMemberMap.keyAt(i));
            micMemberInfo.setUid(JavaUtil.str2long(account));
            micMemberInfos.add(micMemberInfo);
        }

        return micMemberInfos;
    }

    /**
     * 判断是否包含房主
     *
     * @param mMicQueueMemberMap
     * @return
     */
    private boolean checkHasOwner(SparseArray<RoomQueueInfo> mMicQueueMemberMap) {
        UserInfo roomOwner = CoreManager.getCore(IAVRoomCore.class).getRoomOwner();

        if (roomOwner == null) {
            return true;
        }

        for (int i = 0; i < mMicQueueMemberMap.size(); i++) {
            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.get(mMicQueueMemberMap.keyAt(i));
            if (roomQueueInfo != null && roomQueueInfo.mChatRoomMember != null) {
                String account = roomQueueInfo.mChatRoomMember.getAccount();
                if ((roomOwner.getUid() + "").equals(account)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 判断是否包含房主
     *
     * @param mMicQueueMemberMap
     * @return
     */
    private boolean ownerCheck(SparseArray<IMRoomQueueInfo> mMicQueueMemberMap) {
        UserInfo roomOwner = CoreManager.getCore(IAVRoomCore.class).getRoomOwner();

        if (roomOwner == null) {
            return true;
        }

        for (int i = 0; i < mMicQueueMemberMap.size(); i++) {
            IMRoomQueueInfo roomQueueInfo = mMicQueueMemberMap.get(mMicQueueMemberMap.keyAt(i));
            if (roomQueueInfo != null && roomQueueInfo.mChatRoomMember != null) {
                String account = roomQueueInfo.mChatRoomMember.getAccount();
                if ((roomOwner.getUid() + "").equals(account)) {
                    return true;
                }
            }
        }
        return false;
    }

    private void changeGiftTagSelectStatus(GiftType giftType) {
        int color = context.getResources().getColor(R.color.room_tip_color);
        int colorNormal = Color.parseColor("#777878");
        tv_giftTypePkg.setTextColor(giftType == GiftType.Package ? color : colorNormal);
        tv_giftTypeNormal.setTextColor(giftType == GiftType.Normal ? color : colorNormal);
        tv_giftTypeBox.setTextColor(giftType == GiftType.Box ? color : colorNormal);
        tv_giftTypeExc.setTextColor(giftType == GiftType.Exclusive ? color : colorNormal);
        tvBosomFriend.setTextColor(giftType == GiftType.BosomFriend ? color : colorNormal);

        tv_giftTypePkg.setTextSize(giftType == GiftType.Package ? 13 : 12);
        tv_giftTypeNormal.setTextSize(giftType == GiftType.Normal ? 13 : 12);
        tv_giftTypeBox.setTextSize(giftType == GiftType.Box ? 13 : 12);
        tv_giftTypeExc.setTextSize(giftType == GiftType.Exclusive ? 13 : 12);
        tvBosomFriend.setTextSize(giftType == GiftType.BosomFriend ? 13 : 12);
    }


    public void setRoomToShowLimitedExempteGift(boolean roomToShowLimitedExempteGift) {
        isRoomToShowLimitedExempteGift = roomToShowLimitedExempteGift;
        LogUtils.d(TAG, "setRoomToShowLimitedExempteGift-isRoomToShowLimitedExempteGift:" + isRoomToShowLimitedExempteGift);
    }

    /**
     * 是否房间内可以显示限免礼物标识
     */
    private boolean isRoomToShowLimitedExempteGift = false;

    private void showGiftList(GiftType giftType, GiftInfo detonatingGiftInfo) {
        currGiftType = giftType;
        //获取普通类型礼物
        giftInfoList = CoreManager.getCore(IGiftCore.class).getGiftInfosByType(giftType, isRoomToShowLimitedExempteGift);
        boolean hasGiftData = giftInfoList != null && giftInfoList.size() > 0;
        tv_giftListEmpty.setVisibility(hasGiftData ? View.GONE : View.VISIBLE);
        if (hasGiftData) {
            adapter.setGiftInfoList(giftInfoList);
            gridView.setAdapter(adapter);
//            PagerGridLayoutManager pagerGridLayoutManager = new PagerGridLayoutManager(ROWS, COLUMNS, PagerGridLayoutManager.HORIZONTAL);
//            pagerGridLayoutManager.setPageListener(this);
//            gridView.setLayoutManager(pagerGridLayoutManager);
            int numPerPage = ROWS * COLUMNS;
            giftIndicator.initIndicator((int) Math.ceil((float) giftInfoList.size() / (numPerPage)));
            if (null != detonatingGiftInfo) {
                //拿到的是list中detonateGift总的索引
                int index = giftInfoList.indexOf(detonatingGiftInfo);
                if (index >= 0) {
                    //假设每页4个，index从0+1=1开始算起，index
                    // index=5,那么实际为第6个，那么temp=2，itemIndex=1
                    // index=3,那么实际为第4个，那么temp=0，itemIndex=3
                    int temp = (index + 1) % numPerPage;
                    int pageIndex = ((index + 1) / numPerPage + temp > 0 ? 1 : 0) - 1;
                    int itemIndex = temp > 0 ? temp - 1 : numPerPage - 1;
                    current = giftInfoList.get(index);
                    giftIndicator.setSelectedPage(pageIndex);
                    if (adapter.getmDatas() != null && adapter.getmDatas().size() > itemIndex) {
                        adapter.setIndex(itemIndex);
                    } else {
                        adapter.setIndex(0);
                    }
                    return;
                }
            } else {
                current = giftInfoList.get(0);
                adapter.setIndex(0);
            }
        } else {
//            adapter = new GiftAdapter(getContext());
//            adapter = new GiftGridPageAdapter(getContext());
            adapter.setGiftInfoList(new ArrayList<>());
            gridView.setAdapter(adapter);
            if (currGiftType == GiftType.Package) {
                tv_giftListEmpty.setText(context.getResources().getString(R.string.gift_list_empty_pkg));
            } else if (currGiftType == GiftType.Exclusive) {
                tv_giftListEmpty.setText(context.getResources().getString(R.string.gift_list_empty_exclusive));
            } else {
                tv_giftListEmpty.setText(null);
            }
            current = null;
        }
    }

    private void initEasyPop() {
//        giftNumberEasyPopup = new PopupWindow(getContext());
//        giftNumberEasyPopup.setContentView(LayoutInflater.from(getContext()).inflate(R.layout.dialog_gift_number,null));
//        giftNumberEasyPopup.setWidth(DisplayUtil.dip2px(getContext(),100));
//        giftNumberEasyPopup.setHeight(DisplayUtil.dip2px(getContext(),210));
//        giftNumberEasyPopup.getContentView().findViewById(R.id.number_1).setOnClickListener(this);
//        giftNumberEasyPopup.getContentView().findViewById(R.id.number_10).setOnClickListener(this);
//        giftNumberEasyPopup.getContentView().findViewById(R.id.number_38).setOnClickListener(this);
//        giftNumberEasyPopup.getContentView().findViewById(R.id.number_66).setOnClickListener(this);
//        giftNumberEasyPopup.getContentView().findViewById(R.id.number_188).setOnClickListener(this);
//        giftNumberEasyPopup.getContentView().findViewById(R.id.number_520).setOnClickListener(this);
//        giftNumberEasyPopup.getContentView().findViewById(R.id.number_1314).setOnClickListener(this);
        giftNumberEasyPopup = new SendGiftCountWindow(getContext());
        giftNumberEasyPopup.setOnSendGiftCountListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.flOpenNoble:
                if (ButtonUtils.isFastDoubleClick(v.getId())) {
                    return;
                }
                Map<String, String> maps = StatisticModel.getInstance().getUMAnalyCommonMap(context);
                maps.put("type", "gift");
                StatisticManager.get().onEvent(context,
                        StatisticModel.EVENT_ID_BECOME_NOBLE, maps);
                CommonWebViewActivity.start(context, UriProvider.getMyNobleUrl(), true, roomId);
                dismiss();
                break;
            case R.id.ctv_info:
                dismiss();
                break;
            case R.id.tv_sendTo:
            case R.id.tv_allMic:
            case R.id.iv_allMicSelected:
                if (null != avatarListAdapter && null != avatarListAdapter.getMicMemberInfos()
                        && avatarListAdapter.getMicMemberInfos().size() > 0) {
                    boolean hasAllMicSelected = null != avatarListAdapter.getSelectedMemberInfoMap()
                            && avatarListAdapter.getSelectedMemberInfoMap().size() == avatarListAdapter.getMicMemberInfos().size();
                    avatarListAdapter.updateAllMicSelectedStatus(!hasAllMicSelected);
                    updateAllMicChooseStatus(!hasAllMicSelected);
                }
                break;
            case R.id.btn_recharge:
                if (giftDialogBtnClickListener != null) {
                    giftDialogBtnClickListener.onRechargeBtnClick();
                }
                dismiss();
                if (null != current) {
                    //房间-点击送礼充值入口
                    StatisticManager.get().onEvent(context,
                            StatisticModel.EVENT_ID_ROOM_GIFT_RECHARGE,
                            StatisticModel.getInstance().getUMAnalyCommonMap(context));
                }

                break;
            case R.id.btn_send:
                List<GiftInfo> giftInfos = CoreManager.getCore(IGiftCore.class).getGiftInfosByType(currGiftType, isRoomToShowLimitedExempteGift);
                boolean hasGiftData = giftInfos != null && giftInfos.size() > 0;
                if (!hasGiftData || null == current) {
                    if (null != context) {
                        SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), context.getResources().getString(R.string.room_gift_send_no_gift_selected), Toast.LENGTH_SHORT);
                    }
                    return;
                }
                UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
                if (context != null && userInfo != null && current.getNobleId() > userInfo.getVipId()/* && !TextUtils.isEmpty(current.getNobleName())*/) {
                    //SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), current.getNobleName() + "以上贵族才可以赠送该礼物", Toast.LENGTH_SHORT);
                    showVipTipInfo(current, userInfo);
                    return;
                }
                if (giftDialogBtnClickListener != null) {
                    if (uid > 0) {
                        giftDialogBtnClickListener.onSendGiftBtnClick(current, uid, giftNumber);
                    } else if (avatarListAdapter != null && null != avatarListAdapter.getSelectedMemberInfoMap()
                            && avatarListAdapter.getSelectedMemberInfoMap().size() > 0) {
                        if (avatarListAdapter.getSelectedMemberInfoMap().size() == micMemberInfos.size() && micMemberInfos.size() > 1) {
                            giftDialogBtnClickListener.onSendGiftBtnClick(current, avatarListAdapter.getSelectedMemberInfoMap(), giftNumber);
                        } else {
                            for (MicMemberInfo micMemberInfo : avatarListAdapter.getSelectedMemberInfoMap()) {
                                giftDialogBtnClickListener.onSendGiftBtnClick(current, micMemberInfo.getUid(), giftNumber);
                            }
                        }
                    } else {
                        if (null != context) {
                            SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), context.getResources().getString(R.string.room_gift_send_no_member_selected), Toast.LENGTH_SHORT);
                        }
                        dismiss();
                    }
                }
                break;
            case R.id.number_1:
                updateNumber(1);
                break;
            case R.id.number_10:
                updateNumber(10);
                break;
            case R.id.number_38:
                updateNumber(38);
                break;
            case R.id.number_66:
                updateNumber(66);
                break;
            case R.id.number_188:
                updateNumber(188);
                break;
            case R.id.number_520:
                updateNumber(520);
                break;
            case R.id.number_1314:
                updateNumber(1314);
                break;
            case R.id.gift_number_layout:
                showGiftNumberEasyPopup();
                break;
            case R.id.gift_dialog_info_text:
                displayUserInfo();
                //房间-点击送礼资料入口
                StatisticManager.get().onEvent(v.getContext(),
                        StatisticModel.EVENT_ID_ROOM_GIFT_USERINFO,
                        StatisticModel.getInstance().getUMAnalyCommonMap(v.getContext()));
                break;
            case R.id.tv_giftTypePkg:
                showGiftList(GiftType.Package, null);
                changeGiftTagSelectStatus(GiftType.Package);
                break;
            case R.id.tv_giftTypeNormal:
                showGiftList(GiftType.Normal, null);
                changeGiftTagSelectStatus(GiftType.Normal);
                break;
            case R.id.tv_giftTypeBox:
                showGiftList(GiftType.Box, null);
                changeGiftTagSelectStatus(GiftType.Box);
                break;
            case R.id.tv_giftTypeExc:
                showGiftList(GiftType.Exclusive, null);
                changeGiftTagSelectStatus(GiftType.Exclusive);
                break;
            case R.id.tvBosomFriend:
                showGiftList(GiftType.BosomFriend, null);
                changeGiftTagSelectStatus(GiftType.BosomFriend);
                break;
            default:
                break;
        }
    }

    private void updateNumber(int number) {
        giftNumber = number;
        giftNumberText.setText(giftNumber + "");
        giftNumberEasyPopup.dismiss();
    }

    private void displayUserInfo() {
        int index = avatarListAdapter.getSelectIndex();
        if (index < 0) {
//            new UserInfoDialog(getContext(), this.uid).show();
            UserInfoActivity.start(getContext(), this.uid);
            return;
        }
//        new UserInfoDialog(getContext(), micMemberInfos.get(index).getUid()).show();
        UserInfoActivity.start(getContext(), micMemberInfos.get(index).getUid());
    }

    private void showGiftNumberEasyPopup() {
        if (!giftNumberEasyPopup.isShowing()) {
            giftNumberEasyPopup.showGiftCountAtAnchor(giftNumLayout);
        }
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        CoreManager.removeClient(this);
        detonatingBoxGiftInfo = null;
        if (gridView != null) {
            gridView.removeOnScrollListener(onScrollListener);
        }
    }


    @CoreEvent(coreClientClass = IPayCoreClient.class)
    public void onWalletInfoUpdate(WalletInfo walletInfo) {
        if (walletInfo != null) {
            goldText.setText(String.format("%d", (int) walletInfo.getGoldNum()));
            tvDouziAmount.setText(String.format("%d", walletInfo.getPeaNum()));
        }
    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void refreshFreeGift(int giftNum) {
        giftInfoList = CoreManager.getCore(IGiftCore.class).getGiftInfosByType(currGiftType, isRoomToShowLimitedExempteGift);
        boolean hasGiftData = giftInfoList != null && giftInfoList.size() > 0;
        if (currGiftType == GiftType.Package) {
            if (hasGiftData) {
                if (0 == giftNum) {
                    adapter.setIndex(0);
                    current = giftInfoList.get(0);
                }
            } else {
                tv_giftListEmpty.setText(context.getResources().getString(R.string.gift_list_empty_pkg));
                current = null;
            }
            tv_giftListEmpty.setVisibility(hasGiftData ? View.GONE : View.VISIBLE);
        }
        adapter.setGiftInfoList(hasGiftData ? giftInfoList : new ArrayList<>());
        adapter.notifyDataSetChanged();
    }


    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onGiftPastDue() {
        SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), "该礼物已过期", Toast.LENGTH_SHORT);
    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onGiftSendErrorMsg(String errorMsg) {
        LogUtils.d("onGiftSendErrorMsg", errorMsg + "");
        if (errorMsg != null) {
            SingleToastUtil.showToast(errorMsg);
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfo(UserInfo info) {

    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onSendPersonalGiftFailAuth(String msg) {
        Context context = getContext();
        if (context instanceof ContextThemeWrapper) {
            context = ((ContextThemeWrapper) context).getBaseContext();
        }
        CoreManager.notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_NEED_REAL_NAME_AUTH, context, msg);
    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onSendPersonalGiftFail(int code, String msg) {
        if (!TextUtils.isEmpty(msg)) {
            //无权限
            SingleToastUtil.showToast(msg);
        }
    }

    @Override
    public void onItemSelected(int position) {
        boolean isSendToAllMic = null != avatarListAdapter && null != avatarListAdapter.getMicMemberInfos()
                && null != avatarListAdapter.getSelectedMemberInfoMap()
                && avatarListAdapter.getMicMemberInfos().size() == avatarListAdapter.getSelectedMemberInfoMap().size();
        updateAllMicChooseStatus(isSendToAllMic);
    }

    @CoreEvent(coreClientClass = IPayCoreClient.class)
    public void onGetWalletInfo(WalletInfo walletInfo) {
        if (walletInfo != null) {
            CoreManager.getCore(IPayCore.class).setCurrentWalletInfo(walletInfo);
            goldText.setText(String.format("%d", (int) walletInfo.getGoldNum()));
            tvDouziAmount.setText(String.format("%d", walletInfo.getPeaNum()));
            //goldText.setText(getContext().getString(R.string.gold_num_text, walletInfo.getGoldNum()));
        }
    }

//    @Override
//    public void onPageSizeChanged(int pageSize) {
//        LogUtils.d(TAG, "onPageSizeChanged() called with: pageSize = [" + pageSize + "]");
//    }
//
//    @Override
//    public void onPageSelect(int pageIndex) {
//        giftIndicator.setSelectedPage(pageIndex);
//    }

    @Override
    public void onItemClick(GiftInfo giftInfo, int position) {
//        LogUtils.d(TAG, "onItemClick-position:" + position);
//        if (giftInfoList == null || position >= giftInfoList.size()) {
//            return;
//        }
//        current = giftInfoList.get(position);
        current = giftInfo;
    }

    @Override
    public void onSelectCount(int giftCount) {
        updateNumber(giftCount);
    }

    public interface OnGiftDialogBtnClickListener {
        void onRechargeBtnClick();

        void onSendGiftBtnClick(GiftInfo giftInfo, long uid, int number);

        void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, int number);
    }

    private ImageView imvNeedVip;
    private TextView tvNeedVipName;
    private TextView tvCurVipName;
    private View vTarget;
    private ImageView imvEnter;
    private View vClose;

    private void initSendGiftVipTip() {
        vTarget = LayoutInflater.from(context).inflate(R.layout.dialog_gift_send_viptip, null);
        imvNeedVip = vTarget.findViewById(R.id.imvNeedVip);
        tvNeedVipName = vTarget.findViewById(R.id.tvNeedVipName);
        tvCurVipName = vTarget.findViewById(R.id.tvCurVipName);
        imvEnter = vTarget.findViewById(R.id.imvEnter);
        vClose = vTarget.findViewById(R.id.vClose);
    }

    private void showVipTipInfo(GiftInfo giftInfo, UserInfo userInfo) {
        try {
            if (vTarget == null) {
                initSendGiftVipTip();
            }
            vTarget.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            imvEnter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Map<String, String> maps = StatisticModel.getInstance().getUMAnalyCommonMap(context);
                    maps.put("type", "gift");
                    StatisticManager.get().onEvent(context,
                            StatisticModel.EVENT_ID_BECOME_NOBLE, maps);
                    CommonWebViewActivity.start(context, UriProvider.getMyNobleUrl(), true, roomId);
                    removeVipTip();
                    dismiss();
                }
            });
            vClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeVipTip();
                }
            });
            if (TextUtils.isEmpty(userInfo.getVipName())) {
                tvCurVipName.setText("当前为平民");
            } else {
                tvCurVipName.setText("当前为" + userInfo.getVipName());
            }

            if (TextUtils.isEmpty(giftInfo.getNobleName())) {
                String vipName = "";
                if (giftInfo.getNobleId() == 1) {
                    vipName = "骑土";
                } else if (giftInfo.getNobleId() == 2) {
                    vipName = "男爵";
                } else if (giftInfo.getNobleId() == 3) {
                    vipName = "子爵";
                } else if (giftInfo.getNobleId() == 4) {
                    vipName = "伯爵";
                } else if (giftInfo.getNobleId() == 5) {
                    vipName = "侯爵";
                } else if (giftInfo.getNobleId() == 6) {
                    vipName = "公爵";
                } else if (giftInfo.getNobleId() == 7) {
                    vipName = "国王";
                }
                tvNeedVipName.setText("需要开通" + vipName);
            } else {
                tvNeedVipName.setText("需要开通" + giftInfo.getNobleName());
            }

            int drawableId = context.getResources().getIdentifier("ic_order_medal_" + giftInfo.getNobleId(), "mipmap", context.getPackageName());
            imvNeedVip.setImageResource(drawableId);

            final ViewGroup decorView = (ViewGroup) getWindow().getDecorView();
            if (vTarget.getParent() != null) {
                ((ViewGroup) vTarget.getParent()).removeView(vTarget);
            }
            decorView.addView(vTarget);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void removeVipTip() {
        if (vTarget.getParent() != null) {
            ((ViewGroup) vTarget.getParent()).removeView(vTarget);
        }
    }

    private RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                LinearLayoutManager l = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (l != null) {
                    int adapterNowPos = l.findFirstVisibleItemPosition();
                    giftIndicator.setSelectedPage(adapterNowPos);
                }
            }
        }

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
        }
    };
}
