package com.yuhuankj.tmxq.ui.liveroom.imroom.room.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

public interface IRoomAdmireView extends IMvpBaseView {

    void updateRoomAdmireStatus();
}
