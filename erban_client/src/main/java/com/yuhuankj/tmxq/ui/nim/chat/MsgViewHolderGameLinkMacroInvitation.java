package com.yuhuankj.tmxq.ui.nim.chat;

import android.view.View;

import com.netease.nim.uikit.common.ui.recyclerview.adapter.BaseMultiItemFetchLoadAdapter;
import com.netease.nim.uikit.session.viewholder.MsgViewHolderBase;

/**
 * @author liaoxy
 * @Description:游戏连麦邀请
 * @date 2019/2/15 11:21
 */
public class MsgViewHolderGameLinkMacroInvitation extends MsgViewHolderBase {

    public MsgViewHolderGameLinkMacroInvitation(BaseMultiItemFetchLoadAdapter adapter) {
        super(adapter);
    }

    @Override
    protected int getContentResId() {
        return com.netease.nim.uikit.R.layout.view_minigame_msg_gone;
    }

    @Override
    protected void inflateContentView() {
    }

    @Override
    protected boolean isShowHeadImage() {
        return false;
    }

    @Override
    protected boolean isShowBubble() {
        return false;
    }

    @Override
    protected void bindContentView() {
        //不显示整个View
        view.setVisibility(View.GONE);
    }
}
