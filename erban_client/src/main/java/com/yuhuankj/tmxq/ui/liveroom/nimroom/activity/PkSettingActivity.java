package com.yuhuankj.tmxq.ui.liveroom.nimroom.activity;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.growingio.android.sdk.collection.GrowingIO;
import com.makeramen.roundedimageview.RoundedImageView;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.pk.IPKCoreClient;
import com.tongdaxing.xchat_core.pk.IPkCore;
import com.tongdaxing.xchat_core.pk.bean.PkVoteInfo;
import com.tongdaxing.xchat_core.room.bean.OnlineChatMember;
import com.tongdaxing.xchat_core.room.presenter.HomePartyUserListPresenter;
import com.tongdaxing.xchat_core.room.view.IHomePartyUserListView;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.adapter.PkUserAdapter;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.widget.ButtonItemFactory;
import com.yuhuankj.tmxq.widget.TitleBar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

@CreatePresenter(HomePartyUserListPresenter.class)
public class PkSettingActivity extends BaseMvpActivity<IHomePartyUserListView, HomePartyUserListPresenter>
        implements IHomePartyUserListView, View.OnClickListener {
    @BindView(R.id.title_bar)
    TitleBar titleBar;
    @BindView(R.id.riv_pk_user_icon_1)
    RoundedImageView rivPkUserIcon1;
    @BindView(R.id.tv_pk_user_name_1)
    TextView tvPkUserName1;
    @BindView(R.id.riv_pk_user_icon_2)
    RoundedImageView rivPkUserIcon2;
    @BindView(R.id.tv_pk_user_name_2)
    TextView tvPkUserName2;
    @BindView(R.id.tv_pk_type)
    TextView tvPkType;
    @BindView(R.id.rl_select_pk_type)
    RelativeLayout rlSelectPkType;
    @BindView(R.id.tv_pk_time)
    TextView tvPkTime;
    @BindView(R.id.rl_select_pk_time)
    RelativeLayout rlSelectPkTime;
    @BindView(R.id.bu_pk_submit)
    TextView buPkSubmit;
    @BindView(R.id.rcvUser)
    RecyclerView rcvUser;
    @BindView(R.id.edtPkName)
    EditText edtPkName;

    private int pkTime = 60;
    private int pkType = 1;

    private PkUserAdapter pkUserAdapter;
    private List<RoomQueueInfo> datas;
    private List<RoomQueueInfo> micUsers;//麦上用户

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pk_setting);
        ButterKnife.bind(this);
        initTitleBar("PK设置");
        if (mTitleBar != null) {
            TextView textView = new TextView(this);
            textView.setText("记录");
            textView.setTextSize(13);
            textView.setTextColor(ContextCompat.getColor(this, R.color.color_333333));
            mTitleBar.mRightLayout.addView(textView);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PkHistoryActivity.start(PkSettingActivity.this);
                }
            });
        }
        rivPkUserIcon1.setOnClickListener(this);
        rivPkUserIcon2.setOnClickListener(this);
        rlSelectPkTime.setOnClickListener(this);
        rlSelectPkType.setOnClickListener(this);
        buPkSubmit.setOnClickListener(this);

        GridLayoutManager layoutmanager = new GridLayoutManager(this, 4);
        rcvUser.setLayoutManager(layoutmanager);
        pkUserAdapter = new PkUserAdapter();
        pkUserAdapter.bindToRecyclerView(rcvUser);
        pkUserAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                try {
                    if (datas.get(position) == null || datas.get(position).mChatRoomMember == null) {
                        return;
                    }
                    datas.get(position).setSelect(!datas.get(position).isSelect());
                    adapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        micUsers = new ArrayList<>();
        datas = new ArrayList<>(Collections.nCopies(8, null));
        if (AvRoomDataManager.get().mMicQueueMemberMap == null) {
            pkUserAdapter.setNewData(datas);
            return;
        }
        SparseArray<RoomQueueInfo> mMicQueueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap.clone();
        int size = mMicQueueMemberMap.size();
        RoomQueueInfo roomQueueInfo = null;
        for (int i = 0; i < 8; i++) {
            if (i > size) {
                datas.set(i, null);
            } else {
                roomQueueInfo = mMicQueueMemberMap.get(i);
                if (roomQueueInfo != null) {
                    //默认全选
                    roomQueueInfo.setSelect(true);
                    micUsers.add(roomQueueInfo);
                    datas.set(i, roomQueueInfo);
                }
            }
        }
        pkUserAdapter.setNewData(datas);
//        getMvpPresenter().getRoomOnLineUserList(Constants.PAGE_START, 0, 101,
//                AvRoomDataManager.get().mCurrentRoomInfo.getRoomId(), new ArrayList<>());

        GrowingIO.getInstance().trackEditText(edtPkName);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_select_pk_type:
                Map<String, Integer> jsonsType = new LinkedHashMap<>();
                jsonsType.put("按投票人数PK(1人=1pk值）", 1);
                jsonsType.put("按礼物价值PK(1金币=1pk值）", 2);
                buildOptionDialog(jsonsType, pkType);
                break;
            case R.id.rl_select_pk_time:
                Map<String, Integer> jsonsTime = new LinkedHashMap<>();
                jsonsTime.put("30秒", 30);
                jsonsTime.put("60秒", 60);
                jsonsTime.put("90秒", 90);
                jsonsTime.put("180秒", 180);
                jsonsTime.put("5分钟", 300);
                jsonsTime.put("10分钟", 600);
                jsonsTime.put("20分钟", 1200);
                buildOptionDialog(jsonsTime, pkTime);
                break;
            case R.id.bu_pk_submit:
                submit();
                break;
            default:

        }

    }

    private void submit() {
        if (micUsers == null || micUsers.size() <= 1) {
            toast("请选择PK成员〜");
            return;
        }
        try {
            StringBuilder sb = new StringBuilder();
            List<PkVoteInfo.PkUser> selMembers = new ArrayList<>();
            for (RoomQueueInfo member : datas) {
                if (member != null && member.isSelect && member.mChatRoomMember != null) {
                    PkVoteInfo.PkUser pkUser = new PkVoteInfo.PkUser();
                    pkUser.setAvatar(member.mChatRoomMember.getAvatar());
                    pkUser.setNick(member.mChatRoomMember.getNick());
                    pkUser.setUid(member.mChatRoomMember.getAccount());
                    selMembers.add(pkUser);
                    String targetUid = member.mChatRoomMember.getAccount();
                    sb.append(targetUid).append(",");
                }
            }
            if (selMembers.size() < 2) {
                toast("请选择PK成员〜");
                return;
            }
            sb.deleteCharAt(sb.length() - 1);

            String pkTitle = edtPkName.getText().toString();
            pkTitle = pkTitle.replaceAll(" ", "");
            if (TextUtils.isEmpty(pkTitle)) {
                pkTitle = "快来参与投票吧〜";
            }
            PkVoteInfo info = new PkVoteInfo();

            //旧版本PK所需参数
            info.setOpUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            info.setUid(Long.valueOf(selMembers.get(0).getUid()));
            info.setNick(selMembers.get(0).getNick());
            info.setAvatar(selMembers.get(0).getAvatar());
            info.setPkUid(Long.valueOf(selMembers.get(1).getUid()));
            info.setPkNick(selMembers.get(1).getNick());
            info.setPkAvatar(selMembers.get(1).getAvatar());
            //新的PK
            info.setTitle(pkTitle);
            info.setPkType(pkType);
            info.setDuration(pkTime);
            info.setPkList(selMembers);
            info.setTargetUidList(sb.toString());
            info.setExpireSeconds(pkTime);
            getDialogManager().showProgressDialog(this, getString(R.string.network_loading));
            buPkSubmit.setEnabled(false);
            CoreManager.getCore(IPkCore.class).savePK(AvRoomDataManager.get().mCurrentRoomInfo == null ? 0 : AvRoomDataManager.get().mCurrentRoomInfo.getRoomId(), info);
            //房间-进入PK设置
            StatisticManager.get().onEvent(this,
                    StatisticModel.EVENT_ID_ROOM_OPEN_PK,
                    StatisticModel.getInstance().getUMAnalyCommonMap(this));
        } catch (Exception e) {
            e.printStackTrace();
            toast("发起PK失败");
        }
    }

    @CoreEvent(coreClientClass = IPKCoreClient.class)
    public void onSavePk(PkVoteInfo pkVoteInfo) {
        buPkSubmit.setEnabled(true);
        getDialogManager().dismissDialog();
        toast("发起PK成功");
        IMNetEaseManager.get().sendPkNotificationBySdk(CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK,
                CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_START_NEW, pkVoteInfo);
        finish();
    }

    @CoreEvent(coreClientClass = IPKCoreClient.class)
    public void onSavePkFail(String error) {
        buPkSubmit.setEnabled(true);
        getDialogManager().dismissDialog();
        toast(error);
    }

    public void buildOptionDialog(Map<String, Integer> jsons, int type) {
        List<ButtonItem> buttonItems = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : jsons.entrySet()) {
            ButtonItem msgBlackListItem = ButtonItemFactory.createMsgBlackListItem(entry.getKey(), new ButtonItemFactory.OnItemClick() {
                @Override
                public void itemClick() {
                    optionAction(entry.getValue(), entry.getKey());
                }
            });
            if (entry.getValue() == type) {
                msgBlackListItem.textColor = "#09CAA2";
            }
            buttonItems.add(msgBlackListItem);
        }
        getDialogManager().showCommonPopupDialog(buttonItems, getString(R.string.cancel));
    }

    private void optionAction(int type, String name) {
        if (type < 30) {
            pkType = type;
            if (type == 1) {
                tvPkType.setText("按投票人数PK ");
            } else {
                tvPkType.setText("按礼物价值PK ");
            }
        } else {
            pkTime = type;
            tvPkTime.setText(name + " ");
        }
    }

    @Override
    public void onRequestChatMemberByPageSuccess(List<OnlineChatMember> memberList, int page) {

    }

    @Override
    public void onRequestChatMemberByPageFail(String errorStr, int page) {

    }

    @Override
    public void onGetOnLineUserList(boolean isSuccess, String message, int page, List<OnlineChatMember> onlineChatMembers) {
        if (isSuccess) {
            micUsers = new ArrayList<>();
            for (int i = 0; i < onlineChatMembers.size(); i++) {
                //房主不能参与PK
//                if (onlineChatMembers.get(i).isRoomOwer) {
//                    continue;
//                }
                //在麦上才可以参与PK
//                if (onlineChatMembers.get(i).isOnMic) {
//                    micUsers.add(onlineChatMembers.get(i));
//                }
            }
            datas = new ArrayList<>(Collections.nCopies(8, null));
            Collections.copy(datas, micUsers);
            pkUserAdapter.setNewData(datas);
        } else if (!TextUtils.isEmpty(message)) {
            toast(message);
        } else {
            toast("获取麦上用户失败");
        }
    }
}
