package com.yuhuankj.tmxq.ui.home.view.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;

import com.uuzuche.lib_zxing.DisplayUtil;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.thirdsdk.nim.MsgCenterRedPointStatusManager;

/**
 * @author weihaitao
 * @date 2019/5/21
 */
public class NewChannelHomeTopView extends LinearLayout implements MsgCenterRedPointStatusManager.OnRedPointStatusChangedListener {

    private final String TAG = NewChannelHomeTopView.class.getSimpleName();
    private OppoHomeTopClickListener homeTopClickListener;
    private View vRedPoint;

    public NewChannelHomeTopView(Context context) {
        this(context, null);
    }

    public NewChannelHomeTopView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NewChannelHomeTopView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialView();
    }

    public void initialView() {
        inflate(getContext(), R.layout.layout_oppo_home_top, this);
        vRedPoint = findViewById(R.id.vRedPoint);
        vRedPoint.setVisibility(MsgCenterRedPointStatusManager.getInstance().isShowSignInRedPoint()
                ? View.VISIBLE : View.GONE);
        //merge标签设置的如下属性会失效，需要重新设置
        setOrientation(LinearLayout.HORIZONTAL);
        setGravity(Gravity.CENTER_VERTICAL);
        setPadding(0,DisplayUtil.dip2px(getContext(),6),0,DisplayUtil.dip2px(getContext(),6));
        findViewById(R.id.ivLeftIcon).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != homeTopClickListener) {
                    homeTopClickListener.onLeftClick();
                }
            }
        });
        findViewById(R.id.ivRightIcon).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != homeTopClickListener) {
                    homeTopClickListener.onRightClick();
                }
            }
        });
        findViewById(R.id.dtvSearch).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != homeTopClickListener) {
                    homeTopClickListener.onSearchClick();
                }
            }
        });
        MsgCenterRedPointStatusManager.getInstance().addListener(this);
    }

    public void release() {
        homeTopClickListener = null;
        MsgCenterRedPointStatusManager.getInstance().removeListener(this);
    }

    /**
     * 显示新粉丝红点
     */
    @Override
    public void showRedPointOnNewFan() {

    }

    /**
     * 显示新好友红点
     */
    @Override
    public void showRedPointOnNewFriend() {

    }

    /**
     * 显示新点赞红点
     */
    @Override
    public void showRedPointOnNewLike() {

    }

    /**
     * 签到红点
     */
    @Override
    public void showRedPointOnSignIn() {
        if (null != vRedPoint) {
            vRedPoint.setVisibility(MsgCenterRedPointStatusManager.getInstance().isShowSignInRedPoint()
                    ? View.VISIBLE : View.GONE);
        }
    }

    public void setHomeTopClickListener(OppoHomeTopClickListener homeTopClickListener) {
        this.homeTopClickListener = homeTopClickListener;
    }

    public interface OppoHomeTopClickListener {

        void onLeftClick();

        void onSearchClick();

        void onRightClick();
    }
}
