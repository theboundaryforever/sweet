package com.yuhuankj.tmxq.ui.message.emoji;

import com.netease.nim.uikit.session.emoji.Entry;

import java.io.Serializable;
import java.util.List;

/**
 * @author weihaitao
 * @date 2019/8/20
 */
public class ServerEmojiListRespon implements Serializable {

    /**
     * 普通表情
     */
    public List<Entry> normal;
    /**
     * 火热表情
     */
    public List<Entry> hot;
    /**
     * 表情滑动栏
     */
    public List<Entry> slide;
}
