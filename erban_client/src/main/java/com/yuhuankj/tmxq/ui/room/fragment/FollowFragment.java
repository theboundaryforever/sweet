package com.yuhuankj.tmxq.ui.room.fragment;

import android.graphics.Color;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.cache.NimUserInfoCache;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.tencent.bugly.crashreport.BuglyLog;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.ButtonUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.praise.IPraiseClient;
import com.tongdaxing.xchat_core.praise.IPraiseCore;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.fragment.BaseMvpFragment;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.room.adapter.FollowRecommendAdapter;
import com.yuhuankj.tmxq.ui.room.adapter.FollowRoomAdapter;
import com.yuhuankj.tmxq.ui.room.adapter.FollowUserAdapter;
import com.yuhuankj.tmxq.ui.room.interfaces.FollowView;
import com.yuhuankj.tmxq.ui.room.presenter.FollowPresenter;
import com.yuhuankj.tmxq.ui.widget.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

@CreatePresenter(FollowPresenter.class)
public class FollowFragment extends BaseMvpFragment<FollowView, FollowPresenter> implements FollowView, BaseQuickAdapter.RequestLoadMoreListener {

    public static final String hasFollow = "hasFollow";
    private RecyclerView rvFollowUser;
    private RecyclerView rvFollowRecommend;
    private RecyclerView rvFollowRoom;
    private FollowRecommendAdapter followRecommendAdapter;
    private FollowUserAdapter followUserAdapter;
    private FollowRoomAdapter followRoomAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView tvFollowRecommendMore;
    private ImageView ivFollowUserEmpty;
    private TextView tvFollowUserEmpty;
    private TextView tvFollowUserOnlineTips;
    private View rlRecommendUser;
    private boolean isOppositeRoomListEmpty = false;
    private boolean isShowUserEmptyView = false;
    private boolean isRequestingOppositeRomeList = false;
    private boolean isRequestingFollowUserList = false;
    private boolean isRequestingFollowRoomList = false;
    //是否下拉刷新|刷新操作
    private boolean isRefreshOpera = false;
    //当前关注/取消关注操作的用户在推荐用户列表中的顺序
    private int followIndex = 0;
    //当前关注房间列表页加载的页数索引
    private int mCurrentRoomListPage = Constants.PAGE_START;
    //private SkeletonScreen skeletonScreen;//加载状态view
    private View rlMyAttentionUser = null;
    private View vHeader = null;
    private View vFooter = null;

    private View vMyFootprint = null;
    private View vAttentRoom = null;
    private TextView tvAttentRoom = null;
    private TextView tvMyFootprint = null;
    private LinearLayout llAttentRoom, llMyFootprint;
    private List<Json> attentRooms = new ArrayList<>();
    private List<Json> myFooprint = new ArrayList<>();
    private TextView tv_follow_room_empty;
    private int curIdex = 0;
    private LinearLayout llRoomFootTitle;

    @Override
    public void loadLazyIfYouWant() {
        BuglyLog.d(TAG, "loadLazyIfYouWant");
        swipeRefreshLayout.setRefreshing(true);
        loadData();
    }

    private void loadData() {
        if (!CoreManager.getCore(IAuthCore.class).isLogin()) {
            return;
        }
        isRefreshOpera = true;

        isRequestingOppositeRomeList = true;
        getMvpPresenter().getOppositeRoom();

        isRequestingFollowUserList = true;
        getMvpPresenter().getFollowUserList();

        if (curIdex == 0) {
            mCurrentRoomListPage = Constants.PAGE_START;
            isRequestingFollowRoomList = true;
            getMvpPresenter().getAttentionRoomList(mCurrentRoomListPage);
        } else {
            getDialogManager().showProgressDialog(getContext(), "加载中...");
            getMvpPresenter().getMyFooprint();
        }
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_follow;
    }

    @Override
    public void onFindViews() {
        vHeader = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_follow_head, null);
        vFooter = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_follow_buttom, null);

        vMyFootprint = vHeader.findViewById(R.id.vMyFootprint);
        vAttentRoom = vHeader.findViewById(R.id.vAttentRoom);
        tvAttentRoom = vHeader.findViewById(R.id.tvAttentRoom);
        tvMyFootprint = vHeader.findViewById(R.id.tvMyFootprint);
        llAttentRoom = vHeader.findViewById(R.id.llAttentRoom);
        llMyFootprint = vHeader.findViewById(R.id.llMyFootprint);
        rlRecommendUser = vHeader.findViewById(R.id.rl_recommend_user);
        rlRecommendUser.setVisibility(View.GONE);

        rvFollowRecommend = vHeader.findViewById(R.id.rv_follow_recommend);
        tvFollowRecommendMore = vHeader.findViewById(R.id.tv_follow_recommend_more);

        tv_follow_room_empty = vFooter.findViewById(R.id.tv_follow_room_empty);
        llRoomFootTitle = vFooter.findViewById(R.id.llRoomFootTitle);

        rlMyAttentionUser = vHeader.findViewById(R.id.rlMyAttentionUser);
        rvFollowUser = vHeader.findViewById(R.id.rv_follow_user);
        ivFollowUserEmpty = vHeader.findViewById(R.id.iv_follow_user_empty);
        tvFollowUserEmpty = vHeader.findViewById(R.id.tv_follow_user_empty);
        tvFollowUserOnlineTips = vHeader.findViewById(R.id.tv_follow_user_online_tips);
        rvFollowUser.setVisibility(View.GONE);
        ivFollowUserEmpty.setVisibility(View.VISIBLE);
        tvFollowUserEmpty.setVisibility(View.VISIBLE);
        tvFollowUserOnlineTips.setVisibility(View.GONE);

        rvFollowRoom = mView.findViewById(R.id.rv_follow_room);
        swipeRefreshLayout = mView.findViewById(R.id.swipe_follow_refresh);
    }

    @Override
    public void onSetListener() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                BuglyLog.d(TAG, "onRefresh-loadData");
                loadData();
            }
        });
        llAttentRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectRoom();
            }
        });
        llMyFootprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectMyFootprint();
            }
        });
    }

    @Override
    public void initiate() {
        GridLayoutManager layout = new GridLayoutManager(mContext, 3);
        layout.setAutoMeasureEnabled(true);
        rvFollowRecommend.setLayoutManager(layout);

        followRecommendAdapter = new FollowRecommendAdapter();
        rvFollowRecommend.setAdapter(followRecommendAdapter);

        //换一换 点击事件
        tvFollowRecommendMore.setOnClickListener(v -> {
            if (!ButtonUtils.isFastDoubleClick()) {
                isRequestingOppositeRomeList = true;
                getMvpPresenter().getOppositeRoom();
            }
        });

        followRecommendAdapter.setFollowRecommendEvent(new FollowRecommendAdapter.FollowRecommendEvent() {
            @Override
            public void onFollowEvent(int index, long uid) {
                followEvent(index, uid);
            }
        });

        LinearLayoutManager linearLayoutManager = new RecyclerViewNoBugLinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mContext, linearLayoutManager.getOrientation(),
                28, R.color.color_white);
        dividerItemDecoration.setIngoreLastDividerItem(true);
        rvFollowUser.addItemDecoration(dividerItemDecoration);
        rvFollowUser.setLayoutManager(linearLayoutManager);

        followUserAdapter = new FollowUserAdapter();
        followUserAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                LogUtils.d(TAG, "onItemClick-position:" + position);
                try {
                    Json json = FollowFragment.this.followUserAdapter.getData().get(position);
                    long uid = json.num_l("uid");
                    NimUserInfo nimUserInfo = NimUserInfoCache.getInstance().getUserInfo(uid + "");
                    if (nimUserInfo != null) {
                        NimUIKit.startP2PSession(mContext, uid + "");
                    } else {
                        NimUserInfoCache.getInstance().getUserInfoFromRemote(uid + "", new RequestCallbackWrapper<NimUserInfo>() {
                            @Override
                            public void onResult(int i, NimUserInfo nimUserInfo, Throwable throwable) {
                                if (i == 200) {
                                    NimUIKit.startP2PSession(mContext, uid + "");
                                } else {
                                    toast(getResources().getString(R.string.network_error_retry));
                                }
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        rvFollowUser.setAdapter(followUserAdapter);

        LinearLayoutManager linearLayoutManager1 = new RecyclerViewNoBugLinearLayoutManager(mContext);
        rvFollowRoom.setLayoutManager(linearLayoutManager1);
        DividerItemDecoration dividerItemDecoration1 = new DividerItemDecoration(mContext, linearLayoutManager1.getOrientation(),
                10, R.color.color_white);
        dividerItemDecoration.setIngoreLastDividerItem(true);
        rvFollowRoom.addItemDecoration(dividerItemDecoration1);
        followRoomAdapter = new FollowRoomAdapter();
        followRoomAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                try {
                    Json json = FollowFragment.this.followRoomAdapter.getData().get(position);
                    long uid = json.num_l("uid");
                    int roomType = RoomInfo.ROOMTYPE_HOME_PARTY;
                    if (json.has("type")) {
                        roomType = json.num("type");
                    }
                    RoomServiceScheduler.getInstance().enterOtherTypeRoomFromService(getActivity(), uid, roomType);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        rvFollowRoom.setAdapter(followRoomAdapter);
        vHeader.setVisibility(View.INVISIBLE);
        followRoomAdapter.addHeaderView(vHeader);

//        skeletonScreen = Skeleton.bind(swipeRefreshLayout)
//                .load(R.layout.fragment_follow_head_loading)
//                .show();
    }

    private void followEvent(int index, long uid) {
        followIndex = index;
        CoreManager.getCore(IPraiseCore.class).praise(uid);
        //首页-关注-推荐关注
        StatisticManager.get().onEvent(getContext(),
                StatisticModel.EVENT_ID_FOLLOW_NOT_ATTENTION_ATTENTION,
                StatisticModel.getInstance().getUMAnalyCommonMap(getContext()));
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraise(long likedUid) {
        changeFollowState(followIndex);
        toast(R.string.fan_success);
        isRequestingFollowUserList = true;
        getMvpPresenter().getFollowUserList();
    }

    private void changeFollowState(int followIndex) {
        List<Json> data = followRecommendAdapter.getData();
        if (followIndex >= data.size() || followIndex < 0) {
            return;
        }

        data.get(followIndex).set(hasFollow, 1);
        followRecommendAdapter.notifyDataSetChanged();
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraiseFaith(String error) {
        toast(error);
    }


    /**
     * 搜索朋友 关键字搜索结果返回
     *
     * @param isSuccess
     * @param data
     */
    @Override
    public void onGetAttentionRoomList(boolean isSuccess, List data) {
        BuglyLog.d(TAG, "onGetAttentionRoomList-isSuccess:" + isSuccess + " mCurrentRoomListPage:" + mCurrentRoomListPage);
        isRequestingFollowRoomList = false;
        if (!isSuccess) {
            //展示关注房间列表为空的界面
            if (mCurrentRoomListPage != Constants.PAGE_START) {
                //更多加载操作需要更新底部view的状态
                followRoomAdapter.loadMoreFail();
            } else {
                try {
                    followRoomAdapter.removeAllFooterView();
                    followRoomAdapter.addFooterView(vFooter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            if (followRoomAdapter != null && null != data) {
                if (mCurrentRoomListPage == Constants.PAGE_START) {
                    attentRooms = data;
                    followRoomAdapter.setNewData(data);
                    //下拉刷新后返回的数据，不够10个的话，禁用上拉加载更多的功能
                    if (data.size() >= Constants.PAGE_SIZE) {
                        followRoomAdapter.setEnableLoadMore(true);
                        followRoomAdapter.setOnLoadMoreListener(this, rvFollowRoom);
                    } else {
                        followRoomAdapter.setEnableLoadMore(false);
                        followRoomAdapter.setOnLoadMoreListener(null, rvFollowRoom);
                    }

                    //展示关注方式列表为空的界面
                    followRoomAdapter.removeAllFooterView();
                    if (data.size() == 0) {
                        followRoomAdapter.addFooterView(vFooter);
                    }
                } else {
                    //更多加载操作需要更新底部view的状态
                    if (0 == data.size()) {
                        followRoomAdapter.loadMoreEnd(true);
                    } else {
                        followRoomAdapter.addData(data);
                        attentRooms.addAll(data);
                        followRoomAdapter.loadMoreComplete();
                    }
                }
                //不论data是否大于零，肯定非空，因此索引值叠加
                mCurrentRoomListPage += 1;
            }
        }

        onRefreshComplete();
    }

    private void onRefreshComplete() {
        if (!isRequestingOppositeRomeList && !isRequestingFollowUserList && !isRequestingFollowRoomList) {
            if (isRefreshOpera) {
                vHeader.setVisibility(View.VISIBLE);
                /*
                 * 一、ScrollView.scrollTo(0,0) 直接置顶，瞬间回到顶部，没有滚动过程，其中Y值可以设置为大于0的值，使Scrollview停在指定位置;
                 * 二、ScrollView.fullScroll(View.FOCUS_UP) 类似于手动拖回顶部,有滚动过程;
                 * 三、ScrollView.smoothScrollTo(0, 0) 类似于手动拖回顶部,有滚动过程，其中Y值可以设置为大于0的值，使Scrollview停在指定位置。
                 */
                rvFollowRoom.scrollTo(0, 0);
            }
            isRefreshOpera = false;
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onGetOppositeRoom(boolean isSuccess, List data) {
        isRequestingOppositeRomeList = false;
        isOppositeRoomListEmpty = !isSuccess || null == data || data.size() == 0;
        BuglyLog.d(TAG, "onGetOppositeRoom-isSuccess:" + isSuccess + " isOppositeRoomListEmpty:" + isOppositeRoomListEmpty);
        if (isSuccess) {
            if (null != data && null != followRecommendAdapter) {
                followRecommendAdapter.setNewData(data);
            }
        }
        rlRecommendUser.setVisibility(isShowUserEmptyView && !isRequestingFollowUserList && !isRequestingOppositeRomeList && !isOppositeRoomListEmpty ? View.VISIBLE : View.GONE);
        onRefreshComplete();
//        try {
//            skeletonScreen.hide();
//        } catch (Exception e1) {
//            e1.printStackTrace();
//        }
    }

    /**
     * 获取关注用户列表
     *
     * @param isSuccess
     * @param data
     */
    @Override
    public void onGetFollowUserList(boolean isSuccess, List data) {
        isRequestingFollowUserList = false;
        if (!isSuccess) {
            //展示关注用户列表为空的界面
            isShowUserEmptyView = true;
        } else {
            if (null != data) {
                if (followUserAdapter != null) {
                    followUserAdapter.setNewData(data);
                }
                tvFollowUserOnlineTips.setText(mContext.getResources().getString(R.string.follow_user_online_num, data.size()));
            }

            //展示关注方式列表为空的界面
            isShowUserEmptyView = null == data || data.size() == 0;
        }
        BuglyLog.d(TAG, "onGetOppositeRoom-isSuccess:" + isSuccess + " isShowUserEmptyView:" + isShowUserEmptyView);

        rlMyAttentionUser.setVisibility(isShowUserEmptyView ? View.GONE : View.VISIBLE);
        rvFollowUser.setVisibility(isShowUserEmptyView ? View.GONE : View.VISIBLE);
        ivFollowUserEmpty.setVisibility(isShowUserEmptyView ? View.VISIBLE : View.GONE);
        tvFollowUserEmpty.setVisibility(isShowUserEmptyView ? View.VISIBLE : View.GONE);
        tvFollowUserOnlineTips.setVisibility(isShowUserEmptyView ? View.GONE : View.VISIBLE);
        rlRecommendUser.setVisibility(isShowUserEmptyView && !isRequestingFollowUserList && !isRequestingOppositeRomeList && !isOppositeRoomListEmpty ? View.VISIBLE : View.GONE);
        onRefreshComplete();
//        try {
//            skeletonScreen.hide();
//        } catch (Exception e1) {
//            e1.printStackTrace();
//        }
    }


    @Override
    public void onLoadMoreRequested() {
        BuglyLog.d(TAG, "onLoadMoreRequested-mCurrentRoomListPage:" + mCurrentRoomListPage);
        isRequestingFollowRoomList = true;
        getMvpPresenter().getAttentionRoomList(mCurrentRoomListPage);
    }

    private void selectRoom() {
        curIdex = 0;
        tvAttentRoom.setTextColor(Color.parseColor("#333333"));
        tvAttentRoom.setTextSize(18);
        vAttentRoom.setVisibility(View.VISIBLE);

        tvMyFootprint.setTextColor(Color.parseColor("#666666"));
        tvMyFootprint.setTextSize(15);
        vMyFootprint.setVisibility(View.INVISIBLE);

        followRoomAdapter.removeAllFooterView();
        followRoomAdapter.setNewData(attentRooms);
        if (attentRooms != null && attentRooms.size() == 0) {
            tv_follow_room_empty.setText("尚无关注的房间");
            followRoomAdapter.addFooterView(vFooter);
        }
    }

    private void selectMyFootprint() {
        curIdex = 1;
        tvMyFootprint.setTextColor(Color.parseColor("#333333"));
        tvMyFootprint.setTextSize(18);
        vMyFootprint.setVisibility(View.VISIBLE);

        tvAttentRoom.setTextColor(Color.parseColor("#666666"));
        tvAttentRoom.setTextSize(15);
        vAttentRoom.setVisibility(View.INVISIBLE);

        try {
            followRoomAdapter.removeAllFooterView();
            followRoomAdapter.setNewData(myFooprint);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (myFooprint.size() == 0) {
            tv_follow_room_empty.setText("尚无访问的房间");
            followRoomAdapter.addFooterView(vFooter);
        }
        if (myFooprint == null || myFooprint.size() == 0) {
            getDialogManager().showProgressDialog(getContext(), "加载中...");
            getMvpPresenter().getMyFooprint();
        }
    }

    @Override
    public void onMyFooprint(boolean isSuccess, List data) {
        getDialogManager().dismissDialog();
        if (data != null && data.size() > 0) {
            if (myFooprint == null) {
                myFooprint = new ArrayList<>();
            }
            myFooprint.clear();
            myFooprint.addAll(data);
            followRoomAdapter.removeAllFooterView();
            followRoomAdapter.setNewData(myFooprint);
        }
    }
}
