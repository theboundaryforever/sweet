package com.yuhuankj.tmxq.ui.home.game;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.netease.nim.uikit.common.util.sys.TimeUtil;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.GameRoomEnitity;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

/**
 * @author liaoxy
 * @Description:游戏大厅页房间adapter
 * @date 2019/2/15 10:11
 */
public class GameHomeLiveRoomAdapter extends BaseQuickAdapter<GameRoomEnitity, BaseViewHolder> {

    private int imvIconWidthAndHeight = 0;
    private int imvMarkWidth = 0;
    private int imvMarkHeight = 0;

    public GameHomeLiveRoomAdapter(List<GameRoomEnitity> data) {
        super(R.layout.item_game_home_live_room, data);
        imvIconWidthAndHeight = DisplayUtility.dp2px(mContext, 76);
        imvMarkWidth = DisplayUtility.dp2px(mContext, 54);
        imvMarkHeight = DisplayUtility.dp2px(mContext, 15);
    }

    @Override
    protected void convert(BaseViewHolder helper, GameRoomEnitity item) {
        ImageView imvIcon = helper.getView(R.id.imvIcon);
        ImageView imvRunning = helper.getView(R.id.imvRunning);
        ImageView imvMark = helper.getView(R.id.imvMark);
        TextView tvRoomName = helper.getView(R.id.tvRoomName);
        TextView tvRoomInfo = helper.getView(R.id.tvRoomInfo);
        TextView tvTime = helper.getView(R.id.tvTime);

        GlideApp.with(mContext).load(ImageLoadUtils.toThumbnailUrl(imvIconWidthAndHeight, imvIconWidthAndHeight, item.getAvatar()))
                .dontAnimate()
                .error(R.drawable.bg_default_cover_round_placehold_15)
                .transforms(new CenterCrop(),
                        new RoundedCorners(DisplayUtility.dp2px(mContext, 15)))
                .into(imvIcon);

        GlideApp.with(mContext).asGif().load(R.drawable.anim_game_home_room_running).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).into(imvRunning);
        if (!TextUtils.isEmpty(item.getLabelUrl())) {
            imvMark.setVisibility(View.VISIBLE);
            Glide.with(mContext).load(ImageLoadUtils.toThumbnailUrl(imvMarkWidth, imvMarkHeight, item.getLabelUrl())).into(imvMark);
        } else {
            imvMark.setVisibility(View.GONE);
        }
        tvRoomName.setText(item.getTitle());
        tvRoomInfo.setText(mContext.getResources().getString(R.string.game_room_online_tips, item.getRoomTag(), String.valueOf(item.getOnlineNum())));
        tvTime.setText(TimeUtil.getTimeExp(item.getNewestTime()));
        helper.addOnClickListener(R.id.rlRoomInfo);
    }
}