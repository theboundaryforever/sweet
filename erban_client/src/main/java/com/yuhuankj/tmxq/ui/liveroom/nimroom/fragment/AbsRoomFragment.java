package com.yuhuankj.tmxq.ui.liveroom.nimroom.fragment;

import com.tongdaxing.xchat_core.redpacket.bean.ActionDialogInfo;
import com.yuhuankj.tmxq.base.fragment.BaseMvpFragment;

import java.util.List;

/**
 * @author chenran
 * @date 2017/8/8
 */

public abstract class AbsRoomFragment extends BaseMvpFragment<HomePartView, HomePartyPresenter>  {

    public abstract void onShowActivity(List<ActionDialogInfo> dialogInfo);


    public void onRoomOnlineNumberSuccess(int onlineNumber) {

    }

    public void release() {
    }
}
