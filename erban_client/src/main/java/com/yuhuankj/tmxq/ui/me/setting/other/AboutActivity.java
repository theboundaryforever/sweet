package com.yuhuankj.tmxq.ui.me.setting.other;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;

import java.util.Locale;

public class AboutActivity extends BaseActivity {

    TextView versinos;
    TextView slogan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        initTitleBar(getString(R.string.title_about_text));
        initView();
        initData();
    }

    private void initData() {
        versinos.setText(String.format(Locale.getDefault(), getString(R.string.about_version_name),
                BasicConfig.getLocalVersionName(getApplication())));
        SpannableString spannableString = new SpannableString(slogan.getText());
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.color_fe5b69)),
                slogan.getText().length() - 2, slogan.getText().length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        slogan.setText(spannableString);
    }

    private void initView() {
        versinos = (TextView) findViewById(R.id.versions);
        slogan = (TextView) findViewById(R.id.about_slogan);
    }
}
