package com.yuhuankj.tmxq.ui.signAward.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.noober.background.view.BLImageView;
import com.noober.background.view.BLTextView;
import com.noober.background.view.BLView;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.ButtonUtils;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.signAward.model.RecommMicUserInfo;
import com.yuhuankj.tmxq.ui.signAward.model.RecvNewsGiftPkgResult;
import com.yuhuankj.tmxq.ui.signAward.model.SignRecvGiftPkgInfo;
import com.yuhuankj.tmxq.ui.signAward.presenter.SignAwardTipsPresenter;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 萌新大礼包提示页面
 *
 * @author weihaitao
 * @date 2019/5/22
 */
@CreatePresenter(SignAwardTipsPresenter.class)
public class SignAwardTipsDialog extends BaseMvpActivity<ISignAwardTipsView, SignAwardTipsPresenter>
        implements ISignAwardTipsView, View.OnClickListener {

    private BLView mBlvTop;
    private ImageView mIvLeftColorBlock;
    private ImageView mIvRightColorBlock;
    private TextView mTvMicRecommTips;
    private ImageView mIvCenterMicAvatar;
    private BLView mBlivCenterMicAvatar;
    private BLImageView mIvCenterSelectStatus;
    private TextView mTvCenterMicNick;
    private ImageView mIvLeftMicAvatar;
    private BLView mBlivLeftMicAvatar;
    private BLImageView mIvLeftSelectStatus;
    private TextView mTvLeftMicNick;
    private ImageView mIvRightMicAvatar;
    private BLView mBlivRightMicAvatar;
    private BLImageView mIvRightSelectStatus;
    private TextView mTvRightMicNick;
    private BLView mBlvLeftRod;
    private BLView mBlvRightRod;
    private TextView mTvFirstDay;
    private TextView mTvSecondDay;
    private TextView mTvThirdDay;
    private ImageView mIvSecondRecv;
    private TextView tvSecondRecv;
    private BLView mBlvLeftDiv;
    private ImageView ivFirstRecv;
    private TextView tvFirstRecv;
    private BLView mBlvRightDiv;
    private ImageView mIvThridRecv;
    private TextView tvThridRecv;
    private BLTextView mBltvSecondDay;
    private BLTextView mBltvFirstDay;
    private BLTextView mBltvThirdDay;
    private BLTextView bltvTake;
    private BLTextView bltvTakeAready;
    private RelativeLayout rlCenter;
    private BLView blvDayIndex;

    private int currChooseMicIndex = 1;
    private int currChooseDay = 1;
    private int giftPkgPicSize = 0;
    private int getDay = 1;

    private List<RecommMicUserInfo> micRoomUserList;
    private Map<Integer, List<SignRecvGiftPkgInfo>> newsBigGiftMap;

    public static void start(Context context, int getDay, List<RecommMicUserInfo> micRoomUserList,
                             Map<Integer, List<SignRecvGiftPkgInfo>> newsBigGiftMap) {
        Intent intent = new Intent(context, SignAwardTipsDialog.class);
        intent.putExtra("getDay", getDay);
        intent.putExtra("micRoomUserList", (Serializable) micRoomUserList);
        intent.putExtra("newsBigGiftMap", (Serializable) newsBigGiftMap);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_award_tips);
        initData();
        initView();
        setListener();
    }

    private void initView() {
        mBlvTop = (BLView) findViewById(R.id.blvTop);
        bltvTake = (BLTextView) findViewById(R.id.bltvTake);
        bltvTakeAready = (BLTextView) findViewById(R.id.bltvTakeAready);
        blvDayIndex = (BLView) findViewById(R.id.blvDayIndex);
        rlCenter = (RelativeLayout) findViewById(R.id.rlCenter);
        mIvLeftColorBlock = (ImageView) findViewById(R.id.ivLeftColorBlock);
        mIvRightColorBlock = (ImageView) findViewById(R.id.ivRightColorBlock);
        mTvMicRecommTips = (TextView) findViewById(R.id.tvMicRecommTips);
        mIvCenterMicAvatar = (ImageView) findViewById(R.id.ivCenterMicAvatar);
        mBlivCenterMicAvatar = (BLView) findViewById(R.id.blivCenterMicAvatar);
        mIvCenterSelectStatus = (BLImageView) findViewById(R.id.ivCenterSelectStatus);
        mTvCenterMicNick = (TextView) findViewById(R.id.tvCenterMicNick);
        mIvLeftMicAvatar = (ImageView) findViewById(R.id.ivLeftMicAvatar);
        mBlivLeftMicAvatar = (BLView) findViewById(R.id.blivLeftMicAvatar);
        mIvLeftSelectStatus = (BLImageView) findViewById(R.id.ivLeftSelectStatus);
        mTvLeftMicNick = (TextView) findViewById(R.id.tvLeftMicNick);
        mIvRightMicAvatar = (ImageView) findViewById(R.id.ivRightMicAvatar);
        mBlivRightMicAvatar = (BLView) findViewById(R.id.blivRightMicAvatar);
        mIvRightSelectStatus = (BLImageView) findViewById(R.id.ivRightSelectStatus);
        mTvRightMicNick = (TextView) findViewById(R.id.tvRightMicNick);
        mBlvLeftRod = (BLView) findViewById(R.id.blvLeftRod);
        mBlvRightRod = (BLView) findViewById(R.id.blvRightRod);
        mTvFirstDay = (TextView) findViewById(R.id.tvSecondDay);
        mTvSecondDay = (TextView) findViewById(R.id.tvFirstDay);
        mTvThirdDay = (TextView) findViewById(R.id.tvThirdDay);
        mIvSecondRecv = (ImageView) findViewById(R.id.ivSecondRecv);
        tvSecondRecv = (TextView) findViewById(R.id.tvSecondRecv);
        mBlvLeftDiv = (BLView) findViewById(R.id.blvLeftDiv);
        ivFirstRecv = (ImageView) findViewById(R.id.ivFirstRecv);
        tvFirstRecv = (TextView) findViewById(R.id.tvFirstRecv);
        mBlvRightDiv = (BLView) findViewById(R.id.blvRightDiv);
        mIvThridRecv = (ImageView) findViewById(R.id.ivThridRecv);
        tvThridRecv = (TextView) findViewById(R.id.tvThridRecv);
        mBltvSecondDay = (BLTextView) findViewById(R.id.bltvSecondDay);
        mBltvFirstDay = (BLTextView) findViewById(R.id.bltvFirstDay);
        mBltvThirdDay = (BLTextView) findViewById(R.id.bltvThirdDay);

        findViewById(R.id.rlContainer).setOnClickListener(this);
        findViewById(R.id.rlRoot).setOnClickListener(this);

        //更新显示状态
        if (null == micRoomUserList || micRoomUserList.size() == 0) {
            mBlvTop.setVisibility(View.GONE);
            mTvMicRecommTips.setVisibility(View.GONE);
            mIvLeftColorBlock.setVisibility(View.GONE);
            mIvRightColorBlock.setVisibility(View.GONE);

            mBlivLeftMicAvatar.setVisibility(View.GONE);
            mBlivLeftMicAvatar.setVisibility(View.GONE);
            mIvLeftSelectStatus.setVisibility(View.GONE);
            mTvLeftMicNick.setVisibility(View.GONE);

            mBlivCenterMicAvatar.setVisibility(View.GONE);
            mBlivCenterMicAvatar.setVisibility(View.GONE);
            mIvCenterSelectStatus.setVisibility(View.GONE);
            mTvCenterMicNick.setVisibility(View.GONE);

            mBlivRightMicAvatar.setVisibility(View.GONE);
            mBlivRightMicAvatar.setVisibility(View.GONE);
            mIvRightSelectStatus.setVisibility(View.GONE);
            mTvRightMicNick.setVisibility(View.GONE);

            RelativeLayout.LayoutParams lp1 = (RelativeLayout.LayoutParams) mBlvLeftRod.getLayoutParams();
            lp1.height = DisplayUtility.dp2px(this, 25);
            mBlvLeftRod.setLayoutParams(lp1);

            RelativeLayout.LayoutParams lp3 = (RelativeLayout.LayoutParams) rlCenter.getLayoutParams();
            lp3.topMargin = -DisplayUtility.dp2px(this, 12);
            rlCenter.setLayoutParams(lp3);
        } else {
            int avatarWidth = DisplayUtility.dp2px(this, 60);
            int avatarCorner = DisplayUtility.dp2px(this, 30);
            RecommMicUserInfo recommMicUserInfo1 = micRoomUserList.get(0);
            ImageLoadUtils.loadBannerRoundBgWithOutPlaceHolder(this,
                    ImageLoadUtils.toThumbnailUrl(avatarWidth, avatarWidth, recommMicUserInfo1.getAvatar()),
                    mIvLeftMicAvatar,
                    avatarCorner,
                    R.drawable.bg_default_cover_round_placehold_size60);
            String nick = recommMicUserInfo1.getNick();
            mTvLeftMicNick.setText(nick);

            if (micRoomUserList.size() > 1) {
                recommMicUserInfo1 = micRoomUserList.get(1);
                ImageLoadUtils.loadBannerRoundBgWithOutPlaceHolder(this,
                        ImageLoadUtils.toThumbnailUrl(avatarWidth, avatarWidth, recommMicUserInfo1.getAvatar()),
                        mIvCenterMicAvatar,
                        avatarCorner,
                        R.drawable.bg_default_cover_round_placehold_size60);
                nick = recommMicUserInfo1.getNick();
                mTvCenterMicNick.setText(nick);
            }

            if (micRoomUserList.size() > 2) {
                recommMicUserInfo1 = micRoomUserList.get(2);
                ImageLoadUtils.loadBannerRoundBgWithOutPlaceHolder(this,
                        ImageLoadUtils.toThumbnailUrl(avatarWidth, avatarWidth, recommMicUserInfo1.getAvatar()),
                        mIvRightMicAvatar,
                        avatarCorner,
                        R.drawable.bg_default_cover_round_placehold_size60);
                nick = recommMicUserInfo1.getNick();
                mTvRightMicNick.setText(nick);
            }

            changeMicUserChooseStatus();
        }
        refreshDayGiftPkgData();
    }

    /**
     * 选中用户状态切换
     */
    private void changeMicUserChooseStatus() {
        mIvLeftSelectStatus.setVisibility(currChooseMicIndex == 1 ? View.VISIBLE : View.GONE);
        mIvCenterSelectStatus.setVisibility(currChooseMicIndex == 2 ? View.VISIBLE : View.GONE);
        mIvRightSelectStatus.setVisibility(currChooseMicIndex == 3 ? View.VISIBLE : View.GONE);
    }

    private void refreshDayGiftPkgData() {
        if (null != newsBigGiftMap && newsBigGiftMap.containsKey(currChooseDay)) {
            //显示对应的天数
            mBltvSecondDay.setVisibility(currChooseDay == 2 ? View.VISIBLE : View.GONE);
            mBltvFirstDay.setVisibility(currChooseDay == 1 ? View.VISIBLE : View.GONE);
            mBltvThirdDay.setVisibility(currChooseDay == 3 ? View.VISIBLE : View.GONE);

            //控制按钮是否可点击
            if (currChooseDay <= getDay) {
                //点击已经领取过的天数对应的礼包,显示已领取按钮
                bltvTakeAready.setVisibility(View.VISIBLE);
                bltvTake.setVisibility(View.GONE);
            } else {
                bltvTakeAready.setVisibility(View.GONE);
                bltvTake.setVisibility(View.VISIBLE);
            }

            List<SignRecvGiftPkgInfo> signRecvGiftPkgInfos = newsBigGiftMap.get(currChooseDay);
            if (null != signRecvGiftPkgInfos) {
                SignRecvGiftPkgInfo signRecvGiftPkgInfo = null;

                if (signRecvGiftPkgInfos.size() > 0) {
                    signRecvGiftPkgInfo = signRecvGiftPkgInfos.get(0);
                    ImageLoadUtils.loadImage(this,
                            ImageLoadUtils.toThumbnailUrl(giftPkgPicSize, giftPkgPicSize,
                                    signRecvGiftPkgInfo.getGiftPicUrl())
                            , ivFirstRecv);
                    tvFirstRecv.setText(signRecvGiftPkgInfo.getGiftName());
                }

                if (signRecvGiftPkgInfos.size() > 1) {
                    signRecvGiftPkgInfo = signRecvGiftPkgInfos.get(1);
                    ImageLoadUtils.loadImage(this,
                            ImageLoadUtils.toThumbnailUrl(giftPkgPicSize, giftPkgPicSize,
                                    signRecvGiftPkgInfo.getGiftPicUrl())
                            , mIvSecondRecv);
                    tvSecondRecv.setText(signRecvGiftPkgInfo.getGiftName());
                }

                if (signRecvGiftPkgInfos.size() > 2) {
                    signRecvGiftPkgInfo = signRecvGiftPkgInfos.get(2);
                    ImageLoadUtils.loadImage(this,
                            ImageLoadUtils.toThumbnailUrl(giftPkgPicSize, giftPkgPicSize,
                                    signRecvGiftPkgInfo.getGiftPicUrl())
                            , mIvThridRecv);
                    tvThridRecv.setText(signRecvGiftPkgInfo.getGiftName());
                }
            }
        }
    }

    private void initData() {
        Intent intent = getIntent();
        if (null == intent) {
            finish();
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            return;
        }
        if (!intent.hasExtra("getDay") || !intent.hasExtra("newsBigGiftMap")) {
            finish();
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            return;
        }
        getDay = intent.getIntExtra("getDay", 1);
        currChooseDay = getDay + 1;
        //推荐麦上用户，仅oppo时长才有，因此这里不需要判断为空finish
        if (intent.hasExtra("micRoomUserList")) {
            micRoomUserList = (List<RecommMicUserInfo>) intent.getSerializableExtra("micRoomUserList");
        }

        newsBigGiftMap = (Map<Integer, List<SignRecvGiftPkgInfo>>) intent.getSerializableExtra("newsBigGiftMap");
        if (null == newsBigGiftMap) {
            finish();
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
        giftPkgPicSize = DisplayUtility.dp2px(this, 62);
    }

    private void setListener() {
        mIvCenterMicAvatar.setOnClickListener(this);
        mBlivCenterMicAvatar.setOnClickListener(this);
        mTvCenterMicNick.setOnClickListener(this);
        mIvLeftMicAvatar.setOnClickListener(this);
        mBlivLeftMicAvatar.setOnClickListener(this);
        mTvLeftMicNick.setOnClickListener(this);
        mIvRightMicAvatar.setOnClickListener(this);
        mBlivRightMicAvatar.setOnClickListener(this);
        mTvRightMicNick.setOnClickListener(this);
        mTvFirstDay.setOnClickListener(this);
        mTvSecondDay.setOnClickListener(this);
        mTvThirdDay.setOnClickListener(this);
        bltvTake.setOnClickListener(this);
        bltvTakeAready.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rlContainer:
                break;
            case R.id.rlRoot:
                finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
            case R.id.ivCenterMicAvatar:
            case R.id.blivCenterMicAvatar:
            case R.id.tvCenterMicNick:
                currChooseMicIndex = 2;
                changeMicUserChooseStatus();
                break;
            case R.id.ivLeftMicAvatar:
            case R.id.blivLeftMicAvatar:
            case R.id.tvLeftMicNick:
                currChooseMicIndex = 1;
                changeMicUserChooseStatus();
                break;
            case R.id.ivRightMicAvatar:
            case R.id.blivRightMicAvatar:
            case R.id.tvRightMicNick:
                currChooseMicIndex = 3;
                changeMicUserChooseStatus();
                break;
            case R.id.tvSecondDay:
                if (currChooseDay != 2) {
                    currChooseDay = 2;
                    refreshDayGiftPkgData();
                }
                break;
            case R.id.tvFirstDay:
                if (currChooseDay != 1) {
                    currChooseDay = 1;
                    refreshDayGiftPkgData();
                }
                break;
            case R.id.tvThirdDay:
                if (currChooseDay != 3) {
                    currChooseDay = 3;
                    refreshDayGiftPkgData();
                }
                break;
            case R.id.bltvTakeAready:
                break;
            case R.id.bltvTake:
                if (ButtonUtils.isFastDoubleClick(bltvTake.getId())) {
                    return;
                }
                if (currChooseDay - getDay > 1) {
                    toast(getResources().getString(R.string.sign_award_tips_day_after));
                    return;
                }
                getDialogManager().showProgressDialog(this, getResources().getString(R.string.network_loading), false);
                long micUid = 0L;
                long roomId = 0L;
                int roomType = 0;
                long roomUid = 0L;

                if (null != micRoomUserList && micRoomUserList.size() > currChooseMicIndex - 1) {
                    RecommMicUserInfo recommMicUserInfo = micRoomUserList.get(currChooseMicIndex - 1);
                    micUid = recommMicUserInfo.getUid();
                    roomId = recommMicUserInfo.getRoomId();
                    roomType = recommMicUserInfo.getRoomType();
                    roomUid = recommMicUserInfo.getRoomUid();
                }

                getMvpPresenter().receiveNewsBigGift(getDay + 1, micUid, roomId, roomType, roomUid);

                //萌新大礼包--领取按钮打点事件
                Map<String, String> maps = StatisticModel.getInstance().getUMAnalyCommonMap(this);
                maps.put("getDay", String.valueOf(getDay + 1));
                StatisticManager.get().onEvent(this,
                        StatisticModel.EVENT_ID_NEWS_GIFT_TAKE, maps);
                break;
            default:
                break;
        }
    }

    /**
     * 显示萌新大礼包领取结果弹框
     *
     * @param result
     */
    @Override
    public void showSignAwardResultDialog(RecvNewsGiftPkgResult result) {
        getDialogManager().dismissDialog();
        List<SignRecvGiftPkgInfo> signRecvGiftPkgInfos = new ArrayList<>();
        //选择的天数礼物列表
        if (null != newsBigGiftMap && newsBigGiftMap.containsKey(getDay + 1)) {
            signRecvGiftPkgInfos = newsBigGiftMap.get(currChooseDay);
        }
        //判断是否走的选择用户流程
        if (null != micRoomUserList && micRoomUserList.get(currChooseMicIndex - 1) != null) {
            SignAwardNeedEnterRoomDialog.start(this, result.getRoomUid(), result.getRoomType(),
                    result.getMessage(), signRecvGiftPkgInfos);
        } else {
            //非选择用户流程，后台随机返回一个房间及麦上用户
            SignAwardTakeDialog.start(this, result.getRoomUid(), result.getRoomType(),
                    result.getMessage(), signRecvGiftPkgInfos);
        }

        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    public void showRecvSignPkgError(String message) {
        getDialogManager().dismissDialog();
        if (!TextUtils.isEmpty(message)) {
            toast(message);
        }
    }
}
