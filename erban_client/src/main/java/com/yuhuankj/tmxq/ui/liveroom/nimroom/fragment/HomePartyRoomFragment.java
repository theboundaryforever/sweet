package com.yuhuankj.tmxq.ui.liveroom.nimroom.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jude.rollviewpager.hintview.ColorPointHintView;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.netease.nim.uikit.glide.GlideApp;
import com.netease.nimlib.sdk.chatroom.ChatRoomMessageBuilder;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomKickOutEvent;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomMessage;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.msg.model.RecentContact;
import com.opensource.svgaplayer.SVGACallback;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tencent.bugly.crashreport.BuglyLog;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.coremanager.IUserInfoCore;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.ButtonUtils;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.JavaUtil;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.NotchFixUtil;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.TouchAndClickOperaUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.auth.RealNameAuthStatusChecker;
import com.tongdaxing.xchat_core.bean.RoomMicInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.bean.RoomRedPacket;
import com.tongdaxing.xchat_core.bean.RoomRedPacketFinish;
import com.tongdaxing.xchat_core.bean.RoomRedPacketReceiveResult;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.gift.IGiftCoreClient;
import com.tongdaxing.xchat_core.gift.MultiGiftReceiveInfo;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCore;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCoreClient;
import com.tongdaxing.xchat_core.im.custom.bean.AnnualRoomRankAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.BosonFriendUpMicAnimAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.DetonateGiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.GiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.LoverUpMicAnimAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.MultiGiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.OpenNobleNotifyAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomRedPacketAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomRedPacketFinishAttachment;
import com.tongdaxing.xchat_core.im.message.IIMMessageCore;
import com.tongdaxing.xchat_core.im.message.IIMMessageCoreClient;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.manager.agora.AgoraEngineManager;
import com.tongdaxing.xchat_core.pair.IPairClient;
import com.tongdaxing.xchat_core.pair.IPairCore;
import com.tongdaxing.xchat_core.praise.IPraiseClient;
import com.tongdaxing.xchat_core.praise.IPraiseCore;
import com.tongdaxing.xchat_core.redpacket.bean.ActionDialogInfo;
import com.tongdaxing.xchat_core.room.IRoomCore;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.face.FaceInfo;
import com.tongdaxing.xchat_core.room.face.IFaceCore;
import com.tongdaxing.xchat_core.room.presenter.HomePartyRoomPresenter;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.tongdaxing.xchat_core.room.queue.bean.RoomConsumeInfo;
import com.tongdaxing.xchat_core.room.view.IHomePartyRoomView;
import com.tongdaxing.xchat_core.room_red_packet.RedPacketModel;
import com.tongdaxing.xchat_core.share.IShareCore;
import com.tongdaxing.xchat_core.share.IShareCoreClient;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_core.user.bean.BosonFriendEnitity;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.base.fragment.BaseMvpFragment;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.databinding.FragmentAvRoomGameBinding;
import com.yuhuankj.tmxq.ui.audio.widget.MusicPlayerView;
import com.yuhuankj.tmxq.ui.launch.guide.NoviceGuideDialog;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter.RoomBannerAdapter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.listener.AnimationListener;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget.RoomBannerShapeHintView;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.AVRoomActivity;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.activity.DetonateGiftNotifyDialog;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.activity.RoomInviteActivity;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.activity.RoomTopicActivity;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.adapter.MicroViewAdapter;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.box.LotteryBoxDialog;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.callback.BottomViewListenerWrapper;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.callback.SoftKeyBoardListener;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.dialog.CallUpDialog;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.dialog.RoomPrivateMsgDialog;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.face.DynamicFaceDialog;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.gift.GiftDialog;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.widget.ButtonItemFactory;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.widget.MicroView;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.widget.PairState;
import com.yuhuankj.tmxq.ui.me.charge.ChargeActivity;
import com.yuhuankj.tmxq.ui.me.noble.NobleBusinessManager;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderMiniGameInvited;
import com.yuhuankj.tmxq.ui.share.ShareDialog;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;
import com.yuhuankj.tmxq.ui.widget.BigListDataDialog;
import com.yuhuankj.tmxq.ui.widget.MicInListDialog;
import com.yuhuankj.tmxq.ui.widget.RoomMoreOpearDialog;
import com.yuhuankj.tmxq.ui.widget.RoomTopicDIalog;
import com.yuhuankj.tmxq.ui.widget.UserInfoDialog;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;

import cn.sharesdk.framework.Platform;
import io.reactivex.disposables.Disposable;

import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_CLOSE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_OPEN;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_ROOM_PAIR_CHANGE;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_ROOM_PAIR_END;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_ROOM_PAIR_START;

/**
 * 轰趴房间
 *
 * @author chenran
 * @date 2017/7/26
 */
@CreatePresenter(HomePartyRoomPresenter.class)
public class HomePartyRoomFragment extends BaseMvpFragment<IHomePartyRoomView, HomePartyRoomPresenter> implements View.OnClickListener,
        GiftDialog.OnGiftDialogBtnClickListener, ShareDialog.OnShareDialogItemClick, IHomePartyRoomView, MicroViewAdapter.OnMicroItemClickListener,
        MicroView.OnMicroGuideViewLocationListener, BaseQuickAdapter.OnItemClickListener, BottomViewListenerWrapper {
    private long myUid;

    private MusicPlayerView musicPlayerView;
    private ViewStub mVsMusicPlayer;
    private String shareName;

    public boolean isFristEnterRoom = true;

    private List<ActionDialogInfo> mActionDialogInfoList;
    private RoomBannerAdapter roomBannerAdapter;
    private FragmentAvRoomGameBinding gameBinding;
    private CustomAttachment giftAttachment;
    private Button buMicInListCount;
    private boolean micInListOption = true;

    private final String TAG = HomePartyRoomFragment.class.getSimpleName();
    private long getPairTime = 0;

    //红包相关
    private RelativeLayout rlRedPacket;
    private RelativeLayout rlRedPacketOpen;
    private RelativeLayout rlRedPacketOpenGet;
    private RelativeLayout rlRedPacketOpenNo;
    private TextView tvName;
    private TextView tvTimer;
    private CountDownTimer timer;
    //红包横幅
    private RelativeLayout rlRedPacketBanner;
    private ImageView imvBannerHeadImg;
    private TextView tvRedPacketBanner;
    //红包model层
    private RedPacketModel redPacketModel = new RedPacketModel();
    private RoomRedPacket roomRedPacket;//红包信息
    private SVGAImageView svRedPacketRain;
    //当前房间信息
    private RoomInfo mCurrentRoomInfo;
    private String lastGetRedId = "";

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_av_room_game;
    }

    private boolean isShowMoreOperaBtn = false;

    //获取房间历史红包
    private void getRoomHistoryRedPacket() {
        final RoomInfo currentRoom = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoom == null) {
            return;
        }
        new RedPacketModel().getRoomHistoryRedPacket(currentRoom.getRoomId(), new OkHttpManager.MyCallBack<ServiceResult<List<RoomRedPacket>>>() {
            @Override
            public void onError(Exception e) {
                LogUtils.e(e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<List<RoomRedPacket>> response) {
                if (response.isSuccess()) {
                    List<RoomRedPacket> redPackets = response.getData();
                    if (redPackets != null && redPackets.size() > 0) {
                        roomRedPacket = redPackets.get(0);
                        //显示右下角红包View
                        String nick = roomRedPacket.getNick();
                        tvName.setText(nick);
                        if (roomRedPacket.getType() == 1) {
                            //手气红包
                            rlRedPacket.setVisibility(View.VISIBLE);
                            tvTimer.setVisibility(View.GONE);
                            return;
                        }
                        long startTime = roomRedPacket.getRedPacketTime();
                        long curTime = MsgViewHolderMiniGameInvited.getCurTimeMillis();
                        long residue = curTime - startTime;
                        int timing = roomRedPacket.getTiming() * 1000;
                        if (residue > timing) {
                            //红包已经过期了
                            rlRedPacket.setVisibility(View.VISIBLE);
                            rlRedPacket.setEnabled(true);
                            tvTimer.setVisibility(View.GONE);
                            return;
                        }
                        residue = timing - residue;
                        residue = residue + (1000 - residue % 1000);
                        if (residue > 1200 * 1000) {
                            residue = 1200 * 1000;
                        }
                        rlRedPacket.setVisibility(View.VISIBLE);
                        rlRedPacket.setEnabled(false);
                        tvTimer.setVisibility(View.VISIBLE);
                        if (timer != null) {
                            timer.cancel();
                        }
                        timer = new CountDownTimer(residue, 1000) {
                            @Override
                            public void onTick(long l) {
                                long showTime = l / 1000;
                                tvTimer.setText(showTime + "s");
                            }

                            @Override
                            public void onFinish() {
                                tvTimer.setVisibility(View.GONE);
                                rlRedPacket.setEnabled(true);
                                //播放红包雨动画
                                if (roomRedPacket.getGoldNum() >= 10000) {
                                    playRedPacketRainAnimation(roomRedPacket.getRedEnvelopedRain());
                                }
                            }
                        };
                        timer.start();
                    }
                }
            }
        });
    }

    //领取红包
    private void receiveRedPacket() {
        if (mCurrentRoomInfo == null) {
            rlRedPacket.setEnabled(true);
            SingleToastUtil.showToast("领取红包失败");
            return;
        }
        if (roomRedPacket == null) {
            rlRedPacket.setEnabled(true);
            SingleToastUtil.showToast("领取红包失败");
            return;
        }
        getDialogManager().showProgressDialog(getActivity(), "请稍候...");
        redPacketModel.receiveRedPacket(roomRedPacket.getRedPacketId(), mCurrentRoomInfo.getRoomId(), new OkHttpManager.MyCallBack<ServiceResult<RoomRedPacketReceiveResult>>() {
            @Override
            public void onError(Exception e) {
                rlRedPacket.setEnabled(true);
                getDialogManager().dismissDialog();
                SingleToastUtil.showToast("领取红包失败");
            }

            @Override
            public void onResponse(ServiceResult<RoomRedPacketReceiveResult> response) {
                rlRedPacket.setEnabled(true);
                getDialogManager().dismissDialog();
                if (response.isSuccess() && response.getData() != null) {
                    if (roomRedPacket != null) {
                        lastGetRedId = roomRedPacket.getRedPacketId();
                    }
                    RoomRedPacketReceiveResult receiveResult = response.getData();
                    //抢到红包
                    ImageView imvHeadImg = rlRedPacketOpenGet.findViewById(R.id.imvHeadImg);
                    TextView tvName = rlRedPacketOpenGet.findViewById(R.id.tvName);
                    TextView tvGetCount = rlRedPacketOpenGet.findViewById(R.id.tvGetCount);
                    rlRedPacketOpenNo.setVisibility(View.GONE);
                    rlRedPacketOpenGet.setVisibility(View.VISIBLE);
                    String moneyStr = String.format("恭喜，你抢到了%d金币", receiveResult.getGoldNum());
                    tvGetCount.setText(moneyStr);
                    Glide.with(getActivity()).load(receiveResult.getAvatar()).into(imvHeadImg);
                    tvName.setText(receiveResult.getNick());
                    //隐藏右下角红包View
                    //rlRedPacket.setVisibility(View.GONE);
                    svRedPacketRain.stopAnimation();
                    svRedPacketRain.setVisibility(View.GONE);

                    playRedPacketOpenAnimation("red_packet_get.svga");
                } else if (response.getCode() == 100003) {
                    //没有抢到红包
                    RoomRedPacketReceiveResult receiveResult = response.getData();
                    ImageView imvHeadImg = rlRedPacketOpenNo.findViewById(R.id.imvHeadImg);
                    TextView tvName = rlRedPacketOpenNo.findViewById(R.id.tvName);
                    rlRedPacketOpenGet.setVisibility(View.GONE);
                    rlRedPacketOpen.setVisibility(View.VISIBLE);
                    Glide.with(getActivity()).load(receiveResult.getAvatar()).into(imvHeadImg);
                    tvName.setText(receiveResult.getNick());
                    //隐藏右下角红包View
                    //rlRedPacket.setVisibility(View.GONE);
                    svRedPacketRain.stopAnimation();
                    svRedPacketRain.setVisibility(View.GONE);
                    //显示红包抢光图片
                    Animation animation = new AlphaAnimation(0F, 1F);
                    animation.setDuration(700);
                    rlRedPacketOpenNo.startAnimation(animation);
                    rlRedPacketOpenNo.setVisibility(View.VISIBLE);
                } else {
                    SingleToastUtil.showToast(response.getErrorMessage());
                }
            }
        });
    }

    private View.OnLayoutChangeListener onLayoutChangeListener = new View.OnLayoutChangeListener() {
        @Override
        public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft,
                                   int oldTop, int oldRight, int oldBottom) {
            if (oldBottom - bottom == 0) {
                LogUtils.d(TAG, "onLayoutChange-不用滚动");
                return;
            }
            int showScrolled = oldBottom - bottom;
            LogUtils.d(TAG, "onLayoutChange-滚动距离showScrolled:" + showScrolled);
            if (null != gameBinding && null != gameBinding.svInput) {
                gameBinding.svInput.scrollBy(0, showScrolled);
            }
        }
    };
    private OnPkVoteInfoChangeListener onPkVoteInfoChangeListener;

    private void initRoomLotteryBox() {
        if (null == getActivity()) {
            return;
        }
        int mWidthPixels = getActivity().getResources().getDisplayMetrics().widthPixels;
        int mHeightPixels = getActivity().getResources().getDisplayMetrics().heightPixels;
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) gameBinding.ivRoomLotteryBox.getLayoutParams();
        int width1 = lp.width;
        int height1 = lp.height;
        int mh = mHeightPixels - height1 - DisplayUtility.dp2px(getActivity(), 98);
        int mw = mWidthPixels - width1 - DisplayUtility.dp2px(getActivity(), 9);
        lp.leftMargin = mw;
        lp.topMargin = mh;
        gameBinding.ivRoomLotteryBox.setLayoutParams(lp);

        TouchAndClickOperaUtils touchAndClickOperaUtils1 = new TouchAndClickOperaUtils(getActivity());
        touchAndClickOperaUtils1.setTargetView(gameBinding.ivRoomLotteryBox);
        touchAndClickOperaUtils1.setMarginButtom(DisplayUtility.dp2px(getActivity(), 20));
        touchAndClickOperaUtils1.setListener(new TouchAndClickOperaUtils.OnQuickClickListener() {
            @Override
            public void onQuickClick() {
                LotteryBoxDialog lotteryBoxDialog = LotteryBoxDialog.newInstance();
                lotteryBoxDialog.show(getChildFragmentManager(), "lotteryBoxDialog");

                //房间-打开挖宝界面
                StatisticManager.get().onEvent(getActivity(),
                        StatisticModel.EVENT_ID_ROOM_OPEN_BOX,
                        StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
            }
        });
    }

    @Override
    public void onFindViews() {
        gameBinding = DataBindingUtil.bind(mView);
        mVsMusicPlayer = mView.findViewById(R.id.vs_music_player);
        gameBinding.setClick(this);
        buMicInListCount = mView.findViewById(R.id.bu_mic_in_list_count);
        buMicInListCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMicInListDialog();
            }
        });
        //保证有未读私聊消息就展示红点
        changeState();
        gameBinding.messageView.setOnItemClickListener(this);
        gameBinding.wrpvDenoteProgress.setCycle(DisplayUtility.dp2px(getActivity(), 5));
        gameBinding.wrpvDenoteProgress.setWaveHeight(DisplayUtility.dp2px(getActivity(), 5));
        gameBinding.wrpvDenoteProgress.setColor(Color.parseColor("#864dff"));
        gameBinding.wrpvDenoteProgress.setAlpha(127);
        gameBinding.wrpvDenoteProgress.setTranslateX(6);
        gameBinding.wrpvDenoteProgress.setTimeInner(120);
        gameBinding.svInput.addOnLayoutChangeListener(onLayoutChangeListener);
        gameBinding.bannerActivity.setVisibility(View.GONE);
        gameBinding.bannerActivity.setEnabled(false);
        //红包view
        mCurrentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        svRedPacketRain = mView.findViewById(R.id.svRedPacketRain);
        rlRedPacket = mView.findViewById(R.id.rlRedPacket);
        tvName = mView.findViewById(R.id.tvName);
        tvTimer = mView.findViewById(R.id.tvTimer);
        rlRedPacketOpen = mView.findViewById(R.id.rlRedPacketOpen);
        rlRedPacketOpenGet = mView.findViewById(R.id.rlRedPacketOpenGet);
        rlRedPacketOpenNo = mView.findViewById(R.id.rlRedPacketOpenNo);
        //红包横幅
        rlRedPacketBanner = mView.findViewById(R.id.rlRedPacketBanner);
        imvBannerHeadImg = mView.findViewById(R.id.imvBannerHeadImg);
        tvRedPacketBanner = mView.findViewById(R.id.tvRedPacketBanner);
        //设置点击事件
        rlRedPacket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (lastGetRedId.equals(roomRedPacket.getRedPacketId())) {
                    SingleToastUtil.showToast("你已经领取过该红包");
                    return;
                }
                rlRedPacket.setEnabled(false);
                receiveRedPacket();
            }
        });
        rlRedPacketOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation animation = new AlphaAnimation(1F, 0F);
                animation.setDuration(500);
                rlRedPacketOpen.startAnimation(animation);
                rlRedPacketOpen.setVisibility(View.GONE);
            }
        });
        rlRedPacketBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getContext() == null){
                    return;
                }
                if (roomRedPacket == null) {
                    SingleToastUtil.showToast("红包信息为空");
                    return;
                }
                //跳转到红包发送者所在的房间
                if (roomRedPacket.getRoomId() != mCurrentRoomInfo.getRoomId()) {
                    RoomServiceScheduler.getInstance().enterOtherTypeRoomFromService(getContext(),roomRedPacket.getRoomUid(),roomRedPacket.getRoomType());
//                    AVRoomActivity.start(getContext(), roomRedPacket.getRoomUid());
                }
            }
        });

        //只有非牌照、房间类型为交友的房主,才显示召集令
        if (AvRoomDataManager.get().isRoomOwner() && null != AvRoomDataManager.get().mServiceRoominfo
                && AvRoomDataManager.get().mServiceRoominfo.showCallUp()) {
            gameBinding.imvCallUp.setVisibility(View.VISIBLE);
        } else {
            gameBinding.imvCallUp.setVisibility(View.GONE);
        }
        gameBinding.imvCallUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCurrentRoomInfo == null) {
                    toast("房间信息为空");
                    return;
                }

                if (null != getActivity()) {
                    CallUpDialog callUprDialog = new CallUpDialog(getActivity(), mCurrentRoomInfo.getRoomId());
                    callUprDialog.show();

                    //交友方，左上角召集令，控件点击统计
                    StatisticManager.get().onEvent(getActivity(),
                            StatisticModel.EVENT_ID_ROOM_CALL_UP_CLICK,
                            StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
                }
            }
        });
        //获取历史红包
        getRoomHistoryRedPacket();
        gameBinding.ivMoreOperation.setOnClickListener(this);
        refreshMoreOperaBtnShowStatus();
    }

    //播放红包打开动画
    private void playRedPacketOpenAnimation(String srcPath) {
        if (TextUtils.isEmpty(srcPath)) {
            return;
        }
        //rlRedPacketOpen.setVisibility(View.VISIBLE);
        svRedPacketRain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        svRedPacketRain.setCallback(new SVGACallback() {
            @Override
            public void onPause() {

            }

            @Override
            public void onFinished() {
                Animation animation = new AlphaAnimation(0F, 1F);
                animation.setDuration(700);
                rlRedPacketOpen.startAnimation(animation);
                rlRedPacketOpen.setVisibility(View.VISIBLE);
            }

            @Override
            public void onRepeat() {
                svRedPacketRain.stopAnimation();
                svRedPacketRain.setVisibility(View.GONE);
            }

            @Override
            public void onStep(int i, double v) {

            }
        });
        playSvga(srcPath);
    }

    //播放红包雨动画
    private void playRedPacketRainAnimation(String srcPath) {
        if (TextUtils.isEmpty(srcPath)) {
            return;
        }
        svRedPacketRain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                svRedPacketRain.stopAnimation();
                svRedPacketRain.setVisibility(View.GONE);
                receiveRedPacket();
            }
        });
        svRedPacketRain.setCallback(new SVGACallback() {
            @Override
            public void onPause() {

            }

            @Override
            public void onFinished() {

            }

            @Override
            public void onRepeat() {
                svRedPacketRain.stopAnimation();
                svRedPacketRain.setVisibility(View.GONE);
            }

            @Override
            public void onStep(int i, double v) {

            }
        });
        playSvga(srcPath);
    }

    private void playSvga(String srcPath) {
        if (srcPath.startsWith("http")) {
            try {
                URL url = new URL(srcPath);
                BuglyLog.d(TAG, "play-svga anim, url:" + srcPath);
                new SVGAParser(getActivity()).parse(url, new SVGAParser.ParseCompletion() {
                    @Override
                    public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {
                        svRedPacketRain.setVisibility(View.VISIBLE);
                        SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);
                        svRedPacketRain.setImageDrawable(drawable);
                        svRedPacketRain.startAnimation();
                    }

                    @Override
                    public void onError() {

                    }
                });
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        } else {
            BuglyLog.d(TAG, "play-svga anim, url:" + srcPath);
            new SVGAParser(getActivity()).parse(srcPath, new SVGAParser.ParseCompletion() {
                @Override
                public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {
                    svRedPacketRain.setVisibility(View.VISIBLE);
                    SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);
                    svRedPacketRain.setImageDrawable(drawable);
                    svRedPacketRain.startAnimation();
                }

                @Override
                public void onError() {

                }
            });
        }
    }

    //显示红包横幅动画
    private void showBannerAnima() {
        if (roomRedPacket.getGoldNum() < 10000) {
            return;
        }
        //显示红包横幅
        Glide.with(getActivity()).load(roomRedPacket.getAvatar()).into(imvBannerHeadImg);
        String nickName = roomRedPacket.getNick();
        if (TextUtils.isEmpty(nickName)) {
            nickName = "";
        }
        if (nickName.length() > 6) {
            nickName = nickName.substring(0, 6) + "...";
        }
        String roomName = roomRedPacket.getTitle();
        if (TextUtils.isEmpty(roomName)) {
            roomName = "";
        }
        if (roomName.length() > 6) {
            roomName = roomName.substring(0, 6) + "...";
        }
        String bannerStr = String.format("哇，%s在%s（id:%s）里发了一个大红包，快来抢吧！", nickName, roomName, roomRedPacket.getRoomNo() + "");
        SpannableStringBuilder ssb = new SpannableStringBuilder(bannerStr);
        ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#FFED21")), 2, 2 + nickName.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        tvRedPacketBanner.setText(ssb);
        rlRedPacketBanner.setVisibility(View.VISIBLE);
        //红包横幅显示动画
        TranslateAnimation translateAni = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, -1f, Animation.RELATIVE_TO_SELF,
                0f, Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_SELF, 0);
        translateAni.setDuration(2000);
        translateAni.setRepeatCount(0);
        translateAni.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                handler.sendEmptyMessageDelayed(MSG_WHAT_HIDE_ROOM_RED_PACKET_BANNER, 3000);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        rlRedPacketBanner.startAnimation(translateAni);
    }

    private void sendRoomPairNotifyMessage(Json json) {
        if (json == null || json.key_names().length == 0) {
            return;
        }
        int showPairIndex = 0;
        while (showPairIndex < json.key_names().length) {
            Json pairUser = json.json_ok(json.key_names()[showPairIndex++]);
            LogUtils.d(TAG, "checkForShowLoverUpMicAnim-发送相亲结果 showPairIndex:" + showPairIndex + " pairUser:" + pairUser);
            if (pairUser.key_names().length != 2) {
                return;
            }
            Json user1 = pairUser.json_ok(pairUser.key_names()[0]);
            Json user2 = pairUser.json_ok(pairUser.key_names()[1]);
            LoverUpMicAnimAttachment customAttachment = new LoverUpMicAnimAttachment(CustomAttachment.CUSTOM_MSG_ROOM_PAIR_SUC_ROOM_NOTICE,
                    CustomAttachment.CUSTOM_MSG_ROOM_PAIR_SUC_ROOM_NOTICE);
            customAttachment.setFirstNick(user1.str(Constants.USER_NICK));
            customAttachment.setFirstUid(user1.str(Constants.USER_UID));
            customAttachment.setSecondUid(user2.str(Constants.USER_UID));
            customAttachment.setSecondNick(user2.str(Constants.USER_NICK));

            if (user1.has(Constants.USER_MEDAL_ID)) {
                customAttachment.setFirstVipId(user1.num(Constants.USER_MEDAL_ID));
            }
            if (user1.has(Constants.USER_MEDAL_DATE)) {
                customAttachment.setFirstVipDate(user1.num(Constants.USER_MEDAL_DATE));
            }
            if (user1.has(Constants.NOBLE_INVISIABLE_ENTER_ROOM)) {
                customAttachment.setFirstIsInvisible(user1.num(Constants.NOBLE_INVISIABLE_ENTER_ROOM) == 1);
            }

            if (user2.has(Constants.USER_MEDAL_ID)) {
                customAttachment.setSecondVipId(user2.num(Constants.USER_MEDAL_ID));
            }
            if (user2.has(Constants.USER_MEDAL_DATE)) {
                customAttachment.setSecondVipDate(user2.num(Constants.USER_MEDAL_DATE));
            }
            if (user2.has(Constants.NOBLE_INVISIABLE_ENTER_ROOM)) {
                customAttachment.setSecondIsInvisible(user2.num(Constants.NOBLE_INVISIABLE_ENTER_ROOM) == 1);
            }

            ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                    AvRoomDataManager.get().mCurrentRoomInfo.getRoomId() + "",
                    customAttachment);
//            CoreManager.getCore(IMRoomCore.class).sendMessage(message);
            IMNetEaseManager.get().addMessagesImmediately(message);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        initPair();
    }

    private final int MSG_WHAT_HIDE_ENTER_ROOM_ANIM = 6;

    /**
     * 重连，再次回到页面都会请求
     */
    private void initPair() {
        if (gameBinding == null) {
            return;
        }
        //避免重复请求
        long time = System.currentTimeMillis();
        if (time - this.getPairTime < 500) {
            return;
        }

        gameBinding.microView.getPairStateView().initState();
        this.getPairTime = time;
        PairState pairStateView = gameBinding.microView.getPairStateView();

        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (roomInfo != null && pairStateView != null) {
            String roomId = "" + roomInfo.getRoomId();
            if (roomInfo.tagId == 6) {
                CoreManager.getCore(IPairCore.class).getPairInfo(roomId);
            }

            pairStateView.setiPairStateAction(new PairState.IPairStateAction() {
                @Override
                public void onPairAction(boolean isPairStart) {

                    String msg = isPairStart ? "是否结束本轮相亲?" : "是否开始新的一轮相亲?";
                    getDialogManager().showOkCancelDialog(
                            msg, true,
                            new DialogManager.AbsOkDialogListener() {
                                @Override
                                public void onOk() {
                                    if (isPairStart) {
                                        CoreManager.getCore(IPairCore.class).stopPair(roomId);
                                    } else {
                                        CoreManager.getCore(IPairCore.class).startPair(roomId);
                                    }
                                }
                            });
                }
            });
        }
    }

    private void updateMicCharm(int charmSwitch) {
        if (gameBinding != null && gameBinding.microView != null && gameBinding.microView.getAdapter() != null) {
            gameBinding.microView.getAdapter().setShowCharm(charmSwitch == 1);
        }
    }

    private void ownerUpMic() {
        final RoomInfo currentRoom = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoom == null) {
            return;
        }
        final String currentUid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        if (AvRoomDataManager.get().isRoomOwner(currentUid)) {
            onOwnerUpMicroClick(-1, currentRoom.getUid());
        }
    }

    /**
     * 监听消息和房间事件
     */
    private void subscribeMsgAndRoomEvent() {
        //当前观察者只关注当前房间的
        Disposable subscribe = IMNetEaseManager.get().getChatRoomMsgFlowable()
                .compose(bindToLifecycle())
                .subscribe(messages -> {
                    if (messages.size() == 0 || null == messages.get(0)) {
                        return;
                    }
                    for (ChatRoomMessage chatRoomMessage : messages) {
                        //拦截其他房间的信息
                        if (IMNetEaseManager.get().filterOtherRoomMsg(chatRoomMessage)) {
                            LogUtils.d(TAG, "subscribeMsgAndRoomEvent - 其他房间信息_被拦截，sessionId = " + chatRoomMessage.getSessionId());
                            return;
                        }

                        checkForbidSendMsg(chatRoomMessage);
                        setGiftMsg(chatRoomMessage);
                        //dealLoverUpMicAnimMsg(chatRoomMessage);
                        dealUpMicroMsg(chatRoomMessage);
                    }

                });
        mCompositeDisposable.add(subscribe);

        Disposable subscribeRoomEvent = IMNetEaseManager.get().getChatRoomEventObservable()
                .compose(bindToLifecycle())
                .subscribe(roomEvent -> {
                    if (roomEvent == null) {
                        return;
                    }
                    if (roomEvent.getEvent() == RoomEvent.RECEIVE_MSG && null != roomEvent.getChatRoomMessage()) {//消息
                        //广场/房间内消息列表
                        ChatRoomMessage chatRoomMessage = roomEvent.getChatRoomMessage();
                        //拦截其他房间的信息
                        if (IMNetEaseManager.get().filterOtherRoomMsg(chatRoomMessage)) {
                            LogUtils.d(TAG, "subscribeMsgAndRoomEvent - 其他房间信息_被拦截，sessionId = " + chatRoomMessage.getSessionId());
                            return;
                        }

                        comeMsg(chatRoomMessage);
                        setGiftMsg(chatRoomMessage);
                        checkForbidSendMsg(chatRoomMessage);
                        //dealLoverUpMicAnimMsg(chatRoomMessage);
                        dealUpMicroMsg(chatRoomMessage);
                    } else {//事件
                        dealRoomEvent(roomEvent);
                    }
                });
        mCompositeDisposable.add(subscribeRoomEvent);
    }

    private final int MSG_WHAT_HIDE_OPEN_NOBLE_ANIM = 7;

    /**
     * 控制公屏消息是否可以发送消息
     *
     * @param chatRoomMessage
     */
    private void checkForbidSendMsg(ChatRoomMessage chatRoomMessage) {
        if (chatRoomMessage.getMsgType() == MsgTypeEnum.custom) {
            if (chatRoomMessage.getAttachment() instanceof CustomAttachment) {
                if (((CustomAttachment) chatRoomMessage.getAttachment()).getSecond() == CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_OPEN) {
                    gameBinding.bottomView.setInputMsgBtnEnable(true);
                } else if (((CustomAttachment) chatRoomMessage.getAttachment()).getSecond() == CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_CLOSE) {
                    gameBinding.bottomView.setInputMsgBtnEnable(false);
                }
            }
        }
    }

    @CoreEvent(coreClientClass = IPairClient.class)
    public void onResponsePairInfo(Json json) {
        if (gameBinding == null) {
            return;
        }
        long l = json.num_l("duration");
        //重连，连接成功，
        // 如果仍处于相亲过程，则会继续接受服务器下发的相亲选中状态通知，不必担心状态显示的问题
        //如果相亲结束，则需要清理选择信息
        //如果刚开始相亲过程，同仍处于相亲过程中一样,不处理，等待通知
        if (l > 0) {
            PairState pairStateView = gameBinding.microView.getPairStateView();
            pairStateView.startPair(l * 1000);
        } else {
            PairState pairStateView = gameBinding.microView.getPairStateView();
            pairStateView.stopPair(false);
        }
    }

    private void comeMsg(ChatRoomMessage chatRoomMessage) {
        if (chatRoomMessage.getMsgType() == MsgTypeEnum.notification) {
            if (null != handler) {
                handler.removeMessages(MSG_WHAT_HIDE_ENTER_ROOM_ANIM);
            }
            String msgNick = "";
            String aliasIconUrl = "";
//            String newUserContent = "";
            int experLevel = -1;
            boolean isInvisible = false;
            try {
                experLevel = (Integer) chatRoomMessage.getChatRoomMessageExtension().getSenderExtension().get(Constants.USER_EXPER_LEVEL);
                msgNick = (String) chatRoomMessage.getChatRoomMessageExtension().getSenderExtension().get(Constants.USER_NICK_IN_ROOM);
//                newUserContent = (String) chatRoomMessage.getChatRoomMessageExtension().getSenderExtension().get("is_new_user");
                aliasIconUrl = (String) chatRoomMessage.getChatRoomMessageExtension().getSenderExtension().get(Constants.ALIAS_ICON_URL);
                isInvisible = (int) chatRoomMessage.getChatRoomMessageExtension().getSenderExtension().get(Constants.NOBLE_INVISIABLE_ENTER_ROOM) == 1;
            } catch (Exception e) {
                e.printStackTrace();
                experLevel = -1;
            }
            LogUtils.d(TAG, "comeMsg-msgNick:" + msgNick + " isInvisible:" + isInvisible);
            if (!showNobleEnterRoomAnim(chatRoomMessage, experLevel, msgNick, aliasIconUrl, isInvisible)) {
                showEnterRoomAnim(chatRoomMessage, experLevel, msgNick, aliasIconUrl);
            }
        }
    }

    /**
     * 贵族入场动画
     *
     * @param chatRoomMessage
     * @param experLevel
     * @param msgNick
     * @return
     */

    private boolean showNobleEnterRoomAnim(ChatRoomMessage chatRoomMessage, int experLevel, String msgNick,
                                           String aliasIconUrl, boolean isInvisible) {
        int enterAnimBgResId = 0;
        if (chatRoomMessage.getChatRoomMessageExtension() != null) {
            int medalId = 0;
            int medalDate = 0;
            Map<String, Object> map = chatRoomMessage.getChatRoomMessageExtension().getSenderExtension();
            if (null != map) {
                if (map.containsKey(Constants.USER_MEDAL_ID)) {
                    medalId = (Integer) map.get(Constants.USER_MEDAL_ID);
                }
                if (map.containsKey(Constants.USER_MEDAL_DATE)) {
                    medalDate = (Integer) map.get(Constants.USER_MEDAL_DATE);
                }
                enterAnimBgResId = NobleBusinessManager.getNobleEnterRoomAnimBgResId(medalId, medalDate);
                if (enterAnimBgResId > 0) {
                    gameBinding.llNobleEnterRoomAnim.setVisibility(View.VISIBLE);
                    gameBinding.llEnterRoomAnim.setVisibility(View.GONE);

                    gameBinding.llNobleEnterNotify.setBackgroundResource(enterAnimBgResId);

                    gameBinding.ivNobleLevelIcon.setVisibility(experLevel > 0 ? View.VISIBLE : View.GONE);
                    if (experLevel > 0 && GlideContextCheckUtil.checkContextUsable(mContext)) {
                        ImageLoadUtils.loadImage(mContext, UriProvider.getCFImgUrl(experLevel), gameBinding.ivNobleLevelIcon);
                    }

                    if (!TextUtils.isEmpty(aliasIconUrl) && GlideContextCheckUtil.checkContextUsable(mContext)) {
                        gameBinding.ivNobleNewUserIcon.setVisibility(View.VISIBLE);
                        GlideApp.with(mContext).load(aliasIconUrl).dontAnimate().into(gameBinding.ivNobleNewUserIcon);
                    } else {
                        gameBinding.ivNobleNewUserIcon.setVisibility(View.GONE);
                    }

                    if (!TextUtils.isEmpty(msgNick)) {
                        if (msgNick.length() > 6 && null != getActivity()) {
                            msgNick = getActivity().getResources().getString(R.string.nick_length_max_six, msgNick.substring(0, 6));
                        }
                        gameBinding.tvNobleEnterNotify.setText(msgNick);
                        gameBinding.tvNobleEnterNotify.setTextColor(NobleBusinessManager.getNobleRoomNickColor(medalId, medalDate, isInvisible, Color.WHITE, msgNick));
                    }
                    if (null != handler) {
                        handler.sendEmptyMessageDelayed(MSG_WHAT_HIDE_ENTER_ROOM_ANIM, 3000L);
                    }
                }
            }
        }

        return enterAnimBgResId > 0;
    }

    private SvgaAciton svgaAciton;

    public void setSvgaAciton(SvgaAciton svgaAciton) {
        this.svgaAciton = svgaAciton;
    }

    public interface SvgaAciton {
        void showCar(String url);

        void showPair(Json json);
    }

    private void setGiftMsg(ChatRoomMessage chatRoomMessage) {
        if (chatRoomMessage.getMsgType() == MsgTypeEnum.custom) {
            CustomAttachment attachment = (CustomAttachment) chatRoomMessage.getAttachment();
            if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT || attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT) {
                gameBinding.msgContainer.setVisibility(View.VISIBLE);
                if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT) {
                    giftAttachment = attachment;
                    gameBinding.setAttachment(giftAttachment);
                } else {
                    giftAttachment = attachment;
                    gameBinding.setAttachment(giftAttachment);

                }
            }
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        // 释放公屏和麦上的所有信息信息和动画
        long roomUid = intent.getLongExtra(Constants.ROOM_UID, 0);
        if (roomUid != 0 && roomUid != AvRoomDataManager.get().mCurrentRoomInfo.getUid()) {
            releaseView();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (musicPlayerView != null) {
            musicPlayerView.updateVoiceValue();
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onSetListener() {
        gameBinding.bottomView.setBottomViewListener(this);
        gameBinding.flInput.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gameBinding.etMsg.clearFocus();
                gameBinding.flInput.setVisibility(View.GONE);
                hideKeyBoard();
                return false;
            }
        });
        softKeyboardListener();
    }

    private void onOwnerUpMicroClick(final int micPosition, final long currentUid) {
        RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoByMicPosition(micPosition);
        if (roomQueueInfo == null) {
            return;
        }
        getMvpPresenter().upMicroPhone(micPosition, currentUid + "", false);
    }

    /**
     * 显示活动图标
     */
    public void showActivity(List<ActionDialogInfo> dialogInfo) {
        mActionDialogInfoList = dialogInfo;
        if (!ListUtils.isListEmpty(dialogInfo)) {
            gameBinding.bannerActivity.setVisibility(View.VISIBLE);
            roomBannerAdapter = new RoomBannerAdapter(dialogInfo, getActivity());
            roomBannerAdapter.setOnRoomBannerClickListener(new RoomBannerAdapter.OnRoomBannerClickListener() {
                @Override
                public void onGameBannerClicked(ActionDialogInfo actionDialogInfo) {
                    LogUtils.d(TAG, "onGameBannerClicked-actionDialogInfo:" + actionDialogInfo);
                    if (null != getActivity()) {
                        CommonWebViewActivity.start(getActivity(), actionDialogInfo.getSkipUrl());
                        onRoomBannerClick();
                    }
                }
            });
            gameBinding.bannerActivity.setAdapter(roomBannerAdapter);
            if (dialogInfo.size() > 1) {
                if (null != getActivity()) {
                    gameBinding.bannerActivity.setHintView(new RoomBannerShapeHintView(getActivity()));
                }
                gameBinding.bannerActivity.setPlayDelay(3000);
                gameBinding.bannerActivity.setAnimationDurtion(500);
            } else {
                if (null != getActivity()) {
                    gameBinding.bannerActivity.setHintView(new ColorPointHintView(getActivity(), Color.TRANSPARENT, Color.TRANSPARENT));
                }
            }
        } else {
            gameBinding.bannerActivity.setVisibility(View.GONE);
        }
    }

    private void releaseView() {
        gameBinding.messageView.removeCallbacks(genAttentionMsgRunnable);
        gameBinding.messageView.release();
        gameBinding.microView.release();
        if (musicPlayerView != null) {
            musicPlayerView.release();
        }
        gameBinding.svInput.removeOnLayoutChangeListener(onLayoutChangeListener);
        gameBinding.flInput.removeCallbacks(showKeyBoardRunable);
    }

    private void updateView() {
        // 是否可以显示一起玩的骰子活动
//        playTogether.setVisibility((AvRoomDataManager.get().isRoomAdmin()
//                || AvRoomDataManager.get().isRoomOwner()) ?
//                View.VISIBLE :
//                View.GONE);
        // 更新底栏
        showBottomViewForDifRole();
    }

    /**
     * 根据角色显示不同的状态
     */
    private void showBottomViewForDifRole() {
        boolean isOnMic = AvRoomDataManager.get().isOnMic(myUid);
        // 更新播放器界面
        RoomInfo mCurrentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (mCurrentRoomInfo == null) {
            return;
        }
        if (isOnMic) {
            if (musicPlayerView == null) {
                musicPlayerView = (MusicPlayerView) mVsMusicPlayer.inflate();
            }
            musicPlayerView.setVisibility(View.VISIBLE);
            musicPlayerView.setImageBg(mCurrentRoomInfo.getBackPic());
        }
        if (musicPlayerView != null) {
            musicPlayerView.setVisibility(isOnMic ? View.VISIBLE : View.GONE);
        }
        if (isOnMic) {
            gameBinding.bottomView.showHomePartyUpMicBottom();
        } else {
            gameBinding.bottomView.showHomePartyDownMicBottom();
        }

        boolean isShowLotteryBox = AvRoomDataManager.get().mServiceRoominfo != null
                && AvRoomDataManager.get().mServiceRoominfo.isLotteryBoxOption();
        gameBinding.ivRoomLotteryBox.setVisibility(isShowLotteryBox ? View.VISIBLE : View.GONE);
        updateRemoteMuteBtn();
    }

    private void updateMicBtn() {
        RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoomInfo != null) {
            if (AgoraEngineManager.get().isAudienceRole) {
                gameBinding.bottomView.setMicBtnEnable(false);
                gameBinding.bottomView.setMicBtnOpen(false);
            } else {
                if (AgoraEngineManager.get().isMute) {
                    gameBinding.bottomView.setMicBtnEnable(true);
                    gameBinding.bottomView.setMicBtnOpen(false);
                } else {
                    gameBinding.bottomView.setMicBtnEnable(true);
                    gameBinding.bottomView.setMicBtnOpen(true);
                }
            }
        } else {
            gameBinding.bottomView.setMicBtnEnable(false);
            gameBinding.bottomView.setMicBtnOpen(false);
        }
    }

    private void updateRemoteMuteBtn() {
        if (AvRoomDataManager.get().mCurrentRoomInfo != null) {
            gameBinding.bottomView.setRemoteMuteOpen(!AgoraEngineManager.get().isRemoteMute);
        }
    }

    public void onMicStateChanged() {
        updateMicBtn();
    }

    @CoreEvent(coreClientClass = IIMMessageCoreClient.class)
    public void onReceiveRecentContactChanged(List<RecentContact> imMessages) {
        LogUtils.d(TAG, "onReceiveRecentContactChanged-imMessages:" + imMessages);
        //有新的联系人消息
        changeState();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo userInfo) {
        LogUtils.d(TAG, "onCurrentUserInfoUpdate-userInfo:" + userInfo);
        //切换用户
        changeState();
    }

    /**
     * 更新新消息提示红点显示状态
     */
    private void changeState() {
        int unreadCount = CoreManager.getCore(IIMMessageCore.class).queryUnreadMsg();
        LogUtils.d(TAG, "changeState-unreadCount:" + unreadCount);
        gameBinding.bottomView.showMsgMark(unreadCount > 0);
    }

    /**
     * 是否已经生成过关注房主消息到消息列表
     */
    private boolean hasGenAttentionMsg = false;

    /**
     * 进入房间的时间 毫秒
     */
    private long timeInRoom = 0L;

    /**
     * 生成本地分享房间消息
     */
    public void genShareRoomMsg() {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (null != roomInfo) {
            LogUtils.d(TAG, "genShareRoomMsg-roomUid:" + roomInfo.getRoomId());
            CustomAttachment customAttachment = new CustomAttachment();
            customAttachment.setExperLevel(-1);
            customAttachment.setFirst(CustomAttachment.CUSTOM_MSG_ROOM_SHARE);
            ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                    String.valueOf(roomInfo.getRoomId()), customAttachment);
//            CoreManager.getCore(IMRoomCore.class).sendMessage(message);
            List<ChatRoomMessage> messages = new ArrayList<>();
            messages.add(message);
            gameBinding.messageView.onCurrentRoomReceiveNewMsg(messages);
        }

    }

    /**
     * 生成本地红包完成消息
     */
    public void genRedPacketFinishRoomMsg() {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (null != roomInfo) {
            CustomAttachment customAttachment = new CustomAttachment();
            customAttachment.setExperLevel(-1);
            customAttachment.setFirst(CustomAttachment.CUSTOM_MSG_ROOM_RED_PACKET_FINISH);
            ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                    String.valueOf(roomInfo.getRoomId()), customAttachment);
//            CoreManager.getCore(IMRoomCore.class).sendMessage(message);
            List<ChatRoomMessage> messages = new ArrayList<>();
            messages.add(message);
            gameBinding.messageView.onCurrentRoomReceiveNewMsg(messages);
        }

    }

    /**
     * 生成本地关注房主消息
     */
    public void genAttentionMsg() {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (null != roomInfo && isFristEnterRoom) {
            LogUtils.d(TAG, "genAttentionMsg-roomUid:" + roomInfo.getRoomId());
            CustomAttachment customAttachment = new CustomAttachment();
            customAttachment.setExperLevel(-1);
            customAttachment.setFirst(CustomAttachment.CUSTOM_MSG_ROOM_ATTENTION);
            ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                    String.valueOf(roomInfo.getRoomId()), customAttachment);
//            CoreManager.getCore(IMRoomCore.class).sendMessage(message);
            List<ChatRoomMessage> messages = new ArrayList<>();
            messages.add(message);
            gameBinding.messageView.refreshLikeMsgStatus(false, false);
            gameBinding.messageView.onCurrentRoomReceiveNewMsg(messages);
        }
    }

    //------------------------------IShareCoreClient----------------------------------
    @CoreEvent(coreClientClass = IShareCoreClient.class)
    public void onShareRoom() {
        toast("分享成功");
    }

    @CoreEvent(coreClientClass = IShareCoreClient.class)
    public void onShareRoomFail() {
        toast("分享失败，请重试");
    }

    @CoreEvent(coreClientClass = IShareCoreClient.class)
    public void onShareRoomCancel() {
        getDialogManager().dismissDialog();
    }

    /**
     * 普通用户入场动画
     *
     * @param chatRoomMessage
     * @param experLevel
     * @param
     * @param msgNick
     */
    private void showEnterRoomAnim(ChatRoomMessage chatRoomMessage, int experLevel, String msgNick, String aliasIconUrl) {
        gameBinding.llNobleEnterRoomAnim.setVisibility(View.GONE);

        int enterAnimBgResId = 0;
        if (experLevel >= 91) {
            enterAnimBgResId = R.drawable.ic_enter_room_anima_bg100;
        } else if (experLevel >= 81) {
            enterAnimBgResId = R.drawable.ic_enter_room_anima_bg90;
        } else if (experLevel >= 71) {
            enterAnimBgResId = R.drawable.ic_enter_room_anima_bg80;
        } else if (experLevel >= 61) {
            enterAnimBgResId = R.drawable.ic_enter_room_anima_bg70;
        } else if (experLevel >= 51) {
            enterAnimBgResId = R.drawable.ic_enter_room_anima_bg60;
        } else if (experLevel >= 41) {
            enterAnimBgResId = R.drawable.ic_enter_room_anima_bg50;
        } else if (experLevel >= 31) {
            enterAnimBgResId = R.drawable.ic_enter_room_anima_bg40;
        } else if (experLevel >= 21) {
            enterAnimBgResId = R.drawable.ic_enter_room_anima_bg30;
        } else if (experLevel >= 11) {
            enterAnimBgResId = R.drawable.ic_enter_room_anima_bg20;
        } else if (experLevel >= 10) {
            enterAnimBgResId = R.drawable.ic_enter_room_anima_bg10;
        } else {
            return;
        }

        gameBinding.llNoti.setBackgroundResource(enterAnimBgResId);

        gameBinding.levelIcon.setVisibility(experLevel > 0 ? View.VISIBLE : View.GONE);
        if (experLevel > 0 && GlideContextCheckUtil.checkContextUsable(mContext)) {
            ImageLoadUtils.loadImage(mContext, UriProvider.getCFImgUrl(experLevel), gameBinding.levelIcon);
        }
        gameBinding.ivNewUserIcon.setVisibility(View.GONE);
//        if (!TextUtils.isEmpty(aliasIconUrl)) {
//            gameBinding.ivNewUserIcon.setVisibility(View.VISIBLE);
//            GlideApp.with(getActivity()).load(aliasIconUrl).dontAnimate().into(gameBinding.ivNewUserIcon);
//        } else {
//            gameBinding.ivNewUserIcon.setVisibility(View.GONE);
//        }

        if (!TextUtils.isEmpty(msgNick)) {
            if (msgNick.length() > 6 && null != getActivity()) {
                msgNick = getActivity().getResources().getString(R.string.nick_length_max_six, msgNick.substring(0, 6));
            }
            gameBinding.tvNotify.setText(msgNick);
        }
//        if (null != handler) {
//            handler.sendEmptyMessageDelayed(MSG_WHAT_HIDE_ENTER_ROOM_ANIM, 3000L);
//        }
        gameBinding.llEnterRoomAnim.setVisibility(View.VISIBLE);
        startEnterAnimation(gameBinding.llEnterRoomAnim, new AnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
                super.onAnimationEnd(animation);
                gameBinding.llEnterRoomAnim.setVisibility(View.GONE);
            }
        });

    }

    /**
     * 做弹幕动画
     */
    public void startEnterAnimation(View view, AnimationListener listener) {
        float inInterval = 0.16f;
        float inSpeed = 3f;
        float inValue = inInterval * inSpeed;

        float middleInterval = 0.95f;
        float middleSpeed = 0.06f;
        float middleValue = inValue + (middleInterval - inInterval) * middleSpeed;

        float outSpeed = 2.65f;
        TranslateAnimation animation = new TranslateAnimation(ScreenUtil.getDisplayWidth(), -getContext().getResources()
                .getDimensionPixelOffset(R.dimen.layout_come_msg_width), 0, 0);
        animation.setDuration(4000);
        animation.setInterpolator(v -> {
            if (v < inInterval) {
                return inSpeed * v;
            } else if (v < middleInterval) {
                return inValue + middleSpeed * (v - inInterval);
            } else {
                return middleValue + outSpeed * (v - middleInterval);
            }
        });
        animation.setAnimationListener(listener);
        view.startAnimation(animation);
    }

    @Override
    public void initiate() {
        AvRoomDataManager.get().setMinimize(false);
        micInListOption = CoreManager.getCore(VersionsCore.class).getConfigData().num("micInListOption") == 1;
        subscribeMsgAndRoomEvent();
        myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();

        updateView();
        updateMicBtn();
        MicroViewAdapter microViewAdapter = gameBinding.microView.getAdapter();
        if (microViewAdapter != null) {
            microViewAdapter.setOnMicroItemClickListener(this);
        }
        gameBinding.microView.setMicroGuideViewLocationListener(this);
        gameBinding.bannerActivity.setAlpha(200);

//        Disposable disposable = IMNetEaseManager.get().getIMRoomEventObservable()
//                .compose(bindToLifecycle())
//                .subscribe(new Consumer<RoomEvent>() {
//                    @Override
//                    public void accept(RoomEvent roomEvent) throws Exception {
//                        dealRoomEvent(roomEvent);
//                    }
//                });
//        mCompositeDisposable.add(disposable);
        //房主自动上麦
        ownerUpMic();
        final RoomInfo mCurrentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (mCurrentRoomInfo != null) {
            CoreManager.getCore(IAVRoomCore.class).getRoomCharm(myUid, mCurrentRoomInfo.getRoomId());
            updateMicCharm(mCurrentRoomInfo.getCharmSwitch());
        }
        RoomInfo serverRoomInfo = AvRoomDataManager.get().mServiceRoominfo;
        if (null != serverRoomInfo) {
            lastGiftDetonatingProgress = serverRoomInfo.getDetonatingState();
            showGiftDetonatingProgress(lastGiftDetonatingProgress, serverRoomInfo.getDetonatingDuration(), true);
            if (100 <= lastGiftDetonatingProgress) {
                updateCurrRoomGiftDetoanteStatus(serverRoomInfo.getDetonatingUid(), serverRoomInfo.getDetonatingNick(),
                        serverRoomInfo.getDetonatingAvatar(), serverRoomInfo.getDetonatingGifts(),
                        serverRoomInfo.getDetonatingTime(), serverRoomInfo.getDetonatingDuration(),
                        serverRoomInfo.getDetonatingState());
            }
            getMvpPresenter().getFortuneRankTopData(serverRoomInfo.getUid(), serverRoomInfo.getType());
        }
        initPair();

        final UserInfo selfUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (null != selfUserInfo && null != mCurrentRoomInfo && mCurrentRoomInfo.getUid() != selfUserInfo.getUid()) {
            timeInRoom = System.currentTimeMillis();
            gameBinding.messageView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //查询是否已经关注
                    CoreManager.getCore(IPraiseCore.class).isPraised(selfUserInfo.getUid(), mCurrentRoomInfo.getUid());
                    if (isFristEnterRoom) {
                        genShareRoomMsg();
                    }
                }
            }, 1000L);
        }

        initAnnualRoomRankView();
        CoreManager.getCore(VersionsCore.class).requestSensitiveWord();

        initRoomLotteryBox();
    }

    private Runnable showKeyBoardRunable = new Runnable() {
        @Override
        public void run() {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm == null || null == gameBinding || null == gameBinding.etMsg) {
                return;
            }
            imm.showSoftInput(gameBinding.etMsg, InputMethodManager.SHOW_FORCED);
        }
    };
    private SoftKeyBoardListener.OnSoftKeyBoardChangeListener
            mOnSoftKeyBoardChangeListener = new SoftKeyBoardListener.OnSoftKeyBoardChangeListener() {
        @Override
        public void keyBoardShow(int height) {
            LogUtils.d(TAG, "keyBoardShow-height:" + height);
            /*软键盘显示：执行隐藏title动画，并修改listview高度和装载礼物容器的高度*/
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) gameBinding.flInput.getLayoutParams();
            lp.bottomMargin = height;
            if (NotchFixUtil.check(NotchFixUtil.ROM_EMUI) && android.os.Build.VERSION.SDK_INT >= 28) {
                if (NotchFixUtil.hasHwNotchInScreen(getActivity())) {
                    //HUAWEI CLT-AL01 Android9 API28、
                    LogUtils.d(TAG, "keyBoardShow-华为，齐柳海，sdk:" + android.os.Build.VERSION.SDK_INT);
//                    lp.bottomMargin -= NotchFixUtil.getNotchSize(getActivity())[1];
                }
            }
            gameBinding.flInput.setLayoutParams(lp);
        }

        @Override
        public void keyBoardHide(int height) {
            LogUtils.d(TAG, "keyBoardHide-height:" + height);
            /*软键盘隐藏：隐藏聊天输入框并显示聊天按钮，执行显示title动画，并修改listview高度和装载礼物容器的高度*/
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) gameBinding.flInput.getLayoutParams();
            lp.bottomMargin = 0;
            gameBinding.flInput.setLayoutParams(lp);
            gameBinding.flInput.setVisibility(View.GONE);
        }
    };

    /**
     * 打开软键盘并显示头布局
     */
    public void showKeyBoard() {
        gameBinding.flInput.postDelayed(showKeyBoardRunable, 80L);
        gameBinding.flInput.setVisibility(View.VISIBLE);
    }

    /**
     * 送礼物的昵称点击弹窗
     */
    private void nickClickDialog(boolean isTarget) {
        String account = "";
        if (giftAttachment instanceof GiftAttachment) {
            GiftAttachment attachment = (GiftAttachment) giftAttachment;
            if (isTarget) {
                account = attachment.getGiftRecieveInfo().getUid() + "";
            } else {
                account = attachment.getGiftRecieveInfo().getTargetUid() + "";
            }
        } else if (giftAttachment instanceof MultiGiftAttachment) {
            MultiGiftAttachment attachment = (MultiGiftAttachment) giftAttachment;
            MultiGiftReceiveInfo multiGiftRecieveInfo = attachment.getMultiGiftRecieveInfo();
            account = multiGiftRecieveInfo.getUid() + "";
        }
        if (TextUtils.isEmpty(account)) {
            return;
        }
        final List<ButtonItem> buttonItems = new ArrayList<>();
        List<ButtonItem> items = ButtonItemFactory.createAllRoomPublicScreenButtonItems(mContext, account);
        if (items == null) {
            return;
        }
        buttonItems.addAll(items);
        ((BaseMvpActivity) mContext).getDialogManager().showCommonPopupDialog(buttonItems, "取消");
    }

    /**
     * 隐藏软键盘并隐藏头布局
     */
    public void hideKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm == null) {
            return;
        }
        imm.hideSoftInputFromWindow(gameBinding.etMsg.getWindowToken(), 0);
    }

    /**
     * 软键盘显示与隐藏的监听
     */
    private void softKeyboardListener() {
        SoftKeyBoardListener.setListener(getActivity(), mOnSoftKeyBoardChangeListener);
    }

    @Override
    public void onRechargeBtnClick() {
        //尝试修复https://bugly.qq.com/v2/crash-reporting/crashes/23dbf7aab1/23204?pid=1
        if (null != getActivity()) {
            ChargeActivity.start(getActivity());
        }
    }

    @Override
    public void onSendGiftBtnClick(GiftInfo giftInfo, long uid, int number) {
        LogUtils.d(TAG, "onSendGiftBtnClick-giftInfo:" + giftInfo + " uid:" + uid + " number:" + number);
        RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoomInfo == null) {
            return;
        }
        CoreManager.getCore(IGiftCore.class).sendRoomGift(giftInfo.getGiftId(), uid, currentRoomInfo.getUid(), number, giftInfo.getGoldPrice());
    }

    @Override
    public void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, int number) {
        LogUtils.d(TAG, "onSendGiftBtnClick-giftInfo:" + giftInfo + " micMemberInfos.length:" + micMemberInfos.size() + " number:" + number);
        RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        if (currentRoomInfo == null) {
            return;
        }
        List<Long> targetUids = new ArrayList<>();
        for (MicMemberInfo micMemberInfo : micMemberInfos) {
            LogUtils.d(TAG, "onSendGiftBtnClick-uid:" + micMemberInfo.getUid());
            targetUids.add(micMemberInfo.getUid());
        }
        CoreManager.getCore(IGiftCore.class).sendRoomMultiGift(giftInfo.getGiftId(), targetUids, currentRoomInfo.getUid(), number, giftInfo.getGoldPrice());
    }


    void onUpMicro(int micPosition) {
        showBottomViewForDifRole();
        updateMicBtn();
        LogUtils.d(TAG, "onUpMicro updatePairInfoForMicQueueChanged MicroViewAdapter.updateMicUi");
        updatePairInfoForMicQueueChanged();
    }


    void onInviteUpMic(final int micPosition) {
        AvRoomDataManager.get().mIsNeedOpenMic = false;
        getMvpPresenter().upMicroPhone(micPosition, String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()), true);
        ((BaseMvpActivity) getActivity()).getDialogManager()
                .showOkBigTips(getString(R.string.tip_tips),
                        getString(R.string.embrace_on_mic), true, null);
    }

    void onDownMicro(int micPosition) {
        showBottomViewForDifRole();
        updateMicBtn();
        updatePairInfoForMicQueueChanged();
    }


    void onQueueMicStateChange(int micPosition, int micPosState) {
        gameBinding.microView.getAdapter().notifyItemChanged(micPosition);
        onMicStateChanged();
    }


    @Override
    public void onSharePlatformClick(Platform platform) {
        RoomInfo currentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        shareName = platform.getName();
        if (currentRoomInfo != null) {
            CoreManager.getCore(IShareCore.class).shareRoom(platform, currentRoomInfo.getUid(),
                    currentRoomInfo.getTitle(), currentRoomInfo.getType());
        }
    }

    @Override
    public SparseArray<ButtonItem> getAvatarButtonItemList(final int position,
                                                           final ChatRoomMember chatRoomMember,
                                                           RoomInfo currentRoom, int gender) {
        if (chatRoomMember == null || currentRoom == null) {
            return null;
        }
        SparseArray<ButtonItem> buttonItemMap = new SparseArray<>(10);
        ButtonItem buttonItem1 = ButtonItemFactory.createSendGiftItem(getContext(), chatRoomMember, this, true);
        ButtonItem buttonItem2 = ButtonItemFactory.createLockMicItem(position, new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                getMvpPresenter().closeMicroPhone(position);
            }
        });

        Map<String, Object> extension = chatRoomMember.getExtension();
        int targetMedalId = 0;
        int targetMedalDate = 0;
        boolean targetIsInvisible = false;

        if (extension != null) {
            if (extension.containsKey(Constants.USER_MEDAL_ID)) {
                targetMedalId = (int) extension.get(Constants.USER_MEDAL_ID);
            }
            if (extension.containsKey(Constants.USER_MEDAL_DATE)) {
                targetMedalDate = (int) extension.get(Constants.USER_MEDAL_DATE);
            }
            if (extension.containsKey(Constants.NOBLE_INVISIABLE_ENTER_ROOM)) {
                targetIsInvisible = (int) extension.get(Constants.NOBLE_INVISIABLE_ENTER_ROOM) == 1;
            }
        }

        ButtonItem buttonItem3 = ButtonItemFactory.createKickDownMicItem(chatRoomMember.getAccount(),
                targetMedalId, targetMedalDate, targetIsInvisible, chatRoomMember.getNick());
        ButtonItem buttonItem4 = ButtonItemFactory.createKickOutRoomItem(mContext, chatRoomMember,
                String.valueOf(currentRoom.getRoomId()), chatRoomMember.getAccount());

        //房主或者管理员，点击麦上其他用户，查看资料直接跳转个人资料页面而非弹窗
        boolean gotoUserInfoActivity = AvRoomDataManager.get().isRoomOwner() || AvRoomDataManager.get().isRoomAdmin();
        ButtonItem buttonItem5 = ButtonItemFactory.createCheckUserInfoDialogItem(getContext(),
                chatRoomMember.getAccount(), gotoUserInfoActivity);

        ButtonItem buttonItem6 = ButtonItemFactory.createDownMicItem();
        ButtonItem buttonItem7 = ButtonItemFactory.createFreeMicItem(position, new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                getMvpPresenter().openMicroPhone(position);
            }
        });
        ButtonItem buttonItem8 = ButtonItemFactory.createMarkManagerListItem(String.valueOf(currentRoom.getRoomId()),
                chatRoomMember.getAccount(), true, chatRoomMember);
        ButtonItem buttonItem9 = ButtonItemFactory.createMarkManagerListItem(String.valueOf(currentRoom.getRoomId()),
                chatRoomMember.getAccount(), false, chatRoomMember);
        ButtonItem buttonItem10 = ButtonItemFactory.createMarkBlackListItem(mContext, chatRoomMember, String.valueOf(currentRoom.getRoomId()));
        ButtonItem buttonItem11 = ButtonItemFactory.createChoicePair(currentRoom.getRoomId() + "", chatRoomMember.getAccount(), gender);
        buttonItemMap.put(0, buttonItem1);
        buttonItemMap.put(1, buttonItem2);
        buttonItemMap.put(2, buttonItem3);
        buttonItemMap.put(3, buttonItem4);
        buttonItemMap.put(4, buttonItem5);
        buttonItemMap.put(5, buttonItem6);
        buttonItemMap.put(6, buttonItem7);
        buttonItemMap.put(7, buttonItem8);
        buttonItemMap.put(8, buttonItem9);
        buttonItemMap.put(9, buttonItem10);
        buttonItemMap.put(10, buttonItem11);

        return buttonItemMap;
    }

    @Override
    public void showMicAvatarClickDialog(List<ButtonItem> buttonItemList) {
        if (ListUtils.isListEmpty(buttonItemList)) {
            return;
        }
        getDialogManager().showCommonPopupDialog(buttonItemList, getString(R.string.cancel));
    }

    @Override
    public void showGiftDialog(ChatRoomMember chatRoomMember) {
        GiftDialog dialog = new GiftDialog(getActivity(), AvRoomDataManager.get().mMicQueueMemberMap,
                chatRoomMember);
        dialog.setGiftDialogBtnClickListener(this);
        dialog.setRoomToShowLimitedExempteGift(true);
        dialog.setRoomId(null == AvRoomDataManager.get().mCurrentRoomInfo ? 0L :
                AvRoomDataManager.get().mCurrentRoomInfo.getRoomId());
        dialog.show();
    }

    @Override
    public void showOwnerSelfInfo(ChatRoomMember chatRoomMember) {
        new UserInfoDialog(getActivity(), JavaUtil.str2long(chatRoomMember.getAccount())).show();
    }

    @Override
    public void notifyRefresh() {
        gameBinding.microView.postDelayed(new Runnable() {
            @Override
            public void run() {
                RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoByMicPosition(-1);
                if (null == roomQueueInfo) {
                    return;
                }
                if (roomQueueInfo.mChatRoomMember == null) {
//                    toast("网络错误");
//                    getActivity().finish();
                    LogUtils.e(TAG, "房主信息空的");
                    ownerUpMic();
                }
            }
        }, 500);
        MicroViewAdapter microViewAdapter = gameBinding.microView.getAdapter();
        if (microViewAdapter != null) {
            microViewAdapter.notifyDataSetChanged();
        }
    }

    /**
     * 更新相亲房间相亲选择状态
     */
    @Override
    public void updatePairInfoForMicQueueChanged() {
        //1.处于相亲过程中，则也同时说明是相亲房间
        //如果是房主或者管理员,并且处于相亲过程中，那么队列有变之后，需要及时更新选择状态
        String currUid = CoreManager.getCore(IAuthCore.class).getCurrentUid() + "";
        boolean isAdminOrOwner = AvRoomDataManager.get().isRoomAdmin(currUid)
                || AvRoomDataManager.get().isRoomOwner();
        if (AvRoomDataManager.get().isPair() && AvRoomDataManager.get().isShowLikeTa && isAdminOrOwner) {
            CoreManager.getCore(IPairCore.class).updatePairSelectMicPosition();
        }
        if (gameBinding != null && gameBinding.microView != null && gameBinding.microView.getAdapter() != null) {
            LogUtils.d(TAG, "updatePairInfoForMicQueueChanged MicroViewAdapter.updateMicUi");
            gameBinding.microView.getAdapter().updateMicUi();
        }
    }

    @Override
    public void onGetFortuneRankTop(boolean isSuccess, String message, List<RoomConsumeInfo> roomConsumeInfos) {
        LogUtils.d(TAG, "onGetFortuneRankTop-isSuccess:" + isSuccess + " message:" + message);
        if (isSuccess && null != roomConsumeInfos) {
            if (roomConsumeInfos.size() >= 1) {
                RoomConsumeInfo roomConsumeInfo = roomConsumeInfos.get(0);
                GlideApp.with(this).load(roomConsumeInfo.getAvatar()).dontAnimate().into(gameBinding.civFortuneFirst);
            }

            if (roomConsumeInfos.size() >= 2) {
                RoomConsumeInfo roomConsumeInfo = roomConsumeInfos.get(1);
                GlideApp.with(this).load(roomConsumeInfo.getAvatar()).dontAnimate().into(gameBinding.civFortuneSecond);
            }

        }
    }

    private void onRoomBannerClick() {
        if (null == getActivity()) {
            return;
        }
        //新多人房，活动banner，点击
        StatisticManager.get().onEvent(getActivity(),
                StatisticModel.EVENT_ID_HOME_PARTY_ROOM_BANNER_CLICK,
                StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
    }

    @Override
    public void kickDownMicroPhoneSuccess() {
        updateMicBtn();
        toast(R.string.kick_mic);
    }

    @Override
    public void showOwnerClickDialog(final RoomMicInfo roomMicInfo, final int micPosition,
                                     final long currentUid, boolean isOwner) {
        List<ButtonItem> buttonItems = new ArrayList<>(4);
        final ButtonItem buttonItem1 = new ButtonItem(getString(R.string.embrace_up_mic), new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                RoomInviteActivity.openActivity(getActivity(), micPosition);
            }
        });
        ButtonItem buttonItem2 = new ButtonItem(roomMicInfo.isMicMute() ? getString(R.string.no_forbid_mic) : getString(R.string.forbid_mic), new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                if (roomMicInfo.isMicMute()) {
                    getMvpPresenter().openMicroPhone(micPosition);
                } else {
                    getMvpPresenter().closeMicroPhone(micPosition);
                }
            }
        });
        ButtonItem buttonItem3 = new ButtonItem(roomMicInfo.isMicLock() ? getString(R.string.unlock_mic) :
                getString(R.string.lock_mic), new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                if (roomMicInfo.isMicLock()) {
                    getMvpPresenter().unLockMicroPhone(micPosition);
                } else {
                    getMvpPresenter().lockMicroPhone(micPosition, currentUid);
                }
            }
        });
        ButtonItem buttonItem4 = new ButtonItem(roomMicInfo.isMicLock() ? "一键全开" :
                "一键全锁", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                if (roomMicInfo.isMicLock()) {
                    getMvpPresenter().unLockAllMicroPhone();
                } else {
                    getMvpPresenter().lockAllMicroPhone(currentUid);
                }
            }
        });
        ButtonItem buttonItem5 = new ButtonItem("移到此座位", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                //管理员切换麦位，currentUid对应房主 ID，所以这里应当修改为当前用户ID
                getMvpPresenter().upMicroPhone(micPosition,
                        CoreManager.getCore(IAuthCore.class).getCurrentUid() + "", false);
            }
        });
        buttonItems.add(buttonItem1);
        buttonItems.add(buttonItem2);
        buttonItems.add(buttonItem3);
        buttonItems.add(buttonItem4);
        if (!isOwner) {
            buttonItems.add(buttonItem5);
        }
        getDialogManager().showCommonPopupDialog(buttonItems, getString(R.string.cancel));
    }

    @Override
    public void chatRoomReConnectView() {
        if (gameBinding.microView != null && gameBinding.microView.getAdapter() != null) {
            gameBinding.microView.getAdapter().notifyDataSetChanged();
        }
    }

    @Override
    public void onAvatarBtnClick(int position) {
        getMvpPresenter().avatarClick(position);
    }

    private int lastMicroPos = -1;//自己上一次所在的麦位

    @Override
    public void onUpMicBtnClick(int position, ChatRoomMember chatRoomMember) {
        if (lastMicroPos != position) {
            lastMicroPos = position;
            getMvpPresenter().microPhonePositionClick(position, chatRoomMember);
            //房间-上麦
            StatisticManager.get().onEvent(getActivity(),
                    StatisticModel.EVENT_ID_ROOM_UP_MIC,
                    StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    lastMicroPos = -1;
                }
            }, 1000);
        }
    }

    @Override
    public void onLockBtnClick(int position) {
        getMvpPresenter().unLockMicroPhone(position);
    }

    @Override
    public void onRoomSettingsClick() {

        if (AvRoomDataManager.get().isRoomOwner() || AvRoomDataManager.get().isRoomAdmin()) {
            RoomTopicActivity.start(getContext());
//            RoomSettingActivity.start(getContext(), AvRoomDataManager.get().mCurrentRoomInfo);
        } else {
            RoomTopicDIalog roomTopicDIalog = new RoomTopicDIalog();
            roomTopicDIalog.show(getChildFragmentManager());
        }
    }

    public void onContributeListClick() {
        BigListDataDialog bigListDataDialog = BigListDataDialog.newContributionListInstance(getActivity());
        bigListDataDialog.setSelectOptionAction(new BigListDataDialog.SelectOptionAction() {
            @Override
            public void optionClick() {
                getDialogManager().showProgressDialog(mContext, "请稍后");
            }

            @Override
            public void onDataResponse() {
                //请求结束前退出可能会导致奔溃，直接捕获没关系
                try {
                    getDialogManager().dismissDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        bigListDataDialog.show(getChildFragmentManager());
        //房间-贡献榜
        StatisticManager.get().onEvent(getActivity(),
                StatisticModel.EVENT_ID_ROOM_CONTRIBUTIONI_LIST,
                StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
    }

    private DynamicFaceDialog dynamicFaceDialog = null;
    private GiftDialog giftDialog = null;


    //----------------------------------底部按钮点击处理----------------------------------------
    @Override
    public void onOpenMicBtnClick() {
        RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoByAccount(
                String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        if (roomQueueInfo == null || roomQueueInfo.mRoomMicInfo == null) {
            return;
        }
        //先判断麦上是否是开麦的
        if (!roomQueueInfo.mRoomMicInfo.isMicMute() && !AgoraEngineManager.get().isAudienceRole) {
            AvRoomDataManager.get().mIsNeedOpenMic = !AvRoomDataManager.get().mIsNeedOpenMic;
            boolean mute = !AgoraEngineManager.get().isMute;
            AgoraEngineManager.get().setMute(mute);
            updateMicBtn();
        }
    }

    @Override
    public void onSendFaceBtnClick() {
        if (AvRoomDataManager.get().isOnMic(myUid) || AvRoomDataManager.get().isRoomOwner()) {
            if (dynamicFaceDialog == null) {
                dynamicFaceDialog = new DynamicFaceDialog(getContext());
                dynamicFaceDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        dynamicFaceDialog = null;
                    }
                });
            }
            if (!dynamicFaceDialog.isShowing()) {
                dynamicFaceDialog.show();
            }
        } else {
            toast("上麦才能发表情哦!");
        }

    }

    @Override
    public void onSendMsgBtnClick() {
        Json json = CoreManager.getCore(IUserInfoCore.class).getBannedMap();
        boolean all = json.boo(IUserInfoCore.BANNED_ALL + "");
        boolean room = json.boo(IUserInfoCore.BANNED_ROOM + "");
        if (all || room) {
            if (null != getActivity()) {
                toast(getActivity().getResources().getString(R.string.banned));
            }

            return;
        }
        gameBinding.etMsg.setText("");
        gameBinding.etMsg.setFocusableInTouchMode(true);
        gameBinding.etMsg.requestFocus();
        showKeyBoard();
        //房间-发言
        StatisticManager.get().onEvent(getActivity(),
                StatisticModel.EVENT_ID_ROOM_OPEN_MSG,
                StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
    }

    @Override
    public void onSendGiftBtnClick() {
        clickBtnToShowGiftDialog(null);
        //房间-打开送礼界面
        StatisticManager.get().onEvent(getActivity(),
                StatisticModel.EVENT_ID_ROOM_OPEN_GIFT,
                StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
    }

    @Override
    public void onShareBtnClick() {
        ShareDialog shareDialog = new ShareDialog(getActivity());
        shareDialog.setOnShareDialogItemClick(HomePartyRoomFragment.this);
        shareDialog.show();
    }

    @Override
    public void onRemoteMuteBtnClick() {
        AgoraEngineManager.get().setRemoteMute(!AgoraEngineManager.get().isRemoteMute);
        updateRemoteMuteBtn();
    }

    @Override
    public void onBuShowMicInList() {
        if (AvRoomDataManager.get().isOwnerOnMic()) {
            toast("您已经在麦上");
            return;
        }
        showMicInListDialog();
    }

    @Override
    public void onPublicRoomMsgBtnClick() {
        LogUtils.d(TAG, "onPublicRoomMsgBtnClick");
        RoomPrivateMsgDialog msgDialog = new RoomPrivateMsgDialog();
        msgDialog.show(getChildFragmentManager(), null);
    }

    @Override
    public void onRoomMoreOperaClick() {

    }

    /**
     * 非主播，点击底部语音连接按钮
     */
    @Override
    public void onFansReqAudioConnClick() {

    }

    /**
     * 主播，点击底部语音连接按钮
     */
    @Override
    public void onAnchorAudioConnRecvClick() {

    }

    /**
     * 需要留意点击引爆礼物墙的礼物跳转礼物列表的时候，礼物列表是否已经展示给用户了
     *
     * @param detonatingBoxGiftInfo
     */
    private void clickBtnToShowGiftDialog(GiftInfo detonatingBoxGiftInfo) {
        if (giftDialog == null) {
            giftDialog = new GiftDialog(getContext(), AvRoomDataManager.get().mMicQueueMemberMap, null);
            giftDialog.setRoomToShowLimitedExempteGift(true);
            giftDialog.setGiftDialogBtnClickListener(HomePartyRoomFragment.this);
            giftDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    giftDialog = null;
                }
            });
        }
        if (null != detonatingBoxGiftInfo) {
            giftDialog.setDetonatingBoxGiftInfo(detonatingBoxGiftInfo);
        }
        if (!giftDialog.isShowing()) {
            giftDialog.setRoomId(null == AvRoomDataManager.get().mCurrentRoomInfo ? 0L :
                    AvRoomDataManager.get().mCurrentRoomInfo.getRoomId());
            giftDialog.show();

            checkShouldShowGiftGuideView();
        }
    }

    private void showMicInListDialog() {

        if (!micInListOption) {
            return;
        }

        MicInListDialog micInListDialog = new MicInListDialog(mContext);
        boolean isRoomOwner = AvRoomDataManager.get().isRoomOwner();
        micInListDialog.isAdmin = AvRoomDataManager.get().isRoomAdmin() || isRoomOwner;
        micInListDialog.isRoomOwner = isRoomOwner;
        micInListDialog.iSubmitAction = new MicInListDialog.ISubmitAction() {
            @Override
            public void onSubmitClick() {
                boolean checkInMicInlist = AvRoomDataManager.get().checkInMicInlist();
                if (checkInMicInlist) {
                    getMvpPresenter().removeMicInList();
                } else {
                    getMvpPresenter().addMicInList();
                }

            }
        };
        micInListDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogUtils.d(TAG, "onActivityResult-requestCode:" + requestCode + " resultCode:" + resultCode);
        if (requestCode == 200 && resultCode == 100) {
            if (data != null && data.getExtras() != null) {
                String account = data.getExtras().getString("account");
                if (TextUtils.isEmpty(account)) {
                    return;
                }
                int micPosition = data.getExtras().getInt(Constants.KEY_POSITION, Integer.MIN_VALUE);
                if (micPosition == Integer.MIN_VALUE) {
                    return;
                }
                SparseArray<RoomQueueInfo> mMicQueueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;
                if (mMicQueueMemberMap != null && mMicQueueMemberMap.get(micPosition).mChatRoomMember != null) {
                    //该麦位有人了
                    toast("该麦位已经有人了");
                    return;
                }
                //抱人上麦
                getMvpPresenter().inviteMicroPhone(JavaUtil.str2long(account), micPosition);
            }
        } else if (requestCode == REQUEST_CODE_DETONATE_GIFT_DIALOG && null != data && data.hasExtra("info")) {
            clickBtnToShowGiftDialog((GiftInfo) data.getSerializableExtra("info"));
        }
    }

    void onChatRoomMemberBlackAdd(String account) {
        //拉黑
        if (AvRoomDataManager.get().isOnMic(JavaUtil.str2long(account))) {
            int micPosition = AvRoomDataManager.get().getMicPosition(JavaUtil.str2long(account));
            getMvpPresenter().downMicroPhone(micPosition, true);
        }

        ListIterator<ChatRoomMember> memberListIterator = AvRoomDataManager.get().mRoomManagerList.listIterator();
        for (; memberListIterator.hasNext(); ) {
            if (Objects.equals(memberListIterator.next().getAccount(), account)) {
                memberListIterator.remove();
            }
        }


        if (AvRoomDataManager.get().isRoomOwner(account)) {
            //当前是房主
            AvRoomDataManager.get().mRoomCreateMember = null;
        }
    }

    @CoreEvent(coreClientClass = IAVRoomCoreClient.class)
    public void onUserCarIn(String carUrl) {
        if (svgaAciton != null) {
            svgaAciton.showCar(carUrl);
        }

    }

    @CoreEvent(coreClientClass = IAVRoomCoreClient.class)
    public void sendMsg(String msg) {
        if (getMvpPresenter() != null) {
            Disposable disposable = getMvpPresenter().sendTextMsg(msg);
            if (null != mCompositeDisposable && null != disposable) {
                mCompositeDisposable.add(disposable);
            }
        }

    }

    @CoreEvent(coreClientClass = IAVRoomCoreClient.class)
    public void micInlistMoveToTop(int micPosition, String roomId, String value) {
        if (getMvpPresenter() != null) {
            if (TextUtils.isEmpty(roomId)) {
                roomId = AvRoomDataManager.get().mCurrentRoomInfo.getRoomId() + "";
            }
            getMvpPresenter().updataQueueExBySdk(micPosition, roomId, value);
        }

    }

    @CoreEvent(coreClientClass = IAVRoomCoreClient.class)
    public void onMicInListToUpMic(int micPosition, String uid) {
        if (getMvpPresenter() != null && micPosition != -1) {
            AvRoomDataManager.get().mIsNeedOpenMic = false;
            getMvpPresenter().upMicroPhone(micPosition, uid, false, true);
            toast("您上麦了");
        }

    }

    @CoreEvent(coreClientClass = IAVRoomCoreClient.class)
    public void onMicInListChange() {
        int size = AvRoomDataManager.get().mMicInListMap.size();
        boolean b = size == 0 || !micInListOption || (!AvRoomDataManager.get().isRoomAdmin() && !AvRoomDataManager.get().isRoomOwner());
        buMicInListCount.setVisibility(b ? View.GONE : View.VISIBLE);
        buMicInListCount.setText(String.valueOf(size));
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        onMicInListChange();
    }


    //------------------------------------新手引导----------------------------------------------
    private NoviceGuideDialog giftGuideDialog;

    /**
     * 判断是否显示新手引导界面
     */
    private void checkShouldShowGiftGuideView() {
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        if (null != userInfo && userInfo.getGuideState() != null && "0".equals(userInfo.getGuideState().getType4())) {
            if (null == giftGuideDialog) {
                if (null == getActivity()) {
                    return;
                }
                giftGuideDialog = new NoviceGuideDialog(getActivity(), NoviceGuideDialog.GuideType.AVROOM_GIFT_DIALOG, null);
            }
            giftGuideDialog.show();
        }
    }

    private NoviceGuideDialog avRoomGuideDialog;

    /**
     * 判断是否显示新手引导界面
     */
    private void checkShouldShowMicroGuideView(int[] locations) {
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        if (null != userInfo && userInfo.getGuideState() != null && "0".equals(userInfo.getGuideState().getType3())) {
            if (null == avRoomGuideDialog) {
                if (null == getActivity()) {
                    return;
                }
                avRoomGuideDialog = new NoviceGuideDialog(getActivity(), NoviceGuideDialog.GuideType.AVROOM_GIFT_BTN, locations);
            }
            avRoomGuideDialog.show();
        }
    }

    @Override
    public void onMicroGuideViewLocationChanged(int position, int[] locations) {
        LogUtils.d(TAG, "onMicroGuideViewLocationChanged-position:" + position + " x:" + locations[0] + " y:" + locations[1]);
        if (7 == position) {
            checkShouldShowMicroGuideView(locations);
        }
    }


    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onIsLiked(boolean islike, long uid) {
        LogUtils.d(TAG, "onIsLiked-islike:" + islike + " uid:" + uid);
        if (null != AvRoomDataManager.get().mCurrentRoomInfo &&
                uid == AvRoomDataManager.get().mCurrentRoomInfo.getUid()) {
            if (islike) {
                gameBinding.messageView.refreshLikeMsgStatus(islike, islike);
            } else {
                if (!hasGenAttentionMsg) {
                    hasGenAttentionMsg = true;
                    long time = System.currentTimeMillis() - timeInRoom;
                    if (time >= 30000L) {
                        genAttentionMsg();
                    } else {
                        if (gameBinding.messageView != null) {
                            gameBinding.messageView.postDelayed(genAttentionMsgRunnable, 30000L - time);
                        }
                    }
                }
            }

        }
    }

    private Runnable genAttentionMsgRunnable = new Runnable() {
        @Override
        public void run() {
            genAttentionMsg();
        }
    };

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onIsLikedFail(String error) {
        LogUtils.d(TAG, "onIsLikedFail-error:" + error);
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraise(long likedUid) {
        LogUtils.d(TAG, "onPraise-likedUid:" + likedUid);
        getDialogManager().dismissDialog();
        toast("关注成功，相互关注可成为好友哦！");
        if (null != AvRoomDataManager.get().mCurrentRoomInfo &&
                likedUid == AvRoomDataManager.get().mCurrentRoomInfo.getUid()) {
            gameBinding.messageView.refreshLikeMsgStatus(true, true);
        }
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraiseFaith(String error) {
        LogUtils.d(TAG, "onPraiseFaith-error:" + error);
        toast(error);
        getDialogManager().dismissDialog();
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onCanceledPraise(long likedUid) {
        LogUtils.d(TAG, "onCanceledPraise-likedUid:" + likedUid);
        toast("取消关注成功");
        getDialogManager().dismissDialog();
        if (null != AvRoomDataManager.get().mCurrentRoomInfo &&
                likedUid == AvRoomDataManager.get().mCurrentRoomInfo.getUid()) {
            gameBinding.messageView.refreshLikeMsgStatus(false, true);
        }
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onCanceledPraiseFaith(String error) {
        LogUtils.d(TAG, "onCanceledPraiseFaith-error:" + error);
        toast(error);
        getDialogManager().dismissDialog();
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        LogUtils.d(TAG, "onItemClick");
        if (null != view.getTag()) {
            ChatRoomMessage chatRoomMessage = (ChatRoomMessage) view.getTag();
            if (null != chatRoomMessage && chatRoomMessage.getMsgType() == MsgTypeEnum.custom) {
                CustomAttachment attachment = (CustomAttachment) chatRoomMessage.getAttachment();
                if (null != attachment && attachment.getFirst() == CustomAttachment.CUSTOM_MSG_ROOM_SHARE) {
                    ShareDialog shareDialog = new ShareDialog(getActivity());
                    shareDialog.setOnShareDialogItemClick(HomePartyRoomFragment.this);
                    shareDialog.showShareFans = true;
                    shareDialog.show();

                    //房间-公屏分享
                    StatisticManager.get().onEvent(getActivity(),
                            StatisticModel.EVENT_ID_ROOM_PUBLIC_SCREEN_SHARE,
                            StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
                }
            }
        }
    }

//------------------------------------收发礼物,情侣同时上麦动画播放---------------------------------------

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onRecieveGiftMsg(GiftReceiveInfo giftReceiveInfo) {
        LogUtils.d(TAG, "onRecieveGiftMsg-giftReceiveInfo:" + giftReceiveInfo);
        checkForRefreshLoverUserInfo(giftReceiveInfo.getUid(), giftReceiveInfo.getTargetUid(), giftReceiveInfo.getGiftId());
    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onSuperGiftMsg(GiftReceiveInfo giftReceiveInfo) {
        LogUtils.d(TAG, "onSuperGiftMsg-giftReceiveInfo:" + giftReceiveInfo);
        checkForRefreshLoverUserInfo(giftReceiveInfo.getUid(), giftReceiveInfo.getTargetUid(), giftReceiveInfo.getGiftId());
    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onRecieveMultiGiftMsg(MultiGiftReceiveInfo multiGiftReceiveInfo) {
        LogUtils.d(TAG, "onRecieveMultiGiftMsg-multiGiftReceiveInfo:" + multiGiftReceiveInfo);
        for (long targetUid : multiGiftReceiveInfo.getTargetUids()) {
            checkForRefreshLoverUserInfo(multiGiftReceiveInfo.getUid(), targetUid, multiGiftReceiveInfo.getGiftId());
        }
    }

    private void checkForRefreshLoverUserInfo(long senderUid, long receiverUid, long giftId) {
        LogUtils.d(TAG, "checkForRefreshLoverUserInfo-senderUid:" + senderUid + " receiverUid:" + receiverUid + " giftId:" + giftId);
        UserInfo selfUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (null != selfUserInfo && giftId == LoverUpMicAnimAttachment.loverGiftId && (selfUserInfo.getUid() == senderUid || selfUserInfo.getUid() == receiverUid)) {
            LogUtils.d(TAG, "checkForRefreshLoverUserInfo-发送方或者接收方是自己");
            //发送方或者接收方是自己 更新个人信息 获取情侣信息
            CoreManager.getCore(IUserCore.class).requestUserInfo(selfUserInfo.getUid());
        }
    }

    private final int MSG_WHAT_SHOW_LOVER_UP_MIC_ANIM = 0;
    private Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_WHAT_HIDE_OPEN_NOBLE_ANIM: {
                    TranslateAnimation translateAni = new TranslateAnimation(
                            Animation.RELATIVE_TO_SELF, 0f, Animation.RELATIVE_TO_SELF,
                            5f, Animation.RELATIVE_TO_SELF, 0,
                            Animation.RELATIVE_TO_SELF, 0);
                    translateAni.setDuration(1000);
                    translateAni.setRepeatCount(0);
                    translateAni.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            gameBinding.rlOpenNobleAnim.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    gameBinding.rlOpenNobleAnim.startAnimation(translateAni);
                }
                return true;
                case MSG_WHAT_HIDE_ENTER_ROOM_ANIM:
                    gameBinding.llEnterRoomAnim.setVisibility(View.GONE);
                    gameBinding.llNobleEnterRoomAnim.setVisibility(View.GONE);
                    return true;
                case MSG_WHAT_UPDATE_GIFT_DETONATE_PROGRESS:
                    DetonateGiftAttachment detonateGiftAttachment = (DetonateGiftAttachment) msg.obj;
                    if (null != detonateGiftAttachment) {
                        //仅更新当前房间的引爆进度
                        if (null != AvRoomDataManager.get().mCurrentRoomInfo
                                && detonateGiftAttachment.getRoomUid() == AvRoomDataManager.get().mCurrentRoomInfo.getUid()
                                && detonateGiftAttachment.getRoomType() == AvRoomDataManager.get().mCurrentRoomInfo.getType()
                                && lastGiftDetonatingProgress <= detonateGiftAttachment.getDetonatingState()) {

                            lastGiftDetonatingProgress = detonateGiftAttachment.getDetonatingState();
                            showGiftDetonatingProgress(lastGiftDetonatingProgress,
                                    detonateGiftAttachment.getDetonatingDuration(),
                                    true);
                        }
                        //但是引爆礼物横幅动画是整个全服房间的
                        if (100 <= detonateGiftAttachment.getDetonatingState()) {
                            showGiftDetonateAnim(detonateGiftAttachment.getDetonatingUid(),
                                    detonateGiftAttachment.getRoomUid(),detonateGiftAttachment.getRoomType(),
                                    detonateGiftAttachment.getDetonatingNick(),
                                    detonateGiftAttachment.getDetonatingAvatar(),
                                    detonateGiftAttachment.getDetonatingGifts());

                            //仅当前房间显示引爆通知弹框
                            if (null != AvRoomDataManager.get().mCurrentRoomInfo
                                    && detonateGiftAttachment.getRoomUid() == AvRoomDataManager.get().mCurrentRoomInfo.getUid()
                                    && detonateGiftAttachment.getRoomType() == AvRoomDataManager.get().mCurrentRoomInfo.getType()) {

                                updateCurrRoomGiftDetoanteStatus(detonateGiftAttachment.getDetonatingUid(),
                                        detonateGiftAttachment.getDetonatingNick(), detonateGiftAttachment.getDetonatingAvatar(),
                                        detonateGiftAttachment.getDetonatingGifts(), detonateGiftAttachment.getDetonatingTime(),
                                        detonateGiftAttachment.getDetonatingDuration(), detonateGiftAttachment.getDetonatingState());

                                showDetnoateGiftNotifyDialog();
                            }
                        }
                    }
                    return true;
                case MSG_WHAT_HIDE_GIFT_DETONATE_ANIM:
                    //到期了，停止动画
                    lastGiftDetonatingProgress = 0;
                    showGiftDetonatingProgress(-1, 0L, false);
                    return true;
                case MSG_WHAT_HIDE_GIFT_DETONATE_NOTIFY:
                    gameBinding.flSuperGiftFullServNoty.setVisibility(View.GONE);
                    return true;
                case MSG_WHAT_SHOW_LOVER_UP_MIC_ANIM:
                    gameBinding.flLoverAnim.setVisibility(View.VISIBLE);
                    RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) gameBinding.flLoverAnim.getLayoutParams();
//                    gameBinding.flLoverAnim.scrollTo((int)gameBinding.flLoverAnim.getX()-marginInterval,(int)gameBinding.flLoverAnim.getY());
                    currLeft -= marginInterval;
                    currRight -= marginInterval;
                    gameBinding.flLoverAnim.layout(currLeft,
                            gameBinding.flLoverAnim.getTop(),
                            currRight
                            , gameBinding.flLoverAnim.getBottom());

                    LogUtils.d(TAG, "MSG_WHAT_SHOW_LOVER_UP_MIC_ANIM-leftMargin:" + lp.leftMargin + " rightLeftMargin:" + rightLeftMargin + " marginInterval:" + marginInterval);
                    LogUtils.d(TAG, "MSG_WHAT_SHOW_LOVER_UP_MIC_ANIM-currLeft:" + currLeft + " currRight:" + currRight);
                    if (currLeft >= rightLeftMargin) {
                        lp.leftMargin = rightLeftMargin;
                        gameBinding.flLoverAnim.postDelayed(stopLoverUpMicAnimRunnalbe, 1500L);
                    } else {
                        handler.sendEmptyMessageDelayed(MSG_WHAT_SHOW_LOVER_UP_MIC_ANIM, 25L);
                    }
                    return true;
                case MSG_WHAT_HIDE_ROOM_RED_PACKET_BANNER:
                    TranslateAnimation translateAni = new TranslateAnimation(
                            Animation.RELATIVE_TO_SELF, 0f, Animation.RELATIVE_TO_SELF,
                            5f, Animation.RELATIVE_TO_SELF, 0,
                            Animation.RELATIVE_TO_SELF, 0);
                    translateAni.setDuration(2000);
                    translateAni.setRepeatCount(0);
                    rlRedPacketBanner.startAnimation(translateAni);
                    rlRedPacketBanner.setVisibility(View.GONE);
                    return true;
                default:
                    return false;
            }
        }
    });

    private ObjectAnimator loverUpMicInAnim = null;

    private int marginInterval = 0;
    private int rightLeftMargin = 0;
    private int currLeft = 0;
    private int currRight = 0;

    Runnable newShowLoverUpMicAnimRunnalbe = new Runnable() {
        @Override
        public void run() {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) gameBinding.flLoverAnim.getLayoutParams();
            LogUtils.d(TAG, "showLoverUpMicAnimRunnalbe-leftMargin:" + lp.leftMargin
                    + " measureWidth:" + gameBinding.flLoverAnim.getMeasuredWidth()
                    + " width:" + gameBinding.flLoverAnim.getWidth());

            rightLeftMargin = lp.leftMargin;
            lp.leftMargin = -gameBinding.flLoverAnim.getWidth();
            currLeft = lp.leftMargin - rightLeftMargin;
            currRight = -rightLeftMargin;
            marginInterval = currLeft / 40;
            handler.sendEmptyMessageDelayed(MSG_WHAT_SHOW_LOVER_UP_MIC_ANIM, 40L);
        }
    };

    Runnable showLoverUpMicAnimRunnalbe = new Runnable() {
        @Override
        public void run() {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) gameBinding.flLoverAnim.getLayoutParams();
            LogUtils.d(TAG, "showLoverUpMicAnimRunnalbe-leftMargin:" + lp.leftMargin
                    + " measureWidth:" + gameBinding.flLoverAnim.getMeasuredWidth()
                    + " width:" + gameBinding.flLoverAnim.getWidth());
            int totalTranX = gameBinding.flLoverAnim.getWidth();
            float curTranX = gameBinding.flLoverAnim.getTranslationX();
            loverUpMicInAnim = ObjectAnimator.ofFloat(gameBinding.flLoverAnim, "translationX", -totalTranX, curTranX);
            loverUpMicInAnim.setDuration(1000L);
            loverUpMicInAnim.addListener(loverUpMicAnimInAdapter);
            loverUpMicInAnim.start();
        }
    };

    private AnimatorListenerAdapter loverUpMicAnimInAdapter = new AnimatorListenerAdapter() {

        @Override
        public void onAnimationStart(Animator animation, boolean isReverse) {
            LogUtils.d(TAG, "loverUpMicAnimInAdapter-onAnimationStart");
            gameBinding.flLoverAnim.setVisibility(View.VISIBLE);
        }

        @Override
        public void onAnimationEnd(Animator animation, boolean isReverse) {
            LogUtils.d(TAG, "loverUpMicAnimInAdapter-onAnimationEnd");
            loverUpMicInAnim.removeAllListeners();
            loverUpMicInAnim = null;
            gameBinding.flLoverAnim.postDelayed(stopLoverUpMicAnimRunnalbe, 1500L);
        }
    };

    private void checkForShowLoverUpMicAnim(String account) {
        LogUtils.d(TAG, "checkForShowLoverUpMicAnim-account:" + account);
        //首先查询个人信息 拿到情侣信息
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        boolean showLoverAnim = false;
        if (null == userInfo) {
            return;
        }
        UserInfo loverUserInfo = userInfo.getLoverUser();
        //判断情侣是否都在麦上
        if (null != loverUserInfo) {
            if (account.equals(String.valueOf(userInfo.getUid())) && AvRoomDataManager.get().isOnMic(loverUserInfo.getUid())) {
                showLoverAnim = true;
                LogUtils.d(TAG, "checkForShowLoverUpMicAnim-我自己上麦,我情侣已在麦上");
            }
        }

        //新需求，情侣一方开启了神秘人，就不展示情侣上麦动画
        if (showLoverAnim && NobleBusinessManager.isShowLoverUpMicAnim(userInfo.getVipId(),
                userInfo.getIsInvisible(), userInfo.getVipDate(), loverUserInfo.getVipId(),
                loverUserInfo.getIsInvisible(), loverUserInfo.getVipDate())) {
            LoverUpMicAnimAttachment customAttachment = new LoverUpMicAnimAttachment(CustomAttachment.CUSTOM_MSG_ROOM_LOVER_UP_MIC,
                    CustomAttachment.CUSTOM_MSG_ROOM_LOVER_UP_MIC);
            customAttachment.setFirstNick(userInfo.getNick());
            customAttachment.setFirstUid(userInfo.getUid() + "");
            customAttachment.setSecondUid(loverUserInfo.getUid() + "");
            customAttachment.setSecondNick(loverUserInfo.getNick());
            customAttachment.setFirstVipId(userInfo.getVipId());
            customAttachment.setFirstVipDate(userInfo.getVipDate());
            customAttachment.setFirstIsInvisible(userInfo.getIsInvisible());
            customAttachment.setSecondVipId(loverUserInfo.getVipId());
            customAttachment.setSecondVipDate(loverUserInfo.getVipDate());
            customAttachment.setSecondIsInvisible(loverUserInfo.getIsInvisible());
            ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                    AvRoomDataManager.get().mCurrentRoomInfo.getRoomId() + "",
                    customAttachment);
            CoreManager.getCore(IRoomCore.class).sendMessage(message);
            LogUtils.d(TAG, "checkForShowLoverUpMicAnim-发送情侣上麦自定义消息");
        }
        //检测是否是挚友上麦
        checkBosonFriendUpMic(account);
    }

    private void checkBosonFriendUpMic(String account) {
        LogUtils.d(TAG, "checkBosonFriendUpMic-account:" + account);
        //首先查询个人信息 拿到挚友信息
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (null == userInfo) {
            return;
        }
        if (!account.equals(String.valueOf(userInfo.getUid())) || !isFristEnterRoom) {
            return;
        }
        List<BosonFriendEnitity> bestFriendList = userInfo.getBestFriendList();
        for (BosonFriendEnitity friend : bestFriendList) {
            if (friend == null) {
                continue;
            }
            //判断挚友是否都在麦上
            boolean isFdOnMic = AvRoomDataManager.get().isOnMic(friend.getfUid());
            //挚友一方开启了神秘人，就不展示挚友上麦动画
            boolean isVisible = NobleBusinessManager.isShowLoverUpMicAnim(userInfo.getVipId(),
                    userInfo.getIsInvisible(), userInfo.getVipDate(), friend.getVipId(),
                    friend.isInvisible(), friend.getVipDate());
            if (isFdOnMic && isVisible) {
                BosonFriendUpMicAnimAttachment customAttachment = new BosonFriendUpMicAnimAttachment(CustomAttachment.CUSTOM_MSG_BOSON_FIRST,
                        CustomAttachment.CUSTOM_MSG_BOSON_UP_MACRO);
                customAttachment.setFirstNick(userInfo.getNick());
                customAttachment.setFirstUid(userInfo.getUid() + "");
                customAttachment.setSecondUid(friend.getfUid() + "");
                customAttachment.setSecondNick(friend.getNick());
                customAttachment.setFirstVipId(userInfo.getVipId());
                customAttachment.setFirstVipDate(userInfo.getVipDate());
                customAttachment.setFirstIsInvisible(userInfo.getIsInvisible());
                customAttachment.setSecondVipId(friend.getVipId());
                customAttachment.setSecondVipDate(friend.getVipDate());
                customAttachment.setSecondIsInvisible(friend.isInvisible());
                customAttachment.setBosonFriendLevel(friend.getLevel());
                customAttachment.setBosonFriendLevelName(friend.getLevelName());
                ChatRoomMessage message = ChatRoomMessageBuilder.createChatRoomCustomMessage(
                        AvRoomDataManager.get().mCurrentRoomInfo.getRoomId() + "",
                        customAttachment);
                CoreManager.getCore(IRoomCore.class).sendMessage(message);
                LogUtils.d(TAG, "checkBosonFriendUpMic-发送挚友上麦自定义消息");
            }
        }
    }

    //上麦消息队列
    LinkedList<ChatRoomMessage> upMicroMsgQueue = new LinkedList<>();

    private void dealUpMicroMsg(ChatRoomMessage chatRoomMessage) {
        if (null == chatRoomMessage) {
            return;
        }
        if (chatRoomMessage.getMsgType() != MsgTypeEnum.custom) {
            return;
        }
        MsgAttachment msgAttachment = chatRoomMessage.getAttachment();
        if (!(msgAttachment instanceof CustomAttachment)) {
            return;
        }
        CustomAttachment attachment = (CustomAttachment) msgAttachment;
        if (attachment.getFirst() != CustomAttachment.CUSTOM_MSG_ROOM_LOVER_UP_MIC
                && attachment.getFirst() != CustomAttachment.CUSTOM_MSG_BOSON_FIRST) {
            return;
        }
        upMicroMsgQueue.offer(chatRoomMessage);
        if (upMicroMsgQueue.size() == 1) {
            dealLoverUpMicAnimMsg(upMicroMsgQueue.peek());
        }
    }

    private void dealLoverUpMicAnimMsg(ChatRoomMessage chatRoomMessage) {
        LogUtils.d(TAG, "dealLoverUpMicAnimMsg-chatRoomMessage:" + chatRoomMessage);
        if (null == chatRoomMessage) {
            return;
        }
        if (chatRoomMessage.getMsgType() != MsgTypeEnum.custom) {
            return;
        }
        MsgAttachment msgAttachment = chatRoomMessage.getAttachment();
        if (null == msgAttachment) {
            return;
        }
        CustomAttachment attachment = (CustomAttachment) msgAttachment;
        LogUtils.d(TAG, "dealLoverUpMicAnimMsg-attachment.first:" + attachment.getFirst() + " second:" + attachment.getSecond());
        if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_ROOM_LOVER_UP_MIC) {
            LoverUpMicAnimAttachment loverUpMicAnimAttachment = (LoverUpMicAnimAttachment) attachment;
            //修改提示文本显示样式
            String firstNick = NobleBusinessManager.getNobleRoomNick(
                    loverUpMicAnimAttachment.getFirstVipId(),
                    loverUpMicAnimAttachment.isFirstIsInvisible(),
                    loverUpMicAnimAttachment.getFirstVipDate(),
                    loverUpMicAnimAttachment.getFirstNick());
            if (firstNick.length() > 6 && null != getActivity()) {
                firstNick = getResources().getString(R.string.nick_length_max_six, firstNick.substring(0, 6));
            }
            String secondNick = NobleBusinessManager.getNobleRoomNick(
                    loverUpMicAnimAttachment.getSecondVipId(),
                    loverUpMicAnimAttachment.isSecondIsInvisible(),
                    loverUpMicAnimAttachment.getSecondVipDate(),
                    loverUpMicAnimAttachment.getSecondNick());

            if (secondNick.length() > 6 && null != getActivity()) {
                secondNick = getActivity().getResources().getString(R.string.nick_length_max_six, secondNick.substring(0, 6));
            }

            String tips = getActivity().getString(R.string.room_lover_anim_tips, firstNick, secondNick);
            LogUtils.d(TAG, "checkForShowLoverUpMicAnim-tips:" + tips);

            SpannableStringBuilder ssb = new SpannableStringBuilder(tips);
            int firstIndex = tips.indexOf(firstNick);
            int firstLength = firstNick.length();
            int secondIndex = tips.lastIndexOf(secondNick);
            int secondLength = secondNick.length();

            int firstNickColor = NobleBusinessManager.getNobleRoomNickColor(
                    loverUpMicAnimAttachment.getFirstVipId(),
                    loverUpMicAnimAttachment.getFirstVipDate(),
                    loverUpMicAnimAttachment.isFirstIsInvisible(),
                    Color.parseColor("#FFFF00"), firstNick);

            int secondNickColor = NobleBusinessManager.getNobleRoomNickColor(
                    loverUpMicAnimAttachment.getSecondVipId(),
                    loverUpMicAnimAttachment.getSecondVipDate(),
                    loverUpMicAnimAttachment.isSecondIsInvisible(),
                    Color.parseColor("#FFFF00"), secondNick);

            ssb.setSpan(new ForegroundColorSpan(firstNickColor), firstIndex, firstIndex + firstLength, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            ssb.setSpan(new ForegroundColorSpan(Color.WHITE), firstIndex + firstLength, secondIndex, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            ssb.setSpan(new ForegroundColorSpan(secondNickColor), secondIndex, secondIndex + secondLength, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            ssb.setSpan(new ForegroundColorSpan(Color.WHITE), secondIndex + secondLength, tips.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            gameBinding.tvLoverUpMic.setText(ssb);
            gameBinding.flLoverAnim.setVisibility(View.VISIBLE);
            //gameBinding.flLoverAnim.post(newShowLoverUpMicAnimRunnalbe);

            //直接用平移动画就可以啊，为什么要用上面乱七八燥的东西？by liaoxy
            TranslateAnimation translateAni = new TranslateAnimation(
                    Animation.RELATIVE_TO_SELF, -1f, Animation.RELATIVE_TO_SELF,
                    0, Animation.RELATIVE_TO_SELF, 0,
                    Animation.RELATIVE_TO_SELF, 0);
            //设置动画执行的时间，单位是毫秒
            translateAni.setDuration(1000);
            //translateAni.setFillAfter(true);//不回到起始位置
            translateAni.setAnimationListener(new Animation.AnimationListener() {

                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    gameBinding.flLoverAnim.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            gameBinding.flLoverAnim.setVisibility(View.GONE);
                            upMicroMsgQueue.poll();
                            dealLoverUpMicAnimMsg(upMicroMsgQueue.peek());
                        }
                    }, 1500);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            // 启动动画
            gameBinding.flLoverAnim.startAnimation(translateAni);
        } else if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_BOSON_FIRST) {
            //处理挚友上麦消息
            dealBosonFriendUpMicAnimMsg(chatRoomMessage);
        }
    }

    //处理挚友上麦
    private void dealBosonFriendUpMicAnimMsg(ChatRoomMessage chatRoomMessage) {
        if (getActivity() == null) {
            return;
        }
        LogUtils.d(TAG, "dealLoverUpMicAnimMsg-chatRoomMessage:" + chatRoomMessage);
        if (chatRoomMessage.getMsgType() == MsgTypeEnum.custom) {
            MsgAttachment msgAttachment = chatRoomMessage.getAttachment();
            if (null == msgAttachment) {
                return;
            }
            CustomAttachment attachment = (CustomAttachment) msgAttachment;
            LogUtils.d(TAG, "dealLoverUpMicAnimMsg-attachment.first:" + attachment.getFirst() + " second:" + attachment.getSecond());
            if (attachment.getFirst() == CustomAttachment.CUSTOM_MSG_BOSON_FIRST) {
                BosonFriendUpMicAnimAttachment bfUpMicAnimAttachment = (BosonFriendUpMicAnimAttachment) attachment;
                //修改提示文本显示样式
                String firstNick = NobleBusinessManager.getNobleRoomNick(
                        bfUpMicAnimAttachment.getFirstVipId(),
                        bfUpMicAnimAttachment.isFirstIsInvisible(),
                        bfUpMicAnimAttachment.getFirstVipDate(),
                        bfUpMicAnimAttachment.getFirstNick());
                if (firstNick.length() > 4 && null != getActivity()) {
                    firstNick = getActivity().getResources().getString(R.string.nick_length_max_six, firstNick.substring(0, 4));
                }
                String secondNick = NobleBusinessManager.getNobleRoomNick(
                        bfUpMicAnimAttachment.getSecondVipId(),
                        bfUpMicAnimAttachment.isSecondIsInvisible(),
                        bfUpMicAnimAttachment.getSecondVipDate(),
                        bfUpMicAnimAttachment.getSecondNick());

                if (secondNick.length() > 4 && null != getActivity()) {
                    secondNick = getActivity().getResources().getString(R.string.nick_length_max_six, secondNick.substring(0, 4));
                }

                String bosonFriendLevelName = bfUpMicAnimAttachment.getBosonFriendLevelName();
                String tips = getActivity().getString(R.string.room_bosonfriend_anim_tips, bosonFriendLevelName, firstNick, secondNick);
                LogUtils.d(TAG, "checkForShowLoverUpMicAnim-tips:" + tips);

                SpannableStringBuilder ssb = new SpannableStringBuilder(tips);
                int firstIndex = tips.indexOf(firstNick);
                int firstLength = firstNick.length();
                int secondIndex = tips.lastIndexOf(secondNick);
                int secondLength = secondNick.length();

                int firstNickColor = NobleBusinessManager.getNobleRoomNickColor(
                        bfUpMicAnimAttachment.getFirstVipId(),
                        bfUpMicAnimAttachment.getFirstVipDate(),
                        bfUpMicAnimAttachment.isFirstIsInvisible(),
                        Color.parseColor("#FFFF00"), firstNick);

                int secondNickColor = NobleBusinessManager.getNobleRoomNickColor(
                        bfUpMicAnimAttachment.getSecondVipId(),
                        bfUpMicAnimAttachment.getSecondVipDate(),
                        bfUpMicAnimAttachment.isSecondIsInvisible(),
                        Color.parseColor("#FFFF00"), secondNick);

                ssb.setSpan(new ForegroundColorSpan(Color.WHITE), 0, firstIndex, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                ssb.setSpan(new ForegroundColorSpan(firstNickColor), firstIndex, firstIndex + firstLength, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                ssb.setSpan(new ForegroundColorSpan(Color.WHITE), firstIndex + firstLength, secondIndex, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                ssb.setSpan(new ForegroundColorSpan(secondNickColor), secondIndex, secondIndex + secondLength, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                ssb.setSpan(new ForegroundColorSpan(Color.WHITE), secondIndex + secondLength, tips.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);

                try {
                    gameBinding.tvBosonFriendUpMic.setText(ssb);
                    int level = bfUpMicAnimAttachment.getBosonFriendLevel();
                    if (null != getActivity()) {
                        int bfUpMicroHeadResId = getActivity().getResources().getIdentifier("ic_bf_upmicro_head_" + level, "drawable", mContext.getPackageName());
                        int bfUpMicroBgResId = getActivity().getResources().getIdentifier("ic_bf_upmicro_bg_" + level, "drawable", mContext.getPackageName());
                        gameBinding.imvBosonFriendUpMicHead.setImageResource(bfUpMicroHeadResId);
                        gameBinding.imvBosonFriendUpMicBg.setImageResource(bfUpMicroBgResId);
                    }
                    gameBinding.flBosonFriendAnim.setVisibility(View.VISIBLE);

                    TranslateAnimation translateAni = new TranslateAnimation(
                            Animation.RELATIVE_TO_SELF, -1f, Animation.RELATIVE_TO_SELF,
                            0, Animation.RELATIVE_TO_SELF, 0,
                            Animation.RELATIVE_TO_SELF, 0);
                    //设置动画执行的时间，单位是毫秒
                    translateAni.setDuration(1000);
                    //translateAni.setFillAfter(true);//不回到起始位置
                    translateAni.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            gameBinding.flBosonFriendAnim.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    gameBinding.flBosonFriendAnim.setVisibility(View.GONE);
                                    if (upMicroMsgQueue != null) {
                                        upMicroMsgQueue.poll();
                                        dealLoverUpMicAnimMsg(upMicroMsgQueue.peek());
                                    }
                                }
                            }, 1500);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    // 启动动画
                    gameBinding.flBosonFriendAnim.startAnimation(translateAni);
                } catch (Exception e) {
                    e.printStackTrace();
                    upMicroMsgQueue.poll();
                    dealLoverUpMicAnimMsg(upMicroMsgQueue.peek());
                }
            }
        }
    }

    Runnable stopLoverUpMicAnimRunnalbe = new Runnable() {
        @Override
        public void run() {
            LogUtils.d(TAG, "stopLoverUpMicAnimRunnalbe-run");
            gameBinding.flLoverAnim.setVisibility(View.GONE);
            upMicroMsgQueue.poll();
            dealLoverUpMicAnimMsg(upMicroMsgQueue.peek());
        }
    };

    public void releaseLoverUpMicAnim() {
        if (null != gameBinding && null != gameBinding.flLoverAnim) {
            gameBinding.flLoverAnim.removeCallbacks(showLoverUpMicAnimRunnalbe);
            gameBinding.flLoverAnim.removeCallbacks(newShowLoverUpMicAnimRunnalbe);
            if (null != loverUpMicInAnim) {
                loverUpMicInAnim.removeAllListeners();
                loverUpMicInAnim.cancel();
                loverUpMicInAnim = null;
            }
            gameBinding.flLoverAnim.removeCallbacks(stopLoverUpMicAnimRunnalbe);
        }
        if (null != handler) {
            handler.removeMessages(MSG_WHAT_SHOW_LOVER_UP_MIC_ANIM);
        }
    }

    //----------------------引爆礼物全服通知----------------------------------
// 引爆礼物id
    private int detonatingGifts;

    //引爆人头像
    private String detonatingAvatar;
    //引爆人昵称
    private String detonatingNick;

    //下面的几个字段由于暂时是仅在引爆球svga动画显示时才能触发弹出，所以暂时没什么用
    //引爆协议有效期，以秒为单位
    private long detonatingDuration;
    // 引爆起始时间，协议约定有效时间十五分钟
    private long detonatingTime;
    // 引爆进度,-1为隐藏
    private int detonatingState;
    //引爆人ID
    private long detonatingUid;

    private final static int REQUEST_CODE_DETONATE_GIFT_DIALOG = 1;

    /**
     * 显示引爆礼物信息墙
     */
    private void showDetnoateGiftNotifyDialog() {
        RoomInfo roomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        DetonateGiftNotifyDialog.startForResult(this,
                detonatingGifts,
                null == roomInfo ? 0L : roomInfo.getUid(),
                detonatingNick,
                detonatingAvatar,
                detonatingGifts,
                REQUEST_CODE_DETONATE_GIFT_DIALOG);
    }

    /**
     * 进入房间或者接收到当前房间引爆进度更新通知后，更新引爆进度
     * 控制在引爆进度>=100的情况下才更新信息
     *
     * @param detonatingUid
     * @param detonatingNick
     * @param detonatingAvatar
     * @param detonatingGifts
     * @param detonatingTime
     * @param detonatingDuration
     * @param detonatingState
     */
    private void updateCurrRoomGiftDetoanteStatus(long detonatingUid, String detonatingNick,
                                                  String detonatingAvatar, int detonatingGifts,
                                                  long detonatingTime, long detonatingDuration,
                                                  int detonatingState) {
        LogUtils.d(TAG, "updateCurrRoomGiftDetoanteStatus-detonatingUid:" + detonatingUid
                + " detonatingNick:" + detonatingNick + " detonatingAvatar:" + detonatingAvatar
                + " detonatingGifts:" + detonatingGifts + " detonatingTime:" + detonatingTime
                + " detonatingDuration:" + detonatingDuration + " detonatingState:" + detonatingState);
        this.detonatingUid = detonatingUid;
        this.detonatingNick = detonatingNick;
        this.detonatingAvatar = detonatingAvatar;
        this.detonatingGifts = detonatingGifts;
        this.detonatingTime = detonatingTime;
        this.detonatingDuration = detonatingDuration;
        this.detonatingState = detonatingState;
    }

    private final int MSG_WHAT_HIDE_GIFT_DETONATE_NOTIFY = 1;

    private TranslateAnimation animation;

    private Runnable giftDetonateAnimRunnable = new Runnable() {
        @Override
        public void run() {
            animation = new TranslateAnimation(-gameBinding.flSuperGiftFullServNoty.getMeasuredWidth(), 0, 0, 0);
            animation.setDuration(2000);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    gameBinding.flSuperGiftFullServNoty.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    handler.sendEmptyMessageDelayed(MSG_WHAT_HIDE_GIFT_DETONATE_NOTIFY, 4000l);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            gameBinding.flSuperGiftFullServNoty.startAnimation(animation);
        }
    };

    private void showGiftDetonateAnim(long uid, long roomUid,int roomType, String nick, String avatar, int giftId) {
        LogUtils.d(TAG, "showGiftDetonateAnim-uid:" + uid + " roomUid:" + roomUid + " nick:" + nick + " giftId:" + giftId + " avatar:" + avatar);
        GiftInfo info = CoreManager.getCore(IGiftCore.class).getGiftInfoByGiftId(giftId);
        if (null == info || getActivity() == null) {
            return;
        }
        //富文本
        if (!TextUtils.isEmpty(avatar)) {
            ImageLoadUtils.loadCircleImage(getActivity(), avatar, gameBinding.ivDetonateAvatar, R.drawable.icon_default_auction_avatar);
        }
//        gameBinding.ivDetonateAvatar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                UserInfoActivity.start(getActivity(),uid);
//            }
//        });
        if (!TextUtils.isEmpty(nick) && null != getActivity()) {
            if (nick.length() > 6) {
                nick = getActivity().getResources().getString(R.string.nick_length_max_six, nick.substring(0, 6));
            }
            String content = getActivity().getResources().getString(R.string.room_detonate_gift_notify, nick, info.getGiftName());
            SpannableStringBuilder ssb = new SpannableStringBuilder(content);
            int firstIndex = content.indexOf(nick);
            ssb.setSpan(new ForegroundColorSpan(Color.WHITE), 0, firstIndex, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#E7DA27")), firstIndex, firstIndex + nick.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            int secondIndex = content.indexOf(info.getGiftName());
            ssb.setSpan(new ForegroundColorSpan(Color.WHITE), firstIndex + nick.length(), secondIndex, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#E7DA27")), secondIndex, secondIndex + info.getGiftName().length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            ssb.setSpan(new ForegroundColorSpan(Color.WHITE), secondIndex + info.getGiftName().length(), content.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            gameBinding.tvNotifyContent.setText(ssb);
        }
        gameBinding.flSuperGiftFullServNoty.setVisibility(View.INVISIBLE);
        gameBinding.flSuperGiftFullServNoty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (null != AvRoomDataManager.get().mCurrentRoomInfo && roomUid != AvRoomDataManager.get().mCurrentRoomInfo.getUid()) {
//                    AVRoomActivity.start(getActivity(), roomUid);
//                }
                RoomInfo roomInfo = RoomServiceScheduler.getCurrentRoomInfo();
                if (null != roomInfo && roomInfo.getUid() != roomUid && roomInfo.getType() != roomType) {
                    RoomServiceScheduler.getInstance().enterOtherTypeRoomFromService(getContext(), roomUid,roomType);
                }
            }
        });
        gameBinding.flSuperGiftFullServNoty.postDelayed(giftDetonateAnimRunnable, 0l);
    }

    public void releaseGiftDetonateAnim() {
        if (gameBinding.svgaDenoteAnim.isAnimating()) {
            gameBinding.svgaDenoteAnim.stopAnimation(true);
        }
        if (null != gameBinding && null != gameBinding.flSuperGiftFullServNoty) {
            gameBinding.flSuperGiftFullServNoty.removeCallbacks(giftDetonateAnimRunnable);
            if (null != animation) {
                animation.setAnimationListener(null);
                animation.cancel();
                animation = null;
            }
        }
        if (null != handler) {
            handler.removeMessages(MSG_WHAT_HIDE_GIFT_DETONATE_NOTIFY);
            handler.removeMessages(MSG_WHAT_HIDE_GIFT_DETONATE_ANIM);
            handler.removeMessages(MSG_WHAT_UPDATE_GIFT_DETONATE_PROGRESS);
        }
    }

    //------------------------引爆进度及引爆特效------------------------------------

    private final int MSG_WHAT_HIDE_GIFT_DETONATE_ANIM = 2;
    private final int MSG_WHAT_UPDATE_GIFT_DETONATE_PROGRESS = 3;
    private TouchAndClickOperaUtils touchAndClickOperaUtils;
    private int lastGiftDetonatingProgress = -1;
    private final int MSG_WHAT_HIDE_ROOM_RED_PACKET_BANNER = 5;


    //1.修改当前礼物引爆进度的方法
    private void showGiftDetonatingProgress(int progress, long duar, boolean shouldRequestRoomInfo) {
        LogUtils.d(TAG, "showGiftDetonatingProgress-progress:" + progress + " shouldRequestRoomInfo:" + shouldRequestRoomInfo + " duar:" + duar);
        if (null == touchAndClickOperaUtils && null != getActivity()) {
            int mWidthPixels = getActivity().getResources().getDisplayMetrics().widthPixels;
            int mHeightPixels = getActivity().getResources().getDisplayMetrics().heightPixels;
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) gameBinding.flDenoteGift.getLayoutParams();
            int width1 = lp.width;
            int height1 = lp.height;
            int mh = mHeightPixels - height1 - DisplayUtility.dp2px(getActivity(), 285);
            int mw = mWidthPixels - width1 - DisplayUtility.dp2px(getActivity(), 5);
            lp.leftMargin = mw;
            lp.topMargin = mh;
            gameBinding.flDenoteGift.setLayoutParams(lp);
//            gameBinding.flGiftDenoteProgress.getParent().requestLayout();

            touchAndClickOperaUtils = new TouchAndClickOperaUtils(getActivity());
            touchAndClickOperaUtils.setTargetView(gameBinding.flDenoteGift);
            touchAndClickOperaUtils.setMarginButtom(DisplayUtility.dp2px(getActivity(), 40));
            touchAndClickOperaUtils.setListener(new TouchAndClickOperaUtils.OnQuickClickListener() {
                @Override
                public void onQuickClick() {
                    if (View.VISIBLE == gameBinding.svgaDenoteAnim.getVisibility()) {
                        showDetnoateGiftNotifyDialog();
                    }
                }
            });
        }

        handler.removeMessages(MSG_WHAT_HIDE_GIFT_DETONATE_ANIM);
        if (-1 == progress) {
            //隐藏礼物引爆进度和引爆动画
            gameBinding.flGiftDenoteProgress.setVisibility(View.GONE);
            gameBinding.wrpvDenoteProgress.release();
            gameBinding.svgaDenoteAnim.setVisibility(View.GONE);
            gameBinding.flDenoteGift.setVisibility(View.GONE);
        } else if (100 <= progress) {
            if (duar <= 0L) {
                gameBinding.flDenoteGift.setVisibility(View.GONE);
                return;
            }
            //隐藏礼物引爆进度,
            gameBinding.flDenoteGift.setVisibility(View.VISIBLE);
            gameBinding.flGiftDenoteProgress.setVisibility(View.GONE);
            gameBinding.wrpvDenoteProgress.release();
            try {
                SVGAParser svgaParser = new SVGAParser(getActivity());
                svgaParser.parse(new URL(UriProvider.detonateGiftAnimUrl()), new SVGAParser.ParseCompletion() {
                    @Override
                    public void onComplete(SVGAVideoEntity svgaVideoEntity) {
                        LogUtils.d(TAG, "showGiftDetonatingProgress-parse-onComplete");
                        if (null != gameBinding && null != gameBinding.svgaDenoteAnim) {
                            SVGADrawable svgaDrawable = new SVGADrawable(svgaVideoEntity);
                            gameBinding.svgaDenoteAnim.setImageDrawable(svgaDrawable);
                            //显示引爆动画,加载svga动画文件
                            gameBinding.svgaDenoteAnim.setVisibility(View.VISIBLE);
                            gameBinding.svgaDenoteAnim.startAnimation();
                        }
                    }

                    @Override
                    public void onError() {
                        LogUtils.d(TAG, "showGiftDetonatingProgress-parse-onError");
                    }
                });
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            handler.sendEmptyMessageDelayed(MSG_WHAT_HIDE_GIFT_DETONATE_ANIM, duar * 1000L);
            if (shouldRequestRoomInfo) {
                // 引爆礼物之后要重新查询一遍roominfo和giftinfolist
                getMvpPresenter().updateRoomInfoAndGiftInfo();
            }
        } else {
            //显示礼物引爆进度,
            gameBinding.flDenoteGift.setVisibility(View.VISIBLE);
            gameBinding.flGiftDenoteProgress.setVisibility(View.VISIBLE);
            gameBinding.wrpvDenoteProgress.setProgress(progress);
            //隐藏引爆动画
            if (gameBinding.svgaDenoteAnim.isAnimating()) {
                gameBinding.svgaDenoteAnim.stopAnimation(true);
            }
            gameBinding.svgaDenoteAnim.setVisibility(View.GONE);
        }
    }

    //------------------------------年度房间排行榜--------------------------

    private long startTime = 0L;
    private final long retractAfterTime = 1000L * 5;
    private boolean isJumpToAnnualRoomRankWebUrl = false;

    private View.OnClickListener onOpenClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //判断是否超过五秒，五秒内要把计时器关掉
            if (System.currentTimeMillis() - startTime <= retractAfterTime) {
                gameBinding.flAnnualRoomListOpen.removeCallbacks(retractAnnualRoomRankRunnable);
            }
            if (v.getId() == R.id.ll_annualRoomRank) {
                if (ButtonUtils.isFastDoubleClick(v.getId())) {
                    return;
                }
                if (null != AvRoomDataManager.get().mServiceRoominfo
                        && !TextUtils.isEmpty(AvRoomDataManager.get().mServiceRoominfo.getAnnualCeremonyUrl())) {
                    CommonWebViewActivity.start(getActivity(), AvRoomDataManager.get().mServiceRoominfo.getAnnualCeremonyUrl());
                }
            } else if (v.getId() == R.id.fl_annualRoomListOpen) {
                isJumpToAnnualRoomRankWebUrl = false;
                //open向右补间动画-->retract向左补间动画
                retractRoomRankOpenView();
            }
        }
    };

    private void retractRoomRankOpenView() {
        LogUtils.d(TAG, "retractRoomRankOpenView");
        TranslateAnimation translateAnimation = new TranslateAnimation(0, DisplayUtility.dp2px(getActivity(), 93), 0, 0);
        translateAnimation.setDuration(1000L);
//        gameBinding.flAnnualRoomListOpen.setAnimation(translateAnimation);
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                gameBinding.flAnnualRoomListOpen.setVisibility(View.GONE);
                gameBinding.ivAnnualRoomListRetract.setVisibility(View.INVISIBLE);
                if (isJumpToAnnualRoomRankWebUrl && null != AvRoomDataManager.get().mServiceRoominfo
                        && !TextUtils.isEmpty(AvRoomDataManager.get().mServiceRoominfo.getAnnualCeremonyUrl())) {
                    CommonWebViewActivity.start(getActivity(), AvRoomDataManager.get().mServiceRoominfo.getAnnualCeremonyUrl());
                }
                openRoomRankRetractView();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
//        translateAnimation.start();
        translateAnimation.setFillAfter(false);//设置为true，动画完了之后停留在最后状态；false的话会返回view的初始状态
        gameBinding.flAnnualRoomListOpen.startAnimation(translateAnimation);
    }

    private void openRoomRankOpenView() {
        LogUtils.d(TAG, "openRoomRankOpenView");
        TranslateAnimation translateAnimation = new TranslateAnimation(DisplayUtility.dp2px(getActivity(), 93), 0, 0, 0);
        translateAnimation.setDuration(1000L);
//        gameBinding.flAnnualRoomListOpen.setAnimation(translateAnimation);
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                gameBinding.flAnnualRoomListOpen.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
//        translateAnimation.start();
        translateAnimation.setFillAfter(false);//设置为true，动画完了之后停留在最后状态；false的话会返回view的初始状态
        gameBinding.flAnnualRoomListOpen.startAnimation(translateAnimation);
    }

    private void openRoomRankRetractView() {
        LogUtils.d(TAG, "openRoomRankRetractView");
        TranslateAnimation translateAnimation = new TranslateAnimation(DisplayUtility.dp2px(getActivity(), 38), 0, 0, 0);
        translateAnimation.setDuration(1000L);
//        gameBinding.ivAnnualRoomListRetract.setAnimation(translateAnimation);
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                gameBinding.ivAnnualRoomListRetract.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
//        translateAnimation.start();
        translateAnimation.setFillAfter(false);//设置为true，动画完了之后停留在最后状态；false的话会返回view的初始状态
        gameBinding.ivAnnualRoomListRetract.startAnimation(translateAnimation);
    }

    private void retractRoomRankRetractView() {
        LogUtils.d(TAG, "retractRoomRankRetractView");
        TranslateAnimation translateAnimation = new TranslateAnimation(0, DisplayUtility.dp2px(getActivity(), 38), 0, 0);
        translateAnimation.setDuration(1000L);
//        gameBinding.ivAnnualRoomListRetract.setAnimation(translateAnimation);
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                gameBinding.ivAnnualRoomListRetract.setVisibility(View.GONE);
                gameBinding.flAnnualRoomListOpen.setVisibility(View.INVISIBLE);
                openRoomRankOpenView();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
//        translateAnimation.start();
        translateAnimation.setFillAfter(false);//设置为true，动画完了之后停留在最后状态；false的话会返回view的初始状态
        gameBinding.ivAnnualRoomListRetract.startAnimation(translateAnimation);
    }

    private View.OnClickListener onRetractClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //retract向右补间动画-->open向左补间动画
            retractRoomRankRetractView();
        }
    };

    private Runnable retractAnnualRoomRankRunnable = new Runnable() {
        @Override
        public void run() {
            //open向右补间动画-->retract向左补间动画
            isJumpToAnnualRoomRankWebUrl = false;
            retractRoomRankOpenView();
        }
    };

    private void initAnnualRoomRankView() {
        if (null == getActivity()) {
            return;
        }
        startTime = System.currentTimeMillis();
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) gameBinding.flAnnualRoomList.getLayoutParams();
        int mWidthPixels = getActivity().getResources().getDisplayMetrics().widthPixels;
        int mHeightPixels = getActivity().getResources().getDisplayMetrics().heightPixels;
        int mh = mHeightPixels - DisplayUtility.dp2px(getActivity(), 285);
        int mw = mWidthPixels - DisplayUtility.dp2px(getActivity(), 102);
        lp.topMargin = mh;
        lp.leftMargin = mw;
        gameBinding.flAnnualRoomList.setLayoutParams(lp);
        gameBinding.flAnnualRoomListOpen.setOnClickListener(onOpenClickListener);
        gameBinding.llAnnualRoomRank.setOnClickListener(onOpenClickListener);
        gameBinding.ivAnnualRoomListRetract.setOnClickListener(onRetractClickListener);
        if (null != AvRoomDataManager.get().mServiceRoominfo) {
            Long ranking = AvRoomDataManager.get().mServiceRoominfo.getRanking();
            boolean hideAnnualRoomRank = null == ranking || ranking.longValue() == 0L;
            if (!hideAnnualRoomRank) {
                gameBinding.flAnnualRoomListOpen.postDelayed(retractAnnualRoomRankRunnable, retractAfterTime);
            }
            gameBinding.bannerActivity.setVisibility(hideAnnualRoomRank ? View.VISIBLE : View.GONE);
            gameBinding.flAnnualRoomList.setVisibility(hideAnnualRoomRank ? View.GONE : View.VISIBLE);
            gameBinding.tvAnnualRank.setText(getActivity().getString(R.string.annual_room_list_rank,
                    hideAnnualRoomRank ? "0" : (ranking.longValue() + "")));
            Double nextScore = AvRoomDataManager.get().mServiceRoominfo.getNextScore();
            gameBinding.tvAnnualRankNextCoins.setText(null == nextScore ? "0" : (nextScore.longValue() + ""));

            UserInfo ownerUserInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(AvRoomDataManager.get().mServiceRoominfo.getUid());
            if (null != ownerUserInfo) {
                String nick = ownerUserInfo.getNick();
                if (!TextUtils.isEmpty(nick) && nick.length() > 6) {
                    nick = nick.substring(0, 6) + "...";
                }
                gameBinding.tvFirstRankNick.setText(nick);
                GlideApp.with(this).load(ownerUserInfo.getAvatar()).dontAnimate().into(gameBinding.civFirstRankHead);
            }

            if (null != ranking) {
                //left top right buttom
                gameBinding.llAnnualRoomRank.setPadding(0,
                        DisplayUtility.dp2px(getActivity(), ranking.longValue() == 1L ? 40 : 45),
                        0, 0);
                gameBinding.tvNextScoreTips.setVisibility(ranking.longValue() == 1L ? View.GONE : View.VISIBLE);
                gameBinding.tvAnnualRankNextCoins.setVisibility(ranking.longValue() == 1L ? View.GONE : View.VISIBLE);
                gameBinding.llAnnualRoomRankFirst.setVisibility(ranking.longValue() == 1L ? View.VISIBLE : View.GONE);
                gameBinding.tvFirstRankNick.setVisibility(ranking.longValue() == 1L ? View.VISIBLE : View.GONE);
            }
        }
    }

    private void releaseAnnualRoomRankView() {
        gameBinding.flAnnualRoomListOpen.removeCallbacks(retractAnnualRoomRankRunnable);
        Animation anim = gameBinding.flAnnualRoomListOpen.getAnimation();
        if (null != anim) {
            anim.setAnimationListener(null);
            anim.cancel();
        }
        anim = gameBinding.ivAnnualRoomListRetract.getAnimation();
        if (null != anim) {
            anim.setAnimationListener(null);
            anim.cancel();
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfo(UserInfo info) {
        if (null != AvRoomDataManager.get().mServiceRoominfo && null != info
                && AvRoomDataManager.get().mServiceRoominfo.getUid() == info.getUid()
                && gameBinding.tvFirstRankNick.getVisibility() == View.VISIBLE) {
            String nick = info.getNick();
            if (!TextUtils.isEmpty(nick) && nick.length() > 6) {
                nick = nick.substring(0, 6) + "...";
            }
            gameBinding.tvFirstRankNick.setText(nick);
            GlideApp.with(this).load(info.getAvatar()).dontAnimate().into(gameBinding.civFirstRankHead);
        }
    }

    //-----------------------------------底部更多按钮---------------------------------

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        releaseView();
        releaseLoverUpMicAnim();
        releaseGiftDetonateAnim();
        releaseAnnualRoomRankView();

        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        if (handler != null) {
            handler.removeMessages(MSG_WHAT_HIDE_ENTER_ROOM_ANIM);
            handler.removeMessages(MSG_WHAT_HIDE_OPEN_NOBLE_ANIM);
            handler.removeCallbacksAndMessages(null);
            handler = null;
        }

        if (null != gameBinding && null != gameBinding.rlOpenNobleAnim) {
            Animation animation = gameBinding.rlOpenNobleAnim.getAnimation();
            if (null != animation && !animation.hasEnded()) {
                animation.cancel();
            }
        }
        if (gameBinding != null && gameBinding.microView != null) {
            gameBinding.microView.setMicroGuideViewLocationListener(null);
        }
        svRedPacketRain.stopAnimation();
        svRedPacketRain.setVisibility(View.GONE);
        gameBinding.unbind();
    }

    @Override
    public void onClick(View v) {
        if (ButtonUtils.isFastDoubleClick(v.getId())) {
            return;
        }
        switch (v.getId()) {
            case R.id.ivMoreOperation:
                onMoreOperaClick();
                break;
            case R.id.ll_room_fortune_list:
                onContributeListClick();
                break;
            case R.id.btn_send:
                if (null == getActivity()) {
                    return;
                }
                if (!RealNameAuthStatusChecker.getInstance().needBindPhoneFirst(getActivity(),
                        getActivity().getResources().getString(R.string.phone_bind_tips))) {
                    return;
                }

                RoomInfo mCurrentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
                if (mCurrentRoomInfo != null && mCurrentRoomInfo.getPublicChatSwitch() == 1) {
                    toast("公屏消息已经被管理员禁止，请稍候再试");
                    return;
                }

                String sensitiveWordData = CoreManager.getCore(VersionsCore.class).getSensitiveWordData();
                String content = gameBinding.etMsg.getText().toString().trim();
                if (!TextUtils.isEmpty(sensitiveWordData) && !TextUtils.isEmpty(content)
                        && content.replaceAll("\n", "").replace(" ", "").matches(sensitiveWordData)) {
                    SingleToastUtil.showToast(getActivity().getResources().getString(R.string.sensitive_word_data));
                    return;
                }

                Disposable disposable = getMvpPresenter().sendTextMsg(content);
                if (null != mCompositeDisposable && null != disposable) {
                    mCompositeDisposable.add(disposable);
                }
                gameBinding.etMsg.setText("");
                break;
            case R.id.play_together:
                if (!CoreManager.getCore(IFaceCore.class).isShowingFace()) {
                    FaceInfo faceInfo = CoreManager.getCore(IFaceCore.class).getPlayTogetherFace();
                    if (faceInfo != null) {
                        Disposable disposable1 = CoreManager.getCore(IFaceCore.class).sendAllFace(faceInfo);
                        if (null != disposable1 && null != mCompositeDisposable) {
                            mCompositeDisposable.add(disposable1);
                        }
                    } else {
                        toast("加载失败，请重试!");
                    }
                }
                break;
            case R.id.tv_nick:
                if (giftAttachment == null) {
                    return;
                }
                nickClickDialog(true);
                break;
            case R.id.tv_nick_target:
                if (giftAttachment == null) {
                    return;
                }
                nickClickDialog(false);
                break;
            default:
                break;
        }
    }

    public void setShowMoreOperaBtn(boolean showMoreOperaBtn) {
        isShowMoreOperaBtn = showMoreOperaBtn;
        refreshMoreOperaBtnShowStatus();
    }

    private void refreshMoreOperaBtnShowStatus() {
        if (null != gameBinding && null != gameBinding.ivMoreOperation) {
            gameBinding.ivMoreOperation.setVisibility(isShowMoreOperaBtn ? View.VISIBLE : View.GONE);
        }
    }

    public void setOnPkVoteInfoChangeListener(OnPkVoteInfoChangeListener onPkVoteInfoChangeListener) {
        this.onPkVoteInfoChangeListener = onPkVoteInfoChangeListener;
    }

    public void onMoreOperaClick() {
        //测试炸房校验逻辑的测试代码,备用测试
//        AgoraEngineManager.get().setRole(AgoraEngineManager.get().isAudienceRole ? io.agora.rtc.Constants.CLIENT_ROLE_BROADCASTER : io.agora.rtc.Constants.CLIENT_ROLE_AUDIENCE);

        RoomInfo serverRoomInfo = AvRoomDataManager.get().mServiceRoominfo;
        RoomMoreOpearDialog roomMoreOpearDialog = new RoomMoreOpearDialog(getActivity());
        boolean showLuckyWheel = (serverRoomInfo != null && serverRoomInfo.isBigWheelSwitch());
        boolean isRedPacketSwitch = (serverRoomInfo != null && serverRoomInfo.isRedPacketSwitch());
        RoomInfo mCurrentRoomInfo = AvRoomDataManager.get().mCurrentRoomInfo;
        boolean hasPKOpened = false;
        if (null != onPkVoteInfoChangeListener) {
            hasPKOpened = onPkVoteInfoChangeListener.hasPKOpened();
        }
        roomMoreOpearDialog.setHasPKOpened(hasPKOpened);
        roomMoreOpearDialog.setLuckyWheelSwitch(showLuckyWheel);
        roomMoreOpearDialog.setRedPacketSwitch(isRedPacketSwitch);
        roomMoreOpearDialog.setCharmValueSwitch(mCurrentRoomInfo != null && mCurrentRoomInfo.getCharmSwitch() == 1);
        roomMoreOpearDialog.show();
    }

    public interface OnPkVoteInfoChangeListener {
        boolean hasPKOpened();
    }

    private void dealRoomEvent(RoomEvent roomEvent) {
        if (roomEvent == null) {
            return;
        }
        LogUtils.d("dealRoomEvent", "roomEven:" + roomEvent.getEvent());
        int event = roomEvent.getEvent();
        switch (event) {
            case RoomEvent.ROOM_CHAT_RECONNECTION:
                LogUtils.d("noticeImNetReLogin", "chatRoomReConnect1");
                //从新获取队列信息
                Disposable disposable = getMvpPresenter().chatRoomReConnect(roomEvent.roomQueueInfo);
                if (null != disposable && null != mCompositeDisposable) {
                    mCompositeDisposable.add(disposable);
                }
                initPair();
                break;
            case RoomEvent.ROOM_EXIT:
                //清楚静态内部类的数据，使activity可以被销毁
                if (gameBinding.messageView != null) {
                    gameBinding.messageView.clear();
                }
                break;
            case RoomEvent.DOWN_CROWDED_MIC:
                if (AvRoomDataManager.get().isOwner(roomEvent.getAccount())) {
                    toast(R.string.crowded_down);
                }
                break;
            case RoomEvent.ROOM_MANAGER_ADD:
            case RoomEvent.ROOM_MANAGER_REMOVE:
                updateView();
                if (gameBinding != null) {
                    gameBinding.microView.getPairStateView().initPermission();
                }
                break;
            case RoomEvent.ROOM_INFO_UPDATE:
                //更新一下房间信息
                getMvpPresenter().updateRoomInfo();
            case RoomEvent.ENTER_ROOM:
                if (gameBinding != null) {
                    gameBinding.microView.getPairStateView().initState();
                }
                updateView();
                updateRemoteMuteBtn();
                onMicInListChange();
                if (roomEvent != null && roomEvent.getRoomInfo() != null) {
                    updateMicCharm(roomEvent.getRoomInfo().getCharmSwitch());
                }
                break;
            case RoomEvent.ADD_BLACK_LIST:
                onChatRoomMemberBlackAdd(roomEvent.getAccount());
                break;
            case RoomEvent.MIC_QUEUE_STATE_CHANGE:
                onQueueMicStateChange(roomEvent.getMicPosition(), roomEvent.getPosState());
                break;
            case RoomEvent.KICK_DOWN_MIC:
                if (null != getActivity()) {
                    /*getMvpPresenter().downMicroPhone(roomEvent.getMicPosition(), false);*/
                    SingleToastUtil.showToast(getActivity().getResources().getString(R.string.kick_mic));
                }
                break;
            case RoomEvent.DOWN_MIC:
                onDownMicro(roomEvent.getMicPosition());
                break;
            case RoomEvent.UP_MIC:
                onUpMicro(roomEvent.getMicPosition());
                checkForShowLoverUpMicAnim(roomEvent.getAccount());
                break;
            case RoomEvent.INVITE_UP_MIC:
                onInviteUpMic(roomEvent.getMicPosition());
                break;
            case RoomEvent.KICK_OUT_ROOM:
                ChatRoomKickOutEvent reason = roomEvent.getReason();
                if (reason != null && reason.getReason() == ChatRoomKickOutEvent.ChatRoomKickOutReason.CHAT_ROOM_INVALID) {
                    releaseView();
                }
                break;
            case RoomEvent.ROOM_CHARM:
                LogUtils.d(TAG, "dealRoomEvent-ROOM_CHARM MicroViewAdapter.updateMicUi");
                if (gameBinding != null) {
                    gameBinding.microView.getAdapter().updateMicUiOnCharmChanged(roomEvent.getRoomCharmList());
                }
                break;
            case RoomEvent.ROOM_PAIR:
                String pairInfo = roomEvent.getPairInfo();
                LogUtils.d(TAG, "ROOM_PAIR-pairInfo:" + pairInfo);
                Json parse = Json.parse(pairInfo);
                int second = parse.num("second");
                LogUtils.d(TAG, "ROOM_PAIR-second:" + second);
                if (gameBinding != null) {
                    PairState pairStateView = gameBinding.microView.getPairStateView();
                    if (null != AvRoomDataManager.get().pairSelectMap) {
                        synchronized (AvRoomDataManager.get().pairSelectMap) {
                            AvRoomDataManager.get().pairSelectMap.clear();
                        }
                    }
                    String currUid = CoreManager.getCore(IAuthCore.class).getCurrentUid() + "";
                    boolean isAdminOrOwner = AvRoomDataManager.get().isRoomAdmin(currUid)
                            || AvRoomDataManager.get().isRoomOwner();
                    if (second == CUSTOM_MSG_ROOM_PAIR_START) {
                        AvRoomDataManager.get().isShowLikeTa = true;
                        CoreManager.getCore(IPairCore.class).clearPairInfo();
                        if (null != AvRoomDataManager.get().pairSelectMap) {
                            AvRoomDataManager.get().pairSelectMap.clear();
                        }
                        //房主或者管理员显示麦上相亲选择对象麦位
                        gameBinding.microView.getAdapter().setShowPairSelectStatus(isAdminOrOwner);
                        long l = parse.num_l("duration");
                        pairStateView.startPair(l * 1000);
                    } else if (second == CUSTOM_MSG_ROOM_PAIR_END) {
                        AvRoomDataManager.get().isShowLikeTa = false;
                        gameBinding.microView.getAdapter().setShowPairSelectStatus(false);
                        pairStateView.stopPair(true);
                        Json json = CoreManager.getCore(IPairCore.class).checkPair(parse);
                        LogUtils.d(TAG, "ROOM_PAIR-json-2:" + json);
                        if (svgaAciton != null) {
                            svgaAciton.showPair(json);
                        }
                        sendRoomPairNotifyMessage(json);
                    } else if (second == CUSTOM_MSG_ROOM_PAIR_CHANGE) {
                        AvRoomDataManager.get().isShowLikeTa = true;
                        //房主或者管理员显示麦上相亲选择对象麦位
                        gameBinding.microView.getAdapter().setShowPairSelectStatus(isAdminOrOwner);
                        if (isAdminOrOwner) {
                            CoreManager.getCore(IPairCore.class).parsePairSelectMicPosition(parse);
                        }
                    }
                }
                break;
            case RoomEvent.ROOM_DETONATE_GIFT_PROGRESS: {
                DetonateGiftAttachment detonateGiftAttachment = roomEvent.getDetonateGiftAttachment();
                if (null != detonateGiftAttachment) {
                    Message msg = handler.obtainMessage(MSG_WHAT_UPDATE_GIFT_DETONATE_PROGRESS);
                    msg.obj = detonateGiftAttachment;
                    handler.sendMessageDelayed(msg, 200L);
                }
            }
            break;
            case RoomEvent.ROOM_DETONATE_GIFT_NOTIFY:
                DetonateGiftAttachment detonateGiftAttachment = roomEvent.getDetonateGiftAttachment();
                if (null != detonateGiftAttachment) {
                    Message msg = handler.obtainMessage(MSG_WHAT_UPDATE_GIFT_DETONATE_PROGRESS);
                    msg.obj = detonateGiftAttachment;
                    handler.sendMessageDelayed(msg, 200L);
                }
                break;
            case RoomEvent.ANNUAL_ROOM_RANK_NOTIFY:
                AnnualRoomRankAttachment annualRoomRankAttachment = roomEvent.getAnnualRoomRank();
                Long ranking = annualRoomRankAttachment.getRanking();
                Double nextScore = annualRoomRankAttachment.getNextScore();
                gameBinding.tvAnnualRankNextCoins.setText(null == nextScore ? "0" : (nextScore.longValue() + ""));
                boolean hideAnnualRoomRank = null == ranking || ranking.longValue() == 0L;
                gameBinding.bannerActivity.setVisibility(hideAnnualRoomRank ? View.VISIBLE : View.GONE);
                gameBinding.flAnnualRoomList.setVisibility(hideAnnualRoomRank ? View.GONE : View.VISIBLE);
                gameBinding.tvAnnualRank.setText(getActivity().getString(R.string.annual_room_list_rank,
                        hideAnnualRoomRank ? "0" : (ranking.longValue() + "")));
                break;
            case RoomEvent.ROOM_RECEIVE_RED_PACKET://收到红包
                RoomRedPacketAttachment redPacketAttachment = roomEvent.getRedPacketAttachment();
                if (redPacketAttachment != null) {
                    roomRedPacket = redPacketAttachment.getDataInfo();
                    if (roomRedPacket != null) {
                        //不是本房间成员发的红包只显示横幅
                        if (roomRedPacket.getRoomId() != mCurrentRoomInfo.getRoomId()) {
                            if (roomRedPacket.getShowAll() == 1) {
                                showBannerAnima();
                            }
                            return;
                        }
                        //显示右下角红包View
                        String nick = roomRedPacket.getNick();
                        tvName.setText(nick);
                        rlRedPacket.setEnabled(true);
                        if (roomRedPacket.getType() == 1) {
                            //手气红包
                            rlRedPacket.setVisibility(View.VISIBLE);
                            tvTimer.setVisibility(View.GONE);
                            showBannerAnima();
                            //播放红包雨动画
                            if (roomRedPacket.getGoldNum() >= 10000) {
                                playRedPacketRainAnimation(roomRedPacket.getRedEnvelopedRain());
                            }
                            return;
                        }
                        long startTime = redPacketAttachment.getMsgTime();
                        long curTime = MsgViewHolderMiniGameInvited.getCurTimeMillis();
                        long residue = curTime - startTime;
                        int timing = roomRedPacket.getTiming() * 1000;
                        rlRedPacket.setVisibility(View.VISIBLE);
                        if (residue > timing) {
                            //红包已经过期了
                            rlRedPacket.setEnabled(true);
                            tvTimer.setVisibility(View.GONE);
                            return;
                        }
                        residue = timing - residue;
                        if (residue > 1200 * 1000) {
                            residue = 1200 * 1000;
                        }
                        rlRedPacket.setEnabled(false);
                        tvTimer.setVisibility(View.VISIBLE);
                        if (timer != null) {
                            timer.cancel();
                        }
                        timer = new CountDownTimer(residue, 1000) {
                            @Override
                            public void onTick(long l) {
                                long showTime = l / 1000;
                                tvTimer.setText(showTime + "s");
                            }

                            @Override
                            public void onFinish() {
                                tvTimer.setVisibility(View.GONE);
                                rlRedPacket.setEnabled(true);
                                //播放红包雨动画
                                if (roomRedPacket.getGoldNum() >= 10000) {
                                    playRedPacketRainAnimation(roomRedPacket.getRedEnvelopedRain());
                                }
                            }
                        };
                        timer.start();
                        showBannerAnima();
                    }
                }
                break;
            case RoomEvent.ROOM_RECEIVE_RED_PACKET_FINISH:
                try {
                    RoomRedPacketFinishAttachment finishAttachment = roomEvent.getRoomRedPacketFinishAttachment();
                    RoomRedPacketFinish finish = finishAttachment.getDataInfo();
                    if (finish != null && finish.getRedPacketId().equals(roomRedPacket.getRedPacketId())) {
                        Animation animation = new AlphaAnimation(1F, 0F);
                        animation.setDuration(1000);
                        rlRedPacket.startAnimation(animation);
                        rlRedPacket.setVisibility(View.GONE);
                        if (timer != null) {
                            timer.cancel();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case RoomEvent.ROOM_RECEIVE_OPEN_NOBLE_NOTIFY:
                showOpenNobleNotifyAnim(roomEvent.getOpenNobleNotifyAttachment());
                break;
            default:
                break;
        }
    }

    //显示公爵/国王开通/续费全站横幅动画
    private void showOpenNobleNotifyAnim(OpenNobleNotifyAttachment attachment) {
        if (null == attachment || !NobleBusinessManager.showOpenNobleNotifyAnim(attachment.getVipId())
                || TextUtils.isEmpty(attachment.getNick()) || null == getActivity()) {
            return;
        }
        if (null != handler) {
            handler.removeMessages(MSG_WHAT_HIDE_OPEN_NOBLE_ANIM);
        }
        Animation animation = gameBinding.rlOpenNobleAnim.getAnimation();
        if (null != animation && !animation.hasEnded()) {
            animation.cancel();
        }

        gameBinding.rlOpenNobleAnim.setVisibility(View.VISIBLE);

        int headResId = NobleBusinessManager.getOpenNobleNotifyHeadResId(attachment.getVipId());
        if (headResId > 0) {
            gameBinding.vNobleHead.setBackgroundDrawable(getActivity().getResources().getDrawable(headResId));
        } else {
            gameBinding.vNobleHead.setBackgroundDrawable(null);
        }

        int bgResId = NobleBusinessManager.getOpenNobleNotifyBgResId(attachment.getVipId());
        if (bgResId > 0) {
            gameBinding.ivOpenNobleNotifyLevel.setImageDrawable(getActivity().getResources().getDrawable(bgResId));
        } else {
            gameBinding.ivOpenNobleNotifyLevel.setImageDrawable(null);
        }

        int width = DisplayUtility.dp2px(getActivity(), 50);
        ImageLoadUtils.loadCircleImage(getActivity(), ImageLoadUtils.toThumbnailUrl(width, width, attachment.getAvatar()),
                gameBinding.ivNobleHead, R.drawable.default_user_head);

        String content = null;
        SpannableStringBuilder ssb = null;
        if (attachment.getUserNo() > 0 && !TextUtils.isEmpty(attachment.getRoomTitle())) {
            String tips = getActivity().getResources().getString(R.string.noble_open_in_room_notify_tips,
                    attachment.getRoomTitle(), String.valueOf(attachment.getUserNo()));
            content = getActivity().getResources().getString(R.string.noble_open_in_room_notify,
                    attachment.getNick(), tips, attachment.getVipName());
            ssb = new SpannableStringBuilder(content);
            //哇，恭喜用户
            int nickIndex = content.indexOf(attachment.getNick());
            ssb.setSpan(new ForegroundColorSpan(Color.WHITE), 0,
                    nickIndex, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            int color = NobleBusinessManager.getOpenNobleNotifyColor(attachment.getVipId());
            //用户昵称
            int nickLength = attachment.getNick().length();
            ssb.setSpan(new ForegroundColorSpan(color), nickIndex,
                    nickIndex + nickLength, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            //在房间名
            int roomTipsIndex = content.lastIndexOf(tips);
            ssb.setSpan(new ForegroundColorSpan(Color.WHITE), nickIndex + nickLength,
                    roomTipsIndex, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);

            //xxx(id:xxx)
            int roomTipsLength = tips.length();
            ssb.setSpan(new ForegroundColorSpan(color), roomTipsIndex,
                    roomTipsIndex + roomTipsLength, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            //开通xxx，快去围观吧~
            ssb.setSpan(new ForegroundColorSpan(Color.WHITE), roomTipsIndex + roomTipsLength,
                    content.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            gameBinding.rlOpenNobleAnim.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AVRoomActivity.start(getActivity(), attachment.getRoomUid());
                }
            });
        } else {
            gameBinding.rlOpenNobleAnim.setOnClickListener(null);
            content = getActivity().getResources().getString(R.string.noble_open_out_room_notify,
                    attachment.getNick(), attachment.getVipName());
            ssb = new SpannableStringBuilder(content);
            //哇，恭喜用户
            int nickIndex = content.indexOf(attachment.getNick());
            ssb.setSpan(new ForegroundColorSpan(Color.WHITE), 0,
                    nickIndex, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            int color = NobleBusinessManager.getOpenNobleNotifyColor(attachment.getVipId());
            //用户昵称
            int nickLength = attachment.getNick().length();
            ssb.setSpan(new ForegroundColorSpan(color), nickIndex,
                    nickIndex + nickLength, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            //成功开通xxx，快快为TA欢呼吧~
            ssb.setSpan(new ForegroundColorSpan(Color.WHITE), nickIndex + nickLength,
                    content.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        }
        gameBinding.tvNobleNotify.setText(ssb);
        TranslateAnimation translateAni = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, -1f, Animation.RELATIVE_TO_SELF,
                0f, Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_SELF, 0);
        translateAni.setDuration(1000);
        translateAni.setRepeatCount(0);
        translateAni.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (null != handler) {
                    handler.sendEmptyMessageDelayed(MSG_WHAT_HIDE_OPEN_NOBLE_ANIM, 3000);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        gameBinding.rlOpenNobleAnim.startAnimation(translateAni);
    }
}
