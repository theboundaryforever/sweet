package com.yuhuankj.tmxq.ui.message.friend;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.tencent.bugly.crashreport.BuglyLog;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.JavaUtil;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.thirdsdk.nim.MsgCenterRedPointStatusManager;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;

import java.util.List;

/**
 * 好友列表
 *
 * @author weihaitao
 * @date 2019/4/17
 */
@CreatePresenter(FriendListPresenter.class)
public class FriendListActivity extends BaseMvpActivity<FriendListView, FriendListPresenter> implements FriendListView, SwipeRefreshLayout.OnRefreshListener, MsgCenterRedPointStatusManager.OnRedPointStatusChangedListener {

    private final String TAG = FriendListActivity.class.getSimpleName();
    private final int dataLoadStartPage = 1;
    private final int dataLoadPageSize = 10;
    private SwipeRefreshLayout srlDownLoadAnim;
    private RecyclerView rvFriendList;
    private FriendListAdapter friendListAdapter;
    private int nextDataLoadPage = 1;
    private boolean isLoadingData = false;

    public static void start(Context context) {
        context.startActivity(new Intent(context, FriendListActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_list);
        initTitleBar(getResources().getString(R.string.friend));
        if (null != mTitleBar) {
            mTitleBar.setCommonBackgroundColor(Color.WHITE);
        }
        initView();
        initListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ListUtils.isListEmpty(friendListAdapter.getData())) {
            loadData();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MsgCenterRedPointStatusManager.getInstance().removeListener(this);
    }

    private void initView() {
        srlDownLoadAnim = (SwipeRefreshLayout) findViewById(R.id.srlDownLoadAnim);
        srlDownLoadAnim.setEnabled(true);
        rvFriendList = (RecyclerView) findViewById(R.id.rvFriendList);
        LinearLayoutManager linearLayoutManager = new RecyclerViewNoBugLinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvFriendList.setLayoutManager(linearLayoutManager);
        friendListAdapter = new FriendListAdapter(R.layout.list_item_friend, com.yuhuankj.tmxq.BR.userInfo);
        rvFriendList.setAdapter(friendListAdapter);
    }

    private void initListener() {
        srlDownLoadAnim.setOnRefreshListener(this);
        friendListAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (adapter.getData().size() > position) {
                    NimUserInfo nimUserInfo = (NimUserInfo) adapter.getData().get(position);
                    UserInfoActivity.start(FriendListActivity.this, JavaUtil.str2long(nimUserInfo.getAccount()));
                }
            }
        });
        MsgCenterRedPointStatusManager.getInstance().addListener(this);
    }

    private void loadData() {
        BuglyLog.d(TAG, "loadData-isLoadingData:" + isLoadingData);
        if (!isLoadingData) {
            nextDataLoadPage = dataLoadStartPage;
            isLoadingData = true;
            getDialogManager().showProgressDialog(this, getResources().getString(R.string.network_loading));
            getMvpPresenter().getFriendList(nextDataLoadPage, dataLoadPageSize);
        }
    }

    @Override
    public void onRefresh() {
        if (!isLoadingData) {
            nextDataLoadPage = dataLoadStartPage;
            isLoadingData = true;
            BuglyLog.d(TAG, "onRefresh-nextDataLoadPage:" + nextDataLoadPage);
            getMvpPresenter().getFriendList(nextDataLoadPage, dataLoadPageSize);
        }
    }


    @Override
    public void showFriendList(List list) {
        LogUtils.d(TAG, "showBlackList");
        nextDataLoadPage += 1;
        friendListAdapter.setNewData(list);
        isLoadingData = false;
        getDialogManager().dismissDialog();
        srlDownLoadAnim.setRefreshing(false);
    }

    @Override
    public void showFriendListEmptyView() {
        LogUtils.d(TAG, "showBlackListEmptyView");
        friendListAdapter.setEmptyView(getEmptyView(rvFriendList, getString(R.string.no_frenids_text)));
        isLoadingData = false;
        getDialogManager().dismissDialog();
        srlDownLoadAnim.setRefreshing(false);
    }

    /**
     * 显示新粉丝红点
     */
    @Override
    public void showRedPointOnNewFan() {

    }

    /**
     * 显示新好友红点
     */
    @Override
    public void showRedPointOnNewFriend() {
        loadData();
    }

    /**
     * 显示新点赞红点
     */
    @Override
    public void showRedPointOnNewLike() {

    }

    /**
     * 签到红点
     */
    @Override
    public void showRedPointOnSignIn() {

    }
}
