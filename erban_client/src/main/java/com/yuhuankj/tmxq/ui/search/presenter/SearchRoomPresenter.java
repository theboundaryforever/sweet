package com.yuhuankj.tmxq.ui.search.presenter;

import com.netease.nim.uikit.common.util.string.StringUtil;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.room.bean.SearchRoomPersonInfo;
import com.tongdaxing.xchat_core.room.model.RoomSettingModel;
import com.yuhuankj.tmxq.ui.search.interfaces.SearchRoomView;

import java.util.List;
import java.util.Map;

public class SearchRoomPresenter extends AbstractMvpPresenter<SearchRoomView> {

    private final String TAG = SearchRoomPresenter.class.getSimpleName();

    private RoomSettingModel model;

    public SearchRoomPresenter() {
        model = new RoomSettingModel();
    }

    public void searchRoom(String content){
        LogUtils.d(TAG,"searchRoom-content:"+content);
        if (!StringUtil.isEmpty(content)) {
            Map<String,String> params = CommonParamUtil.getDefaultParam();
            params.put("key", content);
            params.put("type", "1");
            params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
            params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid()+"");

            OkHttpManager.getInstance().getRequest(UriProvider.searchByType(), params, new OkHttpManager.MyCallBack<ServiceResult<List<SearchRoomPersonInfo>>>() {
                @Override
                public void onError(Exception e) {
                    e.printStackTrace();
                    try {
                        getMvpView().onRoomSearched(false,e.getMessage(),null);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }

                @Override
                public void onResponse(ServiceResult<List<SearchRoomPersonInfo>> response) {
                    try {
                        if (null != response && response.isSuccess()) {
                            getMvpView().onRoomSearched(true,response.getMessage(),response.getData());
                        }else{
                            getMvpView().onRoomSearched(false,"",null);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public void refreshRecommendRoomList(){
        LogUtils.d(TAG,"refreshRecommendRoomList");
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        IAuthCore iAuthCore = CoreManager.getCore(IAuthCore.class);
        if (iAuthCore == null)
            return;
        params.put("ticket", iAuthCore.getTicket());
        params.put("uid", iAuthCore.getCurrentUid() + "");

        OkHttpManager.getInstance().getRequest(UriProvider.recommRoom(), params, new OkHttpManager.MyCallBack<ServiceResult<List<SearchRoomPersonInfo>>>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                if(null != getMvpView()){
                    getMvpView().onRoomRecommend(false,e.getMessage(),null);
                }
            }

            @Override
            public void onResponse(ServiceResult<List<SearchRoomPersonInfo>> response) {
                if(null != getMvpView()){
                    if (null != response && response.isSuccess()) {
                        getMvpView().onRoomRecommend(true,response.getMessage(),response.getData());
                    }else{
                        getMvpView().onRoomRecommend(false,"",null);
                    }
                }
            }
        });
    }
}
