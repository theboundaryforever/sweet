package com.yuhuankj.tmxq.ui.find.bean;

import java.io.Serializable;

/**
 * 发现页model层
 */
public class FindHotRoomEnitity implements Serializable {
    private long uid;//: 93000766,
    private int count;//: 0,
    private String avatar;//: https://img.pinjin88.com/FnnrP8Ja2yP3ZAs75bNQNnII7SjD?imageslim,
    private String title;//: [落叶生拉拉let不不不多特也,
    private int  type;//
    private int onlineNum;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getOnlineNum() {
        return onlineNum;
    }

    public void setOnlineNum(int onlineNum) {
        this.onlineNum = onlineNum;
    }
}
