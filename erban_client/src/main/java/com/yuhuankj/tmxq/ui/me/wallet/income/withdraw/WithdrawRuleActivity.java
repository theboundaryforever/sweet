package com.yuhuankj.tmxq.ui.me.wallet.income.withdraw;

import android.os.Bundle;
import android.webkit.WebView;

import com.tongdaxing.xchat_core.UriProvider;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;

public class WithdrawRuleActivity extends BaseActivity {
    private WebView mWebView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw_rule);
        initTitleBar("提现规则");
        mWebView = (WebView) findViewById(R.id.wv_view);
        mWebView.loadUrl(UriProvider.IM_SERVER_URL+"/modules_tmxq/guide/withdraw.html");
    }
}
