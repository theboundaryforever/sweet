package com.yuhuankj.tmxq.ui.signAward.model;

import java.io.Serializable;

/**
 * 签到大礼包--推荐的麦上用户信息
 *
 * @author weihaitao
 * @date 2019/5/22
 */
public class RecommMicUserInfo implements Serializable {

    /**
     * 所在房间的房主UID
     */
    private long roomUid;

    /**
     * 当前用户ID
     */
    private long uid;

    /**
     * 用户所在房间魅力值
     */
    private long charm;

    /**
     * 用户性别
     */
    private int gender;

    /**
     * 用户所在房间的roomId
     */
    private long roomId;

    /**
     * 用户昵称
     */
    private String nick;

    /**
     * 用户头像
     */
    private String avatar;

    private int roomType;

    public int getRoomType() {
        return roomType;
    }

    public void setRoomType(int roomType) {
        this.roomType = roomType;
    }

    public long getRoomUid() {
        return roomUid;
    }

    public void setRoomUid(long roomUid) {
        this.roomUid = roomUid;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public long getCharm() {
        return charm;
    }

    public void setCharm(long charm) {
        this.charm = charm;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }
}
