package com.yuhuankj.tmxq.ui.liveroom.imroom.publicscreen.base;

import android.view.View;

import com.tongdaxing.erban.libcommon.widget.messageview.MsgHolderLayoutId;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMessage;
import com.yuhuankj.tmxq.R;

/**
 * 文件描述：房间消息多类型 - 默认的hodler类
 *
 * @auther：zwk
 * @data：2019/4/28
 */
@MsgHolderLayoutId(value = R.layout.item_room_msg_default_txt)
public class RoomMsgDefaultTextHolder extends RoomMsgBaseHolder<IMRoomMessage> {

    public RoomMsgDefaultTextHolder(View itemView) {
        super(itemView);
    }



    @Override
    public void onDataTransformView(RoomMsgBaseHolder holder, IMRoomMessage message, int position) {
        holder.setText(R.id.tv_room_base_txt_content, "不支持消息类型");
    }

    @Override
    protected int getItemClickLayoutId() {
        return 0;
    }

    @Override
    protected int getItemUserClickLayoutId() {
        return 0;
    }

}
