package com.yuhuankj.tmxq.ui.user.other;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.jph.takephoto.app.TakePhotoActivity;
import com.jph.takephoto.compress.CompressConfig;
import com.jph.takephoto.model.CropOptions;
import com.jph.takephoto.model.TResult;
import com.netease.nim.uikit.common.util.sys.NetworkUtil;
import com.netease.nim.uikit.common.util.sys.TimeUtil;
import com.sleepbot.datetimepicker.time.RadialPickerLayout;
import com.sleepbot.datetimepicker.time.TimePickerDialog;
import com.tongdaxing.erban.libcommon.audio.AudioPlayer;
import com.tongdaxing.erban.libcommon.audio.OnPlayListener;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.erban.libcommon.utils.TimeUtils;
import com.tongdaxing.erban.libcommon.utils.file.JXFileUtils;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.audio.AudioPlayAndRecordManager;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.file.IFileCore;
import com.tongdaxing.xchat_core.file.IFileCoreClient;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_core.user.bean.UserPhoto;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.base.permission.PermissionActivity;
import com.yuhuankj.tmxq.ui.audio.AudioRecordActivity;
import com.yuhuankj.tmxq.ui.login.ModifyInfoActivity;
import com.yuhuankj.tmxq.ui.login.ModifySexActivity;
import com.yuhuankj.tmxq.ui.widget.address.CityWheelDialog;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.utils.UIHelper;
import com.yuhuankj.tmxq.widget.TitleBar;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cn.bingoogolapple.baseadapter.BGAOnRVItemLongClickListener;
import cn.bingoogolapple.photopicker.widget.BGASortableNinePhotoLayout;

/**
 * @author zhouxiangfeng
 * @date 2017/5/23
 */

public class UserInfoModifyActivity extends TakePhotoActivity implements View.OnClickListener,
        TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener,
        BGASortableNinePhotoLayout.Delegate, IMvpBaseView {

    private static final String CAMERA_PREFIX = "picture_";
    private static final String BMP_TEMP_NAME = "bmp_temp_name";
    private String avatar;
    private ImageView civAvatar;
    private DatePickerDialog datePickerDialog;
    private TextView tvBirth;
    private TextView tvNick;
    private TextView tvSex;
    private TextView tvDesc;
    private UserInfo mUserInfo;
    private long userId;
    private TextView tvVoice;
    private String audioFileUrl;
    private AudioPlayAndRecordManager audioManager;
    private AudioPlayer audioPlayer;
    private UserInfoModifyActivity mActivity;

    private String birth;

    private TextView tvCity;
    private UserInfo userInfo;
    private ImageView imvSexRightArrow;
    //图片拖拽排序控件所需变量
    private BGASortableNinePhotoLayout bgasRvPhoto;
    private ArrayList<Long> userPhotoIdList = new ArrayList<>();
    private boolean isSortChange = false;//是否照片顺序有变动

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info_modify);
        mActivity = this;
        initTitleBar("编辑");
        mTitleBar.setCommonBackgroundColor(Color.WHITE);
        mTitleBar.setActionTextColor(Color.parseColor("#23CFB1"));
        mTitleBar.addAction(new TitleBar.TextAction("保存") {
            @Override
            public void performAction(View view) {
                getDialogManager().showProgressDialog(UserInfoModifyActivity.this, "请稍候...");
                if (isSortChange && userPhotoIdList != null && userPhotoIdList.size() > 0) {
                    CoreManager.getCore(IUserCore.class).sortPhoto(userPhotoIdList);
                } else {
                    if (userInfo != null) {
                        CoreManager.getCore(IUserCore.class).requestUpdateUserInfo(userInfo);
                    } else {
                        if (NetworkUtil.isNetAvailable(UserInfoModifyActivity.this)) {
                            SingleToastUtil.showToast("保存成功");
                            finish();
                        } else {
                            SingleToastUtil.showToast("网络连接失败");
                        }
                    }
                }
            }
        });
        findViews();
        init();
        onSetListener();

        userId = getIntent().getLongExtra("userId", 0);
        mUserInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(userId);
        if (mUserInfo != null) {
            initData(mUserInfo);
        }
    }

    private void findViews() {
        bgasRvPhoto = (BGASortableNinePhotoLayout) findViewById(R.id.bgasRvPhoto);
        civAvatar = (ImageView) findViewById(R.id.civ_avatar);
        tvBirth = (TextView) findViewById(R.id.tv_birth);
        tvNick = (TextView) findViewById(R.id.tvNick);
        tvSex = (TextView) findViewById(R.id.tv_sex);
        tvDesc = (TextView) findViewById(R.id.tv_desc);
        tvVoice = (TextView) findViewById(R.id.tv_voice);
        findViewById(R.id.layout_avatar).setOnClickListener(this);
        findViewById(R.id.layout_birth).setOnClickListener(this);
        findViewById(R.id.tvNick).setOnClickListener(this);
        findViewById(R.id.layout_desc).setOnClickListener(this);
        findViewById(R.id.layout_audiorecord).setOnClickListener(this);
        findViewById(R.id.llMainTop).setOnClickListener(this);

        //设置图片接口回调
        bgasRvPhoto.setDelegate(this);
        bgasRvPhoto.setNestedScrollingEnabled(false);

        //增加城市
        tvCity = (TextView) findViewById(R.id.tvCity);
        tvCity.setOnClickListener(this);
        imvSexRightArrow = (ImageView) findViewById(R.id.imvSexRightArrow);
    }

    private void init() {
        audioManager = AudioPlayAndRecordManager.getInstance();
        audioPlayer = audioManager.getAudioPlayer(UserInfoModifyActivity.this, null, mOnPlayListener);

        Calendar calendar = Calendar.getInstance();
        datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), true);
    }

    private void initData(UserInfo userInfo) {
        if (null != userInfo) {
            audioFileUrl = userInfo.getUserVoice();
            ImageLoadUtils.loadCircleImage(this, userInfo.getAvatar(), civAvatar, R.drawable.default_user_head);
            birth = TimeUtil.getDateTimeString(Long.valueOf(userInfo.getBirth()), "yyyy-MM-dd");
            tvBirth.setText(birth);
            tvNick.setText(userInfo.getNick());
            tvDesc.setText(userInfo.getUserDesc());
            if (userInfo.getGender() == 1) {
                tvSex.setText("男");
            } else {
                tvSex.setText("女");
            }
            //只能修改一次性别
            if (userInfo.getEditGenderCount() <= 0) {
                imvSexRightArrow.setVisibility(View.VISIBLE);
            } else {
                imvSexRightArrow.setVisibility(View.INVISIBLE);
            }
            tvSex.setOnClickListener(this);
            if (userInfo.getVoiceDura() > 0) {
                tvVoice.setVisibility(View.VISIBLE);
                tvVoice.setText(userInfo.getVoiceDura() + "'");
            } else {
                tvVoice.setVisibility(View.GONE);
            }
            tvCity.setText(userInfo.getCity());

            ArrayList<UserPhoto> images = new ArrayList<>(userInfo.getPrivatePhoto());
            bgasRvPhoto.setData(images, true);
            userPhotoIdList.clear();
            for (UserPhoto image : images) {
                userPhotoIdList.add(image.getPid());
            }

            bgasRvPhoto.getAdapter().setOnRVItemLongClickListener(new BGAOnRVItemLongClickListener() {
                @Override
                public boolean onRVItemLongClick(ViewGroup parent, View itemView, int position) {
                    if (bgasRvPhoto.getAdapter().getItem(position) != null) {
                        if (!bgasRvPhoto.isEditable()) {
                            bgasRvPhoto.setEditable(true);
                            return true;
                        }
                    }
                    return false;
                }
            });
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfo(UserInfo info) {
        if (info.getUid() == userId) {
            mUserInfo = info;
            initData(mUserInfo);
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo info) {
        if (info.getUid() == userId) {
            mUserInfo = info;
            initData(mUserInfo);
        }
        getDialogManager().dismissDialog();
    }

    private int playState = PlayState.NORMAL;


    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
        String monthstr;
        if ((month + 1) < 10) {
            monthstr = "0" + (month + 1);
        } else {
            monthstr = String.valueOf(month + 1);
        }
        String daystr;
        if ((day) < 10) {
            daystr = "0" + (day);
        } else {
            daystr = String.valueOf(day);
        }
        String birth = year + "-" + monthstr + "-" + daystr;
        if (userInfo == null) {
            userInfo = new UserInfo();
            userInfo.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        }
        userInfo.setBirthStr(birth);
        tvBirth.setText(birth);
        //  requestUpdateUserInfo(userInfo);
    }

    private void onSetListener() {
        tvVoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView tvVoice = (TextView) v;
                voiceClicked(tvVoice);
            }
        });

    }

    private void voiceClicked(TextView tvVoice) {
        if (playState == PlayState.NORMAL) {
            playState = PlayState.PLAYING;
            setTextViewLeftDrawable(tvVoice, R.drawable.icon_play_stop);
            if (!StringUtils.isEmpty(audioFileUrl)) {
                audioPlayer.setDataSource(audioFileUrl);
                audioManager.play();
            }

        } else if (playState == PlayState.PLAYING) {
            playState = PlayState.NORMAL;
            setTextViewLeftDrawable(tvVoice, R.drawable.icon_play);
            audioManager.stopPlay();

        }
    }

    @Override
    public void onClickAddNinePhotoItem(BGASortableNinePhotoLayout sortableNinePhotoLayout, View view, int position, ArrayList<UserPhoto> models) {
        uploadPicture(position);
    }

    @Override
    public void onClickDeleteNinePhotoItem(BGASortableNinePhotoLayout sortableNinePhotoLayout, View view, int position, UserPhoto model, ArrayList<UserPhoto> models) {
        getDialogManager().showOkCancelDialog("确定要删除这张照片吗?", true, new DialogManager.OkCancelDialogListener() {
            @Override
            public void onCancel() {
                getDialogManager().dismissDialog();
            }

            @Override
            public void onOk() {
                if (position > 0 && position <= bgasRvPhoto.getItemCount()) {
                    onPhotoDeleteClick(model);
                }

            }
        });
    }

    @Override
    public void onClickNinePhotoItem(BGASortableNinePhotoLayout sortableNinePhotoLayout, View view, int position, UserPhoto model, ArrayList<UserPhoto> models) {
        Intent intent = new Intent(mActivity, ShowPhotoActivity.class);
        intent.putExtra("position", position);
        intent.putExtra("userId", userId);
        startActivity(intent);
    }

    private void uploadPicture(int position) {
        if (mUserInfo.getPrivatePhoto() != null && mUserInfo.getPrivatePhoto().size() >= 50) {
            toast(R.string.album_has_out_size);
            return;
        }
        ButtonItem upItem = new ButtonItem(getResources().getString(R.string.photo_upload), this::checkPermissionAndStartCamera);
        ButtonItem loaclItem = new ButtonItem(getResources().getString(R.string.local_album), () -> {
            CompressConfig compressConfig = new CompressConfig.Builder().create();
            compressConfig.setMaxSize(500 * 1024);
            getTakePhoto().onEnableCompress(compressConfig, true);


            String mCameraCapturingName = CAMERA_PREFIX + System.currentTimeMillis() + ".jpg";
            File cameraOutFile = JXFileUtils.getTempFile(UserInfoModifyActivity.this, mCameraCapturingName);
            if (!cameraOutFile.getParentFile().exists()) {
                cameraOutFile.getParentFile().mkdirs();
            }
            Uri uri = Uri.fromFile(cameraOutFile);
            CropOptions options = new CropOptions.Builder().setWithOwnCrop(true).create();
            getTakePhoto().onPickFromGalleryWithCrop(uri, options);
        });
        List<ButtonItem> buttonItemList = new ArrayList<>();
        buttonItemList.add(upItem);
        buttonItemList.add(loaclItem);
        getDialogManager().showCommonPopupDialog(buttonItemList, getResources().getString(R.string.cancel), false);
    }

    public void onPhotoDeleteClick(UserPhoto userPhoto) {
        getDialogManager().showProgressDialog(this, getResources().getString(R.string.network_loading));
        CoreManager.getCore(IUserCore.class).requestDeletePhoto(userPhoto.getPid());
    }

    @Override
    public void onNinePhotoItemExchanged(BGASortableNinePhotoLayout sortableNinePhotoLayout, int fromPosition, int toPosition, ArrayList<UserPhoto> models) {
        //记录拖拽更改顺序后的相册列表
        userPhotoIdList.clear();
        //models就是拖拽移动之后的顺序
        if (null != models) {
            for (UserPhoto userPhoto : models) {
                userPhotoIdList.add(userPhoto.getPid());
            }
            isSortChange = true;
        }
    }

    private void setTextViewLeftDrawable(TextView tvVoice, int drawId) {
        Drawable drawablePlay = getResources().getDrawable(drawId);
        drawablePlay.setBounds(0, 0, drawablePlay.getMinimumWidth(), drawablePlay.getMinimumHeight());
        tvVoice.setCompoundDrawables(drawablePlay, null, null, null);
    }

    private void showByPlayState() {
        if (playState == PlayState.NORMAL) {
            setTextViewLeftDrawable(tvVoice, R.drawable.icon_play);
        } else if (playState == PlayState.PLAYING) {
            setTextViewLeftDrawable(tvVoice, R.drawable.icon_play_stop);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        if (requestCode == Method.NICK) {
            String stringExtra = data.getStringExtra(ModifyInfoActivity.CONTENTNICK);
            tvNick.setText(stringExtra);
            if (userInfo == null) {
                userInfo = new UserInfo();
                userInfo.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            }
            userInfo.setNick(stringExtra);
            //   CoreManager.getCore(IUserCore.class).requestUpdateUserInfo(userInfo);
        } else if (requestCode == Method.DESC) {
            String stringExtra = data.getStringExtra(ModifyInfoActivity.CONTENT);
            tvDesc.setText(stringExtra);
            if (userInfo == null) {
                userInfo = new UserInfo();
                userInfo.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            }
            userInfo.setUserDesc(stringExtra);
            //  CoreManager.getCore(IUserCore.class).requestUpdateUserInfo(userInfo);
        } else if (requestCode == Method.AUDIO) {
            audioFileUrl = data.getStringExtra(AudioRecordActivity.AUDIO_FILE);
            int audioDura = data.getIntExtra(AudioRecordActivity.AUDIO_DURA, 0);
            if (audioDura > 1) {
                tvVoice.setText(audioDura + "'");
            }
        } else if (requestCode == Method.SEX && data != null) {
            int sex = data.getIntExtra("sex", -1);
            if (sex != -1) {
                if (userInfo == null) {
                    userInfo = new UserInfo();
                    userInfo.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
                }
                userInfo.setGender(sex);
                tvSex.setText(sex == 1 ? "男" : "女");
            }
        }
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {

    }

    public interface Method {
        /**
         * 录音
         */
        int AUDIO = 2;
        /**
         * 昵称
         */
        int NICK = 3;
        /**
         * 个人介绍
         */
        int DESC = 4;
        /**
         * 性别
         */
        int SEX = 5;
    }


    PermissionActivity.CheckPermListener checkPermissionListener = new PermissionActivity.CheckPermListener() {
        @Override
        public void superPermission() {
            takePhoto();
        }
    };

    private void takePhoto() {
        String mCameraCapturingName = CAMERA_PREFIX + System.currentTimeMillis() + ".jpg";
        File cameraOutFile = JXFileUtils.getTempFile(UserInfoModifyActivity.this, mCameraCapturingName);
        if (!cameraOutFile.getParentFile().exists()) {
            cameraOutFile.getParentFile().mkdirs();
        }
        Uri uri = Uri.fromFile(cameraOutFile);
        CompressConfig compressConfig = new CompressConfig.Builder().create();
        getTakePhoto().onEnableCompress(compressConfig, false);
        CropOptions options = new CropOptions.Builder().setWithOwnCrop(true).create();
        getTakePhoto().onPickFromCaptureWithCrop(uri, options);
    }

    private void checkPermissionAndStartCamera() {
        //低版本授权检查
        checkPermission(checkPermissionListener, R.string.ask_camera, android.Manifest.permission.CAMERA);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_avatar:
                ButtonItem buttonItem = new ButtonItem("拍照上传", new ButtonItem.OnClickListener() {
                    @Override
                    public void onClick() {
                        checkPermissionAndStartCamera();
                    }
                });
                ButtonItem buttonItem1 = new ButtonItem("本地相册", new ButtonItem.OnClickListener() {
                    @Override
                    public void onClick() {
                        String mCameraCapturingName = CAMERA_PREFIX + System.currentTimeMillis() + ".jpg";
                        File cameraOutFile = JXFileUtils.getTempFile(UserInfoModifyActivity.this, mCameraCapturingName);
                        if (!cameraOutFile.getParentFile().exists()) {
                            cameraOutFile.getParentFile().mkdirs();
                        }
                        Uri uri = Uri.fromFile(cameraOutFile);
                        CompressConfig compressConfig = new CompressConfig.Builder().create();
                        getTakePhoto().onEnableCompress(compressConfig, true);
                        CropOptions options = new CropOptions.Builder().setWithOwnCrop(true).create();
                        getTakePhoto().onPickFromGalleryWithCrop(uri, options);

                    }
                });
                List<ButtonItem> buttonItems = new ArrayList<>();
                buttonItems.add(buttonItem);
                buttonItems.add(buttonItem1);
                getDialogManager().showCommonPopupDialog(buttonItems, "取消", false);

                isAvatar = true;
                break;
            case R.id.layout_birth:
                datePickerDialog.setVibrate(true);
                datePickerDialog.setYearRange(1945, 2017);
                if (mUserInfo != null) {
                    int year = TimeUtils.getYear(mUserInfo.getBirth());
                    int month = TimeUtils.getMonth(mUserInfo.getBirth());
                    int day = TimeUtils.getDayOfMonth(mUserInfo.getBirth());
                    datePickerDialog = DatePickerDialog.newInstance(this, year, (month - 1), day, true);
                }
                try {
                    datePickerDialog.show(getSupportFragmentManager(), "DATEPICKER_TAG_1");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.tvNick:
                UIHelper.showModifyInfoAct(UserInfoModifyActivity.this, Method.NICK, "昵称");
                break;
            case R.id.layout_desc:
                UIHelper.showModifyInfoAct(UserInfoModifyActivity.this, Method.DESC, "个性签名");
                break;
            case R.id.layout_audiorecord:
                checkPermission(new PermissionActivity.CheckPermListener() {
                                    @Override
                                    public void superPermission() {
                                        Intent intent = new Intent(UserInfoModifyActivity.this, AudioRecordActivity.class);
                                        UserInfoModifyActivity.this.startActivityForResult(intent, Method.AUDIO);
                                        isAvatar = false;
                                    }
                                }, R.string.ask_again,
                        Manifest.permission.RECORD_AUDIO);

                break;
            case R.id.layout_photos:
                UIHelper.showModifyPhotosAct(UserInfoModifyActivity.this, userId);
                break;
            case R.id.tvCity:
                String curCity = tvCity.getText().toString();
                CityWheelDialog cityWheelDialog = new CityWheelDialog(this, curCity, new CityWheelDialog.CallBack() {
                    @Override
                    public void callback(String area) {
                        tvCity.setText(area);
                        if (userInfo == null) {
                            userInfo = new UserInfo();
                            userInfo.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
                        }
                        userInfo.setCity(area);
                    }
                });
                cityWheelDialog.show();
                break;
            case R.id.tvSave:
                if (userInfo != null) {
                    getDialogManager().showProgressDialog(this, "请稍候...");
                    if (isSortChange && userPhotoIdList != null && userPhotoIdList.size() > 0) {
                        CoreManager.getCore(IUserCore.class).sortPhoto(userPhotoIdList);
                    } else {
                        CoreManager.getCore(IUserCore.class).requestUpdateUserInfo(userInfo);
                    }
                } else {
                    SingleToastUtil.showToast("保存成功");
                    finish();
                }
                break;
            case R.id.tv_sex:
                //只能修改一次性别
                if (imvSexRightArrow.isShown()) {
                    Intent intent = new Intent(this, ModifySexActivity.class);
                    intent.putExtra("sex", tvSex.getText().toString().equals("女") ? 2 : 1);
                    startActivityForResult(intent, Method.SEX);
                }
                break;
            case R.id.llMainTop:
                bgasRvPhoto.setEditable(false);
                break;
            default:
        }
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUpload(String url) {
        if (isAvatar) {
            avatar = url;
            if (userInfo == null) {
                userInfo = new UserInfo();
                userInfo.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            }
            userInfo.setAvatar(avatar);
            ImageLoadUtils.loadCircleImage(this, avatar, civAvatar, R.drawable.default_user_head);
            //CoreManager.getCore(IUserCore.class).requestUpdateUserInfo(userInfo);
            getDialogManager().dismissDialog();
        } else {
            CoreManager.getCore(IUserCore.class).requestAddPhoto(url);
        }
    }

    private boolean isAvatar = false;

    private interface PlayState {
        int PLAYING = 1;
        int NORMAL = 0;
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUploadPhoto(String url) {
        CoreManager.getCore(IUserCore.class).requestAddPhoto(url);
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUploadPhotoFail() {
        toast("操作失败，请检查网络");
        getDialogManager().dismissDialog();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestAddPhoto() {
        getDialogManager().dismissDialog();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestAddPhotoFaith(String msg) {
        if (!TextUtils.isEmpty(msg)) {
            toast(msg);
        }

        getDialogManager().dismissDialog();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestDeletePhoto(long pid) {
        try {
            userPhotoIdList.remove(pid);
            int position = -1;
            List<UserPhoto> photoBeans = bgasRvPhoto.getAdapter().getData();
            for (UserPhoto photo : photoBeans) {
                if (pid == photo.getPid()) {
                    position = photoBeans.indexOf(photo);
                    bgasRvPhoto.getAdapter().getData().remove(position);
                    bgasRvPhoto.getAdapter().notifyDataSetChanged();
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        getDialogManager().dismissDialog();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestDeletePhotoFaith(String msg) {
        toast(msg);
        getDialogManager().dismissDialog();
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUploadFail() {
        toast("上传照片失败");
        getDialogManager().dismissDialog();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfoUpdate(UserInfo userInfo) {
        SingleToastUtil.showToast("保存成功");
        finish();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfoUpdateError(String error) {
        getDialogManager().dismissDialog();
        if (!TextUtils.isEmpty(error)) {
            toast(error);
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onPhotoSortComplete(boolean isSuccess, String message) {
        //getDialogManager().dismissDialog();
        if (!isSuccess && !TextUtils.isEmpty(message)) {
            toast(message);
        }
        if (userInfo != null) {
            CoreManager.getCore(IUserCore.class).requestUpdateUserInfo(userInfo);
        } else {
            if (NetworkUtil.isNetAvailable(UserInfoModifyActivity.this)) {
                SingleToastUtil.showToast("保存成功");
                finish();
            } else {
                SingleToastUtil.showToast("网络连接失败");
            }
        }
    }

    @Override
    public void takeSuccess(TResult result) {
        getDialogManager().showProgressDialog(UserInfoModifyActivity.this, "请稍后");
        CoreManager.getCore(IFileCore.class).upload(new File(result.getImage().getCompressPath()));
    }

    @Override
    public void takeFail(TResult result, String msg) {
        toast(msg);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mOnPlayListener = null;
        audioManager.release();
    }

    private OnPlayListener mOnPlayListener = new OnPlayListener() {
        @Override
        public void onPrepared() {

        }

        @Override
        public void onCompletion() {
            playState = PlayState.NORMAL;
            showByPlayState();
        }

        @Override
        public void onInterrupt() {
            playState = PlayState.NORMAL;
            showByPlayState();
        }

        @Override
        public void onError(String s) {
            playState = PlayState.NORMAL;
            showByPlayState();
        }

        @Override
        public void onPlaying(long l) {

        }
    };
}
