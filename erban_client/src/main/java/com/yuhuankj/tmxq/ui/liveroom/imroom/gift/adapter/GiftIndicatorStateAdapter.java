package com.yuhuankj.tmxq.ui.liveroom.imroom.gift.adapter;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.yuhuankj.tmxq.ui.liveroom.imroom.gift.RoomGiftListFragment;

import java.util.List;

public class GiftIndicatorStateAdapter extends FragmentStatePagerAdapter {

    private List<RoomGiftListFragment> mTabs;

    public GiftIndicatorStateAdapter(FragmentManager fm, List<RoomGiftListFragment> tabs) {
        super(fm);
        mTabs = tabs;
    }
    @Override
    public int getCount() {
        return mTabs.size();
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE ;
    }

    @Override
    public Fragment getItem(int position) {
        return mTabs.get(position);
    }


    public void clear(){
        if (mTabs != null){
            mTabs.clear();
            mTabs = null;
        }
    }
}
