package com.yuhuankj.tmxq.ui.liveroom.nimroom.checkmicro;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.databinding.ActivityCheckMicroQueueBinding;

@CreatePresenter(CheckMicroQueuePresenter.class)
public class CheckMicroQueueActivity extends BaseMvpActivity<CheckMicroQueueView, CheckMicroQueuePresenter>
        implements CheckMicroQueueView, View.OnClickListener {

    private final String TAG = CheckMicroQueueActivity.class.getSimpleName();

    private ActivityCheckMicroQueueBinding checkMicroQueueBinding;

    private long roomUid;
    private int roomType;

    public static void start(Context context, long roomUid, int roomType) {
        Intent intent = new Intent(context, CheckMicroQueueActivity.class);
        intent.putExtra("roomUid",roomUid);
        intent.putExtra("roomType", roomType);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkMicroQueueBinding = DataBindingUtil.setContentView(this,R.layout.activity_check_micro_queue);
        checkMicroQueueBinding.setClick(this);
        initTitleBar(R.string.check_micro_queue_title, getResources().getColor(R.color.color_1A1A1A));
        roomUid = getIntent().getLongExtra("roomUid",0L);
        roomType = getIntent().getIntExtra("roomType", RoomInfo.ROOMTYPE_HOME_PARTY);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_checkMicroQueue:
                getDialogManager().showProgressDialog(this,getResources().getString(R.string.network_loading));
                getMvpPresenter().requestCheckMicroQueue(roomUid, roomType);
                break;
            default:
                break;
        }
    }


    @Override
    public void onRequestCheckMicroQueue(boolean isSuccess, String msg) {
        getDialogManager().dismissDialog();
        if(isSuccess){
            if(!TextUtils.isEmpty(msg)){
                getDialogManager().showOkCancelWithTitleDialog(getResources().getString(R.string.check_micro_queue_resp),
                        getResources().getString(R.string.check_micro_queue_wrong_tips,msg),
                        getResources().getString(R.string.yes),
                        getResources().getString(R.string.no),
                         new DialogManager.OkCancelDialogListener() {
                            @Override
                            public void onCancel() {

                            }

                            @Override
                            public void onOk() {
                                getDialogManager().showProgressDialog(CheckMicroQueueActivity.this,
                                        getResources().getString(R.string.network_loading));
                                getMvpPresenter().requestCleanMicroQueue(roomUid, roomType);
                            }
                        });
            }else{
                getDialogManager().showOkCancelWithTitleDialog(getResources().getString(R.string.check_micro_queue_resp),
                        getResources().getString(R.string.check_micro_queue_right_tips),
                        getResources().getString(R.string.sure),null,null);
            }
        }else{
            toast(msg);
        }
    }

    @Override
    public void onRequestCleanMicroQueue(boolean isSuccess, String msg) {
        getDialogManager().dismissDialog();
        toast(msg);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getDialogManager().dismissDialog();
    }
}
