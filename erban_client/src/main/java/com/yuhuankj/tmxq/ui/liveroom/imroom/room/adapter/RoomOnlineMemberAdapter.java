package com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMember;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomOnlineMember;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

/**
 * <p> 房间在线人数列表 （上麦，房主，游客，管理员）  </p>
 *
 * @author weihaitao
 * @date 2019年4月29日 16:00:47
 */
public class RoomOnlineMemberAdapter extends BaseQuickAdapter<IMRoomOnlineMember, BaseViewHolder> {

    private int avatarImgWidthAndHeight = 0;

    public RoomOnlineMemberAdapter() {
        super(R.layout.list_item_online_user);
        avatarImgWidthAndHeight = DisplayUtility.dp2px(BasicConfig.INSTANCE.getAppContext(), 35);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, IMRoomOnlineMember onlineChatMember) {

        if (onlineChatMember != null && onlineChatMember.imRoomMember != null) {
            final ImageView sexImage = baseViewHolder.getView(R.id.sex);
            if (onlineChatMember.imRoomMember.getGender() == 1) {
                sexImage.setVisibility(View.VISIBLE);
                sexImage.setImageResource(R.drawable.icon_home_page_male);
            } else if (onlineChatMember.imRoomMember.getGender() == 2) {
                sexImage.setVisibility(View.VISIBLE);
                sexImage.setImageResource(R.drawable.icon_home_page_female);
            } else {
                sexImage.setVisibility(View.GONE);
            }

            ImageView roomOwnnerTag = baseViewHolder.getView(R.id.room_owner_logo);
            ImageView managerLogo = baseViewHolder.getView(R.id.manager_logo);
            if (onlineChatMember.isOnMic) {
                baseViewHolder.setVisible(R.id.manager_logo, onlineChatMember.isAdmin || onlineChatMember.isRoomOwer);
                managerLogo.setImageResource(onlineChatMember.isAdmin ? R.drawable.icon_admin_logo
                        : R.drawable.icon_user_list_room_owner);
                if (!onlineChatMember.isRoomOwer) {
                    roomOwnnerTag.setImageResource(R.drawable.icon_user_list_up_mic);
                    roomOwnnerTag.setVisibility(View.VISIBLE);
                } else {
                    roomOwnnerTag.setVisibility(View.GONE);
                }
            } else {
                baseViewHolder.setVisible(R.id.manager_logo, false);
                roomOwnnerTag.setVisibility((onlineChatMember.isAdmin || onlineChatMember.isRoomOwer)
                        ? View.VISIBLE : View.GONE);
                roomOwnnerTag.setImageResource(onlineChatMember.isAdmin ? R.drawable.icon_admin_logo
                        : R.drawable.icon_user_list_room_owner);
            }

            View container = baseViewHolder.getView(R.id.container);
            TextView nick = baseViewHolder.getView(R.id.nick);
            ImageView avatar = baseViewHolder.getView(R.id.avatar);

            IMRoomMember chatRoomMember = onlineChatMember.imRoomMember;
            String nickStr = null;
            if (null != chatRoomMember) {
                nickStr = chatRoomMember.getNick();

            }
            nick.setText(nickStr);
            nick.setTextColor(ContextCompat.getColor(mContext, R.color.color_1A1A1A));
//                container.setBackgroundColor(position % 2 == 0 ? Color.parseColor("#05FFFFFF") : 0);
            ImageLoadUtils.loadAvatar(mContext, ImageLoadUtils.toThumbnailUrl(
                    avatarImgWidthAndHeight, avatarImgWidthAndHeight,
                    onlineChatMember.imRoomMember.getAvatar()), avatar);
        }
    }
}
