package com.yuhuankj.tmxq.ui.me.wallet.bills;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.ui.me.wallet.bills.widget.BillItemView;

/**
 * @author Administrator
 */
public class TotalBillsActivity extends BaseActivity implements View.OnClickListener {
    private BillItemView mBillGiftInCome, mBillGiftExpend;
    private BillItemView mBillChat;
    private BillItemView mBillCharge;
    private BillItemView mBillWithdraw;
    private BillItemView mBillRed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_total_bills);
        initTitleBar(getString(R.string.bill_title));
        initView();
        initData();
        setListener();
    }

    private void initView() {
        mBillGiftInCome = (BillItemView) findViewById(R.id.bill_item_income);
        mBillGiftExpend = (BillItemView) findViewById(R.id.bill_item_expend);
        mBillChat = (BillItemView) findViewById(R.id.bill_item_chat);
        mBillCharge = (BillItemView) findViewById(R.id.bill_item_charge);
        mBillWithdraw = (BillItemView) findViewById(R.id.bill_item_withdraw);
        mBillRed = (BillItemView) findViewById(R.id.bill_item_red);
    }

    private void initData() {

    }


    private void setListener() {
        mBillGiftInCome.setOnClickListener(this);
        mBillGiftExpend.setOnClickListener(this);
        mBillChat.setOnClickListener(this);
        mBillCharge.setOnClickListener(this);
        mBillWithdraw.setOnClickListener(this);
        mBillRed.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bill_item_income:
                startActivity(new Intent(this, BillGiftInComeActivity.class));
                break;
            case R.id.bill_item_expend:
                startActivity(new Intent(this, BillGiftExpendActivity.class));
                break;
            case R.id.bill_item_chat:
                startActivity(new Intent(this, ChatBillsActivity.class));
                break;
            case R.id.bill_item_charge:
                startActivity(new Intent(this, ChargeBillsActivity.class));
                break;
            case R.id.bill_item_withdraw:
                startActivity(new Intent(this, WithdrawBillsActivity.class));
                break;
            case R.id.bill_item_red:
                startActivity(new Intent(this, RedBagBillsActivity.class));
                break;
            default:
        }
    }
}
