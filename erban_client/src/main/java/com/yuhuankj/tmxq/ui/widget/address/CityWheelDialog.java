package com.yuhuankj.tmxq.ui.widget.address;

import android.content.Context;
import android.support.v7.app.AppCompatDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.aigestudio.wheelpicker.WheelPicker;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.yuhuankj.tmxq.R;

/**
 * @author liaoxy
 * @Description:地区选择弹窗
 * @date 2019/1/25 17:02
 */
public class CityWheelDialog extends AppCompatDialog implements View.OnClickListener {

    private final String TAG = CityWheelDialog.class.getSimpleName();
    private Context context;
    private WheelCityPicker wpArea;
    private TextView tvCancel, tvEnter;
    private TextView tvCurCity;
    private String curCity = "";

    public CityWheelDialog(Context context, String curCity, CallBack callBack) {
        super(context, R.style.dialog);
        this.context = context;
        this.callBack = callBack;
        this.curCity = curCity;
        init();
    }

    private void init() {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_city_wheel, null, false);
        setContentView(view);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = ScreenUtil.getScreenWidth(context);
        params.height = (int) (ScreenUtil.getScreenHeight(context) * 0.53);
        params.gravity = Gravity.BOTTOM;
        getWindow().setAttributes(params);
        setCanceledOnTouchOutside(true);

        wpArea = view.findViewById(R.id.wpArea);
        wpArea.hideArea();
        tvCancel = view.findViewById(R.id.tvCancel);
        tvEnter = view.findViewById(R.id.tvEnter);
        tvCurCity = view.findViewById(R.id.tvCurCity);
        tvCancel.setOnClickListener(this);
        tvEnter.setOnClickListener(this);

        wpArea.getmWPProvince().setOnWheelChangeListener(new WheelPicker.OnWheelChangeListener() {
            @Override
            public void onWheelScrolled(int i) {
            }

            @Override
            public void onWheelSelected(int i) {
                tvCurCity.setText(wpArea.getCity());
            }

            @Override
            public void onWheelScrollStateChanged(int i) {

            }
        });
        wpArea.getmWPCity().setOnWheelChangeListener(new WheelPicker.OnWheelChangeListener() {
            @Override
            public void onWheelScrolled(int i) {
            }

            @Override
            public void onWheelSelected(int i) {
                tvCurCity.setText(wpArea.getCity());
            }

            @Override
            public void onWheelScrollStateChanged(int i) {

            }
        });
        wpArea.setCurCity(curCity);
        tvCurCity.setText(wpArea.getCity());
    }

    @Override
    public void onClick(View view) {
        if (view == tvCancel) {
            dismiss();
        } else if (view == tvEnter) {
            String areaStr = wpArea.getCity();
            try {
                //对重庆特殊处理，重庆下面有县，自治区等，如果只取市，则无法识别具体地区
                //这里全部返回重庆市
                if (wpArea.getProvince().equals("重庆")) {
                    areaStr = "重庆";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (callBack != null) {
                callBack.callback(areaStr);
            }
            dismiss();
        }
    }

    private CallBack callBack;

    public interface CallBack {
        void callback(String area);
    }
}
