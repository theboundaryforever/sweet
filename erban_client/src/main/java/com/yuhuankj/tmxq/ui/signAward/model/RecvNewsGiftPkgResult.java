package com.yuhuankj.tmxq.ui.signAward.model;

import java.io.Serializable;

/**
 * @author weihaitao
 * @date 2019/5/23
 */
public class RecvNewsGiftPkgResult implements Serializable {

    /**
     * 即将跳转的房主UID
     */
    private long roomUid;

    /**
     * 即将跳转的房间类型
     */
    private int roomType;

    /**
     * 领取人的UID
     */
    private long uid;

    /**
     * 即将跳转的房间ID
     */
    private long roomId;

    /**
     * 选取的麦上用户UID
     */
    private long micUid;

    /**
     * 提示信息,如果是有选择用户的流程，那么对应的是用户的昵称，否则为提示信息
     */
    private String message;

    public long getRoomUid() {
        return roomUid;
    }

    public void setRoomUid(long roomUid) {
        this.roomUid = roomUid;
    }

    public int getRoomType() {
        return roomType;
    }

    public void setRoomType(int roomType) {
        this.roomType = roomType;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public long getMicUid() {
        return micUid;
    }

    public void setMicUid(long micUid) {
        this.micUid = micUid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
