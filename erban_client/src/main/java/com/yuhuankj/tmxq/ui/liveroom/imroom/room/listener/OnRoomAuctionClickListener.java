package com.yuhuankj.tmxq.ui.liveroom.imroom.room.listener;

import com.tongdaxing.xchat_core.room.auction.AuctionUserBean;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/7/29
 */
public interface OnRoomAuctionClickListener {
    void onAucitonUserClick(AuctionUserBean auctionUserBean);
}
