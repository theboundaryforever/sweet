package com.yuhuankj.tmxq.ui.user.other;

public class UserPropInfo {

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPropName() {
        return propName;
    }

    public void setPropName(String propName) {
        this.propName = propName;
    }

    private String url;
    private String propName;

    @Override
    public String toString() {
        return "UserPropInfo{" +
                "url='" + url + '\'' +
                ", propName='" + propName + '\'' +
                '}';
    }

    public UserPropInfo(String url, String propName) {
        this.url = url;
        this.propName = propName;
    }
}
