package com.yuhuankj.tmxq.ui.room.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;

import com.tongdaxing.xchat_core.home.TabInfo;
import com.yuhuankj.tmxq.ui.room.fragment.FollowFragment;
import com.yuhuankj.tmxq.ui.room.fragment.HotProFragment;
import com.yuhuankj.tmxq.ui.room.fragment.NotHotFragment;

import java.util.List;

/**
 * <p> 主页adapter  热门等 </p>
 * Created by Administrator on 2017/11/15.
 */
public class MainAdapter extends FragmentPagerAdapter {
    private SparseArray<Fragment> mFragmentList;
    private List<TabInfo> mTitleList;

    public MainAdapter(FragmentManager fm, List<TabInfo> titleList) {
        super(fm);
        this.mTitleList = titleList;
        mFragmentList = new SparseArray<>();
        mFragmentList.put(0, new FollowFragment());
        mFragmentList.put(1, HotProFragment.newInstance());

        int size = titleList.size();
        for (int i = 2; i < size; i++) {
            NotHotFragment value = NotHotFragment.newInstance(titleList.get(i));
            //隐藏底部导航栏
            value.setShowTopMenu(false);
            mFragmentList.put(i, value);
        }
    }

    @Override
    public Fragment getItem(int position) {
       /* int size = mFragmentList.size();
        BaseFragment fragment = null;
        if (position < size && ((fragment = mFragmentList.get(position)) != null)) {
            return fragment;
        }

        if (position == 0) {
            if (fragment == null) {
                fragment = new HotFragment();
                mFragmentList.put(0, fragment);
            }
        } else { //其他界面
            if (size > 1) {
                int count = getCount();
                for (int i = 1; i < count; i++) {

                }
            }
        }*/

        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mTitleList == null ? 0 : mTitleList.size();
    }
}