package com.yuhuankj.tmxq.ui.chancemeeting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.netease.nim.uikit.glide.GlideApp;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tencent.bugly.crashreport.BuglyLog;
import com.tongdaxing.erban.libcommon.audio.AudioPlayer;
import com.tongdaxing.erban.libcommon.audio.OnPlayListener;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.audio.AudioPlayAndRecordManager;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_core.utils.StarUtils;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.widget.RoundImageView;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

/**
 * 我的声音界面
 *
 * @author weihaitao
 * @date 2019/1/4
 */
public class MyVoiceActivity extends BaseMvpActivity
        implements IMvpBaseView, View.OnClickListener {

    public static final String TAG = MyVoiceActivity.class.getSimpleName();

    private View llChanceMeeting;
    private RoundImageView rivMeetingHead;
    private ImageView ivSex;
    private View llSex;
    private TextView tvConst;
    private TextView tvNick;
    private TextView tvAge;
    private TextView tvVoice;
    private TextView tvVoiceAuditStatus;
    private ImageView ivPlayStatus;
    private SVGAImageView svgaivBgAnim;
    private SVGAParser bgSvgaAnimParser;

    private boolean bgAnimLoaded = false;

    private UserInfo userInfo;
    private int imgWidthAndHeight = 0;

    private AudioPlayAndRecordManager audioManager;
    private AudioPlayer audioPlayer;
    private boolean isAudioPlaying = false;
    OnPlayListener onPlayListener = new OnPlayListener() {

        @Override
        public void onPrepared() {
            LogUtils.d(TAG, "onPrepared");
            isAudioPlaying = true;
            ivPlayStatus.setImageDrawable(getResources().getDrawable(
                    R.mipmap.icon_chance_meeting_audio_pause));
        }

        @Override
        public void onCompletion() {
            LogUtils.d(TAG, "onCompletion");
            isAudioPlaying = false;
            ivPlayStatus.setImageDrawable(getResources().getDrawable(
                    R.mipmap.icon_chance_meeting_audio_play));
        }


        @Override
        public void onInterrupt() {
            LogUtils.d(TAG, "onInterrupt");
            isAudioPlaying = false;
            ivPlayStatus.setImageDrawable(getResources().getDrawable(
                    R.mipmap.icon_chance_meeting_audio_play));
        }

        @Override
        public void onError(String s) {
            LogUtils.d(TAG, "onError :" + s);
            isAudioPlaying = false;
            ivPlayStatus.setImageDrawable(getResources().getDrawable(
                    R.mipmap.icon_chance_meeting_audio_play));

        }

        @Override
        public void onPlaying(long l) {
            LogUtils.d(TAG, "onPlaying-l:" + l);
            isAudioPlaying = true;
        }
    };

    public static void start(Context context) {
        context.startActivity(new Intent(context, MyVoiceActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtils.d(TAG, "onCreate");
        setContentView(R.layout.activity_my_voice);
        onFindViews();
        onSetListener();
        userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        refreshVoiceCardView();
    }

    private void onFindViews() {
        LogUtils.d(TAG, "onFindViews");
        llChanceMeeting = findViewById(R.id.ll_chanceMeeting);
        int screenWidth = DisplayUtility.getScreenWidth(this);
        ViewGroup.LayoutParams lp = llChanceMeeting.getLayoutParams();
        lp.width = screenWidth * 3 / 4;
        lp.height = lp.width * 400 / 282;
        llChanceMeeting.setLayoutParams(lp);

        rivMeetingHead = (RoundImageView) findViewById(R.id.rivMeetingHead);
        ViewGroup.LayoutParams lp1 = rivMeetingHead.getLayoutParams();
        lp1.width = lp.width;
        lp1.height = lp1.width;
        rivMeetingHead.setLayoutParams(lp1);

        imgWidthAndHeight = lp1.width;

        findViewById(R.id.tv_report).setVisibility(View.GONE);
        findViewById(R.id.tvFindTA).setVisibility(View.GONE);

        ivSex = (ImageView) findViewById(R.id.iv_sex);
        llSex = findViewById(R.id.ll_sex);
        tvConst = (TextView) findViewById(R.id.tv_const);
        tvNick = (TextView) findViewById(R.id.tv_nick);
        tvAge = (TextView) findViewById(R.id.tv_age);
        ivPlayStatus = (ImageView) findViewById(R.id.iv_playStatus);
        tvVoiceAuditStatus = (TextView) findViewById(R.id.tvVoiceAuditStatus);
        tvVoice = (TextView) findViewById(R.id.tvVoice);

        svgaivBgAnim = (SVGAImageView) findViewById(R.id.svgaiv_bgAnim);
        loadBgSvgaAnim();
    }

    private void loadBgSvgaAnim() {
        bgSvgaAnimParser = new SVGAParser(this);
        BuglyLog.d(TAG, "loadBgSvgaAnim-svga anim, url:" + UriProvider.getChanceMeetingBgAnimUrl());
        try {
            bgSvgaAnimParser.parse(new URL(UriProvider.getChanceMeetingBgAnimUrl()), new SVGAParser.ParseCompletion() {
                @Override
                public void onComplete(SVGAVideoEntity svgaVideoEntity) {
                    LogUtils.d(TAG, "loadBgSvgaAnim-parse-onComplete1");
                    bgAnimLoaded = true;
                    svgaivBgAnim.setVisibility(View.VISIBLE);
                    svgaivBgAnim.setImageDrawable(new SVGADrawable(svgaVideoEntity));
                    svgaivBgAnim.startAnimation();
                }

                @Override
                public void onError() {

                }
            });
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private void onSetListener() {
        LogUtils.d(TAG, "onSetListener");
        ivPlayStatus.setOnClickListener(this);
        findViewById(R.id.iv_back).setOnClickListener(this);
        findViewById(R.id.tvEditMyVoice).setOnClickListener(this);
        llChanceMeeting.setOnClickListener(this);
    }

    private void refreshVoiceCardView() {
        if (null != userInfo) {
            //加载房间背景图
            if (!TextUtils.isEmpty(userInfo.getBackground())) {
                GlideApp.with(this)
                        .load(ImageLoadUtils.toThumbnailUrl(imgWidthAndHeight, imgWidthAndHeight, userInfo.getBackground()))
                        .placeholder(R.mipmap.icon_voice_bg_default)
                        .into(rivMeetingHead);
            } else {
                //没有上传显示默认图
                rivMeetingHead.setImageDrawable(getResources().getDrawable(R.mipmap.icon_voice_bg_default));
            }

            ivSex.setImageDrawable(ivSex.getContext().getResources().getDrawable(
                    userInfo.getGender() == 1 ? R.mipmap.icon_sex_man : R.mipmap.icon_sex_woman));
            llSex.setBackground(llSex.getContext().getResources().getDrawable(
                    userInfo.getGender() == 1 ? R.drawable.bg_chance_meeting_card_info_male
                            : R.drawable.bg_chance_meeting_card_info_woman));

            String star = StarUtils.getConstellation(new Date(userInfo.getBirth()));
            if (null == star) {
                tvConst.setVisibility(View.GONE);
            } else {
                tvConst.setText(star);
                tvConst.setVisibility(View.VISIBLE);
            }

            ivPlayStatus.setImageDrawable(getResources().getDrawable(R.mipmap.icon_chance_meeting_audio_play));
            ivPlayStatus.setVisibility(!TextUtils.isEmpty(userInfo.getUserVoice()) && userInfo.getVoiceDura() > 0 ? View.VISIBLE : View.GONE);
            tvVoice.setVisibility(!TextUtils.isEmpty(userInfo.getTimbre()) ? View.VISIBLE : View.GONE);
            tvVoice.setText(userInfo.getTimbre());
            String nick = userInfo.getNick();
            if (!TextUtils.isEmpty(nick) && nick.length() > 8) {
                nick = nick.substring(0, 8);
                tvNick.setText(getResources().getString(R.string.nick_length_max_six, nick));
            } else {
                tvNick.setText(nick);
            }
            int age = StarUtils.getAge(new Date(userInfo.getBirth()));
            tvAge.setText(String.valueOf(age));

            //-1未上传  0待审核 1审核通过  2审核不通过
            if (userInfo.getBackgroundStatus() == 0) {
                tvVoiceAuditStatus.setVisibility(View.VISIBLE);
                tvVoiceAuditStatus.setText(getResources().getString(R.string.edit_voice_audit_status_1));
            } else if (userInfo.getBackgroundStatus() == 2) {
                tvVoiceAuditStatus.setVisibility(View.VISIBLE);
                tvVoiceAuditStatus.setText(getResources().getString(R.string.edit_voice_audit_status_2));
            } else {
                tvVoiceAuditStatus.setVisibility(View.GONE);
            }
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfo(UserInfo info) {
        LogUtils.d(TAG, "onRequestUserInfo-info:" + info);
        if (null != userInfo && info.getUid() == userInfo.getUid()) {
            userInfo = info;
            refreshVoiceCardView();
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo info) {
        LogUtils.d(TAG, "onCurrentUserInfoUpdate-info:" + info);
        if (null != userInfo && info.getUid() == userInfo.getUid()) {
            userInfo = info;
            refreshVoiceCardView();
        }
    }

    private void audioRelease() {
        if (audioManager != null) {
            //audioManager.release();会停止player播放器并设置listener为null
            audioManager.release();
            if (audioPlayer != null) {
                audioPlayer = null;
            }
            audioManager = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (null != svgaivBgAnim && bgAnimLoaded) {
            svgaivBgAnim.stopAnimation(true);
            bgAnimLoaded = false;
        }
        audioRelease();
        LogUtils.d(TAG, "onDestroy");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.ll_chanceMeeting:
                if (null != userInfo) {
                    UserInfoActivity.start(this, userInfo.getUid());
                }
                break;
            case R.id.tvEditMyVoice:
                EditVoiceActivity.start(this);
                break;
            case R.id.iv_playStatus:
                playOrPauseAudio();
                break;
            default:
                break;
        }
    }

    private void playOrPauseAudio() {
        if (null == audioManager) {
            audioManager = AudioPlayAndRecordManager.getInstance();
        }

        if (null == audioPlayer) {
            audioPlayer = audioManager.getAudioPlayer(this, null, onPlayListener);
        }

        //如果音频文件不为空且不在播放，那么点击播放按钮控件就是播放操作
        if (null != userInfo && !audioPlayer.isPlaying() && !TextUtils.isEmpty(userInfo.getUserVoice())) {
            LogUtils.d(TAG, "playOrPauseAudio-play audioFileUrl: " + userInfo.getUserVoice());
            audioPlayer.setDataSource(userInfo.getUserVoice());
            audioManager.play();
            //体验上按钮会响应的快一点
            isAudioPlaying = true;
            ivPlayStatus.setImageDrawable(getResources().getDrawable(
                    R.mipmap.icon_chance_meeting_audio_pause));
        } else {
            //反之，为空，或者正在播放，那么停止播放
            if (audioPlayer.isPlaying()) {
                audioManager.stopPlay();
            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        final UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        boolean result = null != userInfo && TextUtils.isEmpty(userInfo.getUserVoice()) && userInfo.getVoiceDura() > 0;
        LogUtils.d(TAG, "onResume result:" + result);
        if (null != svgaivBgAnim && bgAnimLoaded) {
            svgaivBgAnim.startAnimation();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        LogUtils.d(TAG, "onPause");
        if (null != svgaivBgAnim && bgAnimLoaded) {
            svgaivBgAnim.stopAnimation(false);
        }
        if (audioManager != null) {
            //audioManager.release();会停止player播放器并设置listener为null
            audioManager.release();
        }
    }

    @Override
    public boolean isStatusBarDarkFont() {
        return false;
    }
}
