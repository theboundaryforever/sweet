package com.yuhuankj.tmxq.ui.message.black;

import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCore;

import java.util.List;


public class BlackListPresenter extends AbstractMvpPresenter<BlackListView> {

    private final String TAG = BlackListPresenter.class.getSimpleName();


    public BlackListPresenter() {
    }


    public void getBlackList(int pageNum, int pageSize) {
        LogUtils.d(TAG, "getBlackList-pageNum:" + pageNum + " pageSize:" + pageSize);
        List<NimUserInfo> userInfos = CoreManager.getCore(IIMFriendCore.class).getMyBlackList();
        if (null != getMvpView()) {
            if (!ListUtils.isListEmpty(userInfos)) {
                getMvpView().showBlackList(userInfos);
            } else {
                getMvpView().showBlackListEmptyView();
            }
        }
    }

}
