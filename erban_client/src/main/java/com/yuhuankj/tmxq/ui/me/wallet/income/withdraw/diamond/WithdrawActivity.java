package com.yuhuankj.tmxq.ui.me.wallet.income.withdraw.diamond;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.auth.RealNameAuthStatusChecker;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_core.withdraw.IWithdrawCore;
import com.tongdaxing.xchat_core.withdraw.IWithdrawCoreClient;
import com.tongdaxing.xchat_core.withdraw.bean.ExchangerInfo;
import com.tongdaxing.xchat_core.withdraw.bean.WithdrawInfo;
import com.tongdaxing.xchat_core.withdraw.bean.WithdrwaListInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.ui.login.GetSmsCodeToBindThirdPlatformActivity;
import com.yuhuankj.tmxq.ui.me.wallet.income.withdraw.BinderAlipayActivity;
import com.yuhuankj.tmxq.ui.me.wallet.income.withdraw.WithdrawRuleActivity;
import com.yuhuankj.tmxq.ui.user.password.PaypwdDialog;
import com.yuhuankj.tmxq.ui.widget.ChooseThirdPlatformDialog;
import com.yuhuankj.tmxq.widget.TitleBar;

import java.util.List;

/**
 * 钻石提现
 */
public class WithdrawActivity extends BaseActivity implements ChooseThirdPlatformDialog.OnThirdPlatformChooseListener {

    private final String TAG = WithdrawActivity.class.getSimpleName();

    private TitleBar mTitleBar;
    private TextView tv_platformAccount;
    private TextView tv_platformNick;
    private TextView tv_unBindingTips;
    private ImageView iv_bindPlatform;
    private TextView diamondNumWithdraw;
    private RecyclerView mRecyclerView;
    private Button btnWithdraw;
    private WithdrawJewelAdapter mJewelAdapter;
    public WithdrwaListInfo checkedPosition;
    private WithdrawInfo withdrawInfos = new WithdrawInfo();
    private UserInfo userInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw);
        initTitleBar(getString(R.string.withdraw));
        initView();
        setListener();
        initData();
    }

    private void initData() {
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        mJewelAdapter = new WithdrawJewelAdapter();
        mJewelAdapter.setOnItemClickListener((baseQuickAdapter, view, position) -> {
            List<WithdrwaListInfo> list = mJewelAdapter.getData();
            if (ListUtils.isListEmpty(list)) return;
            int size = list.size();
            for (int i = 0; i < size; i++) {
                list.get(i).isSelected = position == i;
            }
            mJewelAdapter.notifyDataSetChanged();
            checkedPosition = list.get(position);
            if (isWithdraw()) {
                btnWithdraw.setOnClickListener(v -> {
                    if (withdraw_third_platform_type == 0) {
                        SingleToastUtil.showToast("请选择提现平台");
                        return;
                    }
                    if (userInfo == null) {
                        SingleToastUtil.showToast("用户信息为空");
                        return;
                    }
                    if (checkedPosition == null) {
                        SingleToastUtil.showToast("选择的提现信息为空");
                        return;
                    }
                    if (!userInfo.isPayPassword()) {
                        //没有设置提现密码按以前的逻辑走
                        getDialogManager().showProgressDialog(WithdrawActivity.this, getResources().getString(R.string.loading));
                        CoreManager.getCore(IWithdrawCore.class).requestExchange(
                                CoreManager.getCore(IAuthCore.class).getCurrentUid(),
                                checkedPosition.cashProdId, withdraw_third_platform_type);
                    } else {
                        PaypwdDialog paypwdDialog = new PaypwdDialog(WithdrawActivity.this, "确认提现", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                getDialogManager().showProgressDialog(WithdrawActivity.this, getResources().getString(R.string.loading));
                                CoreManager.getCore(IWithdrawCore.class).requestExchange(
                                        CoreManager.getCore(IAuthCore.class).getCurrentUid(),
                                        checkedPosition.cashProdId, withdraw_third_platform_type);
                            }
                        });
                        paypwdDialog.show();
                    }
                });
            }
        });
        mRecyclerView.setAdapter(mJewelAdapter);
        getWithdrawUserInfo();
        loadRecyclerViewData();
    }

    private void getWithdrawUserInfo() {
        getDialogManager().showProgressDialog(this, getResources().getString(R.string.loading));
        CoreManager.getCore(IWithdrawCore.class).getWithdrawUserInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }

    private int withdraw_third_platform_type = 0;//1--微信,2--支付宝

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onGetWithdrawUserInfo(WithdrawInfo withdrawInfo) {
        getDialogManager().dismissDialog();
        if (withdrawInfo != null) {
            withdrawInfos = withdrawInfo;
            if (withdrawInfos.withDrawType == 0) {
                if (withdraw_third_platform_type == 2 && !TextUtils.isEmpty(withdrawInfo.alipayAccount) && !withdrawInfo.alipayAccount.equals("null")) {
                    tv_platformAccount.setVisibility(View.VISIBLE);
                    tv_platformAccount.setText(getResources().getString(R.string.third_platform_binding_alipay_account, withdrawInfo.alipayAccount));
                    tv_platformNick.setText(getResources().getString(R.string.third_platform_binding_alipay_name, withdrawInfo.alipayAccountName));
                    tv_platformNick.setVisibility(View.VISIBLE);
                    tv_unBindingTips.setVisibility(View.GONE);
                    iv_bindPlatform.setVisibility(View.VISIBLE);
                    iv_bindPlatform.setBackgroundDrawable(getResources().getDrawable(R.drawable.zhifubao));
                } else {
                    iv_bindPlatform.setVisibility(View.GONE);
                    tv_platformNick.setVisibility(View.GONE);
                    tv_platformAccount.setVisibility(View.GONE);
                    tv_unBindingTips.setVisibility(View.VISIBLE);
                    tv_unBindingTips.setText(getResources().getString(R.string.my_income_withdraw_bind_tips));
                }
            } else if (withdrawInfo.withDrawType == 2) {
                if (!TextUtils.isEmpty(withdrawInfo.alipayAccount) && !withdrawInfo.alipayAccount.equals("null")) {
                    tv_platformAccount.setVisibility(View.VISIBLE);
                    tv_platformAccount.setText(getResources().getString(R.string.third_platform_binding_alipay_account, withdrawInfo.alipayAccount));
                    tv_platformNick.setText(getResources().getString(R.string.third_platform_binding_alipay_name, withdrawInfo.alipayAccountName));
                    tv_platformNick.setVisibility(View.VISIBLE);
                    tv_unBindingTips.setVisibility(View.GONE);
                    iv_bindPlatform.setVisibility(View.VISIBLE);
                    withdraw_third_platform_type = 2;
                    iv_bindPlatform.setBackgroundDrawable(getResources().getDrawable(R.drawable.zhifubao));
                } else {
                    iv_bindPlatform.setVisibility(View.GONE);
//                    iv_bindPlatform.setBackground(getResources().getDrawable(R.drawable.zhifubao_un));
                    tv_platformNick.setVisibility(View.GONE);
                    tv_platformAccount.setVisibility(View.GONE);
                    tv_unBindingTips.setVisibility(View.VISIBLE);
                    withdraw_third_platform_type = 2;
                }
            } else if (withdrawInfo.withDrawType == 1) {
                iv_bindPlatform.setVisibility(withdrawInfo.hasWx ? View.VISIBLE : View.GONE);
                if (withdrawInfo.hasWx) {
                    iv_bindPlatform.setBackgroundDrawable(getResources().getDrawable(R.drawable.icon_withdraw_platform_wx_big));
                }
                tv_platformNick.setVisibility(View.GONE);
                tv_platformAccount.setVisibility(View.GONE);
                tv_unBindingTips.setVisibility(View.VISIBLE);
                withdraw_third_platform_type = withdrawInfo.hasWx ? 1 : 0;
                tv_unBindingTips.setText(getResources().getString(withdrawInfo.hasWx ?
                        R.string.my_income_withdraw_binding_wx_tips : R.string.my_income_withdraw_bind_tips));
            }
            diamondNumWithdraw.setText(String.valueOf(withdrawInfo.diamondNum));
        }
    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onGetWithdrawUserInfoFail(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    private void loadRecyclerViewData() {
        CoreManager.getCore(IWithdrawCore.class).getWithdrawList();
    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onGetWithdrawList(List<WithdrwaListInfo> withdrwaListInfo) {
        if (withdrwaListInfo != null && withdrwaListInfo.size() > 0) {
            mJewelAdapter.setNewData(withdrwaListInfo);
        }
    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onGetWithdrawListFail(String error) {
        toast("获取提现列表失败");
    }


    private void setListener() {
        //用户点击绑定弹出绑定平台提示dialog
        findViewById(R.id.rl_binder).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //无论如何都弹提现方式对话框
                ChooseThirdPlatformDialog dialog = new ChooseThirdPlatformDialog(WithdrawActivity.this, true);
                dialog.setOnThirdPlatformChooseListener(WithdrawActivity.this);
                dialog.show();
            }
        });
    }

    private boolean isWithdraw() {
        if (withdrawInfos != null && !withdrawInfos.isNotBoundPhone) {
            //这里需要动态修改背景
            if (checkedPosition != null) {
                //用户的钻石余额 > 选中金额的钻石数时
                btnWithdraw.setBackground(getResources().getDrawable(withdrawInfos.diamondNum >= checkedPosition.diamondNum ? R.drawable.shape_red_5dp : R.drawable.shape_red_cant_5dp));
                return true;
            }
        } else {
            return false;
        }
        //如果选中position不为空的时候
        return false;
    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onRequestExchange(ExchangerInfo exchangerInfo) {
        getDialogManager().dismissDialog();
        if (exchangerInfo != null && diamondNumWithdraw != null) {
            diamondNumWithdraw.setText(exchangerInfo.diamondNum + "");
            toast(R.string.withdraw_success);
        }
    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onRequestExchangeFail(int code, String error) {
        getDialogManager().dismissDialog();
        if (code == RealNameAuthStatusChecker.RESULT_FAILED_NEED_REAL_NAME_AUTH) {
            CoreManager.notifyClients(IAuthClient.class, IAuthClient.METHOD_ON_NEED_REAL_NAME_AUTH, this,
                    getResources().getString(R.string.real_name_auth_tips,
                            getResources().getString(R.string.real_name_auth_tips_withdraw)));

            return;
        }
        toast(error);
    }

    private void initView() {
        diamondNumWithdraw = (TextView) findViewById(R.id.tv_diamondNums);
        tv_platformAccount = (TextView) findViewById(R.id.tv_platformAccount);
        tv_platformNick = (TextView) findViewById(R.id.tv_platformNick);
        tv_unBindingTips = (TextView) findViewById(R.id.tv_unBindingTips);
        iv_bindPlatform = (ImageView) findViewById(R.id.iv_bindPlatform);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        btnWithdraw = (Button) findViewById(R.id.btn_withdraw);
    }

    @Override
    public void initTitleBar(String title) {
        mTitleBar = (TitleBar) findViewById(R.id.title_bar);
        if (mTitleBar != null) {
            mTitleBar.setTitle(title);
            mTitleBar.setImmersive(false);
            mTitleBar.setTitleColor(getResources().getColor(R.color.black));
            mTitleBar.setSubTitleColor(getResources().getColor(R.color.text_1A1A1A));
            mTitleBar.setLeftImageResource(R.drawable.arrow_left);
            mTitleBar.setLeftClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
        mTitleBar.setActionTextColor(getResources().getColor(R.color.text_1A1A1A));
        mTitleBar.addAction(new TitleBar.TextAction(getResources().getString(R.string.withdraw_rule)) {
            @Override
            public void performAction(View view) {
                startActivity(new Intent(getApplicationContext(), WithdrawRuleActivity.class));
            }
        });
    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onBinderAlipay() {
        toast(getResources().getString(R.string.third_platform_bind_success, getResources().getString(R.string.third_platform_alipay)));
        withdraw_third_platform_type = 2;
        getWithdrawUserInfo();
    }

    @Override
    protected void onResume() {
        super.onResume();
        userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
    }

    @Override
    public void onThirdPlatformChoosed(ChooseThirdPlatformDialog.ThirdPlatformType third, boolean bindOrUnBind) {
        //这里需要判断是否对应平台帐号已经绑定，绑定则直接走提现流程，否则走绑定流程
        if (null == withdrawInfos) {
            return;
        }
        if (third == ChooseThirdPlatformDialog.ThirdPlatformType.alipay) {
            if (!TextUtils.isEmpty(withdrawInfos.alipayAccount) && !withdrawInfos.alipayAccount.equals("null")) {
                tv_platformAccount.setVisibility(View.VISIBLE);
                tv_platformAccount.setText(getResources().getString(R.string.third_platform_binding_alipay_account, withdrawInfos.alipayAccount));
                tv_platformNick.setText(getResources().getString(R.string.third_platform_binding_alipay_name, withdrawInfos.alipayAccountName));
                tv_platformNick.setVisibility(View.VISIBLE);
                tv_unBindingTips.setVisibility(View.GONE);
                iv_bindPlatform.setVisibility(View.VISIBLE);
                withdraw_third_platform_type = 2;
                iv_bindPlatform.setBackgroundDrawable(getResources().getDrawable(R.drawable.zhifubao));
            } else {
                //跳转旧的绑定支付宝界面
                Intent intent = new Intent(WithdrawActivity.this, BinderAlipayActivity.class);
                Bundle mBundle = new Bundle();
                mBundle.putSerializable("withdrawInfo", withdrawInfos);
                intent.putExtras(mBundle);
                startActivity(intent);
            }
        } else {
            if (withdrawInfos.hasWx) {
                iv_bindPlatform.setVisibility(View.VISIBLE);
                iv_bindPlatform.setBackgroundDrawable(getResources().getDrawable(R.drawable.icon_withdraw_platform_wx_big));
                tv_platformNick.setVisibility(View.GONE);
                tv_platformAccount.setVisibility(View.GONE);
                tv_unBindingTips.setVisibility(View.VISIBLE);
                tv_unBindingTips.setText(getResources().getString(R.string.my_income_withdraw_binding_wx_tips));
                withdraw_third_platform_type = 1;
            } else {
                //跳转微信绑定帐号界面
                GetSmsCodeToBindThirdPlatformActivity.startForResult(this, request_code_bind_wx, true, request_code_bind_wx);
            }
        }
    }

    public final int request_code_bind_wx = 1;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogUtils.d(TAG, "onActivityResult-requestCode:" + requestCode + " resultCode:" + resultCode);
        if (request_code_bind_wx == requestCode && null != data &&
                data.getIntExtra("third_platform_type", 0) == request_code_bind_wx) {
            iv_bindPlatform.setVisibility(View.VISIBLE);
            iv_bindPlatform.setBackgroundDrawable(getResources().getDrawable(R.drawable.icon_withdraw_platform_wx_big));
            tv_platformNick.setVisibility(View.GONE);
            tv_platformAccount.setVisibility(View.GONE);
            tv_unBindingTips.setVisibility(View.VISIBLE);
            tv_unBindingTips.setText(getResources().getString(R.string.my_income_withdraw_binding_wx_tips));
            getWithdrawUserInfo();
            withdraw_third_platform_type = 1;
            toast(getResources().getString(R.string.third_platform_bind_success, getResources().getString(R.string.third_platform_wechat)));
        }
    }
}
