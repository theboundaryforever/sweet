package com.yuhuankj.tmxq.ui.audio.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.netease.nimlib.sdk.StatusCode;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomKickOutEvent;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.TimeUtils;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.im.login.IIMLoginClient;
import com.tongdaxing.xchat_core.music.IMusicDownloaderCore;
import com.tongdaxing.xchat_core.music.IMusicDownloaderCoreClient;
import com.tongdaxing.xchat_core.music.bean.HotMusicInfo;
import com.tongdaxing.xchat_core.player.IPlayerCore;
import com.tongdaxing.xchat_core.player.IPlayerCoreClient;
import com.tongdaxing.xchat_core.player.bean.LocalMusicInfo;
import com.tongdaxing.xchat_core.room.IRoomCoreClient;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.databinding.ActivityUploadMusicListBinding;
import com.yuhuankj.tmxq.ui.audio.adapter.MusicPlayListAdapter;
import com.yuhuankj.tmxq.ui.audio.adapter.MusicUploadListAdapter;
import com.yuhuankj.tmxq.ui.audio.presenter.MyUploadMusicListPresenter;
import com.yuhuankj.tmxq.ui.widget.DividerItemDecoration;
import com.yuhuankj.tmxq.ui.widget.VoiceSeekDialog;

import java.util.ArrayList;
import java.util.List;

import static com.tongdaxing.xchat_core.player.IPlayerCore.PLAY_STATUS_MUSIC_LIST_EMPTY;

/**
 * Created by chenran on 2017/10/28.
 */
@CreatePresenter(MyUploadMusicListPresenter.class)
public class MyUploadMusicListActivity extends BaseMvpActivity<MyUploadMusicView, MyUploadMusicListPresenter>
        implements MyUploadMusicView,View.OnClickListener, SeekBar.OnSeekBarChangeListener,
        MusicPlayListAdapter.OnPlayErrorListener, MusicUploadListAdapter.OnUploadMusicItemClickListener {

    private final boolean displayCustomBg = false;

    private final String TAG = MyUploadMusicListActivity.class.getSimpleName();

    private String imgBgUrl;
    private MusicUploadListAdapter adapter;
    private ActivityUploadMusicListBinding musicListBinding;

    private LocalMusicInfo currMusicInfo;
    private List<HotMusicInfo> myUploadMusicInfoList = new ArrayList<>();

    public static void start(Context context, String imgBgUrl) {
        Intent intent = new Intent(context, MyUploadMusicListActivity.class);
        intent.putExtra("imgBgUrl", imgBgUrl);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View content = findViewById(android.R.id.content);
        ViewGroup.LayoutParams params = content.getLayoutParams();
        params.height = getResources().getDisplayMetrics().heightPixels;

        musicListBinding = DataBindingUtil.setContentView(this,R.layout.activity_upload_music_list);
        musicListBinding.setClick(this);
        initMusicPlayControlView();

        imgBgUrl = getIntent().getStringExtra("imgBgUrl");
        initView();
    }

    private void initView() {
        adapter = new MusicUploadListAdapter(this);
        adapter.setHotMusicInfos(myUploadMusicInfoList);
        adapter.setOnPlayErrorListener(this);
        adapter.setOnUploadMusicItemClickListener(this);
        LinearLayoutManager linearLayoutManager = new RecyclerViewNoBugLinearLayoutManager(this);
        musicListBinding.rvLocalMusic.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, linearLayoutManager.getOrientation(),
                1, R.color.color_1Affffff);
        dividerItemDecoration.setIngoreLastDividerItem(true);
        musicListBinding.rvLocalMusic.addItemDecoration(dividerItemDecoration);
        musicListBinding.rvLocalMusic.setAdapter(adapter);

        currMusicInfo = CoreManager.getCore(IPlayerCore.class).getCurrLocalMusicInfo();
        updateMusicPlayView();
        getDialogManager().showProgressDialog(this,getResources().getString(R.string.loading));
        getMvpPresenter().getUploadMusicList(1,1000,null);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fl_report:
            case R.id.tv_report:
                try {
                    long songId = Long.valueOf(currMusicInfo.getSongId());
                    if(null != currMusicInfo && songId>0L){
                        ReportMusicActivity.start(this,songId);
                    }
                }catch (NumberFormatException nfe){
                    nfe.printStackTrace();
                }catch (IllegalStateException ise){
                    ise.printStackTrace();
                }
                break;
            case R.id.fl_musicVoice:
                VoiceSeekDialog voiceSeekDialog = new VoiceSeekDialog(this);
                voiceSeekDialog.show();
                break;
            case R.id.iv_musicPlayStatus:
                int state = CoreManager.getCore(IPlayerCore.class).getState();
                if (state == IPlayerCore.STATE_PLAY) {
                    CoreManager.getCore(IPlayerCore.class).pause();
                } else if (state == IPlayerCore.STATE_PAUSE) {
                    int result = CoreManager.getCore(IPlayerCore.class).play(currMusicInfo);
                    if (result < 0) {
                        toast(R.string.music_play_failed_file_format_error);
                    }
                } else {
                    int result = CoreManager.getCore(IPlayerCore.class).play(null);
                    if (result < 0) {
                        if (result == PLAY_STATUS_MUSIC_LIST_EMPTY) {
                            toast(R.string.music_play_failed_list_empty);
                        } else {
                            toast(R.string.music_play_failed_file_format_error);
                        }
                    }
                }
                break;
            case R.id.back_btn:
                finish();
                break;
            default:
        }
    }

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onBeKickOut(ChatRoomKickOutEvent.ChatRoomKickOutReason reason) {
        finish();
    }

    @CoreEvent(coreClientClass = IIMLoginClient.class)
    public void onKickedOut(StatusCode code) {
        finish();
    }

    @Override
    public void onGetUploadMusicInfoList(boolean isSuccess, String msg, List list) {
        LogUtils.d(TAG,"onGetUploadMusicInfoList-isSuccess:"+isSuccess+" msg:"+msg);
        myUploadMusicInfoList = list;
        adapter.setHotMusicInfos(myUploadMusicInfoList);
        adapter.notifyDataSetChanged();
        getDialogManager().dismissDialog();
        if (isSuccess && myUploadMusicInfoList != null && myUploadMusicInfoList.size() > 0) {
            musicListBinding.rvLocalMusic.setVisibility(View.VISIBLE);
            musicListBinding.llLocalMusicEmpty.setVisibility(View.GONE);
        } else {
            musicListBinding.rvLocalMusic.setVisibility(View.GONE);
            musicListBinding.llLocalMusicEmpty.setVisibility(View.VISIBLE);
        }
    }

    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient.class)
    public void onHotMusicDownloadProgressUpdate(LocalMusicInfo localMusicInfo, HotMusicInfo hotMusicInfo, int progress) {
        int index = myUploadMusicInfoList.indexOf(hotMusicInfo);
        if(-1 != index){
            adapter.notifyItemChanged(index,progress);
        }
    }

    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient.class)
    public void onHotMusicDownloadError(String msg, LocalMusicInfo localMusicInfo, HotMusicInfo hotMusicInfo) {
        int index = myUploadMusicInfoList.indexOf(hotMusicInfo);
        if(-1 != index){
            adapter.notifyItemChanged(index,-1);
        }
    }

    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient.class)
    public void onHotMusicDownloadCompleted(HotMusicInfo hotMusicInfo) {
        int index = myUploadMusicInfoList.indexOf(hotMusicInfo);
        if(-1 != index){
            adapter.notifyItemChanged(index,101);
        }
    }

    @Override
    public void onUploadMusicClickToDown(HotMusicInfo hotMusicInfo) {
        CoreManager.getCore(IMusicDownloaderCore.class).addHotMusicToDownQueue(hotMusicInfo);
    }

    @Override
    public void onPlayError(String msg) {
        if(!TextUtils.isEmpty(msg)){
            toast(msg);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getDialogManager().dismissDialog();
    }

    //==============================音乐播放控制面板===================================

    private View fl_report;
    private View tv_report;
    private View fl_musicVoice;
    private View rl_musicPlayControl;
    private TextView tv_musicName;
    private ImageView iv_musicPlayStatus;
    private SeekBar sb_musicPlayProgress;
    private TextView tv_totalPlayTime;
    private TextView tv_currPlayTime;

    private boolean isSBProgressChangedByUserTouch = false;

    private void initMusicPlayControlView() {
        tv_musicName = (TextView) findViewById(R.id.tv_musicName);
        tv_totalPlayTime = (TextView) findViewById(R.id.tv_totalPlayTime);
        tv_currPlayTime = (TextView) findViewById(R.id.tv_currPlayTime);
        fl_report = findViewById(R.id.fl_report);
        tv_report = findViewById(R.id.tv_report);
        fl_musicVoice = findViewById(R.id.fl_musicVoice);
        rl_musicPlayControl = findViewById(R.id.rl_musicPlayControl);
        iv_musicPlayStatus = (ImageView) findViewById(R.id.iv_musicPlayStatus);
        sb_musicPlayProgress = (SeekBar) findViewById(R.id.sb_musicPlayProgress);
        sb_musicPlayProgress.setMax(100);
        sb_musicPlayProgress.setOnSeekBarChangeListener(this);
        iv_musicPlayStatus.setOnClickListener(this);
        fl_report.setOnClickListener(this);
        tv_report.setOnClickListener(this);
        fl_musicVoice.setOnClickListener(this);
    }

    private void updateMusicPlayView() {
        String dur = null;
        if (currMusicInfo != null && CoreManager.getCore(IPlayerCore.class).getState() != IPlayerCore.STATE_STOP) {
            rl_musicPlayControl.setVisibility(View.VISIBLE);
            long songId = 0;
            try {
                songId = Long.valueOf(currMusicInfo.getSongId());
            }catch (NumberFormatException nfe){
                nfe.printStackTrace();
            }catch (IllegalStateException ise){
                ise.printStackTrace();
            }
            fl_report.setVisibility(songId>0 ? View.VISIBLE : View.INVISIBLE);
            tv_musicName.setText(currMusicInfo.getSongName());
            dur = TimeUtils.getFormatTimeString(currMusicInfo.getDuration(), "min:sec");
            int state = CoreManager.getCore(IPlayerCore.class).getState();
            if (state == IPlayerCore.STATE_PLAY) {
                iv_musicPlayStatus.setImageResource(R.mipmap.icon_music_pause);
            } else {
                iv_musicPlayStatus.setImageResource(R.mipmap.icon_music_play);
            }
        } else {
            tv_musicName.setText(getResources().getString(R.string.music_play_empty));
            dur = TimeUtils.getFormatTimeString(0, "min:sec");
            iv_musicPlayStatus.setImageResource(R.mipmap.icon_music_play);
            fl_report.setVisibility(View.INVISIBLE);
            rl_musicPlayControl.setVisibility(View.GONE);
        }
        tv_totalPlayTime.setText(getResources().getString(R.string.music_play_control_dur,dur));
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicProgressUpdate(long total, long current) {
        int progress = (int) (current*100/total);
        LogUtils.d(TAG, "onMusicProgressUpdate-total:" + total + " current:" + current + " progress:" + progress + " isSBProgressChangedByUserTouch:" + isSBProgressChangedByUserTouch);
        if(isSBProgressChangedByUserTouch){
            return;
        }
        tv_currPlayTime.setText(TimeUtils.getFormatTimeString(current, "min:sec"));
        tv_totalPlayTime.setText(getResources().getString(R.string.music_play_control_dur,TimeUtils.getFormatTimeString(total, "min:sec")));
        sb_musicPlayProgress.setProgress(progress);
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicPlaying(LocalMusicInfo localMusicInfo) {
        this.currMusicInfo = localMusicInfo;
        updateMusicPlayView();
//        notifyLastMusicStopPlay();
//        changeMusicPlayStatus(localMusicInfo);
        adapter.notifyDataSetChanged();
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicPause(LocalMusicInfo localMusicInfo) {
        this.currMusicInfo = localMusicInfo;
        updateMusicPlayView();
//        changeMusicPlayStatus(localMusicInfo);
        adapter.notifyDataSetChanged();
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicStop() {
        updateMusicPlayView();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        isSBProgressChangedByUserTouch = true;
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        CoreManager.getCore(IPlayerCore.class).setAudioMixCurrPosition(seekBar.getProgress());
        isSBProgressChangedByUserTouch = false;
    }

    private HotMusicInfo currHotInfo;

    private void notifyLastMusicStopPlay() {
        if(null != currHotInfo){
            int index = myUploadMusicInfoList.indexOf(currHotInfo);
            if(-1 != index){
                adapter.notifyItemChanged(index);
            }
        }
    }

    private void changeMusicPlayStatus(LocalMusicInfo localMusicInfo){
        try {
            int songId = Integer.valueOf(localMusicInfo.getSongId());
            HotMusicInfo tempInfo = new HotMusicInfo();
            tempInfo.setId(songId);
            if(songId>0){
                int index = myUploadMusicInfoList.indexOf(tempInfo);
                if(-1 != index){
                    adapter.notifyItemChanged(index);
                    currHotInfo = myUploadMusicInfoList.get(index);
                }
            }
        } catch (NumberFormatException nfex){
            nfex.printStackTrace();
        } catch (IllegalStateException ise){
            ise.printStackTrace();
        }
    }

    @Override
    public boolean isStatusBarDarkFont() {
        return false;
    }
}
