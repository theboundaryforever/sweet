package com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter;

import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.xchat_core.user.bean.GiftWallInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

/**
 * oppo市场，首页，单人音频直播间列表，adapter
 *
 * @author weihaitao
 * @date 2019年5月21日 17:18:56
 */
public class MultiRoomRecvGiftListAdapter extends BaseQuickAdapter<GiftWallInfo, BaseViewHolder> {

    private int imvIconWidth = 0;
    private int imvIconHeight = 0;

    public MultiRoomRecvGiftListAdapter(List<GiftWallInfo> data) {
        super(R.layout.item_room_gift_list, data);
        imvIconWidth = DisplayUtility.dp2px(mContext, 40);
        imvIconHeight = DisplayUtility.dp2px(mContext, 48);
    }

    @Override
    protected void convert(BaseViewHolder helper, GiftWallInfo item) {
        helper.addOnClickListener(R.id.rlGiftInfo);
        ImageView ivGiftIcon = helper.getView(R.id.ivGiftIcon);
        if (GlideContextCheckUtil.checkContextUsable(mContext) && !TextUtils.isEmpty(item.getPicUrl())) {
            GlideApp.with(mContext).load(ImageLoadUtils.toThumbnailUrl(imvIconWidth,
                    imvIconHeight, item.getPicUrl()))
                    .dontAnimate()
                    .error(R.drawable.bg_default_cover_round_placehold_size60)
                    .into(ivGiftIcon);
        }
        TextView tvGiftName = helper.getView(R.id.tvGiftName);
        tvGiftName.setText(item.getGiftName());
        TextView tvGiftNum = helper.getView(R.id.tvGiftNum);
        tvGiftNum.setText(String.valueOf(item.getReciveCount()));
    }
}