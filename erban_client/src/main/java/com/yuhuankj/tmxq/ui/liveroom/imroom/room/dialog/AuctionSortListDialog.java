package com.yuhuankj.tmxq.ui.liveroom.imroom.room.dialog;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.noober.background.view.BLTextView;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.room.auction.ParticipateAuctionBean;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.AppDialogConstant;
import com.yuhuankj.tmxq.base.dialog.BaseAppMvpDialogFragment;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter.AuctionSortListAdapter;

import java.util.List;

/**
 * 文件描述：参与拍卖列表dialog
 *
 * @auther：zwk
 * @data：2019/7/30
 */
@CreatePresenter(AuctionSortListPresenter.class)
public class AuctionSortListDialog extends BaseAppMvpDialogFragment<IAuctionSortListView, AuctionSortListPresenter> implements IAuctionSortListView {
    private LinearLayout mContentLayout;
    private RelativeLayout rlPriority;
    private TextView tvJoinCount;
    private RecyclerView rvAuctionSort;
    private TextView tvAuctionEmpty;
    private AuctionSortListAdapter auctionSortListAdapter;
    private BLTextView bltvJoin;
    private long myUid = -1;
    private ParticipateAuctionBean auctionBean;


    @Override
    protected int getDialogStyle() {
        return AppDialogConstant.STYLE_BOTTOM_BACK_DARK;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_auction_sort_list;
    }

    @Override
    protected void initView(View view) {
        mContentLayout = view.findViewById(R.id.ll_sort_list_content);
        rlPriority = view.findViewById(R.id.rl_priority_card);
        tvJoinCount = view.findViewById(R.id.tv_current_join_auction);
        rvAuctionSort = view.findViewById(R.id.rv_auction_sort_list);
        tvAuctionEmpty = view.findViewById(R.id.tv_auction_sort_list_empty);
        auctionSortListAdapter = new AuctionSortListAdapter();
        rvAuctionSort.setAdapter(auctionSortListAdapter);
        bltvJoin = view.findViewById(R.id.bltv_participate_auction);
    }

    @Override
    protected void initListener(View view) {
        rlPriority.setOnClickListener(v -> {
            AuctionPriorityChannelDialog priorityChannelDialog = new AuctionPriorityChannelDialog();
            priorityChannelDialog.show(getFragmentManager(), null);
            dismiss();
        });
        auctionSortListAdapter.setOnItemClickListener((adapter, view1, position) -> {
            if (RoomDataManager.get().isRoomOwner() || RoomDataManager.get().isRoomAdmin()) {
                if (RoomDataManager.get().getAuction() != null && RoomDataManager.get().getAuction().getUid() > 0) {//判断是否有用户在拍卖位
                    toast("当前有用户在拍卖中!");
                } else {
                    if (auctionSortListAdapter != null
                            && !ListUtils.isListEmpty(auctionSortListAdapter.getData())
                            && auctionSortListAdapter.getData().size() > position) {
                        getMvpPresenter().operateUpMicro(1, auctionSortListAdapter.getData().get(position).getUid(), true);
                        dismiss();
                    }
                }
            }
        });
        bltvJoin.setOnClickListener(v -> {
            if (bltvJoin.isSelected()) {
                if (auctionBean != null) {
                    AuctionSortListDialog.this.getMvpPresenter().cancelRoomAuction(auctionBean.getRoomId(), auctionBean.getDay(), auctionBean.getProject());
                }
            } else {
                AuctionSettingDialog settingDialog = new AuctionSettingDialog();
                //在这里使用getChildFragmentManager会不起作用
                settingDialog.show(AuctionSortListDialog.this.getFragmentManager(), null);
                AuctionSortListDialog.this.dismiss();
            }
        });
    }

    @Override
    protected void initViewState() {
        myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        showLoading();
        getMvpPresenter().getRoomAuctionList(RoomDataManager.get().getCurrentRoomInfo() != null ? RoomDataManager.get().getCurrentRoomInfo().getRoomId() : 0);
    }

    @Override
    public void onReloadData() {
        super.onReloadData();
        initViewState();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mContentLayout != null) {
            mContentLayout.removeAllViews();
            mContentLayout = null;
        }
        if (auctionSortListAdapter != null) {
            auctionSortListAdapter.setOnItemClickListener(null);
        }
        if (getDialog() != null) {
            getDialog().setOnShowListener(null);
            getDialog().setOnCancelListener(null);
            getDialog().setOnDismissListener(null);
        }
    }

    @Override
    public void isShowPriorityCardView(boolean show) {
        rlPriority.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showSortListCount(int count) {
        tvJoinCount.setText(String.valueOf(count));
    }

    @Override
    public void isShowParticipateBtn(boolean hasJoin) {
        if (RoomDataManager.get().isRoomOwner()) {
            bltvJoin.setVisibility(View.GONE);
        } else {
            bltvJoin.setVisibility(View.VISIBLE);
            bltvJoin.setSelected(hasJoin);
            bltvJoin.setText(hasJoin ? "取消参加" : "我要参加");
        }
    }

    @Override
    public void showAuctionSortListView(List<ParticipateAuctionBean> datas) {
        hideStatus();
        if (ListUtils.isListEmpty(datas)) {
            tvAuctionEmpty.setVisibility(View.VISIBLE);
            rvAuctionSort.setVisibility(View.GONE);
        } else {
            tvAuctionEmpty.setVisibility(View.GONE);
            rvAuctionSort.setVisibility(View.VISIBLE);
            if (auctionSortListAdapter != null) {
                auctionSortListAdapter.setNewData(datas);
            }
            for (int i = 0; i < datas.size(); i++) {
                if (datas.get(i).getUid() == myUid) {
                    auctionBean = datas.get(i);
                }
            }
        }
    }

    @Override
    public void showErrorAuctionSortListView() {
        hideStatus();
        showNetworkErr();
    }

    @Override
    public void updateSortListInfo() {
        initViewState();
    }

    @Override
    public void downMicro() {
        if (RoomDataManager.get().isOwnerOnMic()) {
            IMRoomMessageManager.get().downMicroPhoneBySdk(
                    RoomDataManager.get().getMicPosition(CoreManager.getCore(IAuthCore.class).getCurrentUid()), null);
        }
    }
}
