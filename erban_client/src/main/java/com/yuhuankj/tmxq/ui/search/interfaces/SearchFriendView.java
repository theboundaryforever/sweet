package com.yuhuankj.tmxq.ui.search.interfaces;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.home.HomeRoom;

import java.util.List;

public interface SearchFriendView<T extends AbstractMvpPresenter> extends IMvpBaseView {

    /**
     * 搜索朋友 关键字搜索结果返回
     * @param success
     * @param message
     * @param friends
     */
    void onFriendSearched(boolean success, String message, List<HomeRoom> friends);
}
