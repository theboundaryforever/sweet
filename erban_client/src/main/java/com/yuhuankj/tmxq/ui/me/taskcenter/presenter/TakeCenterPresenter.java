package com.yuhuankj.tmxq.ui.me.taskcenter.presenter;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.me.taskcenter.model.TaskCenterEnitity;
import com.yuhuankj.tmxq.ui.me.taskcenter.model.TaskCenterModel;
import com.yuhuankj.tmxq.ui.me.taskcenter.view.ITaskCenterView;
import com.yuhuankj.tmxq.ui.signAward.model.SignAwardResult;
import com.yuhuankj.tmxq.ui.signAward.model.SignInModel;

/**
 * @author weihaitao
 * @date 2019/5/21
 */
public class TakeCenterPresenter extends AbstractMvpPresenter<ITaskCenterView> {

    private SignInModel signInModel;
    private TaskCenterModel taskCenterModel;

    public TakeCenterPresenter() {
        signInModel = new SignInModel();
        taskCenterModel = new TaskCenterModel();
    }

    public void receiveSignInGift() {
        signInModel.receiveSignInGift(new HttpRequestCallBack<SignAwardResult>() {
            @Override
            public void onSuccess(String message, SignAwardResult result) {
                if (null == getMvpView()) {
                    return;
                }
                if (null == result) {
                    getMvpView().showSignErr(BasicConfig.INSTANCE.getAppContext().getResources().getString(R.string.network_error));
                    return;
                }
                //视为成功签到，直接修改状态为已经领取
                getMvpView().refreshSignInAwardGotStatus(result);

                //这里需要刷新礼物列表
                CoreManager.getCore(IGiftCore.class).requestGiftInfos();
                //视为成功签到，直接修改状态为已经领取
                getMvpView().showResultAfterSignSuc(result);

                //刷新一遍任务中心列表数据
                getTaskList();
            }

            @Override
            public void onFailure(int code, String msg) {
                if (null == getMvpView()) {
                    return;
                }
                getMvpView().showSignErr(msg);
            }
        });
    }

    public void getTaskList() {
        taskCenterModel.getTaskList(new HttpRequestCallBack<TaskCenterEnitity>() {
            @Override
            public void onSuccess(String message, TaskCenterEnitity response) {
                if (null == getMvpView()) {
                    return;
                }
                if (null == response) {
                    getMvpView().showSignErr(BasicConfig.INSTANCE.getAppContext().getResources().getString(R.string.network_error));
                    return;
                }
                getMvpView().refreshTaskList(response);
            }

            @Override
            public void onFailure(int code, String msg) {
                if (null == getMvpView()) {
                    return;
                }
                getMvpView().showSignErr(msg);
            }
        });
    }
}
