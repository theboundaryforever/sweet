package com.yuhuankj.tmxq.ui.liveroom.imroom.room.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/7/30
 */
public interface IAuctionPriorityChannelView extends IMvpBaseView {
    void showToast(String result);
    void updateWalletGoldCount(WalletInfo data);
}
