package com.yuhuankj.tmxq.ui.nim.game;

import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

public class GameAdapter extends BaseQuickAdapter<GameEnitity, BaseViewHolder> {

    public GameAdapter(int layoutResId, List data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, GameEnitity item) {
        ImageView imvContent = helper.getView(R.id.imvIcon);
        TextView tvName = helper.getView(R.id.tvName);
        TextView tvTip = helper.getView(R.id.tvOnLineCount);

        ImageLoadUtils.loadImage(mContext, item.getImage(), imvContent);
        tvName.setText(item.getName());
        tvTip.setText(item.getPlayers() + "对在玩");
    }
}