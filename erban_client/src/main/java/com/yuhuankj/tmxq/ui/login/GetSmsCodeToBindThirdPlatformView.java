package com.yuhuankj.tmxq.ui.login;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

public interface GetSmsCodeToBindThirdPlatformView<T extends AbstractMvpPresenter> extends IMvpBaseView {

    void onGetSmsCode(boolean isSuccess, String message);

    void onBindThirdPlatform(boolean isSuccess, String message, int third_platform_type);

    void onUnBindThirdPlatform(boolean isSuccess, String message, int third_platform_type);

    void onCheckSmsCode(boolean isSuccess, String message);

    void onThirdPlatformLogined(boolean isSuccess, String message, int third_platform_type, String accessToken, String openId, String unionId);
}
