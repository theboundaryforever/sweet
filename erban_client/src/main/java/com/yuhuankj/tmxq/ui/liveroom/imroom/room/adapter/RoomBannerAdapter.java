package com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jude.rollviewpager.adapter.StaticPagerAdapter;
import com.tongdaxing.erban.libcommon.utils.ButtonUtils;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.xchat_core.redpacket.bean.ActionDialogInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

/**
 * @author Administrator
 * @date 2017/8/7
 */

public class RoomBannerAdapter extends StaticPagerAdapter {
    private Context context;
    private List<ActionDialogInfo> bannerInfoList;
    private LayoutInflater mInflater;
    private OnRoomBannerClickListener onRoomBannerClickListener;
    private int bannerImgWidAndHeight = 0;

    public RoomBannerAdapter(List<ActionDialogInfo> bannerInfoList, Context context) {
        this.context = context;
        this.bannerInfoList = bannerInfoList;
        mInflater = LayoutInflater.from(context);
        bannerImgWidAndHeight = DisplayUtility.dp2px(context, (int) context.getResources().getDimension(R.dimen.room_banner_width));
    }

    @Override
    public View getView(ViewGroup container, int position) {
        final ActionDialogInfo bannerInfo = bannerInfoList.get(position);
        ImageView imgBanner = (ImageView) mInflater.inflate(R.layout.banner_page_item_room_action, container, false);
        ImageLoadUtils.loadBannerRoundBackground(context,
                ImageLoadUtils.toThumbnailUrl(bannerImgWidAndHeight,
                        bannerImgWidAndHeight, bannerInfo.getAlertWinPic()), imgBanner);
        imgBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick(v.getId())) {
                    return;
                }
                if (null != onRoomBannerClickListener) {
                    onRoomBannerClickListener.onGameBannerClicked(bannerInfo);
                }
            }
        });
        return imgBanner;
    }

    @Override
    public int getCount() {
        if (bannerInfoList == null) {
            return 0;
        } else {
            return bannerInfoList.size();
        }
    }

    public void setOnRoomBannerClickListener(OnRoomBannerClickListener onRoomBannerClickListener) {
        this.onRoomBannerClickListener = onRoomBannerClickListener;
    }


    public interface OnRoomBannerClickListener {
        void onGameBannerClicked(ActionDialogInfo actionDialogInfo);
    }
}
