package com.yuhuankj.tmxq.ui.liveroom.imroom.room.dialog;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.AppDialogConstant;
import com.yuhuankj.tmxq.base.dialog.BaseAppDialog;

/**
 * 文件描述：房间加载动画 -- 过度作用
 *
 * @auther：zwk
 * @data：2019/6/28
 */
public class RoomLoadingDialog extends BaseAppDialog {
    private ImageView ivRoomLoading;

    public RoomLoadingDialog(Context context) {
        super(context);
    }

    @Override
    public int onDialogStyle() {
        return AppDialogConstant.STYLE_CENTER;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_room_loading;
    }

    @Override
    protected void initView() {
        ivRoomLoading = findViewById(R.id.iv_room_loading_anim);
    }

    @Override
    public void initListener() {
        setCancelable(false);
        setCanceledOnTouchOutside(false);
    }

    @Override
    public void initViewState() {
        Drawable loadingAnim = ivRoomLoading.getDrawable();
        if (loadingAnim instanceof AnimationDrawable) {
            ((AnimationDrawable) loadingAnim).start();
        }
    }


    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (null != ivRoomLoading) {
            ivRoomLoading.clearAnimation();
        }
    }
}
