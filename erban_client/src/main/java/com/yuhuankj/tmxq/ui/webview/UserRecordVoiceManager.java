package com.yuhuankj.tmxq.ui.webview;

import android.content.Context;
import android.text.TextUtils;

import com.netease.nimlib.sdk.media.record.AudioRecorder;
import com.netease.nimlib.sdk.media.record.IAudioRecordCallback;
import com.netease.nimlib.sdk.media.record.RecordType;
import com.tongdaxing.erban.libcommon.audio.AudioPlayer;
import com.tongdaxing.erban.libcommon.audio.OnPlayListener;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.audio.AudioPlayAndRecordManager;
import com.tongdaxing.xchat_core.audio.AudioRecordStatus;
import com.tongdaxing.xchat_core.utils.ThreadUtil;

import java.io.File;

/**
 * 用户录制音频业务管理类，避免多个类实例包含重复性业务代码
 */
public class UserRecordVoiceManager {

    private final String TAG = UserRecordVoiceManager.class.getSimpleName();

    private Context context;
    private AudioRecordStatus audioRecordStatus = AudioRecordStatus.STATE_RECORD_NORMAL;
    private AudioPlayAndRecordManager audioManager;
    private AudioPlayer audioPlayer;
    private AudioRecorder audioRecorder;
    private String audioUrl;
    private long lastRecordStartTime = 0L;
    private long lastRecordDuration = 0L;
    private boolean hasUserStopRecorded = false;
    private boolean hasRecordOverMaxDuration = false;
    private int maxDuration = 30;
    private int minDuration = 6;
    private long audioDura = 0L;
    private File audioFile;
    /**
     * true-manager自己监听录制时长是否满足最小和最大录制时长显示，用于app自身的业务逻辑处理
     * false-manager自己不做监听录制时长是否满足条件的处理，用于webview声鉴卡的业务流程
     */

    private boolean isDuraRecordListenedByOther = false;
    private IAudioRecordCallback commOnRecordCallback;
    private IAudioRecordCallback mOnnRecordCallback = new IAudioRecordCallback() {
        @Override
        public void onRecordReady() {
            LogUtils.d(TAG, "onRecordReady");
            hasUserStopRecorded = false;
            audioUrl = null;
            if (null != commOnRecordCallback) {
                commOnRecordCallback.onRecordReady();
            }
        }

        @Override
        public void onRecordStart(File audioFile, RecordType recordType) {
            LogUtils.d(TAG, "onRecordStart-recordType:" + recordType);
            if (isDuraRecordListenedByOther) {
                //录制最小时长和最大时长由外部控制，manager不做判断处理
                if (hasUserStopRecorded) {
                    audioManager.stopRecord(true);
                }
            } else {
                //录制最小时长和最大时长由内部控制，manager做判断处理
                if (hasUserStopRecorded) {
                    audioManager.stopRecord(lastRecordDuration < minDuration * 1000);
                }
            }

            if (null != commOnRecordCallback) {
                commOnRecordCallback.onRecordStart(audioFile, recordType);
            }
        }

        @Override
        public void onRecordSuccess(File audioFile, long audioLength, RecordType recordType) {
            LogUtils.d(TAG, "onRecordSuccess-audioLength:" + audioLength + " recordType:" + recordType);
            hasUserStopRecorded = true;
            audioDura = audioLength;
            UserRecordVoiceManager.this.audioFile = audioFile;
            audioRecordStatus = AudioRecordStatus.STATE_RECORD_SUCCESS;
            if (null != commOnRecordCallback) {
                commOnRecordCallback.onRecordSuccess(audioFile, audioLength, recordType);
            }
        }

        @Override
        public void onRecordFail() {
            LogUtils.d(TAG, "onRecordFail");
            audioRecordStatus = AudioRecordStatus.STATE_RECORD_NORMAL;
            hasUserStopRecorded = true;
            if (null != commOnRecordCallback) {
                commOnRecordCallback.onRecordFail();
            }
        }

        @Override
        public void onRecordCancel() {
            LogUtils.d(TAG, "onRecordCancel");
            audioRecordStatus = AudioRecordStatus.STATE_RECORD_NORMAL;
            hasUserStopRecorded = true;
            if (null != commOnRecordCallback) {
                commOnRecordCallback.onRecordCancel();
            }
        }

        @Override
        public void onRecordReachedMaxTime(int maxTime) {
            LogUtils.d(TAG, "onRecordReachedMaxTime-maxTime:" + maxTime);
            hasRecordOverMaxDuration = true;
            if (null != commOnRecordCallback) {
                commOnRecordCallback.onRecordReachedMaxTime(maxTime);
            }
        }
    };
    private OnPlayListener commOnPlayListener;
    private OnPlayListener mOnPlayListener = new OnPlayListener() {
        @Override
        public void onPrepared() {
            LogUtils.d(TAG, "onPrepared");
            if (null != commOnPlayListener) {
                commOnPlayListener.onPrepared();
            }
        }

        @Override
        public void onCompletion() {
            LogUtils.d(TAG, "onCompletion");
            if (null != commOnPlayListener) {
                commOnPlayListener.onCompletion();
            }
        }

        @Override
        public void onInterrupt() {
            LogUtils.d(TAG, "onInterrupt");
            if (null != commOnPlayListener) {
                commOnPlayListener.onInterrupt();
            }
        }

        @Override
        public void onError(String error) {
            LogUtils.d(TAG, "onError-error:" + error);
            if (null != commOnPlayListener) {
                commOnPlayListener.onError(error);
            }
        }

        @Override
        public void onPlaying(long curPosition) {
            LogUtils.d(TAG, "onError-curPosition:" + curPosition);
            if (null != commOnPlayListener) {
                commOnPlayListener.onPlaying(curPosition);
            }
        }
    };
    private Runnable deleteAudioFileRunnable = null;

    public UserRecordVoiceManager(Context context) {
        this.context = context;
    }

    public long getAudioDura() {
        return audioDura;
    }

    public long getLastRecordDuration() {
        return lastRecordDuration;
    }

    public File getAudioFile() {
        return audioFile;
    }

    public boolean isUserStopRecorded() {
        return hasUserStopRecorded;
    }

    /**
     * 设置外部音频URL，用户调用方已有音频文件直接播放场景
     *
     * @param audioUrl
     */
    public void setAudioUrl(String audioUrl) {
        this.audioUrl = audioUrl;
        LogUtils.d(TAG, "setAudioUrl-audioUrl:" + audioUrl);
    }

    public boolean isRecordOverMaxDuration() {
        return hasRecordOverMaxDuration;
    }

    public void setDuraRecordListenedByOther(boolean isDuraRecordListenedByOther) {
        this.isDuraRecordListenedByOther = isDuraRecordListenedByOther;
    }

    public void setMaxDuration(int maxDuration) {
        this.maxDuration = maxDuration;
    }

    public void setMinDuration(int maxDuration) {

    }

    public void setCommOnRecordCallback(IAudioRecordCallback commOnRecordCallback) {
        this.commOnRecordCallback = commOnRecordCallback;
    }

    /**
     * 设置录音播放监听器
     *
     * @param commOnPlayListener
     */
    public void setCommOnPlayListener(OnPlayListener commOnPlayListener) {
        LogUtils.d(TAG, "setCommOnPlayListener");
        this.commOnPlayListener = commOnPlayListener;
    }

    /**
     * 开始录音
     * 注意：判断是否处于房间内，属于调用层的处理逻辑，该manager不做这个判断
     */
    public void startRecord() {
        hasUserStopRecorded = false;
        LogUtils.d(TAG, "startRecord-hasUserStopRecorded:" + hasUserStopRecorded);
        if (audioRecordStatus == AudioRecordStatus.STATE_RECORD_RECORDING) {
            LogUtils.w(TAG, "已经处于录音过程中");
            return;
        } else if (audioRecordStatus == AudioRecordStatus.STATE_RECORD_SUCCESS) {
            //重置界面，删除录音文件
            LogUtils.w(TAG, "重置界面，删除录音文件");
            audioRecordStatus = AudioRecordStatus.STATE_RECORD_NORMAL;
            deleteLastRecord();
        }

        lastRecordStartTime = System.currentTimeMillis();
        LogUtils.w(TAG, "开始录音-lastRecordStartTime:" + lastRecordStartTime);
        audioRecordStatus = AudioRecordStatus.STATE_RECORD_RECORDING;
        if (null == audioManager) {
            audioManager = AudioPlayAndRecordManager.getInstance();
        }

        if (null == audioRecorder) {
            audioRecorder = audioManager.getAudioRecorder(context, mOnnRecordCallback);
        }
        audioManager.startRecord();
    }

    /**
     * 停止录音
     */
    public void stopRecord(boolean cancel) {
        hasUserStopRecorded = true;
        lastRecordDuration = System.currentTimeMillis() - lastRecordStartTime;
        LogUtils.d(TAG, "stopRecord-hasUserStopRecorded:" + hasUserStopRecorded + " lastRecordDuration:" + lastRecordDuration);
        if (null == audioManager) {
            audioManager = AudioPlayAndRecordManager.getInstance();
        }
        if (isDuraRecordListenedByOther) {
            audioManager.stopRecord(cancel);
        } else {
            //这里有可能调用方控件在触摸监听过程中，抬手触发了up事件后，录音回调还没回调到onRecordReady，
            // 也即recorder.isRecording()返回false导致操作中止，故需要以hasUserStopRecorded和lastRecordDuration两个字段来做记录处理
            audioManager.stopRecord(lastRecordDuration < minDuration * 1000);
        }
    }

    /**
     * 录音试听
     *
     * @param startOrStop true-试听，false-结束试听
     * @return true-操作成功，false-操作出错
     */
    public boolean tryToListener(boolean startOrStop) {
        LogUtils.d(TAG, "tryToListener-startOrStop:" + startOrStop);
        if (null == audioManager) {
            audioManager = AudioPlayAndRecordManager.getInstance();
        }

        if (null == audioPlayer) {
            audioPlayer = audioManager.getAudioPlayer(context, null, mOnPlayListener);
        }

        if (startOrStop && !audioManager.isPlaying()) {
            LogUtils.d(TAG, "tryToListener 当前处于未播放录音状态,即将播放");
            if (null != audioFile && audioFile.exists()) {
                LogUtils.d(TAG, "tryToListener-audioFile.path:" + audioFile.getPath());
                audioPlayer.setDataSource(audioFile.getPath());
                audioManager.play();
                return true;
            } else if (!TextUtils.isEmpty(audioUrl)) {
                LogUtils.d(TAG, "tryToListener-audioUrl:" + audioUrl);
                audioPlayer.setDataSource(audioUrl);
                audioManager.play();
                return true;
            } else {
                LogUtils.w(TAG, "tryToListener 播放失败，未找到播放源");
                return false;
            }
        } else {
            LogUtils.d(TAG, "tryToListener 当前处于已播放录音状态,即将停止播放");
            audioManager.stopPlay();
            return !startOrStop;
        }
    }

    public void releaseOnPause() {
        LogUtils.d(TAG, "releaseOnPause");
        if (audioManager != null) {
            //audioManager.release();会停止player播放器并设置listener为null
            audioManager.release();
        }
    }

    public void releaseOnDestory() {
        LogUtils.d(TAG, "releaseonDestory");
        if (audioManager != null) {
            //audioManager.release();会停止player播放器并设置listener为null
            audioManager.release();
            if (audioPlayer != null) {
                audioPlayer = null;
            }
            audioManager = null;
        }
        commOnPlayListener = null;
        commOnRecordCallback = null;
        if (null != deleteAudioFileRunnable) {
            ThreadUtil.getThreadPool().execute(deleteAudioFileRunnable);
            deleteAudioFileRunnable = null;
        }

    }

    /**
     * 删除上次的录音文件
     */
    private void deleteLastRecord() {
        LogUtils.d(TAG, "deleteLastRecord");

        if (null != audioFile && audioFile.exists()) {
            if (null == deleteAudioFileRunnable) {
                deleteAudioFileRunnable = new Runnable() {
                    @Override
                    public void run() {
                        audioFile.delete();
                        audioFile = null;
                    }
                };
            }
            ThreadUtil.getThreadPool().cancel(deleteAudioFileRunnable);
        }
    }

}
