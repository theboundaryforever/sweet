package com.yuhuankj.tmxq.ui.audio.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.netease.nimlib.sdk.StatusCode;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomKickOutEvent;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.TimeUtils;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_core.im.login.IIMLoginClient;
import com.tongdaxing.xchat_core.music.IMusicDownloaderCoreClient;
import com.tongdaxing.xchat_core.music.bean.HotMusicInfo;
import com.tongdaxing.xchat_core.player.IPlayerCore;
import com.tongdaxing.xchat_core.player.IPlayerCoreClient;
import com.tongdaxing.xchat_core.player.bean.LocalMusicInfo;
import com.tongdaxing.xchat_core.room.IRoomCoreClient;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.databinding.ActivityMusicLibraryBinding;
import com.yuhuankj.tmxq.ui.audio.adapter.MusicLibraryAdapter;
import com.yuhuankj.tmxq.ui.audio.adapter.MusicLibraryMagicIndicatorAdapter;
import com.yuhuankj.tmxq.ui.widget.VoiceSeekDialog;
import com.yuhuankj.tmxq.widget.magicindicator.ViewPagerHelper;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.UIUtil;
import com.yuhuankj.tmxq.widget.magicindicator.buildins.commonnavigator.CommonNavigator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author chenran
 * @date 2017/10/28
 */
@CreatePresenter(MusicLibraryPresenter.class)
public class MusicLibraryActivity extends BaseMvpActivity<MusicLibraryView, MusicLibraryPresenter>
        implements MusicLibraryView,View.OnClickListener,
        SeekBar.OnSeekBarChangeListener, MusicLibraryMagicIndicatorAdapter.OnItemSelectListener {

    private final String TAG = MusicLibraryActivity.class.getSimpleName();

    private String imgBgUrl;
    ActivityMusicLibraryBinding musicLibraryBinding;

    private LocalMusicInfo currMusicInfo;
    private List<TabInfo> mTabInfoList;

    public static void start(Context context, String imgBgUrl) {
        Intent intent = new Intent(context, MusicLibraryActivity.class);
        intent.putExtra("imgBgUrl", imgBgUrl);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        musicLibraryBinding = DataBindingUtil.setContentView(this,R.layout.activity_music_library);
        imgBgUrl = getIntent().getStringExtra("imgBgUrl");
        musicLibraryBinding.setClick(this);

        View content = findViewById(android.R.id.content);
        ViewGroup.LayoutParams params = content.getLayoutParams();
        params.height = getResources().getDisplayMetrics().heightPixels;

        initMusicPlayControlView();

        currMusicInfo = CoreManager.getCore(IPlayerCore.class).getCurrLocalMusicInfo();
        updateMusicPlayView();
        initTabIndicator();
        musicLibraryBinding.vpMusicList.setAdapter(new MusicLibraryAdapter(getSupportFragmentManager(), mTabInfoList));
        musicLibraryBinding.vpMusicList.setOffscreenPageLimit(2);
        ViewPagerHelper.bind(musicLibraryBinding.miInd, musicLibraryBinding.vpMusicList);
    }

    private void initTabIndicator(){
        mTabInfoList = new ArrayList<>();
        mTabInfoList.add(0,new TabInfo(-2,getString(R.string.music_list_tab_hot)));
        mTabInfoList.add(0,new TabInfo(-1,getString(R.string.music_list_tab_mine)));
        CommonNavigator commonNavigator = new CommonNavigator(this);
        MusicLibraryMagicIndicatorAdapter magicIndicatorAdapter = new MusicLibraryMagicIndicatorAdapter(this,
                mTabInfoList, 0,UIUtil.dip2px(this, 4),UIUtil.dip2px(this, 19));
        magicIndicatorAdapter.setOnItemSelectListener(this);
        commonNavigator.setAdapter(magicIndicatorAdapter);
        musicLibraryBinding.miInd.setNavigator(commonNavigator);
        LinearLayout titleContainer = commonNavigator.getTitleContainer();
        titleContainer.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.BOTTOM);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_report:
            case R.id.fl_report:
                try {
                    long songId = Long.valueOf(currMusicInfo.getSongId());
                    if(null != currMusicInfo && songId>0L){
                        ReportMusicActivity.start(this,songId);
                    }
                } catch (NumberFormatException | IllegalStateException nfe) {
                    nfe.printStackTrace();
                }
                break;
            case R.id.fl_musicVoice:
                VoiceSeekDialog voiceSeekDialog = new VoiceSeekDialog(this);
                voiceSeekDialog.show();
                break;
            case R.id.ll_uploadMusic:
                getDialogManager().showOkCancelDialog(getResources().getString(R.string.music_library_menu_upload_tips),
                        getResources().getString(R.string.sure),null,null);
                musicLibraryBinding.llMenu.setVisibility(View.GONE);
                break;
            case R.id.ll_localMusic:
                LocalMusicListActivity.start(this,imgBgUrl);
                musicLibraryBinding.llMenu.setVisibility(View.GONE);
                break;
            case R.id.ll_myUpload:
                MyUploadMusicListActivity.start(this,imgBgUrl);
                musicLibraryBinding.llMenu.setVisibility(View.GONE);
                break;
            case R.id.fl_menu:
                boolean hasShow = musicLibraryBinding.llMenu.getVisibility() == View.VISIBLE;
                musicLibraryBinding.llMenu.setVisibility(hasShow ? View.GONE : View.VISIBLE);
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_musicPlayStatus:
                int state = CoreManager.getCore(IPlayerCore.class).getState();
                if (state == IPlayerCore.STATE_PLAY) {
                    CoreManager.getCore(IPlayerCore.class).pause();
                } else if (state == IPlayerCore.STATE_PAUSE) {
                    int result = CoreManager.getCore(IPlayerCore.class).play(currMusicInfo);
                    if (result < 0) {
                        toast(R.string.music_play_failed_file_format_error);
                    }
                } else {
                    int result = CoreManager.getCore(IPlayerCore.class).play(null);
                    if (result < 0) {
                        if (result == IPlayerCore.PLAY_STATUS_MUSIC_LIST_EMPTY) {
                            toast(R.string.music_play_failed_list_empty);
                        } else {
                            toast(R.string.music_play_failed_file_format_error);
                        }
                    }
                }
                break;
            default:
        }
    }

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onBeKickOut(ChatRoomKickOutEvent.ChatRoomKickOutReason reason) {
        finish();
    }

    @CoreEvent(coreClientClass = IIMLoginClient.class)
    public void onKickedOut(StatusCode code) {
        finish();
    }

    @Override
    public void onItemSelect(int position) {
        musicLibraryBinding.vpMusicList.setCurrentItem(position);
    }

//==============================音乐播放控制面板===================================

    private View fl_report;
    private View tv_report;
    private View fl_musicVoice;
    private View rl_musicPlayControl;
    private TextView tv_musicName;
    private ImageView iv_musicPlayStatus;
    private SeekBar sb_musicPlayProgress;
    private TextView tv_totalPlayTime;
    private TextView tv_currPlayTime;

    private boolean isSBProgressChangedByUserTouch = false;

    private void initMusicPlayControlView() {
        tv_musicName = (TextView) findViewById(R.id.tv_musicName);
        tv_totalPlayTime = (TextView) findViewById(R.id.tv_totalPlayTime);
        tv_currPlayTime = (TextView) findViewById(R.id.tv_currPlayTime);
        fl_report = findViewById(R.id.fl_report);
        tv_report = findViewById(R.id.tv_report);
        rl_musicPlayControl = findViewById(R.id.rl_musicPlayControl);
        fl_musicVoice = findViewById(R.id.fl_musicVoice);
        iv_musicPlayStatus = (ImageView) findViewById(R.id.iv_musicPlayStatus);
        sb_musicPlayProgress = (SeekBar) findViewById(R.id.sb_musicPlayProgress);
        sb_musicPlayProgress.setMax(100);
        sb_musicPlayProgress.setOnSeekBarChangeListener(this);
        iv_musicPlayStatus.setOnClickListener(this);
        tv_report.setOnClickListener(this);
        fl_report.setOnClickListener(this);
        fl_musicVoice.setOnClickListener(this);
    }

    private void updateMusicPlayView() {
        String dur = null;
        if (currMusicInfo != null && CoreManager.getCore(IPlayerCore.class).getState() != IPlayerCore.STATE_STOP) {
            rl_musicPlayControl.setVisibility(View.VISIBLE);
            long songId = 0;
            try {
                songId = Long.valueOf(currMusicInfo.getSongId());
            }catch (NumberFormatException nfe){
                nfe.printStackTrace();
            }catch (IllegalStateException ise){
                ise.printStackTrace();
            }
            fl_report.setVisibility(songId>0 ? View.VISIBLE : View.INVISIBLE);
            tv_musicName.setText(currMusicInfo.getSongName());
            dur = TimeUtils.getFormatTimeString(currMusicInfo.getDuration(), "min:sec");
            int state = CoreManager.getCore(IPlayerCore.class).getState();
            if (state == IPlayerCore.STATE_PLAY) {
                iv_musicPlayStatus.setImageResource(R.mipmap.icon_music_pause);
            } else {
                iv_musicPlayStatus.setImageResource(R.mipmap.icon_music_play);
            }
        } else {
            tv_musicName.setText(getResources().getString(R.string.music_play_empty));
            dur = TimeUtils.getFormatTimeString(0, "min:sec");
            iv_musicPlayStatus.setImageResource(R.mipmap.icon_music_play);
            fl_report.setVisibility(View.INVISIBLE);
            rl_musicPlayControl.setVisibility(View.GONE);
        }
        tv_totalPlayTime.setText(getResources().getString(R.string.music_play_control_dur,dur));
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicProgressUpdate(long total, long current) {
        int progress = (int) (current*100/total);
        LogUtils.d(TAG, "onMusicProgressUpdate-total:" + total + " current:" + current + " progress:" + progress + " isSBProgressChangedByUserTouch:" + isSBProgressChangedByUserTouch);
        if(isSBProgressChangedByUserTouch){
            return;
        }
        tv_currPlayTime.setText(TimeUtils.getFormatTimeString(current, "min:sec"));
        tv_totalPlayTime.setText(getResources().getString(R.string.music_play_control_dur,TimeUtils.getFormatTimeString(total, "min:sec")));
        sb_musicPlayProgress.setProgress(progress);
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicPlaying(LocalMusicInfo localMusicInfo) {
        this.currMusicInfo = localMusicInfo;
        updateMusicPlayView();
        getMvpPresenter().reportMusicPlay(localMusicInfo);
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicPause(LocalMusicInfo localMusicInfo) {
        this.currMusicInfo = localMusicInfo;
        updateMusicPlayView();
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicStop() {
        updateMusicPlayView();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        isSBProgressChangedByUserTouch = true;
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        CoreManager.getCore(IPlayerCore.class).setAudioMixCurrPosition(seekBar.getProgress());
        isSBProgressChangedByUserTouch = false;
    }

    @CoreEvent(coreClientClass = IMusicDownloaderCoreClient.class)
    public void onHotMusicDownloadError(String msg, LocalMusicInfo localMusicInfo, HotMusicInfo hotMusicInfo) {
        toast(msg);
    }

    @Override
    public void onMusicPlayReport(boolean isSuccess, String msg) {

    }

    @Override
    public boolean isStatusBarDarkFont() {
        return false;
    }
}
