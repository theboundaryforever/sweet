package com.yuhuankj.tmxq.ui.widget.address;

import android.content.Context;
import android.content.res.AssetManager;
import android.text.TextUtils;

import com.tongdaxing.erban.libcommon.utils.LogUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

/**
 * @author liaoxy
 * @Description:地区助手类
 * @date 2019/1/24 11:22
 */
public class AddressHelper {
    private AssetManager assetManager;
    private List<Province> provinces = new ArrayList<>();
    private List<City> allCities = new ArrayList<>();
    private Context context;

    public static AddressHelper getInstance(@Nonnull Context context) {
        AddressHelper instance = Builder.INSTANCE;
        if (instance.getProvinces() == null || instance.getProvinces().size() == 0) {
            //用全局Context避免内存泄露
            instance.context = context.getApplicationContext();
            instance.parseAddress();
        }
        return instance;
    }

    //静态内部类实现单例模式
    private static final class Builder {
        private static final AddressHelper INSTANCE = new AddressHelper();
    }

    //解析asserts目录下放的地区文件
    //用SDK自带的JSON解析，不依赖其它第三个解析，提高耦合性
    private void parseAddress() {
        if (assetManager == null) {
            assetManager = context.getAssets();
        }
        StringBuilder newstringBuilder = new StringBuilder();
        InputStream inputStream = null;
        InputStreamReader isr = null;
        BufferedReader reader = null;
        try {
            inputStream = assetManager.open("address.json");
            isr = new InputStreamReader(inputStream);
            reader = new BufferedReader(isr);
            String jsonLine;
            while ((jsonLine = reader.readLine()) != null) {
                newstringBuilder.append(jsonLine);
            }
            reader.close();
            isr.close();
            inputStream.close();

            String result = newstringBuilder.toString();
            LogUtils.d("json", result);
            JSONArray jsonArray = new JSONArray(result);
            int provinceId;
            int cityId;
            int areaId;
            int level;
            String provinceName;
            String cityName;
            String areaName;
            List<City> cities;
            List<Area> areas;
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObj = jsonArray.getJSONObject(i);
                provinceId = jsonObj.getInt("id");
                level = jsonObj.getInt("level");
                provinceName = jsonObj.getString("province_name");
                Province province = new Province();
                province.setId(provinceId);
                province.setLevel(level);
                province.setParentId(0);
                province.setProvinceName(provinceName);

                JSONArray jsonCityArray = jsonObj.getJSONArray("citys");
                cities = new ArrayList<>();
                for (int j = 0; j < jsonCityArray.length(); j++) {
                    JSONObject jsonCityObj = jsonCityArray.getJSONObject(j);
                    cityId = jsonCityObj.getInt("id");
                    level = jsonCityObj.getInt("level");
                    cityName = jsonCityObj.getString("city_name");
                    City city = new City();
                    city.setId(cityId);
                    city.setLevel(level);
                    city.setParentId(provinceId);
                    city.setCityName(cityName);

                    areas = new ArrayList<>();
                    JSONArray jsonAreaArray = jsonCityObj.getJSONArray("countys");
                    for (int k = 0; k < jsonAreaArray.length(); k++) {
                        JSONObject jsonAreaObj = jsonAreaArray.getJSONObject(k);
                        areaId = jsonAreaObj.getInt("id");
                        level = jsonAreaObj.getInt("level");
                        areaName = jsonAreaObj.getString("county_name");
                        Area area = new Area();
                        area.setId(areaId);
                        area.setLevel(level);
                        area.setParentId(cityId);
                        area.setCounty_name(areaName);
                        areas.add(area);
                    }
                    city.setCountys(areas);
                    cities.add(city);
                }
                province.setCitys(cities);
                provinces.add(province);
                allCities.addAll(cities);
            }
        } catch (Exception e) {
            try {
                if (reader != null) {
                    reader.close();
                }
                if (isr != null) {
                    isr.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            provinces.clear();
            allCities.clear();
            context = null;
            e.printStackTrace();
        }
    }

    //获取所有省份
    public List<Province> getProvinces() {
        return provinces;
    }

    //获取所有城市
    public List<City> getAllCities() {
        return allCities;
    }

    //根据省ID获取该省所有城市
    public List<City> getCitiesByProvince(int provinceId) {
        if (provinces == null || provinces.size() == 0) {
            return null;
        }
        for (Province province : provinces) {
            if (province.getId() == provinceId) {
                return province.getCitys();
            }
        }
        return null;
    }

    //根据省名称获取该省所有城市
    public List<City> getCitiesByProvince(@Nonnull String provinceName) {
        if (provinces == null || provinces.size() == 0 || TextUtils.isEmpty(provinceName)) {
            return null;
        }
        for (Province province : provinces) {
            if (!TextUtils.isEmpty(province.getProvinceName()) && province.getProvinceName().equals(provinceName)) {
                return province.getCitys();
            }
        }
        return null;
    }

    //地区实体类
    public abstract static class Address implements Serializable {
        private int id;
        private int parentId;
        private int level;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Address address = (Address) o;

            return id == address.id;
        }

        @Override
        public int hashCode() {
            return id;
        }

        public int getParentId() {
            return parentId;
        }

        public void setParentId(int parentId) {
            this.parentId = parentId;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getLevel() {
            return level;
        }

        public void setLevel(int level) {
            this.level = level;
        }
    }

    public static class Province extends Address {
        private String provinceName;
        private List<City> citys = new ArrayList<>();

        public List<City> getCitys() {
            return citys;
        }

        public void setCitys(List<City> citys) {
            this.citys = citys;
        }

        public String getProvinceName() {
            return provinceName;
        }

        public void setProvinceName(String provinceName) {
            this.provinceName = provinceName;
        }
    }

    public static class City extends Address {
        private String cityName;
        private List<Area> countys = new ArrayList<>();

        public List<Area> getCountys() {
            return countys;
        }

        public void setCountys(List<Area> countys) {
            this.countys = countys;
        }

        public String getCityName() {
            return cityName;
        }

        public void setCityName(String cityName) {
            this.cityName = cityName;
        }
    }

    public static class Area extends Address {
        private String county_name;

        public String getCounty_name() {
            return county_name;
        }

        public void setCounty_name(String county_name) {
            this.county_name = county_name;
        }
    }
}
