package com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.base.AbstractMvpRelativeLayout;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomEvent;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.yuhuankj.tmxq.R;

import io.reactivex.disposables.Disposable;

/**
 * 创建者      魏海涛
 * 创建时间    2019年4月25日 18:12:44
 * 描述        房间左上角召集令view
 * <p>
 * <p>
 * 1.初始化状态，RoomDataManager.conveneUserCount值是否为零,为0展示默认召集令状态，不为0展示已召集人数状态
 * 2.RoomDataManager.conveneUserCount,在进房、退房两部分操作时，置为0
 * 3.ImRoomMessageMaanger中，接收到对应通知事件时，更新该值，刷新界面
 *
 * @author dell
 */
public class RoomCallUpView extends AbstractMvpRelativeLayout implements IMvpBaseView {

    private final String TAG = RoomCallUpView.class.getSimpleName();

    private TextView tvCallUpTips;
    private Disposable subscribe;
    private OnRoomCallUpViewClickListener onRoomCallUpViewClickListener;

    public RoomCallUpView(Context context) {
        this(context, null);
    }

    public RoomCallUpView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoomCallUpView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getViewLayoutId() {
        return R.layout.layout_room_call_up;
    }

    @Override
    public void initialView(Context context) {
        tvCallUpTips = findViewById(R.id.tvCallUpTips);
    }

    public void initCallUpStatus() {
        if (!GlideContextCheckUtil.checkContextUsable(getContext())) {
            return;
        }
        //召集令已发布且仍旧有效状态
        if (RoomDataManager.get().conveneState == 1) {
            String roomCallUpNumStr = String.valueOf(RoomDataManager.get().conveneUserCount);
            String tips = getContext().getResources().getString(R.string.room_call_up_tips_2, roomCallUpNumStr);
            SpannableStringBuilder ssb = new SpannableStringBuilder(tips);
            int firstIndex = tips.indexOf(roomCallUpNumStr);
            ssb.setSpan(new ForegroundColorSpan(Color.WHITE), 0, firstIndex, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#FFF04C")),
                    firstIndex, firstIndex + roomCallUpNumStr.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            ssb.setSpan(new ForegroundColorSpan(Color.WHITE),
                    firstIndex + roomCallUpNumStr.length(), tips.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            tvCallUpTips.setText(ssb);
        } else {
            tvCallUpTips.setText(getContext().getResources().getString(R.string.room_call_up_tips_1));
            tvCallUpTips.setTextColor(Color.WHITE);
            setVisibility(RoomDataManager.get().isRoomOwner()
                    && null != RoomDataManager.get().getCurrentRoomInfo()
                    && RoomDataManager.get().getCurrentRoomInfo().showCallUp() ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    protected void initListener() {
        LogUtils.d(TAG, "initListener");
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtils.d(TAG, "initListener-->onClick conveneState:" + RoomDataManager.get().conveneState);
                if (null != onRoomCallUpViewClickListener) {
                    onRoomCallUpViewClickListener.onRoomCallUpViewClick();
                }
            }
        });
    }

    @Override
    protected void initViewState() {
        LogUtils.d(TAG, "initViewState");
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        LogUtils.d(TAG, "onAttachedToWindow");
        subscribe = IMRoomMessageManager.get().getIMRoomEventObservable().subscribe(this::onReceiveRoomEvent);
    }

    private void onReceiveRoomEvent(IMRoomEvent roomEvent) {
        if (roomEvent == null || roomEvent.getEvent() == RoomEvent.NONE) {
            return;
        }
        switch (roomEvent.getEvent()) {
            case IMRoomEvent.ROOM_CALL_UP_NUM_NOTIFY:
                LogUtils.d(TAG, "onReceiveRoomEvent-召集令进房人数更新通知，切换界面显示状态");
                initCallUpStatus();
                break;
            default:
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        LogUtils.d(TAG, "onDetachedFromWindow");
        release();
    }

    public void release() {
        LogUtils.d(TAG, "release");
        setOnClickListener(null);
        onRoomCallUpViewClickListener = null;
        if (subscribe != null) {
            subscribe.dispose();
            subscribe = null;
        }
    }

    public void setOnRoomCallUpViewClickListener(OnRoomCallUpViewClickListener listener) {
        this.onRoomCallUpViewClickListener = listener;
    }

    public interface OnRoomCallUpViewClickListener {

        void onRoomCallUpViewClick();
    }

}
