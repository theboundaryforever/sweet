package com.yuhuankj.tmxq.ui.audio.activity;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

public interface MusicReportView<T extends AbstractMvpPresenter> extends IMvpBaseView {
    void onMusicReport(boolean isSuccess, String msg);
}
