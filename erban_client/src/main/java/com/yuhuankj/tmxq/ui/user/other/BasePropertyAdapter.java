package com.yuhuankj.tmxq.ui.user.other;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.user.bean.GiftWallInfo;
import com.yuhuankj.tmxq.R;

import java.util.List;

public abstract class BasePropertyAdapter extends RecyclerView.Adapter<BasePropertyAdapter.GiftWallHolder>  {


    public abstract void setGiftWallInfoList(List<GiftWallInfo> giftWallInfoList);

    public abstract void setJsonData(List<Json> data);

    public class GiftWallHolder extends RecyclerView.ViewHolder {
        public ImageView giftPic;
        public TextView giftName;
        public TextView giftNum;

        public GiftWallHolder(View itemView) {
            super(itemView);
            giftPic = itemView.findViewById(R.id.gift_img);
            giftName = itemView.findViewById(R.id.gift_name);
            giftNum = itemView.findViewById(R.id.gift_num);
        }

    }
}
