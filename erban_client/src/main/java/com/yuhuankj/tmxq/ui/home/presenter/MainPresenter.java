package com.yuhuankj.tmxq.ui.home.presenter;

import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nim.uikit.session.emoji.SerEmojiTopicDataManager;
import com.netease.nimlib.sdk.util.Entry;
import com.tencent.bugly.crashreport.BuglyLog;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.auth.RealNameAuthStatusChecker;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.initial.InitInfo;
import com.tongdaxing.xchat_core.initial.InitModel;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.tongdaxing.xchat_core.liveroom.im.model.ReUsedSocketManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomAdmireTimeCounter;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomOwnerLiveTimeCounter;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RtcEngineManager;
import com.tongdaxing.xchat_core.result.RoomResult;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.face.IFaceCore;
import com.tongdaxing.xchat_core.room.model.AvRoomModel;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.ui.home.game.GameHomeModel;
import com.yuhuankj.tmxq.ui.home.model.MainModel;
import com.yuhuankj.tmxq.ui.home.view.IMainView;
import com.yuhuankj.tmxq.ui.message.UnreadMsgResult;
import com.yuhuankj.tmxq.ui.message.emoji.ServerEmojiListRespon;
import com.yuhuankj.tmxq.ui.signAward.model.SignAwardResponse;
import com.yuhuankj.tmxq.ui.signAward.model.SignInModel;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;


/**
 * <p>  </p>
 *
 * @author jiahui
 * @date 2017/12/12
 */
public class MainPresenter extends AbstractMvpPresenter<IMainView> implements InitModel.OnInitResultListener {

    private final String TAG = MainPresenter.class.getSimpleName();

    private final AvRoomModel mAvRoomMode;
    private final MainModel mainModel;
    private SignInModel signInModel;

    public MainPresenter() {
        mAvRoomMode = new AvRoomModel();
        mainModel = new MainModel();
        signInModel = new SignInModel();
    }

    public void getUnreadMsgCount(OkHttpManager.MyCallBack<ServiceResult<UnreadMsgResult>> myCallBack) {
        new GameHomeModel().getUnreadMsgCount(myCallBack);
    }

    /**
     * 获取萌新大礼包和签到奖励信息
     */
    public void getNewsBigGift() {

        signInModel.getNewsBigGift(new HttpRequestCallBack<SignAwardResponse>() {

            @Override
            public void onSuccess(String message, SignAwardResponse response) {
                //1.先判断走哪个业务流程
                if (null != response) {
                    if (null != response.getNewsBigGiftMap() && response.getNewsBigGiftMap().size() > 0) {
                        //走萌新大礼包流程
                        getMvpView().showNewPkgDialog(response.getGetDay(), response.getMicRoomUserList(),
                                response.getNewsBigGiftMap());
                    } else if(null != response.getSignInList() && response.getSignInList().size() > 0) {
                        //走连续签到奖励逻辑
                        getMvpView().showSignRecvDialog(response.getSignDay(), response.getSignInList());
                    }
                }
            }

            @Override
            public void onFailure(int code, String msg) {

            }
        });
    }

    private boolean isCreatingRoom = false;

    //检测多人轰趴房间麦序状态
    public Disposable checkMicType(String roomId, final RoomQueueInfo queueInfo, int roomType) {
        Consumer consumer = new Consumer<List<Entry<String, String>>>() {
            @Override
            public void accept(List<Entry<String, String>> entries) throws Exception {

                boolean onMicBefore = queueInfo != null && queueInfo.mChatRoomMember != null
                        && queueInfo.mRoomMicInfo != null;
                boolean needUpMic = true;

                int roomMicInfoPosition = -100;
                if (queueInfo != null && queueInfo.mRoomMicInfo != null) {
                    roomMicInfoPosition = queueInfo.mRoomMicInfo.getPosition();
                }

                if (!ListUtils.isListEmpty(entries)) {
                    for (Entry<String, String> entry : entries) {
                        if (entry.key != null && entry.key.length() > 2) {
                            continue;
                        }

                        //判断是否有别人在麦位上
                        if ((roomMicInfoPosition + "").equals(entry.key)) {
                            String oldQueueUid = queueInfo.mChatRoomMember.getAccount() + "";
                            String queueUid = Json.parse(entry.value).str("uid");
                            if (!oldQueueUid.equals(queueUid)) {
                                needUpMic = false;
                            }
                        }
                    }
                }

                LogUtils.d("micInListLogFetchQueue2", onMicBefore + "   " + needUpMic);
                if (queueInfo == null || queueInfo.mChatRoomMember == null || queueInfo.mRoomMicInfo == null) {
                    return;
                }
                //之前在轰趴房麦上
                RoomQueueInfo roomQueueInfo = AvRoomDataManager.get()
                        .getRoomQueueMemberInfoByMicPosition(queueInfo.mRoomMicInfo.getPosition());
                //麦上没人
                String account = queueInfo.mChatRoomMember.getAccount();
                if (null == roomQueueInfo) {
                    return;
                }
                if (roomQueueInfo.mChatRoomMember == null ||
                        Objects.equals(account, roomQueueInfo.mChatRoomMember.getAccount())) {
                    //自己在麦上，移除掉
                    if (needUpMic) {
                        roomQueueInfo.mChatRoomMember = null;
                        // 通知界面更新麦序信息
                        IMNetEaseManager.get().noticeDownMic(queueInfo.mRoomMicInfo.getPosition() + "", account);
                    }
                    if (AvRoomDataManager.get().isOwner(account)) {
                        // 更新声网闭麦 ,发送状态信息
                        RtcEngineManager.get().setMute(true);
                        AvRoomDataManager.get().mIsNeedOpenMic = false;
                    }
                }
            }
        };
        return mAvRoomMode.queryRoomMicInfo(roomId).delay(1, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread()).subscribe(consumer);
    }

    public void getRecommAdInfo() {
        mainModel.getRecommAdInfo(new OkHttpManager.MyCallBack<ServiceResult<String>>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(ServiceResult<String> response) {
                if (200 == response.getCode() && null != getMvpView()) {
                    getMvpView().onGetRecommAd(response.getData());
                }
            }
        });
    }

    public void exitRoom() {
        CallBack exitHomePartCallBack = new CallBack<String>() {
            @Override
            public void onSuccess(String data) {
                if (getMvpView() != null) {
                    LogUtil.d(ReUsedSocketManager.TAG, "exitRoom->onSuccess data:" + data);
                    BuglyLog.d(TAG, "exitRoom->onSuccess-->onRoomExited");
                    getMvpView().onRoomExited();
                }
            }

            @Override
            public void onFail(int code, String error) {
                if (getMvpView() != null) {
                    LogUtil.d(ReUsedSocketManager.TAG, "exitRoom->onFail code:" + code + " error:" + error);
                    BuglyLog.d(TAG, "exitRoom->onFail-->onRoomExited");
                    getMvpView().onRoomExited();
                }
            }
        };
        BaseRoomServiceScheduler.exitRoom(exitHomePartCallBack);
        //退出房间，暂时可以不判断是否是房主，直接关闭即时
        RoomOwnerLiveTimeCounter.getInstance().release();
        RoomAdmireTimeCounter.getInstance().release();
    }

    /**
     * 同步当前在线状态至服务器
     */
    public void syncOnLineStatus() {
        mainModel.syncOnLineStatus(new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Json json) {

            }
        });
    }

    public boolean isCreatingRoom() {
        return isCreatingRoom;
    }

    public void createHomePartRoom(long queryUid, int roomType) {
        LogUtils.d(TAG, "createHomePartRoom-queryUid:" + queryUid + " roomType:" + roomType);
        if (isCreatingRoom) {
            return;
        }
        isCreatingRoom = true;
        OkHttpManager.MyCallBack<ServiceResult<RoomInfo>> myCallBack = new OkHttpManager.MyCallBack<ServiceResult<RoomInfo>>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                isCreatingRoom = false;
                if (null != getMvpView()) {
                    getMvpView().notifyHomePartRoomCreateFailed(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<RoomInfo> response) {
                if (null == getMvpView()) {
                    isCreatingRoom = false;
                    return;
                }

                if (null == response) {
                    //创建房间失败，提示用户，隐藏loading
                    isCreatingRoom = false;
                    getMvpView().notifyHomePartRoomCreateFailed(null);
                    return;
                }

                if (response.isSuccess()) {
                    UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
                    boolean hasInSelfRoom = null != BaseRoomServiceScheduler.getCurrentRoomInfo()
                            && BaseRoomServiceScheduler.getCurrentRoomInfo().getUid() == userInfo.getUid();
                    if (null != userInfo && !userInfo.isRealNameAudit() && null != response.getData()
                            && !hasInSelfRoom) {
                        LogUtils.d(TAG, "createHomePartRoom 本地缓存数据判断，未实名认证，弹框提示");
                        isCreatingRoom = false;
                        getMvpView().notifyHomePartRoomNeedRealNameAuth(queryUid,
                                response.getData().getType(), response.getData());
                        return;
                    }
                    checkRoomUsable(userInfo.getUid(), response.getData());
                } else {
                    isCreatingRoom = false;
                    if (response.getCode() == RealNameAuthStatusChecker.RESULT_FAILED_NEED_REAL_NAME_AUTH) {
                        getMvpView().notifyHomePartRoomNeedRealNameAuth(queryUid,
                                null != response.getData() ? response.getData().getType()
                                        : RoomInfo.ROOMTYPE_HOME_PARTY, response.getData());
                    } else {
                        getMvpView().notifyHomePartRoomCreateFailed(response.getMessage());
                    }

                }
            }
        };
        mAvRoomMode.requestRoomInfoFromService(String.valueOf(queryUid), roomType, myCallBack);
    }

    public void checkRoomUsable(long queryUid, RoomInfo roomInfo) {
        if (null == roomInfo) {
            openRoom(queryUid, RoomInfo.ROOMTYPE_HOME_PARTY);
            return;
        }
        boolean valid = roomInfo.isValid();
        long uid = roomInfo.getUid();
        //如果不是默认房间类型要先修改数据类型
        LogUtils.d(TAG, "checkRoomUsable-roomType:" + roomInfo.getType());
        if (!valid) {
            //服务器会做定时器校验，20分钟内房间没人，就会关闭，需要客户端重新打开
            openRoom(uid, roomInfo.getType());
            return;
        }

        if (roomInfo.getType() == RoomInfo.ROOMTYPE_HOME_PARTY) {
            isCreatingRoom = false;
            getMvpView().enterHomePartRoom(uid);
            return;
        }
        closeRoomInfo(uid, roomInfo.getType());
    }

    private void closeRoomInfo(long uid, int roomType) {
        mAvRoomMode.closeRoomInfo(uid, roomType, new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                isCreatingRoom = false;
                if (null != getMvpView()) {
                    getMvpView().notifyHomePartRoomCreateFailed(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (null == getMvpView()) {
                    isCreatingRoom = false;
                    return;
                }

                if (null == response) {
                    isCreatingRoom = false;
                    getMvpView().notifyHomePartRoomCreateFailed(null);
                    return;
                }

                if (response.isSuccess()) {
                    openRoom(uid, RoomInfo.ROOMTYPE_HOME_PARTY);
                } else {
                    isCreatingRoom = false;
                    getMvpView().notifyHomePartRoomCreateFailed(response.getMessage());
                }
            }
        });
    }

    private void openRoom(final long uid, final int type) {
        mAvRoomMode.openRoom(uid, type, new OkHttpManager.MyCallBack<RoomResult>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                isCreatingRoom = false;
                if (null != getMvpView()) {
                    getMvpView().notifyHomePartRoomCreateFailed(e.getMessage());
                }
            }

            @Override
            public void onResponse(RoomResult response) {
                if (null == getMvpView()) {
                    isCreatingRoom = false;
                    return;
                }
                if (null == response) {
                    isCreatingRoom = false;
                    getMvpView().notifyHomePartRoomCreateFailed(null);
                    return;
                }
                if (response.isSuccess()) {
                    isCreatingRoom = false;
                    getMvpView().enterHomePartRoom(uid);
                } else {
                    if (response.getCode() == RealNameAuthStatusChecker.RESULT_FAILED_NEED_REAL_NAME_AUTH) {
                        //服务器已经去掉了openroom接口的实名认证逻辑，统一改为客户端控制
                        isCreatingRoom = false;
                        getMvpView().notifyHomePartRoomNeedRealNameAuth(uid, type, response.getData());
                    } else if (response.getCode() == 1500) {
                        //房间已经打开了, 需要关闭重新创建
                        closeRoomInfo(uid, type);
                    } else {
                        isCreatingRoom = false;
                        getMvpView().notifyHomePartRoomCreateFailed(response.getMessage());
                    }
                }
            }
        });
    }

    public void init(boolean force) {
        InitModel.get().registerInitResultListener(this);
        InitModel.get().init(force);
    }

    @Override
    public void onInitResult(ServiceResult<InitInfo> initResult, Throwable throwable) {
        if (initResult != null && initResult.getData() != null) {
            InitInfo data = initResult.getData();
            LogUtils.d(TAG, "init-accept data:" + data);
            // 表情
            if (data != null && data.getFaceJson() != null) {
                CoreManager.getCore(IFaceCore.class).onReceiveOnlineFaceJson(data.getFaceJson().getJson());
            }
            // 闪屏
            if (data != null && data.getSplashVo() != null && getMvpView() != null) {
                getMvpView().onInitSuccess(data);
            }
        }
        if (null != throwable) {
            throwable.printStackTrace();
        }
        InitModel.get().unregisterInitResultListener(MainPresenter.this);
    }

    @Override
    public void onDestroyPresenter() {
        InitModel.get().unregisterInitResultListener(this);
        super.onDestroyPresenter();
    }

    public void getEmojiListFromServer(long targetUid) {
        LogUtils.d(TAG, "getEmojiListFromServer targetUid:" + targetUid);

        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if (0L == uid) {
            return;
        }

        //初始化普通表情和火热表情列表的场景
        if (targetUid == uid) {
            //只要有一个列表数据不为空即表明已经有过初始化操作，即可忽略
            if (!ListUtils.isListEmpty(SerEmojiTopicDataManager.getInstance().serverNormalEmojiList)
                    || !ListUtils.isListEmpty(SerEmojiTopicDataManager.getInstance().serverHotEmojiList)) {
                return;
            }
        }

        mainModel.getEmojiListFromServer(targetUid, new HttpRequestCallBack<ServerEmojiListRespon>() {

            @Override
            public void onSuccess(String message, ServerEmojiListRespon response) {
                LogUtils.d(TAG, "getEmojiListFromServer-->onSuccess message:" + message);
                boolean showEmojiList = false;
                if (null != response) {
                    //先更新表情滑动栏数据
                    showEmojiList = !ListUtils.isListEmpty(response.slide);
                    if (showEmojiList) {
                        SerEmojiTopicDataManager.getInstance().refreshSlideEmojiList(response.slide);
                    }

                    //仅数据为空时才做更新处理
                    if (ListUtils.isListEmpty(SerEmojiTopicDataManager.getInstance().serverNormalEmojiList)
                            && !ListUtils.isListEmpty(response.normal)) {
                        SerEmojiTopicDataManager.getInstance().refreshNormalEmojiList(response.normal);
                    }
                    //仅数据为空时才做更新处理
                    if (ListUtils.isListEmpty(SerEmojiTopicDataManager.getInstance().serverHotEmojiList)
                            && !ListUtils.isListEmpty(response.hot)) {
                        SerEmojiTopicDataManager.getInstance().refreshHotEmojiList(response.hot);
                    }
                }
                SerEmojiTopicDataManager.getInstance().notifyEmojiListDataChanged(targetUid, showEmojiList);
            }

            @Override
            public void onFailure(int code, String msg) {
                LogUtils.d(TAG, "getEmojiListFromServer-->onFailure code:" + code + " msg:" + msg);
            }
        });
    }

    public void reportEmojiSent(long targetUid) {
        LogUtils.d(TAG, "reportEmojiSent targetUid:" + targetUid);

        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if (0L == uid) {
            return;
        }

        mainModel.reportEmojiSent(targetUid, new HttpRequestCallBack<Boolean>() {

            @Override
            public void onSuccess(String message, Boolean response) {
                LogUtils.d(TAG, "reportEmojiSent-->onSuccess message:" + message + " response:" + response);
            }

            @Override
            public void onFailure(int code, String msg) {
                LogUtils.d(TAG, "reportEmojiSent-->onFailure code:" + code + " msg:" + msg);
            }
        });
    }

    public void getRandomTopic(long targetUid) {
        LogUtils.d(TAG, "getRandomTopic targetUid:" + targetUid);

        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if (0L == uid) {
            return;
        }

        mainModel.getRandomTopic(targetUid, new HttpRequestCallBack<String>() {

            @Override
            public void onSuccess(String message, String response) {
                LogUtils.d(TAG, "getRandomTopic-->onSuccess message:" + message + " response:" + response);
                SerEmojiTopicDataManager.getInstance().notifyTopicDataUpdated(targetUid, response);
            }

            @Override
            public void onFailure(int code, String msg) {
                LogUtils.d(TAG, "getRandomTopic-->onFailure code:" + code + " msg:" + msg);
            }
        });
    }

}
