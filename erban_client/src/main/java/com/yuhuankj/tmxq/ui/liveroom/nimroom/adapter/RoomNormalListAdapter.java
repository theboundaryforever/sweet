package com.yuhuankj.tmxq.ui.liveroom.nimroom.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomMember;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.widget.SquareImageView;

import java.util.List;


/**
 * Created by chenran on 2017/10/11.
 */

public class RoomNormalListAdapter extends RecyclerView.Adapter<RoomNormalListAdapter.RoomNormalListHolder> implements View.OnClickListener{
    private Context context;
    private List<ChatRoomMember> normalList;
    private OnRoomNormalListOperationClickListener listOperationClickListener;

    public RoomNormalListAdapter(Context context) {
        this.context = context;
    }

    public void setNormalList(List<ChatRoomMember> normalList) {
        this.normalList = normalList;
    }

    public List<ChatRoomMember> getNormalList() {
        return normalList;
    }

    public void setListOperationClickListener(OnRoomNormalListOperationClickListener listOperationClickListener) {
        this.listOperationClickListener = listOperationClickListener;
    }

    @Override
    public RoomNormalListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_room_normal, parent, false);
        return new RoomNormalListHolder(view);
    }

    @Override
    public void onBindViewHolder(RoomNormalListHolder holder, int position) {
        ChatRoomMember chatRoomMember  = normalList.get(position);
        holder.nick.setText(chatRoomMember.getNick());
        holder.operationImg.setTag(chatRoomMember);
        holder.operationImg.setOnClickListener(this);
        ImageLoadUtils.loadSmallRoundBackground(context, chatRoomMember.getAvatar(), holder.avatar);
    }

    @Override
    public int getItemCount() {
        if (normalList == null) {
            return 0;
        } else {
            return normalList.size();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getTag() != null && v.getTag() instanceof ChatRoomMember) {
            ChatRoomMember chatRoomMember = (ChatRoomMember) v.getTag();
            if (listOperationClickListener != null) {
                listOperationClickListener.onRemoveOperationClick(chatRoomMember);
            }
        }
    }

    public class RoomNormalListHolder extends RecyclerView.ViewHolder {
        private SquareImageView avatar;
        private TextView nick;
        private TextView erbanNo;
        private ImageView operationImg;

        public RoomNormalListHolder(View itemView) {
            super(itemView);
            avatar = itemView.findViewById(R.id.avatar);
            nick = itemView.findViewById(R.id.nick);
            erbanNo = itemView.findViewById(R.id.erban_no);
            operationImg = itemView.findViewById(R.id.remove_opration);
        }
    }

    public interface OnRoomNormalListOperationClickListener {
        void onRemoveOperationClick(ChatRoomMember chatRoomMember);
    }
}
