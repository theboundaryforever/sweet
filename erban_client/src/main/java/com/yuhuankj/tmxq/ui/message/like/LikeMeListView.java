package com.yuhuankj.tmxq.ui.message.like;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

import java.util.List;

public interface LikeMeListView<T extends AbstractMvpPresenter> extends IMvpBaseView {

    void showLikeList(List<LikeEnitity> likeEnitities);

    void showLikeListEmptyView();


}
