package com.yuhuankj.tmxq.ui.search.ui.fragment;

import android.graphics.Color;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.erban.libcommon.utils.config.SpEvent;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.praise.IPraiseClient;
import com.tongdaxing.xchat_core.praise.IPraiseCore;
import com.tongdaxing.xchat_core.room.bean.SearchRoomPersonInfo;
import com.yanzhenjie.recyclerview.swipe.widget.DefaultItemDecoration;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.fragment.BaseMvpFragment;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.search.interfaces.SearchRoomView;
import com.yuhuankj.tmxq.ui.search.presenter.SearchRoomPresenter;
import com.yuhuankj.tmxq.ui.search.ui.adapter.SearchRecommendRoomAdapter;
import com.yuhuankj.tmxq.ui.search.ui.adapter.SearchRoomPersonAdapter;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;
import com.zhy.view.flowlayout.TagView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 搜索房间界面
 *
 * @author weihaitao
 * @date 2017/10/3
 */
@CreatePresenter(SearchRoomPresenter.class)
public class SearchRoomFragment extends BaseMvpFragment<SearchRoomView, SearchRoomPresenter> implements SearchRoomView, SearchRecommendRoomAdapter.OnSearchRecommendRoomItemClickListener, SwipeRefreshLayout.OnRefreshListener {

    private final String TAG = SearchRoomFragment.class.getSimpleName();
    //显示几个推荐热门房间
    private final int RECOMMEND_ROOM_SHOW_NUM = 9;
    private final int HISTORY_ROOM_SHOW_NUM = 10;

    private RecyclerView rv_searchRoom;
    private SearchRoomPersonAdapter searchRoomPersonAdapter;
    private SearchRecommendRoomAdapter searchRecommendAdapter;
    private TextView tv_search;
    private EditText searchEdit;
    private ImageView iv_inputDelete;
    private TextView tv_recomRoomTips;
    private TagFlowLayout tfl_searchHistory;
    private TagFlowLayout tfl_searchHot;
    private LayoutInflater mInflater;
    private TagAdapter<String> historyAdapter;
    private TagAdapter<String> hotAdapter;
    private Json searchHistoryCacheJson;
    private View ll_search_history;
    private View ll_search_hot;
    private View clearSearchHistory;

    private List<SearchRoomPersonInfo> recommendRoomList = new ArrayList<>();
    private String lastSearchContent = null;

    private View ll_searchEmpty;
    private SwipeRefreshLayout srl_refresh;
    List<String> searchHistory = new ArrayList<>();

    private List<String> hotTabs = new ArrayList<>();
    private int lastSearchHotPageNum = 1;

    @Override
    public void initiate() {

    }

    @Override
    public void onFindViews() {
        defaultItemDecoration = new DefaultItemDecoration(Color.WHITE,
                DisplayUtility.dp2px(getActivity(),10), 0, -1);
        rv_searchRoom = mView.findViewById(R.id.rv_searchRoom);
        tfl_searchHistory = mView.findViewById(R.id.tfl_searchHistory);
        tfl_searchHot = mView.findViewById(R.id.tfl_searchHot);
        tv_search = mView.findViewById(R.id.tv_search);
        tv_recomRoomTips = mView.findViewById(R.id.tv_recomRoomTips);
        iv_inputDelete = mView.findViewById(R.id.iv_inputDelete);
        ll_search_history = mView.findViewById(R.id.ll_search_history);
        ll_search_hot = mView.findViewById(R.id.ll_search_hot);
        clearSearchHistory = mView.findViewById(R.id.tv_searchClearHistory);
        searchEdit = mView.findViewById(R.id.search_edit);
        GrowingIO.getInstance().trackEditText(searchEdit);
        searchEdit.clearFocus();
        searchEdit.setHint(R.string.search_room_hint);
        searchEdit.setFilters(new InputFilter[]{enterBlankInputFilter});
        searchEdit.addTextChangedListener(textWatcher);
        searchRoomPersonAdapter = new SearchRoomPersonAdapter(getActivity());
        searchRecommendAdapter = new SearchRecommendRoomAdapter(getActivity());
        mInflater = LayoutInflater.from(getActivity());
        ll_searchEmpty = mView.findViewById(R.id.ll_searchEmpty);
        srl_refresh = mView.findViewById(R.id.srl_refresh);
        srl_refresh.setEnabled(true);
    }

    InputFilter enterBlankInputFilter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            if (source.equals(" ") || source.toString().contentEquals("\n")) {
                return "";
            } else {
                return null;
            }
        }
    };

    @Override
    public void onSetListener() {
        iv_inputDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchEdit.setText(null);
            }
        });
        srl_refresh.setOnRefreshListener(this);
        tv_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchRoom(searchEdit.getText().toString());
            }
        });
        clearSearchHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearSearchHistory();
            }
        });
        searchRoomPersonAdapter.setOnItemClick(new SearchRoomPersonAdapter.OnSearchAdapterItemClickListener() {
            @Override
            public void onItemClicked(SearchRoomPersonInfo homeRoom) {
                RoomServiceScheduler.getInstance().enterRoom(mContext, homeRoom.getUid(),
                        homeRoom.getType());
                //首页-搜索-搜索结果进入
                StatisticManager.get().onEvent(getActivity(),
                        StatisticModel.EVENT_ID_SEARCH_LIST_ROOM,
                        StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
            }
        });
        searchRecommendAdapter.setListener(this);
    }

    private void searchRoom(String content){
        LogUtils.d(TAG,"searchRoom-content:"+content);
        if (StringUtil.isEmpty(content)) {
            srl_refresh.setRefreshing(false);
        } else {
            if(null == lastSearchContent || !lastSearchContent.equals(content)){
                lastSearchContent = content;
                markSearchContent(content);
            }
            srl_refresh.setRefreshing(true);
            getMvpPresenter().searchRoom(content);
        }
    }

    /**
     * 数据懒加载
     */
    @Override
    public void loadLazyIfYouWant() {
        if(null == searchHistoryCacheJson){
            initSearchHistory();
            //这里加载热搜列表
            refreshSearchHotTag();
        }
        if(null == recommendRoomList || recommendRoomList.size() == 0){
            getMvpPresenter().refreshRecommendRoomList();
        }
    }

    /**
     * 搜索房间 关键字搜索结果返回
     *
     * @param success
     * @param message
     * @param list
     */
    @Override
    public void onRoomSearched(boolean success, String message, List list) {
        srl_refresh.setRefreshing(false);
        if(success){
            refreshSearchRoomList(list);
        }else{
            if(!TextUtils.isEmpty(message)){
                toast(message);
            }
        }
    }

    private void markSearchContent(String s) {
        if (TextUtils.isEmpty(s)) {
            return;
        }
        if(null == searchHistoryCacheJson){
            searchHistoryCacheJson = new Json();
        }
        String[] strings = searchHistoryCacheJson.key_names();
        if(null != strings && strings.length > HISTORY_ROOM_SHOW_NUM){
            searchHistoryCacheJson.remove(strings[0]);
        }
        searchHistoryCacheJson.set(s, "");
        SpUtils.put(getActivity(), SpEvent.search_room_history, searchHistoryCacheJson.toString());
//        refreshSearchHistory();
    }

    private void clearSearchHistory() {
        SpUtils.put(getActivity(), SpEvent.search_room_history, "");
        searchHistoryCacheJson = new Json();
        searchHistory.clear();
        refreshSearchHistory();
    }

    private void initSearchHistory() {
        String searchCache = (String) SpUtils.get(getActivity(), SpEvent.search_room_history, "");
        searchHistoryCacheJson = Json.parse(searchCache);
        refreshSearchHistory();
    }

    private void refreshSearchHistory() {
        if (null != searchHistoryCacheJson && !TextUtils.isEmpty(searchHistoryCacheJson.toString()) && null != searchHistoryCacheJson.key_names()) {
            for (String key : searchHistoryCacheJson.key_names()) {
                if(!searchHistory.contains(key)){
                    searchHistory.add(key);
                }
            }
        }
        Collections.reverse(searchHistory);
        historyAdapter = new TagAdapter<String>(searchHistory) {
            @Override
            public View getView(FlowLayout parent, int position, String s) {
                TextView tv = (TextView) mInflater.inflate(R.layout.tv_flowlayout_menu_search,
                        tfl_searchHistory, false);
                tv.setText(s);
                return tv;
            }

        };
        tfl_searchHistory.setAdapter(historyAdapter);
        tfl_searchHistory.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
            @Override
            public boolean onTagClick(View view, int position, FlowLayout parent) {
                searchEdit.setText(searchHistory.get(position));
                searchEdit.setSelection(searchEdit.getText().length());
                searchRoom(searchHistory.get(position));
                return false;
            }
        });
        boolean historyNotEmpty = null != searchHistory && searchHistory.size()>0;
        ll_search_history.setVisibility(historyNotEmpty ? View.VISIBLE : View.GONE);
    }

    private void refreshSearchHotTag() {
        hotTabs = Arrays.asList(getResources().getStringArray(R.array.search_room_tags));
        ll_search_hot.setVisibility(View.VISIBLE);
        if(null == mInflater){
            mInflater = LayoutInflater.from(getActivity());
        }
        hotAdapter = new TagAdapter<String>(hotTabs) {
            @Override
            public View getView(FlowLayout parent, int position, String tabName) {
                TextView tv = (TextView) mInflater.inflate(R.layout.tv_flowlayout_menu_search,
                        tfl_searchHistory, false);
                tv.setText(tabName);
                tv.setTag(tabName);
                return tv;
            }

        };
        tfl_searchHot.setAdapter(hotAdapter);
        tfl_searchHot.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
            @Override
            public boolean onTagClick(View view, int position, FlowLayout parent) {
                TagView tagView = (TagView)view;
                TextView textView = (TextView) tagView.getChildAt(0);
                String tabName = (String) textView.getTag();
                searchEdit.setText(tabName);
                searchEdit.setSelection(searchEdit.getText().length());
                lastSearchHotPageNum = 1;
                searchRoom(tabName);
                return false;
            }
        });
    }

    private void refreshSearchRoomList(List<SearchRoomPersonInfo> homeRooms) {
        ll_search_history.setVisibility(View.GONE);
        ll_search_hot.setVisibility(View.GONE);
        if (homeRooms != null && homeRooms.size() > 0) {
            searchRoomPersonAdapter.replaceData(homeRooms);
            rv_searchRoom.setLayoutManager(new RecyclerViewNoBugLinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
            rv_searchRoom.setAdapter(searchRoomPersonAdapter);
            ll_searchEmpty.setVisibility(View.GONE);
            tv_recomRoomTips.setVisibility(View.GONE);
        } else {
            ll_searchEmpty.setVisibility(View.VISIBLE);
            tv_recomRoomTips.setVisibility(recommendRoomList.size()>0 ? View.VISIBLE : View.GONE);
            searchRoomPersonAdapter.replaceData(new ArrayList<>());
            rv_searchRoom.setLayoutManager(new GridLayoutManager(getActivity(),3,GridLayoutManager.VERTICAL,false));
            rv_searchRoom.setAdapter(searchRecommendAdapter);
        }
    }

    private DefaultItemDecoration defaultItemDecoration = null;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_search_room;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LogUtils.d(TAG,"onDestroy");
    }

    @Override
    public void onRoomInfoItemClick(long userid, int type) {
        LogUtils.d(TAG, "onRoomInfoItemClick-userid:" + userid + " type:" + type);
        RoomServiceScheduler.getInstance().enterRoom(mContext, userid, type);
        //首页-搜索-推荐房间
        StatisticManager.get().onEvent(getActivity(),
                StatisticModel.EVENT_ID_SEARCH_RECOM_ROOM,
                StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
    }

    /**
     * 搜索房间 关键字搜索结果返回
     *
     * @param success
     * @param message
     * @param list
     */
    @Override
    public void onRoomRecommend(boolean success, String message, List list) {
        LogUtils.d(TAG, "onRoomRecommend-success:" + success + " message:" + message);
        if (list != null && list.size() > 0) {
            if(list.size()>RECOMMEND_ROOM_SHOW_NUM){
                list = list.subList(0,RECOMMEND_ROOM_SHOW_NUM);
            }
            recommendRoomList.clear();
            recommendRoomList.addAll(list);
            searchRecommendAdapter.replaceData(recommendRoomList);
            ll_searchEmpty.setVisibility(View.GONE);
            tv_recomRoomTips.setVisibility(View.VISIBLE);
            rv_searchRoom.setLayoutManager(new GridLayoutManager(getActivity(),3,GridLayoutManager.VERTICAL,false));
            rv_searchRoom.setAdapter(searchRecommendAdapter);
            rv_searchRoom.addItemDecoration(defaultItemDecoration);
        }
    }

    @Override
    public void onRefresh() {
        lastSearchHotPageNum = 1;
        if(null == recommendRoomList || recommendRoomList.size() == 0){
            getMvpPresenter().refreshRecommendRoomList();
        }
        searchRoom(searchEdit.getText().toString());
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String content = searchEdit.getText().toString();
            if(TextUtils.isEmpty(content) || TextUtils.isEmpty(content.trim())){
                ll_searchEmpty.setVisibility(View.GONE);
                iv_inputDelete.setVisibility(View.INVISIBLE);
                //https://bugly.qq.com/v2/crash-reporting/crashes/23dbf7aab1/30578?pid=1
                if(null == searchHistoryCacheJson){
                    initSearchHistory();
                }else{
                    refreshSearchHistory();
                }

                ll_search_hot.setVisibility(View.VISIBLE);
                tv_recomRoomTips.setVisibility(recommendRoomList.size()>0 ? View.VISIBLE : View.GONE);
                rv_searchRoom.setLayoutManager(new GridLayoutManager(getActivity(),3,GridLayoutManager.VERTICAL,false));
                rv_searchRoom.setAdapter(searchRecommendAdapter);
            }else{
                iv_inputDelete.setVisibility(View.VISIBLE);
            }
        }
    };

//---------------------------------------------关注---------------------------------------

    private SearchRoomPersonInfo lastClickToAttentRoomPersonInfo;

    @Override
    public void onClickToAttention(SearchRoomPersonInfo info) {
        LogUtils.d(TAG,"onClickToAttention-info:"+info);
        lastClickToAttentRoomPersonInfo = info;
        getDialogManager().showProgressDialog(getActivity(),getResources().getString(R.string.network_loading),false);
        if(info.isFollow()){
            CoreManager.getCore(IPraiseCore.class).cancelPraise(info.getUid());
        }else{
            CoreManager.getCore(IPraiseCore.class).praise(info.getUid());
            //首页-搜索-推荐关注
            StatisticManager.get().onEvent(getActivity(),
                    StatisticModel.EVENT_ID_SEARCH_RECOM_ATTENTION,
                    StatisticModel.getInstance().getUMAnalyCommonMap(getActivity()));
        }
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraise(long likedUid) {
        getDialogManager().dismissDialog();
        toast(R.string.fan_success);
        if(null != lastClickToAttentRoomPersonInfo && likedUid == lastClickToAttentRoomPersonInfo.getUid()){
            lastClickToAttentRoomPersonInfo.setFollow(true);
            searchRecommendAdapter.notifyItemChanged(recommendRoomList.indexOf(lastClickToAttentRoomPersonInfo));
            lastClickToAttentRoomPersonInfo = null;
        }
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraiseFaith(String error) {
        lastClickToAttentRoomPersonInfo = null;
        getDialogManager().dismissDialog();
        toast(error);
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onCanceledPraise(long likedUid) {
        getDialogManager().dismissDialog();
        toast(R.string.fan_cancel_success);
        if(null != lastClickToAttentRoomPersonInfo && likedUid == lastClickToAttentRoomPersonInfo.getUid()){
            lastClickToAttentRoomPersonInfo.setFollow(false);
            searchRecommendAdapter.notifyItemChanged(recommendRoomList.indexOf(lastClickToAttentRoomPersonInfo));
            lastClickToAttentRoomPersonInfo = null;
        }
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onCanceledPraiseFaith(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }
}
