package com.yuhuankj.tmxq.ui.webview;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

/**
 * <p> </p>
 *
 * @author jiahui
 * @date 2017/12/19
 */
public interface IVoiceAuthCardView extends IMvpBaseView {

    void onUpdateAuthCardVoice(boolean isSuccess, String message);
}
