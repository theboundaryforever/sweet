package com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.base.AbstractMvpRelativeLayout;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter.RoomHeatPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IRoomHeatView;

/**
 * 创建者      魏海涛
 * 创建时间    2019年4月25日 18:12:44
 * 描述        房间内左上角热度值的封装view
 * TODO 点赞5分钟刷新逻辑
 *
 * @author dell
 */
@CreatePresenter(RoomHeatPresenter.class)
public class RoomHeatView extends AbstractMvpRelativeLayout<IRoomHeatView, RoomHeatPresenter<IRoomHeatView>>
        implements IRoomHeatView/*, IDisposableAddListener*/ {

    private final String TAG = RoomHeatView.class.getSimpleName();

    private RoomHeatClickListener listener;

    private TextView tvRoomHeatRank;
    private TextView tvRoomHeatValue;

    public RoomHeatView(Context context) {
        this(context, null);
    }

    public RoomHeatView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoomHeatView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getViewLayoutId() {
        return 0;
    }

    @Override
    public void initialView(Context context) {
        inflate(getContext(), R.layout.layout_single_audio_room_heat, this);
        tvRoomHeatRank = findViewById(R.id.tvRoomHeatRank);
        tvRoomHeatValue = findViewById(R.id.tvRoomHeatValue);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != listener) {
                    listener.onShowRoomHeatRankList();
                }
            }
        });
        getMvpPresenter().getRoomHeatValue();
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initViewState() {

    }


    public void setRoomHeatClickListener(RoomHeatClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void refreshRoomHeatData(long hotRank, long hotScore) {
        LogUtils.d(TAG, "refreshRoomHeatData-hotRank:" + hotRank + " hotRank:" + hotScore);
        String rankStr = null;
        if (hotRank > 99) {
            rankStr = "99+";
        } else {
            rankStr = String.valueOf(hotRank);
        }
        tvRoomHeatRank.setText(rankStr);
        tvRoomHeatValue.setText("热度：".concat(String.valueOf(hotScore)));
    }

    public interface RoomHeatClickListener {

        /**
         * 点击打开土豪榜
         */
        void onShowRoomHeatRankList();
    }

    public void release() {
        setOnClickListener(null);
        if (null != listener) {
            listener = null;
        }
    }
}
