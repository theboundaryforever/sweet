package com.yuhuankj.tmxq.ui.nim.chat;

import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.netease.nim.uikit.common.ui.recyclerview.adapter.BaseMultiItemFetchLoadAdapter;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.netease.nim.uikit.session.viewholder.MsgViewHolderBase;
import com.netease.nimlib.sdk.msg.constant.MsgDirectionEnum;
import com.tongdaxing.xchat_core.bean.GameLinkMacroInvitaion;
import com.tongdaxing.xchat_core.im.custom.bean.GameLinkMacroFinishAttachment;
import com.yuhuankj.tmxq.R;

/**
 * @author liaoxy
 * @Description:游戏连麦邀请正常结束
 * @date 2019/2/15 11:21
 */
public class MsgViewHolderGameLinkMacroFinish extends MsgViewHolderBase {

    protected TextView bodyTextView;
    private View llGameLinkMicro;

    public MsgViewHolderGameLinkMacroFinish(BaseMultiItemFetchLoadAdapter adapter) {
        super(adapter);
    }

    @Override
    protected int getContentResId() {
        return com.netease.nim.uikit.R.layout.nim_message_item_text_icon;
    }

    @Override
    protected void inflateContentView() {
        bodyTextView = findViewById(com.netease.nim.uikit.R.id.nim_message_item_text_body);
        llGameLinkMicro = findViewById(com.netease.nim.uikit.R.id.llGameLinkMicro);
    }

    @Override
    protected void bindContentView() {
        GameLinkMacroInvitaion info = ((GameLinkMacroFinishAttachment) message.getAttachment()).getDataInfo();

        if (message.getDirect() == MsgDirectionEnum.Out) {
            bodyTextView.setTextColor(Color.WHITE);
        } else {
            bodyTextView.setTextColor(Color.BLACK);
        }

        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) bodyTextView.getLayoutParams();
        layoutParams.weight = LinearLayout.LayoutParams.WRAP_CONTENT;
        layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        bodyTextView.setLayoutParams(layoutParams);
        bodyTextView.setMinHeight(0);

        contentContainer.setBackgroundColor(Color.TRANSPARENT);
        if (isReceivedMessage()) {
            llGameLinkMicro.setBackgroundResource(com.netease.nim.uikit.R.drawable.nim_message_content_left_bg);
            llGameLinkMicro.setPadding(ScreenUtil.dip2px(10), 0, ScreenUtil.dip2px(10), 0);
        } else {
            llGameLinkMicro.setBackgroundResource(com.netease.nim.uikit.R.drawable.nim_message_content_right_bg);
            llGameLinkMicro.setPadding(ScreenUtil.dip2px(10), 0, ScreenUtil.dip2px(10), 0);
        }

        int talkTime = 0;
        if (info != null) {
            talkTime = info.getTalkTime();
        }
        String timeStr = String.format("聊天时长%02d:%02d", talkTime / 60, talkTime % 60);
        bodyTextView.setText(timeStr);
        ImageView imvIcon = findViewById(com.netease.nim.uikit.R.id.imvIcon);
        imvIcon.setImageResource(R.drawable.ic_p2p_linkmacro_dianhua_d);
    }
}
