package com.yuhuankj.tmxq.ui.webview;

import java.util.List;

/**
 * @author weihaitao
 * @date 2019/4/12
 */
public class VoiceUploadResponse {
    /**
     * bestPartner : 可爱少女音
     * fascinates : 10.0
     * main : {"name":"性感轻叔音","percent":94}
     * pounceOn : 8.4
     * starScore : 4
     * top : false
     * topStr : 受
     * vices : [{"name":"软糯轻受音","percent":57},{"name":"初恋小奶音","percent":49},{"name":"霸道总裁音","percent":56},{"name":"萌萌正太音","percent":5}]
     */

    private String bestPartner;
    private double fascinates;
    private MainBean main;
    private double pounceOn;
    private int starScore;
    private boolean top;
    private String topStr;
    private List<VicesBean> vices;

    public String getBestPartner() {
        return bestPartner;
    }

    public void setBestPartner(String bestPartner) {
        this.bestPartner = bestPartner;
    }

    public double getFascinates() {
        return fascinates;
    }

    public void setFascinates(double fascinates) {
        this.fascinates = fascinates;
    }

    public MainBean getMain() {
        return main;
    }

    public void setMain(MainBean main) {
        this.main = main;
    }

    public double getPounceOn() {
        return pounceOn;
    }

    public void setPounceOn(double pounceOn) {
        this.pounceOn = pounceOn;
    }

    public int getStarScore() {
        return starScore;
    }

    public void setStarScore(int starScore) {
        this.starScore = starScore;
    }

    public boolean isTop() {
        return top;
    }

    public void setTop(boolean top) {
        this.top = top;
    }

    public String getTopStr() {
        return topStr;
    }

    public void setTopStr(String topStr) {
        this.topStr = topStr;
    }

    public List<VicesBean> getVices() {
        return vices;
    }

    public void setVices(List<VicesBean> vices) {
        this.vices = vices;
    }

    public static class MainBean {
        /**
         * name : 性感轻叔音
         * percent : 94
         */

        private String name;
        private float percent;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public float getPercent() {
            return percent;
        }

        public void setPercent(float percent) {
            this.percent = percent;
        }
    }

    public static class VicesBean {
        /**
         * name : 软糯轻受音
         * percent : 57
         */

        private String name;
        private int percent;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getPercent() {
            return percent;
        }

        public void setPercent(int percent) {
            this.percent = percent;
        }
    }
    /**
     * {
     * 	"bestPartner": "可爱少女音",
     * 	"fascinates": 10.0,
     * 	"main": {
     * 		"name": "性感轻叔音",
     * 		"percent": 94
     *        },
     * 	"pounceOn": 8.4,
     * 	"starScore": 4,
     * 	"top": false,
     * 	"topStr": "受",
     * 	"vices": [{
     * 		"name": "软糯轻受音",
     * 		"percent": 57
     *    }, {
     * 		"name": "初恋小奶音",
     * 		"percent": 49
     *    }, {
     * 		"name": "霸道总裁音",
     * 		"percent": 56
     *    }, {
     * 		"name": "萌萌正太音",
     * 		"percent": 5
     *    }]
     * }
     */

}
