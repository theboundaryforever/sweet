package com.yuhuankj.tmxq.ui.signAward.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.noober.background.view.BLTextView;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.ButtonUtils;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.erban.libcommon.utils.TimeUtils;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.thirdsdk.nim.MsgCenterRedPointStatusManager;
import com.yuhuankj.tmxq.ui.signAward.model.SignAwardResult;
import com.yuhuankj.tmxq.ui.signAward.model.SignInAwardInfo;
import com.yuhuankj.tmxq.ui.signAward.presenter.SignAwardDaysPresenter;
import com.yuhuankj.tmxq.ui.signAward.view.adapter.SignAwardRecvAdapter;
import com.yuhuankj.tmxq.ui.widget.DividerItemDecoration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;

/**
 * 连续签到天数弹框
 *
 * @author weihaitao
 * @date 2019/5/22
 */
@CreatePresenter(SignAwardDaysPresenter.class)
public class SignAwardDaysDialog extends BaseMvpActivity<ISignAwardDaysView, SignAwardDaysPresenter>
        implements ISignAwardDaysView, View.OnClickListener {

    public final static String FLAG_SIGN_DAY_AWARD_DIALOG_SHOWED = "lastShowSignAwardDialogTime";

    private ImageView mIvTop;
    private RecyclerView mRvRecv1;
    private RecyclerView mRvRecv2;
    private BLTextView mBltvTake;
    private TextView tvSignDays;

    private SignAwardRecvAdapter adapter1;
    private SignAwardRecvAdapter adapter2;

    private int signDay;
    private List<SignInAwardInfo> signInAwardInfos = new ArrayList<>();

    public static void start(Context context, int signDay, List<SignInAwardInfo> signInAwardInfos) {
        //先判断当天是否已经显示过弹框了
        long lastShowTimerLong = (Long) SpUtils.get(context,FLAG_SIGN_DAY_AWARD_DIALOG_SHOWED,0L);
        if(lastShowTimerLong > 0L && TimeUtils.isSameDay(lastShowTimerLong,System.currentTimeMillis())){
            LogUtils.d(SignAwardDaysDialog.class.getSimpleName(),"start-今天已经弹过一次框了");
            return;
        }

        if(signDay <=0 || signDay > 7){
            //拦截后端数据异常的情况，避免数组越界
            return;
        }
        Intent intent = new Intent(context, SignAwardDaysDialog.class);
        intent.putExtra("signDay", signDay);
        intent.putExtra("signInAwardInfos", (Serializable) signInAwardInfos);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_award_days);
        ButterKnife.bind(this);
        initData();
        initView();
        setListener();
    }

    private void initData() {
        Intent intent = getIntent();
        if (intent.hasExtra("signDay")) {
            signDay = intent.getIntExtra("signDay", 0);
        }
        if (intent.hasExtra("signInAwardInfos")) {
            signInAwardInfos = (List<SignInAwardInfo>) intent.getSerializableExtra("signInAwardInfos");
        }
        if (null == signInAwardInfos || signInAwardInfos.size() != 7) {
            finish();
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        } else {
            SpUtils.put(this, FLAG_SIGN_DAY_AWARD_DIALOG_SHOWED, System.currentTimeMillis());
        }

    }

    private void initView() {
        tvSignDays = (TextView) findViewById(R.id.tvSignDays);
        mIvTop = (ImageView) findViewById(R.id.ivTop);

        mRvRecv1 = (RecyclerView) findViewById(R.id.rvRecv1);
        LinearLayoutManager manager1 = new RecyclerViewNoBugLinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRvRecv1.setLayoutManager(manager1);
        DividerItemDecoration dividerItemDecoration1 = new DividerItemDecoration(manager1.getOrientation(),
                Color.TRANSPARENT, this, DisplayUtility.dp2px(this, 10));
        mRvRecv1.addItemDecoration(dividerItemDecoration1);
        adapter1 = new SignAwardRecvAdapter(signDay, signInAwardInfos.subList(0, 4));
        mRvRecv1.setAdapter(adapter1);

        mRvRecv2 = (RecyclerView) findViewById(R.id.rvRecv2);
        LinearLayoutManager manager2 = new RecyclerViewNoBugLinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRvRecv2.setLayoutManager(manager2);
        DividerItemDecoration dividerItemDecoration2 = new DividerItemDecoration(manager2.getOrientation(),
                Color.TRANSPARENT, this, DisplayUtility.dp2px(this, 10));
        mRvRecv2.addItemDecoration(dividerItemDecoration2);
        adapter2 = new SignAwardRecvAdapter(signDay, signInAwardInfos.subList(4, 7));
        mRvRecv2.setAdapter(adapter2);

        mBltvTake = (BLTextView) findViewById(R.id.bltvTake);
        tvSignDays.setText(getResources().getString(R.string.sign_award_days_tips, String.valueOf(signDay)));

        //根据当天领取礼包的类型，实时修改文案
        if (null != signInAwardInfos && signInAwardInfos.size() > signDay - 1) {
            SignInAwardInfo signInAwardInfo = signInAwardInfos.get(signDay - 1);
            mBltvTake.setText(getResources().getString(signInAwardInfo.getFreebiesType() == 1 ?
                    R.string.sign_award_tips_open_box : R.string.sign_award_tips_get));
        }

        findViewById(R.id.llRoot).setOnClickListener(this);
        findViewById(R.id.flTop).setOnClickListener(this);
        findViewById(R.id.bllBottom).setOnClickListener(this);
    }

    private void setListener() {
        mBltvTake.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llRoot:
                finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
            case R.id.flTop:
            case R.id.bllBottom:
                break;
            case R.id.bltvTake:

                if (ButtonUtils.isFastDoubleClick(mBltvTake.getId())) {
                    return;
                }

                getDialogManager().showProgressDialog(this, getResources().getString(R.string.network_loading), false);
                getMvpPresenter().receiveSignInGift();

                Map<String, String> params = StatisticModel.getInstance().getUMAnalyCommonMap(this);
                params.put("signDay", String.valueOf(signDay));
                //连续签到奖励弹框-领取按钮打点事件
                StatisticManager.get().onEvent(this,
                        StatisticModel.EVENT_ID_SIGN_IN_DAYS_TAKE, params);
                break;
            default:
                break;
        }
    }

    /**
     * 显示萌新大礼包领取结果弹框
     */
    @Override
    public void showResultAfterSignSuc(SignAwardResult result) {
        getDialogManager().dismissDialog();
        SignAwardResultDialog.start(this, result);
        MsgCenterRedPointStatusManager.getInstance().setShowSignInRedPoint(false);
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    /**
     * 提示签到失败
     *
     * @param message
     */
    @Override
    public void showSignErr(String message) {
        getDialogManager().dismissDialog();
        if (!TextUtils.isEmpty(message)) {
            toast(message);
        }
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}
