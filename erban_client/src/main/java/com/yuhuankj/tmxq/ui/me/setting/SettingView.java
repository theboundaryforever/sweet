package com.yuhuankj.tmxq.ui.me.setting;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

public interface SettingView<T extends AbstractMvpPresenter> extends IMvpBaseView {

    void onChangeNobleEnterRoomVisiableStatus(boolean isSuccess, String message);

}
