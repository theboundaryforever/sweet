package com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter;

import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomModel;
import com.tongdaxing.xchat_core.result.RoomConsumeInfoListResult;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.base.presenter.BaseRoomDetailPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IMultiAudioView;

/**
 * 文件描述：多人语聊房的MVP 的P层
 *
 * @auther：zwk
 * @data：2019/6/3
 */
public class MultiAudioRoomPresenter extends BaseRoomDetailPresenter<IMultiAudioView> {


    /**
     * 房间获取排行榜数据
     *
     * @param roomUid
     * @param roomType
     */
    public void getFortuneRankTopData(long roomUid, int roomType) {
        OkHttpManager.MyCallBack<RoomConsumeInfoListResult> myCallBack =
                new OkHttpManager.MyCallBack<RoomConsumeInfoListResult>() {
                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                        if (null != getMvpView()) {
                            getMvpView().onGetFortuneRankTop(false, null, null);
                        }
                    }

                    @Override
                    public void onResponse(RoomConsumeInfoListResult result) {
                        if (null != getMvpView()) {
                            getMvpView().onGetFortuneRankTop(result.isSuccess(), result.getMessage(), result.getData());
                        }
                    }
                };
        new IMRoomModel().getRoomConsumeList(roomUid, roomType, 1, 1, myCallBack);
    }


}
