package com.yuhuankj.tmxq.ui.liveroom.nimroom.fragment;


import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.tongdaxing.erban.libcommon.coremanager.CoreEvent;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.coremanager.IUserInfoCore;
import com.tongdaxing.erban.libcommon.http_image.result.ServiceResult;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.TimeUtils;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.utils.json.JsonParser;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.auth.RealNameAuthStatusChecker;
import com.tongdaxing.xchat_core.bean.NoblePublicMsg;
import com.tongdaxing.xchat_core.bean.NoblePublicMsgBd;
import com.tongdaxing.xchat_core.bean.RoomRedPacket;
import com.tongdaxing.xchat_core.im.custom.bean.NoblePublicMsgAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomRedPacketAttachment;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.room.publicchatroom.PublicChatRoomController;
import com.tongdaxing.xchat_core.room_red_packet.RedPacketModel;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.BaseDialogFragment;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.adapter.PublicRoomRedPacketAdapter;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.widget.PublicMessageView;
import com.yuhuankj.tmxq.ui.me.noble.NobleBusinessManager;
import com.yuhuankj.tmxq.ui.me.noble.NobleModel;
import com.yuhuankj.tmxq.ui.me.taskcenter.model.TaskCenterModel;
import com.yuhuankj.tmxq.ui.nim.chat.MsgViewHolderMiniGameInvited;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * 广场公聊界面
 */
public class PublicChatFragment extends BaseDialogFragment implements View.OnClickListener {

    private final String TAG = PublicChatFragment.class.getSimpleName();

    public final static String ACTION_SHOW_IN_ROOM = "showInRoom";
    private boolean showInRoom = false;

    private PublicMessageView mMessageView;
    private EditText mInputEdit;
    private TextView mInputSend;
    private TextView mTvCountDown;

    private PublicChatRoomController mPublicChatRoomController;
    private CountDownTimer mCountDownTimer;

    //    private RelativeLayout rlInput;
//    private int statusHeight = 0;
//    private int screenHeight = 0;
//    private int keyboardHeight = 0;
//    private boolean lastStatus = false;
    private final int KEY_ORI_BOTTOM = R.id.emoticon_picker_view;

    private RelativeLayout rlNobleStick;//贵族置顶广播View
    private ImageView imvStick;
    private TextView tvTimer;
    private boolean isCanSend = true;//是否能发送置顶广播
    private static int MSG_NOBLE_MSG_STICK = 2;//贵族广播置顶消息

    //红包相关UI
    //可设置滚动速度的LinearLayoutManager
    private class ScrollSpeedLinearLayoutManger extends LinearLayoutManager {

        private float MILLISECONDS_PER_INCH = 0.03f;

        public ScrollSpeedLinearLayoutManger(Context context) {
            super(context);
        }

        @Override
        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
            LinearSmoothScroller linearSmoothScroller = new LinearSmoothScroller(recyclerView.getContext()) {
                @Override
                protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                    //返回滑动一个pixel需要多少毫秒
                    return MILLISECONDS_PER_INCH / displayMetrics.density;
                }
            };
            linearSmoothScroller.setTargetPosition(position);
            startSmoothScroll(linearSmoothScroller);
        }

        public void setSpeed(float speed) {
            MILLISECONDS_PER_INCH = speed;
        }

        public void resetSpeed() {
            MILLISECONDS_PER_INCH = 0.03f;
        }
    }

    //让RecyclerView滚动到指定位置
    private void scrollToPosition() {
        if (layoutManager != null && lastPosition >= 0) {
            layoutManager.scrollToPositionWithOffset(lastPosition, lastOffset - 29);
        }
    }

    //上一次滚动的位置
    private int lastOffset = 0;
    private int lastPosition = 0;

    //记录RecyclerView当前位置
    private void getPositionAndOffset() {
        if (layoutManager == null) {
            return;
        }
        //获取可视的第一个view
        View topView = layoutManager.getChildAt(0);
        if (topView != null) {
            //获取与该view的顶部的偏移量
            lastOffset = topView.getTop();
            //得到该View的数组位置
            lastPosition = layoutManager.getPosition(topView);
        }
    }

    private void initRedPacket(View view) {
        //红包相关UI初始化
        rcvRedPacket = view.findViewById(R.id.rcvRedPacket);
        layoutManager = new ScrollSpeedLinearLayoutManger(getContext());
        layoutManager.setSpeed(0.6f);
        layoutManager.setStackFromEnd(true);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rcvRedPacket.setLayoutManager(layoutManager);
        listRedPacket = new ArrayList<>(10);
        adapter = new PublicRoomRedPacketAdapter(listRedPacket);
        rcvRedPacket.setAdapter(adapter);
        //设置动画
        DefaultItemAnimator defaultItemAnimator = new DefaultItemAnimator();
        defaultItemAnimator.setAddDuration(3000);
        defaultItemAnimator.setRemoveDuration(3000);
        rcvRedPacket.setItemAnimator(defaultItemAnimator);

        rcvRedPacket.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                //静止没有滚动
                //public static final int SCROLL_STATE_IDLE = 0;
                //正在被外部拖拽,一般为用户正在用手指滚动
                //public static final int SCROLL_STATE_DRAGGING = 1;
                //自动滚动
                //public static final int SCROLL_STATE_SETTLING = 2;

                //只有滚动条静止的时候才刷新
                if (newState == 0) {
                    getPositionAndOffset();
                    isCanUpdateTimer = true;
                    if (listRedPacketTime.size() == 0) {
                        adapter.notifyDataSetChanged();
                        scrollToPosition();
                    }
                } else {
                    isCanUpdateTimer = false;
                }
            }
        });

        adapter.setOnItemClickListener((adapter, view1, position) -> {
            RoomRedPacket roomRedPacket = listRedPacket.get(position);
            if (roomRedPacket != null && roomRedPacket.getRoomUid() != 0) {
                //单人音频房等新类型房间暂时没有添加派红包业务功能，因此这里保持默认的轰趴房间类型
                RoomServiceScheduler.getInstance().enterRoom(
                        getContext(), roomRedPacket.getRoomUid(),
                        roomRedPacket.getRoomType());
            } else {
                SingleToastUtil.showToast("进房失败");
                listRedPacket.remove(position);
                adapter.notifyDataSetChanged();
            }
        });
        getRoomHistoryRedPacket();
    }

    //获取房间历史红包
    private void getRoomHistoryRedPacket() {
        listRedPacket.clear();
        listRedPacketTime.clear();
        new RedPacketModel().getRoomHistoryRedPacket(PublicChatRoomController.getPublicChatRoomId(), new OkHttpManager.MyCallBack<ServiceResult<List<RoomRedPacket>>>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(ServiceResult<List<RoomRedPacket>> response) {
                if (response.isSuccess()) {
                    List<RoomRedPacket> redPackets = response.getData();
                    if (redPackets != null && redPackets.size() > 0) {
                        for (RoomRedPacket roomRedPacket : redPackets) {
                            if (roomRedPacket.getType() == 2) {
                                long startTime = roomRedPacket.getRedPacketTime();
                                long curTime = MsgViewHolderMiniGameInvited.getCurTimeMillis();
                                long residue = curTime - startTime;
                                int timing = roomRedPacket.getTiming() * 1000;
                                if (residue < timing) {
                                    residue = timing - residue;
                                    //转换成秒
                                    residue = residue / 1000;
                                    if (residue > 1200) {
                                        residue = 1200;
                                    }
                                    roomRedPacket.setUseabelTime(residue);
                                    listRedPacketTime.add(roomRedPacket);
                                }
                            }
                        }
                        listRedPacket.addAll(redPackets);
                        adapter.notifyDataSetChanged();

                        rcvRedPacket.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                rcvRedPacket.smoothScrollToPosition(listRedPacket.size() - 1);
                            }
                        }, 100);
                        if (listRedPacketTime != null && listRedPacketTime.size() > 0) {
                            handler.removeMessages(MSG_RED_PACKET);
                            handler.sendEmptyMessage(MSG_RED_PACKET);
                        }
                    } else {
                        adapter.notifyDataSetChanged();
                    }
                }
            }
        });
    }

    private RecyclerView rcvRedPacket;
    private ScrollSpeedLinearLayoutManger layoutManager;
    private List<RoomRedPacket> listRedPacket = new ArrayList<>();
    private List<RoomRedPacket> listRedPacketTime = new ArrayList<>();
    private PublicRoomRedPacketAdapter adapter;
    private boolean isCanUpdateTimer = true;//是否能更新计时器,滚动时不更新
    private static int MSG_RED_PACKET = 1;//红包消息
    private Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == MSG_RED_PACKET) {
                if (!listRedPacketTime.isEmpty()) {
                    List<RoomRedPacket> rmRedPacket = new ArrayList<>();
                    for (RoomRedPacket timeRedPacket : listRedPacketTime) {
                        timeRedPacket.setUseabelTime(timeRedPacket.getUseabelTime() - 1);
                        if (timeRedPacket.getUseabelTime() <= 0) {
                            rmRedPacket.add(timeRedPacket);
                        }
                    }
                    if (rmRedPacket.size() > 0) {
                        listRedPacketTime.removeAll(rmRedPacket);
                    }
                    if (isCanUpdateTimer) {
                        adapter.notifyDataSetChanged();
                        scrollToPosition();
                    }

                    handler.removeMessages(MSG_RED_PACKET);
                    handler.sendEmptyMessageDelayed(MSG_RED_PACKET, 1000);
                }
            } else if (msg.what == MSG_NOBLE_MSG_STICK) {
                int time = msg.arg1;
                if (time <= 0) {
                    if (rlNobleStick != null) {
                        rlNobleStick.setVisibility(View.GONE);
                    }
                } else {
                    if (tvTimer != null) {
                        tvTimer.setText(TimeUtils.getProgresstime(time));
                    }
                    Message message = new Message();
                    message.what = MSG_NOBLE_MSG_STICK;
                    message.arg1 = time - 1000;
                    sendMessageDelayed(message, 1000);
                }
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_public_chat_room, container, false);
//        LogUtils.d(PublicChatRoomController.TAG, "onCreateView");
        if (getArguments() != null) {
            showInRoom = getArguments().getBoolean(ACTION_SHOW_IN_ROOM, false);
//            LogUtils.d(PublicChatRoomController.TAG, "onCreateView-showInRoom:" + showInRoom);
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMessageView = view.findViewById(R.id.message_view);
        mInputEdit = view.findViewById(R.id.et_msg);
        GrowingIO.getInstance().trackEditText(mInputEdit);
        mInputSend = view.findViewById(R.id.btn_send);
        mTvCountDown = view.findViewById(R.id.tv_count_down);
        imvStick = view.findViewById(R.id.imvStick);
//        rlInput = view.findViewById(R.id.rlInput);
        rlNobleStick = view.findViewById(R.id.rlNobleStick);
        mPublicChatRoomController = new PublicChatRoomController();
        initView();

        initRedPacket(view);
//        LogUtils.d(PublicChatRoomController.TAG, "onViewCreated showInRoom:" + showInRoom);
        CoreManager.getCore(VersionsCore.class).requestSensitiveWord();
        if (showInRoom) {
            initData();
        }
    }

    private void initView() {
        mInputSend.setOnClickListener(this);
        mTvCountDown.setOnClickListener(this);

//        screenHeight = ResolutionUtils.getScreenHeight(getContext());
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        //是贵族国王显示置顶按纽
        if (userInfo != null && NobleBusinessManager.hasAllAppBroadcast(userInfo.getVipId())) {
            imvStick.setVisibility(View.VISIBLE);
            imvStick.setOnClickListener(this);
        } else {
            imvStick.setVisibility(View.GONE);
        }
        rlNobleStick.setVisibility(View.GONE);
        rlNobleStick.setOnClickListener(this);

        initEvent();
    }

    private void initData() {
//        LogUtils.d(PublicChatRoomController.TAG, "initData-->enterRoom");
        if (mMessageView != null) {
            mMessageView.clear();
        }
        mPublicChatRoomController.enterRoom(new PublicChatRoomController.ActionCallBack() {
            @Override
            public void success() {
//                LogUtils.d(PublicChatRoomController.TAG, "initData-->enterRoom-进入房间成功");
            }

            @Override
            public void error() {
//                LogUtils.d(PublicChatRoomController.TAG, "initData-->enterRoom-进入房间失败");
            }
        });
        mPublicChatRoomController.setSendMsgCallBack(new PublicChatRoomController.SendMsgCallBack() {
            @Override
            public void success() {
                //完成大厅发言任务
                new TaskCenterModel().finishTask(2, new OkHttpManager.MyCallBack<ServiceResult<Object>>() {
                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onResponse(ServiceResult<Object> response) {

                    }
                });
            }

            @Override
            public void error() {

            }
        });
        mPublicChatRoomController.inintCacheTime();
        checkCountDown();
        getNoblePublicMsg();
    }

    //初始化接收房间消息事件
    private void initEvent() {
        if (mCompositeDisposable == null) {
            return;
        }
        Disposable disposable = IMNetEaseManager.get().getChatRoomEventObservable()
                .subscribe(new Consumer<RoomEvent>() {
                    @Override
                    public void accept(RoomEvent roomEvent) throws Exception {
                        if (roomEvent == null) {
                            return;
                        }
                        LogUtils.d("dealRoomEvent", "roomEven:" + roomEvent.getEvent());
                        int event = roomEvent.getEvent();
                        switch (event) {
                            case RoomEvent.ROOM_RECEIVE_NOBLE_PUBLIC_MSG:
                                NoblePublicMsgAttachment nobleAttachment = roomEvent.getNoblePublicMsgAttachment();
                                if (nobleAttachment == null) {
                                    break;
                                }
                                NoblePublicMsg noblePublicMsg = nobleAttachment.getDataInfo();
                                if (noblePublicMsg == null) {
                                    break;
                                }
                                setMsgNobleMsgStick(noblePublicMsg);
                                break;
                            case RoomEvent.ROOM_RECEIVE_RED_PACKET:
                                RoomRedPacketAttachment redPacketAttachment = roomEvent.getRedPacketAttachment();
                                if (redPacketAttachment != null) {
                                    RoomRedPacket roomRedPacket = redPacketAttachment.getDataInfo();

                                    if (roomRedPacket == null) {
                                        return;
                                    }
                                    //如果说不在广播中显示,直接返回
                                    if (roomRedPacket.getShowBroadcast() == 0) {
                                        return;
                                    }
                                    listRedPacket.add(roomRedPacket);
                                    adapter.notifyDataSetChanged();

                                    rcvRedPacket.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            rcvRedPacket.smoothScrollToPosition(listRedPacket.size() - 1);
                                        }
                                    }, 100);

                                    if (roomRedPacket.getType() == 2) {
                                        long startTime = roomRedPacket.getRedPacketTime();
                                        long curTime = MsgViewHolderMiniGameInvited.getCurTimeMillis();
                                        long residue = curTime - startTime;
                                        int timing = roomRedPacket.getTiming() * 1000;
                                        if (residue < timing) {
                                            residue = timing - residue;
                                            //取整
                                            residue = residue + (1000 - residue % 1000);
                                            //转换成秒
                                            residue = residue / 1000;
                                            if (residue > 1200) {
                                                residue = 1200;
                                            }
                                            roomRedPacket.setUseabelTime(residue);
                                            listRedPacketTime.add(roomRedPacket);
                                        }
                                        if (listRedPacketTime != null && listRedPacketTime.size() > 0) {
                                            handler.removeMessages(MSG_RED_PACKET);
                                            handler.sendEmptyMessage(MSG_RED_PACKET);
                                        }
                                    }
                                }
                                break;
                            case RoomEvent.ROOM_RECEIVE_RED_PACKET_FINISH:
                                break;
                            default:
                                break;
                        }
                    }
                });
        mCompositeDisposable.add(disposable);
        //限制最大输入三行
        mInputEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int lineCount = mInputEdit.getLineCount();
                if (lineCount > 3) {
                    //发现输入的内容大于最大行数，则删除多余的内容
                    String str = mInputEdit.getText().toString();
                    str = str.substring(0, str.length() - 1);
                    mInputEdit.setText(str);
                    mInputEdit.setSelection(mInputEdit.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        boolean doInitDataOpear = isVisibleToUser && mMessageView != null && mMessageView.isEmply();
//        LogUtils.d(PublicChatRoomController.TAG,"PublicChatFragment setUserVisibleHint-doInitDataOpear:"
//                +doInitDataOpear+" isVisibleToUser:"+isVisibleToUser);
        if (doInitDataOpear) {
            initData();
        }
        if (isVisibleToUser) {
            getRoomHistoryRedPacket();
        } else if (getActivity() != null) {
            try {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(mInputEdit.getWindowToken(), 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void openInputLayout(final View view, int visibleHeight) {
        int[] vPoint = new int[2];
        view.getLocationOnScreen(vPoint);
        int viewY = vPoint[1] + view.getMeasuredHeight();
        final int bottomMargin = viewY - visibleHeight;

        if (view.getParent() instanceof FrameLayout) {
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) view.getLayoutParams();
            view.setTag(KEY_ORI_BOTTOM, lp.bottomMargin);
            lp.bottomMargin += bottomMargin;
            view.setLayoutParams(lp);
        } else if (view.getParent() instanceof RelativeLayout) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) view.getLayoutParams();
            view.setTag(KEY_ORI_BOTTOM, lp.bottomMargin);
            lp.bottomMargin += bottomMargin;
            view.setLayoutParams(lp);
        } else if (view.getParent() instanceof LinearLayout) {
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) view.getLayoutParams();
            view.setTag(KEY_ORI_BOTTOM, lp.bottomMargin);
            lp.bottomMargin += bottomMargin;
            view.setLayoutParams(lp);
        }
        view.requestLayout();
    }

    private void closeInputLayout(final View view) {

        if (view.getParent() instanceof FrameLayout) {
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) view.getLayoutParams();
            if (view.getTag(KEY_ORI_BOTTOM) != null) {
                lp.bottomMargin = (int) view.getTag(KEY_ORI_BOTTOM);
            } else {
                lp.bottomMargin = 0;
            }
        } else if (view.getParent() instanceof RelativeLayout) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) view.getLayoutParams();
            if (view.getTag(KEY_ORI_BOTTOM) != null) {
                lp.bottomMargin = (int) view.getTag(KEY_ORI_BOTTOM);
            } else {
                lp.bottomMargin = 0;
            }
        } else if (view.getParent() instanceof LinearLayout) {
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) view.getLayoutParams();
            if (view.getTag(KEY_ORI_BOTTOM) != null) {
                lp.bottomMargin = (int) view.getTag(KEY_ORI_BOTTOM);
            } else {
                lp.bottomMargin = 0;
            }
        }
        view.requestLayout();
    }

    //获取贵族置顶广播
    private void getNoblePublicMsg() {
        new NobleModel().getNoblePublicMsg(new OkHttpManager.MyCallBack<ServiceResult<NoblePublicMsgBd>>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(ServiceResult<NoblePublicMsgBd> response) {
                if (response != null && response.isSuccess()) {
                    try {
                        NoblePublicMsgBd noblePublicMsgBd = response.getData();
                        if (noblePublicMsgBd == null) {
                            return;
                        }
                        isCanSend = noblePublicMsgBd.isCanSend();
                        NoblePublicMsg noblePublicMsg = JsonParser.parseJsonToNormalObject(noblePublicMsgBd.getBroadcast(), NoblePublicMsg.class);
                        setMsgNobleMsgStick(noblePublicMsg);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    //设置置顶广播
    private void setMsgNobleMsgStick(NoblePublicMsg noblePublicMsg) {
        //全站广播消息
        try {
            //3分钟
            long allTime = 3 * 60 * 1000;
            long offset = 0;
            if (rlNobleStick == null || tvTimer == null) {
                return;
            }
            ImageView imvAvatar = rlNobleStick.findViewById(R.id.imvAvatar);
            TextView tvContent = rlNobleStick.findViewById(R.id.tvContent);
            tvTimer = rlNobleStick.findViewById(R.id.tvTimer);

            ImageLoadUtils.loadCircleImage(getContext(), noblePublicMsg.getAvatar(), imvAvatar, R.drawable.default_user_head);
            tvContent.setText(noblePublicMsg.getMessage());

            //跑马灯效果必须加
            tvContent.setSelected(true);
            rlNobleStick.setVisibility(View.VISIBLE);
            rlNobleStick.setTag(noblePublicMsg.getUid());

            //3分钟后移除置顶广播
            handler.removeMessages(MSG_NOBLE_MSG_STICK);
            Message message = new Message();
            message.what = MSG_NOBLE_MSG_STICK;
            message.arg1 = (int) (allTime - offset);
            handler.sendMessage(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void checkCountDown() {
        long time = System.currentTimeMillis();
        long l = time - (mPublicChatRoomController != null ? mPublicChatRoomController.cacheTime : 0);
        l = PublicChatRoomController.maxWaitTime - l;
        LogUtils.d(TAG, "checkCountDown-l:" + l);
        if (l <= PublicChatRoomController.maxWaitTime && l >= 0) {
            if (mInputSend == null || mTvCountDown == null) {
                return;
            }
            mInputSend.setVisibility(View.GONE);
            mTvCountDown.setVisibility(View.VISIBLE);
            mCountDownTimer = new CountDownTimer(l, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    if (null != getActivity() && mTvCountDown != null) {
                        int countDownTime = (int) (millisUntilFinished / 1000);
                        mTvCountDown.setText(getActivity().getString(R.string.send_msg_count, countDownTime));
                    }
                }

                @Override
                public void onFinish() {
                    if (mInputEdit == null) {
                        return;
                    }
                    mInputSend.setVisibility(View.VISIBLE);
                    mTvCountDown.setVisibility(View.GONE);
                }
            };
            mCountDownTimer.start();
        } else {
            if (mInputSend == null || mTvCountDown == null) {
                return;
            }
            mInputSend.setVisibility(View.VISIBLE);
            mTvCountDown.setVisibility(View.GONE);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        LogUtils.d(TAG, "onDestroy");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        LogUtils.d(TAG, "onDestroyView");
        try {
            if (mCountDownTimer != null) {
                mCountDownTimer.cancel();
            }
            if (!showInRoom && mPublicChatRoomController != null) {
                mPublicChatRoomController.leaveRoom();
            }
            if (null != mMessageView) {
                mMessageView.release();
                mMessageView = null;
            }
            if (rlNobleStick != null) {
                rlNobleStick.setOnClickListener(null);
                rlNobleStick = null;
            }
            if (imvStick != null) {
                imvStick.setOnClickListener(null);
                imvStick = null;
            }
            tvTimer = null;
            mInputEdit = null;
            if (mInputSend != null) {
                mInputSend.setOnClickListener(null);
                mInputSend = null;
            }
            if (mTvCountDown != null) {
                mTvCountDown.setOnClickListener(null);
                mTvCountDown = null;
            }
            if (handler != null) {
                handler.removeCallbacksAndMessages(null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Called when a view has been clicked.
     *
     * @param view The view that was clicked.
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_send:
                if (!RealNameAuthStatusChecker.getInstance().authStatus(getActivity(), getResources().getString(R.string.real_name_auth_tips,
                        getResources().getString(R.string.real_name_auth_tips_send_msg)))) {
//                    LogUtils.d(PublicChatRoomController.TAG,"onClick-->sendMsg, 实名认证拦截");
                    return;
                }
                if (mInputSend == null || mPublicChatRoomController == null) {
//                    LogUtils.d(PublicChatRoomController.TAG,"onClick-->sendMsg, 控件为空拦截");
                    return;
                }
                if (!checkSendPower() || checkBanned()) {
//                    LogUtils.d(PublicChatRoomController.TAG,"onClick-->sendMsg, 未满足发送条件拦截");
                    return;
                }

                String trim = mInputEdit.getText().toString().trim();
                String sensitiveWordData = CoreManager.getCore(VersionsCore.class).getSensitiveWordData();
                if (!TextUtils.isEmpty(sensitiveWordData) && !TextUtils.isEmpty(trim) && trim.replaceAll("\n", "").replace(" ", "").matches(sensitiveWordData)) {
                    SingleToastUtil.showToast(this.getResources().getString(R.string.sensitive_word_data));
                    return;
                }

                if (trim.length() > 40) {
                    trim = trim.substring(0, 40);
                }
                if (imvStick != null && imvStick.isShown() && imvStick.isSelected()) {
                    //发送贵族广播
                    sendNoblePublicMsg(trim);
                }
//                LogUtils.d(PublicChatRoomController.TAG,"onClick-->sendMsg trim:"+trim);
                Disposable disposable = mPublicChatRoomController.sendMsg(trim);
                if (null == disposable || null == mCompositeDisposable) {
                    return;
                }
                mCompositeDisposable.add(disposable);
                mPublicChatRoomController.refreshTime();
                checkCountDown();
                mInputEdit.setText("");
//                置顶广播电台测试代码
//                NoblePublicMsg noblePublicMsg = new NoblePublicMsg();
//                noblePublicMsg.setUid(123);
//                noblePublicMsg.setMessage("50万模板_涵盖海报/营销海报/邀请函/公众号封面等,素材图片库 免费使用.功能简单好用,可快速掌握,图怪兽_国内在线编辑器,不用下载,直接使用.");
//                noblePublicMsg.setAvatar("https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3300305952,1328708913&fm=27&gp=0.jpg");
//                setMsgNobleMsgStick(noblePublicMsg);
                break;
            case R.id.tv_count_down:
                if (null != getActivity()) {
                    SingleToastUtil.showShortToast(getActivity().getString(R.string.wait_please));
                }
                break;
            case R.id.imvStick:
                //贵族置顶按纽
                if (imvStick == null) {
                    return;
                }
                if (!imvStick.isSelected() && !isCanSend) {
                    SingleToastUtil.showShortToast("每天只可置顶一次，你今天已经置顶过了哦~");
                    return;
                }
                imvStick.setSelected(!imvStick.isSelected());
                break;
            case R.id.rlNobleStick:
                if (rlNobleStick == null) {
                    return;
                }
                try {
                    long uid = (long) rlNobleStick.getTag();
                    UserInfoActivity.start(getContext(), uid);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
        }
    }

    //发送贵族置顶广播
    private void sendNoblePublicMsg(String message) {
        DialogManager dialogManager = new DialogManager(getContext());
        dialogManager.showProgressDialog(getContext(), "请稍候...");
        new NobleModel().sendNoblePublicMsg(message, new OkHttpManager.MyCallBack<ServiceResult<Json>>() {
            @Override
            public void onError(Exception e) {
                dialogManager.dismissDialog();
                e.printStackTrace();
                SingleToastUtil.showToast("发送广播失败");
                if (mCountDownTimer != null) {
                    mCountDownTimer.cancel();
                }
                if (mInputSend != null) {
                    mInputSend.setVisibility(View.VISIBLE);
                }
                if (mTvCountDown != null) {
                    mTvCountDown.setVisibility(View.GONE);
                }
            }

            @Override
            public void onResponse(ServiceResult<Json> response) {
                dialogManager.dismissDialog();
                if (response == null) {
                    SingleToastUtil.showToast("发送广播失败");
                    return;
                }
                if (!response.isSuccess()) {
                    SingleToastUtil.showToast(response.getMessage());
                    if (mCountDownTimer != null) {
                        mCountDownTimer.cancel();
                    }
                    if (mInputSend != null) {
                        mInputSend.setVisibility(View.VISIBLE);
                    }
                    if (mTvCountDown != null) {
                        mTvCountDown.setVisibility(View.GONE);
                    }
                } else {
                    SingleToastUtil.showToast("发送成功");
                    if (imvStick != null) {
                        imvStick.setSelected(false);
                    }
                    isCanSend = false;
                    getNoblePublicMsg();
                }
            }
        });
    }


    /**
     * 检查发送条件是否满足
     *
     * @return
     */
    public boolean checkSendPower() {
        if (!AvRoomDataManager.get().isOwnerOnMic() && !RoomDataManager.get().isOwnerOnMic()) {
            SingleToastUtil.showShortToast("发广播需要在房间麦序上哦");
            return false;
        }

        if (System.currentTimeMillis() - (mPublicChatRoomController != null ? mPublicChatRoomController.cacheTime : 0) <= 60000) {
            SingleToastUtil.showShortToast("广播发送间隔为60秒，请稍等");
            return false;
        }
        return true;
    }

    private boolean checkBanned() {
        Json json = CoreManager.getCore(IUserInfoCore.class).getBannedMap();
        boolean all = json.boo(IUserInfoCore.BANNED_ALL + "");
        boolean room = json.boo(IUserInfoCore.BANNED_PUBLIC_ROOM + "");
        if (all || room) {
            SingleToastUtil.showShortToast(getResources().getString(R.string.banned));
            return true;
        }
        return false;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        CoreManager.addClient(this);
//        LogUtils.d(TAG, "onAttach");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        CoreManager.removeClient(this);
//        LogUtils.d(TAG, "onDetach");
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo userInfo) {
//        LogUtils.d(PublicChatRoomController.TAG, "onCurrentUserInfoUpdate showInRoom:" + showInRoom);
        if (showInRoom && null != userInfo && userInfo.getUid() == CoreManager.getCore(IAuthCore.class).getCurrentUid()) {
//            initData();
        }
    }
}
