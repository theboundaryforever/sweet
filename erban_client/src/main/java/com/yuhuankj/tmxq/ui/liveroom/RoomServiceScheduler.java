package com.yuhuankj.tmxq.ui.liveroom;

import android.content.Context;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.gift.ComboGiftPublicScreenMsgSender;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomEvent;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.ui.MainActivity;
import com.yuhuankj.tmxq.ui.liveroom.imroom.RoomFrameActivity;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.AVRoomActivity;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;

/**
 * 进房、切换房间、创建房间，业务逻辑调度器
 *
 * @author weihaitao
 * @date 2019/4/22
 */
public class RoomServiceScheduler extends BaseRoomServiceScheduler {

    private static RoomServiceScheduler instance = null;
    private final String TAG = RoomServiceScheduler.class.getSimpleName();

    private RoomServiceScheduler() {

    }

    public static RoomServiceScheduler getInstance() {
        if (null == instance) {
            instance = new RoomServiceScheduler();
        }
        return instance;
    }

    public void enterRoom(Context context, long uid, int roomType) {
        LogUtils.d(TAG, "enterRoom uid:" + uid + " roomType:" + roomType);
        RoomInfo cRoomInfo = RoomServiceScheduler.getCurrentRoomInfo();
        if (cRoomInfo == null) {
            if (roomType == RoomInfo.ROOMTYPE_HOME_PARTY) {
                AVRoomActivity.start(context, uid);
            }
            if (roomType == RoomInfo.ROOMTYPE_SINGLE_AUDIO || roomType == RoomInfo.ROOMTYPE_MULTI_AUDIO
                    || roomType == RoomInfo.ROOMTYPE_NEW_AUCTION) {
                RoomFrameActivity.start(context, uid, roomType);
            }
        } else {
            enterOtherTypeRoomFromService(context, uid, roomType);
        }

    }

    public void enterRoom(Context context, long uid) {
        LogUtils.d(TAG, "enterRoom uid:" + uid);
        RoomInfo cRoomInfo = RoomServiceScheduler.getCurrentRoomInfo();
        if (cRoomInfo == null) {
            return;
        }
        enterRoom(context, uid, cRoomInfo.getType());
    }

    public void enterOtherTypeRoomFromService(Context context, long targetRoomUid, int targetRoomType) {
        RoomInfo cRoomInfo = RoomServiceScheduler.getCurrentRoomInfo();
        //房间信息必定不为空
        if (null == cRoomInfo) {
            enterRoomFromService(context, targetRoomUid, targetRoomType);
            return;
        }
        if (targetRoomType == cRoomInfo.getType()) {
            ComboGiftPublicScreenMsgSender.getInstance().setCurrRoomId(cRoomInfo.getRoomId());
            if (targetRoomType == RoomInfo.ROOMTYPE_HOME_PARTY && AvRoomDataManager.get().isMinimize()) {
                //相同类型的轰趴房 则可以直接跳
                enterRoomFromService(context, targetRoomUid, targetRoomType);
                return;
            } else if (RoomDataManager.get().isMinimize()) {
                //非轰趴房的IM新类型房间且已经最小化
                enterRoomFromService(context, targetRoomUid, targetRoomType);
                return;
            }
            //如果是IM新类型房间且未最小化，则还是走先finish后enterRoom的逻辑
        }
        //不同类型的房间，需要退出原有房间，并且界面finish,
        // 注意单纯的退出房间界面并不会finish，因为主动退出房间并不会收到IM退房通知
        if (cRoomInfo.getType() == RoomInfo.ROOMTYPE_HOME_PARTY) {
            if (AvRoomDataManager.get().isMinimize()) {
                enterRoomFromService(context, targetRoomUid, targetRoomType);
                ComboGiftPublicScreenMsgSender.getInstance().setCurrRoomId(cRoomInfo.getRoomId());
            } else {
                IMNetEaseManager.get().getChatRoomEventObservable()
                        .onNext(new RoomEvent().setEvent(RoomEvent.ROOM_ENTER_OTHER_ROOM)
                                .setRoomUid(targetRoomUid)
                                .setRoomType(targetRoomType));
            }
        } else if (cRoomInfo.getType() == RoomInfo.ROOMTYPE_SINGLE_AUDIO || cRoomInfo.getType() == RoomInfo.ROOMTYPE_MULTI_AUDIO
                || cRoomInfo.getType() == RoomInfo.ROOMTYPE_NEW_AUCTION) {
            if (RoomDataManager.get().isMinimize()) {
                enterRoomFromService(context, targetRoomUid, targetRoomType);
                ComboGiftPublicScreenMsgSender.getInstance().setCurrRoomId(cRoomInfo.getRoomId());
            } else {
                IMRoomEvent imRoomEvent = new IMRoomEvent();
                imRoomEvent.setEvent(RoomEvent.ROOM_ENTER_OTHER_ROOM);
                imRoomEvent.setRoomUid(targetRoomUid);
                imRoomEvent.setRoomType(targetRoomType);
                IMRoomMessageManager.get().getIMRoomEventObservable()
                        .onNext(imRoomEvent);
            }
        }

    }

    public void enterRoomFromService(Context context, long uid, int roomType) {
        LogUtils.d(TAG, "enterRoomFromService uid:" + uid + " roomType:" + roomType);
        if (roomType == RoomInfo.ROOMTYPE_HOME_PARTY) {
            AVRoomActivity.startFromService(context, uid);
        }
        if (roomType == RoomInfo.ROOMTYPE_SINGLE_AUDIO || roomType == RoomInfo.ROOMTYPE_MULTI_AUDIO
                || roomType == RoomInfo.ROOMTYPE_NEW_AUCTION) {
            RoomFrameActivity.start(context, uid, roomType);
        }
    }

    public void enterRoomFromJPush(Context context, long uid, int roomType) {
        LogUtils.d(TAG, "enterRoomFromJPush uid:" + uid + " roomType:" + roomType);
        RoomInfo cRoomInfo = RoomServiceScheduler.getCurrentRoomInfo();
        if (cRoomInfo == null) {
            if (roomType == RoomInfo.ROOMTYPE_HOME_PARTY) {
                AVRoomActivity.startFromService(context, uid);
            }
            if (roomType == RoomInfo.ROOMTYPE_SINGLE_AUDIO || roomType == RoomInfo.ROOMTYPE_MULTI_AUDIO
                    || roomType == RoomInfo.ROOMTYPE_NEW_AUCTION) {
                RoomFrameActivity.startFromService(context, uid, roomType);
            }
            ComboGiftPublicScreenMsgSender.getInstance().setCurrRoomId(cRoomInfo.getRoomId());
        } else {
            enterOtherTypeRoomFromService(context, uid, roomType);
        }


    }

    /**
     * 创建房间的逻辑，全部在MainActivity中做统一处理
     * @deprecated
     * @param mainActivity
     * @param uid
     */
    public void createRoom(MainActivity mainActivity, long uid) {
        LogUtils.d(TAG, "createRoom uid:" + uid);
        //房间最小化状态再次点击我的房间，直接进
        if (null != AvRoomDataManager.get().mCurrentRoomInfo
                && AvRoomDataManager.get().mCurrentRoomInfo.getUid() == uid) {
            mainActivity.getDialogManager().showProgressDialog(mainActivity,
                    mainActivity.getResources().getString(R.string.network_loading));
            mainActivity.getMvpPresenter().createHomePartRoom(uid, RoomInfo.ROOMTYPE_HOME_PARTY);
            return;
        }

        if (null != RoomDataManager.get().getCurrentRoomInfo()
                && RoomDataManager.get().getCurrentRoomInfo().getUid() == uid) {
            RoomFrameActivity.start(mainActivity, uid, RoomDataManager.get().getCurrentRoomInfo().getType());
            return;
        }
        if (CoreManager.getCore(IUserCore.class) == null) {
            return;
        }
        UserInfo selfInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (selfInfo == null) {
            return;
        }
        //如果服务端控制该用户不可创建单人音频房间，那么直接走轰趴房逻辑
        if (selfInfo.isCreateSingleLive() != 1 && selfInfo.getMultiRoom() != 1) {
            mainActivity.getMvpPresenter().createHomePartRoom(uid, RoomInfo.ROOMTYPE_HOME_PARTY);
            return;
        }
        mainActivity.getDialogManager().showEnterRoomDialog(selfInfo.isCreateSingleLive(), selfInfo.getMultiRoom(), new DialogManager.OkCancelDialogListener() {
            @Override
            public void onCancel() {
                if (null != mainActivity) {
                    mainActivity.getDialogManager().showProgressDialog(mainActivity,
                            mainActivity.getResources().getString(R.string.network_loading));
                    mainActivity.getMvpPresenter().createHomePartRoom(uid, RoomInfo.ROOMTYPE_HOME_PARTY);
                }
            }

            @Override
            public void onOk() {
                if (null == mainActivity) {
                    return;
                }
                //本地校验是否已经实名认证
                UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
                if (null != userInfo && !userInfo.isRealNameAudit()) {
                    mainActivity.getDialogManager().showOkCancelDialog(
                            mainActivity.getResources().getString(R.string.real_name_auth_tips,
                                    mainActivity.getResources().getString(R.string.real_name_auth_tips_create_room)),
                            mainActivity.getResources().getString(R.string.real_name_auth_accept),
                            mainActivity.getResources().getString(R.string.cancel), new DialogManager.OkCancelDialogListener() {
                                @Override
                                public void onCancel() {
                                    if (null != mainActivity) {
                                        RoomFrameActivity.start(mainActivity, uid, RoomInfo.ROOMTYPE_SINGLE_AUDIO);
                                    }
                                }

                                @Override
                                public void onOk() {
                                    if (null != mainActivity) {
                                        CommonWebViewActivity.start(mainActivity, UriProvider.getRealNameAuthUrl());
                                    }
                                }
                            });
                } else {
                    //已经实名认证，直接走进单人音频房的流程
                    RoomFrameActivity.start(mainActivity, uid, RoomInfo.ROOMTYPE_SINGLE_AUDIO);
                }
            }

            @Override
            public void onNewMultiRoom() {
                //本地校验是否已经实名认证
                UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
                if (null != userInfo && !userInfo.isRealNameAudit()) {
                    mainActivity.getDialogManager().showOkCancelDialog(
                            mainActivity.getResources().getString(R.string.real_name_auth_tips,
                                    mainActivity.getResources().getString(R.string.real_name_auth_tips_create_room)),
                            mainActivity.getResources().getString(R.string.real_name_auth_accept),
                            mainActivity.getResources().getString(R.string.cancel), new DialogManager.OkCancelDialogListener() {
                                @Override
                                public void onCancel() {
                                    if (null != mainActivity) {
                                        RoomFrameActivity.start(mainActivity, uid, RoomInfo.ROOMTYPE_MULTI_AUDIO);
                                    }
                                }

                                @Override
                                public void onOk() {
                                    if (null != mainActivity) {
                                        CommonWebViewActivity.start(mainActivity, UriProvider.getRealNameAuthUrl());
                                    }
                                }
                            });
                } else {
                    //已经实名认证，直接走进单人音频房的流程
                    RoomFrameActivity.start(mainActivity, uid, RoomInfo.ROOMTYPE_MULTI_AUDIO);
                }
            }

            @Override
            public void onAuctionRoom() {
                //本地校验是否已经实名认证
                UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
                if (null != userInfo && !userInfo.isRealNameAudit()) {
                    mainActivity.getDialogManager().showOkCancelDialog(
                            mainActivity.getResources().getString(R.string.real_name_auth_tips,
                                    mainActivity.getResources().getString(R.string.real_name_auth_tips_create_room)),
                            mainActivity.getResources().getString(R.string.real_name_auth_accept),
                            mainActivity.getResources().getString(R.string.cancel), new DialogManager.OkCancelDialogListener() {
                                @Override
                                public void onCancel() {
                                    if (null != mainActivity) {
                                        RoomFrameActivity.start(mainActivity, uid, RoomInfo.ROOMTYPE_NEW_AUCTION);
                                    }
                                }

                                @Override
                                public void onOk() {
                                    if (null != mainActivity) {
                                        CommonWebViewActivity.start(mainActivity, UriProvider.getRealNameAuthUrl());
                                    }
                                }
                            });
                } else {
                    //已经实名认证，直接走进单人音频房的流程
                    RoomFrameActivity.start(mainActivity, uid, RoomInfo.ROOMTYPE_NEW_AUCTION);
                }
            }
        });
    }

}
