package com.yuhuankj.tmxq.ui.liveroom.imroom.publicscreen.holder;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.netease.nim.uikit.common.util.string.StringUtil;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.im.IMReportRoute;
import com.tongdaxing.erban.libcommon.ninepatchloader.NinePatchBitmapLoader;
import com.tongdaxing.erban.libcommon.utils.RichTextUtil;
import com.tongdaxing.xchat_core.bean.attachmsg.RoomQueueMsgAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMember;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomMessage;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.liveroom.imroom.publicscreen.base.RoomMsgBaseHolder;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;
import com.yuhuankj.tmxq.ui.widget.UserInfoDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 文件描述：有指定发送人的  -- 文本消息holder类
 *
 * @auther：zwk
 * @data：2019/5/7
 */
public class RoomMsgChatTxtHolder extends RoomMsgBaseChatHolder {

    public RoomMsgChatTxtHolder(View itemView) {
        super(itemView);
    }

    @Override
    protected void onDataTransformContent(RoomMsgBaseHolder holder, IMRoomMessage message, int position, TextView etvContent) {
        if (IMReportRoute.sendTextReport.equalsIgnoreCase(message.getRoute())) {
            etvContent.setTextColor(Color.WHITE);
            setChatTextMsg(message, etvContent);
        } else if (IMReportRoute.chatRoomMemberIn.equalsIgnoreCase(message.getRoute())) {
            etvContent.setTextColor(Color.WHITE);
            setMemberInfoNotification(message, etvContent);
        } else if (IMReportRoute.sendMessageReport.equalsIgnoreCase(message.getRoute())) {
            etvContent.setTextColor(0xFFFFDA81);
            if (message.getAttachment() != null) {
                if (message.getAttachment().getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE) {
                    setSystemMsg(message.getImRoomMember(), (RoomQueueMsgAttachment) message.getAttachment(),
                            message.getAttachment().getSecond(), etvContent);
                } else if (message.getAttachment().getFirst() == CustomAttachment.CUSTOM_MSG_ROOM_OPERA_TIPS
                        || message.getAttachment().getFirst() == CustomAttachment.CUSTOM_MSG_FIRST_ROOM_ADMIRE_NOTIFY
                        || message.getAttachment().getFirst() == CustomAttachment.CUSTOM_MSG_FIRST_ROOM_ATTENTION_TIPS) {
                    setRoomOperaTips(etvContent, message.getAttachment().getFirst(), message.getAttachment().getSecond());
                } else if (message.getAttachment().getFirst() == CustomAttachment.CUSTOM_MSG_ROOM_SYSTEM_RICH_TXT) {
                    setRichTextMsg(message.getAttachment(), etvContent);
                } else {
                    etvContent.setText("不支持消息类型");
                }
            } else {
                etvContent.setText("不支持消息类型");
            }
        }else {
            etvContent.setText("不支持消息类型");
        }
    }

    /**
     * 设置富文本消息
     *
     * @param customAttachment
     * @param tvContent
     */
    private void setRichTextMsg(CustomAttachment customAttachment, TextView tvContent) {
        if (null != customAttachment.getData()) {
            String msgContent = customAttachment.getData().toJSONString();
            //新版本富文本消息一律没有单独的点击事件
            if (!StringUtil.isEmpty(msgContent)) {
                tvContent.setText(RichTextUtil.getSpannableStringFromJson(tvContent, msgContent, new RichTextUtil.OnClickableRichTxtItemClickedListener() {
                    @Override
                    public void onClickToEnterRoomActivity(long roomUid) {
                        RoomServiceScheduler.getInstance().enterRoom(tvContent.getContext(), roomUid);
                    }

                    @Override
                    public void onClickToShowUserInfoDialog(long uid) {
                        new UserInfoDialog(tvContent.getContext(), uid).show();
                    }

                    @Override
                    public void onClickToShowWebViewLoadUrl(String url) {
                        CommonWebViewActivity.start(tvContent.getContext(), url, false);
                    }
                }));
            } else {
                tvContent.setText("");
            }
        } else {
            tvContent.setText("");
        }
    }

    /**
     * 用户房间聊天信息
     *
     * @param message
     * @param etvContent
     */
    private void setChatTextMsg(IMRoomMessage message, TextView etvContent) {
        etvContent.setText(message.getContent());
        IMRoomMember imRoomMember = message.getImRoomMember();
        if (null != imRoomMember && !TextUtils.isEmpty(imRoomMember.getPointNineImg())) {
            NinePatchBitmapLoader.getInstance()
                    .loadNinePatchBitmap(etvContent, imRoomMember.getPointNineImg(), R.drawable.shape_liveroom_message_bg);
        } else {
            etvContent.setBackgroundResource(R.drawable.shape_liveroom_message_bg);
        }
    }

    /**
     * 用户进入通知消息
     *
     * @param chatRoomMessage
     * @param tvContent
     */
    private void setMemberInfoNotification(IMRoomMessage chatRoomMessage, TextView tvContent) {
        IMRoomMember member = chatRoomMessage.getImRoomMember();
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        HashMap<String, Object> map = new HashMap<String, Object>();
        if (!TextUtils.isEmpty(member.getCarName())) {
            map.put(RichTextUtil.RICHTEXT_STRING, "骑着");
            map.put(RichTextUtil.RICHTEXT_COLOR, Color.WHITE);
            list.add(map);
            map = new HashMap<String, Object>();
            map.put(RichTextUtil.RICHTEXT_STRING, "“".concat(member.getCarName()).concat("”"));
            map.put(RichTextUtil.RICHTEXT_COLOR, 0xFFFFDA81);
            list.add(map);

            map = new HashMap<String, Object>();
        }
        map.put(RichTextUtil.RICHTEXT_STRING, "来了");
        map.put(RichTextUtil.RICHTEXT_COLOR, Color.WHITE);
        list.add(map);

        CharSequence cs = RichTextUtil.getSpannableStringFromList(list);
        //保持同IOS样式一致
        tvContent.setText(cs);
    }

    /**
     * 屏蔽礼物特效等系统消息
     */
    private void setSystemMsg(IMRoomMember member, RoomQueueMsgAttachment roomQueueMsgAttachment, int second, TextView tvContent) {
        String content = "我";
        if (member == null) {
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            if (userInfo != null) {
                content = userInfo.getNick();
            }
        } else {
            content = member.getNick();
        }
        String targetNick = roomQueueMsgAttachment.getTargetNick() + "";
        if (com.netease.nim.uikit.common.util.string.StringUtil.isEmpty(targetNick) || "null".equals(targetNick)) {
            targetNick = "";
        }
        if (second == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_CLOSE) {
            content = content.concat("关闭了公屏");
        } else if (second == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_OPEN) {
            content = content.concat("打开了公屏");
        } else if (second == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_OPEN) {
            content = content.concat("开启低价值礼物特效");
        } else if (second == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_CLOSE) {
            content = content.concat("关闭低价值礼物特效");
        } else if (second == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_KICK_ROOM) {
            content = "玩家 ".concat(targetNick).concat(" 被[管理] ").concat(content).concat(" 踢出了房间");
        } else if (second == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_ADD_BLACK_LIST) {
            content = "玩家 ".concat(targetNick).concat(" 被[管理] ").concat(content).concat(" 加入了黑名单");
        } else if (second == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_BLACK_LIST) {
            content = "玩家 ".concat(targetNick).concat(" 被[管理] ").concat(content).concat(" 移除了黑名单");
        }
        tvContent.setTextColor(0xFFFFDA81);
        tvContent.setText(content);
    }

    /**
     * 显示自定义的关注房主消息
     */
    private void setRoomOperaTips(TextView tvContent, int first, int second) {
        RoomInfo roomInfo = RoomDataManager.get().getCurrentRoomInfo();
        boolean isSingleAudioRoom = null != roomInfo && roomInfo.getType() == RoomInfo.ROOMTYPE_SINGLE_AUDIO;
        if (first == CustomAttachment.CUSTOM_MSG_ROOM_OPERA_TIPS) {
            if (second == CustomAttachment.CUSTOM_MSG_ROOM_OPERA_TIPS_SHARE) {
                tvContent.setText(isSingleAudioRoom ? "我分享了本次直播" : "我分享了房间");
                tvContent.setTextColor(0xFFFFDA81);
            } else if (second == CustomAttachment.CUSTOM_MSG_ROOM_OPERA_TIPS_ATTENTION) {
                tvContent.setText(isSingleAudioRoom ? "我关注了主播" : "我关注了房间");
                tvContent.setTextColor(0xFFFFDA81);
            }
        } else if (first == CustomAttachment.CUSTOM_MSG_FIRST_ROOM_ATTENTION_TIPS
                && second == CustomAttachment.CUSTOM_MSG_SECOND_ROOM_ATTENTION_TIPS) {
            tvContent.setText(isSingleAudioRoom ? "我关注了主播" : "我关注了房间");
            tvContent.setTextColor(0xFFFFDA81);
        } else if (first == CustomAttachment.CUSTOM_MSG_FIRST_ROOM_ADMIRE_NOTIFY
                && second == CustomAttachment.CUSTOM_MSG_SECOND_ROOM_ADMIRE_MSG) {
            if (roomInfo != null && roomInfo.getType() == RoomInfo.ROOMTYPE_SINGLE_AUDIO) {
                tvContent.setText("我为主播点了个赞");
            } else {
                tvContent.setText("我为这个房间点个赞");
            }

            tvContent.setTextColor(0xFFFFDA81);
        } else {
            tvContent.setText(null);
        }
    }

}
