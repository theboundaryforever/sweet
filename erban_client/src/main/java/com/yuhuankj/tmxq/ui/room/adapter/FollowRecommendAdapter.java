package com.yuhuankj.tmxq.ui.room.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.constant.StatisticModel;
import com.yuhuankj.tmxq.ui.user.other.UserInfoActivity;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.yuhuankj.tmxq.ui.room.fragment.FollowFragment.hasFollow;

public class FollowRecommendAdapter extends BaseQuickAdapter<Json, FollowRecommendAdapter.ViewHolder> {
    public FollowRecommendAdapter() {
        super(R.layout.item_follow_recommend);
    }

    @Override
    protected void convert(ViewHolder viewHolder, Json json) {
        if (GlideContextCheckUtil.checkContextUsable(viewHolder.ivFollowUserIcon.getContext())) {
            ImageLoadUtils.loadCircleImage(viewHolder.ivFollowUserIcon.getContext(),
                    ImageLoadUtils.toThumbnailUrl(DisplayUtility.dp2px(viewHolder.ivFollowUserIcon.getContext(), 50),
                            DisplayUtility.dp2px(viewHolder.ivFollowUserIcon.getContext(), 50), json.str("avatar")),
                    viewHolder.ivFollowUserIcon, R.drawable.default_user_head);
        }
        viewHolder.tvFunCount.setText(viewHolder.tvFunCount.getContext().getResources().getString(
                R.string.follow_user_recommend_fans, json.num("fansNum")));
        String nick = json.str("nick");
        if (!StringUtil.isEmpty(nick) && nick.length() > 6) {
            viewHolder.tvFollowUserName.setText(viewHolder.tvFollowUserName.getContext().getResources().getString(R.string.nick_length_max_six, nick.substring(0, 6)));
        } else {
            viewHolder.tvFollowUserName.setText(nick);
        }
        int num = json.num(hasFollow);
        viewHolder.tvFollowUserEvent.setBackgroundDrawable(
                viewHolder.tvFollowUserEvent.getContext().getResources().getDrawable(num == 1 ?
                        R.drawable.bg_follow_recommend_has_attention : R.drawable.bg_follow_recommend_attention));
        viewHolder.tvFollowUserEvent.setText(viewHolder.tvFollowUserEvent.getContext().getResources().getString(num == 1 ?
                R.string.user_info_already_attention : R.string.user_info_attention));
    }

    public interface FollowRecommendEvent {
        void onFollowEvent(int index, long uid);
    }

    private FollowRecommendEvent followRecommendEvent;

    public void setFollowRecommendEvent(FollowRecommendEvent followRecommendEvent) {
        this.followRecommendEvent = followRecommendEvent;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        Json json = getData().get(position);
        long uid = json.num_l("uid");
        holder.tvFollowUserEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (followRecommendEvent != null) {
                    if (json.num(hasFollow) != 1) {
                        followRecommendEvent.onFollowEvent(position, uid);
                    }
                }
            }
        });

        holder.ivFollowUserIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserInfoActivity.start(holder.ivFollowUserIcon.getContext(), uid);
                //首页-关注-推荐用户
                StatisticManager.get().onEvent(holder.ivFollowUserIcon.getContext(),
                        StatisticModel.EVENT_ID_FOLLOW_NOT_ATTENTION_ROOM,
                        StatisticModel.getInstance().getUMAnalyCommonMap(holder.ivFollowUserIcon.getContext()));
            }
        });
    }

    public class ViewHolder extends BaseViewHolder {
        @BindView(R.id.tvFunCount)
        TextView tvFunCount;
        @BindView(R.id.ivFollowUserIcon)
        ImageView ivFollowUserIcon;
        @BindView(R.id.tvFollowUserName)
        TextView tvFollowUserName;
        @BindView(R.id.tvFollowUserEvent)
        TextView tvFollowUserEvent;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


}
