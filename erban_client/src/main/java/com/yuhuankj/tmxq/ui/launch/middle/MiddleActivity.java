package com.yuhuankj.tmxq.ui.launch.middle;

import android.os.Bundle;

import com.microquation.linkedme.android.LinkedME;
import com.microquation.linkedme.android.util.LinkProperties;
import com.tencent.bugly.crashreport.BuglyLog;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.erban.libcommon.utils.config.SpEvent;
import com.tongdaxing.xchat_core.linked.ILinkedCore;
import com.tongdaxing.xchat_core.linked.LinkedInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.base.activity.BaseActivity;

import java.util.HashMap;

/**
 * Created by chenran on 2017/8/5.
 */

public class MiddleActivity extends BaseActivity {

    private final String TAG = MiddleActivity.class.getSimpleName();
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent() != null) {
            //获取与深度链接相关的值
            LinkProperties linkProperties = getIntent().getParcelableExtra(LinkedME.LM_LINKPROPERTIES);
            if (linkProperties != null) {
                String linkerMeChannel = linkProperties.getChannel();
                BuglyLog.d(TAG, "Channel " + linkerMeChannel);
                BuglyLog.d(TAG, "control params " + linkProperties.getControlParams());
                BuglyLog.d(TAG, "link(深度链接) " + linkProperties.getLMLink());
                BuglyLog.d(TAG, "是否为新安装 " + linkProperties.isLMNewUser());
                //获取自定义参数封装成的hashmap对象,参数键值对由集成方定义
                HashMap<String, String> hashMap = linkProperties.getControlParams();
                //根据key获取传入的参数的值,该key关键字View可为任意值,由集成方规定,请与web端商议,一致即可
                LinkedInfo linkedInfo = new LinkedInfo();
                linkedInfo.setNewUser(linkProperties.isLMNewUser());
                String roomuid = hashMap.get("roomuid");
                String uid = hashMap.get("uid");
                String channel = hashMap.get("linkedmeChannel");
                String type = hashMap.get("type");

                //甜蜜星球1.0.0.2版本新增参数，区分房间类型
                String roomType = String.valueOf(RoomInfo.ROOMTYPE_HOME_PARTY);
                if (hashMap.containsKey("roomType")) {
                    roomType = hashMap.get("roomType");
                }
                linkedInfo.setRoomType(roomType);

                BuglyLog.d(TAG, "roomuid:" + roomuid + " uid:" + uid + " type：" + type + " roomType:" + roomType);
                if (roomuid != null) {
                    linkedInfo.setRoomUid(roomuid);
                }
                if (uid != null) {
                    SpUtils.put(MiddleActivity.this, SpEvent.linkedMeShareUid, uid);
                    linkedInfo.setUid(uid);
                }
                if (type != null) {
                    linkedInfo.setType(type);
                }
                if (channel != null) {
                    SpUtils.put(MiddleActivity.this, SpEvent.linkedMeChannel, channel);
                }
                CoreManager.getCore(ILinkedCore.class).setLinkedInfo(linkedInfo);
            }
        }
        finish();
    }
}
