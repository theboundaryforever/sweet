package com.yuhuankj.tmxq.ui.liveroom.imroom.room.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter.PayReqAudioConnPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IPayReqAudioConnView;
import com.yuhuankj.tmxq.ui.me.charge.ChargeActivity;

import java.util.Locale;

/**
 * 付费申请连线
 *
 * @author 魏海涛
 * @date 2019年7月17日 11:25:54
 */
@CreatePresenter(PayReqAudioConnPresenter.class)
public class PayReqAudioConnActivity extends BaseMvpActivity<IPayReqAudioConnView, PayReqAudioConnPresenter<IPayReqAudioConnView>>
        implements IPayReqAudioConnView, View.OnClickListener {

    public static final int PAY_REQ_AUDIO_CONN_REQUEST_CODE = 1;
    public static final int PAY_REQ_AUDIO_CONN_RESULT_CODE = 1;
    private EditText edPayGold;
    private TextView tvCurrPayInfo;
    private TextView tvGoldNum;
    private double lastGetGoldNum = 0d;

    public static void startForResult(FragmentActivity fragmentActivity, int requestCode) {
        Intent intent = new Intent(fragmentActivity, PayReqAudioConnActivity.class);
        fragmentActivity.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_req_audio_conn);
        initTitleBar(getResources().getString(R.string.audio_connect_req));
        initView();
        initData();
    }

    private void initData() {
        refreshAudioConnPayHistoryInfo(0L, 0L);
        refreshGoldNum(0D);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getDialogManager().showProgressDialog(this, getResources().getString(R.string.network_loading));
        getMvpPresenter().getRoomApplyInfo();
    }

    private void initView() {
        edPayGold = (EditText) findViewById(R.id.edPayGold);
        tvCurrPayInfo = (TextView) findViewById(R.id.tvCurrPayInfo);
        tvGoldNum = (TextView) findViewById(R.id.tvGoldNum);
        findViewById(R.id.tvRecharge).setOnClickListener(this);
        findViewById(R.id.bltvReqConn).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bltvReqConn:
                reqAudioConn();
                break;
            case R.id.tvRecharge:
                //跳转充值界面
                ChargeActivity.start(this);
                break;
            default:
                break;
        }
    }

    private void reqAudioConn() {
        //检测是否满足申请连线条件，走对应的流程
        if (null == edPayGold.getText() || TextUtils.isEmpty(edPayGold.getText().toString())) {
            toast(getResources().getString(R.string.audio_connect_req_pay_tips4));
            return;
        }
        long inputNum = 0L;
        try {
            String inputNumStr = edPayGold.getText().toString();
            inputNum = Long.valueOf(inputNumStr);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (inputNum < 10) {
            toast(getResources().getString(R.string.audio_connect_req_pay_tips5));
            return;
        }
        if (inputNum > Double.valueOf(lastGetGoldNum).longValue()) {
            getDialogManager().showOkCancelDialog(
                    getResources().getString(R.string.audio_connect_req_pay_tips6),
                    getResources().getString(R.string.sure),
                    getResources().getString(R.string.cancel), new DialogManager.OkCancelDialogListener() {
                        @Override
                        public void onCancel() {
                            // do nothing
                        }

                        @Override
                        public void onOk() {
                            //跳转充值界面
                            ChargeActivity.start(PayReqAudioConnActivity.this);
                        }
                    });
            return;
        }
        final long payConnReqGoldNum = inputNum;
        getDialogManager().showOkCancelDialog(
                getResources().getString(R.string.audio_connect_req_pay_tips7, String.valueOf(inputNum)),
                getResources().getString(R.string.sure),
                getResources().getString(R.string.cancel), new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {
                        // do nothing
                    }

                    @Override
                    public void onOk() {
                        getDialogManager().showProgressDialog(PayReqAudioConnActivity.this,
                                getResources().getString(R.string.network_loading));
                        //调付费申请接口
                        getMvpPresenter().reqAudioConn(getResources().getString(R.string.audio_connect_req_reason_default),
                                payConnReqGoldNum);
                    }
                });
    }

    /**
     * 刷新最高、最低出价
     *
     * @param hightest
     * @param lowest
     */
    @Override
    public void refreshAudioConnPayHistoryInfo(long hightest, long lowest) {
        String tips = getResources().getString(R.string.audio_connect_req_pay_notify,
                String.valueOf(lowest), String.valueOf(hightest));
        SpannableStringBuilder ssb = new SpannableStringBuilder(tips);
        int firstIndex = tips.indexOf(String.valueOf(lowest));
        int secondIndex = tips.lastIndexOf(String.valueOf(hightest));
        ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#999999")), 0,
                firstIndex, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#FF9E70")),
                firstIndex, firstIndex + String.valueOf(lowest).length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#999999")),
                firstIndex + String.valueOf(lowest).length(), secondIndex, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#FF9E70")),
                secondIndex, secondIndex + String.valueOf(hightest).length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#999999")),
                secondIndex + String.valueOf(hightest).length(), tips.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        tvCurrPayInfo.setText(ssb);
    }

    @Override
    public void refreshGoldNum(double goldNum) {
        lastGetGoldNum = goldNum;
        tvGoldNum.setText(getResources().getString(R.string.audio_connect_req_pay_gold_has,
                String.format(Locale.getDefault(), "%.1f", goldNum)));
    }

    @Override
    public void toast(String toast) {
        if (!TextUtils.isEmpty(toast)) {
            super.toast(toast);
        }
        getDialogManager().dismissDialog();
    }

    @Override
    public void finishForResult() {
        setResult(PayReqAudioConnActivity.PAY_REQ_AUDIO_CONN_REQUEST_CODE);
        finish();
    }

}
