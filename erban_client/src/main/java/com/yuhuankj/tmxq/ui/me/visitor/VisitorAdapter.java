package com.yuhuankj.tmxq.ui.me.visitor;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.netease.nim.uikit.common.util.sys.TimeUtil;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;

import java.util.List;

public class VisitorAdapter extends BaseQuickAdapter<VisitorEnitity, BaseViewHolder> {

    public VisitorAdapter(List data) {
        super(R.layout.item_visitor, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, VisitorEnitity item) {
        ImageView imvPortrait = helper.getView(R.id.imvPortrait);
        View vSex = helper.getView(R.id.vSex);
        TextView tvUserName = helper.getView(R.id.tvUserName);
        TextView tvTime = helper.getView(R.id.tvTime);

        try {
            tvUserName.setText(item.getNick());
            tvTime.setText(TimeUtil.getTimeExp(item.getCreateDate()));

            ImageLoadUtils.loadCircleImage(mContext, item.getAvatar(), imvPortrait, R.drawable.ic_userinfo_default);
            if (item.getGender() == 1) {
                vSex.setBackgroundResource(R.drawable.icon_gender_man);
            } else {
                vSex.setBackgroundResource(R.drawable.icon_gender_woman);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}