package com.yuhuankj.tmxq.ui.ranklist;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import java.util.List;

/**
 * @author liaoxy
 * @Description: viewpager的fragment适配器
 * @date 2015-9-10 下午4:19:08
 */
public class TabPageAdapter extends FragmentPagerAdapter {

    private List<Fragment> fragments;

    public TabPageAdapter(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        if (fragments != null && fragments.size() > 0) {
            return fragments.get(position);
        } else {
            return null;
        }
    }

    @Override
    public int getCount() {
        if (fragments != null) {
            return fragments.size();
        } else {
            return 0;
        }
    }

    @NonNull
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
//        Fragment fragment = (Fragment) super.instantiateItem(container, position);
//        fm.beginTransaction().show(fragment).commit();
//        return fragment;

        return super.instantiateItem(container, position);
    }

    // 重写此方法，不做实现，可防止fragment被回收
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        //      Fragment fragment = fragments.get(position);
        //       fm.beginTransaction().hide(fragment).commit();
    }

}
