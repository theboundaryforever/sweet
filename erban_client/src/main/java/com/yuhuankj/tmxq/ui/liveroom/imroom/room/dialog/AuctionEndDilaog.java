package com.yuhuankj.tmxq.ui.liveroom.imroom.room.dialog;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.room.auction.AuctionUserBean;
import com.tongdaxing.xchat_core.room.auction.RoomAuctionBean;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.BaseAppDialog;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;
import com.yuhuankj.tmxq.utils.StringUtil;

/**
 * 文件描述:拍卖结束弹框提示
 *
 * @auther：zwk
 * @data：2019/8/1
 */
public class AuctionEndDilaog extends BaseAppDialog {
    private RoomAuctionBean roomAuctionBean;

    public AuctionEndDilaog(Context context, RoomAuctionBean roomAuctionBean) {
        super(context);
        this.roomAuctionBean = roomAuctionBean;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_auction_end;
    }

    @Override
    protected void initView() {
        ImageView ivJoinAvatar = findViewById(R.id.civ_join_auction_user_avatar);
        ImageView ivJoinGender = findViewById(R.id.iv_join_auction_user_gender);
        ImageView ivAvatar = findViewById(R.id.civ_auction_user_avatar);
        ImageView ivGender = findViewById(R.id.iv_auction_user_gender);
        TextView tvJoinNick = findViewById(R.id.tv_auction_user_nick);
        TextView tvNick = findViewById(R.id.tv_auction_user_nick2);
        TextView tvInfo = findViewById(R.id.tv_auction_info);
        if (roomAuctionBean == null || roomAuctionBean.getUid() <= 0) {
            dismiss();
        } else {
            ImageLoadUtils.loadCircleImage(getContext(), roomAuctionBean.getAvatar(), ivJoinAvatar, R.drawable.ic_default_avatar);
            ivJoinGender.setImageResource(roomAuctionBean.getGender() == 1 ? R.drawable.icon_man : R.drawable.icon_female);
            tvJoinNick.setText(StringUtil.limitStr(getContext(), roomAuctionBean.getNick(), 4));
            tvInfo.setText(roomAuctionBean.getProject() + " | " + roomAuctionBean.getDay() + "天");
            if (!ListUtils.isListEmpty(roomAuctionBean.getList())) {
                AuctionUserBean auctionUserBean = roomAuctionBean.getList().get(0);
                ImageLoadUtils.loadCircleImage(getContext(), auctionUserBean.getAvatar(), ivAvatar, R.drawable.ic_default_avatar);
                ivGender.setImageResource(auctionUserBean.getGender() == 1 ? R.drawable.icon_man : R.drawable.icon_female);
                tvNick.setText(StringUtil.limitStr(getContext(), auctionUserBean.getNick(), 4));
            }
        }
    }

    @Override
    public void initListener() {

    }

    @Override
    public void initViewState() {

    }
}
