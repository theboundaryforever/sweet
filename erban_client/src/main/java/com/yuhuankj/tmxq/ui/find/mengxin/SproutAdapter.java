package com.yuhuankj.tmxq.ui.find.mengxin;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;

import com.tongdaxing.xchat_core.home.TabInfo;

import java.util.List;

/**
 * <p> 主页adapter  热门等 </p>
 * Created by Administrator on 2017/11/15.
 */
public class SproutAdapter extends FragmentPagerAdapter {
    public SparseArray<SproutNewFragment> getmFragmentList() {
        return mFragmentList;
    }

    private SparseArray<SproutNewFragment> mFragmentList;
    private List<TabInfo> mTitleList;

    public SproutAdapter(FragmentManager fm, List<TabInfo> titleList) {
        super(fm);
        this.mTitleList = titleList;
        mFragmentList = new SparseArray<>();
        SproutNewFragment fragment = new SproutNewFragment();
        fragment.setSproutListType(SproutNewFragment.SproutListType.sprout);
        mFragmentList.put(0, fragment);
        fragment = new SproutNewFragment();
        fragment.setSproutListType(SproutNewFragment.SproutListType.nearby);
        mFragmentList.put(1, fragment);
    }

    @Override
    public SproutNewFragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mTitleList == null ? 0 : mTitleList.size();
    }
}