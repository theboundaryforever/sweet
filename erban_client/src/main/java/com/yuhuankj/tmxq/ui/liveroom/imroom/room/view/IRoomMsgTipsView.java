package com.yuhuankj.tmxq.ui.liveroom.imroom.room.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

import java.util.List;

public interface IRoomMsgTipsView extends IMvpBaseView {

    void refreshMsgTipsView(List<String> roomTipsList);

    void refreshMsgTipsShowStatus(boolean show);
}
