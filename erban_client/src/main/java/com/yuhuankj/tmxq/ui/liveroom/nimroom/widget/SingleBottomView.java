package com.yuhuankj.tmxq.ui.liveroom.nimroom.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.R;


/**
 * 2019年4月28日 00:49:54
 * 针对轰趴房
 * 1.房主，依次显示，发言、禁麦、声音、表情、私聊以及礼物按钮
 * 2.非房主，
 * 没有上麦，发言、禁麦、声音、以及礼物按钮
 * 上麦则，发言、禁麦、声音、表情、私聊以及礼物按钮
 * 分享房间按钮始终都是隐藏不可见的
 * <p>
 * 针对个人直播房
 * 1.房主，依次显示，发言、禁麦、声音、分享以及礼物按钮
 * 2.非房主，依次显示，说点什么，声音、分享以及礼物按钮
 * 表情、私聊（因没有上麦操作）、禁麦始终都是不可见的
 * <p>
 * xml默认设置为不可见的控件为，表情、私聊、分享
 * <p>
 * UI上，轰趴房父布局左边距为10dp，个人音频房左边距为15dp
 * 轰趴房btn之间的间距为10dp，个人音频房为12dp
 *
 * @author weihaitao
 * @date 2019年4月28日 00:41:09
 */

public class SingleBottomView extends AbstractBottomView implements View.OnClickListener {

    private final String TAG = SingleBottomView.class.getSimpleName();

    public SingleBottomView(Context context) {
        super(context);
    }

    public SingleBottomView(Context context, AttributeSet attr) {
        super(context, attr);
    }

    public SingleBottomView(Context context, AttributeSet attr, int i) {
        super(context, attr, i);
    }

    @Override
    protected void resetLayoutParams(ImageView iv_sendMsg, ImageView openMic, ImageView remoteMute) {
        if (null == iv_sendMsg || null == openMic || null == remoteMute) {
            return;
        }
        LayoutParams lp1 = (LayoutParams) iv_sendMsg.getLayoutParams();
        LayoutParams lp2 = (LayoutParams) openMic.getLayoutParams();
        LayoutParams lp3 = (LayoutParams) remoteMute.getLayoutParams();
        lp1.leftMargin = DisplayUtility.dp2px(getContext(), 15);
        int leftMargin = DisplayUtility.dp2px(getContext(), 12);
        lp2.leftMargin = leftMargin;
        lp3.leftMargin = leftMargin;
        iv_sendMsg.setLayoutParams(lp1);
        openMic.setLayoutParams(lp2);
        remoteMute.setLayoutParams(lp3);
    }

    @Override
    public void updateBottomView(boolean isOnMicMyself) {
        if (getVisibility() == View.INVISIBLE) {
            setVisibility(View.VISIBLE);
        }
        if (isOnMicMyself) {
            showSingleAudioRoomOwnerBottom();
        } else {
            showSingleAudioRoomNormalBottom();
        }
        RoomInfo mCurrentRoomInfo = BaseRoomServiceScheduler.getCurrentRoomInfo();
        boolean inputMsgBtnEnable = null != mCurrentRoomInfo && mCurrentRoomInfo.getPublicChatSwitch() == 0;
        setInputMsgBtnEnable(inputMsgBtnEnable);
        if (RoomDataManager.get().getAdditional() != null) {
            //1.0.1.1版本，单人音频房，更多操作按钮暂时限于大转盘抽奖，因此如果此开关时关闭的，[+]就没必要显示了
            iconMoreOpera.setVisibility(RoomDataManager.get().getAdditional().isBigWheelSwitch() ? View.VISIBLE : View.GONE);
        } else {
            iconMoreOpera.setVisibility(View.GONE);
        }
        sendGift.setVisibility(RoomDataManager.get().isRoomOwner() ? View.GONE : View.VISIBLE);
        updateAnchorRecvAudioConnCount(RoomDataManager.get().singleAudioConnReqNum);
    }

    /**
     * 更新主播接收到的语音连接申请数量
     *
     * @param count
     */
    public void updateAnchorRecvAudioConnCount(long count) {
        LogUtils.d(TAG, "updateAnchorRecvAudioConnCount-count");
        //避免后端通知下发逻辑混乱导致的客户端非房主也能显示的问题
        boolean showRecvReqNumTips = RoomDataManager.get().isRoomOwner() && count > 0;
        if (null != bltvRecvCount) {
            bltvRecvCount.setVisibility(showRecvReqNumTips ? View.VISIBLE : View.GONE);
            bltvRecvCount.setText(count > 99 ? "99+" : String.valueOf(count));
        }

        if (null != iv_anchor_mic_connect) {
            iv_anchor_mic_connect.setImageResource(showRecvReqNumTips ? R.drawable.icon_anchor_mic_recv : R.drawable.icon_mic_connect);
        }

        if (null != rl_anchor_mic_recv) {
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) rl_anchor_mic_recv.getLayoutParams();
            RelativeLayout.LayoutParams lp1 = (RelativeLayout.LayoutParams) iv_anchor_mic_connect.getLayoutParams();
            if (null == lp || null == lp1) {
                return;
            }
            lp.bottomMargin = DisplayUtility.dp2px(getContext(), showRecvReqNumTips ? 2 : 4);
//            lp.height = DisplayUtility.dp2px(getContext(),showRecvReqNumTips ? 40 : 41);
//            lp.leftMargin = DisplayUtility.dp2px(getContext(), showRecvReqNumTips ? 12 : 10);
            rl_anchor_mic_recv.setLayoutParams(lp);

            lp1.width = DisplayUtility.dp2px(getContext(), showRecvReqNumTips ? 32 : 33);
            lp1.height = lp.width;
            iv_anchor_mic_connect.setLayoutParams(lp1);
        }
    }

}
