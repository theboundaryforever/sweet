package com.yuhuankj.tmxq.ui.liveroom.nimroom.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.juxiao.library_ui.widget.DrawableTextView;
import com.tongdaxing.erban.libcommon.utils.ButtonUtils;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.nimroom.callback.BottomViewListenerWrapper;


/**
 * 2019年4月28日 00:49:54
 * 针对轰趴房
 * 1.房主，依次显示，发言、禁麦、声音、表情、私聊以及礼物按钮
 * 2.非房主，
 * 没有上麦，发言、禁麦、声音、以及礼物按钮
 * 上麦则，发言、禁麦、声音、表情、私聊以及礼物按钮
 * 分享房间按钮始终都是隐藏不可见的
 * <p>
 * 针对个人直播房
 * 1.房主，依次显示，发言、禁麦、声音、分享以及礼物按钮
 * 2.非房主，依次显示，说点什么，声音、分享以及礼物按钮
 * 表情、私聊（因没有上麦操作）、禁麦始终都是不可见的
 * <p>
 * xml默认设置为不可见的控件为，表情、私聊、分享
 * <p>
 * UI上，轰趴房父布局左边距为10dp，个人音频房左边距为15dp
 * 轰趴房btn之间的间距为10dp，个人音频房为12dp
 *
 * @author weihaitao
 * @date 2019年4月28日 00:41:09
 */

public abstract class AbstractBottomView extends LinearLayout implements View.OnClickListener {

    private final String TAG = AbstractBottomView.class.getSimpleName();
    public ImageView sendGift;
    public ImageView iconMoreOpera;
    protected TextView bltvRecvCount;
    protected View rl_anchor_mic_recv;
    protected ImageView iv_anchor_mic_connect;
    private BottomViewListenerWrapper wrapper;
    private ImageView openMic;
    private ImageView iv_sendMsg;
    private ImageView sendFace;
    private ImageView icon_room_share;
    private ImageView remoteMute;
    private RelativeLayout rlMsg;
    private ImageView ivMsgMark;
    private ImageView micInListIcon;
    private DrawableTextView dtvInputTips;
    private ImageView icon_fans_mic_connect;
    private boolean micInListOption;

    //是否可发言，标识
    private boolean isInputEnableForUser = false;

    public AbstractBottomView(Context context) {
        super(context);
        init();
    }

    public AbstractBottomView(Context context, AttributeSet attr) {
        super(context, attr);
        init();
    }

    public AbstractBottomView(Context context, AttributeSet attr, int i) {
        super(context, attr, i);
        init();
    }

    public void init() {
        inflate(getContext(), R.layout.layout_bottom_view, this);
        iconMoreOpera = findViewById(R.id.icon_more_opera);
        openMic = findViewById(R.id.icon_room_open_mic);
        iv_sendMsg = findViewById(R.id.iv_sendMsg);
        sendFace = findViewById(R.id.icon_room_face);
        sendGift = findViewById(R.id.icon_room_send_gift);
        icon_room_share = findViewById(R.id.icon_room_share);
        remoteMute = findViewById(R.id.icon_room_open_remote_mic);
        micInListIcon = findViewById(R.id.icon_room_mic_in_list);
        rlMsg = findViewById(R.id.rl_room_msg);
        ivMsgMark = findViewById(R.id.iv_room_msg_mark);
        dtvInputTips = findViewById(R.id.dtvInputTips);
        icon_fans_mic_connect = findViewById(R.id.icon_fans_mic_connect);
        rl_anchor_mic_recv = findViewById(R.id.rl_anchor_mic_recv);
        iv_anchor_mic_connect = findViewById(R.id.iv_anchor_mic_connect);
        bltvRecvCount = findViewById(R.id.bltvRecvCount);

        icon_fans_mic_connect.setOnClickListener(this);
        rl_anchor_mic_recv.setOnClickListener(this);

        iv_sendMsg.setOnClickListener(this);
        openMic.setOnClickListener(this);
        sendFace.setOnClickListener(this);
        sendGift.setOnClickListener(this);
        icon_room_share.setOnClickListener(this);
        remoteMute.setOnClickListener(this);
        micInListIcon.setOnClickListener(this);
        rlMsg.setOnClickListener(this);
        dtvInputTips.setOnClickListener(this);
        iconMoreOpera.setOnClickListener(this);
        setMicBtnEnable(false);
        setMicBtnOpen(false);
    }

    public void initState() {
//        Json configData = CoreManager.getCore(VersionsCore.class).getConfigData();
//        int micInListOption = configData.num("micInListOption");
//        this.micInListOption = micInListOption == 1;
//        int roomType = null == mCurrentRoomInfo ? RoomInfo.ROOMTYPE_HOME_PARTY : mCurrentRoomInfo.getType();
        resetLayoutParams(iv_sendMsg, openMic, remoteMute);
//        rlMsg.setVisibility(View.GONE);
    }

    protected abstract void resetLayoutParams(ImageView iv_sendMsg, ImageView openMic, ImageView remoteMute);

    /**
     * 公屏发言按钮状态
     *
     * @param usuable
     */
    public void setInputMsgBtnEnable(boolean usuable) {
        LogUtils.d(TAG, "setInputMsgBtnEnable-enable:" + usuable);
        isInputEnableForUser = usuable;
        iv_sendMsg.setImageResource(usuable ? R.drawable.icon_room_send_msg_usuable : R.drawable.icon_room_send_msg_unusuable);
    }

    public void setBottomViewListener(BottomViewListenerWrapper wrapper) {
        this.wrapper = wrapper;
    }

    public void setMicBtnEnable(boolean enable) {
        if (enable) {
            openMic.setClickable(true);
            openMic.setOnClickListener(this);
        } else {
            openMic.setClickable(false);
            openMic.setOnClickListener(null);
        }
    }

    /**
     * 新私聊消息提示
     *
     * @param isShow true 显示 false不显示
     */
    public void showMsgMark(boolean isShow) {
        if (ivMsgMark != null) {
            ivMsgMark.setVisibility(isShow ? View.VISIBLE : View.GONE);
        }
    }

    public void setMicBtnOpen(boolean isOpen) {
        if (isOpen) {
            openMic.setImageResource(R.drawable.icon_room_open_mic);
        } else {
            openMic.setImageResource(R.drawable.icon_room_close_mic);
        }
    }

    public void setRemoteMuteOpen(boolean isOpen) {
        if (isOpen) {
            remoteMute.setImageResource(R.drawable.icon_remote_mute_open);
        } else {
            remoteMute.setImageResource(R.drawable.icon_remote_mute_close);
        }
    }

    public void setShareBtnEnable(boolean enable) {
        icon_room_share.setVisibility(enable ? VISIBLE : GONE);
    }


    public abstract void updateBottomView(boolean isOnMicMyself);

    public void showMultiUpMicBottom() {
        /**
         * 针对轰趴房
         * 1.房主，依次显示，发言、禁麦、声音、表情、私聊以及礼物按钮
         * 2.非房主，
         * 上麦则，发言、禁麦、声音、表情、私聊以及礼物按钮
         */
        iv_sendMsg.setVisibility(View.VISIBLE);
        dtvInputTips.setVisibility(View.GONE);
        remoteMute.setVisibility(VISIBLE);
        icon_room_share.setVisibility(View.GONE);
        sendFace.setVisibility(VISIBLE);
        openMic.setVisibility(VISIBLE);
        rlMsg.setVisibility(VISIBLE);
//        micInListIcon.setVisibility(GONE);
        iconMoreOpera.setVisibility(GONE);
        icon_fans_mic_connect.setVisibility(View.GONE);
        rl_anchor_mic_recv.setVisibility(View.GONE);
        bltvRecvCount.setVisibility(View.GONE);
    }

    public void showMultiyDownMicBottom() {
        /**
         * 针对轰趴房 房主下麦即离开房间
         * 2.非房主，
         * 没有上麦，发言、排麦、声音、以及礼物按钮
         */
        iv_sendMsg.setVisibility(View.VISIBLE);
        dtvInputTips.setVisibility(View.GONE);
        remoteMute.setVisibility(VISIBLE);
        icon_room_share.setVisibility(View.GONE);
        sendFace.setVisibility(GONE);
        openMic.setVisibility(GONE);
        rlMsg.setVisibility(VISIBLE);
        iconMoreOpera.setVisibility(GONE);
//        micInListIcon.setVisibility(micInListOption ? VISIBLE : GONE);
        icon_fans_mic_connect.setVisibility(View.GONE);
        rl_anchor_mic_recv.setVisibility(View.GONE);
        bltvRecvCount.setVisibility(View.GONE);
    }

    public void showSingleAudioRoomOwnerBottom() {
        //1.房主，依次显示，发言、禁麦、声音、分享以及礼物按钮
        iv_sendMsg.setVisibility(View.VISIBLE);
        dtvInputTips.setVisibility(View.GONE);
        openMic.setVisibility(VISIBLE);
        remoteMute.setVisibility(VISIBLE);
        sendFace.setVisibility(GONE);
        micInListIcon.setVisibility(GONE);
        rlMsg.setVisibility(GONE);
        icon_room_share.setVisibility(View.VISIBLE);
        iconMoreOpera.setVisibility(VISIBLE);
        icon_fans_mic_connect.setVisibility(View.GONE);
        rl_anchor_mic_recv.setVisibility(View.VISIBLE);
        bltvRecvCount.setVisibility(View.GONE);
        if (null != getContext()) {
            int screenWidth = DisplayUtility.getScreenWidth(getContext());
            if (screenWidth < DisplayUtility.dp2px(getContext(), (128 + 15) + (33 + 12) + (33 + 12) + (33 + 12) + (33 + 12) + (48 + 10 + 15))) {
                LogUtils.d(TAG, "showSingleAudioRoomOwnerBottom-小屏幕，主播端视图，保持控件间隙不变");
                //恢复默认布局参数
                LinearLayout.LayoutParams remoteMuteLp = (LayoutParams) remoteMute.getLayoutParams();
                remoteMuteLp.leftMargin = (int) getContext().getResources().getDimension(R.dimen.buttom_left_view);
                remoteMute.setLayoutParams(remoteMuteLp);

                LinearLayout.LayoutParams icon_room_shareLp = (LayoutParams) icon_room_share.getLayoutParams();
                icon_room_shareLp.leftMargin = (int) getContext().getResources().getDimension(R.dimen.buttom_left_view);
                icon_room_share.setLayoutParams(icon_room_shareLp);

                LinearLayout.LayoutParams iconMoreOperaLp = (LayoutParams) iconMoreOpera.getLayoutParams();
                iconMoreOperaLp.leftMargin = (int) getContext().getResources().getDimension(R.dimen.buttom_left_view);
                iconMoreOpera.setLayoutParams(iconMoreOperaLp);

                LinearLayout.LayoutParams sendGiftLp = (LayoutParams) sendGift.getLayoutParams();
                sendGiftLp.leftMargin = (int) getContext().getResources().getDimension(R.dimen.buttom_gift_left);
//                sendGiftLp.rightMargin = (int) getContext().getResources().getDimension(R.dimen.buttom_left_right_normal);
                sendGift.setLayoutParams(sendGiftLp);
            }
        }
    }

    public void showSingleAudioRoomNormalBottom() {
        //2.非房主，依次显示，说点什么，声音、分享以及礼物按钮,
        //外部以上下麦来作为条件判断，房主下麦即为离开房间
        iv_sendMsg.setVisibility(View.GONE);
        dtvInputTips.setVisibility(View.VISIBLE);
        openMic.setVisibility(GONE);
        remoteMute.setVisibility(VISIBLE);
        sendFace.setVisibility(GONE);
        micInListIcon.setVisibility(GONE);
        icon_room_share.setVisibility(View.VISIBLE);
        rlMsg.setVisibility(GONE);
        iconMoreOpera.setVisibility(VISIBLE);
        icon_fans_mic_connect.setVisibility(View.VISIBLE);
        rl_anchor_mic_recv.setVisibility(View.GONE);
        bltvRecvCount.setVisibility(View.GONE);

        LinearLayout.LayoutParams dtvInputTipsLp = (LayoutParams) dtvInputTips.getLayoutParams();
        dtvInputTipsLp.leftMargin = (int) getContext().getResources().getDimension(R.dimen.buttom_left_margin_single);
        dtvInputTips.setLayoutParams(dtvInputTipsLp);

        if (null != getContext()) {
            int screenWidth = DisplayUtility.getScreenWidth(getContext());
            if (screenWidth < DisplayUtility.dp2px(getContext(), (116 + 11) + (33 + 12) + (33 + 12) + (33 + 12) + (33 + 12) + (48 + 10 + 15))) {
                LogUtils.d(TAG, "showSingleAudioRoomNormalBottom-屏幕宽度过窄，减小控件间隙");

                LinearLayout.LayoutParams remoteMuteLp = (LayoutParams) remoteMute.getLayoutParams();
                remoteMuteLp.leftMargin = (int) getContext().getResources().getDimension(R.dimen.buttom_left_normal_view);
                remoteMute.setLayoutParams(remoteMuteLp);

                LinearLayout.LayoutParams icon_room_shareLp = (LayoutParams) icon_room_share.getLayoutParams();
                icon_room_shareLp.leftMargin = (int) getContext().getResources().getDimension(R.dimen.buttom_left_normal_view);
                icon_room_share.setLayoutParams(icon_room_shareLp);

                LinearLayout.LayoutParams iconMoreOperaLp = (LayoutParams) iconMoreOpera.getLayoutParams();
                iconMoreOperaLp.leftMargin = (int) getContext().getResources().getDimension(R.dimen.buttom_left_normal_view);
                iconMoreOpera.setLayoutParams(iconMoreOperaLp);

                LinearLayout.LayoutParams icon_fans_mic_connectLp = (LayoutParams) icon_fans_mic_connect.getLayoutParams();
                icon_fans_mic_connectLp.leftMargin = (int) getContext().getResources().getDimension(R.dimen.buttom_left_normal_view);
                icon_fans_mic_connect.setLayoutParams(icon_fans_mic_connectLp);

                LinearLayout.LayoutParams sendGiftLp = (LayoutParams) sendGift.getLayoutParams();
                sendGiftLp.leftMargin = (int) getContext().getResources().getDimension(R.dimen.buttom_gift_normal_left);
//                sendGiftLp.rightMargin = (int) getContext().getResources().getDimension(R.dimen.buttom_left_right_normal);
                sendGift.setLayoutParams(sendGiftLp);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.icon_fans_mic_connect:
                if (ButtonUtils.isFastDoubleClick(R.id.icon_fans_mic_connect)) {
                    return;
                }
                //粉丝弹出语音连接申请弹框
                if (wrapper != null) {
                    wrapper.onFansReqAudioConnClick();
                }
                break;
            case R.id.rl_anchor_mic_recv:
                if (ButtonUtils.isFastDoubleClick(R.id.rl_anchor_mic_recv)) {
                    return;
                }
                //主播弹出查看语音连接申请列表弹框
                if (wrapper != null) {
                    wrapper.onAnchorAudioConnRecvClick();
                }
                break;
            case R.id.icon_more_opera:
                if (wrapper != null) {
                    wrapper.onRoomMoreOperaClick();
                }
                break;
            case R.id.icon_room_open_mic:
                if (wrapper != null) {
                    wrapper.onOpenMicBtnClick();
                }
                break;
            case R.id.dtvInputTips:
            case R.id.iv_sendMsg:
                if (wrapper != null && isInputEnableForUser) {
                    wrapper.onSendMsgBtnClick();
                }
                break;
            case R.id.icon_room_send_gift:
                if (wrapper != null) {
                    wrapper.onSendGiftBtnClick();
                }
                break;
            case R.id.icon_room_face:
                if (wrapper != null) {
                    wrapper.onSendFaceBtnClick();
                }
                break;
            case R.id.icon_room_share:
                if (wrapper != null) {
                    wrapper.onShareBtnClick();
                }
                break;
            case R.id.icon_room_open_remote_mic:
                if (wrapper != null) {
                    wrapper.onRemoteMuteBtnClick();
                }
                break;
            case R.id.icon_room_mic_in_list:
                if (wrapper != null) {
                    wrapper.onBuShowMicInList();
                }
                break;

            case R.id.rl_room_msg:
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                if (wrapper != null) {
                    wrapper.onPublicRoomMsgBtnClick();
                }
                break;
            default:
        }
    }
}
