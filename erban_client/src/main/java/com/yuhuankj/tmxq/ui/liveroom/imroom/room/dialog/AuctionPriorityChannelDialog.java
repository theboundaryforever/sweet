package com.yuhuankj.tmxq.ui.liveroom.imroom.room.dialog;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.noober.background.view.BLTextView;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomQueueInfo;
import com.tongdaxing.xchat_core.pay.IPayCore;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.base.dialog.AppDialogConstant;
import com.yuhuankj.tmxq.base.dialog.BaseAppMvpDialogFragment;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter.AuctionPriorityChannelPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IAuctionPriorityChannelView;

/**
 * 文件描述：排麦房的优先送礼通道弹框
 *
 * @auther：zwk
 * @data：2019/7/30
 */
@CreatePresenter(AuctionPriorityChannelPresenter.class)
public class AuctionPriorityChannelDialog extends BaseAppMvpDialogFragment<IAuctionPriorityChannelView, AuctionPriorityChannelPresenter> implements IAuctionPriorityChannelView, View.OnClickListener {
    private LinearLayout llContainer;
    private TextView tvOwner;
    private TextView tvHost;
    private TextView tvAndTag;
    private ImageView bltvAdd;
    private ImageView bltvMinus;
    private BLTextView bltvCardNum;
    private TextView tvGoldNum;
    private BLTextView bltvSend;
    private int cardCount = 2;
    private long hostUid = 0;

    @Override
    protected int getDialogStyle() {
        return AppDialogConstant.STYLE_BOTTOM_BACK_DARK;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_auction_priority_channel;
    }

    @Override
    protected void initView(View view) {
        llContainer = view.findViewById(R.id.ll_priority_card_container);
        tvOwner = view.findViewById(R.id.tv_auction_room_owner_nick);
        tvHost = view.findViewById(R.id.tv_auction_room_host_nick);
        tvAndTag = view.findViewById(R.id.tv_auction_priority_txt);
        bltvAdd = view.findViewById(R.id.iv_auction_priority_add);
        bltvMinus = view.findViewById(R.id.iv_auction_priority_minus);
        bltvCardNum = view.findViewById(R.id.bltv_auction_priority_num);
        tvGoldNum = view.findViewById(R.id.tv_auction_priority_user_gold);
        bltvSend = view.findViewById(R.id.bltv_auction_priority_send);
    }

    @Override
    protected void initListener(View view) {
        bltvAdd.setOnClickListener(this);
        bltvMinus.setOnClickListener(this);
        bltvSend.setOnClickListener(this);
    }

    @Override
    protected void initViewState() {
        bltvCardNum.setText(String.valueOf(cardCount));
        WalletInfo walletInfo = CoreManager.getCore(IPayCore.class).getCurrentWalletInfo();
        if (walletInfo != null) {
            tvGoldNum.setText(String.valueOf(walletInfo.getGoldNum()));
        }
        if (RoomDataManager.get().getmOwnerRoomMember() != null) {
            tvOwner.setText(StringUtils.limitStr(getContext(), RoomDataManager.get().getmOwnerRoomMember().getNick(), 6));
        }
        IMRoomQueueInfo roomQueueInfo = RoomDataManager.get().getRoomQueueMemberInfoByMicPosition(0);
        if (roomQueueInfo != null && roomQueueInfo.mChatRoomMember != null) {
            tvAndTag.setVisibility(View.VISIBLE);
            tvHost.setVisibility(View.VISIBLE);
            tvHost.setText(StringUtils.limitStr(getContext(), roomQueueInfo.mChatRoomMember.getNick(), 6));
            hostUid = roomQueueInfo.mChatRoomMember.getLongAccount();
        }
        getMvpPresenter().updateWalletInfo();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_auction_priority_add:
                cardCount = cardCount + 2;
                bltvCardNum.setText(String.valueOf(cardCount));
                break;
            case R.id.iv_auction_priority_minus:
                if (cardCount <= 2) {
                    toast("赠送的优先卡数量不能少于2个哦！");
                } else {
                    cardCount = cardCount - 2;
                    bltvCardNum.setText(String.valueOf(cardCount));
                }
                break;
            case R.id.bltv_auction_priority_send:
                if (RoomDataManager.get().getCurrentRoomInfo() != null) {
                    getMvpPresenter().sendProiorityCard(RoomDataManager.get().getCurrentRoomInfo().getRoomId(), hostUid, cardCount);
                }
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (llContainer != null) {
            llContainer.removeAllViews();
            llContainer = null;
        }
    }

    @Override
    public void showToast(String result) {
        SingleToastUtil.showToast(result);
        dismiss();
    }

    @Override
    public void updateWalletGoldCount(WalletInfo data) {
        if (data != null) {
            tvGoldNum.setText(String.valueOf(data.getGoldNum()));
        }
    }
}
