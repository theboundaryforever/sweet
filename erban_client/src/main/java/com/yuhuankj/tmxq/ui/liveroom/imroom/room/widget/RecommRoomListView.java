package com.yuhuankj.tmxq.ui.liveroom.imroom.room.widget;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tongdaxing.erban.libcommon.base.AbstractMvpRelativeLayout;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.liveroom.im.model.IMRoomMessageManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomEvent;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.IMRoomQueueInfo;
import com.tongdaxing.xchat_core.liveroom.im.model.bean.SingleAudioRoomEnitity;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.adapter.SingleRecommRoomAdapter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.presenter.RecommRoomListPresenter;
import com.yuhuankj.tmxq.ui.liveroom.imroom.room.view.IRecommRoomListView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.Disposable;

/**
 * 创建者      魏海涛
 * 创建时间    2019年4月25日 18:12:44
 * 描述        单人房，房主离线，推荐房间view
 * NOTE 仅一次请求检测判断
 *
 * @author dell
 */
@CreatePresenter(RecommRoomListPresenter.class)
public class RecommRoomListView extends AbstractMvpRelativeLayout<IRecommRoomListView, RecommRoomListPresenter<IRecommRoomListView>>
        implements IRecommRoomListView {

    private final String TAG = RecommRoomListView.class.getSimpleName();

    private SingleRecommRoomAdapter adapter;

    private RecyclerView rvFriendList;
    private View flClose;

    private Runnable delayRequestRunnable;
    private Disposable subscribe;

    public RecommRoomListView(Context context) {
        this(context, null);
    }

    public RecommRoomListView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RecommRoomListView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getViewLayoutId() {
        return R.layout.view_single_room_recomm_room_list;
    }

    @Override
    public void initialView(Context context) {
        setBackgroundColor(Color.parseColor("#80000000"));
        rvFriendList = findViewById(R.id.rvFriendList);
        flClose = findViewById(R.id.flClose);
        adapter = new SingleRecommRoomAdapter(new ArrayList<>());
        rvFriendList.setAdapter(adapter);
    }

    @Override
    protected void initListener() {
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        adapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            /**
             * callback method to be invoked when an item in this view has been
             * click and held
             *
             * @param adapter
             * @param view     The view whihin the ItemView that was clicked
             * @param position The position of the view int the adapter
             */
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                List<SingleAudioRoomEnitity> enitityList = adapter.getData();
                if (null != enitityList && enitityList.size() > position) {
                    SingleAudioRoomEnitity enitity = enitityList.get(position);
                    if (null != enitity) {
                        IMRoomEvent imRoomEvent = new IMRoomEvent();
                        imRoomEvent.setEvent(RoomEvent.ROOM_ENTER_OTHER_ROOM);
                        imRoomEvent.setRoomUid(enitity.getUid());
                        imRoomEvent.setRoomType(enitity.getType());
                        IMRoomMessageManager.get().getIMRoomEventObservable()
                                .onNext(imRoomEvent);
                    }
                    setVisibility(View.GONE);
                }
            }
        });
        flClose.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected void initViewState() {
        LogUtils.d(TAG, "initViewState");
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        LogUtils.d(TAG, "onAttachedToWindow");
        subscribe = IMRoomMessageManager.get().getIMRoomEventObservable().subscribe(this::onReceiveRoomEvent);
    }

    private void onReceiveRoomEvent(IMRoomEvent roomEvent) {
        if (roomEvent == null || roomEvent.getEvent() == RoomEvent.NONE) {
            return;
        }
        switch (roomEvent.getEvent()) {
            case IMRoomEvent.SOCKET_ROOM_ENTER_SUC:
                LogUtils.d(TAG, "onReceiveRoomEvent-socket进房成功，延迟刷新推荐房间列表，显示弹框");
                refreshRecommRoomList();
                break;
            default:
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        LogUtils.d(TAG, "onDetachedFromWindow");
        release();
    }

    public void refreshRecommRoomList() {
        //这里首先要判断是否在房间中
        if (RoomDataManager.get().isRoomOwner()) {
            LogUtils.d(TAG, "refreshRecommRoomList 房主，忽略");
            setVisibility(View.GONE);
            return;
        }
        if (RoomDataManager.get().hasRequestRecommRoomList) {
            LogUtils.d(TAG, "refreshRecommRoomList 已经将请求线程塞进队列，等等吧");
            return;
        }
        RoomDataManager.get().hasRequestRecommRoomList = true;
        LogUtils.d(TAG, "refreshRecommRoomList hasRequestRecommRoomList:" + RoomDataManager.get().hasRequestRecommRoomList);
        //房主不再房间，延迟一秒请求接口
//        if (delayRequestRunnable == null) {
//            delayRequestRunnable = new Runnable() {
//                @Override
//                public void run() {
                    IMRoomQueueInfo roomQueueInfo = RoomDataManager.get().getRoomQueueMemberInfoByMicPosition(-1);
                    if (roomQueueInfo.mChatRoomMember == null) {
                        LogUtils.d(TAG, "refreshRecommRoomList-->run 房主离线，请求推荐房间列表");
                        //房主已离线
                        getMvpPresenter().getRecommRoomList();
                        return;
                    }
                    LogUtils.d(TAG, "refreshRecommRoomList-->run 房主未离线，忽略该次请求");
//                }
//            };
//        }
//        postDelayed(delayRequestRunnable, 1000L);
    }

    public void release() {
        LogUtils.d(TAG, "release");
        if (null != delayRequestRunnable) {
            removeCallbacks(delayRequestRunnable);
        }
        if (subscribe != null) {
            subscribe.dispose();
            subscribe = null;
        }
    }

    @Override
    public void refreshRecommRoomList(List<SingleAudioRoomEnitity> enitityList) {
        LogUtils.d(TAG, "refreshRecommRoomList");
        adapter.setNewData(enitityList);
        setVisibility(View.VISIBLE);
    }
}
