package com.yuhuankj.tmxq;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Color;
import android.net.http.HttpResponseCache;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.support.multidex.MultiDex;
import android.text.TextUtils;
import android.util.Log;

import com.bumptech.glide.request.target.ViewTarget;
import com.growingio.android.sdk.collection.Configuration;
import com.growingio.android.sdk.collection.GrowingIO;
import com.juxiao.library_utils.DisplayUtils;
import com.microquation.linkedme.android.LinkedME;
import com.mob.MobApplication;
import com.netease.nim.uikit.NimUIKit;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.SDKOptions;
import com.netease.nimlib.sdk.StatusBarNotificationConfig;
import com.netease.nimlib.sdk.msg.MessageNotifierCustomization;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.pingplusplus.android.Pingpp;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.DefaultRefreshFooterCreater;
import com.scwang.smartrefresh.layout.api.DefaultRefreshHeaderCreater;
import com.scwang.smartrefresh.layout.api.RefreshFooter;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import com.tongdaxing.erban.libcommon.im.IMProCallBack;
import com.tongdaxing.erban.libcommon.im.IMReportBean;
import com.tongdaxing.erban.libcommon.utils.ChannelUtil;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SystemUtils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.DemoCache;
import com.tongdaxing.xchat_core.Env;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.liveroom.im.model.ReUsedSocketManager;
import com.tongdaxing.xchat_core.liveroom.im.model.RoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.uuzuche.lib_zxing.activity.ZXingLibrary;
import com.yuhuankj.tmxq.broadcast.ConnectiveChangedReceiver;
import com.yuhuankj.tmxq.thirdsdk.nim.ContactHelper;
import com.yuhuankj.tmxq.thirdsdk.nim.SessionHelper;
import com.yuhuankj.tmxq.ui.launch.middle.NimMiddleActivity;
import com.yuhuankj.tmxq.ui.launch.splash.SplashActivity;
import com.yuhuankj.tmxq.utils.CrashHandler;
import com.yuhuankj.tmxq.utils.SplashInitUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.plugins.RxJavaPlugins;

import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_OPEN_ROOM_NOTI;

/**
 * @author chenran
 * @date 2017/2/11
 */

public class XChatApplication extends MobApplication {
    public static final String TAG = "XChatApplication";
    public static SDKOptions sdkOptions;

    //static 代码段可以防止内存泄露
    static {
        SmartRefreshLayout.setDefaultRefreshHeaderCreater(new DefaultRefreshHeaderCreater() {
            @Override
            public RefreshHeader createRefreshHeader(Context context, RefreshLayout layout) {
//                layout.setPrimaryColorsId(R.color.colorPrimary, android.R.color.white);//全局设置主题颜色
//                layout.setLoadmoreFinished(false);
                layout.setEnableHeaderTranslationContent(false);
                MaterialHeader materialHeader = new MaterialHeader(context);
                materialHeader.setShowBezierWave(false);
                return materialHeader;
            }
        });
        SmartRefreshLayout.setDefaultRefreshFooterCreater(new DefaultRefreshFooterCreater() {
            @NonNull
            @Override
            public RefreshFooter createRefreshFooter(Context context, RefreshLayout layout) {
                return new ClassicsFooter(context).setDrawableSize(20);
            }
        });
    }

    private RefWatcher mRefWatcher;
    private MessageNotifierCustomization messageNotifierCustomization = new MessageNotifierCustomization() {
        @Override
        public String makeNotifyContent(String nick, IMMessage message) {
            if (message.getMsgType() == MsgTypeEnum.custom) {
                CustomAttachment customAttachment = (CustomAttachment) message.getAttachment();
                if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_OPEN_ROOM_NOTI) {
                    return message.getFromNick();
                }
            }
            // 采用SDK默认文案
            return "收到一条消息";
        }

        @Override
        public String makeTicker(String nick, IMMessage message) {
            if (message.getMsgType() == MsgTypeEnum.custom) {
                CustomAttachment customAttachment = (CustomAttachment) message.getAttachment();
                if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_OPEN_ROOM_NOTI) {
                    return message.getFromNick();
                }
            }
            // 采用SDK默认文案
            return "收到一条消息";
        }

        @Override
        public String makeRevokeMsgTip(String revokeAccount, IMMessage item) {
            return null;
        }
    };

    public static boolean inMainProcess(Context context) {
        String packageName = context.getPackageName();
        String processName = getProcessName(context);
        return packageName.equals(processName);
    }

    /**
     * 获取当前进程名
     *
     * @param context
     * @return 进程名
     */
    public static String getProcessName(Context context) {
        String processName = null;

        // ActivityManager
        ActivityManager am = ((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE));

        while (true) {
            for (ActivityManager.RunningAppProcessInfo info : am.getRunningAppProcesses()) {
                if (info.pid == android.os.Process.myPid()) {
                    processName = info.processName;
                    break;
                }
            }

            // go home
            if (!TextUtils.isEmpty(processName)) {
                return processName;
            }

            // take a rest and again
            try {
                Thread.sleep(100L);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * 获取进程号对应的进程名
     *
     * @param pid 进程号
     * @return 进程名
     */
    private static String getProcessName(int pid) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("/proc/" + pid + "/cmdline"));
            String processName = reader.readLine();
            if (!TextUtils.isEmpty(processName)) {
                processName = processName.trim();
            }
            return processName;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
        return null;
    }

    public static void initRxNet(Context context, String url) {
        SplashInitUtils.initRxNet(context, url);
    }

    private static void enabledStrictMode() {
        StrictMode.ThreadPolicy.Builder builder = new StrictMode.ThreadPolicy.Builder()
                //                .detectAll() //检测所有的想检测的
                .detectCustomSlowCalls(); //自定义的耗时调用
//                .detectDiskReads() /*检测磁盘读操作*/
//                .detectDiskWrites(); /*检测磁盘写入操作*/
        /*解决 java.lang.Throwable: Untagged socket detected; use TrafficStats.setThreadSocketTag() to track all network usage
         * 参考https://stackoverflow.com/questions/53195068/crashlytics-with-strictmode-enabled-detect-all-gives-untagged-socket-detected*/
//              .penaltyDeath()
//              .detectNetwork(); //检测网络
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            builder = builder.detectResourceMismatches();

        }

        StrictMode.setThreadPolicy(builder.penaltyLog().build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                        .detectAll()/*也可以采用detectAll()来检测所有想检测的东西*/
//                .detectActivityLeaks()/*检测Activity内存泄露*/
//                .detectLeakedClosableObjects()/*检测未关闭的Closable对象*/
//                .detectLeakedSqlLiteObjects() /*检测Sqlite对象是否关闭*/
                        .penaltyLog()
                        /*解决 java.lang.Throwable: Untagged socket detected; use TrafficStats.setThreadSocketTag() to track all network usage
                         * 参考https://stackoverflow.com/questions/53195068/crashlytics-with-strictmode-enabled-detect-all-gives-untagged-socket-detected*/
//                        .penaltyDeath()
                        .build()
        );
    }

    public static RefWatcher getRefWatcher(Context context) {
        XChatApplication application = (XChatApplication) context.getApplicationContext();
        return application.mRefWatcher;
    }

    public static long appStartTime = 0L;

    @Override
    public void onCreate() {
        super.onCreate();

        if (0L == SplashActivity.startTime) {
            appStartTime = System.currentTimeMillis();
            LogUtils.d("runTest", "onCreate-appStartTime:" + appStartTime);
        }

        SystemUtils.initHuaweiVerifier(this);
        SystemUtils.initVivoAbsListViewCrashHander();
        SystemUtils.fixOppoTimeoutException(false);

        //growingio
        GrowingIO.startWithConfiguration(this, new Configuration()
                .trackAllFragments()
                .setChannel(ChannelUtil.getChannel(this))
                .setHashTagEnable(true)
                .setDebugMode(BasicConfig.isDebug)
                .setTestMode(BasicConfig.isDebug));

        sdkOptions = getSDKOptions();
        NIMClient.init(this, null, sdkOptions);
        if (inMainProcess(this)) {
            LogUtils.d(TAG, "onCreate-LinkedME_DeviceId:" + LinkedME.getInstance(this).getDeviceId());
            RxJavaPlugins.setErrorHandler(new Consumer<Throwable>() {
                @Override
                public void accept(Throwable throwable) throws Exception {
                    throwable.printStackTrace();
                    Log.e(TAG, "the subscribe() method default error handler", throwable);
                }
            });
            //fixed: Glide Exception:"You must not call setTag() on a view Glide is targeting"
            ViewTarget.setTagId(R.id.tag_glide);

//            if (BasicConfig.isDebug) {
//                LogUtils.d(TAG, "onCreate-deviceInfo:" + UmengInteTestUtil.getDeviceInfo(this));
//            }
            init();
            CrashHandler.getInstance().init(this);
            if (BasicConfig.isDebug) {
                //设置debug模式下打印LinkedME日志
                LinkedME.getInstance(this).setDebug();
            } else {
                LinkedME.getInstance(this);
            }
            LinkedME.getInstance().setImmediate(false);
        }
        LogUtils.d("runTest", " onCreate end, time: " + (System.currentTimeMillis() - appStartTime));
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        // you must install multiDex whatever tinker is installed!
        MultiDex.install(base);
        // 安装tinker
//        Beta.installTinker();
    }

    /**
     * 云信配置信息
     *
     * @return
     */
    public SDKOptions getSDKOptions() {
        SDKOptions options = new SDKOptions();
        options.asyncInitSDK = true;
        options.checkManifestConfig = BasicConfig.isDebug;
        // 如果将新消息通知提醒托管给 SDK 完成，需要添加以下配置。否则无需设置。
        StatusBarNotificationConfig config = new StatusBarNotificationConfig();
        // 点击通知栏跳转到该Activity
        config.notificationEntrance = NimMiddleActivity.class;
//        config.notificationSmallIconId = R.drawable.icon_msg_normal;
        // 呼吸灯配置
        config.ledARGB = Color.GREEN;
        config.ledOnMs = 1000;
        config.ledOffMs = 1500;
        // 通知铃声的uri字符串--三星等手机失效
        config.notificationSound = "android.resource://" + getPackageName() + "/raw/ring_on_receive_msg"/*+R.raw.ring_on_receive_msg*/;
        config.ring = false;
        config.vibrate = true;
        options.statusBarNotificationConfig = config;
        // 定制通知栏提醒文案（可选，如果不定制将采用SDK默认文案）
        options.messageNotifierCustomization = messageNotifierCustomization;

        options.appKey = Constants.nimAppKey;

        // 配置保存图片，文件，log 等数据的目录
        // 如果 options 中没有设置这个值，SDK 会使用下面代码示例中的位置作为 SDK 的数据目录。
        // 该目录目前包含 log, file, image, audio, video, thumb 这6个目录。
        // 如果第三方 APP 需要缓存清理功能， 清理这个目录下面个子目录的内容即可。
        String sdkPath = null;
        try {
            sdkPath = Environment.getExternalStorageDirectory() + "/" + this.getPackageName() + "/nim";
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        options.sdkStorageRootPath = sdkPath;

        // 配置是否需要预下载附件缩略图，默认为 true
        options.preloadAttach = true;

        // 配置附件缩略图的尺寸大小。表示向服务器请求缩略图文件的大小
        // 该值一般应根据屏幕尺寸来确定， 默认值为 Screen.width / 2
        options.thumbnailSize = DisplayUtils.getScreenWidth(this) / 2;
//        // save cache，留做切换账号备用
        DemoCache.setNotificationConfig(config);
        return options;
    }

    private void init() {
        initNimUIKit();

        BasicConfig.INSTANCE.setAppContext(getApplicationContext());

        //切换生产坏境和测试环境 true/测试环境 false/生产环境
        BasicConfig.INSTANCE.setDebuggable(BasicConfig.isDebug);
        BasicConfig.INSTANCE.setChannel(ChannelUtil.getChannel(this));
        Env.instance().init();
        BasicConfig.INSTANCE.setRootDir(Constants.ERBAN_DIR_NAME);
        BasicConfig.INSTANCE.setLogDir(Constants.LOG_DIR);
        BasicConfig.INSTANCE.setConfigDir(Constants.CONFIG_DIR);
        BasicConfig.INSTANCE.setVoiceDir(Constants.VOICE_DIR);
        BasicConfig.INSTANCE.setCacheDir(Constants.CACHE_DIR);
        try {
            File cacheDir = new File(Constants.ERBAN_DIR_NAME, "http");
            boolean newFile = false;
            if (!cacheDir.exists()){
                newFile = cacheDir.createNewFile();
            }else {
                newFile = true;
            }
            if (newFile) {
                HttpResponseCache.install(cacheDir, 1024 * 1024 * 128);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Pingpp.DEBUG = BasicConfig.isDebug;

        // url
        UriProvider.initDevUri(UriProvider.JAVA_WEB_URL);
        initRxNet(BasicConfig.INSTANCE.getAppContext(), UriProvider.JAVA_WEB_URL);
        //内存泄漏监控
        setupLeakCanary();
        ConnectiveChangedReceiver.getInstance().init(getApplicationContext());
        ZXingLibrary.initDisplayOpinion(this);
        LogUtils.setLogSwitch(BasicConfig.isDebug);
    }

    private void initNimUIKit() {
        NimUIKit.init(this);
        // 可选定制项
        // 注册定位信息提供者类（可选）,如果需要发送地理位置消息，必须提供。
        // demo中使用高德地图实现了该提供者，开发者可以根据自身需求，选用高德，百度，google等任意第三方地图和定位SDK。
//        NimUIKit.setLocationProvider(new NimDemoLocationProvider());

        // 会话窗口的定制: 示例代码可详见demo源码中的SessionHelper类。
        // 1.注册自定义消息附件解析器（可选）
        // 2.注册各种扩展消息类型的显示ViewHolder（可选）
        // 3.设置会话中点击事件响应处理（一般需要）
        SessionHelper.init();

        // 通讯录列表定制：示例代码可详见demo源码中的ContactHelper类。
        // 1.定制通讯录列表中点击事响应处理（一般需要，UIKit 提供默认实现为点击进入聊天界面)
        ContactHelper.init();
    }

    private void setupLeakCanary() {
        if (!BasicConfig.INSTANCE.isDebuggable()) {
            return;
        }
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanaryWithoutDisplay for heap analysis.
            // You should not init your app in this process.
            return;
        }

//        if (LeakCanaryWithoutDisplay.isRunningMonkey) {
//            //方法1 - 仅适用于debug模式，release模式下没有包LeakCanaryInternals
//            mRefWatcher = LeakCanary.install(this);
//            LeakCanaryInternals.setEnabled(this, DisplayLeakActivity.class, false);
////            方法2
////            mRefWatcher = LeakCanaryWithoutDisplay.install(this);
//        } else {
        mRefWatcher = LeakCanary.install(this);
//        }

    }

    /**
     * 退出房间
     */
    public void exitRoom() {
        RoomInfo currentRoom = RoomDataManager.get().getCurrentRoomInfo();
        if (currentRoom == null) {
            return;
        }
        RoomDataManager.get().release();
        ReUsedSocketManager.get().exitRoom(currentRoom.getRoomId(), new IMProCallBack() {
            @Override
            public void onSuccessPro(IMReportBean imReportBean) {
            }

            @Override
            public void onError(int errorCode, String errorMsg) {
            }
        });
    }

}
