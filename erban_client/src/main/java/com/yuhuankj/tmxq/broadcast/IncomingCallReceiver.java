package com.yuhuankj.tmxq.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;

import com.tencent.bugly.crashreport.BuglyLog;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.xchat_core.im.state.IPhoneCallStateCore;

/**
 * Created by zhouxiangfeng on 2017/5/31.
 */

public class IncomingCallReceiver extends BroadcastReceiver{

    private final String TAG = IncomingCallReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (TelephonyManager.ACTION_PHONE_STATE_CHANGED.equals(action)) {
            final String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
            BuglyLog.d(TAG, "onReceive-action:" + action + " state:" + state);
            //接听电话会抛空指针异常，这里暂时捕获，后面定位
            if (null != CoreManager.getCore(IPhoneCallStateCore.class)) {
                CoreManager.getCore(IPhoneCallStateCore.class).callStateChanged(state);
            }
        }
    }
}
