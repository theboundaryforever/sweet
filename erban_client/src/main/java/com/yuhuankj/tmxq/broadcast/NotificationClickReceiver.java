package com.yuhuankj.tmxq.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.ui.launch.empty.EmptyActivity;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;

/**
 * Created by chenran on 2017/11/16.
 */

public class NotificationClickReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        //跳转之前要处理的逻辑
        RoomInfo roomInfo = RoomServiceScheduler.getCurrentRoomInfo();
        if (roomInfo != null) {
//            RoomServiceScheduler.getInstance().enterRoomFromService(context, roomInfo.getUid(), roomInfo.getType());
            EmptyActivity.start(context, roomInfo.getUid(), roomInfo.getType());
        } else {
            //发广播通知MainActivity打开相应页面
//            if (CommonWebViewActivity.isGameRunning) {
//                LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(MainActivity.ACTION_OPEN_WEB));
//            } else if (MainActivity.isLinkMicroing) {
//                LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(MainActivity.ACTION_OPEN_P2P));
//            }
        }
    }
}
