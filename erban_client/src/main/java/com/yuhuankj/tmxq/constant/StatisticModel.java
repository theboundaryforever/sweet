package com.yuhuankj.tmxq.constant;

import android.content.Context;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.ChannelUtil;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.HashMap;
import java.util.Map;

public class StatisticModel {
    //参数长度不能超过128个字符
    /**
     * 登录注册补全信息
     */
    public static final String EVENT_ID_LOGIN_REG_ADD_INFO = "login_reg_add_info";
    public static final String EVENT_ID_HOT_RECOMMEND_ROOM = "hot_recommend_room";
    public static final String EVENT_ID_HOT_ROOM = "hot_room";
    public static final String EVENT_ID_HOT_GREEN_ROOM = "hot_green_room";
    public static final String EVENT_ID_FIND_BROADCAST_MAKE_FRIEND = "find_broadcast_make_friend";
    private static final String EVENT_ID_MAIN_TAB = "main_tab_";
    private static final String EVENT_ID_HOT_BANNER = "hot_banner_";
    private static final String EVENT_ID_HOT_ICON = "hot_icon_";
    private static final String EVENT_ID_RECHARGE_VALUE = "recharge_";
    public static final String EVENT_ID_RECHARGE_WX = "recharge_wx";
    public static final String EVENT_ID_RECHARGE_ALIPAY = "recharge_alipay";
    public static final String EVENT_ID_RECHARGE_SUCCESS = "recharge_success";
    public static final String EVENT_ID_ME_MINE_ROOM = "me_my_room";
    public static final String EVENT_ID_USERINFO_FINDTA = "userinfo_findta";
    public static final String EVENT_ID_USERINFO_HISHOME = "userinfo_hishome";
    public static final String EVENT_ID_USERINFO_MESSAGE = "userinfo_message";
    public static final String EVENT_ID_USERINFO_ATTENTION = "userinfo_attention";
    public static final String EVENT_ID_USERINFO_REPORT = "userinfo_report";

    public static final String EVENT_ID_FOLLOW_ALREADY_ATTENTION_ROOM = "follow_already_attention_user";
    public static final String EVENT_ID_FOLLOW_NOT_ATTENTION_ROOM = "follow_not_attention_user";
    public static final String EVENT_ID_FOLLOW_NOT_ATTENTION_ATTENTION = "follow_not_attention_attention";
    public static final String EVENT_ID_SEARCH_RECOM_ROOM = "search_recom_room";
    public static final String EVENT_ID_SEARCH_RECOM_ATTENTION = "search_recom_attention";
    public static final String EVENT_ID_SEARCH_LIST_ROOM = "search_list_room";
    public static final String EVENT_ID_MAIN_TAB_MYHOME = "main_tab_myhome";
    public static final String EVENT_ID_ROOM_UP_MIC = "room_up_mic";
    public static final String EVENT_ID_ROOM_OPEN_GIFT = "room_open_gift";
    public static final String EVENT_ID_ROOM_OPEN_MSG = "room_open_msg";
    public static final String EVENT_ID_ROOM_OPEN_SHARE = "room_open_share";
    public static final String EVENT_ID_ROOM_ENTER_PK_SETTING = "room_enter_pk_setting";
    public static final String EVENT_ID_ROOM_OPEN_PK = "room_open_pk";
    public static final String EVENT_ID_ROOM_CLOSE_PK = "room_close_pk";
    public static final String EVENT_ID_ROOM_OPEN_PUBLIC_SCREEN = "room_open_public_screen";
    public static final String EVENT_ID_ROOM_CLOSE_PUBLIC_SCREEN = "room_close_public_screen";
    public static final String EVENT_ID_ROOM_GIFT_RECHARGE = "room_gift_recharge";
    public static final String EVENT_ID_ROOM_OPEN_BOX = "room_open_box";
    public static final String EVENT_ID_ROOM_BOX_RECHARGE = "room_box_recharge";
    public static final String EVENT_ID_ROOM_BOX_LUCK_1 = "room_box_luck_1";
    public static final String EVENT_ID_ROOM_BOX_LUCK_10 = "room_box_luck_10";
    public static final String EVENT_ID_ROOM_BOX_RULE = "room_box_rule";
    public static final String EVENT_ID_ROOM_PUBLIC_SCREEN_ATTENTION = "room_public_screen_attention";
    public static final String EVENT_ID_ROOM_PUBLIC_SCREEN_SHARE = "room_public_screen_share";
    public static final String EVENT_ID_ROOM_GIFT_USERINFO = "room_gift_userinfo";
    public static final String EVENT_ID_ROOM_CONTRIBUTIONI_LIST = "room_contribution_list";
    public static final String EVENT_ID_ROOM_OPEN_MUSIC_CONTROL_PANEL = "room_open_music_control_panel";
    public static final String EVENT_ID_ROOM_HOT_MUSIC_DOWNLOAD = "room_hot_music_download";
    public static final String EVENT_ID_ROOM_HOT_MUSIC_SEARCH = "room_hot_music_search";
    public static final String EVENT_ID_ROOM_HOT_MUSIC_SEARCH_DOWNLOAD = "room_hot_music_search_download";
    public static final String EVENT_ID_ROOM_ENTER = "room_enter";

    public static final String EVENT_ID_ENTER_SPROUT = "enter_sprout";
    public static final String EVENT_ID_SPROUT_NEW_VIEW_USERINFO = "sprout_new_view_userinfo";
    public static final String EVENT_ID_SPROUT_NEW_ENTER_ROOM = "sprout_new_enter_room";
    public static final String EVENT_ID_SPROUT_NEWARBY_VIEW_USERINFO = "sprout_nearby_view_userinfo";
    public static final String EVENT_ID_SPROUT_NEWARBY_ENTER_ROOM = "sprout_nearby_enter_room";

    public static final String EVENT_ID_PLAY_TOGETHER_GAMELIST_INVITE = "play_together_gamelist_invite";
    public static final String EVENT_ID_PLAY_TOGETHER_GAMELIST_1 = "play_together_gamelist_1";
    public static final String EVENT_ID_PLAY_TOGETHER_GAMELIST_2 = "play_together_gamelist_2";
    public static final String EVENT_ID_PLAY_TOGETHER_GAMELIST_3 = "play_together_gamelist_3";
    public static final String EVENT_ID_MAIN_TAB_CHANCE_MEETING = "main_tab_chance_meeting";
    public static final String EVENT_ID_MAIN_TAB_CHANCE_GAME_HOME = "main_tab_chance_game_home";
    public static final String EVENT_ID_CHANCE_MEETING_AUTO_PLAY_CLOSE = "chance_meeting_auto_play_close";
    public static final String EVENT_ID_CHANCE_MEETING_AUTO_PLAY_OPEN = "chance_meeting_auto_play_open";
    public static final String EVENT_ID_CHANCE_MEETING_ATTENTION = "chance_meeting_attention";
    public static final String EVENT_ID_CHANCE_MEETING_ATTENTION_CANCEL = "chance_meeting_attention_cancel";
    public static final String EVENT_ID_CHANCE_MEETING_MSG = "chance_meeting_msg";
    public static final String EVENT_ID_CHANCE_MEETING_UPLOAD_AUDIO_CANCEL = "chance_meeting_upload_audio_cancel";
    public static final String EVENT_ID_CHANCE_MEETING_UPLOAD_AUDIO_UPLOAD = "chance_meeting_upload_audio_upload";
    public static final String EVENT_ID_CHANCE_MEETING_AUDIO_PLAY = "chance_meeting_audio_play";
    public static final String EVENT_ID_USERINFO_AUDIO_UPLOAD_CLICK = "userinfo_audio_upload_click";
    public static final String EVENT_ID_AUDIO_UPLOAD_CHANGE_ANOTHER = "audio_upload_change_another";
    public static final String EVENT_ID_AUDIO_UPLOAD_UPLOAD_CLICK = "audio_upload_upload_click";
    public static final String EVENT_ID_AUDIO_UPLOAD_LISTEN_CLICK = "audio_upload_listen_click";
    public static final String EVENT_ID_AUDIO_UPLOAD_DELETE_CLICK = "audio_upload_delete_click";
    public static final String EVENT_ID_PRIVATE_MSG_OPEN_RECORD = "private_msg_open_record";
    public static final String EVENT_ID_PRIVATE_MSG_OPEN_GIFT_DIALOG = "private_msg_open_gift_dialog";
    public static final String EVENT_ID_PRIVATE_MSG_OPEN_GAME_LIST = "private_msg_open_game_list";
    public static final String EVENT_ID_PRIVATE_MSG_GAME_INVITE_CLICK = "private_msg_game_invite_click";

    public static final String EVENT_ID_ENTER_ADD_USERINFO = "enter_add_user_info";
    public static final String EVENT_ID_ENTER_LOGIN = "enter_login";
    public static final String EVENT_ID_ENTER_CHANCE_MEETING_ADMIRE = "chance_meeting_admire";
    public static final String EVENT_ID_ENTER_CHANCE_MEETING_ADMIRE_CANCEL = "chance_meeting_admire_cancel";
    //贵族统计事件
    public static final String EVENT_ID_PAYFOR_NOBLE = "payfor_noble";
    public static final String EVENT_ID_ABOUT_NOBLE = "about_noble";
    public static final String EVENT_ID_BECOME_NOBLE = "become_noble";
    //任务统计事件
    public static final String EVENT_ID_TASK_CHOUJIANG_TASKCENTER = "task_choujiang_taskcenter";//任务中心去抽奖
    public static final String EVENT_ID_TASK_DO_TASK = "task_do_task";//去完成任务
    public static final String EVENT_ID_TASK_TO_SIGNIN = "task_to_signin";//首页去签到
    public static final String EVENT_ID_TASK_CHOUJIANG_HOME = "task_choujiang_home";//首页去抽奖

    //房间-banner-声鉴卡
    public static final String EVENT_ID_HOT_BANNER_VOICEAUTHCARD = "hot_banner_voice_auth_card";
    //发现-banner-声鉴卡
    public static final String EVENT_ID_FIND_BANNER_VOICEAUTHCARD = "find_banner_voice_auth_card";
    //声鉴卡-未上传
    public static final String EVENT_ID_VOICEAUTHCARD_1 = "voice_auth_card_1";
    //声鉴卡-结果页
    public static final String EVENT_ID_VOICEAUTHCARD_2 = "voice_auth_card_2";
    //偶遇-上传录音提示-跳转我的声音界面
    public static final String EVENT_ID_CHANCE_MEETING_TIPS_MYVOICE = "chance_meeting_tips_myvoice";
    //偶遇-左上角按钮-跳转我的声音界面
    public static final String EVENT_ID_CHANCE_MEETING_BTN_MYVOICE = "chance_meeting_btn_myvoice";
    //声音编辑页-声鉴卡
    public static final String EVENT_ID_VOICEEDIT_ENTER_VOICEAUTHCARD = "voice_edit_enter_voice_auth_card";

    //登录注册相关统计
    public static final String EVENT_ID_CLICK_PROTOCAL_LOGIN = "click_protocal_login";// 点击用户协议的次数
    public static final String EVENT_ID_CLICK_PHONE_LOGIN = "click_phone_login";// 点击手机登录的次数
    public static final String EVENT_ID_CLICK_QQ_LOGIN = " click_qq_login";//点击QQ登录的次数
    public static final String EVENT_ID_CLICK_WEIXIN_LOGIN = "click_weixin_login";// 点击微信登录的次数
    public static final String EVENT_ID_CLICK_LOGIN = "click_login";// 点击登录的次数
    public static final String EVENT_ID_CLICK_REGISTE = " click_registe";// 点击注册的次数
    public static final String EVENT_ID_CLICK_LOGIN_NOW = "click_login_now";// 点击立即登录按钮的次数
    public static final String EVENT_ID_CLICK_REGISTE_NOW = "click_registe_now";// 点击立即注册按钮次数
    public static final String EVENT_ID_CLICK_GIVEUP_REGISTE = "click_giveup_registe";// 点击放弃注册的次数
    public static final String EVENT_ID_CLICK_EDIT_PIC = "click_edit_pic";// 点击编辑头像的次数
    public static final String EVENT_ID_CLICK_CHOOSE_CITY = "click_choose_city";// 点击切换城市的次数
    public static final String EVENT_ID_CLICK_CHOOSE_BIRTHDAY = "click_choose_birthday";// 点击选择生日的次数
    public static final String EVENT_ID_CLICK_NEXT = "click_next";// 点击下一步的次数
    public static final String EVENT_ID_CLICK_PRESERVE = "click_preserve";// 点击保存的次数

    //交友速配相关统计
    public static final String EVENT_ID_CLICK_QM_START_MATCHING = "qm_start_matching";//点击开始匹配的次数
    public static final String EVENT_ID_CLICK_QM_CANCEL_MATCHING = "qm_cancel_matching";//      点击取消匹配的次数
    public static final String EVENT_ID_CLICK_QM_WALK_AROUND = "qm_walk_around";// 点击到处逛逛的次数
    public static final String EVENT_ID_CLICK_QM_WANT_CONTINUE_MATCHING = "qm_want_continue_matching";//    点击我还想继续匹配的次数
    public static final String EVENT_ID_CLICK_QM_CHANGE_MEETING = "qm_change_meeting";//点击偶遇好声音的次数
    public static final String EVENT_ID_CLICK_QM_RULER = "qm_ruler";//   点击规则页的次数
    public static final String EVENT_ID_CLICK_QM_CAIQUAN = "qm_caiquan";// 点击剪刀石头的次数
    public static final String EVENT_ID_CLICK_QM_TALK_TOPIC = "qm_talk_topic";//     点击话题夹的次数
    public static final String EVENT_ID_CLICK_QM_EMOJI = "qm_emoji";// 点击表情包的次数
    public static final String EVENT_ID_CLICK_QM_SEND_FLOWER = "qm_send_flower";//  点击送花的次数
    public static final String EVENT_ID_CLICK_QM_RENEW = "qm_renew";// 点击续费得次数
    public static final String EVENT_ID_CLICK_QM_DISLIKE = "qm_dislike";//    点击哭脸的次数
    public static final String EVENT_ID_CLICK_QM_LIKE = "qm_like";// 点击点赞的次数
    public static final String EVENT_ID_CLICK_QM_CONTINUE_MATCHING = "qm_continue_matching";//    点击继续匹配的次数
    public static final String EVENT_ID_CLICK_QM_ENTRANCE = "qm_entrance";//点击交友速配入口的次数
    public static final String EVENT_ID_CLICK_QM_ENTER_USERPAGE = "qm_enter_userpage";//匹配完成页进入到个人主页
    public static final String EVENT_ID_CLICK_QM_USERPAGE_CHAT = "qm_userpage_chat";//匹配进入个人主页再进入私聊页

    //附近相关统计
    public static final String EVENT_ID_CLICK_NEARBY_ENTER = "nearby_enter";//进入附近的次数
    public static final String EVENT_ID_CLICK_NEARBY_SELECT_GENDER = "nearby_select_gender";//点击附近筛选性别的次数
    public static final String EVENT_ID_CLICK_NEARBY_ENTER_USERPAGE = "nearby_enter_userpage";//点击附近的人的次数
    public static final String EVENT_ID_CLICK_NEARBY_P2PCHAT = "nearby_p2pchat";//从附近进入资料页点击私聊的次数
    public static final String EVENT_ID_CLICK_NEARBY_P2PCHAT_SEND = "nearby_p2pchat_send";//从附近进入资料也点击私聊后发送消息的次数

    /**
     * 萌新大礼包弹框--领取按钮
     */
    public static final String EVENT_ID_NEWS_GIFT_TAKE = "news_gift_pkg_take";
    /**
     * 萌新大礼包--领取结果弹框，去找他
     */
    public static final String EVENT_ID_NEWS_GIFT_FINDTA = "news_gift_pkg_findTa";
    /**
     * 连续签到奖励弹框--领取、打开按钮
     */
    public static final String EVENT_ID_SIGN_IN_DAYS_TAKE = "sign_in_award_days_take";
    /**
     * 萌新大礼包期间，任务中心弹出的签到弹框，领取按钮
     */
    public static final String EVENT_ID_TASK_CENTER_SIGN_IN_TAKE = "task_center_sign_in_take";

    /**
     * 新首页，直播间列表，点击事件
     */
    public static final String EVENT_ID_HOME_LIST_LIVEROOM_CLICK = "home_list_liveroom_click";
    /**
     * 新首页，聊天室列表，点击事件
     */
    public static final String EVENT_ID_HOME_LIST_CHATROOM_CLICK = "home_list_chatroom_click";

    /**
     * 点击召集令的次数
     */
    public static final String EVENT_ID_ROOM_CALL_UP_CLICK = "room_call_up_click";

    /**
     * 成功发布召集令发布的次数
     */
    public static final String EVENT_ID_ROOM_CALL_UP_SEND_SUC = "room_call_up_send_suc";

    /**
     * 点击召集令邀请接受按钮的次数
     */
    public static final String EVENT_ID_HOME_CALL_UP_INVITE_ACCEPT = "home_call_up_invite_accept";

    /**
     * 点击召集令邀请拒绝按钮的次数
     */
    public static final String EVENT_ID_HOME_CALL_UP_INVITE_REFUSE = "home_call_up_invite_refuse";

    /**
     * 点击广场召集令邀请的次数
     */
    public static final String EVENT_ID_BROADCAST_CALL_UP_MSG_CLICK = "broadcast_call_up_msg_click";

    /**
     * 交友房点击加号-邀请好友的点击次数
     */
    public static final String EVENT_ID_ROOM_MORE_INVITE_CLICK = "room_more_invite_click";

    /**
     * 个人房点[赞]控件的次数
     */
    public static final String EVENT_ID_ROOM_ADMIRE_CLICK = "room_admire_click";
    /**
     * 多人房[赞]控件的点击次数
     */
    public static final String EVENT_ID_ROOM_MULIT_ADMIRE_CLICK = "room_mulit_admire_click";
    /**
     * 个人房点击活动的次数
     */
    public static final String EVENT_ID_ROOM_BANNER_CLICK = "room_banner_click";
    public static final String EVENT_ID_MULTI_ROOM_BANNER_CLICK = "multi_room_banner_click";
    public static final String EVENT_ID_HOME_PARTY_ROOM_BANNER_CLICK = "homeparty_room_banner_click";
    /**
     * 个人房点击右上角[关注]的次数
     */
    public static final String EVENT_ID_ROOM_ATTENTION_CLICK = "room_attention_click";

    /**
     * 交友房点击加号-邀请好友-好友列表[邀请]按钮的点击次数
     */
    public static final String EVENT_ID_ROOM_MORE_INVITE_INVITE_CLICK = "room_more_invite_invite_click";

    /**
     * 新首页-右上角-跳转任务中心
     */
    public static final String EVENT_ID_NEW_HOME_GOTO_TASK_CENTER = "new_home_goto_task_center";


    public String getRechargeValueClickEventId(int price) {
        return EVENT_ID_RECHARGE_VALUE + price;
    }

    public String getMainTabChangeToEventId(int position) {
        return EVENT_ID_MAIN_TAB + position;
    }

    public String getMainHotBannerClickEventId(int position) {
        return EVENT_ID_HOT_BANNER + position;
    }

    public String getMainHotIconClickEventId(int position) {
        return EVENT_ID_HOT_ICON + position;
    }

    private final String TAG = StatisticModel.class.getSimpleName();

    private static Map<String, String> map = new HashMap<>();

    private StatisticModel() {
    }

    private static StatisticModel instance = null;

    public static StatisticModel getInstance() {
        if (null == instance) {
            instance = new StatisticModel();
        }
        return instance;
    }

    /**
     * 友盟统计事件通用基础属性参
     *
     * @return
     */
    public Map<String, String> getUMAnalyCommonMap(Context context) {
        if (null == map) {
            map = new HashMap<>();
        }
        map.clear();
        //是否新注册用户，条件为注册时间少于7天
        map.put("isNewRegUser", String.valueOf(isNewRegisterUser()));
        map.put("experLevel", String.valueOf(getUserExperLevelSection()));
        map.put("appChannel", ChannelUtil.getChannel(context));
        map.put("gender", getGenderDescr());
        return map;
    }

    /**
     * 性别描述
     *
     * @return
     */
    public String getGenderDescr() {
        String gender = "未知";
        UserInfo cacheLoginUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (null != cacheLoginUserInfo && cacheLoginUserInfo.getGender() != 0) {
            gender = cacheLoginUserInfo.getGender() == 1 ? "男" : "女";
        }
        return gender;
    }

    /**
     * 获取安装包渠道信息
     *
     * @param context
     * @return
     */
    public String getChannel(Context context) {
        return ChannelUtil.getChannel(context);
    }

    /**
     * 获取用户财富等级区间
     *
     * @return
     */
    public String getUserExperLevelSection() {
        String levelSection = "0";
        UserInfo cacheLoginUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (null != cacheLoginUserInfo) {
            int experLevel = cacheLoginUserInfo.getExperLevel();
            if (0 >= experLevel) {
                levelSection = "0";
            } else if (experLevel <= 10) {
                levelSection = "1-10";
            } else if (experLevel <= 20) {
                levelSection = "11-20";
            } else if (experLevel <= 30) {
                levelSection = "21-30";
            } else if (experLevel <= 40) {
                levelSection = "31-40";
            } else if (experLevel <= 50) {
                levelSection = "41-50";
            } else if (experLevel <= 60) {
                levelSection = "51-60";
            } else if (experLevel <= 70) {
                levelSection = "61-70";
            } else if (experLevel <= 80) {
                levelSection = "71-80";
            } else if (experLevel <= 90) {
                levelSection = "81-90";
            } else if (experLevel <= 100) {
                levelSection = "91-100";
            } else {
                levelSection = "101";
            }
            LogUtils.d(TAG, "getUserExperLevelSection-experLevel:" + experLevel + " levelSection:" + levelSection);
        }
        return levelSection;
    }

    public boolean isNewRegisterUser() {
        boolean isNewRegUser = true;
        UserInfo cacheLoginUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (null != cacheLoginUserInfo) {
            //7天内注册为新用户，否则为老用户
            long createTime = cacheLoginUserInfo.getCreateTime();
            long delTime = System.currentTimeMillis() - createTime;
            isNewRegUser = createTime > 0L && delTime < 604800000L;
            LogUtils.d(TAG, "isNewRegisterUser-createTime:" + createTime + " delTime:" + delTime + " isNewRegUser:" + isNewRegUser);
        }
        return isNewRegUser;
    }

}
