package com.yuhuankj.tmxq.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.tencent.bugly.crashreport.BuglyLog;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.xchat_core.liveroom.im.model.AudioConnTimeCounter;
import com.tongdaxing.xchat_core.liveroom.im.model.AudioConnTimeOutTipsQueueScheduler;
import com.tongdaxing.xchat_core.liveroom.im.model.BaseRoomServiceScheduler;
import com.tongdaxing.xchat_core.liveroom.im.model.ReUsedSocketManager;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.broadcast.NotificationClickReceiver;

/**
 * @author chenran
 * @date 2017/11/16
 */
public class DaemonService extends Service {
    private static final String TAG = "DaemonService";

    public static final String ACTION_STOP_SERVICE = "com.yuhuankj.tmxq.service.DaemonService.ACTION_STOP";

    private static boolean hasServiceStarted = false;

    public static final int NOTICE_ID = 100;
    private String title;
    private String content;
    private String ticker;
    private static long lastStopServiceTime = 0L;

    public static void start(Context context, String title, String content, String ticker) {
        BuglyLog.d(TAG, "start-title:" + title + " content:" + content + " ticker:" + ticker);
        Intent intent = new Intent(context, DaemonService.class);
        intent.putExtra("title", title);
        intent.putExtra("content", content);
        intent.putExtra("ticker", ticker);
        //适配Android O
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(intent);
        } else {
            context.startService(intent);
        }
    }

    public static void stop(Context context) {
        BuglyLog.d(TAG, "stop-hasServiceStarted:" + hasServiceStarted);

        //避免连续多次调用stop方法，Android 9.0 抛
        //android.app.RemoteServiceException: Bad notification for startForeground:
        // java.lang.RuntimeException: invalid channel for service notification: null
        long timeInner = System.currentTimeMillis() - lastStopServiceTime;
        BuglyLog.d(TAG, "stop-timeInner:" + timeInner);
        if(timeInner < 250L){
            return;
        }
        lastStopServiceTime = System.currentTimeMillis();

        Intent intent = new Intent(context, DaemonService.class);
        intent.setAction(ACTION_STOP_SERVICE);
        //stopService在onStartCommand方法执行之前调用，
        // 会抛错Context.startForegroundService() did not then call Service.startForeground()
        //参考https://stackoverflow.com/questions/44425584/context-startforegroundservice-did-not-then-call-service-startforeground
        //复现方法：进入房间--然后立马退出房间--并关闭最小化房间面板
        if (hasServiceStarted) {
            //适配Android O
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(intent);
            } else {
                context.startService(intent);
            }
        } else {
            context.stopService(intent);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        BuglyLog.d(TAG, "onStartCommand-flags:" + flags + " startId:" + startId + " hasServiceStarted:" + hasServiceStarted);
        if(null == intent){
            /**
             * 进程被杀后有可能被系统自动重启
             * onStartCommand方法的默认值START_STICKY_COMPATIBILITY或者START_STICKY不能保证intent不为null
             *
             * START_NOT_STICKY：
             * 若执行完onStartCommand()方法后，系统就kill了service，不要再重新创建service，
             * 除非系统回传了一个pending intent。这避免了在不必要的时候运行service，您的应用也可以restart任何未完成的操作
             *
             * START_STICKY：
             * 若系统在onStartCommand()执行并返回后kill了service，
             * 那么service会被recreate并回调onStartCommand()。dangerous不要重新传递最后一个Intent（do not redeliver the last intent）。
             * 相反，系统回调onStartCommand()时回传一个空的Intent，除非有 pending intents传递，否则Intent将为null。该模式适合做一些类似播放音乐的操作
             *
             * START_REDELIVER_INTENT：
             * 若系统在onStartCommand()执行并返回后kill了service，那么service会被recreate并回调onStartCommand()
             * 并将最后一个Intent回传至该方法。任何 pending intents都会被轮流传递。该模式适合做一些类似下载文件的操作。
             *
             * super.onStartCommand 在targetSdkVersion>=5的情况下，都是return START_STICKY
             */
            return super.onStartCommand(intent, flags, startId);
        }

        title = intent.getStringExtra("title");
        content = intent.getStringExtra("content");
        ticker = intent.getStringExtra("ticker");
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, getPackageName());
        //适配Android O通知栏,解决：
        // NotificationService: No Channel found for pkg=com.yuhuankj.tmxq, channelId=null,
        // id=100, tag=null, opPkg=com.yuhuankj.tmxq, callingUid=10348,
        // userId=0, incomingUserId=0, notificationUid=10348,
        // notification=Notification(channel=null pri=0 contentView=null
        // vibrate=null sound=null tick defaults=0x0 flags=0x40 color=0x00000000
        // number=0 vis=PRIVATE semFlags=0x0 semPriority=0 semMissedCount=0)
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && null != manager) {
            //新安装的情况下，通知不会有横幅下拉展示后上拉的效果，
            // 覆盖安装则会出现通知横幅展示后消息的效果，体验上有点侵扰用户，
            // 猜测跟没有删除测试通知渠道有关deleteNotificationChannel
            NotificationChannel channel = new NotificationChannel(getPackageName(), getResources().getString(R.string.app_name),
                    NotificationManager.IMPORTANCE_LOW);
            //是否可以显示icon角标
            channel.canShowBadge();
//                channel.enableLights(true);//是否显示通知闪灯
//                channel.enableVibration(true);//收到通知时震动提示
//                channel.setBypassDnd(true);//设置绕过免打扰

            //VISIBILITY_PUBLIC 任何情况下都显示通知的完整内容。
            //VISIBILITY_SECRET 不会在锁定屏幕上显示此通知的任何部分。
            //VISIBILITY_PRIVATE 显示通知图标和内容标题等基本信息，但是隐藏通知的完整内容。
            channel.setLockscreenVisibility(NotificationCompat.VISIBILITY_SECRET);
            //设置闪光灯颜色
//                channel.setLightColor(Color.RED);
            //获取设置铃声设置
//                channel.getAudioAttributes();
            //设置震动模式
//                channel.setVibrationPattern(new long[]{100, 200, 100});
            //是否会闪光
//                channel.shouldShowLights();
            manager.deleteNotificationChannel(getPackageName());
            manager.createNotificationChannel(channel);
        }
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentTitle(title);
        builder.setContentText(content);
        builder.setTicker(ticker);
//        builder.setContentText("点击返回房间");
//        builder.setTicker("正在房间内");
        builder.setAutoCancel(true);
        Intent clickIntent = new Intent(this, NotificationClickReceiver.class);
        PendingIntent contentIntent = PendingIntent.getBroadcast(this.getApplicationContext(),
                (int) System.currentTimeMillis(), clickIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);
        Notification notification = builder.build();
        startForeground(NOTICE_ID, notification);

        hasServiceStarted = true;

        //判断条件方法块下沉到startForeground(NOTICE_ID, notification);之后处理
        //参考：https://www.codetd.com/article/3528335
        if (intent.getAction() != null && ACTION_STOP_SERVICE.equals(intent.getAction()) && hasServiceStarted) {
            BuglyLog.d(TAG, "onStartCommand-stopSelf");
            stopForeground(true);
            hasServiceStarted = false;
            stopSelf();
            BuglyLog.d(TAG, "onStartCommand-stopSelf hasServiceStarted:" + hasServiceStarted);
            return START_NOT_STICKY;
        }

        BuglyLog.d(TAG, "onStartCommand hasServiceStarted:" + hasServiceStarted);
        return START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        BuglyLog.d(TAG, "onTaskRemoved");
        super.onTaskRemoved(rootIntent);
//        BuglyLog.d(TAG, "onTaskRemoved->stopForeground");
//        stopForeground(true);
        BuglyLog.d(TAG, "onTaskRemoved->stop");
        stop(this);
        BuglyLog.d(TAG, "onTaskRemoved-hasServiceStarted:" + hasServiceStarted);
        BaseRoomServiceScheduler.exitRoom(new CallBack<String>() {
            @Override
            public void onSuccess(String data) {

            }

            @Override
            public void onFail(int code, String error) {

            }
        });
        ReUsedSocketManager.get().destroy();
        AudioConnTimeOutTipsQueueScheduler.getInstance().clear();
        AudioConnTimeCounter.getInstance().release();
    }
}
