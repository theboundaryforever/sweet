package com.yuhuankj.tmxq.base.fragment;

import android.view.View;

/**
 * Created by xujiexing on 14-7-21.
 */
public interface IStatusFragment {
    public void setListener(View.OnClickListener listener);
}
