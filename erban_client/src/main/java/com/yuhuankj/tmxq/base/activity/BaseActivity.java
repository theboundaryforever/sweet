package com.yuhuankj.tmxq.base.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.netease.nim.uikit.common.activity.UI;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.tongdaxing.erban.libcommon.utils.NetworkUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.UIUtils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.UriProvider;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.XChatApplication;
import com.yuhuankj.tmxq.base.callback.IDataStatus;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.base.fragment.BaseFragment;
import com.yuhuankj.tmxq.base.fragment.LoadingFragment;
import com.yuhuankj.tmxq.base.fragment.NetworkErrorFragment;
import com.yuhuankj.tmxq.base.fragment.NoDataFragment;
import com.yuhuankj.tmxq.base.fragment.ReloadFragment;
import com.yuhuankj.tmxq.base.permission.EasyPermissions;
import com.yuhuankj.tmxq.base.permission.PermissionActivity;
import com.yuhuankj.tmxq.broadcast.ConnectiveChangedReceiver;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;
import com.yuhuankj.tmxq.widget.DefaultToolBar;
import com.yuhuankj.tmxq.widget.StatusLayout;
import com.yuhuankj.tmxq.widget.TitleBar;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;


/**
 * @author alvin hwang
 */
public abstract class BaseActivity extends UI implements IDataStatus,
        ConnectiveChangedReceiver.ConnectiveChangedListener,
        EasyPermissions.PermissionCallbacks, View.OnClickListener {

    private static final Object TAG = "BaseActivity";
    //底部登录弹窗
    private DialogManager mDialogManager;
    protected TitleBar mTitleBar;
    protected DefaultToolBar mToolBar;
    protected CompositeDisposable mCompositeDisposable;


    public void autoJump(String activity, String url, Json json) {
        if ("".equals(activity)) {
            if (!"".equals(url)) {
                if (url.startsWith("/")) {
                    url = UriProvider.JAVA_WEB_URL + url;
                }
                CommonWebViewActivity.start(this, url);
                return;
            }
        }

        try {
            activity = "com.yuhuankj.tmxq." + activity;
            Intent intent = new Intent(this, Class.forName(activity));
            String[] strings = json.key_names();
            for (int i = 0; i < strings.length; i++) {
                String key = strings[i];
                String value = json.str(key);
                intent.putExtra(key, value);
            }
            startActivity(intent);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        /*解决8.0，透明的Activity，则不能固定它的方向问题*/
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O && isTranslucentOrFloating()) {
            fixOrientation();
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        super.onCreate(savedInstanceState);
        mCompositeDisposable = new CompositeDisposable();
        if (setBgColor() > 0) {
            getWindow().setBackgroundDrawableResource(setBgColor());
        }
        DisplayUtility.initDisplay(this);
        //全局竖屏
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        CoreManager.addClient(this);
    }

    public void initToolBar(CharSequence title) {
        mToolBar = (DefaultToolBar) findViewById(R.id.toolbar);
        if (mToolBar != null) {
            mToolBar.setCenterTitle(title);
            mToolBar.setNavigationIcon(R.drawable.arrow_left);
            mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onLeftClickListener();
                }
            });
        }
    }

    public void initTitleBar() {
        mTitleBar = (TitleBar) findViewById(R.id.title_bar);
        if (mTitleBar != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                mTitleBar.setImmersive(true);
            }
            mTitleBar.setBackgroundColor(getResources().getColor(R.color.primary));
            mTitleBar.setTitleColor(getResources().getColor(R.color.text_primary));
        }
    }

    public void initTitleBar(String title) {
        mTitleBar = (TitleBar) findViewById(R.id.title_bar);
        if (mTitleBar != null) {
            mTitleBar.setTitle(title);
            mTitleBar.setImmersive(false);
            mTitleBar.setTitleColor(getResources().getColor(R.color.back_font));
            mTitleBar.setLeftImageResource(R.drawable.arrow_left);
            mTitleBar.setLeftClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onLeftClickListener();
                }
            });
        }
    }

    protected void onLeftClickListener() {
        finish();
    }

    protected int setBgColor() {
        return 0;
    }

    @Override
    public void onClick(View view) {
    }

    /**
     * 获取状态栏的高度
     *
     * @return
     */
    protected int getStatusBarHeight() {
        try {
            Class<?> c = Class.forName("com.android.internal.R$dimen");
            Object obj = c.newInstance();
            Field field = c.getField("status_bar_height");
            int x = Integer.parseInt(field.get(obj).toString());
            return getResources().getDimensionPixelSize(x);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        initTitleBar();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }


    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (mCompositeDisposable != null) {
            mCompositeDisposable.dispose();
            mCompositeDisposable = null;
        }
        super.onDestroy();
        CoreManager.removeClient(this);
        /* ImageLoadUtils.clearMemory(this);*/

        if (BasicConfig.INSTANCE.isDebuggable()) {
            XChatApplication.getRefWatcher(this).watch(this);
        }
    }

    public DialogManager getDialogManager() {
        if (mDialogManager == null) {
            mDialogManager = new DialogManager(this);
            mDialogManager.setCanceledOnClickOutside(false);
        }
        return mDialogManager;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Fragment fragment = getTopFragment();
        if (fragment != null && fragment instanceof BaseFragment) {
            if (((BaseFragment) fragment).onKeyDown(keyCode, event)) {
                return true;
            }
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Fragment fragment = getTopFragment();
        if (fragment != null && fragment instanceof BaseFragment) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public Fragment getTopFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment != null && fragment.isVisible()) {
                    return fragment;
                }
            }
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        hideIME();
        try {
            super.onBackPressed();
        } catch (Exception ex) {
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Fragment fragment = getTopFragment();
                if (fragment != null && fragment instanceof BaseFragment) {
                    if (fragment.onOptionsItemSelected(item)) {
                        return true;
                    }
                }
                onBackPressed();
                return true;
            default:
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onMenuOpened(int featureId, Menu menu) {
        if (featureId == Window.FEATURE_ACTION_BAR && menu != null) {
            if (menu.getClass().getSimpleName().equals("MenuBuilder")) {
                try {
                    Method m = menu.getClass().getDeclaredMethod("setOptionalIconsVisible", Boolean.TYPE);
                    m.setAccessible(true);
                    m.invoke(menu, true);
                } catch (Exception e) {
                }
            }
        }
        return super.onMenuOpened(featureId, menu);
    }

    @Override
    protected void onResume() {
        super.onResume();
        StatisticManager.get().onResume(this);

        GlideApp.with(this).resumeRequests();
    }

    @Override
    protected void onPause() {
        super.onPause();
        StatisticManager.get().onPause(this);
        GlideApp.with(this).pauseRequests();
    }

    /**
     * 网络连接变化
     */
    /**
     * 网络连接变化
     */
    /**
     * wifi 转 2G/3G/4G
     */
    @Override
    public void wifiChange2MobileData() {

    }

    /**
     * 有网络变为无网络
     */
    @Override
    public void change2NoConnection() {
        if (isTopActive()) {

        }
    }

    /**
     * 连上wifi
     */
    @Override
    public void connectiveWifi() {
        if (isTopActive()) {

        }
    }

    /**
     * 连上移动数据网络
     */
    @Override
    public void connectiveMobileData() {
        if (isTopActive()) {

        }
    }

    /**
     * 移动数据网络 改为连上wifi
     */
    @Override
    public void mobileDataChange2Wifi() {

    }

    protected boolean checkActivityValid() {
        return UIUtils.checkActivityValid(this);
    }


    /**
     * --------------------------------------------------
     * -------------------------数据状态状态相关-------------
     * --------------------------------------------------
     */

    private static final String STATUS_TAG = "STATUS_TAG";

    @Override
    public View.OnClickListener getLoadListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onReloadDate();
            }
        };
    }

    /**
     * 网络错误重新加载数据
     */
    public void onReloadDate() {

    }

    @Override
    public View.OnClickListener getLoadMoreListener() {
        return null;
    }

    @Override
    public View.OnClickListener getNoMobileLiveDataListener() {
        return null;
    }

    @Override
    public void showLoading() {
        showLoading(0, 0);
    }

    @Override
    public void showLoading(View view) {
        showLoading(view, 0, 0);
    }

    @Override
    public void showReload() {
        showReload(0, 0);
    }

    @Override
    public void showNoData() {
        showNoData(0, "");
    }

    @Override
    public void showNoLogin() {

    }

    @SuppressLint("ResourceType")
    @Override
    public void showLoading(int drawable, int tips) {
        if (!checkActivityValid()) {
            return;
        }

        View status = findViewById(R.id.status_layout);
        if (status == null || status.getId() <= 0) {
            return;
        }
        Fragment fragment = LoadingFragment.newInstance(drawable, tips);
        getSupportFragmentManager().beginTransaction().replace(status.getId(), fragment, STATUS_TAG).commitAllowingStateLoss();
    }

    @Override
    public void showLoading(View view, int drawable, int tips) {

    }

    @SuppressLint("ResourceType")
    @Override
    public void showReload(int drawable, int tips) {
        if (!checkActivityValid()) {
            return;
        }

        View status = findViewById(R.id.status_layout);
        if (status == null || status.getId() <= 0) {
            return;
        }
        ReloadFragment fragment = ReloadFragment.newInstance(drawable, tips);
        fragment.setListener(getLoadListener());
        getSupportFragmentManager().beginTransaction().replace(status.getId(), fragment, STATUS_TAG).commitAllowingStateLoss();
    }

    @Override
    public void showReload(View view, int drawable, int tips) {

    }

    @Override
    public void showNoData(CharSequence charSequence) {
        showNoData(0, charSequence);
    }

    @SuppressLint("ResourceType")
    @Override
    public void showNoData(int drawable, CharSequence charSequence) {
        if (!checkActivityValid()) {
            return;
        }

        View status = findViewById(R.id.status_layout);
        if (status == null || status.getId() <= 0) {
            return;
        }
        NoDataFragment fragment = NoDataFragment.newInstance(drawable, charSequence);
        fragment.setListener(getLoadListener());
        getSupportFragmentManager().beginTransaction().replace(status.getId(), fragment, STATUS_TAG).commitAllowingStateLoss();
    }

    @Override
    public void showNoData(View view, int drawable, CharSequence charSequence) {

    }

    @SuppressLint("ResourceType")
    @Override
    public void showNetworkErr() {
        if (!checkActivityValid()) {
            return;
        }

        View status = findViewById(R.id.status_layout);
        if (status == null || status.getId() <= 0) {
            return;
        }
        NetworkErrorFragment fragment = new NetworkErrorFragment();
        fragment.setListener(getLoadListener());
        getSupportFragmentManager().beginTransaction().replace(status.getId(), fragment, STATUS_TAG).commitAllowingStateLoss();
    }

    @Override
    public void hideStatus() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(STATUS_TAG);
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
        }
    }

    @Override
    public void showPageError(int tips) {
        if (!checkActivityValid()) {
            return;
        }

        View more = findViewById(R.id.loading_more);
        if (more == null) {
            return;
        }
        StatusLayout statusLayout = (StatusLayout) more.getParent();
        statusLayout.showErrorPage(tips, getLoadMoreListener());
    }

    @Override
    public void showPageError(View view, int tips) {

    }

    @Override
    public void showPageLoading() {
        if (!checkActivityValid()) {
            return;
        }

        View more = findViewById(R.id.loading_more);
        if (more == null) {
            return;
        }
        StatusLayout statusLayout = (StatusLayout) more.getParent();
        statusLayout.showLoadMore();
    }

    /**
     * 当前网络是否可用
     *
     * @return
     */
    public boolean isNetworkAvailable() {
        return NetworkUtils.isNetworkStrictlyAvailable(this);
    }

    /**
     * --------------------------------------------------
     * -------------------------UI基本功能------------------
     * --------------------------------------------------
     */
    public void hideIME() {
        View v = getCurrentFocus();
        if (null != v) {
            hideIME(v);
        }
    }

    public void hideIME(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public void showIME(final View vv) {
        View v = vv;
        if (null == v) {
            v = getCurrentFocus();
            if (null == v) {
                return;
            }
        }
        ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
                .showSoftInput(v, InputMethodManager.SHOW_FORCED);
    }

    /**
     * 当前无Fragment 或 无Fragment添加 视为顶部激活状态
     *
     * @return
     */
    public boolean isTopActive() {

        if (isTopActivity()) {
            FragmentManager fragmentManager = getSupportFragmentManager();

            List<Fragment> fragments = fragmentManager.getFragments();
            if (fragments == null || fragments.size() == 0) {
                return true;
            } else {
                int size = fragments.size();
                for (int i = 0; i < size; i++) {
                    if (fragments.get(i) != null && fragments.get(i).isAdded()) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    public boolean isTopActivity() {
        return UIUtils.isTopActivity(this);
    }

    /**
     * 通用消息提示
     *
     * @param resId
     */
    public void toast(int resId) {
        toast(resId, Toast.LENGTH_SHORT);
    }

    public void toast(String toast) {
        toast(toast, Toast.LENGTH_SHORT);
    }

    /**
     * 通用消息提示
     *
     * @param resId
     * @param length
     */
    public void toast(int resId, int length) {
        SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), resId, length);
    }

    public void toast(String toast, int length) {
        SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), toast, length);
    }


    /**
     * 权限回调接口
     */
    private PermissionActivity.CheckPermListener mListener;

    protected static final int RC_PERM = 123;

    public void checkPermission(PermissionActivity.CheckPermListener listener, int resString, String... mPerms) {
        mListener = listener;
        if (EasyPermissions.hasPermissions(this, mPerms)) {
            if (mListener != null) {
                mListener.superPermission();
            }
        } else {
            EasyPermissions.requestPermissions(this, getString(resString), RC_PERM, mPerms);
        }
    }

    /**
     * 用户权限处理,
     * 如果全部获取, 则直接过.
     * 如果权限缺失, 则提示Dialog.
     *
     * @param requestCode  请求码
     * @param permissions  权限
     * @param grantResults 结果
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        //同意了某些权限可能不是全部
    }

    @Override
    public void onPermissionsAllGranted() {
        if (mListener != null) {
            mListener.superPermission();//同意了全部权限的回调
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

        EasyPermissions.checkDeniedPermissionsNeverAskAgain(this,
                getString(R.string.perm_tip),
                R.string.setting, R.string.cancel, null, perms);
    }

    //-------------解决8.0，透明的Activity，则不能固定它的方向问题----------------------------------------------
    private void fixOrientation() {
        try {
            Field field = Activity.class.getDeclaredField("mActivityInfo");
            field.setAccessible(true);
            ActivityInfo o = (ActivityInfo) field.get(this);
            o.screenOrientation = -1;
            field.setAccessible(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("PrivateApi")
    private boolean isTranslucentOrFloating() {
        boolean isTranslucentOrFloating = false;
        try {
            int[] styleableRes = (int[]) Class.forName("com.android.internal.R$styleable").getField("Window").get(null);
            final TypedArray ta = obtainStyledAttributes(styleableRes);
            Method m = ActivityInfo.class.getMethod("isTranslucentOrFloating", TypedArray.class);
            m.setAccessible(true);
            isTranslucentOrFloating = (boolean) m.invoke(null, ta);
            m.setAccessible(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isTranslucentOrFloating;
    }

    @Override
    public void setRequestedOrientation(int requestedOrientation) {
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O && isTranslucentOrFloating()) {
            return;
        }
        super.setRequestedOrientation(requestedOrientation);
    }

    public View getEmptyView(ViewGroup viewGroup, String s) {
        View inflate = LayoutInflater.from(this).inflate(R.layout.layout_empty_view, viewGroup, false);
        TextView view = inflate.findViewById(R.id.no_data_text);
        view.setText(s);

        if (null != mTitleBar) {
            inflate.setPadding(0, -mTitleBar.getHeight() / 2, 0, 0);
        }

        return inflate;
    }
}