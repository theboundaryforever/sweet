package com.yuhuankj.tmxq.base.callback;

/**
 * Created by zhouxiangfeng on 17/3/5.
 */

public interface IAcitivityBase extends IBase {

    void onFindViews();

    void onSetListener();

    void initiate();

}
