package com.yuhuankj.tmxq.base.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tongdaxing.erban.libcommon.utils.NetworkUtils;
import com.yuhuankj.tmxq.R;

/**
 * Created by xujiexing on 14-4-9.
 */
public class NetworkErrorFragment extends AbsStatusFragment {
    public static NetworkErrorFragment newInstance() {
        return new NetworkErrorFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_network_error, container, false);
        view.setOnClickListener(mSelfListener);
        return view;
    }

    private View.OnClickListener mSelfListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if(!NetworkUtils.isNetworkStrictlyAvailable(getActivity())){
                checkNetToast();
                return;
            }

            if(mLoadListener != null){
                mLoadListener.onClick(v);
            }
        }
    };

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
