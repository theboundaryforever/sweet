package com.yuhuankj.tmxq.base.fragment;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.gyf.immersionbar.ImmersionBar;
import com.gyf.immersionbar.components.SimpleImmersionOwner;
import com.tongdaxing.erban.libcommon.base.AbstractMvpFragment;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.NetworkUtils;
import com.tongdaxing.erban.libcommon.utils.SingleToastUtil;
import com.tongdaxing.erban.libcommon.utils.UIUtils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.liveroom.im.IDisposableAddListener;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.XChatApplication;
import com.yuhuankj.tmxq.base.activity.BaseActivity;
import com.yuhuankj.tmxq.base.activity.BaseMvpActivity;
import com.yuhuankj.tmxq.base.callback.IAcitivityBase;
import com.yuhuankj.tmxq.base.callback.IDataStatus;
import com.yuhuankj.tmxq.base.dialog.DialogManager;
import com.yuhuankj.tmxq.broadcast.ConnectiveChangedReceiver;
import com.yuhuankj.tmxq.widget.StatusLayout;

import java.util.List;
import java.util.Stack;

import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * @author alvin hwang
 */
public abstract class BaseMvpFragment<V extends IMvpBaseView, P extends AbstractMvpPresenter<V>>
        extends AbstractMvpFragment<V, P> implements KeyEvent.Callback, IDataStatus,
        ConnectiveChangedReceiver.ConnectiveChangedListener,
        FragmentManager.OnBackStackChangedListener, IAcitivityBase,
        IDisposableAddListener, SimpleImmersionOwner {

    public String TAG = BaseMvpFragment.class.getSimpleName();

    //实例是否存活 用户fragment createdview后destoryview后再次createdview，但没有走onDestory方法的情况判断
    private boolean mHasViewLive;

    //数据是否已经请求加载，用于子类手动修改，数据加载为空或者加载失败的情况，修改为false，下次可见再次加载
    public boolean mIsLoaded;

    protected View mView;
    protected Context mContext;
    protected CompositeDisposable mCompositeDisposable;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCompositeDisposable = new CompositeDisposable();
        mContext = getContext();
        onInitArguments(getArguments());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        int rootLayoutId = getRootLayoutId();
        mView = inflater.inflate(rootLayoutId, container, false);
        ButterKnife.bind(this, mView);
        return mView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mHasViewLive = false;
        //不可见的情况下界面销毁，那么下次创建界面的时候需要重新加载数据
        mIsLoaded = false;
        if (mCompositeDisposable != null) {
            mCompositeDisposable.dispose();
            mCompositeDisposable = null;
        }
        LogUtils.d(TAG,"onDestroyView");
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        //isVisibleToUser对用户可见，mHasViewLive界面仍旧存活，!mIsLoaded数据未能成功加载
        if(isVisibleToUser && mHasViewLive && !mIsLoaded){
            loadLazyIfYouWant();
            mIsLoaded = true;
        }
        LogUtils.d(TAG,"setUserVisibleHint-isVisibleToUser:"+isVisibleToUser);
    }

    /**
     * 如果需要在mvp模式下实现fragment懒加载模式 请重写该方法来加载数据
     * 子类需要在该方法里面判断数据是否为空 是否需要每次可见都要更新数据
     */
    public void loadLazyIfYouWant(){
        LogUtils.d(TAG,"loadLazyIfYouWant");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        CoreManager.addClient(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        CoreManager.removeClient(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        onFindViews();
        onSetListener();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        restoreState(savedInstanceState);
        initiate();
        mHasViewLive = true;
        if(getUserVisibleHint() && !mIsLoaded){
            loadLazyIfYouWant();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        getFragmentManager().addOnBackStackChangedListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (getFragmentManager() != null) {
            getFragmentManager().removeOnBackStackChangedListener(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        StatisticManager.get().onResume(getContext());

    }

    @Override
    public void onPause() {
        super.onPause();
        StatisticManager.get().onPause(getContext());

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (BasicConfig.INSTANCE.isDebuggable() && getActivity() != null) {
            XChatApplication.getRefWatcher(getActivity()).watch(this);
        }
    }

    public void onNewIntent(Intent intent) {

    }

    protected void onInitArguments(Bundle bundle) {

    }

    protected void restoreState(@Nullable Bundle savedInstanceState) {

    }

    public abstract int getRootLayoutId();

    @Override
    public void onBackStackChanged() {
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
    }

    /**
     * 有网络变为无网络
     */
    @Override
    public void change2NoConnection() {
        if (isTopActive()) {
        }
    }

    /**
     * 连上wifi
     */
    @Override
    public void connectiveWifi() {
        if (isTopActive()) {
        }
    }

    /**
     * 连上移动数据网络
     */
    @Override
    public void connectiveMobileData() {
        if (isTopActive()) {
        }
    }

    /**
     * 移动数据网络 改为连上wifi
     */
    @Override
    public void mobileDataChange2Wifi() {
        if (isTopActive()) {
        }
    }

    public Stack<Integer> activityForResult = new Stack<Integer>();

    /**
     * 为解决嵌套Fragment 收不到onActivityResult消息问题， Fragment需要调用onFragment
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        while (!activityForResult.isEmpty()) {
            Integer index = activityForResult.pop();

            FragmentManager manager = getChildFragmentManager();
            if (manager == null) { //说明为activity是manager
                manager = getFragmentManager();
            }
            if (manager != null) {
                @SuppressLint("RestrictedApi")
                List<Fragment> list = manager.getFragments();
                if (list != null && list.size() > index) {
                    list.get(index).onActivityResult(requestCode, resultCode, data);
                }
            } else {
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        Fragment fragment = getFragmentInParent();
        if (fragment == null) {
            super.startActivityForResult(intent, requestCode);
        } else {
            fragment.startActivityForResult(intent, requestCode);
        }
    }

    protected Fragment getFragmentInParent() {

        FragmentManager manager;

        Fragment parentFragment = this.getParentFragment();

        if (parentFragment != null) {
            manager = parentFragment.getChildFragmentManager();
        } else {
            manager = this.getFragmentManager();
        }

        @SuppressLint("RestrictedApi")
        List<Fragment> list = manager.getFragments();
        if (list != null) {
            int index = list.indexOf(this);

            if (parentFragment != null && parentFragment instanceof BaseMvpFragment) {
                ((BaseMvpFragment) parentFragment).activityForResult.push(index);
            }
        }
        return parentFragment;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    /**
     * --------------------------------------------------
     * -------------------------数据状态相关-----------------
     * --------------------------------------------------
     */

    private static final String STATUS_TAG = "STATUS_TAG";

    @Override
    public View.OnClickListener getLoadListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onReloadData();
            }
        };
    }

    /**
     * 重新加载页面数据：通常应用于无网络切换到有网络情况，重新刷新页面
     */
    public void onReloadData() {

    }

    @Override
    public View.OnClickListener getLoadMoreListener() {
        return null;
    }

    @Override
    public View.OnClickListener getNoMobileLiveDataListener() {
        return null;
    }

    @Override
    public void showLoading() {
        showLoading(0, 0);
    }

    @Override
    public void showLoading(View view) {
        showLoading(view, 0, 0);
    }

    @Override
    public void showReload() {
        showReload(0, 0);
    }

    @Override
    public void showNoData() {
        showNoData(0, "");
    }

    @Override
    public void showNoData(CharSequence charSequence) {
        showNoData(0, charSequence);
    }

    @Override
    public void showNoLogin() {

    }

    @Override
    public void showLoading(int drawable, int tips) {
        showLoading(getView(), drawable, tips);
    }

    @Override
    public void showLoading(View view, int drawable, int tips) {
        showLoading(view, drawable, tips, 0);
    }

    @SuppressLint("ResourceType")
    protected void showLoading(View view, int drawable, int tips, int paddingBottom) {
        if (!checkActivityValid()) {
            return;
        }
        if (view == null) {
            return;
        }
        View status = view.findViewById(R.id.status_layout);
        if (status == null || status.getId() <= 0) {
            return;
        }
        Fragment fragment;
        if (paddingBottom == 0) {
            fragment = LoadingFragment.newInstance(drawable, tips);
        } else {
            fragment = LoadingFragment.newInstance(drawable, tips, paddingBottom);
        }
        getChildFragmentManager().beginTransaction().replace(status.getId(), fragment, STATUS_TAG).commitAllowingStateLoss();
    }

    @Override
    public void showReload(int drawable, int tips) {
        showReload(getView(), drawable, tips);
    }

    @SuppressLint("ResourceType")
    @Override
    public void showReload(View view, int drawable, int tips) {
        if (!checkActivityValid()) {
            return;
        }

        if (view == null) {
            return;
        }
        View status = view.findViewById(R.id.status_layout);
        if (status == null || status.getId() <= 0) {
            return;
        }
        ReloadFragment fragment = ReloadFragment.newInstance(drawable, tips);
        fragment.setListener(getLoadListener());
        getChildFragmentManager().beginTransaction().replace(status.getId(), fragment, STATUS_TAG).commitAllowingStateLoss();
    }

    @Override
    public void showNoData(int drawable, CharSequence charSequence) {
        showNoData(getView(), 0, charSequence);
    }

    @SuppressLint("ResourceType")
    @Override
    public void showNoData(View view, int drawable, CharSequence charSequence) {
        if (!checkActivityValid()) {
            return;
        }

        if (view == null) {
            return;
        }
        View status = view.findViewById(R.id.status_layout);
        if (status == null || status.getId() <= 0) {
            return;
        }
        NoDataFragment fragment = NoDataFragment.newInstance(drawable, charSequence);
        fragment.setListener(getLoadListener());
        getChildFragmentManager().beginTransaction().replace(status.getId(), fragment, STATUS_TAG).commitAllowingStateLoss();
    }

    @SuppressLint("ResourceType")
    @Override
    public void showNetworkErr() {
        if (!checkActivityValid()) {
            return;
        }

        if (getView() == null) {
            return;
        }
        View view = getView().findViewById(R.id.status_layout);
        if (view == null || view.getId() <= 0) {
            return;
        }
        NetworkErrorFragment fragment = new NetworkErrorFragment();
        fragment.setListener(getLoadListener());
        getChildFragmentManager().beginTransaction().replace(view.getId(), fragment, STATUS_TAG).commitAllowingStateLoss();
    }

    @Override
    public void hideStatus() {
        Fragment fragment = getChildFragmentManager().findFragmentByTag(STATUS_TAG);
        if (fragment != null) {
            getChildFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
        } else {
        }
    }

    @Override
    public void showPageError(int tips) {
        showPageError(getView(), tips);
    }

    @Override
    public void showPageError(View view, int tips) {
        if (!checkActivityValid()) {
            return;
        }

        if (view == null) {
            return;
        }
        View more = view.findViewById(R.id.loading_more);
        if (more == null) {
            return;
        }
        StatusLayout statusLayout = (StatusLayout) more.getParent();
        statusLayout.showErrorPage(tips, getLoadMoreListener());
    }

    @Override
    public void showPageLoading() {
        if (!checkActivityValid()) {
            return;
        }

        if (getView() == null) {
            return;
        }
        View more = getView().findViewById(R.id.loading_more);
        if (more == null) {
            return;
        }
        StatusLayout statusLayout = (StatusLayout) more.getParent();
        statusLayout.showLoadMore();
    }

    protected boolean checkActivityValid() {
        return UIUtils.checkActivityValid(getActivity());
    }

    //====================================================================================//
    //==============================请求过滤条件筛选的返回================================//
    //====================================================================================//
    public boolean isTopActive() {//FIXME 到我的音乐后，RecommendFragment仍然是isTopActive()
        boolean isVisible = this.isResumed() && this.isVisible() && getUserVisibleHint();
        if (!isVisible) {
            return false;
        }

        FragmentManager manager = this.getChildFragmentManager();
        if (manager == null) {
            return true;
        }

        int count = manager.getFragments() == null ? 0 : manager.getFragments().size();
        if (count > 0) {
            for (int i = 0; i < count; i++) {
                Fragment fragment = manager.getFragments().get(i);
                if (fragment == null) {
                    return true;
                }

                if (fragment instanceof AbsStatusFragment) {
                    return true;
                }

                if (fragment.isVisible()) {
                    return false;
                }
            }
        }

        return true;
    }


    /**
     * 当前网络是否可用
     *
     * @return
     */
    public boolean isNetworkAvailable() {
        return NetworkUtils.isNetworkStrictlyAvailable(getActivity());
    }

    /**
     * 通用消息提示
     *
     * @param resId
     */
    public void toast(int resId) {
        toast(resId, Toast.LENGTH_SHORT);
    }

    /**
     * 通用消息提示
     *
     * @param tip
     */
    public void toast(String tip) {
        toast(tip, Toast.LENGTH_SHORT);
    }

    /**
     * 通用消息提示
     *
     * @param resId
     * @param length
     */
    public void toast(int resId, int length) {
        SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), resId);
    }

    /**
     * 通用消息提示
     *
     * @param tip
     * @param length
     */
    public void toast(String tip, int length) {
        SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), tip, length);
    }

    public boolean checkNetToast() {
        boolean flag = isNetworkAvailable();
        if (!flag) {
            toast(R.string.str_network_not_capable);
        }
        return flag;
    }

    private DialogManager mDialogManager;

    protected DialogManager getDialogManager() {
        FragmentActivity activity = getActivity();
        if (activity instanceof BaseActivity) {
            return ((BaseActivity) activity).getDialogManager();
        } else if (activity instanceof BaseMvpActivity) {
            return ((BaseMvpActivity) activity).getDialogManager();
        } else {
            if (mDialogManager == null) {
                mDialogManager = new DialogManager(activity);
                mDialogManager.setCanceledOnClickOutside(false);
            }
            return mDialogManager;
        }
    }

    public BaseMvpActivity getBaseActivity() {
        return (BaseMvpActivity) getActivity();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_MENU) {
            showOptionsMenu();
        }
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            AudioManager audio = (AudioManager) (getContext().getSystemService(Service.AUDIO_SERVICE));
            audio.adjustStreamVolume(
                    AudioManager.STREAM_VOICE_CALL,
                    AudioManager.ADJUST_LOWER,
                    AudioManager.FLAG_PLAY_SOUND | AudioManager.FLAG_SHOW_UI);
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            AudioManager audio = (AudioManager) (getContext().getSystemService(Service.AUDIO_SERVICE));
            audio.adjustStreamVolume(
                    AudioManager.STREAM_VOICE_CALL,
                    AudioManager.ADJUST_RAISE,
                    AudioManager.FLAG_PLAY_SOUND | AudioManager.FLAG_SHOW_UI);
            return true;
        }
        return false;
    }

    protected void showOptionsMenu() {

    }

    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        return false;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        return false;
    }

    @Override
    public boolean onKeyMultiple(int keyCode, int count, KeyEvent event) {
        return false;
    }

    @Override
    public void addDisposable(Disposable disposable) {
        if (mCompositeDisposable != null && disposable != null) {
            mCompositeDisposable.add(disposable);
        }
    }

    /**
     * wifi 转 2G/3G/4G
     */
    @Override
    public void wifiChange2MobileData() {

    }

    @Override
    public void initImmersionBar() {
        ImmersionBar.with(this).statusBarDarkFont(true, 0.2f).init();
    }

    @Override
    public boolean immersionBarEnabled() {
        return true;
    }

}