package com.yuhuankj.tmxq.base.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.base.factory.BaseMvpProxy;
import com.tongdaxing.erban.libcommon.base.factory.PresenterMvpFactory;
import com.tongdaxing.erban.libcommon.base.factory.PresenterMvpFactoryImpl;
import com.tongdaxing.erban.libcommon.base.factory.PresenterProxyInterface;

/**
 * <p>  1. 子类的Presenter必须继承自AbstractMvpPresenter；
 * 2. 子类的View必须继承自IMvpBaseView
 * </p>
 *
 * @author jiahui
 * @date 2017/12/8
 */
public abstract class AbstractMvpDialogFragment<V extends IMvpBaseView, P extends AbstractMvpPresenter<V>> extends RxDialogFragment
        implements PresenterProxyInterface<V, P> {
    protected final String TAG = getClass().getSimpleName();
    private static final String TAG_LOG = "Super-mvp";
    private static final String KEY_SAVE_PRESENTER = "key_save_presenter";
    /**
     * 创建代理对象，传入默认的Presenter工厂
     */
    private BaseMvpProxy<V, P> mMvpProxy = new BaseMvpProxy<>(PresenterMvpFactoryImpl.<V, P>createFactory(getClass()));
    private String mFragmentName = getClass().getName();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mMvpProxy.onRestoreInstanceState(savedInstanceState.getBundle(KEY_SAVE_PRESENTER));
        }
        mMvpProxy.onCreate((V) this);
    }

    @Override
    public void onStart() {
        super.onStart();
        mMvpProxy.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mMvpProxy.onResume();
    }

    @Override
    public void onPause() {
        mMvpProxy.onPause();
        super.onPause();
    }

    @Override
    public void onStop() {
        mMvpProxy.onStop();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        mMvpProxy.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle(KEY_SAVE_PRESENTER, mMvpProxy.onSaveInstanceState());
    }

    @Override
    public void setPresenterFactory(PresenterMvpFactory<V, P> presenterFactory) {
        mMvpProxy.setPresenterFactory(presenterFactory);
    }

    @Override
    public PresenterMvpFactory<V, P> getPresenterFactory() {
        return mMvpProxy.getPresenterFactory();
    }

    @Override
    public P getMvpPresenter() {
        return mMvpProxy.getMvpPresenter();
    }
}
