package com.yuhuankj.tmxq.base.fragment;

import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.netease.nim.uikit.common.util.sys.NetworkUtil;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.util.CommonParamUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.erban.libcommon.widget.RecyclerViewNoBugLinearLayoutManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.yuhuankj.tmxq.R;

import java.util.List;
import java.util.Map;

public class BaseListFragment extends BaseFragment {

    public int pageSize = 10;
    public int maxPage = 50;
    private SwipeRefreshLayout swipeRefreshLayout;
    //后端这个参数还有可能是pageNo
    private String pageNoParmasName = "pageNum";
    private int page = 1;
    private Handler handler = new Handler();
    private BaseQuickAdapter adapter = null;
    private RecyclerView recyclerView;
    private int columns = 1;
    private IDataFilter iDataFilter;
    private String shortUrl = "";
    private String emptyStr = "";

    public BaseListFragment setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public void setPageNoParmasName(String pageNoParmasName) {
        this.pageNoParmasName = pageNoParmasName;
    }

    public BaseQuickAdapter getBaseQuickAdapter() {
        return adapter;
    }

    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_base_list;
    }

    @Override
    public void onFindViews() {
        recyclerView = mView.findViewById(R.id.rv_base_list);
        swipeRefreshLayout = mView.findViewById(R.id.refresh_layout_base_list);
    }

    @Override
    public void onSetListener() {

    }

    @Override
    public void initiate() {
        if (getBaseQuickAdapter() == null) {
            return;
        }
        if (columns > 1) {
            recyclerView.setLayoutManager(new GridLayoutManager(mContext, columns));
        } else {
            recyclerView.setLayoutManager(new RecyclerViewNoBugLinearLayoutManager(mContext));
        }

        recyclerView.setAdapter(getBaseQuickAdapter());
        getBaseQuickAdapter().setEmptyView(getEmptyView(recyclerView, getEmptyStr()));
        getBaseQuickAdapter().setOnLoadMoreListener(new com.chad.library.adapter.base.BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                page++;
                if (NetworkUtil.isNetAvailable(mContext)) {
                    request();
                } else {
                    getBaseQuickAdapter().loadMoreEnd(true);
                }

                if (page > maxPage) {
                    getBaseQuickAdapter().loadMoreEnd(true);
                }
            }
        }, recyclerView);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                request();
                swipeRefreshLayout.setRefreshing(true);
            }
        });
        request();
    }

    private void request() {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put(pageNoParmasName, "" + page);
        param.put("pageSize", pageSize + "");
        param.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        OkHttpManager.getInstance().getRequest(getShortUrl(), param, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                swipeRefreshLayout.setRefreshing(false);
                getBaseQuickAdapter().loadMoreComplete();
                getBaseQuickAdapter().loadMoreEnd(true);
                if (null != getActivity()) {
                    toast(getActivity().getResources().getString(R.string.network_error));
                }
                if (handler != null) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            getDialogManager().dismissDialog();
                        }
                    });
                }
            }

            @Override
            public void onResponse(Json json) {
                if (handler != null) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            getDialogManager().dismissDialog();
                        }
                    });
                }

                swipeRefreshLayout.setRefreshing(false);
                if (json.num("code") != 200) {
                    toast(json.str("message", null == getActivity() ? null : getActivity().getResources().getString(R.string.network_error)));
                    return;
                }
                List<Json> data;
                if (iDataFilter != null) {
                    data = iDataFilter.dataFilter(json);
                } else {
                    data = json.jlist("data");
                }

                if (page > Constants.PAGE_START) {
                    getBaseQuickAdapter().addData(data);
                    getBaseQuickAdapter().loadMoreComplete();

                } else if (page == Constants.PAGE_START) {
                    getBaseQuickAdapter().setNewData(data);
                }
                if (data.size() < pageSize) {
                    getBaseQuickAdapter().loadMoreEnd(true);
                }
            }
        });
    }

    public BaseListFragment setDataFilter(IDataFilter iDataFilter) {
        this.iDataFilter = iDataFilter;
        return this;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        handler.removeCallbacksAndMessages(null);
        handler = null;
    }

    public String getEmptyStr() {
        return emptyStr;
    }

    public BaseListFragment setEmptyStr(String emptyStr) {
        this.emptyStr = emptyStr;
        return this;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public BaseListFragment setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
        return this;
    }

    public BaseListFragment setAdapter(BaseQuickAdapter shareFansAdapter) {
        this.adapter = shareFansAdapter;
        return this;
    }


    public interface IDataFilter {
        List<Json> dataFilter(Json json);
    }


}
