package com.yuhuankj.tmxq.base.dialog;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.yuhuankj.tmxq.R;

/**
 * 文件描述：Dilaog的基类--用于统一封装规范dialog的使用
 * 使用场景为简单的界面类的Dialog 此时避免创建mvp的空接口等
 *
 * @auther：zwk
 * @data：2019/5/13
 */
public abstract class BaseAppDialogFragment extends AppCompatDialogFragment {
    private int dialogStyle = 0;
    private boolean mIsInitView;
    private boolean mIsLoaded;
    /** 当前fragment是否还活着 */
    private boolean mIsDestroyView = true;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArguments(savedInstanceState);
        dialogStyle = getDialogStyle();
        if (dialogStyle == AppDialogConstant.STYLE_BOTTOM_BACK_DARK) {
            setStyle(STYLE_NO_TITLE, R.style.AppBaseDialogBottom);
        } else if (dialogStyle == AppDialogConstant.STYLE_CENTER_BACK_DARK) {
            setStyle(STYLE_NO_TITLE, R.style.AppBaseDialog);
        } else if (dialogStyle == AppDialogConstant.STYLE_BOTTOM) {
            setStyle(STYLE_NO_TITLE, R.style.AppBaseDialogBottomNoDark);
        } else if (dialogStyle == AppDialogConstant.STYLE_CENTER) {
            setStyle(STYLE_NO_TITLE, R.style.AppBaseDialogNoDark);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIsInitView = true;
        if (getUserVisibleHint() && !mIsLoaded && mIsDestroyView) {
            mIsDestroyView = false;
            onLazyLoadData();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (mIsInitView && !mIsLoaded && mIsDestroyView) {
                mIsDestroyView = false;
                mIsLoaded = true;
                onLazyLoadData();
            }
        } else {
            mIsLoaded = false;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mIsDestroyView = true;
    }

    /** 数据懒加载 */
    protected void onLazyLoadData(){}

    public boolean ismIsLoaded() {
        return mIsLoaded;
    }

    /**
     * 提供dialog的样式：
     * 目前提供四种：0.中间显示背景不变暗  1.底部显示背景不变暗 2.中间显示背景变暗  3.底部显示背景变暗
     *
     * @return
     */
    protected abstract int getDialogStyle();

    /**
     * 获取传递进入dialog的数据
     *
     * @param savedInstanceState
     */
    public void initArguments(Bundle savedInstanceState) {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        if (getDialog() != null) {
            Window window = getDialog().getWindow();
            if (window != null) {
                view = inflater.inflate(getLayoutId(), window.findViewById(android.R.id.content), false);
                if (dialogStyle == AppDialogConstant.STYLE_CENTER_BACK_DARK || dialogStyle == AppDialogConstant.STYLE_CENTER) {
                    window.setGravity(Gravity.CENTER);
                } else if (dialogStyle == AppDialogConstant.STYLE_BOTTOM_BACK_DARK || dialogStyle == AppDialogConstant.STYLE_BOTTOM) {
                    window.setGravity(Gravity.BOTTOM);
                }
            }
        }
        if (view == null) {
            view = inflater.inflate(getLayoutId(), container, false);
        }
        return view;
    }

    /**
     * 提供dialog对应的布局
     *
     * @return
     */
    protected abstract @LayoutRes
    int getLayoutId();

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        initViewState();
        initListener(view);
    }

    /**
     * 初始化view
     *
     * @param view
     */
    protected abstract void initView(View view);

    /**
     * 添加事件监听
     */
    protected abstract void initListener(View view);

    /**
     * 根据初始数据改变view的样式
     */
    protected abstract void initViewState();


    @Override
    public void show(FragmentManager fragmentManager, String tag) {
        try {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            transaction.add(this, tag).addToBackStack(null);
            transaction.commitAllowingStateLoss();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 在activity显示使用上面的方法会导致
     * dialog出现几次，activity需要多返回几次才能finish
     * 将fragment添加到stack中dismiss是因为mBackStackId=-1没有退栈
     *
     * @param fragmentManager
     * @param tag
     */
    public void showDialog(FragmentManager fragmentManager, String tag) {
        try {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            transaction.add(this, tag);
            transaction.commitAllowingStateLoss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dismiss() {
        try {
            super.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
