package com.yuhuankj.tmxq.base.fragment;

import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Toast;

import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.erban.libcommon.utils.toast.ToastCompat;
import com.yuhuankj.tmxq.R;

/**
 * Created by xujiexing on 14-7-21.
 */
public abstract class AbsStatusFragment extends Fragment implements IStatusFragment{
    protected View.OnClickListener mLoadListener;

    @Override
    public void setListener(View.OnClickListener listener) {
        this.mLoadListener = listener;
    }


    public void checkNetToast(){

        ToastCompat.makeText(BasicConfig.INSTANCE.getAppContext(), R.string.str_network_not_capable, Toast.LENGTH_SHORT).show();
    }
}
