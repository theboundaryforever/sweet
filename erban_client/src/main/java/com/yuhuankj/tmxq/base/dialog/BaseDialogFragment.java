package com.yuhuankj.tmxq.base.dialog;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatDialogFragment;

import io.reactivex.disposables.CompositeDisposable;

/**
 * 解决AppCompatDialogFragment show方法的异常问题
 */
public class BaseDialogFragment extends AppCompatDialogFragment {

    protected CompositeDisposable mCompositeDisposable;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mCompositeDisposable != null) {
            mCompositeDisposable.dispose();
            mCompositeDisposable = null;
        }
    }

    @Override
    public void show(FragmentManager fragmentManager, String tag) {
        //activity中，需要复制一套本方法，并把addToBackStack方法调用逻辑去掉，否则只要弹过一次，就需要点两次返回键，才能退出activity
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(this, tag).addToBackStack(null);
        transaction.commitAllowingStateLoss();

    }


    @Override
    public void dismiss() {
        try {
            super.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
