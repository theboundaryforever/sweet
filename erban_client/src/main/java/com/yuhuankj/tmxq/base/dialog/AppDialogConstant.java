package com.yuhuankj.tmxq.base.dialog;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/5/29
 */
public class AppDialogConstant {
    public static final int STYLE_CENTER_BACK_DARK = 0;
    public static final int STYLE_BOTTOM_BACK_DARK = 1;
    public static final int STYLE_CENTER = 2;
    public static final int STYLE_BOTTOM = 3;
}
