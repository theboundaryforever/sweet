package com.yuhuankj.tmxq.base.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Window;

import com.yuhuankj.tmxq.R;

/**
 * 文件描述：Dialog的基类，用于简单的弹框显示
 * 如果界面比较复杂，建议使用DialogFragment
 * @auther：zwk
 * @data：2019/5/28
 */
public abstract class BaseAppDialog extends Dialog {

    public BaseAppDialog(Context context) {
        super(context, R.style.AppBaseDialog);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        if (window != null) {
            switch (onDialogStyle()) {
                case AppDialogConstant.STYLE_BOTTOM_BACK_DARK:
                    window.setGravity(Gravity.BOTTOM);
                    window.setWindowAnimations(R.style.BottomDialogAnimation);
                    window.setDimAmount(getDialogAlpha());
                    break;
                case AppDialogConstant.STYLE_CENTER_BACK_DARK:
                    window.setGravity(Gravity.CENTER);
                    window.setDimAmount(getDialogAlpha());
                    break;
                case AppDialogConstant.STYLE_BOTTOM:
                    window.setGravity(Gravity.BOTTOM);
                    window.setWindowAnimations(R.style.BottomDialogAnimation);
                    window.setDimAmount(0);
                    break;
                case AppDialogConstant.STYLE_CENTER:
                    window.setGravity(Gravity.CENTER);
                    window.setDimAmount(0);
                    break;
            }
        }
        setContentView(getLayoutId());
        initView();
        initListener();
        initViewState();
    }

    public float getDialogAlpha() {
        return 0.5f;
    }

    public int onDialogStyle() {
        return AppDialogConstant.STYLE_CENTER_BACK_DARK;
    }

    protected abstract int getLayoutId();

    protected abstract void initView();

    public abstract void initListener();

    public abstract void initViewState();

    @Override
    public void show() {
        try {
            super.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dismiss() {
        try {
            super.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
