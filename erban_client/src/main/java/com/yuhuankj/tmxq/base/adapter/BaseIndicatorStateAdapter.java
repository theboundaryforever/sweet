package com.yuhuankj.tmxq.base.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;

import java.util.List;

/**
 * 文件描述：
 * 因为FragmentPagerAdapter只要加载过，fragment中的视图就一直在内存中，在这个过程中无论你怎么刷新，清除都是无用的，直至程序退出
 * FragmentStatePagerAdapter可以满足首页刷新更新标签和底部对应的fragment内容的需求
 *
 * @auther：zwk
 * @data：2019/1/4
 */
public class BaseIndicatorStateAdapter extends FragmentStatePagerAdapter {

    private List<Fragment> mTabs;

    public BaseIndicatorStateAdapter(FragmentManager fm, List<Fragment> tabs) {
        super(fm);
        mTabs = tabs;
    }

    @Override
    public int getItemPosition(Object object) {
        // TODO Auto-generated method stub
        return PagerAdapter.POSITION_NONE;
    }

    @Override
    public int getCount() {
        return mTabs.size();
    }

    @Override
    public Fragment getItem(int position) {
        return mTabs.get(position);
    }
}
