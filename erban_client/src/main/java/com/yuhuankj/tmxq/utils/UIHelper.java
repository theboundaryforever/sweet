package com.yuhuankj.tmxq.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;

import com.tongdaxing.xchat_core.UriProvider;
import com.yuhuankj.tmxq.ui.audio.AudioRecordActivity;
import com.yuhuankj.tmxq.ui.launch.guide.UserGuideActivity;
import com.yuhuankj.tmxq.ui.login.ForgetPswActivity;
import com.yuhuankj.tmxq.ui.login.ModifyInfoActivity;
import com.yuhuankj.tmxq.ui.login.RegisterActivity;
import com.yuhuankj.tmxq.ui.me.setting.SettingActivity;
import com.yuhuankj.tmxq.ui.me.wallet.MyWalletActivity;
import com.yuhuankj.tmxq.ui.user.other.UserInfoModifyActivity;
import com.yuhuankj.tmxq.ui.user.other.UserModifyPhotosActivity;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;


/**
 * *************************************************************************
 *
 * @Version 1.0
 * @ClassName: UIHelper
 * @Description: 应用程序UI工具包：封装UI相关的一些操作
 * @Author zhouxiangfeng
 * @date 2013-8-6 下午1:39:11
 * **************************************************************************
 */
public class UIHelper {


//
//    /**
//     * 登录
//     *git
//     * @param context
//     */
//    public static void showLoginAct(Context context) {
//        Intent intent = new Intent(context, LoginActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        context.startActivity(intent);
//    }
//
//    /**
//     * 显示 主界面
//     */
//    public static void showMyMainAct(Context context) {
//        Intent intent = new Intent(context, Main2Activity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        context.startActivity(intent);
//    }


    public static void showUserGuideAct(Context mContext) {
        mContext.startActivity(new Intent(mContext, UserGuideActivity.class));
    }

    public static void showRegisterAct(Context mContext) {
        mContext.startActivity(new Intent(mContext, RegisterActivity.class));
    }

    public static void showSettingAct(Context mContext) {
        mContext.startActivity(new Intent(mContext, SettingActivity.class));
    }

    public static void showForgetPswAct(Context mContext) {
        mContext.startActivity(new Intent(mContext, ForgetPswActivity.class));
    }

    //修改用户资料
    public static void showUserInfoModifyAct(Context mContext, long userId) {
        Intent intent = new Intent(mContext, UserInfoModifyActivity.class);
        intent.putExtra("userId", userId);
        mContext.startActivity(intent);
    }

    //我的钱包
    public static void showWalletAct(Context mContext) {
        Intent intent = new Intent(mContext, MyWalletActivity.class);
        //intent.putExtra("userInfo",userInfo);
        mContext.startActivity(intent);
    }

    public static void showAudioRecordAct(Context mContext) {
        Intent intent = new Intent(mContext, AudioRecordActivity.class);
        mContext.startActivity(intent);
    }


    public static void showModifyInfoAct(Activity mActivity, int requestCode, String title) {
        Intent intent = new Intent(mActivity, ModifyInfoActivity.class);
        intent.putExtra("title", title);
        mActivity.startActivityForResult(intent, requestCode);
    }

    public static void showModifyPhotosAct(Activity mActivity, long userId) {
        Intent intent = new Intent(mActivity, UserModifyPhotosActivity.class);
        intent.putExtra("userId", userId);
        mActivity.startActivity(intent);
    }


    /**
     * 启动应用的设置
     *
     * @param context
     */
    public static void startAppSettings(Context context) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.parse("package:" + context.getPackageName()));
        context.startActivity(intent);
    }

    public static void openContactUs(Context context) {
        CommonWebViewActivity.start(context, UriProvider.IM_SERVER_URL + "/ttyy/links_tmxq/links.html");
    }
}
