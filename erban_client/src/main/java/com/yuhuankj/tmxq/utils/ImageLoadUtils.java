package com.yuhuankj.tmxq.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.NinePatch;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.target.ViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.netease.nim.uikit.glide.GlideApp;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.widget.CacheImageView;
import com.yuhuankj.tmxq.widget.CircleImageView;

import java.io.File;

/**
 * 图片加载处理
 * Created by chenran on 2017/11/9.
 */
public class ImageLoadUtils {
    private static final String PIC_PROCESSING = "?imageslim";
    private static final String ACCESS_URL = "img.erbanyy.com";

    public static void loadAvatar(Context context, String avatar, ImageView imageView, boolean isCircle) {
        if (StringUtil.isEmpty(avatar)) {
            return;
        }

        StringBuilder sb = new StringBuilder(avatar);
        if (avatar.contains(ACCESS_URL)) {
            if (!avatar.contains("?")) {
                sb.append(PIC_PROCESSING);
            }
            sb.append("|imageView2/1/w/100/h/100");
        }
        if (isCircle) {
            loadCircleImage(context, sb.toString(), imageView, R.drawable.ic_default_avatar);
        } else {
            loadImage(context, sb.toString(), imageView, R.drawable.default_user_head);
        }
    }

    public static void loadBigAvatar(Context context, String avatar, ImageView imageView, boolean isCircle) {
        if (StringUtil.isEmpty(avatar)) {
            return;
        }
        StringBuffer sb = new StringBuffer(avatar);
        if (avatar.contains(ACCESS_URL)) {
            if (!avatar.contains("?")) {
                sb.append(PIC_PROCESSING);
            }
            sb.append("|imageView2/1/w/150/h/150");
        }
        if (isCircle) {
            loadCircleImage(context, sb.toString(), imageView, R.drawable.default_user_head);
        } else {
            loadImage(context, sb.toString(), imageView, R.drawable.default_user_head);
        }
    }

    public static void loadAvatar(Context context, String avatar, ImageView imageView) {
        loadAvatar(context, avatar, imageView, false);
    }

    public static void loadSmallRoundBackground(Context context, String url, ImageView imageView) {
        if (StringUtil.isEmpty(url)) {
            return;
        }
        StringBuilder sb = new StringBuilder(url);
        if (url.contains(ACCESS_URL)) {
            if (!url.contains("?")) {
                sb.append(PIC_PROCESSING);
            }
            sb.append("|imageView2/1/w/220/h/220");
        }
        GlideApp.with(context)
                .load(sb.toString())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transforms(new CenterCrop(),
                        new RoundedCorners(context.getResources().getDimensionPixelOffset(R.dimen.common_cover_round_size)))
                .placeholder(R.drawable.bg_default_cover_round_placehold_5)
                .error(R.drawable.bg_default_cover_round_placehold_5)
                .into(imageView);
    }


    public static void loadRoundImage(Context context, String url, ImageView imageView, int resourcesId) {
        if (StringUtil.isEmpty(url)) {
            return;
        }

        GlideApp.with(context)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transforms(new CenterCrop(),
                        new RoundedCorners(context.getResources().getDimensionPixelOffset(resourcesId)))
                .placeholder(R.drawable.bg_default_cover_round_placehold_5)
                .error(R.drawable.bg_default_cover_round_placehold_5)
                .into(imageView);
    }

    public static void loadRoundImage(Context context, String url, ImageView imageView, int corners, int defResId) {
        if (StringUtil.isEmpty(url)) {
            return;
        }

        GlideApp.with(context)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transforms(new CenterCrop(), new RoundedCorners(corners))
                .placeholder(defResId)
                .error(defResId)
                .into(imageView);
    }

    public static void loadBannerRoundBackground(Context context, String url, ImageView imageView) {
        if (StringUtil.isEmpty(url)) {
            return;
        }
        StringBuffer sb = new StringBuffer(url);
        if (url.contains(ACCESS_URL)) {
            if (!url.contains("?")) {
                sb.append(PIC_PROCESSING);
            }
            sb.append("|imageView2/1/w/660/h/220");
        }

        GlideApp.with(context)
                .load(sb.toString())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transforms(new CenterCrop(), new RoundedCorners(context.getResources().getDimensionPixelOffset(R.dimen.common_cover_round_size)))
                .placeholder(R.drawable.bg_default_cover_round_placehold_5)
                .into(imageView);
    }

    public static void loadPhotoThumbnail(Context context, String url, ImageView imageView) {
        if (StringUtil.isEmpty(url)) {
            return;
        }

        StringBuffer sb = new StringBuffer(url);
        if (url.contains(ACCESS_URL)) {
            if (!url.contains("?")) {
                sb.append(PIC_PROCESSING);
            }
            sb.append("|imageView2/1/w/150/h/150");
        }

        loadImage(context, sb.toString(), imageView, R.drawable.bg_default_cover_round_placehold_5);
    }

    /**
     * 加载图片，并做高斯模糊处理
     *
     * @param context
     * @param url
     * @param imageView
     * @param radius
     * @param sampling
     */
    public static void loadImageWithBlurTransformation(Context context, String url, final ImageView imageView, int radius, int sampling) {
        if (StringUtil.isEmpty(url)) {
            return;
        }
        StringBuffer sb = new StringBuffer(url);
        if (url.contains(ACCESS_URL)) {
            if (!url.contains("?")) {
                sb.append(PIC_PROCESSING);
            }
            sb.append("|imageView2/1/w/75/h/75");
        }
        GlideApp.with(context)
                .load(sb.toString())
                .dontTransform()
                .dontAnimate()
                .override(75, 75)
                .centerInside()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object o, Target<Drawable> target, boolean isFirstResource) {
                        e.printStackTrace();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable drawable, Object o, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        imageView.setImageDrawable(drawable);
//                        if (!isFirstResource) {
//                            imageView.setAlpha(0.F);
//                            imageView.animate().alpha(1F).setDuration(500).start();
//                        }
                        return true;
                    }
                })
                .transforms(new BlurTransformation(context, radius, sampling))
                .into(imageView);
    }

    public static void loadImageWithBlurTransformationAndCorner(Context context, String url, ImageView imageView) {
        if (StringUtil.isEmpty(url)) {
            return;
        }
        StringBuffer sb = new StringBuffer(url);
        if (url.contains(ACCESS_URL)) {
            if (!url.contains("?")) {
                sb.append(PIC_PROCESSING);
            }
            sb.append("|imageView2/1/w/75/h/75");
        }
        GlideApp.with(context)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transforms(new CenterCrop(),
                        new BlurTransformation(context, 25, 3),
                        new RoundedCorners(context.getResources().getDimensionPixelOffset(R.dimen.common_cover_round_size))
                )
                .into(imageView);

    }


    public static void loadCircleImage(Context context, String url, ImageView imageView, int defaultRes) {
        try {
            GlideApp.with(context).load(url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontAnimate()
                    .transform(new CircleCrop())
                    .error(defaultRes)
                    .placeholder(defaultRes)
                    .into(imageView);
        } catch (Exception illae) {
            illae.printStackTrace();
        }
    }

    public static void loadImage(Context context, String url, ImageView imageView, int defaultRes) {
        GlideApp.with(context).load(url)
                .dontAnimate()
                .placeholder(defaultRes)
                .error(defaultRes)
                .into(imageView);
    }


    /**
     * 加载旧的云信多人房间背景
     * @param context
     * @param url
     * @param imageView
     * @param defaultRes
     */
    public static void loadRoomBackgroundImage(Context context, String url, ImageView imageView, int defaultRes) {
        GlideApp.with(context).load(url)
                .dontAnimate()
                .placeholder(defaultRes)
//                .format(DecodeFormat.PREFER_ARGB_8888)//房间背景图片失真问题
                .error(defaultRes)
                .into(imageView);
    }

    /**
     * 加载新的多人和单人房间背景
     * 默认格式RGB_565使用内存是ARGB_8888的一半，但是图片质量就没那么高了，而且不支持透明度
     * @param context
     * @param url
     * @param imageView
     */
    public static void loadRoomBgImage(Context context, String url, ImageView imageView, boolean hasAnim) {
//        if (imageView.getDrawable() != null) {//避免Canvas: trying to use a recycled bitmap
//            if (hasAnim) {
//                RequestOptions requestOptions = RequestOptions.bitmapTransform(new CenterCrop());
//                requestOptions.disallowHardwareConfig();
                //Software rendering doesn't support hardware bitmaps
                // Glide 4.3上，hardware bitmaps(开启hardware bitmaps作用大概就是在使用ARGB_8888这种高质量的图片时，图片在内存中只会存在一份，反正就是一种节省内存的机制。
                GlideApp.with(context).load(url)
                        .centerCrop()
//                        .listener(new RequestListener<Drawable>() {
//                            @Override
//                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                                return false;
//                            }
//
//                            @Override
//                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
//                                if (dataSource == DataSource.MEMORY_CACHE) {
//                                    //当图片位于内存缓存时，glide默认不会加载动画
//                                    AlphaAnimation animator = new AlphaAnimation(0.4f, 1.0f);
//                                    animator.setDuration(1000);
//                                    imageView.startAnimation(animator);
//                                }
//                                return false;
//                            }
//                        })
                        .format(DecodeFormat.PREFER_ARGB_8888)//房间背景图片失真问题
//                        .apply(requestOptions)
                        .placeholder(imageView.getDrawable())
                        .error(imageView.getDrawable())
                        .into(imageView);
//            }else {
//                GlideApp.with(context).load(url)
//                        .dontAnimate()
//                        .centerCrop()
//                        .placeholder(imageView.getDrawable())
//                        .format(DecodeFormat.PREFER_ARGB_8888)//房间背景图片失真问题
//                        .into(imageView);
//            }
//        } else {
//            if (hasAnim) {
//                RequestOptions requestOptions = RequestOptions.bitmapTransform(new CenterCrop());
//                requestOptions.disallowHardwareConfig();
//                GlideApp.with(context).load(url)
//                        .centerCrop()
//                        .listener(new RequestListener<Drawable>() {
//                            @Override
//                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                                return false;
//                            }
//
//                            @Override
//                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
//                                if (dataSource == DataSource.MEMORY_CACHE) {
//                                    //当图片位于内存缓存时，glide默认不会加载动画
//                                    AlphaAnimation animator = new AlphaAnimation(0.4f, 1.0f);
//                                    animator.setDuration(1000);
//                                    imageView.startAnimation(animator);
//                                }
//                                return false;
//                            }
//                        })
//                        .transition(GenericTransitionOptions.with(R.anim.fade_int))
//                        .format(DecodeFormat.PREFER_ARGB_8888)//房间背景图片失真问题
//                        .apply(requestOptions)
//                        .into(imageView);
//            }else {
//                GlideApp.with(context).load(url)
//                        .centerCrop()
//                        .dontAnimate()
//                        .format(DecodeFormat.PREFER_ARGB_8888)//房间背景图片失真问题
//                        .into(imageView);
//            }
//        }
    }


    public static void loadRoomBgImage(Context context, int urlId, ImageView imageView) {
        GlideApp.with(context).load(urlId)
                .dontAnimate()
                .centerCrop()
                .into(imageView);
    }

    public static void loadRoomBgImage(Context context, String url, ImageView imageView, int defaultRes) {
        GlideApp.with(context).load(url)
                .dontAnimate()
                .centerCrop()
                .placeholder(defaultRes)
                .error(defaultRes)
                .into(imageView);
    }

    public static void loadImage(Context context, int urlId, ImageView imageView, int defaultRes) {
        GlideApp.with(context).load(urlId)
                .dontAnimate()
                .placeholder(defaultRes)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .centerCrop()
                .error(defaultRes)
                .into(imageView);
    }


    public static void loadImage(Context context, String url, CircleImageView imageView) {
        GlideApp.with(context).load(url)
                .dontAnimate()
                .into(imageView);
    }

    public static void loadImage(Context context, File file, ImageView imageView, int defaultRes) {
        GlideApp.with(context).load(file).dontAnimate().placeholder(defaultRes).into(imageView);
    }

    public static void loadImage(Context context, File file, ImageView imageView) {
        GlideApp.with(context).load(file).dontAnimate().into(imageView);
    }

    public static void loadImage(Context context, String url, ImageView imageView) {
        GlideApp.with(context).load(url).dontAnimate().into(imageView);
    }

    public static void loadImage(Context context, String url, CacheImageView imageView) {
        if (TextUtils.isEmpty(url) || url.equals(imageView.getImageUrl())) {
            return;
        }
        imageView.setImageUrl(url);
        GlideApp.with(context).load(url).dontAnimate().into(imageView);
    }

    public static void loadGifImage(Context context, String url, CacheImageView imageView) {
        if (TextUtils.isEmpty(url) || url.equals(imageView.getImageUrl())) {
            return;
        }
        imageView.setImageUrl(url);
        Glide.with(context).asGif().load(url).into(imageView);
    }

    public static String toThumbnailUrl(int width, int height, String imgUrl) {
        if (!TextUtils.isEmpty(imgUrl) && imgUrl.endsWith("?imageslim")) {
            imgUrl = imgUrl.concat("|imageView2/1/w/").concat(String.valueOf(width)).concat("/h/").concat(String.valueOf(height));
        }
        return imgUrl;
    }

    public static void loadCustomCornerImage(Context context, String url, ImageView imageView, int roundingRadius) {
        if (StringUtil.isEmpty(url)) {
            return;
        }

        GlideApp.with(context)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transforms(new CenterCrop(),
                        new RoundedCorners(roundingRadius))
                .placeholder(R.drawable.bg_default_cover_round_placehold_5)
                .error(R.drawable.bg_default_cover_round_placehold_5)
                .into(imageView);
    }

    //亲测无效
    public static void loadNinePatchImage(Context context, String url, TextView textView, int defaultResId) {
        GlideApp.with(context).asBitmap().load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL).into(new ViewTarget<TextView, Bitmap>(textView) {
            /**
             * The method that will be called when the resource load has finished.
             *
             * @param resource   the loaded resource.
             * @param transition
             */
            @Override
            public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                byte[] chunk = resource.getNinePatchChunk();
                if (NinePatch.isNinePatchChunk(chunk)) {
                    textView.setBackgroundDrawable(new NinePatchDrawable(context.getResources(), resource, chunk, new Rect(), null));
                } else {
                    textView.setBackgroundDrawable(new BitmapDrawable(resource));
                }
            }

        });
    }

    private Drawable getNinePatchDrawable(Bitmap bitmap, Context context) {
        byte[] chunk = bitmap.getNinePatchChunk();
        if (NinePatch.isNinePatchChunk(chunk)) {
            return new NinePatchDrawable(context.getResources(), bitmap, chunk, new Rect(), null);
        } else {
            return new BitmapDrawable(bitmap);
        }
        //说明：即使这样，你会发现点9的右线和低线的文字区域还是不管用，不过这已经不影响我们使用啦。
    }

    /**
     * 首页banner图统一加载方式
     *
     * @param context
     * @param url
     * @param imageView
     */
    public static void loadBannerRoundBg(Context context, String url, ImageView imageView) {
        if (StringUtil.isEmpty(url)) {
            return;
        }
        StringBuffer sb = new StringBuffer(url);
        if (url.contains(ACCESS_URL)) {
            if (!url.contains("?")) {
                sb.append(PIC_PROCESSING);
            }
            sb.append("|imageView2/1/w/660/h/220");
        }

        GlideApp.with(context)
                .load(sb.toString())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transforms(new CenterCrop(),
                        new RoundedCorners(context.getResources().getDimensionPixelOffset(R.dimen.common_cover_round_size)))
                .placeholder(R.drawable.bg_default_cover_round_placehold_5)
                .error(R.drawable.bg_default_cover_round_placehold_5)
                .into(imageView);
    }

    /**
     * 首页banner图统一加载方式
     *
     * @param context
     * @param url
     * @param imageView
     */
    public static void loadBannerRoundBg(Context context, String url, ImageView imageView, int roundCorner) {
        if (StringUtil.isEmpty(url)) {
            return;
        }
        StringBuffer sb = new StringBuffer(url);
        if (url.contains(ACCESS_URL)) {
            if (!url.contains("?")) {
                sb.append(PIC_PROCESSING);
            }
            sb.append("|imageView2/1/w/660/h/220");
        }

        GlideApp.with(context)
                .load(sb.toString())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transforms(new CenterCrop(),
                        new RoundedCorners(roundCorner))
                .placeholder(R.drawable.bg_default_cover_round_placehold_5)
                .error(R.drawable.bg_default_cover_round_placehold_5)
                .into(imageView);
    }

    /**
     * 首页banner图统一加载方式
     *
     * @param context
     * @param url
     * @param imageView
     */
    public static void loadBannerRoundBgWithOutPlaceHolder(Context context, String url, ImageView imageView) {
        if (StringUtil.isEmpty(url)) {
            return;
        }
        StringBuffer sb = new StringBuffer(url);
        if (url.contains(ACCESS_URL)) {
            if (!url.contains("?")) {
                sb.append(PIC_PROCESSING);
            }
            sb.append("|imageView2/1/w/660/h/220");
        }

        GlideApp.with(context)
                .load(sb.toString())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transforms(new CenterCrop(),
                        new RoundedCorners(context.getResources().getDimensionPixelOffset(R.dimen.common_cover_round_size)))
                .error(R.drawable.bg_default_cover_round_placehold_5)
                .into(imageView);
    }

    /**
     * 首页banner图统一加载方式
     *
     * @param context
     * @param url
     * @param imageView
     * @param roundCorner in px
     */
    public static void loadBannerRoundBgWithOutPlaceHolder(Context context, String url, ImageView imageView,
                                                           int roundCorner, int defaultResId) {
        if (StringUtil.isEmpty(url)) {
            return;
        }

        GlideApp.with(context)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontAnimate()
                .transforms(new CenterCrop(),
                        new RoundedCorners(roundCorner))
                .placeholder(defaultResId)
                .error(defaultResId)
                .into(imageView);
    }

    /**
     * 用于加载高度固定但是宽度不固定的图片情况
     * @param context
     * @param imageUrl
     * @param imageView
     * @param height
     */
    public static void loadImageWidthLimitView(Context context, String imageUrl, ImageView imageView, int height) {
        GlideApp.with(context)
                .load(imageUrl)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object o,
                                                Target<Drawable> target, boolean b) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable drawable, Object o,
                                                   Target<Drawable> target,
                                                   DataSource dataSource, boolean b) {
                        if (height > 0) {
                            ViewGroup.LayoutParams lp = imageView.getLayoutParams();
                            lp.width = (height * drawable.getIntrinsicWidth()) / drawable.getIntrinsicHeight();
                            imageView.setLayoutParams(lp);
                        }
                        return false;
                    }
                })
                .into(imageView);
    }
}
