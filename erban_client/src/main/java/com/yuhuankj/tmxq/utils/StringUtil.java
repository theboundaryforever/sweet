package com.yuhuankj.tmxq.utils;

import android.content.Context;
import android.text.TextUtils;

import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.yuhuankj.tmxq.R;

/**
 * @author weihaitao
 * @date 2019/5/6
 */
public class StringUtil {

    public static String limitStr(Context context, String str, int limit) {
        if (GlideContextCheckUtil.checkContextUsable(context) && !TextUtils.isEmpty(str) && str.length() > limit) {
            str = context.getResources().getString(R.string.nick_length_max_six, str.substring(0, limit));
        }
        return str;
    }

}
