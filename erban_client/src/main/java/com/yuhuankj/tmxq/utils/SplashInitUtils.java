package com.yuhuankj.tmxq.utils;

import android.content.Context;
import android.net.http.HttpResponseCache;
import android.text.TextUtils;

import com.mob.MobSDK;
import com.qiniu.android.utils.ContextGetter;
import com.tencent.bugly.crashreport.CrashReport;
import com.tongdaxing.erban.libcommon.coremanager.CoreFactory;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.http_image.image.ImageManager;
import com.tongdaxing.erban.libcommon.net.ErBanAllHostnameVerifier;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.RxNet;
import com.tongdaxing.erban.libcommon.ninepatchloader.NinePatchBitmapLoader;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SiemulatorCheckUtil;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.CoreRegisterCenter;
import com.tongdaxing.xchat_core.Env;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.home.IHomeCore;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCore;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCore;
import com.tongdaxing.xchat_core.im.login.IIMLoginCore;
import com.tongdaxing.xchat_core.im.message.IIMMessageCore;
import com.tongdaxing.xchat_core.im.notification.INotificationCore;
import com.tongdaxing.xchat_core.im.sysmsg.ISysMsgCore;
import com.tongdaxing.xchat_core.pay.IPayCore;
import com.tongdaxing.xchat_core.realm.IRealmCore;
import com.tongdaxing.xchat_core.redpacket.IRedPacketCore;
import com.tongdaxing.xchat_core.room.face.IFaceCore;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.umeng.analytics.MobclickAgent;
import com.umeng.commonsdk.UMConfigure;

import java.io.File;
import java.io.IOException;

import cn.jpush.android.api.JPushInterface;

import static com.tongdaxing.xchat_core.Constants.CRASH_REPORT_KEY;
import static com.tongdaxing.xchat_core.Constants.CRASH_REPORT_KEY_DEBUG;

/**
 * @author weihaitao
 * @date 2019/5/13
 */
public class SplashInitUtils {

    private static SplashInitUtils instance = null;
    private final String TAG = SplashInitUtils.class.getSimpleName();
    private boolean hasInited = false;

    private SplashInitUtils() {

    }

    public static SplashInitUtils getInstance() {
        if (null == instance) {
            instance = new SplashInitUtils();
        }

        return instance;
    }

    public boolean isHasInited() {
        return hasInited;
    }

    public void setHasInited(boolean hasInited) {
        this.hasInited = hasInited;
    }

    public void initIfNeed(Context context) {
        LogUtils.d(TAG, "initIfNeed-hasInited:" + hasInited);
        if (!hasInited) {
            LogUtils.d(TAG, "initIfNeed-->initiate");
            initiate(context);
            hasInited = true;
        }
    }

    public static void initBasicConfig() {
        BasicConfig.INSTANCE.setAppContext(ContextGetter.applicationContext());
        SiemulatorCheckUtil.chechSiemulatorEnv(BasicConfig.INSTANCE.getAppContext());
        //切换生产坏境和测试环境 true/测试环境 false/生产环境
        BasicConfig.INSTANCE.setDebuggable(BasicConfig.isDebug);
        BasicConfig.INSTANCE.setChannel("tt");

        Env.instance().init();
        BasicConfig.INSTANCE.setRootDir(Constants.ERBAN_DIR_NAME);
        BasicConfig.INSTANCE.setLogDir(Constants.LOG_DIR);
        BasicConfig.INSTANCE.setConfigDir(Constants.CONFIG_DIR);
        BasicConfig.INSTANCE.setVoiceDir(Constants.VOICE_DIR);
        BasicConfig.INSTANCE.setCacheDir(Constants.CACHE_DIR);
        try {
            File cacheDir = new File(Constants.ERBAN_DIR_NAME, "http");
            HttpResponseCache.install(cacheDir, 1024 * 1024 * 128);
        } catch (IOException e) {
            e.printStackTrace();
        }
        NinePatchBitmapLoader.getInstance().init(ContextGetter.applicationContext());
    }

    private void initCrashReportSdk(Context applicationContext) {
        // 设置是否为上报进程
        CrashReport.UserStrategy strategy = new CrashReport.UserStrategy(applicationContext);
        //只在主进程下上报数据
        strategy.setUploadProcess(true);
        //bugly初始化
        CrashReport.initCrashReport(applicationContext, BasicConfig.isDebug ?
                CRASH_REPORT_KEY_DEBUG : CRASH_REPORT_KEY, BasicConfig.isDebug, strategy);
        CrashReport.setAppChannel(applicationContext, BasicConfig.INSTANCE.getChannel());
        CrashReport.setIsDevelopmentDevice(applicationContext, BasicConfig.isDebug);
        CrashReport.putUserData(applicationContext, "isDevelopEnv", BasicConfig.isDebug + "");
        CrashReport.putUserData(applicationContext, "killBySecCheck", "false");
        CrashReport.putUserData(applicationContext, "cpuCores", String.valueOf(SiemulatorCheckUtil.getNumberOfCPUCores()));

        if(!TextUtils.isEmpty(SiemulatorCheckUtil.getCurrSimulatorEnv())){
            CrashReport.putUserData(applicationContext, "runEnv", SiemulatorCheckUtil.getSiemulatorCrashReportValue());
        }
    }

    private void initUMeng(Context applicationContext) {
        UMConfigure.setLogEnabled(BasicConfig.isDebug);
        UMConfigure.init(applicationContext, Constants.UMENG_KEY, BasicConfig.INSTANCE.getChannel(),
                UMConfigure.DEVICE_TYPE_PHONE, "");
        UMConfigure.setEncryptEnabled(!BasicConfig.isDebug);
        MobclickAgent.setCatchUncaughtExceptions(false);
        MobclickAgent.openActivityDurationTrack(false);
    }

    /**
     * 极光初始化
     */
    private void initJPushSdk(Context applicationContext) {
        JPushInterface.setDebugMode(BasicConfig.isDebug);
//            JPushInterface.setChannel(this,channel);
        JPushInterface.init(applicationContext);
//        BasicPushNotificationBuilder builder = new BasicPushNotificationBuilder(this);
//        //设置为自动消失和呼吸灯闪烁
//        builder.notificationFlags = Notification.FLAG_AUTO_CANCEL
//                | Notification.FLAG_SHOW_LIGHTS;
//        // 设置为铃声、震动、呼吸灯闪烁都要
//        builder.notificationDefaults = Notification.DEFAULT_SOUND
//                | Notification.DEFAULT_VIBRATE
//                | Notification.DEFAULT_LIGHTS;
//        JPushInterface.setDefaultPushNotificationBuilder(builder);
    }

    public static void initRxNet(Context applicationContext, String url) {
        RxNet.init(applicationContext)
                .debug(BasicConfig.isDebug)
                .setBaseUrl(url)
                .certificates()
                .hostnameVerifier(new ErBanAllHostnameVerifier())
                .build();
    }

    public static void initRxNet(String url) {
        if(null != ContextGetter.applicationContext()){
            RxNet.init(ContextGetter.applicationContext())
                    .debug(BasicConfig.isDebug)
                    .setBaseUrl(url)
                    .certificates()
                    .hostnameVerifier(new ErBanAllHostnameVerifier())
                    .build();
        }
    }

    public static void initCore() {
        //corexxxw
//        CoreManager.init(BasicConfig.INSTANCE.getLogDir().getAbsolutePath());
        CoreRegisterCenter.registerCore();

        CoreManager.getCore(IRealmCore.class);
        CoreManager.getCore(IUserCore.class);
        CoreManager.getCore(IRedPacketCore.class);
        CoreManager.getCore(IIMLoginCore.class);
        CoreManager.getCore(IIMFriendCore.class);
        CoreManager.getCore(IGiftCore.class);
        CoreManager.getCore(IFaceCore.class);
        CoreManager.getCore(IPayCore.class);
        CoreManager.getCore(IIMMessageCore.class);
        CoreManager.getCore(IAVRoomCore.class);

        //系统通知的到达和已读
        CoreManager.getCore(ISysMsgCore.class).registSystemMessageObserver(true);
        //注册im用户状态的系统通知
        CoreManager.getCore(IIMLoginCore.class).registAuthServiceObserver(true);
        //其他端登录观察者
//        CoreManager.getCore(IIMLoginCore.class).registerOtherClientsObserver(true);
        //全局自定义通知
        CoreManager.getCore(INotificationCore.class).observeCustomNotification(true);
    }

    private void initiate(Context applicationContext) {
        long initStartTime = System.currentTimeMillis();
        LogUtils.d(TAG, "initIfNeed-->initiate-->initCrashReportSdk");
        initCrashReportSdk(applicationContext);
        long initCrashReportSdkTime = System.currentTimeMillis();
        LogUtils.d(TAG, "initIfNeed-->initiate-->initCrashReportSdk 耗时:" + (initCrashReportSdkTime - initStartTime));

        LogUtils.d(TAG, "initIfNeed-->initiate-->initUMeng");
        initUMeng(applicationContext);
        long initUMengSdkTime = System.currentTimeMillis();
        LogUtils.d(TAG, "initIfNeed-->initiate-->initUMeng 耗时:" + (initUMengSdkTime - initCrashReportSdkTime));

        LogUtils.d(TAG, "initIfNeed-->initiate-->initJPushSdk");
        initJPushSdk(applicationContext);
        long initJPushSdkTime = System.currentTimeMillis();
        LogUtils.d(TAG, "initIfNeed-->initiate-->initJPushSdk 耗时:" + (initJPushSdkTime - initUMengSdkTime));

        LogUtils.d(TAG, "initIfNeed-->initiate-->MobSDK");
        MobSDK.init(applicationContext);
        long initMobSDKTime = System.currentTimeMillis();
        LogUtils.d(TAG, "initIfNeed-->initiate-->MobSDK 耗时:" + (initMobSDKTime - initJPushSdkTime));

        LogUtils.d(TAG, "initIfNeed-->initiate-->initRxNet");
        initRxNet(applicationContext, UriProvider.JAVA_WEB_URL);
        long initRxNetTime = System.currentTimeMillis();
        LogUtils.d(TAG, "initIfNeed-->initiate-->initRxNet 耗时:" + (initRxNetTime - initMobSDKTime));

        LogUtils.d(TAG, "initIfNeed-->initiate-->initCore");
        CoreFactory.doNotReInitCoreAction();
        initCore();
        long initCoreTime = System.currentTimeMillis();
        LogUtils.d(TAG, "initIfNeed-->initiate-->initCore 耗时:" + (initCoreTime - initRxNetTime));

        ImageManager.instance().init(applicationContext, Constants.IMAGE_CACHE_DIR);
        OkHttpManager.getInstance().init(applicationContext);
        CoreManager.getCore(IHomeCore.class).getMainTabData();
        CoreFactory.setHasCoreManagerInited(true);

        NinePatchBitmapLoader.getInstance().init(applicationContext);

        long initEndTime = System.currentTimeMillis();
        LogUtils.d(TAG, "initIfNeed-->initiate- 总耗时:" + (initEndTime - initStartTime));
    }
}
