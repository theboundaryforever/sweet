package com.yuhuankj.tmxq.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yuhuankj.tmxq.R;

/**
 * <p> main tab 有消息个数 控件  (todo:还可以优化，将字体写在字体上面)
 * </p>
 * Created by Administrator on 2017/11/14.
 */
public class MainRedPointTab extends RelativeLayout {
    private MainTab mMainTab;
    private TextView mTvNum;

    public MainRedPointTab(@NonNull Context context) {
        this(context, null);
    }

    public MainRedPointTab(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MainRedPointTab(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.maint_tab_red_poin_layout, this);
        setGravity(Gravity.CENTER);

        mMainTab = (MainTab) findViewById(R.id.main_tab_msg);
        mTvNum = (TextView) findViewById(R.id.msg_number);
    }

    public void select(boolean select) {
        mMainTab.select(select);
    }

    @SuppressLint("SetTextI18n")
    public void setNumber(int number) {
        mTvNum.setVisibility(number <= 0 ? GONE : VISIBLE);
        if (number > 99) {
            mTvNum.setText("99+");
        } else
            mTvNum.setText(String.valueOf(number));
    }
}
