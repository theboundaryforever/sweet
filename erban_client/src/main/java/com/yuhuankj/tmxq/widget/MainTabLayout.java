package com.yuhuankj.tmxq.widget;


import android.content.Context;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.yuhuankj.tmxq.R;

/**
 * <p> 底部tab导航 </p>
 * Created by Administrator on 2017/11/14.
 */
public class MainTabLayout extends LinearLayout implements View.OnClickListener {

    private MainTab mHomeTab, mAttentionTab, mMeTab, mtGameHome;
    private MainRedPointTab mMsgTab;
    private int mLastPosition = -1;

    public MainTabLayout(Context context) {
        this(context, null);
    }

    public MainTabLayout(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MainTabLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        setOrientation(HORIZONTAL);
        inflate(context, R.layout.main_tab_layout, this);

        mHomeTab = findViewById(R.id.main_home_tab);
        mAttentionTab = findViewById(R.id.main_attention_tab);
        mMsgTab = findViewById(R.id.main_msg_tab);
        mMeTab = findViewById(R.id.main_me_tab);
        mtGameHome = findViewById(R.id.mtGameHome);

        mHomeTab.setOnClickListener(this);
        mAttentionTab.setOnClickListener(this);
        mMsgTab.setOnClickListener(this);
        mMeTab.setOnClickListener(this);
        mtGameHome.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mtGameHome:
                select(0);//4--0
                break;
            case R.id.main_home_tab:
                select(1);//0--1
                break;
            case R.id.main_attention_tab:
                select(2);//1--2
                break;
            case R.id.main_msg_tab:
                select(3);//2--3
                break;
            case R.id.main_me_tab:
                select(4);//3--4
                break;
            default:
                break;
        }
    }

    public void setMsgNum(int number) {
        mMsgTab.setNumber(number);
    }

    public void select(int position) {
        if (mLastPosition == position) {
            return;
        }
        mtGameHome.select(position == 0);
        mHomeTab.select(position == 1);
        mAttentionTab.select(position == 2);
        mMsgTab.select(position == 3);
        mMeTab.select(position == 4);
        if (mOnTabClickListener != null) {
            mOnTabClickListener.onTabClick(position);
        }
        mLastPosition = position;
    }

    private OnTabClickListener mOnTabClickListener;

    public void setOnTabClickListener(OnTabClickListener onTabClickListener) {
        mOnTabClickListener = onTabClickListener;
    }

    public interface OnTabClickListener {
        void onTabClick(int position);
    }

    @Nullable
    @Override
    protected Parcelable onSaveInstanceState() {
        return super.onSaveInstanceState();
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        super.onRestoreInstanceState(state);

    }
}
