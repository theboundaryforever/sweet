package com.yuhuankj.tmxq.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

public class CacheImageView extends AppCompatImageView {
    private String imageUrl;

    public CacheImageView(Context context) {
        super(context);
    }

    public CacheImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CacheImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
