package com.yuhuankj.tmxq.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

import com.tongdaxing.erban.libcommon.utils.DisplayUtility;
import com.yuhuankj.tmxq.R;

public class CustomTrapezoidView extends View {
    private int triangleWidth;
    private int upperSideLength;

    private Paint paint;
    private Path path;

    public CustomTrapezoidView(Context context) {
        this(context, null);
    }

    public CustomTrapezoidView(Context context, AttributeSet attr) {
        this(context, attr, 0);
    }

    public CustomTrapezoidView(Context context, AttributeSet attr, int i) {
        super(context, attr, i);
        TypedArray array = context.obtainStyledAttributes(attr, R.styleable.CustomTrapezoidView);
        triangleWidth = (int) array.getDimension(R.styleable.CustomTrapezoidView_triangleWidth, DisplayUtility.dp2px(context, 10));
        upperSideLength = (int) array.getDimension(R.styleable.CustomTrapezoidView_upperSideLength, 0);
        int trapeziumColor = array.getColor(R.styleable.CustomTrapezoidView_upperSideLength, Color.parseColor("#d918121e"));
        paint = new Paint();
        paint.setColor(trapeziumColor);
        paint.setStyle(Paint.Style.FILL);
        path = new Path();
        array.recycle();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (upperSideLength > 0) {
            path.moveTo(getWidth() - triangleWidth - upperSideLength, getHeight());
            path.lineTo(getWidth() - upperSideLength, 0);
        } else {
            path.moveTo(getWidth() - triangleWidth, getHeight());
        }

        path.lineTo(getWidth(), 0);
        path.lineTo(getWidth(), getHeight());
        path.lineTo(getWidth() - triangleWidth - upperSideLength, getHeight());
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawPath(path, paint);
    }
}
