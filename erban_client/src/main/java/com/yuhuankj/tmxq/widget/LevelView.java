package com.yuhuankj.tmxq.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.juxiao.library_utils.DisplayUtils;
import com.tongdaxing.erban.libcommon.glide.GlideContextCheckUtil;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.StringUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.yuhuankj.tmxq.R;
import com.yuhuankj.tmxq.utils.ImageLoadUtils;


/**
 * Created by Administrator on 2018/3/10.
 */

public class LevelView extends LinearLayout {

    private final String TAG = LevelView.class.getSimpleName();

    ImageView mExperLevel;
    ImageView mCharmLevel;
    private Context mContext;
    private View mInflate;

    private int maxHeight = 0;

    public LevelView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        maxHeight = DisplayUtils.dip2px(mContext, 15);
        mContext = context;
        mInflate = View.inflate(context, R.layout.level_view, this);
        mCharmLevel = mInflate.findViewById(R.id.charm_level);
        mExperLevel = mInflate.findViewById(R.id.exper_level);
        if (maxHeight > 0) {
            LinearLayout.LayoutParams lp = (LayoutParams) mCharmLevel.getLayoutParams();
            lp.height = maxHeight;
            mCharmLevel.setLayoutParams(lp);
            mCharmLevel.setAdjustViewBounds(true);

            LinearLayout.LayoutParams lp1 = (LayoutParams) mExperLevel.getLayoutParams();
            lp1.height = maxHeight;
            mExperLevel.setLayoutParams(lp1);
            mExperLevel.setAdjustViewBounds(true);
        }
    }

    public LevelView(Context context) {
        this(context, null);
    }

    public ImageView getExperLevel() {
        return mExperLevel;
    }

    public ImageView getCharmLevel() {
        return mCharmLevel;
    }

    /**
     * 设置等级图片的实际高度，用户levelview有需要自定义高度而非wrapcontent时
     *
     * @param maxHeight
     */
    public void setLevelImageViewHeight(int maxHeight) {
        this.maxHeight = maxHeight;
        if (maxHeight > 0) {
            if (null != mCharmLevel) {
                LinearLayout.LayoutParams lp = (LayoutParams) mCharmLevel.getLayoutParams();
                lp.height = maxHeight;
                mCharmLevel.setLayoutParams(lp);
                mCharmLevel.setAdjustViewBounds(true);
            }

            if (null != mExperLevel) {
                LinearLayout.LayoutParams lp1 = (LayoutParams) mExperLevel.getLayoutParams();
                lp1.height = maxHeight;
                mExperLevel.setLayoutParams(lp1);
                mExperLevel.setAdjustViewBounds(true);
            }

            if (null != mExperLevel || null != mCharmLevel) {
                requestLayout();
            }
        }
    }

    //设置财富等级
    public void setExperLevel(int experLevel) {
        LogUtils.d(TAG, "setExperLevel-experLevel:" + experLevel);
        if (experLevel < 1) {
            mExperLevel.setVisibility(GONE);
            return;
        }
        if (experLevel > 100) {
            experLevel = 100;
        }
        mExperLevel.setVisibility(VISIBLE);
//        int drawableId = getResources().getIdentifier("lv" + experLevel, "drawable", mContext.getPackageName());
//        mExperLevel.setImageResource(drawableId);
        if (GlideContextCheckUtil.checkContextUsable(getContext())) {
            ImageLoadUtils.loadImageWidthLimitView(getContext(), UriProvider.getCFImgUrl(experLevel), mExperLevel,maxHeight);
        }
    }

    //设置魅力等级
    public void setCharmLevel(int charmLevel) {
        LogUtils.d(TAG, "setCharmLevel-charmLevel:" + charmLevel);
        if (charmLevel < 1) {
            mCharmLevel.setVisibility(GONE);
            return;
        }

        if (charmLevel > 100) {
            charmLevel = 100;
        }
        mCharmLevel.setVisibility(VISIBLE);
//        int drawableId = getResources().getIdentifier("ml" + charmLevel, "drawable", mContext.getPackageName());
//
//        mCharmLevel.setImageResource(drawableId);
//        LogUtils.d(TAG, "setCharmLevel-drawableId" + drawableId + "   " + R.drawable.ml20);
        if (GlideContextCheckUtil.checkContextUsable(getContext())) {
            ImageLoadUtils.loadImageWidthLimitView(getContext(), UriProvider.getMLImgUrl(charmLevel), mCharmLevel,maxHeight);
        }
    }


    //设置财富等级
    public void setExperLevel(String experLevel) {
        if (StringUtils.isNotEmpty(experLevel)) {
            mExperLevel.setVisibility(View.VISIBLE);
            if (GlideContextCheckUtil.checkContextUsable(getContext())) {
                ImageLoadUtils.loadImageWidthLimitView(getContext(), experLevel, mExperLevel,maxHeight);
            }
        } else {
            mExperLevel.setVisibility(GONE);
        }
    }

    //设置魅力等级
    public void setCharmLevel(String charmLevel) {
        if (StringUtils.isNotEmpty(charmLevel)) {
            mCharmLevel.setVisibility(View.VISIBLE);
            if (GlideContextCheckUtil.checkContextUsable(getContext())) {
                ImageLoadUtils.loadImageWidthLimitView(getContext(), charmLevel, mCharmLevel,maxHeight);
            }
        } else {
            mCharmLevel.setVisibility(GONE);
        }
    }
}
