package com.yuhuankj.tmxq.thirdsdk.jpush;

import android.content.Context;

import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.erban.libcommon.utils.config.SpEvent;

import cn.jpush.android.api.JPushMessage;
import cn.jpush.android.service.JPushMessageReceiver;

public class JPushMessageBroadcast extends JPushMessageReceiver {

    private final String TAG = JPushMessageBroadcast.class.getSimpleName();

    @Override
    public void onAliasOperatorResult(Context context, JPushMessage jPushMessage) {
        super.onAliasOperatorResult(context, jPushMessage);
        String currAlias = jPushMessage.getAlias();
        int errCode = jPushMessage.getErrorCode();
        if (0 != errCode) {
            SpUtils.remove(context, SpEvent.has_set_alias);
        }
        LogUtils.d(TAG, "onAliasOperatorResult-currAlias:" + currAlias + " errCode:" + errCode);
    }
}
