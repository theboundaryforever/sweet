package com.yuhuankj.tmxq.thirdsdk.nim;

import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.text.TextUtils;

import com.netease.nim.uikit.session.NIMSessionStatusManager;
import com.tencent.bugly.crashreport.BuglyLog;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.config.BasicConfig;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yuhuankj.tmxq.R;

public class RingPersonalMessageManager {

    private final String TAG = RingPersonalMessageManager.class.getSimpleName();

    private Ringtone rt;

    public RingPersonalMessageManager() {
    }

    /**
     * 私聊消息铃声提示,这个仅仅用户在线状态，接收到私聊消息提醒时，过滤铃声提醒
     */
    public void ringOnRecvivePersonalMessage() {
        //设置接收消息后的提示铃声--由于云信的铃声设置失败，暂时自行实现
//        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        LogUtils.d(TAG, "ringOnRecvivePersonalMessage-isChatting:" + NIMSessionStatusManager.getInstance().isChatting());
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (!NIMSessionStatusManager.getInstance().isChatting() && (null != userInfo && userInfo.getMessageRingRestriction() == 1)) {
            String uriPath = "android.resource://" + BasicConfig.INSTANCE.getAppContext().getPackageName() + "/" + R.raw.ring_on_receive_msg;
            Uri uri = Uri.parse(uriPath);
            if (null != uri && !TextUtils.isEmpty(uri.toString())) {
                if (null != rt) {
                    if (rt.isPlaying()) {
                        rt.stop();
                    }
                    rt = null;
                }
                rt = RingtoneManager.getRingtone(BasicConfig.INSTANCE.getAppContext(), uri);
                rt.setStreamType(5);
                rt.play();
            }
            BuglyLog.d(TAG, "ringOnRecvivePersonalMessage-uri:" + uri);
        }
    }

    public void release() {
        if (null != rt) {
            if (rt.isPlaying()) {
                rt.stop();
            }
            rt = null;
        }
    }
}
