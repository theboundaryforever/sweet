package com.yuhuankj.tmxq.thirdsdk.jpush;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.NotchFixUtil;
import com.tongdaxing.erban.libcommon.utils.json.Json;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yuhuankj.tmxq.ui.launch.splash.SplashActivity;
import com.yuhuankj.tmxq.ui.liveroom.RoomServiceScheduler;
import com.yuhuankj.tmxq.ui.webview.CommonWebViewActivity;

import cn.jiguang.api.JCoreInterface;
import cn.jpush.android.api.JPushInterface;

public class JpushActionBroadcast extends BroadcastReceiver {

    private final String TAG = JpushActionBroadcast.class.getSimpleName();

    /**
     * 小米和魅族、VIVO是通过极光回调自己判断如何跳转，被强杀场景下，传递给SplashActivity的参数名
     */
    public static final String JPushExtras = "JPushCustomExtras";

    @Override
    public void onReceive(Context context, Intent intent) {

        try {
            Bundle bundle = intent.getExtras();
            if (JPushInterface.ACTION_REGISTRATION_ID.equals(intent.getAction())) {
                String regId = bundle.getString(JPushInterface.EXTRA_REGISTRATION_ID);
                LogUtils.d(TAG,"onReceive-ACTION_REGISTRATION_ID regId:"+regId);
                //send the Registration Id to your server...

            } else if (JPushInterface.ACTION_MESSAGE_RECEIVED.equals(intent.getAction())) {
                String title = bundle.getString(JPushInterface.EXTRA_TITLE);
                LogUtils.d(TAG,"onReceive-ACTION_MESSAGE_RECEIVED title:"+title);
                String message = bundle.getString(JPushInterface.EXTRA_MESSAGE);
                LogUtils.d(TAG,"onReceive-ACTION_MESSAGE_RECEIVED message:"+message);
                String extras = bundle.getString(JPushInterface.EXTRA_EXTRA);
                LogUtils.d(TAG,"onReceive-ACTION_MESSAGE_RECEIVED extras:"+extras);
                String msgId = bundle.getString(JPushInterface.EXTRA_MSG_ID);
                LogUtils.d(TAG,"onReceive-ACTION_MESSAGE_RECEIVED msgId:"+msgId);

            } else if (JPushInterface.ACTION_NOTIFICATION_RECEIVED.equals(intent.getAction())) {
                String title = bundle.getString(JPushInterface.EXTRA_NOTIFICATION_TITLE);
                LogUtils.d(TAG,"onReceive-ACTION_NOTIFICATION_RECEIVED title:"+title);
                String content = bundle.getString(JPushInterface.EXTRA_ALERT);
                LogUtils.d(TAG,"onReceive-ACTION_NOTIFICATION_RECEIVED content:"+content);
                String extras = bundle.getString(JPushInterface.EXTRA_EXTRA);
                LogUtils.d(TAG,"onReceive-ACTION_NOTIFICATION_RECEIVED extras:"+extras);
                int notificationId = bundle.getInt(JPushInterface.EXTRA_NOTIFICATION_ID);
                LogUtils.d(TAG,"onReceive-ACTION_NOTIFICATION_RECEIVED notificationId:"+notificationId);
                String fileHtml = bundle.getString(JPushInterface.EXTRA_RICHPUSH_HTML_PATH);
                LogUtils.d(TAG,"onReceive-ACTION_NOTIFICATION_RECEIVED fileHtml:"+fileHtml);
                String fileStr = bundle.getString(JPushInterface.EXTRA_RICHPUSH_HTML_RES);
                LogUtils.d(TAG,"onReceive-ACTION_NOTIFICATION_RECEIVED fileStr:"+fileStr);
//                String[] fileNames = fileStr.split(",");
                String msgId = bundle.getString(JPushInterface.EXTRA_MSG_ID);
                LogUtils.d(TAG,"onReceive-ACTION_NOTIFICATION_RECEIVED msgId:"+msgId);
                String bigText = bundle.getString(JPushInterface.EXTRA_BIG_TEXT);
                LogUtils.d(TAG,"onReceive-ACTION_NOTIFICATION_RECEIVED bigText:"+bigText);
                String bigPicPath = bundle.getString(JPushInterface.EXTRA_BIG_PIC_PATH);
                LogUtils.d(TAG,"onReceive-ACTION_NOTIFICATION_RECEIVED bigPicPath:"+bigPicPath);
                String inboxJson = bundle.getString(JPushInterface.EXTRA_INBOX);
                LogUtils.d(TAG,"onReceive-ACTION_NOTIFICATION_RECEIVED inboxJson:"+inboxJson);
                String prio = bundle.getString(JPushInterface.EXTRA_NOTI_PRIORITY);
                LogUtils.d(TAG,"onReceive-ACTION_NOTIFICATION_RECEIVED prio:"+prio);
                String category = bundle.getString(JPushInterface.EXTRA_NOTI_CATEGORY);
                LogUtils.d(TAG,"onReceive-ACTION_NOTIFICATION_RECEIVED category:"+category);

            } else if (JPushInterface.ACTION_NOTIFICATION_OPENED.equals(intent.getAction())) {
                String title = bundle.getString(JPushInterface.EXTRA_NOTIFICATION_TITLE);
                LogUtils.d(TAG,"onReceive-ACTION_NOTIFICATION_OPENED title:"+title);
                String content = bundle.getString(JPushInterface.EXTRA_ALERT);
                LogUtils.d(TAG,"onReceive-ACTION_NOTIFICATION_OPENED content:"+content);
                String extra = bundle.getString(JPushInterface.EXTRA_EXTRA);
                LogUtils.d(TAG,"onReceive-ACTION_NOTIFICATION_OPENED extra:"+extra);
                String message = bundle.getString(JPushInterface.EXTRA_MESSAGE);
                LogUtils.d(TAG, "onReceive-ACTION_NOTIFICATION_OPENED message:" + message);
                int notificationId = bundle.getInt(JPushInterface.EXTRA_NOTIFICATION_ID);
                LogUtils.d(TAG,"onReceive-ACTION_NOTIFICATION_OPENED notificationId:"+notificationId);
                String msgId = bundle.getString(JPushInterface.EXTRA_MSG_ID);
                LogUtils.d(TAG,"onReceive-ACTION_NOTIFICATION_OPENED msgId:"+msgId);

                boolean isLogin = CoreManager.getCore(IAuthCore.class).isLogin();
                LogUtils.d(TAG, "onReceive-ACTION_NOTIFICATION_OPENED isLogin:" + isLogin);
                LogUtils.d(TAG, "onReceive-ACTION_NOTIFICATION_OPENED startTime:" + SplashActivity.startTime);
                //没登录的时候不响应事件
                if (!isLogin) {
                    return;
                }
                Json parse = Json.parse(extra);
                onClickEvent(context, parse, CoreManager.getCore(IAuthCore.class).getCurrentUid());

            } else if (JPushInterface.ACTION_NOTIFICATION_CLICK_ACTION.equals(intent.getAction())) {
                LogUtils.d(TAG, "onReceive-ACTION_NOTIFICATION_CLICK_ACTION");
            } else if (JPushInterface.ACTION_RICHPUSH_CALLBACK.equals(intent.getAction())) {
                LogUtils.d(TAG, "onReceive-ACTION_RICHPUSH_CALLBACK");
            } else if (JPushInterface.ACTION_CONNECTION_CHANGE.equals(intent.getAction())) {
                boolean connected = bundle.getBoolean(JPushInterface.EXTRA_CONNECTION_CHANGE, false);
                LogUtils.d(TAG,"onReceive-ACTION_CONNECTION_CHANGE connected:"+connected);
                String regId = JCoreInterface.getRegistrationID(context);
                LogUtils.d(TAG, "onReceive-ACTION_CONNECTION_CHANGE regId:" + regId);

            } else {
                LogUtils.d(TAG,"onReceive-Unhandled intent action:"+intent.getAction());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onClickEvent(Context context, Json json, long currentUid) {
        LogUtils.d(TAG,"onClickEvent-json:"+json+" currentUid:"+currentUid);
        if (NotchFixUtil.check(NotchFixUtil.ROM_MIUI) || NotchFixUtil.check(NotchFixUtil.ROM_FLYME)
                || NotchFixUtil.check(NotchFixUtil.ROM_VIVO)) {
            if (SplashActivity.startTime == 0L) {
                Intent intent = new Intent(context, SplashActivity.class);
                intent.putExtra(JPushExtras, json.toString());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                return;
            }
        }

        long roomUid = json.num_l("roomUid");
        if (roomUid > 0) {
            if (roomUid == currentUid) {
                return;
            }
            //甜蜜星球V1.0.0.2及后续版本需要新增的字段
            int roomType = RoomInfo.ROOMTYPE_HOME_PARTY;
            if (json.has("roomType")) {
                roomType = json.num("roomType");
            }
            RoomServiceScheduler.getInstance().enterRoomFromJPush(context, roomUid, roomType);
        } else {
            autoJump(context, json.str("activity"), json.str("url"), json.json_ok("params"));
        }
    }

    private void autoJump(Context context, String activity, String url, Json json) {
        LogUtils.d(TAG,"autoJump-activity:"+activity+" url:"+url+" json:"+json);
        if ("".equals(activity)) {
            if (!"".equals(url)) {
                if (url.startsWith("/")) {
                    url = UriProvider.JAVA_WEB_URL + url;
                }
                CommonWebViewActivity.start(context, url);
                return;
            }
        }

        try {
            activity = "com.yuhuankj.tmxq.".concat(activity);
            LogUtils.d(TAG,"autoJump-activity1:"+activity);
            Intent intent = new Intent(context, Class.forName(activity));
            String[] strings = json.key_names();
            for (int i = 0; i < strings.length; i++) {
                String key = strings[i];
                String value = json.str(key);
                intent.putExtra(key, value);
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
