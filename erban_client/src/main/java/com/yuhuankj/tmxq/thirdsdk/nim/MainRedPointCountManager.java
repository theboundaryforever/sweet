package com.yuhuankj.tmxq.thirdsdk.nim;

import android.content.Context;
import android.text.TextUtils;

import com.netease.nim.uikit.session.NIMSessionStatusManager;
import com.netease.nim.uikit.session.OfficSecrRedPointCountManager;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.msg.MsgService;
import com.netease.nimlib.sdk.msg.constant.MsgStatusEnum;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.netease.nimlib.sdk.msg.model.RecentContact;
import com.tencent.bugly.crashreport.BuglyLog;
import com.tongdaxing.erban.libcommon.coremanager.CoreManager;
import com.tongdaxing.erban.libcommon.utils.LogUtils;
import com.tongdaxing.erban.libcommon.utils.SpUtils;
import com.tongdaxing.erban.libcommon.utils.json.JsonParser;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.message.IIMMessageCore;
import com.yuhuankj.tmxq.widget.MainTabLayout;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_QUICK_MATING_CHAT_CARD_FIRST;
import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_SECOND_PRIVATE_CHAT_MSG;

public class MainRedPointCountManager {

    private static final String TAG = MainRedPointCountManager.class.getSimpleName();

    private static MainRedPointCountManager instance = null;

    /**
     * 自甜蜜星球1.0.2.0后，用于记录私聊单对单发送的自定义消息，红点未读数，主要用户主界面底部的tab红点数量提示
     * 留意更新场景：
     * 1.add
     * 2.save
     * 3.restore
     * 4.clear
     * 5.更新到界面,#updatePersonalMessageUnreadCound(com.yuhuankj.tmxq.widget.MainTabLayout)
     * 索引为发送者的sessionId
     */
    private Map<String, Integer> customU2UMsgCountMap = new HashMap<>();

    private MainRedPointCountManager() {

    }

    public static MainRedPointCountManager getInstance() {
        if (null == instance) {
            instance = new MainRedPointCountManager();
        }
        return instance;
    }

    /**
     * 添加私聊会话自定义新消息未读
     * 对应com.yuhuankj.tmxq.ui.MainActivity#onReceivePersonalMessages(java.util.List)
     *
     * @param first
     * @param second
     * @param account
     */
    public void addU2UMsgCount(int first, int second, String account) {
        LogUtils.d(TAG, "addU2UMsgCount-first:" + first + " second:" + second + " account:" + account);

        try {
            if (first == CUSTOM_MSG_QUICK_MATING_CHAT_CARD_FIRST
                    || (first == CustomAttachment.CUSTOM_MSG_FIRST_CALL_UP && second == CUSTOM_MSG_SECOND_PRIVATE_CHAT_MSG)) {
                int unReadCount = 0;
                if (customU2UMsgCountMap.containsKey(account)) {
                    unReadCount = customU2UMsgCountMap.get(account);
                    LogUtils.d(TAG, "addU2UMsgCount-unReadCount0:" + unReadCount);
                }
                unReadCount += 1;
                LogUtils.d(TAG, "addU2UMsgCount-unReadCount1:" + unReadCount);
                customU2UMsgCountMap.put(account, unReadCount);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 移除私聊会话自定义消息未读
     * com.netease.nim.uikit.NimUIKit#startP2PSession(android.content.Context,
     * java.lang.String,
     * com.netease.nimlib.sdk.msg.model.IMMessage, android.os.Bundle)
     * com.netease.nim.uikit.recent.RecentContactsFragment#ignoreUnreadMsg()
     *
     * @param account
     */
    public void delU2UMsgCount(String account) {
        LogUtils.d(TAG, "delU2UMsgCount-account:" + account);
        customU2UMsgCountMap.remove(account);
    }

    /**
     * 本地缓存私聊会话自定义消息未读数，json格式
     * com.yuhuankj.tmxq.ui.MainActivity#onStop()
     *
     * @param context
     * @param uid
     */
    public void saveU2UMsgCount(Context context, long uid) {
        try {
            SpUtils.put(context, "u2u_custom_msg_count".concat(String.valueOf(uid)), JsonParser.toJson(customU2UMsgCountMap));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 从本地缓存中拿存储的未读数消息
     * com.yuhuankj.tmxq.ui.MainActivity#onLogin(com.tongdaxing.xchat_core.auth.AccountInfo)
     *
     * @param context
     * @param uid
     */
    public void restoreU2UMsgCount(Context context, long uid) {
        try {
            String mapStr = (String) SpUtils.get(context,
                    "u2u_custom_msg_count".concat(String.valueOf(uid)), "");
            if (!TextUtils.isEmpty(mapStr)) {
                customU2UMsgCountMap = JsonParser.parseJsonMap(mapStr);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 更新消息未读数
     *
     * @param mMainTabLayout
     */
    public void updatePersonalMessageUnreadCound(MainTabLayout mMainTabLayout) {
        LogUtils.d(TAG, "updatePersonalMessageUnreadCound");
        try {
            if (null != mMainTabLayout) {
                int unreadCount = CoreManager.getCore(IIMMessageCore.class).queryUnreadMsg();
                LogUtils.d(TAG, "updatePersonalMessageUnreadCound-queryUnreadMsg:" + unreadCount);
                unreadCount -= OfficSecrRedPointCountManager.getRedCountDelNum();
                LogUtils.d(TAG, "updatePersonalMessageUnreadCound-redCountDelNum:" + OfficSecrRedPointCountManager.getRedCountDelNum());
                unreadCount += OfficSecrRedPointCountManager.likeCount;
                LogUtils.d(TAG, "updatePersonalMessageUnreadCound-likeCount:" + OfficSecrRedPointCountManager.likeCount);
                // 1. entrySet遍历，在键和值都需要时使用（最常用）,性能也比较好的一种方式
                for (Map.Entry<String, Integer> entry : customU2UMsgCountMap.entrySet()) {
                    BuglyLog.d(TAG, "updatePersonalMessageUnreadCound-entry.key:" + entry.getKey()
                            + " value:" + entry.getValue());
                    unreadCount += entry.getValue().intValue();
                }
                LogUtils.d(TAG, "updatePersonalMessageUnreadCound-unreadCount:" + unreadCount);
                mMainTabLayout.setMsgNum(unreadCount);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 过滤指定消息类型的自定义私聊消息,需要注意以下几个场景的消息未读数过滤
     * 1.IM登录后会拉取一次未读数
     * 2.接收到私聊消息会拉取一次未读数 MainActivity#onReceiveRecentContactChanged
     * 3.个人信息更新，会拉取一次未读数
     *
     * @param imMessages
     */
    public boolean filterCustomNotifyPersonalMessage(List<IMMessage> imMessages) {
        LogUtils.d(TAG, "filterCustomNotifyPersonalMessage-isChatting:" + NIMSessionStatusManager.getInstance().isChatting());
        boolean ringOnRecvMsg = true;
        if (null != imMessages) {
            LogUtils.d(TAG, "filterCustomNotifyPersonalMessage-imMessages.size:" + imMessages.size());
            //大部分情况下，imMessages.size=1
            if (1 == imMessages.size()) {
                IMMessage imMessage = imMessages.get(0);
                LogUtils.d(TAG, "filterCustomNotifyPersonalMessage-imMessage.msgType:" + imMessage.getMsgType());
                if (imMessage.getMsgType() == MsgTypeEnum.custom) {
                    CustomAttachment customAttachment = ((CustomAttachment) imMessage.getAttachment());
                    if (null != customAttachment) {
                        LogUtils.d(TAG, "filterCustomNotifyPersonalMessage-first=" + customAttachment.getFirst() + " second=" + customAttachment.getSecond());
                        if (customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_OFFICIAL_PRESENTATION_GIFT
                                || customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_UPDATE_USER_NOBLE_INFO
                                || customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_UPDATE_MUTE_INFO
                                || customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_NEW_FANS
                                || customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_NEW_FRIEND
                                || customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_LIKE_FIRST
                                || customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_QUICK_MATING_FIRST
                                || (customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_FIRST_CALL_UP
                                && customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_CALL_UP)) {
                            LogUtils.d(TAG, "filterCustomNotifyPersonalMessage-过滤非界面展示私聊自定义消息 first=" + customAttachment.getFirst());
                            //即使已读回执, 并不会删除未读红点数
                            NIMClient.getService(MsgService.class).sendMessageReceipt(imMessage.getSessionId(), imMessage);
                            //删除消息记录, 并不会删除界面上的红点消息数量
                            NIMClient.getService(MsgService.class).deleteChattingHistory(imMessage);
                            imMessage.setStatus(MsgStatusEnum.read);
                            NIMClient.getService(MsgService.class).updateIMMessageStatus(imMessage);
                            OfficSecrRedPointCountManager.addRedCountDelNum();
                            ringOnRecvMsg = false;
                        }

                    } else {
                        //通知没有customAttachment的情况
                        LogUtils.d(TAG, "filterCustomNotifyPersonalMessage-过滤通知没有customAttachment的情况");
                        //即使已读回执, 并不会删除未读红点数
                        NIMClient.getService(MsgService.class).sendMessageReceipt(imMessage.getSessionId(), imMessage);
                        //删除消息记录, 并不会删除界面上的红点消息数量
                        NIMClient.getService(MsgService.class).deleteChattingHistory(imMessage);
                        ringOnRecvMsg = false;
                    }
                }
            }
        }
        return ringOnRecvMsg;
    }

    /**
     * 过滤指定消息类型的自定义私聊消息,需要注意以下几个场景的消息未读数过滤
     * 1.IM登录后会拉取一次未读数
     * 2.接收到私聊消息会拉取一次未读数 MainActivity#onReceiveRecentContactChanged
     * 3.个人信息更新，会拉取一次未读数
     *
     * @param recentContacts
     */
    public boolean filterRecentContactMessage(List<RecentContact> recentContacts) {
        LogUtils.d(TAG, "filterRecentContactMessage-isChatting:" + NIMSessionStatusManager.getInstance().isChatting());
        boolean updateMsgUnReadCount = true;
        if (null != recentContacts) {
            LogUtils.d(TAG, "filterRecentContactMessage-imMessages.size:" + recentContacts.size());
            //大部分情况下，recentContacts.size=1
            if (1 == recentContacts.size()) {
                RecentContact recentContact = recentContacts.get(0);
                if (recentContact.getMsgType() == MsgTypeEnum.custom) {
                    CustomAttachment customAttachment = ((CustomAttachment) recentContact.getAttachment());
                    if (null != customAttachment) {
                        LogUtils.d(TAG, "filterRecentContactMessage first=" + customAttachment.getFirst() + " second:" + customAttachment.getSecond());
                        if (customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_OFFICIAL_PRESENTATION_GIFT
                                || customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_UPDATE_USER_NOBLE_INFO
                                || customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_UPDATE_MUTE_INFO
                                || (customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_FIRST_CALL_UP
                                && customAttachment.getSecond() == CustomAttachment.CUSTOM_MSG_SECOND_CALL_UP)) {
                            LogUtils.d(TAG, "filterRecentContactMessage-设置最近一条消息为已读 first=" + customAttachment.getFirst());
                            //设置最近一条消息为已读
                            recentContact.setMsgStatus(MsgStatusEnum.read);
                            updateMsgUnReadCount = false;
                        }
                    }
                }
            }
        }
        return updateMsgUnReadCount;
    }

    public boolean countRecentContactUnRead(RecentContact recentContact) {
        boolean countRecentContactUnRead = true;
        if (null == recentContact) {
            countRecentContactUnRead = false;
            return countRecentContactUnRead;
        }
        LogUtils.d(TAG, "countRecentContactUnRead-recentContact.msgType:" + recentContact.getMsgType());
        if (recentContact.getMsgType() == MsgTypeEnum.custom) {
            CustomAttachment customAttachment = ((CustomAttachment) recentContact.getAttachment());
            if (null != customAttachment) {
                LogUtils.d(TAG, "countRecentContactUnRead-first=" + customAttachment.getFirst());
                if (customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_OFFICIAL_PRESENTATION_GIFT
                        || customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_UPDATE_USER_NOBLE_INFO
                        || customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_UPDATE_MUTE_INFO) {
                    LogUtils.d(TAG, "countRecentContactUnRead-设置最近一条消息为已读 first=" + customAttachment.getFirst());
                    //设置最近一条消息为已读
                    recentContact.setMsgStatus(MsgStatusEnum.read);
                    countRecentContactUnRead = false;
                }
            }
        }
        return countRecentContactUnRead;
    }
}
