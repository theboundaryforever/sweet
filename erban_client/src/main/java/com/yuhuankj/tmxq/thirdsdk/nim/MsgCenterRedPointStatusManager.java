package com.yuhuankj.tmxq.thirdsdk.nim;

import com.tongdaxing.erban.libcommon.utils.LogUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 消息中心，新点赞、新好友、新粉丝消息对应红点管理器
 *
 * @author weihaitao
 * @date 2019/4/19
 */
public class MsgCenterRedPointStatusManager {

    private static final String TAG = MsgCenterRedPointStatusManager.class.getSimpleName();
    private static MsgCenterRedPointStatusManager instance = null;
    private List<OnRedPointStatusChangedListener> listeners = new ArrayList<>();

    private boolean showSignInRedPoint = false;

    public boolean isShowSignInRedPoint() {
        return showSignInRedPoint;
    }

    public void setShowSignInRedPoint(boolean showSignInRedPoint) {
        this.showSignInRedPoint = showSignInRedPoint;
        if (null != listeners) {
            for (OnRedPointStatusChangedListener listener : listeners) {
                listener.showRedPointOnSignIn();
            }
        }
    }

    private MsgCenterRedPointStatusManager() {

    }

    public static MsgCenterRedPointStatusManager getInstance() {
        if (null == instance) {
            instance = new MsgCenterRedPointStatusManager();
        }
        return instance;
    }

    /**
     * 初始化，主要初始化数据观察器
     */
    public void init() {
        LogUtils.d(TAG, "init");
    }

    /**
     * 添加红点状态更新监听器
     *
     * @param listeneer
     */
    public void addListener(OnRedPointStatusChangedListener listeneer) {
        if (null == listeners) {
            listeners = new ArrayList<>();
        }
        if (!listeners.contains(listeneer)) {
            listeners.add(listeneer);
            LogUtils.d(TAG, "注册监听器");
        } else {
            LogUtils.w(TAG, "请勿重复注册监听器");
        }
    }

    /**
     * 移除红点状态更新监听器
     *
     * @param listeneer
     */
    public void removeListener(OnRedPointStatusChangedListener listeneer) {

        if (null != listeners && listeners.contains(listeneer)) {
            listeners.remove(listeneer);
            LogUtils.d(TAG, "移除监听器");
        } else {
            LogUtils.w(TAG, "未注册该监听器，移除失败");
        }
    }


    /**
     * 资源释放，释放数据观察器
     */
    public void release() {
        if (null != listeners) {
            listeners.clear();
        }
    }

    /**
     * 接收到新的点赞通知，分配通知更新UI
     */
    public void onRecvNewLikeNotify() {
        LogUtils.d(TAG, "onRecvNewLikeNotify:");

        if (null != listeners) {
            for (OnRedPointStatusChangedListener listener : listeners) {
                listener.showRedPointOnNewLike();
            }
        }
    }

    /**
     * 接收到新的粉丝通知，分配通知更新UI
     */
    public void onRecvNewFanNotify() {
        LogUtils.d(TAG, "onRecvNewFanNotify:");

        if (null != listeners) {
            for (OnRedPointStatusChangedListener listener : listeners) {
                listener.showRedPointOnNewFan();
            }
        }
    }

    /**
     * 接收到新的好友通知，分配通知更新UI
     */
    public void onRecvNewFriendNotify() {
        LogUtils.d(TAG, "onRecvNewFriendNotify:");

        if (null != listeners) {
            for (OnRedPointStatusChangedListener listener : listeners) {
                listener.showRedPointOnNewFriend();
            }
        }
    }

    /**
     * 消息中心红点状态更新监听器
     */
    public interface OnRedPointStatusChangedListener {

        /**
         * 显示新粉丝红点
         */
        void showRedPointOnNewFan();

        /**
         * 显示新好友红点
         */
        void showRedPointOnNewFriend();

        /**
         * 显示新点赞红点
         */
        void showRedPointOnNewLike();

        /**
         * 签到红点
         */
        void showRedPointOnSignIn();

    }
}
